﻿namespace SIL.AARTO.PrintEngine
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reprintOneDocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendADocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.receiveDocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslLastUpdated = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(145)))), ((int)(((byte)(157)))), ((int)(((byte)(206)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.searchToolStripMenuItem,
            this.reprintOneDocToolStripMenuItem,
            this.sendADocToolStripMenuItem,
            this.receiveDocToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(934, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.searchToolStripMenuItem.Text = "Search";
            this.searchToolStripMenuItem.Click += new System.EventHandler(this.searchToolStripMenuItem_Click);
            // 
            // reprintOneDocToolStripMenuItem
            // 
            this.reprintOneDocToolStripMenuItem.Name = "reprintOneDocToolStripMenuItem";
            this.reprintOneDocToolStripMenuItem.Size = new System.Drawing.Size(151, 20);
            this.reprintOneDocToolStripMenuItem.Text = "Reprint Single Document";
            this.reprintOneDocToolStripMenuItem.Click += new System.EventHandler(this.reprintOneDocToolStripMenuItem_Click);
            // 
            // sendADocToolStripMenuItem
            // 
            this.sendADocToolStripMenuItem.Name = "sendADocToolStripMenuItem";
            this.sendADocToolStripMenuItem.Size = new System.Drawing.Size(113, 20);
            this.sendADocToolStripMenuItem.Text = "Send a Document";
            this.sendADocToolStripMenuItem.Visible = false;
            this.sendADocToolStripMenuItem.Click += new System.EventHandler(this.sendADocToolStripMenuItem_Click);
            // 
            // receiveDocToolStripMenuItem
            // 
            this.receiveDocToolStripMenuItem.Name = "receiveDocToolStripMenuItem";
            this.receiveDocToolStripMenuItem.Size = new System.Drawing.Size(118, 20);
            this.receiveDocToolStripMenuItem.Text = "Receive Document";
            this.receiveDocToolStripMenuItem.Visible = false;
            this.receiveDocToolStripMenuItem.Click += new System.EventHandler(this.receiveDocToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslLastUpdated});
            this.statusStrip1.Location = new System.Drawing.Point(0, 602);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(934, 22);
            this.statusStrip1.TabIndex = 10;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslLastUpdated
            // 
            this.tslLastUpdated.Name = "tslLastUpdated";
            this.tslLastUpdated.Size = new System.Drawing.Size(118, 17);
            this.tslLastUpdated.Text = "toolStripStatusLabel1";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(221)))), ((int)(((byte)(253)))));
            this.BackgroundImage = global::SIL.AARTO.PrintEngine.Properties.Resources.Bg1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(934, 624);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "AARTO Print Engine";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reprintOneDocToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendADocToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receiveDocToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslLastUpdated;
    }
}

