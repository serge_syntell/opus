﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using SIL.AARTO.BLL.Enum;
using SIL.AARTO.BLL.InfringerOption;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.PrintEngine
{
    public partial class frmBatchList : Form
    {
        public frmBatchList()
        {
            InitializeComponent();

            AartoDocumentTypeService tService = new AartoDocumentTypeService();
            List<AartoDocumentType> tData = tService.GetAll()
                .Where(dt => dt.AaRepDocOutgoing == true).OrderBy(dt => dt.AaDocTypeName).ToList();
            AartoDocumentType t = new AartoDocumentType();
            t.AaDocTypeId = -1;
            t.AaDocTypeName = "All";
            tData.Insert(0, t);
            cbxDocType.DataSource = tData;

            AartoDocumentStatusService sService = new AartoDocumentStatusService();
            List<AartoDocumentStatus> sData = sService.GetAll().ToList();
            //AartoDocumentStatus s = new AartoDocumentStatus();
            //s.AaDocStatusId = -1;
            //s.AaDocStatusName = "All";
            //sData.Insert(0, s);
            cbxStatus.DataSource = sData;

            AuthorityService authorityService = new AuthorityService();
            List<Authority> aData = authorityService.GetAll().OrderBy(dt => dt.AutName).ToList();
            Authority a = new Authority();
            a.AutIntNo = -1;
            a.AutCode = "";
            a.AutName = "All";
            aData.Insert(0, a);
            cbxLA.DataSource = aData;
        }

        private void frmBatchList_Load(object sender, EventArgs e)
        {

        }
        private DataTable SearchResult;
        private void btnSearch_Click(object sender, EventArgs e)
        {
            RetrieveData();
        }

        private void RetrieveData()
        {
            string batchNo = tbxBatchNo.Text;
            int typeId = (int)cbxDocType.SelectedValue;
            int statusId = (int)cbxStatus.SelectedValue;
            AartoDocBatchService aService = new AartoDocBatchService();
            Authority selectedAuthority = cbxLA.SelectedItem as Authority;
            int autIntNo = selectedAuthority.AutIntNo;
            using (IDataReader reader
                = aService.GetAARTODocBatchList(batchNo, typeId, statusId, autIntNo))
            {
                SearchResult = new DataTable();
                SearchResult.Load(reader);
                dgBatchList.AutoGenerateColumns = false;
                dgBatchList.DataSource = SearchResult;
            }
            AdjustUI();
        }
        private void dgBatchList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string CurrentUserName = GlobalVariates<User>.CurrentUser.UserLoginName;
            int colIndex = e.ColumnIndex;
            int batchId = (int)SearchResult.Rows[e.RowIndex]["AaDocBatchID"];
            switch (colIndex)
            {
                case 7:
                    if (PrintHelper.PrintConfirm())
                    {
                        this.Enabled = false;
                        int ifsIntNo = (int)SearchResult.Rows[e.RowIndex]["IfsIntNo"];
                        string aaDocBatchPath = SearchResult.Rows[e.RowIndex]["AaDocBatchPath"] as string;
                        string docxFilePath = AARTODocBatchHelper.GetDocxFilePath(ifsIntNo, aaDocBatchPath);

                        //string printerLocationName = ConfigurationManager.AppSettings["PrinterLocation"];
                        //PrinterLocationEnum printerLocation
                        //    = (PrinterLocationEnum)Enum.Parse(typeof(PrinterLocationEnum), printerLocationName);

                        //switch (printerLocation)
                        //{
                        //    case PrinterLocationEnum.Local:
                                bool b = PrintHelper.PrintPDF(docxFilePath);
                                if (b)
                                {
                                    AartoDocBatchService aService = new AartoDocBatchService();
                                    AartoDocBatch batch = aService.GetByAaDocBatchId(batchId);
                                    if (batch.AaDocStatusId == (int)AartoDocumentStatusList.Issued)
                                    {
                                        AARTODocumentManager.UpdateAARTODocumentBatchAfterPrinted(batchId, CurrentUserName);
                                        RetrieveData();
                                    }
                                }
                        //        break;
                        //    case PrinterLocationEnum.PF:
                        //        PrintHelper.PrintInPF(batchId);
                        //        break;
                        //}
                        this.Enabled = true;
                    }
                    break;
                case 8:
                    DocumentLayout docLayout = InitPdf();
                    AartoDocBatchService aartoDocBatchService = new AartoDocBatchService();
                    using (IDataReader reader = aartoDocBatchService.GetSummery(batchId))
                    {
                        if (reader.Read())
                        {
                            ShowPdfHeader(docLayout, reader);
                        }
                    }
                    byte[] pdfStream = ShowPdfBody(docLayout, batchId);
                    SavePdf(batchId, pdfStream);
                    break;
            }
        }
        private void SavePdf(int batchId, byte[] pdfStream)
        {
            //SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            //saveFileDialog1.Filter = "pdf files(*.pdf)|*.pdf";
            //saveFileDialog1.RestoreDirectory = true;
            //if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            //{
            //    string localFilePath = saveFileDialog1.FileName.ToString();
            //    using (FileStream fs = (FileStream)saveFileDialog1.OpenFile())
            //    {
            //        fs.Write(pdfStream, 0, pdfStream.Length);
            //    }
            //}
            string BatchSummaryPdfFolder = "BatchSummaryPdf";
            DirectoryInfo di = new DirectoryInfo(BatchSummaryPdfFolder);
            if (!di.Exists)
            {
                di.Create();
            }
            string summaryFileName = Path.Combine(BatchSummaryPdfFolder, batchId.ToString() + ".pdf");
            using (FileStream fs = new FileStream(summaryFileName, FileMode.Create))
            {
                fs.Write(pdfStream, 0, pdfStream.Length);
            }
            System.Diagnostics.Process.Start(summaryFileName);
        }
        private DocumentLayout InitPdf()
        {
            string reportPage = @"ReportFiles\BatchSummary.dplx";
            //string fullPath = Path.Combine(ConfigurationManager.AppSettings["ReportFilesDir"], reportPage);
            DocumentLayout docLayout = new DocumentLayout(reportPage);
            return docLayout;
        }
        private void ShowPdfHeader(DocumentLayout docLayout, IDataReader reader)
        {
            string autName = reader["AutName"] as string;
            string aaDocBatchNo = reader["AaDocBatchNo"] as string;
            string aaDocTypeName = reader["AaDocTypeName"] as string;
            string aaDocStatusName = reader["AaDocStatusName"] as string;
            string aaDocIssuedDate = reader["AaDocIssuedDate"].ToString();
            string aaDocPrintedDate = reader["AaDocPrintedDate"].ToString();
            string aaDocPostedDate = reader["AaDocPostedDate"].ToString();
            int noOfDocuments = (int)reader["NoOfDocuments"];

            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl;

            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblAuthName");
            lbl.Text = autName;
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblBatchNo");
            lbl.Text = aaDocBatchNo;
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblCurrentDateTime");
            lbl.Text = DateTime.Now.ToString();
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblDocType");
            lbl.Text = aaDocTypeName;
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblBatchStatus");
            lbl.Text = aaDocStatusName;
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblCountOfDocs");
            lbl.Text = noOfDocuments.ToString();
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblBatchIssuedDate");
            lbl.Text = aaDocIssuedDate;
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblBatchPrintedDate");
            lbl.Text = aaDocPrintedDate;
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblBatchPostedDate");
            lbl.Text = aaDocPostedDate;
        }
        private byte[] ShowPdfBody(DocumentLayout docLayout, int batchId)
        {
            StoredProcedureQuery query = (StoredProcedureQuery)docLayout.GetQueryById("Query");
            query.ConnectionString
                = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
            ParameterDictionary parameters = new ParameterDictionary();
            parameters.Add("BatchId", batchId);
            Document doc = docLayout.Run(parameters);
            byte[] buffer = doc.Draw();
            return buffer;
        }

        private void btnSetPosted_Click(object sender, EventArgs e)
        {
            DateTime dt = dateTimePicker1.Value;
            int k = 0;
            for (int i = 0; i < dgBatchList.Rows.Count; i++)
            {
                bool selection = (bool)dgBatchList.Rows[i].Cells["Selection"].FormattedValue;
                if (selection)
                {
                    k++;
                    DataRow row = SearchResult.Rows[i];
                    int batchId = (int)row["AaDocBatchID"];

                    AARTODocumentManager.UpdateAARTODocumentBatchAfterPosted(
                        batchId, dt, GlobalVariates<User>.CurrentUser.UserLoginName);
                }
            }
            if (k > 0)
            {
                string msg = string.Format("{0} batche(s) are updated.", k);
                MessageBox.Show(msg);
                RetrieveData();
            }
            else
            {
                MessageBox.Show("Please select item(s).");
            }
        }

        private void cbxStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgBatchList.DataSource = null;
            AdjustUI();
        }
        private void AdjustUI()
        {
            if (dgBatchList.Rows.Count > 0 &&
                (int)cbxStatus.SelectedValue == (int)AartoDocumentStatusList.Printed)
            {
                dateTimePicker1.Visible = true;
                btnSetPosted.Visible = true;
            }
            else
            {
                dateTimePicker1.Visible = false;
                btnSetPosted.Visible = false;
            }
        }
    }
}
