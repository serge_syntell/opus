﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.EntLib;
using System.Reflection;
using System.Configuration;

namespace SIL.AARTO.PrintEngine
{
    static class Program
    {
        public static string APP_TITLE = string.Empty;
        public static string APP_NAME = AartoProjectList.AARTOPrintEngine.ToString();
        public static string APP_DATABASE = string.Empty;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
            Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);

            // Check Last updated Version            
            string errorMessage;
            if (!CheckVersionManager.CheckVersion(AartoProjectList.AARTOPrintEngine, out errorMessage))
            {                
                EntLibLogger.WriteLog(LogCategory.General, null, errorMessage);
                MessageBox.Show(errorMessage);
                return;
            }

            // Write the started Log
            APP_TITLE = string.Format("{0} - Version {1} Last Updated {2} Started at: {3}",
                APP_NAME,
                Assembly.GetExecutingAssembly().GetName().Version.ToString(2),
                ProjectLastUpdated.AARTOPrintEngine, DateTime.Now);

            APP_DATABASE = EntLibLogger.GetServerAndDatabaseName(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());

            EntLibLogger.WriteLog(LogCategory.General, null, string.Format("{0}  {1}", APP_TITLE, APP_DATABASE));
            //Console.Title = APP_TITLE; 

            ceTe.DynamicPDF.Document.AddLicense("DPS70NPDFJJGEJFZ9ikRGHBua2M3Sl0Pwtw63QxBds5agxfAQMSTfCfEtiI2O6I13jHEsL6smMhYn63QJBLCZ3B8XtMbaTRJ+C1w");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);            
            Application.Run(new UserLogin());            
        }
    }
}
