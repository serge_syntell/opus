﻿namespace SIL.AARTO.PrintEngine
{
    partial class PrintOneDoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintOneDoc));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.textNoticeCode = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dgDocs = new System.Windows.Forms.DataGridView();
            this.InfringementNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AaDocTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Offender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OffenderIdNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NotOffenceDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VehicleRegisterNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocIssuedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Print = new System.Windows.Forms.DataGridViewButtonColumn();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDocs)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.textNoticeCode);
            this.flowLayoutPanel1.Controls.Add(this.btnSearch);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(836, 48);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Infringement No.";
            // 
            // textNoticeCode
            // 
            this.textNoticeCode.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textNoticeCode.Location = new System.Drawing.Point(94, 15);
            this.textNoticeCode.Name = "textNoticeCode";
            this.textNoticeCode.Size = new System.Drawing.Size(195, 20);
            this.textNoticeCode.TabIndex = 0;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSearch.Location = new System.Drawing.Point(295, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(133, 45);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dgDocs
            // 
            this.dgDocs.AllowUserToAddRows = false;
            this.dgDocs.AllowUserToDeleteRows = false;
            this.dgDocs.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgDocs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDocs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.InfringementNo,
            this.AaDocTypeName,
            this.Offender,
            this.OffenderIdNo,
            this.NotOffenceDate,
            this.VehicleRegisterNo,
            this.DocIssuedDate,
            this.Print});
            this.dgDocs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgDocs.Location = new System.Drawing.Point(0, 48);
            this.dgDocs.Name = "dgDocs";
            this.dgDocs.ReadOnly = true;
            this.dgDocs.Size = new System.Drawing.Size(836, 588);
            this.dgDocs.TabIndex = 1;
            this.dgDocs.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDocs_CellContentClick);
            // 
            // InfringementNo
            // 
            this.InfringementNo.DataPropertyName = "InfringementNo";
            this.InfringementNo.HeaderText = "Infringement No";
            this.InfringementNo.Name = "InfringementNo";
            this.InfringementNo.ReadOnly = true;
            // 
            // AaDocTypeName
            // 
            this.AaDocTypeName.DataPropertyName = "AaDocTypeName";
            this.AaDocTypeName.HeaderText = "Document Type";
            this.AaDocTypeName.Name = "AaDocTypeName";
            this.AaDocTypeName.ReadOnly = true;
            // 
            // Offender
            // 
            this.Offender.DataPropertyName = "Offender";
            this.Offender.HeaderText = "Offender";
            this.Offender.Name = "Offender";
            this.Offender.ReadOnly = true;
            // 
            // OffenderIdNo
            // 
            this.OffenderIdNo.DataPropertyName = "OffenderIdNo";
            this.OffenderIdNo.HeaderText = "Offender ID No";
            this.OffenderIdNo.Name = "OffenderIdNo";
            this.OffenderIdNo.ReadOnly = true;
            // 
            // NotOffenceDate
            // 
            this.NotOffenceDate.DataPropertyName = "NotOffenceDate";
            this.NotOffenceDate.HeaderText = "Offence Date";
            this.NotOffenceDate.Name = "NotOffenceDate";
            this.NotOffenceDate.ReadOnly = true;
            // 
            // VehicleRegisterNo
            // 
            this.VehicleRegisterNo.DataPropertyName = "VehicleRegisterNo";
            this.VehicleRegisterNo.HeaderText = "Vehicle Registration";
            this.VehicleRegisterNo.Name = "VehicleRegisterNo";
            this.VehicleRegisterNo.ReadOnly = true;
            // 
            // DocIssuedDate
            // 
            this.DocIssuedDate.DataPropertyName = "DocIssuedDate";
            this.DocIssuedDate.HeaderText = "Issued Date";
            this.DocIssuedDate.Name = "DocIssuedDate";
            this.DocIssuedDate.ReadOnly = true;
            // 
            // Print
            // 
            this.Print.HeaderText = "Print";
            this.Print.Name = "Print";
            this.Print.ReadOnly = true;
            this.Print.Text = "Print";
            this.Print.ToolTipText = "Print";
            this.Print.UseColumnTextForButtonValue = true;
            // 
            // PrintOneDoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(836, 636);
            this.Controls.Add(this.dgDocs);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PrintOneDoc";
            this.Text = "PrintOneDoc";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDocs)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textNoticeCode;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridView dgDocs;
        private System.Windows.Forms.DataGridViewTextBoxColumn InfringementNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn AaDocTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Offender;
        private System.Windows.Forms.DataGridViewTextBoxColumn OffenderIdNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn NotOffenceDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn VehicleRegisterNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocIssuedDate;
        private System.Windows.Forms.DataGridViewButtonColumn Print;
    }
}