﻿using System;
using System.Data;
using System.Windows.Forms;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.PrintEngine
{
    public partial class PrintOneDoc : Form
    {
        public PrintOneDoc()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            RetrieveData();
        }
        private DataTable SearchResult;
        private void RetrieveData()
        {
            string noticeCode = textNoticeCode.Text;
            AartoDocBatchService aService = new AartoDocBatchService();
            using (IDataReader reader
                = aService.GetAartoDocsByNoticeCode(noticeCode))
            {
                SearchResult = new DataTable();
                SearchResult.Load(reader);
                dgDocs.AutoGenerateColumns = false;
                dgDocs.DataSource = SearchResult;
            }
        }
        private void dgDocs_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int colIndex = e.ColumnIndex;
            int rowIndex = e.RowIndex;
            long docId = (long)SearchResult.Rows[rowIndex]["DocId"];
            switch (colIndex)
            {
                case 6:
                    this.Enabled = false;
                    string aaDocBatchPath = SearchResult.Rows[rowIndex]["DocPath"] as string;
                    if (!string.IsNullOrEmpty(aaDocBatchPath))
                    {
                        int ifsIntNo = (int)SearchResult.Rows[rowIndex]["IfsIntNo"];
                        string docxFilePath = AARTODocBatchHelper.GetDocxFilePath(ifsIntNo, aaDocBatchPath);
                        bool b = PrintHelper.PrintConfirm();
                        if (b)
                        {
                            PrintHelper.PrintPDF(docxFilePath);
                            switch (SearchResult.Rows[rowIndex]["DocTable"] as string)
                            {
                                case "Notice":
                                    AartoNoticeDocumentService nService = new AartoNoticeDocumentService();
                                    AartoNoticeDocument nDoc = nService.GetByAaNotDocId(docId);
                                    if (nDoc.AaDocStatusId == (int)AartoDocumentStatusList.Issued)
                                    {
                                        nDoc.AaDocStatusId = (int)AartoDocumentStatusList.Printed;
                                        nDoc.AaDocPrintedDate = DateTime.Now;
                                        nService.Update(nDoc);
                                    }
                                    break;
                                case "Rep":
                                    AartoRepDocumentService rService = new AartoRepDocumentService();
                                    AartoRepDocument rDoc = rService.GetByAaRepDocId(docId);
                                    if (rDoc.AaDocStatusId == (int)AartoDocumentStatusList.Issued)
                                    {
                                        rDoc.AaDocStatusId = (int)AartoDocumentStatusList.Printed;
                                        rDoc.AaRepDocPrintedDate = DateTime.Now;
                                        rService.Update(rDoc);
                                    }
                                    break;
                            }
                        }
                    }
                    else
                    {
                        string msg = "The print file for this document cannot be found";
                        MessageBox.Show(msg);
                    }
                    this.Enabled = true;
                    break;
            }
        }
    }
}
