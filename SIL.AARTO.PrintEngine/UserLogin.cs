﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SIL.AARTO.PrintEngine
{
    public partial class UserLogin : WinformUC.UserLogin
    {
        public UserLogin()
        {
            InitializeComponent();
        }
        public override void OpenMainForm()
        {
            frmMain mainForm = new frmMain();
            mainForm.Show();
        }
    }
}
