﻿using System.Configuration;
using System.Reflection;
using System.Windows.Forms;

using System.IO;
using System;

//using Microsoft.Office.Interop.Word;

using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.PrintEngine
{
    public abstract class PrintHelper
    {
        public static void PrintInPF(int batchId)
        {
        }
        //public static bool PrintInLocal(object filePath)
        //{
        //    try
        //    {
        //        string fp = filePath as string;
        //        if (!File.Exists(fp))
        //        {
        //            string msg = string.Format("The file ({0}) not found", fp);
        //            MessageBox.Show(msg);
        //            return false;
        //        }
        //        object nullobj = Missing.Value;
        //        Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
        //        Document doc = wordApp.Documents.Open(ref filePath,
        //                                    ref nullobj, ref nullobj, ref nullobj,
        //                                    ref nullobj, ref nullobj, ref nullobj,
        //                                    ref nullobj, ref nullobj, ref nullobj,
        //                                    ref nullobj, ref nullobj, ref nullobj,
        //                                    ref nullobj, ref nullobj, ref nullobj);
        //        doc.Activate();
        //        bool showDialogBeforePrint = bool.Parse(ConfigurationManager.AppSettings["ShowDialogBeforePrint"]);
        //        int dialogResult;
        //        if (showDialogBeforePrint)
        //        {
        //            dialogResult = wordApp.Dialogs[Microsoft.Office.Interop.Word.WdWordDialog.wdDialogFilePrint].Show(ref nullobj);
        //        }
        //        else
        //        {
        //            dialogResult = -1;
        //            doc.PrintOut(ref nullobj, ref nullobj, ref nullobj, ref nullobj,
        //                             ref nullobj, ref nullobj, ref nullobj, ref nullobj,
        //                             ref nullobj, ref nullobj, ref nullobj, ref nullobj,
        //                             ref nullobj, ref nullobj, ref nullobj, ref nullobj,
        //                             ref nullobj, ref nullobj);
        //        }

        //        doc.Close(ref nullobj, ref nullobj, ref nullobj);
        //        wordApp.Quit(ref nullobj, ref nullobj, ref nullobj);

        //        bool b = (dialogResult == -1); // 0 is cancel, -1 is print
        //        return b;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        public static bool PrintPDF(object filePath)
        {
            try
            {
                PdfPrintOperation operation = new PdfPrintOperation(filePath.ToString());
                Retryer retryer = new Retryer(operation);
                retryer.Perform(10, 5000);
                // Print the file.
                operation.Print(); 
                return true;
            }
            catch (Exception)
            {
                return false;
            }    
        }

        public static bool PrintConfirm()
        {
            string message = "Are you sure you wish to print?";
            string caption = "Are you sure you wish to print?";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            result = MessageBox.Show(message, caption, buttons);

            return (result == System.Windows.Forms.DialogResult.Yes);
        }
    }


}
