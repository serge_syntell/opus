﻿namespace SIL.AARTO.PrintEngine
{
    partial class frmBatchList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBatchList));
            this.tbxBatchNo = new System.Windows.Forms.TextBox();
            this.labStatus = new System.Windows.Forms.Label();
            this.cbxStatus = new System.Windows.Forms.ComboBox();
            this.labSearch = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dgBatchList = new System.Windows.Forms.DataGridView();
            this.Selection = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BatchName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocumentType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocsCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateIssued = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DatePrinted = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DatePosted = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Print = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Summary = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.cbxDocType = new System.Windows.Forms.ComboBox();
            this.labDocType = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbxLA = new System.Windows.Forms.ComboBox();
            this.btnSetPosted = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBatchList)).BeginInit();
            this.SuspendLayout();
            // 
            // tbxBatchNo
            // 
            this.tbxBatchNo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tbxBatchNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxBatchNo.Location = new System.Drawing.Point(709, 15);
            this.tbxBatchNo.Name = "tbxBatchNo";
            this.tbxBatchNo.Size = new System.Drawing.Size(100, 20);
            this.tbxBatchNo.TabIndex = 0;
            // 
            // labStatus
            // 
            this.labStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labStatus.AutoSize = true;
            this.labStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labStatus.Location = new System.Drawing.Point(408, 18);
            this.labStatus.Name = "labStatus";
            this.labStatus.Size = new System.Drawing.Size(73, 13);
            this.labStatus.TabIndex = 3;
            this.labStatus.Text = "Select Status:";
            // 
            // cbxStatus
            // 
            this.cbxStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbxStatus.DisplayMember = "AaDocStatusName";
            this.cbxStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxStatus.FormattingEnabled = true;
            this.cbxStatus.Location = new System.Drawing.Point(487, 14);
            this.cbxStatus.Name = "cbxStatus";
            this.cbxStatus.Size = new System.Drawing.Size(105, 21);
            this.cbxStatus.TabIndex = 5;
            this.cbxStatus.ValueMember = "AaDocStatusId";
            this.cbxStatus.SelectedIndexChanged += new System.EventHandler(this.cbxStatus_SelectedIndexChanged);
            // 
            // labSearch
            // 
            this.labSearch.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labSearch.AutoSize = true;
            this.labSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSearch.Location = new System.Drawing.Point(613, 18);
            this.labSearch.Name = "labSearch";
            this.labSearch.Size = new System.Drawing.Size(90, 13);
            this.labSearch.TabIndex = 6;
            this.labSearch.Text = "Search for Batch:";
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(821, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(110, 44);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 9;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 134F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 93F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 116F));
            this.tableLayoutPanel1.Controls.Add(this.dgBatchList, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePicker1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnSearch, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbxBatchNo, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.labSearch, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbxStatus, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.labStatus, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbxDocType, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.labDocType, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbxLA, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnSetPosted, 3, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(934, 624);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // dgBatchList
            // 
            this.dgBatchList.AllowUserToAddRows = false;
            this.dgBatchList.AllowUserToDeleteRows = false;
            this.dgBatchList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgBatchList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgBatchList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Selection,
            this.BatchName,
            this.DocumentType,
            this.DocsCount,
            this.DateIssued,
            this.DatePrinted,
            this.DatePosted,
            this.Print,
            this.Summary});
            this.tableLayoutPanel1.SetColumnSpan(this.dgBatchList, 9);
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgBatchList.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgBatchList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgBatchList.Location = new System.Drawing.Point(3, 53);
            this.dgBatchList.Name = "dgBatchList";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgBatchList.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgBatchList.RowHeadersVisible = false;
            this.dgBatchList.RowHeadersWidth = 4;
            this.dgBatchList.Size = new System.Drawing.Size(928, 520);
            this.dgBatchList.TabIndex = 5;
            this.dgBatchList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBatchList_CellContentClick);
            // 
            // Selection
            // 
            this.Selection.FillWeight = 25.38071F;
            this.Selection.HeaderText = "";
            this.Selection.Name = "Selection";
            // 
            // BatchName
            // 
            this.BatchName.DataPropertyName = "AaDocBatchNo";
            this.BatchName.FillWeight = 108.291F;
            this.BatchName.HeaderText = "Batch No";
            this.BatchName.Name = "BatchName";
            this.BatchName.ReadOnly = true;
            // 
            // DocumentType
            // 
            this.DocumentType.DataPropertyName = "AaDocTypeName";
            this.DocumentType.FillWeight = 108.291F;
            this.DocumentType.HeaderText = "Document Type";
            this.DocumentType.Name = "DocumentType";
            this.DocumentType.ReadOnly = true;
            // 
            // DocsCount
            // 
            this.DocsCount.DataPropertyName = "CountsOfDoc";
            this.DocsCount.FillWeight = 108.291F;
            this.DocsCount.HeaderText = "No. of Documents";
            this.DocsCount.Name = "DocsCount";
            this.DocsCount.ReadOnly = true;
            // 
            // DateIssued
            // 
            this.DateIssued.DataPropertyName = "AaDocIssuedDate";
            this.DateIssued.FillWeight = 108.291F;
            this.DateIssued.HeaderText = "Date Issued";
            this.DateIssued.Name = "DateIssued";
            this.DateIssued.ReadOnly = true;
            // 
            // DatePrinted
            // 
            this.DatePrinted.DataPropertyName = "AaDocPrintedDate";
            this.DatePrinted.FillWeight = 108.291F;
            this.DatePrinted.HeaderText = "Date Printed";
            this.DatePrinted.Name = "DatePrinted";
            this.DatePrinted.ReadOnly = true;
            // 
            // DatePosted
            // 
            this.DatePosted.DataPropertyName = "AaDocPostedDate";
            this.DatePosted.FillWeight = 108.291F;
            this.DatePosted.HeaderText = "Date Posted";
            this.DatePosted.Name = "DatePosted";
            this.DatePosted.ReadOnly = true;
            // 
            // Print
            // 
            this.Print.HeaderText = "Print";
            this.Print.Name = "Print";
            this.Print.Text = "Print";
            this.Print.ToolTipText = "Print";
            this.Print.UseColumnTextForButtonValue = true;
            // 
            // Summary
            // 
            this.Summary.HeaderText = "Summary";
            this.Summary.Name = "Summary";
            this.Summary.Text = "Summary";
            this.Summary.ToolTipText = "Summary";
            this.Summary.UseColumnTextForButtonValue = true;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.dateTimePicker1, 3);
            this.dateTimePicker1.Location = new System.Drawing.Point(3, 590);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(274, 20);
            this.dateTimePicker1.TabIndex = 16;
            // 
            // cbxDocType
            // 
            this.cbxDocType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbxDocType.DisplayMember = "AaDocTypeName";
            this.cbxDocType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxDocType.FormattingEnabled = true;
            this.cbxDocType.Location = new System.Drawing.Point(283, 14);
            this.cbxDocType.Name = "cbxDocType";
            this.cbxDocType.Size = new System.Drawing.Size(105, 21);
            this.cbxDocType.TabIndex = 14;
            this.cbxDocType.ValueMember = "AaDocTypeId";
            // 
            // labDocType
            // 
            this.labDocType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labDocType.AutoSize = true;
            this.labDocType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labDocType.Location = new System.Drawing.Point(158, 18);
            this.labDocType.Name = "labDocType";
            this.labDocType.Size = new System.Drawing.Size(119, 13);
            this.labDocType.TabIndex = 2;
            this.labDocType.Text = "Select Document Type:";
            this.labDocType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "LA: ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbxLA
            // 
            this.cbxLA.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbxLA.DisplayMember = "AutName";
            this.cbxLA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxLA.FormattingEnabled = true;
            this.cbxLA.Location = new System.Drawing.Point(38, 14);
            this.cbxLA.Name = "cbxLA";
            this.cbxLA.Size = new System.Drawing.Size(105, 21);
            this.cbxLA.TabIndex = 18;
            this.cbxLA.ValueMember = "AutCode";
            // 
            // btnSetPosted
            // 
            this.btnSetPosted.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetPosted.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetPosted.Location = new System.Drawing.Point(283, 579);
            this.btnSetPosted.Name = "btnSetPosted";
            this.btnSetPosted.Size = new System.Drawing.Size(105, 42);
            this.btnSetPosted.TabIndex = 15;
            this.btnSetPosted.Text = "Set as Posted";
            this.btnSetPosted.UseVisualStyleBackColor = true;
            this.btnSetPosted.Click += new System.EventHandler(this.btnSetPosted_Click);
            // 
            // frmBatchList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(934, 624);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmBatchList";
            this.Text = "AARTO PrintEngine - Batch List";
            this.Load += new System.EventHandler(this.frmBatchList_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBatchList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbxBatchNo;
        private System.Windows.Forms.Label labStatus;
        private System.Windows.Forms.ComboBox cbxStatus;
        private System.Windows.Forms.Label labSearch;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labDocType;
        private System.Windows.Forms.ComboBox cbxDocType;
        private System.Windows.Forms.DataGridView dgBatchList;
        private System.Windows.Forms.Button btnSetPosted;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxLA;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Selection;
        private System.Windows.Forms.DataGridViewTextBoxColumn BatchName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocumentType;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocsCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateIssued;
        private System.Windows.Forms.DataGridViewTextBoxColumn DatePrinted;
        private System.Windows.Forms.DataGridViewTextBoxColumn DatePosted;
        private System.Windows.Forms.DataGridViewButtonColumn Print;
        private System.Windows.Forms.DataGridViewButtonColumn Summary;
    }
}