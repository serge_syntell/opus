﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SIL.AARTO.PrintEngine.AARTOPrintingDocument
{
    public class PrintingDocument : IPrintingDocument
    {
        public string Parent
        {
            get;
            set;
        }        

        public void Print()
        {
            MessageBox.Show("Call print.");
        }

        public void GenerateToPDF()
        {
            MessageBox.Show("Call GenerateToPDF.");
        }

        public virtual void Replace()
        {
            MessageBox.Show("Call Replace.");
        }
    }    
}
