﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Configuration;
using System.Data;
using System.Xml;

using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.PrintEngine.AARTOPrintingDocument
{
    public class PrintDocumentManager
    {
        public String RootFolder
        {
            get { return ConfigurationSettings.AppSettings["RootFolder"]; }
        }

        public PrintDocumentManager()
        {
            
        }

        //public void ReplaceData(string fileName, string customXMLTemplate, string companyName)
        //{
        //    using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(fileName, true))
        //    {
        //        MainDocumentPart mainPart = wordDoc.MainDocumentPart;
        //        mainPart.DeleteParts<CustomXmlPart>(mainPart.CustomXmlParts);
        //        CustomXmlPart customXmlPart = mainPart.AddCustomXmlPart(CustomXmlPartType.CustomXml);
        //        string customXML = customXMLTemplate;
        //        Regex regexCompanyName = new Regex("!CompanyName!");
        //        Regex regexContactName = new Regex("!ContactName!");
        //        Regex regexContactTitle = new Regex("!ContactTitle!");
        //        Regex regexContactPhone = new Regex("!Phone!");
        //        customXML = regexCompanyName.Replace(customXML, companyName);
        //        customXML = regexContactName.Replace(customXML, "SIL");
        //        customXML = regexContactTitle.Replace(customXML, "david");
        //        customXML = regexContactPhone.Replace(customXML, "021-123456");
        //        using (StreamWriter ts = new StreamWriter(customXmlPart.GetStream())) ts.Write(customXML);
        //        wordDoc.MainDocumentPart.Document.Save();
        //        wordDoc.Close();
        //    }
        //}

        //public string ReplaceData(string customXML, DataRow row)
        //{
        //    XmlDocument xmlDoc = new XmlDocument();
        //    xmlDoc.LoadXml(customXML);

        //    XmlNodeList nodeList = xmlDoc.FirstChild.ChildNodes;
        //    if (nodeList != null && nodeList.Count > 0)
        //    {
        //        foreach (XmlNode node in nodeList)
        //        {
        //            if (row[node.Name] != null && row[node.Name] != DBNull.Value)
        //            {
        //                node.InnerText = row[node.Name].ToString();
        //            }
        //        }
        //    }
        //    return customXML;
        //}

        //public string ReplaceData(string customXML, Dictionary<string, object> dictionary)
        //{            
        //    XmlDocument xmlDoc = new XmlDocument();
        //    xmlDoc.LoadXml(customXML);

        //    XmlNodeList nodeList = xmlDoc.FirstChild.ChildNodes;
        //    if (nodeList != null && nodeList.Count > 0)
        //    {
        //        foreach (XmlNode node in nodeList)
        //        {
        //            if (dictionary.ContainsKey(node.Name.Trim().ToLower()))
        //            {
        //                node.InnerText = dictionary[node.Name.Trim().ToLower()].ToString();
        //            }
        //        }
        //    }
        //    return customXML;
        //}

    }
}
