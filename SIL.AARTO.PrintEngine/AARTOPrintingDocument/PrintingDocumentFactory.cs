﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.PrintEngine.AARTOPrintingDocument
{
    public class PrintingDocumentFactory
    {
        public static PrintingDocument CreatePrintingDocumentInstance(int documentType)
        {
            string assmName = "SIL.AARTO.PrintEngine";
            string className = ((AartoDocumentTypeList)documentType).ToString();
            Assembly assm = Assembly.Load(assmName);
            return (PrintingDocument)assm.CreateInstance(assmName + ".AARTOPrintingDocument.Documents" + className);           
        }
    }
}
