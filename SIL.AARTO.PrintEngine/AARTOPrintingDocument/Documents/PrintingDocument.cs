﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using SIL.AARTO.DAL.Entities;
using System.Data.SqlClient;

namespace SIL.AARTO.PrintEngine.AARTOPrintingDocument
{
    public class PrintingDocument : IPrintingDocument
    {
        private AartoDocumentTypeList _documentType;

        public XmlDocument customXml
        {
            get;
            set;
        }        

        public PrintingDocument(AartoDocumentTypeList documentType)
        {
            this._documentType = documentType;
        }      

        public virtual Dictionary<string, object> FillDictionaryWithData(XmlDocument customXml, SqlDataReader dr)
        {            
            Dictionary<string, object> dic = new Dictionary<string, object>();
            return dic;
        }
    }    
}
