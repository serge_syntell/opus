﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility;
using System.IO;

namespace SIL.AARTO.PrintEngine
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            tslLastUpdated.Text = string.Format("{0} {1}", Program.APP_TITLE, Program.APP_DATABASE);
        }
        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBatchList f = new frmBatchList();
            f.MdiParent = this;
            f.Show();
        }
        private void reprintOneDocToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrintOneDoc f = new PrintOneDoc();
            f.MdiParent = this;
            f.Show();
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void sendADocToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string fileToSend = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Docs/Batch.pebs");
            using (IndabaClientUtil indabaClientUtil = new IndabaClientUtil())
            {
                indabaClientUtil.SendADoc(fileToSend);
                MessageBox.Show("Send OK.");
            }
        }

        private void receiveDocToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (IndabaClientUtil indabaClientUtil = new IndabaClientUtil())
            {
                List<string> fileReceivedList = indabaClientUtil.GetReceivedFiles();
                StringBuilder sb = new StringBuilder();
                foreach (var fileName in fileReceivedList)
                {
                    if (sb.Length > 0)
                        sb.Append(",");
                    sb.Append(fileName);
                }
                MessageBox.Show(sb.ToString());
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.MdiClient mc = GetMdiClient(this);
            if (mc != null)
            {

                mc.BackColor = Color.FromArgb(213, 221, 253);
                mc.Invalidate();
            }
        }
        public static System.Windows.Forms.MdiClient GetMdiClient(System.Windows.Forms.Form f)
        {
            foreach (System.Windows.Forms.Control c in f.Controls)
                if (c is System.Windows.Forms.MdiClient)
                    return (System.Windows.Forms.MdiClient)c;
            return null;
        }
    }
}
