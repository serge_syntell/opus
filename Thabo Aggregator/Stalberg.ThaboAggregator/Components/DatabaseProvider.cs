using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Stalberg.ThaboAggregator.Objects;
using Stalberg.TMS_TPExInt.Objects;
using Stalberg.TMS;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Diagnostics;

namespace Stalberg.ThaboAggregator.Components
{
    /// <summary>
    /// Represents the data access layer for the Thabo Aggregator
    /// </summary>
    internal class DatabaseProvider
    {
        // Fields
        private SqlConnection con;
        private string LastBatchNumber = string.Empty;
        private int increment;

        /// <summary>
        /// Tries to connect to the database
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if a connection is successful to the database.
        /// </returns>
        public bool Connect()
        {
            bool response = false;

            try
            {
                if (this.con != null)
                    this.con.Dispose();

                this.con = new SqlConnection(ConfigurationManager.ConnectionStrings["Thabo"].ConnectionString);
                this.con.Open();

                response = true;
                //Logger.Write("Database connection re-established.", "General", (int)TraceEventType.Information);
            }
            catch (Exception ex)
            {
                Logger.Write("There was a problem connecting to the SQL Server." + ex.Message, "General", (int)TraceEventType.Critical);
            }
            finally
            {
                if (this.con != null)
                    this.con.Close();
            }

            return response;
        }

        /// <summary>
        /// Gets a value indicating whether this instance has made a successful connection to the database.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is connected; otherwise, <c>false</c>.
        /// </value>
        public bool IsConnected
        {
            get { return (this.con != null); }
        }

        /// <summary>
        /// Updates all the data in the Authority.
        /// </summary>
        /// <param name="data">The data to update.</param>
        /// <param name="isFullyProcessed">if set to <c>true</c> [is fully processed].</param>
        /// <param name="errorMessage">The error message.</param>
        //public void UpdateData(FineExchangeDataFile data, ref bool isFullyProcessed)
        public void UpdateData(FineExchangeDataFile data, ref bool isFullyProcessed, ref string errorMessage)
       {
            // Check the Authority
            bool failed = this.UpdateAuthority(data, ref errorMessage);
            if (failed)
            {
                //mrs 20090115 by ref returns any system error messages
                isFullyProcessed = false;
                return;
            }

            // mrs 20080714 need some error checking here
            // mrs 20090115 added the errorMessage
            switch (data.Authority.ID) // there was a major problem finding the authority
            {
                case 0:
                    errorMessage = "Unknown error in stored procedure";
                    isFullyProcessed = false;
                    return;
                case -1:
                    errorMessage = "problem with insert in region table";
                    isFullyProcessed = false;
                    return;
                case -2:
                    errorMessage = "problem with insert in metro table";
                    isFullyProcessed = false;
                    return;
                case -3:
                    errorMessage = "problem with update in authority table";
                    isFullyProcessed = false;
                    return;
                case -4:
                    errorMessage = "problem with insert in authority table";
                    isFullyProcessed = false;
                    return;
                case -5:
                    errorMessage = "problem with insert in authorityEasyPay table";
                    isFullyProcessed = false;
                    return;
                default:
                    // we likes it
                    isFullyProcessed = true;
                    break;
            } //end of switch

            // Check the Notice
            failed = this.UpdateNotice(data);
            if (failed)
            {
                isFullyProcessed = false;
                return;
            }

            // Add the EasyPay transaction
            failed = this.UpdateEasyPay(data);
            if (failed)
            {
                isFullyProcessed = false;
                return;
            }

            //it got all the way through without errors
            isFullyProcessed = true;
        }

        private bool UpdateEasyPay(FineExchangeDataFile data)
        {
            bool failed = false;
            try
            {
                var com = new SqlCommand("ImportEasyPayTran", this.con);
                com.CommandType = CommandType.StoredProcedure;
                this.con.Open();

                foreach (EasyPayTransaction tran in data.Authority.EasyPayTransactions)
                {
                    com.Parameters.Clear();
                    com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = tran.NoticeID;
                    com.Parameters.Add("@EasyPayNo", SqlDbType.VarChar, 20).Value = tran.EasyPayNumber;
                    com.Parameters.Add("@ExpiryDate", SqlDbType.DateTime, 8).Value = tran.ExpiryDate;
                    com.Parameters.Add("@EPAction", SqlDbType.Char, 1).Value = tran.EasyPayAction;
                    com.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = tran.Amount;

                    com.ExecuteNonQuery();
                }

                com.Dispose();
            }
            catch (Exception ex)
            {
                Logger.Write(string.Format("Error while updating EasyPay ({0})\n{1}", ex.Message, ex.StackTrace), "General", (int)TraceEventType.Critical);
                failed = true;
            }
            finally
            {
                this.con.Close();
            }
            return failed;
        }

        private bool UpdateNotice(FineExchangeDataFile data)
        {
            int notIntNo = 0;
            int oldNotIntNo = 0;
            bool failed = false;
            try
            {
                //FT Set up Batch Number
                string strBatchNumber = "TBO_" + DateTime.Now.ToString("yyyyMMdd_HHmm");
                if(LastBatchNumber ==  strBatchNumber)
                {
                    increment ++;
                }
                else
                {
                    LastBatchNumber =  strBatchNumber;
                }
                data.BatchNumber = LastBatchNumber + increment.ToString().PadLeft(2, '0');


                SqlCommand com = new SqlCommand("ImportNotice", this.con);
                com.CommandType = CommandType.StoredProcedure;
                this.con.Open();

                foreach (EasyPayFineData fine in data.Authority.Fines)
                {
                    fine.UpdateDate = DateTime.Now;
                    // Check if the Notice exists and update it if necessary, return the ID
                    com.CommandText = "ImportNotice";
                    com.Parameters.Clear();
                    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = fine.Authority.ID;
                    com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = fine.TicketNumber;
                    com.Parameters.Add("@OffenceDate", SqlDbType.DateTime, 8).Value = fine.OffenceDate;
                    if (DateTime.Compare(fine.PaymentDate, DateTime.MinValue) > 0)
                        com.Parameters.Add("@PaymentDate", SqlDbType.DateTime, 8).Value = fine.PaymentDate;
                    com.Parameters.Add("@FilmNo", SqlDbType.VarChar, 10).Value = fine.FilmNumber;
                    com.Parameters.Add("@FrameNo", SqlDbType.VarChar, 4).Value = fine.FrameNumber;
                    com.Parameters.Add("@Location", SqlDbType.VarChar, 60).Value = fine.Location;
                    com.Parameters.Add("@AccusedName", SqlDbType.VarChar, 111).Value = fine.OwnerName;
                    com.Parameters.Add("@AccusedID", SqlDbType.VarChar, 25).Value = fine.OwnerID;
                    com.Parameters.Add("@RegNo", SqlDbType.VarChar, 10).Value = fine.RegNo;
                    com.Parameters.Add("@CameraSerialNo", SqlDbType.VarChar, 18).Value = fine.CameraSerialNumber;
                    com.Parameters.Add("@Speed1", SqlDbType.Int, 4).Value = fine.Speed1;
                    com.Parameters.Add("@Speed2", SqlDbType.Int, 4).Value = fine.Speed2;
                    com.Parameters.Add("@SpeedZone", SqlDbType.Int, 4).Value = fine.SpeedZone;
                    com.Parameters.Add("@NotTEBatchNo", SqlDbType.VarChar, 20).Value = data.BatchNumber;
                    com.Parameters.Add("@NotRowChanged", SqlDbType.DateTime, 8).Value = fine.UpdateDate;


                    oldNotIntNo = fine.NoticeID;
                    notIntNo = int.Parse(com.ExecuteScalar().ToString());
                    fine.NoticeID = notIntNo;

                    // Check the Charge and update it if necessary
                    com.CommandText = "ImportCharge";
                    com.Parameters.Clear();
                    com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
                    com.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = fine.FineAmount;
                    com.Parameters.Add("@Status", SqlDbType.Int, 4).Value = fine.Status;
                    com.Parameters.Add("@StatusDescr", SqlDbType.VarChar, 100).Value = fine.StatusDescription;
                    com.Parameters.Add("@OffenceCode", SqlDbType.VarChar, 15).Value = fine.OffenceCode;
                    com.Parameters.Add("@OffenceDescr", SqlDbType.VarChar, 50).Value = fine.OffenceDescription;
                    com.Parameters.Add("@ChgTEBatchNo", SqlDbType.VarChar, 20).Value = data.BatchNumber;
                    com.Parameters.Add("@ChgRowChanged", SqlDbType.DateTime, 8).Value = fine.UpdateDate;

                    if (com.ExecuteNonQuery() > 0)
                        fine.UpdateSuccess = true;


                    // Update the ID for all the EasyPay transactions that relate to this notice
                    foreach (EasyPayTransaction tran in data.Authority.EasyPayTransactions)
                    {
                        if (tran.NoticeID.Equals(oldNotIntNo))
                            tran.NoticeID = notIntNo;
                    }
                }

                com.Dispose();
            }
            catch (Exception ex)
            {
                Logger.Write(string.Format("Error while updating Notice ({0})\n{1}", ex.Message, ex.StackTrace), "General", (int)TraceEventType.Critical);
                failed = true;
            }
            finally
            {
                this.con.Close();
            }
            return failed;
        }

        private bool UpdateAuthority(FineExchangeDataFile data, ref string errorMessage)
        {
            bool failed = false;

            try
            {
                var com = new SqlCommand("ImportUpdateAuthority", this.con);
                com.CommandType = System.Data.CommandType.StoredProcedure;
                com.Parameters.Add("@AutCode", SqlDbType.VarChar, 3).Value = data.Authority.AuthorityCode; //this is returning null
                com.Parameters.Add("@AutName", SqlDbType.VarChar, 50).Value = data.Authority.Name;
                com.Parameters.Add("@AutNo", SqlDbType.VarChar, 6).Value = data.Authority.AuthorityNumber;
                com.Parameters.Add("@MachineName", SqlDbType.VarChar, 5).Value = data.MachineName;
                if (data.Authority.IsEasyPay)
                    com.Parameters.Add("@EasyPayReceiptID", SqlDbType.Int, 4).Value = data.Authority.ReceiverIdentifier;
                else
                    com.Parameters.Add("@EasyPayReceiptID", SqlDbType.Int, 4).Value = null;
                com.Parameters.Add("@RegCode", SqlDbType.VarChar, 3).Value = data.Authority.RegionCode;
                com.Parameters.Add("@RegName", SqlDbType.VarChar, 100).Value = data.Authority.RegionName;
                com.Parameters.Add("@RegTel", SqlDbType.VarChar, 30).Value = data.Authority.RegionPhone;
                com.Parameters.Add("@MetroCode", SqlDbType.VarChar, 3).Value = data.Authority.MetroCode;
                com.Parameters.Add("@MetroName", SqlDbType.VarChar, 100).Value = data.Authority.MetroName;
                com.Parameters.Add("@MetroNo", SqlDbType.VarChar, 6).Value = data.Authority.MetroNumber;

                if(this.con.State != ConnectionState.Open)
                    this.con.Open();
                data.Authority.ID = int.Parse(com.ExecuteScalar().ToString());

                com.Dispose();
            }
            catch (Exception ex)
            {
                failed = true;
                errorMessage = ex.Message;
                Logger.Write(string.Format("Error while updating Authority ({0})\n{1}", ex.Message, ex.StackTrace), "General", (int)TraceEventType.Critical);
            }
            finally
            {
                this.con.Close();
            }

            return failed;
        }

        /// <summary>
        /// Gets the data to send to EasyPay.
        /// </summary>
        /// <param name="authorities">The list of authorities to be populate with data to send.</param>
        internal void GetEasyPayDataToSend(List<EasyPayAuthority> authorities)
        {
            this.Connect();

            DateTime dt = DateTime.Now;

            Logger.Write("Executing SQL: EasyPayDataToSend...", "General", (int)TraceEventType.Information);

            var com = new SqlCommand("EasyPayDataToSend2", this.con);
            com.CommandType = CommandType.StoredProcedure;
            com.CommandTimeout = 1800;

            this.con.Open();
            SqlDataReader reader = com.ExecuteReader();

            // Process the Authorities
            EasyPayAuthority authority = null;
            int count = 0;
            while (reader.Read())
            {
                authority = new EasyPayAuthority();
                authority.ID = (int)reader["AutIntNo"];
                authority.Name = (string)reader["AutName"];
                authority.ReceiverIdentifier = (int)reader["EasyPayReceiptID"];

                authorities.Add(authority);

                Logger.Write(string.Format("EasyPay Authority - {0}", authority), "General", (int)TraceEventType.Information);
            }

            Logger.Write(string.Format("Authorities received: " + authorities.Count), "General", (int)TraceEventType.Information);

            // Process the EasyPay Transactions
            int autIntNo;
            int receiverId;
            reader.NextResult();
            while (reader.Read())
            {
                autIntNo = (int)reader["AutIntNo"];
                receiverId = (int)reader["EasyPayReceiptID"];

                // Check if this EasyPayTran is for a different Authority
                //if (!autIntNo.Equals(authority.ID) || !receiverId.Equals(authority.ReceiverIdentifier))
                if (!receiverId.Equals(authority.ReceiverIdentifier))
                {
                    authority = null;
                    foreach (EasyPayAuthority epAuthority in authorities)
                    {
                        //dls 2012-05-23 - stop checking for specific Authority - use only receiverid
                        //if (epAuthority.ID.Equals(autIntNo) && epAuthority.ReceiverIdentifier.Equals(receiverId))
                        if (epAuthority.ReceiverIdentifier.Equals(receiverId))
                        {
                            authority = epAuthority;
                            break;
                        }
                    }
                    if (authority == null)
                    {
                        Logger.Write(string.Format("There was no authority found for ID {0}.", autIntNo), "General", (int)TraceEventType.Error);
                        continue;
                    }
                }

                // Create the transaction object
                // 20080509 error in amount - integer cents not decimal rand.cent - extract from Easypay spec 1.8
                //Record Type	Char	1
                //EasyPay Number	Numeric	Var20
                //Amount	Numeric	Var12
                //Expiry Date**	Date	8
                //Email Address	Char	Var50
                //Cell phone Number	Char 	10

                var tran = new EasyPayTransaction(authority);
                tran.EasyPayAction = ((string)reader["EPAction"])[0];
                //20080509 tran.Amount = (decimal)reader["Amount"]; amount is money - has 4 digits to the right of the decimal point in sql
                //the likelihood of fines with cents is zero at this point
                //tran.Amount = (decimal)reader["Amount"]; set default to 0
                tran.Cents = (Int32)((decimal)reader["Amount"] * 100);
                tran.EasyPayNumber = (string)reader["EasyPayNo"];
                tran.ExpiryDate = (DateTime)reader["ExpiryDate"];
                tran.RegNo = (string)reader["RegNo"];

                // Add it to the authority's transaction list
                authority.EasyPayTransactions.Add(tran);
                count++;

            }
            reader.Close();
            reader.Dispose();
            com.Dispose();
            this.con.Close();

            TimeSpan ts = DateTime.Now - dt;
            Logger.Write(string.Format("Completed - EasyPayDataToSend{0} in {1})", count.ToString("###,###,###"), ts), "General", (int)TraceEventType.Information);
        }

        /// <summary>
        /// Gets the list of remote authorities that have feedback data for export.
        /// </summary>
        /// <returns>A list of <see cref="RemoteAuthority"/> objects.</returns>
        internal void GetRemoteAuthoritiesForExport(List<RemoteAuthority> authorities)
        {
            Logger.Write("Resetting the database connection.", "General", (int)TraceEventType.Information);
            if (!this.Connect())
            {
                Logger.Write("Resetting the database connection failed!.", "General", (int)TraceEventType.Critical);
                return;
            }

            Logger.Write("Getting EasyPay Authority Feedback ...", "General", (int)TraceEventType.Information);

            using (var com = new SqlCommand("EasyPayAuthorityFeedback", this.con))
            {
                com.CommandTimeout = 180;
                com.CommandType = CommandType.StoredProcedure;

                this.con.Open();
                Logger.Write("Connection opened ...", "General", (int)TraceEventType.Information);
                SqlDataReader reader = com.ExecuteReader();
                Logger.Write("Reader opened ...", "General", (int)TraceEventType.Information);
                while (reader.Read())
                {
                    RemoteAuthority authority = new RemoteAuthority();
                    authority.ID = (int)reader["AutIntNo"];
                    authority.Name = (string)reader["AutName"];
                    authority.AuthorityCode = reader["AutCode"].ToString().Trim();
                    if (reader["MachineName"] != DBNull.Value)
                        authority.MachineName = (string)reader["MachineName"];

                    authorities.Add(authority);
                }
                reader.Close();
                reader.Dispose();
                this.con.Close();
            }

            Logger.Write("EasyPay Authorities retrieved", "General", (int)TraceEventType.Information);
        }

        /// <summary>
        /// Gets EasyPay feedback data for an authority, writing it directly into a feedback file.
        /// </summary>
        /// <param name="file">The file.</param>
        internal void GetEasyPayAuthorityFeedbackData(FineExchangeFeedbackFile file)
        {
            using (var com = new SqlCommand("EasyPayAuthorityFeedbackData", this.con))
            {
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = file.Authority.ID;

                this.con.Open();
                SqlDataReader reader = com.ExecuteReader();
                while (reader.Read())
                    file.WriteError((string)reader["EasyPayNo"], (string)reader["EasyPayError"]);
                reader.Close();
                this.con.Close();
            }
        }

        //mrs 20081002 3970 added transType as we will have to test for pending in future
        internal void GetPaymentInformation(FineExchangeFeedbackFile file)
        {
            //mrs 20081002 3970 added transType as we will have to test for pending in future
            SqlCommand com = new SqlCommand("PaymentsUpdates", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("AutIntNo", SqlDbType.Int, 4).Value = file.Authority.ID;

            this.con.Open();
            SqlDataReader reader = com.ExecuteReader();
            while (reader.Read())
            {
                file.WriterPaymentFeedback(
                    (string)reader["TicketNo"],
                    (string)reader["PartnerID"],
                    (string)reader["OperatorName"],
                    (DateTime)reader["PaymentDate"],
                    (decimal)reader["Amount"],
                    (DateTime)reader["MsgDateTime"],
                    (byte)reader["PaymentAction"],
                    (string)reader["TransType"]);
            }
            reader.Close();
            reader.Dispose();
            com.Dispose();
            this.con.Close();
        }

        /// <summary>
        /// Sets the EasyPay hold-back file errors in the database.
        /// </summary>
        /// <param name="tdf">The TDF.</param>
        public void SetHoldbackErrors(TrafficDataFile tdf)
        {
            var com = new SqlCommand("EasyPayTranSetError", this.con);
            com.CommandType = CommandType.StoredProcedure;
            SqlParameter paramEPNumber = com.Parameters.Add("@EasyPayNumber", SqlDbType.VarChar, 30);
            SqlParameter paramEPAction = com.Parameters.Add("@EasyPayAction", SqlDbType.Char, 1);
            SqlParameter paramError = com.Parameters.Add("@Error", SqlDbType.VarChar, 255);

            try
            {
                this.con.Open();

                // Update the row-level errors
                foreach (HoldbackFileError err in tdf.Errors)
                {
                    if (err.Transaction == null)
                        continue;

                    paramEPNumber.Value = err.Transaction.EasyPayNumber;
                    paramEPAction.Value = err.Transaction.EasyPayAction;
                    paramError.Value = err.Error;

                    com.ExecuteNonQuery();
                }

                // Update any file level errors
                foreach (HoldbackFileError err in tdf.Errors)
                {
                    if (err.Transaction != null)
                        continue;

                    // TODO: Update the database
                }
            }
            finally
            {
                com.Dispose();
                this.con.Close();
            }
        }

        /// <summary>
        /// Sets the name of the easy pay data file.
        /// </summary>
        /// <param name="easyPayNumber">The easy pay number.</param>
        /// <param name="easyPayAction">The easy pay action.</param>
        /// <param name="fileName">Name of the file.</param>
        public void SetEasyPayDataFileName(string easyPayNumber, char easyPayAction, string fileName)
        {
            var com = new SqlCommand("EasyPayTranSetDataFileName", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@EasyPayNumber", SqlDbType.VarChar, 30).Value = easyPayNumber;
            com.Parameters.Add("@EasyPayAction", SqlDbType.Char, 1).Value = easyPayAction;
            com.Parameters.Add("@FileName", SqlDbType.VarChar, 255).Value = fileName;

            try
            {
                this.con.Open();
                com.ExecuteNonQuery();
                com.Dispose();
            }
            finally
            {
                this.con.Close();
            }
        }

        /// <summary>
        /// Sets the file sent status.
        /// </summary>
        /// <param name="file">The file.</param>
        public void SetFileSent(FineExchangeFeedbackFile file)
        {
            var com = new SqlCommand("PaymentsUpdateSent", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = file.Authority.ID;

            try
            {
                this.con.Open();
                com.ExecuteNonQuery();
                com.Dispose();
            }
            finally
            {
                this.con.Close();
            }
        }

        /// <summary>
        /// Records the contents of the SOF file into the database.
        /// </summary>
        /// <param name="sof">The SOF object.</param>
        public void RecordSOFContents(StandardOutputFile sof)
        {
            using (var com = new SqlCommand())
            {
                com.Connection = this.con;
                com.CommandType = CommandType.StoredProcedure;

                // Record the SOF File header and footer
                com.CommandText = "EasyPaySOFFileAdd";
                com.Parameters.Add("@FileName", SqlDbType.VarChar, 150).Value = sof.FileName;
                com.Parameters.Add("@Version", SqlDbType.Int, 4).Value = sof.Header.Version;
                com.Parameters.Add("@ReceiverIdentifier", SqlDbType.Int, 4).Value = sof.Header.ReceiverID;
                com.Parameters.Add("@TimeStamp", SqlDbType.DateTime, 8).Value = sof.Header.TimeStamp;
                com.Parameters.Add("@FileGenerationNo", SqlDbType.Int, 4).Value = sof.Header.FileGenerationNumber;
                com.Parameters.Add("@NoPayments", SqlDbType.Int, 4).Value = sof.Footer.NumberOfPayments;
                com.Parameters.Add("@ValuePayments", SqlDbType.Money, 8).Value = sof.Footer.ValueOfPayments;
                com.Parameters.Add("@ValueFees", SqlDbType.Money, 8).Value = sof.Footer.ValueOfFees;
                com.Parameters.Add("@NoTenders", SqlDbType.Int, 4).Value = sof.Footer.NumberOfTenders;
                com.Parameters.Add("@ValueTenders", SqlDbType.Money, 8).Value = sof.Footer.ValueOfTenders;
                com.Parameters.Add("@ValueBankCosts", SqlDbType.Money, 8).Value = sof.Footer.ValueOfBankCharges;

                int sofIntNo = 0;
                try
                {
                    this.con.Open();
                    sofIntNo = Convert.ToInt32(com.ExecuteScalar());
                    sof.ID = sofIntNo;
                }
                finally
                {
                    this.con.Close();
                }

                // Record each line in the database 
                int estIntNo = 0;
                foreach (SOFTransaction tran in sof.Transactions)
                {
                    com.CommandText = "EasyPaySOFTransactionAdd";
                    com.Parameters.Clear();

                    com.Parameters.Add("@SOFIntNo", SqlDbType.Int, 4).Value = sofIntNo;
                    com.Parameters.Add("@CollectorIdentifier", SqlDbType.BigInt, 8).Value = tran.CollectorID;
                    com.Parameters.Add("@Date", SqlDbType.DateTime, 8).Value = tran.TimeStamp;
                    com.Parameters.Add("@PointOfService", SqlDbType.VarChar, 10).Value = tran.PointOfSale;
                    if (tran.Trace.Length > 0)
                        com.Parameters.Add("@Trace", SqlDbType.VarChar, 50).Value = tran.Trace;
                    com.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = tran.Payment.Amount;
                    com.Parameters.Add("@Fee", SqlDbType.Money, 8).Value = tran.Payment.Fee;
                    com.Parameters.Add("@EasyPayNo", SqlDbType.VarChar, 50).Value = tran.Payment.EasyPayNumber;

                    try
                    {
                        this.con.Open();
                        estIntNo = Convert.ToInt32(com.ExecuteScalar());
                        tran.ID = estIntNo;
                    }
                    finally
                    {
                        this.con.Close();
                    }

                    // Record each tender amount
                    com.CommandText = "EasyPaySOFTenderAdd";

                    foreach (SOFTender tender in tran.Payment.Tendered)
                    {
                        com.Parameters.Clear();

                        com.Parameters.Add("@ESTIntNo", SqlDbType.Int, 4).Value = estIntNo;
                        com.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = tender.Amount;
                        com.Parameters.Add("@BankCharge", SqlDbType.Money, 8).Value = tender.BankCharge;
                        com.Parameters.Add("@TenderType", SqlDbType.VarChar, 20).Value = tender.TenderType;
                        if (tender.AccountNumber.Length > 0)
                            com.Parameters.Add("@AccountNumber", SqlDbType.VarChar, 50).Value = tender.AccountNumber;

                        try
                        {
                            this.con.Open();
                            Convert.ToInt32(com.ExecuteScalar());
                        }
                        finally
                        {
                            this.con.Close();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Records a pending payment for  transmission to the LA
        /// </summary>
        /// <param name="tran">The tran.</param>
        public void RecordEasyPayPaymentProblemPending(SOFPayment payment, string error)
        {
            using (var cmd = new SqlCommand("PaymentAdd", this.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PaymentAddPaymentPending";

                // Catch invalid notices
                if (payment.NotIntNo < 1)
                    return;

                cmd.Parameters.Clear();

                cmd.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = payment.NotIntNo;
                cmd.Parameters.Add("@PartnerID", SqlDbType.VarChar, 3).Value = "EP";
                cmd.Parameters.Add("@TerminalID", SqlDbType.VarChar, 13).Value = string.Empty;
                cmd.Parameters.Add("@OperatorName", SqlDbType.VarChar, 10).Value = "EasyPay";
                cmd.Parameters.Add("@MsgDateTime", SqlDbType.DateTime, 8).Value = DateTime.Now;
                cmd.Parameters.Add("@UniqueNumber", SqlDbType.VarChar, 18).Value = DateTime.Now.ToString("yyyyMMddHHmmssff");
                cmd.Parameters.Add("@Source", SqlDbType.VarChar, 3).Value = "EP";
                cmd.Parameters.Add("@PaymentDate", SqlDbType.DateTime, 8).Value = payment.Transaction.TimeStamp;
                cmd.Parameters.Add("@StatusDescr", SqlDbType.VarChar, 100).Value = error;
                cmd.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = 0;    //send no value
                cmd.Parameters.Add("@TransType", SqlDbType.VarChar, 3).Value = "PEN";

                try
                {
                    this.con.Open();
                    int id = Convert.ToInt32(cmd.ExecuteScalar());
                    if (id < 1)
                        Logger.Write(string.Format("The payment pending for notice ({0}) was not processed successfully.", payment.NotIntNo), "General", (int)TraceEventType.Warning);
                }
                finally
                {
                    this.con.Close();
                }
            }
        }

        /// <summary>
        /// Records the EasyPay payments on the SOF file.
        /// </summary>
        /// <param name="sof">The SOF object.</param>
        [Obsolete("The contents of the SOF File are now imported a Payment at a time using RecordEasyPayPayment(SOFPayment payment)", true)]
        public void RecordEasyPayPayments(StandardOutputFile sof)
        {
            // Record each payment in the payments table
            using (var com = new SqlCommand("PaymentAdd", this.con))
            {
                com.CommandType = CommandType.StoredProcedure;

                foreach (SOFTransaction tran in sof.Transactions)
                {
                    // Catch blank payments
                    if (tran.Payment == null)
                        continue;

                    com.CommandText = "PaymentAdd";

                    // Catch invalid notices
                    if (tran.Payment.NotIntNo < 1)
                        continue;

                    com.Parameters.Clear();

                    com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = tran.Payment.NotIntNo;
                    com.Parameters.Add("@PartnerID", SqlDbType.VarChar, 3).Value = "EP";
                    com.Parameters.Add("@TerminalID", SqlDbType.VarChar, 13).Value = string.Empty;
                    com.Parameters.Add("@OperatorName", SqlDbType.VarChar, 10).Value = "EasyPay";
                    com.Parameters.Add("@MsgDateTime", SqlDbType.DateTime, 8).Value = DateTime.Now;
                    com.Parameters.Add("@UniqueNumber", SqlDbType.VarChar, 18).Value = DateTime.Now.ToString("yyyyMMddHHmmssff");
                    com.Parameters.Add("@Source", SqlDbType.VarChar, 3).Value = "EP";
                    com.Parameters.Add("@PaymentDate", SqlDbType.DateTime, 8).Value = tran.TimeStamp;
                    com.Parameters.Add("@StatusDescr", SqlDbType.VarChar, 100).Value = string.Empty;
                    com.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = tran.Payment.Amount;
                    com.Parameters.Add("@TransType", SqlDbType.VarChar, 3).Value = "REC";

                    try
                    {
                        this.con.Open();
                        int id = Convert.ToInt32(com.ExecuteScalar());
                        if (id < 1)
                            Logger.Write(string.Format("The payment for notice ({0}) was not processed successfully.", tran.Payment.NotIntNo), "General", (int)TraceEventType.Error);
                    }
                    finally
                    {
                        this.con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Checks the SOF file transactions for referenced notices.
        /// </summary>
        /// <param name="sof">The sof.</param>
        public void CheckEasyPayNumbers(StandardOutputFile sof)
        {
            using (var cmd = new SqlCommand("EasyPayCheckNotice", this.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param = cmd.Parameters.Add("@EPNo", SqlDbType.VarChar, 30);

                foreach (SOFTransaction tran in sof.Transactions)
                {
                    if (tran.Payment == null)
                        continue;

                    param.Value = tran.Payment.EasyPayNumber;

                    try
                    {
                        this.con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            tran.Payment.NotIntNo = Convert.ToInt32(reader[0]);
                            tran.Payment.OriginalAmount = Convert.ToDecimal(reader[1]);
                        }
                        reader.Close();
                    }
                    finally
                    {
                        this.con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Checks that the sequence number on the SOF file is the next in sequence for the LA
        /// </summary>
        /// <param name="sof">The SOF file.</param>
        public void CheckSequenceNumber(StandardOutputFile sof)
        {
            var com = new SqlCommand("EasyPayCheckSequenceNo", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@ReceiverIdentifier", SqlDbType.Int, 4).Value = sof.Header.ReceiverID;

            try
            {
                this.con.Open();
                SqlDataReader reader = com.ExecuteReader();
                if (reader.Read())
                {
                    int lastSequenceNo = (int)reader["LastSequence"];
                    sof.IsValid = (lastSequenceNo + 1 == sof.Header.FileGenerationNumber);

                    if (reader["AutIntNo"] != DBNull.Value)
                    {
                        sof.Authority = new SOFAuthority();
                        sof.Authority.ID = (int)reader["AutIntNo"];
                        sof.Authority.Name = (string)reader["AutName"];
                        sof.Authority.Email = reader["Email"].ToString();
                    }
                    reader.Close();
                    reader.Dispose();
                }
                com.Dispose();
            }
            finally
            {
                this.con.Close();
            }
        }

        /// <summary>
        /// Records the easy pay payment.
        /// </summary>
        /// <param name="payment">The payment.</param>
        public void RecordEasyPayPayment(SOFPayment payment)
        {
            using (var cmd = new SqlCommand("PaymentAdd", this.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PaymentAdd";

                // Catch invalid notices
                if (payment.NotIntNo < 1)
                    return;

                cmd.Parameters.Clear();

                cmd.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = payment.NotIntNo;
                cmd.Parameters.Add("@PartnerID", SqlDbType.VarChar, 3).Value = "EP";
                cmd.Parameters.Add("@TerminalID", SqlDbType.VarChar, 13).Value = string.Empty;
                cmd.Parameters.Add("@OperatorName", SqlDbType.VarChar, 10).Value = "EasyPay";
                cmd.Parameters.Add("@MsgDateTime", SqlDbType.DateTime, 8).Value = DateTime.Now;
                cmd.Parameters.Add("@UniqueNumber", SqlDbType.VarChar, 18).Value = DateTime.Now.ToString("yyyyMMddHHmmssff");
                cmd.Parameters.Add("@Source", SqlDbType.VarChar, 3).Value = "EP";
                cmd.Parameters.Add("@PaymentDate", SqlDbType.DateTime, 8).Value = payment.Transaction.TimeStamp;
                cmd.Parameters.Add("@StatusDescr", SqlDbType.VarChar, 100).Value = string.Empty;
                cmd.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = payment.Amount;
                cmd.Parameters.Add("@TransType", SqlDbType.VarChar, 3).Value = "REC";

                try
                {
                    this.con.Open();
                    int id = Convert.ToInt32(cmd.ExecuteScalar());
                    if (id < 1)
                        Logger.Write(string.Format("The payment for notice ({0}) was not processed successfully.", payment.NotIntNo), "General", (int)TraceEventType.Error);
                }
                finally
                {
                    this.con.Close();
                }
            }
        }
  
        public void RecordEasyPayProblemPayment(SOFTransaction tran)
        {
            using (var cmd = new SqlCommand("ProblemPaymentAdd", this.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = tran.Payment.NotIntNo;
                cmd.Parameters.Add("@ESEIntNo", SqlDbType.Int, 4).Value = tran.ID;

                try
                {
                    this.con.Open();
                    int id = Convert.ToInt32(cmd.ExecuteScalar());
                    if (id < 1)
                        Logger.Write(string.Format("The payment for notice ({0}) was not processed successfully.", tran.Payment.NotIntNo), "General", (int)TraceEventType.Error);
                }
                finally
                {
                    this.con.Close();
                }
            }
        }

        /// <summary>
        /// Checks if there is a duplicate payment.
        /// </summary>
        /// <param name="tran">The easy pay transaction object.</param>
        /// <returns>
        /// 	<c>false</c> if there have already been payments on the EasyPay number
        /// </returns>
        public bool CheckDuplicatePayment(SOFTransaction tran)
        {
            using (var cmd = new SqlCommand("EasyPayCheckDuplicateTran", this.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@EasyPayNo", SqlDbType.VarChar, 50).Value = tran.Payment.EasyPayNumber;
                cmd.Parameters.Add("@SOFIntNo", SqlDbType.Int, 4).Value = tran.SOFFile.ID;

                try
                {
                    this.con.Open();
                    int transactions = Convert.ToInt32(cmd.ExecuteScalar());
                    return (transactions == 1);
                }
                finally
                {
                    this.con.Close();
                }
            }
        }

        /// <summary>
        /// Deletes the expired easy pay tran records.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns>The number of records deleted</returns>
        public int DeleteExpiredEasyPayTranRecords(DateTime dateTime)
        {
            using (var cmd = new SqlCommand("EasyPayTranDeleteExpired", this.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 600;

                cmd.Parameters.Add("@ExpiredDate", SqlDbType.DateTime, 8).Value = dateTime;

                try
                {
                    this.con.Open();
                    int recordsDeleted = Convert.ToInt32(cmd.ExecuteScalar());
                    return recordsDeleted;
                }
                finally
                {
                    this.con.Close();
                }
            }
        }

        /// <summary>
        /// Gets the name of the machine from the LA code in the fine.
        /// </summary>
        /// <param name="fine">The fine.</param>
        /// <returns>true if a machine name was found; else false</returns>
        public bool GetMachineName(FineExchangeDataFile fine)
        {
            bool response = false;

            using (var cmd = new SqlCommand("MachineNameFromAuthCode", this.con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@AutCode", SqlDbType.Char, 3).Value = fine.Authority.AuthorityCode;
                try
                {
                    this.con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        response = true;
                        fine.MachineName = reader.GetString(0);
                    }
                    reader.Dispose();
                }
                finally
                {
                    this.con.Close();
                }
            }

            return response;
        }

        /// <summary>
        /// jerry 2012-1-19 add
        /// set AuthorityEasyPay.EasyPaySentDate column value to today by AutIntNo 
        /// </summary>
        /// <param name="autIntNo"></param>
        //public void SetEasyPaySentDateByAutIntNo(int autIntNo)
        public void SetEasyPaySentDateByAutIntNo(int easyPayReceiptID)
        {
            var com = new SqlCommand("SetEasyPaySentDate", this.con);
            com.CommandType = CommandType.StoredProcedure;

            //com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@EasyPayReceiptID", SqlDbType.Int, 4).Value = easyPayReceiptID;

            try
            {
                this.con.Open();
                com.ExecuteNonQuery();
                com.Dispose();
            }
            finally
            {
                this.con.Close();
            }
        }
    }
}
