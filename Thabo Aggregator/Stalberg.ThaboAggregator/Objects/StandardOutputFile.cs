using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Stalberg.TMS_TPExInt.Objects;
using Stalberg.TMS;

namespace Stalberg.ThaboAggregator.Objects
{
    /// <summary>
    /// Represents an EasyPay SOF (Standard Output File) with its data
    /// </summary>
    internal class StandardOutputFile
    {
        // Fields
        private int id = 0;
        private SOFHeader header = null;
        private List<SOFTransaction> transactions = null;
        private SOFFooter footer = null;
        private string fileName = string.Empty;
        private SOFAuthority authority = null;
        private bool isValid = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="StandardOutputFile"/> class.
        /// </summary>
        /// <param name="file">The file.</param>
        public StandardOutputFile(FileInfo file)
        {
            StreamReader reader = new StreamReader(file.Open(FileMode.Open, FileAccess.Read, FileShare.Read));
            string contents = reader.ReadToEnd();
            reader.Close();

            this.transactions = new List<SOFTransaction>();
            this.Parse(contents);
        }

        private void Parse(string contents)
        {
            SOFTransaction transaction = null;
            SOFPayment payment = null;

            string[] split = contents.Split(new char[] { '\n' });

            foreach (string line in split)
            {
                if (line.Length == 0)
                    continue;

                switch (line[0])
                {
                    case 'S':
                        this.header = new SOFHeader(line.Trim());
                        break;
                    case 'X':
                        transaction = new SOFTransaction(line.Trim(), this);
                        this.transactions.Add(transaction);
                        break;
                    case 'P':
                        payment = new SOFPayment(line.Trim(), transaction);
                        transaction.Payment = payment;
                        break;
                    case 'T':
                        payment.Tendered.Add(new SOFTender(line.Trim()));
                        break;
                    default:
                        this.footer = new SOFFooter(line.Trim());
                        break;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is valid.
        /// </summary>
        /// <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
        public bool IsValid
        {
            get { return this.isValid; }
            set { this.isValid = value; }
        }

        /// <summary>
        /// Gets or sets the authority.
        /// </summary>
        /// <value>The authority.</value>
        public SOFAuthority Authority
        {
            get { return this.authority; }
            set { this.authority = value; }
        }

        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>The name of the file.</value>
        public string FileName
        {
            get { return this.fileName; }
            set { this.fileName = value; }
        }

        /// <summary>
        /// Gets or sets the database ID of the SOF file.
        /// </summary>
        /// <value>The ID.</value>
        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Gets the transactions.
        /// </summary>
        /// <value>The transactions.</value>
        public List<SOFTransaction> Transactions
        {
            get { return this.transactions; }
        }

        /// <summary>
        /// Gets or sets the footer.
        /// </summary>
        /// <value>The footer.</value>
        public SOFFooter Footer
        {
            get { return this.footer; }
            set { this.footer = value; }
        }

        /// <summary>
        /// Gets the header.
        /// </summary>
        /// <value>The header.</value>
        public SOFHeader Header
        {
            get { return this.header; }
        }

    }

    /// <summary>
    /// Represents the footer of and SOF file
    /// </summary>
    internal class SOFFooter
    {
        // Fields
        private int noPayments;
        private decimal valuePayments;
        private decimal valueFees;
        private int noTenders;
        private decimal valueTenders;
        private decimal valueBankCharges;

        /// <summary>
        /// Initializes a new instance of the <see cref="SOFFooter"/> class.
        /// </summary>
        /// <param name="line">The line.</param>
        public SOFFooter(string line)
        {
            string[] split = line.Split(new char[] { ',' });

            this.noPayments = int.Parse(split[0].Trim());
            this.valuePayments = decimal.Parse(split[1].Trim());
            this.valueFees = decimal.Parse(split[2].Trim());
            this.noTenders = int.Parse(split[3].Trim());
            this.valueTenders = decimal.Parse(split[4].Trim());
            this.valueBankCharges = decimal.Parse(split[5].Trim());
        }

        /// <summary>
        /// Gets the value of bank charges.
        /// </summary>
        /// <value>The value of bank charges.</value>
        public decimal ValueOfBankCharges
        {
            get { return this.valueBankCharges; }
        }

        /// <summary>
        /// Gets the value of tenders.
        /// </summary>
        /// <value>The value of tenders.</value>
        public decimal ValueOfTenders
        {
            get { return this.valueTenders; }
        }

        /// <summary>
        /// Gets the number of tenders.
        /// </summary>
        /// <value>The number of tenders.</value>
        public int NumberOfTenders
        {
            get { return this.noTenders; }
        }

        /// <summary>
        /// Gets the value of fees.
        /// </summary>
        /// <value>The value of fees.</value>
        public decimal ValueOfFees
        {
            get { return this.valueFees; }
        }

        /// <summary>
        /// Gets the value of payments.
        /// </summary>
        /// <value>The value of payments.</value>
        public decimal ValueOfPayments
        {
            get { return this.valuePayments; }
        }

        /// <summary>
        /// Gets the number of payments.
        /// </summary>
        /// <value>The number of payments.</value>
        public int NumberOfPayments
        {
            get { return this.noPayments; }
        }

    }

    /// <summary>
    /// Represents a tendered amount line on an SOF file
    /// </summary>
    internal class SOFTender
    {
        // Fields
        private decimal amount;
        private decimal bankCharge;
        private string type;
        private string account = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="SOFTender"/> class.
        /// </summary>
        /// <param name="line">The line.</param>
        public SOFTender(string line)
        {
            string[] split = line.Split(new char[] { ',' });

            this.amount = decimal.Parse(split[1].Trim());
            this.bankCharge = decimal.Parse(split[2].Trim());
            this.type = split[3].Trim();
            if (split.Length > 4)
                this.account = split[4].Trim();
        }

        /// <summary>
        /// Gets the account number.
        /// </summary>
        /// <value>The account number.</value>
        public string AccountNumber
        {
            get { return this.account; }
        }

        /// <summary>
        /// Gets the type of the tender.
        /// </summary>
        /// <value>The type of the tender.</value>
        public string TenderType
        {
            get { return this.type; }
        }

        /// <summary>
        /// Gets the bank charge.
        /// </summary>
        /// <value>The bank charge.</value>
        public decimal BankCharge
        {
            get { return this.bankCharge; }
        }

        /// <summary>
        /// Gets the amount.
        /// </summary>
        /// <value>The amount.</value>
        public decimal Amount
        {
            get { return this.amount; }
        }

    }

    /// <summary>
    /// Represents a payment line from an SOF File (this is a header of the <see cref="SOFTender"/> rows that it contains)
    /// </summary>
    internal class SOFPayment
    {
        // Fields
        private decimal originalAmount = 0;
        private decimal amount;
        private decimal fee;
        private decimal epNo;
        private int notIntNo = 0;
        private List<SOFTender> tendered = null;
        private SOFTransaction tran;

        /// <summary>
        /// Initializes a new instance of the <see cref="SOFPayment"/> class.
        /// </summary>
        /// <param name="line">The line.</param>
        public SOFPayment(string line, SOFTransaction tran)
        {
            this.tran = tran;
            this.tendered = new List<SOFTender>();

            string[] split = line.Split(new char[] { ',' });

            this.amount = decimal.Parse(split[1].Trim());
            this.fee = decimal.Parse(split[2].Trim());
            this.epNo = decimal.Parse(split[3]);
        }

        /// <summary>
        /// Gets the parent transaction object.
        /// </summary>
        /// <value>The transaction.</value>
        public SOFTransaction Transaction
        {
            get { return this.tran; }
        }

        /// <summary>
        /// Gets or sets the original fine amount on the notice.
        /// </summary>
        /// <value>The original amount.</value>
        public decimal OriginalAmount
        {
            get { return this.originalAmount; }
            set { this.originalAmount = value; }
        }

        /// <summary>
        /// Gets or sets the notice int no.
        /// </summary>
        /// <value>The not int no.</value>
        public int NotIntNo
        {
            get { return this.notIntNo; }
            set { this.notIntNo = value; }
        }

        /// <summary>
        /// Gets the amounts tendered.
        /// </summary>
        /// <value>The tendered.</value>
        public List<SOFTender> Tendered
        {
            get { return this.tendered; }
        }

        /// <summary>
        /// Gets the easy pay number.
        /// </summary>
        /// <value>The easy pay number.</value>
        public decimal EasyPayNumber
        {
            get { return this.epNo; }
        }

        /// <summary>
        /// Gets the fee.
        /// </summary>
        /// <value>The fee.</value>
        public decimal Fee
        {
            get { return this.fee; }
        }

        /// <summary>
        /// Gets the amount.
        /// </summary>
        /// <value>The amount.</value>
        public decimal Amount
        {
            get { return this.amount; }
        }

    }

    /// <summary>
    /// Represents a transaction line in an SOF file
    /// </summary>
    internal class SOFTransaction
    {
        // Fields
        private int id;
        private long collectorID;
        private DateTime timeStamp;
        private string pointOfService;
        private string trace = string.Empty;
        private SOFPayment payment = null;
        private StandardOutputFile sof = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="SOFTransaction"/> class.
        /// </summary>
        /// <param name="line">The line.</param>
        public SOFTransaction(string line, StandardOutputFile sof)
        {
            this.payment = null;
            this.sof = sof;

            string[] split = line.Split(new char[] { ',' });

            this.collectorID = long.Parse(split[1]);
            this.timeStamp = SOFHeader.ParseDateTime(split[2], split[3]);
            this.pointOfService = split[4].Trim();
            if (split.Length > 5)
                this.trace = split[5].Trim();
        }

        /// <summary>
        /// Gets a reference to the the SOF file this transaction belongs to.
        /// </summary>
        /// <value>The SOF file.</value>
        public StandardOutputFile SOFFile
        {
            get { return this.sof; }
        }

        /// <summary>
        /// Gets or sets the database ID of the transaction.
        /// </summary>
        /// <value>The ID.</value>
        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Gets the payments.
        /// </summary>
        /// <value>The payments.</value>
        public SOFPayment Payment
        {
            get { return this.payment; }
            set { this.payment = value; }
        }

        /// <summary>
        /// Gets any trace information.
        /// </summary>
        /// <value>The trace.</value>
        public string Trace
        {
            get { return this.trace; }
        }

        /// <summary>
        /// Gets the point of sale.
        /// </summary>
        /// <value>The point of sale.</value>
        public string PointOfSale
        {
            get { return this.pointOfService; }
        }

        /// <summary>
        /// Gets the time stamp.
        /// </summary>
        /// <value>The time stamp.</value>
        public DateTime TimeStamp
        {
            get { return this.timeStamp; }
        }

        /// <summary>
        /// Gets the collector ID.
        /// </summary>
        /// <value>The collector ID.</value>
        public long CollectorID
        {
            get { return this.collectorID; }
        }
    }

    /// <summary>
    /// Represents the header line of the SOF file
    /// </summary>
    internal class SOFHeader
    {
        // Fields
        private int version;
        private int receiverID;
        private DateTime timeStamp;
        private int fileGenNo;

        /// <summary>
        /// Initializes a new instance of the <see cref="SOFHeader"/> class.
        /// </summary>
        /// <param name="line">The line.</param>
        public SOFHeader(string line)
        {
            string[] split = line.Split(new char[] { ',' });

            this.version = int.Parse(split[1]);
            this.receiverID = int.Parse(split[2]);
            this.timeStamp = SOFHeader.ParseDateTime(split[3], split[4]);
            this.fileGenNo = int.Parse(split[5]);
        }

        /// <summary>
        /// Gets the file generation number.
        /// </summary>
        /// <value>The file generation number.</value>
        public int FileGenerationNumber
        {
            get { return this.fileGenNo; }
        }

        /// <summary>
        /// Gets the time stamp.
        /// </summary>
        /// <value>The time stamp.</value>
        public DateTime TimeStamp
        {
            get { return this.timeStamp; }
        }

        /// <summary>
        /// Gets the EasyPay Receiver Identifier.
        /// </summary>
        /// <value>The receiver ID.</value>
        public int ReceiverID
        {
            get { return this.receiverID; }
        }

        /// <summary>
        /// Gets the file version number.
        /// </summary>
        /// <value>The version.</value>
        public int Version
        {
            get { return this.version; }
        }

        /// <summary>
        /// Parses a SOF date time.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="time">The time.</param>
        /// <returns></returns>
        public static DateTime ParseDateTime(string date, string time)
        {
            date = date.Trim();
            if (date.Length == 0)
                return DateTime.MinValue;
            time = time.Trim();
            if (time.Length == 0)
                return DateTime.MinValue;

            int year = int.Parse(date.Substring(0, 4));
            int month = int.Parse(date.Substring(4, 2));
            int day = int.Parse(date.Substring(6, 2));
            int hour = int.Parse(time.Substring(0, 2));
            int minute = int.Parse(time.Substring(2, 2));
            int second = int.Parse(time.Substring(4, 2));

            return new DateTime(year, month, day, hour, minute, second);
        }
    }

    /// <summary>
    /// Represents and authority used with SOF files
    /// </summary>
    internal class SOFAuthority : AuthorityBase
    {
        private string email = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="SOFAuthority"/> class.
        /// </summary>
        public SOFAuthority() { }

        /// <summary>
        /// Gets or sets the payment problem email address.
        /// </summary>
        /// <value>The email.</value>
        public string Email
        {
            get { return this.email; }
            set { this.email = value; }
        }

    }

}
