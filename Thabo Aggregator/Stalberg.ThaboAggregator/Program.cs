using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Stalberg.Indaba;
using Stalberg.ThaboAggregator.Components;
using Stalberg.ThaboAggregator.Objects;
using Stalberg.TMS;
using Stalberg.ThaboAggregator.Properties;
using Stalberg.TMS_TPExInt;
using Stalberg.TMS_TPExInt.Objects;
using System.Diagnostics;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.DAL.Entities;


namespace Stalberg.ThaboAggregator
{
    /// <summary>
    /// Contains the main entry point of the application
    /// </summary>
    public class Program
    {
        // Constants
        private static ICommunicate indaba;
        //internal static LogWriter Writer;
        internal const string APP_NAME = "Thabo Aggregator";
        //private const string LAST_UPDATED = ProjectLastUpdated.AARTOThaboAggregator.ToShortDateString();
        internal static string LOG_FILEPATH;

        // Static
        //private static readonly string LAST_UPDATED = BuildTimeStampAttribute.GetTimeStamp().Value.ToString("yyyy-MM-dd HH:mm");

        /// <summary>
        /// The main entry point into the Thabo Aggregator
        /// </summary>
        /// <param name="args">The command line arguments.</param>
        public static void Main(string[] args)
        {
            // Check Last updated Version            
            string errorMessage;

            try
            {
                // Setup the Log Writer
                string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
                Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);

                // 2010/10/11 jerry add 
                if (!CheckVersionManager.CheckVersion(AartoProjectList.AARTOThaboAggregator, out errorMessage))
                {
                    Console.WriteLine(errorMessage);
                    EntLibLogger.WriteLog(LogCategory.General, "", errorMessage);
                    return;
                }

                LoggingSettings settings = LoggingSettings.GetLoggingSettings(new SystemConfigurationSource());
                string strFileName = ((FlatFileTraceListenerData)(settings.TraceListeners.Get("Flat File Destination"))).FileName;

                LOG_FILEPATH = System.IO.Path.GetFullPath(strFileName.Replace("%FILENAME%", strDate));

                Logger.Write(string.Format("Beginning {0} at {1}", APP_NAME, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")), "General", (int)TraceEventType.Information);
                Logger.Write(string.Format("Last Version updated on {0}", ProjectLastUpdated.AARTOThaboAggregator.ToShortDateString()), "General", (int)TraceEventType.Information);

                // Singleton
                if (Program.CheckForExistingProcess())
                    return;

                // Setup Indaba
                Client.ApplicationName = APP_NAME;
                Client.ConnectionString = ConfigurationManager.ConnectionStrings["Indaba"].ConnectionString;
                Client.Initialise();
                Program.indaba = Client.Create();

                // Application Title
                Console.Title = string.Format("{0} ({1})", APP_NAME, ProjectLastUpdated.AARTOThaboAggregator.ToShortDateString());

                //091030 Fred Generate ResponseFile
                // Get the folder path for response files
                string responseFilePath = ConfigurationManager.AppSettings["ResponseFile"];
                var resFileDir = new DirectoryInfo(responseFilePath);
                if (!resFileDir.Exists)
                {
                    Logger.Write(string.Format("The response file path ({0}) does not exist!", resFileDir.FullName), "General", (int)TraceEventType.Warning);
                    return;
                }

                // Check the FTP site for Fine Exchange Data Files from the local authorities
                Logger.Write("Get fine exchange data files", "General", (int)TraceEventType.Information);
                List<Stream> files = Program.GetFineExchangeDataFiles();
                // Checked for files ready (null > return)
                if (files == null)
                {
                    Logger.Write("The fine data exchange folder has not been set up correctly or there were no valid files available for processing", "General", (int)TraceEventType.Warning);
                    return;
                }
                Logger.Write("Finished processing fine exchange data files", "General", (int)TraceEventType.Information);

                //// mrs 20080714 this next block must be moved inside the getFineExchangeDataFiles so we can handle the errors and not delete the file until it has been processed correctly
                //// Process the data files
                //List<FineExchangeDataFile> fines = Program.ProcessFineDataFiles(files);

                //// Update the Thabo database with the fine data
                //DatabaseProvider db = Program.ProcessFinesIntoDatabase(fines);
                //if (!db.IsConnected)
                //{
                //    Program.Writer.WriteLine("Program is aborting as the database connection has not been configured correctly, or is not available.");
                //    return;
                //}

                Logger.Write("Setting up connection", "General", (int)TraceEventType.Information);
                DatabaseProvider db = Program.DbConnect();
                Logger.Write("Connection established.", "General", (int)TraceEventType.Information);

                // Create EasyPay data files
                Logger.Write("Processing EasyPay Data", "General", (int)TraceEventType.Information);
                Program.ProcessEasyPayData(db);
                Logger.Write("Processing EasyPay Data complete", "General", (int)TraceEventType.Information);

                // Send EasyPay Files to EasyPay
                Logger.Write("Sending EasyPay Files", "General", (int)TraceEventType.Information);
                Program.SendEasyPayFiles();
                Logger.Write("Finished sending EasyPay files.", "General", (int)TraceEventType.Information);

                // Get Holdback Files from EasyPay
                Logger.Write("Getting Holdback Files", "General", (int)TraceEventType.Information);
                var holdbackFiles = new List<FileInfo>();
                Program.GetHoldbackFiles(holdbackFiles);

                // Process the EasyPay HoldBack files
                Logger.Write("Processing Holdback Files", "General", (int)TraceEventType.Information);
                Program.ProcessHoldBackFiles(holdbackFiles, db);
                Logger.Write("Processing Holdback files complete", "General", (int)TraceEventType.Information);

                // Get any SOF Files
                Logger.Write("Processing SOF Files", "General", (int)TraceEventType.Information);
                holdbackFiles.Clear();
                Program.ProcessSOFFiles(holdbackFiles, db);
                Logger.Write("Processing SOF files complete", "General", (int)TraceEventType.Information);

                // Generate payment feedback files to send back to individual LAs
                Logger.Write("Generate Feedback Files", "General", (int)TraceEventType.Information);
                Program.GenerateFeedbackFiles(db);
                Logger.Write("Generate Feedback files complete", "General", (int)TraceEventType.Information);

                // Cleanup expired EasyPay Tran data
                Logger.Write("Cleaning up expired EasyPay transactions...", "General", (int)TraceEventType.Information);
                Program.CleanupEasyPayTran(db);
                Logger.Write("Finished cleaning up expired transactions.", "General", (int)TraceEventType.Information);
            }
            catch (Exception ex)
            {
                Logger.Write(string.Format("There was a general error running the Thabo Aggregator.\n{0}\n{1}", ex.Message, ex.StackTrace), "General", (int)TraceEventType.Critical);
            }
            finally
            {
                // Always cleanup and close the log writer
                Logger.Write(string.Format("Finished {0} at {1}\n", APP_NAME, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")), "General", (int)TraceEventType.Information);
                Logger.Writer.Dispose();

                // Email the Log file
                var sb = new StringBuilder();
                sb.Append("The Thabo Aggregator log file for ");
                sb.Append(DateTime.Now.ToString());
                sb.Append(" it attached\n\nRegards\n\nThabo Aggregator");
                
                Program.EmailErrorMessages(sb, "Log file from Thabo Aggregator " + DateTime.Now.ToString(), LOG_FILEPATH);

                // Cleanup Indaba
                Client.Dispose();
            }
        }

        private static void CleanupEasyPayTran(DatabaseProvider db)
        {
            try
            {
                int recordsDeleted = db.DeleteExpiredEasyPayTranRecords(DateTime.Today.AddDays(-1));
                Logger.Write(string.Format("{0} expired rows deleted from EasyPayTran.", recordsDeleted), "General", (int)TraceEventType.Information);
            }
            catch (Exception ex)
            {
                Logger.Write(ex.Message, "General", (int)TraceEventType.Critical);
            }
        }

        private static bool CheckForExistingProcess()
        {
            string processName = Process.GetCurrentProcess().ProcessName;
            Process[] processes = Process.GetProcessesByName(processName);
            if (processes.Length > 1)
            {
                Logger.Write(string.Format("Another copy of the {0} is already running, so this one will be terminated.\n", APP_NAME), "General", (int)TraceEventType.Information);
                return true;
            }

            return false;
        }

        

        private static void ProcessSOFFiles(List<FileInfo> holdBackFiles, DatabaseProvider db)
        {
            // Get the files from the drop folder
            string easyPayDropFolder = ConfigurationManager.AppSettings["EasyPayDropFolder"];
            DirectoryInfo dir = new DirectoryInfo(easyPayDropFolder);
            if (!dir.Exists)
            {
                Logger.Write("The EasyPay drop file does not exist!", "General", (int)TraceEventType.Warning);
                return;
            }

            foreach (FileInfo file in dir.GetFiles())
            {
                //jerry 2012-1-16 add transaction
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(10)))
                {
                    try
                    {
                        // Parse them into the SOF File object
                        StandardOutputFile sof = new StandardOutputFile(file);
                        sof.FileName = file.Name;

                        // Check that this is the next file in order for the LA
                        db.CheckSequenceNumber(sof);
                        if (!sof.IsValid)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append(string.Format("The the FileGenerationNumber number ({1}) in the SOF file ({0}) was not the next one in the sequence.", sof.FileName, sof.Header.FileGenerationNumber));
                            Logger.Write(sb.ToString(), "General", (int)TraceEventType.Warning);

                            sb.Append("\n\nRegards\n\n");
                            sb.Append(Program.APP_NAME);
                            Program.EmailErrorMessages(sb, "EasyPay SOF File Error", string.Empty);

                            continue;
                        }

                        // Record the SOF record details
                        db.RecordSOFContents(sof);

                        // Check and link notices to the SOF Transactions
                        db.CheckEasyPayNumbers(sof);
                        foreach (SOFTransaction tran in sof.Transactions)
                        {
                            if (tran.Payment == null)
                                continue;

                            if (tran.Payment.NotIntNo < 0)
                            {
                                Logger.Write(string.Format("ERROR: The Notice for EasyPay number: {0} cannot be found!", tran.Payment.EasyPayNumber), "General", (int)TraceEventType.Error);

                                // TODO: Send Email to SI & LA (sof.Authority.Email)
                                continue;
                            }

                            // See if there's been a transaction already for this EasyPay number
                            if (!db.CheckDuplicatePayment(tran))
                            {
                                // Record it as a problem Payment
                                db.RecordEasyPayProblemPayment(tran);

                                //mrs 20081001 Can't send a payment notification in this case as a payment (of some nature) has previously been processed
                                Logger.Write(string.Format("The EasyPay number {0} already has at least one payment processed for it.", tran.Payment.EasyPayNumber), "General", (int)TraceEventType.Warning);
                                continue;
                            }

                            // Check if the payment amount matches the original fine amount
                            if (tran.Payment.Amount == tran.Payment.OriginalAmount)
                            {
                                // Record the payment details
                                db.RecordEasyPayPayment(tran.Payment);
                            }
                            else
                            {
                                // mrs 20081002 3970 Record a remote payment pending
                                db.RecordEasyPayPaymentProblemPending(tran.Payment, "payment and fine amount do not match");

                                // Record it as a problem Payment
                                db.RecordEasyPayProblemPayment(tran);

                                // TODO: Send Email to SI & LA (sof.Authority.Email)
                            }
                        }

                        // Log the file before deleting it
                        Logger.Write(string.Format("Successfully Processed SOF File: {0} at {1}", sof.FileName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")), "General", (int)TraceEventType.Information);

                        // Cleanup
                        file.Delete();
                    }
                    catch (Exception ex)
                    {
                        Logger.Write(string.Format("There was an error processing the SOF File {0}.\n{1}\n{2}", file.Name, ex.Message, ex.StackTrace), "General", (int)TraceEventType.Critical);
                    }

                    scope.Complete();
                }
            }
        }

        private static void SendEasyPayFiles()
        {
            // Get the pickup folder path
            string outputDirectory = ConfigurationManager.AppSettings["EasyPayPickupFolder"];
            if (outputDirectory.Length == 0)
                return;

            // Check the EasyPay Pickup Folder
            DirectoryInfo dir = new DirectoryInfo(outputDirectory);
            if (!dir.Exists)
            {
                Logger.Write("The EasyPay Pickup Folder does not exist!", "General", (int)TraceEventType.Warning);
                return;
            }

            // Get the export path
            string exportPath = ConfigurationManager.AppSettings["EasyPayExportPath"];
            DirectoryInfo exportDir = new DirectoryInfo(exportPath);

            // Loop the Traffic Data Files and send them the the EasyPay pickup folder
            string destination;
            foreach (FileInfo file in exportDir.GetFiles("*" + TrafficDataFile.EXTENSION))
            {
                destination = Path.Combine(outputDirectory, file.Name);
                try
                {
                    file.CopyTo(destination, true);
                    file.Delete();
                    Logger.Write(string.Format("Easypay file moved to Pickup folder: {0}", destination), "General", (int)TraceEventType.Information);
                }
                catch (Exception ex)
                {
                    Logger.Write(ex.Message, "General", (int)TraceEventType.Critical);
                }
            }
        }

        private static void GenerateFeedbackFiles(DatabaseProvider db)
        {
            DirectoryInfo fbPath = new DirectoryInfo(ConfigurationManager.AppSettings["FeedbackPath"].ToString());

            if (!fbPath.Exists)
            {
                Logger.Write(string.Format("The Feedback path {0} does not exist!", fbPath.ToString()), "General", (int)TraceEventType.Warning);
                return;
            }

            // Get the list of authorities with feedback data to send
            List<RemoteAuthority> authorities = new List<RemoteAuthority>();
            db.GetRemoteAuthoritiesForExport(authorities);

            List<FineExchangeFeedbackFile> files = new List<FineExchangeFeedbackFile>();
            foreach (RemoteAuthority authority in authorities)
            {
                Logger.Write(string.Format("Processing data for {0} ...", authority.Name), "General", (int)TraceEventType.Information);

                try
                {
                    // Make sure the authority has a valid machine name
                    if (authority.MachineName.Length == 0)
                    {
                        Logger.Write(string.Format("{0} does not have a machine name to send responses back to!", authority.Name), "General", (int)TraceEventType.Warning);
                        continue;
                    }

                    // Process data into feedback files
                    var file = new FineExchangeFeedbackFile(authority);
                    file.WriteHeader();

                    if (authority.IsEasyPay)
                        db.GetEasyPayAuthorityFeedbackData(file);
                    db.GetPaymentInformation(file);

                    file.Close();

                    files.Add(file);
                    Logger.Write(string.Format("\tCreated Feedback File: {0}", Path.GetFileName(file.FileName)), "General", (int)TraceEventType.Information);
                }
                catch (Exception ex)
                {
                    Logger.Write("ERROR creating feedback files." + ex.Message, "General", (int)TraceEventType.Critical);
                }

            }

            // Send all these files
            if (files.Count > 0)
                Program.SendFeedbackFiles(files, db);
        }

        private static void SendFeedbackFiles(IEnumerable<FineExchangeFeedbackFile> files, DatabaseProvider db)
        {
            foreach (FineExchangeFeedbackFile file in files)
            {
                try
                {
                    // Set the file's destination and send it
                    //Program.indaba.Destination = file.Authority.MachineName;
                    //Added by Oscar 2011/09/29
                    //In order to keep the Indaba interface unchanged,we decided to pass client ID as the parameter to take the place of IndabaDestination.
                    //Code changes will happen in Indaba client to look up  the destination site based on  the passed client ID.
                    Program.indaba.Destination = file.Authority.AuthorityCode;

                    var fi = new FileInfo(file.FileName);
                    Guid id = Program.indaba.Enqueue(fi.OpenRead(), fi.Name);
                    fi = null;

                    // Make sure there were no problems adding the file to Indaba
                    if (id.Equals(Guid.Empty))
                        Logger.Write(string.Format("There was a problem enqueueing the file {0}", file.FileName), "General", (int)TraceEventType.Warning);
                    else
                        db.SetFileSent(file);

                    // Cleanup the file
                    file.Dispose();

                    Logger.Write(string.Format("Sent {0} to {1} ({2}).", file.FileName, file.Authority.MachineName, id.ToString()), "General", (int)TraceEventType.Information);
                }
                catch (Exception ex)
                {
                    Logger.Write(string.Format("Error Sending Feedback file {0} to {1}.\n{2}\n{3}", file.FileName, file.Authority.MachineName, ex.Message, ex.StackTrace), "General", (int)TraceEventType.Critical);
                }
            }
        }

        private static void ProcessHoldBackFiles(IEnumerable<FileInfo> files, DatabaseProvider db)
        {
            try
            {
                var sb = new StringBuilder();

                foreach (FileInfo file in files)
                {
                    var tdf = new TrafficDataFile(file.FullName);
                    tdf.Read();
                    if (tdf.HasErrors)
                    {
                        // Log the Files to the logger and email them
                        Program.BuildErrorMessage(sb, tdf);

                        // Process files into the database
                        db.SetHoldbackErrors(tdf);
                    }

                    // Cleanup the processed File
                    try
                    {
                        file.Delete();
                    }
                    catch (IOException e)
                    {
                        Logger.Write(
                            string.Format("There was a problem trying to delete the holdback file: {0}.\n{1}", file.Name,
                                          e), "General", (int)TraceEventType.Critical);
                    }
                }

                // Email the error messages
                if (sb.Length > 0)
                {
                    string subject = "Errors in an EasyPay Holdback File.";
                    sb.Insert(0,
                              "The following errors were found while processing Holdback Files received from easyPay:\n\n");
                    sb.Append("\n\nRegards\n\nThabo Aggregator.");
                    Program.EmailErrorMessages(sb, subject, string.Empty);

                    Logger.Write("Sent Holdback File errors to " + ConfigurationManager.AppSettings["Email"], "General",
                                 (int)TraceEventType.Information);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(string.Format("Error in ProcessHoldBackFiles: {0} ", ex), "General", (int)TraceEventType.Error);
            }
        }

        private static void EmailErrorMessages(StringBuilder sb, string subject, string attachment)
        {
            try
            {
                string emailAddress = ConfigurationManager.AppSettings["Email"];
                var from = new MailAddress(emailAddress);
                var to = new MailAddress(emailAddress);

                var mail = new MailMessage(from, to);
                mail.Subject = subject;
                mail.Body = sb.ToString();
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = false;

                if (!string.IsNullOrEmpty(attachment))
                {
                    var myAttachment = new Attachment(attachment);
                    mail.Attachments.Add(myAttachment);
                }

                try
                {
                    var mailClient = new SmtpClient();
                    mailClient.Host = ConfigurationManager.AppSettings["SMTP"];
                    mailClient.UseDefaultCredentials = true;
                    mailClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;

                    mailClient.Send(mail);
                }
                catch (Exception smtpEx)
                {
                    Console.WriteLine("Main: Failed to send email file " + smtpEx.Message);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Main: Failed to send email file " + ex.Message);
            }
        }

        private static void BuildErrorMessage(StringBuilder sb, TrafficDataFile file)
        {
            try
            {
                sb.Append("\nFile: ");
                sb.Append(Path.GetFileName(file.FileName));
                sb.Append(":\n");

                foreach (HoldbackFileError error in file.Errors)
                {
                    sb.Append("Error          - ");
                    sb.Append(error.Error);
                    sb.Append("\nAt line         - ");
                    sb.Append(error.LineNumber);
                    sb.Append('\n');
                    if (error.Transaction != null)
                    {
                        sb.Append("EasyPay number  - ");
                        sb.Append(error.Transaction.EasyPayNumber);
                        sb.Append("\nAction             - ");
                        sb.Append(error.Transaction.EasyPayAction);
                        sb.Append("\n\n");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(string.Format("Error in BuildErrorMessage: {0} ", ex), "General", (int)TraceEventType.Error);
            }
        }

        private static void GetHoldbackFiles(List<FileInfo> files)
        {
            try
            {
                // Get the EasyPay Pickup Folder, and if blank return immediately
                string importPath = ConfigurationManager.AppSettings["EasyPayPickupFolder"];
                if (importPath.Length == 0)
                    return;

                // Make sure the EasyPay Pickup Folder exists
                var dir = new DirectoryInfo(importPath);
                if (!dir.Exists)
                {
                    Logger.Write(string.Format("The import path ({0}) does not exist!", dir.FullName), "General",
                                 (int)TraceEventType.Warning);
                    return;
                }

                // Get the Holdback files in the EasyPay Pickup Folder
                foreach (FileInfo file in dir.GetFiles("HB*", SearchOption.TopDirectoryOnly))
                    files.Add(file);
            }
            catch (Exception ex)
            {
                Logger.Write(string.Format("Error in GetHoldbackFiles: {0} ", ex), "General", (int)TraceEventType.Error);
            }
        }

        private static DatabaseProvider ProcessFinesIntoDatabase(FineExchangeDataFile fine, ref bool isFullyProcessed)
        {
            DatabaseProvider db = null;
            try
            {
                db = new DatabaseProvider();
                if (db.Connect())
                {
                    string errorMessage = "";
                    db.UpdateData(fine, ref isFullyProcessed, ref errorMessage);

                    if (isFullyProcessed)
                    {
                        Logger.Write(string.Format("\tImported data for {0} fines for {1}", fine.NoticeCount, fine.Authority), "General", (int)TraceEventType.Information);
                    }
                    else
                    {
                        Logger.Write(string.Format("\tThe process fines routine returned an error: {0} ", errorMessage), "General", (int)TraceEventType.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(string.Format("Error in ProcessFinesIntoDatabase: {0} ", ex), "General", (int)TraceEventType.Error);
            }

            return db;
        }

        private static DatabaseProvider DbConnect()
        {
            var db = new DatabaseProvider();
            db.Connect();
            return db;
        }

        private static FineExchangeDataFile ProcessFineDataFiles(Stream stream)
        {
            // Import Files into Thabo Database
            //mrs 20080714 made it handle a single stream
            var fine = new FineExchangeDataFile(stream);

            //foreach (Stream stream in streams)
            //{
            try
            {
                //FineExchangeDataFile fedf = new FineExchangeDataFile(stream);
                if (fine.Read())
                {
                    return fine;
                    //fine.Add(fedf);
                    //fedf.Delete();
                }
            }
            catch (Exception ex)
            {
                Logger.Write("An error occurred reading the stream." + ex.Message, "General", (int)TraceEventType.Critical);
            }
            //}

            return null;
        }

        private static void ProcessEasyPayData(DatabaseProvider db)
        {
            var authorities = new List<EasyPayAuthority>();

            try
            {
                // Get the export path
                string exportPath = ConfigurationManager.AppSettings["EasyPayExportPath"];
                DirectoryInfo exportDir = new DirectoryInfo(exportPath);

                if (!exportDir.Exists)
                {
                    Logger.Write("The EasyPay Export path does not exist!", "General", (int)TraceEventType.Warning);
                    return;
                }

                Logger.Write(string.Format("\tProcessing EasyPay Data ({0})...", exportPath), "General", (int)TraceEventType.Information);
                // Create aggregated EasyPay files
                db.GetEasyPayDataToSend(authorities);
                Logger.Write(string.Format("Got {0} EasyPay Authorities with data to send.", authorities.Count), "General", (int)TraceEventType.Information);
                DateTime dt = DateTime.Now;

                foreach (EasyPayAuthority authority in authorities)
                {
                    //jerry 2012-1-16 add transaction
                    using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(10)))
                    {
                        if (authority.EasyPayTransactions.Count == 0)
                        {
                            Logger.Write(string.Format("No transactions for {0}.", authority), "General", (int)TraceEventType.Information);
                            continue;
                        }

                        TrafficDataFile tdf = new TrafficDataFile(authority);
                        tdf.CleanExportData();

                        string fileName = Path.Combine(exportPath, string.Format("TF{0:0000}{1}{2}",
                            authority.ReceiverIdentifier, DateTime.Now.ToString(TrafficDataFile.DATE_FORMAT), TrafficDataFile.EXTENSION));
                        FileInfo file = new FileInfo(fileName);
                        if (!file.Directory.Exists)
                            file.Directory.Create();

                        // FBJ (2009-05-26): Added a try/catch to catch the throwing of a new exception in the CreateFile Method when the file already exists for the day
                        Logger.Write(string.Format("Creating EasyPay file {0}", fileName), "General", (int)TraceEventType.Information);
                        try
                        {
                            //jerry 2012-03-09 add this conditon, so create file method can create file immediately
                            //Jerry 20120309: We have found a problem in FreeState that 3 different authorties are using the same EasyPay receiver ID which resulted in the same file name.The current code hereby will throw out an exception which will be captuered in the parent method which will invoke rollback and delete the file.
                            //Jerry 2012-04-09 if file is exists then append data, else create file
                            FileInfo fi = new FileInfo(fileName);
                            if (fi.Exists && fi.Length > 0)
                            {
                                //dls 2012-05-23 - put this back - files are being overwritten later
                                Logger.Write("The file: " + fileName + " already exists and has data in it, it cannot be overwritten.");
                                continue;
                                //tdf.AppendDataToFile(fileName);
                                //tdf.HandleTrafficDataFile(fileName);
                            }
                            else
                            {
                                tdf.CreateFile(fileName);
                            }

                            foreach (EasyPayTransaction tran in tdf.Authority.EasyPayTransactions)
                                db.SetEasyPayDataFileName(tran.EasyPayNumber, tran.EasyPayAction, fileName);

                            Logger.Write(string.Format("Created Daily Traffic Data File: {0}", tdf.FileName), "General", (int)TraceEventType.Information);
                        }
                        catch (Exception ex)
                        {
                            //jerry 2012-1-18 add delete file
                            File.Delete(fileName);
                            Logger.Write(ex.StackTrace, "General", (int)TraceEventType.Critical);
                            if (scope != null)
                                scope.Dispose();
                            continue;
                        }

                        //jerry 2012-1-19 add
                        //db.SetEasyPaySentDateByAutIntNo(authority.ID);
                        db.SetEasyPaySentDateByAutIntNo(authority.ReceiverIdentifier);

                        scope.Complete();
                    }
                }

                TimeSpan ts = DateTime.Now - dt;
                Logger.Write(string.Format("Traffic data files created in {0} - {1}", exportPath, ts), "General", (int)TraceEventType.Information);
            }
            catch (Exception ex)
            {
                Logger.Write(ex.StackTrace, "General", (int)TraceEventType.Critical);
            }
        }

        private static List<Stream> GetFineExchangeDataFiles()
        {
            var files = new List<Stream>();

            IndabaFileInfo[] fileInfo = Program.indaba.GetFileInformation(FineExchangeDataFile.EXTENSION);
            if (fileInfo == null || fileInfo.Length == 0)
                return files;

            var timer = new Stopwatch();
            foreach (IndabaFileInfo file in fileInfo)
            {
                FineExchangeDataFile fine = null;
                try
                {
                    timer.Reset();
                    timer.Start();
                    bool isFullyProcessed = false;

                    Stream stream = Program.indaba.Peek(file);
                    timer.Stop();
                    Logger.Write(string.Format("Peeked file File: {0}, in {1}", file.Name, timer.Elapsed), "General", (int)TraceEventType.Information);
                    timer.Reset();

                    //FileStream stream = new FileStream(@"C:\Users\francis.MRSA\Desktop\MRSA\363 George FEDF\FEDF.txt", FileMode.Open, FileAccess.Read);
                    //files.Add(stream);

                    //mrs 20080714 must delay the dequeue until after the file has been fully processed
                    //mrs 20090115 the "return" kicks out of the indaba foreach loop - if one file is wrong then any subsequent file in the collection will not load

                    // Process the data files
                    // process one at a time so that we can handle failure elegantly
                    //List<FineExchangeDataFile> fines = Program.ProcessFineDataFiles(files);

                    timer.Start();
                    fine = Program.ProcessFineDataFiles(stream);
                    timer.Stop();
                    Logger.Write(string.Format("Processing fine data file for: {0}, in {1}", file.Name, timer.Elapsed), "General", (int)TraceEventType.Information);
                    timer.Reset();

                    if (fine == null || fine.Authority == null)
                    {
                        //throw toys
                        Logger.Write(string.Format("There was no data for File: {0}", file.Name), "General", (int)TraceEventType.Warning);
                        //files = null;
                        //return files;
                        continue;
                    }

                    // Update the Thabo database with the fine data
                    timer.Start();
                    DatabaseProvider db = Program.ProcessFinesIntoDatabase(fine, ref isFullyProcessed);
                    timer.Stop();
                    Logger.Write(string.Format("Processing fines into database for File: {0}, in {1}", file.Name, timer.Elapsed), "General", (int)TraceEventType.Information);
                    timer.Reset();
                    if (!db.IsConnected)
                    {
                        Logger.Write("Program is aborting as the database connection has not been configured correctly, or is not available.", "General", (int)TraceEventType.Error);
                        files = null;
                        return files; //return is good here - there is a disaster!
                    }

                    //091030 Fred Generate ResponseFile
                    // Get the ResponseFile folder path
                    string responseFilePath = ConfigurationManager.AppSettings["ResponseFile"];
                    timer.Start();
                    
                    //Set FileName
                    // Get the authority's machine name 
                    db.GetMachineName(fine);
                    if (string.IsNullOrEmpty(fine.MachineName))
                    {
                        Logger.Write(string.Format("The machine name for auth code {0} could not be found.", fine.MachineName), "General", (int)TraceEventType.Warning);
                        continue;
                    }

                    //Program.indaba.Destination = fine.MachineName;
                    //Added by Oscar 2011/09/29
                    //In order to keep the Indaba interface unchanged,we decided to pass client ID as the parameter to take the place of IndabaDestination.
                    //Code changes will happen in Indaba client to look up  the destination site based on  the passed client ID.
                    Program.indaba.Destination = fine.Authority.AuthorityCode;

                    // Create the response file
                    string strfileName = responseFilePath + fine.Authority.AuthorityCode + "_" +
                                         DateTime.Now.ToString(ResponseFile.DATE_FORMAT) +
                                         ResponseFile.RESPONSE_FILE_EXTENSION;

                    Logger.Write(string.Format("Creating response file for File: {0}", file.Name), "General", (int)TraceEventType.Information);
                    var responseFile = new ResponseFile(fine, strfileName);
                    if (responseFile.CreateFile())
                    {
                        Guid id = Program.indaba.Enqueue(strfileName);

                        // Check it was enqueued successfully
                        if (!id.Equals(Guid.Empty))
                        {
                            // Delete the file
                            File.Delete(strfileName);
                        }
                        else
                        {
                            timer.Stop();
                            Logger.Write(string.Format("Response file {0} enqueue failed for ({1}), in {2}.", strfileName, fine.MachineName, timer.Elapsed), "General", (int)TraceEventType.Warning);
                            timer.Reset();
                        }
                    }

                    if (isFullyProcessed)
                    {
                        Program.indaba.Dequeue(file);
                        Logger.Write(string.Format("De-queued File: {0}", file.Name), "General", (int)TraceEventType.Information);
                    }
                    else
                    {
                        Logger.Write(
                            string.Format(
                                "A program was detected updating the Thabo database with fine data for LA: {0}",
                                fine.Authority), "General", (int)TraceEventType.Warning);
                        //files = null;
                        //return files;
                        continue;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Write(string.Format("There was a general error trying to send an IRF files to: {0}.\r\n{1}", fine == null ? null : fine.Authority, ex), "General", (int)TraceEventType.Warning);
                }
            }
            return files;
        }

    }
}
