﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.SqlClient;
using System.Configuration;

using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.EntLib;
using System.Reflection;
using System.Threading;
using System.Globalization;
using SIL.AARTO.BLL.Culture;


namespace SIL.AARTO.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });

            //routes.IgnoreRoute("{*img}", new { img = @"(.*/)?\w+(.jpg|.gif|.png)" });

            routes.MapRoute(
                 "Default",
                 "{controller}/{action}",
                 new { controller = "Home", action = "Index" }
             );

            routes.MapRoute(
                "AppConfig",
                "AppConfig/{action}",
                new { controller = "AppConfig", action = "Edit" }
            );
        
            routes.MapRoute(
                "InfringerOption",
                "InfringerOption/{action}/{id}",
                new { controller = "InfringerOption", action = "AARTO04Decision", id = "" }
            );
        }

        protected void Application_Start()
        {

            string APP_TITLE = string.Empty;
            //string APP_NAME = AartoProjectList.AARTOWebApplication.ToString();
            string APP_NAME = "AARTO_Opus";
            string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
            Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);

            RegisterRoutes(RouteTable.Routes);

            // Add the DynamicPDF License
            ceTe.DynamicPDF.Document.AddLicense("DPS70NPDFJJGEJFZ9ikRGHBua2M3Sl0Pwtw63QxBds5agxfAQMSTfCfEtiI2O6I13jHEsL6smMhYn63QJBLCZ3B8XtMbaTRJ+C1w");

            SqlConnection conn = null; ;
            try
            {
                // Check Last updated Version            
                string errorMessage;
                if (!CheckVersionManager.CheckVersion(AartoProjectList.AARTOWebApplication, out errorMessage))
                {
                    EntLibLogger.WriteLog(LogCategory.General, null, errorMessage);
                    //Response.Write(errorMessage);
                    //Response.Redirect("/Home/ApplicationError?errorMessage=" + errorMessage);
                    Application["ErrorMessage"] = errorMessage;
                    throw new Exception(errorMessage);
                }

                if (!CheckVersionManager.CheckVersion(AartoProjectList.TMS, out errorMessage))
                {
                    EntLibLogger.WriteLog(LogCategory.General, null, errorMessage);
                    //Response.Write(errorMessage);
                    //Response.Redirect("/Home/ApplicationError?errorMessage=" + errorMessage);
                    Application["ErrorMessage"] = errorMessage;
                    throw new Exception(errorMessage);
                }

                // Write the started Log
                APP_TITLE = string.Format("{0} - Version {1}\n Last Updated {2}\n Started at: {3}",
                    APP_NAME,
                    Assembly.GetExecutingAssembly().GetName().Version.ToString(2),
                    ProjectLastUpdated.AARTOWebApplication, DateTime.Now);


                string strDatabaseInfo = EntLibLogger.GetServerAndDatabaseName(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());

                //Logger.Write(string.Format("{0} \n\r {1}", APP_TITLE, strDatabaseInfo), "General", (int)TraceEventType.Information);
                EntLibLogger.WriteLog(LogCategory.General, null, string.Format("{0} \n\r {1}", APP_TITLE, strDatabaseInfo));

                //SqlDependency.Start(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString); 
                
                // if usequeue we need to test connection to queue database here
                bool useQueue = false;

                if (ConfigurationManager.AppSettings.AllKeys.Contains("UseQueue"))
                {
                    useQueue = Convert.ToBoolean(ConfigurationManager.AppSettings["UseQueue"]);
                }
                if (useQueue)
                {
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["QueueConnectionString"].ConnectionString);
                    conn.Open();
                }
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteErrorLog(ex, LogCategory.Exception, "AARTO_Opus");
                Application["ErrorMessage"] = ex.Message;
                throw ex;
            }
            finally
            {
                if (conn != null && conn.State == System.Data.ConnectionState.Open)
                    conn.Close();
            }
        }

        protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {

            Config.ProcessMultiLanguage(HttpContext.Current);

            // 2015-01-15, Oscar added (bontq 1771, ref 1770)
            Config.ResetCultureNumberFormat();
        }

        protected void Application_BeginRequest()
        {
            var a = "a";
        }

        protected void Application_End()
        {
            //SqlDependency.Stop(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString); 
        }
    }
}