﻿using System;
using System.Collections.Generic;
using SIL.AARTO.BLL.Account;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.UserAutoLogin;
using System.Web;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.Web
{
    public partial class AutoLogin : System.Web.UI.Page
    {
        private AartoAndTMSAutoLogin autoLoginService = new AartoAndTMSAutoLogin();
        protected void Page_Load(object sender, EventArgs e)
        {
            EntLibLogger.WriteLog(LogCategory.General, null, "autoLoginService Started" + DateTime.Now);

            string PageID = Request.QueryString["PageID"];
            string Key = Request.QueryString["Key"];

            string Ip = string.Empty;
            try
            {
                Ip = GetServerIp();
            }
            catch (Exception ex)
            {
                // Application["ErrorMessage"] = String.Format("Error Message:{0}" , ex.Message);

                SIL.AARTO.BLL.EntLib.EntLibLogger.WriteErrorLog(ex, "Error", "SIL.AARTO.Web");

                //Response.Redirect("/Home/ApplicationError");

            }
            string HostName = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
            string tmsDomain = string.Empty;

            //AartoLoginSynchronization aartoLoginSynchronization = null;
            AartoAndTMSAutoLoginEntity aartoLoginSynchronization = null;
            if (String.IsNullOrEmpty(Key))
            {
                aartoLoginSynchronization = new AartoAndTMSAutoLoginEntity();
            }
            else
            {
                aartoLoginSynchronization = autoLoginService.GetLoginSynchronizationByKey(Key);
            }
            if (String.IsNullOrEmpty(aartoLoginSynchronization.AaLogSynSecretKey))
            {
                // If SecretKey is null , request from AARTO to TMS
                if (!String.IsNullOrEmpty(PageID))
                {

                    AartoPageList aartoPage = UserLoginManager.GetPageByPageID(Convert.ToInt32(PageID));
                    if (aartoPage != null)
                    {
                        UserLoginInfo userLoginInfo = (UserLoginInfo)Session["UserLoginInfo"];
                        if (userLoginInfo == null)
                        {
                            Response.Redirect("~/Account/Login");//2014-09-04 Heidi changed for fixing issue that when Session Expired-Web site there is 404 error page(bontq1483)
                        }
                        if (CheckUserInRole(userLoginInfo.UserRoles, aartoPage.AaUserRoleId.Value))
                        {
                            EntLibLogger.WriteLog(LogCategory.General, null, "autoLoginService Doing Something" + DateTime.Now);

                            string autIntNo = string.Empty;
                            string authName = string.Empty;
                            if (Session["DefaultAuthority"] != null)
                            {
                                autIntNo = Session["DefaultAuthority"].ToString();
                            }
                            if (Session["autName"] != null)
                            {
                                authName = Session["autName"].ToString();
                            }
                            try
                            {
                                aartoLoginSynchronization = autoLoginService.CreateUserKeyByUserIDAndAutID(userLoginInfo.UserIntNo, userLoginInfo.AuthIntNo);
                                if (aartoLoginSynchronization == null || String.IsNullOrEmpty(aartoLoginSynchronization.AaLogSynSecretKey))
                                {
                                    Application["ErrorMessage"] = "Either version check or connection to the database by WebService  has failed . Please check the log files of WebService  for details.";
                                    Response.Redirect("~/Home/ApplicationError"); //2014-09-04 Heidi changed for fixing issue that when Session Expired-Web site there is 404 error page(bontq1483)

                                }
                            }
                            catch (Exception ex)
                            {
                                EntLibLogger.WriteLog(LogCategory.Error, null, ex.Message );

                                Application["ErrorMessage"] =String.Format("Either version check or connection to the database by WebService  has failed . Please check the log files of WebService  for details.");
                                Response.Redirect("~/Home/ApplicationError");//2014-09-04 Heidi changed for fixing issue that when Session Expired-Web site there is 404 error page(bontq1483)
                            }
                            //string tmsDomain = System.Configuration.ConfigurationManager.AppSettings["TMSDomainURL"];
                            if (String.IsNullOrEmpty(Ip))
                            {
                                tmsDomain = System.Configuration.ConfigurationManager.AppSettings["TMSDomainURL"];
                            }
                            else if (System.Configuration.ConfigurationManager.AppSettings["AARTODomainURL"].IndexOf(Ip) >= 0
                                || System.Configuration.ConfigurationManager.AppSettings["AARTODomainURL"].IndexOf(HostName) >= 0)
                            {
                                tmsDomain = System.Configuration.ConfigurationManager.AppSettings["TMSDomainURL"];
                            }
                            else if (System.Configuration.ConfigurationManager.AppSettings["AARTODomainExternalURL"].IndexOf(Ip) >= 0
                                || System.Configuration.ConfigurationManager.AppSettings["AARTODomainExternalURL"].IndexOf(HostName) >= 0)
                            {
                                tmsDomain = System.Configuration.ConfigurationManager.AppSettings["TMSDomainExternalURL"];
                            }
                            else
                            {
                                Application["ErrorMessage"] = String.Format("Incorrect domain name in web.config");
                                Response.Redirect("~/Home/ApplicationError");//2014-09-04 Heidi changed for fixing issue that when Session Expired-Web site there is 404 error page(bontq1483)
                            }

                            Response.Redirect(String.Format(tmsDomain + 
                                "/AutoLogin.aspx?Key={0}&PageID={1}&AutIntNo={2}&AuthName={3}&LsCode={4}", 
                                aartoLoginSynchronization.AaLogSynSecretKey, 
                                aartoPage.AaPageId, 
                                autIntNo, 
                                authName,
                                (string)(Session["CurrentLanguage"] ?? string.Empty)));
                        }
                        else
                        {
                            Response.Write("<h3>No permission to access this page!<h3>");
                        }
                    }
                }
            }

            //else
            //{
            //    //Request from TMS to AARTO
            //    if (!String.IsNullOrEmpty(Key))
            //    {
            //        if (Key == aartoLoginSynchronization.AaLogSynSecretKey)
            //        {
            //            //Auto login aarto
            //            UserEntity user = autoLoginService.GetUserByUserID(aartoLoginSynchronization.AaLogSynUserID);

            //            UserLoginInfo userLoginInfo = new UserLoginInfo();

            //            //set session
            //            userLoginInfo.UserIntNo = user.UserIntNo;
            //            userLoginInfo.UserName = user.UserLoginName;
            //            userLoginInfo.UserEmail = user.UserEmail;
            //            userLoginInfo.AuthIntNo = user.UserDefaultAutIntNo;
            //            userLoginInfo.UserAccessLevel = user.UserAccessLevel;

            //            userLoginInfo.UserRoles = new List<int>();
            //            foreach (AartoUserRoleConjoin conj in
            //                UserLoginManager.GetUserRolesByUserIntNo(userLoginInfo.UserIntNo))
            //            {
            //                userLoginInfo.UserRoles.Add(conj.AaUserRoleId);
            //            }
            //            Session["UserLoginInfo"] = userLoginInfo;

            //            FormsAuthentication.SetAuthCookie(userLoginInfo.UserName, true);

            //            Response.Redirect("/" + PageID);
            //        }
            //    }

            //} 
        }

        private bool CheckUserInRole(List<int> userRoles, int roleID)
        {
            return userRoles.Contains(roleID);
        }

        private string GetServerIp()
        {
            string serverIP = string.Empty;
            try
            {
                if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                {
                    serverIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else
                {
                    if (HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] != null)
                    {
                        serverIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                //string msg = ex.Message;
                throw ex;
            }
            return serverIP;
        }

    }
}
