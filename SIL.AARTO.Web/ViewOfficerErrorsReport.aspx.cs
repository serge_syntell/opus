﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using SIL.AARTO.BLL.OfficerError.Model;
using SIL.AARTO.BLL.Utility;
using System.Data;
using SIL.AARTO.BLL.OfficerError;
using System.IO;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.AartoDocType;
using SIL.AARTO.BLL.Utility.Authority;

namespace SIL.AARTO.Web
{
    public partial class ViewOfficerErrorsReport : System.Web.UI.Page
    {
        private ReportDocument ReportDoc;
        private DiskFileDestinationOptions FileOPS;
        private ExportOptions ExOPS;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserLoginInfo"] == null || Session["ViewOfficerErrorsModel"] == null)
            {
                Response.Redirect("/Account/Login");
            }

            ReportDoc = new ReportDocument();

            FileOPS = new DiskFileDestinationOptions();
            ViewOfficerErrorsModel viewOfficerErrorsModel = (ViewOfficerErrorsModel)Session["ViewOfficerErrorsModel"];
            String reportPath = Server.MapPath("~/Reports/OfficerErrorsReport.rpt");

            UserLoginInfo userLoginInfo = (UserLoginInfo)Session["UserLoginInfo"];

            //get default authority
            int autInt = 0;
            if (Session["DefaultAuthority"] != null)
            {
                autInt = Convert.ToInt32(Session["DefaultAuthority"].ToString());
            }
            else
            {
                autInt = userLoginInfo.AuthIntNo;
            }

            

            DataSet ds = OfficerErrorsManager.GetOfficerErrorsForReport(autInt,
                viewOfficerErrorsModel.AaDocTypeID, viewOfficerErrorsModel.OfficerNo, viewOfficerErrorsModel.DateStart,
                viewOfficerErrorsModel.DateEnd, userLoginInfo.UserRoles, userLoginInfo.UserName);
            
            //ReturnPDF(reportPath, ds, this);
            ReportDoc.Load(reportPath);
            
            ReportDoc.SetDataSource(ds.Tables[0]);

            ReportDoc.SetParameterValue("ParaAutName", SIL.AARTO.BLL.Utility.Authority.AuthorityManager.GetAutNameByAutIntNo(autInt));
            ReportDoc.SetParameterValue("ParaDocTypeName", AartoDocumentTypeManager.GetAartoDocTypeName(viewOfficerErrorsModel.AaDocTypeID));
            ReportDoc.SetParameterValue("ParaDateStart", viewOfficerErrorsModel.DateStart);
            ReportDoc.SetParameterValue("ParaDateEnd", viewOfficerErrorsModel.DateEnd);
            
            MemoryStream ms = new MemoryStream();
            ms = (MemoryStream)ReportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
            ReportDoc.Dispose();

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(ms.ToArray());
            Response.End();
            //PunchStats805806 enquiry ViewOfficerErrors
        }

        

        
    }
}
