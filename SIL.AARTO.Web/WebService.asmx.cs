﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using Stalberg.TMS;
using System.Diagnostics;
using System.IO;
using System.Transactions;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.Web
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {

        private string connectionString;
        public WebService()
        {

            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;// Application["constr"].ToString();
        }


        [WebMethod(EnableSession = true)]
        public string FrameAction(ActionParam param)
        {
            //throw new Exception("a");
            string message = string.Empty;
            try
            {
                FrameList frameList = (FrameList)HttpContext.Current.Session["frameList"];
                bool isNaTISLast = (bool)HttpContext.Current.Session["NaTISLast"];
                bool adjAt100 = (bool)Session["adjAt100"];

                //Oscar 20111130 add DateRule for push queue.
                //int autIntNo = Convert.ToInt32(Session["autIntNo"]);
                //DateRulesDetails dateRule = new DateRulesDetails();
                //dateRule.AutIntNo = autIntNo;
                //dateRule.DtRStartDate = "NotOffenceDate";
                //dateRule.DtREndDate = "NotIssue1stNoticeDate";
                //dateRule.LastUser = "TMS";
                //DefaultDateRules rule = new DefaultDateRules(dateRule, this.connectionString);
                //int NoOfDays = rule.SetDefaultDateRule();

                int iDateRule = Convert.ToInt32(Session["NoOfDays"]);
                string AutCode = Session["AutCode"].ToString();

                QueueItemProcessor queProcessor = new QueueItemProcessor();
                QueueItem item = new QueueItem();
                FrameDB frameDB = new FrameDB(this.connectionString);

                //Oscar 20120412 changed group
                string violationType = frameDB.GetViolationType(Convert.ToInt32(param.FrameIntNo));

                if (param.ProcessName.Equals("VerifyFrame") || param.ProcessName.Equals("ReturnToNatis"))
                {
                    FrameUpdates frameUpdate = new FrameUpdates(this.connectionString, ref frameList);

                    // dls 070417 - some people are entereing spaces in the regno - need to eliminate them
                    param.Registration = param.Registration.Replace(@" ", @"").Trim().ToUpper();

                    using (TransactionScope scope = new TransactionScope())
                    {
                        message = frameUpdate.Verify(param.ProcessName,
                                                    param.RejReason,
                                                    Convert.ToInt32(param.RejIntNo),
                                                    param.Registration,
                                                    param.VehType,
                                                    param.VehMake,
                                                    param.UserName,
                                                    param.ProcessType,
                                                    Int32.Parse(param.StatusVerified),
                                                    Int32.Parse(param.StatusRejected),
                                                    Int32.Parse(param.StatusNatis),
                                                    param.StatusInvestigate,
                                                    Int32.Parse(param.Speed1),
                                                    Int32.Parse(param.Speed2),
                                                    Convert.ToDateTime(param.OffenceDateTime),
                                                    param.FrameNo,
                                                    param.PreUpdatedRegNo,
                                                    param.PreUpdatedRejReason,
                                                    isNaTISLast,
                                                    Int32.Parse(param.FrameIntNo),
                                                    param.AllowContinue,
                                                    param.PreUpdatedAllowContinue,
                                                    Int64.Parse(param.RowVersion),
                                                    Convert.ToInt32(Session["autIntNo"]),
                                                    adjAt100,
                                                    param.InReIntNo);

                        //Oscar 20111130 add push queue.
                        if (string.IsNullOrEmpty(message))
                        {
                            FrameDetails frame = frameDB.GetFrameDetails(Convert.ToInt32(param.FrameIntNo));
                            if (frame.FrameStatus == "50" || frame.FrameStatus == "740")
                            {
                                item = new QueueItem();
                                item.Body = param.FrameIntNo.Trim();
                                item.Group = AutCode;
                                item.Priority = iDateRule - ((TimeSpan)(DateTime.Now - Convert.ToDateTime(param.OffenceDateTime))).Days;
                                item.QueueType = ServiceQueueTypeList.Frame_Natis;
                                queProcessor.Send(item);
                            }
                            else if (frame.FrameStatus == "800")
                            {
                                item = new QueueItem();
                                item.Body = param.FrameIntNo;
                                item.Group = string.Format("{0}|{1}", AutCode.Trim(), violationType);
                                TimeSpan ts = DateTime.Now - Convert.ToDateTime(param.OffenceDateTime);
                                item.Priority = iDateRule - ts.Days;
                                item.QueueType = ServiceQueueTypeList.Frame_Generate1stNotice;
                                queProcessor.Send(item);
                            }
                        }

                        scope.Complete();
                    }
                    //need to reset this here to update the ones that have been processed
                    HttpContext.Current.Session["frameList"] = frameList;


                }
                else if (param.ProcessName.Equals("AdjudicateFrame") || param.ProcessName.Equals("RejectFrame") || param.ProcessName.Equals("ReturnToNatisAdj"))
                {
                    string sessionAdjOfficerNo = HttpContext.Current.Session["OfficerNo"].ToString();
                    int sessionUSIntNo = Convert.ToInt32(HttpContext.Current.Session["USIntNo"]);

                    //dls 090619 - added to allow the officer to reject the frame
                    bool reject = param.ProcessName.Equals("RejectFrame") ? true : false;
                    bool sendToNatis = param.ProcessName.Equals("ReturnToNatisAdj") ? true : false;

                    FrameUpdates frameUpdate = new FrameUpdates(this.connectionString, ref frameList);

                    // dls 070417 - some people are entereing spaces in the regno - need to eliminate them
                    param.Registration = param.Registration.Replace(@" ", @"").Trim().ToUpper();

                    using (TransactionScope scope = new TransactionScope())
                    {
                        message = frameUpdate.Adjudicate(param.RejReason,
                                                        Convert.ToInt32(param.RejIntNo),
                                                        param.Registration,
                                                        param.ChangedMake,
                                                        param.VehType,
                                                        param.VehMake,
                                                        param.UserName,
                                                        param.ProcessType,
                                                        Int32.Parse(param.StatusProsecute),
                                                        Int32.Parse(param.StatusCancel),
                                                        Int32.Parse(param.StatusNatis),
                                                        param.FrameNo,
                                                        param.PreUpdatedRegNo,
                                                        param.PreUpdatedRejReason,
                                                        sessionAdjOfficerNo,
                                                        sessionUSIntNo,
                                                        Int32.Parse(param.FrameIntNo),
                                                        param.AllowContinue,
                                                        param.PreUpdatedAllowContinue,
                                                        Int64.Parse(param.RowVersion),
                                                        isNaTISLast,
                                                        Convert.ToInt32(Session["autIntNo"]),
                                                        Convert.ToBoolean((Session["NatisData"]) ?? false),
                                                        adjAt100,
                                                        reject,
                                                        sendToNatis,
                                                        param.SaveImageSettings,
                                                        Convert.ToDecimal(param.Contrast),
                                                        Convert.ToDecimal(param.Brightness));



                        if (string.IsNullOrEmpty(message))
                        {
                            DateTime dtOffence = Convert.ToDateTime(param.OffenceDateTime);
                            FrameDetails frame = frameDB.GetFrameDetails(Convert.ToInt32(param.FrameIntNo));

                            if (frame.FrameStatus == "600")
                            {
                                item = new QueueItem();
                                item.Body = param.FrameIntNo;
                                item.Group = string.Format("{0}|{1}", AutCode.Trim(), violationType);
                                TimeSpan ts = DateTime.Now - dtOffence;
                                item.Priority = iDateRule - ts.Days;
                                item.QueueType = ServiceQueueTypeList.Frame_Generate1stNotice;
                                queProcessor.Send(item);
                            }
                            else if (frame.FrameStatus == "740" || frame.FrameStatus == "50")
                            {
                                item = new QueueItem();
                                item.Body = param.FrameIntNo;
                                item.Group = AutCode;
                                TimeSpan ts = DateTime.Now - dtOffence;
                                item.Priority = iDateRule - ts.Days;
                                item.QueueType = ServiceQueueTypeList.Frame_Natis;
                                queProcessor.Send(item);
                            }
                        }
                        scope.Complete();
                    }

                    //need to reset this here to update the ones that have been processed
                    HttpContext.Current.Session["frameList"] = frameList;

                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(message))
                    message += Environment.NewLine;
                message += ex.Message;
            }
            return message;
        }

        //[WebMethod]
        //public bool RemoteConnect(string remoteHost, string shareName, string userName, string passWord)
        //{
        //    string StrConnectCommand = string.Format(@"NET USE \\{0}\{1} /User:{2} {3} /Persistent:Yes", remoteHost, shareName, userName, passWord);
        //    bool connectResult = RemoteCommand(StrConnectCommand);
        //    if (connectResult == false)
        //    {
        //        // if some error occour run disconnect and reconnect again only one time
        //        bool disConnectResult = RemoteCommand(@"net use \\" + remoteHost + @"\" + shareName + " /Delete");
        //        if (disConnectResult)
        //        {
        //            connectResult = RemoteCommand(StrConnectCommand);
        //        }
        //    }
        //    return connectResult;        
        //}

        [WebMethod]
        public bool RemoteCommand(string commandString)
        {
            bool flag = false;
            Process proc = new Process();
            try
            {
                proc.StartInfo.FileName = "cmd.exe";
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardInput = true;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.RedirectStandardError = true;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.StandardInput.WriteLine(commandString);
                proc.StandardInput.WriteLine("exit");
                while (!proc.HasExited)
                {
                    proc.WaitForExit(1000);
                }

                string errormsg = proc.StandardError.ReadToEnd();
                proc.StandardError.Close();
                if (String.IsNullOrEmpty(errormsg))
                {
                    flag = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                proc.Close();
                proc.Dispose();
            }
            return flag;
        }

        [WebMethod]
        public byte[] GetImagesFromRemoteFileServer(ScanImageDetails image)
        {
            return ImageHelper.GetImagesFromRemoteFileServer(image);
        }
    }
}