﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

using SIL.AARTO.BLL.Utility;
namespace SIL.AARTO.Web
{
    public partial class GetImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["id"] != null && SIL.AARTO.BLL.Utility.Validate.IsInt(Request["id"]))
            {
                long id = 0;
                id = Convert.ToInt64(Request["id"]);
                string imgPath = "";
                //imgPath=@"\\192.168.1.193\Images\test.jpg";
                imgPath = SIL.AARTO.BLL.InfringerOption.DecisionManager.GetAARTORepresentationDocumentImagePathByAaDocImgID(id);
                Bitmap bitmap = new Bitmap(imgPath);
                MemoryStream ms = new MemoryStream();
                Response.ContentType = "image/jpeg";
                bitmap.Save(Response.OutputStream, ImageFormat.Jpeg);
                bitmap.Dispose();
            }
        }
    }
}
