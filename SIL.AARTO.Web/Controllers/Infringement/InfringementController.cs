﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Infringement;
using SIL.AARTO.Web.Helpers.Paginator;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.ViewModels.Infringement;
using System.Configuration;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;


namespace SIL.AARTO.Web.Controllers.Infringement
{
    public class InfringementController : Controller
    {
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
        // GET: /Resubmission/
        InfringementManager infringementManager = new InfringementManager();
        public ActionResult Resubmission()
        {
            ResubmissionModel model = new ResubmissionModel();
            model.Page = 0;
            if (!String.IsNullOrEmpty(QueryString.GetString("Page")))
                model.Page = Convert.ToInt16(QueryString.GetString("Page"));

            if (!String.IsNullOrEmpty(QueryString.GetString("NotTicketNo")))
                model.NotTicketNo = QueryString.GetString("NotTicketNo");

            InitModel(model);

            return View(model);
        }

        [HttpPost]
        public ActionResult Resubmission(ResubmissionModel model)
        {
            InitModel(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult PostResubmissions(JsonRequest request)
        {
            Message msg = new Message() { Text = "" };

            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] != null)
                lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;

            if (request != null && request.Resubmissions.Count > 0)
            {
                int notIntNo = 0;
                int creIntNo = 0;
                string[] idArra = new string[2];
                foreach (string arra in request.Resubmissions)
                {
                    idArra = arra.Split('_');
                    if (idArra.Length > 1)
                    {
                        notIntNo = Convert.ToInt32(idArra[0]);
                        creIntNo = Convert.ToInt32(idArra[1]);
                        try
                        {
                            infringementManager.ResubmitInfringement(notIntNo, creIntNo, lastUser);
                        }
                        catch (Exception ex)
                        {
                            msg.Text += ex.Message + "\r\n";
                        }
                    }
                }
              
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.InfringementResubmission, PunchAction.Change);  

            }
            return Json(msg);
        }
        void InitModel(ResubmissionModel model)
        {
            int totalCount = 0;

            model.PageSize = Convert.ToInt16(ConfigurationManager.AppSettings["PageSize"]);

            model.ResubmissionList = infringementManager.GetResubmission(model.NotTicketNo, model.Page, model.PageSize, out totalCount);

            if (model.ResubmissionList.Count == 0 && model.Page > 0)
            {
                model.Page--;
                model.ResubmissionList = infringementManager.GetResubmission(model.NotTicketNo, model.Page, model.PageSize, out totalCount);
            }

            if (!String.IsNullOrEmpty(model.NotTicketNo))
                model.URLPara = string.Format("NotTicketNo={0}", model.NotTicketNo);
            else model.URLPara = null;

            model.TotalCount = totalCount;
        }

    }
}
