﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.ActDateMaintenance;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.Helpers.Paginator;
using SIL.AARTO.Web.Resource.ActDateMaintenance;
using SIL.AARTO.Web.ViewModels.ActDateMaintenance;
using SIL.ServiceQueueLibrary.DAL.Entities;

namespace SIL.AARTO.Web.Controllers.ActDateMaintenance
{
    [AARTOErrorLog]
    public partial class ActDateMaintenanceController : Controller
    {
        string connectString = System.Configuration.ConfigurationManager.AppSettings["AARTOConnectionString"];
        //
        // GET: /ActDateMaintenance/
        ActDateManager actDateManager = new ActDateManager();
        [HttpGet]
        public ActionResult PrintSummonsQueue()
        {
            PrintSummonsModel model = new PrintSummonsModel() { FirstLoad = String.IsNullOrEmpty(QueryString.GetString("Page")) };

            int pageIndex = 0;
            if (!String.IsNullOrEmpty(QueryString.GetString("Page")))
            {
                model.PageIndex = Convert.ToInt32(QueryString.GetString("Page"));
            }

            InitialModel(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult PrintSummonsQueue(PrintSummonsModel model)
        {
            model.PageIndex = 0;
            InitialModel(model);
            model.FirstLoad = false;
            return View(model);
        }

        [HttpPost]
        public ActionResult SetActionDate(int pfnIntNo, string actDate)
        {
            if (Session["autIntNo"] == null || Session["UserLoginInfo"] == null)
            {
                return Redirect("~/Account/Logon");
            }
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            string msg = string.Empty;
            bool isIBMPrinter = AuthorityRule.GetAuthorityRulesKeyValue("6209", autIntNo).Value.ToUpper().Trim().Equals("Y");

            int sqtIntNo = (int)(isIBMPrinter ? ServiceQueueTypeList.PrintToFile : ServiceQueueTypeList.PrintSummons);

            DateTime date = Convert.ToDateTime(actDate);
            int status = actDateManager.SetActionDate(sqtIntNo, pfnIntNo.ToString(), date, userInfo.UserName);
            switch (status)
            {
                case 0:
                    msg = PrintSummons.successMsg;
                    break;
                case -1:
                    msg = PrintSummons.errorMsg1;// "No data found in ServiceQueue table or it has been processed by other application.";
                    break;
                case -2:
                    msg = PrintSummons.errorMsg2;//"Action Date must be greater or equal than today.";
                    break;
                case -3:
                    msg = PrintSummons.errorMsg3;// "Action Date must be less than existing value in DB.";
                    break;

            }
            return Json(new { Status = status == 0, Message = msg });
        }

        void InitialModel(PrintSummonsModel model)
        {

            int totalCount = 0;
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            model.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
             
            if (model.CrtIntNo == 0)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["CrtIntNo"]))
                    model.CrtIntNo = Convert.ToInt32(Request.QueryString["CrtIntNo"]);
            }

            if (model.CourtDateFrom.HasValue == false)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["CourtDateFrom"]))
                    model.CourtDateFrom = Convert.ToDateTime(Request.QueryString["CourtDateFrom"]);
            }

            if (model.CourtDateTo.HasValue == false)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["CourtDateTo"]))
                    model.CourtDateTo = Convert.ToDateTime(Request.QueryString["CourtDateTo"]);
            }

            AuthorityRule.ConnnectionString = connectString;
            bool isIBMPrinter = AuthorityRule.GetAuthorityRulesKeyValue("6209", autIntNo).Value.ToUpper().Trim().Equals("Y");
            int sqtIntNo = (int)(isIBMPrinter ? ServiceQueueTypeList.PrintToFile : ServiceQueueTypeList.PrintSummons);
            model.Result = actDateManager.GetByCourtAndCourtDate(sqtIntNo, model.CrtIntNo, model.CourtDateFrom, model.CourtDateTo, model.PageIndex, model.PageSize, out totalCount);
            model.TotalCount = totalCount;


            if (model.CrtIntNo > 0)
            {
                model.URLPara = "CrtIntNo=" + model.CrtIntNo.ToString();
            } if (model.CourtDateFrom.HasValue)
            {
                //2014-11-07 Heidi changed for Court date range tool is consistent(bontq1693).
                model.URLPara += "&CourtDateFrom=" + model.CourtDateFrom.Value.ToString("yyyy/MM/dd");
            }
            if (model.CourtDateTo.HasValue)
            {
                //2014-11-07 Heidi changed for Court date range tool is consistent(bontq1693).
                model.URLPara += "&CourtDateTo=" + model.CourtDateTo.Value.ToString("yyyy/MM/dd");
            }
        }


    }
}
