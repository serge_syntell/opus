﻿using SIL.AARTO.BLL.PrintManagement;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.ViewModels.PrintManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace SIL.AARTO.Web.Controllers.PrintManagement
{
    public partial class PrintManagementController : Controller
    {
        private SAPOEquivalenceManager noticePrintCountractManager = new SAPOEquivalenceManager();

        [HttpGet]
        public ActionResult SAPOEquivalence(SAPOEquivalenceModel model)
        {
            SIL.AARTO.DAL.Entities.SapoEquivalence notice = null;
            int recordCount = 0, pageStart = 0, pageSize = 10;

            if (model != null)
            {
                notice = new SapoEquivalence
                {
                    SpEqSapoLabel=model.SearchStr, 
                    SpEqOpusLabel=model.SearchStr
                };
            }

            if (!Int32.TryParse(Request["page"], out pageStart))
            {
                pageStart = 0;
            }

            if (!Int32.TryParse(WebConfigurationManager.AppSettings["PageSize"], out pageSize))
            {
                pageSize = 10;
            }

            var list = noticePrintCountractManager.GetSAPOEquivalence(pageStart, pageSize, notice, out recordCount);
            IList<SAPOEquivalenceModel> modeList = new List<SAPOEquivalenceModel>();
           
            TempData["PageStart"] = pageStart;
            TempData["PageSize"] = pageSize;
            TempData["RecordCount"] = recordCount;

            foreach (var l in list)
            {

                modeList.Add(new SAPOEquivalenceModel
                {
                    SEIntNo = l.SpEqIntNo,
                     OPUSLable=l.SpEqOpusLabel,
                    OPUSDataType=l.SpEqOpusDataType,
                    OPUSDataLength=l.SpEqOpusDataLength,
                    SAPOLable=l.SpEqSapoLabel,
                    SAPODataType=l.SpEqSapoDataType,
                    SAPODataLength=l.SpEqSapoDataLength,
                    LastUser=l.LastUser,
                    //NPCDataTypeList=new SelectList(null,"",""),
                    //NPCSAPODataTypeList = new SelectList(null, "", ""),
                });
            }


            return View(modeList);
        }

        //[HttpPost]
        //public ActionResult NoticePrintContract(IList<NoticePrintContractModel> modelList)
        //{
        //    if (modelList == null)
        //    {
        //        return NoticePrintControl(new NoticePrintControlModel());
        //    }

        //    TList<NoticePrintContract> list = new TList<DAL.Entities.NoticePrintContract>();
            
        //    foreach (var model in modelList)
        //    {
        //        var entity = noticePrintCountractManager.GetNoticePrintContract(model.NPCIntNo);
        //        if (entity != null)
        //        {
        //            entity.NpcLable = model.NPCLable;
        //            entity.NpcDataType = model.NPCDataType;
        //            entity.NpcDataLength = model.NPCDataLength;
        //            entity.NpcNotices = model.NPCNotices;
        //            entity.NpcLastUser = model.NPCLastUser;
        //            entity.NpcsapoLable = model.NPCSAPOLable;
        //            entity.NpcsapoDataType = model.NPCSAPODataType;
        //            entity.NpcsapoDataLength = model.NPCSAPODataLength;
        //            list.Add(entity);
        //        }
        //    }

        //    noticePrintCountractManager.SaveNoticePrintContracts(list);
        //    return NoticePrintControl(new NoticePrintControlModel());

        //}
    }
}
