﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.PrintManagement;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.PrintManagement;
using SIL.AARTO.DAL.Entities;
using System.Web.Configuration;

namespace SIL.AARTO.Web.Controllers.PrintManagement
{
    public partial class PrintManagementController:Controller
    {
        private SapoPrintingControlManager noticePrintCountrolManager = new SapoPrintingControlManager();
        

        [HttpGet]
        public ActionResult SAPOPrintingControl(SAPOPrintingControlModel model)
        {
            SIL.AARTO.DAL.Entities.SapoPrintingControl notice = null;
            int recordCount = 0, pageStart = 0, pageSize =10 ;

            if (model != null)
            {
                notice = new SapoPrintingControl
                {
                    
                    SppcIntNo = model.SPPCIntNo,
                    SppcOption = model.SPPCOption,
                    SppcTemplate = model.SPPCTemplate,
                    SppcPrintMethod = model.SPPCPrintMethod,
                    LastUser = model.LastUser
                };
            }

            

            if (!Int32.TryParse(Request["page"], out pageStart))
            {
                pageStart = 0;
            }

            if (!Int32.TryParse(WebConfigurationManager.AppSettings["PageSize"], out pageSize))
            {
                pageSize = 10;
 
            }

            var list = noticePrintCountrolManager.GetNoticePrintControl(pageStart, pageSize, notice, out recordCount);
            IList<SAPOPrintingControlModel> modeList = new List<SAPOPrintingControlModel>();
            var channelList = list.Select(c => new { c.SppcPrintMethod, Text = c.SppcPrintMethod ? Resource.PrintManagement.SAPOPrintingControl.SPPCPrintMethodSAPO:
                Resource.PrintManagement.SAPOPrintingControl.SPPCPrintMethodOPUS
            }).Distinct().ToList();
            if (!channelList.Contains(new { SppcPrintMethod = true, Text = Resource.PrintManagement.SAPOPrintingControl.SPPCPrintMethodSAPO }))
            {
                channelList.Add(new { SppcPrintMethod = true, Text = Resource.PrintManagement.SAPOPrintingControl.SPPCPrintMethodSAPO });
            }

            if (!channelList.Contains(new { SppcPrintMethod = false, Text = Resource.PrintManagement.SAPOPrintingControl.SPPCPrintMethodOPUS }))
            {
                channelList.Add(new { SppcPrintMethod = false, Text = Resource.PrintManagement.SAPOPrintingControl.SPPCPrintMethodOPUS });
            }

             ViewData["PageStart"] = pageStart;
             ViewData["PageSize"] = pageSize;
             ViewData["RecordCount"] = recordCount;

             var reportList = noticePrintCountrolManager.GetSAPOReportType();

            foreach (var l in list)
            {
                var select = new SelectList(channelList, "SppcPrintMethod", "Text", l.SppcPrintMethod);

                modeList.Add(new SAPOPrintingControlModel
                {
                    SPPCIntNo = l.SppcIntNo,
                    SPPCOption = l.SppcOption,
                    SPPCTemplate = l.SppcTemplate,
                    SPPCPrintMethod = l.SppcPrintMethod,
                    SPRTIntNo=l.SprtIntNo,
                    SPRTReportName=(reportList.Where(c=>c.SprtIntNo==l.SprtIntNo).FirstOrDefault()??new SapoReportType()).SprtName,
                    RowVersion=l.Rowversion,

                    SPPCPrintMethodList = select
                });
            }


            return View(modeList);
        }

        [HttpPost]
        public void SAPOPrintingControl()
        {

            Int32 sppcIntNo=0;
            string printMethod = Request.Form["printMethod"];
            if(!Int32.TryParse(Request.Form["sPPCIntNo"],out sppcIntNo))
            {
                Response.Write(Resource.PrintManagement.SAPOPrintingControl.SPPCUpdateFailure);
                Response.End();
            }

            if (string.IsNullOrEmpty(printMethod))
            {
                 Response.Write(Resource.PrintManagement.SAPOPrintingControl.SPPCUpdateFailure);
                 Response.End();
            }




            TList<SapoPrintingControl> list = new TList<SIL.AARTO.DAL.Entities.SapoPrintingControl>();
            var model = noticePrintCountrolManager.GetNoticePrintControl(sppcIntNo);
            if (model == null)
            {
                Response.Write(Resource.PrintManagement.SAPOPrintingControl.SPPCNotExists);
             
            }
            else
            {
                model.SppcPrintMethod = printMethod.ToUpper().Equals("TRUE") ? true : false;
            }

            var exeFlag= noticePrintCountrolManager.SaveNoticePrintControls(new TList<SapoPrintingControl> { model });

            if (exeFlag)
            {
                Response.Write(Resource.PrintManagement.SAPOPrintingControl.SPPCUpdateSuccess);
            }
            else
            {
                Response.Write(Resource.PrintManagement.SAPOPrintingControl.SPPCUpdateFailure);
            }

            Response.End();
            
            //foreach (var model in modelList)
            //{
            //    var entity = noticePrintCountrolManager.GetNoticePrintControl(model.SPPCIntNo);
            //    if (entity != null)
            //    {
            //        entity.SppcPrintMethod = model.SPPCPrintMethod;
            //        entity.Rowversion = model.RowVersion;
            //        list.Add(entity);
            //    }
            //}

            //noticePrintCountrolManager.SaveNoticePrintControls(list);
            //return SAPOPrintingControl(new SAPOPrintingControlModel());

        }
    }
}