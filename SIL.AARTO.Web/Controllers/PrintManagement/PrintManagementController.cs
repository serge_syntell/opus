﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.PostalManagement;
using SIL.AARTO.BLL.PostalManagement;
using SIL.AARTO.BLL.Culture;
using System.Data;
using Stalberg.TMS;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using System.Transactions;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.PrintManagement;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.ViewModels.PrintManagement;
using SIL.AARTO.DAL.Data;

namespace SIL.AARTO.Web.Controllers.PrintManagement
{
    public partial class PrintManagementController : Controller
    {
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }

        SummonsService summonsService = new SummonsService();
        ImageFileServerService imageFileServerService = new ImageFileServerService();
        PrintFileNameService printFileNameService = new PrintFileNameService();
        PrintFileNameSummonsService printFileNameSummonsService = new PrintFileNameSummonsService();

        //
        // GET: /PrintManagement/

        public ActionResult PrintScheduleList(PrintScheduleModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            int? totalCount = 0;
            model.PageSize = Config.PageSize;

            //2014-01-10 added by Nancy for initial report type
            if (string.IsNullOrEmpty(model.rdoType))
                model.rdoType = "1ST";

            model.PrintList = new List<PrintScheduleDetail>();
            IDataReader printReader = new PrintFileNameService().GetPrintFileNameSchedule(autIntNo, model.rdoType, true, model.Page, Config.PageSize, ref totalCount);//2014-01-10 updated by Nancy for getting data depending on report type
            while (printReader.Read())
            {
                PrintScheduleDetail detail = new PrintScheduleDetail();
                detail.PFNIntNo = printReader["PFNIntNo"].ToString();
                detail.PrintFileName = printReader["PrintFileName"].ToString();
                detail.DocumentCount = printReader["DocumentCount"].ToString();
                detail.BatchGeneratedDate = printReader["DateAdded"].ToString() == "" ? "" : DateTime.Parse(printReader["DateAdded"].ToString()).ToString("yyyy-MM-dd");
                detail.BatDocOldestOffDate = printReader["BatDocOldestOffDate"].ToString() == "" ? "" : DateTime.Parse(printReader["BatDocOldestOffDate"].ToString()).ToString("yyyy-MM-dd");

                model.PrintList.Add(detail);
            }

            if (printReader.NextResult())
            {
                if (printReader.Read())
                {
                    model.TotalCount = int.Parse(printReader[0].ToString());
                }
            }
            printReader.Close();

            model.PostList = new List<PrintScheduleDetail>();
            IDataReader postReader = new PrintFileNameService().GetPrintFileNameSchedule(autIntNo, model.rdoType, false, model.SortExpr, Config.PageSize, ref totalCount);//2014-01-10 updated by Nancy for getting data depending on report type
            while (postReader.Read())
            {
                PrintScheduleDetail detail = new PrintScheduleDetail();
                detail.PFNIntNo = postReader["PFNIntNo"].ToString();
                detail.PrintFileName = postReader["PrintFileName"].ToString();
                detail.DocumentCount = postReader["DocumentCount"].ToString();
                detail.BatchGeneratedDate = postReader["DateAdded"].ToString() == "" ? "" : DateTime.Parse(postReader["DateAdded"].ToString()).ToString("yyyy-MM-dd");
                detail.BatDocOldestOffDate = postReader["BatDocOldestOffDate"].ToString() == "" ? "" : DateTime.Parse(postReader["BatDocOldestOffDate"].ToString()).ToString("yyyy-MM-dd");
                detail.ProdSchePriCapDate = postReader["ProdSchePriCapDate"].ToString() == "" ? "" : DateTime.Parse(postReader["ProdSchePriCapDate"].ToString()).ToString("yyyy-MM-dd");
                model.PostList.Add(detail);
            }

            if (postReader.NextResult())
            {
                if (postReader.Read())
                {
                    model.PostTotalCount = int.Parse(postReader[0].ToString());
                }
            }
            postReader.Close();
            //2014-01-13 Modified by Nancy start(Message for different kinds of reprot)
            if (model.TotalCount == 0 && model.PostTotalCount == 0)
            {
                model.IsShow = false;
                model.ErrorMsg = PrintSchedule_cshtml.ErrorMsg2;
                model.ErrorMsgPost = PrintSchedule_cshtml.ErrorMsg2;
            }
            else if (model.TotalCount == 0)
            {
                model.ErrorMsg = PrintSchedule_cshtml.ErrorMsg2;
                model.ErrorMsgPost = "";
                model.IsShow = true;
            }
            else if (model.PostTotalCount == 0)
            {
                model.ErrorMsgPost = PrintSchedule_cshtml.ErrorMsg2;
                model.ErrorMsg = "";
                model.IsShow = true;
            }//2014-01-13 Modified by Nancy end
            else
            {
                model.IsShow = true;
            }
            return View(model);
        }

        public JsonResult Print(string PFNIntNo)
        {
            Message message = new Message();
            message.Status = true;

            try
            {
                string[] split = PFNIntNo.Split(',');
                PrintFileNameQuery query = new PrintFileNameQuery();
                query.AppendIn(PrintFileNameColumn.PfnIntNo, split);

                SIL.AARTO.DAL.Entities.TList<PrintFileName> list = new PrintFileNameService().Find(query as IFilterParameterCollection);
                for (int i = 0; i < list.Count; i++)
                {
                    list[i].IsActuallyPrinted = true;
                    list[i].ProdSchePriCapDate = DateTime.Now;
                    list[i].ProdSchePriCapUser = this.LastUser;
                    list[i].LastUser = this.LastUser;
                }
                new PrintFileNameService().Save(list);
            }
            catch (Exception ex)
            {
                message.Status = false;
                message.Text = ex.ToString();
            }

            return Json(message);
        }

        public JsonResult Post(string PFNIntNo)
        {
            Message message = new Message();
            message.Status = true;

            try
            {
                string[] split = PFNIntNo.Split(',');
                PrintFileNameQuery query = new PrintFileNameQuery();
                query.AppendIn(PrintFileNameColumn.PfnIntNo, split);

                SIL.AARTO.DAL.Entities.TList<PrintFileName> list = new PrintFileNameService().Find(query as IFilterParameterCollection);
                for (int i = 0; i < list.Count; i++)
                {
                    list[i].IsActuallyPosted = true;
                    list[i].ProdSchePostCapDate = DateTime.Now;
                    list[i].ProdSchePostCapUser = this.LastUser;
                    list[i].LastUser = this.LastUser;
                }
                new PrintFileNameService().Save(list);
            }
            catch (Exception ex)
            {
                message.Status = false;
                message.Text = ex.ToString();
            }

            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PrintMobileS56ControlDocument(string startDate = "", string endDate = "", bool checkedCompletedFiles = false, int pageIndex = 0, string msg = "")
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            PrintMobileS56ControlDocumentModel model = new PrintMobileS56ControlDocumentModel();

            model.PageIndex = pageIndex;
            model.Msg = msg;
            try
            {
                if (!string.IsNullOrEmpty(startDate))
                    model.DateStart = string.Format(startDate, "yyyy-MM-dd");
                else
                    model.DateStart = DateTime.Now.Date.ToString("yyyy-MM-dd");

                if (!string.IsNullOrEmpty(endDate))
                    model.DateEnd = string.Format(endDate, "yyyy-MM-dd");
                else
                    model.DateEnd = DateTime.Now.Date.ToString("yyyy-MM-dd");

                InitModel(model);
                if (Session["HasConfirmed"] != null)
                    model.HasConfirmed = Session["HasConfirmed"].ToString();
                else
                    model.HasConfirmed = "0";
            }
            catch (Exception ex)
            {
                model.Msg = ex.Message;
            }

            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PrintMobileS56ControlDocument(PrintMobileS56ControlDocumentModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            InitModel(model);
            if (model.PagePrintFileList.Count <= 0)
                model.Msg = SIL.AARTO.Web.Resource.PrintManagement.PrintMobileS56ControlDocument.NoMobileControlDocumentName;
            else
                model.Msg = "";

            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ViewMobileS56ControlDocument()
        {
            // Get user info from session variable
            if (Session["UserLoginInfo"] == null)
                return RedirectToAction("login", "account");

            string mobileControlDocumentName;
            if (Request.QueryString["MobileControlDocumentName"] == null || string.IsNullOrEmpty(Request.QueryString["MobileControlDocumentName"].ToString()))
            {
                Response.Write(SIL.AARTO.Web.Resource.PrintManagement.PrintMobileS56ControlDocument.NoMobileControlDocumentName);
                Response.Flush();
                Response.End();
                return View();
            }
            mobileControlDocumentName = Request.QueryString["MobileControlDocumentName"].ToString().Trim();


            PrintFileName printFileNameEntity = printFileNameService.GetByPrintFileName(mobileControlDocumentName);
            if (printFileNameEntity == null)
            {
                Response.Write(SIL.AARTO.Web.Resource.PrintManagement.PrintMobileS56ControlDocument.InvalidMobileControlDocumentName);
                Response.Flush();
                Response.End();
                return View();
            }

            SIL.AARTO.DAL.Entities.TList<PrintFileNameSummons> printFileNameSummonsList = printFileNameSummonsService.GetByPfnIntNo(printFileNameEntity.PfnIntNo);
            foreach (PrintFileNameSummons printFileNameSummons in printFileNameSummonsList)
            {
                SIL.AARTO.DAL.Entities.Summons summonsEntity = summonsService.GetBySumIntNo(printFileNameSummons.SumIntNo);
                if (summonsEntity == null || summonsEntity.IsMobile == null || !summonsEntity.IsMobile.Value || summonsEntity.SumType.ToUpper() != "S56")
                {
                    Response.Write(SIL.AARTO.Web.Resource.PrintManagement.PrintMobileS56ControlDocument.IsNotMobile);
                    Response.Flush();
                    Response.End();
                    return View();
                }

                //if (summonsEntity.MobileControlDocumentPrintedDate != null && !string.IsNullOrEmpty(summonsEntity.MobileControlDocumentPrintedDate.Value.ToString()))
                //{
                //    Response.Write(string.Format(SIL.AARTO.Web.Resource.PrintManagement.PrintMobileS56ControlDocument.HasPrinted, summonsEntity.MobileControlDocumentName));
                //    Response.Flush();
                //    Response.End();
                //    return View();
                //}

            }

            try
            {
                string filePath = GetControlDocumentPath(mobileControlDocumentName);
                if (string.IsNullOrEmpty(filePath))
                {
                    Response.Write(SIL.AARTO.Web.Resource.PrintManagement.PrintMobileS56ControlDocument.PDFPathEmpty);
                    Response.Flush();
                    Response.End();
                    return View();
                }
                else
                {
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.ContentType = "application/pdf";
                    Response.WriteFile(filePath);
                    Response.End();

                    //update the mobile S56 summons print date
                    foreach (PrintFileNameSummons printFileNameSummons in printFileNameSummonsList)
                    {
                        SIL.AARTO.DAL.Entities.Summons summonsEntity = summonsService.GetBySumIntNo(printFileNameSummons.SumIntNo);
                        summonsEntity.MobileControlDocumentPrintedDate = DateTime.Now;
                        summonsService.Update(summonsEntity);
                    }

                    Session.Add("HasConfirmed", "1");

                    return View();
                }
            }
            catch (Exception ex)
            {
                Response.ContentType = "";
                Response.Write(ex.Message);
                Response.Flush();
                Response.End();
                return View();
            }
        }

        private void InitModel(PrintMobileS56ControlDocumentModel model)
        {
            model.TotalCount = 0;
            model.PageSize = Config.PageSize;

            List<PrintMobileS56ControlDocumentListModel> PagePrintFileList = new List<PrintMobileS56ControlDocumentListModel>();

            using (IDataReader reader = printFileNameService.GetPagedMobileS56ControlDocument(model.CheckedCompletedFiles, Convert.ToDateTime(model.DateStart), Convert.ToDateTime(model.DateEnd).AddDays(1), model.PageIndex, model.PageSize))
            {
                while (reader.Read())
                {
                    PrintMobileS56ControlDocumentListModel printMobileS56ControlDocumentListModel = new PrintMobileS56ControlDocumentListModel();
                    printMobileS56ControlDocumentListModel.MobileControlDocumentGeneratedDate = Convert.ToDateTime(reader["MobileControlDocumentGeneratedDate"]).ToString("yyyy-MM-dd HH:mm");
                    printMobileS56ControlDocumentListModel.MobileControlDocumentPrintedDate = reader["MobileControlDocumentPrintedDate"] == DBNull.Value ? "" : Convert.ToDateTime(reader["MobileControlDocumentPrintedDate"]).ToString("yyyy-MM-dd HH:mm");
                    printMobileS56ControlDocumentListModel.MobileControlDocumentName = reader["PrintFileName"].ToString();

                    PagePrintFileList.Add(printMobileS56ControlDocumentListModel);
                }

                if (reader.NextResult())
                {
                    if (reader.Read())
                    {
                        model.TotalCount = Convert.ToInt32(reader["TotalRowCount"]);
                    }
                }
            }

            model.PagePrintFileList = PagePrintFileList;
        }

        private string GetControlDocumentPath(string printFileName)
        {
            string controlDocumentPath = string.Empty;
            string imageMachineName = string.Empty;
            string imageShareName = string.Empty;

            PrintFileName printFileNameEntity = printFileNameService.GetByPrintFileName(printFileName);
            if (printFileNameEntity != null && printFileNameEntity.IfsIntNo != null)
            {
                ImageFileServer imageFileServerEntity = imageFileServerService.GetByIfsIntNo(printFileNameEntity.IfsIntNo.Value);
                if (imageFileServerEntity != null)
                {
                    imageMachineName = imageFileServerEntity.ImageMachineName;
                    imageShareName = imageFileServerEntity.ImageShareName;
                }
            }

            if (printFileNameEntity != null && !string.IsNullOrEmpty(imageMachineName) && !string.IsNullOrEmpty(imageShareName))
            {
                controlDocumentPath = @"\\" + imageMachineName + @"\" + imageShareName +
                        @"\" + printFileNameEntity.MobileControlDocumentPath;
            }

            return controlDocumentPath;
        }

    }
}
