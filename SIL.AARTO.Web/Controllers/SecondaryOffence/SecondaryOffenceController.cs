﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.SecondaryOffence;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Utility.SecondaryOffence;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using System.Collections.Specialized;
using SIL.AARTO.BLL.Admin;
using System.Configuration;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.Web.Controllers.SecondaryOffence
{
    public partial class SecondaryOffenceController : Controller
    {
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

        [HttpGet]
        public ActionResult SecondaryOffenceList(int ogIntNo = 0, int offIntNo = 0, int page = 0, string msg = "")
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            SecondaryOffenceModel model = new SecondaryOffenceModel();
            model.OffenceGroup = ogIntNo;
            model.Offence = offIntNo;
            model.PageIndex = page;
            model.ErrorMsg = msg;
            InitModel(model);

            return View(model);
        }

        [HttpPost]
        public ActionResult SearchOffenceGroup(SecondaryOffenceModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            return RedirectToAction("SecondaryOffenceList", new { ogIntNo = model.OffenceGroup, offIntNo = model.Offence, msg = model.ErrorMsg });
        }

        public JsonResult SelectOffence(int ogIntNo, int SeOfIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            //2014-01-23 Heidi fixed bug for when click "edit" button,offence dropdownlist no data selected.(5103)
            int offIntNo = 0;
            if (SeOfIntNo != null && SeOfIntNo!=0)
            {
                SecondaryOffenceService secondaryOffenceService = new SecondaryOffenceService();
                SIL.AARTO.DAL.Entities.SecondaryOffence secondaryOffenceEntity = secondaryOffenceService.GetBySeOfIntNo(SeOfIntNo);
                if (secondaryOffenceEntity != null)
                {
                    offIntNo = secondaryOffenceEntity.OffIntNo;
                }
            }

            return Json(new
            {
                result = 1,
                data = SIL.AARTO.BLL.Utility.Offence.OffenceManager.GetOffenceListByOGIntNo(ogIntNo),
                OffIntNo = offIntNo
                              
            });
        }

        [HttpPost]
        public ActionResult SaveSecondaryOffence(SecondaryOffenceModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];

            TList<SecondaryOffenceLookup> secondaryOffenceLookupList = GetSecondaryOffenceLookupList(Request.Form, model.SeOfIntNo, userInfo.UserName);
            SIL.AARTO.DAL.Entities.SecondaryOffence secondaryOffence = new SIL.AARTO.DAL.Entities.SecondaryOffence
            {
                SeOfIntNo = model.SeOfIntNo,
                SeOfCode = model.SeOfCode,
                OffIntNo = model.ChangeOffence,
                LastUser = userInfo.UserName,
                SeOfDescription = Request.Form["mul_en-US"]
            };

            string errorMsg = string.Empty;
            if (model.SeOfIntNo == 0)
            {
                errorMsg = ErrorMsgProcess(SecondaryOffenceManager.Add(secondaryOffence, secondaryOffenceLookupList), true);
                if (errorMsg == SIL.AARTO.Web.Resource.SecondaryOffence.SecondaryOffenceList.AddSuccessfully)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.SecondaryOffenceMaintenance, PunchAction.Add);  

                }
            }
            else
            {
                errorMsg = ErrorMsgProcess(SecondaryOffenceManager.Update(secondaryOffence, secondaryOffenceLookupList, model.SeOfIntNo), false);
                if (errorMsg == SIL.AARTO.Web.Resource.SecondaryOffence.SecondaryOffenceList.UpdateSucessfully)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.SecondaryOffenceMaintenance, PunchAction.Change); 
                }
            }

            return RedirectToAction("SecondaryOffenceList", new { msg = errorMsg });
        }

        [HttpGet]
        public ActionResult DeleteSecondaryOffence(int seOfIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            SecondaryOffenceManager.Delete(seOfIntNo);
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.SecondaryOffenceMaintenance, PunchAction.Delete); 

            return RedirectToAction("SecondaryOffenceList", new { msg = SIL.AARTO.Web.Resource.SecondaryOffence.SecondaryOffenceList.DeleteSuccessfully });
        }

        [HttpPost]
        public JsonResult GetSecondaryOffenceLangList(int seOfIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
            SecondaryOffenceEntity entity = new SecondaryOffenceEntity(seOfIntNo);
            return Json(new { result = 1, data = entity });
        }

        [HttpPost]
        public JsonResult GetAllByLanguage()
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            List<SecondaryOffenceLookup> lookupList = SecondaryOffenceManager.GetAllByLanguage();
            List<SecondaryOffenceEntity> entityList = new List<SecondaryOffenceEntity>();
            foreach (SecondaryOffenceLookup lookup in lookupList)
            {
                SecondaryOffenceEntity entity = new SecondaryOffenceEntity(lookup.SeOfIntNo);
                entity.SeOfDescription = lookup.SeOfDescription;
                entityList.Add(entity);
            }

            return Json(new { result = 1, data = entityList });
        }

        [HttpPost]
        public JsonResult GetSecondaryOffence(string filmNo, string frameNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            List<SecondaryOffenceLookup> lookupList = SecondaryOffenceManager.GetAllByLanguage();
            SecondaryOffenceEntity entity;

            Film filmEntity = new SIL.AARTO.DAL.Services.FilmService().GetByFilmNo(filmNo);
            if (filmEntity == null)
            {
                return Json(new { result = -1 });
            }
            Frame frameEntity = new SIL.AARTO.DAL.Services.FrameService().GetByFilmIntNoFrameNo(filmEntity.FilmIntNo, frameNo);
            if (frameEntity == null)
            {
                return Json(new { result = -1 });
            }

            FrameOffence frameOffence = new SIL.AARTO.DAL.Services.FrameOffenceService().GetByFrameIntNo(frameEntity.FrameIntNo).FirstOrDefault();
            SIL.AARTO.DAL.Entities.SecondaryOffence secondaryOffenceEntity;
            if (frameOffence != null)
            {
                secondaryOffenceEntity = new SIL.AARTO.DAL.Services.SecondaryOffenceService().GetByOffIntNo(frameOffence.OffIntNo).FirstOrDefault();
                if (secondaryOffenceEntity != null)
                {
                    foreach (SecondaryOffenceLookup lookup in lookupList)
                    {
                        if (lookup.SeOfIntNo == secondaryOffenceEntity.SeOfIntNo)
                        {
                            entity = new SecondaryOffenceEntity(lookup.SeOfIntNo);
                            entity.SeOfDescription = lookup.SeOfDescription;
                            return Json(new { result = 1, data = entity });
                        }
                    }
                }
            }

            return Json(new { result = -1 });
        }

        private string ErrorMsgProcess(int errorCode, bool isAdd)
        {
            string errorMsg = string.Empty;
            switch (errorCode)
            {
                case -1:
                    return SIL.AARTO.Web.Resource.SecondaryOffence.SecondaryOffenceList.DuplicateSecondaryOffence;
                default:
                    if(isAdd)
                        return SIL.AARTO.Web.Resource.SecondaryOffence.SecondaryOffenceList.AddSuccessfully;
                    else
                        return SIL.AARTO.Web.Resource.SecondaryOffence.SecondaryOffenceList.UpdateSucessfully;
            }
        }

        private TList<SecondaryOffenceLookup> GetSecondaryOffenceLookupList(NameValueCollection form, int seOfIntNo, string userName)
        {
            TList<SecondaryOffenceLookup> secondaryOffenceLookupList = new TList<SecondaryOffenceLookup>();
            foreach (string key in form.AllKeys)
            {
                if (key.Substring(0, 4) == "mul_")
                {
                    secondaryOffenceLookupList.Add(new SecondaryOffenceLookup()
                    {
                        SeOfDescription = Request.Form[key],
                        SeOfIntNo = seOfIntNo,
                        LsCode = key.Substring(4, key.Length - 4),
                        LastUser = userName
                    });
                }
            }
            return secondaryOffenceLookupList;
        }

        private void InitModel(SecondaryOffenceModel model)
        {
            int totalCount = 0;
            model.PageSize = Config.PageSize;
            model.OffenceGroupList = SIL.AARTO.BLL.Utility.OffenceGroup.OffenceGroupManager.GetOffenceGroupSelectList();
            model.OffenceList = SIL.AARTO.BLL.Utility.Offence.OffenceManager.GetOffenceSelectListByOGIntNo(model.OffenceGroup);

            model.ChangeOffenceGroupList = SIL.AARTO.BLL.Utility.OffenceGroup.OffenceGroupManager.GetOffenceGroupSelectList();
            model.ChangeOffenceList = SIL.AARTO.BLL.Utility.Offence.OffenceManager.GetOffenceSelectListByOGIntNo(model.ChangeOffenceGroup);

            model.PageSecondaryOffence = new SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.SecondaryOffence>();

            //Jerry 2013-06-28 add
            if (model.OffenceGroup > 0 && model.Offence <= 0)
            {
                model.PageSecondaryOffence = SecondaryOffenceManager.GetPageByOGIntNo(model.OffenceGroup, model.PageIndex, model.PageSize, out totalCount);
            }

            if (model.Offence > 0)
            {
                //model.PageSecondaryOffence = SecondaryOffenceManager.GetPage(model.PageIndex, model.PageSize, out totalCount);
                model.PageSecondaryOffence = SecondaryOffenceManager.GetPageByOffIntNo(model.Offence, model.PageIndex, model.PageSize, out totalCount);
            }
            //else
            //{
            //    model.PageSecondaryOffence = new SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.SecondaryOffence>();
            //}
           
            model.TotalCount = totalCount;
            model.LookupModel = new ViewModels.LookupPartViewModel { EnUSLength = 100 };
        }


        private class SecondaryOffenceEntity
        {
            public int SeOfIntNo { get; set; }
            public string SeOfCode { get; set; }
            public string SeOfDescription { get; set; }
            public byte[] RowVersion { get; set; }
            public string LastUser { get; set; }
            public bool IsAutomatic { get; set; }

            public List<LanguageLookupEntity> LanguageList { get; set; }

            public SecondaryOffenceEntity(int seOfIntNo)
            {
                SIL.AARTO.DAL.Entities.SecondaryOffence secondaryOffence = SecondaryOffenceManager.GetByseOfIntNo(seOfIntNo);
                SeOfIntNo = secondaryOffence.SeOfIntNo;
                SeOfCode = secondaryOffence.SeOfCode;
                SeOfDescription = secondaryOffence.SeOfDescription;
                LastUser = secondaryOffence.LastUser;
                IsAutomatic = secondaryOffence.IsAutomatic;
                RowVersion = secondaryOffence.RowVersion;
                LanguageList = new List<LanguageLookupEntity>();

                foreach (SecondaryOffenceLookup lookup in secondaryOffence.SecondaryOffenceLookupCollection)
                {
                    LanguageList.Add(new LanguageLookupEntity()
                    {
                        LookupValue = lookup.SeOfDescription,
                        LsCode = lookup.LsCode,
                        LookUpId = lookup.SeOfIntNo.ToString()
                    });
                }

            }
        }

    }
}
