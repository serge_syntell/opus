using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using SIL.AARTO.Web.Helpers;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.BLL.Admin.Model;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.UserMenu;
using SIL.AARTO.BLL.CameraManagement;
using SIL.AARTO.DAL.Entities;
using System.Data;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.Admin
{
    public partial class AdminController : Controller
    {
        //
        // GET: /Admin/
        UserMenuManager menuManager = new UserMenuManager();
        private  MenuUrlManager manager = new MenuUrlManager();
        public ActionResult MenuManage()
        {
            MenuModel model = new MenuModel();
            //ViewData["language"] = Language.EnvironmentLanguage;
            ViewData["language"] = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post),Authorize]
        public ActionResult MenuManage(MenuModel model)
        {
            UpdateModel<MenuModel>(model);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult CreateMenu(decimal parentMenuID, string menuName, string menuDesc, string orderNo, string language, string lastUser, string menuListStr)
        {
            Message message = new Message();
            UserMenuManager menuManager = new UserMenuManager();
            byte order = Convert.ToByte(orderNo);
            decimal menuID = menuManager.AddMenu(parentMenuID, menuName, menuDesc, order, language, lastUser);
            message.Text = menuID.ToString();
            if (!String.IsNullOrEmpty(message.Text) && message.Text.Trim() != "0")
            {
                message.Status = true;
            }

            //add to MenuLookup by menuId
            if (menuListStr != "" && menuListStr!=null)
            {
                string[] pagelistValue = menuListStr.Split(';');
                for (int i = 0; i < pagelistValue.Length-1; i += 3)
                {
                    AartoMenuLookupService menuLookupService = new AartoMenuLookupService();
                    AartoMenuLookup menuItemGet = menuLookupService.GetByAaMeIdAaMlLanguage(menuID, pagelistValue[i]);
                    if (menuItemGet == null)
                    {
                        AartoMenuLookup menuItem = new AartoMenuLookup();
                        menuItem.AaMeId = menuID;
                        menuItem.AaMlLanguage = pagelistValue[i];
                        menuItem.AaMlMenuItemName = pagelistValue[i + 1];
                        menuItem.AaMlMenuItemDesc = pagelistValue[i + 2];
                        menuItem.LastUser = this.LastUser; // 2013-07-17 add by Henry
                        menuLookupService.Save(menuItem);
                    }
                }
            }
            message.Text = this.CreateFullMenuForAdmin(menuManager.GetUserMenuCollectionUserRolesFromDB(new List<int>(), language, 4), new object[] { }).ToString();
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.Menu, PunchAction.Add); 

            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult EditMenu(decimal menuID, string menuName, string menuDesc, string orderNo, string language, string lastUser, string menuListStr)
        {
            Message message = new Message();
            
            byte order = Convert.ToByte(orderNo);
            message.Status = menuManager.EditMenu(menuID, menuName, menuDesc, order, language, lastUser);

            //add to MenuLookup by menuId
            if (menuListStr != "" && menuListStr != null)
            {
                string[] pagelistValue = menuListStr.Split(';');
                for (int i = 0; i < pagelistValue.Length - 1; i += 3)
                {
                    AartoMenuLookupService menuLookupService = new AartoMenuLookupService();
                    AartoMenuLookup menuItemGet = menuLookupService.GetByAaMeIdAaMlLanguage(menuID, pagelistValue[i]);
                    if (menuItemGet == null)
                    {
                        AartoMenuLookup menuItem = new AartoMenuLookup();
                        menuItem.AaMeId = menuID;
                        menuItem.AaMlLanguage = pagelistValue[i];
                        menuItem.AaMlMenuItemName = pagelistValue[i + 1];
                        menuItem.AaMlMenuItemDesc = pagelistValue[i + 2];
                        menuItem.LastUser = this.LastUser; // 2013-07-17 add by Henry
                        menuLookupService.Save(menuItem);
                    }
                    else
                    {
                        menuItemGet.AaMlMenuItemName = pagelistValue[i + 1];
                        menuItemGet.AaMlMenuItemDesc = pagelistValue[i + 2];
                        menuItemGet.LastUser = this.LastUser; // 2013-07-17 add by Henry
                        menuLookupService.Save(menuItemGet);
                    }
                }
            }
            message.Text = this.CreateFullMenuForAdmin(menuManager.GetUserMenuCollectionUserRolesFromDB(new List<int>(), language, 4), new object[] { }).ToString();
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.Menu, PunchAction.Change); 
            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult GetUserMenuEntityByMenuID(decimal menuID, string language)
        {
            Message message = new Message();
            AartoMenuLookup menuLookup =menuManager.GetMenuLookUpByMenuIDAndLanguage(menuID,language);
            if (menuLookup != null)
            {
                AartoMenu menu = menuManager.GetMenuByMenuID(menuID);                
                if (menu != null)
                {
                    message.Status = true;
                    message.Text = menuLookup.AaMlMenuItemName + ";" + menuLookup.AaMlMenuItemDesc +";"+menu.AaMeOrderNo.ToString()+";";
                }

                DataSet ds = AARTOMenulistLookupDB.GetListByAaMeID((int)menuID);
                int rowCount = ds.Tables[0].Rows.Count;
                for (int i = 0; i < rowCount; i++)
                {
                    message.Text += ds.Tables[0].Rows[i]["LSCode"].ToString() + ";";
                    message.Text += ds.Tables[0].Rows[i]["LSDescription"].ToString() + ";";
                    message.Text += ds.Tables[0].Rows[i]["AaMLMenuItemName"].ToString() + ";";
                    if (i == rowCount - 1)
                    {
                        message.Text += ds.Tables[0].Rows[i]["AaMLMenuItemDesc"].ToString();
                    }
                    else
                    {
                        message.Text += ds.Tables[0].Rows[i]["AaMLMenuItemDesc"].ToString() + ";";
                    }
                }

            }

            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult GetMenuByID(decimal menuID)
        {
            Message message = new Message();
             
            MenuUrlEntity menuUrl = manager.GetMenuPageUrlByID(menuID);
            if (menuUrl != null && !String.IsNullOrEmpty(menuUrl.MenuPageUrl))
            {
                message.Text =menuUrl.PageName +";" + menuUrl.MenuPageDesc+";"+ menuUrl.MenuPageUrl+";";

                AartoMenuService menuService = new AartoMenuService();
                AartoMenu menu = menuService.GetByAaMeId(menuID);
                DataSet ds = AARTOPagelistLookupDB.GetListByAaPageID(menu.AaPageId.Value);
                int rowCount = ds.Tables[0].Rows.Count;
                for (int i = 0; i < rowCount; i++)
                {
                    message.Text += ds.Tables[0].Rows[i]["LSDescription"].ToString() + ";";
                    message.Text += ds.Tables[0].Rows[i]["AaDefaultPageName"].ToString() + ";";
                    if (i == rowCount - 1)
                    {
                        message.Text += ds.Tables[0].Rows[i]["AaDefaultPageDesc"].ToString();
                    }
                    else
                    {
                        message.Text += ds.Tables[0].Rows[i]["AaDefaultPageDesc"].ToString() + ";";
                    }
                }
                message.Status = true;
            }
            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult DeleteMenu(decimal menuID)
        {
            UserMenuManager menuManager = new UserMenuManager();
            Message message = new Message();
            message.Status = menuManager.DeleteMenu(menuID);
            if (message.Status)
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.Menu, PunchAction.Delete); 

                string correntLanguage = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
                message.Text = this.CreateFullMenuForAdmin(menuManager.GetUserMenuCollectionUserRolesFromDB(new List<int>(), correntLanguage, 4), new object[] { }).ToString();
            }
            return Json(message);
        }

        [Authorize]
        public ActionResult MenuUrlManage()
        {
            MenuModel model = new MenuModel();
            //ViewData["language"] = Language.EnvironmentLanguage;
            ViewData["language"] = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
            manager.InitMenuUrlModel(model);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult CheckMenuByID(decimal menuID)
        {
            Message message = new Message();
             
            if (manager.CheckMenuByMenuID(menuID))
            {
                message.Status = true;
            }
            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult AssignMenuUrl(decimal menuID, int pageID)
        {
            Message message = new Message();

            message.Status = manager.AssignMenuUrl(menuID, pageID, (Session["UserLoginInfo"] as UserLoginInfo).UserName);
            if (message.Status)
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.MenuUrl, PunchAction.Add); 
            }

            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult GetPageListNotUse()
        {
            List<MenuUrlEntity> menuList = new List<MenuUrlEntity>();
             
            menuList = manager.GetPageListNotUse();

            return Json(menuList);
        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult RemoveMenuUrlByID(decimal menuID)
        {
            Message message = new Message();

            message.Status = manager.RemoveMenuUrlByMenuID(menuID, (Session["UserLoginInfo"] as UserLoginInfo).UserName);
            if (message.Status)
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.MenuUrl, PunchAction.Add);
            }

            return Json(message);
        }

        public ActionResult GetPageDetail(int pageID)
        {
            Message message = new Message();
            AartoPageList page = manager.GetPageDetailByPageID(pageID);
            message.Status = page == null;
            message.Text += page.AaDefaultPageName+";";
            message.Text += page.AaDefaultPageDesc + ";";
            message.Text += page.AaPageUrl + ";";
            DataSet ds = AARTOPagelistLookupDB.GetListByAaPageID(pageID);
            int rowCount = ds.Tables[0].Rows.Count;
            for (int i = 0; i < rowCount; i++)
            {
                message.Text += ds.Tables[0].Rows[i]["LSDescription"].ToString() + ";";
                message.Text += ds.Tables[0].Rows[i]["AaDefaultPageName"].ToString() + ";";
                if (i == rowCount - 1)
                {
                    message.Text += ds.Tables[0].Rows[i]["AaDefaultPageDesc"].ToString();
                }
                else
                {
                    message.Text += ds.Tables[0].Rows[i]["AaDefaultPageDesc"].ToString() + ";";
                }
            }
            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult GetLanguageCount() {
            Message message = new Message();
            List<LanguageSelector> languageSelectorList = LanguageSelectorManager.GetAllLanSelector().OrderByDescending(ls => ls.LsCode).OrderByDescending(ls => ls.LsIsDefault).ToList();
            for (int i = 0; i < languageSelectorList.Count;i++ )
            {
                message.Text += languageSelectorList[i].LsDescription + ";";
                if (i == languageSelectorList.Count - 1)
                {
                    message.Text += languageSelectorList[i].LsCode;
                }
                else
                {
                    message.Text += languageSelectorList[i].LsCode + ";";
                }
            }
            return Json(message);
        }

    }


}
