﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.Admin;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource.Admin;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.Admin
{
    public partial class AdminController : Controller
    {
        public ActionResult OfficerGroup(int page = 0, string ofGrDescr = "")
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            return View(InitModel(page, ofGrDescr));
        }

        public JsonResult DeleteGroup(int ofGrIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            if (OfficerGroupManager.DeleteOfficerGroup(ofGrIntNo))
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.TrafficOfficerGroupsMaintenance, PunchAction.Delete);  

                return Json(new { result = 0 });
            }
            return Json(new { result = -1 });
        }

        public JsonResult SaveGroup(OfficerGroupModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
            if (Save(model))
            {
                return Json(new { result = 0 });
            }
            string errorMsg = model.OfGrIntNo == 0 ? OfficerGroupRes.AddGroupError : OfficerGroupRes.UpdateGroupError;
            return Json(new { result = -1, errorMsg = errorMsg });
        }

        public JsonResult GetGroup(int ofGrIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            OfficerGroup officerGroup = OfficerGroupManager.GetOfficerGroupByOfGrIntNo(ofGrIntNo);

            return Json(new
            {
                result = 0,
                group = new
                {
                    OfGrIntNo = officerGroup.OfGrIntNo,
                    OfGrDescr = officerGroup.OfGrDescr,
                    OfGrCode = officerGroup.OfGrCode,
                }
            });
        }

        private bool Save(OfficerGroupModel model)
        {
            bool isSuccess = false;
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            OfficerGroup officer = new OfficerGroup
            {
                OfGrIntNo = model.OfGrIntNo,
                OfGrDescr = model.OfGrDescr,
                OfGrCode = model.OfGrCode,
                LastUser = userInfo.UserName
            };

            if (model.OfGrIntNo > 0)
            {
                 isSuccess=OfficerGroupManager.UpdateOfficerGroup(officer);
                 if (isSuccess)
                 {
                     //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                     SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                     punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.TrafficOfficerGroupsMaintenance, PunchAction.Change); 
                 }
                 return isSuccess;
            }
            else
            {
                isSuccess = OfficerGroupManager.AddOfficerGroup(officer);
                if (isSuccess)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.TrafficOfficerGroupsMaintenance, PunchAction.Add);
                }
                return isSuccess;
            }
        }

        private OfficerGroupModel InitModel(int pageIndex, string ofGrDescr)
        {
            OfficerGroupModel model = new OfficerGroupModel();
            model.PageSize = Config.PageSize;
            model.PageIndex = pageIndex;
            int totalCount = 0;

            ofGrDescr = ofGrDescr.Trim();
            model.OfficerGroups = OfficerGroupManager.GetOfficerGroupsPage(ofGrDescr, pageIndex, model.PageSize, out totalCount);

            if (model.OfficerGroups.Count <= 0)
            {
                model.ErrorMsg = OfficerGroupRes.SearchNotFound;
            }
            model.OfGrDescrSearch = ofGrDescr;
            model.TotalCount = totalCount;
            return model;
        }
    }
}
