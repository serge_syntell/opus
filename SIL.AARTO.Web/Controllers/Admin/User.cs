using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using SIL.AARTO.BLL.Admin;
using SIL.AARTO.BLL.Admin.Model;
using System.Collections;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Helpers;
using System.Configuration;
using SIL.AARTO.Web.Helpers.Paginator;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.BLL.Account;

namespace SIL.AARTO.Web.Controllers.Admin
{
    public partial class AdminController : Controller
    {
        //
        // GET: /User/
        UserManager userManager = new UserManager();
        public ActionResult UserManage()
        {
            UserModel model = new UserModel();
            int pageIndex = 0;
            if (!String.IsNullOrEmpty(QueryString.GetString("Page")))
            {
                pageIndex = Convert.ToInt32(QueryString.GetString("Page"));
            }
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            ViewData["pageSize"] = pageSize;
            InitUserModel(model, pageIndex, pageSize);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserManage(UserModel model, FormCollection form)
        {
            model.HasErrors = false;
            int pageIndex = 0;
            if (!String.IsNullOrEmpty(QueryString.GetString("Page")))
            {
                pageIndex = Convert.ToInt32(QueryString.GetString("Page"));
            }
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            ViewData["pageSize"] = pageSize;
            if (!String.IsNullOrEmpty(form["btnSearch"]))
            {
                InitUserModel(model, pageIndex, pageSize);
                return View(model);
            }
            if (!String.IsNullOrEmpty(form["btnDeleteUser"]))
            {
                if (!String.IsNullOrEmpty(model.UserIntNo))
                {
                    userManager.DeleteUser(Convert.ToInt32(model.UserIntNo), (Session["UserLoginInfo"] as UserLoginInfo).UserName);
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.UserMaintenance, PunchAction.Delete);  

                }
            }
            if (!model.IsValid)
            {
                ModelState.AddModelError(this, model.GetRuleViolations());
                InitUserModel(model, pageIndex, pageSize);
                model.HasErrors = true;
                return View(model);
            }
            if (!String.IsNullOrEmpty(form["btnSaveUser"]))
            {
                UpdateModel<UserModel>(model);
                bool isNew;
                if (String.IsNullOrEmpty(model.UserIntNo))
                    isNew = true;
                else isNew = false;

                userManager.SaveUser(model, isNew, (Session["UserLoginInfo"] as UserLoginInfo).UserName);
                if (isNew)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.UserMaintenance, PunchAction.Add);
                }
                else
                {
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.UserMaintenance, PunchAction.Change);
                }
                
            }

            InitUserModel(model, pageIndex, pageSize);

            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetUserDetail(int userID)
        {
            Message message = new Message();
            SIL.AARTO.DAL.Entities.User user = userManager.GetUserByUserID(userID);

            if (user != null)
            {
                string trafficOffer = string.Empty;
                if (user.UserDefaultAutIntNo != null)
                {
                    TList<UserOfficer> listUserOfficer = userManager.GetUserOfficerByUserIntNo(userID);
                    if (listUserOfficer.Count > 0)
                    {
                        trafficOffer = listUserOfficer[0].UotoIntNo.ToString();
                    }
                }

                message.Text = (user.UserDefaultAutIntNo == null ? "" : user.UserDefaultAutIntNo.Value.ToString()) + ";" + user.UserSname + ";" +
                    user.UserInit + ";" + user.UserFname + ";" + user.UserLoginName + ";" +
                    user.UserEmail + ";" + user.UserAccessLevel.ToString() + ";" + trafficOffer + ";" + user.DefaultLanguage; 

                message.Status = true;
            }
            return Json(message);

        }

        public string GenerateTempPassword()
        {
            var rnd = new Random(DateTime.Now.Second);
            var output = new char[] {
                (char)(rnd.Next(65,90)),    // Caps Letter
                (char)(rnd.Next(97,122)),   // Lower Case Letter
                (char)(rnd.Next(97,122)),   // Lower Case Letter
                (char)(rnd.Next(48,57)),    // Number
                (char)(rnd.Next(65,90)),    // Caps Letter
                (char)(rnd.Next(65,90)),    // Caps Letter
            };


            return String.Concat(output);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ResetPassword(int userId)
        {
            Message message = new Message();
            try
            {
                SIL.AARTO.DAL.Entities.User user = userManager.GetUserByUserID(userId);

                if (user != null)
                {
                    if ( String.IsNullOrEmpty(user.UserEmail))
                    {
                        var tempPassword = GenerateTempPassword();
                        user.UserPassword = Encrypt.HashPassword(tempPassword);
                        //Jerry 2012-06-12 add
                        int expiryDays = new SIL.AARTO.DAL.Services.SysParamService().GetBySpColumnName(SIL.AARTO.DAL.Entities.SysParamList.UserPasswordExpiryDays.ToString()).SpIntegerValue;
                        user.UserPasswordReset = true;
                        user.UserPasswordGraceLoginCount = 1;
                        user.UserPasswordExpiryDate = DateTime.Now.AddDays(expiryDays);
                        // 2013-07-17 add LastUser by Henry
                        user.LastUser = (Session["UserLoginInfo"] as UserLoginInfo).UserName;
                        user = userManager.SaveUser(user);

                        message.Text = String.Format("User password has been changed to '{0}', Password must be changed on next login.", tempPassword);
                        message.Status = true;
                    
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.UserMaintenance, PunchAction.Change);

                        UserLoginManager.CreateAccountActivity(user.UserIntNo, BLL.Enum.UserAccountActivity.PasswordReset,null);
                    }
                    else
                    {
                        ResetPasswordLink.CreateAndSendResetLink(
                            userId, 
                            user.UserEmail,
                            string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"))
                            );

                        message.Text = "A reset password link has been sent to the user.";
                        message.Status = true;
                    }
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }

            return Json(message);
        }

        void InitUserModel(UserModel model, int pageIndex, int pageSize)
        {

            int totalCount = 0;
            model.Users = userManager.GetAllUser(model.SearchKey, pageIndex, pageSize, out totalCount);
            model.TotalCount = totalCount;

            model.AuthorityList = new SelectList(userManager.GetAllAuthority() as IEnumerable,
                "AutIntNo",
                "AutDesc",
                model.UserDefaultAuthIntNo);

            model.TrafficOfficerList = new SelectList(userManager.GetAllTrafficOffericer() as IEnumerable,
                "ToIntNo",
                "ToDesc",
                model.TrafficOfficerIntNo);

            model.LanguageSlectorList = new SelectList(SIL.AARTO.BLL.CameraManagement.LanguageSelectorManager.GetAllLanSelector() as IEnumerable,
                "LSCode", 
                "LSDescription",
                model.LanguageSlectorListIntNo );
        }

        //public ActionResult BindDropDownList() {
        //    TList<LanguageSelector> lsList = SIL.AARTO.BLL.CameraManagement.LanguageSelectorManager.GetAllLanSelector();
        //    List<SelectListItem> languagSelectors = new List<SelectListItem>();

        //    foreach (LanguageSelector selector in lsList) {
        //        languagSelectors.Add(new SelectListItem
        //        {
        //             Text =selector.LsCode,
        //             Value = selector.LsDescription
        //        });
        //    }
        //    ViewData["languagSelectors"] = new SelectList(languagSelectors, "Value", "Text", "Please select a default language");

        //    //ViewData["languagSelectors"] = new SelectList(lsList, "lsCode", "lsDescription");
        //    return View();
        //}



    }
}
