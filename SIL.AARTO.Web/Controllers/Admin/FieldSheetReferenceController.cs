﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.Admin;
using SIL.AARTO.Web.ViewModels;
using SIL.AARTO.Web.ViewModels.Admin;

using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.Admin
{
    public partial class AdminController : Controller
    {
        //
        // GET: /FieldSheetReference/

        public ActionResult FieldSheetReference(string SearchFilmNo)
        {
            FieldSheetReferenceModel model = new FieldSheetReferenceModel();

            if (!string.IsNullOrWhiteSpace(SearchFilmNo))
            {
                Film film = new FilmService().GetByFilmNo(SearchFilmNo);
                if (film != null)
                {
                    if (film.FilmType == "M" || film.FilmType == "H")
                    {
                        model.ErrorMsg = FieldSheetReference_cshtml.ErrorMsg3;
                    }
                    else
                    {
                        model.IsHaveDate = true;
                        model.FilmIntNo = film.FilmIntNo.ToString();
                        model.FilmNo = film.FilmNo.Trim();
                        model.OffenceDate = film.FirstOffenceDate.HasValue ? film.FirstOffenceDate.Value.ToString("yyyy-MM-dd HH:mm") : "";
                        model.ReferenceNumber = film.FilmFieldSheetReference;

                        Frame frame = new FrameService().GetByFilmIntNo(film.FilmIntNo).Where(m => m.RegNo != "0000000000").FirstOrDefault();
                        if (frame != null)
                        {
                            Location location = new LocationService().GetByLocIntNo(frame.LocIntNo);
                            model.LocationCode = location.LocCode;
                            model.LocationDescription = location.LocDescr;

                            TrafficOfficer officer = new TrafficOfficerService().GetByToIntNo(frame.ToIntNo);
                            model.OfficerNumber = officer.ToNo;
                            model.OfficerFullName = officer.TosName + " " + officer.ToInit;
                        }
                    }
                }
                else
                {
                    model.ErrorMsg = FieldSheetReference_cshtml.ErrorMsg1;
                }
            }
            return View(model);
        }

        public JsonResult UpdateFieldSheetReferenceNumber(int FilmIntNo, string FieldSheetReferenceNumber)
        {
            Message message = new Message();
            message.Status = true;

            try
            {
                Film film = new FilmService().GetByFilmIntNo(FilmIntNo);

                if (film != null)
                {
                    film.FilmFieldSheetReference = FieldSheetReferenceNumber.Trim();
                    new FilmService().Update(film);
                    message.Text = FieldSheetReference_cshtml.ErrorMsg2;
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.FieldSheetReference, PunchAction.Change); 
                }
            }
            catch (Exception ex)
            {
                message.Status = false;
                message.Text = ex.ToString();
            }

            return Json(message);
        }
    }
}
