using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using SIL.AARTO.BLL.Admin.Model;
using System.Collections;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.BLL.Admin.Model;
using SIL.AARTO.DAL.Entities;
using System.Data;
using SIL.AARTO.BLL.CameraManagement;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.Admin
{
    public partial class AdminController : Controller
    {
        //
        // GET: /UserRole/
        [Authorize]
        public ActionResult UserRoleManage()
        {
            UserRoleModel model = new UserRoleModel();
            model.UserIntNo = String.IsNullOrEmpty(Request.QueryString["UserIntNo"]) ? 0 : Convert.ToInt32(Request.QueryString["UserIntNo"]);

            InitUserRoleModel(model);

            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult UserRoleManage(UserRoleModel model)
        {
            model.UserIntNo = String.IsNullOrEmpty(Request.QueryString["UserIntNo"]) ? 0 : Convert.ToInt32(Request.QueryString["UserIntNo"]);

            InitUserRoleModel(model);

            List<int> userRoles = new List<int>();
            foreach (UserRoleEntity userRole in userRoleManager.GetUserRoleEntityByUserIntNo(model.UserIntNo))
            {
                userRoles.Add(userRole.AaUserRoleID);
            }
            string correntLanguage = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
            ViewData["MenuCollection"] = new UserMenuManager().GetUserMenuCollectionUserRolesFromDB(userRoles, correntLanguage, 4);

            return View(model);
        }
        
        [Authorize]
        public ActionResult RoleManage()
        {
            RoleModel model = new RoleModel();
            InitRoleModel(model);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post),Authorize]
        public ActionResult RoleManage(RoleModel model)
        {
            UpdateModel<RoleModel>(model);
            AartoUserRole userRole = null;
            // 2013-07-17 add lastUser by Henry
            string lastUser = (Session["UserLoginInfo"] as UserLoginInfo).UserName;
            if (!String.IsNullOrEmpty(model.RoleID))
            {
                userRole = userRoleManager.GetUserRoleByRoleID(Convert.ToInt32(model.RoleID));
                if (userRole != null)
                {
                    userRole.AaUserRoleName = model.RoleName;
                    userRole.AaUserRoleDescription = model.RoleDescription;
                    userRole.LastUser = lastUser;
                    userRoleManager.SaveUserRole(userRole);
                }
            }
            else
            {
                userRole = new AartoUserRole();
                userRole.AaUserRoleId = userRoleManager.GetMaxRoleID() + 1;
                userRole.AaUserRoleName = model.RoleName;
                userRole.AaUserRoleDescription = model.RoleDescription;
                userRole.LastUser = lastUser;
                userRoleManager.SaveUserRole(userRole);
            }
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.RoleMaintenance, PunchAction.Change);  

            InitRoleModel(model);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult DeleteRoleByID(int roleID)
        {
            Message message = new Message();
            //delete UserRolelookup data
            bool result =AARTOUserRoleLookupDB.DeleteByAaUserRoleLid(roleID);
            AartoUserRole role = userRoleManager.GetUserRoleByRoleID(roleID);
            if (role != null)
            {
                role.MarkToDelete();
                // 2013-07-17 add LastUser by Henry
                role.LastUser = (Session["UserLoginInfo"] as UserLoginInfo).UserName;
                message.Status = userRoleManager.SaveUserRole(role)!=null;
            }
            if (result)
            {
              
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.RoleMaintenance, PunchAction.Delete); 
            }
            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult GetUserRoleByID(int roleID)
        {
            Message message = new Message();
            AartoUserRole role = userRoleManager.GetUserRoleByRoleID(roleID);
            if (role != null)
            {
                message.Status = true;
                message.Text = role.AaUserRoleName + ";" + role.AaUserRoleDescription + ";";
            }

            DataSet ds = AARTOUserRoleLookupDB.GetListByAaUserRoleId(roleID);
            int rowCount = ds.Tables[0].Rows.Count;
            for (int i = 0; i < rowCount; i++)
            {
                message.Text += ds.Tables[0].Rows[i]["LSCode"].ToString() + ";";
                message.Text += ds.Tables[0].Rows[i]["LSDescription"].ToString() + ";";
                if (i == rowCount - 1)
                {
                    message.Text += ds.Tables[0].Rows[i]["AaUserRoleDescription"].ToString();
                }
                else
                {
                    message.Text += ds.Tables[0].Rows[i]["AaUserRoleDescription"].ToString() + ";";
                }
            }

            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Post),Authorize]
        public ActionResult AddRoleToUser(int userIntNo, string roles)
        {
            List<int> userRoles = new List<int>();
            Message message = new Message();
            string lastUser = (Session["UserLoginInfo"] as UserLoginInfo).UserName; // 2013-08-23 modify by Henry
            foreach (string role in roles.Split(';'))
            {
                if (!String.IsNullOrEmpty(role))
                {
                    userRoles.Add(Convert.ToInt32(role));
                }
            }
            message.Status = userRoleManager.AddRolesForUser(userIntNo, userRoles, lastUser) != null;
            if (message.Status)
            {
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.UserRoleMaintenance, PunchAction.Add);  

            }
            return Json(message);
        }


        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult RemoveRoleFormUser(int userIntNo, string roles)
        {
            List<int> userRoles = new List<int>();
            Message message = new Message();
            foreach (string role in roles.Split(';'))
            {
                if (!String.IsNullOrEmpty(role))
                {
                    userRoles.Add(Convert.ToInt32(role));
                }
            }
            message.Status = userRoleManager.RemoveRolesFromUser(userIntNo, userRoles,  (Session["UserLoginInfo"] as UserLoginInfo).UserName) != null;
            if (message.Status)
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.UserRoleMaintenance, PunchAction.Delete);  
            }
            return Json(message);
        }

        void InitRoleModel(RoleModel model)
        {
            model.UserRoleList = userRoleManager.GetAllUserRole();
        }

        void InitUserRoleModel(UserRoleModel model)
        {
            SIL.AARTO.DAL.Entities.User user = userManager.GetUserByUserID(model.UserIntNo);
            if (user != null)
            {
                model.UserName = user.UserLoginName;
            }

            model.UserRoleList = new SelectList(userRoleManager.GetUserRoleEntityByUserIntNo(model.UserIntNo) as IEnumerable,
                "AaUserRoleID",
                "AaUserRoleName",
                null);

            model.NotUseRoleList = new SelectList(userRoleManager.GetUserRoleEntityNotUse(model.UserIntNo) as IEnumerable,
               "AaUserRoleID",
               "AaUserRoleName",
               null);
        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult EditeUserRole(int userRoleID,  string userRoleListStr)
        {
            Message message = new Message();

            if (userRoleListStr != "" && userRoleListStr != null)
            {
                string[] listValue = userRoleListStr.Split(';');
                for (int i = 0; i < listValue.Length - 1; i += 2)
                {                   
                    AartoUserRoleLookupService userRoleLookupService = new AartoUserRoleLookupService();
                    AartoUserRoleLookup userRoleLookup = userRoleLookupService.GetByAaUserRoleIdLsCode(userRoleID, listValue[i]);
                    if (userRoleLookup == null)
                    {
                        AartoUserRoleLookup userRoleLookupTemp = new AartoUserRoleLookup();
                        userRoleLookupTemp.AaUserRoleId = userRoleID;
                        userRoleLookupTemp.LsCode = listValue[i];
                        userRoleLookupTemp.AaUserRoleDescription = listValue[i + 1];
                        userRoleLookupTemp.LastUser = this.LastUser; // 2013-07-18 add by Henry
                        userRoleLookupService.Save(userRoleLookupTemp);
                    }
                    else
                    {
                        userRoleLookup.AaUserRoleDescription = listValue[i + 1];
                        userRoleLookup.LastUser = this.LastUser; // 2013-07-18 add by Henry
                        userRoleLookupService.Save(userRoleLookup);
                    }
                }
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.RoleMaintenance, PunchAction.Change); 
            }
            //message.Status = userRoleManager.RemoveRolesFromUser(userIntNo, userRoles) != null;

            return Json(message);
        }
        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult GetLanguageCountUserRole()
        {
            Message message = new Message();
            TList<LanguageSelector> languageSelectorList = LanguageSelectorManager.GetAllLanSelector();
            for (int i = 0; i < languageSelectorList.Count; i++)
            {
                message.Text += languageSelectorList[i].LsDescription + ";";
                if (i == languageSelectorList.Count - 1)
                {
                    message.Text += languageSelectorList[i].LsCode;
                }
                else
                {
                    message.Text += languageSelectorList[i].LsCode + ";";
                }
            }
            return Json(message);
        }

    }
}
