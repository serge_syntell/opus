﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.BLL.Admin.Model;
using System.Collections;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.Helpers;

namespace SIL.AARTO.Web.Controllers.Admin
{
    public partial class AdminController : Controller
    {
        CourtManager crtMana = new CourtManager();

        [HttpPost]
        public ActionResult GetCrtByAutIntNo(int autId)
        {
            List<Court> crts = null;
            crts = crtMana.GetCourtsByAutIntNoForDropDown(autId);
            List<SelectListItem> Items = new List<SelectListItem>();
            string descr = string.Empty;
            foreach (Court crt in crts)
            {
                if (!String.IsNullOrEmpty(crt.CrtNo))
                {
                    descr = crt.CrtName + " (" + crt.CrtNo + ")";
                }
                else
                {
                    descr = crt.CrtName;
                }
                Items.Add(new SelectListItem { Value = crt.CrtIntNo.ToString(), Text = descr });
            }
            return Json(Items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCourts(int autIntNo)
        {
            List<Court> crts = null;
            crts = crtMana.GetCourtsByAutIntNo(autIntNo);
            List<SelectListItem> Items = new List<SelectListItem>();
            string descr = string.Empty;
            foreach (Court crt in crts)
            {
                if (!String.IsNullOrEmpty(crt.CrtNo))
                {
                    descr = crt.CrtName + " (" + crt.CrtNo + ")";
                }
                else
                {
                    descr = crt.CrtName;
                }
                Items.Add(new SelectListItem { Value = crt.CrtIntNo.ToString(), Text = descr });
            }
            return Json(Items);
        }

    }
}
