using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using SIL.AARTO.BLL.Admin;
using SIL.AARTO.BLL.Admin.Model;
using System.Collections;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.Admin
{
    [AARTOErrorLog, LanguageFilter]
    public partial class AdminController : Controller
    {
        //
        // GET: /ActionRole/
        ActionRoleManager actionRoleManager = new ActionRoleManager();
        UserRoleManager userRoleManager = new UserRoleManager();
        public ActionResult ActionRoleManage()
        {
            UserLoginInfo userLoginInfo = Session["UserLoginInfo"] as UserLoginInfo;
            if (userLoginInfo == null)
                return Redirect("/Account/Login");
            ActionRoleModel model = new ActionRoleModel();
            InitActionRoleModel(model,userLoginInfo.UserRoles);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ActionRoleManage(ActionRoleModel model)
        {
            return View(model);
        }

        private void InitActionRoleModel(ActionRoleModel model,List<int> roles)
        {
             
            model.MenuList = new SelectList(actionRoleManager.GetAllActionMenu() as IEnumerable ,
                "ActionID",
                "MenuName",
                null);

            model.RoleList = new SelectList(userRoleManager.GetAllUserRoleEntity() as IEnumerable,
                "AaUserRoleId",
                "AaUserRoleName",
                null);

            model.ActionRoleList = new SelectList(userRoleManager.GetUserRoleEntityByUserIntNo(model.UserIntNo) as IEnumerable,
                "AaUserRoleId",
                "AaUserRoleName",
                null);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetActionRolesByActID(int actID)
        {
            Message message = new Message();
            List<UserRoleEntity> list= actionRoleManager.GetRolesByActID(actID);
            int count =0;
            foreach (UserRoleEntity userRole in list)
            {
                count++;
                if (count == list.Count)
                {
                    message.Text += userRole.AaUserRoleID + ":" + userRole.AaUserRoleName;
                }
                else
                {
                    message.Text += userRole.AaUserRoleID + ":" + userRole.AaUserRoleName + ";";
                }
            }
            message.Status = true;
            return Json(message);
        }

        public ActionResult GetActionRolesNotUseByActID(int actID)
        {
            Message message = new Message();
            List<UserRoleEntity> list = new List<UserRoleEntity>();
            list =actionRoleManager.GetActionRolesNotUseByActID(actID);
            int count =0 ;
            foreach (UserRoleEntity userRole in list)
            {
                count++;
                if (count == list.Count)
                {
                    message.Text += userRole.AaUserRoleID + ":" + userRole.AaUserRoleName;
                }
                else
                {
                    message.Text += userRole.AaUserRoleID + ":" + userRole.AaUserRoleName + ";";
                }
            }

            message.Status = true;

            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddRoleToAction(int actID, string userRoles)
        {
            string lastUser = (Session["UserLoginInfo"] as UserLoginInfo).UserName; // 2013-07-16 add lastUser by Henry
            int autIntNo = 0; //2013-12-02 Heidi added
            if (Session["autIntNo"] != null)
            {
                autIntNo = Convert.ToInt32(Session["autIntNo"]);
            }
            Message message = new Message();
            List<int> userRoleList = new List<int>();
            if (string.IsNullOrEmpty(userRoles))
            {//20120118 updated by Nancy(add checking whether userRoles is null, if true return false directly)
                message.Status = false;
            }
            else
            {
                foreach (string role in userRoles.Split(';'))
                {
                    if (!String.IsNullOrEmpty(role))
                    {
                        userRoleList.Add(Convert.ToInt32(role));
                    }
                }
                message.Status = actionRoleManager.AddRolesToAction(actID, userRoleList, lastUser) != null;
                if (message.Status)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, lastUser, PunchStatisticsTranTypeList.ActionRole, PunchAction.Add);  
                }
            }
            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReomveRoleFromAction(int actID, string userRoles)
        {
            string lastUser = (Session["UserLoginInfo"] as UserLoginInfo).UserName;
            int autIntNo = 0; //2013-12-02 Heidi added
            if (Session["autIntNo"] != null)
            {
                autIntNo = Convert.ToInt32(Session["autIntNo"]);
            }
            Message message = new Message();
            List<int> userRoleList = new List<int>();
            if (string.IsNullOrEmpty(userRoles))
            {//20120118 updated by Nancy(add checking whether userRoles is null, if true return false directly)
                message.Status = false;
            }
            else
            {
                foreach (string role in userRoles.Split(';'))
                {
                    if (!String.IsNullOrEmpty(role))
                    {
                        userRoleList.Add(Convert.ToInt32(role));
                    }
                }   
                // 2013-07-16 add lastUser by Henry
                message.Status = actionRoleManager.RemoveRolesFromAction(actID, userRoleList, lastUser) != null;
                if (message.Status)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, lastUser, PunchStatisticsTranTypeList.ActionRole, PunchAction.Delete);  

                }
            }
            return Json(message);
        }
        
    }
}
