﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.Admin;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource.Admin;
using SIL.AARTO.BLL.Utility;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.Admin
{
    public partial class AdminController : Controller
    {
        public ActionResult TrafficOfficer(int mtrIntNo = 0, int page = 0, string toIdNumber = "", string surname = "")
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            return View(InitModel(mtrIntNo, page, toIdNumber, surname));
        }

        public JsonResult ArchiveOfficer(int toIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            if (TrafficOfficerManager.ArchiveTrafficOfficer(toIntNo, (Session["UserLoginInfo"] as UserLoginInfo).UserName))
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.TrafficOfficers, PunchAction.Change);  

                return Json(new { result = 0 });
            }
            return Json(new { result = -1 });
        }

        public JsonResult ActiveOfficer(int toIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            if (TrafficOfficerManager.ActiveTrafficOfficer(toIntNo, (Session["UserLoginInfo"] as UserLoginInfo).UserName))
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.TrafficOfficers, PunchAction.Change);  
                return Json(new { result = 0 });
            }
            return Json(new { result = -1 });
        }

        public JsonResult SaveOfficer(TrafficOfficerModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
            if (Save(model))
            {
                return Json(new { result = 0 });
            }
            string errorMsg = model.ToIntNo == 0 ? TrafficOfficerRes.lblError_Text4 : TrafficOfficerRes.lblError_Text7;
            return Json(new { result = -1, errorMsg = errorMsg });
        }

        public JsonResult GetOfficer(int toIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            TrafficOfficer officer = TrafficOfficerManager.GetTrafficOfficerByToIntNo(toIntNo);

            return Json(new
            {
                result = 0,
                officer = new
                {
                    ToIntNo = officer.ToIntNo,
                    MtrIntNo = officer.MtrIntNo,
                    TOSName = officer.TosName,
                    TOInit = officer.ToInit,
                    ToIDNumber = officer.ToIdNumber,
                    TONo = officer.ToNo,
                    //TOGroup = officer.ToGroup,
                    OfGrIntNo = officer.OfGrIntNo,
                    Senior = officer.Senior == "Y" ? true : false,
                    TOInfrastructureNumber = officer.ToInfrastructureNumber
                }
            });
        }


        // jake 2013-05-26 modified
        private bool Save(TrafficOfficerModel model)
        {
            bool isSuccess = false;
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];

            // Jake 2013-09-16 changed the current code to handle insert new traffic officer here
            TrafficOfficer officer = null;
            if (model.ToIntNo > 0)
            {
                officer = TrafficOfficerManager.GetTrafficOfficerByToIntNo(model.ToIntNo);
            }
            else
            {
                officer = new TrafficOfficer();
            }

            officer.ToIntNo = model.ToIntNo;
            officer.MtrIntNo = model.MtrIntNo;
            officer.TosName = model.TOSName;
            officer.ToInit = model.TOInit ?? string.Empty;
            officer.ToIdNumber = model.ToIDNumber;
            officer.ToNo = model.TONo ?? string.Empty;
            //officer.ToGroup = model.TOGroup ?? string.Empty;
            officer.OfGrIntNo = model.OfGrIntNo;
            officer.Senior = model.Senior == true ? "Y" : "N";
            officer.ToInfrastructureNumber = model.TOInfrastructureNumber ?? string.Empty;
            officer.LastUser = userInfo.UserName;
            
            //TrafficOfficer officer = new TrafficOfficer
            //{
            //    ToIntNo = model.ToIntNo,
            //    MtrIntNo = model.MtrIntNo,
            //    TosName = model.TOSName,
            //    ToInit = model.TOInit ?? string.Empty,
            //    ToIdNumber = model.ToIDNumber,
            //    ToNo = model.TONo ?? string.Empty,
            //    //ToGroup = model.TOGroup ?? string.Empty,
            //    OfGrIntNo = model.OfGrIntNo,
            //    Senior = model.Senior == true ? "Y" : "N",
            //    ToInfrastructureNumber = model.TOInfrastructureNumber ?? string.Empty,
            //    LastUser = userInfo.UserName
            //};
            try
            {
                if (model.ToIntNo > 0)
                {
                    isSuccess=TrafficOfficerManager.UpdateTrafficOfficer(officer);
                    if (isSuccess)
                    {
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.TrafficOfficers, PunchAction.Change); 
                    }
                    return isSuccess;
                }
                else
                {
                    isSuccess=TrafficOfficerManager.AddTrafficOfficer(officer);
                    if (isSuccess)
                    {
                        
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.TrafficOfficers, PunchAction.Add); 
                    }
                    return isSuccess;
                }
            }
            catch (SqlException ex)
            {
                return false;
            }
        }

        private TrafficOfficerModel InitModel(int mtrIntNo, int pageIndex, string toIdNumber, string surname)
        {
            TrafficOfficerModel model = new TrafficOfficerModel();
            model.Metros = new SelectList(MetroManager.GetMetroListItem(), "MtrIntNo", "MtrName");
            model.Metro = mtrIntNo == 0 ? int.Parse(model.Metros.First().Value) : mtrIntNo;
            model.OfficerGroups = new SelectList(OfficerGroupManager.GetOfficerGroupListItem(), "OfGrIntNo", "OfGrDescr");
            model.PageSize = Config.PageSize;
            model.PageIndex = pageIndex;
            int totalCount = 0;

            toIdNumber = toIdNumber.Trim();
            surname = surname.Trim();
            model.TrafficOfficers = new List<OfficerEntity>();

            List<TrafficOfficer> officers = TrafficOfficerManager.GetTrafficOfficersPage(model.Metro, toIdNumber, surname, pageIndex, model.PageSize, out totalCount);
            Dictionary<int, OfficerGroup> officerGroups = OfficerGroupManager.GetAllOfficerGroupKeyValues();
            foreach (var officer in officers)
            {
                model.TrafficOfficers.Add(new OfficerEntity
                {
                    ToIntNo = officer.ToIntNo,
                    Senior = officer.Senior,
                    TOIdNumber = officer.ToIdNumber,
                    TOInit = officer.ToInit,
                    TONo = officer.ToNo,
                    TOGroup = officerGroups[officer.OfGrIntNo].OfGrCode,
                    TOSName = officer.TosName,
                    IsActive = officer.IsActive

                });
            }

            if (model.TrafficOfficers.Count <= 0)
            {
                model.ErrorMsg = TrafficOfficerRes.lblError_Text1;
            }
            model.ToIdNumberSearch = toIdNumber;
            model.SurnameSearch = surname;
            model.TotalCount = totalCount;
            return model;
        }
    }
}
