﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.Admin;
using SIL.AARTO.Web.ViewModels;
using SIL.AARTO.Web.ViewModels.Admin;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.Admin
{
    public partial class AdminController : Controller
    {
        CooCompanyInformationService infoService = new CooCompanyInformationService();
        CooCompanyProxyService proxyService = new CooCompanyProxyService();
        string pwdPattern = @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,50}$";
        string emailPattern = @"^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$";

        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
       
        public ActionResult ChangeOfOwnerAutomationSetup(ChangeOfOwnerAutomationModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            model.CompanyInfoList = GetCompanylist(model);
            model.CompanyProxyList = GetProxyList(model);
            return View(model);
        }

        private List<COOCompanyInformationEntity> GetCompanylist(ChangeOfOwnerAutomationModel model)
        {
            model.PageSize = Config.PageSize;
            CooCompanyInformationQuery query = new CooCompanyInformationQuery();
            if (!string.IsNullOrWhiteSpace(model.SearchStr))
            {
                query.AppendContains("or", CooCompanyInformationColumn.CciCompanyName, model.SearchStr.Trim());
                query.AppendContains("or", CooCompanyInformationColumn.CciCompanyCode, model.SearchStr.Trim());
            }
            int totalcount = 0;
            TList<CooCompanyInformation> list = infoService.Find(query as IFilterParameterCollection, "CCIIntNo", model.Page * model.PageSize, model.PageSize, out totalcount);
            model.TotalCount = totalcount;

            List<COOCompanyInformationEntity> companyList = new List<COOCompanyInformationEntity>();
            for (int i = 0; i < list.Count; i++)
            {
                COOCompanyInformationEntity company = new COOCompanyInformationEntity();
                company.OrderNo = model.Page * model.PageSize + i + 1;
                company.CCIIntNo = list[i].CciIntNo;
                company.CCICompanyName = list[i].CciCompanyName.Trim();
                company.CCICompanyCode = list[i].CciCompanyCode.Trim();
                company.CCIRegistrationNumber = list[i].CciRegistrationNumber.Trim();
                company.CCITelephone = list[i].CciTelephone.Trim();
                company.CCIEmail = list[i].CciEmail.Trim();
                company.LastUser = list[i].LastUser.Trim();
                companyList.Add(company);
            }

            CooCompanyInformation com = infoService.GetByCciIntNo(model.SelectCCIIntNo);
            if (com != null)
            {
                model.SelectCompanyName = com.CciCompanyName.Trim();
            }

            return companyList;
        }

        private List<COOCompanyProxyEntity> GetProxyList(ChangeOfOwnerAutomationModel model)
        {
            List<COOCompanyProxyEntity> proxyList = new List<COOCompanyProxyEntity>();
            TList<CooCompanyProxy> list = proxyService.GetByCciIntNo(model.SelectCCIIntNo);

            for (int i = 0; i < list.Count; i++)
            {
                COOCompanyProxyEntity proxy = new COOCompanyProxyEntity();
                proxy.OrderNo = i + 1;
                proxy.CCIIntNo = list[i].CciIntNo;
                proxy.CCPIntNo = list[i].CcpIntNo;
                proxy.CCPNumber = list[i].CcpNumber.Trim();
                proxy.CCPEmailAddress = list[i].CcpEmailAddress.Trim();
                proxy.CCPAuthorisedDate = list[i].CcpAuthorisedDate.ToString("yyyy-MM-dd");
                proxy.CCPRevokedDate = list[i].CcpRevokedDate.HasValue ? list[i].CcpRevokedDate.Value.ToString("yyyy-MM-dd") : "";

                proxyList.Add(proxy);
            }
            return proxyList;
        }

        [HttpPost]
        public ActionResult COOIGet(int CCIIntNo)
        {
            CooCompanyInformation company = infoService.GetByCciIntNo(CCIIntNo);
            COOCompanyInformationEntity entity = new COOCompanyInformationEntity();

            if (company != null)
            {
                entity.CCIIntNo = company.CciIntNo;
                entity.CCICompanyName = company.CciCompanyName.Trim();
                entity.CCICompanyCode = company.CciCompanyCode.Trim();
                entity.CCIRegistrationNumber = company.CciRegistrationNumber.Trim();
                entity.CCITelephone = company.CciTelephone.Trim();
                entity.CCIEmail = company.CciEmail.Trim();
                entity.CCIPhysicalAddress = company.CciPhysicalAddress.Trim();
                entity.CCIPostalAddress = company.CciPostalAddress.Trim();
            }

            return Json(entity);
        }
        
        [HttpPost]
        public ActionResult COOISave(ChangeOfOwnerAutomationModel model)
        {
            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] != null)
            {
                lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;
            }

            if (model.CCIIntNo == 0)
            {
                CooCompanyInformationQuery query1 = new CooCompanyInformationQuery();
                query1.AppendEquals(CooCompanyInformationColumn.CciCompanyName, model.CompanyName.Trim());

                TList<CooCompanyInformation> list = infoService.Find(query1 as IFilterParameterCollection);
                if (list != null && list.Count > 0)
                {
                    model.ErrorMsg = ChangeOfOwnerAutomation_cshtml.ErrorMsg1;
                }
                else
                {
                    CooCompanyInformationQuery query2 = new CooCompanyInformationQuery();
                    query2.AppendEquals(CooCompanyInformationColumn.CciCompanyCode, model.CompanyCode.Trim());

                    list = infoService.Find(query2 as IFilterParameterCollection);
                    if (list != null && list.Count > 0)
                    {
                        model.ErrorMsg = ChangeOfOwnerAutomation_cshtml.ErrorMsg2;
                    }
                    else
                    {
                        CooCompanyInformation company = new CooCompanyInformation();
                        company.CciCompanyName = model.CompanyName.Trim();
                        company.CciCompanyCode = model.CompanyCode.Trim();
                        company.CciRegistrationNumber = model.RegistrationNumber.Trim();
                        company.CciTelephone = model.Telephone.Trim();
                        company.CciEmail = model.Email.Trim();
                        company.CciPhysicalAddress = model.PhysicalAddress.Trim();
                        company.CciPostalAddress = model.PostalAddress.Trim();
                        company.LastUser = LastUser;
                        company = infoService.Save(company);
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, this.LastUser, PunchStatisticsTranTypeList.OwnerAutomationSetup, PunchAction.Add);
                    }
                }
            }
            else
            {
                CooCompanyInformationQuery query1 = new CooCompanyInformationQuery();
                query1.AppendNotEquals(CooCompanyInformationColumn.CciIntNo, model.CCIIntNo.ToString());
                query1.AppendEquals(CooCompanyInformationColumn.CciCompanyName, model.CompanyName.Trim());

                TList<CooCompanyInformation> list = infoService.Find(query1 as IFilterParameterCollection);
                if (list != null && list.Count > 0)
                {
                    model.ErrorMsg = ChangeOfOwnerAutomation_cshtml.ErrorMsg1;
                }
                else
                {
                    CooCompanyInformationQuery query2 = new CooCompanyInformationQuery();
                    query2.AppendNotEquals(CooCompanyInformationColumn.CciIntNo, model.CCIIntNo.ToString());
                    query2.AppendEquals(CooCompanyInformationColumn.CciCompanyCode, model.CompanyCode.Trim());

                    list = infoService.Find(query2 as IFilterParameterCollection);
                    if (list != null && list.Count > 0)
                    {
                        model.ErrorMsg = ChangeOfOwnerAutomation_cshtml.ErrorMsg2;
                    }
                    else
                    {
                        CooCompanyInformation company = infoService.GetByCciIntNo(model.CCIIntNo);
                        company.CciCompanyName = model.CompanyName.Trim();
                        company.CciCompanyCode = model.CompanyCode.Trim();
                        company.CciRegistrationNumber = model.RegistrationNumber.Trim();
                        company.CciTelephone = model.Telephone.Trim();
                        company.CciEmail = model.Email.Trim();
                        company.CciPhysicalAddress = model.PhysicalAddress.Trim();
                        company.CciPostalAddress = model.PostalAddress.Trim();
                        company.LastUser = LastUser;
                        company = infoService.Save(company);
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, this.LastUser, PunchStatisticsTranTypeList.OwnerAutomationSetup, PunchAction.Change);
                    }
                }
            }

            return RedirectToAction("ChangeOfOwnerAutomationSetup", new { ErrorMsg = model.ErrorMsg });
        }

        [HttpPost]
        public ActionResult COOIDelete(int CCIIntNo)
        {
            bool Status = true;
            string ErrorMsg = "";
            TList<CooCompanyProxy> proxyList = proxyService.GetByCciIntNo(CCIIntNo);
            if (proxyList != null && proxyList.Count > 0)
            {
                Status = false;
                ErrorMsg = ChangeOfOwnerAutomation_cshtml.DeleteProxiesFirst;
                return Json(new { Status = Status, ErrorMsg = ErrorMsg });
            }

            TList<CooForCityApproval> approvalList = new CooForCityApprovalService().GetByCciIntNo(CCIIntNo);
            if (approvalList != null && approvalList.Count > 0)
            {
                Status = false;
                ErrorMsg = ChangeOfOwnerAutomation_cshtml.DeleteInfoError;
                return Json(new { Status = Status, ErrorMsg = ErrorMsg });
            }

            CooCompanyInformation company = infoService.GetByCciIntNo(CCIIntNo);
            if (company != null)
            {
                infoService.Delete(company);
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.OwnerAutomationSetup, PunchAction.Delete);
            }
            return Json(new { Status = Status, ErrorMsg = ErrorMsg });
        }

        [HttpPost]
        public ActionResult COOPGet(int CCPIntNo)
        {
            CooCompanyProxy proxy = proxyService.GetByCcpIntNo(CCPIntNo);
            COOCompanyProxyEntity entity = new COOCompanyProxyEntity();

            if (proxy != null)
            {
                entity.CCPIntNo = proxy.CcpIntNo;
                entity.CCIIntNo = proxy.CciIntNo;
                entity.CCPNumber = proxy.CcpNumber.Trim();
                entity.CCPEmailAddress = proxy.CcpEmailAddress.Trim();
                entity.IsRevoked = proxy.CcpRevokedDate.HasValue;

            }
            return Json(entity);
        }

        [HttpPost]
        public ActionResult CheckProxy(string CCPNumber, int CCIIntNo)
        {
            bool Status = true;
            string ErrorMsg = "";
            int Count = 0;
            long num = 0;
            if (long.TryParse(CCPNumber.Trim(), out num) && CCPNumber.Trim().Length == 13)
            {
                CooCompanyProxy proxy = proxyService.GetByCciIntNoCcpNumber(CCIIntNo, CCPNumber.Trim());
                if (proxy != null)
                {
                    Status = false;
                    ErrorMsg = ChangeOfOwnerAutomation_cshtml.AlreadyExisted;
                }
                else
                {
                    CooCompanyProxyQuery query = new CooCompanyProxyQuery();

                    query.AppendEquals(CooCompanyProxyColumn.CcpNumber, CCPNumber.Trim());

                    TList<CooCompanyProxy> list = proxyService.Find(query as IFilterParameterCollection);
                    if (list != null)
                    {
                        Count = list.Count;
                    }
                }
            }
            else
            {
                Status = false;
                ErrorMsg = ChangeOfOwnerAutomation_cshtml.Number_NotAvailable;
            }

            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            //punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.OwnerAutomationSetup, PunchAction.Other);
            return Json(new { Count = Count, Status = Status, ErrorMsg = ErrorMsg });
        }

        [HttpPost]
        public ActionResult COOPSave(ChangeOfOwnerAutomationModel model)
        {
            bool Status = true;
            string ErrorMsg = "";
            if (model.CCPIntNo == 0)
            {
                CooCompanyProxyQuery query = new CooCompanyProxyQuery();
                query.AppendEquals(CooCompanyProxyColumn.CcpNumber, model.CCPNumber.Trim());
                TList<CooCompanyProxy> list = proxyService.Find(query as IFilterParameterCollection);

                CooCompanyProxy proxy = new CooCompanyProxy();
                proxy.CciIntNo = model.CCIIntNo_PK;
                proxy.CcpNumber = model.CCPNumber.Trim();
                proxy.CcpAuthorisedDate = DateTime.Now;
                if (model.IsRevoked)
                {
                    proxy.CcpRevokedDate = DateTime.Now;
                }
                proxy.LastUser = LastUser;

                if (list != null && list.Count > 0)
                {
                    proxy.CcpPassword = list[0].CcpPassword;
                    proxy.CcpEmailAddress = list[0].CcpEmailAddress.Trim();
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(model.CCPPassword))
                    {
                        ErrorMsg = ChangeOfOwnerAutomation_cshtml.Password_NotAvailable;
                        Status = false;
                    }
                    else if (string.IsNullOrWhiteSpace(model.CCPEmailAddress))
                    {
                        ErrorMsg = ChangeOfOwnerAutomation_cshtml.EmailAddress_NotAvailable;
                        Status = false;
                    }
                    else
                    {
                        proxy.CcpPassword = Encrypt.HashPassword(model.CCPPassword);
                        proxy.CcpEmailAddress = model.CCPEmailAddress.Trim();
                    }
                }

                if (Status)
                {
                    proxy = proxyService.Save(proxy);
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, this.LastUser, PunchStatisticsTranTypeList.OwnerAutomationSetup, PunchAction.Add);
                }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(model.CCPEmailAddress))
                {
                    ErrorMsg = ChangeOfOwnerAutomation_cshtml.EmailAddress_NotAvailable;
                    Status = false;
                }
                if (Status)
                {
                    CooCompanyProxy proxy = proxyService.GetByCcpIntNo(model.CCPIntNo);
                    if (proxy != null)
                    {
                        proxy.CcpRevokedDate = model.IsRevoked ? DateTime.Now : (DateTime?)null;
                        proxy.LastUser = this.LastUser;
                        proxy = proxyService.Save(proxy);

                        CooCompanyProxyQuery query = new CooCompanyProxyQuery();
                        query.AppendEquals(CooCompanyProxyColumn.CcpNumber, model.CCPNumber.Trim());

                        TList<CooCompanyProxy> list = proxyService.Find(query as IFilterParameterCollection);
                        for (int i = 0; i < list.Count; i++)
                        {
                            if(!string.IsNullOrWhiteSpace(model.CCPPassword))
                            {
                                list[i].CcpPassword = Encrypt.HashPassword(model.CCPPassword);
                            }
                            list[i].CcpEmailAddress = model.CCPEmailAddress.Trim();
                            list[i].LastUser = this.LastUser; // 2013-07-17 add by Henry
                        }
                        proxyService.Save(list);
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, this.LastUser, PunchStatisticsTranTypeList.OwnerAutomationSetup, PunchAction.Change);

                    }
                }
            }
            return Json(new { Status = Status, ErrorMsg = ErrorMsg });
        }

        [HttpPost]
        public ActionResult COOPDelete(int CCPIntNo)
        {
            bool Status = true;
            string ErrorMsg = "";
            TList<CooIdNotice> noticeList = new CooIdNoticeService().GetByCcpIntNo(CCPIntNo);
            if (noticeList != null && noticeList.Count > 0)
            {
                Status = false;
                ErrorMsg = ChangeOfOwnerAutomation_cshtml.DeleteProxyError;
                return Json(new { Status = Status, ErrorMsg = ErrorMsg });
            }

            TList<CooCompanyAffidavit> affidavitList = new CooCompanyAffidavitService().GetByCcpIntNo(CCPIntNo);
            if (affidavitList != null && affidavitList.Count > 0)
            {
                Status = false;
                ErrorMsg = ChangeOfOwnerAutomation_cshtml.DeleteProxyError;
                return Json(new { Status = Status, ErrorMsg = ErrorMsg });
            }

            TList<CooForCityApproval> approvalList = new CooForCityApprovalService().GetByCcpIntNo(CCPIntNo);
            if (approvalList != null && approvalList.Count > 0)
            {
                Status = false;
                ErrorMsg = ChangeOfOwnerAutomation_cshtml.DeleteProxyError;
                return Json(new { Status = Status, ErrorMsg = ErrorMsg });
            }

            CooCompanyProxy proxy = proxyService.GetByCcpIntNo(CCPIntNo);
            if (proxy != null)
            {
                proxyService.Delete(proxy);
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, lastUser, PunchStatisticsTranTypeList.OwnerAutomationSetup, PunchAction.Delete);
            }
            return Json(new { Status = Status, ErrorMsg = ErrorMsg });
        }

        string GetRowVersionString(object obj)
        {
            if (obj == null) return string.Empty;
            if (obj is byte[]) return Convert.ToBase64String(obj as byte[]);

            var pi = obj.GetType().GetProperty("RowVersion") ?? obj.GetType().GetProperty("Rowversion");
            if (pi == null) return string.Empty;
            var bytes = pi.GetValue(obj, null) as byte[];
            return bytes != null ? Convert.ToBase64String(bytes) : string.Empty;
        }

    }
}