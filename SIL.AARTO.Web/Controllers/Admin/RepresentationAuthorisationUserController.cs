﻿using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.Web.Resource.Admin;
using SIL.AARTO.Web.ViewModels.Admin;

namespace SIL.AARTO.Web.Controllers.Admin
{
    [AARTOAuthorize]
    public partial class AdminController
    {
        readonly RepresentationAuthorisationUserService rauService = new RepresentationAuthorisationUserService();
        bool showEdit;

        RepresentationAuthorisationUserEntity FromEntity(IRepresentationAuthorisationUser entity)
        {
            if (entity == null) return null;

            var model = new RepresentationAuthorisationUserEntity
            {
                RauIntNo = entity.RauIntNo,
                RauName = entity.RauName,
                RauForceNumber = entity.RauForceNumber,
                LastUser = entity.LastUser,
                RowVersion = entity.RowVersion
            };

            return model;
        }

        RepresentationAuthorisationUser FromModel(RepresentationAuthorisationUserEntity model)
        {
            if (model == null) return null;

            var entity = new RepresentationAuthorisationUser
            {
                RauIntNo = model.RauIntNo,
                RauName = model.RauName,
                RauForceNumber = model.RauForceNumber,
                LastUser = ((UserLoginInfo)HttpContext.Session["UserLoginInfo"]).UserName,
                RowVersion = model.RowVersion ?? new byte[8]
            };
            entity.EntityState = entity.RauIntNo > 0 ? EntityState.Changed : EntityState.Added;

            return entity;
        }

        void FillEntity(RepresentationAuthorisationUserModel model, int id)
        {
            if (model == null) model = new RepresentationAuthorisationUserModel();
            if (id < 0 || !this.showEdit)
            {
                model.RauEntity = null;
                return;
            }
            if (id == 0)
            {
                model.RauEntity = new RepresentationAuthorisationUserEntity();
                return;
            }

            var entity = this.rauService.GetByRauIntNo(id);
            model.RauEntity = FromEntity(entity);
        }

        void FillList(RepresentationAuthorisationUserModel model, int pageIndex)
        {
            if (model == null) model = new RepresentationAuthorisationUserModel();
            model.RauList.Clear();
            int totalCount, index = 0;
            var rauList = this.rauService.GetPaged(pageIndex, Config.PageSize, out totalCount);
            var pageTotal = Math.Ceiling((double)totalCount/Config.PageSize);
            if (pageIndex < 0 || pageIndex > pageTotal - 1)
            {
                if (pageIndex < 0)
                    rauList = this.rauService.GetPaged(0, Config.PageSize, out totalCount);
                if (pageIndex > pageTotal)
                    rauList = this.rauService.GetPaged((int)pageTotal, Config.PageSize, out totalCount);
            }
            rauList.ForEach(r =>
            {
                var entity = FromEntity(r);
                entity.Order = ++index;
                model.RauList.Add(entity);
            });
            model.PageIndex = pageIndex;
            model.PageSize = Config.PageSize;
            model.TotalCount = totalCount;
        }


        public ActionResult RauIndex(int id = 0, int page = 0)
        {
            var model = new RepresentationAuthorisationUserModel();

            FillList(model, page);
            FillEntity(model, id);

            return View("RepresentationAuthorisationUser", model);
        }

        public ActionResult RauCreate(int page = 0)
        {
            this.showEdit = true;
            return RauIndex(0, page);
        }

        public ActionResult RauEdit(int id, int page = 0)
        {
            this.showEdit = true;
            return RauIndex(id, page);
        }

        [HttpPost]
        public ActionResult RauUpdate(RepresentationAuthorisationUserModel model, int page = 0)
        {
            this.showEdit = true;

            if (!ModelState.IsValid)
            {
                var sb = new StringBuilder();
                foreach (var err in ModelState.SelectMany(ms => ms.Value.Errors))
                {
                    sb.Append(err.ErrorMessage);
                    sb.Append("<br />");
                }
                ViewBag.Error = sb.ToString();
                return RauIndex(model.RauEntity != null ? model.RauEntity.RauIntNo : 0, page);
            }

            try
            {
                var create = model.RauEntity.RauIntNo == 0;
                var entity = this.rauService.Save(FromModel(model.RauEntity));
                ViewBag.Message = create
                    ? RepresentationAuthorisationUser_cshtml.CreateSuccessful
                    : RepresentationAuthorisationUser_cshtml.EditSuccessful;
                if (create)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.RepresentationAuthorisationUser, PunchAction.Add);  

                }
                else
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.RepresentationAuthorisationUser, PunchAction.Change); 
                }
                return RedirectToAction("RauIndex", new { id = entity.RauIntNo, page });
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return RauIndex(model.RauEntity != null ? model.RauEntity.RauIntNo : 0, page);
            }
        }

        [HttpPost]
        public ActionResult RauDelete(int id = 0, int page = 0)
        {
            try
            {
                var entity = this.rauService.GetByRauIntNo(id);
                this.rauService.Delete(entity);
              
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.RepresentationAuthorisationUser, PunchAction.Delete); 
                ViewBag.Message = RepresentationAuthorisationUser_cshtml.DeleteSuccessful;
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
            }
            return RedirectToAction("RauIndex", new { id = 0, page });
        }

        [HttpPost]
        public JsonResult CheckRauName([Bind(Prefix = "RauEntity.RauName")] string rauName, [Bind(Prefix = "RauEntity.RauIntNo")] int rauIntNo)
        {
            RepresentationAuthorisationUser entity;
            var exists = string.IsNullOrWhiteSpace(rauName)
                || (rauIntNo <= 0 || (entity = this.rauService.GetByRauIntNo(rauIntNo)) == null || !rauName.Equals(entity.RauName, StringComparison.OrdinalIgnoreCase)) && this.rauService.GetByRauName(rauName) != null;
            return Json(!exists, JsonRequestBehavior.AllowGet);
        }
    }
}
