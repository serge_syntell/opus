﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.Admin;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.Admin
{
    public partial class AdminController : Controller
    {
        [AARTOErrorLog, LanguageFilter]
        public ActionResult ProvincialTrafficCentreList(string searchStr, string errorMsg, int page = 0)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            ProvincialTrafficCentreModel model = new ProvincialTrafficCentreModel();
            InitModel(model, searchStr, page);
            model.ErrorMsg = errorMsg;
            return View(model);
        }

        [HttpPost]
        public ActionResult SaveProvincialTrafficCentre(ProvincialTrafficCentreModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            string errorMsg = string.Empty;
            ProvincialTrafficCentre provincialTrafficCentre;

            if (model.PTCIntNo == 0)
            {
                provincialTrafficCentre = new ProvincialTrafficCentre();
                errorMsg = SIL.AARTO.Web.Resource.Admin.ProvincialTrafficCentreList.AddSuccess;
            }
            else
            {
                provincialTrafficCentre = ProvincialTrafficCentreManager.GetByPTCIntNo(model.PTCIntNo);
                if (provincialTrafficCentre == null)
                    return View();
                errorMsg = SIL.AARTO.Web.Resource.Admin.ProvincialTrafficCentreList.UpdateSuccess;
            }
            provincialTrafficCentre.PtcIntNo = model.PTCIntNo;
            provincialTrafficCentre.PtcCode = model.PTCCode;
            provincialTrafficCentre.PtcDescription = model.PTCDescription;
            provincialTrafficCentre.LastUser = userInfo.UserName;

            try
            {
                ProvincialTrafficCentreManager.Save(provincialTrafficCentre);
                if (errorMsg == SIL.AARTO.Web.Resource.Admin.ProvincialTrafficCentreList.AddSuccess)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.ProvincialTrafficCentre, PunchAction.Add);  

                }
                if (errorMsg == SIL.AARTO.Web.Resource.Admin.ProvincialTrafficCentreList.UpdateSuccess)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.ProvincialTrafficCentre, PunchAction.Change);
                }
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
            }

            return RedirectToAction("ProvincialTrafficCentreList", new { errorMsg = errorMsg });
        }

        public JsonResult GetProvincialTrafficCentre(int pTCIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            ProvincialTrafficCentre provincialTrafficCentre = ProvincialTrafficCentreManager.GetByPTCIntNo(pTCIntNo);
            if (provincialTrafficCentre == null)
                return Json(new { result = -2 });

            ProvincialTrafficCentreModel model = new ProvincialTrafficCentreModel();
            model.PTCCode = provincialTrafficCentre.PtcCode;
            model.PTCDescription = provincialTrafficCentre.PtcDescription;
            model.PTCIntNo = provincialTrafficCentre.PtcIntNo;
            return Json(new { result = 1, data = model });
        }

        public JsonResult DeleteProvincialTrafficCentre(int pTCIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            string errorMsg = string.Empty;
            try
            {
                ProvincialTrafficCentreManager.Delete(pTCIntNo);
                errorMsg = SIL.AARTO.Web.Resource.Admin.ProvincialTrafficCentreList.DeleteSuccess;
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.ProvincialTrafficCentre, PunchAction.Delete);
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
            }

            return Json(new { result = 1, data = errorMsg });
        }

        private void InitModel(ProvincialTrafficCentreModel model, string searchStr, int pageIndex)
        {
            int totalCount = 0;
            model.PageSize = Config.PageSize;
            model.SearchStr = searchStr;
            if (string.IsNullOrWhiteSpace(searchStr))
            {
                model.ProvincialTrafficCentreList = ProvincialTrafficCentreManager.GetPage(pageIndex, model.PageSize, out totalCount);
            }
            else
            {
                model.ProvincialTrafficCentreList = ProvincialTrafficCentreManager.Search(searchStr, pageIndex, model.PageSize, out totalCount);
            }
            model.TotalCount = totalCount;
        }
    }
}
