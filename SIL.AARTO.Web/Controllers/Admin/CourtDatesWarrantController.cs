﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.Admin;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.BLL.Utility;
using System.Collections;
using System.Configuration;
using SIL.AARTO.Web.Helpers;
using Stalberg.TMS;
using System.Data.SqlClient;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
using Stalberg.TMS.Data;
using System.Threading;
using SIL.AARTO.BLL.CourtDatesForWarrant;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.Admin
{
    public partial class AdminController : Controller
    {
        //
        // GET: /CourtDatesWarrant/
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
        [HttpGet]
        public ActionResult CourtDatesForWarrant()
        {
            CourtDatesWarrantModel model = new CourtDatesWarrantModel();

            InitModel(model);

            model.FirstLoad = true;

            return View(model);
        }

        [HttpPost]
        public ActionResult CourtDatesForWarrant(CourtDatesWarrantModel model)
        {
            InitModel(model);
            model.FirstLoad = false;

            return View(model);
        }

        [HttpPost]
        public ActionResult GetCourtDateForWarrant(int id)
        {
            CourtDatesForWarrant date = new CourtDatesForWarrantService().GetByCdWarIntNo(id);
            //2014-07-17 Heidi Comment out for allowing other WOA to use the same court date.(5321)
            //if (date.CdateLocked.HasValue && date.CdateLocked.Value)
            //{
            //    return Json(new { Status = false, Message = this.ResourceValue("CourtDatesForWarrant", "lblError.Text18"), });
            //}
            //else
            //{
                return Json(new { Status = true, CdateWar = date.CdateWar.ToString("yyyy-MM-dd"), CdNoOfCases = date.CdNoOfCases, OGIntNo = date.OgIntNo });
            //}
        }

        [HttpPost]
        public ActionResult SubmitCourtDatesForWarrant(int id, int autIntNo, int crtRIntNo, string cDate, int noofCases, int orginGroup)
        {
            Message msg = new Message();
            int returnValue;
            DateTime courtDate;
            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] != null)
                lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;

            courtDate = Convert.ToDateTime(cDate);

            if (courtDate <= DateTime.Today)
            {
                msg.Text = this.ResourceValue("CourtDatesForWarrant", "lblError.Text11");
                return Json(msg);
            }

            if (noofCases == 0)
            {
                msg.Text = this.ResourceValue("CourtDatesForWarrant", "lblError.Text12");
                return Json(msg);
            }

            CourtDatesForWarrantManager manager = new CourtDatesForWarrantManager(connectionString);
            if (id > 0)
            {
                returnValue = manager.UpdateCourtDates(id, Convert.ToDateTime(cDate), (short)noofCases, lastUser, orginGroup);
                if (returnValue == 0)
                    msg.Text = this.ResourceValue("CourtDatesForWarrant", "lblError.Text7");
                else if (returnValue == -2)
                    msg.Text = this.ResourceValue("CourtDatesForWarrant", "lblError.Text8");
                else if (returnValue == -3)
                    msg.Text = this.ResourceValue("CourtDatesForWarrant", "lblError.Text9");
                else
                {
                    msg.Text = this.ResourceValue("CourtDatesForWarrant", "lblError.Text10");
                    //2013-12-23 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, this.LastUser, PunchStatisticsTranTypeList.CourtDatesForWarrantMaintenance, PunchAction.Change);
                }
            }
            else
            {
                returnValue = manager.CreateCourtDates(crtRIntNo, autIntNo, Convert.ToDateTime(cDate), (short)noofCases, lastUser, orginGroup);
                if (returnValue == 0)
                    msg.Text = this.ResourceValue("CourtDatesForWarrant", "lblError.Text13");
                else if (returnValue == -2)
                    msg.Text = this.ResourceValue("CourtDatesForWarrant", "lblError.Text14");
                else if (returnValue == -3)
                    msg.Text = this.ResourceValue("CourtDatesForWarrant", "lblError.Text15");
                else
                {
                    msg.Text = this.ResourceValue("CourtDatesForWarrant", "lblError.Text16");
                    //2013-12-23 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, this.LastUser, PunchStatisticsTranTypeList.CourtDatesForWarrantMaintenance, PunchAction.Add);
                }
            }

            msg.Status = returnValue > 0;

            return Json(msg);
        }

        [HttpPost]
        public ActionResult GetOriginGroup(int crtIntNo)
        {
            if (Session["UserLoginInfo"] != null)
            {
                lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;
            }
            List<SelectListItem> list = GetriginGroup(crtIntNo, lastUser);

            return Json(list);
        }
        void InitModel(CourtDatesWarrantModel model)
        {
            UserManager userManager = new UserManager();
            List<AuthorityEntity> autList = userManager.GetAllAuthority();
            if (model.SearchAuthority == 0)
            {
                if (Session["UserLoginInfo"] != null)
                    model.SearchAuthority = ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo;
            }

            model.AuthorityList = new SelectList(autList as IEnumerable, "AutIntNo", "AutDesc", model.SearchAuthority);

            List<Court> courts = new List<Court>();
            CourtDB db = new CourtDB(connectionString);
            SqlDataReader reader = db.GetAuth_CourtListByAuth(model.SearchAuthority);
            while (reader.Read())
            {
                Court court = new Court();
                court.CrtIntNo = Convert.ToInt32(reader["CrtIntNo"].ToString());
                court.CrtName = reader["CrtDetails"].ToString();
                courts.Add(court);
            }
            reader.Close();

            courts.Insert(0, new Court() { CrtIntNo = 0, CrtName = this.Resource("msgSelectCourt") });
            model.CourtList = new SelectList(courts as IEnumerable, "CrtIntNo", "CrtName");


            List<CourtRoom> courtRooms = CourtRoomCache.GetAvailableCourtRoom(model.SearchCourt);
            foreach (CourtRoom r in courtRooms)
            {
                r.CrtRoomName = r.CrtRoomNo + " (" + r.CrtRoomName + ")";
            }
            courtRooms.Insert(0, new CourtRoom() { CrtRintNo = 0, CrtRoomName = this.Resource("msgSelectCourtRoom") });
            model.CourtRoomList = new SelectList(courtRooms, "CrtRIntNo", "CrtRoomName", model.SearchCourtRoom);

            model.OriginGroupList = new SelectList(GetriginGroup(model.SearchCourt, lastUser), "Value", "Text", model.OriginGroup);

            CourtDatesForWarrantQuery query = new CourtDatesForWarrantQuery();
            query.Append(CourtDatesForWarrantColumn.AutIntNo, model.SearchAuthority.ToString());
            query.Append(CourtDatesForWarrantColumn.CrtRintNo, model.SearchCourtRoom.ToString());
            CourtDatesForWarrantService serrvice = new CourtDatesForWarrantService();
            TList<CourtDatesForWarrant> courtDates = serrvice.Find(query as IFilterParameterCollection);
            if (courtDates != null)
            {
                serrvice.DeepLoad(courtDates, false, DeepLoadType.IncludeChildren, new Type[] { typeof(OriginGroup) });
            }

            model.CourtDates = courtDates.OrderByDescending(c => c.CdateWar).ToList();
        }

        private List<SelectListItem> GetriginGroup(int crtIntNo, string lastUser)
        {
            if (crtIntNo <= 0)
            {
                return new List<SelectListItem>();
            }
            // get court rules and set OGIntNo
            string ogIntNo;

            //Jerry 2012-05-17 add 3040 court rule
            CourtRulesDetails courtRulesDetails = new CourtRulesDetails();
            courtRulesDetails.CrtIntNo = crtIntNo;
            courtRulesDetails.CRCode = "3040";
            courtRulesDetails.LastUser = lastUser;
            DefaultCourtRules courtRule = new DefaultCourtRules(courtRulesDetails, connectionString);
            KeyValuePair<int, string> rule3040 = courtRule.SetDefaultCourtRule();
            string defaultRule3040 = rule3040.Value;
            if (defaultRule3040 == "Y")
            {
                ogIntNo = ((int)OriginGroupList.ALL).ToString();
                ogIntNo = ogIntNo + "," + ((int)OriginGroupList.S56).ToString();
                ogIntNo = ogIntNo + "," + ((int)OriginGroupList.CAM).ToString();
                ogIntNo = ogIntNo + "," + ((int)OriginGroupList.HWO).ToString();
                ogIntNo = ogIntNo + "," + ((int)OriginGroupList.NoAOG_only_All).ToString();
                ogIntNo = ogIntNo + "," + ((int)OriginGroupList.NoAOG_only_CAM_only).ToString();
                ogIntNo = ogIntNo + "," + ((int)OriginGroupList.NoAOG_only_HWO_only).ToString();
                ogIntNo = ogIntNo + "," + ((int)OriginGroupList.NoAOG_only_S56_only).ToString();
            }
            else
            {
                courtRulesDetails = new CourtRulesDetails();
                courtRulesDetails.CrtIntNo = crtIntNo;
                courtRulesDetails.CRCode = "3010";
                courtRulesDetails.LastUser = lastUser;
                courtRule = new DefaultCourtRules(courtRulesDetails, connectionString);
                KeyValuePair<int, string> rule3010 = courtRule.SetDefaultCourtRule();
                string defaultRule3010 = rule3010.Value;
                if (defaultRule3010 == "N")
                {
                    ogIntNo = ((int)OriginGroupList.ALL).ToString();
                }
                else
                {
                    courtRulesDetails = new CourtRulesDetails();
                    courtRulesDetails.CrtIntNo = crtIntNo;
                    courtRulesDetails.CRCode = "3020";
                    courtRulesDetails.LastUser = lastUser;
                    courtRule = new DefaultCourtRules(courtRulesDetails, connectionString);
                    KeyValuePair<int, string> rule3020 = courtRule.SetDefaultCourtRule();
                    string defaultRule3020 = rule3020.Value;
                    if (defaultRule3020 == "N")// is HWO or CAM
                    {
                        ogIntNo = ((int)OriginGroupList.HWO).ToString();
                        ogIntNo = ogIntNo + "," + ((int)OriginGroupList.CAM).ToString();
                    }
                    else
                    {
                        ogIntNo = ((int)OriginGroupList.HWO).ToString();
                        ogIntNo = ogIntNo + "," + ((int)OriginGroupList.S56).ToString();
                        ogIntNo = ogIntNo + "," + ((int)OriginGroupList.CAM).ToString();
                    }
                }
            }

            OriginGroupDB originGroup = new OriginGroupDB(connectionString);
            SqlDataReader reader = originGroup.OriginGroupListByArrayStrOGIntNo(ogIntNo);

            Dictionary<int, string> lookups =
                OriginGroupLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);

            List<SelectListItem> list = new List<SelectListItem>();
            while (reader.Read())
            {
                int oGIntNo = (int)reader["OGIntNo"];
                if (lookups.ContainsKey(oGIntNo))
                {
                    list.Add(new SelectListItem() { Value = oGIntNo.ToString(), Text = lookups[oGIntNo] });
                }
            }
            //ddlOriginGroup.DataSource = reader;
            //ddlOriginGroup.DataValueField = "OGIntNo";
            //ddlOriginGroup.DataTextField = "OGDescrption";
            //ddlOriginGroup.DataBind();
            reader.Close();

            return list;
        }

    }
}
