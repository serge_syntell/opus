using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.BLL.Admin.Model;
using System.Collections;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.Helpers;
using System.Configuration;
using SIL.AARTO.BLL.Utility.Printing;
using System.Data;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.CameraManagement;

namespace SIL.AARTO.Web.Controllers.Admin
{
    public partial class AdminController : Controller
    {
        //
        // GET: /Depot/
        MetroManager metroManager = new MetroManager();
        DepotManager depotManager = new DepotManager();
        private string lastUser = string.Empty;

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult DepotManage(int page=0)
        {
            DepotModel model = new DepotModel();
            InitDepotModel(model, page);
            lastUser = "";
            return View(model);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult DepotManage(DepotModel model, int page=0)
        {
            if (ModelState.IsValid)
            {
                InitDepotModel(model, page);
            }
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetDepot(int depId)
        {
            Message message = new Message();
            if (depId != 0)
            {
                AartobmDepot depot = depotManager.GetDepotByDepotId(depId);
                message.Text = depot.MtrIntNo + ":" + depot.AaBmDepotCode + ":" + depot.AaBmDepotDescription;
                message.Status = true;
            }
            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetDepotLookUpList(int depId)
        {
            //Heidi 2014-03-25 added for maintaining multiple languages(5141)
            Message message = new Message();
            if (depId != 0)
            {
                message.Status = true;
                DataSet ds = AARTOBMDepotLookupDB.GetListByAaBMDepotID(depId);
                int rowCount = ds.Tables[0].Rows.Count;
                for (int i = 0; i < rowCount; i++)
                {
                    message.Text += ds.Tables[0].Rows[i]["LSCode"].ToString() + ";";
                    message.Text += ds.Tables[0].Rows[i]["LSDescription"].ToString() + ";";
                    if (i == rowCount - 1)
                    {
                        message.Text += ds.Tables[0].Rows[i]["AaBMDepotDescription"].ToString();
                    }
                    else
                    {
                        message.Text += ds.Tables[0].Rows[i]["AaBMDepotDescription"].ToString() + ";";
                    }
                }
            }
            
            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveDepot(int depId, int mtrIntNo, string depCode, string depDescr, string depotListStr)
        {
            bool IsAdd = true;
            Message message = new Message();
            AartobmDepot depot = null;
            if (depId!=0)
            {
                depot = depotManager.GetDepotByDepotId(depId);
                IsAdd = false;
            }
            else
            {
                depot = new AartobmDepot();
                IsAdd = true;
            }
            using (ConnectionScope.CreateTransaction())
            {
                depot.AaBmDepotCode = depCode;
                depot.AaBmDepotDescription = depDescr;
                depot.MtrIntNo = mtrIntNo;
                depot.LastUser = (Session["UserLoginInfo"] as UserLoginInfo).UserName; // 2013-07-17 add by Henry
                depot = depotManager.Save(depot);

                //Heidi 2014-03-25 added for maintaining multiple languages(5141)
                if (depotListStr != "" && depotListStr != null)
                {
                    string[] listValue = depotListStr.Split(';');
                    for (int i = 0; i < listValue.Length - 1; i += 2)
                    {
                        AartobmDepotLookupService aartoBmDepotLookupService = new AartobmDepotLookupService();
                        AartobmDepotLookup aartobmDepotLookup = aartoBmDepotLookupService.GetByAaBmDepotIdLsCode(depot.AaBmDepotId, listValue[i]);
                        if (aartobmDepotLookup == null)
                        {
                            AartobmDepotLookup aartobmDepotLookupTemp = new AartobmDepotLookup();
                            aartobmDepotLookupTemp.AaBmDepotId = depot.AaBmDepotId;
                            aartobmDepotLookupTemp.LsCode = listValue[i];
                            aartobmDepotLookupTemp.AaBmDepotDescription = listValue[i + 1];
                            aartobmDepotLookupTemp.LastUser = this.LastUser; // 2013-07-18 add by Henry
                            aartoBmDepotLookupService.Save(aartobmDepotLookupTemp);
                        }
                        else
                        {
                            aartobmDepotLookup.AaBmDepotDescription = listValue[i + 1];
                            aartobmDepotLookup.LastUser = this.LastUser; // 2013-07-18 add by Henry
                            aartoBmDepotLookupService.Save(aartobmDepotLookup);
                        }
                    }
                }

                if (IsAdd)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.DepotMaintenance, PunchAction.Add);
                }
                else
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.DepotMaintenance, PunchAction.Change);
                }

                ConnectionScope.Complete();
            }

            message.Status = true;

            return Json(message);
        }

        private void InitDepotModel(DepotModel model, int pageIndex)
        {
            Metro metro = new Metro();
            metro.MtrIntNo = 0;
            metro.MtrName = this.Resource("SelectMetro");
            TList<Metro> list = metroManager.GetList();
            list.Insert(0,metro);
            model.MetroList = new SelectList(list as IEnumerable, "MtrIntNo", "MtrName", model.MetroIntNo);
            model.MetroSearchList = new SelectList(list as IEnumerable, "MtrIntNo", "MtrName", model.MetroSearchIntNo);
            model.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            int totalCount= 0;
            if (model.MetroSearchIntNo != 0)
            {
                model.DeoptList = depotManager.GetListByMetroIntNoPage(model.MetroSearchIntNo, pageIndex, model.PageSize, out totalCount);
            }
            else
            {
                model.DeoptList = depotManager.GetAllDeoptPage(pageIndex, model.PageSize, out totalCount);
            }
            model.TotalCount = totalCount;
        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult GetLanguageCountDepot()
        {
            //Heidi 2014-03-25 added for maintaining multiple languages(5141)
            Message message = new Message();
            TList<LanguageSelector> languageSelectorList = LanguageSelectorManager.GetAllLanSelector();
            for (int i = 0; i < languageSelectorList.Count; i++)
            {
                message.Text += languageSelectorList[i].LsDescription + ";";
                if (i == languageSelectorList.Count - 1)
                {
                    message.Text += languageSelectorList[i].LsCode;
                }
                else
                {
                    message.Text += languageSelectorList[i].LsCode + ";";
                }
            }
            return Json(message);
        }

    }
}
