﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.IO;
using System.Transactions;
using System.Configuration;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.Web.ViewModels.ChangeOfOwner;
using Stalberg.TMS.Data;
using SIL.AARTO.Web.Resource.ChangeOfOwner;
using SIL.AARTO.BLL.Report;

using SIL.QueueLibrary;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.ChangeOfOwner
{
    [AARTOErrorLog, LanguageFilter]
    public class ChangeOfOwnerController : Controller
    {
        //
        // GET: /ChangeOfOwner/
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

        public ActionResult Authorisation(AuthorisationModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            model.ISAccepted = 0;
            model.PageSize = Config.PageSize;
            model.TotalCount = 0;
            model.SearchCompanyList = GetCompanyList();
            model.SearchProxyList = GetProxyList(model.SearchCCIIntNo);

            ChangeOfOwnerDB db = new ChangeOfOwnerDB(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());

            int autIntNo = int.Parse(Session["DefaultAuthority"].ToString());
            DataSet ds = db.GetCOOIDNotice(model.SearchCCIIntNo, model.SearchCCPIntNo, model.Page, Config.PageSize);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    CooCompanyAffidavit affidavit = new CooCompanyAffidavitService().GetByCcpIntNo(model.SearchCCPIntNo).FirstOrDefault();
                    if (affidavit != null)
                    {
                        model.IsExistAffidavit = 1;

                        if (affidavit.IsAccepted == "N")
                        {
                            model.ISAccepted = 1;
                        }

                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            AuthorisationDetail detail = new AuthorisationDetail();
                            detail.CFCAIntNo = ds.Tables[0].Rows[i]["CFCAIntNo"].ToString();
                            detail.NotIntNo = ds.Tables[0].Rows[i]["NotIntNo"].ToString();
                            detail.NotTicketNo = ds.Tables[0].Rows[i]["NotTicketNo"].ToString();
                            detail.NotRegNo = ds.Tables[0].Rows[i]["NotRegNo"].ToString();
                            detail.NotOffenceDate = DateTime.Parse(ds.Tables[0].Rows[i]["NotOffenceDate"].ToString()).ToString("yyyy-MM-dd HH:mm");
                            detail.CompanyName = ds.Tables[0].Rows[i]["CompanyName"].ToString().Trim();

                            //detail.ProxyIDNumber = ds.Tables[0].Rows[i]["ProxyIDNumber"].ToString();
                            // 2014-05-16, Oscar added
                            detail.DrvIDNumber = ds.Tables[0].Rows[i]["DrvIDNumber"].ToString();

                            detail.DrvForenames = ds.Tables[0].Rows[i]["DrvForenames"].ToString();
                            detail.PhysicalAddress1 = ds.Tables[0].Rows[i]["PhysicalAddress1"].ToString();
                            detail.CACDescription = ds.Tables[0].Rows[i]["CACDescription"].ToString();
                            model.AuthorisationDetail.Add(detail);
                        }
                        model.TotalCount = int.Parse(ds.Tables[1].Rows[0]["TotalRowCount"].ToString());
                    }
                    else
                    {
                        model.IsExistAffidavit = 0;
                        model.ErrorMsg = AuthorisationManager.ErrorMsg1;
                        model.ShowAffidavit = 0;
                    }
                }
                else
                {
                    model.ShowAffidavit = 0;
                    model.IsExistAffidavit = 0;
                    model.ErrorMsg = AuthorisationManager.ErrorMsg2;
                }
            }
            return View(model);
        }

        public ActionResult ShowStructureGraph(int CCPIntNo)
　　    {
            CooCompanyAffidavit affidavit = new CooCompanyAffidavitService().GetByCcpIntNo(CCPIntNo).FirstOrDefault();
            if (affidavit != null)
            {
                return new FileContentResult(affidavit.Affidavit, "image/jpeg");
            }
            else
            {
                return null;
            }
　　    }  

        public SelectList GetCompanyList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            SelectListItem select = new SelectListItem()
            {
                Text = "",
                Value = ""
            };
            list.Add(select);

            SIL.AARTO.DAL.Entities.TList<CooCompanyInformation> companyList = new CooCompanyInformationService().GetAll();
            if (companyList != null)
            {
                for (int i = 0; i < companyList.Count; i++)
                {
                    SelectListItem item = new SelectListItem()
                    {
                        Text = companyList[i].CciCompanyName.Trim(),
                        Value = companyList[i].CciIntNo.ToString()
                    };
                    list.Add(item);
                }
            }
            return new SelectList(list, "Value", "Text");
        }

        public SelectList GetProxyList(int CCIIntNo)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            SelectListItem select = new SelectListItem()
            {
                Text = "",
                Value = ""
            };
            list.Add(select);

            SIL.AARTO.DAL.Entities.TList<CooCompanyProxy> proxyList = new CooCompanyProxyService().GetByCciIntNo(CCIIntNo);
            if (proxyList != null)
            {
                for (int i = 0; i < proxyList.Count; i++)
                {
                    SelectListItem item = new SelectListItem()
                    {
                        Text = proxyList[i].CcpNumber.Trim(),
                        Value = proxyList[i].CcpIntNo.ToString()
                    };
                    list.Add(item);
                }
            }
            return new SelectList(list, "Value", "Text");
        }

        public JsonResult GetProxyByQuery(int CCIIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            return Json(new { result = 1, data = GetProxyList(CCIIntNo) });
        }

        public JsonResult AffidavitAccept(int CCIIntNo, int CCPIntNo)
        {
            Message message = new Message();
            message.Status = true;

            try
            {
                CooCompanyAffidavit affidavit = new CooCompanyAffidavitService().GetByCcpIntNo(CCPIntNo).FirstOrDefault();
                if (affidavit != null)
                {
                    affidavit.IsAccepted = "Y";
                    affidavit.LastUser = this.LastUser;
                    affidavit = new CooCompanyAffidavitService().Save(affidavit);
                }
            }
            catch (Exception ex)
            {
                message.Status = false;
                message.Text = ex.ToString();
            }

            return Json(message);
        }

        public JsonResult AffidavitReject(int CCIIntNo, int CCPIntNo)
        {
            Message message = new Message();
            message.Status = true;
            
            try
            {
                CooCompanyAffidavit affidavit = new CooCompanyAffidavitService().GetByCcpIntNo(CCPIntNo).FirstOrDefault();
                if (affidavit != null)
                {
                    affidavit.IsAccepted = "R";
                    affidavit.LastUser = this.LastUser;
                    affidavit = new CooCompanyAffidavitService().Save(affidavit);
                }

                CooForCityApprovalQuery query = new CooForCityApprovalQuery();
                query.AppendEquals(CooForCityApprovalColumn.CciIntNo, CCIIntNo.ToString());
                query.AppendEquals(CooForCityApprovalColumn.CcpIntNo, CCPIntNo.ToString());
                query.AppendIsNull(CooForCityApprovalColumn.DateActionOfficer);
                query.AppendIsNull(CooForCityApprovalColumn.ActionCode);

                SIL.AARTO.DAL.Entities.TList<CooForCityApproval> list = new CooForCityApprovalService().Find(query as IFilterParameterCollection);
                for (int i = 0; i < list.Count; i++)
                {
                    list[i].ActionCode = (int)CooActionCodeList.InvalidAffidavit;
                    list[i].LastUser = this.LastUser;   // 2013-07-18 add by Henry

                    // 2015-03-17, Oscar changed, can not do further decision after affidavit rejected. (1896, ref 1895)
                    list[i].OfficerName = this.LastUser;
                    list[i].DateActionOfficer = DateTime.Now;
                    CooIdNotice idNotice = new CooIdNoticeService().GetByNotIntNo(list[i].NotIntNo).FirstOrDefault();
                    if (idNotice != null)
                    {
                        idNotice.FlagDate = DateTime.Now;
                        idNotice.LastUser = this.LastUser;
                        new CooIdNoticeService().Save(idNotice);
                    }
                }
                new CooForCityApprovalService().Save(list);
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.ChangeOfOwnerAuthorisation, PunchAction.Change);  

            }
            catch (Exception ex)
            {
                message.Status = false;
                message.Text = ex.ToString();
            }

            return Json(message);
        }

        public JsonResult Accept(int CFCAIntNo)
        {
            Message message = new Message();
            message.Status = true;

            try
            {
                CooForCityApproval approval = new CooForCityApprovalService().GetByCfcaIntNo(CFCAIntNo);
                if (approval != null)
                {
                    if (!approval.DateActionOfficer.HasValue)
                    {
                        approval.OfficerName = this.LastUser;
                        approval.ActionCode = (int)CooActionCodeList.AcceptedByOfficer;
                        approval.LastUser = this.LastUser;
                        approval = new CooForCityApprovalService().Save(approval);
                        
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.ChangeOfOwnerAuthorisation, PunchAction.Change); 
                    }
                }
            }
            catch (Exception ex)
            {
                message.Status = false;
                message.Text = ex.ToString();
            }

            return Json(message);
        }

        public JsonResult Reject(int CFCAIntNo)
        {
            Message message = new Message();
            message.Status = true;

            try
            {
                CooForCityApproval approval = new CooForCityApprovalService().GetByCfcaIntNo(CFCAIntNo);
                if (approval != null)
                {
                    if (!approval.DateActionOfficer.HasValue)
                    {
                        approval.OfficerName = this.LastUser;
                        approval.ActionCode = (int)CooActionCodeList.RejectedInsufficientDetails;
                        approval.LastUser = this.LastUser;
                        approval = new CooForCityApprovalService().Save(approval);
                       
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.ChangeOfOwnerAuthorisation, PunchAction.Change); 
                    }
                }
            }
            catch (Exception ex)
            {
                message.Status = false;
                message.Text = ex.ToString();
            }

            return Json(message);
        }

        public JsonResult ProcessSelected(int CCIIntNo, int CCPIntNo)
        {
            Message message = new Message();
            message.Status = true;
            try
            {
                DataTable dt = this.CreateTable();

                using (TransactionScope scope = new TransactionScope())
                {
                    CooForCityApprovalQuery query = new CooForCityApprovalQuery();
                    query.AppendEquals(CooForCityApprovalColumn.CciIntNo, CCIIntNo.ToString());
                    query.AppendEquals(CooForCityApprovalColumn.CcpIntNo, CCPIntNo.ToString());
                    query.AppendIsNull(CooForCityApprovalColumn.DateActionOfficer);
                    query.AppendIn(CooForCityApprovalColumn.ActionCode, new string[] { "3", "4" });

                    SIL.AARTO.DAL.Entities.TList<CooForCityApproval> list = new CooForCityApprovalService().Find(query as IFilterParameterCollection);
                    for (int i = 0; i < list.Count; i++)
                    {
                        CooForCityApproval approval = list[i];

                        approval.DateActionOfficer = DateTime.Now;
                        approval.OfficerName = this.LastUser;
                        approval.LastUser = this.LastUser;
                        approval = new CooForCityApprovalService().Save(approval);

                        CooIdNotice idNotice = new CooIdNoticeService().GetByNotIntNo(approval.NotIntNo).FirstOrDefault();
                        idNotice.FlagDate = DateTime.Now;
                        idNotice.LastUser = this.LastUser;
                        idNotice = new CooIdNoticeService().Save(idNotice);

                        SIL.AARTO.DAL.Entities.Notice notice = new NoticeService().GetByNotIntNo(approval.NotIntNo);
                        SIL.AARTO.DAL.Entities.Charge chg = new ChargeService().GetByNotIntNo(notice.NotIntNo).Where(m => m.ChgIsMain).FirstOrDefault();

                        string descr = approval.ActionCode == (int)CooActionCodeList.AcceptedByOfficer ? "Notice:" + notice.NotTicketNo + " accepted through automated change of offender" : "Notice:" + notice.NotTicketNo + " rejected through automated change of offender";
                        EvidencePack pack = new EvidencePack();
                        pack.ChgIntNo = chg.ChgIntNo;
                        pack.EpItemDate = DateTime.Now;
                        pack.EpItemDescr = descr;
                        pack.EpSourceTable = "Notice";
                        pack.EpSourceId = notice.NotIntNo;
                        pack.LastUser = this.LastUser;
                        pack.EpItemDateUpdated = true;
                        new EvidencePackService().Insert(pack);

                        //Push Queue
                        if (approval.ActionCode == (int)CooActionCodeList.AcceptedByOfficer)
                        {
                            QueueItemProcessor queProcessor = new QueueItemProcessor();
                            queProcessor.Send(new QueueItem()
                            {
                                Body = approval.CfcaIntNo.ToString(),
                                ActDate = DateTime.Now,
                                QueueType = ServiceQueueTypeList.COOForApproval,
                                Group = new AuthorityService().GetByAutIntNo(notice.AutIntNo).AutCode.Trim(),
                                LastUser = this.LastUser
                            });
                        }

                        DataRow dr = dt.NewRow();
                        dr["Notice No"] = notice.NotTicketNo;
                        dr["Registration"] = approval.RegNo;
                        dr["Offence Date"] = notice.NotOffenceDate.ToString("yyyy-MM-dd HH:mm:ss");
                        dr["Offender"] = approval.DrvSurname.Trim() + "" + approval.DrvInitials.Trim();
                        dr["Status"] = approval.ActionCode == (int)CooActionCodeList.AcceptedByOfficer ? "Accept" : new CooActionCodeService().GetByCacIntNo(approval.ActionCode.Value).CacDescription;
                        dt.Rows.Add(dr);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        CooCompanyInformation company = new CooCompanyInformationService().GetByCciIntNo(CCIIntNo);
                        CooCompanyProxy proxy = new CooCompanyProxyService().GetByCcpIntNo(CCPIntNo);

                        this.SendEmail(proxy.CcpEmailAddress.Trim(), company.CciCompanyName.Trim(), proxy.CcpNumber.Trim(), this.LastUser, dt);
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                message.Status = false;
                message.Text = ex.ToString();
            }

            return Json(message);
        }

        public JsonResult AcceptAll(int CCIIntNo, int CCPIntNo)
        {
            Message message = new Message();
            message.Status = true;
            try
            {
                DataTable dt = this.CreateTable();

                using (TransactionScope scope = new TransactionScope())
                {
                    CooForCityApprovalQuery query = new CooForCityApprovalQuery();
                    query.AppendEquals(CooForCityApprovalColumn.CciIntNo, CCIIntNo.ToString());
                    query.AppendEquals(CooForCityApprovalColumn.CcpIntNo, CCPIntNo.ToString());
                    query.AppendIsNull(CooForCityApprovalColumn.DateActionOfficer);

                    SIL.AARTO.DAL.Entities.TList<CooForCityApproval> list = new CooForCityApprovalService().Find(query as IFilterParameterCollection);
                    for (int i = 0; i < list.Count; i++)
                    {
                        CooForCityApproval approval = list[i];

                        if (!approval.ActionCode.HasValue)
                        {
                            approval.ActionCode = (int)CooActionCodeList.AcceptedByOfficer;
                        }
                        approval.DateActionOfficer = DateTime.Now;
                        approval.OfficerName = this.LastUser;
                        approval.LastUser = this.LastUser;
                        approval = new CooForCityApprovalService().Save(approval);

                        CooIdNotice idNotice = new CooIdNoticeService().GetByNotIntNo(approval.NotIntNo).FirstOrDefault();
                        idNotice.FlagDate = DateTime.Now;
                        idNotice.LastUser = this.LastUser;
                        idNotice = new CooIdNoticeService().Save(idNotice);

                        SIL.AARTO.DAL.Entities.Notice notice = new NoticeService().GetByNotIntNo(approval.NotIntNo);
                        SIL.AARTO.DAL.Entities.Charge chg = new ChargeService().GetByNotIntNo(notice.NotIntNo).Where(m => m.ChgIsMain).FirstOrDefault();

                        string descr = approval.ActionCode == (int)CooActionCodeList.AcceptedByOfficer ? "Notice:" + notice.NotTicketNo + " accepted through automated change of offender" : "Notice:" + notice.NotTicketNo + " rejected through automated change of offender";
                        EvidencePack pack = new EvidencePack();
                        pack.ChgIntNo = chg.ChgIntNo;
                        pack.EpItemDate = DateTime.Now;
                        pack.EpItemDescr = descr;
                        pack.EpSourceTable = "Notice";
                        pack.EpSourceId = notice.NotIntNo;
                        pack.LastUser = this.LastUser;
                        pack.EpItemDateUpdated = true;
                        new EvidencePackService().Insert(pack);

                        //Push Queue
                        if (approval.ActionCode == (int)CooActionCodeList.AcceptedByOfficer)
                        {
                            QueueItemProcessor queProcessor = new QueueItemProcessor();
                            queProcessor.Send(new QueueItem()
                            {
                                Body = approval.CfcaIntNo.ToString(),
                                ActDate = DateTime.Now,
                                QueueType = ServiceQueueTypeList.COOForApproval,
                                Group = new AuthorityService().GetByAutIntNo(notice.AutIntNo).AutCode.Trim(),
                                LastUser = this.LastUser
                            });
                        }

                        DataRow dr = dt.NewRow();
                        dr["Notice No"] = notice.NotTicketNo;
                        dr["Registration"] = approval.RegNo;
                        dr["Offence Date"] = notice.NotOffenceDate.ToString("yyyy-MM-dd HH:mm:ss");
                        dr["Offender"] = approval.DrvSurname.Trim() + "" + approval.DrvInitials.Trim();
                        dr["Status"] = approval.ActionCode == (int)CooActionCodeList.AcceptedByOfficer ? "Accept" : new CooActionCodeService().GetByCacIntNo(approval.ActionCode.Value).CacDescription;
                        dt.Rows.Add(dr);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        CooCompanyInformation company = new CooCompanyInformationService().GetByCciIntNo(CCIIntNo);
                        CooCompanyProxy proxy = new CooCompanyProxyService().GetByCcpIntNo(CCPIntNo);

                        this.SendEmail(proxy.CcpEmailAddress.Trim(), company.CciCompanyName.Trim(), proxy.CcpNumber.Trim(), this.LastUser, dt);
                    }

                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.ChangeOfOwnerAuthorisation, PunchAction.Change); 
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                message.Status = false;
                message.Text = ex.ToString();
            }

            return Json(message);
        }

        public JsonResult RejectAll(int CCIIntNo, int CCPIntNo)
        {
            Message message = new Message();
            message.Status = true;

            try
            {
                DataTable dt = this.CreateTable();

                using (TransactionScope scope = new TransactionScope())
                {
                    CooForCityApprovalQuery query = new CooForCityApprovalQuery();
                    query.AppendEquals(CooForCityApprovalColumn.CciIntNo, CCIIntNo.ToString());
                    query.AppendEquals(CooForCityApprovalColumn.CcpIntNo, CCPIntNo.ToString());
                    query.AppendIsNull(CooForCityApprovalColumn.DateActionOfficer);

                    SIL.AARTO.DAL.Entities.TList<CooForCityApproval> list = new CooForCityApprovalService().Find(query as IFilterParameterCollection);
                    for (int i = 0; i < list.Count; i++)
                    {
                        CooForCityApproval approval = list[i];

                        if (!approval.ActionCode.HasValue)
                        {
                            approval.ActionCode = (int)CooActionCodeList.RejectedByOfficer;
                        }
                        approval.DateActionOfficer = DateTime.Now;
                        approval.OfficerName = this.LastUser;
                        approval.LastUser = this.LastUser;
                        approval = new CooForCityApprovalService().Save(approval);

                        CooIdNotice idNotice = new CooIdNoticeService().GetByNotIntNo(approval.NotIntNo).FirstOrDefault();
                        idNotice.FlagDate = DateTime.Now;
                        idNotice.LastUser = this.LastUser;
                        idNotice = new CooIdNoticeService().Save(idNotice);

                        SIL.AARTO.DAL.Entities.Notice notice = new NoticeService().GetByNotIntNo(approval.NotIntNo);
                        SIL.AARTO.DAL.Entities.Charge chg = new ChargeService().GetByNotIntNo(notice.NotIntNo).Where(m => m.ChgIsMain).FirstOrDefault();

                        string descr = approval.ActionCode == (int)CooActionCodeList.AcceptedByOfficer ? "Notice:" + notice.NotTicketNo + " accepted through automated change of offender" : "Notice:" + notice.NotTicketNo + " rejected through automated change of offender";
                        EvidencePack pack = new EvidencePack();
                        pack.ChgIntNo = chg.ChgIntNo;
                        pack.EpItemDate = DateTime.Now;
                        pack.EpItemDescr = descr;
                        pack.EpSourceTable = "Notice";
                        pack.EpSourceId = notice.NotIntNo;
                        pack.LastUser = this.LastUser;
                        pack.EpItemDateUpdated = true;
                        new EvidencePackService().Insert(pack);

                        //Push Queue
                        if (approval.ActionCode == (int)CooActionCodeList.AcceptedByOfficer)
                        {
                            QueueItemProcessor queProcessor = new QueueItemProcessor();
                            queProcessor.Send(new QueueItem()
                            {
                                Body = approval.CfcaIntNo.ToString(),
                                ActDate = DateTime.Now,
                                QueueType = ServiceQueueTypeList.COOForApproval,
                                Group = new AuthorityService().GetByAutIntNo(notice.AutIntNo).AutCode.Trim(),
                                LastUser = this.LastUser
                            });
                        }


                        DataRow dr = dt.NewRow();
                        dr["Notice No"] = notice.NotTicketNo;
                        dr["Registration"] = approval.RegNo;
                        dr["Offence Date"] = notice.NotOffenceDate.ToString("yyyy-MM-dd HH:mm:ss");
                        dr["Offender"] = approval.DrvSurname.Trim() + "" + approval.DrvInitials.Trim();
                        dr["Status"] = approval.ActionCode == (int)CooActionCodeList.AcceptedByOfficer ? "Accept" : new CooActionCodeService().GetByCacIntNo(approval.ActionCode.Value).CacDescription;
                        dt.Rows.Add(dr);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        CooCompanyInformation company = new CooCompanyInformationService().GetByCciIntNo(CCIIntNo);
                        CooCompanyProxy proxy = new CooCompanyProxyService().GetByCcpIntNo(CCPIntNo);
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.ChangeOfOwnerAuthorisation, PunchAction.Change); 
                        this.SendEmail(proxy.CcpEmailAddress.Trim(), company.CciCompanyName.Trim(), proxy.CcpNumber.Trim(), this.LastUser, dt);
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                message.Status = false;
                message.Text = ex.ToString();
            }

            return Json(message);
        }

        private DataTable CreateTable()
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("Notice No");
            dt.Columns.Add(dc);

            dc = new DataColumn("Registration");
            dt.Columns.Add(dc);

            dc = new DataColumn("Offence Date");
            dt.Columns.Add(dc);

            dc = new DataColumn("Offender");
            dt.Columns.Add(dc);

            dc = new DataColumn("Status");
            dt.Columns.Add(dc);

            return dt;
        }
        private void SendEmail(string emailAddress, string companyName,string proxy, string officer, DataTable dt)
        {
            ChangeOfOwnerReport report = new ChangeOfOwnerReport();
            MemoryStream stream = report.ExportToExcel(companyName, proxy, officer, dt);

            string fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("{0}.xls", "Nomination Notice " + Guid.NewGuid().ToString()));
            FileStream ss = new FileStream(fileName, FileMode.OpenOrCreate);
            byte[] data = stream.GetBuffer();
            ss.Write(data, 0, data.Length);
            ss.Flush();
            ss.Close();

            EmailTransaction email = new EmailTransaction();
            //create zip file
            string zipFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("{0}.zip", "Nomination Notice " + Guid.NewGuid().ToString()));

            List<string> srcList = new List<string>();
            srcList.Add(fileName);

            ZipUtility.Zip(zipFileName, srcList, String.Empty);
            FileInfo zipFile = new FileInfo(zipFileName);
            FileStream fs = zipFile.OpenRead();
            byte[] zipByte = new byte[(int)zipFile.Length];
            fs.Read(zipByte, 0, (int)zipFile.Length);

            email.EmTrAttachment = zipByte;
            email.EmTrAttachmentName = "Nomination Notice Response.zip";

            fs.Close();
            fs.Dispose();
            System.IO.File.Delete(zipFileName);
            System.IO.File.Delete(fileName);

            email.EmTrSubject = "Nomination Notice Response";
            email.EmTrTo = emailAddress;
            string content = "Greetings\r\nPlease find attached CofCT adjudication of change of offender, request submitted by you.\r\nRegards";
            content += "\r\nCity of Cape Town Traffic management";
            email.EmTrContent = content;

            EmailManager emailManager = new EmailManager();
            emailManager.SendMail(email);
        }
    }
}
