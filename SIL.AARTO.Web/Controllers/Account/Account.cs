using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Security.Principal;

using SIL.AARTO.BLL.Account.Model;
using System.Web.Security;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Account;

using SIL.AARTO.Web.Helpers;
using SIL.AARTO.BLL.EntLib;
using System.Reflection;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI;

namespace SIL.AARTO.Web.Controllers
{
    [AARTOErrorLog, LanguageFilter]
    partial class AccountController : Controller
    {
        //
        // GET: /Account/

        public ActionResult Login()
        {
            UserLoginModel model = new UserLoginModel();
            //UserLoginManager.InitUserLoginModel(model);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Login(string userName, string password, string returnUrl)
        {
            Message message = new Message();
            string APP_TITLE = string.Empty;
            string APP_NAME = AartoProjectList.AARTOWebApplication.ToString();
            string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
            Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);

            //Jerry 2014-10-15 record user name info
            EntLibLogger.WriteLog(LogCategory.General, null, "Login UserName: " + userName.Trim());

            if (UserLoginManager.GetUserByUserLoginName(userName) == null)
            {
                message.Text = this.Resource("JsUserNameIsIncorrect");
                message.Status = false;

                //Jerry 2014-10-15 record failed login info
                EntLibLogger.WriteLog(LogCategory.General, null, message.Text);
                return Json(message);
            }
            if (Membership.ValidateUser(userName.ToLower(), password))
            {
                UserLoginInfo userLoginInfo = new UserLoginInfo();
                //set session
                SIL.AARTO.DAL.Entities.User user = UserLoginManager.GetUserByUserLoginName(userName);
                userLoginInfo.UserIntNo = user.UserIntNo;
                userLoginInfo.UserName = user.UserLoginName;
                userLoginInfo.UserEmail = user.UserEmail;
                userLoginInfo.AuthIntNo = user.UserDefaultAutIntNo.Value;
                userLoginInfo.UserAccessLevel = user.UserAccessLevel;
                userLoginInfo.CurrentLanguage = user.DefaultLanguage;

                userLoginInfo.UserRoles = new List<int>();
                string roleStr = string.Empty;
                foreach (AartoUserRoleConjoin conj in
                    UserLoginManager.GetUserRolesByUserIntNo(userLoginInfo.UserIntNo))
                {
                    userLoginInfo.UserRoles.Add(conj.AaUserRoleId);
                }
                int index = 1;
                foreach (int roles in userLoginInfo.UserRoles)
                {
                    if (index == userLoginInfo.UserRoles.Count)
                        roleStr += roles;
                    else
                        roleStr += roles + ";";
                }
                //Jerry 2012-06-12 change
                userLoginInfo.UserPasswordReset = user.UserPasswordReset;
                userLoginInfo.UserPasswordGraceLoginCount = user.UserPasswordGraceLoginCount;
                userLoginInfo.UserPasswordExpiryDate = user.UserPasswordExpiryDate;
                if (userLoginInfo.UserPasswordReset || userLoginInfo.UserPasswordExpiryDate < DateTime.Now)
                {
                    if (userLoginInfo.UserPasswordGraceLoginCount == 0)
                    {
                        message.Text = this.Resource("JsBlockCannotLogin");
                        message.Status = false;

                        //Jerry 2014-10-15 record login info
                        EntLibLogger.WriteLog(LogCategory.General, null, message.Text);
                        return Json(message);
                    }
                }

                Session["UserLoginInfo"] = userLoginInfo;
                Session["autIntNo"] = userLoginInfo.AuthIntNo;
                Session["DefaultAuthority"] = userLoginInfo.AuthIntNo;
                SIL.AARTO.DAL.Entities.Authority authority = SIL.AARTO.BLL.Admin.AuthorityManager.GetAuthorityByAuthIntNo(userLoginInfo.AuthIntNo);
                if (authority != null)
                {
                    Session["autName"] = authority.AutName;
                    Session["mtrIntNo"] = authority.MtrIntNo;
                    Session["TicketProcessor"] = authority.AutTicketProcessor;
                }
                /*Try to connect all the file server(add by lucas)*/
                //FileServer.ConnectAll();
                /**/
                FormsAuthentication.SetAuthCookie(userName, true);
                //If need to set cookies, write here
                if (Request.IsAuthenticated)
                {
                    //Jerry 2014-11-12 comment out the if condition check,  write cookie everytime
                    //// Create the roles cookie if it doesn't exist yet for this session.
                    //if ((Request.Cookies["userLoginInfo"] == null) || (Request.Cookies["userLoginInfo"].Value == ""))
                    //{

                        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                                1,                              // version
                                HttpContext.User.Identity.Name,     // user name
                                DateTime.Now,                   // issue time
                                DateTime.Now.AddHours(1),       // expires every hour
                                false,                          // don't persist cookie
                                roleStr                         // roles
                            );

                        // Encrypt the ticket
                        String cookieStr = FormsAuthentication.Encrypt(ticket);

                        // Send the cookie to the client
                        Response.Cookies["userLoginInfo"].Value = cookieStr;
                        Response.Cookies["userLoginInfo"].Path = "/";
                        //Response.Cookies["portalroles"].Expires = DateTime.Now.AddMinutes(1);

                        HttpContext.User = new GenericPrincipal(HttpContext.User.Identity, roleStr.Split(';'));
                    //}
                    //else
                    //{

                    //    // Get roles from roles cookie
                    //    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(HttpContext.Request.Cookies["userLoginInfo"].Value);

                    //    HttpContext.User = new GenericPrincipal(HttpContext.User.Identity, ticket.UserData.Split(';'));
                    //}

                }
                if (userLoginInfo.UserPasswordReset || userLoginInfo.UserPasswordExpiryDate < DateTime.Now)
                {
                    message.Text = "/Account/Reset";
                    message.User = "Block";
                    user.UserPasswordGraceLoginCount = 0;
                    user.LastUser = userLoginInfo.UserName;
                    new SIL.AARTO.DAL.Services.UserService().Save(user);
                }
                else
                {
                    message.Text = "/Home/Index";
                }

                //if (String.IsNullOrEmpty(returnUrl))
                //{

                //}
                //else
                //{
                //    message.Text = returnUrl;
                //}
                message.Status = true;
               // return Json(new { message = message });

                return Json(message,"application/json");
            }
            else
            {
                message.Text = this.Resource("JsPasswordIsIncorrect");
                message.Status = false;

                //Jerry 2014-10-15 record failed login info
                EntLibLogger.WriteLog(LogCategory.General, null, message.Text);
                return Json(message);
            }

        }

        public ActionResult RequestReset()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult RequestReset(string UserLoginName, string UserEmail)
        {
            Message message = new Message { Status = false };

            using (User user = UserLoginManager.GetUserByUserLoginName(UserLoginName))
            {
                if (user != null)
                {
                    if (user.UserEmail.ToLower() == UserEmail.ToLower())
                    {
                        //reset and send link
                        message.Status = ResetPasswordLink.CreateAndSendResetLink(user.UserIntNo, user.UserEmail, string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));

                        if (message.Status == true)
                            message.Text = "Reset password link has been sent to your Email Address.";
                        else
                            message.Text = "Faild to email password. Please contact support.";

                    }
                    else
                    {
                        message.Text = "Username and email address combination do not exist";
                        //return Json(message, "application/json");
                    }
                }
                else
                {
                    message.Text = "Username not found";
                    //return Json(message, "application/json");
                }
            }
            return Json(message, "application/json");
        }

        public ActionResult Reset(string ID)
        {
            var userIntNo = UserLoginManager.CheckResetLinkId(ID);

            // If below returns null, then continue
            // else get the usermodel from database linked to this ID
            if (userIntNo.HasValue)
            {
                //first get the DB entry using the GUID

                //using above results, get user model from userId;
                User user = UserLoginManager.GetUserByUserID(userIntNo.Value);

                //UserLoginInfo userLoginInfo = new UserLoginInfo();

                //userLoginInfo.UserPasswordReset = user.UserPasswordReset;
                //userLoginInfo.UserPasswordGraceLoginCount = user.UserPasswordGraceLoginCount;
                //userLoginInfo.UserPasswordExpiryDate = user.UserPasswordExpiryDate;
                ////if (userLoginInfo.UserPasswordReset || userLoginInfo.UserPasswordExpiryDate < DateTime.Now)
                ////{
                ////    if (userLoginInfo.UserPasswordGraceLoginCount == 0)
                ////    {
                ////        message.Text = this.Resource("JsBlockCannotLogin");
                ////        message.Status = false;

                ////        //Jerry 2014-10-15 record login info
                ////        EntLibLogger.WriteLog(LogCategory.General, null, message.Text);
                ////        return Json(message);
                ////    }
                ////}

                //Session["UserLoginInfo"] = userLoginInfo;
                //Session["autIntNo"] = userLoginInfo.AuthIntNo;
                //Session["DefaultAuthority"] = userLoginInfo.AuthIntNo;
                //SIL.AARTO.DAL.Entities.Authority authority = SIL.AARTO.BLL.Admin.AuthorityManager.GetAuthorityByAuthIntNo(userLoginInfo.AuthIntNo);
                //if (authority != null)
                //{
                //    Session["autName"] = authority.AutName;
                //    Session["mtrIntNo"] = authority.MtrIntNo;
                //    Session["TicketProcessor"] = authority.AutTicketProcessor;
                //}
                /*Try to connect all the file server(add by lucas)*/
                //FileServer.ConnectAll();
                /**/
                FormsAuthentication.SetAuthCookie(user.UserLoginName, true);

                ViewBag.Authenticated = true;
            }

            return View();
        }

        [HttpPost]
        public ActionResult Reset(string Password1, string PasswordRetype)
        {
            Message message = new Message();
            if (Password1 == PasswordRetype)
            {
                var errors = UserLoginManager.ValidatePassword(Password1).ToList();

                if (errors.Count() == 0)
                {
                    if (UserLoginManager.ChangePassword(User.Identity.Name, Password1))
                    {
                        FormsAuthentication.SignOut();
                        message.Text = "Your password has been changed.\n You will be directed to the login page.";
                        //return RedirectToAction("Index", "Home");
                        message.Status = true;
                    }
                    else
                        message.Text = "Error saving password";
                }
                else {
                    foreach (var error in errors)
                        message.Text += error + Environment.NewLine;
                }
                // after password is submitted to DB, notify user, and redirect to home screen
               

            }
            else
                message.Text = "Passwords do not match";

            return Json(message);
        }
    }
}
