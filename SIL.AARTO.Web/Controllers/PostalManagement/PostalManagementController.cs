﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.PostalManagement;
using SIL.AARTO.BLL.PostalManagement;
using SIL.AARTO.BLL.Culture;
using System.Data;
using Stalberg.TMS;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using System.Transactions;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.PostalManagement;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using System.Configuration;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.ServiceQueueLibrary.DAL.Services;
using SIL.ServiceQueueLibrary.DAL.Data;


namespace SIL.AARTO.Web.Controllers.PostalManagement
{
    [AARTOErrorLog, LanguageFilter]
    public partial class PostalManagementController : Controller
    {
        #region Define variables
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
        NoticeService noticeService = new NoticeService();
        Notice noticeEntity = new Notice();
        DateRulesDetails dateRule_OffenceDate_1stNoticePostedDate = new DateRulesDetails();
        DefaultDateRules DefaultDateRule_OffenceDate_1stNoticePostedDate;
        PrintFileNameService pfnSvc = new PrintFileNameService();
        PrintFileNameNoticeService pfnnSvc = new PrintFileNameNoticeService();
        ChargeService chargeService = new ChargeService();
        SIL.AARTO.DAL.Entities.TList<Charge> charges;
        List<SIL.AARTO.DAL.Entities.Charge> chargeList;
        PrintFileName pfnEntity;
        PrintFileName oldPfnEntity;
        PrintFileNameNotice pfnnEntity;
        PrintFileNameNotice oldPfnnEntity;
        QueueItemProcessor queProcessor = new QueueItemProcessor();
        AuthorityService anthorityService = new AuthorityService();
        SIL.AARTO.DAL.Entities.Authority authorityEntity;
        NoticeFrameService noticeFrameService = new NoticeFrameService();
        SIL.AARTO.DAL.Entities.NoticeFrame noticeFrameEntity;
        SIL.AARTO.DAL.Entities.TList<Notice> notices;
        List<SIL.AARTO.DAL.Entities.Notice> noticeList;
        NoticeDB noticedb;
        EvidencePackService evidencePackService= new EvidencePackService();
        List<EvidencePack> evidencePackList;
        ServiceQueueService _serviceQueueService = new ServiceQueueService();
        ServiceQueueQuery _serviceQueueQuery = new ServiceQueueQuery();
       
        #endregion
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
      
        /// <summary>
        /// 2013-2-22 Heidi change NoticePost.aspx page to  MVC NoticePost page
        /// </summary>
        public ActionResult NoticePost(int page = 0, string NoticeType = "", string PrintNamePrefix = "", string BeginDate = "", string EndDate = "", string PrintFileName = "")
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            if (PrintNamePrefix != "")
            {
                //PunchStats805806 enquiry ConfirmNoticePosted
            }
            NoticePostModelList model = new NoticePostModelList();
            Session["currPageIndex"] = page;
            return View(InitMode(page, NoticeType, PrintNamePrefix, BeginDate, EndDate, PrintFileName));
        }

        private NoticePostModelList InitMode(int page, string NoticeType, string PrintNamePrefix, string BeginDate, string EndDate, string printFileName)
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString; //Application["constr"].ToString();

            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            if (NoticeType == "")
            {
                NoticeType = "1ST";
            }
            if (PrintNamePrefix == "")
            {
                PrintNamePrefix = "SPD";
            }

            //Add by Brian 2013-10-22.
            printFileName = string.IsNullOrEmpty(printFileName) ? string.Empty : HttpUtility.UrlDecode(printFileName).Trim();

            DateTime beginDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(-30);
            DateTime endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(1).AddSeconds(-1);
            if (BeginDate != "")
            {
                beginDate = Convert.ToDateTime(BeginDate);
            }
            if (EndDate != "")
            {
                endDate = Convert.ToDateTime(EndDate).AddDays(1).AddSeconds(-1);
            }

            NoticePostModelList model = new NoticePostModelList();
            model.PageSize = Config.PageSize;
            int totalCount;
            AARTO.DAL.Services.NoticeService noticeService = new SIL.AARTO.DAL.Services.NoticeService();
            NoticeDB db = new NoticeDB(connectionString);

            DataSet ds = db.GetPagedNoticePostalList(autIntNo, NoticeType, PrintNamePrefix, beginDate, endDate, printFileName, page, model.PageSize, out  totalCount);

            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            NoticePostModel item = new NoticePostModel();
                            item.PrintFileName = dt.Rows[i]["PrintFileName"] == null ? "" : Convert.ToString(dt.Rows[i]["PrintFileName"]);
                            item.FirstPostedDate = dt.Rows[i]["FirstPostedDate"] == null ? "" : Convert.ToString(dt.Rows[i]["FirstPostedDate"]);
                            item.PrintDate = dt.Rows[i]["PrintDate"] == null ? "" : Convert.ToString(dt.Rows[i]["PrintDate"]);
                            item.NumberOfNotices = Convert.ToInt32(dt.Rows[i]["NumberOfNotices"]);
                            item.IsFirst = Convert.ToString(dt.Rows[i]["IsFirst"]);
                            model.NoticePostModels.Add(item);
                        }
                    }
                    else
                    {
                        if (page == 0)
                        {
                            model.ErrorMsg = NoticePostRes.lblErrorText13; //"There are no notices for post.";
                        }
                        else if (page > 0)
                        {
                            model.ErrorMsg = NoticePostRes.lblErrorText13;
                        }

                    }
                }
            }

            model.TotalCount = Convert.ToInt32(totalCount);
            model.PageIndex = page;
            model.PrintNamePrefixs1 = PostalConfigManager.GetSelectList("1ST");
            model.PrintNamePrefixs2 = PostalConfigManager.GetSelectList("2ND");
            return model;
        }

        [HttpPost]
        public JsonResult CheckDate(string printFileName, string firstPostOrder, string printDate, string actualDate)
        {
            string error = "";

            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
      
            DateTime dtPrinted;
            if (!DateTime.TryParse(printDate, out dtPrinted))
            {
                error = NoticePostRes.lblErrorText;
                return Json(new
                {
                    result = -3,
                    errorlist = new
                    {
                        errorTip = error
                    }
                });
            }

            DateTime dtDate;

            if (!DateTime.TryParse(actualDate, out dtDate))
            {
                error = NoticePostRes.lblErrorText1;
                return Json(new
                {
                    result = -3,
                    errorlist = new
                    {
                        errorTip = error
                    }
                });
            }

            if (dtDate.Ticks < dtPrinted.Ticks)
            {
                error = NoticePostRes.lblErrorText2;
                return Json(new
                {
                    result = -3,
                    errorlist = new
                    {
                        errorTip = error
                    }
                });
            }
            if (dtDate.Date > DateTime.Now.Date)
            {
                error = NoticePostRes.lblErrorText16;
                return Json(new
                {
                    result = -3,
                    errorlist = new
                    {
                        errorTip = error
                    }
                });
            }

            if (dtDate.Date < DateTime.Now.AddDays(-14).Date)
            {
                error = NoticePostRes.lblErrorText17;
                return Json(new
                {
                    result = -4,
                    errorlist = new
                    {
                        errorTip = error
                    }
                });
            }

            return Json(new
            {
                result =1,
                errorlist = new
                {
                    errorTip =error
                }
            });
            
           
        }

        [HttpPost]
        public JsonResult SetPostageStatus(string printFileName, string firstPostOrder, string printDate, string actualDate,
                                            string NoticeType, string PrintNamePrefix, string BeginDate, string EndDate, string queryPrintFileName)
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString; //Application["constr"].ToString();
            SIL.AARTO.BLL.Utility.UserLoginInfo userDetails = new BLL.Utility.UserLoginInfo();
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            string login = userDetails.UserName;
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            string error = "";
            string authCode = "";

            #region validation
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            DateTime dtPrinted;
            if (!DateTime.TryParse(printDate, out dtPrinted))
            {
                error = NoticePostRes.lblErrorText;
                return Json(new
                {
                    result = -3,
                    errorlist = new
                    {
                        errorTip = error
                    }
                });
            }

            DateTime dtDate;

            if (!DateTime.TryParse(actualDate, out dtDate))
            {
                error = NoticePostRes.lblErrorText1;
                return Json(new
                {
                    result = -3,
                    errorlist = new
                    {
                        errorTip = error
                    }
                });
            }

            if (dtDate.Ticks < dtPrinted.Ticks)
            {
                error = NoticePostRes.lblErrorText2;
                return Json(new
                {
                    result = -3,
                    errorlist = new
                    {
                        errorTip = error
                    }
                });
            }
            if (dtDate.Date > DateTime.Now.Date)
            {
                error = NoticePostRes.lblErrorText16;
                return Json(new
                {
                    result = -3,
                    errorlist = new
                    {
                        errorTip = error
                    }
                });
            }
            #endregion

            try
            {
                #region update postal status

                #region ExpirePrintFile

                authorityEntity = anthorityService.GetByAutIntNo(autIntNo);

                if (authorityEntity!=null)
                {
                    authCode = authorityEntity.AutCode;
                }
                
                PrintNamePrefix = printFileName.Substring(0, 3).ToUpper();
                if (printFileName.ToUpper().Contains("NAG"))
                {
                    PrintNamePrefix = "NAG";
                }
                string cameraAndVideoNoticeProfix = "SPD,RLV,ASD,NAG,BUS,VID";
                if (cameraAndVideoNoticeProfix.Contains(PrintNamePrefix))
                {
                    //DR_NotOffenceDate_NotPosted1stNoticeDate
                    dateRule_OffenceDate_1stNoticePostedDate.AutIntNo = autIntNo;
                    dateRule_OffenceDate_1stNoticePostedDate.DtRStartDate = "NotOffenceDate";
                    dateRule_OffenceDate_1stNoticePostedDate.DtREndDate = "NotPosted1stNoticeDate";
                    dateRule_OffenceDate_1stNoticePostedDate.LastUser = LastUser;
                    DefaultDateRule_OffenceDate_1stNoticePostedDate = new DefaultDateRules(dateRule_OffenceDate_1stNoticePostedDate, connectionString);
                    int noOfDays = DefaultDateRule_OffenceDate_1stNoticePostedDate.SetDefaultDateRule();

                    notices = noticeService.GetByAutIntNoNotCiprusPrintReqName(autIntNo, printFileName);

                    noticeList = notices.Where(n => (n.NoticeStatus == 510 || n.NoticeStatus == 250) && dtDate.Date > n.NotOffenceDate.Date.AddDays(noOfDays)).ToList();

                    string oldPrintFileName = printFileName;
                    string newPrintFileName = printFileName.Replace("_PostCanPost", "").Replace("_PostCan", "");
                    newPrintFileName = newPrintFileName + "_PostCan";
                    oldPfnEntity = pfnSvc.GetByPrintFileName(oldPrintFileName);
                    pfnEntity = pfnSvc.GetByPrintFileName(newPrintFileName);


                    if (noticeList != null && noticeList.Count > 0)
                    {
                        foreach (var item in noticeList)
                        {

                            using (TransactionScope scope = new TransactionScope())
                            {

                                #region remove notice from OriginalPrintFileName

                                if (oldPfnEntity != null)
                                {
                                    oldPfnnEntity = pfnnSvc.GetByNotIntNoPfnIntNo(item.NotIntNo, oldPfnEntity.PfnIntNo);
                                    if (oldPfnnEntity != null)
                                    {
                                        pfnnSvc.Delete(oldPfnnEntity);
                                    }
                                }


                                if (pfnEntity == null)
                                {
                                    pfnEntity = new PrintFileName()
                                    {
                                        PrintFileName = newPrintFileName,
                                        AutIntNo = autIntNo,
                                        LastUser = LastUser,
                                        EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                                    };
                                    pfnEntity = pfnSvc.Save(pfnEntity);
                                }

                                pfnnEntity = pfnnSvc.GetByNotIntNoPfnIntNo(item.NotIntNo, pfnEntity.PfnIntNo);
                                if (pfnnEntity == null)
                                {
                                    pfnnEntity = new PrintFileNameNotice()
                                    {
                                        NotIntNo = item.NotIntNo,
                                        PfnIntNo = pfnEntity.PfnIntNo,
                                        LastUser = LastUser,
                                        EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                                    };
                                    pfnnEntity = pfnnSvc.Save(pfnnEntity);
                                }

                                #endregion

                                item.NotOriginalPrint1stNoticeDate = item.NotPrint1stNoticeDate;
                                item.NotPrint1stNoticeDate = null;
                                item.NotPrevNoticeStatus = item.NoticeStatus;
                                item.NotCiprusPrintReqName = pfnEntity.PrintFileName;
                                noticeService.Update(item);

                                #region push queue
                                noticeFrameEntity = noticeFrameService.GetByNotIntNo(item.NotIntNo).FirstOrDefault();
                                if (noticeFrameEntity != null)
                                {
                                    #region Get existing queue list
                                    _serviceQueueQuery = new ServiceQueueQuery();
                                    _serviceQueueQuery.Append(ServiceQueueColumn.SeQuKey, noticeFrameEntity.FrameIntNo.ToString());
                                    _serviceQueueQuery.Append(ServiceQueueColumn.SeQuGroup, authCode.Trim());
                                    _serviceQueueQuery.Append(ServiceQueueColumn.SqtIntNo, ((int)ServiceQueueTypeList.CancelExpiredViolations_Frame).ToString());
                                    var serviceQueueList = _serviceQueueService.Find((_serviceQueueQuery as SIL.ServiceQueueLibrary.DAL.Data.IFilterParameterCollection)).ToList();
                                    #endregion

                                    if (!(serviceQueueList != null && serviceQueueList.Count > 0))
                                    {
                                        queProcessor.Send(
                                            new QueueItem()
                                            {
                                                Body = noticeFrameEntity.FrameIntNo,
                                                Group = authCode.Trim(),
                                                ActDate = DateTime.Now.AddHours(24),
                                                LastUser = LastUser,
                                                QueueType = SIL.ServiceQueueLibrary.DAL.Entities.ServiceQueueTypeList.CancelExpiredViolations_Frame
                                            }
                                        );
                                    }
                                }
                                #endregion

                                scope.Complete();

                            }

                        }
                    }
                }

                #endregion

                using (TransactionScope scope = new TransactionScope())
                {
                    PostDateNotice postDate = new PostDateNotice(connectionString);
                    int ret = postDate.SetNoticePostedDate(printFileName, autIntNo, dtDate, login, ref error);
                    if (ret > 0)
                    {
                            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.ConfirmNoticePosted, PunchAction.Add);  
                        scope.Complete();
                    }
                    else
                    {
                        return Json(new
                        {
                            result = -3,
                            errorlist = new
                            {
                                errorTip = error
                            }
                        });
                    }
                }
                //}
                #endregion

                #region Set CurrPage Index
                if (NoticeType == "")
                {
                    NoticeType = "1ST";
                }
                if (PrintNamePrefix == "")
                {
                    PrintNamePrefix = "SPD";
                }

                //Add by Brian 2013-10-22
                queryPrintFileName = string.IsNullOrEmpty(queryPrintFileName) ? string.Empty : HttpUtility.UrlDecode(queryPrintFileName).Trim();

                DateTime beginDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(-30);
                DateTime endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(1).AddSeconds(-1);
                if (BeginDate != "")
                {
                    beginDate = Convert.ToDateTime(BeginDate);
                }
                if (EndDate != "")
                {
                    endDate = Convert.ToDateTime(EndDate).AddDays(1).AddSeconds(-1);
                }
                int pageSize = Config.PageSize;
                int totalCount = 0;
                NoticeDB Noticedb = new NoticeDB(connectionString);

                DataSet ds = Noticedb.GetPagedNoticePostalList(autIntNo, NoticeType, PrintNamePrefix, beginDate, endDate, queryPrintFileName, 0, pageSize, out  totalCount);

                int pageCount = totalCount / pageSize;
                int temp = totalCount % pageSize;
                if (temp > 0)
                {
                    pageCount = pageCount + 1;
                }
                int oldCurrpageIndex = (int)Session["currPageIndex"];
                int currpageIndex = 0;

                if (pageCount == 1 || pageCount == 0)
                {
                    currpageIndex = 0;
                }
                else
                {

                    if (pageCount >= (oldCurrpageIndex + 1))
                    {
                        currpageIndex = oldCurrpageIndex;
                    }
                    else
                    {
                        currpageIndex = oldCurrpageIndex - 1;
                    }
                }

                #endregion
                return Json(new
                {
                    result = 1,
                    pageIndex = new
                    {
                        currpageIndex = currpageIndex
                    }
                });

            }
            catch (Exception e)
            {
                return Json(new
                {
                    result = -1,
                    errorlist = new
                    {
                        errorTip = e.Message
                    }
                });
            }

        }
    }
}
