﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.PostalManagement;
using SIL.AARTO.BLL.PostalManagement;
using SIL.AARTO.BLL.Culture;
using System.Data;
using Stalberg.TMS;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using System.Transactions;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.PostalManagement;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using System.Configuration;
using SIL.AARTO.BLL.Utility.Printing;
using System.Threading;
using SIL.AARTO.BLL.Utility.Cache;

namespace SIL.AARTO.Web.Controllers.PostalManagement
{
    public partial class PostalManagementController : Controller
    {
        public ActionResult RevertWholePrintFile(int page = 0, int AutIntNo = 0, string DocumentType = "", string BeginDate = "", string EndDate = "", string PrintFileName = "")
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            int autIntNo = AutIntNo;
            if (autIntNo == 0)
            {
                autIntNo = Convert.ToInt32(Session["autIntNo"]);
            }


            PrintFileModelList model = new PrintFileModelList();
            Session["currPageIndex_Revert"] = page;
            return View(InitModeForRevertWholePrintFile(page, autIntNo, DocumentType, BeginDate, EndDate, PrintFileName));
        }

        private PrintFileModelList InitModeForRevertWholePrintFile(int page, int autIntNo, string DocumentType, string BeginDate, string EndDate, string printFileName)
        {
            //Add by Brian 2013-10-22.
            printFileName = string.IsNullOrEmpty(printFileName) ? string.Empty : HttpUtility.UrlDecode(printFileName).Trim();

            DateTime beginDate = DateTime.Now.Date.AddDays(-30);
            DateTime endDate = DateTime.Now.Date.AddDays(1).AddSeconds(-1);
            if (BeginDate != "")
            {
                beginDate = Convert.ToDateTime(BeginDate);
            }
            if (EndDate != "")
            {
                endDate = Convert.ToDateTime(EndDate).AddDays(1).AddSeconds(-1);
            }

            DocumentType = DocumentType == "" ? "SPD" : DocumentType;

            PrintFileModelList model = new PrintFileModelList();
            model.PageSize = Config.PageSize;
            int totalCount;
            AARTO.DAL.Services.NoticeService noticeService = new SIL.AARTO.DAL.Services.NoticeService();
            noticedb = new NoticeDB(connectionString);

            DataSet ds = noticedb.GetRevertPrintFileList(autIntNo, DocumentType, beginDate, endDate, printFileName, page + 1, model.PageSize, out  totalCount);

            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            PrintFileModel item = new PrintFileModel();
                            item.PrintFileName = dt.Rows[i]["PrintFileName"] == null ? "" : Convert.ToString(dt.Rows[i]["PrintFileName"]);
                            item.PrintDate = dt.Rows[i]["PrintDate"] == null ? "" : Convert.ToString(dt.Rows[i]["PrintDate"]);
                            item.NumberOfNotices = Convert.ToInt32(dt.Rows[i]["NumberOfNotices"]);
                            model.PrintFileModels.Add(item);
                        }
                    }
                    else
                    {
                        if (page == 0)
                        {
                            //model.ErrorMsg = NoticePostRes.lblErrorText13;
                        }
                        else if (page > 0)
                        {
                            //model.ErrorMsg = NoticePostRes.lblErrorText13;
                        }

                    }
                }
            }

            model.TotalCount = Convert.ToInt32(totalCount);
            model.PageIndex = page;
            model.AutIntNo = autIntNo;
            model.DocumentType = DocumentType;
            model.AuthorityList = GetAuthoritySelectList();
            model.DocumentTypeList = GetDocumentTypeSelectlist();
            return model;
        }

        [HttpPost]
        public JsonResult SetRevertWholePrintFile(string printFileName, int AutIntNo,
                                            string DocumentType, string BeginDate, string EndDate, string queryPrintFileName)
        {
            SIL.AARTO.BLL.Utility.UserLoginInfo userDetails = new BLL.Utility.UserLoginInfo();
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            string login = userDetails.UserName;
            int autIntNo = AutIntNo;
            string authCode = "";

            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            try
            {
                #region ExpirePrintFile

                authorityEntity = anthorityService.GetByAutIntNo(autIntNo);

                if (authorityEntity != null)
                {
                    authCode = authorityEntity.AutCode;
                }

                notices = noticeService.GetByAutIntNoNotCiprusPrintReqName(autIntNo, printFileName);

                noticeList = notices.Where(n => (n.NoticeStatus == 510 || n.NoticeStatus == 250)).ToList();

                string oldPrintFileName = printFileName;
                string newPrintFileName = printFileName.Replace("_PostCanPost", "").Replace("_PostCan", "");
                newPrintFileName = newPrintFileName + "_PostCanPost";
                oldPfnEntity = pfnSvc.GetByPrintFileName(oldPrintFileName);
                pfnEntity = pfnSvc.GetByPrintFileName(newPrintFileName);

                if (noticeList != null && noticeList.Count > 0)
                {
                    foreach (var item in noticeList)
                    {

                        using (TransactionScope scope = new TransactionScope())
                        {

                            #region remove notice from OriginalPrintFileName

                            if (oldPfnEntity != null)
                            {
                                oldPfnnEntity = pfnnSvc.GetByNotIntNoPfnIntNo(item.NotIntNo, oldPfnEntity.PfnIntNo);
                                if (oldPfnnEntity != null)
                                {
                                    pfnnSvc.Delete(oldPfnnEntity);
                                }
                            }


                            if (pfnEntity == null)
                            {
                                pfnEntity = new PrintFileName()
                                {
                                    PrintFileName = newPrintFileName,
                                    AutIntNo = autIntNo,
                                    LastUser = LastUser,
                                    EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                                };
                                pfnEntity = pfnSvc.Save(pfnEntity);
                            }

                            pfnnEntity = pfnnSvc.GetByNotIntNoPfnIntNo(item.NotIntNo, pfnEntity.PfnIntNo);
                            if (pfnnEntity == null)
                            {
                                pfnnEntity = new PrintFileNameNotice()
                                {
                                    NotIntNo = item.NotIntNo,
                                    PfnIntNo = pfnEntity.PfnIntNo,
                                    LastUser = LastUser,
                                    EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                                };
                                pfnnEntity = pfnnSvc.Save(pfnnEntity);
                            }

                            #endregion

                            item.NotPrint1stNoticeDate = item.NotOriginalPrint1stNoticeDate;
                            item.NotCiprusPrintReqName = pfnEntity.PrintFileName;
                            item.NotOriginalPrint1stNoticeDate = null;
                            item.LastUser = LastUser;
                            noticeService.Update(item);

                            #region insert EvidencePack

                            charges = chargeService.GetByNotIntNo(item.NotIntNo);
                            chargeList = charges.Where(m => m.ChgSequence == 1).ToList();
                            if (chargeList != null && chargeList.Count > 0)
                            {
                                evidencePackList = evidencePackService.Find("ChgIntNo=" + chargeList[0].ChgIntNo).Where(c => c.EpItemDescr.Contains(NoticePostRes.lblEvidencePackDesc2)).ToList();
                                if (evidencePackList == null || evidencePackList.Count == 0)
                                {
                                    EvidencePack ep = new EvidencePack();
                                    ep.ChgIntNo = chargeList[0].ChgIntNo;
                                    ep.EpItemDate = DateTime.Now;
                                    ep.EpItemDescr = String.Format(NoticePostRes.EvidencePackDesc2, LastUser);
                                    ep.EpSourceTable = "Notice";
                                    ep.EpSourceId = item.NotIntNo;
                                    ep.LastUser = LastUser;
                                    ep.EpTransactionDate = DateTime.Now;
                                    ep.EpItemDateUpdated = false;
                                    ep.EpatIntNo = (int)EpActionTypeList.Others;
                                    new EvidencePackService().Insert(ep);
                                }
                            }

                            #endregion

                            scope.Complete();

                        }

                    }
                }

                #endregion

                #region Set CurrPage Index


                //Add by Brian 2013-10-22
                queryPrintFileName = string.IsNullOrEmpty(queryPrintFileName) ? string.Empty : HttpUtility.UrlDecode(queryPrintFileName).Trim();

                DateTime beginDate = DateTime.Now.Date.AddDays(-30);
                DateTime endDate = DateTime.Now.Date.AddDays(1).AddSeconds(-1);
                if (BeginDate != "")
                {
                    beginDate = Convert.ToDateTime(BeginDate);
                }
                if (EndDate != "")
                {
                    endDate = Convert.ToDateTime(EndDate).AddDays(1).AddSeconds(-1);
                }
                int pageSize = Config.PageSize;
                int totalCount = 0;
                noticedb = new NoticeDB(connectionString);
                DataSet ds = noticedb.GetRevertPrintFileList(autIntNo, DocumentType, beginDate, endDate, queryPrintFileName, 1, pageSize, out  totalCount);

                int pageCount = totalCount / pageSize;
                int temp = totalCount % pageSize;
                if (temp > 0)
                {
                    pageCount = pageCount + 1;
                }
                int oldCurrpageIndex = (int)Session["currPageIndex_Revert"];
                int currpageIndex = 0;

                if (pageCount == 1 || pageCount == 0)
                {
                    currpageIndex = 0;
                }
                else
                {

                    if (pageCount >= (oldCurrpageIndex + 1))
                    {
                        currpageIndex = oldCurrpageIndex;
                    }
                    else
                    {
                        currpageIndex = oldCurrpageIndex - 1;
                    }
                }

                #endregion


                return Json(new
                {
                    result = 1,
                    pageIndex = new
                    {
                        currpageIndex = currpageIndex
                    }
                });

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    result = -1,
                    errorlist = new
                    {
                        errorTip = ex.Message
                    }
                });
            }
        }

        [HttpPost]
        public JsonResult BatchProcessingRevertPrintFile(int AutIntNo,
                                            string DocumentType, string BeginDate, string EndDate, string queryPrintFileName, string PrintFileNameStr)
        {
            SIL.AARTO.BLL.Utility.UserLoginInfo userDetails = new BLL.Utility.UserLoginInfo();
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            string login = userDetails.UserName;
            int autIntNo = AutIntNo;
            string authCode = "";

            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            try
            {
                #region ExpirePrintFile

                authorityEntity = anthorityService.GetByAutIntNo(autIntNo);

                if (authorityEntity != null)
                {
                    authCode = authorityEntity.AutCode;
                }

                if (!string.IsNullOrEmpty(PrintFileNameStr))
                {
                    var printFileNameArr = PrintFileNameStr.Split(',');

                    if (printFileNameArr != null && printFileNameArr.Length > 0)
                    {

                        foreach (var printFileNameitem in printFileNameArr)
                        {

                            #region processing each printfilename

                            notices = noticeService.GetByAutIntNoNotCiprusPrintReqName(autIntNo, printFileNameitem);

                            noticeList = notices.Where(n => (n.NoticeStatus == 510 || n.NoticeStatus == 250)).ToList();

                            string oldPrintFileName = printFileNameitem;
                            string newPrintFileName = printFileNameitem.Replace("_PostCanPost", "").Replace("_PostCan", "");
                            newPrintFileName = newPrintFileName + "_PostCanPost";
                            oldPfnEntity = pfnSvc.GetByPrintFileName(oldPrintFileName);
                            pfnEntity = pfnSvc.GetByPrintFileName(newPrintFileName);


                            if (noticeList != null && noticeList.Count > 0)
                            {
                                foreach (var item in noticeList)
                                {

                                    using (TransactionScope scope = new TransactionScope())
                                    {

                                        #region remove notice from OriginalPrintFileName

                                        if (oldPfnEntity != null)
                                        {
                                            oldPfnnEntity = pfnnSvc.GetByNotIntNoPfnIntNo(item.NotIntNo, oldPfnEntity.PfnIntNo);
                                            if (oldPfnnEntity != null)
                                            {
                                                pfnnSvc.Delete(oldPfnnEntity);
                                            }
                                        }


                                        if (pfnEntity == null)
                                        {
                                            pfnEntity = new PrintFileName()
                                            {
                                                PrintFileName = newPrintFileName,
                                                AutIntNo = autIntNo,
                                                LastUser = LastUser,
                                                EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                                            };
                                            pfnEntity = pfnSvc.Save(pfnEntity);
                                        }

                                        pfnnEntity = pfnnSvc.GetByNotIntNoPfnIntNo(item.NotIntNo, pfnEntity.PfnIntNo);
                                        if (pfnnEntity == null)
                                        {
                                            pfnnEntity = new PrintFileNameNotice()
                                            {
                                                NotIntNo = item.NotIntNo,
                                                PfnIntNo = pfnEntity.PfnIntNo,
                                                LastUser = LastUser,
                                                EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                                            };
                                            pfnnEntity = pfnnSvc.Save(pfnnEntity);
                                        }

                                        #endregion

                                        item.NotPrint1stNoticeDate = item.NotOriginalPrint1stNoticeDate;
                                        item.NotCiprusPrintReqName = pfnEntity.PrintFileName;
                                        item.NotOriginalPrint1stNoticeDate = null;
                                        item.LastUser = LastUser;
                                        noticeService.Update(item);

                                        #region insert EvidencePack

                                        charges = chargeService.GetByNotIntNo(item.NotIntNo);
                                        chargeList = charges.Where(m => m.ChgSequence == 1).ToList();
                                        if (chargeList != null && chargeList.Count > 0)
                                        {
                                            evidencePackList = evidencePackService.Find("ChgIntNo=" + chargeList[0].ChgIntNo).Where(c => c.EpItemDescr.Contains(NoticePostRes.lblEvidencePackDesc2)).ToList();
                                            if (evidencePackList == null || evidencePackList.Count == 0)
                                            {
                                                EvidencePack ep = new EvidencePack();
                                                ep.ChgIntNo = chargeList[0].ChgIntNo;
                                                ep.EpItemDate = DateTime.Now;
                                                ep.EpItemDescr = String.Format(NoticePostRes.EvidencePackDesc2, LastUser);
                                                ep.EpSourceTable = "Notice";
                                                ep.EpSourceId = item.NotIntNo;
                                                ep.LastUser = LastUser;
                                                ep.EpTransactionDate = DateTime.Now;
                                                ep.EpItemDateUpdated = false;
                                                ep.EpatIntNo = (int)EpActionTypeList.Others;
                                                new EvidencePackService().Insert(ep);
                                            }
                                        }

                                        #endregion

                                        scope.Complete();

                                    }

                                }
                            }

                            #endregion

                        }

                    }
                }

                #endregion

                #region Set CurrPage Index


                //Add by Brian 2013-10-22
                queryPrintFileName = string.IsNullOrEmpty(queryPrintFileName) ? string.Empty : HttpUtility.UrlDecode(queryPrintFileName).Trim();

                DateTime beginDate = DateTime.Now.Date.AddDays(-30);
                DateTime endDate = DateTime.Now.Date.AddDays(1).AddSeconds(-1);
                if (BeginDate != "")
                {
                    beginDate = Convert.ToDateTime(BeginDate);
                }
                if (EndDate != "")
                {
                    endDate = Convert.ToDateTime(EndDate).AddDays(1).AddSeconds(-1);
                }
                int pageSize = Config.PageSize;
                int totalCount = 0;
                noticedb = new NoticeDB(connectionString);
                DataSet ds = noticedb.GetRevertPrintFileList(autIntNo, DocumentType, beginDate, endDate, queryPrintFileName, 1, pageSize, out  totalCount);

                int pageCount = totalCount / pageSize;
                int temp = totalCount % pageSize;
                if (temp > 0)
                {
                    pageCount = pageCount + 1;
                }
                int oldCurrpageIndex = (int)Session["currPageIndex_Revert"];
                int currpageIndex = 0;

                if (pageCount == 1 || pageCount == 0)
                {
                    currpageIndex = 0;
                }
                else
                {

                    if (pageCount >= (oldCurrpageIndex + 1))
                    {
                        currpageIndex = oldCurrpageIndex;
                    }
                    else
                    {
                        currpageIndex = oldCurrpageIndex - 1;
                    }
                }

                #endregion


                return Json(new
                {
                    result = 1,
                    pageIndex = new
                    {
                        currpageIndex = currpageIndex
                    }
                });

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    result = -1,
                    errorlist = new
                    {
                        errorTip = ex.Message
                    }
                });
            }
        }
      
    }
}
