﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.PostalManagement;
using SIL.AARTO.BLL.PostalManagement;
using SIL.AARTO.BLL.Culture;
using System.Data;
using Stalberg.TMS;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using System.Transactions;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.PostalManagement;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using System.Configuration;
using SIL.AARTO.BLL.Utility.Printing;
using System.Threading;
using SIL.AARTO.BLL.Utility.Cache;


namespace SIL.AARTO.Web.Controllers.PostalManagement
{
    public partial class PostalManagementController : Controller
    {
        readonly string cultureName = Thread.CurrentThread.CurrentCulture.Name;

        public ActionResult ExpireWholePrintFile(int page = 0, int AutIntNo = 0, string DocumentType = "", string BeginDate = "", string EndDate = "", string PrintFileName = "")
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            int autIntNo = AutIntNo;
            if (autIntNo == 0)
            {
                autIntNo = Convert.ToInt32(Session["autIntNo"]);
            }
            

            PrintFileModelList model = new PrintFileModelList();
            Session["currPageIndex_Expire"] = page;
            return View(InitMode(page, autIntNo, DocumentType, BeginDate, EndDate, PrintFileName));
        }

        private PrintFileModelList InitMode(int page, int autIntNo, string DocumentType, string BeginDate, string EndDate, string printFileName)
        {
            //Add by Brian 2013-10-22.
            printFileName = string.IsNullOrEmpty(printFileName) ? string.Empty : HttpUtility.UrlDecode(printFileName).Trim();

            DateTime beginDate = DateTime.Now.Date.AddDays(-30);
            DateTime endDate = DateTime.Now.Date.AddDays(1).AddSeconds(-1);
            if (BeginDate != "")
            {
                beginDate = Convert.ToDateTime(BeginDate);
            }
            if (EndDate != "")
            {
                endDate = Convert.ToDateTime(EndDate).AddDays(1).AddSeconds(-1);
            }

            DocumentType = DocumentType == "" ? "SPD" : DocumentType;

            PrintFileModelList model = new PrintFileModelList();
            model.PageSize = Config.PageSize;
            int totalCount;
            AARTO.DAL.Services.NoticeService noticeService = new SIL.AARTO.DAL.Services.NoticeService();
            noticedb = new NoticeDB(connectionString);

            //DR_NotOffenceDate_NotPosted1stNoticeDate
            dateRule_OffenceDate_1stNoticePostedDate.AutIntNo = autIntNo;
            dateRule_OffenceDate_1stNoticePostedDate.DtRStartDate = "NotOffenceDate";
            dateRule_OffenceDate_1stNoticePostedDate.DtREndDate = "NotPosted1stNoticeDate";
            dateRule_OffenceDate_1stNoticePostedDate.LastUser = LastUser;
            DefaultDateRule_OffenceDate_1stNoticePostedDate = new DefaultDateRules(dateRule_OffenceDate_1stNoticePostedDate, connectionString);
            int noOfDays = DefaultDateRule_OffenceDate_1stNoticePostedDate.SetDefaultDateRule();

            DataSet ds = noticedb.GetExpirePrintFileList(autIntNo, DocumentType, beginDate, endDate, printFileName, noOfDays,page + 1, model.PageSize, out  totalCount);

            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            PrintFileModel item = new PrintFileModel();
                            item.PrintFileName = dt.Rows[i]["PrintFileName"] == null ? "" : Convert.ToString(dt.Rows[i]["PrintFileName"]);
                            item.PrintDate = dt.Rows[i]["PrintDate"] == null ? "" : Convert.ToString(dt.Rows[i]["PrintDate"]);
                            item.NumberOfNotices = Convert.ToInt32(dt.Rows[i]["NumberOfNotices"]);
                            model.PrintFileModels.Add(item);
                        }
                    }
                    else
                    {
                        if (page == 0)
                        {
                            //model.ErrorMsg = NoticePostRes.lblErrorText13; 
                        }
                        else if (page > 0)
                        {
                            //model.ErrorMsg = NoticePostRes.lblErrorText13;
                        }

                    }
                }
            }

            model.TotalCount = Convert.ToInt32(totalCount);
            model.PageIndex = page;
            model.AutIntNo = autIntNo;
            model.DocumentType = DocumentType;
            model.AuthorityList = GetAuthoritySelectList();
            model.DocumentTypeList = GetDocumentTypeSelectlist();
            return model;
        }

        [HttpPost]
        public JsonResult SetExpireWholePrintFile(string printFileName, int AutIntNo, 
                                            string DocumentType, string BeginDate, string EndDate, string queryPrintFileName)
        {
            SIL.AARTO.BLL.Utility.UserLoginInfo userDetails = new BLL.Utility.UserLoginInfo();
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            string login = userDetails.UserName;
            int autIntNo = AutIntNo;
            string authCode = "";

            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            try
            {
                #region ExpirePrintFile

                authorityEntity = anthorityService.GetByAutIntNo(autIntNo);

                if (authorityEntity != null)
                {
                    authCode = authorityEntity.AutCode;
                }

                notices = noticeService.GetByAutIntNoNotCiprusPrintReqName(autIntNo, printFileName);

                noticeList = notices.Where(n => (n.NoticeStatus == 510 || n.NoticeStatus == 250) ).ToList();

                string oldPrintFileName = printFileName;
                string newPrintFileName = printFileName.Replace("_PostCanPost", "").Replace("_PostCan", "");
                newPrintFileName = newPrintFileName + "_PostCan";
                oldPfnEntity = pfnSvc.GetByPrintFileName(oldPrintFileName);
                pfnEntity = pfnSvc.GetByPrintFileName(newPrintFileName);

                if (noticeList != null && noticeList.Count > 0)
                {
                    foreach (var item in noticeList)
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                           
                            #region remove notice from OriginalPrintFileName

                            if (oldPfnEntity != null)
                            {
                                oldPfnnEntity = pfnnSvc.GetByNotIntNoPfnIntNo(item.NotIntNo, oldPfnEntity.PfnIntNo);
                                if (oldPfnnEntity != null)
                                {
                                    pfnnSvc.Delete(oldPfnnEntity);
                                }
                            }

                            if (pfnEntity == null)
                            {
                                pfnEntity = new PrintFileName()
                                {
                                    PrintFileName = newPrintFileName,
                                    AutIntNo = autIntNo,
                                    LastUser = LastUser,
                                    EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                                };
                                pfnEntity = pfnSvc.Save(pfnEntity);
                            }

                            pfnnEntity = pfnnSvc.GetByNotIntNoPfnIntNo(item.NotIntNo, pfnEntity.PfnIntNo);
                            if (pfnnEntity == null)
                            {
                                pfnnEntity = new PrintFileNameNotice()
                                {
                                    NotIntNo = item.NotIntNo,
                                    PfnIntNo = pfnEntity.PfnIntNo,
                                    LastUser = LastUser,
                                    EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                                };
                                pfnnEntity = pfnnSvc.Save(pfnnEntity);
                            }

                            #endregion

                            item.NotOriginalPrint1stNoticeDate = item.NotPrint1stNoticeDate;
                            item.NotPrint1stNoticeDate = null;
                            item.NotPrevNoticeStatus = item.NoticeStatus;
                            item.NotCiprusPrintReqName = pfnEntity.PrintFileName;
                            item.LastUser = LastUser;
                            noticeService.Update(item);
                            

                            #region push queue
                            noticeFrameEntity = noticeFrameService.GetByNotIntNo(item.NotIntNo).FirstOrDefault();
                            if (noticeFrameEntity != null)
                            {
                                #region Get existing queue list
                                  _serviceQueueQuery = new ServiceQueueLibrary.DAL.Data.ServiceQueueQuery();
                                  _serviceQueueQuery.Append(ServiceQueueColumn.SeQuKey, noticeFrameEntity.FrameIntNo.ToString());
                                  _serviceQueueQuery.Append(ServiceQueueColumn.SeQuGroup, authCode.Trim());
                                  _serviceQueueQuery.Append(ServiceQueueColumn.SqtIntNo, ((int)ServiceQueueTypeList.CancelExpiredViolations_Frame).ToString());
                                  var serviceQueueList = _serviceQueueService.Find((_serviceQueueQuery as SIL.ServiceQueueLibrary.DAL.Data.IFilterParameterCollection)).ToList();
                                 #endregion

                                if (!(serviceQueueList != null && serviceQueueList.Count > 0))
                                {
                                    queProcessor.Send(
                                        new QueueItem()
                                        {
                                            Body = noticeFrameEntity.FrameIntNo,
                                            Group = authCode.Trim(),
                                            ActDate = DateTime.Now.AddHours(24),
                                            LastUser = LastUser,
                                            QueueType = SIL.ServiceQueueLibrary.DAL.Entities.ServiceQueueTypeList.CancelExpiredViolations_Frame
                                        }
                                    );
                                }
                            }
                            #endregion

                            scope.Complete();

                        }

                    }
                }

                #endregion
                
                #region Set CurrPage Index
                

                //Add by Brian 2013-10-22
                queryPrintFileName = string.IsNullOrEmpty(queryPrintFileName) ? string.Empty : HttpUtility.UrlDecode(queryPrintFileName).Trim();

                DateTime beginDate = DateTime.Now.Date.AddDays(-30);
                DateTime endDate = DateTime.Now.Date.AddDays(1).AddSeconds(-1);
                if (BeginDate != "")
                {
                    beginDate = Convert.ToDateTime(BeginDate);
                }
                if (EndDate != "")
                {
                    endDate = Convert.ToDateTime(EndDate).AddDays(1).AddSeconds(-1);
                }
                int pageSize = Config.PageSize;
                int totalCount = 0;
                noticedb = new NoticeDB(connectionString);

                //DR_NotOffenceDate_NotPosted1stNoticeDate
                dateRule_OffenceDate_1stNoticePostedDate.AutIntNo = autIntNo;
                dateRule_OffenceDate_1stNoticePostedDate.DtRStartDate = "NotOffenceDate";
                dateRule_OffenceDate_1stNoticePostedDate.DtREndDate = "NotPosted1stNoticeDate";
                dateRule_OffenceDate_1stNoticePostedDate.LastUser = LastUser;
                DefaultDateRule_OffenceDate_1stNoticePostedDate = new DefaultDateRules(dateRule_OffenceDate_1stNoticePostedDate, connectionString);
                int noOfDays = DefaultDateRule_OffenceDate_1stNoticePostedDate.SetDefaultDateRule();

                DataSet ds = noticedb.GetExpirePrintFileList(autIntNo, DocumentType, beginDate, endDate, queryPrintFileName,noOfDays, 1, pageSize, out  totalCount);

                int pageCount = totalCount / pageSize;
                int temp = totalCount % pageSize;
                if (temp > 0)
                {
                    pageCount = pageCount + 1;
                }
                int oldCurrpageIndex = (int)Session["currPageIndex_Expire"];
                int currpageIndex = 0;

                if (pageCount == 1 || pageCount == 0)
                {
                    currpageIndex = 0;
                }
                else
                {

                    if (pageCount >= (oldCurrpageIndex + 1))
                    {
                        currpageIndex = oldCurrpageIndex;
                    }
                    else
                    {
                        currpageIndex = oldCurrpageIndex - 1;
                    }
                }

                #endregion


                return Json(new
                {
                    result = 1,
                    pageIndex = new
                    {
                        currpageIndex = currpageIndex
                    }
                });
                
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    result = -1,
                    errorlist = new
                    {
                        errorTip = ex.Message
                    }
                });
            }
        }

        [HttpPost]
        public JsonResult BatchProcessingExpirePrintFile(int AutIntNo,
                                            string DocumentType, string BeginDate, string EndDate, string queryPrintFileName, string PrintFileNameStr)
        {
            SIL.AARTO.BLL.Utility.UserLoginInfo userDetails = new BLL.Utility.UserLoginInfo();
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            string login = userDetails.UserName;
            int autIntNo = AutIntNo;
            string authCode = "";

            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            try
            {
                #region ExpirePrintFile

                authorityEntity = anthorityService.GetByAutIntNo(autIntNo);

                if (authorityEntity != null)
                {
                    authCode = authorityEntity.AutCode;
                }

                if (!string.IsNullOrEmpty(PrintFileNameStr))
                {
                    var printFileNameArr = PrintFileNameStr.Split(',');

                    if (printFileNameArr != null && printFileNameArr.Length > 0)
                    {
                     
                        foreach (var printFileNameitem in printFileNameArr)
                        {

                            #region processing each printfilename

                            notices = noticeService.GetByAutIntNoNotCiprusPrintReqName(autIntNo, printFileNameitem);

                            noticeList = notices.Where(n => (n.NoticeStatus == 510 || n.NoticeStatus == 250)).ToList();

                            string oldPrintFileName = printFileNameitem;
                            string newPrintFileName = printFileNameitem.Replace("_PostCanPost", "").Replace("_PostCan", "");
                            newPrintFileName = newPrintFileName + "_PostCan";
                            oldPfnEntity = pfnSvc.GetByPrintFileName(oldPrintFileName);
                            pfnEntity = pfnSvc.GetByPrintFileName(newPrintFileName);

                            if (noticeList != null && noticeList.Count > 0)
                            {
                                foreach (var item in noticeList)
                                {

                                    using (TransactionScope scope = new TransactionScope())
                                    {
                                       
                                        #region remove notice from OriginalPrintFileName

                                        if (oldPfnEntity != null)
                                        {
                                            oldPfnnEntity = pfnnSvc.GetByNotIntNoPfnIntNo(item.NotIntNo, oldPfnEntity.PfnIntNo);
                                            if (oldPfnnEntity != null)
                                            {
                                                pfnnSvc.Delete(oldPfnnEntity);
                                            }
                                        }


                                        if (pfnEntity == null)
                                        {
                                            pfnEntity = new PrintFileName()
                                            {
                                                PrintFileName = newPrintFileName,
                                                AutIntNo = autIntNo,
                                                LastUser = LastUser,
                                                EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                                            };
                                            pfnEntity = pfnSvc.Save(pfnEntity);
                                        }

                                        pfnnEntity = pfnnSvc.GetByNotIntNoPfnIntNo(item.NotIntNo, pfnEntity.PfnIntNo);
                                        if (pfnnEntity == null)
                                        {
                                            pfnnEntity = new PrintFileNameNotice()
                                            {
                                                NotIntNo = item.NotIntNo,
                                                PfnIntNo = pfnEntity.PfnIntNo,
                                                LastUser = LastUser,
                                                EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                                            };
                                            pfnnEntity = pfnnSvc.Save(pfnnEntity);
                                        }

                                        #endregion

                                        item.NotOriginalPrint1stNoticeDate = item.NotPrint1stNoticeDate;
                                        item.NotPrint1stNoticeDate = null;
                                        item.NotPrevNoticeStatus = item.NoticeStatus;
                                        item.NotCiprusPrintReqName = pfnEntity.PrintFileName;
                                        item.LastUser = LastUser;
                                        noticeService.Update(item);

                                        #region push queue
                                        noticeFrameEntity = noticeFrameService.GetByNotIntNo(item.NotIntNo).FirstOrDefault();
                                        if (noticeFrameEntity != null)
                                        {
                                            #region Get existing queue list
                                            _serviceQueueQuery = new ServiceQueueLibrary.DAL.Data.ServiceQueueQuery();
                                            _serviceQueueQuery.Append(ServiceQueueColumn.SeQuKey, noticeFrameEntity.FrameIntNo.ToString());
                                            _serviceQueueQuery.Append(ServiceQueueColumn.SeQuGroup, authCode.Trim());
                                            _serviceQueueQuery.Append(ServiceQueueColumn.SqtIntNo, ((int)ServiceQueueTypeList.CancelExpiredViolations_Frame).ToString());
                                            var serviceQueueList = _serviceQueueService.Find((_serviceQueueQuery as SIL.ServiceQueueLibrary.DAL.Data.IFilterParameterCollection)).ToList();
                                            #endregion

                                            if (!(serviceQueueList != null && serviceQueueList.Count > 0))
                                            {
                                                queProcessor.Send(
                                                    new QueueItem()
                                                    {
                                                        Body = noticeFrameEntity.FrameIntNo,
                                                        Group = authCode.Trim(),
                                                        ActDate = DateTime.Now.AddHours(24),
                                                        LastUser = LastUser,
                                                        QueueType = SIL.ServiceQueueLibrary.DAL.Entities.ServiceQueueTypeList.CancelExpiredViolations_Frame
                                                    }
                                                );
                                            }
                                    
                                        }
                                        #endregion

                                        scope.Complete();

                                    }

                                }
                            }

                            #endregion

                        }

                    }
                }

                #endregion

                #region Set CurrPage Index


                //Add by Brian 2013-10-22
                queryPrintFileName = string.IsNullOrEmpty(queryPrintFileName) ? string.Empty : HttpUtility.UrlDecode(queryPrintFileName).Trim();

                DateTime beginDate = DateTime.Now.Date.AddDays(-30);
                DateTime endDate = DateTime.Now.Date.AddDays(1).AddSeconds(-1);
                if (BeginDate != "")
                {
                    beginDate = Convert.ToDateTime(BeginDate);
                }
                if (EndDate != "")
                {
                    endDate = Convert.ToDateTime(EndDate).AddDays(1).AddSeconds(-1);
                }
                int pageSize = Config.PageSize;
                int totalCount = 0;
                noticedb = new NoticeDB(connectionString);

                //DR_NotOffenceDate_NotPosted1stNoticeDate
                dateRule_OffenceDate_1stNoticePostedDate.AutIntNo = autIntNo;
                dateRule_OffenceDate_1stNoticePostedDate.DtRStartDate = "NotOffenceDate";
                dateRule_OffenceDate_1stNoticePostedDate.DtREndDate = "NotPosted1stNoticeDate";
                dateRule_OffenceDate_1stNoticePostedDate.LastUser = LastUser;
                DefaultDateRule_OffenceDate_1stNoticePostedDate = new DefaultDateRules(dateRule_OffenceDate_1stNoticePostedDate, connectionString);
                int noOfDays = DefaultDateRule_OffenceDate_1stNoticePostedDate.SetDefaultDateRule();

                DataSet ds = noticedb.GetExpirePrintFileList(autIntNo, DocumentType, beginDate, endDate, queryPrintFileName,noOfDays, 1, pageSize, out  totalCount);

                int pageCount = totalCount / pageSize;
                int temp = totalCount % pageSize;
                if (temp > 0)
                {
                    pageCount = pageCount + 1;
                }
                int oldCurrpageIndex = (int)Session["currPageIndex_Expire"];
                int currpageIndex = 0;

                if (pageCount == 1 || pageCount == 0)
                {
                    currpageIndex = 0;
                }
                else
                {

                    if (pageCount >= (oldCurrpageIndex + 1))
                    {
                        currpageIndex = oldCurrpageIndex;
                    }
                    else
                    {
                        currpageIndex = oldCurrpageIndex - 1;
                    }
                }

                #endregion


                return Json(new
                {
                    result = 1,
                    pageIndex = new
                    {
                        currpageIndex = currpageIndex
                    }
                });

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    result = -1,
                    errorlist = new
                    {
                        errorTip = ex.Message
                    }
                });
            }
        }

        public List<SelectListItem> GetAuthoritySelectList()
        {
            var lookups = AuthorityLookupCache.GetCacheLookupValue(this.cultureName);
            var authorityList = anthorityService.GetAll()
                .Where(a => lookups.Keys.Contains(a.AutIntNo))
                .OrderBy(a => a.AutName)
                .Select(a => new SelectListItem
                {
                    Text = lookups[a.AutIntNo],
                    Value = Convert.ToString(a.AutIntNo),
                    Selected = a.AutIntNo == AutIntNo
                }).ToList();

            return authorityList;
        }

        public List<SelectListItem> GetDocumentTypeSelectlist()
        {
            List<SelectListItem> itemList = new List<SelectListItem>();

            itemList.Add(new SelectListItem { Text = "SPD", Value = "SPD" });
            itemList.Add(new SelectListItem { Text = "RLV", Value = "RLV" });
            itemList.Add(new SelectListItem { Text = "ASD", Value = "ASD" });
            itemList.Add(new SelectListItem { Text = "NAG", Value = "NAG" });
            itemList.Add(new SelectListItem { Text = "BUS", Value = "BUS" });
            itemList.Add(new SelectListItem { Text = "VID", Value = "VID" });

            return itemList;
           
        }

    }
}
