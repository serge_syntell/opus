﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Representation.Model;
using SIL.AARTO.BLL.SummonsManagement;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.ViewModels.SummonsManagement;
using Stalberg.TMS;
using RES = SIL.AARTO.Web.Resource.SummonsMangement;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;

namespace SIL.AARTO.Web.Controllers.SummonsManagement
{
    public class SummonsManagementController : Controller
    {
        //
        // GET: /StagnantSummons/
        static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
        static int daysOfExpirySummons = 0;
        public int AutIntNo
        {
            get
            {
                if (Session["autIntNo"] != null)
                {
                    return Convert.ToInt32(Session["autIntNo"]);
                }
                else
                {
                    return 0;
                }
            }
        }
        public UserLoginInfo UserInfo
        {
            get
            {
                if (Session["UserLoginInfo"] != null)
                {
                    return (UserLoginInfo)Session["UserLoginInfo"];
                }
                else
                {
                    return null;
                }
            }
        }

        public ActionResult StagnantSummons()
        {
            return View(InitialModel());
        }


        [HttpPost]
        public ActionResult Search(string notTicketNo, string summonsNo, int crtIntNo, int crtRIntNo, string startDate, string endDate)
        {
            var result = new MessageResult() { IsSuccessful = false };

            DateTime? startCDate = !string.IsNullOrEmpty(startDate) ? (DateTime?)Convert.ToDateTime(startDate) : null;
            DateTime? endCDate = !string.IsNullOrEmpty(endDate) ? (DateTime?)Convert.ToDateTime(endDate) : null;

            if (UserInfo == null)
            {
                result.AddError(RES.StagnantSummons.msg_Error_13);
                return Json(result);
            }
            if (startCDate >= endCDate)
            {
                result.AddError(RES.StagnantSummons.msg_CourtDateInvalid);
                return Json(result);
            }
            try
            {
                StagnantSummonsManager mgmt = new StagnantSummonsManager(connectionString);

                daysOfExpirySummons = GetExpiryDaysForSummons(AutIntNo, UserInfo.UserName);
                if (daysOfExpirySummons == 0)
                {
                    result.AddError(RES.StagnantSummons.msg_Error_14);
                    return Json(result);
                }
                //This rule should be able to deal with the issue of expiring unserved summons after 18 months (+ 2 weeks) 
                daysOfExpirySummons += 14;
                int rowCount = mgmt.GetStagnantSummonsCount(notTicketNo, summonsNo, crtIntNo, crtRIntNo, startCDate, endCDate, daysOfExpirySummons);
                result.Body = rowCount;
                if (rowCount == 0)
                {
                    result.AddError(RES.StagnantSummons.msg_NoDataFound);
                }
                else
                {
                    result.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                result.AddError(ex.Message);
            }
            //SP: SearchStagnantSummons

            return Json(result);
        }

        [HttpPost]
        public ActionResult WithdrawnStagnantSummonsForReIssue(string notTicketNo, string summonsNo, int crtIntNo,
            int crtRIntNo, string startDate, string endDate, int maxProcessNumber)
        {
            var result = new MessageResult() { IsSuccessful = false };

            if (UserInfo == null)
            {
                result.AddError(RES.StagnantSummons.msg_Error_13);
                return Json(result);
            }

            result = ProcessWithdrawnForReissueStagnantSummons(notTicketNo, summonsNo, crtIntNo,
                crtRIntNo, startDate, endDate, maxProcessNumber, UserInfo.UserName);

            return Json(result);
        }


        MessageResult ProcessWithdrawnForReissueStagnantSummons(string notTicketNo, string summonsNo, int crtIntNo,
            int crtRIntNo, string startDate, string endDate, int maxProcessNumber, string lastUser)
        {
            var result = new MessageResult() { IsSuccessful = false };

            int returnValue = 0;
            int processedCount = 0;
            int notIntNo = 0, sumIntNo = 0;
            string autCode = string.Empty, notFilmType = string.Empty, crtName = string.Empty;
            DateTime offenceDate = DateTime.Now;
            DateTime? startCDate = !string.IsNullOrEmpty(startDate) ? (DateTime?)Convert.ToDateTime(startDate) : null;
            DateTime? endCDate = !string.IsNullOrEmpty(endDate) ? (DateTime?)Convert.ToDateTime(endDate) : null;

            StagnantSummonsManager mgmt = new StagnantSummonsManager(connectionString);
            QueueItemProcessor queueProcessor = new QueueItemProcessor();


            DateRulesDetails dateRuleIssueSum = new DateRulesDetails()
            {
                AutIntNo = AutIntNo,
                DtRStartDate = "DataWashDate",
                DtREndDate = "NotIssueSummonDate",
                LastUser = lastUser
            };

            try
            {
                DefaultDateRules ruleIssueSum = new DefaultDateRules(dateRuleIssueSum, connectionString);
                int noOfDaysToSummons = ruleIssueSum.SetDefaultDateRule();

                daysOfExpirySummons = GetExpiryDaysForSummons(AutIntNo, UserInfo.UserName);
                if (daysOfExpirySummons == 0)
                {
                    result.AddError(RES.StagnantSummons.msg_Error_14);
                    return result;
                }
                //This rule should be able to deal with the issue of expiring unserved summons after 18 months (+ 2 weeks) 
                //daysOfExpirySummons += 14;

                DataTable dt = mgmt.GetStagnantSummons(notTicketNo, summonsNo, crtIntNo, crtRIntNo, startCDate, endCDate, maxProcessNumber, daysOfExpirySummons);
                foreach (DataRow dr in dt.Rows)
                {
                    using (TransactionScope tran = new TransactionScope())
                    {
                        notIntNo = Convert.ToInt32(dr["NotIntNo"]);
                        sumIntNo = Convert.ToInt32(dr["SumIntNo"]);
                        autCode = dr["AutCode"].ToString();
                        notFilmType = dr["NotFilmType"].ToString();
                        crtName = dr["CrtName"].ToString();
                        offenceDate = Convert.ToDateTime(dr["NotOffenceDate"]);

                        returnValue = mgmt.WithDrawForReIssueSummons(notIntNo, sumIntNo, daysOfExpirySummons, lastUser);

                        var group = new[] { autCode.Trim(), notFilmType, crtName };

                        if (returnValue > 0)
                        {
                            queueProcessor.Send(new QueueItem()
                            {
                                Body = notIntNo,
                                Group = string.Join("|", group),
                                ActDate = DateTime.Now.AddDays(noOfDaysToSummons),
                                Priority = offenceDate.Subtract(DateTime.Now).Days,
                                QueueType = ServiceQueueTypeList.GenerateSummons,
                                LastUser = lastUser
                            },
                            new QueueItem()
                            {
                                Body = notIntNo,
                                Group = autCode,
                                ActDate = DateTime.Now,
                                QueueType = ServiceQueueTypeList.DataWashingExtractor,
                                LastUser = lastUser
                            });

                            tran.Complete();

                            processedCount += 1;
                        }
                        else
                        {
                            ProcessErrorMessage(result, returnValue, dr["SummonsNo"].ToString());
                            break;
                        }
                    }
                }

                if (processedCount == dt.Rows.Count)
                {
                    result.IsSuccessful = true;
                    result.AddMessage(string.Format(RES.StagnantSummons.msg_Infor_01, processedCount));
                }
                else {
                    if (processedCount > 0) {
                        result.AddMessage(string.Format(RES.StagnantSummons.msg_Error_01, processedCount));
                    }
                }
            }
            catch (Exception ex)
            {
                result.AddError(ex.Message);
                return result;
            }

            return result;
        }

        void ProcessErrorMessage(MessageResult result, int returnValue, string summons)
        {
            string message = string.Format(RES.StagnantSummons.msg_Error_15, summons);
            switch (returnValue)
            {
                case -1:
                    result.AddError(RES.StagnantSummons.msg_Error_02 + message);
                    break;
                case -2:
                    result.AddError(RES.StagnantSummons.msg_Error_03 + message);
                    break;
                case -3:
                    result.AddError(RES.StagnantSummons.msg_Error_04 + message);
                    break;
                case -4:
                    result.AddError(RES.StagnantSummons.msg_Error_05 + message);
                    break;
                case -5:
                    result.AddError(RES.StagnantSummons.msg_Error_06 + message);
                    break;
                case -6:
                    result.AddError(RES.StagnantSummons.msg_Error_07 + message);
                    break;
                case -7:
                    result.AddError(RES.StagnantSummons.msg_Error_08 + message);
                    break;
                case -8:
                    result.AddError(RES.StagnantSummons.msg_Error_09 + message);
                    break;
                case -9:
                    result.AddError(RES.StagnantSummons.msg_Error_10 + message);
                    break;
                case -10:
                    result.AddError(RES.StagnantSummons.msg_Error_11 + message);
                    break;
                default:
                    result.AddError(RES.StagnantSummons.msg_Error_12 + message);
                    break;
            }
        }

        StagnantSummonsModel InitialModel()
        {

            int autIntNo = Session["autIntNo"] == null ? 0 : Convert.ToInt32(Session["autIntNo"]);

            StagnantSummonsModel model = new StagnantSummonsModel();

            List<Court> courts = new List<Court>();
            List<CourtRoom> courtRooms = new List<CourtRoom>();

            CourtDB db = new CourtDB(connectionString);
            SqlDataReader reader = db.GetAuth_CourtListByAuth(autIntNo);
            while (reader.Read())
            {
                Court court = new Court();
                court.CrtIntNo = Convert.ToInt32(reader["CrtIntNo"].ToString());
                court.CrtName = reader["CrtDetails"].ToString();
                courts.Add(court);
            }
            reader.Close();
            reader.Dispose();//Jerry 2014-10-24 add

            courts.Insert(0, new Court() { CrtIntNo = 0, CrtName = RES.StagnantSummons.msg_SelectCourt });
            model.Courts = new SelectList(courts as System.Collections.IEnumerable, "CrtIntNo", "CrtName");

            courtRooms.Insert(0, new CourtRoom() { CrtRintNo = 0, CrtRoomNo = RES.StagnantSummons.msg_SelectCourtRoom });
            model.CourtRooms = new SelectList(courtRooms, "CrtrIntNo", "CrtRoomNo", model.CrtRIntNo);

            return model;
        }

        public int GetExpiryDaysForSummons(int autIntNo, string lastUser)
        {
            int daysExpirySummons = 0;
            DateRulesDetails drExpirySummonsDays = new DateRulesDetails();
            drExpirySummonsDays.AutIntNo = autIntNo;
            drExpirySummonsDays.DtRStartDate = "NotOffenceDate";
            drExpirySummonsDays.DtREndDate = "SumExpireDate";
            drExpirySummonsDays.LastUser = lastUser;

            DefaultDateRules dfDateRule = new DefaultDateRules(drExpirySummonsDays, connectionString);
            daysExpirySummons = dfDateRule.SetDefaultDateRule();

            return daysExpirySummons;
        }
    }
}
