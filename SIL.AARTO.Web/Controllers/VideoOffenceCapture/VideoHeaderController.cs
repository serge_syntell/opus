﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.VideoOffenceCapture;
using SIL.AARTO.BLL.VideoOffenceCapture;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource.VideoOffenceCapture;
using System.Threading;
using System.Configuration;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.VideoOffenceCapture
{
    public partial class VideoOffenceCaptureController : Controller
    {
        public ActionResult ShowHeader(string errorMsg, int autIntNo = 0)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            Session["HeaderMsg"] = null;
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            HeaderModel model = new HeaderModel();
            InitModel(model, userInfo, autIntNo);
            model.ErrorMsg = errorMsg;
            return View("Header", model);
        }

        public ActionResult SaveHeader(HeaderModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            string autCode = (string)Session["VHC_AutCode"];
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];

            model.StartTime = string.Format("{0} {1}", model.Date, model.StartTime);
            model.EndTime = string.Format("{0} {1}", model.Date, model.EndTime);

            int autIntNo = (int)Session["VHC_AutIntNo"];

             //2013-12-12 Heidi added for check Is Railway Crossing Location(5149)
            if (model.IsRailwayCrossing && model.RailwayLocation != null)
            {
                model.Court = model.CrtIntNo;
                model.LocationSuburb = model.LoSuIntNo.ToString();
            }

            if (DateTime.Parse(model.Date).Date >= DateTime.Now.Date.AddDays(1))
            {
                model.ErrorMsg = VideoHeaderController.ErrorMsgOffDate;
                InitModel(model, userInfo, autIntNo);
                return View("Header", model);
            }

           
            //2013-12-12 Heidi added for check Is Railway Crossing Location(5149)
            if (model.IsRailwayCrossing && model.RailwayLocation != null)
            {
                LocationService locationService = new LocationService();
                int locIntNo = 0;
                int.TryParse(model.RailwayLocation, out locIntNo);
                
                Location locationEntity = locationService.GetByLocIntNo(locIntNo);
                if (locationEntity != null)
                {
                    model.Location = locationEntity.LocDescr;
                    model.LocIntNo = locationEntity.LocIntNo;
                }
                CourtService courtService = new CourtService();
                Court courtEntity = courtService.GetByCrtIntNo(model.CrtIntNo);
                if (courtEntity != null)
                {
                    model.CrtNo = courtEntity.CrtNo;
                }
            }

            long vchIntNo;
            int returnInt;
            

            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            int _autIntNo = Session["VHC_AutIntNo"] == null ? AutIntNo : (int)Session["VHC_AutIntNo"];
			try
            {

                if (model.VCHIntNo > 0)
                {
                    vchIntNo = model.VCHIntNo;
                    returnInt = VideoCaptureHeaderManager.Update(GetVideoCaptureHeader(model, autCode, userInfo.UserName));
                }
                else
                {
                    returnInt = VideoCaptureHeaderManager.Add(GetVideoCaptureHeader(model, autCode, userInfo.UserName), out vchIntNo);
                }
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(_autIntNo, LastUser, PunchStatisticsTranTypeList.VideoOffenceCapture, PunchAction.Change);
            }
            catch (Exception ex)
            {
                model.ErrorMsg = ex.Message;
                InitModel(model, userInfo, autIntNo);
                return View("Header", model);
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(_autIntNo, LastUser, PunchStatisticsTranTypeList.VideoOffenceCapture, PunchAction.Add);
            }

            if (returnInt == -1)
            {
                model.ErrorMsg = VideoHeaderController.ErrorMsg1;
                InitModel(model, userInfo, autIntNo);
                return View("Header", model);
            }
            return RedirectToAction("ShowDetail", new { vchIntNo = vchIntNo });
        }

        //2013-12-19 Heidi added for check Is Railway Crossing Location(5149)
        private bool CheckRailwayLocation(string locIntNo, string loSuIntNo, int crtIntNo, int autIntNo)
        {
            bool flag = false;

            LocationService locationService = new LocationService();
            AuthCourtService authCourtService = new AuthCourtService();
            int _locIntNo = 0;
            int locationLoSuIntNo = 0;
            int locationAcIntNo = 0;
            int authCourtAcIntNo = 0;
            int.TryParse(locIntNo, out _locIntNo);
            Location locationEntity = locationService.GetByLocIntNo(_locIntNo);
            if (locationEntity != null)
            {
                locationLoSuIntNo = locationEntity.LoSuIntNo;
                locationAcIntNo = locationEntity.AcIntNo;
                AuthCourt authCourtEntity = authCourtService.GetByAutIntNoCrtIntNo(autIntNo, crtIntNo);
                if (authCourtEntity != null)
                {
                    authCourtAcIntNo = authCourtEntity.AcIntNo;
                }

                if (locationEntity.IsRailwayCrossing.HasValue
                    && Convert.ToBoolean(locationEntity.IsRailwayCrossing)
                    && locationLoSuIntNo.ToString() == loSuIntNo
                    && locationAcIntNo == authCourtAcIntNo)
                {
                    flag = true;
                }
            }
            return flag;
        }

        public JsonResult SelectOffence(int offIntNo, string offDate)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
            Offence offence = OffenceManager.GetByOffIntNo(offIntNo);
            float fine = OffenceManager.GetFineByOffIntNoAndAutIntNo(offIntNo, offDate, (int)Session["VHC_AutIntNo"]);

            return Json(new { result = 0, offCode = offence.OffCode, fine = fine });
        }

        //2013-12-16 Heidi added  IsRailwayCrossing for check Location is Railway Crossing(5149)
        public JsonResult GetCrtIntNoAndLoSuIntNoByLocIntNo(int LocIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
            string strLoSuIntNoCrtIntNo = SIL.AARTO.BLL.VideoOffenceCapture.LocationManager.GetCrtIntNoAndLoSuIntNoByLocIntNo((int)Session["VHC_AutIntNo"], LocIntNo);
            return Json(new { result = 0, StrLoSuIntNoCrtIntNo = strLoSuIntNoCrtIntNo });
        }

        public JsonResult SelectOfficer(int toIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
            TrafficOfficer officer = TrafficOfficerManager.GetOfficerByTOIntNo(toIntNo);

            return Json(new { result = 0, offcerName = officer.TosName, offcerCode = officer.ToNo });
        }

        public JsonResult SelectCourt(int crtIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            Court court = SIL.AARTO.BLL.CameraManagement.CourtManager.GetByCrtIntNo(crtIntNo);
            return Json(new { result = 0, crtNo = court.CrtNo });
        }

        //2013-12-13 Heidi commented out for the method is not used(5149)
        //public JsonResult SelectLocationSuburb(string LocationSuburbValue)
        //{
        //    if (Session["UserLoginInfo"] == null)
        //    {
        //        return Json(new { result = -2 });
        //    }
        //    int loSuIntNo = 0;
        //    //Location loc = SIL.AARTO.BLL.VideoOffenceCapture.LocationManager.GetByLocIntNo(locIntNo);
        //    if (LocationSuburbValue.Trim().Length > 0)
        //    {

        //        if (LocationSuburbValue.Trim().Contains("~"))
        //        {
        //            string[] tempArr = LocationSuburbValue.Trim().Split('~');
        //            if (tempArr.Length > 0)
        //            {
        //                int.TryParse(tempArr[0], out loSuIntNo);
        //            }
        //        }
        //    }
        //    return Json(new { result = 0, LoSuIntNo = loSuIntNo });
        //}

        public ActionResult GetReport(int vchIntNo)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + @"Reports\VideoOffenceCaptureReport.dplx";
            //PunchStats805806 enquiry VideoOffenceCapture
            return File(OffenceReport.GetPdfByte(path, vchIntNo), "application/pdf");
        }


        private void InitModel(HeaderModel model, UserLoginInfo userInfo, int autIntNo)
        {
            VideoCaptureHeader vcHeader;
            int vhcAutIntNo = autIntNo == 0 ? userInfo.AuthIntNo : autIntNo;
            if (autIntNo != 0)
            {
                vcHeader = VideoCaptureHeaderManager.GetNoCompleteData(userInfo.UserName, autIntNo);
            }
            else
            {
                vcHeader = VideoCaptureHeaderManager.GetNoCompleteData(userInfo.UserName);
            }

            if (vcHeader != null)
            {
                vhcAutIntNo = vcHeader.AutIntNo;
            }

            model.Authorities = AuthorityManager.GetSelectList();
            Session["VHC_AutIntNo"] = model.Authority = vhcAutIntNo;

            string autCode = AuthorityManager.GetAuthCodeByAutIntNo(vhcAutIntNo).Trim();
            Session["VHC_AutCode"] = autCode;

            model.CapturedBy = userInfo.UserName;
            model.CapturedDate = DateTime.Now.ToString("yyyy-MM-dd");

            string lsCode = Thread.CurrentThread.CurrentCulture.Name;
            model.Offences = VideoCaptureHeaderManager.GetOffencesWithCode(lsCode);
            model.Officers = VideoCaptureHeaderManager.GetOfficers(vhcAutIntNo);
            model.Courts = SIL.AARTO.BLL.CameraManagement.CourtManager.GetSelectList(vhcAutIntNo);
            model.LocationSuburbs = SIL.AARTO.BLL.VideoOffenceCapture.LocationSuburbManager.GetSelectList(vhcAutIntNo);
            //2013-12-12 Heidi added for Get Railway Locations DDL by AutIntNo (5149)
            model.RailwayLocations = SIL.AARTO.BLL.VideoOffenceCapture.LocationManager.GetSelectRailwayLocationsList(vhcAutIntNo);
            // model.Locations = SIL.AARTO.BLL.VideoOffenceCapture.LocationManager.GetSelectList(vhcAutIntNo);
            model.NSTypes = NoticeStatisticalTypeManager.GetSelectListByGroup("V");
            model.Language = lsCode;

            if (vcHeader != null)
            {
                model.VCHIntNo = vcHeader.VchIntNo;
                string tapeNo = vcHeader.VchTapeNo;
                model.TapeCassetteNo = GetPartFilmNoOfInput(tapeNo, autCode);
                model.Offence = vcHeader.OffIntNo;
                model.Officer = vcHeader.ToIntNo;
                model.Court = vcHeader.CrtIntNo;
                model.NSType = vcHeader.NstIntNo;

                model.CrtNo = vcHeader.CrtNo;
                model.OfficerCode = vcHeader.ToNo;
                model.OffenceCode = vcHeader.OffCode;

                model.Date = vcHeader.VchOffenceDate.ToString("yyyy-MM-dd");

                model.Location = vcHeader.VchLocation;
                model.LocationSuburb = vcHeader.LoSuIntNo.ToString();


                model.OffenceCode = OffenceManager.GetByOffIntNo(vcHeader.OffIntNo).OffCode;
                model.Fine = OffenceManager.GetFineByOffIntNoAndAutIntNo(vcHeader.OffIntNo, vcHeader.VchOffenceDate.ToString("yyyy-MM-dd"), vhcAutIntNo);
                model.OfficerName = TrafficOfficerManager.GetOfficerByTOIntNo(vcHeader.ToIntNo).TosName;
                model.StartTime = vcHeader.VchStartTime.ToString("HH:mm");
                model.EndTime = vcHeader.VchEndTime.ToString("HH:mm");
                model.CompiledBy = vcHeader.VchCompiledName;
                model.CompiledDate = vcHeader.VchCompiledDate.ToString("yyyy-MM-dd");
                model.CheckedBy = vcHeader.VchCheckedName;
                model.CheckedDate = vcHeader.VchCheckedDate.ToString("yyyy-MM-dd");
                //2013-12-16 Heidi added  IsRailwayCrossing for check Location is Railway Crossing(5149)
                if (vcHeader.IsRailwayCrossing != null)
                {
                    model.IsRailwayCrossing = Convert.ToBoolean(vcHeader.IsRailwayCrossing);
                }
                else
                {
                    model.IsRailwayCrossing = false;
                }

                if (vcHeader.IsRailwayCrossing.HasValue)
                {
                    if (Convert.ToBoolean(vcHeader.IsRailwayCrossing))
                    {
                        LocationService locationService = new LocationService();
                        int _locIntno = vcHeader.LocIntNo.HasValue ? Convert.ToInt32(vcHeader.LocIntNo) : 0;
                        Location locationEntity = locationService.GetByLocIntNo(_locIntno);
                        if (locationEntity != null)
                        {
                            model.OldLocIntNo = _locIntno;
                            model.Location = locationEntity.LocDescr;
                            //model.OldLocIntNo = SIL.AARTO.BLL.VideoOffenceCapture.LocationManager.GetLocationByLocDescrAndIsRailwayCrossing(vhcAutIntNo, model.Location);
                        }
                        

                    }
                }

            }
        }

        private VideoCaptureHeader GetVideoCaptureHeader(HeaderModel model, string autCode, string userName)
        {
            #region old code
            //2013-12-20 Heidi changed for check Railway Crossing on Location
            //return new VideoCaptureHeader()
            //{
            //    VchIntNo = model.VCHIntNo,
            //    AutIntNo = model.Authority,
            //    VchTapeNo = string.Format("VID-{0}_{1}", autCode.Trim(), model.TapeCassetteNo),
            //    VchOffenceDate = DateTime.Parse(model.Date),
            //    VchLocation = model.Location,
            //    LoSuIntNo = GetLoSuIntNoByLocationSuburb(model.LocationSuburb),
            //    CrtIntNo = model.Court,
            //    OffIntNo = model.Offence,
            //    VchFineAmount = model.Fine,
            //    ToIntNo = model.Officer,
            //    VchStartTime = DateTime.Parse(model.StartTime),
            //    VchEndTime = DateTime.Parse(model.EndTime),
            //    VchCompiledName = model.CompiledBy,
            //    VchCompiledDate = DateTime.Parse(model.CompiledDate),
            //    VchCheckedName = model.CheckedBy,
            //    VchCheckedDate = DateTime.Parse(model.CheckedDate),
            //    VchCaptureUserName = model.CapturedBy,
            //    VchCaptureDate = DateTime.Parse(model.CapturedDate),
            //    LastUser = userName,
            //    OffCode = model.OffenceCode,
            //    ToNo = model.OfficerCode,
            //    CrtNo = model.CrtNo,
            //    NstIntNo = model.NSType
            //};
            #endregion
            VideoCaptureHeader videoCaptureHeaderEntity = new VideoCaptureHeader();
            videoCaptureHeaderEntity.VchIntNo = model.VCHIntNo;
            videoCaptureHeaderEntity.AutIntNo = model.Authority;
            videoCaptureHeaderEntity.VchTapeNo = string.Format("VID-{0}_{1}", autCode.Trim(), model.TapeCassetteNo);
            videoCaptureHeaderEntity.VchOffenceDate = DateTime.Parse(model.Date);
            videoCaptureHeaderEntity.VchLocation = model.Location;
            videoCaptureHeaderEntity.LoSuIntNo = model.LoSuIntNo;//2013-12-20 Heidi changed for check Railway Crossing on Location //GetLoSuIntNoByLocationSuburb(model.LocationSuburb),
            videoCaptureHeaderEntity.CrtIntNo = model.CrtIntNo;//2013-12-20 Heidi changed for check Railway Crossing on Location// model.Court,
            videoCaptureHeaderEntity.OffIntNo = model.Offence;
            videoCaptureHeaderEntity.VchFineAmount = model.Fine;
            videoCaptureHeaderEntity.ToIntNo = model.Officer;
            videoCaptureHeaderEntity.VchStartTime = DateTime.Parse(model.StartTime);
            videoCaptureHeaderEntity.VchEndTime = DateTime.Parse(model.EndTime);
            videoCaptureHeaderEntity.VchCompiledName = model.CompiledBy;
            videoCaptureHeaderEntity.VchCompiledDate = DateTime.Parse(model.CompiledDate);
            videoCaptureHeaderEntity.VchCheckedName = model.CheckedBy;
            videoCaptureHeaderEntity.VchCheckedDate = DateTime.Parse(model.CheckedDate);
            videoCaptureHeaderEntity.VchCaptureUserName = model.CapturedBy;
            videoCaptureHeaderEntity.VchCaptureDate = DateTime.Parse(model.CapturedDate);
            videoCaptureHeaderEntity.LastUser = userName;
            videoCaptureHeaderEntity.OffCode = model.OffenceCode;
            videoCaptureHeaderEntity.ToNo = model.OfficerCode;
            videoCaptureHeaderEntity.CrtNo = model.CrtNo;
            videoCaptureHeaderEntity.NstIntNo = model.NSType;
            //2013-12-16 Heidi added  IsRailwayCrossing and LocIntNo for check Location is Railway Crossing(5149)
            videoCaptureHeaderEntity.IsRailwayCrossing = model.IsRailwayCrossing;
            if (model.IsRailwayCrossing)
            {
                videoCaptureHeaderEntity.LocIntNo = model.LocIntNo;
            }
            return videoCaptureHeaderEntity;

        }

        private string GetPartFilmNoOfInput(string filmNo, string autCode)
        {
            return filmNo.Substring(4, filmNo.Length - 4).Replace(autCode.Trim(), "").TrimStart('_');
        }

        private int GetLoSuIntNoByLocationSuburb(string LocationSuburbValue)
        {
            int loSuIntNo = 0;

            int.TryParse(LocationSuburbValue.Trim(), out loSuIntNo);


            return loSuIntNo;

        }
    }
}
