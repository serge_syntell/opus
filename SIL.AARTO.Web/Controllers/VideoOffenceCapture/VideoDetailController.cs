﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.ViewModels.VideoOffenceCapture;
using SIL.AARTO.BLL.VideoOffenceCapture;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.CameraManagement;
using Stalberg.TMS;
using SIL.AARTO.Web.Resource.VideoOffenceCapture;
using Stalberg.TMS.Data;
using System.Transactions;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.BLL.Culture;
using System.Threading;
using SIL.AARTO.BLL.Model;
using System.Configuration;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.VideoOffenceCapture
{
    [AARTOErrorLog, LanguageFilter]
    public partial class VideoOffenceCaptureController : Controller
    {
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

        public ActionResult ShowDetail(long vchIntNo, string errorMsg, bool isEndShow = false)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            DetailModel model = new DetailModel();
            InitModel(model, vchIntNo);
            model.ErrorMsg = errorMsg;
            ViewBag.IsEndShow = isEndShow;
            return View("Detail", model);
        }

        public ActionResult SaveDetail(DetailModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            //Heidi 2014-10-16 added for fixing AARTOErrorLog shows issue.(5367)
            if (model.VmCode == null || model.VtCode == null || model.VcCode==null)
            {
                return RedirectToAction("ShowDetail", new { vchIntNo = model.VchIntNo, errorMsg = VideoDetailController.ErrorMsg4 });
            }

            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            HeaderMsg header = (HeaderMsg)Session["HeaderMsg"];

            model.OffTime = string.Format("{0} {1}", header.Date, model.OffTime);

            int retInt = 0;

            if (model.VcdIntNo > 0)
            {
                VideoCaptureDetailManager.Update(GetDetail(model, userInfo.UserName));
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.VideoOffenceCapture, PunchAction.Change);

            }
            else
            {
                if (VideoCaptureDetailManager.GetByVchIntNo(model.VchIntNo).Count >= 30)
                {
                    retInt = -2;
                }
                else
                {
                    VideoCaptureDetailManager.Save(GetDetail(model, userInfo.UserName));
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.VideoOffenceCapture, PunchAction.Add);
                }
            }


            string errorMsg = string.Empty;
            switch (retInt)
            {
                case -2:
                    errorMsg = VideoDetailController.ErrorMsg2;
                    break;
                default:
                    break;
            }

            return RedirectToAction("ShowDetail", new { vchIntNo = model.VchIntNo, errorMsg = errorMsg });
        }

        public ActionResult DeleteDetail(long vcdIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }//2013-12-09 Heidi added for check user login(5084)

            long vchIntNo;
            VideoCaptureDetailManager.Delete(vcdIntNo, out vchIntNo);
           
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.VideoOffenceCapture, PunchAction.Delete);
            return RedirectToAction("ShowDetail", new { vchIntNo = vchIntNo });
        }

        public JsonResult GetDetail(long vcdIntNo)
        {
            VideoCaptureDetail offenceDetail = VideoCaptureDetailManager.GetByVcdIntNo(vcdIntNo);
            return Json(new
            {
                result = 0,
                time = offenceDetail.VcdTime.ToString("hh:mm"),
                regNo = offenceDetail.VcdRegNo,
                color = offenceDetail.VcIntNo,
                maker = offenceDetail.VmIntNo,
                type = offenceDetail.VtIntNo,
                vcCode = offenceDetail.VcCode,
                vmCode = offenceDetail.VmCode,
                vtCode = offenceDetail.VtCode,
                ticktNo = offenceDetail.VtNoticeTicketNo
            });
        }

        public JsonResult SelectVehicleColour(int vcIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            VehicleColour colour = VehicleColourManager.GetByVcIntNo(vcIntNo);
            return Json(new { result = 0, vcCode = colour.VcCode });
        }

        public JsonResult SelectVehicleMake(int vmIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            VehicleMakeDetails colour = VehicleMakeManager.GetByVmIntNo(vmIntNo);
            return Json(new { result = 0, vmCode = colour.VMCode });
        }

        public JsonResult SelectVehicleType(int vtIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            VehicleTypeDetails colour = VehicleTypeManager.GetByVtIntNo(vtIntNo);
            return Json(new { result = 0, vtCode = colour.VTCode });
        }

        public JsonResult FinishAddOffence(long vchIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            VideoCaptureHeader header = VideoCaptureHeaderManager.GetByVchIntNo(vchIntNo);
            TList<VideoCaptureDetail> details = VideoCaptureDetailManager.GetByVchIntNo(vchIntNo);
            TrafficOfficer officer = TrafficOfficerManager.GetOfficerByTOIntNo(header.ToIntNo);
            OfficerGroup ofGroup = SIL.AARTO.BLL.Admin.OfficerGroupManager.GetOfficerGroupByOfGrIntNo(officer.OfGrIntNo);
            Offence off = OffenceManager.GetByOffIntNo(header.OffIntNo);
            NoticeStatisticalType nsType = NoticeStatisticalTypeManager.GetByNstIntNo(header.NstIntNo);
            //Jerry 2012-10-24 change
            string constr = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString();
            ImportHandwrittenOffences handWrite = new ImportHandwrittenOffences(constr);
            DateRuleInfo dateRule = new DateRuleInfo();
            NoticeFrame notFrame = new NoticeFrame();
            int autIntNo = (int)Session["VHC_AutIntNo"];
            string autCode = ((string)Session["VHC_AutCode"]).Trim();
            
            string frameNo = "0000";
            int countDetail = 1;

          


            string errorMsg = string.Empty;
            using (TransactionScope scope = new TransactionScope())
            {
                string vtNoticeTicketNo = string.Empty;
                try
                {
                    //2013-05-21 Heidi Added for locationSuburb

                    //if (header.VchLocation.Trim().Length > 0)
                    //{
                    //    if (header.VchLocation.Trim().Contains("~"))
                    //    {
                    //        string[] tempArr = header.VchLocation.Trim().Split('~');

                    //    }
                    //}
                    NoticeNumbers noticeNumbers = new NoticeNumbers(Config.ConnectionString);
                    int returnIntNo = 0;
                    DateRuleInfo dateRule1stNotice = dateRule.GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "NotIssue1stNoticeDate");
                    DateRuleInfo dateRuleNotExpire = dateRule.GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "NotExpireDate");
                    DateRuleInfo notPaymentDate = dateRule.GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "NotPaymentDate");
                    foreach (var item in details)
                    {
                        //vtNoticeTicketNo = noticeNumbers.GenerateNoticeNumber(userInfo.UserName, "VID", autIntNo).NotTicketNo;
                        // 2015-02-26, Oscar changed. (1868, ref 1866)
                        var ticket = noticeNumbers.GenerateNoticeNumber(userInfo.UserName, "VID", autIntNo);
                        vtNoticeTicketNo = ticket.NotTicketNo;

                        // Heidi 2014-06-11 Remove extra Spaces for fixing Performance issues(5299)
                        vtNoticeTicketNo = vtNoticeTicketNo == null ? vtNoticeTicketNo : vtNoticeTicketNo.Trim();

                        // 2015-02-26, Oscar added. (1868, ref 1866)
                        if (vtNoticeTicketNo == "0"
                            || ticket.ErrorCode < 0
                            || !string.IsNullOrWhiteSpace(ticket.Error))
                        {
                            var error = ticket.Error;
                            switch (ticket.ErrorCode)
                            {
                                case -1:
                                    error = string.Format(VideoDetailController.InvalidValueOfAuthorityRule, "9300");
                                    break;
                                case -2:
                                    error = VideoDetailController.GettingNoticeTypeFailed;
                                    break;
                                case -3:
                                    error = VideoDetailController.AddingNoticePrefixFailed;
                                    break;
                                case -4:
                                    error = VideoDetailController.ResettingTranNumberFailed;
                                    break;
                                case -5:
                                    error = VideoDetailController.UpdatingNoticePrefixFailed;
                                    break;
                                case -6:
                                    error = VideoDetailController.GettingAuthorityFailed;
                                    break;
                                case -7:
                                    error = VideoDetailController.GettingTranNumberFailed;
                                    break;
                            }
                            return Json(new { result = -1, errorMsg = error });
                        }

                        frameNo = countDetail.ToString().PadLeft(4, '0');
                        item.VtNoticeTicketNo = vtNoticeTicketNo;
                        returnIntNo = handWrite.ImportVideoOffencesSection341(
                            header.AutIntNo, item.VtNoticeTicketNo,
                            header.CrtNo, header.VchOffenceDate.Add(item.VcdTime.TimeOfDay), header.VchLocation, item.VcdRegNo, item.VcdRegNo,
                            item.VmCode, string.Empty, item.VtCode, string.Empty, item.VcCode,
                            null, header.ToNo, officer.TosName, ofGroup.OfGrCode,//officer.ToGroup,
                            header.VchCaptureUserName, header.VchCaptureDate,
                            header.OffCode, off.OffDescr, header.VchFineAmount,
                            header.VchTapeNo, frameNo, false,
                            string.Empty, userInfo.UserName, string.Empty, off.OffNoAog,
                            string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                            nsType.NstCode, "VideoCameraS341", header.LoSuIntNo, header.VchOffenceDate.AddDays(notPaymentDate.ADRNoOfDays));

                        if (returnIntNo < 0)
                        {
                            errorMsg = string.Format(VideoDetailController.ErrorPostOpus1, GetPartFilmNoOfInput(header.VchTapeNo, autCode), vtNoticeTicketNo);
                            errorMsg = WriteErrorLog(returnIntNo, errorMsg, userInfo.UserName);
                            return Json(new { result = -1, errorMsg = errorMsg });
                        }

                        notFrame = NoticeFrameManager.GetByNotIntNo(returnIntNo);
                        FrameDetails frame = FrameManager.GetFrameDetails(notFrame.FrameIntNo);

                        if ((DateTime.Now - frame.OffenceDate).Days <= dateRule1stNotice.ADRNoOfDays)
                        {
                            //EntLibLogger.WriteLog(LogCategory.General, "Expired", "This offience expired");
                            QueueManager.PushQueue(
                                notFrame.FrameIntNo,
                                autCode,
                                dateRule1stNotice.ADRNoOfDays - (DateTime.Now - frame.OffenceDate).Days,
                                SIL.ServiceQueueLibrary.DAL.Entities.ServiceQueueTypeList.Frame_Natis);
                        }
                        QueueManager.PushQueue(
                            notFrame.FrameIntNo,
                            string.Format("{0}|H",autCode),
                            0,
                            SIL.ServiceQueueLibrary.DAL.Entities.ServiceQueueTypeList.CancelExpiredViolations_Frame,
                            frame.OffenceDate.AddDays(dateRule1stNotice.ADRNoOfDays));
                        QueueManager.PushQueue(
                            notFrame.NotIntNo,
                            autCode,
                            0,
                            SIL.ServiceQueueLibrary.DAL.Entities.ServiceQueueTypeList.CancelExpiredNotice,
                            frame.OffenceDate.AddDays(dateRuleNotExpire.ADRNoOfDays));

                        item.NotIntNo = returnIntNo;
                        VideoCaptureDetailManager.Update(item);
                        countDetail++;
                    }
                    if (returnIntNo > 0)
                    {
                        header.VchSubmitStatus = true;
                        VideoCaptureHeaderManager.Update(header);
                        
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        //punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.VideoOffenceCapture, PunchAction.Add);
                        scope.Complete();
                    }
                }
                catch (Exception e)
                {
                    errorMsg = string.Format(VideoDetailController.ErrorPostOpus2, GetPartFilmNoOfInput(header.VchTapeNo, ((string)Session["VHC_AutCode"]).Trim()), vtNoticeTicketNo, e.Message, "\n");
                    errorMsg = WriteErrorLog(0, errorMsg, userInfo.UserName);
                    return Json(new { result = -1, errorMsg = errorMsg });
                }
            }
            return Json(new { result = 0 });
        }



        private void InitModel(DetailModel model, long vchIntNo)
        {
            if (Session["HeaderMsg"] == null || ((HeaderMsg)Session["HeaderMsg"]).VCHIntNo != vchIntNo)
            {
                Session["HeaderMsg"] = GetHeaderMsg(vchIntNo);
            }
            model.VchIntNo = vchIntNo;
            model.Colours = VehicleColourManager.GetSelectList();
            model.Makers = VehicleMakeManager.GetSelectList();
            model.Types = VehicleTypeManager.GetSelectList();
            model.Header = (HeaderMsg)Session["HeaderMsg"];
            model.Details = GetTableRows(VideoCaptureDetailManager.GetByVchIntNo(vchIntNo));
            model.Language = Thread.CurrentThread.CurrentCulture.Name;
        }

        private HeaderMsg GetHeaderMsg(long vchIntNo)
        {
            string lsCode = Thread.CurrentThread.CurrentCulture.Name;
            VideoCaptureHeader header = VideoCaptureHeaderManager.GetByVchIntNo(vchIntNo);
            string autCode = (string)Session["VHC_AutCode"];
            TrafficOfficer officer = TrafficOfficerManager.GetOfficerByTOIntNo(header.ToIntNo);
            Offence offence = OffenceManager.GetByOffIntNoAndLsCode(header.OffIntNo,lsCode);
            NoticeStatisticalType nsType = NoticeStatisticalTypeManager.GetByNstIntNoAndLsCode(header.NstIntNo, lsCode);

            HeaderMsg headerMsg = new HeaderMsg()
            {
                VCHIntNo = vchIntNo,
                Date = header.VchOffenceDate.ToString("yyyy-MM-dd"),
                TapeCassetteNo = GetPartFilmNoOfInput(header.VchTapeNo, autCode),
                OfficerCode = officer.ToNo,
                OfficerName = officer.TosName,
                Location = header.VchLocation,
                StartTime = header.VchStartTime.ToString("HH:mm"),
                EndTime = header.VchEndTime.ToString("HH:mm"),
                Offence = string.Format("{0}({1})", offence.OffDescr, offence.OffCode),
                NSTypeDesc = string.Format("{0}({1})", nsType.NstDescription, nsType.NstCode),
            };
            return headerMsg;
        }

        private VideoCaptureDetail GetDetail(DetailModel model, string userName)
        {
            return new VideoCaptureDetail()
            {
                VchIntNo = model.VchIntNo,
                VcdIntNo = model.VcdIntNo,
                VcdRegNo = model.RegNo,
                VcIntNo = model.Colour,
                VmIntNo = model.Maker,
                VtIntNo = model.Type,
                VcCode = model.VcCode,
                VtCode = model.VtCode,
                VmCode = model.VmCode,
                VtNoticeTicketNo = string.Empty,
                LastUser = userName,
                VcdTime = DateTime.Parse(model.OffTime)
            };
        }

        private List<TableRowDetail> GetTableRows(TList<VideoCaptureDetail> details)
        {
            List<TableRowDetail> tableRows = new List<TableRowDetail>();
            int count = 1;
            foreach (var item in details)
            {
                tableRows.Add(new TableRowDetail()
                {
                    RowNo = count++,
                    VcdIntNo = item.VcdIntNo,
                    VCDRegNo = item.VcdRegNo,
                    VCDTime = item.VcdTime,
                    VechileColour = VehicleColourManager.GetByVcIntNo(item.VcIntNo).VcDescr,
                    VechileMaker = VehicleMakeManager.GetByVmIntNo(item.VmIntNo).VMDescr,
                    VechileType = VehicleTypeManager.GetByVtIntNo(item.VtIntNo).VTDescr,
                    VTNoticeTicketNo = item.VtNoticeTicketNo
                });
            }
            return tableRows;
        }


        private string WriteErrorLog(int notIntNo, string errorMsg, string lastUser)
        {
            if (notIntNo == -1)
            {
                errorMsg += VideoDetailController.ErrorPostOpusPart1;
            }
            else if (notIntNo == -2)
            {
                errorMsg += VideoDetailController.ErrorPostOpusPart2;
            }
            else if (notIntNo == -3)
            {
                errorMsg += VideoDetailController.ErrorPostOpusPart3;
            }
            else if (notIntNo == -4)
            {
                errorMsg += VideoDetailController.ErrorPostOpusPart4;
            }
            else if (notIntNo == -5)
            {
                errorMsg += VideoDetailController.ErrorPostOpusPart5;
            }
            else if (notIntNo == -6)
            {
                errorMsg += VideoDetailController.ErrorPostOpusPart6;
            }
            else if (notIntNo == -30)
            {
                errorMsg += VideoDetailController.ErrorPostOpusPart30;
            }
            else if (notIntNo == -50)
            {
                errorMsg += VideoDetailController.ErrorPostOpusPart50;
            }
            else if (notIntNo == -51)
            {
                errorMsg += VideoDetailController.ErrorPostOpusPart51;
            }
            else if (notIntNo == -16)
            {
                errorMsg += VideoDetailController.ErrorPostOpusPart16;
            }
            else if (notIntNo == -103 || notIntNo == -104)
            {
                errorMsg += VideoDetailController.ErrorPostOpusPart103_104;
            }
            else if (notIntNo == -150)
            {
                errorMsg += string.Format(VideoDetailController.ErrorPostOpusPart150, notIntNo.ToString());
            }
            else if (notIntNo == -170)
            {
                errorMsg += string.Format(VideoDetailController.ErrorPostOpusPart170, notIntNo.ToString());
            }
            else
            {
                if (notIntNo < 0)
                {
                    errorMsg = string.Format(String.Format(VideoDetailController.ErrorPostOpusPart_0 + notIntNo.ToString()));
                }
            }

            EntLibLogger.WriteLog(LogCategory.Error, lastUser, errorMsg);
            return errorMsg;
        }



    }
}
