﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Stalberg.TMS;
using SIL.AARTO.BLL.Utility;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.Imaging;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using SIL.AARTO.Web.Resource.Enquiry;
using SIL.AARTO.DAL.Services;
using System.Globalization;
namespace SIL.AARTO.Web.Controllers.Enquiry
{
    public partial class EnquiryController : Controller
    {
        public ActionResult ViewOffenceDetailAllReport_CT(string searchField, string searchValue, string Since)
        {

            if (Session["UserLoginInfo"] == null)
                return RedirectToAction("login", "account");
         
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            login = userDetails.UserName;
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
             GeneratedDateRole = GetGeneratedDateReplacePrintDateRule(autIntNo);


            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //2014-09-28 Heidi changed for According to the template decided to use which data(bontq1523)
            //AuthorityService autService = new AuthorityService();
            //SIL.AARTO.DAL.Entities.Authority autEntity = autService.GetByAutIntNo(autIntNo);
            //string autCode = "";
            //if (autEntity != null)
            //{
            //    autCode = autEntity.AutCode.Trim().ToLower();
            //}
            //if (autCode != "cp")
            //{

            //    return RedirectToAction("ViewOffenceDetailAllReport", new { searchField = searchField, searchValue = searchValue, Since = Since });
            //}

            thisPageURL = "ViewOffenceDetailAllReport";
            int nNotIntNo = 0; //Convert.ToInt32(Request.QueryString["NotIntNo"].ToString());
            // SD: 26.11.2008 Added noOfLines to count the number of Lines in Evidence pack of HistoryPrintAll stored Proc
            //int noOfLines = CheckNumberOfLines(nNotIntNo);

            //if (noOfLines <= 10)
            //{
            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "NoticeEnquiryAll");

            //2014-09-28 Heidi added for According to the template decided to use which data(bontq1523)
            if (!string.IsNullOrEmpty(reportPage))
            {
                if (reportPage.Trim() == "NoticeEnquiryAll.dplx")
                {
                    return RedirectToAction("ViewOffenceDetailAllReport", new { searchField = searchField, searchValue = searchValue, Since = Since });
                }
            }


            if (reportPage.Equals(string.Empty))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "NoticeEnquiryAll_CoCT.dplx", "NoticeEnquiryAll", "system", "");
                reportPage = "NoticeEnquiryAll_CoCT.dplx";
            }

            string path = Server.MapPath("~/Reports/" + reportPage);

            if (!System.IO.File.Exists(path))
            {
                string error = string.Format(ViewOffenceDetailAllReportRes.error, reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, ViewOffenceDetailAllReportRes.thisPage, thisPageURL);

                Response.Redirect(errorURL);
                return View();
            }

            try
            {
                DocumentLayout doc = new DocumentLayout(path);

                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea raTotal = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("raTotal");
                raTotal.LayingOut += ra_Total6;

                //Barry Dickson - add control in to not show receipt fields if there has been no payment made or there was a reversal
                //label for raTotal
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRctAmount = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRctAmount");
                lblRctAmount.LayingOut += lbl_Amount6;
                //Receipt Number
                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcbRctNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rcbRctNo");
                rcbRctNo.LayingOut += ra_RctNo6;
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRctNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRctNo");
                lblRctNo.LayingOut += lbl_RctNo6;

                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcbOriginalSumChargeFine = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rcbOriginalSumChargeFine");
                rcbOriginalSumChargeFine.LayingOut += new LayingOutEventHandler(rcbOriginalSumChargeFine_LayingOut6);

                //Heidi 2013-05-29 added 1st notice posted date
                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcbNotPosted1stNoticeDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rcbNotPosted1stNoticeDate");
                rcbNotPosted1stNoticeDate.LayingOut += new LayingOutEventHandler(rcbNotPosted1stNoticeDate_LayingOut6);
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl1stNoticePostedDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl1stNoticePostedDate");
                lbl1stNoticePostedDate.LayingOut += new LayingOutEventHandler(lbl1stNoticePostedDate_LayingOut6);
                
                //Heidi 2013-05-29 added 2st notice posted date
                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcbNotPosted2ndNoticeDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rcbNotPosted2ndNoticeDate");
                rcbNotPosted2ndNoticeDate.LayingOut += new LayingOutEventHandler(rcbNotPosted2ndNoticeDate_LayingOut6);
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl2ndNoticePostedDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl2ndNoticePostedDate");
                lbl2ndNoticePostedDate.LayingOut += new LayingOutEventHandler(lbl2ndNoticePostedDate_LayingOut6);

                ////EasyPay Number
                //ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcbEasyPay = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rcbEasyPay");
                //rcbEasyPay.LayingOut += ra_Easy6;
                //ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblEasyPay = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblEasyPay");
                //lblEasyPay.LayingOut += lbl_Easy6;

                //Jerry 2012-11-26 Receipt AGNumber
                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea raRctAGNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("raAGNumber");
                raRctAGNo.LayingOut += ra_RctAgNo6;
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRctAgNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAGNumber");
                lblRctAgNo.LayingOut += lbl_RctAgNo6;

                //Remanded from case number and court date

                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcdRemandedFromCaseNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rdaSumRemandedFromCaseNumber");
                rcdRemandedFromCaseNo.LayingOut += new LayingOutEventHandler(ra_RemandedFromCaseNo6);

                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcdRemandedFromCourtDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rdaSumRemandedFromCourtDate");
                rcdRemandedFromCourtDate.LayingOut += new LayingOutEventHandler(ra_RemandedFromCourtDate6);

                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRemandedFromCourtDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRemandedFromCourtDate");
                lblRemandedFromCourtDate.LayingOut += new LayingOutEventHandler(lbl_RemandedFromCourtDate6);

                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRemandedFromCaseNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRemandedFromCaseNumber");
                lblRemandedFromCaseNo.LayingOut += new LayingOutEventHandler(lbl_RemandedFromCaseNo6);


                // Check that there's a Query criteria in the Session
                //NoticeQueryCriteria criteria = Session["NoticeQueryCriteria"] as NoticeQueryCriteria;
                //if (criteria == null)
                //    Response.Redirect("Login.aspx");
                //Session.Remove("NoticeQueryCriteria");

                StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
                query.ConnectionString = this.connectionString;

                StoredProcedureQuery query2 = (StoredProcedureQuery)doc.GetQueryById("Query2");
                query2.ConnectionString = this.connectionString;

                StoredProcedureQuery query3 = (StoredProcedureQuery)doc.GetQueryById("Query3");
                query3.ConnectionString = this.connectionString;

                ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder imagePlaceHolder = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("Img1");
                imagePlaceHolder.LaidOut += imagePlaceHolder_LaidOut6;
                ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder imagePlaceHolder2 = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("Img2");
                imagePlaceHolder2.LaidOut += imagePlaceHolder2_LaidOut6;

                autIntNo = 0;
                searchValue = searchValue.Replace("/", "").Replace("-", "");
                DateTime dtSince;
                if (DateTime.TryParse(Since, out dtSince))
                { }
                else
                {
                    dtSince = new DateTime(2000, 1, 1);
                }
                int minStatus = CheckMinimumStatusRule();



                ParameterDictionary parameters = new ParameterDictionary();
                parameters.Add("NotIntNo", nNotIntNo);
                parameters.Add("AutIntNo", autIntNo);
                parameters.Add("ColName", searchField);
                parameters.Add("ColValue", searchValue);
                parameters.Add("DateSince", dtSince);
                parameters.Add("MinStatus", minStatus);
                parameters.Add("GenerDateReplacePrintDateRole", GeneratedDateRole);

                Document report = doc.Run(parameters);

                byte[] buffer = report.Draw();

                imagePlaceHolder.LaidOut -= imagePlaceHolder_LaidOut6;
                imagePlaceHolder2.LaidOut -= imagePlaceHolder2_LaidOut6;

                raTotal.LayingOut -= ra_Total;
                lblRctAmount.LayingOut -= lbl_Amount6;
                //lblRctAmount.LayingOut -= lbl_Easy6;
                lblRctAmount.LayingOut -= lbl_RctNo6;
                lblRctAmount.LayingOut -= ra_RctNo6;
                //lblRctAmount.LayingOut -= ra_Easy6;
                lblRctAmount.LayingOut -= ra_RctAgNo6;
                lblRctAmount.LayingOut -= lbl_RctAgNo6;

                //Response.ClearContent();
                //Response.ClearHeaders();
                //Response.ContentType = "application/pdf";
                //Response.BinaryWrite(buffer);
                //Response.End();
                //buffer = null;

                return File(buffer, "application/pdf");




            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                Response.End();
                return View();
            }
            finally
            {
                GC.Collect();
            }
            //return View();
        }

        void lbl2ndNoticePostedDate_LayingOut6(object sender, LayingOutEventArgs e)
        {
            try
            {

                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblE = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;

                if (GeneratedDateRole!= "A")
                {
                    lblE.Text = lblE.Text = ViewOffenceDetailReportRes.lbl2ndNoticePostedDate;
                }
                else
                {
                    lblE.Text = "";
                }

            }
            catch { }
        }

        void rcbNotPosted2ndNoticeDate_LayingOut6(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (GeneratedDateRole!= "A")
                {
                    if (e.LayoutWriter.RecordSets.Current["NotPosted2ndNoticeDate"] != DBNull.Value)
                    {
                        if (e.LayoutWriter.RecordSets.Current["NotPosted2ndNoticeDate"].ToString() != string.Empty)
                        {
                            ra.Text = Convert.ToDateTime(e.LayoutWriter.RecordSets.Current["NotPosted2ndNoticeDate"]).ToString("yyyy-MM-dd");
                        }
                    }
                }
                else
                {
                    ra.Text = "";

                }
            }
            catch { }
        }

        void lbl1stNoticePostedDate_LayingOut6(object sender, LayingOutEventArgs e)
        {
            try
            {


                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblE = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (GeneratedDateRole!= "A")
                {
                    lblE.Text = lblE.Text = ViewOffenceDetailReportRes.lbl1stNoticePostedDate;
                }
                else
                {
                    lblE.Text = "";
                }

            }
            catch { }


        }

        void rcbNotPosted1stNoticeDate_LayingOut6(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (GeneratedDateRole!= "A")
                {
                    if (e.LayoutWriter.RecordSets.Current["NotPosted1stNoticeDate"] != DBNull.Value)
                    {
                        if (e.LayoutWriter.RecordSets.Current["NotPosted1stNoticeDate"].ToString() != string.Empty)
                        {
                            ra.Text = Convert.ToDateTime(e.LayoutWriter.RecordSets.Current["NotPosted1stNoticeDate"]).ToString("yyyy-MM-dd");
                        }
                    }
                }
                else
                {
                    ra.Text = "";

                }
            }
            catch { }
        }

        void rcbOriginalSumChargeFine_LayingOut6(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["OriginalSumChargeFine"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["OriginalSumChargeFine"].ToString() == "NoAOG")
                    {
                        ra.Text = "NoAOG";
                    }
                    else
                    {
                        if (e.LayoutWriter.RecordSets.Current["OriginalSumChargeFine"].ToString() != string.Empty
                            //&& int.Parse(e.LayoutWriter.RecordSets.Current["OriginalSumChargeFine"].ToString()) != 0
                            )
                            //ra.Text = String.Format("R {0:#0.00}", e.LayoutWriter.RecordSets.Current["OriginalSumChargeFine"]);
                            ra.Text = "R  " + Convert.ToString(e.LayoutWriter.RecordSets.Current["OriginalSumChargeFine"]);
                    }

                }
            }
            catch { }
        }


        public void lbl_RemandedFromCourtDate6(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"].ToString() != string.Empty)
                        lblA.Text = ViewOffenceDetailAllReportRes.lblAText;

                }
            }
            catch { }
        }

        public void lbl_RemandedFromCaseNo6(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"].ToString() != string.Empty)
                        lblA.Text = ViewOffenceDetailAllReportRes.lblAText1;

                }
            }
            catch { }
        }

        public void ra_RemandedFromCaseNo6(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"].ToString() != string.Empty)
                        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"]);
                }
            }
            catch
            {
            }
        }

        public void ra_RemandedFromCourtDate6(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"].ToString() != string.Empty)
                    {
                        DateTime courtDate = Convert.ToDateTime(e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"].ToString());
                        ra.Text = String.Format("{0}", courtDate.ToString("yyyy-MM-dd"));
                    }
                }
            }
            catch
            {
            }
        }

        public void ra_Total6(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;

                ra.Text = string.Empty;

                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                    {
                        //update by Rachel 20140821 for 5337
                        //ra.Text = String.Format("R {0:#0.00}", e.LayoutWriter.RecordSets.Current["Total"]);
                        ra.Text = String.Format(CultureInfo.InvariantCulture,"R {0:#0.00}", e.LayoutWriter.RecordSets.Current["Total"]);
                        //end update by Rachel 20140821 for 5337
                    }
                }
            }
            catch { }
        }

        // SD: 26.11.2008 Added noOfLines to count the number of Lines in Evidence pack of HistoryPrintAll stored Proc
        //protected int CheckNumberOfLines(int NotIntNo)
        //{
        //    OffenceDB lines = new OffenceDB(this.connectionString);
        //    int noOfLines = lines.GetNumberofLines(NotIntNo);
        //    return noOfLines;
        //}

        //Barry Dickson - to handle to check on receipt amount and if to show label.
        public void lbl_Amount6(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;

                lblA.Text = string.Empty;

                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (!string.IsNullOrEmpty(e.LayoutWriter.RecordSets.Current["Total"].ToString()) && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                        lblA.Text = ViewOffenceDetailAllReportRes.lblAText2;
                }
            }
            catch { }
        }
        //Barry Dickson
        public void lbl_RctNo6(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRN = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;

                lblRN.Text = string.Empty;

                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                        lblRN.Text = ViewOffenceDetailAllReportRes.lblRNText;
                }
            }
            catch { }
        }

        //Jerry 2012-11-26 add
        public void ra_RctAgNo6(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0 &&
                        e.LayoutWriter.RecordSets.Current["AGNumber"] != null && e.LayoutWriter.RecordSets.Current["AGNumber"].ToString() != string.Empty)
                    {
                        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["AGNumber"]);
                    }
                    else
                    {
                        ra.Text = "";
                    }
                }
            }
            catch { }
        }

        //Jerry 2012-11-26 add
        public void lbl_RctAgNo6(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAGNum = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0 &&
                        e.LayoutWriter.RecordSets.Current["AGNumber"] != null && e.LayoutWriter.RecordSets.Current["AGNumber"].ToString() != string.Empty)
                    {
                        lblAGNum.Text = ViewOffenceDetailAllReportRes.lblAGNumText;
                    }
                    else
                    {
                        lblAGNum.Text = "";
                    }
                }
            }
            catch { }
        }

        //Barry Dickson
        public void lbl_Easy6(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblE = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;

                lblE.Text = string.Empty;

                if (e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"].ToString() != string.Empty)
                        lblE.Text = ViewOffenceDetailAllReportRes.lblEText;
                }
            }
            catch { }
        }

        //Barry Dickson
        public void ra_RctNo6(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;

                ra.Text = string.Empty;

                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["RctNumber"]);
                }
            }
            catch { }
        }

        //Barry Dickson
        public void ra_Easy6(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;

                ra.Text = string.Empty;

                if (e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"].ToString() != string.Empty)
                        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"]);
                }
            }
            catch { }
        }

        private void imagePlaceHolder_LaidOut6(object sender, PlaceHolderLaidOutEventArgs e)
        {
            try
            {
                //byte[] buffer = (byte[])this.ds.Tables[0].Rows[0]["ScanImage1"];

                // david lin 20100324 - remove images from database
                //byte[] buffer = e.LayoutWriter.RecordSets.Current["Max_SI"] == System.DBNull.Value ? null : (byte[])e.LayoutWriter.RecordSets.Current["Max_Si"];                
                byte[] buffer = null;
                if (e.LayoutWriter.RecordSets.Current["Max_SI"] != System.DBNull.Value)
                {
                    ScanImageDB imgDB = new ScanImageDB(this.connectionString);
                    ScanImageDetails imgMax = imgDB.GetImageFullPath(Convert.ToInt32(e.LayoutWriter.RecordSets.Current["Max_SI"]));
                    WebService webService = new WebService();
                    buffer = webService.GetImagesFromRemoteFileServer(imgMax);
                }

                //MemoryStream ms = new MemoryStream(buffer, false);

                //System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(ms);

                //ceTe.DynamicPDF.PageElements.Image image = new ceTe.DynamicPDF.PageElements.Image(bmp, 0, 0);
                //float f = 200F / image.Width;
                //image.Width = image.Width * f;
                //image.Height = image.Height * f;

                //e.ContentArea.Add(image);
                if (buffer != null)
                {
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(buffer), 0, 0);
                    img.Height = 72.0F;            //12
                    img.Width = 90.0F;             //15
                    //dls 091126 - since we added the page breaks, the X & Y values of the images seem to be reset to 0
                    img.X = 438.0F;
                    img.Y = 6.0F;
                    e.ContentArea.Add(img);

                    int generation = System.GC.GetGeneration(buffer);
                    buffer = null;
                    System.GC.Collect(generation);
                }
            }
            catch { }
        }

        private void imagePlaceHolder2_LaidOut6(object sender, PlaceHolderLaidOutEventArgs e)
        {
            try
            {
                //byte[] buffer = (byte[])this.ds.Tables[0].Rows[0]["ScanImage1"];

                // david lin 20100324 - remove images from database
                //byte[] buffer = e.LayoutWriter.RecordSets.Current["Min_SI"] == System.DBNull.Value ? null : (byte[])e.LayoutWriter.RecordSets.Current["Min_Si"];
                byte[] buffer = null;
                if (e.LayoutWriter.RecordSets.Current["Min_SI"] != System.DBNull.Value)
                {
                    ScanImageDB imgDB = new ScanImageDB(this.connectionString);
                    ScanImageDetails imgMin = imgDB.GetImageFullPath(Convert.ToInt32(e.LayoutWriter.RecordSets.Current["Min_SI"]));
                    WebService webService = new WebService();
                    buffer = webService.GetImagesFromRemoteFileServer(imgMin);
                }
                //MemoryStream ms = new MemoryStream(buffer, false);

                //System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(ms);

                //ceTe.DynamicPDF.PageElements.Image image = new ceTe.DynamicPDF.PageElements.Image(bmp, 0, 0);
                //float f = 200F / image.Width;
                //image.Width = image.Width * f;
                //image.Height = image.Height * f;

                //e.ContentArea.Add(image);
                if (buffer != null)
                {
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(buffer), 0, 0);
                    img.Height = 72.0F;            //12
                    img.Width = 90.0F;             //15
                    //dls 091126 - since we added the page breaks, the X & Y values of the images seem to be reset to 0
                    img.X = 438.0F;
                    img.Y = 99.5F;
                    e.ContentArea.Add(img);

                    int generation = System.GC.GetGeneration(buffer);
                    buffer = null;
                    System.GC.Collect(generation);
                }
            }
            catch { }
        }
    }
}
