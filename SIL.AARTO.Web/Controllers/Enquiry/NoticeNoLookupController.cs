﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.Resource.Enquiry;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.ViewModels.Enquiry;
using System.Data.SqlClient;
using System.Data;
using Stalberg.TMS;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.Web.Controllers.Enquiry
{
    public partial class EnquiryController : Controller
    {
        #region NoticeNoLookup

        public ActionResult NoticeNoLookup(EnquiryModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            string error="";
            string login;
            string sReturn = String.Empty;
            int nAutIntNo;
            string ticketProcessor;// jerry 2011-1-13 add
            int processorSign;// jerry 2011-1-13 add

            string prefix;// Edge 2012-11-09 add
            string sequence;// Edge 2012-11-09 add
            


            // Get user details
            SIL.AARTO.BLL.Utility.UserLoginInfo userDetails = new BLL.Utility.UserLoginInfo();
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            login = userDetails.UserName;
            int autIntNo = 0;
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // jerry 2011-1-13 add
            ticketProcessor = Session["TicketProcessor"] != null ? Session["TicketProcessor"].ToString() : "TMS";
            if (ticketProcessor.Equals("TMS") || ticketProcessor.Equals("Cip_CofCT") || ticketProcessor.Equals("CiprusPI"))
            {
                processorSign = 1;
            }
            else
            {
                processorSign = 2;
            }

            sReturn = string.Empty;
            model.originalTicketNo = "";
            if (model.NoticeNoLookupSubmitToAction != null && model.NoticeNoLookupSubmitToAction == "EnquiryList")
            {
                Session["ISFromNoticeLookup"] = 1;
            }
       
            DataTable dt = null;
            if (model.searchField2 == null || model.searchField2 == "")
            {
                model.searchField2 = "TickSequenceNo";
            }
            if (model.searchField2 == "TickSequenceNo")
            {
                dt = Search_Ticket(model, processorSign, ticketProcessor, out error);
            }
            else
            {
                // model.searchField = "EasyPayNo";

                dt = Search_EasyPay(model, out error);
            }
            model.LookUpErrorMsg = error;
            if (error != "")
            {
                model.NoticeNoLookupHasError = "1";

                TempData["EnquiryModel"] = model;
                return RedirectToAction(model.NoticeNoLookupSubmitToAction, model.NoticeNoLookupSubmitToController);
            }
            if (dt == null || dt.Rows.Count == 0)
            {
                if (model.searchField2 == "TickSequenceNo")
                {
                    if (model.CDV == null)
                    {
                        model.CDV = "";
                    }
                    if (model.Prefix == null)
                    {
                        model.Prefix = "";
                    }
                    if (model.Sequence == null)
                    {
                        model.Sequence = "";
                    }
                    if (model.AuthCode==null)
                    {
                        model.AuthCode = "";
                    }
                    string sNoticeNumber = model.Prefix.Trim() + "/" + model.Sequence.Trim() + "/" + model.AuthCode.Trim() + "/" + model.CDV.Trim();
                    model.LookUpErrorMsg = string.Format("{0} {1} {2}", NoticeNoLookupRes.lblErrorText9, sNoticeNumber, NoticeNoLookupRes.lblErrorText10);
                    model.TicketNo = model.Prefix.Trim() + model.Sequence.Trim() + model.AuthCode.Trim() + model.CDV.Trim();
                    model.searchField = "TicketNo";
                    TempData["EnquiryModel"] = model;
                    return RedirectToAction(model.NoticeNoLookupSubmitToAction, model.NoticeNoLookupSubmitToController);
                }
                else
                {
                    model.TicketNo = "";
                    model.searchField = "TicketNo";
                    TempData["EnquiryModel"] = model;
                    return RedirectToAction(model.NoticeNoLookupSubmitToAction, model.NoticeNoLookupSubmitToController);
                }
            }

            if (dt != null && dt.Rows.Count == 1)
            {
                sReturn = dt.Rows[0]["NotTicketNo_Enquiry"] == null ? "" : Convert.ToString(dt.Rows[0]["NotTicketNo_Enquiry"]);

                string ticketNo = dt.Rows[0]["NotTicketNo"] == null ? "" : Convert.ToString(dt.Rows[0]["NotTicketNo"]);
                if (ticketNo.IndexOf("/") > 0)
                {
                    model.Prefix = ticketNo.Split('/')[0];
                    model.Sequence = ticketNo.Split('/')[1];
                }
                model.TicketNo = sReturn;
                model.searchField = "TicketNo";
                TempData["EnquiryModel"] = model;
                return RedirectToAction(model.NoticeNoLookupSubmitToAction, model.NoticeNoLookupSubmitToController);
               
            }
            else if (dt != null && dt.Rows.Count > 1)
            {
                
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Header item = new Header();
                        item.NotTicketNo_Enquiry = dt.Rows[i]["NotTicketNo_Enquiry"] == null ? "" : Convert.ToString(dt.Rows[i]["NotTicketNo_Enquiry"]);
                        item.NotTicketNo = dt.Rows[i]["NotTicketNo"] == null ? "" : Convert.ToString(dt.Rows[i]["NotTicketNo"]);
                        item.NotRegNo = dt.Rows[i]["NotRegNo"] == null ? "" : Convert.ToString(dt.Rows[i]["NotRegNo"]);
                        item.Name = dt.Rows[i]["Name"] == null ? "" : Convert.ToString(dt.Rows[i]["Name"]);
                        item.NotIntNo = dt.Rows[i]["NotIntNo"] == null ? 0 : Convert.ToInt32(dt.Rows[i]["NotIntNo"]);
                        model.HeaderList.Add(item);
                       
                    }
                    model.TicketNo = "";
                    model.searchField = "RegNo";
            }
            else
            {
               sReturn = model.Prefix.Trim() + model.Sequence.Trim() + model.AuthCode.Trim() + model.CDV.Trim();
               model.Sequence = string.Empty;
               model.TicketNo = sReturn;
               model.searchField = "TicketNo";
               TempData["EnquiryModel"] = model;
               return RedirectToAction(model.NoticeNoLookupSubmitToAction, model.NoticeNoLookupSubmitToController);
            }

            TempData["EnquiryModel"] = model;
            return RedirectToAction(model.NoticeNoLookupSubmitToAction, model.NoticeNoLookupSubmitToController);
          
        }

       
      

        public DataTable Search_Ticket(EnquiryModel model, int processorSign, string ticketProcessor, out string error)
        {

            // Heidi 2014-10-16 added "try catch and WriteLog" for fixing AARTOErrorLog shows issue.(5367)
            error = "";
            try
            {
                char cTemp = Convert.ToChar("0");
                int autIntNo = Convert.ToInt32(Session["autIntNo"]);
                SIL.AARTO.BLL.Utility.UserLoginInfo userDetails = new BLL.Utility.UserLoginInfo();
                userDetails = (UserLoginInfo)Session["UserLoginInfo"];
                string login = userDetails.UserName;

                int nCdv;
                int nSeq;
                String sNoticeNumber = string.Empty;

                NoticeDB ndb = new NoticeDB(connectionString);
                //SqlDataReader reader = null;

                DataTable dt = new DataTable();

                if (model.Prefix != null && model.Prefix.Trim().Length == 0)
                {
                    //2012-3-6 linda modified into a multi-language
                    error = NoticeNoLookupRes.lblErrorText;
                    return null;
                }

                if (processorSign == 1)//processor is TMS
                {
                    // TMS Ticket Processors
                    // Order of text boxes:
                    //  1.	1st text box = Notice Prefix/Document Type  blank, user must enter (might be hidden if AuthRule 4905 Rule to hide prefix in Ticket Search = ‘Y’)
                    //  2.	2nd text box = Sequence no  blank, user must enter
                    //  3.	3rd text box = Authority.AutNo for current Authority (pre-populate)

                    //Heidi 2014-10-16 added validation for fixing AARTOErrorLog shows issue.(5367)
                    if ((model.Prefix == null) || (model.Prefix != null && model.Prefix.Trim().Length == 0))
                    {
                        error = NoticeNoLookupRes.lblErrorText1 + ticketProcessor + NoticeNoLookupRes.lblErrorText6;
                        model.setControlFocus = "txtPrefix";
                        return null;
                    }

                    if ((model.Sequence == null) || (model.Sequence != null && model.Sequence.Trim().Length == 0))
                    {
                        //2012-3-6 linda modified into a multi-language
                        error = NoticeNoLookupRes.lblErrorText1 + ticketProcessor + NoticeNoLookupRes.lblErrorText8;
                        //txtNumber.Focus();
                        model.setControlFocus = "txtNumber";
                        return null;
                    }

                    if ((model.AuthCode == null) || (model.AuthCode != null && model.AuthCode.Trim().Length == 0))
                    {
                        //2012-3-6 linda modified into a multi-language
                        error = NoticeNoLookupRes.lblErrorText1 + ticketProcessor + NoticeNoLookupRes.lblErrorText12;
                        // txtAuthCode.Focus();
                        model.setControlFocus = "txtAuthCode";
                        return null;
                    }
                    if (model.AuthCode != null && model.AuthCode.Trim().Length > 0)
                    {
                        int auno = 0;
                        int.TryParse(model.AuthCode.Trim(), out  auno);
                        if (auno == 0)
                        {
                            //2012-3-6 linda modified into a multi-language
                            // error = NoticeNoLookupRes.lblErrorText1 + ticketProcessor + NoticeNoLookupRes.lblErrorText3;
                            error = "Authority Number portion of the ticket number does not appear to be a valid number";
                            // txtAuthCode.Focus();
                            model.setControlFocus = "txtAuthCode";
                            return null;
                        }
                    }

                    //Jerry 2012-08-21 add
                    AuthorityRulesDetails rule9300 = new AuthorityRulesDetails();
                    rule9300.AutIntNo = autIntNo;
                    rule9300.ARCode = "9300";
                    rule9300.LastUser = login;
                    DefaultAuthRules authRule = new DefaultAuthRules(rule9300, this.connectionString);
                    KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();
                    //This is a problem because the Handwritten offences use 00000 numbers for Sequence Number
                    if (model.Sequence != null)
                    {
                        model.Sequence = model.Sequence.PadLeft(value.Key, cTemp);

                        if (!int.TryParse(model.Sequence.Trim(), out nSeq))
                        {
                            //2012-3-6 linda modified into a multi-language
                            error = NoticeNoLookupRes.lblErrorText4;
                            //txtNumber.Focus();
                            model.setControlFocus = "txtNumber";
                            return null;
                        }
                    }
                    else
                    {
                        //2012-3-6 linda modified into a multi-language
                        error = NoticeNoLookupRes.lblErrorText4;
                        //txtNumber.Focus();
                        model.setControlFocus = "txtNumber";
                        return null;
                    }

                    nCdv = SIL.AARTO.BLL.Utility.TicketNumber.GetCdvOfTicketNumber(nSeq, model.Prefix.Trim(), model.AuthCode.Trim());
                    if (nCdv == -1)
                    {
                        //2012-3-6 linda modified into a multi-language
                        error = NoticeNoLookupRes.lblErrorText5;
                        //txtNoticePrefix.Focus();
                        model.setControlFocus = "txtNoticePrefix";
                        return null;
                    }

                    model.CDV = nCdv.ToString();

                    sNoticeNumber = model.Prefix.Trim() + "/" + model.Sequence.Trim() + "/" + model.AuthCode.Trim() + "/" + model.CDV;

                    //reader = ndb.TicketNumberSearch(this.nAutIntNo, processorSign, nSeq, txtAuthCode.Text.Trim(), txtNoticePrefix.Text.Trim(), 0);
                    dt = ndb.TicketNumberSearchDT(processorSign, nSeq, model.AuthCode, model.Prefix.Trim(), 0);
                }
                else// processor is AARTO
                {
                    // PLEASE NOTE: for the AARTO Ticket Processor
                    //	Order of text boxes is different!!!!
                    //  1.	1st text box = Document type  blank, user must enter (always show)
                    //  2.	2nd text box = Authority.ENatisAuthorityNumber for current Authority (pre-populate)
                    //  3.	3rd text box = sequence no  blank, user must enter
                    //	Instead of AutNo, we need to find ENatisAuthorityNumber for current Authority

                    if ((model.Prefix == null) || (model.Prefix != null && model.Prefix.Trim().Length == 0))
                    {
                        //2012-3-6 linda modified into a multi-language
                        error = NoticeNoLookupRes.lblErrorText1 + ticketProcessor + NoticeNoLookupRes.lblErrorText6;
                        //txtNumber.Focus();
                        model.setControlFocus = "txtPrefix";
                        return null;
                    }

                    if ((model.Sequence == null) || (model.Sequence != null && model.Sequence.Trim().Length == 0))
                    {
                        //2012-3-6 linda modified into a multi-language
                        error = NoticeNoLookupRes.lblErrorText1 + ticketProcessor + NoticeNoLookupRes.lblErrorText8;
                        //txtNumber.Focus();
                        model.setControlFocus = "txtNumber";
                        return null;
                    }

                    if ((model.AuthCode == null) || (model.AuthCode != null && model.AuthCode.Trim().Length == 0))
                    {
                        //2012-3-6 linda modified into a multi-language
                        error = NoticeNoLookupRes.lblErrorText1 + ticketProcessor + NoticeNoLookupRes.lblErrorText12;
                        //txtAuthCode.Focus();
                        model.setControlFocus = "txtAuthCode";
                        return null;
                    }

                    //txtAuthCode.Text = txtAuthCode.Text.PadLeft(5, cTemp);
                    model.AuthCode = model.AuthCode.PadLeft(9, cTemp);

                    if (!int.TryParse(model.AuthCode.Trim(), out nSeq))
                    {
                        //2012-3-6 linda modified into a multi-language
                        error = NoticeNoLookupRes.lblErrorText4;
                        //txtAuthCode.Focus();
                        model.setControlFocus = "txtAuthCode";
                        return null;
                    }

                    sNoticeNumber = model.Prefix.Trim() + "-" + model.Sequence.Trim() + "-" + model.AuthCode.Trim() + "-";
                    string tmpStr = sNoticeNumber.Replace("-", "");
                    nCdv = Verhoeff.CalculateCheckDigit(tmpStr);
                    model.CDV = nCdv.ToString();
                    sNoticeNumber += model.CDV;


                    dt = ndb.TicketNumberSearchDT(processorSign, nSeq, "", model.Prefix.Trim(), Convert.ToInt32(model.Sequence.Trim()));

                }
                //2012-3-6 linda modified into a multi-language
                // error = NoticeNoLookupRes.lblErrorText9 + sNoticeNumber + NoticeNoLookupRes.lblErrorText10;
                return dt;
            }
            catch (Exception ex)
            {
                string errortemp = string.Format("{0}-{1}-{2}-{3}", model.Prefix, model.Sequence, model.AuthCode,model.CDV);
                EntLibLogger.WriteLog(LogCategory.Error, "NoticeNo", errortemp);
                EntLibLogger.WriteErrorLog(ex, LogCategory.Error, AartoProjectList.AARTOWebApplication.ToString());
                return null;
            }
           
        }

        public DataTable Search_EasyPay(EnquiryModel model, out string error)
        {
            error = string.Empty;
            NoticeDB ndb = new NoticeDB(connectionString);
            DataTable dt = new DataTable();
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            char cTemp = Convert.ToChar("0");

            if (model.EasyPayNo != null && model.EasyPayNo.Trim().Length >= 1)
            {
                dt = ndb.EasyPaySearch(autIntNo, model.EasyPayNo);
                model.setControlFocus = "txtEasyPayNo";

            }
            else
            {
                //2012-3-6 linda modified into a multi-language
                error = string.Format("{0} {1} {2}", NoticeNoLookupRes.lblErrorText11, model.EasyPayNo, NoticeNoLookupRes.lblErrorText10);
            }
            return dt;
        }

        #endregion

    }
}
