﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Stalberg.TMS;
using SIL.AARTO.BLL.Utility.PrintFile;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.Web.Controllers.Enquiry
{
    public partial class EnquiryController : Controller
    {
        // Fields
         string sLetterTo = string.Empty;
         string sTicketNo = string.Empty;
         int repIntNo = -1;
        
         string description = String.Empty;

        //2011-10-18 jerry add
        private string printFileName = string.Empty;
        private bool printFlag = false;
        public ActionResult RepresentationLetterViewer()
        {
            // Get user info from session variable
            if (Session["UserLoginInfo"] == null)
                return RedirectToAction("login", "account");
            //if (Session["userIntNo"] == null)
            //    return RedirectToAction("login", "account");

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //2011-10-18 jerry add
            if (Request.QueryString["PrintFileName"] != null && Request.QueryString["PrintFlag"] != null)
            {
                this.printFileName = Request.QueryString["PrintFileName"].ToString();
                string tempPrintFlag = Request.QueryString["PrintFlag"].ToString();
                this.printFlag = tempPrintFlag == "N" ? false : true;
            }
            else
            {
                if (Request.QueryString["RepIntNo"] == null)
                {
                    return RedirectToAction("login", "account");
                }
                this.repIntNo = int.Parse(Request.QueryString["RepIntNo"].ToString());
                this.sLetterTo = Request.QueryString["LetterTo"].ToString();
                if (Request.QueryString["TicketNo"] != null)
                {
                    this.sTicketNo = Request.QueryString["TicketNo"].ToString();
                 
                }
            }

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            login = userDetails.UserName;

            if (Request.QueryString["AutIntNo"] == null)
                autIntNo = Convert.ToInt32(Session["autIntNo"]);
            else if (!int.TryParse(Request.QueryString["AutIntNo"].ToString(), out this.autIntNo))
                autIntNo = Convert.ToInt32(Session["autIntNo"]);

            //jerry 2012-02-15, use module to create print pdf file
            PrintFileProcess process = new PrintFileProcess(this.connectionString, userDetails.UserName,true);
            PrintFileModuleRepresentationResultLetterMVC p = new PrintFileModuleRepresentationResultLetterMVC(printFlag, repIntNo, sLetterTo, sTicketNo);
            process.BuildPrintFile(p, this.autIntNo, this.printFileName,null,null,true);
 
            return File(p.GetBuffer(), "application/pdf");
        }

    }
}
