﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Stalberg.TMS;
using System.Web.Services;
using System.Web.Services.Protocols;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using SIL.AARTO.BLL.Utility;
using System.IO;
using SIL.AARTO.Web.Resource.Enquiry;
using ceTe.DynamicPDF.Imaging;

namespace SIL.AARTO.Web.Controllers.Enquiry
{
    public partial class EnquiryController : Controller
    {
        public ActionResult ViewOffenceDetailReportHistory()
        {
            // Get user info from session variable
            if (Session["UserLoginInfo"] == null)
                return RedirectToAction("login", "account");

            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            login = userDetails.UserName;
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

             GeneratedDateRole = GetGeneratedDateReplacePrintDateRule(autIntNo);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "NoticeEnquiry_History");
            if (reportPage.Equals(string.Empty))
            {
                //int arnIntNo = arn.AddAuthReportName(autIntNo, "NoticeEnquiry.dplx", "NoticeEnquiry", "system");
                int arnIntNo = arn.AddAuthReportName(autIntNo, "NoticeEnquiry_CoCT_History.dplx", "NoticeEnquiry_History", "system", "");
                reportPage = "NoticeEnquiry_CoCT_History.dplx";
            }

            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Reports\" + reportPage);
            DocumentLayout doc = new DocumentLayout(path);

            if (Request.QueryString["NotIntNo"] == null)
            {
                Response.Write(ViewOffenceDetailReportRes.strWriteMsg);
                Response.Flush();
                Response.End();
                return View();
            }
            try
            {
                int nNotIntNo = Convert.ToInt32(Request.QueryString["NotIntNo"].ToString());

                StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
                StoredProcedureQuery query3 = (StoredProcedureQuery)doc.GetQueryById("Query3");
                query.ConnectionString = this.connectionString;
                query3.ConnectionString = this.connectionString;
                ParameterDictionary parameters = new ParameterDictionary();
                parameters.Add("NotIntNo", nNotIntNo);
                parameters.Add("GenerDateReplacePrintDateRole", GeneratedDateRole);
                Document report = doc.Run(parameters);
                byte[] buffer = report.Draw();
                return File(report.Draw(), "application/pdf");

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                Response.End();
                return View();
            }
        }

    }
}
