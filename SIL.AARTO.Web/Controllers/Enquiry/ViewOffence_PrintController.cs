﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.Enquiry;
using Stalberg.TMS;
using System.Data;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.Resource.Enquiry;
using System.Data.SqlClient;
using System.IO;

namespace SIL.AARTO.Web.Controllers.Enquiry
{
   
    public partial class EnquiryController : Controller
    {
        int MAIN_IMAGE_MAX_WIDTH = 385;
        int MAIN_IMAGE_MAX_HEIGHT = 244;
        int REG_IMAGE_MAX_WIDTH = 146;
        int REG_IMAGE_MAX_HEIGHT = 34;

        int frameIntNo = 0;
        int scImIntNo = 0;
        string imgType = "A";
        string loginUser;

        // string crossHairSettings = "N";
        //int crossHairStyle = 0;

        //string MONEY_FORMAT = "R #,##0.00";
        DataSet scanDS;
        public ActionResult ViewOffence_Print(string notice, string autintno)
        {
            OffenceDetailModel model = new OffenceDetailModel();
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            SIL.AARTO.BLL.Utility.UserLoginInfo userDetails = new BLL.Utility.UserLoginInfo();
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            login = userDetails.UserName;

            //set domain specific variables
            General gen = new General();

            model.backgroundImage = gen.SetBackground(Session["drBackground"]);
            model.styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            model.PageTitle = gen.SetTitle(Session["drTitle"]) + " ~ Offence details";

            model.isShowImgAreaB = true;
            model.isShowImgAreaA = true;
            model.isShowRegArea = true;

            //mrs 20080816 add getting notIntNo from querystring
            if (Request.QueryString["notice"] == null)
            {
                model.Error = ViewOffence_PrintRes.WriteMsg;
                return View(model);
            }
            int notIntNo = Convert.ToInt32(Request.QueryString["notice"].ToString().Trim());
            if (notIntNo > 0)
            {
                //set the viewstate
                //ViewState.Add("notIntNo", notIntNo);
            }
            else
            {
                //Response.Write((string)GetLocalResourceObject("WriteMsg"));
                //Response.End();
                //return;
                model.Error = ViewOffence_PrintRes.WriteMsg;
                return View(model);
            }

            if (Request.QueryString["autintno"] == null)
            {
                //Response.Write((string)GetLocalResourceObject("WriteMsg1"));
                //Response.End();
                //return;

                model.Error = ViewOffence_PrintRes.WriteMsg1;
                return View(model);
            }

            this.autIntNo = Convert.ToInt32(Request.QueryString["autintno"].ToString().Trim());

            model= ShowFrameDetails(model, notIntNo);

            return View(model);
        }
        private OffenceDetailModel ShowFrameDetails(OffenceDetailModel model, int notIntNo)
        {
            NoticeDB notice = new NoticeDB(connectionString);

            //mrs 20080814 notIntNo not changing during revious and next operations


            this.crossHairSettings = GetCrossHairSettings(this.autIntNo);
            this.crossHairStyle = this.GetCrossHairStyle(this.autIntNo);

            frameIntNo = notice.GetFrameForNotice(notIntNo);

            this.crossHairSettings = GetCrossHairSettings(this.autIntNo);
            this.crossHairStyle = this.GetCrossHairStyle(this.autIntNo);

            if (frameIntNo != 0)
            {
                Session["frameIntNo"] = frameIntNo;
                Session["cvPhase"] = 1;
                Session["scImIntNo"] = 0;

                SqlDataReader reader = notice.GetNoticeViewDetails(notIntNo, this.crossHairSettings, this.crossHairStyle);

                while (reader.Read())
                {
                    this.autIntNo = int.Parse(reader["AutIntNo"].ToString());
                    //dls 070120 - add rules for showing cross-hairs
                    Session["ShowCrossHairs"] = reader["ShowCrossHairs"].ToString().Equals("Y") ? true : false;
                    Session["CrossHairStyle"] = Convert.ToInt16(reader["CrossHairStyle"]);

                    model.FilmFrameNo = reader["NotFilmNo"].ToString() + " ~ " + reader["NotFrameNo"].ToString();
                    model.RegistrationNo = reader["NotRegNo"].ToString();
                    model.Speed = reader["NotSpeed1"].ToString();
                    model.Speed2 = reader["NotSpeed2"].ToString();
                    //txtVehMake.Text = reader["NotVehicleMake"].ToString();
                    //txtVehType.Text = reader["NotVehicleType"].ToString();
                    model.TicketNo = reader["NotTicketNo"].ToString();

                    DateTime offenceDate = Convert.ToDateTime(reader["NotOffenceDate"]);
                    //txtOffenceDate.Text = offenceDate.Year + "/" + offenceDate.Month.ToString().PadLeft(2, '0') + "/" + offenceDate.Day.ToString().PadLeft(2, '0');
                    //txtOffenceTime.Text = offenceDate.Hour.ToString().PadLeft(2, '0') + ":" + offenceDate.Minute.ToString().PadLeft(2, '0');
                    model.OffenceDate = offenceDate.Year + "/" + offenceDate.Month.ToString().PadLeft(2, '0') + "/" + offenceDate.Day.ToString().PadLeft(2, '0')
                        + " " + offenceDate.Hour.ToString().PadLeft(2, '0') + ":" + offenceDate.Minute.ToString().PadLeft(2, '0');
                    model.Offencedesc = reader["ChgOffenceDescr"].ToString();

                    model.Location = reader["NotLocDescr"].ToString();

                    LoadOwnerDetails(notIntNo, model);
                    LoadDriverDetails(notIntNo, model);

                    if (reader["NotProxyFlag"].ToString().Equals("Y"))
                    {
                        LoadProxyDetails(notIntNo, model);
                        model.NotProxyFlag = "Y";
                    }
                    else
                    {
                        model.NotProxyFlag = "N";
                    }

                    model.tblOwnerDetails = true;

                    model.FineAmount = Convert.ToDecimal(reader["ChgRevFineAmount"]).ToString(MONEY_FORMAT);
                }
                reader.Close();

                LoadFilmImages(model);

                if (scImIntNo == 0) FindMainImage(model);

                // ddlScan.SelectedIndex = ddlScan.Items.IndexOf(ddlScan.Items.FindByValue(scImIntNo.ToString()));

                bool checkSettings = AllowImageSettings();

                GetImage(model,"imageA", scImIntNo, MAIN_IMAGE_MAX_WIDTH, MAIN_IMAGE_MAX_HEIGHT, checkSettings);

                ScanImageDB scan = new ScanImageDB(this.connectionString);

                ScanImageDetails scanDetails = scan.GetScanImageDetails(scImIntNo);
                int X = scanDetails.XValue;
                int Y = scanDetails.YValue;

                DrawCrossHairs(model,X, Y);

                imgType = "B";
                int scImIntNoB = FindImage(model,"B");
                if (scImIntNoB != 0)
                    //GetImage(wivImageB, scImIntNoB);
                    GetImage(model,"imageB", scImIntNoB, MAIN_IMAGE_MAX_WIDTH, MAIN_IMAGE_MAX_HEIGHT, checkSettings);
                else
                {
                    model.isShowImgAreaB = false;
                    //imgAreaB.Visible = false;
                }
                //wivImageB.Visibility = VisibilityStyle.Hidden;

                imgType = "R";
                int scImIntNoR = FindImage(model,"R");
                if (scImIntNoR != 0)
                {
                    //GetImage(wivRegNo, scImIntNoR);
                    GetImage(model,"regImage", scImIntNoR, REG_IMAGE_MAX_WIDTH, REG_IMAGE_MAX_HEIGHT, false);
                }
                else
                {
                    model.isShowRegArea = false;
                   // regArea.Visible = false;
                    //wivRegNo.Visibility = VisibilityStyle.Hidden;
                }

            }

            return model;
        }

        protected int FindImage(OffenceDetailModel model,string imgType)
        {
            int scanImage = 0;
            string scImgType = string.Empty;

            foreach (var item in model.ScanImageList)
            {

                scImgType = item.Text;
                switch (imgType)
                {
                    case "A":
                        if (scImgType.ToLower().Contains("main "))
                        {
                            scanImage = Convert.ToInt32(item.Value);
                            return scanImage;
                        }
                        break;
                    case "B":
                        if (scImgType.ToLower().Contains("2nd "))
                        {
                            scanImage = Convert.ToInt32(item.Value);
                            return scanImage;
                        }
                        break;
                    case "R":
                        if (scImgType.ToLower().Contains("registration "))
                        {
                            scanImage = Convert.ToInt32(item.Value);
                            return scanImage;
                        }
                        break;
                }

            }

            //for (int i = 0; i < model.ScanImageList.Items; i++)
            //{
            //    scImgType = ddlScan.Items[i].Text;
            //    switch (imgType)
            //    {
            //        case "A":
            //            if (scImgType.ToLower().Contains("main "))
            //            {
            //                scanImage = Convert.ToInt32(ddlScan.Items[i].Value);
            //                return scanImage;
            //            }
            //            break;
            //        case "B":
            //            if (scImgType.ToLower().Contains("2nd "))
            //            {
            //                scanImage = Convert.ToInt32(ddlScan.Items[i].Value);
            //                return scanImage;
            //            }
            //            break;
            //        case "R":
            //            if (scImgType.ToLower().Contains("registration "))
            //            {
            //                scanImage = Convert.ToInt32(ddlScan.Items[i].Value);
            //                return scanImage;
            //            }
            //            break;
            //    }
            //}
            return scanImage;
        }

        private void DrawCrossHairs(OffenceDetailModel model,int X, int Y)
        {
            if (bool.Parse(Session["ShowCrossHairs"].ToString()).Equals(true)
                && X > 0 && Y > 0)
            {
                string style = Session["CrossHairStyle"].ToString();
                SetQueryStringParameter(model,"Type", "4");
                SetQueryStringParameter(model,"XVal", X.ToString());
                SetQueryStringParameter(model,"YVal", Y.ToString());
                SetQueryStringParameter(model,"CrossStyle", style);
                //Drawing draw = new Drawing();

                //draw.CrossHairs(ref this.wivImageA, style, X, Y);
            }
        }

        private void SetQueryStringParameter(OffenceDetailModel model,string paramName, string paramValue)
        {
            if (model.imageAUrl.Contains("&" + paramName + "="))
            {
                int start = model.imageAUrl.IndexOf("&" + paramName + "=");
                int end = model.imageAUrl.IndexOf("&", start + paramName.Length + 1);
                if (end < 0)
                {
                    model.imageAUrl = model.imageAUrl.Substring(0, start) + "&" + paramName + "=" + HttpUtility.UrlEncode(paramValue);
                }
                else
                {
                    model.imageAUrl = model.imageAUrl.Substring(0, start) + "&" + paramName + "=" + HttpUtility.UrlEncode(paramValue) + model.imageAUrl.Substring(end);
                }
            }
            else
            {
                model.imageAUrl += "&" + paramName + "=" + HttpUtility.UrlEncode(paramValue);
            }
        }


        protected void GetImage(OffenceDetailModel model, string image, int scanImage, int width, int height, bool checkSettings)
        {
            string imageUrl = "";
            try
            {
               
                byte[] data = null;
                ImageProcesses img = new ImageProcesses(this.connectionString);
                //data = img.ProcessImage(scanImage, false, "", "", Server.MapPath("images/NoImage.jpg"));
               WebService service = new WebService();
                ScanImageDB imageDB = new ScanImageDB(connectionString);
                int intImageNo = scanImage;
                ScanImageDetails imageDetail = imageDB.GetImageFullPath(intImageNo);
                data = service.GetImagesFromRemoteFileServer(imageDetail);
                //if (imageDetail.FileServer != null)
                //{
                //    if (service.RemoteConnect(imageDetail.FileServer.ImageMachineName, imageDetail.FileServer.ImageShareName, imageDetail.FileServer.RemoteUserName, imageDetail.FileServer.RemotePassword))
                //    {
                //        data = img.ProcessImage(imageDetail.ImageFullPath);
                //    }
                //}

                if (data == null || data.Length == 0)
                {
                    string strNoImagePath = Server.MapPath("~/Content/Image/NoImage.jpg");
                    data = img.ProcessImage(strNoImagePath);
                }

                System.Drawing.Image Bitimage = null;
                using (MemoryStream ms = new MemoryStream(data))
                {
                    Bitimage = System.Drawing.Image.FromStream(ms);
                }

                if (image == "imageA")
                {
                    model.imageAW = Bitimage.Width;
                    model.imageAH = Bitimage.Height;
                }
                else if (image == "imageB")
                {
                    model.imageBW = Bitimage.Width;
                    model.imageBH = Bitimage.Height;
                }
                else if (image== "regImage")
                {
                    model.imageRW = Bitimage.Width;
                    model.imageRH = Bitimage.Height;
                }

                Bitimage.Dispose();
            }
            catch
            {
            }

           

           imageUrl = "Image?ScImIntNo=" + HttpUtility.UrlEncode(Convert.ToString(scanImage))
                + "&Width=" + HttpUtility.UrlEncode(Convert.ToString(width))
                + "&Height=" + HttpUtility.UrlEncode(Convert.ToString(height));

            if (checkSettings)
            {
                foreach (DataRow dr in scanDS.Tables[0].Rows)
                {
                    if (scImIntNo == int.Parse(dr["ScImIntNo"].ToString()))
                    {
                        decimal contrast;
                        decimal brightness;

                        if (decimal.TryParse(dr["Contrast"].ToString(), out contrast))
                            imageUrl += "&SavedContrast=" + contrast;

                        if (decimal.TryParse(dr["Brightness"].ToString(), out brightness))
                            imageUrl += "&SavedBrightness=" + brightness * 10.0m;
                    }
                }
            }

            if (image == "imageA")
            {
                model.imageAUrl = imageUrl;
            }
            else if (image == "imageB")
            {
                model.imageBUrl = imageUrl;
            }
            else if (image == "regImage")
            {
                model.imageRegUrl = imageUrl;
            }



        }

        private bool AllowImageSettings()
        {
            //dls 100125 - check rule for settings before showing changed image settings
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = this.autIntNo;
            arDetails.ARCode = "2560";
            arDetails.LastUser = this.loginUser;

            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            ar.SetDefaultAuthRule();
            return arDetails.ARString == "Y" ? true : false;
        }

        private void LoadFilmImages(OffenceDetailModel model)
        {
            ScanImageDB scan = new ScanImageDB(connectionString);

            //DataSet scanDS;
            //scanDS = scan.GetScanImageListDS(0, frameIntNo, "%", "");
            scanDS = scan.GetScanImageListDS(0, frameIntNo, "%");
            Dictionary<string, string> Diclist = new Dictionary<string, string>();
            if (scanDS != null && scanDS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < scanDS.Tables[0].Rows.Count; i++)
                {
                    SelectListItem item = new SelectListItem();
                    Diclist.Add(scanDS.Tables[0].Rows[i]["ImageDetails"].ToString(), scanDS.Tables[0].Rows[i]["ScImIntNo"].ToString());
                }
                SelectList list = new SelectList(Diclist,"Value","Key");
                model.ScanImageList = list;
            }


        }

        protected bool FindMainImage(OffenceDetailModel model)
        {
            bool foundScan = false;
            string scImgType = string.Empty;
            foreach (var item in model.ScanImageList)
            {
                if (foundScan == true)
                {
                    Session["scImIntNo"] = item.Value;
                    scImIntNo = Convert.ToInt32(item.Value);
                    break;
                }
                if (scImIntNo == Convert.ToInt32(item.Value) || scImIntNo == 0)
                {
                    scImgType = item.Text;
                    switch (imgType)
                    {
                        case "A":
                            if (scImgType.ToLower().Contains("main "))
                            {
                                foundScan = true;
                                if (scImIntNo == 0)
                                {
                                    Session["scImIntNo"] = item.Value;
                                    scImIntNo = Convert.ToInt32(item.Value);
                                    return foundScan;
                                }
                            }
                            break;
                        case "B":
                            if (scImgType.ToLower().Contains("2nd "))
                            {
                                foundScan = true;
                                if (scImIntNo == 0)
                                {
                                    Session["scImIntNo"] = item.Value;
                                    scImIntNo = Convert.ToInt32(item.Value);
                                    return foundScan;
                                }
                            }
                            break;
                        case "R":
                            if (scImgType.ToLower().Contains("registration "))
                            {
                                foundScan = true;
                                if (scImIntNo == 0)
                                {
                                    Session["scImIntNo"] = item.Value;
                                    scImIntNo = Convert.ToInt32(item.Value);
                                    return foundScan;
                                }
                            }
                            break;
                    }
                }
            }


            //for (int i = 0; i < ddlScan.Items.Count; i++)
            //{
            //    if (foundScan == true)
            //    {
            //        Session["scImIntNo"] = ddlScan.Items[i].Value;
            //        scImIntNo = Convert.ToInt32(ddlScan.Items[i].Value);
            //        break;
            //    }
            //    if (scImIntNo == Convert.ToInt32(ddlScan.Items[i].Value) || scImIntNo == 0)
            //    {
            //        scImgType = ddlScan.Items[i].Text;
            //        switch (imgType)
            //        {
            //            case "A":
            //                if (scImgType.ToLower().Contains("main "))
            //                {
            //                    foundScan = true;
            //                    if (scImIntNo == 0)
            //                    {
            //                        Session["scImIntNo"] = ddlScan.Items[i].Value;
            //                        scImIntNo = Convert.ToInt32(ddlScan.Items[i].Value);
            //                        return foundScan;
            //                    }
            //                }
            //                break;
            //            case "B":
            //                if (scImgType.ToLower().Contains("2nd "))
            //                {
            //                    foundScan = true;
            //                    if (scImIntNo == 0)
            //                    {
            //                        Session["scImIntNo"] = ddlScan.Items[i].Value;
            //                        scImIntNo = Convert.ToInt32(ddlScan.Items[i].Value);
            //                        return foundScan;
            //                    }
            //                }
            //                break;
            //            case "R":
            //                if (scImgType.ToLower().Contains("registration "))
            //                {
            //                    foundScan = true;
            //                    if (scImIntNo == 0)
            //                    {
            //                        Session["scImIntNo"] = ddlScan.Items[i].Value;
            //                        scImIntNo = Convert.ToInt32(ddlScan.Items[i].Value);
            //                        return foundScan;
            //                    }
            //                }
            //                break;
            //        }
            //    }
            //}

            return foundScan;
        }

    }
}
