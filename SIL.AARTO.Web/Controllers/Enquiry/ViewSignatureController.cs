﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.Enquiry;

namespace SIL.AARTO.Web.Controllers.Enquiry
{
    public partial class EnquiryController : Controller
    {
        SIL.AARTO.DAL.Services.ImageFileServerService imageFileServerService = new SIL.AARTO.DAL.Services.ImageFileServerService();
        SIL.AARTO.DAL.Services.NoticeSummonsService noticeSummonsService = new SIL.AARTO.DAL.Services.NoticeSummonsService();
        SIL.AARTO.DAL.Services.SummonsService summonsService = new SIL.AARTO.DAL.Services.SummonsService();

        public ActionResult ViewSignature()
        {
            // Get user info from session variable
            if (Session["UserLoginInfo"] == null)
                return RedirectToAction("login", "account");

            int notIntNo;
            if (Request.QueryString["NotIntNo"] == null || !int.TryParse(Request.QueryString["NotIntNo"].ToString(), out notIntNo))
            {
                Response.Write(SIL.AARTO.Web.Resource.Enquiry.ViewSignature.NoNotIntNoParam);
                Response.Flush();
                Response.End();
                return View();
            }

            string summonsNo = string.Empty;
            SIL.AARTO.DAL.Entities.NoticeSummons noticeSummonsEntity = noticeSummonsService.GetByNotIntNo(notIntNo).FirstOrDefault();
            if (noticeSummonsEntity != null)
            {
                SIL.AARTO.DAL.Entities.Summons summonsEntity = summonsService.GetBySumIntNo(noticeSummonsEntity.SumIntNo);
                if (summonsEntity != null && summonsEntity.IsMobile != null && summonsEntity.IsMobile.Value)
                    summonsNo = summonsEntity.SummonsNo.Replace("/", "").Replace("-", "");
                else
                {
                    Response.Write(SIL.AARTO.Web.Resource.Enquiry.ViewSignature.IsNotMobile);
                    Response.Flush();
                    Response.End();
                    return View();
                }
            }
            else
            {
                Response.Write(SIL.AARTO.Web.Resource.Enquiry.ViewSignature.InvalidNotIntNo);
                Response.Flush();
                Response.End();
                return View();
            }

            MobileSignatureModel model = new MobileSignatureModel();
            model.NotIntNo = notIntNo;
            InitModel(model, summonsNo);

            return View(model);
        }

        private void InitModel(MobileSignatureModel model, string summonsNo)
        {
            using (IDataReader reader = imageFileServerService.GetAllTypesOfImgs(model.NotIntNo, 0, ""))
            {
                while (reader.Read())
                {
                    string imageMachineName = reader["ImageMachineName"].ToString().Trim();
                    string imageShareName = reader["ImageShareName"].ToString().Trim();
                    string imagePath = reader["ImagePath"].ToString().Trim();
                    string imageUrl = @"\\" + imageMachineName + @"\" + imageShareName + @"\" + imagePath;

                    if (imagePath.ToUpper().Contains(summonsNo + "_TOSIG"))
                    {
                        model.TrafficOfficerSignatureImageURL = imageUrl;
                    }
                    else if (imagePath.ToUpper().Contains(summonsNo + "_OFFSIG"))
                    {
                        model.OffenderSignatureImageURL = imageUrl;
                    }
                }
            }
        }
    }
}
