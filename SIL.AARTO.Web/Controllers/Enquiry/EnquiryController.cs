﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Configuration;
using SIL.AARTO.Web.ViewModels.Enquiry;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource.Enquiry;
using Stalberg.TMS;
using Stalberg.TMS.Data;
using SIL.AARTO.BLL.Utility;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.BLL.CameraManagement;
using SIL.Common;
using SIL.AARTO.DAL.Services;
using System.Data.Entity.Core.Objects;
using SIL.AARTO.BLL.PrintManagement;

namespace SIL.AARTO.Web.Controllers.Enquiry
{
    [AARTOErrorLog, LanguageFilter]
    public partial class EnquiryController : Controller
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
        SIL.AARTO.BLL.Utility.UserLoginInfo userDetails = new BLL.Utility.UserLoginInfo();
        string login = "";
        int autIntNo = 0;
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        private string ticketProcessor;// jerry 2011-1-13 add
        private int processorSign;// jerry 2011-1-13 add
        public ActionResult EnquiryList(EnquiryModel model, int page = 0, int IsRedirect= 0)
        {
            if (IsRedirect == 1)
            {
                return RedirectToAction("EnquiryList", "Enquiry");
            }
          
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            model.NoticeNoLookupSubmitToController = "Enquiry";
            model.NoticeNoLookupSubmitToAction = "EnquiryList";
            if (TempData["EnquiryModel"] != null)
            {
                model = (EnquiryModel)TempData["EnquiryModel"];
            }
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            login = userDetails.UserName;

            model.isShowBook = false;
            model.isShowResult = false;
            model.PageSize = Config.PageSize;
            int totalCount = 0;

            //2014-05-08 Heidi added for display autname in enquiry page(5249)
            #region GetAutName
            if (autIntNo>0)
            {
                if (Session["autName"]!= null)
                {
                    model.AuthorityName = Convert.ToString(Session["autName"]);
                }
                else
                {
                    AuthorityService authorityService = new AuthorityService();
                    SIL.AARTO.DAL.Entities.Authority authority = authorityService.GetByAutIntNo(autIntNo);
                    if (authority != null)
                    {
                        model.AuthorityName = authority.AutName;
                    }
                }
            }
            #endregion

            if (model.Prefix == null && model.Sequence == null)
            {
                DataBind(model);
            }
            if (model.SelectSubmit=="1")
            {
                SetCDV(model);
            }

            if (!validate(model))
            {
                model.ErrorMsg = EnquiryManage.Error1;
            }
            else
            {
                model.IsShowCSCode = true;
                if (string.IsNullOrEmpty(model.Since))
                {
                    model.Since = "2000-01-01";
                }
                model.searchValue = model.searchValue.Replace("/", "").Replace("-", "");
                model.minStatus = CheckMinimumStatusRule();

                // string autIntNo = Session["DefaultAuthority"].ToString();
                NoticeReportDB noticeDB = new NoticeReportDB(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());
                //2014-08-13 Heidi changed for fixing time out issue.(bontq1298)
                DataSet ds = noticeDB.GetNoticeSearchDS_GetPaged(0, model.searchField, model.searchValue, DateTime.Parse(model.Since), model.minStatus, page+1, model.PageSize, out totalCount);

                var printHelper = new PrintFileManagement();

                if (ds != null && ds.Tables[1].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                    {
                        NoticeDetail notice = new NoticeDetail();
                        notice.NotIntNo = ds.Tables[1].Rows[i]["NotIntNo"].ToString();
                        notice.NotTicketNo = ds.Tables[1].Rows[i]["NotTicketNo"].ToString().Trim();
                        notice.NotOffenceDate = Convert.ToString(ds.Tables[1].Rows[i]["NotOffenceDate"]) == "" ? "" : Convert.ToDateTime(ds.Tables[1].Rows[i]["NotOffenceDate"]).ToString("yyyy/MM/dd");
                        notice.NotRegNo = ds.Tables[1].Rows[i]["NotRegNo"].ToString().Trim();
                        notice.AutName = ds.Tables[1].Rows[i]["AutName"].ToString().Trim();
                        notice.ChgOffenceCode = ds.Tables[1].Rows[i]["ChgOffenceCode"].ToString().Trim();
                        notice.PaymentDate = Convert.ToString(ds.Tables[1].Rows[i]["PaymentDate"]) == "" ? "" : Convert.ToDateTime(ds.Tables[1].Rows[i]["PaymentDate"]).ToString("yyyy/MM/dd");
                        notice.CSCode = ds.Tables[1].Rows[i]["CSCode"].ToString().Trim();
                        notice.CSDescr = ds.Tables[1].Rows[i]["CSDescr"].ToString().Trim();
                        notice.ChgRevFineAmount = double.Parse(ds.Tables[1].Rows[i]["ChgRevFineAmount"].ToString()).ToString("f2");
                        notice.ChgContemptCourt = ds.Tables[1].Rows[i]["ChgContemptCourt"].ToString().Trim() != "" ? double.Parse(ds.Tables[1].Rows[i]["ChgContemptCourt"].ToString()).ToString("f2") : "";
                        notice.Offender = ds.Tables[1].Rows[i]["Offender"].ToString().Trim();
                        notice.Representation = ds.Tables[1].Rows[i]["Representation"].ToString().Trim();
                        notice.LetterTo = ds.Tables[1].Rows[i]["LetterTo"].ToString().Trim();
                        notice.FixOfficerError = GetFixOfficerError(ds.Tables[1].Rows[i]["NotIntNo"], ds.Tables[1].Rows[i]["NotIsOfficerError"], ds.Tables[1].Rows[i]["ChargeStatus"], ds.Tables[1].Rows[i]["ChgPaidDate"], login);
                        model.IsShowCSCode = IsShowCSCode((int)ds.Tables[1].Rows[0]["AutIntNo"]);
                        notice.HasComment = ds.Tables[1].Rows[i]["HasComment"].ToString().Trim();
                        notice.IsEnable = IsEnable(ds.Tables[1].Rows[i]["NotFilmType"]);
                        // Heidi 2014-05-08 added CourtName,CourtDate for display on enquiry page(5249)
                        notice.NotCourtName = ds.Tables[1].Rows[i]["NotCourtName"].ToString().Trim();
                        notice.SumCourtDate = Convert.ToString(ds.Tables[1].Rows[i]["SumCourtDate"]) == "" ? "" : Convert.ToDateTime(ds.Tables[1].Rows[i]["SumCourtDate"]).ToString("yyyy/MM/dd");
                        //Jerry 2014-08-07 add
                        //Heidi 2014-10-16 checked IsMobile is null(5367)
                        notice.IsMobile = ds.Tables[1].Rows[i]["IsMobile"].ToString() == "" ? false : Convert.ToBoolean(ds.Tables[1].Rows[i]["IsMobile"].ToString().Trim());
                        notice.NotFilmType = ds.Tables[1].Rows[i]["NotFilmType"] == null ? "" : ds.Tables[1].Rows[i]["NotFilmType"].ToString();
                        notice.MobileControlDocumentGeneratedDate = ds.Tables[1].Rows[i]["MobileControlDocumentGeneratedDate"] == null ? "" : ds.Tables[1].Rows[i]["MobileControlDocumentGeneratedDate"].ToString();
                        
                        if (notice.IsMobile)
                        {
                            model.HasMobile = true;
                        }
                        if (notice.IsMobile && notice.NotFilmType == "M")
                        {
                            model.HasMobleS56 = true;
                        }
                        
                        // IsEnabled is used to work out if this is a handwritten document.
                        if (notice.IsEnable == false)
                        {
                            notice.Source = NoticeSource.camera;
                            notice.HasDocument = System.IO.File.Exists(printHelper.GetPrintFilePath(notice.NotIntNo,notice.NotTicketNo));
                        }
                        else
                        {
                            notice.Source = NoticeSource.unknown;
                            notice.HasDocument = true;
                        }

                        model.NoticeList.Add(notice);
                    }
                    model.isShowResult = true;
                }
                else
                {
                    //bind bookList
                    if (BindBookGrid(model))
                    {
                        model.isShowBook = true;
                    }
                    else
                    {
                        model.ErrorMsg = EnquiryManage.Error3;
                    }
                }
            }
            model.IsCofCT = GetIsCofCT();
            model.TotalCount = totalCount;
            model.PageIndex = page;
            Session["ISFromNoticeLookup"] = 0;
            return View("EnquiryList", model);
        }

        public FileResult CamaraDocument(int notIntNo, NoticeSource source)
        {
            var printHelper = new PrintFileManagement();

            return File(printHelper.GetPrintFilePath(notIntNo), "application/pdf");
        }

        private bool GetIsCofCT()
        {
            bool f = false;
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            //2014-09-28 Heidi changed for According to the template decided to use which data(bontq1523)
            //string autCode = AuthorityManager.GetAutCode(autIntNo);
            //if (autCode.ToLower().Trim() == "cp")
            //{
            //    f = true;
            //}
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "NoticeEnquiry");
            if (!string.IsNullOrEmpty(reportPage))
            {
                if (reportPage.Trim() == "NoticeEnquiry_CoCT.dplx")
                {
                    f = true;
                }
                
            }
            return f;
        }

        public bool IsShowCSCode(int AutIntNo)
        {
            bool f = true;
            if (!AuthorityRulesManager.GetIsOrNotShowStatusCodeOfViewOffence_Search(login, AutIntNo))
            {
                f = false;

            }
            return f;

        }

        private string GetFixOfficerError(object NotIntNo, object NotIsOfficerError, object ChargeStatus, object ChgPaidDate, string login)
        {
            string fixOfficerError = "";

            try
            {
                //jerry 2010/7/22 add fix officer errors
                int userIntno = 0;
                if (userDetails != null)
                {
                    userIntno = userDetails.UserIntNo;
                }
                List<string> userRoleNameList = new AARTOUserRoleDB(connectionString).GetAARTOUserRoleNameByUserID(userIntno);
                   

                //Role == Supervisor
                string DmsWebUrl = string.Empty;
                string Ip = string.Empty;

                Ip = GetServerIp();

                string HostName = Request.ServerVariables["SERVER_NAME"];
                if (String.IsNullOrEmpty(Ip))
                {
                    DmsWebUrl = ConfigurationManager.AppSettings["DMSDomainURL"];
                }
                if (ConfigurationManager.AppSettings["DMSDomainURL"].IndexOf(Ip) >= 0
                    || ConfigurationManager.AppSettings["DMSDomainURL"].IndexOf(HostName) >= 0)
                {
                    DmsWebUrl = ConfigurationManager.AppSettings["DMSDomainURL"];
                }
                else if (ConfigurationManager.AppSettings["DMSDomainExternalURL"].IndexOf(Ip) >= 0
                   || ConfigurationManager.AppSettings["DMSDomainExternalURL"].IndexOf(HostName) >= 0)
                {
                    DmsWebUrl = ConfigurationManager.AppSettings["DMSDomainExternalURL"];
                }
                else
                {
                    DmsWebUrl = ConfigurationManager.AppSettings["DMSDomainURL"];
                }

                if (userRoleNameList.Contains("Supervisor"))
                {

                    //Notice.NotIsOfficerError == true && Charge.ChargeStatus < 900 
                    //&& Charge.ChgPaidDate IS NULL

                    if (Convert.ToBoolean(NotIsOfficerError) &&
                        Convert.ToInt32(ChargeStatus.ToString()) < 900 &&
                        string.IsNullOrEmpty(ChgPaidDate.ToString()))
                    {
                        string tempBatchDocID = new FrameDB(connectionString).GetFrameDmsDocumentNoByNoticeNotIntNo(
                            Convert.ToInt32(NotIntNo));

                        if (tempBatchDocID != "" && tempBatchDocID.Contains("_"))
                        {
                            tempBatchDocID = tempBatchDocID.Split(new Char[] { '_' })[0];
                        }

                        string baseUrl = String.Format(DmsWebUrl
                            + "/Management/EditDocInputs.aspx?BatchDocumentId={0}&UserName={1}", tempBatchDocID, login);

                        fixOfficerError = "<a href = '" + UrlVerify.GetSecretUrl(baseUrl, tempBatchDocID + login) + "' target='blank'>Fix Officer Errors</a>";
                    }

                }
            }
            catch { }

            return fixOfficerError;
        }

        public bool IsEnable(object o)
        {
            string filmtype = "";
            if (o != null)
            {
                filmtype = Convert.ToString(o);

            }
            if (filmtype.ToLower() == "m" || filmtype.ToLower() == "h")
            {
                return true;
            }

            return false;

        }

        // 2011-03-11 Added before redirect to dms  need to check ip or host name
        private string GetServerIp()
        {
            string serverIP = string.Empty;
            try
            {
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    serverIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                }
                else
                {
                    if (Request.ServerVariables["REMOTE_ADDR"] != null)
                    {
                        serverIP = Request.ServerVariables["REMOTE_ADDR"];
                    }
                }
            }
            catch (Exception ex)
            {
                // string msg = ex.Message;
                throw ex;
            }
            return serverIP;
        }

        private bool BindBookGrid(EnquiryModel model)
        {
            bool IsSucced = false;

            try
            {
                NoticeDB db = new NoticeDB(connectionString);
                DataSet ds = null;

                if (model.searchField != null && model.searchField == "TicketNo")
                {

                    //if (model.searchValue != null && model.searchValue.IndexOf("/") > 0) // Heidi 2013-06-25 changed
                    if (model.originalTicketNo != null && model.originalTicketNo.IndexOf("/") > 0)
                    {
                        int sequence = 0;
                        string seq = string.Empty;
                        string[] split = model.originalTicketNo.Trim().Split('/');
                        if (split.Length == 4 && int.TryParse(split[1], out sequence))
                        {
                            SqlDataReader read = new AuthorityDB(connectionString).GetAuthorityDetailsByAutNo(split[2].Trim());
                            if (read != null && read.Read())
                            {
                                AuthorityRulesDetails rule9300 = new AuthorityRulesDetails();
                                rule9300.AutIntNo = (int)read["AutIntNo"];
                                rule9300.ARCode = "9300";
                                rule9300.LastUser = login;
                                DefaultAuthRules authRule = new DefaultAuthRules(rule9300, connectionString);
                                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();
                                //This is a problem because the Handwritten offences use 00000 numbers for Sequence Number
                                seq = split[1].Trim().PadLeft(value.Key, Convert.ToChar("0"));
                                read.Close();
                            }
                            else
                            {
                                seq = split[1].Trim();
                            }
                            ds = db.GetBMDocument(split[0].Trim(), seq);
                        }
                    }
                    else
                    {
                        // if (model.NoticeNumber.Trim() == model.searchValue.Trim())
                        //{
                        if (model.Prefix != null && model.Sequence != null)
                        {
                            ds = db.GetBMDocument(model.Prefix.Trim(), model.Sequence.Trim());
                        }
                        //}
                    }
                }

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    IsSucced = true;

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Book book = new Book();
                        book.AaBMDocNo = ds.Tables[0].Rows[i]["AaBMDocNo"].ToString();
                        book.AaBmIsCancelled = ds.Tables[0].Rows[i]["AaBmIsCancelled"].ToString();
                        book.DocumentStatus = ds.Tables[0].Rows[i]["DocumentStatus"].ToString();
                        book.AaBMDepotDescription = ds.Tables[0].Rows[i]["AaBMDepotDescription"].ToString().Trim();
                        book.TOSName = ds.Tables[0].Rows[i]["TOSName"].ToString().Trim();
                        book.AaBMBookNo = ds.Tables[0].Rows[i]["AaBMBookNo"].ToString().Trim();
                        book.AaBMBookTypeName = ds.Tables[0].Rows[i]["AaBMBookTypeName"].ToString();
                        book.BookStatus = ds.Tables[0].Rows[i]["BookStatus"].ToString().Trim();
                        model.BookList.Add(book);
                    }

                }
            }
            catch
            {
                IsSucced = false;
            }
            return IsSucced;
        }

        private bool validate(EnquiryModel model)
        {
            bool value = true;
            model.btnAllDetailsEnabled = true;

            if (!string.IsNullOrEmpty(model.searchField))
            {
                switch (model.searchField)
                {
                    case "RegNo":
                        if (string.IsNullOrEmpty(model.RegNo))
                            value = false;
                        else
                            model.searchValue = model.RegNo.Trim();
                        break;
                    case "IDNo":
                        if (string.IsNullOrEmpty(model.IDNo))
                            value = false;
                        else
                            model.searchValue = model.IDNo.Trim();
                        break;
                    case "TicketNo":
                        model.btnAllDetailsEnabled = false;
                        if (string.IsNullOrEmpty(model.TicketNo))
                            value = false;
                        else
                        {
                            model.searchValue = model.TicketNo.Trim();
                            if (model.TicketNo != null)
                            {
                                model.originalTicketNo = model.TicketNo.Trim();
                            }
                        }
                        break;
                    case "SummonsNO":
                        model.btnAllDetailsEnabled = false;
                        if (string.IsNullOrEmpty(model.SummonsNO))
                            value = false;
                        else
                            model.searchValue = model.SummonsNO.Trim();
                        break;
                    case "CaseNO":
                        model.btnAllDetailsEnabled = false;
                        if (string.IsNullOrEmpty(model.CaseNO))
                            value = false;
                        else
                            model.searchValue = model.CaseNO.Trim();
                        break;
                    case "WOANO":
                        model.btnAllDetailsEnabled = false;
                        if (string.IsNullOrEmpty(model.WOANO))
                            value = false;
                        else
                            model.searchValue = model.WOANO.Trim();
                        break;
                    case "Names":
                        if (string.IsNullOrEmpty(model.Name))
                            value = false;
                        else
                            model.searchValue = model.Initials + "*" + model.Name;
                        break;
                    case "TickSequenceNo":
                        model.btnAllDetailsEnabled = false;
                        if (string.IsNullOrEmpty(model.TicketNo))
                            value = false;
                        else
                            model.searchValue = model.TicketNo;
                        break;
                    case "EasyPayNo":
                        model.btnAllDetailsEnabled = false;
                        if (string.IsNullOrEmpty(model.EasyPayNo))
                            value = false;
                        else
                            model.searchValue = model.EasyPayNo;
                        break;
                    default:
                        value = false;
                        break;
                }
                if (!string.IsNullOrEmpty(model.Since))
                {
                    DateTime date = DateTime.Now;
                    if (!DateTime.TryParse(model.Since, out date))
                    {
                        value = false;
                    }
                }
            }
            else
            {
                value = false;
            }
            return value;
        }


        private int CheckMinimumStatusRule()
        {
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = int.Parse(Session["autIntNo"].ToString());
            rule.ARCode = "4605";
            rule.LastUser = userInfo.UserName;

            DefaultAuthRules authRule = new DefaultAuthRules(rule, ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();
            return value.Key; //rule.ARNumeric;
        }

        public void DataBind(EnquiryModel model)
        {
            // jerry 2011-1-13 add
            ticketProcessor = Session["TicketProcessor"] != null ? Session["TicketProcessor"].ToString() : "TMS";
            if (ticketProcessor.Equals("TMS") || ticketProcessor.Equals("Cip_CofCT") || ticketProcessor.Equals("CiprusPI"))
            {
                processorSign = 1;
            }
            else
            {
                processorSign = 2;
            }


            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            login = userDetails.UserName;

            AuthorityDB db = new AuthorityDB(connectionString);
            AuthorityDetails ad = db.GetAuthorityDetails(autIntNo);
            //this.txtAuthCode.Text = ad.AutNo.Trim();
            if (processorSign == 1)// jerry 2011-1-13 add, processor is TMS
            {
                // this.txtAuthCode.Text = ad.AutNo;
                model.AuthCode = ad.AutNo;
            }
            else// processor is AARTO
            {
                // this.txtNumber.Text = ad.ENatisAuthorityNumber.ToString();
                model.Sequence = ad.ENatisAuthorityNumber.ToString();
            }

            NoticeTypeDB noticeType = new NoticeTypeDB(this.connectionString);
            Stalberg.TMS.NoticeTypeDetails ntDetails = noticeType.GetNoticeTypeDetailsByCode("6");
            NoticePrefixDB noticePrefix = new NoticePrefixDB(connectionString);



            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.ARCode = "4905";
            rule.AutIntNo = autIntNo;
            rule.LastUser = login;

            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> value1 = authRule.SetDefaultAuthRule();

            Stalberg.TMS.NoticePrefixDetails npDetails = noticePrefix.GetNoticePrefixDetailsByType(ntDetails.NTIntNo, autIntNo);
            if (value1.Value == "N")
            {
                //this.txtNoticePrefix.Text = !string.IsNullOrEmpty(npDetails.NPrefix) ? npDetails.NPrefix : "";
                model.Prefix = !string.IsNullOrEmpty(npDetails.NPrefix) ? npDetails.NPrefix : "";
            }
        }

        public void SetCDV(EnquiryModel model)
        {
            char cTemp = Convert.ToChar("0");
            int nCdv;
            int nSeq;
            AuthorityRulesDetails rule9300 = new AuthorityRulesDetails();
            rule9300.AutIntNo = autIntNo;
            rule9300.ARCode = "9300";
            rule9300.LastUser = login;
            DefaultAuthRules authRule = new DefaultAuthRules(rule9300, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();
            //This is a problem because the Handwritten offences use 00000 numbers for Sequence Number
            if (model.Sequence != null)
            {
                model.Sequence = model.Sequence.PadLeft(value.Key, cTemp);

                if (int.TryParse(model.Sequence.Trim(), out nSeq))
                {
                    nCdv = SIL.AARTO.BLL.Utility.TicketNumber.GetCdvOfTicketNumber(nSeq, model.Prefix.Trim(), model.AuthCode.Trim());
                    if (nCdv != -1)
                    {
                        model.CDV = nCdv.ToString();

                    }
                }
            }
        }
    }
}
