﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility;
using Stalberg.TMS;
using Stalberg.TMS.Data.Datasets;
using System.IO;
using SIL.AARTO.Web.Resource.Enquiry;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace SIL.AARTO.Web.Controllers.Enquiry
{
    public partial class EnquiryController : Controller
    {
        public ActionResult ViewHistoricalSummons_Report(string searchField, string searchValue, string Since)
        {
            if (Session["UserLoginInfo"] == null)
                return RedirectToAction("login", "account");
           
           // userDetails = (UserLoginInfo)Session["UserLoginInfo"];
          //  login = userDetails.UserName;
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

          

            // Setup the report
            string reportPage = "HistoricalSummonsSummary.rpt";
            string reportPath = Server.MapPath("~/Reports/HistoricalSummonsSummary.rpt");

            if (!System.IO.File.Exists(reportPath))
            {
               
                string error= string.Format(ViewHistoricalSummons_ReportRes.error, reportPage);
                Response.Write(error);
                Response.Flush();
                Response.End();
                return View();

            }

            this.report = new ReportDocument();
            report.Load(reportPath);

            SummonsDB summonWithdrawnReissue = new SummonsDB(this.connectionString);
            searchValue = searchValue.Replace("/", "").Replace("-", "");
            DateTime dtSince;
            if (DateTime.TryParse(Since, out dtSince))
            { }
            else
            {
                dtSince = new DateTime(2000, 1, 1);
            }
            dsOfenceSummary ds = (dsOfenceSummary)summonWithdrawnReissue.GetSummonsWithdrawnReissueSearchDS(0, searchField, searchValue, dtSince);

            // Populate the report
            report.SetDataSource(ds.Tables[1]);
            ds.Dispose();

            // Export the PDF file
            MemoryStream ms = new MemoryStream();
            ms = (MemoryStream)report.ExportToStream(ExportFormatType.PortableDocFormat);
            report.Dispose();

            //stuff the PDF file into rendering stream
            //first clear everything dynamically created and just send PDF file
            //Response.ClearContent();
            //Response.ClearHeaders();
            //Response.ContentType = "application/pdf";
            //Response.BinaryWrite(ms.ToArray());
            //Response.End();
            //return View();

            return File(ms.ToArray(), "application/pdf");
        }

    }
}
