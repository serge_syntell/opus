﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility;
using Stalberg.TMS;
using System.Data.SqlClient;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using Stalberg.TMS.Data.Datasets;
using System.IO;
using SIL.AARTO.Web.Resource.Enquiry;
using CrystalDecisions.Shared;

namespace SIL.AARTO.Web.Controllers.Enquiry
{
    public partial class EnquiryController : Controller
    {
        string sReportType = "PDF";
         string thisPageURL = "";
        public ActionResult OutstandingFinesReportViewer(string searchField, string searchValue, string Since)
        {

            if (Session["UserLoginInfo"] == null)
                return RedirectToAction("login", "account");
            //if (Session["userIntNo"] == null)
            //    return RedirectToAction("login", "account");
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            login = userDetails.UserName;
            autIntNo = Convert.ToInt32(Session["autIntNo"]);


            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            thisPageURL = "OutstandingFinesReportViewer";



            //dls 090625 - need to get minimum status for showing fines
            AuthorityRulesDetails ard = new AuthorityRulesDetails();
            ard.AutIntNo =autIntNo;
            ard.ARCode = "4605";
            ard.LastUser = login;

            DefaultAuthRules def = new DefaultAuthRules(ard, this.connectionString);

            KeyValuePair<int, string> minStatus = def.SetDefaultAuthRule();

           // sReportType = (ViewState["ReportType"] == null ? "PDF" : ViewState["ReportType"].ToString());

           // autIntNo = Convert.ToInt32(Session["autIntNo"]);
            searchValue = searchValue.Replace("/", "").Replace("-", "");
            DateTime dtSince;
            if (DateTime.TryParse(Since, out dtSince))
            { }
            else
            {
                dtSince = new DateTime(2000, 1, 1);
            }

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = new SqlCommand("OutstandingFinesReport", new SqlConnection(connectionString));
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = 0;
            da.SelectCommand.Parameters.Add("@ColumnName", SqlDbType.VarChar, 15).Value = searchField;
            da.SelectCommand.Parameters.Add("@ColumnValue", SqlDbType.VarChar, 30).Value = searchValue;
            da.SelectCommand.Parameters.Add("@DateSince", SqlDbType.DateTime).Value = dtSince;
            da.SelectCommand.Parameters.Add("@MinStatus", SqlDbType.Int).Value = minStatus.Key;

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "OutstandingFinesReport");
            if (reportPage.Equals(""))
            {
                arn.AddAuthReportName(autIntNo, "OutstandingFinesReport.rpt", "OutstandingFinesReport", "system", "");
                reportPage = "OutstandingFinesReport.rpt";
            }

            string reportPath = Server.MapPath("~/Reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "OutstandingFinesReport");
            if (!System.IO.File.Exists(reportPath))
            {

                string error = string.Format(OutstandingFinesReportViewerRes.error, reportPage);
                Response.Write(error);
                Response.Flush();
                Response.End();
                return View();
            }
            else if (!sTemplate.Equals(""))
            {
                templatePath = Server.MapPath("~/Templates/" + sTemplate);

                if (!System.IO.File.Exists(templatePath))
                {
                    string error = string.Format(OutstandingFinesReportViewerRes.error1, sTemplate);
                    Response.Write(error);
                    Response.Flush();
                    Response.End();
                    return View();
                }
            }

            //****************************************************



            ReportDocument report = new ReportDocument();
            report.Load(reportPath);

            dsOutstandingFinesReport ds = new dsOutstandingFinesReport();
            ds.DataSetName = "dsOutstandingFinesReport";
            try
            {
                da.Fill(ds);
                report.SetDataSource(ds.Tables[1]);
                ds.Dispose();
            }
            catch (Exception e)
            {
                Response.Write(string.Format(OutstandingFinesReportViewerRes.strWriteMsg, e.Message));
                return View();
            }
            finally
            {
                da.Dispose();
            }

            //report.SetDataSource(ds.Tables[1]);
            //ds.Dispose();

            // Export the pdf file
            using (MemoryStream ms = (MemoryStream)report.ExportToStream(ExportFormatType.PortableDocFormat))
            {
                //Response.ClearContent();
                //Response.ClearHeaders();
                //Response.ContentType = "application/pdf";
                //Response.BinaryWrite(ms.ToArray());
                report.Dispose();//2014-07-03 Heidi added for fixing Crystal reports error problem
                return File(ms.ToArray(), "application/pdf");
            }

            //2014-07-03 Heidi Comment out for fixing Crystal reports error problem
            //da.Dispose();
           //report.Dispose();
           //return View();
        }

      

    }
}
