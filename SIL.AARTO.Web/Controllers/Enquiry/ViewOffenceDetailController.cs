﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Stalberg.TMS;
using System.Configuration;
using SIL.AARTO.Web.ViewModels.Enquiry;
using SIL.AARTO.Web.Resource.Enquiry;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.Web.Controllers.Enquiry
{
    
    public partial class EnquiryController : Controller
    {
         string crossHairSettings = "N";
         int crossHairStyle = 0;
         const string MONEY_FORMAT = "R #,##0.00";
         public ActionResult ViewOffenceDetail(string NotIntNo, string searchField, string searchValue, string Since, bool isPre)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            if (Session["CurrentPage"] == null)
            {
                Session["CurrentPage"] = 0;
            }

            DataSet ds = GetNoticeListByCriteria_NotIntNoOnly(searchField, searchValue, Since);
            int currentPage = (int)Session["CurrentPage"];

            if (NotIntNo != null)
            {
                currentPage = 0;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["NotIntNo"].ToString() == NotIntNo)
                    {
                        currentPage = i;
                        break;
                    }
                }
            }
            else
            {
                if (isPre)
                {
                    currentPage -= 1;
                    if (currentPage < 0)
                    {
                        currentPage = 0;
                    }
                }
                else
                {
                    currentPage += 1;
                    if (currentPage > ds.Tables[0].Rows.Count - 1)
                    {
                        currentPage = ds.Tables[0].Rows.Count - 1;
                    }
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        currentPage = 0;
                    }
                }
            }

            Session["CurrentPage"] = currentPage;

            OffenceDetailModel offenceDetailModel = new OffenceDetailModel();
            //offenceDetailModel.Title = ViewOffenceDetailRes.lblPageNameText;
            offenceDetailModel.searchField = searchField;
            offenceDetailModel.searchValue = searchValue;
            offenceDetailModel.Since = Since;
            
            if (currentPage >= 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    int autIntNo = Convert.ToInt32(ds.Tables[0].Rows[currentPage]["AutIntNo"]);
                    int notIntNo = Convert.ToInt32(ds.Tables[0].Rows[currentPage]["NotIntNo"]);
                    offenceDetailModel = ShowFrameDetails(offenceDetailModel, autIntNo, notIntNo);

                    offenceDetailModel.Navigation = string.Format(ViewOffenceDetailRes.lblNavigationText, (currentPage + 1).ToString(), ds.Tables[0].Rows.Count);// +" " + (currentPage + 1).ToString() + " / " + ds.Tables[0].Rows.Count;
                    offenceDetailModel.Count = ds.Tables[0].Rows.Count;
                }
                else
                {
                    offenceDetailModel.FrameIntNo = 0;
                    offenceDetailModel.Navigation = string.Format(ViewOffenceDetailRes.lblNavigationText, (currentPage + 1).ToString(), ds.Tables[0].Rows.Count);//FrameImageView.Navigation + " " + (currentPage + 1).ToString() + " / " + ds.Tables[0].Rows.Count;
                    offenceDetailModel.Count = 0;
                }
            }
            return View("ViewOffenceDetail", offenceDetailModel);
        }

        public  OffenceDetailModel ShowFrameDetails(OffenceDetailModel model, int autIntNo, int notIntNo)
        {
            NoticeDB notice = new NoticeDB(connectionString);

            //mrs 20080814 notIntNo not changing during revious and next operations


           // model.PrintVersionNavigateUrl = string.Format("ViewOffence_Print?notice={0}&autintno={1}", notIntNo.ToString(), autIntNo);

            model.noticePara = notIntNo.ToString();
            model.autintnoPara = autIntNo.ToString();

            this.crossHairSettings = GetCrossHairSettings(autIntNo);
            this.crossHairStyle = this.GetCrossHairStyle(autIntNo);

             int frameIntNo = notice.GetFrameForNotice(notIntNo);

            if (frameIntNo != 0)
            {
                int filmIntNo = 0;
                string filmNo="";

                SqlDataReader reader = notice.GetNoticeViewDetails(notIntNo, this.crossHairSettings, this.crossHairStyle);
                while (reader.Read())
                {
                    //dls 070120 - add rules for showing cross-hairs
                    Session["ShowCrossHairs"] = reader["ShowCrossHairs"].ToString().Equals("Y") ? true : false;
                    Session["CrossHairStyle"] = Convert.ToInt16(reader["CrossHairStyle"]);

                    filmIntNo = (int)reader["FilmIntNo"];
                    filmNo = reader["NotFilmNo"].ToString();
                   model.FilmFrameNo= filmNo + " ~ " + reader["NotFrameNo"].ToString();
                    model.RegistrationNo= reader["NotRegNo"].ToString();
                    model.Speed = "1st Speed " + reader["NotSpeed1"].ToString();
                    model.Speed2 = "2nd Speed " + reader["NotSpeed2"].ToString();
                    //txtVehMake.Text = reader["NotVehicleMake"].ToString();
                    //txtVehType.Text = reader["NotVehicleType"].ToString();
                    model.TicketNo = reader["NotTicketNo"].ToString();

                    DateTime offenceDate = Convert.ToDateTime(reader["NotOffenceDate"]);
                    //txtOffenceDate.Text = offenceDate.Year + "/" + offenceDate.Month.ToString().PadLeft(2, '0') + "/" + offenceDate.Day.ToString().PadLeft(2, '0');
                    //txtOffenceTime.Text = offenceDate.Hour.ToString().PadLeft(2, '0') + ":" + offenceDate.Minute.ToString().PadLeft(2, '0');
                    model.OffenceDate = offenceDate.Year + "/" + offenceDate.Month.ToString().PadLeft(2, '0') + "/" + offenceDate.Day.ToString().PadLeft(2, '0')
                        + " " + offenceDate.Hour.ToString().PadLeft(2, '0') + ":" + offenceDate.Minute.ToString().PadLeft(2, '0');
                    model.Offencedesc = reader["ChgOffenceDescr"].ToString();

                    model.Location = reader["NotLocDescr"].ToString();

                    LoadOwnerDetails(notIntNo,model);
                    LoadDriverDetails(notIntNo,model);

                    if (reader["NotProxyFlag"].ToString().Equals("Y"))
                    {
                        LoadProxyDetails(notIntNo,model);
                        model.NotProxyFlag = "Y";
                    }
                    else
                    {
                        model.NotProxyFlag = "N";
                    }

                   model.tblOwnerDetails = true;

                   model.FineAmount = Convert.ToDecimal(reader["ChgRevFineAmount"]).ToString(MONEY_FORMAT);
                }

                reader.Close();

                // Image Viewer Control
                // Image Viewer Control
                model.NotFilmNo = filmNo;
                model.FrameIntNo = frameIntNo;
                model.ScanImageIntNo = 0;
                model.ImageType = "A";
                model.FilmIntNo = filmIntNo;
                model.Phase = this.Session["cvPhase"] == null ? 1 : Convert.ToInt32(this.Session["cvPhase"]);
            }
            else
            {
                //lblError.Visible = true;
                model.Error = ViewOffenceDetailRes.lblErrorText;
            }

            return model;
        }

        private void LoadOwnerDetails(int notIntNo, OffenceDetailModel model)
        {

            OwnerDB owner = new OwnerDB(connectionString);

            OwnerDetails ownerDet = owner.GetOwnerDetailsByNotice(notIntNo);

            string name = string.Empty;
            string initials = string.Empty;
            string id = string.Empty;
            string postAddr = string.Empty;
            string strAddr = string.Empty;

            try
            {
                name = ownerDet.OwnSurname.Trim();
            }
            catch { }

            try
            {
                initials = ownerDet.OwnInitials.Trim();
            }
            catch { }

            try
            {
                id = ownerDet.OwnIDNumber.Trim();
            }
            catch { }

            try
            {
                postAddr = ownerDet.OwnPOAdd1.Trim();
                if (!ownerDet.OwnPOAdd1.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOAdd2.Trim();
                if (!ownerDet.OwnPOAdd2.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOAdd3.Trim();
                if (!ownerDet.OwnPOAdd3.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOAdd4.Trim();
                if (!ownerDet.OwnPOAdd4.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOAdd5.Trim();
                if (!ownerDet.OwnPOAdd5.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOCode.Trim();
            }
            catch { }

            try
            {
                strAddr = ownerDet.OwnStAdd1.Trim();
                if (!ownerDet.OwnStAdd1.Trim().Equals("")) strAddr += ", ";

                strAddr += ownerDet.OwnStAdd2.Trim();
                if (!ownerDet.OwnStAdd2.Trim().Equals("")) strAddr += ", ";

                strAddr += ownerDet.OwnStAdd3.Trim();
                if (!ownerDet.OwnStAdd3.Trim().Equals("")) strAddr += ", ";

                strAddr += ownerDet.OwnStAdd4.Trim();
                if (!ownerDet.OwnStAdd4.Trim().Equals("")) strAddr += ", ";

                strAddr += ownerDet.OwnStCode.Trim();
            }
            catch { }

            model.OwnerName = ownerDet.OwnFullName;

            model.OwnerID = id;

            model.OwnerPostAddr = postAddr;
            model.OwnerStrAddr = strAddr;
        }

        private void LoadDriverDetails(int notIntNo, OffenceDetailModel model)
        {

            DriverDB driver = new DriverDB(connectionString);

            DriverDetails driverDet = driver.GetDriverDetailsByNotice(notIntNo);

            string name = string.Empty;
            string initials = string.Empty;
            string id = string.Empty;
            string postAddr = string.Empty;
            string strAddr = string.Empty;

            try
            {
                name = driverDet.DrvSurname.Trim();
            }
            catch { }

            try
            {
                initials = driverDet.DrvInitials.Trim();
            }
            catch { }

            try
            {
                id = driverDet.DrvIDNumber.Trim();
            }
            catch { }

            try
            {
                postAddr = driverDet.DrvPOAdd1.Trim();
                if (!driverDet.DrvPOAdd1.Trim().Equals(""))
                    postAddr += ", ";

                postAddr += driverDet.DrvPOAdd2.Trim();
                if (!driverDet.DrvPOAdd2.Trim().Equals(""))
                    postAddr += ", ";

                postAddr += driverDet.DrvPOAdd3.Trim();
                if (!driverDet.DrvPOAdd3.Trim().Equals(""))
                    postAddr += ", ";

                postAddr += driverDet.DrvPOAdd4.Trim();
                if (!driverDet.DrvPOAdd4.Trim().Equals(""))
                    postAddr += ", ";

                postAddr += driverDet.DrvPOAdd5.Trim();
                if (!driverDet.DrvPOAdd5.Trim().Equals(""))
                    postAddr += ", ";

                postAddr += driverDet.DrvPOCode.Trim();
            }
            catch { }

            try
            {
                strAddr = driverDet.DrvStAdd1.Trim();
                if (!driverDet.DrvStAdd1.Trim().Equals(""))
                    strAddr += ", ";

                strAddr += driverDet.DrvStAdd2.Trim();
                if (!driverDet.DrvStAdd2.Trim().Equals(""))
                    strAddr += ", ";

                strAddr += driverDet.DrvStAdd3.Trim();
                if (!driverDet.DrvStAdd3.Trim().Equals(""))
                    strAddr += ", ";

                strAddr += driverDet.DrvStAdd4.Trim();
                if (!driverDet.DrvStAdd4.Trim().Equals(""))
                    strAddr += ", ";

                strAddr += driverDet.DrvStCode.Trim();
            }
            catch { }

            // LMZ Changed 12-07-2007
            model.DriverName = driverDet.DrvFullName;

            model.DriverID = id;

            model.DriverPostAddr = postAddr;
            model.DriverStrAddr = strAddr;
        }

        private void LoadProxyDetails(int notIntNo, OffenceDetailModel model)
        {

            ProxyDB proxy = new ProxyDB(connectionString);

            ProxyDetails proxyDet = proxy.GetProxyDetailsByNotice(notIntNo);

            string name = string.Empty;
            string initials = string.Empty;
            string id = string.Empty;
            //string postAddr = string.Empty;
            //string strAddr = string.Empty;

            try
            {
                name = proxyDet.PrxSurname.Trim();
            }
            catch { }

            try
            {
                initials = proxyDet.PrxInitials.Trim();
            }
            catch { }

            try
            {
                id = proxyDet.PrxIDNumber.Trim();
            }
            catch { }



            model.ProxyName = proxyDet.PrxFullName;

            model.ProxyID = id;

        }

        private string GetCrossHairSettings(int autIntNo)
        {
            //dls 100202 - move rules for showing cross-hairs out of the stored proc into the code
            AuthorityRulesDetails ar = new AuthorityRulesDetails();

            ar.AutIntNo = autIntNo;
            ar.ARCode = "3100";
            ar.LastUser = this.login;

            DefaultAuthRules crossHairs = new DefaultAuthRules(ar, this.connectionString);

            KeyValuePair<int, string> crossHairRule = crossHairs.SetDefaultAuthRule();

            return crossHairRule.Value;
        }

        private int GetCrossHairStyle(int autIntNo)
        {
            //dls 100202 - move rules for showing cross-hairs out of the stored proc into the code
            AuthorityRulesDetails ar = new AuthorityRulesDetails();

            ar.AutIntNo = autIntNo;
            ar.ARCode = "3150";
            ar.LastUser = this.login;

            DefaultAuthRules crossHairs = new DefaultAuthRules(ar, this.connectionString);

            KeyValuePair<int, string> crossHairRule = crossHairs.SetDefaultAuthRule();

            return crossHairRule.Key;
        }

        public DataSet GetNoticeListByCriteria_NotIntNoOnly(string ColumnName, string ColumnValue, string DateSince)
        {
            if (string.IsNullOrEmpty(DateSince))
                DateSince = "2000-01-01";

            string  autIntNo ="0";// Session["autIntNo"].ToString();

            string  minStatus = CheckMinimumStatusRule().ToString();

            NoticeDB db = new NoticeDB(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());
            DataSet ds = db.GetNoticeListByCriteria_NotIntNoOnly(autIntNo, ColumnName, ColumnValue, DateSince, minStatus);

            return ds;
        }

      

    }
}
