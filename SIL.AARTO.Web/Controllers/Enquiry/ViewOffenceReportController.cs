﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility;
using Stalberg.TMS;
using CrystalDecisions.CrystalReports.Engine;
using Stalberg.TMS.Data.Datasets;
using System.IO;
using CrystalDecisions.Shared;
using SIL.AARTO.Web.Resource.Enquiry;

namespace SIL.AARTO.Web.Controllers.Enquiry
{
    public partial class EnquiryController : Controller
    {
        ReportDocument report = new ReportDocument();
        public ActionResult ViewOffenceReport(string searchField, string searchValue, string Since)
        {
            if (Session["UserLoginInfo"] == null)
                return RedirectToAction("login", "account");
            //if (Session["userIntNo"] == null)
            //    return RedirectToAction("login", "account");
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            login = userDetails.UserName;
           

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            // Setup the report
            string reportPath = Server.MapPath("~/Reports/OffenceSummary.rpt");
            string reportPage = "OffenceSummary.rpt";

            if (System.IO.File.Exists(reportPath))
            {
                searchValue = searchValue.Replace("/", "").Replace("-", "");
                DateTime dtSince;
                if (DateTime.TryParse(Since, out dtSince))
                { }
                else
                {
                    dtSince = new DateTime(2000, 1, 1);
                }
                int minStatus = CheckMinimumStatusRule();

                this.report = new ReportDocument();
                report.Load(reportPath);

                NoticeReportDB notice = new NoticeReportDB(this.connectionString);
                dsOfenceSummary ds = (dsOfenceSummary)notice.GetNoticeSearchDS(0, searchField, searchValue, dtSince, minStatus);

                // Populate the report
                report.SetDataSource(ds.Tables[1]);
                ds.Dispose();

                // Export the PDF file
                MemoryStream ms = new MemoryStream();
                ms = (MemoryStream)report.ExportToStream(ExportFormatType.PortableDocFormat);
                report.Dispose();

                return File(ms.ToArray(), "application/pdf");
            }
            else
            {
                string error = string.Format(ViewOffenceReportRes.error, reportPage);
                Response.Write(error);
                Response.Flush();
                Response.End();
                return View();
            }
            
        }

    }
}
