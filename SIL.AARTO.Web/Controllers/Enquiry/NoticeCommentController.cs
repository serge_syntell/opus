﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.Enquiry;
using SIL.AARTO.Web.ViewModels.Enquiry;
using ceTe.DynamicPDF.ReportWriter;
using Path = System.IO.Path;
using ceTe.DynamicPDF.ReportWriter.Data;

namespace SIL.AARTO.Web.Controllers.Enquiry
{
    public partial class EnquiryController : Controller
    {
        NoticeCommentModel model = new NoticeCommentModel();
        NoticeCommentService ncService = new NoticeCommentService();
        NoticeService notService = new NoticeService();
        int notIntNo;
        int pageIndex;
        int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
        string LastUser { get { return (this.Session["UserLoginInfo"] as UserLoginInfo) == null ? 
            string.Empty : (this.Session["UserLoginInfo"] as UserLoginInfo).UserName; } }
        public ActionResult NoticeCommentViewer(string id, string page)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            if (!string.IsNullOrWhiteSpace(id))
            {
                int.TryParse(id, out this.notIntNo);
                int.TryParse(page, out this.pageIndex);
                if (this.pageIndex < 0) this.pageIndex = 0;
                if (this.notIntNo > 0)
                    NCInitializeModel();
            }
            return View(this.model);
        }

        [HttpPost]
        public ActionResult NCGet(NoticeCommentEntity entity)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            if (entity.NcIntNo > 0)
            {
                try
                {
                    var ncEntity = ncService.GetByNcIntNo(entity.NcIntNo);
                    if (ncEntity != null)
                    {
                        //if (CheckNoticeWithAuthority(ncEntity.NotIntNo) == null)
                        //{
                        //    entity.EntityStatus = false;
                        //    entity.EntityMessage = NoticeCommentViewer_cshtml.WrongAuthority;
                        //    return Json(entity);
                        //}

                        entity = ConvertHelper.ConvertClass<NoticeComment, NoticeCommentEntity>(ncEntity);
                        entity.RowVersionString = BitConverter.ToInt64(entity.RowVersion, 0).ToString(CultureInfo.InvariantCulture);
                        entity.EntityStatus = true;
                    }
                    else
                    {
                        entity.EntityStatus = false;
                        entity.EntityMessage = NoticeCommentViewer_cshtml.NotExists;
                    }
                }
                catch (Exception ex)
                {
                    entity.EntityStatus = false;
                    entity.EntityMessage = ex.Message;
                }
            }
            return Json(entity);
        }

        [HttpPost]
        public ActionResult NCSave(NoticeCommentEntity entity)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            if (entity.NotIntNo > 0 && ModelState.IsValid)
            {
                long rowVersion;
                long.TryParse(entity.RowVersionString, out rowVersion);
                entity.RowVersion = BitConverter.GetBytes(rowVersion);

                var isAdd = entity.NcIntNo == 0;
                if (isAdd || entity.NcIntNo > 0)
                {
                    try
                    {
                        //if (CheckNoticeWithAuthority(entity.NotIntNo) == null)
                        //{
                        //    entity.EntityStatus = false;
                        //    entity.EntityMessage = NoticeCommentViewer_cshtml.WrongAuthority;
                        //    return Json(entity);
                        //}

                        var userInfo = (UserLoginInfo) Session["UserLoginInfo"];
                        var ncEntity = new NoticeComment();
                        if (isAdd)
                        {
                            ncEntity.NotIntNo = entity.NotIntNo;
                            ncEntity.NcComment = entity.NcComment;
                            ncEntity.NcUpdateDate = DateTime.Now;
                            ncEntity.NcCreateDate = DateTime.Now;
                            ncEntity.LastUser = userInfo.UserName;
                            ncEntity.EntityState = EntityState.Added;
                        }
                        else
                        {
                            ncEntity = ncService.GetByNcIntNo(entity.NcIntNo);
                            if (ncEntity != null)
                            {
                                if (BitConverter.ToInt64(ncEntity.RowVersion, 0) != rowVersion)
                                {
                                    entity.EntityStatus = false;
                                    entity.EntityMessage = NoticeCommentViewer_cshtml.NeedsRefresh;
                                    return Json(entity);
                                }

                                ncEntity.NcComment = entity.NcComment;
                                ncEntity.NcUpdateDate = DateTime.Now;
                                ncEntity.EntityState = EntityState.Changed;
                            }
                            else
                            {
                                entity.EntityStatus = false;
                                entity.EntityMessage = NoticeCommentViewer_cshtml.NotExists;
                                return Json(entity);
                            }
                        }
                        ncEntity.LastUser = this.LastUser;
                        ncEntity = ncService.Save(ncEntity);
                        entity = ConvertHelper.ConvertClass<NoticeComment, NoticeCommentEntity>(ncEntity);
                        entity.EntityStatus = true;
                        entity.EntityMessage = Global.StatusSave_1;
                    }
                    catch (Exception ex)
                    {
                        entity.EntityStatus = false;
                        entity.EntityMessage = ex.Message;
                        return Json(entity);
                    }
                }
                //Linda 2012-9-12 add for list report19
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());
            punchStatistics.PunchStatisticsTransactionAdd(Convert.ToInt32(Session["AutIntNo"]), ((UserLoginInfo)Session["UserLoginInfo"]).UserName, PunchStatisticsTranTypeList.Notes, isAdd ? PunchAction.Add : PunchAction.Change);   
                  
            }
            return Json(entity);
        }

        [HttpPost]
        public ActionResult NCDelete(NoticeCommentEntity entity)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            if (entity.NcIntNo > 0)
            {
                long rowVersion;
                long.TryParse(entity.RowVersionString, out rowVersion);
                entity.RowVersion = BitConverter.GetBytes(rowVersion);

                try
                {
                    var ncEntity = ncService.GetByNcIntNo(entity.NcIntNo);
                    if (ncEntity != null)
                    {
                        //if (CheckNoticeWithAuthority(ncEntity.NotIntNo) == null)
                        //{
                        //    entity.EntityStatus = false;
                        //    entity.EntityMessage = NoticeCommentViewer_cshtml.WrongAuthority;
                        //    return Json(entity);
                        //}

                        if (BitConverter.ToInt64(ncEntity.RowVersion, 0) != rowVersion)
                        {
                            entity.EntityStatus = false;
                            entity.EntityMessage = NoticeCommentViewer_cshtml.NeedsRefresh;
                            return Json(entity);
                        }
                    }
                    else
                    {
                        entity.EntityStatus = false;
                        entity.EntityMessage = NoticeCommentViewer_cshtml.NotExists;
                        return Json(entity);
                    }
                    entity.EntityStatus = ncService.Delete(ncEntity);
                    entity.EntityMessage = entity.EntityStatus ? Global.StatusDelete_1 : Global.StatusDelete_0;
                }
                catch (Exception ex)
                {
                    entity.EntityStatus = false;
                    entity.EntityMessage = ex.Message;
                }
                //Linda 2012-9-12 add for list report19
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());
                punchStatistics.PunchStatisticsTransactionAdd(Convert.ToInt32(Session["AutIntNo"]), ((UserLoginInfo)Session["UserLoginInfo"]).UserName,PunchStatisticsTranTypeList.Notes,PunchAction.Delete);   
                  
            }
            return Json(entity);
        }

        public ActionResult NCPrint(string id)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            int.TryParse(id, out this.notIntNo);

            //if (CheckNoticeWithAuthority(this.notIntNo) == null)
            //{
            //    this.notIntNo = 0;
            //}

            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Reports\NoticeCommentReport.dplx");
            var layout = new DocumentLayout(path);
            var query = (StoredProcedureQuery)layout.GetQueryById("Query");
            query.ConnectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString();
            var parameters = new ParameterDictionary();
            parameters.Add("NotIntNo", this.notIntNo);
            var doc = layout.Run(parameters);

            return File(doc.Draw(), "application/pdf");
        }


        void NCInitializeModel()
        {
            model.NotTicketNo = null;
            model.NotOffenceDate = null;

            var notEntity = notService.GetByNotIntNo(this.notIntNo);
            if (notEntity != null)
            {
                model.NotIntNo = notEntity.NotIntNo;
                model.NotTicketNo = notEntity.NotTicketNo;
                model.NotOffenceDate = notEntity.NotOffenceDate;

                int totalCount;
                model.NoticeCommentList = NCGetPaged(this.notIntNo, this.pageIndex, this.pageSize, out totalCount);

                var count = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(totalCount)/Convert.ToDouble(this.pageSize)));
                if (totalCount > 0 && this.pageIndex >= count)
                {
                    this.pageIndex = count - 1;
                    model.NoticeCommentList = NCGetPaged(this.notIntNo, this.pageIndex, this.pageSize, out totalCount);
                }

                if (model.NoticeCommentList.Count > 0)
                {
                    var start = this.pageIndex*this.pageSize;
                    foreach (var ncEntity in model.NoticeCommentList)
                    {
                        ncEntity.OrderNo = ++start;

                        ncEntity.RowVersionString = BitConverter.ToInt64(ncEntity.RowVersion, 0).ToString(CultureInfo.InvariantCulture);
                    }
                }
                model.PageIndex = this.pageIndex;
                model.TotalCount = totalCount;
            }
        }

        //Notice CheckNoticeWithAuthority(int notIntNo)
        //{
        //    Notice notEntity = null;
        //    int autIntNo;
        //    int.TryParse(Convert.ToString(Session["AutIntNo"]), out autIntNo);
        //    TList<Notice> notList;
        //    if (autIntNo > 0
        //        && notIntNo > 0
        //        && (notList = notService.GetByAutIntNoNotIntNo(autIntNo, notIntNo)) != null
        //        && notList.Count > 0
        //        )
        //        notEntity = notList[0];
        //    return notEntity;
        //}

        List<NoticeCommentEntity> NCGetPaged(int notIntNo, int pageIndex, int pageSize, out int totalCount)
        {
            var where = string.Format(" NotIntNo = {0} ", notIntNo);
            var orderBy = " NCCreateDate ";
            return ConvertHelper.ConvertClassList<NoticeComment, NoticeCommentEntity, List<NoticeCommentEntity>>(ncService.GetPaged(where, orderBy, pageIndex, pageSize, out totalCount));
        }

    }
}