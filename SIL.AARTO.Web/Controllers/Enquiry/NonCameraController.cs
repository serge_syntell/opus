﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.Enquiry;

namespace SIL.AARTO.Web.Controllers.Enquiry
{
    public partial class EnquiryController : Controller
    {

        public ActionResult NonCamera(int NotIntNo, NoticeSource? Source)
        {
            NonCameraImageModel model = new NonCameraImageModel();
            model.NotIntNo = NotIntNo;
            model.Source = Source;
            return View(model);
        }

    }
}
