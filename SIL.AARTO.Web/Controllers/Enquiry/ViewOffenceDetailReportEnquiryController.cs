﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Stalberg.TMS;
using System.Web.Services;
using System.Web.Services.Protocols;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using SIL.AARTO.BLL.Utility;
using System.IO;
using SIL.AARTO.Web.Resource.Enquiry;
using ceTe.DynamicPDF.Imaging;
using SIL.AARTO.DAL.Services;
using System.Globalization;

namespace SIL.AARTO.Web.Controllers.Enquiry
{
    public partial class EnquiryController : Controller
    {
        public ActionResult ViewOffenceDetailReportEnquiry()
        {
            // Get user info from session variable
            if (Session["UserLoginInfo"] == null)
                return RedirectToAction("login", "account");
            //if (Session["userIntNo"] == null)
            //    return RedirectToAction("login", "account");

            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            login = userDetails.UserName;


            autIntNo = Convert.ToInt32(Session["autIntNo"]);
             GeneratedDateRole = GetGeneratedDateReplacePrintDateRule(autIntNo);

            if (Request.QueryString["NotIntNo"] == null)
            {
                Response.Write(ViewOffenceDetailReportRes.strWriteMsg);
                Response.Flush();
                Response.End();
                return View();
            }

            //2014-09-28 Heidi changed for According to the template decided to use which data(bontq1523)
            //AuthorityService autService = new AuthorityService();
            //SIL.AARTO.DAL.Entities.Authority autEntity = autService.GetByAutIntNo(autIntNo);
            //string autCode = "";
            //if (autEntity != null)
            //{
            //    autCode = autEntity.AutCode.Trim().ToLower();
            //}
            //if (autCode != "cp")
            //{

            //    return RedirectToAction("ViewOffenceDetailReport", new { NotIntNo = Request.QueryString["NotIntNo"] });
            //}

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "NoticeEnquiry");

            //2014-09-28 Heidi added for According to the template decided to use which data(bontq1523)
            if (!string.IsNullOrEmpty(reportPage))
            {
                if (reportPage.Trim() == "NoticeEnquiry_ST.dplx")
                {
                    return RedirectToAction("ViewOffenceDetailReport", new { NotIntNo = Request.QueryString["NotIntNo"] });
                }
            }

            if (reportPage.Equals(string.Empty))
            {
                //int arnIntNo = arn.AddAuthReportName(autIntNo, "NoticeEnquiry.dplx", "NoticeEnquiry", "system");
                int arnIntNo = arn.AddAuthReportName(autIntNo, "NoticeEnquiry_CoCT.dplx", "NoticeEnquiry", "system", "");
                reportPage = "NoticeEnquiry_CoCT.dplx";
            }


            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Reports\" + reportPage);
            DocumentLayout doc = new DocumentLayout(path);
          

           try
           {
                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea raTotal = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("raTotal");
                raTotal.LayingOut += new LayingOutEventHandler(ra_Total2);
                //Barry Dickson - add control in to not show receipt fields if there has been no payment made or there was a reversal
                //label for raTotal
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRctAmount = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRctAmount");
                lblRctAmount.LayingOut += new LayingOutEventHandler(lbl_Amount2);
                //Receipt Number
                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcbRctNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rcbRctNo");
                rcbRctNo.LayingOut += new LayingOutEventHandler(ra_RctNo2);

                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcbOriginalSumChargeFine = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rcbOriginalSumChargeFine");
                rcbOriginalSumChargeFine.LayingOut += new LayingOutEventHandler(rcbOriginalSumChargeFine_LayingOut2);

                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRctNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRctNo");
                lblRctNo.LayingOut += new LayingOutEventHandler(lbl_RctNo2);

                //Heidi 2013-05-29 added 1st notice posted date
                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcbNotPosted1stNoticeDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rcbNotPosted1stNoticeDate");
                rcbNotPosted1stNoticeDate.LayingOut +=new LayingOutEventHandler(rcbNotPosted1stNoticeDate_LayingOut2);
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl1stNoticePostedDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl1stNoticePostedDate");
                lbl1stNoticePostedDate.LayingOut += new LayingOutEventHandler(lbl1stNoticePostedDate_LayingOut2);
                
                //Heidi 2013-05-29 added 2st notice posted date
                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcbNotPosted2ndNoticeDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rcbNotPosted2ndNoticeDate");
                rcbNotPosted2ndNoticeDate.LayingOut += new LayingOutEventHandler(rcbNotPosted2ndNoticeDate_LayingOut2);
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl2ndNoticePostedDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl2ndNoticePostedDate");
                lbl2ndNoticePostedDate.LayingOut += new LayingOutEventHandler(lbl2ndNoticePostedDate_LayingOut2);

                ////EasyPay Number
                //ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcbEasyPay = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rcbEasyPay");
                //rcbEasyPay.LayingOut += new LayingOutEventHandler(ra_Easy2);
                //ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblEasyPay = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblEasyPay");
                //lblEasyPay.LayingOut += new LayingOutEventHandler(lbl_Easy2);

                //Jerry 2012-11-26 Receipt AGNumber
                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea raRctAGNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("raAGNumber");
                raRctAGNo.LayingOut += new LayingOutEventHandler(ra_RctAgNo2);
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRctAgNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAGNumber");
                lblRctAgNo.LayingOut += new LayingOutEventHandler(lbl_RctAgNo2);

                //Remanded from case number and court date

                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcdRemandedFromCaseNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rdaSumRemandedFromCaseNumber");
                rcdRemandedFromCaseNo.LayingOut += new LayingOutEventHandler(ra_RemandedFromCaseNo2);

                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcdRemandedFromCourtDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rdaSumRemandedFromCourtDate");
                rcdRemandedFromCourtDate.LayingOut += new LayingOutEventHandler(ra_RemandedFromCourtDate2);

                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRemandedFromCourtDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRemandedFromCourtDate");
                lblRemandedFromCourtDate.LayingOut += new LayingOutEventHandler(lbl_RemandedFromCourtDate2);

                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRemandedFromCaseNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRemandedFromCaseNumber");
                lblRemandedFromCaseNo.LayingOut += new LayingOutEventHandler(lbl_RemandedFromCaseNo2);

                int nNotIntNo = Convert.ToInt32(Request.QueryString["NotIntNo"].ToString());

                StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
                StoredProcedureQuery query2 = (StoredProcedureQuery)doc.GetQueryById("Query2");
               // Query query3 = (Query)doc.GetQueryById("Query3");
                query.ConnectionString = this.connectionString;
                query2.ConnectionString = this.connectionString;
                //query3.ConnectionString = this.connectionString;
                ParameterDictionary parameters = new ParameterDictionary();
                parameters.Add("NotIntNo", nNotIntNo);
                parameters.Add("GenerDateReplacePrintDateRole", GeneratedDateRole);

                ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder imagePlaceHolder = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("Img1");
                imagePlaceHolder.LaidOut += new PlaceHolderLaidOutEventHandler(imagePlaceHolder_LaidOut2);
                ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder imagePlaceHolder2 = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("Img2");
                imagePlaceHolder2.LaidOut += new PlaceHolderLaidOutEventHandler(imagePlaceHolder2_LaidOut2);

                Document report = doc.Run(parameters);

                byte[] buffer = report.Draw();

                imagePlaceHolder.LaidOut -= new PlaceHolderLaidOutEventHandler(imagePlaceHolder_LaidOut2);
                imagePlaceHolder2.LaidOut -= new PlaceHolderLaidOutEventHandler(imagePlaceHolder2_LaidOut2);

                raTotal.LayingOut -= new LayingOutEventHandler(ra_Total2);
                lblRctAmount.LayingOut -= new LayingOutEventHandler(lbl_Amount2);
                //lblRctAmount.LayingOut -= new LayingOutEventHandler(lbl_Easy2);
                lblRctAmount.LayingOut -= new LayingOutEventHandler(lbl_RctNo2);
                lblRctAmount.LayingOut -= new LayingOutEventHandler(ra_RctNo2);
                //lblRctAmount.LayingOut -= new LayingOutEventHandler(ra_Easy2);
                lblRctAmount.LayingOut -= new LayingOutEventHandler(ra_RctAgNo2);
                lblRctAmount.LayingOut -= new LayingOutEventHandler(lbl_RctAgNo2);

                //Response.ClearContent();
                //Response.ClearHeaders();
                //Response.ContentType = "application/pdf";
                //Response.BinaryWrite(buffer);
                //Response.End();

                return File(report.Draw(), "application/pdf");

          }
           catch (Exception ex)
           {
               Response.Write(ex.Message);
               Response.End();
               return View();
           }


          //  return View();
        }

        void lbl2ndNoticePostedDate_LayingOut2(object sender, LayingOutEventArgs e)
        {
            try
            {

                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblE = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;

                if (GeneratedDateRole!= "A")
                {
                    lblE.Text = lblE.Text = ViewOffenceDetailReportRes.lbl2ndNoticePostedDate;
                }
                else
                {
                    lblE.Text = "";
                }

            }
            catch { }
        }

        void rcbNotPosted2ndNoticeDate_LayingOut2(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (GeneratedDateRole!= "A")
                {
                    if (e.LayoutWriter.RecordSets.Current["NotPosted2ndNoticeDate"] != DBNull.Value)
                    {
                        if (e.LayoutWriter.RecordSets.Current["NotPosted2ndNoticeDate"].ToString() != string.Empty)
                        {
                            ra.Text = Convert.ToDateTime(e.LayoutWriter.RecordSets.Current["NotPosted2ndNoticeDate"]).ToString("yyyy-MM-dd");
                        }
                    }
                }
                else
                {
                    ra.Text = "";

                }
            }
            catch { }
        }

        void lbl1stNoticePostedDate_LayingOut2(object sender, LayingOutEventArgs e)
        {
            try
            {


                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblE = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (GeneratedDateRole!= "A")
                {
                    lblE.Text = lblE.Text = ViewOffenceDetailReportRes.lbl1stNoticePostedDate;
                }
                else
                {
                    lblE.Text = "";
                }

            }
            catch { }


        }

        void rcbNotPosted1stNoticeDate_LayingOut2(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (GeneratedDateRole!= "A")
                {
                    if (e.LayoutWriter.RecordSets.Current["NotPosted1stNoticeDate"] != DBNull.Value)
                    {
                        if (e.LayoutWriter.RecordSets.Current["NotPosted1stNoticeDate"].ToString() != string.Empty)
                        {
                            ra.Text = Convert.ToDateTime(e.LayoutWriter.RecordSets.Current["NotPosted1stNoticeDate"]).ToString("yyyy-MM-dd");
                        }
                    }
                }
                else
                {
                    ra.Text = "";

                }
            }
            catch { }
        }

        void rcbOriginalSumChargeFine_LayingOut2(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["OriginalSumChargeFine"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["OriginalSumChargeFine"].ToString() == "NoAOG")
                    {
                        ra.Text = "NoAOG";
                    }
                    else
                    {
                        if (e.LayoutWriter.RecordSets.Current["OriginalSumChargeFine"].ToString() != string.Empty
                            //&& int.Parse(e.LayoutWriter.RecordSets.Current["OriginalSumChargeFine"].ToString()) != 0
                            )
                            // ra.Text = String.Format("R {0:#0.00}", e.LayoutWriter.RecordSets.Current["OriginalSumChargeFine"]);
                            ra.Text = "R  " + Convert.ToString(e.LayoutWriter.RecordSets.Current["OriginalSumChargeFine"]);
                    }

                }
            }
            catch { }
        }

        public void ra_Total2(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                    {
                        //update by Rachel 20140819 for 5337
                        //ra.Text = String.Format("R {0:#0.00}", e.LayoutWriter.RecordSets.Current["Total"]);
                        ra.Text = String.Format(CultureInfo.InvariantCulture,"R {0:#0.00}", e.LayoutWriter.RecordSets.Current["Total"]);
                        //end update by Rachel 20140819 for 5337
                    }

                }
            }
            catch { }
        }
        //Barry Dickson - to handle to check on receipt amount and if to show label.
        public void lbl_Amount2(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty)
                        lblA.Text = ViewOffenceDetailReportRes.lblAText;

                }
            }
            catch { }

        }
        //Barry Dickson
        public void lbl_RctNo2(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRN = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                        lblRN.Text = ViewOffenceDetailReportRes.lblRNText;
                }
            }
            catch { }
        }

        //Barry Dickson
        public void lbl_Easy2(object sender, LayingOutEventArgs e)
        {
            try
            {
                //dls 080326 - always show easypay number, if there is one

                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblE = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                //if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                //{
                //    if (e.LayoutWriter.RecordSets.Current["Total"] != string.Empty)
                //        lblE.Text = "Easy Pay No:";

                //}

                if (e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"].ToString() != string.Empty)
                        lblE.Text = ViewOffenceDetailReportRes.lblEText;

                }
            }
            catch { }
        }

        //Barry Dickson
        public void ra_RctNo2(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["RctNumber"]);
                }
            }
            catch { }
        }

        //Jerry 2012-11-26 add
        public void ra_RctAgNo2(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["AGNumber"]);
                }
            }
            catch { }
        }

        //Jerry 2012-11-26 add
        public void lbl_RctAgNo2(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAGNum = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0 &&
                        e.LayoutWriter.RecordSets.Current["AGNumber"] != null && e.LayoutWriter.RecordSets.Current["AGNumber"].ToString() != string.Empty)
                        lblAGNum.Text = ViewOffenceDetailReportRes.lblAGNumText;
                }
            }
            catch { }
        }

        public void lbl_RemandedFromCourtDate2(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"].ToString() != string.Empty)
                        lblA.Text = ViewOffenceDetailReportRes.lblAText1;

                }
            }
            catch { }
        }

        public void lbl_RemandedFromCaseNo2(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"].ToString() != string.Empty)
                        lblA.Text = ViewOffenceDetailReportRes.lblAText2;

                }
            }
            catch { }
        }

        public void ra_RemandedFromCaseNo2(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"].ToString() != string.Empty)
                        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"]);
                }
            }
            catch
            {
            }
        }

        public void ra_RemandedFromCourtDate2(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"].ToString() != string.Empty)
                    {
                        DateTime courtDate = Convert.ToDateTime(e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"].ToString());
                        ra.Text = String.Format("{0}", courtDate.ToString("yyyy-MM-dd"));
                    }
                }
            }
            catch
            {
            }
        }

        //Barry Dickson
        public void ra_Easy2(object sender, LayingOutEventArgs e)
        {
            try
            {
                //dls 080326 - always show easypay number, if there is one

                RecordArea ra = (RecordArea)sender;
                //if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                //{
                //    if (e.LayoutWriter.RecordSets.Current["Total"] != string.Empty)
                //        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"]);

                //}

                if (e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"].ToString() != string.Empty)
                        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"]);
                }
            }
            catch { }
        }

        private void imagePlaceHolder_LaidOut2(object sender, PlaceHolderLaidOutEventArgs e)
        {
            try
            {
                //byte[] buffer = (byte[])this.ds.Tables[0].Rows[0]["ScanImage1"];

                // david lin 20100324 - remove images from database
                //byte[] buffer = e.LayoutWriter.RecordSets.Current["Max_SI"] == System.DBNull.Value ? null : (byte[])e.LayoutWriter.RecordSets.Current["Max_Si"];                
                byte[] buffer = null;
                if (e.LayoutWriter.RecordSets.Current["Max_SI"] != System.DBNull.Value)
                {
                    ScanImageDB imgDB = new ScanImageDB(this.connectionString);
                    ScanImageDetails imgMax = imgDB.GetImageFullPath(Convert.ToInt32(e.LayoutWriter.RecordSets.Current["Max_SI"]));
                    WebService webService = new WebService();
                    buffer = webService.GetImagesFromRemoteFileServer(imgMax);
                }

                //MemoryStream ms = new MemoryStream(buffer, false);
                //System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(ms);
                //ceTe.DynamicPDF.PageElements.Image image = new ceTe.DynamicPDF.PageElements.Image(bmp, 0, 0);
                //float f = 200F / image.Width;
                //image.Width = image.Width * f;
                //image.Height = image.Height * f;
                //e.ContentArea.Add(image);
                if (buffer != null)
                {
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(buffer), 0, 0);
                    img.Height = 72.0F;            //12
                    img.Width = 90.0F;             //15
                    //dls 091126 - since we added the page breaks, the X & Y values of the images seem to be reset to 0
                    img.X = 0.0F;
                    img.Y = 0.0F;
                    e.ContentArea.Add(img);
                }
            }
            catch { }
        }

        private void imagePlaceHolder2_LaidOut2(object sender, PlaceHolderLaidOutEventArgs e)
        {
            try
            {
                //byte[] buffer = (byte[])this.ds.Tables[0].Rows[0]["ScanImage1"];               

                // david lin 20100324 - remove images from database
                //byte[] buffer = e.LayoutWriter.RecordSets.Current["Min_SI"] == System.DBNull.Value ? null : (byte[])e.LayoutWriter.RecordSets.Current["Min_Si"];
                byte[] buffer = null;
                if (e.LayoutWriter.RecordSets.Current["Min_SI"] != System.DBNull.Value)
                {
                    ScanImageDB imgDB = new ScanImageDB(this.connectionString);
                    ScanImageDetails imgMin = imgDB.GetImageFullPath(Convert.ToInt32(e.LayoutWriter.RecordSets.Current["Min_SI"]));
                    WebService webService = new WebService();
                    buffer = webService.GetImagesFromRemoteFileServer(imgMin);
                }

                //MemoryStream ms = new MemoryStream(buffer, false);

                //System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(ms);

                //ceTe.DynamicPDF.PageElements.Image image = new ceTe.DynamicPDF.PageElements.Image(bmp, 0, 0);
                //float f = 200F / image.Width;
                //image.Width = image.Width * f;
                //image.Height = image.Height * f;

                //e.ContentArea.Add(image);
                if (buffer != null)
                {
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(buffer), 0, 0);
                    img.Height = 72.0F;            //12
                    img.Width = 90.0F;             //15
                    //dls 091126 - since we added the page breaks, the X & Y values of the images seem to be reset to 0
                    img.X = 0.0F;
                    img.Y = 0.0F;
                    e.ContentArea.Add(img);
                }
            }
            catch { }
        }

    }
}
