﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.Enquiry;
using Stalberg.TMS;
using System.Data.SqlClient;
using System.Drawing;
using Stalberg.TMS.Data;
using System.IO;
using System.Drawing.Imaging;
using System.Data;
using SIL.AARTO.BLL.PrintManagement;

namespace SIL.AARTO.Web.Controllers.Enquiry
{
    public partial class EnquiryController : Controller
    {
        public ActionResult NonCameraImage()
        {
            Response.Buffer = true;
            Response.ExpiresAbsolute = System.DateTime.Now.AddSeconds(-1);
            Response.Expires = 0;
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "No-Cache");
            try
            {
                bool isExistsImagePath = true;
                using (Bitmap bitmap = ImagePreProcess1(out isExistsImagePath))
                {

                        using (MemoryStream ms = new MemoryStream())
                        {
                            bitmap.Save(ms, ImageFormat.Jpeg);
                            return File(ms.ToArray(), "image/jpeg");
                        }
                }
            }
            catch (Exception ex)
            {
                Bitmap image = null;
                string strNoImagePath = Server.MapPath("~/Content/Image/defaultImg.jpg");

                System.Drawing.Image imageConvert = System.Drawing.Image.FromFile(strNoImagePath);
                image = new Bitmap(imageConvert);
                imageConvert.Dispose();
                
                using (MemoryStream ms = new MemoryStream())
                {
                    image.Save(ms, ImageFormat.Jpeg);
                    return File(ms.ToArray(), "image/jpeg");
                }


            }
        }

        public ActionResult NonCameraImagePartialView(int notIntNo, NoticeSource? source)
        {
            NonCameraImageModel model = new NonCameraImageModel();
            model.NotIntNo = notIntNo;

            if (source == null || source == NoticeSource.unknown)
                model.ImageUrls = GetNonCameraImage(notIntNo);
            //else
            //    model.ImageUrls = GetCameraDocument(notIntNo, source.Value);

            return PartialView("NonCameraImagePartialView",model);
        }

        private string GetNonCameraImage(int notIntNo)
        {
            string str = "";
            NoticeDB notice = new NoticeDB(connectionString);
            SqlDataReader reader = notice.GetGetNonCameraImgs(notIntNo,connectionString);
            while (reader.Read())
            {
               
                string NotIntNo = reader["NotIntNo"] == null ? "" : Convert.ToString(reader["NotIntNo"]);
                string AaDocImgPath = reader["AaDocImgPath"] == null ? "" : Convert.ToString(reader["AaDocImgPath"]);
                string IFSIntNo = reader["IFSIntNo"] == null ? "" : Convert.ToString(reader["IFSIntNo"]);

                str += "NonCameraImage?IFSIntNo=" + HttpUtility.UrlEncode(IFSIntNo)
              + "&JpegName=" + HttpUtility.UrlEncode(AaDocImgPath);
                str += ",";

            }
            if (str.Length > 0)
            {
                str = str.Substring(0, str.Length - 1);
            }
            reader.Close();
            reader.Dispose();

            return str;
        }

        //private string GetCameraDocument(int notIntNo, NoticeSource source)
        //{
        //    return string.Format("NonCameraImage?NoticeIntNo={0}&Source={1}", notIntNo, (int)source);
        //}


        private NoticeSource getSourceFromURL()
        {
            var input = Request.QueryString["Source"];

            if (!String.IsNullOrEmpty(input))
            { 
                try
                {
                    return (NoticeSource)Int32.Parse(input);
                }
                catch (Exception ex)
                {
                }
            }

            return NoticeSource.unknown;
        }

        private Bitmap ImagePreProcess1(out bool isExistsImagePath)
        {
            //2014-02-18 Heidi added for download images(5188)
            isExistsImagePath = true;

            Bitmap image = null;
            string imagePath = null;

            // no longer needs to show images from camera.
            //if (getSourceFromURL() == NoticeSource.camera)
            //{
            //    imagePath = getImagePathForCamera();
            //}
            //else
            //{
            //    imagePath = getImagePathForNonCamera();
            //}

            imagePath = getImagePathForNonCamera();

            

            if (imagePath != null)
            {
                if (!System.IO.File.Exists(imagePath))
                {
                    isExistsImagePath = false; //2014-02-18 Heidi added for download images(5188)
                    // strTestImagePath = string.Format(@"\\{0}\{1}\{2}", testServer.ImageMachineName, testServer.ImageShareName, jpegName);
                    imagePath = Server.MapPath("~/Content/Image/defaultImg.jpg");
                }

                System.Drawing.Image imageConvert = System.Drawing.Image.FromFile(imagePath);//strTestImagePath
                image = new Bitmap(imageConvert);
                imageConvert.Dispose();
            }

            return image;
        }

        //private string getImagePathForCamera()
        //{
        //    string NoticeIntNo = HttpUtility.UrlDecode(Request.QueryString["NoticeIntNo"]);

        //    var printHelper = new PrintFileManagement();


        //    return printHelper.GetPrintFilePath(NoticeIntNo);
        //}

        private string getImagePathForNonCamera()
        {
            string jpegName = HttpUtility.UrlDecode(Request.QueryString["JpegName"]);
            byte[] data = null;
            //WebService service = new WebService();
            if (!String.IsNullOrEmpty(Request.QueryString["IFSIntNo"]))
            {
                int ifsIntNo = Convert.ToInt32(HttpUtility.UrlDecode(Request.QueryString["IFSIntNo"]));
                // get test image from ImageFileServer
                ImageFileServerDB ifsDB = new ImageFileServerDB(connectionString);
                ImageFileServerDetails testServer = ifsDB.GetImageFileServer(ifsIntNo);

                //if (service.RemoteConnect(testServer.ImageMachineName, testServer.ImageShareName, testServer.RemoteUserName, testServer.RemotePassword))
                //{
                return string.Format(@"\\{0}\{1}\{2}", testServer.ImageMachineName, testServer.ImageShareName, jpegName);

            

                // string strTestImagePath = string.Format(@"\\{0}\{1}\sxj\111.JPG", "192.168.1.254","HeidiLiu");

                //if (!File.Exists(strTestImagePath))
                //    strTestImagePath = string.Format(@"\\{0}\{1}\sxj\111.JPG", "192.168.1.254", "HeidiLiu");
                //}
            }

            return "";
        }

        //2014-02-18 Heidi added for download images(5188)
        public FileResult DownLoadImage()
        {
            string imagename = HttpUtility.UrlDecode(Request.QueryString["JpegName"]);
            if (!string.IsNullOrEmpty(imagename))
            {
                imagename = imagename.Replace("\\","-");
            }
            else
            {
                imagename = "defaultImg.jpg";
            }

            Response.Buffer = true;
            Response.ExpiresAbsolute = System.DateTime.Now.AddSeconds(-1);
            Response.Expires = 0;
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "No-Cache");
            try
            {
                bool isExistsImagePath = true;
                using (Bitmap bitmap = ImagePreProcess1(out isExistsImagePath))
                {
                    if (isExistsImagePath == false)
                    {
                        imagename = "defaultImg.jpg";
                    }
                    using (MemoryStream ms = new MemoryStream())
                    {
                        bitmap.Save(ms, ImageFormat.Jpeg);
                        return File(ms.ToArray(), "image/jpeg", imagename);
                    }
                }
            }
            catch (Exception ex)
            {
                Bitmap image = null;
                string strNoImagePath = Server.MapPath("~/Content/Image/defaultImg.jpg");

                System.Drawing.Image imageConvert = System.Drawing.Image.FromFile(strNoImagePath);
                image = new Bitmap(imageConvert);
                imageConvert.Dispose();

                using (MemoryStream ms = new MemoryStream())
                {
                    image.Save(ms, ImageFormat.Jpeg);
                    return File(ms.ToArray(), "image/jpeg","defaultImg.jpg");
                }


            }
        }
    }
}
