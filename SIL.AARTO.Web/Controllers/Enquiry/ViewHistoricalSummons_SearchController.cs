﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Configuration;
using SIL.AARTO.Web.ViewModels.Enquiry;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource.Enquiry;
using Stalberg.TMS;
using Stalberg.TMS.Data;
using SIL.AARTO.BLL.Utility;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.BLL.CameraManagement;
using SIL.Common;
namespace SIL.AARTO.Web.Controllers.Enquiry
{
    public partial class EnquiryController : Controller 
    {

        public ActionResult ViewHistoricalSummons_Search(EnquiryModel model, int page = 0, int IsRedirect = 0)
        {
            if (IsRedirect == 1)
            {
                return RedirectToAction("ViewHistoricalSummons_Search", "Enquiry");
            }
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            model.NoticeNoLookupSubmitToController = "Enquiry";
            model.NoticeNoLookupSubmitToAction = "ViewHistoricalSummons_Search";

            if (TempData["EnquiryModel"] != null)
            {
                model = (EnquiryModel)TempData["EnquiryModel"];
            }

            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            login = userDetails.UserName;

            model.isShowBook = false;
            model.isShowResult = false;
            model.PageSize = Config.PageSize;
            int totalCount = 0;

            if (model.Prefix == null && model.Sequence == null)
            {
                DataBind(model);
            }
            if (model.SelectSubmit == "1")
            {
                SetCDV(model);
            }
          

            if (!validate(model))
            {
                model.ErrorMsg = EnquiryManage.Error1;
            }
            else
            {
                model.IsShowCSCode = true;
                if (string.IsNullOrEmpty(model.Since))
                {
                    model.Since = "2000-01-01";
                }
                model.searchValue = model.searchValue.Replace("/", "").Replace("-", "");
                model.minStatus = CheckMinimumStatusRule();

                // string autIntNo = Session["DefaultAuthority"].ToString();
                SummonsDB sumWithdrawnReissue = new SummonsDB(connectionString);

                // DataSet ds = noticeDB.GetNoticeSearchDS_GetPaged(0, model.searchField, model.searchValue, DateTime.Parse(model.Since), model.minStatus, page, model.PageSize, out totalCount);
                DataSet ds = sumWithdrawnReissue.GetSumWithdrawnReissueSearchDS_GetPaged(0, model.searchField, model.searchValue, DateTime.Parse(model.Since), model.minStatus, page, model.PageSize, out totalCount);
                if (ds != null &&ds.Tables.Count>0&& ds.Tables[1].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                    {
                        NoticeDetail notice = new NoticeDetail();
                        notice.NotIntNo = ds.Tables[1].Rows[i]["NotIntNo"].ToString();
                        notice.NotTicketNo = ds.Tables[1].Rows[i]["NotTicketNo"].ToString().Trim();
                        notice.NotOffenceDate = Convert.ToString(ds.Tables[1].Rows[i]["NotOffenceDate"]) == "" ? "" : DateTime.Parse(ds.Tables[1].Rows[i]["NotOffenceDate"].ToString()).ToString("yyyy-MM-dd");
                        notice.NotRegNo = ds.Tables[1].Rows[i]["NotRegNo"].ToString().Trim();
                        notice.AutName = ds.Tables[1].Rows[i]["AutName"].ToString().Trim();
                        notice.ChgOffenceCode = ds.Tables[1].Rows[i]["ChgOffenceCode"].ToString().Trim();
                       // notice.PaymentDate = DateTime.Parse(ds.Tables[1].Rows[i]["PaymentDate"].ToString()).ToString("yyyy-MM-dd HH:mm");
                        notice.CSCode = ds.Tables[1].Rows[i]["CSCode"].ToString().Trim();
                        notice.CSDescr = ds.Tables[1].Rows[i]["CSDescr"].ToString().Trim();
                        notice.ChgRevFineAmount = double.Parse(ds.Tables[1].Rows[i]["ChgRevFineAmount"].ToString()).ToString("f2");
                       // notice.ChgContemptCourt = ds.Tables[1].Rows[i]["ChgContemptCourt"].ToString().Trim() != "" ? double.Parse(ds.Tables[1].Rows[i]["ChgContemptCourt"].ToString()).ToString("f2") : "";
                        notice.Offender = ds.Tables[1].Rows[i]["Offender"].ToString().Trim();
                        notice.LetterTo = ds.Tables[1].Rows[i]["LetterTo"].ToString().Trim();
                        model.NoticeList.Add(notice);
                    }
                   
                }
                else
                {
                    model.ErrorMsg = ViewHistoricalSummons_SearchRes.lblErrorText1;
                }
            }
            model.TotalCount = totalCount;
            model.PageIndex = page;
            return View("ViewHistoricalSummons_Search", model);
        }

      
    }
}
