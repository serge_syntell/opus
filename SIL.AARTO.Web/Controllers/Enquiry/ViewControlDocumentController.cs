﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIL.AARTO.Web.Controllers.Enquiry
{
    public partial class EnquiryController : Controller
    {
        public ActionResult ViewControlDocument()
        {
            // Get user info from session variable
            if (Session["UserLoginInfo"] == null)
                return RedirectToAction("login", "account");

            int notIntNo;
            if (Request.QueryString["NotIntNo"] == null || !int.TryParse(Request.QueryString["NotIntNo"].ToString(), out notIntNo))
            {
                Response.Write(SIL.AARTO.Web.Resource.Enquiry.ViewControlDocument.NoNotIntNoParam);
                Response.Flush();
                Response.End();
                return View();
            }

            string summonsNo = string.Empty;
            SIL.AARTO.DAL.Entities.NoticeSummons noticeSummonsEntity = noticeSummonsService.GetByNotIntNo(notIntNo).FirstOrDefault();
            if (noticeSummonsEntity != null)
            {
                SIL.AARTO.DAL.Entities.Summons summonsEntity = summonsService.GetBySumIntNo(noticeSummonsEntity.SumIntNo);
                if (summonsEntity != null && summonsEntity.IsMobile != null && summonsEntity.IsMobile.Value && summonsEntity.SumType.ToUpper() == "S56")
                    summonsNo = summonsEntity.SummonsNo.Replace("/", "").Replace("-", "");
                else
                {
                    Response.Write(SIL.AARTO.Web.Resource.Enquiry.ViewControlDocument.IsNotMobileS56);
                    Response.Flush();
                    Response.End();
                    return View();
                }

                if (summonsEntity.MobileControlDocumentGeneratedDate == null || string.IsNullOrWhiteSpace(summonsEntity.MobileControlDocumentGeneratedDate.Value.ToString()))
                {
                    Response.Write(SIL.AARTO.Web.Resource.Enquiry.ViewControlDocument.S56ControlDocummentNoneGenerate);
                    Response.Flush();
                    Response.End();
                    return View();
                }
            }
            else
            {
                Response.Write(SIL.AARTO.Web.Resource.Enquiry.ViewControlDocument.InvalidNotIntNo);
                Response.Flush();
                Response.End();
                return View();
            }

            string filePath = GetControlDocumentPath(notIntNo, summonsNo);
            if (string.IsNullOrEmpty(filePath))
            {
                Response.Write(SIL.AARTO.Web.Resource.Enquiry.ViewControlDocument.PDFPathEmpty);
                Response.Flush();
                Response.End();
                return View();
            }
            else
            {
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.WriteFile(filePath);
                Response.End();
                return View();
            }
            
        }

        private string GetControlDocumentPath(int notIntNo, string summonsNo)
        {
            string controlDocumentPath = string.Empty;

            using (IDataReader reader = imageFileServerService.GetAllTypesOfImgs(notIntNo, 0, ""))
            {
                while (reader.Read())
                {
                    string imageMachineName = reader["ImageMachineName"].ToString().Trim();
                    string imageShareName = reader["ImageShareName"].ToString().Trim();
                    string imagePath = reader["ImagePath"].ToString().Trim();
                    string imageUrl = @"\\" + imageMachineName + @"\" + imageShareName + @"\" + imagePath;

                    if (imagePath.ToUpper().Contains(summonsNo + "_CONDOC"))
                    {
                        controlDocumentPath = imageUrl;
                    }
                }
            }

            return controlDocumentPath;
        }


    }
}
