﻿using SIL.AARTO.BLL.Representation;

namespace SIL.AARTO.Web.Controllers.Representation
{
    public class RepresentationDecisionController : RepresentationController<Representation_Decision> {}
}
