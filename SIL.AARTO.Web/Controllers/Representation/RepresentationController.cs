﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Extensions;
using SIL.AARTO.BLL.Representation;
using SIL.AARTO.BLL.Representation.Model;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.Web.Resource.Representation;
using SIL.ServiceBase;

namespace SIL.AARTO.Web.Controllers.Representation
{
    [AARTOErrorLog]
    [AARTOAuthorize]
    public abstract class RepresentationController<T> : Controller
        where T : RepresentationBase, new()
    {
        protected const string Y = "Y";

        protected readonly string connectionStr = Config.ConnectionString;
        protected readonly T repBase;

        protected RepresentationController()
        {
            this.repBase = new T
            {
                //Controller = this,
                ConnectionString = this.connectionStr,
                GetTMSDomainUrl = () => DomainUrlHelper.GetTMSDomain(),
                ViewBag = ViewBag
            };
            this.repBase.OnConstructorLoaded();
        }

        protected string LastUser
        {
            get { return this.repBase.LastUser; }
        }

        #region Assistant

        #region variables

        [EditorBrowsable(EditorBrowsableState.Never)] bool isExecuting, isPackaged, loadInits = true, prePackage = true;

        #endregion

        protected override IActionInvoker CreateActionInvoker()
        {
            UserLoginInfo user;
            if (HttpContext.Session != null && (user = HttpContext.Session["UserLoginInfo"] as UserLoginInfo) != null)
                this.repBase.UserInfo = user;
            return base.CreateActionInvoker();
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            CheckAttributes(filterContext);

            this.isExecuting = true;
            var Keys = new List<string>(filterContext.ActionParameters.Keys);
            var autIntNoFound = false;

            Keys.ForEach(key =>
            {
                var value = filterContext.ActionParameters[key];

                if (key.Equals2("autIntNo"))
                {
                    LoadInits(Convert.ToInt32(value), ref autIntNoFound);
                }
                else if (value.GetType().IsSubclassOf(typeof(RepModelBase)))
                {
                    var model = (RepModelBase)value;

                    LoadInits(model.AutIntNo, ref autIntNoFound);

                    if (this.prePackage)
                        filterContext.ActionParameters[key] = PackageModel(model);
                }
            });

            this.isExecuting = false;
            this.isPackaged = false;
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (this.isPackaged) return;
            var view = filterContext.Result as ViewResultBase;
            if (view != null && view.Model.GetType().IsSubclassOf(typeof(RepModelBase)))
                view.ViewData.Model = PackageModel(view.Model as RepModelBase);
        }

        void CheckAttributes(ActionExecutingContext filterContext)
        {
            this.loadInits = true;
            this.prePackage = true;

            var attrs = filterContext.ActionDescriptor.GetCustomAttributes(true);

            RepNonPreLoadAttribute rnp;
            if ((rnp = attrs.FirstOrDefault(a =>
                a.GetType() == typeof(RepNonPreLoadAttribute)) as RepNonPreLoadAttribute) != null)
            {
                this.prePackage = rnp.PrePackage;
                this.loadInits = rnp.LoadInits;
            }
        }

        void LoadInits(int autIntNo, ref bool isFound)
        {
            if (!this.loadInits || isFound) return;
            this.repBase.AutIntNo = autIntNo;
            if (autIntNo > 0) isFound = true;
        }

        #region PackageModel (Maintain this and Run this before returning ViewResult if necessary)

        string GetViewByName(string viewName, string folder = "Representation", string extension = ".cshtml")
        {
            if (string.IsNullOrWhiteSpace(viewName)) return viewName;
            viewName = viewName.Trim2();
            if (!viewName.EndsWith(extension, StringComparison.OrdinalIgnoreCase)) viewName += extension;
            return Url.Content(string.Format("~/Views/{0}/{1}", folder, viewName));
        }

        void GenerateBaseInfo<TModel>(TModel model)
            where TModel : RepModelBase, new()
        {
            if (string.IsNullOrWhiteSpace(model.Title))
                model.Title = this.repBase.Title;
            if (string.IsNullOrWhiteSpace(model.RepAction))
                model.RepAction = this.repBase.RepAction.Value;
            if (string.IsNullOrWhiteSpace(model.RepActionName))
                model.RepActionName = this.repBase.RepAction.Name;

            if (this.isExecuting)
            {
                this.repBase.RepControlStatus.Absorbs(model.RepControlStatus);
                model.ClearChangedPropertise();
            }
            else
                model.RepControlStatus.Absorbs(this.repBase.RepControlStatus);
        }

        void GeneratePages<TModel>(TModel model)
            where TModel : RepModelBase, new()
        {
            model.ViewName.Absorbs(this.repBase.ViewName, (p1, v1, p2, v2) =>
            {
                string value;
                if (string.IsNullOrWhiteSpace((string)v1)
                    && !string.IsNullOrWhiteSpace(value = (string)v2))
                    p1.SetValue(model.ViewName, GetViewByName(value), null);
                return false;
            });
        }

        protected virtual TModel PackageModel<TModel>(TModel model = null)
            where TModel : RepModelBase, new()
        {
            if (model == null) model = new TModel();

            GenerateBaseInfo(model);
            GeneratePages(model);

            this.isPackaged = true;
            return model;
        }

        #endregion

        #endregion

        #region Actions

        [RepNonPreLoad]
        public virtual ActionResult Index()
        {
            RepresentationSearchModel model;
            try
            {
                model = new RepresentationSearchModel
                {
                    AutIntNo = this.repBase.AutIntNo,
                    AuthorityList = this.repBase.GetAuthoritySelectList()
                };
            }
            catch (Exception ex)
            {
                model = new RepresentationSearchModel();
                model.AddError(ex.Message);
                ViewBag.Exception = ex;
            }
            PackageModel(model);
            return View(model.ViewName.Index, model);
        }

        [HttpPost]
        [RepNonPreLoad(true)]
        public virtual ActionResult Search(int autIntNo, string ticketNumber, bool hideWarning)
        {
            NoticeModel model;
            try
            {
                model = this.repBase.GetChargeList(autIntNo, ticketNumber, hideWarning);
                //PunchStats805806 enquiry PresentationOfDocument
            }
            catch (Exception ex)
            {
                model = new NoticeModel();
                model.AddError(ex.Message);
                ViewBag.Exception = ex;
            }
            PackageModel(model);
            return PartialView(model.ViewName.Search, model);
        }

        [HttpPost]
        [RepNonPreLoad(true)]
        [ActionScope(ExcludedControllerTypes = new[] { typeof(PresentationOfDocumentController) })]
        public virtual ActionResult Select(int autIntNo, string ticketNumber, int chgIntNo, bool isReadOnly)
        {
            ChargeModel model;
            try
            {
                model = this.repBase.GetRepresentationList(autIntNo, ticketNumber, chgIntNo, isReadOnly);
            }
            catch (Exception ex)
            {
                model = new ChargeModel();
                model.AddError(ex.Message);
                ViewBag.Exception = ex;
            }
            PackageModel(model);
            return PartialView(model.ViewName.RepresentationList, model);
        }

        [HttpPost]
        [RepNonPreLoad(true)]
        [ActionScope(ExcludedControllerTypes = new[] { typeof(PresentationOfDocumentController) })]
        public virtual ActionResult Add(int autIntNo, string ticketNumber, bool isSummons, int chgIntNo, bool isReadOnly, long chgRowVersion)
        {
            RepresentationModel model;

            try
            {
                model = this.repBase.Add(autIntNo, ticketNumber, isSummons, chgIntNo, isReadOnly, chgRowVersion);
                AddReasonsForWithDrawal(model);
            }
            catch (Exception ex)
            {
                model = new RepresentationModel();
                model.AddError(ex.Message);
                ViewBag.Exception = ex;
            }

            if (!model.IsSuccessful)
                return Json(model.GetMessageResult());

            PackageModel(model);
            return PartialView(model.ViewName.Editor, model);
        }

        [HttpPost]
        [RepNonPreLoad(true)]
        [ActionScope(ExcludedControllerTypes = new[] { typeof(PresentationOfDocumentController) })]
        public virtual ActionResult Edit(int autIntNo, string ticketNumber, bool isSummons, int chgIntNo, bool isReadOnly, int repIntNo, long chgRowVersion)
        {
            RepresentationModel model;

            try
            {
                model = this.repBase.Edit(autIntNo, ticketNumber, isSummons, chgIntNo, isReadOnly, repIntNo, chgRowVersion);

                AddReasonsForWithDrawal(model);
            }
            catch (Exception ex)
            {
                model = new RepresentationModel();
                model.AddError(ex.Message);
                ViewBag.Exception = ex;
            }

            if (!model.IsSuccessful)
                return Json(model.GetMessageResult());

            PackageModel(model);
            return PartialView(model.ViewName.Editor, model);
        }

        private static void AddReasonsForWithDrawal(RepresentationModel model)
        {

            var context = new SIL.AARTO.Web.DAL.OpusDB ( );
            var WithdrawalReasons = context.WithdrawlReasons.Where ( x => x.IsEnabled == true ).ToList( );

            foreach (var WithdrawalReason in WithdrawalReasons)
            {
                model.WithdrawalReasons.Add ( WithdrawalReason.RWRIntNo, WithdrawalReason.RWRName );    
            }

        }

        [HttpPost]
        [ActionScope(IncludedControllerTypes = new[] { typeof(RepresentationRegisterController) })]
        public virtual ActionResult Save(RepresentationModel model)
        {
            MessageResult result;
            try
            {
                result = this.repBase.Save(ref model).GetMessageResult();
            }
            catch (Exception ex)
            {
                result = new MessageResult();
                result.AddError(ex.Message);
                ViewBag.Exception = ex;
            }
            return Json(result);
        }

        [HttpPost]
        [ActionScope(ExcludedControllerTypes = new[]
        {
            typeof(PresentationOfDocumentController),
            typeof(RepresentationRegisterController)
        })]
        public virtual ActionResult Decide(RepresentationModel model)
        {
            MessageResult result;
            try
            {
                result = this.repBase.Decide(ref model).GetMessageResult();
            }
            catch (Exception ex)
            {
                result = new MessageResult();
                result.AddError(ex.Message);
                ViewBag.Exception = ex;
            }
            return Json(result);
        }

        [HttpPost]
        [ActionScope(ExcludedControllerTypes = new[]
        {
            typeof(PresentationOfDocumentController),
            typeof(ChangeOfOffenderController)
        })]
        public virtual ActionResult Reverse(RepresentationModel model)
        {
            MessageResult result;
            try
            {
                result = this.repBase.Reverse(ref model).GetMessageResult();
            }
            catch (Exception ex)
            {
                result = new MessageResult();
                result.AddError(ex.Message);
                ViewBag.Exception = ex;
            }
            return Json(result);
        }

        [HttpPost]
        [RepNonPreLoad(true)]
        [ActionScope(ExcludedControllerTypes = new[] { typeof(PresentationOfDocumentController) })]
        public virtual ActionResult PostPrintRepLetter(int autIntNo, string ticketNumber,
            int repIntNo, bool isYes, string letterType, string letterTo, string printFileName)
        {
            MessageResult result;
            try
            {
                result = this.repBase.PrintRepLetter(autIntNo, ticketNumber,
                    repIntNo, isYes, letterType, letterTo, printFileName);
            }
            catch (Exception ex)
            {
                result = new MessageResult();
                result.AddError(ex.Message);
                ViewBag.Exception = ex;
            }
            return Json(result);
        }

        #endregion

        #region Ajax Validations

        [HttpPost]
        [RepNonPreLoad]
        [ActionScope(IncludedControllerTypes = new[] { typeof(ChangeOfOffenderController) })]
        public virtual JsonResult CalculateAge(int autIntNo, string idNumber)
        {
            var result = new MessageResult();
            if (!string.IsNullOrWhiteSpace(idNumber)
                && (idNumber = idNumber.Trim()).Length == 13)
            {
                try
                {
                    var birth = idNumber.Substring(0, 6);
                    var year = int.Parse("19" + birth.Substring(0, 2));
                    var month = int.Parse(birth.Substring(2, 2));
                    var day = int.Parse(birth.Substring(4, 2));
                    var age = (int)((DateTime.Now - new DateTime(year, month, day)).TotalDays/365);
                    result.Body = age;
                }
                catch (Exception ex)
                {
                    result.Body = 0;
                    result.AddError(RepresentationResx.InvalidIDNumberInvalidCharactersValues);
                    ViewBag.Exception = ex;
                }
            }
            else if (this.repBase.Rules.AR_4531.GetValueOrDefault())
            {
                result.Body = 0;
            }
            else
            {
                result.Body = 0;
                result.AddError(RepresentationResx.InvalidIDNumberCheckForLength13);
            }
            return Json(result);
        }

        [HttpPost]
        [RepNonPreLoad]
        [ActionScope(IncludedControllerTypes = new[] { typeof(ChangeOfOffenderController) })]
        public virtual JsonResult GetAreaCodes(string town)
        {
            MessageResult result;
            try
            {
                result = this.repBase.GetAreaCodeListResult(town);
            }
            catch (Exception ex)
            {
                result = new MessageResult();
                result.AddError(ex.Message);
                ViewBag.Exception = ex;
            }
            return Json(result);
        }

        [HttpPost]
        [RepNonPreLoad]
        [ActionScope(ExcludedControllerTypes = new[] { typeof(PresentationOfDocumentController) })]
        public virtual JsonResult GetTMSDomain()
        {
            MessageResult result;
            try
            {
                var tmsDomain = DomainUrlHelper.GetTMSDomain();
                result = new MessageResult
                {
                    Body = tmsDomain,
                    IsSuccessful = !string.IsNullOrWhiteSpace(tmsDomain)
                };
            }
            catch (Exception ex)
            {
                result = new MessageResult();
                result.AddError(ex.Message);
                ViewBag.Exception = ex;
            }
            return Json(result);
        }

        [HttpPost]
        [RepNonPreLoad]
        [ActionScope(IncludedControllerTypes = new[]
        {
            typeof(RepresentationRegisterController),
            typeof(RepresentationDecisionController)
        })]
        public virtual JsonResult RepresentationWithNoResultsReport(int autIntNo)
        {
            MessageResult result;
            try
            {
                var tmsDomain = DomainUrlHelper.GetTMSDomain();
                result = new MessageResult
                {
                    Body = string.Format("{0}/Representation_ReportNoResults.aspx?AutIntNo={1}", tmsDomain, autIntNo),
                    IsSuccessful = !string.IsNullOrWhiteSpace(tmsDomain) && autIntNo > 0
                };
            }
            catch (Exception ex)
            {
                result = new MessageResult();
                result.AddError(ex.Message);
                ViewBag.Exception = ex;
            }
            return Json(result);
        }

        #endregion

        #region IDisposable

        bool isDisposed;

        protected override void Dispose(bool disposing)
        {
            if (this.isDisposed) return;
            if (disposing)
                this.repBase.Dispose();
            this.isDisposed = true;
            base.Dispose(disposing);
        }

        #endregion
    }
}
