﻿using System;

namespace SIL.AARTO.Web.Controllers.Representation
{
    public class RepNonPreLoadAttribute : Attribute
    {
        public RepNonPreLoadAttribute(bool loadInits = false)
        {
            LoadInits = loadInits;
            PrePackage = false;
        }

        public bool LoadInits { get; private set; }
        public bool PrePackage { get; private set; }
    }
}
