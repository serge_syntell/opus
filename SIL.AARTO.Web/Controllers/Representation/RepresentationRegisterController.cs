﻿using SIL.AARTO.BLL.Representation;

namespace SIL.AARTO.Web.Controllers.Representation
{
    public class RepresentationRegisterController : RepresentationController<Representation_Register> {}
}
