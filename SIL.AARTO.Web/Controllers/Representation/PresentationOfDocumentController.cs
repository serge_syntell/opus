﻿using System;
using System.Web.Mvc;
using SIL.AARTO.BLL.Representation;
using SIL.AARTO.BLL.Representation.Model;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.Web.Controllers.Representation
{
    public class PresentationOfDocumentController : RepresentationController<Representation_PresentationOfDocument>
    {
        [HttpPost]
        [RepNonPreLoad(true)]
        public JsonResult MakePresentationOfDocument(int autIntNo, string ticketNumber, int chgIntNo, bool isReadOnly, long chgRowVersion)
        {
            MessageResult result;
            try
            {
                result = repBase.MakePresentationOfDocument(autIntNo, ticketNumber, chgIntNo, isReadOnly, chgRowVersion).GetMessageResult();
            }
            catch (Exception ex)
            {
                result = new MessageResult();
                result.AddError(ex.Message);
                ViewBag.Exception = ex;
            }
            return Json(result);
        }
    }
}
