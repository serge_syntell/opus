﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.ViewModels.Representation;
using SIL.AARTO.Web.DAL;
using SIL.AARTO.BLL.EntLib;


namespace SIL.AARTO.Web.Controllers.Representation
{
    public class RepresentationWithDrawlDecisionController : Controller
    {

        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }

        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;


        public ActionResult Index()

        {

            var context = new SIL.AARTO.Web.DAL.OpusDB ( );
            List<RepWithdrawlDecisionReasonViewModel> RWRDModelList = new List<RepWithdrawlDecisionReasonViewModel> ( );
            var WithdrawalReasons = context.WithdrawlReasons.Where(x => x.IsEnabled == true).ToList();

            foreach (var WithdrawalReason in WithdrawalReasons) 
            {

                var RWRDModel = new RepWithdrawlDecisionReasonViewModel ( );

                RWRDModel.RWRIntNo = WithdrawalReason.RWRIntNo;
                RWRDModel.RWRName = WithdrawalReason.RWRName;
                RWRDModel.RWRDescription = WithdrawalReason.RWRDescription;

                RWRDModelList.Add(RWRDModel);
            }

            return View (RWRDModelList); 
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Description"></param>
        /// <param name="Name"></param>
        /// <returns></returns>

        [HttpPost]
        public ActionResult Create ( string Description,string Name )
        {
            Message output = new Message { Status = true};

            try
            {

                var context = new SIL.AARTO.Web.DAL.OpusDB ( );
                var reasonExist = context.WithdrawlReasons.Where(r => r.RWRName == Name).FirstOrDefault();

                if (reasonExist == null)
                {
                    context.WithdrawlReasons.Add(
                        new WithdrawlReason
                        {
                            RWRName = Name,
                            RWRDescription = Description,
                            LastUser = LastUser,
                            IsEnabled = true
                        }
                    );                  
                    
                    context.SaveChanges ( );
                }

                else 
                {
                    output.Status = false;
                    output.Text = "The Reason Name already exist";
                }
            
            }
 
            catch(Exception ex)
            {
                EntLibLogger.WriteLog(LogCategory.General, null, ex.Message);

                output.Status = false;
                output.Text = "Error adding Reason for withdrawl." ;
            }

            return Json(output,"application/json");
        }
        
        

       /// <summary>
       /// 
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>

        public ActionResult Edit(int id)
        {
            using( var context = new SIL.AARTO.Web.DAL.OpusDB ( ))
            {
                var repWithdrawlReason = context.WithdrawlReasons.Find ( id );

                repWithdrawlReason.RWRName = repWithdrawlReason.RWRName;
                repWithdrawlReason.RWRDescription = repWithdrawlReason.RWRDescription;
                repWithdrawlReason.RWRIntNo = repWithdrawlReason.RWRIntNo;
                repWithdrawlReason.LastUser = LastUser;

                return View(repWithdrawlReason);
            }
        }

       

       /// <summary>
       /// 
       /// </summary>
       /// <param name="input"></param>
       /// <returns></returns>

        [HttpPost]
        public ActionResult Edit ( RepWithdrawlDecisionReasonViewModel input )
        {
            try
            {

                using (var context = new SIL.AARTO.Web.DAL.OpusDB())
                {

                    var dbObject = new WithdrawlReason();
                    dbObject = context.WithdrawlReasons.Find(input.RWRIntNo);

                    dbObject.RWRName = input.RWRName;
                    dbObject.RWRDescription = input.RWRDescription;
                    dbObject.LastUser = LastUser;

                    context.SaveChanges();

                    return RedirectToAction("Index");
                }
            }
            catch(Exception ex)
            {
                EntLibLogger.WriteLog(LogCategory.General, null, ex.Message);

                return View();
            }
        }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="id"></param>
      /// <returns></returns>

        public ActionResult Delete(int id)
        {
            using (var context = new SIL.AARTO.Web.DAL.OpusDB())
            {
                var repWithdrawlReason = context.WithdrawlReasons.Find(id);

                repWithdrawlReason.LastUser = LastUser;
                repWithdrawlReason.IsEnabled = false;

                context.SaveChanges();
            }

            return RedirectToAction ( "Index" ); 
        }
        



    }
}
