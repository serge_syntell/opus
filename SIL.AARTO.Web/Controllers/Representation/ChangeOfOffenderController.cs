﻿using System;
using System.Web.Mvc;
using SIL.AARTO.BLL.Representation;
using SIL.AARTO.BLL.Representation.Model;

namespace SIL.AARTO.Web.Controllers.Representation
{
    public class ChangeOfOffenderController : RepresentationController<Representation_ChangeOfOffender>
    {
        [HttpPost]
        public ActionResult InsufficientDetails(RepresentationModel model)
        {
            MessageResult result;
            try
            {
                result = repBase.Reverse(ref model).GetMessageResult();
            }
            catch (Exception ex)
            {
                result = new MessageResult();
                result.AddError(ex.Message);
                ViewBag.Exception = ex;
            }
            return Json(result);
        }
    }
}
