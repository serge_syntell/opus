﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Representation.Model;
using SIL.AARTO.Web.Helpers;

namespace SIL.AARTO.Web.Controllers.Representation
{
   public partial class RepresentationController : Controller
    {
        //
        // GET: /RepresentationWithNoResult/

       public ActionResult RepresentationWithNoResult()
        {
           return View();
        }

       public JsonResult GetRepresentationWithNoResultsUrl(string AutIntNo, string BeginDate = "", string EndDate = "")
       {
           MessageResult result;
           int autIntNo = 0;
           int.TryParse(AutIntNo, out autIntNo);
           try
           {
               if (Session["UserLoginInfo"] == null)
               {
                   result = new MessageResult
                   {
                       Body = "-2",
                       IsSuccessful =false
                   };
                   return Json(result);
               }
               var tmsDomain = DomainUrlHelper.GetTMSDomain();

               if (string.IsNullOrWhiteSpace(tmsDomain) ||autIntNo<=0)
               {
                   result = new MessageResult
                   {
                       Body = "-3",
                       IsSuccessful = false
                   };
                   return Json(result);
               }

               result = new MessageResult
               {
                   Body = string.Format("{0}/Representation_ReportNoResults.aspx?AutIntNo={1}&BeginDate={2}&EndDate={3}", tmsDomain, autIntNo, BeginDate, EndDate),
                   IsSuccessful = !string.IsNullOrWhiteSpace(tmsDomain) && autIntNo > 0
               };
           }
           catch (Exception ex)
           {
               result = new MessageResult();
               result.Body = "-1";
               result.IsSuccessful = false;
               result.AddMessage(ex.Message);
               
           }
           return Json(result);
       }

    }
}
