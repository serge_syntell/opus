using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using SIL.AARTO.BLL.BookManagement.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Admin;

using SIL.AARTO.Web.Helpers;
using System.Collections;
using SIL.AARTO.BLL.BookManagement;
using SIL.AARTO.BLL.Utility;
using System.Text;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.BookManagement
{
    public partial class BookManagementController : Controller
    {
        //
        // GET: /HandInBooks/
        HandInBooks handInBooks = new HandInBooks();

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HandInBooks()
        {
            HandInBookModel model = new HandInBookModel();
            InitModel(model);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HandInBooks(HandInBookModel model)
        {
            if (ModelState.IsValid)
            {
                model.BookList = handInBooks.SearchBooks(model.MetroIntNo, model.DepotIntNo, model.TOIntNo, model.StartNo ?? 0, model.EndNo ?? 0);
            }
            InitModel(model);

            if (Session["HandInBooksSuccessed"] != null && (bool)Session["HandInBooksSuccessed"])
            {
                string bookNo = Session["BookNo"] == null ? "" : Session["BookNo"].ToString();
                ViewData["Message"] = this.Resource("HandInBooksSuccess", new object[] { bookNo });
                Session["HandInBooksSuccessed"] = null;
                Session["BookNo"] = null;
            }
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HandIn(int depotIntNo, int metroIntNo, int officerIntNo, string bookId, string type)
        {
            Message message = new Message();
            try
            {
                StringBuilder bookNum = new  StringBuilder();
                UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
                if (userLofinInfo == null)
                {
                    return Redirect("/Account/Login");
                }
                List<Book> listBook = new List<Book>();
                int status = 0;
                if (type == "H")
                {
                    status = (int)AartobmStatusList.BookHandedInToDepot;
                }
                else
                {
                    status = (int)AartobmStatusList.ReturnedInComplete;
                }

                foreach (string Id in bookId.Split(';'))
                {
                    if (!String.IsNullOrEmpty(Id))
                    {
                        BookS56 book = new BookS56();
                        book.MetrIntNo = metroIntNo;
                        book.BookId = Convert.ToInt32(Id);
                        book.OfficerId = officerIntNo;
                        book.DepotIntNo = depotIntNo;
                        book.BookStatusId = status;
                        book.LastUser = userLofinInfo.UserName;

                        listBook.Add(book);
                    }
                }

                foreach(AartobmBook book in handInBooks.GetBooks(listBook))
                {
                    bookNum.Append(book.AaBmBookNo).Append(";");
                }
                  
                handInBooks.ProcessBooks(listBook, userLofinInfo.AuthIntNo);
                message.Status = true;
                Session["HandInBooksSuccessed"] = true;

                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.HandInBooks, PunchAction.Change);  


                Session["BookNo"] = bookNum;
            }
            catch (Exception ex)
            {
                message.Status = false;
                message.Text = ex.Message;
            }
            return Json(message);
        }

        private void InitModel(HandInBookModel model)
        {
            MetroListItem metroListItem = new MetroListItem();
            metroListItem.MtrIntNo = 0;
            metroListItem.MtrName = this.Resource("SelectMetro");
            List<MetroListItem> listMetro = handInBooks.GetAllMetroList();
            listMetro.Insert(0, metroListItem);

            model.MetroList = new SelectList(listMetro as IEnumerable, "MtrIntNo", "MtrName", model.MetroIntNo);

            DepotListItem depotListItem = new DepotListItem();
            depotListItem.DepotIntNo = 0;
            depotListItem.DepotDescription = this.Resource("SelectDepot");
            List<DepotListItem> listDepot = handInBooks.GetDepotListItem(model.MetroIntNo);
            listDepot.Insert(0, depotListItem);

            model.DepotList = new SelectList(listDepot as IEnumerable, "DepotIntNo", "DepotDescription", model.DepotIntNo);

            TrafficOfficerListItem officerListItem = new TrafficOfficerListItem();
            officerListItem.TOIntNo = 0;
            officerListItem.TOName = this.Resource("SelectOfficer");
            List<TrafficOfficerListItem> listOfficer = handInBooks.GetTrafficOfficers(model.MetroIntNo);
            listOfficer.Insert(0, officerListItem);

            model.OfficerList = new SelectList(listOfficer as IEnumerable, "ToIntNo", "TOName", model.MetroIntNo);
        }
    }
}
