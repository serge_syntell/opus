using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using SIL.AARTO.Web.Helpers;

using SIL.AARTO.BLL.BookManagement.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Admin;
using System.Collections;
using SIL.AARTO.BLL.BookManagement;
using SIL.AARTO.BLL.Utility;
using System.Configuration;

namespace SIL.AARTO.Web.Controllers.BookManagement
{
    [AARTOErrorLog, LanguageFilter]
    public partial class BookManagementController : Controller
    {
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        public string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : ""; }
        }
        
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
        // GET: /BookMonitor/
        BookMonitors bookMonitors = new BookMonitors();
        public ActionResult BookMonitor()
        {
            BookMonitorModel model = new BookMonitorModel();
            InitModel(model);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BookMonitor(BookMonitorModel model)
        {
            if (ModelState.IsValid)
            {
                List<AartobmBook> books = new List<AartobmBook>();
                books = bookMonitors.SearchBooks(model.MetrIntNo, model.DepotIntNo, model.OfficerIntNo, model.Status, model.StartNo ?? 0, model.EndNo ?? 0);
                if (model.BookTypeId > 0)
                {
                    model.BookList = books.Where(b => b.AaBmBookTypeId == model.BookTypeId).ToList();
                }
                else
                {
                    model.BookList = books;
                }
                //model.BookList = bookMonitors.SearchBooks(model.MetrIntNo, model.DepotIntNo, model.OfficerIntNo, model.Status, model.StartNo ?? 0, model.EndNo ?? 0);
            }
            InitModel(model);
            return View(model);
        }

        public ActionResult DocumentMonitor(decimal? bookId)
        {
            DocumentMonitorModel model = new DocumentMonitorModel();
            if (!bookId.HasValue)
            {
                bookId = 0;
            }
            model.BookNo = bookMonitors.GetBookByBookId(bookId.Value).AaBmBookNo;
            model.DocumentList = bookMonitors.GetDocumentsByBookId(bookId.Value);

            return View(model);
        }

        private void InitModel(BookMonitorModel model)
        {
            MetroListItem metroListItem = new MetroListItem();
            metroListItem.MtrIntNo = 0;
            metroListItem.MtrName = this.Resource("SelectMetro");
            List<MetroListItem> listMetro = transferBooks.GetAllMetroList();
            listMetro.Insert(0, metroListItem);

            model.MetroList = new SelectList(listMetro as IEnumerable, "MtrIntNo", "MtrName", model.MetrIntNo);

            DepotListItem depotListItem = new DepotListItem();
            depotListItem.DepotIntNo = 0;
            depotListItem.DepotDescription = this.Resource("SelectDepot");
            List<DepotListItem> listFromDepot = transferBooks.GetDepotListItem(model.MetrIntNo);
            listFromDepot.Insert(0, depotListItem);

            model.DepotList = new SelectList(listFromDepot as IEnumerable, "DepotIntNo", "DepotDescription", model.DepotIntNo);

            TrafficOfficerListItem officerListItem = new TrafficOfficerListItem();
            officerListItem.TOIntNo = 0;
            officerListItem.TOName = this.Resource("SelectOfficer");
            List<TrafficOfficerListItem> listFromOfficer = transferBooks.GetTrafficOfficers(model.MetrIntNo);
            listFromOfficer.Insert(0, officerListItem);

            model.OfficerList = new SelectList(listFromOfficer as IEnumerable, "ToIntNo", "TOName", model.OfficerIntNo);

            BookStatusListItem statusListItem = new BookStatusListItem();
            statusListItem.StatusId = 0;
            statusListItem.StatusName = this.Resource("SelectStatus"); //"Please select a officer";
            List<BookStatusListItem> listStatus = BookStatusManager.GetList();
            listStatus.Insert(0, statusListItem);

            BookTypeItem bookTypeListItem = new BookTypeItem();
            bookTypeListItem.BookTypeId = 0;
            bookTypeListItem.BookTypeName = this.Resource("SelectBookType");
            List<BookTypeItem> listBookType = new BookTypeManager().GetAllBookType();
            listBookType.Insert(0, bookTypeListItem);

            model.BookTypeList = new SelectList(listBookType as IEnumerable, "BookTypeId", "BookTypeName", model.BookTypeId);

            model.BookStatusList = new SelectList(listStatus as IEnumerable, "StatusId", "StatusName", model.Status);

        }
    }
}
