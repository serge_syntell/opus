using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using SIL.AARTO.BLL.BookManagement.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.BLL.BookManagement;
using SIL.AARTO.BLL.Admin;
using System.Collections;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.DAL.Services;
using Stalberg.TMS.Data.Util;
using System.Configuration;
using tms = Stalberg.TMS;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.BookManagement
{
    public partial class BookManagementController : Controller
    {
        //
        // GET: /HandInDocument/
        HandInDocuments handInDocuments = new HandInDocuments();

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HandInDocument()
        {
            HandInDocumentModel model = new HandInDocumentModel();
            UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
            if (userLofinInfo != null)
                model.AuthNo = GetAuthority(userLofinInfo.AuthIntNo).AutNo.Trim();
            model.Rule_9300 = GetAuthorityRule();
            //InitModel(model);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HandInCancelledDocument()
        {
            HandInDocumentModel model = new HandInDocumentModel();
            UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
            if (userLofinInfo == null)
            {
                return Redirect("/Account/Login?ReturnUrl='/BookManagement/HandInCancelledDocument'");
            }
            model.AuthNo = GetAuthority(userLofinInfo.AuthIntNo).AutNo.Trim();

            SIL.AARTO.BLL.Model.AuthorityRuleInfo autRule = handInDocuments.GetAutRuleByCode(userLofinInfo.AuthIntNo, "4830");

            if (autRule.ARString.Trim().Equals("Y"))
            {
                ViewData["showCheckBox"] = true;
            }
            model.Rule_9300 = GetAuthorityRule();
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HandInDocument(string notIntNo)
        {
            //Notice No: 90/154636/764/310126
            Message message = new Message();
            string[] noticeNoArry = notIntNo.Split('/');
            string notPrefix = String.Empty;
            string docNo = String.Empty;
            string autNo = String.Empty;

            if (noticeNoArry.Length >= 3 && noticeNoArry.Length <= 4)
            {
                notPrefix = noticeNoArry[0];
                docNo = noticeNoArry[1];
                autNo = noticeNoArry[2];
            }
            else
            {
                message.Text = this.Resource("InvalidNoticeNumber");
                return Json(message);
            }
            int documentNo = 0;
            try
            {
                documentNo = Convert.ToInt32(docNo);
            }
            catch
            {
                message.Text = this.Resource("DocumentNoIsIncorrect");
                return Json(message);
            }
            UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
            if (userLofinInfo == null)
            {
                return Redirect("/Account/Login?ReturnUrl='/BookManagement/HandInDocument'");
            }
            HandInDocuments handInDocuments = new HandInDocuments();
            //AartobmDocument document = handInDocuments.ProcessHandInDocument(documentNo.ToString(), autNo, userLofinInfo.UserName);
            int status = 0;
            try
            {
                status = handInDocuments.ProcessHandInDocument(notPrefix, documentNo.ToString(), autNo, userLofinInfo.UserName);

                switch (status)
                {
                    case 0:// Hand cancelled document successful
                        message.Status = true;
                        message.Text = this.Resource("SuccessMessage", new object[] { notIntNo });
                       
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.HandInDocument, PunchAction.Change);  

                        break;
                    case -1: //Authority No does not exists
                        message.Text = this.Resource("AuthorityNotExists", new object[] { autNo });
                        break;
                    case -2: // Document No does not exists
                        message.Text = this.Resource("DocumentNoNotExists", new object[] { documentNo });
                        break;
                    case -3:// document status is not incorrect,can not be set to canclled
                        message.Text = this.Resource("DocumentStatusIsIncorect");
                        break;
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
            //if (document != null)
            //{
            //    message.Text = document.AaBmDocNo;
            //    message.Status = true;
            //}
            //else
            //{
            //    message.Text = this.Resource("DocumentNoIsIncorrect");
            //}
            return Json(message);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HandInCancelledDocument(string notIntNo, bool? noAffidavitAllowed)
        {
            //Notice No: 90/154636/764/310126
            Message message = new Message();
            string[] noticeNoArry = notIntNo.Split('/');
            string notPrefix = String.Empty;
            string docNo = String.Empty;
            string autNo = String.Empty;

            if (noticeNoArry.Length >= 3 && noticeNoArry.Length <= 4)
            {
                notPrefix = noticeNoArry[0];
                docNo = noticeNoArry[1];
                autNo = noticeNoArry[2];
            }
            else
            {
                message.Text = this.Resource("InvalidNoticeNumber");
                return Json(message);
            }
            int documentNo = 0;
            try
            {
                documentNo = Convert.ToInt32(docNo);
            }
            catch
            {
                message.Text = this.Resource("DocumentNoIsIncorrect");
                return Json(message);
            }
            UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
            if (userLofinInfo == null)
            {
                return Redirect("/Account/Login?ReturnUrl='/BookManagement/HandInCancelledDocument'");
            }
            HandInDocuments handInDocuments = new HandInDocuments();
            //AartobmDocument document = handInDocuments.ProcessHandInCancelledDocument(documentNo.ToString(), autNo,noAffidavitAllowed, userLofinInfo.UserName);
            int status = 0;
            try
            {
                if (noAffidavitAllowed == null)
                {
                    noAffidavitAllowed = false;
                }
                status = handInDocuments.ProcessHandInCancelledDocument(notPrefix, documentNo.ToString(), autNo, noAffidavitAllowed.Value, userLofinInfo.UserName);
                switch (status)
                {
                    case 0:// Hand cancelled document successful
                        message.Status = true;
                        message.Text = this.Resource("SuccessMessage", new object[] { notIntNo });
                        
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.HandInDocument, PunchAction.Change); 
                        break;
                    case -1: //Authority No does not exists
                        message.Text = this.Resource("AuthorityNotExists", new object[] { autNo });
                        break;
                    case -2: // Document No does not exists
                        message.Text = this.Resource("DocumentNoNotExists", new object[] { documentNo });
                        break;
                    case -3:// document status is not incorrect,can not be set to canclled
                        message.Text = this.Resource("DocumentStatusIsIncorect");
                        break;
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }

            //if (document != null)
            //{
            //    message.Text = document.AaBmDocNo;
            //    message.Status = true;
            //}
            //else
            //{
            //    message.Text = this.Resource("DocumentNoIsIncorrect");
            //}
            return Json(message);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReverseHandledInDocument(string notIntNo, bool? noAffidavitAllowed)
        {
            Message message = new Message();
            string[] noticeNoArry = notIntNo.Split('/');
            string notPrefix = String.Empty;
            string docNo = String.Empty;
            string autNo = String.Empty;

            if (noticeNoArry.Length >= 3 && noticeNoArry.Length <= 4)
            {
                notPrefix = noticeNoArry[0];
                docNo = noticeNoArry[1];
                autNo = noticeNoArry[2];
            }
            else
            {
                message.Text = this.Resource("InvalidNoticeNumber");
                return Json(message);
            }
            int documentNo = 0;
            try
            {
                documentNo = Convert.ToInt32(docNo);
            }
            catch
            {
                message.Text = this.Resource("DocumentNoIsIncorrect");
                return Json(message);
            }
            UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
            if (userLofinInfo == null)
            {
                return Redirect("/Account/Login?ReturnUrl='/BookManagement/HandInDocument'");
            }
            HandInDocuments handInDocuments = new HandInDocuments();
            //AartobmDocument document = handInDocuments.ProcessHandInDocument(documentNo.ToString(), autNo, userLofinInfo.UserName);
            int status = 0;
            try
            {
                status = handInDocuments.ReverseDocument(notPrefix, documentNo.ToString(), autNo, userLofinInfo.UserName);
                //if (status == 0) message.Status = true;
                switch (status)
                {
                    case 0:// Hand cancelled document successful
                        message.Status = true;
                        //message.Text = this.Resource("SuccessMessage", new object[] { notIntNo });
                        
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.HandInDocument, PunchAction.Change);
                        break;
                    case -1: //Authority No does not exists
                        message.Text = string.Format("Authority No: {0}  does not exist", autNo);
                        break;
                    case -2: // Document No does not exists
                        message.Text =string.Format("Document No : {0} does not exist",documentNo);// this.Resource("DocumentNoNotExists", new object[] { documentNo });
                        break;
                    case -3:// document status is not incorrect,can not be set to canclled
                        message.Text =string.Format("Status of document is not 'DocumentHandedInToDepot' or 'DocumentHandedInAsCancelled' and cannot reverse ");// this.Resource("DocumentStatusIsIncorect");
                        break;
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
            //if (document != null)
            //{
            //    message.Text = document.AaBmDocNo;
            //    message.Status = true;
            //}
            //else
            //{
            //    message.Text = this.Resource("DocumentNoIsIncorrect");
            //}
            return Json(message);
        }

        private void InitModel(HandInDocumentModel model)
        {

        }

        #region 2012-09-03 by seawen
        public ActionResult GetTheCDV(string Prefix, string DocumentNo, string AuthrityNo)
        {
            Message message = new Message();
            try
            {
                message.Text = Convert.ToString(GetPrefixIntValue(Prefix) + (2 * Convert.ToInt32(DocumentNo)) + Convert.ToInt32(AuthrityNo));
                message.Status = true;
            }
            catch (Exception e)
            {
                message.Text = e.Message;
            }
            return Json(message);
        }
        public int GetPrefixIntValue(string prefix)
        {
            string conn = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
            Validation validation = Validation.GetInstance(conn);
            return validation.GetIntValue(prefix);
        }
        public Authority GetAuthority(int authIntNo)
        {
            AuthorityService service = new AuthorityService();
            return service.GetByAutIntNo(authIntNo);
        }
        public int GetAuthorityRule()
        {
            string connstr = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
            UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
            tms.AuthorityRulesDetails rule = new tms.AuthorityRulesDetails();
            rule.AutIntNo = (Session["autIntNo"] == null ? 0 : Convert.ToInt32(Session["autIntNo"]));
            rule.ARCode = "9300";
            rule.LastUser = userLofinInfo.UserName;

            tms.DefaultAuthRules authRule = new tms.DefaultAuthRules(rule, connstr);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            return value.Key; //rule.ARNumeric;
        }
        #endregion
    }
}
