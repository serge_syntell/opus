using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using SIL.AARTO.BLL.BookManagement;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.BLL.BookManagement.Model;
using System.Collections;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using System.Configuration;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.BookManagement
{
    public partial class BookManagementController : Controller
    {
        //
        // GET: /BookManagement/
        private static ReceiveBooks receiveBooks = new ReceiveBooks();
        private static NoticePrefixHistoryManager notPrefixManager = new NoticePrefixHistoryManager();
        public ActionResult ReceiveBooks()
        {
            ReceiveBooksModel model = new ReceiveBooksModel();
            InitModel(model);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReceiveBooks(ReceiveBooksModel model, FormCollection collection)
        {
            if (model.IsValid)
            {
                if (model.BookStartNo.HasValue && model.BookEndNo.HasValue)
                {
                    if (receiveBooks.CheckDocumentsExists(model.BookStartNo.Value, model.BookEndNo.Value, model.BookTypeId, model.NPHIntNo.Value, model.MetrIntNo))
                    {
                        ModelState.AddModelError("BookStartNo", this.Resource("DocumentNoHasBeenUsed"));
                        InitModel(model);
                        return View(model);
                    }
                }

                UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];

                Book book = null;
                switch (model.BookTypeId)
                {
                    case (int)AartobmBookTypeList.Section56:
                        book = new BookS56();
                        break;
                    case (int)AartobmBookTypeList.Section341:
                        book = new BookS341();
                        break;
                    case (int)AartobmBookTypeList.AARTO01:
                        book = new BookAarto01();
                        break;
                    case (int)AartobmBookTypeList.AARTO31:
                        book = new BookAarto31();
                        break;
                    default:
                        break;

                }

                book.BookNum = model.BooksNum;
                book.BookStartNo = model.BookStartNo ?? 0;
                book.BookEndNo = model.BookEndNo ?? 0;

                book.PrintingCompany = model.PrintingCompany;
                book.BookStatusId = (int)AartobmStatusList.BookReceivedByDepot;
                //book.BookType = (int)AartobmBookTypeList.Section56;
                book.DepotIntNo = model.DepotIntNo;
                book.LastUser = userLofinInfo.UserName;
                book.MetrIntNo = model.MetrIntNo;
                book.NotPrefixIntNo = model.NPHIntNo ?? 0;

                List<Book> listBook = new List<Book>();
                listBook.Add(book);

                TList<AartobmBook> bmBookList = receiveBooks.ProcessBooks(listBook, userLofinInfo.AuthIntNo);

                //string tempFile = Server.MapPath(@"\Reports\ReceiveBooks.dplx");
                string tempFile = HttpContext.Request.PhysicalApplicationPath + @"\Reports\ReceiveBooks.dplx";
                //int noteNo = receiveBooks.GetNoteNo(book.MetrIntNo, (int)TransactionTypeList.BMReceive);

                int noteNo = BMTransactionManager.GetTransactionNote(bmBookList, model.MetrIntNo, (int)TransactionTypeList.BMReceive, "", userLofinInfo.UserName);

                if (noteNo > 0)
                {
                    Session["PrintData"] = receiveBooks.PrintNote(listBook, noteNo, tempFile);

                    //receiveBooks.SetNoteNo(book.MetrIntNo, (int)TransactionTypeList.BMReceive, noteNo + 1);
                    ViewData["Message"] = this.Resource("ReceiveBooksSuccess", new object[] { book.BookStartNo, book.BookEndNo });

                    ViewData["Successed"] = true;
                }
                //Linda 2012-9-12 add for list report19
                //2013-11-7 Heidi changed for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());
                punchStatistics.PunchStatisticsTransactionAdd(userLofinInfo.AuthIntNo, userLofinInfo.UserName, PunchStatisticsTranTypeList.BookControl,PunchAction.Add);   
                  
            }
            else
            {
                ModelState.AddModelLocalizationError(this, model.GetRuleViolations());
            }

            InitModel(model);

            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetDepotByMetrIntNo(int metroIntNo)
        {
            List<DepotListItem> list = receiveBooks.GetDepotListItem(metroIntNo);

            return Json(list);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetBookEndNo(int bookTypeId, int bookNum, int bookStartNo)
        {
            Message message = new Message();
            int bookSize = receiveBooks.GetBookSize(bookTypeId);
            message.Text = (bookStartNo + bookSize * bookNum - 1).ToString();
            message.Status = true;

            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetPrefixByType(int bookTypeId, int metroIntNo)
        {
            List<NotPrefixItem> items = new List<NotPrefixItem>();
            if (bookTypeId > 0)
            {
                AartobmBookTypeList emBookType = (AartobmBookTypeList)bookTypeId;
                string notType = string.Empty;
                switch (emBookType)
                {
                    case AartobmBookTypeList.Section341:  //H
                    case AartobmBookTypeList.AARTO31:
                        notType = "H";
                        break;
                    case AartobmBookTypeList.Section56:  //M
                    case AartobmBookTypeList.AARTO01:
                        notType = "M";
                        break;
                }

                items = notPrefixManager.GetPrefixByType(notType, metroIntNo);

            }

            return Json(items);

        }

        private void InitModel(ReceiveBooksModel model)
        {
            MetroListItem metroListItem = new MetroListItem();
            metroListItem.MtrIntNo = 0;
            metroListItem.MtrName = this.Resource("SelectMetro");
            List<MetroListItem> listMetro = receiveBooks.GetAllMetroList();
            listMetro.Insert(0, metroListItem);

            model.MetroList = new SelectList(listMetro as IEnumerable, "MtrIntNo", "MtrName", model.MetrIntNo);

            DepotListItem depotListItem = new DepotListItem();
            depotListItem.DepotIntNo = 0;
            depotListItem.DepotDescription = this.Resource("SelectDepot");
            List<DepotListItem> listDepot = receiveBooks.GetDepotListItem(model.MetrIntNo);
            listDepot.Insert(0, depotListItem);

            model.DepotList = new SelectList(listDepot as IEnumerable, "DepotIntNo", "DepotDescription", model.DepotIntNo);

            BookTypeItem bookTypeListItem = new BookTypeItem();
            bookTypeListItem.BookTypeId = 0;
            bookTypeListItem.BookTypeName = this.Resource("SelectBookType");
            List<BookTypeItem> listBookType = new BookTypeManager().GetAllBookType();
            listBookType.Insert(0, bookTypeListItem);

            model.BookTypeList = new SelectList(listBookType as IEnumerable, "BookTypeId", "BookTypeName", model.BookTypeId);

            NotPrefixItem prefixItem = new NotPrefixItem(0, this.Resource("SelectNotPrefix"));
            List<NotPrefixItem> notPrefixItems = new NoticePrefixHistoryManager().GetPrefixByType("", model.MetrIntNo);
            notPrefixItems.Insert(0, prefixItem);

            model.NotPrefixList = new SelectList(notPrefixItems as IEnumerable, "NPHIntNo", "NotPrefix", model.NPHIntNo);

        }
    }
}
