using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using SIL.AARTO.Web.Helpers;

using SIL.AARTO.BLL.BookManagement.Model;
using SIL.AARTO.BLL.Admin;
using System.Collections;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;

using SIL.AARTO.BLL.BookManagement;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.BookManagement
{
    public partial class BookManagementController : Controller
    {
        //
        // GET: /TransferBooks/

        TransferBooks transferBooks = new TransferBooks();
        public ActionResult TransferBooks()
        {
            TransferBookModel model = new TransferBookModel();
            InitModel(model);
            int startNo = model.StartNo ?? 0;
            int endNo = model.EndNo ?? 0;
            model.BookList = transferBooks.SearchBooks(model.MetroIntNo, model.DepotFromIntNo, model.TOFromIntNo, startNo, endNo);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TransferBooks(TransferBookModel model, FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                if (collection["Option"] == "Depot")
                {
                    ViewData["DepotOption"] = true;

                }
                else
                {
                    ViewData["DepotOption"] = false;
                    //model.BookList = transferBooks.GetBooks((int)AartobmStatusList.BookIssuedToOfficer, model.DepotFromIntNo,model.TOFromIntNo);
                }
                int startNo = model.StartNo ?? 0;
                int endNo = model.EndNo ?? 0;
                model.BookList = transferBooks.SearchBooks(model.MetroIntNo, model.DepotFromIntNo, model.TOFromIntNo, startNo, endNo);
            }

            InitModel(model);

            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Transfer(int depotFromIntNo, int depotToIntNo, int metroIntNo, int officerFromIntNo, int officerToIntNo, string bookId)
        {
            Message message = new Message();
            UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
            if (userLofinInfo == null)
            {
                return Redirect("/Account/Login?ReturnUrl='/BookManagement/TransferBooks'");
            }
            List<Book> listBook = new List<Book>();

            foreach (string Id in bookId.Split(';'))
            {
                if (!String.IsNullOrEmpty(Id))
                {
                    BookS56 book = new BookS56();
                    book.MetrIntNo = metroIntNo;
                    book.BookId = Convert.ToInt32(Id);
                    book.OfficerId = officerToIntNo;
                    book.DepotIntNo = depotToIntNo;
                    book.BookStatusId = (int)AartobmStatusList.BookTransferedByDepot;
                    book.LastUser = userLofinInfo.UserName;

                    listBook.Add(book);
                }
            }
            int maxNum = 0; 
            if (transferBooks.CheckMaxBooks(listBook, userLofinInfo.AuthIntNo, officerToIntNo,ref maxNum))
            {
                message.Text =string.Format("Maximun of {0} books allowed to an officer",maxNum);// this.Resource("MaxNumOfBooks");
                return Json(message);
            }

            TList<AartobmBook> aartoBookList = transferBooks.ProcessBooks(listBook, userLofinInfo.AuthIntNo);
           
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.TransferBooks, PunchAction.Change);  

            string tempFile = String.Empty;
            if (officerToIntNo == 0)
            {
                //tempFile = Server.MapPath(@"\Reports\TransferBooksBetweenDepots.dplx");
                tempFile = HttpContext.Request.PhysicalApplicationPath + @"\Reports\TransferBooksBetweenDepots.dplx";
            }
            else
            {
                //tempFile = Server.MapPath(@"\Reports\TransferBooksBetweenOfficers.dplx");
                tempFile = HttpContext.Request.PhysicalApplicationPath + @"\Reports\TransferBooksBetweenOfficers.dplx";
            }

            int noteNo = BMTransactionManager.GetTransactionNote(aartoBookList, metroIntNo, (int)TransactionTypeList.BMTransfer, "", userLofinInfo.UserName);

            Session["PrintData"] = transferBooks.PrintNote(listBook, noteNo, depotFromIntNo, officerFromIntNo, tempFile);

            //receiveBooks.SetNoteNo(book.MetrIntNo, (int)TransactionTypeList.BMReceive, noteNo + 1);
            message.Text = "/ReportView.aspx";
            message.Status = true;
            return Json(message);
        }

        private void InitModel(TransferBookModel model)
        {
            MetroListItem metroListItem = new MetroListItem();
            metroListItem.MtrIntNo = 0;
            metroListItem.MtrName = this.Resource("SelectMetro");
            List<MetroListItem> listMetro = transferBooks.GetAllMetroList();
            listMetro.Insert(0, metroListItem);

            model.MetroList = new SelectList(listMetro as IEnumerable, "MtrIntNo", "MtrName", model.MetroIntNo);

            DepotListItem depotListItem = new DepotListItem();
            depotListItem.DepotIntNo = 0;
            depotListItem.DepotDescription = this.Resource("SelectDepot");
            List<DepotListItem> listFromDepot = transferBooks.GetDepotListItem(model.MetroIntNo);
            listFromDepot.Insert(0, depotListItem);

            model.DepotFromList = new SelectList(listFromDepot as IEnumerable, "DepotIntNo", "DepotDescription", model.DepotFromIntNo);

            TrafficOfficerListItem officerListItem = new TrafficOfficerListItem();
            officerListItem.TOIntNo = 0;
            officerListItem.TOName = this.Resource("SelectOfficer");
            List<TrafficOfficerListItem> listFromOfficer = transferBooks.GetTrafficOfficers(model.MetroIntNo);
            listFromOfficer.Insert(0, officerListItem);

            model.OfficerFromList = new SelectList(listFromOfficer as IEnumerable, "ToIntNo", "TOName", model.TOFromIntNo);

            depotListItem = new DepotListItem();
            depotListItem.DepotIntNo = 0;
            depotListItem.DepotDescription = this.Resource("SelectDepot");
            List<DepotListItem> listToDepot = transferBooks.GetDepotListItem(model.MetroIntNo);
            listToDepot.Insert(0, depotListItem);

            model.DepotToList = new SelectList(listToDepot as IEnumerable, "DepotIntNo", "DepotDescription", model.DepotToIntNo);

            officerListItem = new TrafficOfficerListItem();
            officerListItem.TOIntNo = 0;
            officerListItem.TOName = this.Resource("SelectOfficer");
            List<TrafficOfficerListItem> listToOfficer = transferBooks.GetTrafficOfficers(model.MetroIntNo);
            listToOfficer.Insert(0, officerListItem);

            model.OfficerToList = new SelectList(listToOfficer as IEnumerable, "ToIntNo", "TOName", model.TOToIntNo);

            //model.BookList = _IssueBooks.GetBooks((int)AartobmStatusList.BookIssuedToOfficer, model.DepotFromIntNo);

        }

    }
}
