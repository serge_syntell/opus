using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.BLL.BookManagement;
using SIL.AARTO.BLL.BookManagement.Model;
using SIL.AARTO.BLL.Admin;
using System.Collections;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.BookManagement
{
    public partial class BookManagementController : Controller
    {
        //
        // GET: /ReversalBooks/
        ReversalBooks reversalBooks = new ReversalBooks();
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReversalBooks()
        {
            ReversalBookModel model = new ReversalBookModel();
            InitModel(model);
            model.BookList = reversalBooks.SearchBooks(model.MetroIntNo,model.DepotIntNo,0,0,0);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReversalBooks(ReversalBookModel model)
        {
            InitModel(model);
            model.BookList = reversalBooks.SearchBooks(model.MetroIntNo,model.DepotIntNo,0,0,0);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SetBooksReversed(int depotIntNo,int metroIntNo,string bookId)
        {
            Message message = new Message();
            UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
            if (userLofinInfo == null)
            {
                return Redirect("/Account/Login?ReturnUrl='/BookManagement/ReversalBooks'");
            }
            List<Book> listBook = new List<Book>();
            foreach (string Id in bookId.Split(';'))
            {
                if (!String.IsNullOrEmpty(Id))
                {
                    BookS56 book = new BookS56();
                    book.MetrIntNo = metroIntNo;
                    book.BookId = Convert.ToInt32(Id);
                    book.DepotIntNo = depotIntNo;
                    book.BookStatusId = (int)AartobmStatusList.BookReversed;
                    book.LastUser = userLofinInfo.UserName;

                    listBook.Add(book);
                }
            }

            reversalBooks.ProcessBooks(listBook,userLofinInfo.AuthIntNo);
            message.Status = true;
           
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.ReversalBooks, PunchAction.Change);  

            return Json(message);
        }
        private void InitModel(ReversalBookModel model)
        {
            MetroListItem metroListItem = new MetroListItem();
            metroListItem.MtrIntNo = 0;
            metroListItem.MtrName = this.Resource("SelectMetro");
            List<MetroListItem> listMetro = issueBooks.GetAllMetroList();
            listMetro.Insert(0, metroListItem);

            model.MetroList = new SelectList(listMetro as IEnumerable, "MtrIntNo", "MtrName", model.MetroIntNo);

            DepotListItem depotListItem = new DepotListItem();
            depotListItem.DepotIntNo = 0;
            depotListItem.DepotDescription = this.Resource("SelectDepot");
            List<DepotListItem> listDepot = issueBooks.GetDepotListItem(model.MetroIntNo);
            listDepot.Insert(0, depotListItem);

            model.DepotList = new SelectList(listDepot as IEnumerable, "DepotIntNo", "DepotDescription", model.DepotIntNo);
 
        }


    }
}
