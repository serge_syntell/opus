using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.BLL.BookManagement.Model;
using SIL.AARTO.BLL.Admin;
using System.Collections;
using SIL.AARTO.BLL.BookManagement;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.BookManagement
{
    public partial class BookManagementController : Controller
    {
        //
        // GET: /IssueBook/
        IssueBooks issueBooks = new IssueBooks();
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult IssueBooks()
        {
            IssueBooksModel model = new IssueBooksModel();
            InitModel(model);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult IssueBooks(IssueBooksModel model)
        {
            if (ModelState.IsValid)
            {
                int startNo = model.StartNo.HasValue ? model.StartNo.Value : 0;
                int endNo = model.EndNo.HasValue ? model.EndNo.Value : 0;
                model.BookList = issueBooks.SearchBooks(model.MetroIntNo, model.DepotIntNo, model.TOIntNo, startNo, endNo);
            }
            InitModel(model);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetOfficerByMetrIntNo(int metroIntNo)
        {
            return Json(issueBooks.GetTrafficOfficers(metroIntNo));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult IssueBooksToOfficer(int depotIntNo, int metroIntNo, int officerIntNo, string bookId)
        {
            Message message = new Message();
            UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
            if (userLofinInfo == null)
            {
                return Redirect("/Account/Login?ReturnUrl='/BookManagement/IssueBooks'");
            }
            List<Book> listBook = new List<Book>();
            foreach (string Id in bookId.Split(';'))
            {
                if (!String.IsNullOrEmpty(Id))
                {
                    BookS56 book = new BookS56();
                    book.MetrIntNo = metroIntNo;
                    book.BookId = Convert.ToInt32(Id);
                    book.OfficerId = officerIntNo;
                    book.DepotIntNo = depotIntNo;
                    book.BookStatusId = (int)AartobmStatusList.BookReceivedByDepot;
                    book.LastUser = userLofinInfo.UserName;

                    listBook.Add(book);
                }
            }
            int maxNum = 0;
            if(issueBooks.CheckMaxBooks(listBook,  userLofinInfo.AuthIntNo, officerIntNo,ref maxNum))
            {
                message.Text = string.Format("Maximun of {0} books allowed to an officer",maxNum); // this.Resource("MaxNumOfBooks");
                return Json(message);
            }
             
            TList<AartobmBook> bmBookList = issueBooks.ProcessBooks(listBook, userLofinInfo.AuthIntNo);
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.IssueBooks, PunchAction.Change);  

            //string tempFile = Server.MapPath(@"\Reports\IssueBooks.dplx");
            string tempFile = HttpContext.Request.PhysicalApplicationPath + @"\Reports\IssueBooks.dplx";

            int noteNo = BMTransactionManager.GetTransactionNote(bmBookList, metroIntNo, (int)TransactionTypeList.BMIssue, "", userLofinInfo.UserName);

            Session["PrintData"] = issueBooks.PrintNote(listBook,noteNo, tempFile);
            message.Text = "/ReportView.aspx";

            message.Status = true;
            return Json(message);
        }


        private void InitModel(IssueBooksModel model)
        {
            MetroListItem metroListItem = new MetroListItem();
            metroListItem.MtrIntNo = 0;
            metroListItem.MtrName = this.Resource("SelectMetro");
            List<MetroListItem> listMetro = issueBooks.GetAllMetroList();
            listMetro.Insert(0, metroListItem);

            model.MetroList = new SelectList(listMetro as IEnumerable, "MtrIntNo", "MtrName", model.MetroIntNo);

            DepotListItem depotListItem = new DepotListItem();
            depotListItem.DepotIntNo = 0;
            depotListItem.DepotDescription = this.Resource("SelectDepot");
            List<DepotListItem> listDepot = issueBooks.GetDepotListItem(model.MetroIntNo);
            listDepot.Insert(0, depotListItem);

            model.DepotList = new SelectList(listDepot as IEnumerable, "DepotIntNo", "DepotDescription", model.DepotIntNo);

            TrafficOfficerListItem officerListItem = new TrafficOfficerListItem();
            officerListItem.TOIntNo = 0;
            officerListItem.TOName = this.Resource("SelectOfficer"); //"Please select a officer";
            List<TrafficOfficerListItem> listOfficer = issueBooks.GetTrafficOfficers(model.MetroIntNo);
            listOfficer.Insert(0, officerListItem);

            model.OfficerList = new SelectList(listOfficer as IEnumerable, "ToIntNo", "TOName", model.MetroIntNo);
        }
    }
}
