using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Mvc;
using System.Xml;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.Report
{

    // Jake 2013-05-21 modified , we need to use session state server to manager session , 
    // and XmlDocument can not serializable so we need to store the xml value into session instead of XmlDocument
    [LanguageFilter]
    public partial class ReportController : Controller
    {
        public ActionResult ReportManager()
        {
            ReportModel model = new ReportModel();
            InitReortList(model, null);
            Session["DataFieldList"] = null;
            Session["ItemCount"] = null;
            Session["StoredProcNameList"] = null;
            Session["CourtsList"] = null;
            Session["SelectedCourtsList"] = null;

            CreateNewReportTemplate(model);

            //if (Session["ReportXML"] == null)
            //{
            //    CreateNewReportTemplate(model);
            //}
            //else
            //{
            //    model.xmlDoc = (XmlDocument)Session["ReportXML"];
            //}

            model.DateFrom = DateTime.Now.AddMonths(-1);
            model.DateTo = DateTime.Now;
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReportManager(string button, ReportModel model, FormCollection collection)
        {
            try
            {
                Session["ItemCount"] = null;
                if (Session["ReportXML"] != null)
                {
                    //model.xmlDoc =  (XmlDocument)Session["ReportXML"];
                    if (model.xmlDoc == null) model.xmlDoc = new XmlDocument();
                    model.xmlDoc.LoadXml(Session["ReportXML"].ToString());
                }
                else
                    CreateNewReportTemplate(model);

                XmlDocument xmlDoc = model.xmlDoc;

                model.AutIntNo = collection["AutIntNo"];
                if (button == LocalizationHelper.Resource(this, "btnSelectReport.Value"))
                {
                    if (collection == null || collection["RcIntNo"] == null)
                        return View(model);

                    ReportConfig Rc = SIL.AARTO.BLL.Report.ReportManager.GetReportById(Convert.ToInt32(collection["RcIntNo"]));

                    if (Rc.AutomaticGenerateQuery != null && Rc.AutomaticGenerateQuery.Length > 0)
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            BinaryFormatter formatter = new BinaryFormatter();
                            memoryStream.Write(Rc.AutomaticGenerateQuery, 0, Rc.AutomaticGenerateQuery.Length);
                            memoryStream.Position = 0;
                            model = formatter.Deserialize(memoryStream) as ReportModel;

                            collection["AutomaticGenerateIntervalType"] = model.AutomaticGenerateIntervalType;
                            collection["AutomaticGenerateFormat"] = model.AutomaticGenerateFormat;
                        }
                    }

                    xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(Rc.Template);

                    model.xmlDoc = xmlDoc;
                    model.AutIntNo = Rc.AutIntNo.ToString();
                    model.ReportName = Rc.ReportName;
                    model.ReportCode = Rc.ReportCode;
                    model.StoredProcName = Rc.RspIntNoSource.ReportStoredProcName;
                    Session["ReportXML"] = model.xmlDoc.InnerXml;
                    collection["AutIntNo"] = model.AutIntNo;
                    collection["RspIntNo"] = Rc.RspIntNo.ToString();

                    Session["StoredProcName"] = model.StoredProcName;
                    Session["ItemCount"] = null;
                    Session["PageNumber"] = 1;
                    Session["SelectedCourtsList"] = model.SelectedCourtsList;
                    Session["CourtsList"] = model.CourtsList;
                    Session["SortField"] = null;

                }
                else if (button == LocalizationHelper.Resource(this, "btSaveReport.Value"))
                {
                    SaveReport(model, collection);
                }
                else if (button == LocalizationHelper.Resource(this, "btnDeleteReport.Value"))
                {
                    if (collection == null || collection["RcIntNo"] == null)
                        return View(model);

                    bool isSuccess=SIL.AARTO.BLL.Report.ReportManager.RemoveReportById(Convert.ToInt32(collection["RcIntNo"]));

                    if (isSuccess)
                    {
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        int _autintno=0;
                        if (model.AutIntNo != null)
                        { _autintno = Convert.ToInt32(model.AutIntNo.Trim()); }
                        else
                        { _autintno = AutIntNo; }
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(_autintno, LastUser, PunchStatisticsTranTypeList.ReportManager, PunchAction.Delete);
                    }
                    ResetXML(model);
                    model.ReportName = null;
                    model.ReportCode = null;
                }

                InitReortList(model, collection);


                if (button == LocalizationHelper.Resource(this, "btnAddNewFilters.Value"))
                {
                    AddReortFilters(model, collection);
                }
                else if (button == LocalizationHelper.Resource(this, "btnAddHeadGroupColumn.Value"))
                {
                    AddReortHeadGroupColumn(model, collection);
                }
                else if (button == LocalizationHelper.Resource(this, "btnAddHeadColumn.Value"))
                {
                    AddReortHeadColumn(model, collection);
                }
                else if (button == LocalizationHelper.Resource(this, "btResetXML.Value"))
                {
                    ResetXML(model);
                }
                else if (button == LocalizationHelper.Resource(this, "btnSetStoredProc.Value"))
                {
                    ApplyStoredProc(model, collection);
                }
                else if (button == LocalizationHelper.Resource(this, "btnApplyRemark.Value"))
                {
                    ApplyRemark(model, collection);
                }

                //model.xmlDoc = (XmlDocument)Session["ReportXML"];
                if (model.xmlDoc == null) model.xmlDoc = new XmlDocument();
                model.xmlDoc.LoadXml(Session["ReportXML"].ToString());

                if (Session["StoredProcName"] != null)
                    model.StoredProcName = Session["StoredProcName"].ToString();
                SetupModel(model);

                return View(model);
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteErrorLog(ex, LogCategory.Exception, null);
                throw ex;
            }
        }

        private void AddReortFilters(ReportModel model, FormCollection collection)
        {
            string FilterName = collection["string"].ToString();

            XmlDocument xmlDoc = model.xmlDoc;
            XmlNodeList nodes = model.xmlDoc.GetElementsByTagName("Filters");

            if (nodes != null)
            {
                XmlElement element_Filter = xmlDoc.CreateElement(FilterName);

                if (nodes[0].SelectSingleNode(FilterName) == null)
                    nodes[0].AppendChild(element_Filter);
            }

            Session["ReportXML"] = xmlDoc.InnerXml;
        }

        public void AddReortHeadGroupColumn(ReportModel model, FormCollection collection)
        {
            string ColumnText = collection["HeadGroupColumn"].ToString();
            string ColumnsCount = collection["HeadGroupColumnsCount"].ToString();
            string ColumnColor = collection["HeadGroupColumnColour"].ToString();

            //XmlDocument xmlDoc = (XmlDocument)Session["ReportXML"];
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(Session["ReportXML"].ToString());

            XmlNodeList nodes = xmlDoc.GetElementsByTagName("HeadGroupColumns");
            if (nodes != null)
            {
                XmlElement element_HeadGroupColumn = xmlDoc.CreateElement("HeadGroupColumn");

                XmlElement element_ColumnText = xmlDoc.CreateElement("ColumnText");
                XmlElement element_ColumnCount = xmlDoc.CreateElement("ColumnCount");
                XmlElement element_ColumnColor = xmlDoc.CreateElement("ColumnColor");

                element_ColumnText.InnerText = ColumnText;
                element_ColumnCount.InnerText = ColumnsCount;
                element_ColumnColor.InnerText = ColumnColor;

                element_HeadGroupColumn.AppendChild(element_ColumnText);
                element_HeadGroupColumn.AppendChild(element_ColumnCount);
                element_HeadGroupColumn.AppendChild(element_ColumnColor);

                nodes[0].AppendChild(element_HeadGroupColumn);
            }

            Session["ReportXML"] = xmlDoc.InnerXml;
        }

        public void AddReortHeadColumn(ReportModel model, FormCollection collection)
        {
            string ColumnText = collection["HeadColumn"].ToString();
            string DataField = collection["DataFieldName"].ToString();
            string ColumnFontColor = collection["HeadColumnColour"].ToString();
            bool Sort = Convert.ToBoolean(collection["ColumnIsSortable"].Split(',')[0]);

            //XmlDocument xmlDoc = (XmlDocument)Session["ReportXML"];
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(Session["ReportXML"].ToString());
            XmlNodeList nodes = xmlDoc.GetElementsByTagName("HeadColumns");
            if (nodes != null)
            {
                XmlElement element_HeadColumn = xmlDoc.CreateElement("HeadColumn");

                XmlElement element_ColumnText = xmlDoc.CreateElement("ColumnText");
                XmlElement element_DataField = xmlDoc.CreateElement("DataField");
                XmlElement element_ColumnFontColor = xmlDoc.CreateElement("ColumnFontColor");
                XmlElement element_Sort = xmlDoc.CreateElement("Sort");

                element_ColumnText.InnerText = ColumnText;
                element_DataField.InnerText = DataField;
                element_ColumnFontColor.InnerText = ColumnFontColor;
                element_Sort.InnerText = Sort.ToString();

                element_HeadColumn.AppendChild(element_ColumnText);
                element_HeadColumn.AppendChild(element_DataField);
                element_HeadColumn.AppendChild(element_ColumnFontColor);
                element_HeadColumn.AppendChild(element_Sort);

                nodes[0].AppendChild(element_HeadColumn);
            }

            Session["ReportXML"] = xmlDoc.InnerXml;
        }

        public ActionResult ShowPage(int PageNumber, string SortColumn, ReportModel model)
        {
            model.PageNumber = PageNumber;
            Session["PageNumber"] = model.PageNumber;

            String strASC = "ASC";
            string strDesc = "DESC";

            if (Session["SortField"] != null && Session["Sort"] != null && Session["SortField"].ToString() == SortColumn && Session["Sort"].ToString() == strASC)
                model.Sort = strDesc;
            else
                model.Sort = strASC;

            if (!String.IsNullOrEmpty(SortColumn))
                Session["SortField"] = SortColumn;
            Session["Sort"] = model.Sort;


            Message message = new Message();
            message.Status = true;

            return Json(message);
        }

        public void ResetXML(ReportModel model)
        {
            Session["ItemCount"] = 0;
            Session["PageNumber"] = 1;
            Session["TotalPageCount"] = 1;
            CreateNewReportTemplate(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddCourt(ReportModel model, FormCollection collection)
        {
            Message message = new Message();

            if (collection["Courts[]"] == null && collection["Courts"] == null)
                return Json(message);

            InitCourtList(model);

            TList<Court> CourtList = model.CourtsList;
            TList<Court> SelectedCourtList = model.SelectedCourtsList;

            if (collection["Courts"] != null && collection["Courts"].ToString() == "ALL")
            {
                SelectedCourtList.AddRange(CourtList);
                CourtList.Clear();
            }
            else if (collection["Courts[]"] != null)
            {
                string[] Courts = collection["Courts[]"].Split(',');
                foreach (string Court in Courts)
                {
                    Court c = CourtList.Find(CourtColumn.CrtIntNo, Convert.ToInt32(Court));

                    if (c != null)
                    {
                        SelectedCourtList.Add(c.Copy());
                        CourtList.Remove(c);
                    }
                }
            }

            Session["CourtsList"] = CourtList;
            Session["SelectedCourtsList"] = SelectedCourtList;

            message.Status = true;
            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RemoveCourt(ReportModel model, FormCollection collection)
        {
            Message message = new Message();

            if (collection["Courts[]"] == null && collection["Courts"] == null)
                return Json(message);

            InitCourtList(model);

            TList<Court> CourtList = model.CourtsList;
            TList<Court> SelectedCourtList = model.SelectedCourtsList;

            if (collection["Courts"] != null && collection["Courts"].ToString() == "ALL")
            {
                CourtList.AddRange(SelectedCourtList);
                SelectedCourtList.Clear();
            }
            else if (collection["Courts[]"] != null)
            {
                string[] Courts = collection["Courts[]"].Split(',');
                foreach (string Court in Courts)
                {
                    Court c = SelectedCourtList.Find(CourtColumn.CrtIntNo, Convert.ToInt32(Court));
                    if (c != null)
                    {
                        CourtList.Add(c.Copy());
                        SelectedCourtList.Remove(c);
                    }
                }
            }

            Session["CourtsList"] = CourtList;
            Session["SelectedCourtsList"] = SelectedCourtList;

            message.Status = true;
            return Json(message);
        }

        public void ApplyStoredProc(ReportModel model, FormCollection collection)
        {
            if (collection["RspIntNo"] == null && string.IsNullOrEmpty(model.StoredProcName) || model.AutIntNo == null)
                return;

            if (collection["RspIntNo"] != null)
            {
                int RspIntNo = Convert.ToInt32(collection["RspIntNo"]);
                ReportStoredProc sp = SIL.AARTO.BLL.Report.ReportManager.GetReportStoredProcById(RspIntNo);
                Session["StoredProcName"] = sp.ReportStoredProcName;
                model.StoredProcName = sp.ReportStoredProcName;
            }

            DataSet ds = SIL.AARTO.BLL.Report.ReportManager.GetData(model);
            DataTable dt = ds.Tables[0];
            if (ds.Tables.Count == 3 && ds.Tables[1].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[1].Rows[0];

                model.TotalPageCount = Convert.ToInt32(dr[0]);
                model.ItemCount = Convert.ToInt32(dr[1]);
                model.PageNumber = Convert.ToInt32(dr[2]);

                Session["ItemCount"] = model.ItemCount;
                Session["TotalPageCount"] = model.TotalPageCount;
            }
            //TryUpdateModel(model);

            List<DataField> DataFieldList = new List<DataField>();

            int i = 0;
            foreach (DataColumn column in dt.Columns)
            {
                DataField df = new DataField();
                df.DataFieldId = i;
                df.DataFieldName = column.ColumnName;
                DataFieldList.Add(df);
            }


            model.DataFieldList = new SelectList(DataFieldList, "DataFieldName", "DataFieldName");
            Session["DataFieldList"] = DataFieldList;
            //return Json("SP doesn't exists");
        }

        public void ApplyRemark(ReportModel model, FormCollection collection)
        {
            //model.xmlDoc = (XmlDocument)Session["ReportXML"];
            if (model.xmlDoc == null) model.xmlDoc = new XmlDocument();
            model.xmlDoc.LoadXml(Session["ReportXML"].ToString());

            string Remark = collection["Remark"].ToString();
            XmlDocument xmlDoc = model.xmlDoc;
            XmlNodeList nodes = model.xmlDoc.GetElementsByTagName("Remark");

            if (nodes != null)
            {
                nodes[0].InnerText = Remark;
            }

            Session["ReportXML"] = xmlDoc.InnerXml;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void SaveReport(ReportModel model, FormCollection collection)
        {
            try
            {
                Message message = new Message();
                message.Status = true;

                if (String.IsNullOrEmpty(model.ReportName) || String.IsNullOrEmpty(model.ReportCode))
                {
                    message.Text = "ReportName or ReportCode cannot be empty.";
                    Response.Write(@"<script language='javascript'>alert('" + message.Text + "');</script>");
                    return;
                }

                if (collection["RspIntNo"] == null)
                {
                    message.Text = "Report Stored proc cannot be empty.";
                    Response.Write(@"<script language='javascript'>alert('" + message.Text + "');</script>");
                    return;
                }

                if (collection["AutomaticGenerate"] != null && collection["AutomaticGenerate"].ToString() == "on")
                    model.AutomaticGenerate = true;
                else
                    model.AutomaticGenerate = false;


                int RspIntNo = Convert.ToInt32(collection["RspIntNo"]);
                string AutomaticGenerateIntervalType = collection["AutomaticGenerateIntervalType"].ToString();
                string AutomaticGenerateFormat = collection["AutomaticGenerateFormat"].ToString();


                if (Session["CourtsList"] != null)
                {
                    model.CourtsList = Session["CourtsList"] as TList<Court>;
                }
                if (Session["SelectedCourtsList"] != null)
                {
                    model.SelectedCourtsList = Session["SelectedCourtsList"] as TList<Court>;
                }

                if (SIL.AARTO.BLL.Report.ReportManager.SaveReport(model, RspIntNo, AutomaticGenerateIntervalType, AutomaticGenerateFormat, (Session["UserLoginInfo"] as UserLoginInfo).UserName))
                {
                    message.Text = "Save report successful";
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    int _autintno = 0;
                    if (model.AutIntNo != null)
                    { _autintno = Convert.ToInt32(model.AutIntNo.Trim()); }
                    else
                    { _autintno = AutIntNo; }
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(_autintno, LastUser, PunchStatisticsTranTypeList.ReportManager, PunchAction.Change);
                }
                else
                {
                    message.Text = "Save report failed";
                }

                Response.Write(@"<script language='javascript'>alert('" + message.Text + "');</script>");
                return;
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteErrorLog(ex, LogCategory.Exception, null);
                throw ex;
            }
        }



        private void InitReortList(ReportModel model, FormCollection collection)
        {
            try
            {
                List<ReortFiltersEnum> Filers = new List<ReortFiltersEnum>();
                Filers.Add(ReortFiltersEnum.Courts);
                Filers.Add(ReortFiltersEnum.Date_Range);
                Filers.Add(ReortFiltersEnum.Notice_Type);
                //Filers.Add(ReortFiltersEnum.Representation_Type);
                //Filers.Add(ReortFiltersEnum.Search_Branch);
                model.ReortFiltersList = new SelectList(Filers, Filers[0]);

                List<NoticeTypeEnum> NoticeTypeFilers = new List<NoticeTypeEnum>();
                NoticeTypeFilers.Add(NoticeTypeEnum.S54);
                NoticeTypeFilers.Add(NoticeTypeEnum.S56);
                NoticeTypeFilers.Add(NoticeTypeEnum.S341);
                model.NoticeTypeList = new SelectList(NoticeTypeFilers, NoticeTypeFilers[0]);


                List<Item> AutomaticGenerateIntervalTypes = new List<Item>();
                AutomaticGenerateIntervalTypes.Add(new Item("Hours", LocalizationHelper.Resource(this, "Hour")));
                AutomaticGenerateIntervalTypes.Add(new Item("Days", LocalizationHelper.Resource(this, "Day")));
                AutomaticGenerateIntervalTypes.Add(new Item("Months", LocalizationHelper.Resource(this, "Month")));
                AutomaticGenerateIntervalTypes.Add(new Item("Daily", LocalizationHelper.Resource(this, "Daily")));
                AutomaticGenerateIntervalTypes.Add(new Item("Weekly", LocalizationHelper.Resource(this, "Weekly")));
                AutomaticGenerateIntervalTypes.Add(new Item("Monthly", LocalizationHelper.Resource(this, "Monthly")));

                if (collection != null && collection["AutomaticGenerateIntervalType"] != null)
                {
                    model.AutomaticGenerateIntervalTypeList = new SelectList(AutomaticGenerateIntervalTypes, "Value", "Text", collection["AutomaticGenerateIntervalType"]);
                }
                else
                {
                    model.AutomaticGenerateIntervalTypeList = new SelectList(AutomaticGenerateIntervalTypes, "Value", "Text", AutomaticGenerateIntervalTypes[0]);
                }

                List<Item> AutomaticGenerateFormats = new List<Item>();
                AutomaticGenerateFormats.Add(new Item("ASCII"));
                AutomaticGenerateFormats.Add(new Item("Word"));
                AutomaticGenerateFormats.Add(new Item("Excel"));
                AutomaticGenerateFormats.Add(new Item("PDF"));

                if (collection != null && collection["AutomaticGenerateFormat"] != null)
                {
                    model.AutomaticGenerateFormatList = new SelectList(AutomaticGenerateFormats, "Value", "Text", collection["AutomaticGenerateFormat"]);
                }
                else
                {
                    model.AutomaticGenerateFormatList = new SelectList(AutomaticGenerateFormats, "Value", "Text", AutomaticGenerateFormats[0]);
                }

                if (Session["DataFieldList"] == null)
                {
                    List<DataField> dfList = new List<DataField>();

                    model.DataFieldList = new SelectList(dfList, "DataFieldName", "DataFieldName");
                    Session["DataFieldList"] = dfList;
                }
                else
                {
                    if (collection != null && collection["DataFieldName"] != null)
                        model.DataFieldList = new SelectList((List<DataField>)Session["DataFieldList"], "DataFieldName", "DataFieldName", collection["DataFieldName"]);
                    else
                        model.DataFieldList = new SelectList((List<DataField>)Session["DataFieldList"], "DataFieldName", "DataFieldName");
                }


                if (Session["LocalAuthorityList"] == null)
                {
                    TList<Authority> Authoritys = SIL.AARTO.BLL.Report.ReportManager.GetAllAuthoritys();
                    model.LocalAuthorityList = new SelectList(Authoritys, AuthorityColumn.AutIntNo.ToString(), AuthorityColumn.AutName.ToString());
                    Session["LocalAuthorityList"] = Authoritys;
                }
                else
                {
                    if (collection != null && collection["AutIntNo"] != null)
                        model.LocalAuthorityList = new SelectList((TList<Authority>)Session["LocalAuthorityList"], AuthorityColumn.AutIntNo.ToString(), AuthorityColumn.AutName.ToString(), collection["AutIntNo"]);
                    else
                        model.LocalAuthorityList = new SelectList((TList<Authority>)Session["LocalAuthorityList"], AuthorityColumn.AutIntNo.ToString(), AuthorityColumn.AutName.ToString());
                }

                if (Session["StoredProcNameList"] == null)
                {
                    TList<ReportStoredProc> StoredProcNames = SIL.AARTO.BLL.Report.ReportManager.GetAllStoredProcName();

                    if (collection != null && collection["RspIntNo"] != null)
                        model.StoredProcNameList = new SelectList(StoredProcNames, ReportStoredProcColumn.RspIntNo.ToString(), ReportStoredProcColumn.ReportStoredProcName.ToString(), collection["RspIntNo"]);
                    else
                        model.StoredProcNameList = new SelectList(StoredProcNames, ReportStoredProcColumn.RspIntNo.ToString(), ReportStoredProcColumn.ReportStoredProcName.ToString());
                    Session["StoredProcNameList"] = StoredProcNames;
                }
                else
                {
                    if (collection != null && collection["RspIntNo"] != null)
                        model.StoredProcNameList = new SelectList((TList<ReportStoredProc>)Session["StoredProcNameList"], ReportStoredProcColumn.RspIntNo.ToString(), ReportStoredProcColumn.ReportStoredProcName.ToString(), collection["RspIntNo"]);
                    else
                        model.StoredProcNameList = new SelectList((TList<ReportStoredProc>)Session["StoredProcNameList"], ReportStoredProcColumn.RspIntNo.ToString(), ReportStoredProcColumn.ReportStoredProcName.ToString());
                }


                TList<ReportConfig> ReportConfigs = SIL.AARTO.BLL.Report.ReportManager.GetReportConfigByIsCustomIsFreeStyle();

                if (collection != null && collection["RcIntNo"] != null)
                    model.ReportTypeList = new SelectList(ReportConfigs, ReportConfigColumn.RcIntNo.ToString(), ReportConfigColumn.ReportName.ToString(), collection["RcIntNo"]);
                else
                    model.ReportTypeList = new SelectList(ReportConfigs, ReportConfigColumn.RcIntNo.ToString(), ReportConfigColumn.ReportName.ToString());

                InitCourtList(model);
                //UpdateModel(model);
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteErrorLog(ex, LogCategory.Exception, null);
                throw ex;
            }
        }

        private void InitCourtList(ReportModel model)
        {
            if (Session["CourtsList"] == null)
            {
                TList<Court> Courts = SIL.AARTO.BLL.Report.ReportManager.GetAllCourts();
                Courts.Sort(CourtColumn.CrtName.ToString());
                model.CourtsList = Courts;
                Session["CourtsList"] = Courts;
            }
            else
            {
                model.CourtsList = (TList<Court>)Session["CourtsList"];
            }


            if (Session["SelectedCourtsList"] == null)
            {
                model.SelectedCourtsList = new TList<Court>();
                Session["SelectedCourtsList"] = new TList<Court>();
            }
            else
            {
                model.SelectedCourtsList = (TList<Court>)Session["SelectedCourtsList"];
            }
        }


        public void CreateNewReportTemplate(ReportModel model)
        {
            XmlDocument xmlDoc = new XmlDocument();
            CreateReportTemplate(xmlDoc);

            model.xmlDoc = xmlDoc;
            Session["ReportXML"] = xmlDoc.InnerXml;
        }

        public void CreateReportTemplate(XmlDocument xmlDoc)
        {
            XmlElement element_Report = xmlDoc.CreateElement("Report");

            XmlElement element_Filters = xmlDoc.CreateElement("Filters");
            XmlElement element_HeadGroup = xmlDoc.CreateElement("HeadGroupColumns");
            XmlElement element_Head = xmlDoc.CreateElement("HeadColumns");
            XmlElement element_Footer = xmlDoc.CreateElement("Footer");
            XmlElement element_Remark = xmlDoc.CreateElement("Remark");


            element_Report.AppendChild(element_Filters);
            element_Report.AppendChild(element_HeadGroup);
            element_Report.AppendChild(element_Head);
            element_Report.AppendChild(element_Footer);
            element_Report.AppendChild(element_Remark);

            xmlDoc.AppendChild(element_Report);
            //"<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
        }
    }
}
