﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Resource.Report;
using SIL.AARTO.Web.ViewModels.Report;

namespace SIL.AARTO.Web.Controllers.Report
{
    public partial class ReportController
    {
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

        readonly ReportAdditionalRequestService rarService = new ReportAdditionalRequestService();
        readonly ReportConfigService rcService = new ReportConfigService();

        ReportAdditionalRequest ModelToEntity(ReportAdditionalRequestEntity model)
        {
            if (model == null) return null;
            var lastUser = ((UserLoginInfo)HttpContext.Session["UserLoginInfo"]).UserName;
            var entity = new ReportAdditionalRequest
            {
                RarIntNo = model.RarIntNo,
                RcIntNo = model.RcIntNo,
                RequestedUser = !string.IsNullOrWhiteSpace(model.RequestedUser) ? model.RequestedUser : lastUser,
                DateFrom = model.DateFrom.GetValueOrDefault(DateTime.Now),
                DateTo = model.DateTo.GetValueOrDefault(DateTime.Now),
                RarActionDate = model.RarActionDate.GetValueOrDefault(DateTime.Now),
                RequestedDate = model.RequestedDate.GetValueOrDefault(DateTime.Now),
                PrintedDate = model.PrintedDate,
                LastUser = lastUser,
                RowVersion = model.RowVersion ?? new byte[8],
                EntityState = model.RarIntNo > 0 ? EntityState.Changed : EntityState.Added
            };
            return entity;
        }

        ReportAdditionalRequestEntity EntityToModel(ReportAdditionalRequest entity)
        {
            if (entity == null) return null;
            var model = new ReportAdditionalRequestEntity
            {
                RarIntNo = entity.RarIntNo,
                RcIntNo = entity.RcIntNo,
                RequestedUser = entity.RequestedUser,
                DateFrom = entity.DateFrom,
                DateTo = entity.DateTo,
                RarActionDate = entity.RarActionDate,
                RequestedDate = entity.RequestedDate,
                PrintedDate = entity.PrintedDate,
                LastUser = entity.LastUser,
                RowVersion = entity.RowVersion,
            };
            return model;
        }

        void FillEntity(ReportAdditionalRequestModel model, int id)
        {
            if (model == null) model = new ReportAdditionalRequestModel();
            model.RarEntity = new ReportAdditionalRequestEntity();
            if (id > 0)
                model.RarEntity = EntityToModel(this.rarService.GetByRarIntNo(id));
            if (model.RarEntity == null) model.RarEntity = new ReportAdditionalRequestEntity();
            //model.RarEntity.Visible = id >= 0;
        }

        void FillList(ReportAdditionalRequestModel model, int pageIndex)
        {
            if (model == null) model = new ReportAdditionalRequestModel();
            model.RarList.Clear();
            int totalCount, index = 0;
            var orderBy = "CASE WHEN PrintedDate IS NULL THEN '9999-12-31' ELSE PrintedDate END DESC, RARIntNo";
            var rauList = this.rarService.GetPaged("", orderBy, pageIndex, Config.PageSize, out totalCount);
            var pageTotal = Math.Ceiling((double)totalCount/Config.PageSize);
            if (pageIndex < 0 || pageIndex > pageTotal - 1)
            {
                if (pageIndex < 0)
                    rauList = this.rarService.GetPaged("", orderBy, 0, Config.PageSize, out totalCount);
                if (pageIndex > pageTotal)
                    rauList = this.rarService.GetPaged("", orderBy, (int)pageTotal, Config.PageSize, out totalCount);
            }
            rauList.ForEach(r =>
            {
                var entity = EntityToModel(r);
                entity.Order = ++index;
                model.RarList.Add(entity);
            });
            model.PageIndex = pageIndex;
            model.PageSize = Config.PageSize;
            model.TotalCount = totalCount;
        }

        List<SelectListItem> GetReportList()
        {
            var query = new ReportConfigQuery();
            query.Append(ReportConfigColumn.RcAllowAddRequest, true.ToString());
            var rcList = this.rcService.Find(query);
            var reportList = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = AdditionalRequest_cshtml.SelectAReport,
                    Value = "0"
                }
            };
            rcList.ForEach(r => reportList.Add(new SelectListItem
            {
                Text = r.ReportName,
                Value = r.RcIntNo.ToString(CultureInfo.InvariantCulture)
            }));
            return reportList;
        }

        void Validation(ReportAdditionalRequestModel model)
        {
            if (model.RarEntity.RcIntNo <= 0)
                ModelState.AddModelError("PleaseSelectAReport", AdditionalRequest_cshtml.PleaseSelectAReport);
            if (model.RarEntity.DateTo.GetValueOrDefault(DateTime.Now) < model.RarEntity.DateFrom.GetValueOrDefault(DateTime.Now))
                ModelState.AddModelError("EndDateLessThanStartDate", AdditionalRequest_cshtml.EndDateLessThanStartDate);
            if (model.RarEntity.RarActionDate.GetValueOrDefault(DateTime.Now) < model.RarEntity.DateTo.GetValueOrDefault(DateTime.Now))
                ModelState.AddModelError("ActionDateLessThanEndDate", AdditionalRequest_cshtml.ActionDateLessThanEndDate);
        }

        void FinalizeMessage(string action)
        {
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrWhiteSpace(action))
                {
                    string msg = null;
                    switch (action)
                    {
                        case "insert":
                            msg = AdditionalRequest_cshtml.CreateSuccessful;
                            break;
                        case "update":
                            msg = AdditionalRequest_cshtml.EditSuccessful;
                            break;
                        case "delete":
                            msg = AdditionalRequest_cshtml.DeleteSuccessful;
                            break;
                    }
                    ViewBag.Message = msg;
                }
            }
            else
            {
                var sb = new StringBuilder();
                foreach (var err in ModelState.SelectMany(ms => ms.Value.Errors))
                {
                    sb.Append(err.ErrorMessage).Append("<br />");
                }
                ViewBag.Error = sb.ToString();
            }
            ModelState.Clear();
        }

        void Clear(ReportAdditionalRequestModel model)
        {
            if (model == null) model = new ReportAdditionalRequestModel();
            model.CurrentId = null;
            model.CurrentRowVersion = null;
            model.Action = null;
            ModelState.Clear();
        }

        public ActionResult AdditionalRequest(int page = 0)
        {
            var model = new ReportAdditionalRequestModel
            {
                PageIndex = page
            };
            return AdditionalRequest(model);
        }

        [HttpPost]
        public ActionResult AdditionalRequest(ReportAdditionalRequestModel model)
        {
            if (model == null) model = new ReportAdditionalRequestModel();

            if (!string.IsNullOrWhiteSpace(model.Action))
            {
                switch (model.Action)
                {
                    case "create":
                    case "edit":
                        FillEntity(model, model.CurrentId.GetValueOrDefault());
                        model.ReportList = GetReportList();
                        model.RarEntity.Visible = true;
                        break;
                    case "save":
                        Save(model);
                        model.RarEntity.Visible = false;
                        break;
                    case "delete":
                        Delete(model.CurrentId.GetValueOrDefault(), model.CurrentRowVersion);
                        model.RarEntity.Visible = false;
                        break;
                }
            }

            FillList(model, model.PageIndex);

            Clear(model);

            return View("AdditionalRequest", model);
        }

        void Save(ReportAdditionalRequestModel model)
        {
            if (model == null) model = new ReportAdditionalRequestModel();
            var action = model.RarEntity.RarIntNo > 0 ? "update" : "insert";
            Validation(model);
            if (ModelState.IsValid)
            {
                try
                {
                    var entity = ModelToEntity(model.RarEntity);
                    this.rarService.Save(entity);
                    if (action == "update")
                    {
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.ReportAdditionalRequestSetup, PunchAction.Change);  

                    }
                    if (action == "insert")
                    {
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.ReportAdditionalRequestSetup, PunchAction.Add);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(ex.GetType().Name, ex.Message);
                }
            }
            model.RarEntity = new ReportAdditionalRequestEntity();
            FinalizeMessage(action);
        }

        void Delete(int id, byte[] rowVersion)
        {
            ModelState.Clear();
            try
            {
                this.rarService.Delete(id, rowVersion);
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.ReportAdditionalRequestSetup, PunchAction.Delete);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(ex.GetType().Name, ex.Message);
            }
            FinalizeMessage("delete");
        }
    }
}
