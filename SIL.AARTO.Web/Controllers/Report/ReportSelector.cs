using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Xml;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.Web.Controllers.Report
{
    public partial class ReportController : Controller
    {
        //
        // GET: /ReportSelector/
        public ActionResult ReportSelector()
        {
            ReportModel model = new ReportModel(); // Init();
            InitReortList(model, null);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReportSelector(string button, ReportModel model, FormCollection collection)
        {
            try
            {
                InitReortList(model, collection);

                if (button == LocalizationHelper.Resource(this, "btnSelectReport.Value"))
                {
                    if (collection == null || collection["RcIntNo"] == null)
                        return View(model);

                    ReportConfig Rc = SIL.AARTO.BLL.Report.ReportManager.GetReportById(Convert.ToInt32(collection["RcIntNo"]));

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(Rc.Template);

                    model.xmlDoc = xmlDoc;
                    model.AutIntNo = Rc.AutIntNo.ToString();
                    model.ReportName = Rc.ReportName;
                    model.ReportCode = Rc.ReportCode;
                    model.StoredProcName = Rc.RspIntNoSource.ReportStoredProcName;
                }
                if (button == LocalizationHelper.Resource(this, "btSearch.Value"))
                {
                    //PunchStats805806 enquiry ReportSelector
                    if (collection == null || collection["RcIntNo"] == null)
                        return View(model);

                    ReportConfig Rc = SIL.AARTO.BLL.Report.ReportManager.GetReportById(Convert.ToInt32(collection["RcIntNo"]));

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(Rc.Template);

                    model.xmlDoc = xmlDoc;
                    model.AutIntNo = Rc.AutIntNo.ToString();
                    model.ReportName = Rc.ReportName;
                    model.ReportCode = Rc.ReportCode;
                    model.StoredProcName = Rc.RspIntNoSource.ReportStoredProcName;

                    ApplyStoredProc(model, collection);
                    Session["PageNumber"] = 1;

                    if (model.xmlDoc.SelectSingleNode("//Filters//Courts") == null)
                    {
                        Session["SelectedCourtsList"] = null;
                        Session["CourtsList"] = null;
                        model.SelectedCourtsList = new TList<Court>();
                    }

                    if (model.xmlDoc.SelectSingleNode("//Filters//Date_Range") == null)
                    {
                        model.DateFrom = new DateTime(1900, 1, 1);
                    }

                    Session["SortField"] = null;
                    SetupModel(model);
                    Session["ReportViewModel"] = model;
                    return View("ReportViewer", model);
                }

                //model.xmlDoc = (XmlDocument)Session["ReportXML"];
                //TryUpdateModel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteErrorLog(ex, LogCategory.Exception, null);
                throw ex;
            }
        }
    }
}
