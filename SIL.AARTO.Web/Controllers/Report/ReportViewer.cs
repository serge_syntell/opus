using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Xml;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.Report
{
    public partial class ReportController : Controller
    {
        //
        // GET: /ReportViewer/
        public ActionResult ReportViewer()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReportViewer(string button, ReportModel model, FormCollection collection)
        {
            try
            {
               
                if (Session["ReportViewModel"] != null)
                {
                    model = (ReportModel)Session["ReportViewModel"];
                }
                else
                {
                    model = new ReportModel(); // Init();
                    InitReortList(model, null);
                    return View("ReportSelector", model);
                }

                if (button == LocalizationHelper.Resource(this, "btBack.Value"))
                {
                    return View("ReportSelector", model);
                }

                SetupModel(model);
                //TryUpdateModel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteErrorLog(ex, LogCategory.Exception, null);
                throw ex;
            }
        }

        private void SetupModel(ReportModel model)
        {
            try
            {
                if (Session["ItemCount"] == null)
                {
                    ApplyStoredProc(model, new FormCollection());
                }

                model.ItemCount = Convert.ToInt32(Session["ItemCount"]);
                model.PageNumber = Convert.ToInt32(Session["PageNumber"]);
                model.TotalPageCount = Convert.ToInt32(Session["TotalPageCount"]);

                if (Session["SortField"] != null)
                    model.SortField = Session["SortField"].ToString();
                if (Session["Sort"] != null)
                    model.Sort = Session["Sort"].ToString();
                if (Session["CourtsList"] != null)
                    model.CourtsList = (TList<Court>)(Session["CourtsList"]);
                if (Session["CourtsList"] != null)
                    model.SelectedCourtsList = (TList<Court>)(Session["SelectedCourtsList"]);

                model.Parameters = model.ReportName;
                if (model.DateFrom > new DateTime(1900, 1, 1))
                    model.Parameters += "<BR/>" + model.DateFrom.ToString("yyyy/MM/dd") + " - " + model.DateTo.ToString("yyyy/MM/dd");

                if (model.SelectedCourtsList != null && model.SelectedCourtsList.Count > 0)
                {
                    model.Parameters += "<BR/>";
                    foreach (Court c in model.SelectedCourtsList)
                    {
                        model.Parameters += " " + c.CrtName;
                    }
                }

                if (model.LANameAndCode == null && model.AutIntNo != null)
                {
                    Authority aut = SIL.AARTO.BLL.Report.ReportManager.GetAuthorityById(Convert.ToInt32(model.AutIntNo));
                    model.LANameAndCode = aut.AutNo + " " + aut.AutName;
                }

                if (model.PageNumber == 0)
                    model.PageNumber = 1;
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteErrorLog(ex, LogCategory.Exception, null);
                throw ex;
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ExportReport(string SaveType)
        {
            try
            {
                Message message = new Message();
                message.Status = true;

                try
                {
                    SIL.AARTO.BLL.Report.ReportExporter reportExporter = new SIL.AARTO.BLL.Report.ReportExporter(this.HttpContext.Server.MapPath(("~")));
                    ReportModel model = (ReportModel)Session["ReportViewModel"];
                    if (model == null)
                    {
                        message.Status = false;
                        message.Text = LocalizationHelper.Resource(this, "ErrorMsg");
                        return Json(message);

                    }

                    string strReportUrl = "";
                    switch (SaveType)
                    {
                        case "Word":
                            strReportUrl = reportExporter.ExportToWord(model, 10);
                            break;
                        case "Excel":
                            strReportUrl = reportExporter.ExportToExcel(model, 10);
                            break;
                        case "PDF":
                            Session["PrintData"] = reportExporter.ExportToPDF(model, false, 10);
                            strReportUrl = @"/ReportView.aspx";
                            break;
                        case "ASCII":
                            strReportUrl = reportExporter.ExportToTxt(model, 10);
                            break;
                        default:
                            break;
                    }

                    //string strHostUrl = string.Format("{0}://{1}/~", Request.Url.Scheme, Request.Url.Authority);

                    strReportUrl = Url.Content("~" + strReportUrl);
                    message.Text = strReportUrl;
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    //punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.Viewreport, PunchAction.Other);
                }
                catch (Exception ex)
                {
                    message.Status = false;
                    message.Text = ex.Message;
                }

                return Json(message);
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteErrorLog(ex, LogCategory.Exception, null);
                throw ex;
            }
        }
    }
}
