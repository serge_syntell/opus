﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Xml;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.Web.Helpers
{
    using SIL.AARTO.Web.Controllers.Report;
    public static class ReportExtension
    {
        public static string DisplayFilters(this HtmlHelper html, ReportModel model)
        {
            return ReportDisplay.DisplayFilters(html, model);
        }

        public static string DisplayHeadGroupColumn(this HtmlHelper html, ReportModel model)
        {
            return ReportDisplay.DisplayHeadGroupColumn(html, model);
        }

        public static string DisplayHeadColumn(this HtmlHelper html, ReportModel model)
        {
            return ReportDisplay.DisplayHeadColumn(html, model);
        }

        public static string DisplayDataColumn(this HtmlHelper html, ReportModel model)
        {
            return ReportDisplay.DisplayDataColumn(html, model);
        }

        public static string DisplayResult(this HtmlHelper html, ReportModel model)
        {
            return ReportDisplay.DisplayResult(html, model);
        }

        public static string DisplayPages(this HtmlHelper html, ReportModel model)
        {
            return ReportDisplay.DisplayPages(html, model);
        }

        public static string TextBox2(this HtmlHelper htmlHelper, string name, object value, object htmlAttributes)
        {
            return string.Format(@"<input name=""{0}"" type=""text"" value=""{1}""  {2} />", name, value, htmlAttributes.ToString().Replace('{', ' ').Replace('}', ' '));
        }

        public static string CheckBox2(this HtmlHelper htmlHelper, string name, object value, object htmlAttributes)
        {
            if (value != null && Convert.ToBoolean(value))
                return string.Format(@"<input name=""{0}"" type=""checkbox"" checked=""checked""  {1} />", name, htmlAttributes.ToString().Replace('{', ' ').Replace('}', ' '));
            else
                return string.Format(@"<input name=""{0}"" type=""checkbox"" {1} />", name, htmlAttributes.ToString().Replace('{', ' ').Replace('}', ' '));
        }

        public static string DropDownList2(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> selectList, object htmlAttributes)
        {
            //string strText = System.Web.Mvc.Html.SelectExtensions.DropDownList(htmlHelper, name, selectList, htmlAttributes);
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format(@"<select name=""{0}"" {1} />", name, htmlAttributes.ToString().Replace('{', ' ').Replace('}', ' ').Replace(',', ' ')));
            if (selectList != null) //2013-12-11 Heidi added for fixed if selectList is null it throw Exception (5084)
            {
                foreach (SelectListItem item in selectList)
                {
                    if (item.Selected)
                        sb.Append(string.Format(@"<option value=""{0}"" selected=true>{1}</option>", item.Value, item.Text));
                    else
                        sb.Append(string.Format(@"<option value=""{0}"">{1}</option>", item.Value, item.Text));
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }

        public static string ApplicationInstalled(this HtmlHelper htmlHelper, string name)
        {
            if (Type.GetTypeFromProgID(name) == null)
                return @"<td style = 'visibility:hidden'>";
            else
                return "<td>";
        }
    }
}

namespace SIL.AARTO.Web.Controllers.Report
{
    public class ReportDisplay
    {
        public static string DisplayFilters(HtmlHelper html, ReportModel model)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table class='logoTable' border='1'>");

            if (model.xmlDoc == null)
                return "";

            XmlNodeList nodes = model.xmlDoc.GetElementsByTagName("Filters");
            if (nodes != null)
            {
                foreach (XmlNode node in nodes[0].ChildNodes)
                {
                    if (node.Name == ReortFiltersEnum.Notice_Type.ToString())
                    {
                        sb.Append("<tr>");

                        sb.Append(@"<td>");
                        sb.Append(html.Resource("lblNoticeType.Text"));
                        sb.Append(@"</td>");

                        sb.Append(@"<td>");
                        sb.Append(html.DropDownList("NoticeType", model.NoticeTypeList, new { id = "ddlNoticeType" }));
                        sb.Append(@"</td>");

                        sb.Append("</tr>");
                    }
                    else if (node.Name ==  ReortFiltersEnum.Date_Range.ToString())
                    {

                        if (model.DateFrom == DateTime.MinValue || model.DateTo == DateTime.MinValue)
                        {
                            model.DateFrom = DateTime.Now.AddMonths(-1);
                            model.DateTo = DateTime.Now;
                        }

                        sb.Append("<tr>");

                        sb.Append(@"<td>");
                        sb.Append(html.Resource("DateStart.Text"));
                        sb.Append(@"</td><td>");
                        sb.Append(html.TextBox("DateFrom", model.DateFrom.ToString("yyyy-MM-dd"), new { id = "txtDateStart" }));
                        //sb.Append(@"<td><input type=""text"" name=""DateStart"" id=""txtDateStart"" value=""" + DateTime.Now.AddMonths(-1).ToString("yyyy-MM-dd") + "\"/>");
                        sb.Append(html.ValidationLocalizationError("DateStart"));
                        sb.Append(@"</td>");

                        sb.Append("</tr>");
                        sb.Append("<tr>");

                        sb.Append(@"<td>");
                        sb.Append(html.Resource("DateEnd.Text"));
                        sb.Append(@"</td><td>");
                        sb.Append(html.TextBox("DateTo", model.DateTo.ToString("yyyy-MM-dd"), new { id = "txtDateEnd" }));
                        //sb.Append(@"<td><input type=""text"" name=""DateEnd"" id=""txtDateEnd"" value=""" + DateTime.Now.ToString("yyyy-MM-dd") + "\"/>");
                        sb.Append(html.ValidationLocalizationError("DateEnd"));
                        sb.Append(@"</td>");

                        sb.Append("</tr>");
                    }
                    else if (node.Name ==  ReortFiltersEnum.Courts.ToString())
                    {
                        sb.Append("<tr>");

                        sb.Append(@"<td colspan=""2"">");
                        sb.Append(html.Resource("lblSelectCourts.Text"));
                        sb.Append(@"</td>");

                        sb.Append("</tr>");

                        sb.Append("<tr>");

                        sb.Append(@"<td colspan=""2"">");

                        sb.Append("<table><tr><td>");
                        sb.Append(CourtListBox("CourtsList", model.CourtsList, "lbCourtsList", "height:250px"));
                        sb.Append("</td><td><table><tr><td>");
                        sb.Append("<input type=\"button\" value= \"" + html.Resource("btAddCourt.Value") + "\" style = \"width:50px\" id=\"btAddCourt\" class=\"btnAdd\"  />");
                        sb.Append("</td></tr><tr><td>");
                        sb.Append("<input type=\"button\" value= \"" + html.Resource("btAddAllCourt.Value") + "\" style = \"width:50px\" id=\"btAddAllCourt\" class=\"btnAdd\"  />");
                        sb.Append("</td></tr><tr><td>");
                        sb.Append("<input type=\"button\" value= \"" + html.Resource("btRemoveCourt.Value") + "\" style = \"width:50px\" id=\"btRemoveCourt\" class=\"btnAdd\"  />");
                        sb.Append("</td></tr><tr><td>");
                        sb.Append("<input type=\"button\" value= \"" + html.Resource("btRemoveAllCourt.Value") + "\" style = \"width:50px\" id=\"btRemoveAllCourt\" class=\"btnAdd\"  />");
                        sb.Append("</td></tr></table></td><td>");
                        sb.Append(CourtListBox("SelectedCourtsList", model.SelectedCourtsList, "lbSelectedCourtsList", "height:250px"));
                        sb.Append("</td></tr></table>");

                        sb.Append(@"</td>");

                        sb.Append("</tr>");
                    }


                }

            }
            sb.Append("</table>");
            string strstst = sb.ToString();
            return sb.ToString();
        }

        private static string CourtListBox(string Name, TList<Court> courtList, string id, string style)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<select id= '");
            sb.Append(id);
            sb.Append("' multiple='multiple' name='");
            sb.Append(Name);
            sb.Append("' style='");
            sb.Append(style);
            sb.Append("'>");

            foreach (Court item in courtList)
            {
                sb.Append("<option value='");
                sb.Append(item.CrtIntNo);
                sb.Append("'>");
                sb.Append(item.CrtName);
                sb.Append("</option>");
            }

            sb.Append("</select>");
            return sb.ToString();
        }



        private static List<Column> DataColunms = null;
        public static string DisplayHeadGroupColumn(HtmlHelper html, ReportModel model)
        {
            StringBuilder sb = new StringBuilder();

            DataColunms = new List<Column>();
            XmlNodeList nodes = model.xmlDoc.GetElementsByTagName("HeadGroupColumns");
            if (nodes != null && nodes[0].ChildNodes.Count > 0)
            {
                sb.Append("<tr class=\"CartListHead\">");
                int iColumnsCount = 0;
                foreach (XmlNode node in nodes[0].ChildNodes)
                {
                    if (node.Name == "HeadGroupColumn")
                    {
                        sb.Append(string.Format(@"<td colspan=""{0}"" style=""background-color:{1}"">{2}</td>",
                            node.ChildNodes[1].InnerText,
                            node.ChildNodes[2].InnerText,
                            node.ChildNodes[0].InnerText));

                        int columns = Convert.ToInt32(node.ChildNodes[1].InnerText);
                        iColumnsCount += columns;

                        //
                        for (int i = 0; i < columns; i++)
                        {
                            Column column = new Column();
                            column.Index = DataColunms.Count;
                            column.HeadGroupColumnText = node.ChildNodes[0].InnerText;
                            column.ColumnColor = node.ChildNodes[2].InnerText;
                            column.FontColor = "black";

                            DataColunms.Add(column);
                        }
                    }
                }
                sb.Append("</tr>");
            }

            return sb.ToString();
        }

        public static string DisplayHeadColumn(HtmlHelper html, ReportModel model)
        {
            StringBuilder sb = new StringBuilder();

            XmlNodeList nodes = model.xmlDoc.GetElementsByTagName("HeadColumns");
            if (nodes != null)
            {
                //Color.AliceBlue
                sb.Append("<tr style='background-color:AliceBlue' class=\"CartListHead\">");

                int i = 0;
                foreach (XmlNode node in nodes[0].ChildNodes)
                {
                    if (node.Name == "HeadColumn")
                    {

                        string strSort = "";
                        if (Convert.ToBoolean(node.ChildNodes[3].InnerText) == true)
                            strSort = @" onclick=""Sort('" + node.ChildNodes[1].InnerText + @"')"" style=""text-decoration:underline""";


                        sb.Append(string.Format(@"<td style=""color:{0};""><Font{2}>{1}</Font></td>",
                            node.ChildNodes[2].InnerText,
                            node.ChildNodes[0].InnerText,
                            strSort));

                        Column column = null;
                        if (i < DataColunms.Count)
                            column = DataColunms[i];
                        else
                        {
                            column = new Column();
                            column.ColumnColor = "white";
                        }

                        column.Index = i;
                        column.DataFiled = node.ChildNodes[1].InnerText;
                        column.FontColor = node.ChildNodes[2].InnerText;
                        column.HeadText = node.ChildNodes[0].InnerText;
                        column.Sort = Convert.ToBoolean(node.ChildNodes[3].InnerText);

                        if (i >= DataColunms.Count)
                            DataColunms.Add(column);

                        i++;
                    }
                }

            }

            return sb.ToString();
        }


        public static string DisplayDataColumn(HtmlHelper html, ReportModel model)
        {
            StringBuilder sb = new StringBuilder();

            DataTable dt = GetData(model);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sb.Append("<tr class=\"CartListItem\">");
                foreach (Column column in DataColunms)
                {
                    string strData = "";
                    if (column.DataFiled != null)
                        strData = dt.Rows[i][column.DataFiled].ToString();

                    sb.Append(string.Format(@"<td style=""color:{0};background-color:{1} "">{2}</td>",
                           column.FontColor,
                           column.ColumnColor,
                           strData));
                }
                sb.Append("</tr>");
            }

            return sb.ToString();
        }

        public static string DisplayResult(HtmlHelper html, ReportModel model)
        {
            StringBuilder sb = new StringBuilder();
            try
            {

                sb.Append(DisplayPages(html, model));
                sb.Append(@"<table align=""center"" cellspacing=""0"" cellpadding=""4"" border=""1"" style=""border-color: Black;
        font-size: 8pt; border-collapse: collapse;"" width=""95%"">");
                sb.Append(DisplayHeadGroupColumn(html, model));
                sb.Append(DisplayHeadColumn(html, model));
                sb.Append(DisplayDataColumn(html, model));
                sb.Append("</table>");
                sb.Append("<BR/>");


                XmlNodeList nodes = model.xmlDoc.GetElementsByTagName("Remark");
                if (nodes != null && nodes.Count > 0)
                {
                    sb.Append(@"<table align=""center"" cellspacing=""0"" style=""border-color: Black;
        font-size: 8pt; border-collapse: collapse;"" cellpadding=""4"" border=""0"" width=""80%""><tr><td><pre><font size=""1"">");
                    sb.Append(nodes[0].InnerText);
                    sb.Append(@"<font></pre></td></tr></table>");
                }

            }
            catch (Exception ex)
            {
                sb.Append("/n" + ex.Message);
            }

            return sb.ToString();
        }

        public static string DisplayPages(HtmlHelper html, ReportModel model)
        {
            int CurrentPage = model.PageNumber;
            StringBuilder sb = new StringBuilder();

            sb.Append(@"<table><tr>");

            int RowsCount = model.ItemCount;
            int ItemCount = 10;
            if (CurrentPage == 0)
                CurrentPage = 1;

            
            int pages = RowsCount / ItemCount;
            if (RowsCount % ItemCount != 0)
                pages += 1;

            if (CurrentPage > 1)
                sb.Append("<td><A class=\"AButton\" onclick=\"ShowPage('" + Convert.ToString(CurrentPage - 1) + "')\"><<</A></td>");
            else
                sb.Append("<td><</td>");

            if (CurrentPage > 2)
            {
                sb.Append("<td><A class=\"AButton\" onclick=\"ShowPage('1')\">1</A></td>");

                if (CurrentPage > 3)
                    sb.Append("<td>...</td>");
            }

            if (CurrentPage >= 2)
                sb.Append("<td><A class=\"AButton\" onclick=\"ShowPage('" + Convert.ToString(CurrentPage - 1) + "')\">" + Convert.ToString(CurrentPage - 1) + "</A></td>");

            sb.Append("<td>" + Convert.ToString(CurrentPage) + "</td>");

            if (CurrentPage < pages)
                sb.Append("<td><A class=\"AButton\" onclick=\"ShowPage('" + Convert.ToString(CurrentPage + 1) + "')\">" + Convert.ToString(CurrentPage + 1) + "</A></td>");

            if (CurrentPage < pages - 1)
            {
                if (CurrentPage < pages - 2)
                    sb.Append("<td>...</td>");

                if (CurrentPage + 1 < pages)
                    sb.Append("<td><a class=\"AButton\" onclick=\"ShowPage('" + Convert.ToString(pages) + "')\">" + Convert.ToString(pages) + "</A></td>");
            }

             if (CurrentPage < pages)
                 sb.Append("<td><A class=\"AButton\" onclick=\"ShowPage('" + Convert.ToString(CurrentPage + 1) + "')\">>></A></td>");
            else
                sb.Append("<td>></td>");

            
            sb.Append("</tr></table>");
            return sb.ToString();
        }

        private static DataTable GetData(ReportModel model)
        {
            try
            {
                DataTable dt = new DataTable();
                if (model.StoredProcName == null || model.StoredProcName == "")
                    return dt;

                DataSet ds = SIL.AARTO.BLL.Report.ReportManager.GetData(model);
                if (ds == null || ds.Tables.Count < 3)
                {
                    throw new Exception("Get Data from report data failed! please check the StoredProc:" + model.StoredProcName);
                }
                return ds.Tables[2];
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteErrorLog(ex, LogCategory.Exception, null);
                throw ex;
            }

            ////if (Session["StoredProcName"] == null)
            ////    return dt;

            //dt.Columns.Add("1", typeof(string));
            //dt.Columns.Add("2", typeof(string));
            //dt.Columns.Add("3", typeof(string));
            //dt.Columns.Add("4", typeof(string));
            //dt.Columns.Add("5", typeof(string));
            //dt.Columns.Add("6", typeof(string));
            //dt.Columns.Add("7", typeof(string));
            //dt.Columns.Add("8", typeof(string));
            //dt.Columns.Add("9", typeof(string));


            //for (int i = 0; i < 300; i++)
            //{
            //    if (i >= pageNumber * 4 && i < (pageNumber + 1) * 4)
            //    {
            //        DataRow dr = dt.NewRow();

            //        dr[0] = i.ToString();
            //        dr[1] = new Random(10).Next().ToString();
            //        dr[2] = new Random(20).Next().ToString();
            //        dr[3] = new Random(30).Next().ToString();
            //        dr[4] = "ABC";
            //        dr[5] = "DEF";
            //        dr[6] = "10.00";
            //        dr[7] = "  ";
            //        dr[8] = "  ";

            //        dt.Rows.Add(dr);
            //    }
            //}
            //return dt;
        }
    }
}
