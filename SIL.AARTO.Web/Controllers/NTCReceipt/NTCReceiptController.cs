﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using SIL.AARTO.BLL.NTCReceipt;
using SIL.AARTO.BLL.Representation.Model;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using Stalberg.TMS;
using NTC = SIL.AARTO.BLL.NTCReceipt;
using SIL.AARTO.Web.Resource.NTCReceipt;
using System.Text;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.Web.Controllers.NTCReceipt
{
    /// <summary>
    /// Non Traffic Charge receipt controller
    /// </summary>
    [AARTOErrorLog, AARTOAuthorize]
    public class NTCReceiptController : Controller
    {
        //
        // GET: /NTCReceipt/

        UserLoginInfo GetUserInfor()
        {
            if (Session["UserLoginInfo"] == null) return null;
            return Session["UserLoginInfo"] as UserLoginInfo;
        }

        static Cashier _cashier;
        string connStr = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

        public ActionResult Pay()
        {
            NTCReceiptModel model = new NTCReceiptModel() { ShowQuoteHeaderAddButton = true };
            InitialReptModel(model);

            if (_cashier != null)
            {
                if (_cashier.MustCountCashBox)
                {
                    string tmsDomain = WebUtility.GetTMSDomainName();
                    return Redirect(tmsDomain + "/CashBoxcount.aspx");
                }
            }

            return View(model);
        }

        public ActionResult TMSReceipt()
        {
            string tmsDomain = WebUtility.GetTMSDomainName();
            return Redirect(tmsDomain + "/CashReceiptTraffic.aspx");
        }

        public ActionResult TMSReceiptReversal()
        {
            string tmsDomain = WebUtility.GetTMSDomainName();
            return Redirect(tmsDomain + "/AdminReceiptReversal.aspx");
        }


        [HttpPost]
        public ActionResult AddQuoteHeader(string referenceNumber, string quoteDate, string idNumber, string surname,
            string initials, string addr1, string addr2, string addr3, string addr4, float totalAmount)
        {
            var result = new MessageResult() { IsSuccessful = false };
            UserLoginInfo userInfo = Session["UserLoginInfo"] as UserLoginInfo;
            if (userInfo == null || Session["autIntNo"] == null)
            {
                result.AddError(NTCReceiptPayment.msg_SessionTimeOut);
            }
            if (string.IsNullOrEmpty(referenceNumber))
                result.AddError(NTCReceiptPayment.msg_ReferenceNumber_IsRequired);
            if (string.IsNullOrEmpty(quoteDate))
                result.AddError(NTCReceiptPayment.msg_QuoteValue_IsInvalid);
            if (string.IsNullOrEmpty(surname))
                result.AddError(NTCReceiptPayment.msg_Surname_IsRequired);
            if (string.IsNullOrEmpty(initials))
                result.AddError(NTCReceiptPayment.msg_Initial_IsRequired);
            if (totalAmount <= 0)
            {
                result.AddError(NTCReceiptPayment.msg_Amount_IsInvalid);
            }
            if (_cashier == null)
            {
                BankAccountDB bankAccount = new BankAccountDB(this.connStr);
                _cashier = bankAccount.GetUserBankAccount(userInfo.UserIntNo);
                if (_cashier == null)
                {
                    result.AddError(NTCReceiptPayment.msg_NoCashierBound);
                    return Json(result);
                }
            }
            if (result.HasError == false)
            {
                QuoteHeader header = NTCReceiptManager.GetQuoteHeaderByRefNumnber(referenceNumber);

                if (header == null)
                {
                    int autIntNo = Convert.ToInt32(Session["autIntNo"]);
                    //JsonNTCReceiptHeader jsonHeader = NTCReceiptManager.Mapping<JsonNTCReceiptHeader>(header);
                    JsonNTCReceiptHeader jsonHeader = new JsonNTCReceiptHeader()
                    {
                        AutIntNo = autIntNo,
                        QuHeReferenceNumber = referenceNumber,
                        QuHeDate = quoteDate,
                        QuHeIDNumber = idNumber,
                        QuHeSurName = surname,
                        QuHeInitials = initials,
                        QuHeAddress1 = addr1,
                        QuHeAddress2 = addr2,
                        QuHeAddress3 = addr3,
                        QuHeAddress4 = addr4,
                        TotalAmount = totalAmount,
                        LastUser = userInfo.UserName,
                    };
                    //Cache cache = new Cache();
                    //string key = string.Format("{0}_{1}", GetUserInfor().UserName, "NTCReceiptCasheEntity");
                    NTCReceiptCasheEntity cashEntity = GetNTCReceiptCashEntityFromCach();
                    if (cashEntity == null)
                    {
                        Session["NTCReceiptCasheEntity"] = new NTCReceiptCasheEntity() { QuoteHeader = jsonHeader };
                    }
                    else
                    {
                        cashEntity.QuoteHeader = jsonHeader;
                    }

                    result.IsSuccessful = true;
                }
                else
                {
                    result.AddError(string.Format(NTCReceiptPayment.msg_ExistsRefNumber, referenceNumber));
                }
            }
            return Json(result);
        }


        [HttpPost]
        public ActionResult AddQuoteDetail(int ntcIntNo)
        {
            var result = new MessageResult() { IsSuccessful = false };

            UserLoginInfo userInfo = Session["UserLoginInfo"] as UserLoginInfo;
            if (userInfo == null || Session["autIntNo"] == null)
            {
                result.AddError(NTCReceiptPayment.msg_SessionTimeOut);
                return Json(result);
            }

            NonTrafficCharge chg = NTCReceiptManager.GetNonTrafficCharge(ntcIntNo);
            NTCReceiptCasheEntity casheEntity = GetNTCReceiptCashEntityFromCach();
            if (casheEntity != null && casheEntity.QuoteDetails != null)
            {
                if (casheEntity.QuoteDetails.Where(c => c.NTCIntNo.Equals(chg.NtcIntNo)).Count() == 0)
                {
                    JsonNTCReceiptDetail detail = new JsonNTCReceiptDetail()
                    {
                        NTCIntNo = chg.NtcIntNo,
                        NTCCode = chg.NtcCode,
                        NTCDescr = chg.NtcDescr,
                        NTCComment = chg.NtcComment,
                        UOMIntNo = chg.UomIntNo,
                        NTCStandardAmount = chg.NtcStandardAmount,
                        QuDeActualAmount = chg.NtcStandardAmount,
                        QuDeChargeQuantity = 1,
                        QuDeGrossAmount = chg.NtcStandardAmount,
                        LastUser = userInfo.UserName
                    };
                    result.Body = detail;
                    casheEntity.QuoteDetails.Add(detail);
                    result.IsSuccessful = true;
                }
                else
                {
                    result.AddError(NTCReceiptPayment.msg_ChargeExists);
                }
            }

            return Json(result);
        }

        [HttpPost]
        public ActionResult DelQuoteDetail(string ntcCode)
        {
            var result = new MessageResult() { IsSuccessful = false };
            NTCReceiptCasheEntity casheEntity = GetNTCReceiptCashEntityFromCach();
            if (casheEntity != null)
            {
                JsonNTCReceiptDetail detail = casheEntity.QuoteDetails.Where(c => c.NTCCode.Equals(ntcCode)).FirstOrDefault();
                if (detail != null)
                {
                    casheEntity.QuoteDetails.Remove(detail);
                    result.IsSuccessful = true;
                }
            }
            else
            {
                result.AddError(NTCReceiptPayment.msg_SessionTimeOut);
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult UpdateQuotedetail(string ntcCode, string comment, int quantity, float actualAmount)
        {
            NTCReceiptModel model = new NTCReceiptModel()
            {
                UnitOfMeasures = NTCReceiptManager.GetAllUnitOfMeasure()
            };

            //var result = new MessageResult() { IsSuccessful = false };
            NTCReceiptCasheEntity casheEntity = GetNTCReceiptCashEntityFromCach();
            if (casheEntity != null)
            {
                JsonNTCReceiptDetail detail = casheEntity.QuoteDetails.Where(c => c.NTCCode.Equals(ntcCode)).FirstOrDefault();
                if (detail != null)
                {
                    detail.NTCComment = comment;
                    detail.QuDeChargeQuantity = quantity;
                    detail.QuDeActualAmount = actualAmount;
                    detail.QuDeGrossAmount = actualAmount * (float)quantity;
                    // detail.UOMIntNo = unitOfMeasure;
                    //result.IsSuccessful = true;
                }
            }
            else
            {
                model.NTCReceiptCashEntity = new NTCReceiptCasheEntity();
                //result.AddError("Session lost, Please re-login our system and do it again.");
            }
            model.NTCReceiptCashEntity = casheEntity;

            //return Json(result);
            return PartialView("_QuoteDetail", model);
        }

        [HttpPost]
        public ActionResult LoadQuoteDetail()
        {
            NTCReceiptModel model = new NTCReceiptModel()
            {
                UnitOfMeasures = NTCReceiptManager.GetAllUnitOfMeasure()
            };

            //var result = new MessageResult() { IsSuccessful = false };
            NTCReceiptCasheEntity casheEntity = GetNTCReceiptCashEntityFromCach();

            model.NTCReceiptCashEntity = casheEntity;

            //return Json(result);
            return PartialView("_QuoteDetail", model);
        }

        [HttpPost]
        public ActionResult AddToPaymentList(JsonNTCReceiptPaymentDetail tran)
        {
            var result = new MessageResult() { IsSuccessful = false };

            switch (tran.ReceiptType)
            {
                case "Cash":
                    break;
                case "Cheque":
                    if (tran.ChequeNo <= 0)
                        result.AddError(NTCReceiptPayment.msg_ChequeNo_IsInvalid);
                    if (string.IsNullOrEmpty(tran.ChequeDrawer))
                        result.AddError(NTCReceiptPayment.msg_Drawer_IsRequired);
                    if (string.IsNullOrEmpty(tran.ChequeBank))
                        result.AddError(NTCReceiptPayment.msg_BankName_IsRequired);
                    if (tran.ChequeDate == Convert.ToDateTime("0001-01-01"))
                        result.AddError(NTCReceiptPayment.msg_ChequeDate_IsRequired);
                    break;
                case "CreditCard":
                case "DebitCard":
                    if (string.IsNullOrEmpty(tran.CardIssuer))
                        result.AddError(NTCReceiptPayment.msg_CardIssuer_IsRequired);
                    if (string.IsNullOrEmpty(tran.NameOnCard))
                        result.AddError(NTCReceiptPayment.msg_NameOnCard_IsRequired);
                    if (tran.ExpiryDate < new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1))
                    {
                        result.AddError(NTCReceiptPayment.msg_CardExpired);
                    }
                    break;
                case "BankPayment":
                    //if (string.IsNullOrEmpty(tran.Reference))
                    //{
                    //    result.AddError(NTCReceiptPayment.msg_BankReference_IsRequired);
                    //}
                    tran.Reference = String.IsNullOrEmpty(tran.Reference) ? "" : tran.Reference;
                    break;
                case "PostalOrder":
                    if (string.IsNullOrEmpty(tran.PostalOrderNumber))
                    {
                        result.AddError(NTCReceiptPayment.msg_PostalOrder_IsRequired);
                    }
                    break;
            }
            if (tran.Amount == 0) // || tran.Amount > 10000)
            {
                result.AddError(NTCReceiptPayment.msg_Amount_IsInvalid);
            }
            if (result.HasError)
            {
                return Json(result);
            }


            NTCReceiptCasheEntity casheEntity = GetNTCReceiptCashEntityFromCach();
            if (casheEntity != null)
            {
                if (casheEntity.PaymentDetails == null) casheEntity.PaymentDetails = new List<JsonNTCReceiptPaymentDetail>();
                if ((decimal)casheEntity.QuoteHeader.TotalAmount >= casheEntity.PaymentDetails.Sum(c => c.Amount) + tran.Amount)
                {
                    tran.Id = System.Guid.NewGuid();
                    casheEntity.PaymentDetails.Add(tran);
                    result.IsSuccessful = true;
                    result.Body = tran;
                }
                else
                {
                    result.AddError(NTCReceiptPayment.msg_TotalAmount_IsDiff);
                }
            }


            return Json(result);
        }

        [HttpPost]
        public ActionResult GetDuePaymentDetail()
        {
            var result = new MessageResult() { IsSuccessful = false };

            NTCReceiptCasheEntity casheEntity = GetNTCReceiptCashEntityFromCach();
            if (casheEntity != null)
            {
                if (casheEntity.PaymentDetails != null)
                {
                    result.Body = casheEntity.PaymentDetails;
                    result.IsSuccessful = true;
                }
            }

            return Json(result);
        }

        [HttpPost]
        public ActionResult DelPayment(string id)
        {
            var result = new MessageResult() { IsSuccessful = false };
            NTCReceiptCasheEntity casheEntity = GetNTCReceiptCashEntityFromCach();
            if (casheEntity != null)
            {
                Guid gid = new Guid(id);
                JsonNTCReceiptPaymentDetail tran = casheEntity.PaymentDetails.Where(c => c.Id.Equals(gid)).FirstOrDefault();
                if (tran != null)
                {
                    casheEntity.PaymentDetails.Remove(tran);
                    result.IsSuccessful = true;
                }
            }

            return Json(result);
        }

        [HttpPost]
        public ActionResult CreatePayment(float totalAmount)
        {
            var result = new MessageResult() { IsSuccessful = false };
            NTCReceiptCasheEntity casheEntity = GetNTCReceiptCashEntityFromCach();
            if (casheEntity != null)
            {
                JsonNTCReceiptHeader header = casheEntity.QuoteHeader;
                if (header == null)
                {
                    result.AddError(@NTCReceiptPayment.msg_QuoteHeader_IsMissing);
                }

                if (header.TotalAmount != totalAmount)
                {
                    result.AddMessage(NTCReceiptPayment.msg_TotalAmount_IsDiff);
                }
                if (casheEntity.QuoteDetails.Sum(s => s.QuDeGrossAmount).Equals(totalAmount) == false)
                {
                    result.AddMessage(NTCReceiptPayment.msg_TotalAmount_IsDiff);
                }

                if (result.HasError == false)
                {
                    JsonConfirmPaymentInfo pymtInfo = new JsonConfirmPaymentInfo();
                    pymtInfo.ReferenceNumber = header.QuHeReferenceNumber;
                    pymtInfo.IDNumber = header.QuHeIDNumber;
                    pymtInfo.ReceivedFrom = header.QuHeSurName;
                    pymtInfo.ReceivedDate = header.QuHeDate;
                    pymtInfo.Address = string.Format("{0} {1} {2} {3}", header.QuHeAddress1, header.QuHeAddress2, header.QuHeAddress3, header.QuHeAddress4);
                    pymtInfo.ReceivedAmount = (float)casheEntity.PaymentDetails.Sum(c => c.Amount);

                    foreach (JsonNTCReceiptPaymentDetail d in casheEntity.PaymentDetails)
                    {
                        var descr = "";
                        switch (d.ReceiptType)
                        {
                            case "Cash":
                                descr = "";
                                break;
                            case "Cheque":
                                descr = "Cheque Number:" + d.ChequeNo + "<br />Cheque Drawer:" + d.ChequeDrawer + "<br />Cheque Bank:" + d.ChequeBank + "<br />Cheque Date:" + d.ChequeDate.ToString("yyyy-MM-dd");
                                break;
                            case "CreditCard":
                            case "DebitCard":
                                descr = "Card Type:" + d.CardIssuer + "<br />Name on Card:" + d.NameOnCard + "<br />Card Expiry:" + d.ExpiryDate.ToString("yyyy-MM");
                                break;
                            case "BankPayment":
                                descr = "Description:" + d.Reference;
                                break;
                            case "PostalOrder":
                                descr = "Postal Order Number:" + d.PostalOrderNumber;
                                break;
                            case "Other":
                                descr = "Other";
                                break;
                        }

                        pymtInfo.PaymentDetails.Add(new JsonPaymentDetail()
                        {
                            Amount = (float)d.Amount,
                            PaymentType = d.ReceiptType,
                            Descr = descr
                        });
                    }
                    result.Body = pymtInfo;
                    result.IsSuccessful = true;
                }
            }

            return Json(result);
        }

        [HttpPost]
        public ActionResult CancelPayment()
        {
            var result = new MessageResult() { IsSuccessful = false };
            ClearReceiptFromCash();
            result.IsSuccessful = true;
            return Json(result);
        }

        [HttpPost]
        public ActionResult ConfirmPayment()
        {
            var result = new MessageResult() { IsSuccessful = false };
            NTCReceiptCasheEntity casheEntity = GetNTCReceiptCashEntityFromCach();
            if (casheEntity != null)
            {
                UserLoginInfo userInfo = Session["UserLoginInfo"] as UserLoginInfo;
                if (userInfo == null)
                {
                    result.AddError(NTCReceiptPayment.msg_SessionTimeOut);
                    return Json(result);
                }
                if (_cashier == null)
                {
                    BankAccountDB bankAccount = new BankAccountDB(this.connStr);
                    _cashier = bankAccount.GetUserBankAccount(userInfo.UserIntNo);
                    if (_cashier == null)
                    {
                        result.AddError(NTCReceiptPayment.msg_NoCashierBound);
                        return Json(result);
                    }
                }
                List<NTC.ReceiptTransaction> tranList = new List<NTC.ReceiptTransaction>();

                foreach (JsonNTCReceiptPaymentDetail tran in casheEntity.PaymentDetails)
                {
                    NTC.ReceiptTransaction receiptTran = null;
                    if (tran != null)
                    {
                        switch (tran.ReceiptType)
                        {
                            case "Cash":
                                receiptTran = NTCReceiptManager.Mapping<NTC.ReceiptTransaction>(tran);
                                receiptTran.CashType = CashType.NTC_Cash;
                                break;
                            case "Cheque":
                                receiptTran = NTCReceiptManager.Mapping<NTC.ChequeTransaction>(tran);
                                receiptTran.CashType = CashType.NTC_Cheque;
                                break;
                            case "CreditCard":
                                receiptTran = NTCReceiptManager.Mapping<NTC.CardTransaction>(tran);
                                receiptTran.CashType = CashType.NTC_CreditCard;
                                break;
                            case "DebitCard":
                                receiptTran = NTCReceiptManager.Mapping<NTC.CardTransaction>(tran);
                                receiptTran.CashType = CashType.NTC_DebitCard;
                                break;
                            case "BankPayment":
                                receiptTran = NTCReceiptManager.Mapping<NTC.BankPaymentTransaction>(tran);
                                receiptTran.CashType = CashType.NTC_BankAccount;
                                break;
                            case "PostalOrder":
                                receiptTran = NTCReceiptManager.Mapping<NTC.PostalOrderTransaction>(tran);
                                receiptTran.CashType = CashType.NTC_PostalOrder;
                                break;
                            case "Other":
                                receiptTran = NTCReceiptManager.Mapping<NTC.ReceiptTransaction>(tran);
                                receiptTran.CashType = CashType.NTC_Other;
                                break;
                        }
                    }
                    receiptTran.UserIntNo = userInfo.UserIntNo;
                    receiptTran.Cashier = _cashier;
                    receiptTran.ReceivedDate = string.IsNullOrEmpty(casheEntity.QuoteHeader.QuHeDate) ? Convert.ToDateTime(casheEntity.QuoteHeader.QuHeDate) : DateTime.Now;
                    tranList.Add(receiptTran);
                }

                NTCReceiptManager ntcReceiptMgmt = new NTCReceiptManager(this.connStr);

                string errMsg = string.Empty;

                int rctIntNo = ntcReceiptMgmt.ProcessPayment(casheEntity.QuoteHeader, casheEntity.QuoteDetails, tranList, out errMsg);

                result.IsSuccessful = rctIntNo > 0;
                if (result.IsSuccessful == false)
                {
                    result.AddError(string.IsNullOrEmpty(errMsg) ? NTCReceiptPayment.msg_Payment_IsFailed : errMsg);
                }
                else
                {
                    result.AddMessage(NTCReceiptPayment.msg_Payment_IsSuccess);
                    ClearReceiptFromCash();
                    //int autIntNo = Session["autIntNo"] == null ? 0 : Convert.ToInt32(Session["autIntNo"]);
                    //bool usePostalReceipt = CheckUsePostalReceipt(autIntNo, userInfo.UserName);

                    //string tmsDomain = WebUtility.GetTMSDomainName();
                    //StringBuilder rctSb = new StringBuilder();

                    //if (usePostalReceipt)
                    //{

                    //    result.Body = string.Format(tmsDomain + "/CashReceiptTraffic_PostalReceiptViewer.aspx?Date={0}%CBIntNo=0%RCtIntNo={1}", DateTime.Now.ToString("yyyy-MM-dd"), rctIntNo);
                    //}
                    //else
                    //{
                    //result.Body = string.Format(tmsDomain + "/ReceiptNoteViewer.aspx?receipt={0}", rctSb.ToString());
                    result.Body = string.Format(Url.Action("QuoteReceiptViewer", "NTCReceiptReport") + "?receipts={0}", rctIntNo);
                    //}
                }
            }

            return Json(result);
        }

        [HttpPost]
        public ActionResult ReverseNTC(string refNumber, int rctIntNo, int cbIntNo, string reason)
        {
            var result = new MessageResult() { IsSuccessful = false };

            UserLoginInfo userInfo = Session["UserLoginInfo"] as UserLoginInfo;
            if (userInfo == null)
            {
                result.AddError(NTCReceiptPayment.msg_SessionTimeOut);
                return Json(result);
            }

            NTCReceiptManager ntcReceiptMgmt = new NTCReceiptManager(this.connStr);

            string errMsg = string.Empty;

            int status = ntcReceiptMgmt.ProcessReverseQuotePayment(refNumber, rctIntNo, cbIntNo, reason, userInfo.UserName, out errMsg);
            if (status <= 0 && !string.IsNullOrEmpty(errMsg))
            {
                result.AddError(errMsg);
                return Json(result);
            }
            else
            {
                result.IsSuccessful = true;
            }

            return Json(result);
        }

        [HttpPost]
        public ActionResult AdminReverseNTC(string refNumber, int rctIntNo, string reason)
        {
            var result = new MessageResult() { IsSuccessful = false };

            UserLoginInfo userInfo = Session["UserLoginInfo"] as UserLoginInfo;
            if (userInfo == null)
            {
                result.AddError(NTCReceiptPayment.msg_SessionTimeOut);
                return Json(result);
            }

            NTCReceiptManager ntcReceiptMgmt = new NTCReceiptManager(this.connStr);

            string errMsg = string.Empty;

            int status = ntcReceiptMgmt.AdminProcessReverseQuotePayment(refNumber, rctIntNo, reason, userInfo.UserName, out errMsg);
            if (status <= 0 && !string.IsNullOrEmpty(errMsg))
            {
                result.AddError(errMsg);
                return Json(result);
            }
            else
            {
                result.IsSuccessful = true;
            }

            return Json(result);
        }

        [HttpPost]
        public ActionResult ValidateCashBox(int rctIntNo, int cbIntNo)
        {
            var result = new MessageResult() { IsSuccessful = false };

            NTCReceiptManager ntcReceiptMgmt = new NTCReceiptManager(this.connStr);

            string errMsg = string.Empty;

            int status = ntcReceiptMgmt.ValidateCashBoxForPaymentReversal(rctIntNo, cbIntNo, out errMsg);
            if (status <= 0 && !string.IsNullOrEmpty(errMsg))
            {
                result.AddError(errMsg);
                return Json(result);
            }
            else
            {
                result.IsSuccessful = true;
            }

            return Json(result);
        }

        [HttpPost]
        public ActionResult GetPaymentType()
        {

            int autIntNo = Session["autIntNo"] == null ? 0 : Convert.ToInt32(Session["autIntNo"]);
            bool needOtherPymtType = false;
            AuthorityRulesDB rulesDB = new AuthorityRulesDB(this.connStr);
            AuthorityRulesDetails rulesDetail = rulesDB.GetAuthorityRule(autIntNo, "9520");
            if (rulesDetail != null)
            {
                if (rulesDetail.ARString.ToUpper() == "Y")
                    needOtherPymtType = true;
            }

            List<SelectListItem> items = new List<SelectListItem>();
            var query = from d in NTCReceiptManager.GetPaymentTypes(needOtherPymtType)
                        select new { d.PtCode, d.PtDescription };

            return Json(query.ToList());
        }

        private bool CheckUsePostalReceipt(int autIntNo, string lastUser)
        {
            //20090113 SD	
            //AutIntNo, ARCode and LastUser need to be set from here
            AuthorityRulesDetails ard = new AuthorityRulesDetails();
            ard.AutIntNo = autIntNo;
            ard.ARCode = "4510";
            ard.LastUser = lastUser;

            DefaultAuthRules authRule = new DefaultAuthRules(ard, connStr);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            return ard.ARString.Equals("Y", StringComparison.InvariantCultureIgnoreCase) ? true : false;

        }

        void InitialReptModel(NTCReceiptModel model)
        {
            int autIntNo = Session["autIntNo"] == null ? 0 : Convert.ToInt32(Session["autIntNo"]);
            bool needOtherPymtType = false;
            model.NotTrafficCharges = NTCReceiptManager.GetChargesByAutIntNoForDDL(autIntNo);
            model.UnitOfMeasures = NTCReceiptManager.GetAllUnitOfMeasure();

            UserLoginInfo userInfo = Session["UserLoginInfo"] as UserLoginInfo;

            BankAccountDB bankAccount = new BankAccountDB(this.connStr);
            _cashier = bankAccount.GetUserBankAccount(userInfo.UserIntNo);

            AuthorityRulesDB rulesDB = new AuthorityRulesDB(this.connStr);
            AuthorityRulesDetails rulesDetail = rulesDB.GetAuthorityRule(autIntNo, "9520");
            if (rulesDetail != null)
            {
                if (rulesDetail.ARString.ToUpper() == "Y")
                    needOtherPymtType = true;
            }

            model.PymtTypes = NTCReceiptManager.GetPaymentTypes(needOtherPymtType);
            NTCReceiptCasheEntity casheEntity = GetNTCReceiptCashEntityFromCach();
            if (casheEntity != null)
            {
                model.NTCReceiptCashEntity = casheEntity;
                if (model.NTCReceiptCashEntity.QuoteDetails.Count > 0)
                    model.ShowQuoteDetailPnl = true;
                //if (model.NTCReceiptCashEntity.QuoteHeader != null
                //    && !string.IsNullOrEmpty(model.NTCReceiptCashEntity.QuoteHeader.QuHeReferenceNumber))
                //    model.ShowQuoteHeaderAddButton = false;
            }

        }

        NTCReceiptCasheEntity GetNTCReceiptCashEntityFromCach()
        {
            if (Session["NTCReceiptCasheEntity"] == null) return null;

            return Session["NTCReceiptCasheEntity"] as NTCReceiptCasheEntity;
        }

        void ClearReceiptFromCash()
        {
            NTCReceiptCasheEntity casheEntity = Session["NTCReceiptCasheEntity"] as NTCReceiptCasheEntity;
            if (casheEntity != null)
            {
                casheEntity.QuoteHeader = null;
                casheEntity.QuoteDetails.Clear();
                casheEntity.PaymentDetails.Clear();
            }
            Session.Remove("NTCReceiptCasheEntity");
        }

    }
}
