﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Stalberg.TMS;
using System.IO;
using System.Configuration;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using System.Data.SqlClient;
using System.Data;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.Merger;
using SIL.AARTO.BLL.BarCode;
using System.Globalization;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.Imaging;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.Web.Controllers.NTCReceipt
{
    [AARTOErrorLog, AARTOAuthorize]
    public class NTCReceiptReportController : Controller
    {
        //
        // GET: /NTCReceiptReport/
        string connStr = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

        System.Drawing.Image barCodeImage = null;
        System.Drawing.Image logoImage = null;
        private PlaceHolder _phBarCode;
        private PlaceHolder _logo;

        byte[] mainImage;

        [HttpGet]
        public ActionResult QuoteReceiptViewer(string receipts, string date)
        {

            int autIntNo = Convert.ToInt32(Session["autIntNo"]);

            AuthReportNameDB arn = new AuthReportNameDB(connStr);

            string reportPage = arn.GetAuthReportName(autIntNo, "QuoteReceipt");
            string sTemplate = arn.GetAuthReportNameTemplate(autIntNo, "QuoteReceipt");
            if (reportPage.Equals(""))
            {
                reportPage = "QuoteReceiptNote_EM.dplx";
                int arnIntNo = arn.AddAuthReportName(autIntNo, reportPage, "QuoteReceipt", "system", string.Empty);
            }

           // string path = Server.MapPath("/reports/" + reportPage);
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Reports\" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;

            if (!System.IO.File.Exists(path))
            {
                Response.Write("Report template doen't exists, Path: " + path);
                Response.End();
            }

            if (!sTemplate.Equals(""))
            {
                //templatePath = Server.MapPath("/Templates/" + sTemplate);
                templatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Templates\" + sTemplate);

                if (!System.IO.File.Exists(templatePath))
                {
                    Response.Write("Report template doen't exists, Path: " + templatePath);
                    Response.End();
                }
            }

            //****************************************************
            byte[] buffer = null;
            string sPaymentMethod = string.Empty;

            DocumentLayout doc = new DocumentLayout(path);

            StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
            StoredProcedureQuery query1 = (StoredProcedureQuery)doc.GetQueryById("Query1");
            query.ConnectionString = connStr;
            query1.ConnectionString = connStr;

            ParameterDictionary parameters = new ParameterDictionary();
            parameters.Add("Receipts", receipts);
            parameters.Add("RctIntNo", receipts);
            parameters.Add("Date", date);
            parameters.Add("Authority", autIntNo);

            //Image logo = (Image)doc.GetElementById("logo");

            Label lblAuthNameAndAddr = (Label)doc.GetElementById("lblAuthNameAndAddr");
            Label lblAuthTelNo = (Label)doc.GetElementById("lblAuthTelNo");
            Label lblReceiptNo = (Label)doc.GetElementById("lblReceiptNo");

            Label lblReceiptDate = (Label)doc.GetElementById("lblReceiptDate");

            Label lblReferenceNumber = (Label)doc.GetElementById("lblReferenceNumber");
            Label lblSurname = (Label)doc.GetElementById("lblSurname");
            Label lblInitials = (Label)doc.GetElementById("lblInitials");

            Label lblReceiptAddress = (Label)doc.GetElementById("lblReceiptAddress");
            Label lblHomeNo = (Label)doc.GetElementById("lblHomeNo");
            Label lblWorkNo = (Label)doc.GetElementById("lblWorkNo");
            Label lblCellNo = (Label)doc.GetElementById("lblCellNo");

            Label lblPaymentAmount = (Label)doc.GetElementById("lblPaymentAmount");
            Label lblPaymentMethod = (Label)doc.GetElementById("lblPaymentMethod");


            Label lblTotalAmount = (Label)doc.GetElementById("lblTotalAmount");

            Label lblCashier = (Label)doc.GetElementById("lblCashier");


            PlaceHolder phImage = null;

            SqlConnection con = new SqlConnection(connStr);
            SqlDataReader result = null;
            SqlCommand com = null;

            if (!String.IsNullOrEmpty(receipts))
            {
                con = new SqlConnection(connStr);
                //com = new SqlCommand("ReceiptNotePrintForNonTrafficCharge", con);
                com = new SqlCommand("PostalQuoteReceipts_EM", con);
                com.CommandType = CommandType.StoredProcedure;
                com.CommandTimeout = 0;

                com.Parameters.Add("@Receipts", SqlDbType.VarChar, 100).Value = receipts;
                com.Parameters.Add("@Date", SqlDbType.SmallDateTime).Value = date;
                com.Parameters.Add("@Authority", SqlDbType.Int, 4).Value = autIntNo;


                Document report = null;
                //get data and populate result set
                try
                {
                    con.Open();
                    result = com.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (Exception ee)
                {
                    string sError = ee.Message;
                }
                try
                {
                    MergeDocument merge = new MergeDocument();
                    while (result.Read())
                    {
                        try
                        {

                            //if (reportPage.IndexOf("_EM") > -1)
                            //{
                                lblAuthNameAndAddr.Text = result["AutPostAddress"].ToString();

                                lblAuthTelNo.Text = result["AutTel"].ToString();

                                lblReceiptNo.Text = result["RctNumber"].ToString();
                                lblReceiptDate.Text = result["RctDate"].ToString();
                                lblReferenceNumber.Text = result["QuHeReferenceNumber"].ToString();

                                lblSurname.Text = result["Surname"].ToString();
                                lblInitials.Text = result["Name"].ToString();
                                lblReceiptAddress.Text = result["AddressFull"].ToString();

                                lblHomeNo.Text = result["HomeNo"].ToString();
                                lblWorkNo.Text = result["WorkNo"].ToString();
                                lblCellNo.Text = result["CellNo"].ToString();
                                lblPaymentAmount.Text = result["PaymentAmounts"].ToString();
                                lblPaymentMethod.Text = result["PaymentMethod"].ToString();
                                lblTotalAmount.Text = result["Total"].ToString();
                                lblCashier.Text = result["RctUser"].ToString();

                                phImage = (PlaceHolder)doc.GetElementById("phLogo");
                                phImage.LaidOut += new PlaceHolderLaidOutEventHandler(ph_Image);

                                _phBarCode = (PlaceHolder)doc.GetElementById("phBarCode");
                                _phBarCode.LaidOut += new PlaceHolderLaidOutEventHandler(ph_BarCode);
                                barCodeImage = Code128Rendering.MakeBarcodeImage(result["RctNumber"].ToString(), 1, 30, true);
                                //_logo = (PlaceHolder)doc.GetElementById("logo");
                                //_logo.LaidOut += new PlaceHolderLaidOutEventHandler(logo_image);

                                mainImage = (byte[])result["MtrLogo"];
                            //}

                            report = doc.Run(parameters);
                            ImportedPageArea importedPage;
                            byte[] bufferTemplate;
                            if (!sTemplate.Equals(""))
                            {
                                if (sTemplate.ToLower().IndexOf(".dplx") > 0)
                                {
                                   // DocumentLayout template = new DocumentLayout(Server.MapPath("/Templates/" + sTemplate));
                                    DocumentLayout template = new DocumentLayout(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Templates\" + sTemplate));
                                    StoredProcedureQuery queryTemplate = (StoredProcedureQuery)template.GetQueryById("Query");
                                    queryTemplate.ConnectionString = connStr;
                                    ParameterDictionary parametersTemplate = new ParameterDictionary();
                                    Document reportTemplate = template.Run(parametersTemplate);
                                    bufferTemplate = reportTemplate.Draw();
                                    PdfDocument pdf = new PdfDocument(bufferTemplate);
                                    PdfPage page = pdf.Pages[0];
                                    importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
                                }
                                else
                                {
                                    //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
                                    //importedPage = new ImportedPageArea(Server.MapPath("/Templates/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
                                    importedPage = new ImportedPageArea(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Templates\" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
                                }
                                ceTe.DynamicPDF.Page rptPage = report.Pages[0];
                                rptPage.Elements.Insert(0, importedPage);
                            }
                            buffer = report.Draw();
                            merge.Append(new PdfDocument(buffer));

                            buffer = null;
                            bufferTemplate = null;
                        }

                        catch (Exception ex)
                        {
                            String sError = ex.Message;
                            EntLibLogger.WriteErrorLog(ex, LogCategory.Error, AartoProjectList.AARTOWebApplication.ToString());
                        }

                    }
                    result.Close();
                    con.Dispose();
                    if (merge.Pages.Count <= 0)
                        Response.End();

                    //byte[] buf = merge.Draw();

                    //_phBarCode.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_BarCode);
                    //phImage.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_Image);

                    //Response.ClearContent();
                    //Response.ClearHeaders();
                    //Response.ContentType = "application/pdf";
                    //Response.BinaryWrite(buf);
                    //Response.End();
                    //buf = null;

                    return File(merge.Draw(), "application/pdf");
                }
                catch (Exception ex)
                {
                    //System.Diagnostics.Debug.WriteLine(string.Format((string)GetLocalResourceObject("strDebugWriteMsg"), ex.ToString()));
                    EntLibLogger.WriteErrorLog(ex, LogCategory.Error, AartoProjectList.AARTOWebApplication.ToString());
                }
                finally
                {
                    GC.Collect();
                }
            }

            return View();
        }

        void ph_Image(object sender, PlaceHolderLaidOutEventArgs e)
        {
            if (mainImage != null)
            {
                ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(mainImage), 0, 0);
                img.Height = 75.0F;            //12
                img.Width = 70.0F;             //15
                e.ContentArea.Add(img);
            }
        }

        //Jake 2011-02-25 Removed BarcodeNETImage
        public void ph_BarCode(object sender, PlaceHolderLaidOutEventArgs e)
        {
            //ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(_barCode.GetBarcodeBitmap(FILE_FORMAT.JPG)), 0, 0);
            //img.Height = 30.0F;
            //img.Width = 150.0F;
            //e.ContentArea.Add(img);

            if (barCodeImage != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    barCodeImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(ms.GetBuffer()), 0, 0);
                    img.Height = 30.0F;
                    img.Width = 150.0F;
                    e.ContentArea.Add(img);
                }

                int generation = System.GC.GetGeneration(barCodeImage);
                barCodeImage = null;
                System.GC.Collect(generation);
            }
        }
        public void logo_image(object sender, PlaceHolderLaidOutEventArgs e)
        {
            if (logoImage != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    logoImage.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(ms.GetBuffer()), 0, 0);
                    img.Height = 74.0F;
                    img.Width = 71.0F;
                    e.ContentArea.Add(img);
                }

                int generation = System.GC.GetGeneration(logoImage);
                logoImage = null;
                System.GC.Collect(generation);
            }
        }

    }
}
