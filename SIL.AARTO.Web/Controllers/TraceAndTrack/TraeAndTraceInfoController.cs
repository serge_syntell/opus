﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.TraceAndTrack;
using SIL.AARTO.BLL.TraceAndTrack.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Services;
using System.Collections;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.Web.Helpers.Paginator;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.Resource.TraceAndTrack;
using System.Configuration;
using SIL.AARTO.BLL.Utility.Printing;


namespace SIL.AARTO.Web.Controllers.TraceAndTrack
{
    public partial class TraceAndTrackController : Controller
    {
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
        //
        // GET: /TraeAndTraceInfo

        public ActionResult TraceAndTrackInfo(string SearchRegistrationNumber)
        {
            TraceAndTrackInfoManager manager = new TraceAndTrackInfoManager();
            TraceAndTrackInfoModel traceModel = new TraceAndTrackInfoModel();
            traceModel.SearchRegistrationNumber = SearchRegistrationNumber;
            GetTraceAndTrack(traceModel, manager,"search");
            return View(traceModel);
        }

        [HttpPost]
        public ActionResult TraceAndTrackInfo(TraceAndTrackInfoModel traceModel,FormCollection form)
        {
            //set the new login user
            //set user info
            UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
            if(userLofinInfo!=null)
            traceModel.LastUser = userLofinInfo.UserName;
            //create dropdown list
            TraceAndTrackInfoManager manager = new TraceAndTrackInfoManager();
            string regNo = traceModel.RegistrationNumber;
            string clickName = form["btnSaveSt"];
            if (!string.IsNullOrEmpty(regNo) && clickName=="Save")
            manager.InvokeSaveAndUpdateNameAndAddress(traceModel, userLofinInfo.UserName);
            //show the add info
            traceModel.SearchRegistrationNumber = traceModel.RegistrationNumber;
            GetTraceAndTrack(traceModel,manager,"edit");
            //Linda 2012-9-12 add for list report19
            //2013-11-7 Heidi changed for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());
            punchStatistics.PunchStatisticsTransactionAdd(Convert.ToInt32(Session["autIntNo"].ToString()), userLofinInfo.UserName, PunchStatisticsTranTypeList.TracingRegistrationNumber,PunchAction.Add);   
                
            return View(traceModel);
        }
        public void GetTraceAndTrack(TraceAndTrackInfoModel traceModel, TraceAndTrackInfoManager manager,string model)
        {
            //set user info
            UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
            if (userLofinInfo != null)
            traceModel.LastUser = userLofinInfo.UserName;
            //page
            int totalCount = 0,pageIndex = 0;
            ViewData["pageSize"] = traceModel.PageSize;
            if (!string.IsNullOrEmpty(QueryString.GetString("Page")))
            {
                pageIndex = Convert.ToInt32(QueryString.GetString("Page"));
            }
            if (!string.IsNullOrEmpty(QueryString.GetString("regNo")))
            {
                traceModel.SearchRegistrationNumber = QueryString.GetString("regNo");
            }
            if (model == "edit")
                pageIndex = 0;
            //dropdown list
            ViewData["TypeOfOwnerList"] = manager.GetOwnerTypeList();
            //search
            Hashtable ht=new Hashtable();
            ht.Add("RegistrationNumber",traceModel.SearchRegistrationNumber);
            List<TtAlternateNameAndAddress> list = new List<TtAlternateNameAndAddress>();
            manager.GetNameAndAddressList(ref list,ht, pageIndex, traceModel.PageSize, ref totalCount);
            traceModel.TotalCount = totalCount;
            traceModel.TraceAndTrackInfos = list;
        }
        //check has the data
        [HttpPost]
        public ActionResult IsExitsTheData(string regNo)
        {
            TraceAndTrackInfoManager manager = new TraceAndTrackInfoManager();
            Message message = new Message();
            message.Status = manager.IsExitsTheData(regNo);
            message.Text ="The "+ regNo+" "+TraceAndTrackInfo_cshtml.iblRegNoExits_Text;
            return Json(message);
        }
        //get a edit data
        [HttpPost]
        public ActionResult GetTheTraceEdit(string regNo)
        {
            TraceAndTrackInfoManager manager = new TraceAndTrackInfoManager();
            string traceModel = manager.GetOneEditModel(regNo);
            return Json(traceModel);
        }
        //delete the data
        [HttpPost]
        public ActionResult DeleInfo(string regNo)
        {
            TraceAndTrackInfoManager manager = new TraceAndTrackInfoManager();
            Message message = new Message();
            try
            {
                manager.DeleInfo(regNo);
                message.Status = true;
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.TracingRegistrationNumber, PunchAction.Delete); 
            }
            catch (Exception ex)
            {
                message.Status = false;
            }
            return Json(message);
        }
    }
}
