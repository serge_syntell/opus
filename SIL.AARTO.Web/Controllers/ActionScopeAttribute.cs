﻿using System;
using System.Linq;
using System.Web.Mvc;
using SIL.AARTO.BLL.Representation.Model;
using SIL.ServiceBase;

namespace SIL.AARTO.Web.Controllers
{
    public class ActionScopeAttribute : ActionFilterAttribute
    {
        public Type[] IncludedControllerTypes { get; set; }
        public Type[] ExcludedControllerTypes { get; set; }
        //public string[] AllowedRoles { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var canContinue = true;
            var type = filterContext.Controller.GetType();

            if (!IncludedControllerTypes.IsNullOrEmpty())
                canContinue = IncludedControllerTypes.Any(t => type == t || type.IsSubclassOf(t));

            if (canContinue && !ExcludedControllerTypes.IsNullOrEmpty())
                canContinue = !ExcludedControllerTypes.Any(t => type == t || type.IsSubclassOf(t));

            if (!canContinue)
            {
                filterContext.Result = GetInvalidJsonResult();
                return;
            }

            //if (!AllowedRoles.IsNullOrEmpty())
            //{
            //    var roles = type.GetProperty("UserRoles");
            //    if (roles != null)
            //    {
            //        var roleValues = roles.GetValue(filterContext.Controller, null) as List<string>;
            //        if (!roleValues.IsNullOrEmpty())
            //        {

            //        }
            //    }
            //}
        }

        public static JsonResult GetInvalidJsonResult(bool isRoleFailed = false, JsonRequestBehavior behavior = JsonRequestBehavior.DenyGet)
        {
            const string actionFailed = "Access denied - It is not allowed to perform this action.";
            const string roleFailed = "Access denied - You are not allowed to perform this action due to your granted permission.";
            var message = new MessageResult();
            message.AddError(isRoleFailed ? roleFailed : actionFailed);
            return new JsonResult
            {
                Data = message,
                JsonRequestBehavior = behavior
            };
        }
    }
}
