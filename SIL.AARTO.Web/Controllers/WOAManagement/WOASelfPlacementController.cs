﻿using System;
using System.Web.Mvc;
using SIL.AARTO.BLL.WOAManagement;
using SIL.AARTO.BLL.WOAManagement.Model;

namespace SIL.AARTO.Web.Controllers.WOAManagement
{
    public partial class WOAManagementController : Controller
    {
        readonly WOASelfPlacementManager woasManager = new WOASelfPlacementManager(connectionString);

        [HttpGet]
        public ActionResult WOASelfPlacement()
        {
            return View();
        }

        [HttpPost]
        public ActionResult WOASelfPlacementSearch(int woaIntNo)
        {
            var model = new WOASelfPlacementModel();
            try
            {
                this.woasManager.WOASelfPlacementSearch(ref model, woaIntNo);
            }
            catch (Exception ex)
            {
                model.Status = false;
                model.AddError(ex.Message);
                ViewBag.Exception = ex;
            }
            return model.Status
                ? (ActionResult)View("_WOASelfPlacementContent", model)
                : Json(model.GetMessageResult());
        }

        [HttpPost]
        public ActionResult WOASelfPlacementUpdate(WOASelfPlacementModel model)
        {
            try
            {
                this.woasManager.WOASelfPlacementUpdate(ref model, LastUser);
            }
            catch (Exception ex)
            {
                model.Status = false;
                model.AddError(ex.Message);
                if (ViewBag != null) ViewBag.Exception = ex;
            }

            return Json(model.GetMessageResult());
        }

        public ActionResult WOASelfPlacementReverse(WOASelfPlacementModel model)
        {
            try
            {
                this.woasManager.WOASelfPlacementReverse(ref model, LastUser);
            }
            catch (Exception ex)
            {
                model.Status = false;
                model.AddError(ex.Message);
                if (ViewBag != null) ViewBag.Exception = ex;
            }

            return Json(model.GetMessageResult());
        }
    }
}
