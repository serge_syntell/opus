﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.BLL.WOAManagement;
using SIL.AARTO.BLL.WOAManagement.Model;
using SIL.AARTO.DAL.Services;
using SIL.ServiceBase;
using Stalberg.TMS;

namespace SIL.AARTO.Web.Controllers.WOAManagement
{
    public partial class WOAManagementController : Controller
    {
        readonly AuthorityService authorityService = new AuthorityService();
        readonly ServiceDB serviceDB = new ServiceDB(connectionString);
        readonly WOAReductionManager woarManager = new WOAReductionManager(connectionString);

        [HttpGet]
        public ActionResult WOAReduction()
        {
            return View();
        }

        [HttpPost]
        public ActionResult WOAReductionSearch(int woaIntNo)
        {
            var model = new WOAReductionModel();
            try
            {
                this.woarManager.WOAReductionSearch(ref model, woaIntNo);
            }
            catch (Exception ex)
            {
                model.Status = false;
                model.AddError(ex.Message);
                ViewBag.Exception = ex;
            }
            return model.Status
                ? (ActionResult)View("_WOAReductionContent", model)
                : Json(model.GetMessageResult());
        }

        [HttpPost]
        public ActionResult WOAReductionUpdate(WOAReductionModel model)
        {
            try
            {
                this.woarManager.WOAReductionUpdate(ref model, LastUser);
            }
            catch (Exception ex)
            {
                model.Status = false;
                model.AddError(ex.Message);
                if (ViewBag != null) ViewBag.Exception = ex;
            }

            return Json(model.GetMessageResult());
        }

        [HttpPost]
        public ActionResult WOAReductionReport()
        {
            var model = new WOAReductionReportModel();
            try
            {
                foreach (var a in this.authorityService.GetAll().OrderBy(a => a.AutName))
                {
                    model.AuthorityList.Add(new SelectListItem
                    {
                        Text = string.Format("{0} ({1})", a.AutName, a.AutCode),
                        Value = a.AutIntNo.ToString(CultureInfo.InvariantCulture)
                    });
                }
                if (Session["autIntNo"] != null)
                    model.AutIntNo = Convert.ToInt32(Session["autIntNo"]);
            }
            catch (Exception ex)
            {
                ViewBag.Exception = ex;
                model.Status = false;
                model.AddError(ex.Message);
            }
            return View("_WOAReductionReport", model);
        }

        [HttpPost]
        public ActionResult GetCourtList(int autIntNo)
        {
            var crtList = new List<SelectListItem>();
            if (autIntNo > 0)
            {
                var paras = new[]
                {
                    new SqlParameter("@AutIntNo", autIntNo)
                };
                try
                {
                    this.serviceDB.ExecuteReader("Auth_CourtListByAuth", paras, dr =>
                    {
                        while (dr.Read())
                        {
                            crtList.Add(new SelectListItem
                            {
                                Text = Helper.GetReaderValue<string>(dr, "CrtDetails"),
                                Value = Helper.GetReaderValue<string>(dr, "CrtIntNo")
                            });
                        }
                    });
                }
                catch (Exception ex)
                {
                    ViewBag.Exception = ex;
                    var model = new MessageResult
                    {
                        Status = false
                    };
                    model.AddError(ex.Message);
                    return Json(model);
                }
            }
            return Json(crtList);
        }

        public ActionResult WOAReductionCreateReport(WOAReductionReportModel model)
        {
            const string sessionKey = "WOAReductionCreateReport_DKey";
            var isPost = Request.HttpMethod.ToUpper() == "POST";
            try
            {
                string dkey;
                if (isPost && string.IsNullOrWhiteSpace(model.DKey)
                    || !isPost && (dkey = Session[sessionKey] != null ? Convert.ToString(Session[sessionKey]) : null) != null && dkey == model.DKey)
                {
                    if(!isPost) Session.Remove(sessionKey);

                    var stream = this.woarManager.CreateReport(ref model);
                    //var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    if (model.Status)
                    {
                        if (stream == null)
                        {
                            if (isPost)
                            {
                                dkey = Guid.NewGuid().ToString("N").Substring(0, 8);
                                Session[sessionKey] = dkey;
                                model.Body = dkey;
                            }
                        }
                        else
                            return new FileStreamResult(stream, "application/vnd.ms-excel")
                            {
                                FileDownloadName = string.Format("WOA Reduction Report {0} {1} ({2} ~ {3}).xls", model.CrtName, DateTime.Now.ToString("yyyy-MM-dd"), model.DateFrom.Value.ToString("yyyy-MM-dd"), model.DateTo.Value.ToString("yyyy-MM-dd"))
                            };
                    }
                }
            }
            catch (Exception ex)
            {
                model.Status = false;
                model.AddError(ex.Message);
                ViewBag.Exception = ex;
            }

            return isPost
                ? (ActionResult)Json(model.GetMessageResult())
                : new ContentResult { Content = model.Error };
        }
    }
}
