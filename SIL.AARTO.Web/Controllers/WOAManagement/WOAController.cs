﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.Helpers.ObjectFormatter;
using SIL.AARTO.Web.ViewModels.WOAManagement;
using SIL.AARTO.BLL.WOAManagement;
using SIL.AARTO.BLL.Admin;
using System.Configuration;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.BLL.Utility;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.BLL.Utility.Cache;
using System.Collections;
using Stalberg.TMS;
using System.Data.SqlClient;
using SIL.AARTO.Web.Helpers.Paginator;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Transactions;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.WOAManagement
{
    // Jake 2013-08-27 modified, added new parameter IdNumber 
    [HandleError, AARTOErrorLog, LanguageFilter,AARTOAuthorize]
    public partial class WOAManagementController : Controller
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
        //
        // GET: /WOA/
        int pageSize = int.Parse(ConfigurationManager.AppSettings["PageSize"]);
        WoaAuthorisationManager woaAuthorisationManager = new WoaAuthorisationManager(connectionString);

        public ActionResult WOABookOut()
        {

            WOABookOutModel model = new WOABookOutModel();
            //model.Authorities = AuthorityManager.GetSelectList();
            if (Session["autIntNo"] != null) model.Authority = Convert.ToInt32(Session["autIntNo"]);
            else if (Session["UserLoginInfo"] != null)
                model.Authority = ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo;

            InitWOaBookOutModel(model);

            return View(model);
        }

        [HttpPost]
        public ActionResult WOABookOut(WOABookOutModel model)
        {
            model.ErrorMsg = string.Empty;

            if (string.IsNullOrEmpty(model.WOANumber) && string.IsNullOrEmpty(model.IdNumber))
            {
                InitWOaBookOutModel(model);
                model.ErrorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookOut.ReturnMsg8;
            }
            else
            {
                // 2014-07-22, Oscar added rowversion.
                List<KeyValue<int, long>> noticeRowVersionList;

                model.WOAList = new WOAManager().GetWoaByWoaNumber(model.Authority, model.IdNumber, model.WOANumber, out noticeRowVersionList);
                model.ShowWoaInfo = model.WOAList.Count() > 0;
                model.HidWoaNumber = model.WOANumber;

                // 2014-07-22, Oscar added rowversion.
                model.RowVersionStrings = Formatter.ObjectToString(noticeRowVersionList, FormatterType.Json);

                InitWOaBookOutModel(model);

                if (model.WOAList.Count() == 0)
                    model.ErrorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookOut.NoDataFound;
            }

            return View(model);
        }

        //Jake 2013-08-28 modified parameter
        [HttpPost]
        //public ActionResult BookOutWOAToOfficer(string woaNumber, int toIntNo)
        public ActionResult BookOutWOAToOfficer(string woaIntNoArr, int toIntNo, string noticeRowVersionStrings)
        {
            Message message = new Message();
            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] != null)
                lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;
            else//Jerry 2014-11-03 check the session
            {
                message.Status = false;
                message.Text = "SessionTimeOut";
                return Json(message);
            }

            // 2014-07-22, Oscar added rowversion.
            var noticeRowVersionList = Formatter.StringToObject<List<KeyValue<int, long>>>(noticeRowVersionStrings, FormatterType.Json);

            if (String.IsNullOrEmpty(woaIntNoArr) || woaIntNoArr.IndexOf(";") <= 0
                || noticeRowVersionList == null || noticeRowVersionList.Count <= 0)
            {
                return Json(message);
            }

            WOAManager wm = new WOAManager();

            List<String> listArr = new List<string>();
            foreach (string k in woaIntNoArr.Split(';'))
            {
                if (!String.IsNullOrEmpty(k)) listArr.Add(k);
            }
            String[] array = listArr.ToArray();

            //int returnValue = wm.BookOutWOAToOfficer(array, toIntNo, lastUser, noticeRowVersionList);
            //Jerry 2014-11-13 add errMsg parameter
            string errMsg = string.Empty;
            int returnValue = wm.BookOutWOAToOfficer(array, toIntNo, lastUser, noticeRowVersionList, ref errMsg);

            message.Status = returnValue > 0;
            switch (returnValue)
            {
                case 1:
                    TrafficOfficer to = wm.GetTrafficOfficer(toIntNo);
                    if (to != null)
                    {
                        message.Text = String.Format(SIL.AARTO.Web.Resource.WOAManagement.WOABookOut.SuccessMessage, to.TosName);
                    }
                    else
                    {
                        message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookOut.SuccessMessage;
                    }

                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.WOABookOutToOfficer, PunchAction.Change);

                    break;
                case -1: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookOut.ReturnMsg1;
                    break;
                case -2: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookOut.ReturnMsg2;
                    break;
                case -3: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookOut.ReturnMsg3;
                    break;
                case -4: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookOut.ReturnMsg4;
                    break;
                case -5: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookOut.ReturnMsg5;
                    break;
                case -6: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookOut.ReturnMsg6;
                    break;
                case -7: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookOut.ReturnMsg7;
                    break;

                // 2014-07-22, Oscar added rowversion.
                case -99:
                    message.Text = Resource.WOAManagement.WOABookOut.RowVersionChanged;
                    break;
                case -100:
                    message.Text = errMsg;
                    break;

                default: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookOut.UnSuccessMessage;
                    break;
            }

            return Json(message);
        }

        [HttpPost]
        public ActionResult GetOfficerByAutIntNo(int autIntNo)
        {
            List<TrafficOfficerListItem> items = new List<TrafficOfficerListItem>();
            SIL.AARTO.DAL.Entities.Authority authority = AuthorityManager.GetAuthorityByAuthIntNo(autIntNo);
            if (authority != null)
            {
                items = TrafficOfficerManager.GetTrafficOfficerListItem(authority.MtrIntNo);
            }
            return Json(items);
        }


        public ActionResult WOABookCapture(string ErrorMsg)
        {
            WOACaptureModel model = new WOACaptureModel();

            SIL.AARTO.DAL.Entities.TList<WoaServedStatus> servedStatusList = new WOAManager().GetWoaServedStatus();

            if (!string.IsNullOrEmpty(ErrorMsg))
            {
                model.ErrorMsg = ErrorMsg;
            }
            InitBookCaptureModel(model);

            return View(model);
        }

        void InitBookCaptureModel(WOACaptureModel model)
        {
            string formatTitle = string.Empty;
            SIL.AARTO.DAL.Entities.TList<WoaServedStatus> servedStatusList = new WOAManager().GetWoaServedStatus();
            foreach (WoaServedStatus wss in servedStatusList)
            {
                if (wss.WssIntNo > 0 && wss.WssIntNo != 6)
                    formatTitle += String.Format("{0}. {1}<br/>", wss.WssIntNo, wss.WssDescr);
            }
            model.ServedStatusTitle = formatTitle;

        }

        [HttpPost]
        public ActionResult CheckWOA(int woaIntNo)
        {
            Message message = new Message();
            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] != null)
                lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;

            Woa woa = new WoaService().GetByWoaIntNo(woaIntNo);

            return Json(message);
        }

        [HttpPost]
        public ActionResult WOABookCapture(WOACaptureModel model)
        {
            //Message message = new Message();
            //2013-12-09 Heidi changed for check user login(5084)

            InitBookCaptureModel(model);

            if (Session["UserLoginInfo"] == null)
            {
                //message.Status = false;
                model.ErrorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.UserNotLogged;
                //return Json(message);
                return View(model);
            }

            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] != null)
                lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;

            string woaNumber = string.Empty;
            if (!String.IsNullOrEmpty(model.WOANumber))
            {
                woaNumber = model.WOANumber.Replace("/", "").Replace("-", "").Trim();
            }

            if (model.ServedStatus == (int)WoaServedStatusList.ExecutedReleasedonSec72
                || model.ServedStatus == (int)WoaServedStatusList.ExecutedTransferred)
            {
                //Jake 2014-08-13 modify CheckCourtDate to validate court date
                // Jake 2014-02-25 comment out for package 5196, we can allow the magistrate select any day after today
                //2014-07-18 Heidi changed for fixing the problem is hint information error(5321).
                string _errormsg = "";
                int flag = woaAuthorisationManager.CheckCourtDate(model.NewCourtDate, model.WOANumber, lastUser, out _errormsg);
                if (flag < 0)
                {
                    if (flag == -1)
                    {
                        model.ErrorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg16;
                    }
                    else if (flag == -2)
                    {
                        model.ErrorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg15;
                    }
                    else if (flag == -3)
                    {
                        //message.Status = flag;
                        model.ErrorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg17;
                    }
                    else if (flag == -4)
                    {
                        model.ErrorMsg = _errormsg;
                    }
                    else if (flag == -5)
                    {
                        model.ErrorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg18;
                    }
                    else if (flag == -6)
                    {
                        model.ErrorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg19;
                    }
                    return View(model);
                }
            }

            IEnumerable<Woa> woas = new WOAManager(connectionString).DeepLoadSearchWoaByWoaNumber(model.WOANumber).Where(w => ((w.WoaChargeStatus == 840 || w.WoaChargeStatus == 843 || w.WoaChargeStatus == 847 || w.WoaChargeStatus == 870) && w.WoaExpireDate == null));
            if (woas != null)
            {
                model.WOAResult = new List<WOAReverseResultEntity>();

                foreach (Woa w in woas)
                {
                    model.WOAResult.Add(new WOAReverseResultEntity()
                    {
                        NotTicketNo = w.SumIntNoSource == null ? "" : w.SumIntNoSource.SumNoticeNo,
                        WoaIntNo = w.WoaIntNo,
                        WoaNumber = w.WoaNumber,
                        SummonsNo = w.SumIntNoSource == null ? "" : w.SumIntNoSource.SummonsNo,
                        AccFullName = w.SumIntNoSource == null ? "" : (w.SumIntNoSource.AccusedCollection.Count() == 0 ? "" : w.SumIntNoSource.AccusedCollection[0].AccFullName)
                    });
                }
            }


            return View(model);

            //Jake 2014-03-05 comment out ---START
            //Woa woa = new WoaService().GetByWoaNoEnquiry(woaNumber).Where(w => ((w.WoaChargeStatus == 840 || w.WoaChargeStatus == 843 || w.WoaChargeStatus == 847 || w.WoaChargeStatus == 870) && w.WoaExpireDate == null)).FirstOrDefault();
            //if (woa == null)
            //{
            //    message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg1;
            //}
            //else
            //{
            //    string errorMsg = string.Empty;

            //    if ((model.ServedStatus.Value == (int)WoaServedStatusList.ExecutedReleasedonSec72 || model.ServedStatus.Value == (int)WoaServedStatusList.ExecutedTransferred || model.ServedStatus.Value == (int)WoaServedStatusList.NotExecutedUntraceable) && woa.WoaType == "D")
            //    {
            //        message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ErrorMsg2;
            //    }
            //    else
            //    {
            //        Summons summons = new SummonsService().GetBySumIntNo(woa.SumIntNo);
            //        NoticeSummons ns = new NoticeSummonsService().GetBySumIntNo(summons.SumIntNo).FirstOrDefault();
            //        Notice notice = new NoticeService().GetByNotIntNo(ns.NotIntNo);
            //        if (notice.NoticeStatus == 955 && model.ServedStatus.Value != (int)WoaServedStatusList.ExecutedPaid)
            //        {
            //            message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg10;
            //        }
            //        else
            //        {
            //            if (model.ServedStatus.Value == (int)WoaServedStatusList.ExecutedReleasedonSec72 || model.ServedStatus.Value == (int)WoaServedStatusList.ExecutedTransferred)
            //            {
            //                message.Status = true;
            //            }
            //            else
            //            {
            //                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
            //                //Jake 2013-11-20 add snapshot here 
            //                SIL.AARTO.BLL.WOAExecutionSnapshot.WoaExecutionSnapshotDB wesDB = new BLL.WOAExecutionSnapshot.WoaExecutionSnapshotDB(connectionString);
            //                if (wesDB.CheckSnapshotIsCreated(woa.WoaIntNo) == false)
            //                {
            //                    bool flag = wesDB.SetSnapshot(woa.WoaIntNo, false, lastUser);
            //                    if (flag == false)
            //                    {
            //                        errorMsg = this.ResourceValue("WOABookCapture", "ReturnMsg12");
            //                        message.Text = errorMsg;
            //                        return Json(message);
            //                    }
            //                }
            //                else
            //                {
            //                    wesDB.UnLockSnapshot(woa.WoaIntNo, lastUser);
            //                }
            //                int returnValue = new WOAManager(connectionString).WoaBookCapture(woaNumber, model.ServedStatus.Value, null, null, lastUser, summons.CrtIntNo, null, null, null, out errorMsg);
            //                switch (returnValue)
            //                {
            //                    case 1: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.SuccessMessage;
            //                        message.Status = true;
            //                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            //                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            //                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.WOABookCapture, PunchAction.Change);
            //                        break;
            //                    case -1: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.UnSuccessMessage;
            //                        break;
            //                    case -2: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg1;
            //                        break;
            //                    case -3: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg3;
            //                        break;
            //                    case -4: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg4;
            //                        break;
            //                    case -5: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg5;
            //                        break;
            //                    case -6: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg6;
            //                        break;
            //                    case -7: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg7;
            //                        break;
            //                    case -8: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg8;
            //                        break;
            //                    case -9: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg9;
            //                        break;
            //                    case -10: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg10;
            //                        break;
            //                    case -12: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg13;
            //                        break;
            //                    default: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.UnSuccessMessage;
            //                        break;
            //                }
            //                message.Text = errorMsg;

            //            }
            //        }
            //    }
            //}

            //return Json(message);
            //----END
        }

        public ActionResult SubmitWOABookCapture(int WoaIntNo, string NewCourtDate, int ServedStatus)
        {
            Message message = new Message();

            if (Session["UserLoginInfo"] == null)
            {
                //message.Status = false;
                message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.UserNotLogged;
                //return Json(message);
                return Json(message);
            }

            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] != null)
                lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;

            Woa woa = new WoaService().GetByWoaIntNo(WoaIntNo);
            //.Where(w => ((w.WoaChargeStatus == 840 || w.WoaChargeStatus == 843 || w.WoaChargeStatus == 847 || w.WoaChargeStatus == 870) && w.WoaExpireDate == null)).FirstOrDefault();
            if (new int[] { 840, 843, 847, 870 }.Contains(woa.WoaChargeStatus ?? 0) == false || woa.WoaExpireDate != null)
            {
                message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg1;
            }
            else
            {
                string errorMsg = string.Empty;

                if ((ServedStatus == (int)WoaServedStatusList.ExecutedReleasedonSec72 || ServedStatus == (int)WoaServedStatusList.ExecutedTransferred || ServedStatus == (int)WoaServedStatusList.NotExecutedUntraceable) && woa.WoaType == "D")
                {
                    message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ErrorMsg2;
                }
                else
                {
                    Summons summons = new SummonsService().GetBySumIntNo(woa.SumIntNo);
                    NoticeSummons ns = new NoticeSummonsService().GetBySumIntNo(summons.SumIntNo).FirstOrDefault();
                    Notice notice = new NoticeService().GetByNotIntNo(ns.NotIntNo);
                    if (notice.NoticeStatus == 955 && ServedStatus != (int)WoaServedStatusList.ExecutedPaid)
                    {
                        message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg10;
                    }
                    else
                    {
                        if (ServedStatus == (int)WoaServedStatusList.ExecutedReleasedonSec72 || ServedStatus == (int)WoaServedStatusList.ExecutedTransferred)
                        {
                            // Jake 2014-02-25 comment out for package 5196, we can allow the magistrate select any day after today
                            //2014-07-18 Heidi changed for fixing the problem is hint information error(5321).
                            string _errormsg = "";
                            int flag = woaAuthorisationManager.CheckCourtDate(NewCourtDate, woa.WoaNumber, lastUser, out _errormsg);
                            if (flag < 0)
                            {
                                if (flag == -1)
                                {
                                    message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg16;
                                }
                                else if (flag == -2)
                                {
                                    message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg15;
                                }
                                else if (flag == -3)
                                {
                                    //message.Status = flag;
                                    message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg17;
                                }
                                else if (flag == -4)
                                {
                                    message.Text = _errormsg;
                                }
                                else if (flag == -5)
                                {
                                    message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg18;
                                }
                                else if (flag == -6)
                                {
                                    message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg19;
                                }
                                message.Status = false;
                            }
                            else
                            {
                                message.Status = true;
                            }
                        }
                        else
                        {
                            //Jake 2014-10-09 added transaction to protect snapshot before WOA execution
                            using (TransactionScope scope = new TransactionScope())
                            {
                                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
                                //Jake 2013-11-20 add snapshot here 
                                SIL.AARTO.BLL.WOAExecutionSnapshot.WoaExecutionSnapshotDB wesDB = new BLL.WOAExecutionSnapshot.WoaExecutionSnapshotDB(connectionString);
                                if (wesDB.CheckSnapshotIsCreated(woa.WoaIntNo) == false)
                                {
                                    bool flag = wesDB.SetSnapshot(woa.WoaIntNo, false, lastUser);
                                    if (flag == false)
                                    {
                                        errorMsg = this.ResourceValue("WOABookCapture", "ReturnMsg12");
                                        message.Text = errorMsg;
                                        return Json(message);
                                    }
                                }
                                else
                                {
                                    wesDB.UnLockSnapshot(woa.WoaIntNo, lastUser);
                                }
                                int returnValue = new WOAManager(connectionString).WoaBookCapture(WoaIntNo, ServedStatus, null, null, lastUser, summons.CrtIntNo, null, null, null, out errorMsg);
                                switch (returnValue)
                                {
                                    case 1: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.SuccessMessage;
                                        message.Status = true;
                                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.WOABookCapture, PunchAction.Change);
                                        scope.Complete();
                                        break;
                                    case -1: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.UnSuccessMessage;
                                        break;
                                    case -2: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg1;
                                        break;
                                    case -3: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg3;
                                        break;
                                    case -4: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg4;
                                        break;
                                    case -5: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg5;
                                        break;
                                    case -6: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg6;
                                        break;
                                    case -7: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg7;
                                        break;
                                    case -8: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg8;
                                        break;
                                    case -9: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg9;
                                        break;
                                    case -10: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg10;
                                        break;
                                    case -12: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg13;
                                        break;
                                    default: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.UnSuccessMessage;
                                        break;
                                }
                                message.Text = errorMsg;
                            }

                        }
                    }
                }
            }


            return Json(message);
        }

        [HttpPost]
        public ActionResult WOAReleasedOrCancelledExecute(WOAReleasedOrCancelledModel model)
        {
            Message message = new Message();
            string errorMsg = string.Empty;

            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] != null)
                lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;

            //string woaNumber = string.Empty;
            //if (!String.IsNullOrEmpty(model.WOANumber))
            //{
            //    woaNumber = model.WOANumber.Replace("/", "").Replace("-", "").Trim();
            //}
            Woa woa = new WoaService().GetByWoaIntNo(model.WoaIntNo);

            if (woa == null)
            {
                message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg1;
            }
            else
            {
                if ((model.ServedStatus.Value == (int)WoaServedStatusList.ExecutedReleasedonSec72 || model.ServedStatus.Value == (int)WoaServedStatusList.ExecutedTransferred || model.ServedStatus.Value == (int)WoaServedStatusList.NotExecutedUntraceable) && woa.WoaType == "D")
                {
                    message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ErrorMsg2;
                }
                else
                {
                    //Jake 2013-11-20 add snapshot here 
                    using (TransactionScope tran = new TransactionScope())
                    {
                        SIL.AARTO.BLL.WOAExecutionSnapshot.WoaExecutionSnapshotDB wesDB = new BLL.WOAExecutionSnapshot.WoaExecutionSnapshotDB(connectionString);
                        if (wesDB.CheckSnapshotIsCreated(woa.WoaIntNo) == false)
                        {
                            bool flag = wesDB.SetSnapshot(woa.WoaIntNo, false, lastUser);
                            if (flag == false)
                            {
                                errorMsg = this.ResourceValue("WOABookCapture", "ReturnMsg12");
                                message.Text = errorMsg;
                                return Json(message);
                            }
                        }
                        else
                        {
                            wesDB.UnLockSnapshot(woa.WoaIntNo, lastUser);
                        }
                        Summons summons = new SummonsService().GetBySumIntNo(woa.SumIntNo);
                        int returnValue = new WOAManager(connectionString).WoaBookCapture(model.WoaIntNo, model.ServedStatus.Value, DateTime.Parse(model.NewCourtDate), model.CourtFrom, lastUser,
                            summons.CrtIntNo, model.BailAmount, model.BailReceiptNumber, model.BailPaidDate, out errorMsg);

                        switch (returnValue)
                        {
                            case 1: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.SuccessMessage;
                                message.Status = true;

                                int pfnIntNo = -1;
                                DateTime actionDate; string autCode = string.Empty;
                                WOAManager manager = new WOAManager(connectionString);
                                manager.SetPrintFileName(summons, DateTime.Parse(model.NewCourtDate), lastUser, out pfnIntNo, out actionDate, out autCode);
                                DateTime printActionDate = DateTime.Now.AddHours(new SysParamService().GetBySpColumnName(SysParamList.DelayActionDate_InHours.ToString()).SpIntegerValue);
                                new QueueItemProcessor().Send(
                                    new QueueItem()
                                    {
                                        Body = pfnIntNo,
                                        Group = autCode,
                                        ActDate = actionDate > printActionDate ? actionDate : printActionDate,
                                        QueueType = ServiceQueueTypeList.SecondAppearanceCCR
                                    }
                                );
                                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.WOAExecutionTransferOrReleasedOnSec72, PunchAction.Change);
                                tran.Complete();
                                break;
                            case -1: errorMsg += SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.UnSuccessMessage;
                                break;
                            case -2: errorMsg += SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg1;
                                break;
                            case -3: errorMsg += SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg3;
                                break;
                            case -4: errorMsg += SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg4;
                                break;
                            case -5: errorMsg += SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg5;
                                break;
                            case -6: errorMsg += SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg6;
                                break;
                            case -7: errorMsg += SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg7;
                                break;
                            case -8: errorMsg += SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg8;
                                break;
                            case -9: errorMsg += SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg9;
                                break;
                            case -10: errorMsg += SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg10;
                                break;
                            case -12: errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg13;
                                break;
                            default: errorMsg += SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.UnSuccessMessage;
                                break;
                        }
                        message.Text = errorMsg;
                    }
                }
            }

            return Json(message);
        }

        [HttpPost]
        public ActionResult GetWOAServedStatus(int ServedStatus)
        {
            Message message = new Message();
            WoaServedStatus served = new WoaServedStatusService().GetByWssIntNo(ServedStatus);
            if (served != null && served.WssIntNo != (int)WoaServedStatusList.NotExecutedWOACancelled)
            {
                message.Status = true;
                message.Text = served.WssDescr;
            }

            return Json(message);
        }

        public ActionResult WOAReleasedOrCancelled(WOAReleasedOrCancelledModel model)
        {
            Woa woa = new WoaService().GetByWoaIntNo(model.WoaIntNo);//.GetByWoaNoEnquiry(model.WOANumber.Replace("/", "").Replace("-", "").Trim()).FirstOrDefault();
            if (woa != null)
            {
                Summons summons = new SummonsService().DeepLoadBySumIntNo(woa.SumIntNo, false, DeepLoadType.IncludeChildren, new Type[] { typeof(Court), typeof(CourtRoom), typeof(SIL.AARTO.DAL.Entities.TList<Accused>) });

                model.AccusedName = (summons.AccusedCollection == null || summons.AccusedCollection.Count == 0) ? "" : String.Format("{0} {1}", summons.AccusedCollection[0].AccForenames, summons.AccusedCollection[0].AccSurname);
                model.IDNumber = summons.AccusedCollection[0].AccIdNumber;
                model.IDNumber = summons.AccusedCollection[0].AccIdNumber;
                model.CourtName = summons.CrtIntNoSource == null ? "" : summons.CrtIntNoSource.CrtName;
                model.CourtRoomNo = summons.CrtRintNoSource == null ? "" : String.Format("{0}~{1}", summons.CrtRintNoSource.CrtRoomNo, summons.CrtRintNoSource.CrtRoomName);

                Court court = new CourtService().GetByCrtIntNo(summons.CrtIntNo);
                //model.CourtToDesc = court.CrtName + " (" + court.CrtNo + ")";
                model.CourtToDesc = court.CrtName;
                model.CourtFromList = GetCourtFromList(summons.AutIntNo);
                //Jake 2014-09-01 added, the previous version  not display WOA number
                model.WOANumber = woa.WoaNumber;
            }

            return View(model);
        }

        private SelectList GetCourtFromList(int autIntNo)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            SelectListItem select = new SelectListItem()
            {
                Text = SIL.AARTO.Web.Resource.WOAManagement.WOAReleasedOrCancelled.lblDefaultForCourt,
                Value = ""
            };
            list.Add(select);

            CourtDB db = new CourtDB(connectionString);
            SqlDataReader reader = db.GetAuth_CourtListByAuth(autIntNo);
            while (reader.Read())
            {
                SelectListItem item = new SelectListItem()
                {
                    Text = reader["CrtDetails"].ToString(),
                    Value = reader["CrtIntNo"].ToString()
                };
                list.Add(item);
            }
            reader.Close();
            return new SelectList(list, "Value", "Text");
        }

        [HttpPost]
        public ActionResult SearchAccusedNameByWoaNumber(string woaNumber)
        {
            AccusedInfo accusedInfo = new WOAManager().GetAccusedInfoByWoaNumber(woaNumber);

            return Json(accusedInfo);
        }

        [HttpPost]
        public ActionResult SearchAccusedNameByWoaIntNo(int woaIntNo)
        {
            AccusedInfo accusedInfo = new WOAManager().GetAccusedInfoByWoaIntNo(woaIntNo);

            return Json(accusedInfo);
        }

        public ActionResult WOACaptureReverse()
        {
            WOACaptureReverseModel model = new WOACaptureReverseModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult WOACaptureReverseAsync(int woaIntNo)
        {

            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] == null){
                return Json(new Message() { Status = false, Text = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg9 });
            }
            UserLoginInfo  userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            lastUser = userInfo.UserName;
            AartoUserRole role = new AartoUserRoleService().GetByAaUserRoleName("ReversalOfS72");
            if (role == null) {
                return Json(new Message() { Status = false, Text = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg10 });
            }
            bool reversalOfS72 = userInfo.UserRoles.Contains(role.AaUserRoleId);
            string errorMsg = string.Empty;
            //string errorMsg = string.Empty;
            int returnValue = new WOAManager().WoaCaptureReverse(woaIntNo, lastUser,reversalOfS72, out errorMsg);
            switch (returnValue)
            {
                case 1:
                    errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.SuccessMessage;
                    break;
                case -1:
                    errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg1;
                    break;
                case -3:
                    errorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg3;
                    break;
                default:
                    break;
            }

            Message msg = new Message();
            msg.Status = returnValue > 0;
            msg.Text = errorMsg;

            return Json(msg);
        }

        [HttpPost]
        public ActionResult WOACaptureReverse(WOACaptureReverseModel model)
        {

            SIL.AARTO.DAL.Entities.TList<Woa> woas = new WOAManager(connectionString).DeepLoadSearchWoaByWoaNumber(model.WOANumber);
            model.WOAResult = new List<WOAReverseResultEntity>();
            if (woas != null)
            {
                model.WOAResult = new List<WOAReverseResultEntity>();
                foreach (Woa w in woas)
                {
                    model.WOAResult.Add(new WOAReverseResultEntity()
                    {
                        NotTicketNo = w.SumIntNoSource == null ? "" : w.SumIntNoSource.SumNoticeNo,
                        WoaIntNo = w.WoaIntNo,
                        WoaNumber = w.WoaNumber,
                        SummonsNo = w.SumIntNoSource == null ? "" : w.SumIntNoSource.SummonsNo,
                        AccFullName = w.SumIntNoSource == null ? "" : (w.SumIntNoSource.AccusedCollection.Count() == 0 ? "" : w.SumIntNoSource.AccusedCollection[0].AccFullName)
                    });
                }
            }

            //Jake 2014-03-04 comment out amd move it into WOACaptureReverseAsync function------------------- begin 
            //string lastUser = string.Empty;
            //if (Session["UserLoginInfo"] != null)
            //    lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;

            //model.Msg = string.Empty;
            //string errorMsg = string.Empty;
            //int returnValue = new WOAManager().WoaCaptureReverse(model.WOANumber, lastUser, out errorMsg);
            //switch (returnValue)
            //{
            //    case 1:
            //        model.Msg = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.SuccessMessage;
            //        break;
            //    case -1:
            //        model.Msg = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg1;
            //        break;
            //    case -2:
            //        model.Msg = errorMsg;
            //        break;
            //    case -3:
            //        model.Msg = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg3;
            //        break;
            //    default:
            //        if (!String.IsNullOrEmpty(errorMsg))
            //            model.Msg = errorMsg;
            //        break;
            //}
            //------------------- end

            /*
            switch (returnValue)
            {
                case 1: model.Msg = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.SuccessMessage;
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.WOAServiceOfSection72BookInReverse, PunchAction.Change);
                    break;
                case -1: model.Msg = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg1;
                    break;
                case -2: model.Msg = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg2;
                    break;
                case -3: model.Msg = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg3;
                    break;
                case -4: model.Msg = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg4;
                    break;
                case -5: model.Msg = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg5;
                    break;
                case -6: model.Msg = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg6;
                    break;
                case -7: model.Msg = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg7;
                    break;
                case -8: model.Msg = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg8;
                    break;
                default: model.Msg = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.UnSuccessMessage;
                    break;
            }
             * */
            return View(model);
        }

        public ActionResult WOABookOutReverse(int woaIntNo, long noticeRowVersion)
        {
            Message message = new Message();

            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] != null)
                lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;

            int returnValue = new WOAManager().WoaBookOutReverse(woaIntNo, lastUser, noticeRowVersion);
            message.User = returnValue.ToString();
            message.Status = returnValue > 0;
            switch (returnValue)
            {

                case 1: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.SuccessMessage;
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.WOABookOutToOfficer, PunchAction.Change);
                    break;
                case -1: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg1;
                    break;
                case -2: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg2;
                    break;
                case -3: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg3;
                    break;
                case -4: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg4;
                    break;
                case -5: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg5;
                    break;
                case -6: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg6;
                    break;
                case -7: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg7;
                    break;
                case -8: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOACaptureReverse.ReturnMsg8;
                    break;

                // 2014-07-22, Oscar added rowversion.
                case -99:
                    message.Text = Resource.WOAManagement.WOABookOut.RowVersionChanged;
                    break;

                default: message.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookOut.ReversalUnSuccessmessage;
                    break;
            }

            return Json(message);
        }

        void InitWOaBookOutModel(WOABookOutModel model)
        {

            SIL.AARTO.DAL.Entities.Authority authority = AuthorityManager.GetAuthorityByAuthIntNo(model.Authority);
            if (authority != null)
            {
                TrafficOfficerListItem officerListItem = new TrafficOfficerListItem();
                officerListItem.TOIntNo = 0;
                officerListItem.TOName = SIL.AARTO.Web.Resource.WOAManagement.WOABookOut.SelectTrafficOfficer;
                List<TrafficOfficerListItem> listFromOfficer = TrafficOfficerManager.GetTrafficOfficerListItem(authority.MtrIntNo);
                listFromOfficer.Insert(0, officerListItem);

                model.TrafficOfficers = new SelectList(listFromOfficer as IEnumerable, "ToIntNo", "TOName", model.TOIntNo);
            }
        }

        public ActionResult WoaAuthorisation()
        {
            WoaAuthorisationModel model = new WoaAuthorisationModel();
            //model.AuthorisationType = 1;// default is sent to court


            if (!String.IsNullOrEmpty(QueryString.GetString("SearchCrtIntNo")))
            {
                model.SearchCrtIntNo = Convert.ToInt32(QueryString.GetString("SearchCrtIntNo"));
            }
            if (!String.IsNullOrEmpty(QueryString.GetString("SearchCrtRIntNo")))
            {
                model.SearchCrtRIntNo = Convert.ToInt32(QueryString.GetString("SearchCrtRIntNo"));
            }
            if (!String.IsNullOrEmpty(QueryString.GetString("SearchCDIntNo")))
            {
                model.SearchCDIntNo = Convert.ToInt32(QueryString.GetString("SearchCDIntNo"));
            }
            if (!String.IsNullOrEmpty(QueryString.GetString("WoaNumber")))
            {
                model.WoaNumber = QueryString.GetString("WoaNumber");
            }
            InitModel(model);

            int totalCount = 0;

            if (Request.QueryString.AllKeys.Contains("Page"))
            {
                model.PageIndex = Convert.ToInt32(Request.QueryString["Page"]);
            }

            model.WoaAuthorisationList = woaAuthorisationManager.GetPagedPrintFileName(model.WoaNumber, model.SearchCrtIntNo, model.SearchCrtRIntNo,
               model.SearchCDIntNo, model.AutIntNo, model.PageIndex, model.PageSize, out totalCount);
            model.TotalCount = totalCount;


            List<VersionValidationEntity> versionList = woaAuthorisationManager.GetVersionValidateEntities(model.WoaAuthorisationList);

            Session["VersionList"] = versionList;

            return View(model);
        }

        // Jake 2013-08-28 modified search function
        [HttpPost]
        public ActionResult WoaAuthorisation(WoaAuthorisationModel model, FormCollection form)
        {
            InitModel(model);

            int totalCount = 0;
            if (!String.IsNullOrEmpty(form["btnSelect"]))
            {
                model.PageIndex = 0;
            }


            model.WoaAuthorisationList = woaAuthorisationManager.GetPagedPrintFileName(model.WoaNumber, model.SearchCrtIntNo, model.SearchCrtRIntNo,
                model.SearchCDIntNo, model.AutIntNo, model.PageIndex, model.PageSize, out totalCount);
            model.TotalCount = totalCount;

            model.URLPara = String.Format("WoaNumber={0}&SearchCrtIntNo={1}&SearchCrtRIntNo={2}&SearchCDIntNo={3}", model.WoaNumber, model.SearchCrtIntNo, model.SearchCrtRIntNo, model.SearchCDIntNo);

            List<VersionValidationEntity> versionList = woaAuthorisationManager.GetVersionValidateEntities(model.WoaAuthorisationList);

            Session["VersionList"] = versionList;

            //model.WoaPrintFileNameList = woaAuthorisationManager.GetPagedPrintFileName(model.WoaNumber, model.AuthorisationType, null, model.PageIndex, model.PageSize, out totalCount);
            //model.TotalCount = totalCount;

            //if (model.PFNIntNo.HasValue)
            //{
            //    //string[] arrayPFNIntNo;
            //    //if (model.PFNIntNo.IndexOf("~") >= 0)
            //    //{
            //    //    arrayPFNIntNo = model.PFNIntNo.Substring(0, model.PFNIntNo.LastIndexOf("~")).Split('~');
            //    //}
            //    //else arrayPFNIntNo = new string[] { model.PFNIntNo };

            //    model.WoaAuthorisationList = woaAuthorisationManager.GetByPrintFileID(model.WoaNumber, model.PFNIntNo, model.AuthorisationType);

            //    //foreach (WoaPrintFileNameEntity w in model.WoaPrintFileNameList)
            //    //{
            //    //    w.Checked = arrayPFNIntNo.Contains(w.PFNIntNo.ToString());
            //    //}
            //}
            //if (model.WoaPrintFileNameList != null && model.WoaPrintFileNameList.Count == 0)
            //{
            //    model.ErrorMessage = this.Resource("msgNoDataFound.Text") + (model.AuthorisationType == 1 ? " for woa sent to court" : " for woa return from court");
            //}
            //Jerry 2014-04-21 added by Shirley required
            if (model.WoaAuthorisationList != null && model.WoaAuthorisationList.Count == 0)
            {
                if (!string.IsNullOrEmpty(model.WoaNumber))
                    model.ErrorMessage = this.Resource("magNoDataForWOANo");
                else
                    model.ErrorMessage = this.Resource("msgNoDataFound.Text");
            }
            return View(model);
        }


        #region No longer in use

        [HttpPost]
        public ActionResult WoaSTCAuthorize(string woaIntNo, int crtIntNo, DateTime date)
        {
            Message msg = new Message();
            string errorMessage = string.Empty;

            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] != null)
                lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;

            if (date <= DateTime.Now.Date)
            {
                string[] arrayWoaIntNo;
                if (woaIntNo.IndexOf("~") >= 0)
                {
                    arrayWoaIntNo = woaIntNo.Substring(0, woaIntNo.LastIndexOf("~")).Split('~');
                }
                else arrayWoaIntNo = new string[] { woaIntNo };

                int returnValue = new WoaAuthorisationManager().WoaAuthorize(arrayWoaIntNo, crtIntNo, date, 1, lastUser, ref errorMessage);
                msg.Status = returnValue > 0;
                if (returnValue == -1)
                {
                    msg.Text = this.ResourceValue("WoaAuthorisation", "msgCoutNotMatch");
                    //msg.Text = SIL.AARTO.Web.Views.WOAManagement.App_LocalResources.WoaAuthorisation_cshtml.msgCoutNotMatch;// "Court does not match, can not authorize.";
                }
                else if (returnValue == -2)
                {
                    msg.Text = String.IsNullOrEmpty(errorMessage) ? "An error has occurred" : errorMessage;
                }
                else if (returnValue == -3)
                {
                    msg.Text = this.ResourceValue("WoaAuthorisation", "msgNoticeSummonsNotExists");
                    //msg.Text = SIL.AARTO.Web.Views.WOAManagement.App_LocalResources.WoaAuthorisation_cshtml.msgNoticeSummonsNotExists;// "Notice_summons record does not exist";
                }
            }
            else
            {
                msg.Status = false;
                msg.Text = this.ResourceValue("WoaAuthorisation", "msgInvalidDate");// "Reject date must be less than or equal today";

            }
            return Json(msg);
        }


        [HttpPost]
        public ActionResult WoaRFCAuthorize(int woaIntNo, DateTime date)
        {
            Message msg = new Message();
            string errorMessage = string.Empty;

            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] != null)
                lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;
            if (date <= DateTime.Now.Date)
            {
                int returnValue = new WoaAuthorisationManager().WoaAuthorize(new string[] { woaIntNo.ToString() }, 0, date, 2, lastUser, ref errorMessage);
                msg.Status = returnValue > 0;
                if (returnValue == 0)
                {
                    msg.Text = String.IsNullOrEmpty(errorMessage) ? "An error has occurred" : errorMessage;
                }
                else if (returnValue == -2)
                {
                    msg.Text = this.ResourceValue("WoaAuthorisation", "msgNoticeSummonsNotExists");
                    //msg.Text = SIL.AARTO.Web.Views.WOAManagement.App_LocalResources.WoaAuthorisation_cshtml.msgNoticeSummonsNotExists;// "Notice_summons record does not exist";
                }
            }
            else
            {
                msg.Status = false;
                msg.Text = this.ResourceValue("WoaAuthorisation", "msgInvalidDate");// "Reject date must be less than or equal today";
            }
            return Json(msg);
        }

        #endregion

        [HttpPost]
        public ActionResult GetCourtByWoaIntNo(string id)
        {
            int woaIntNo = 0;
            if (!String.IsNullOrEmpty(id))
            {
                if (id.IndexOf('~') >= 0) woaIntNo = Convert.ToInt32(id.Split('~')[0]);
                else woaIntNo = Convert.ToInt32(id);
            }
            int crtIntNo = new WoaAuthorisationManager().GetCourtByWoaIntNo(woaIntNo);

            return Json(crtIntNo);

        }

        [HttpPost]
        public ActionResult GetCourtRoomByCourt(int crtIntNo)
        {
            List<CourtRoomEntity> courtReturnList = new List<CourtRoomEntity>();
            List<CourtRoom> courtRooms = CourtRoomCache.GetAvailableCourtRoom(crtIntNo);
            if (courtRooms != null)
            {
                foreach (CourtRoom cr in courtRooms)
                {
                    courtReturnList.Add(new CourtRoomEntity() { CrtRIntNo = cr.CrtRintNo, CrtRoomName = cr.CrtRoomNo + " (" + cr.CrtRoomName + ")" });
                }
            }
            // courtRooms.Insert(0, new CourtRoom() { CrtRintNo = 0, CrtRoomName = this.ResourceValue("WoaAuthorisation", "msgSelectCourtRoom") });

            return Json(courtReturnList);
        }

        [HttpPost]
        public ActionResult GetCourtDateByCrtrIntNo(int crtRIntNo)
        {
            int autIntNo = 0;
            if (Session["autIntNo"] != null) autIntNo = Convert.ToInt32(Session["autIntNo"]);
            else if (Session["UserLoginInfo"] != null)
                autIntNo = ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo;

            List<CourtDateEntity> courtDateReturnList = new List<CourtDateEntity>();
            courtDateReturnList = GetCourtDateByCourtRoom(crtRIntNo, autIntNo);

            //courtDateReturnList.Insert(0, new CourtDateEntity() { CDIntNo = 0, CDate = this.ResourceValue("WoaAuthorisation", "msgSelectCourtDate") });


            return Json(courtDateReturnList);
        }

        [HttpPost]
        public ActionResult SubmitAuthorisation(List<WoaAuthorisationJsonEntity> para)
        {
            List<Message> msgs = new List<Message>();
            int updateCount = 0;
            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] != null)
                lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;

            List<VersionValidationEntity> versionList = Session["VersionList"] as List<VersionValidationEntity>;
            if (versionList != null && versionList.Count > 0)
            {
                foreach (WoaAuthorisationJsonEntity w in para)
                {

                    VersionValidationEntity ve = versionList.Where(e => e.WoaIntNo.Equals(w.WoaIntNo)).FirstOrDefault();
                    if (ve != null)
                    {
                        if (ve.NoticeStatus == w.Status)
                        {
                            //831	WarrantPrinted
                            //832	WarrantDeliveredToCourt
                            //840	ReturnedFromCourtAuthorised
                            //875   ReturnedFromCourtRejected

                            Message msg = null;

                            if (w.Status == (int)ChargeStatusList.WarrantPrinted)
                            {
                                // process sent to court logic
                                if (!String.IsNullOrEmpty(w.SentToCourt))
                                {
                                    msg = SentToCourt(ve, w, lastUser);

                                    if (msg.Status == false)
                                    {
                                        msgs.Add(msg);
                                    }
                                    //2013-11-25 Heidi added for add all Punch Statistics Transaction(5084)
                                    if (msg.Status)
                                    {
                                        updateCount = updateCount + 1;
                                    }
                                }
                            }
                            else if (w.Status == (int)ChargeStatusList.WarrantDeliveredToCourt)
                            {
                                // pricess reverse sent to court or return from court authorize logic
                                if (String.IsNullOrEmpty(w.SentToCourt))
                                {
                                    // reverse sent to court
                                    msg = ReverseWoa(w, 1, lastUser);
                                    if (msg.Status == false) msgs.Add(msg);
                                    //2013-11-25 Heidi added for add all Punch Statistics Transaction(5084)
                                    if (msg.Status)
                                    {
                                        updateCount = updateCount + 1;
                                    }
                                }
                                else if (!String.IsNullOrEmpty(w.ReturnFromCourt) && String.IsNullOrEmpty(w.RejectDate))
                                {
                                    // return from court
                                    msg = ReturnFromCourt(ve, w, lastUser);
                                    if (msg.Status == false) msgs.Add(msg);
                                    //2013-11-25 Heidi added for add all Punch Statistics Transaction(5084)
                                    if (msg.Status)
                                    {
                                        updateCount = updateCount + 1;
                                    }

                                }
                                else if (String.IsNullOrEmpty(w.ReturnFromCourt) && !String.IsNullOrEmpty(w.RejectDate))
                                {
                                    // rejected authorisation
                                    msg = RejectWoaAuthorisation(ve, w, lastUser);
                                    if (msg.Status == false) msgs.Add(msg);
                                    //2013-11-25 Heidi added for add all Punch Statistics Transaction(5084)
                                    if (msg.Status)
                                    {
                                        updateCount = updateCount + 1;
                                    }
                                }

                            }
                            else if (w.Status == (int)ChargeStatusList.ReturnedFromCourtAuthorised)
                            {
                                // process reverse return from court authorised logjc
                                if (string.IsNullOrEmpty(w.ReturnFromCourt))
                                {
                                    msg = ReverseWoa(w, 2, lastUser);
                                    if (msg.Status == false) msgs.Add(msg);
                                    //2013-11-25 Heidi added for add all Punch Statistics Transaction(5084)
                                    if (msg.Status)
                                    {
                                        updateCount = updateCount + 1;
                                    }
                                }

                            }
                            else if (w.Status == (int)ChargeStatusList.ReturnedFromCourtRejected)
                            {
                                // process reverse return from court rejected logic
                                if (String.IsNullOrEmpty(w.RejectDate))
                                {
                                    msg = ReverseWoa(w, 3, lastUser);
                                    if (msg.Status == false) msgs.Add(msg);
                                    //2013-11-25 Heidi added for add all Punch Statistics Transaction(5084)
                                    if (msg.Status)
                                    {
                                        updateCount = updateCount + 1;
                                    }
                                }
                            }
                        }

                    }
                }
            }

            if (updateCount > 0)
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.WOAAuthorisation, PunchAction.Change);
            }
            return Json(msgs);
        }

        public ActionResult RejectWoa(int woaIntNo, DateTime rejDate, string rejReason)
        {
            Message msg = new Message();
            string errorMessage = string.Empty;

            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] != null)
                lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;

            if (rejDate <= DateTime.Now.Date)
            {
                msg.Status = new WoaAuthorisationManager().RejectWoa(woaIntNo, rejDate, rejReason, lastUser, ref errorMessage);
                msg.Text = errorMessage;
            }
            else
            {
                msg.Status = false;
                msg.Text = this.ResourceValue("WoaAuthorisation", "msgInvalidDate");// "Reject date must be less than or equal today";
            }
            return Json(msg);
        }

        [HttpGet]
        public ActionResult SendToCourt()
        {
            WOASendToCourtModel model = new WOASendToCourtModel();

            if (!String.IsNullOrEmpty(Request.QueryString["Page"]))
            {
                model.PageIndex = Convert.ToInt32(Request.QueryString["Page"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["CrtIntNo"]))
            {
                model.SearchCrtIntNo = Convert.ToInt32(Request.QueryString["CrtIntNo"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["CrtRIntNo"]))
            {
                model.SearchCrtRIntNo = Convert.ToInt32(Request.QueryString["CrtRIntNo"]);
            }
            //if (!string.IsNullOrEmpty(Request.QueryString["CDate"]))
            //{
            //    model.SearchCourtDate = Request.QueryString["CDate"];
            //}
            if (!String.IsNullOrEmpty(Request.QueryString["CDIntNo"]))
            {
                model.SearchCourtDate = Request.QueryString["CDIntNo"];
                model.SearchCDIntNo = Convert.ToInt32(Request.QueryString["CDIntNo"]);
            }
            InitSendToCourtModel(model);
            model.NoDateFound = false;
            return View(model);
        }

        [HttpPost]
        public ActionResult SendToCourt(WOASendToCourtModel model)
        {
            InitSendToCourtModel(model);
            model.NoDateFound = model.WOAPrintFileNames.Count == 0;

            model.URLPara = String.Format("CrtIntNo={0}&CrtRIntNo={1}&CDIntNo={2}", model.SearchCrtIntNo, model.SearchCrtRIntNo, model.SearchCourtDate);
            return View(model);
        }

        [HttpPost]
        public ActionResult GetWOaByPfnIntNo(int pfnIntNo)
        {
            int totalCount = 0;
            List<WoaAuthorisationEntity> list = woaAuthorisationManager.GetWOAByPfnIntNo(pfnIntNo, 0, Int16.MaxValue, out totalCount);

            var result = from s in list
                         select new { s.WoaIntNo, s.WoaNumber, s.SummonsNo, s.AccFullName };

            return Json(result.ToList());
        }

        [HttpPost]
        public ActionResult SubmitSendToCourtByPFNIntNo(int pfnIntNo, string date)
        {
            // Jake 2014-01-10 added last user
            Message msg = new Message();
            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] != null)
                lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;
            else
            {
                return Json(msg);
            }

            string errMsg = "";
            int returnValue = 0;
            if (!String.IsNullOrEmpty(date))
            {
                //Jerry 2014-10-23 check the date parameter
                DateTime tempDate;
                if (!DateTime.TryParse(date, out tempDate))
                {
                    msg.Status = false;
                    msg.Text = string.Format(this.ResourceValue("SendToCourt", "msgErrorMessage.Text3"), date);
                    return Json(msg);
                }

                returnValue = woaAuthorisationManager.ProcessSendToCourt(pfnIntNo, Convert.ToDateTime(date), lastUser, out errMsg);
                if (returnValue == 0)
                {
                    msg.Text = this.ResourceValue("SendToCourt", "msgErrorMessage.Text1");
                }
                else if (returnValue < 0)
                {
                    msg.Text = this.ResourceValue("SendToCourt", "msgErrorMessage.Text2") + "\r\n" + errMsg;
                }
                if (returnValue > 0)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.WOABookOutToCourt, PunchAction.Change);
                }
            }
            msg.Status = returnValue > 0;

            return Json(msg);
        }

        [HttpPost]
        public ActionResult SubmitSendToCourt(SendToCourtJsonEntity para)
        {
            // Jake 2014-01-10 added last user
            Message msg = new Message();
            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] != null)
                lastUser = ((UserLoginInfo)Session["UserLoginInfo"]).UserName;
            else
            {
                return Json(msg);
            }

            //Jerry 2014-10-23 check the date parameter
            DateTime tempDate;
            if (!DateTime.TryParse(para.CourtDate, out tempDate))
            {
                msg.Status = false;
                msg.Text = this.ResourceValue("SendToCourt", "msgErrorMessage.Text3", para.CourtDate);
                return Json(msg);
            }

            string errorMsg = string.Empty;
            int result = woaAuthorisationManager.WoaAuthorize(para.WoaIntNoList.ToArray(), para.CrtIntNo,
                tempDate, 1, lastUser, ref errorMsg);
            msg.Status = result > 0;

            if (result == -1)
            {
                msg.Text = this.ResourceValue("WoaAuthorisation", "msgCoutNotMatch");
                //msg.Text = SIL.AARTO.Web.Views.WOAManagement.App_LocalResources.WoaAuthorisation_cshtml.msgCoutNotMatch;// "Court does not match, can not authorize.";
            }
            else if (result == -2)
            {
                msg.Text = String.IsNullOrEmpty(errorMsg) ? "An error has occurred" : errorMsg;
            }
            else if (result == -3)
            {
                msg.Text = this.ResourceValue("WoaAuthorisation", "msgNoticeSummonsNotExists") + errorMsg;
                //msg.Text = SIL.AARTO.Web.Views.WOAManagement.App_LocalResources.WoaAuthorisation_cshtml.msgNoticeSummonsNotExists;// "Notice_summons record does not exist";
            }
            else if (result == -4)
            {
                msg.Text = this.ResourceValue("WoaAuthorisation", "msgErrorMessage.Text2");
            }
            if (result > 0)
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.WOABookOutToCourt, PunchAction.Change);
            }
            return Json(msg);
        }

        public ReportResult WoaAtCourt(int crtIntNo, int cdIntNo)
        {
            string reportPath = HttpContext.Request.PhysicalApplicationPath + @"\Reports\WoaAtCourt.dplx";
            DocumentLayout document = new DocumentLayout(reportPath);
            StoredProcedureQuery query = (StoredProcedureQuery)document.GetQueryById("Query");
            query.ConnectionString = connectionString;

            CourtDates cDate = new CourtDatesService().GetByCdIntNo(cdIntNo);

            ParameterDictionary parameters = new ParameterDictionary();
            parameters.Add("CrtIntNo", crtIntNo);
            parameters.Add("CourtDate", cDate.Cdate);

            //ParameterDictionary parameters = new ParameterDictionary();
            //parameters.Add("ReceiptStartDate", dtReceiptStartDate);
            //parameters.Add("ReceiptEndDate", dtReceiptEndDate);
            //parameters.Add("AutIntNo", autIntNo);
            //parameters.Add("CrtIntNo", CrtIntNo);
            //if (AGorSpot == "A")
            //    parameters.Add("CrtRIntNo", CrtRIntNo);
            //parameters.Add("SortOrder", SortOrder);
            //parameters.Add("AGorSpot", AGorSpot);

            //FormattedRecordArea printDate = (FormattedRecordArea)document.GetElementById("printDate");

            //printDate.LaidOut += new FormattedRecordAreaLaidOutEventHandler(printDate_LaidOut);
            byte[] buffer = null;
            if (System.IO.File.Exists(reportPath))
            {
                Document report = document.Run(parameters);

                buffer = report.Draw();

                //Response.Redirect("~/ReportView.aspx");

            }
            return new ReportResult(buffer, "application/pdf");
        }

        [HttpPost]
        public ActionResult CheckNoticeForWOAExecution(int woaIntNo)
        {
            Message msg = new Message();
            try
            {
                msg.Status = woaAuthorisationManager.CheckNoticeForWOAExecution(woaIntNo);
                //if (msg.Status == false)
                //{
                //    msg.Text = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg14;// "Notice is not eligibility for WOA execution";
                //}
            }
            catch (Exception ex)
            {
                msg.Text = ex.ToString();
            }

            return Json(msg);
        }


        void InitModel(WoaAuthorisationModel model)
        {
            model.PageSize = pageSize;
            model.ErrorMessage = string.Empty;

            if (Session["autIntNo"] != null) model.AutIntNo = Convert.ToInt32(Session["autIntNo"]);
            else if (Session["UserLoginInfo"] != null)
                model.AutIntNo = ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo;

            List<AuthorisationTypeEntity> autTypeList = new List<AuthorisationTypeEntity>();

            autTypeList.AddRange(new AuthorisationTypeEntity[] { 
                new AuthorisationTypeEntity() { TypeId=1, Description="Sent to court" }, 
                new AuthorisationTypeEntity() { TypeId=2, Description="Return from court" } });
            model.AuthorisationTypeList = new SelectList(autTypeList, "TypeId", "Description", model.AuthorisationType);

            List<Court> courts = new List<Court>();
            //courts = new CourtManager().GetAllCourts();
            //courts = new CourtManager().GetAllCourts();

            CourtDB db = new CourtDB(connectionString);
            SqlDataReader reader = db.GetAuth_CourtListByAuth(model.AutIntNo.Value);
            while (reader.Read())
            {
                Court court = new Court();
                court.CrtIntNo = Convert.ToInt32(reader["CrtIntNo"].ToString());
                court.CrtName = reader["CrtDetails"].ToString();
                courts.Add(court);
            }
            reader.Close();
            reader.Dispose();//Jerry 2014-10-24 add

            courts.Insert(0, new Court() { CrtIntNo = 0, CrtName = this.Resource("msgSelectCourt") });
            model.Courts = new SelectList(courts as System.Collections.IEnumerable, "CrtIntNo", "CrtName");

            model.SearchCourts = model.Courts;

            List<CourtRoom> courtRooms = CourtRoomCache.GetAvailableCourtRoom(model.SearchCrtIntNo);
            foreach (CourtRoom r in courtRooms)
            {
                r.CrtRoomName = r.CrtRoomNo + " (" + r.CrtRoomName + ")";
            }
            courtRooms.Insert(0, new CourtRoom() { CrtRintNo = 0, CrtRoomName = this.Resource("msgSelectCourtRoom") });
            model.SearchCourtRooms = new SelectList(courtRooms, "CrtRIntNo", "CrtRoomName", model.SearchCrtRIntNo);


            List<CourtDateEntity> courtDates = GetCourtDateByCourtRoom(model.SearchCrtRIntNo, model.AutIntNo ?? 0);
            courtDates.Insert(0, new CourtDateEntity() { CDIntNo = 0, CDate = this.Resource("msgSelectCourtDate") });
            model.SearchCourtDates = new SelectList(courtDates, "CDIntNo", "CDate", model.SearchCDIntNo);

        }

        void InitSendToCourtModel(WOASendToCourtModel model)
        {

            if (Session["autIntNo"] != null) model.AutIntNo = Convert.ToInt32(Session["autIntNo"]);
            else if (Session["UserLoginInfo"] != null)
                model.AutIntNo = ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo;

            List<Court> courts = new List<Court>();
            //courts = new CourtManager().GetAllCourts();

            CourtDB db = new CourtDB(connectionString);
            SqlDataReader reader = db.GetAuth_CourtListByAuth(model.AutIntNo.Value);
            while (reader.Read())
            {
                Court court = new Court();
                court.CrtIntNo = Convert.ToInt32(reader["CrtIntNo"].ToString());
                court.CrtName = reader["CrtDetails"].ToString();
                courts.Add(court);
            }
            reader.Close();
            reader.Dispose();//Jerry 2014-10-24 add

            courts.Insert(0, new Court() { CrtIntNo = 0, CrtName = this.Resource("msgSelectCourt") });

            model.SearchCourts = new SelectList(courts as System.Collections.IEnumerable, "CrtIntNo", "CrtName"); ;

            List<CourtRoom> courtRooms = CourtRoomCache.GetAvailableCourtRoom(model.SearchCrtIntNo);
            foreach (CourtRoom r in courtRooms)
            {
                r.CrtRoomName = r.CrtRoomNo + " (" + r.CrtRoomName + ")";
            }
            courtRooms.Insert(0, new CourtRoom() { CrtRintNo = 0, CrtRoomName = this.Resource("msgSelectCourtRoom") });
            model.SearchCourtRooms = new SelectList(courtRooms, "CrtRIntNo", "CrtRoomName", model.SearchCrtRIntNo);


            List<CourtDateEntity> courtDates = GetCourtDateByCourtRoom(model.SearchCrtRIntNo, model.AutIntNo ?? 0);
            courtDates.Insert(0, new CourtDateEntity() { CDIntNo = 0, CDate = this.Resource("msgSelectCourtDate") });
            model.SearchCourtDates = new SelectList(courtDates, "CDIntNo", "CDate", model.SearchCDIntNo);

            DateTime? cdate = null;
            if (!String.IsNullOrEmpty(model.SearchCourtDate))
            {
                CourtDateEntity courtdate = courtDates.Where(c => c.CDIntNo == Convert.ToInt32(model.SearchCourtDate)).FirstOrDefault();
                if (courtdate != null)
                    cdate = Convert.ToDateTime(courtdate.CDate);
                //cdate = Convert.ToDateTime(model.SearchCourtDate);
            }

            int totalCount = 0;

            model.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);

            model.WOAPrintFileNames = woaAuthorisationManager.GetWoaSendToCourt(model.WoaNumber, model.AutIntNo, model.SearchCrtIntNo, model.SearchCrtRIntNo,
                 cdate, model.PageIndex, model.PageSize, out totalCount);

            model.TotalCount = totalCount;

        }

        List<CourtDateEntity> GetCourtDateByCourtRoom(int CrtRIntNo, int authIntNo)
        {
            CourtDatesDB db = new CourtDatesDB(connectionString);

            //SqlDataReader reader = db.GetCourtDatesList(crtRIntNo, autIntNo);
            SqlDataReader reader = db.GetCourtDatesList(CrtRIntNo, authIntNo);    // Oscar 20100920 - Change 

            List<CourtDateEntity> courtDates = new List<CourtDateEntity>();
            while (reader.Read())
            {

                courtDates.Add(new CourtDateEntity() { CDIntNo = Convert.ToInt32(reader["CDIntNo"].ToString()), CDate = String.Format("{0:yyyy-MM-dd}", reader["CDate"]) });

            }
            reader.Close();

            return courtDates;
        }

        Message SentToCourt(VersionValidationEntity ve, WoaAuthorisationJsonEntity w, string lastUser)
        {
            string errorMessage = string.Empty;

            Message msg = new Message();

            DateTime date = Convert.ToDateTime("2000-01-01");
            if (!String.IsNullOrEmpty(w.SentToCourt))
            {
                date = Convert.ToDateTime(w.SentToCourt);
            }

            if (date <= DateTime.Now.Date)
            {
                string[] arrayWoaIntNo;
                arrayWoaIntNo = new string[] { w.WoaIntNo.ToString() };

                bool validated = woaAuthorisationManager.CheckRowVersion(ve, out errorMessage);
                if (validated == false)
                {
                    msg.Text = errorMessage;
                    return msg;
                }

                int returnValue = woaAuthorisationManager.WoaAuthorize(arrayWoaIntNo, w.CrtIntNo, date, 1, lastUser, ref errorMessage);
                msg.Status = returnValue > 0;
                if (returnValue == -1)
                {
                    msg.Text = this.ResourceValue("WoaAuthorisation", "msgCoutNotMatch");
                    //msg.Text = SIL.AARTO.Web.Views.WOAManagement.App_LocalResources.WoaAuthorisation_cshtml.msgCoutNotMatch;// "Court does not match, can not authorize.";
                }
                else if (returnValue == -2)
                {
                    msg.Text = String.IsNullOrEmpty(errorMessage) ? "An error has occurred" : errorMessage;
                }
                else if (returnValue == -3)
                {
                    msg.Text = this.ResourceValue("WoaAuthorisation", "msgNoticeSummonsNotExists");
                    //msg.Text = SIL.AARTO.Web.Views.WOAManagement.App_LocalResources.WoaAuthorisation_cshtml.msgNoticeSummonsNotExists;// "Notice_summons record does not exist";
                }
                else if (returnValue == -4)
                {
                    msg.Text = this.ResourceValue("WoaAuthorisation", "msgErrorMessage.Text2");

                }
            }
            else
            {
                msg.Status = false;
                msg.Text = this.ResourceValue("WoaAuthorisation", "msgInvalidDate");// "Reject date must be less than or equal today";

            }

            return msg;
        }

        Message ReturnFromCourt(VersionValidationEntity ve, WoaAuthorisationJsonEntity w, string lastUser)
        {
            Message msg = new Message();
            string errorMessage = string.Empty;

            DateTime date = Convert.ToDateTime("2000-01-01");
            if (!String.IsNullOrEmpty(w.ReturnFromCourt))
            {
                date = Convert.ToDateTime(w.ReturnFromCourt);
                DateTime sendToCourtDate = Convert.ToDateTime(w.SentToCourt);
                if (date < sendToCourtDate)
                {
                    msg.Text = this.ResourceValue("WoaAuthorisation", "msgErrorMessage.Text1");
                    return msg;
                }
            }

            if (date <= DateTime.Now.Date)
            {
                bool validated = woaAuthorisationManager.CheckRowVersion(ve, out errorMessage);
                if (validated == false)
                {
                    msg.Text = errorMessage;
                    return msg;
                }

                int returnValue = woaAuthorisationManager.WoaAuthorize(new string[] { w.WoaIntNo.ToString() }, 0, date, 2, lastUser, ref errorMessage);
                msg.Status = returnValue > 0;
                if (returnValue == 0)
                {
                    msg.Text = String.IsNullOrEmpty(errorMessage) ? "An error has occurred" : errorMessage;
                }
                else if (returnValue == -3)
                {
                    msg.Text = this.ResourceValue("WoaAuthorisation", "msgNoticeSummonsNotExists");
                    //msg.Text = SIL.AARTO.Web.Views.WOAManagement.App_LocalResources.WoaAuthorisation_cshtml.msgNoticeSummonsNotExists;// "Notice_summons record does not exist";
                }
                else if (returnValue == -4)
                {
                    msg.Text = this.ResourceValue("WoaAuthorisation", "msgErrorMessage.Text2");

                }
                else
                {
                    msg.Text = errorMessage;
                }
            }
            else
            {
                msg.Status = false;
                msg.Text = this.ResourceValue("WoaAuthorisation", "msgInvalidDate");// "Reject date must be less than or equal today";
            }

            return msg;
        }

        Message RejectWoaAuthorisation(VersionValidationEntity ve, WoaAuthorisationJsonEntity w, string lastUser)
        {
            Message msg = new Message();
            string errorMessage = string.Empty;

            DateTime rejDate = Convert.ToDateTime("2000-01-01");
            if (!String.IsNullOrEmpty(w.RejectDate))
            {
                rejDate = Convert.ToDateTime(w.RejectDate);
            }

            bool validated = woaAuthorisationManager.CheckRowVersion(ve, out errorMessage);
            if (validated == false)
            {
                msg.Text = errorMessage;
                return msg;
            }

            if (rejDate <= DateTime.Now.Date)
            {
                msg.Status = new WoaAuthorisationManager().RejectWoa(w.WoaIntNo, rejDate, w.RejectReason, lastUser, ref errorMessage);
                msg.Text = errorMessage;
            }
            else
            {
                msg.Status = false;
                msg.Text = this.ResourceValue("WoaAuthorisation", "msgInvalidDate");// "Reject date must be less than or equal today";
            }

            return msg;
        }

        Message ReverseWoa(WoaAuthorisationJsonEntity w, int authorizetype, string lastUser)
        {
            string errorMessage = string.Empty;
            Message msg = new Message();
            int returnValue = woaAuthorisationManager.ReverseWoa(w.WoaIntNo, authorizetype, lastUser, ref errorMessage);
            msg.Status = returnValue > 0;

            if (returnValue == 0)
            {
                msg.Text = String.IsNullOrEmpty(errorMessage) ? "An error has occurred" : errorMessage;
            }
            else if (returnValue == -2)
            {
                msg.Text = this.ResourceValue("WoaAuthorisation", "msgNoticeSummonsNotExists") + errorMessage;
                //msg.Text = SIL.AARTO.Web.Views.WOAManagement.App_LocalResources.WoaAuthorisation_cshtml.msgNoticeSummonsNotExists;// "Notice_summons record does not exist";
            }

            return msg;
        }
    }
}
