﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.StreetCoding;
using SIL.AARTO.BLL.StreetCoding.Model;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.Web.Helpers.Paginator;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.StreetCoding;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.StreetCoding
{
    public partial class StreetCodingController : Controller
    {
        //
        // GET: /PlaceAtIntersection/
        PlaceAtIntersectionManager paiMana = new PlaceAtIntersectionManager();

        public ActionResult PlaceAtIntersectionManage()
        {
            PlaceAtIntersectionModel model = new PlaceAtIntersectionModel();
            int pageIndex = 0;
            string searchKey = QueryString.GetString("s");

            if (!string.IsNullOrEmpty(QueryString.GetString("Page")))
            {
                pageIndex = Convert.ToInt32(QueryString.GetString("Page"));
            }
            if (!string.IsNullOrEmpty(QueryString.GetString("a")))
            {
                model.SearchAutIntNo = Convert.ToInt32(QueryString.GetString("a"));
            }
            if (!string.IsNullOrEmpty(QueryString.GetString("sb")))
            {
                model.SearchLoSuIntNo = Convert.ToInt32(QueryString.GetString("sb"));
            }
            if (!string.IsNullOrEmpty(QueryString.GetString("st1")))
            {
                model.SearchStreet1 = Convert.ToInt32(QueryString.GetString("st1"));
            }
            if (!string.IsNullOrEmpty(QueryString.GetString("st2")))
            {
                model.SearchStreet2 = Convert.ToInt32(QueryString.GetString("st2"));
            }
            if (!string.IsNullOrEmpty(searchKey))
            {
                InitModel(model, pageIndex);
            }
            return View(model);
        }

        void InitModel(PlaceAtIntersectionModel model, int pageIndex)
        {
            int totalCount = 0;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            ViewData["pageSize"] = pageSize;

            model.Intersections = paiMana.GetAllPAI(model.SearchAutIntNo, model.SearchLoSuIntNo, model.SearchStreet1, model.SearchStreet2, pageIndex, pageSize, out totalCount);
            model.TotalCount = totalCount;
        }

        [HttpPost]
        public JsonResult GetStSelectTip(int st1, int st2)
        {
            Message message = new Message();
            if (st1 > 0 && st2 > 0)
            {
                message.Text = PlaceAtIntersectionManage_cshtml.StSelectTip;
                message.Status = true;
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult GetPAI(int paiId)
        {
            Message message = new Message();
            if (paiId != 0)
            {
                PlaceAtIntersectionModel pai = paiMana.GetPaiById(paiId);
                message.Text = pai.PAIIntNo + "," + pai.AutIntNo + "," + pai.LoSuIntNo + "," + pai.CrtIntNo + "," + pai.PAIStreet1 + "," + pai.PAIStreet2 + "," + pai.PAIGPSXCoord + "," + pai.PAIGPSYCoord;
                message.Status = true;
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult SavePAI(int paiId, int sub, int st1, int st2, string xCoord, string yCoord, int crtId, int st1Before, int st2Before, int autIntNo)
        {
            Message message = new Message();
            UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
            string lastUser = userLofinInfo.UserName;

            PlaceAtIntersectionModel paiModel = new PlaceAtIntersectionModel();
            paiModel.PAIIntNo = paiId;
            paiModel.LoSuIntNo = sub;
            paiModel.PAIStreet1 = st1;
            paiModel.PAIStreet2 = st2;
            paiModel.PAIGPSXCoord = xCoord;
            paiModel.PAIGPSYCoord = yCoord;
            paiModel.CrtIntNo = crtId;
            paiModel.LastUser = lastUser;
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            paiModel.AutIntNo = autIntNo;

            int actStatus = paiMana.SavePAI(paiModel, st1Before, st2Before);
            if (actStatus == 1)
            {
                message.Status = true;
                message.Text = Global.StatusSave_1;
            }
            else if (actStatus == 2)
            {
                message.Status = false;
                message.Text = PlaceAtIntersectionManage_cshtml.StatusSave_2;
            }
            else
            {
                message.Status = false;
                message.Text = Global.StatusSave_0;
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult DelePAI(int paiId,int autIntNo)
        {
            Message message = new Message();
            if (paiId > 0)
            {
                int actStatus = paiMana.DeletePAIByID(paiId);
                if (actStatus == 1)
                {
                    message.Status = true;
                    message.Text = Global.StatusDelete_1;
                  
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, LastUser, PunchStatisticsTranTypeList.PlaceAtIntersectionMaintenance, PunchAction.Delete);

                }
                else
                {
                    message.Status = false;
                    message.Text = Global.StatusDelete_0;
                }
            }
            return Json(message);
        }
    }
}
