﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.StreetCoding;
using SIL.AARTO.BLL.StreetCoding.Model;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.Web.Helpers.Paginator;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.StreetCoding;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.StreetCoding
{
    public partial class StreetCodingController : Controller
    {
        //
        // GET: /PlaceAtFixedLocation/
        PlaceAtFixedLocationManager fixlocMana = new PlaceAtFixedLocationManager();

        public ActionResult PlaceAtFixedLocationManage()
        {
            PlaceAtFixedLocationModel model = new PlaceAtFixedLocationModel();
            int pageIndex = 0;
            string searchKey = QueryString.GetString("s");

            if (!string.IsNullOrEmpty(QueryString.GetString("Page")))
            {
                pageIndex = Convert.ToInt32(QueryString.GetString("Page"));
            }
            if (!string.IsNullOrEmpty(QueryString.GetString("a")))
            {
                model.SearchAutIntNo = Convert.ToInt32(QueryString.GetString("a"));
            }
            if (!string.IsNullOrEmpty(QueryString.GetString("sb")))
            {
                model.SearchLoSuIntNo = Convert.ToInt32(QueryString.GetString("sb"));
            }
            if (!string.IsNullOrEmpty(QueryString.GetString("st")))
            {
                model.SearchStreet = Convert.ToInt32(QueryString.GetString("st"));
            }
            if (!string.IsNullOrEmpty(searchKey))
            {
                model.SearchKey = searchKey;
            }
            if (searchKey != null)
            {
                InitModel(model, pageIndex);
            }
            return View(model);
        }

        void InitModel(PlaceAtFixedLocationModel model, int pageIndex)
        {
            int totalCount = 0;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            ViewData["pageSize"] = pageSize;

            model.FixedLocations = fixlocMana.GetAllFixLoc(model.SearchAutIntNo, model.SearchLoSuIntNo, model.SearchStreet, model.SearchKey, pageIndex, pageSize, out totalCount);
            model.TotalCount = totalCount;
        }

        [HttpPost]
        public JsonResult GetFixLocSelectTip(int stId, string locName)
        {
            Message message = new Message();
            if (stId > 0 && !string.IsNullOrEmpty(locName))
            {
                message.Text = PlaceAtFixedLocationManage_cshtml.FixLocSelectTip;
                message.Status = true;
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult GetFixLoc(int flId)
        {
            Message message = new Message();
            if (flId != 0)
            {
                PlaceAtFixedLocationModel fixloc = fixlocMana.GetFixLocById(flId);
                message.Text = fixloc.PAFLIntNo + "," + fixloc.AutIntNo + "," + fixloc.LoSuIntNo + "," + fixloc.CrtIntNo + "," + fixloc.PAFLStreet1 + "," + fixloc.PAFLocation + "," + fixloc.PAFLGPSXCoord + "," + fixloc.PAFLGPSYCoord;
                message.Status = true;
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult SaveFixLoc(int flId, int subId, int stId, string locName, string xCoord, string yCoord, int crtId, int subBefore, int stBefore, string locBefore, int autIntNo)
        {
            Message message = new Message();
            UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
            string lastUser = userLofinInfo.UserName;

            locBefore = locBefore.Trim();
            PlaceAtFixedLocationModel fixloc = new PlaceAtFixedLocationModel();
            fixloc.PAFLIntNo = flId;
            fixloc.LoSuIntNo = subId;
            fixloc.PAFLStreet1 = stId;
            fixloc.PAFLocation = locName.Trim();
            fixloc.PAFLGPSXCoord = xCoord.Trim();
            fixloc.PAFLGPSYCoord = yCoord.Trim();
            fixloc.CrtIntNo = crtId;
            fixloc.LastUser = lastUser;
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            fixloc.AutIntNo = autIntNo;

            int actStatus = fixlocMana.SaveFixLoc(fixloc, subBefore, stBefore, locBefore);
            if (actStatus == 1)
            {
                message.Status = true;
                message.Text = Global.StatusSave_1;
            }
            else if (actStatus == 2)
            {
                message.Status = false;
                message.Text = PlaceAtFixedLocationManage_cshtml.StatusSave_2;
            }
            else if (actStatus == 3)
            {
                message.Status = false;
                message.Text = PlaceAtFixedLocationManage_cshtml.StatusSave_3;
            }
            else
            {
                message.Status = false;
                message.Text = Global.StatusSave_0;
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult DeleFixLoc(int flId, int autIntNo)
        {
            Message message = new Message();
            if (flId > 0)
            {
                int actStatus = fixlocMana.DeleFixLocByID(flId);
                if (actStatus == 1)
                {
                    message.Status = true;
                    message.Text = Global.StatusDelete_1;
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, LastUser, PunchStatisticsTranTypeList.PlaceAtFixedLocationMaintenance, PunchAction.Delete);

                }
                else
                {
                    message.Status = false;
                    message.Text = Global.StatusDelete_0;
                }
            }
            return Json(message);
        }

    }
}
