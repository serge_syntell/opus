﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.StreetCoding;
using SIL.AARTO.BLL.StreetCoding.Model;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.Web.Helpers.Paginator;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.StreetCoding;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.StreetCoding
{
    public partial class StreetCodingController : Controller
    {
        //
        // GET: /PlaceBetween/
        PlaceBetweenManager pbMana = new PlaceBetweenManager();

        public ActionResult PlaceBetweenManage()
        {
            PlaceBetweenModel model = new PlaceBetweenModel();
            int pageIndex = 0;
            string searchKey = QueryString.GetString("s");

            if (!string.IsNullOrEmpty(QueryString.GetString("Page")))
            {
                pageIndex = Convert.ToInt32(QueryString.GetString("Page"));
            }
            if (!string.IsNullOrEmpty(QueryString.GetString("a")))
            {
                model.SearchAutIntNo = Convert.ToInt32(QueryString.GetString("a"));
            }
            if (!string.IsNullOrEmpty(QueryString.GetString("sb")))
            {
                model.SearchLoSuIntNo = Convert.ToInt32(QueryString.GetString("sb"));
            }
            if (!string.IsNullOrEmpty(QueryString.GetString("st1")))
            {
                model.SearchStreet1 = Convert.ToInt32(QueryString.GetString("st1"));
            }
            if (!string.IsNullOrEmpty(QueryString.GetString("st2")))
            {
                model.SearchStreet2 = Convert.ToInt32(QueryString.GetString("st2"));
            }
            if (!string.IsNullOrEmpty(QueryString.GetString("st3")))
            {
                model.SearchStreet3 = Convert.ToInt32(QueryString.GetString("st3"));
            }
            if (!string.IsNullOrEmpty(searchKey))
            {
                InitModel(model, pageIndex);
            }
            return View(model);
        }

        void InitModel(PlaceBetweenModel model, int pageIndex)
        {
            int totalCount = 0;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            ViewData["pageSize"] = pageSize;

            model.Betweens = pbMana.GetAllPB(model.SearchAutIntNo, model.SearchLoSuIntNo, model.SearchStreet1, model.SearchStreet2, model.SearchStreet3, pageIndex, pageSize, out totalCount);
            model.TotalCount = totalCount;
        }

        [HttpPost]
        public JsonResult GetBetwStSelectTip(int st1, int st2, int st3)
        {
            Message message = new Message();
            if (st1 > 0 && st2 > 0 && st3 > 0)
            {
                message.Text = PlaceBetweenManage_cshtml.StSelectTip;   
                message.Status = true;
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult GetPB(int pbId)
        {
            Message message = new Message();
            if (pbId != 0)
            {
                PlaceBetweenModel pb = pbMana.GetPbById(pbId);
                message.Text = pb.PBIntNo + "," + pb.AutIntNo + "," + pb.LoSuIntNo + "," + pb.CrtIntNo + "," + pb.PBStreet1 + "," + pb.PBStreet2 + "," + pb.PBStreet3 + "," + pb.PBGPSXCoord + "," + pb.PBGPSYCoord;
                message.Status = true;
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult SavePB(int pbId, int sub, int st1, int st2, int st3, string xCoord, string yCoord, int crtId, int st1Before, int st2Before, int st3Before, int autIntNo)
        {
            Message message = new Message();
            UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
            string lastUser = userLofinInfo.UserName;

            PlaceBetweenModel pbModel = new PlaceBetweenModel();
            pbModel.PBIntNo = pbId;
            pbModel.LoSuIntNo = sub;
            pbModel.PBStreet1 = st1;
            pbModel.PBStreet2 = st2;
            pbModel.PBStreet3 = st3;
            pbModel.PBGPSXCoord = xCoord;
            pbModel.PBGPSYCoord = yCoord;
            pbModel.CrtIntNo = crtId;
            pbModel.LastUser = lastUser;
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            pbModel.AutIntNo = autIntNo;

            int actStatus = pbMana.SavePB(pbModel, st1Before, st2Before, st3Before);
            if (actStatus == 1)
            {
                message.Status = true;
                message.Text = Global.StatusSave_1;
            }
            else if (actStatus == 2)
            {
                message.Status = false;
                message.Text = PlaceBetweenManage_cshtml.StatusSave_2;
            }
            else
            {
                message.Status = false;
                message.Text = Global.StatusSave_0;
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult DelePB(int pbId, int autIntNo)
        {
            Message message = new Message();
            if (pbId > 0)
            {
                int actStatus = pbMana.DeletePBByID(pbId);
                if (actStatus == 1)
                {
                    message.Status = true;
                    message.Text = Global.StatusDelete_1;
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, LastUser, PunchStatisticsTranTypeList.PlaceBetweenMaintenance, PunchAction.Delete);

                }
                else
                {
                    message.Status = false;
                    message.Text = Global.StatusDelete_0;
                }
            }
            return Json(message);
        }
    }
}
