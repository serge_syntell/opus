﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Resource.StreetCoding;
using SIL.AARTO.Web.ViewModels.StreetCoding;

namespace SIL.AARTO.Web.Controllers.StreetCoding
{
    [AARTOAuthorize]
    public partial class StreetCodingController : Controller
    {
        readonly AreaCodeService acService = new AreaCodeService();
        readonly AreaCodeGroupService acgService = new AreaCodeGroupService();
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

        public ActionResult AreaCodeGroupManage(int page = 0)
        {
            var model = new AreaCodeGroupManageModel
            {
                PageIndex = page
            };
            return AreaCodeGroupManage(model);
        }

        [HttpPost]
        public ActionResult AreaCodeGroupManage(AreaCodeGroupManageModel model)
        {
            model = model ?? new AreaCodeGroupManageModel();

            if (model.Action.HasValue)
            {
                switch (model.Action)
                {
                    case PageAction.Save:
                        model.EditorVisible = !Save(model);
                        break;

                    case PageAction.Delete:
                        model.EditorVisible = !Delete(model.EntityId.GetValueOrDefault(), model.EntityVersion);
                        break;
                }
            }

            FillList(model, model.PageIndex);
            Clear(model);
            return View("AreaCodeGroupManage", model);
        }

        [HttpPost]
        public JsonResult CheckCodeStart([Bind(Prefix = "Entity.CodeStart")] string areaCode)
        {
            return Json(CheckAreaCode(areaCode));
        }

        [HttpPost]
        public JsonResult CheckCodeEnd([Bind(Prefix = "Entity.CodeEnd")] string areaCode)
        {
            return Json(CheckAreaCode(areaCode));
        }

        bool CheckAreaCode(string areaCode)
        {
            return !string.IsNullOrWhiteSpace(areaCode)
                && this.acService.GetByAreaCode(areaCode) != null;
        }

        void FillList(AreaCodeGroupManageModel model, int pageIndex)
        {
            model = model ?? new AreaCodeGroupManageModel();
            model.EntityList.Clear();
            int totalCount, pageSize = Config.PageSize;
            const string orderBy = "GroupName, CodeStart, CodeEnd";
            var entityList = this.acgService.GetPaged("", orderBy, pageIndex, pageSize, out totalCount);
            var pageTotal = Math.Ceiling((double)totalCount/pageSize);
            if (pageIndex < 0 || pageIndex > pageTotal - 1)
            {
                if (pageIndex < 0)
                    entityList = this.acgService.GetPaged("", orderBy, 0, pageSize, out totalCount);
                if (pageIndex > pageTotal - 1)
                    entityList = this.acgService.GetPaged("", orderBy, (int)pageTotal - 1, pageSize, out totalCount);
            }
            entityList.ForEach(r => model.EntityList.Add(EntityToModel(r)));
            model.PageIndex = pageIndex;
            model.PageSize = pageSize;
            model.TotalCount = totalCount;
        }

        AreaCodeGroupEntity EntityToModel(IAreaCodeGroup entity)
        {
            if (entity == null) return null;
            return new AreaCodeGroupEntity
            {
                ACGIntNo = entity.AcgIntNo,
                GroupName = entity.GroupName,
                CodeStart = entity.CodeStart,
                CodeEnd = entity.CodeEnd,
                RowVersion = entity.RowVersion
            };
        }

        AreaCodeGroup ModelToEntity(AreaCodeGroupEntity model)
        {
            if (model == null) return null;
            var lastUser = ((UserLoginInfo)HttpContext.Session["UserLoginInfo"]).UserName;
            return new AreaCodeGroup
            {
                AcgIntNo = model.ACGIntNo,
                GroupName = model.GroupName,
                CodeStart = model.CodeStart,
                CodeEnd = model.CodeEnd,
                LastUser = lastUser,
                RowVersion = model.ACGIntNo > 0 ? model.RowVersion : new byte[8],
                EntityState = model.ACGIntNo > 0 ? EntityState.Changed : EntityState.Added
            };
        }

        bool Save(AreaCodeGroupManageModel model)
        {
            var result = true;
            model = model ?? new AreaCodeGroupManageModel();
            var action = model.Entity.ACGIntNo > 0 ? DBAction.Update : DBAction.Insert;
            Validation(model);
            if (ModelState.IsValid)
            {
                try
                {
                    if (int.Parse(model.Entity.CodeStart) > int.Parse(model.Entity.CodeEnd))
                    {
                        var tmp = model.Entity.CodeStart;
                        model.Entity.CodeStart = model.Entity.CodeEnd;
                        model.Entity.CodeEnd = tmp;
                    }
                    this.acgService.Save(ModelToEntity(model.Entity));
                    if (action == DBAction.Insert)
                    {
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.AreaCodeGroupManage, PunchAction.Add);  

                    }
                    if (action == DBAction.Update)
                    {
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.AreaCodeGroupManage, PunchAction.Change);  
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    ProcessingException(ex);
                }
            }
            if (result)
                model.Entity = new AreaCodeGroupEntity();
            FinalizeMessage(action);
            return result;
        }

        bool Delete(int id, byte[] rowVersion)
        {
            var result = true;
            ModelState.Clear();
            try
            {
                this.acgService.Delete(id, rowVersion);
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.AreaCodeGroupManage, PunchAction.Delete);  
            }
            catch (Exception ex)
            {
                result = false;
                ProcessingException(ex);
            }
            FinalizeMessage(DBAction.Delete);
            return result;
        }

        void ProcessingException(Exception ex)
        {
            var type = ex.GetType().Name;
            var message = ex.Message;

            if (ex is DBConcurrencyException)
                message = AreaCodeGroupManage_cshtml.TheCurrentRecordHasBeenModifiedBySomeoneElse;
            else if (ex is SqlException && ((SqlException)ex).Number == 2601)
                message = AreaCodeGroupManage_cshtml.ThisGroupWithAreaCodesIsAlreadyExisted;

            ModelState.AddModelError(type, message);
        }

        void Validation(AreaCodeGroupManageModel model)
        {
            AreaCodeGroup entity;
            if (string.IsNullOrWhiteSpace(model.Entity.GroupName))
                ModelState.AddModelError("PleaseEnterGroupName", AreaCodeGroupManage_cshtml.PleaseEnterGroupName);

            if (string.IsNullOrWhiteSpace(model.Entity.CodeStart))
                ModelState.AddModelError("PleaseEnterCodeStart", AreaCodeGroupManage_cshtml.PleaseEnterCodeStart);
            else if (string.IsNullOrWhiteSpace(model.Entity.CodeEnd))
                ModelState.AddModelError("PleaseEnterCodeEnd", AreaCodeGroupManage_cshtml.PleaseEnterCodeEnd);
            else if (this.acService.GetByAreaCode(model.Entity.CodeStart) == null
                || this.acService.GetByAreaCode(model.Entity.CodeEnd) == null)
                ModelState.AddModelError("PleaseEnterValidAreaCode", AreaCodeGroupManage_cshtml.PleaseEnterValidAreaCode);
            else if (!string.IsNullOrWhiteSpace(model.Entity.GroupName)
                && (entity = this.acgService.GetByGroupNameCodeStartCodeEnd(model.Entity.GroupName, model.Entity.CodeStart, model.Entity.CodeEnd)) != null
                && (model.Entity.ACGIntNo == 0
                    || model.Entity.ACGIntNo != entity.AcgIntNo))
                ModelState.AddModelError("ThisGroupWithAreaCodesIsAlreadyExisted", AreaCodeGroupManage_cshtml.ThisGroupWithAreaCodesIsAlreadyExisted);
        }

        void FinalizeMessage(DBAction action)
        {
            if (ModelState.IsValid)
            {
                string msg = null;
                switch (action)
                {
                    case DBAction.Insert:
                        msg = AreaCodeGroupManage_cshtml.CreateSuccessful;
                        break;
                    case DBAction.Update:
                        msg = AreaCodeGroupManage_cshtml.EditSuccessful;
                        break;
                    case DBAction.Delete:
                        msg = AreaCodeGroupManage_cshtml.DeleteSuccessful;
                        break;
                }
                ViewBag.Message = msg;
            }
            else
            {
                var sb = new StringBuilder();
                foreach (var error in ModelState.SelectMany(ms => ms.Value.Errors))
                {
                    sb.AppendFormat("{0}<br/>", error.ErrorMessage);
                }
                ViewBag.Error = sb.ToString();
            }
            ModelState.Clear();
        }

        void Clear(AreaCodeGroupManageModel model)
        {
            model = model ?? new AreaCodeGroupManageModel();
            model.EntityId = null;
            model.EntityVersion = null;
            model.Action = null;
            ModelState.Clear();
        }
    }
}
