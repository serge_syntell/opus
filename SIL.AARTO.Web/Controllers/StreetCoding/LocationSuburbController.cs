﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.StreetCoding;
using SIL.AARTO.BLL.StreetCoding.Model;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.Web.Helpers.Paginator;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.StreetCoding;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.StreetCoding
{
    public partial class StreetCodingController : Controller
    {
        //
        // GET: /LocationSuburb/
        LocationSuburbManager locSubMana = new LocationSuburbManager();

        public ActionResult LocationSuburbManage()
        {
            LocationSuburbModel model = new LocationSuburbModel();
            int pageIndex = 0;

            int autIntNo = Convert.ToInt32(QueryString.GetString("a"));
            string searchKey = QueryString.GetString("s");

            if (!string.IsNullOrEmpty(QueryString.GetString("Page")))
            {
                pageIndex = Convert.ToInt32(QueryString.GetString("Page"));
            }

            model.SearchKey = searchKey;
            model.AutIntNo = autIntNo;
            InitModel(model, pageIndex);
            return View(model);
        }

        [HttpPost]
        public JsonResult GetSubByAutIntNo(int autId)
        {
            List<LocationSuburb> subs = null;
            subs = locSubMana.GetSuburbsByAutIntNo(autId);
            List<SelectListItem> Items = new List<SelectListItem>();
            foreach (LocationSuburb sub in subs)
            {
                Items.Add(new SelectListItem { Value = sub.LoSuIntNo.ToString(), Text = sub.LoSuDescr });
            }
            return Json(Items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetLocSub(int loSuIntNo)
        {
            Message message = new Message();
            if (loSuIntNo > 0)
            {
                LocationSuburb locSub = locSubMana.GetSubBySubId(loSuIntNo);
                message.Text = locSub.AutIntNo + "," + locSub.LoSuDescr;
                message.Status = true;
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult DeleLocSub(int loSuIntNo, string lacCode, int autIntNo)
        {
            Message message = new Message();
            if (loSuIntNo > 0)
            {
                int actStatus = locSubMana.DeleteLocSubByLoSuIntNo(loSuIntNo, lacCode);
                if (actStatus == 1)
                {
                    message.Status = true;
                    message.Text = Global.StatusDelete_1;
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, LastUser, PunchStatisticsTranTypeList.SuburbMaintenance, PunchAction.Delete);

                }
                else if (actStatus == 2)
                {
                    message.Status = false;
                    message.Text = LocationSuburbManage_cshtml.StatusDelete_2;
                }
                else if (actStatus == 3)
                {
                    message.Status = false;
                    message.Text = LocationSuburbManage_cshtml.Status_ExistInPlaces;
                }
                else
                {
                    message.Status = false;
                    message.Text = Global.StatusDelete_0;
                }
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult SaveLocSub(int autIntNo, string lacCode, int loSuIntNo, string loSuDescr, int autIntNoBefore, string lacCodeBefore)
        {
            Message message = new Message();
            if (ModelState.IsValid)
            {
                UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
                string lastUser = userLofinInfo.UserName;

                lacCode = lacCode.Trim();
                loSuDescr = loSuDescr.Trim();

                LocationSuburbModel locSub = new LocationSuburbModel();
                locSub.AutIntNo = autIntNo;
                locSub.LACCode = lacCode;
                locSub.LoSuIntNo = loSuIntNo;
                locSub.LoSuDescr = loSuDescr;
                locSub.LastUser = lastUser;

                int actStatus = locSubMana.SaveLocSub(locSub, autIntNoBefore, lacCodeBefore);
                if (actStatus == 1)
                {
                    message.Status = true;
                }
                else
                {
                    message.Status = false;
                }
                string strStatus = string.Empty;
                if (actStatus == 0)
                {
                    strStatus = Global.StatusSave_0;
                }
                else if (actStatus == 1)
                {
                    strStatus = Global.StatusSave_1;
                }
                else if (actStatus == 2)
                {
                    strStatus = LocationSuburbManage_cshtml.StatusSave_2;
                }
                else if (actStatus == 3)
                {
                    strStatus = LocationSuburbManage_cshtml.StatusSave_3;
                }
                else if (actStatus == 4)
                {
                    strStatus = LocationSuburbManage_cshtml.Status_ExistInPlaces;
                }
                message.Text = strStatus;
            }
            return Json(message);
        }

        void InitModel(LocationSuburbModel model, int pageIndex)
        {
            int totalCount = 0;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            ViewData["pageSize"] = pageSize;

            TList<LocationAreaCode> lacList = locSubMana.GetAllLocAreaCodes();
            model.locAreaCodes = new SelectList(lacList as IEnumerable, "LACCode", "LACCode");
            if (model.SearchKey != null)
            {
                model.locSuburbs = locSubMana.GetAllLocSub(model.AutIntNo, model.SearchKey, pageIndex, pageSize, out totalCount);
                model.TotalCount = totalCount;
            }
        }

    }
}
