﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.StreetCoding;
using SIL.AARTO.BLL.StreetCoding.Model;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.Web.Helpers.Paginator;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.StreetCoding;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.StreetCoding
{
    public partial class StreetCodingController : Controller
    {
        // GET: /Street/
        StreetManager streetMana = new StreetManager();

        public ActionResult StreetManage()
        {
            StreetModel model = new StreetModel();
            int pageIndex = 0;

            if (!string.IsNullOrEmpty(QueryString.GetString("Page")))
            {
                pageIndex = Convert.ToInt32(QueryString.GetString("Page"));
            }
            if (!string.IsNullOrEmpty(QueryString.GetString("a")))
            {
                model.AutIntNo = Convert.ToInt32(QueryString.GetString("a"));
            }
            if (!string.IsNullOrEmpty(QueryString.GetString("sb")))
            {
                model.LoSuIntNo = Convert.ToInt32(QueryString.GetString("sb"));
            }

            model.SearchKey = QueryString.GetString("s");
            InitModel(model, pageIndex);
            return View(model);
        }

        [HttpPost]
        public JsonResult GetStreet(int subId, int stId)
        {
            Message message = new Message();
            if (stId != 0)
            {
                StreetModel st = streetMana.GetStreetByStId(subId, stId);
                message.Text =  st.AutIntNo + "," + st.LoSuIntNo + "," + st.StrName;
                message.Status = true;
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult GetStByAutLoSu(int autId, int subId)
        {
            TList<Street> sts = streetMana.GetStreetsByAutIntNoLoSuIntNo(autId, subId);
            List<SelectListItem> Items = new List<SelectListItem>();
            foreach (Street st in sts)
            {
                Items.Add(new SelectListItem { Value = st.StrIntNo.ToString(), Text = st.StrName.ToString() });
            }
            return Json(Items);
        }

        [HttpPost]
        public JsonResult SaveStreet(int autIntNo, int subIntNo, int stIntNo, string stName, int autIntNoBefore, int subIntNoBefore)
        {
            Message message = new Message();
            if (ModelState.IsValid)
            {
                UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
                string lastUser = userLofinInfo.UserName;

                StreetModel model = new StreetModel();
                model.AutIntNo = autIntNo;
                model.LoSuIntNo = subIntNo;
                model.StrIntNo = stIntNo;
                model.StrName = stName;
                model.LastUser = lastUser;

                int actStatus = streetMana.SaveStreet(model, autIntNoBefore, subIntNoBefore); 
                string strStatus = string.Empty;
                if (actStatus == 0)
                {
                    message.Status = false;
                    strStatus = Global.StatusSave_0;
                }
                else if (actStatus == 1)
                {
                    message.Status = true;
                    strStatus = Global.StatusSave_1;
                }
                else if (actStatus == 2)
                {
                    message.Status = false;
                    strStatus = StreetManage_cshtml.StatusSave_2;
                }
                else if (actStatus == 3)
                {
                    message.Status = false;
                    strStatus = StreetManage_cshtml.Status_ExistInPlaces;
                }
                message.Text = strStatus;
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult DeleSt(int loSuIntNo, int strIntNo, int autIntNo)
        {
            Message message = new Message();
            if (strIntNo > 0)
            {
                int actStatus = streetMana.DeleteStreetByStrIntNo(loSuIntNo, strIntNo);
                if (actStatus == 1)
                {
                    message.Status = true;
                    message.Text = Global.StatusDelete_1;
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, LastUser, PunchStatisticsTranTypeList.StreetMaintenance, PunchAction.Delete);

                }
                else if (actStatus == 3)
                {
                    message.Status = false;
                    message.Text = StreetManage_cshtml.Status_ExistInPlaces;
                }
                else
                {
                    message.Status = false;
                    message.Text = Global.StatusDelete_0;
                }
            }
            return Json(message);
        }

        void InitModel(StreetModel model, int pageIndex)
        {
            int totalCount = 0;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            ViewData["pageSize"] = pageSize;

            if (model.SearchKey != null)
            {
                model.Streets = streetMana.GetAllStreet(model.AutIntNo, model.LoSuIntNo, model.SearchKey, pageIndex, pageSize, out totalCount);
                model.TotalCount = totalCount;
            }
        }

    }
}
