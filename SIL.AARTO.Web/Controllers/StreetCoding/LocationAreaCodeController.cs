﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.StreetCoding;
using SIL.AARTO.BLL.StreetCoding.Model;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.Web.Helpers.Paginator;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.StreetCoding;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.StreetCoding
{
    public partial class StreetCodingController : Controller
    {
        //
        // GET: /LocationAreaCode/
        LocationAreaCodeManager lacMana = new LocationAreaCodeManager();

        public ActionResult LocationAreaCodeManage()
        {
            LocationAreaCodeModel model = new LocationAreaCodeModel();
            int pageIndex = 0;
            string searchKey = QueryString.GetString("s");

            if (!string.IsNullOrEmpty(QueryString.GetString("Page")))
            {
                pageIndex = Convert.ToInt32(QueryString.GetString("Page"));
            }
            if (searchKey != null)
            {
                model.SearchKey = searchKey;
                InitModel(model, pageIndex);
            }
            return View(model);
        }

        [HttpPost]
        public JsonResult GetLAC(string lacCode)
        {
            Message message = new Message();
            if (!string.IsNullOrEmpty(lacCode))
            {
                LocationAreaCode lac = lacMana.GetLocAreaCodeByLacCode(lacCode);
                message.Text = lac.LacDescr;
                message.Status = true;
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult DeleLAC(string lacCode)
        {
            Message message = new Message();
            if (!string.IsNullOrEmpty(lacCode))
            {
                int actStatus = lacMana.DeleteLocAreaCodeByLacCode(lacCode);
                if (actStatus == 1)
                {
                    message.Status = true;
                    message.Text = Global.StatusDelete_1;
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.AreaCodeMaintenance, PunchAction.Delete);  

                }
                else if (actStatus == 2)
                {
                    message.Status = false;
                    message.Text = LocationAreaCodeManage_cshtml.StatusDelete_2;
                }
                else
                {
                    message.Status = false;
                    message.Text = Global.StatusDelete_0;
                }
            }
            return Json(message);
        }

        [HttpPost]
        public JsonResult SaveLAC(string lacCodeBefore, string lacCodeNow, string lacDescr)
        {
            Message message = new Message();
            if (ModelState.IsValid)
            {
                UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"];
                string lastUser = userLofinInfo.UserName;

                lacCodeBefore = lacCodeBefore.Trim();
                lacCodeNow = lacCodeNow.Trim();

                LocationAreaCodeModel model = new LocationAreaCodeModel();
                model.LACCode = lacCodeNow;
                model.LACDescr = lacDescr;
                model.LastUser = lastUser;
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                model.AutIntNo = AutIntNo;

                int actStatus = lacMana.SaveLAC(model, lacCodeBefore);
                if (actStatus == 1)
                {
                    message.Status = true;
                }
                else
                {
                    message.Status = false;
                }
                string strStatus = string.Empty;
                if (actStatus == 0)
                {
                    strStatus = Global.StatusSave_0;
                }
                else if (actStatus == 1)
                {
                    strStatus = Global.StatusSave_1;
                }
                else if (actStatus == 2)
                {
                    strStatus = LocationAreaCodeManage_cshtml.StatusSave_2;
                }
                else if (actStatus == 3)
                {
                    strStatus = LocationAreaCodeManage_cshtml.StatusSave_3;
                }
                message.Text = strStatus;
            }
            return Json(message);
        }

        void InitModel(LocationAreaCodeModel model, int pageIndex)
        {
            int totalCount = 0;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            ViewData["pageSize"] = pageSize;

            model.locAreaCodes = lacMana.GetAllAreaCode(model.SearchKey, pageIndex, pageSize, out totalCount);
            model.TotalCount = totalCount;
        }


    }
}
