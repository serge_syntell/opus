﻿using Stalberg.TMS;
using System.Web.Services;
using System.Web.Services.Protocols;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using SIL.AARTO.BLL.Utility;
using System.IO;
using ceTe.DynamicPDF.Imaging;
using System;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Web;
namespace SIL.AARTO.Web.Controllers.QuoteManagement
{
    public partial class QuoteManagementController : Controller
    {
        SIL.AARTO.BLL.Utility.UserLoginInfo userDetails = new BLL.Utility.UserLoginInfo();
        string login = "";
        int autIntNo = 0;
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        string autName = "";

        public ActionResult ViewReport_PaidDifferentToQuote(string BeginDate = "", string EndDate = "",string ReferenceNumber="")
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            login = userDetails.UserName;
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            AuthorityService autService = new AuthorityService();
            SIL.AARTO.DAL.Entities.Authority autEntity = autService.GetByAutIntNo(autIntNo);
            if (autEntity != null)
            {
                autName = autEntity.AutName;
            }

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            DateTime beginDate = DateTime.Now.Date.AddDays(-30);
            DateTime endDate = DateTime.Now.Date.AddDays(1).AddSeconds(-1);
            if (!string.IsNullOrEmpty(BeginDate))
            {
                beginDate = Convert.ToDateTime(BeginDate);
            }
            if (!string.IsNullOrEmpty(EndDate))
            {
                endDate = Convert.ToDateTime(EndDate).AddDays(1).AddSeconds(-1);
            }

            ReferenceNumber = string.IsNullOrEmpty(ReferenceNumber) ? "" : HttpUtility.UrlDecode(ReferenceNumber).Trim();
            

            var path = HttpContext.Request.PhysicalApplicationPath + @"\Reports\PaidDifferentToQuote.dplx";
            DocumentLayout doc = new DocumentLayout(path);

            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl_AutName = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAutName");
                lbl_AutName.LayingOut += new LayingOutEventHandler(lbl_AutName2);

                StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
                query.ConnectionString = connectionString;
                ParameterDictionary parameters = new ParameterDictionary();
                parameters.Add("QuHeReferenceNumber", ReferenceNumber);
                parameters.Add("BeginDate", beginDate);
                parameters.Add("EndDate", endDate);
                parameters.Add("AutIntNo", autIntNo);
                Document report = doc.Run(parameters);
                byte[] buffer = report.Draw();
                return File(report.Draw(), "application/pdf");

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                Response.End();
                return View();
            }
        }

        public void lbl_AutName2(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRN = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;

                lblRN.Text = autName;

            }
            catch { }
        }

    }
}
