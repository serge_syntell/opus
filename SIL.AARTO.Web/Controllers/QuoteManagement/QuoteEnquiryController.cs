﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.NoticeProcess;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Helpers.Paginator;
using SIL.AARTO.Web.Resource.QuoteManagement;
using SIL.AARTO.Web.ViewModels.QuoteManagement;
using Stalberg.TMS;
using Stalberg.TMS.Data;

namespace SIL.AARTO.Web.Controllers.QuoteManagement
{
    [Authorize]
    public partial class QuoteManagementController : Controller
    {
        #region Variable

        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }

        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }

        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
        readonly string cultureName = Thread.CurrentThread.CurrentCulture.Name;

        #endregion

        public ActionResult QuoteEnquiryList(int page = 0, string QuoteReferenceNumber = "", string UserName = "", string PaymentReceiptNumber = "", string ChargeCode = "", string BeginDate = "", string EndDate = "")
        {
            UserLoginInfo userInfo = Session["UserLoginInfo"] as UserLoginInfo;
            if (userInfo == null)
            {
                return RedirectToAction("login", "account");
            }

            int autIntNo = Convert.ToInt32(Session["autIntNo"]);

            QuoteEnquiryManagementModel model = InitialQuoteEnqiuryModel(page, QuoteReferenceNumber, UserName, PaymentReceiptNumber, ChargeCode, BeginDate, EndDate);

            #region Jake comment and move below code into InitialQuoteEnquiryModel function
            /* Jake comment and move below code into InitialQuoteEnquiryModel function
            QuoteReferenceNumber = string.IsNullOrEmpty(QuoteReferenceNumber) ? string.Empty : HttpUtility.UrlDecode(QuoteReferenceNumber).Trim();
            UserName = string.IsNullOrEmpty(UserName) ? string.Empty : HttpUtility.UrlDecode(UserName).Trim();
            PaymentReceiptNumber = string.IsNullOrEmpty(PaymentReceiptNumber) ? string.Empty : HttpUtility.UrlDecode(PaymentReceiptNumber).Trim();
            ChargeCode = string.IsNullOrEmpty(ChargeCode) ? string.Empty : HttpUtility.UrlDecode(ChargeCode).Trim();

            DateTime beginDate = DateTime.Now.Date.AddDays(-30);
            DateTime endDate = DateTime.Now.Date.AddDays(1).AddSeconds(-1);
            if (BeginDate != "")
            {
                beginDate = Convert.ToDateTime(BeginDate);
            }
            if (EndDate != "")
            {
                endDate = Convert.ToDateTime(EndDate).AddDays(1).AddSeconds(-1);
            }

            int totalCount;
            model.PageSize = Config.PageSize;
            QuoteDB db = new QuoteDB(connectionString);
            DataSet ds = db.GetQuoteList(autIntNo, ChargeCode, UserName, QuoteReferenceNumber, PaymentReceiptNumber, beginDate, endDate, page + 1, model.PageSize, out totalCount);

            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            #region Quote Header

                            QuoteEnquiryModel item = new QuoteEnquiryModel();
                            item.QuHeIntNo = Convert.ToString(dt.Rows[i]["QuHeIntNo"]);
                            item.QuHeReferenceNumber = dt.Rows[i]["QuHeReferenceNumber"] == null ? "" : Convert.ToString(dt.Rows[i]["QuHeReferenceNumber"]);
                            item.QuHeSurName = dt.Rows[i]["QuHeSurName"] == null ? "" : Convert.ToString(dt.Rows[i]["QuHeSurName"]);
                            item.QuHeInitials = dt.Rows[i]["QuHeInitials"] == null ? "" : Convert.ToString(dt.Rows[i]["QuHeInitials"]);
                            item.QuHeIDNumber = dt.Rows[i]["QuHeIDNumber"] == null ? "" : Convert.ToString(dt.Rows[i]["QuHeIDNumber"]);
                            item.QuHeDate = Convert.ToString(dt.Rows[i]["QuHeDate"]) == "" ? "" : Convert.ToDateTime(dt.Rows[i]["QuHeDate"]).ToString("yyyy/MM/dd");
                            item.QuHeAddress1 = Convert.ToString(dt.Rows[i]["QuHeAddress1"]);
                            item.QuHeAddress2 = Convert.ToString(dt.Rows[i]["QuHeAddress2"]);
                            item.QuHeAddress3 = Convert.ToString(dt.Rows[i]["QuHeAddress3"]);
                            item.QuHeAddress4 = Convert.ToString(dt.Rows[i]["QuHeAddress4"]);
                            item.QuHeAddress5 = Convert.ToString(dt.Rows[i]["QuHeAddress5"]);
                            item.ReceiptNo = Convert.ToString(dt.Rows[i]["RctNumber"]);
                            item.ReceiptAmount = double.Parse(dt.Rows[i]["Total"].ToString()).ToString("f2");
                            item.RctIntNo = Convert.ToInt32(dt.Rows[i]["RctIntNo"]);
                            item.Reversed = Convert.ToBoolean(dt.Rows[i]["Reversed"]);
                            model.QuoteEnquiryList.Add(item);

                            #endregion

                            #region Quote Detail

                            if (ds.Tables.Count > 1)
                            {
                                DataRow[] drQuoteDetails = ds.Tables[1].Select("QuHeIntNo=" + item.QuHeIntNo);
                                if (drQuoteDetails != null && drQuoteDetails.Length > 0)
                                {
                                    for (int j = 0; j < drQuoteDetails.Length; j++)
                                    {
                                        QuoteDetailModel itemDetial = new QuoteDetailModel();
                                        itemDetial.QuDeIntNo = Convert.ToString(drQuoteDetails[j]["QuDeIntNo"]);
                                        itemDetial.NTCCode = Convert.ToString(drQuoteDetails[j]["NTCCode"]);
                                        itemDetial.NTCDescr = Convert.ToString(drQuoteDetails[j]["NTCDescr"]);
                                        itemDetial.NTCComment = Convert.ToString(drQuoteDetails[j]["NTCComment"]);
                                        itemDetial.QuDeChargeQuantity = Convert.ToString(drQuoteDetails[j]["QuDeChargeQuantity"]);
                                        itemDetial.MeasureUnit = Convert.ToString(drQuoteDetails[j]["MeasureUnit"]);
                                        itemDetial.NTCStandardAmount = double.Parse(drQuoteDetails[j]["NTCStandardAmount"].ToString()).ToString("f2");
                                        itemDetial.QuDeActualAmount = double.Parse(drQuoteDetails[j]["QuDeActualAmount"].ToString()).ToString("f2");
                                        itemDetial.QuDeGrossAmount = double.Parse(drQuoteDetails[j]["QuDeGrossAmount"].ToString()).ToString("f2");
                                        item.QuoteDetailList.Add(itemDetial);
                                    }
                                }
                            }

                            #endregion

                            #region Receipt Detail

                            if (ds.Tables.Count > 2)
                            {
                                DataRow[] drReceiptDetails = ds.Tables[2].Select("QuHeIntNo=" + item.QuHeIntNo);
                                if (drReceiptDetails != null && drReceiptDetails.Length > 0)
                                {
                                    for (int x = 0; x < drReceiptDetails.Length; x++)
                                    {
                                        ReceiptDetailModel itemDetial = new ReceiptDetailModel();
                                        itemDetial.RTIntNo = Convert.ToString(drReceiptDetails[x]["RTIntNo"]);
                                        itemDetial.RTAmount = double.Parse(drReceiptDetails[x]["RTAmount"].ToString()).ToString("f2");
                                        itemDetial.RTGrossAmount = Convert.ToDouble(drReceiptDetails[x]["RTAmount"]);
                                        itemDetial.RTFineAmount = double.Parse(drReceiptDetails[x]["RTFineAmount"].ToString()).ToString("f2");
                                        itemDetial.RTCashType = GetCashTypeName(Convert.ToString(drReceiptDetails[x]["RTCashType"]));
                                        itemDetial.RctNumber = Convert.ToString(drReceiptDetails[x]["RctNumber"]);
                                        itemDetial.RTDetails = GetRTDetails(Convert.ToString(drReceiptDetails[x]["RTCashType"]), drReceiptDetails[x]);
                                        item.ReceiptDetailList.Add(itemDetial);
                                    }
                                }
                            }

                            #endregion
                        }
                    }
                    else
                    {
                        model.ErrorMsg = QuoteEnquiryListResx.lblErrorText2;
                    }

                }
            }

            model.TotalCount = Convert.ToInt32(totalCount);
            */
            #endregion
            //jake 2015-02-03 added
            model.ShowReversalNTCButton = userInfo.UserRoles.Contains((int)AartoUserRoleList.ReversalOfNTC) && model.TotalCount > 0;

            return View(model);
        }

        [HttpGet]
        public ActionResult CashBox(int page = 0)
        {
            CashBoxSelectModel model = BindCashBoxSelectModel(page);
            //if (!String.IsNullOrEmpty(rctNumber))
            //    Session["RctNumber"] = rctNumber;
            return View(model);
        }

        QuoteEnquiryManagementModel InitialQuoteEnqiuryModel(int page = 0, string QuoteReferenceNumber = "", string UserName = "", string PaymentReceiptNumber = "", string ChargeCode = "", string BeginDate = "", string EndDate = "")
        {
            QuoteEnquiryManagementModel model = new ViewModels.QuoteManagement.QuoteEnquiryManagementModel();
            QuoteReferenceNumber = string.IsNullOrEmpty(QuoteReferenceNumber) ? string.Empty : HttpUtility.UrlDecode(QuoteReferenceNumber).Trim();
            UserName = string.IsNullOrEmpty(UserName) ? string.Empty : HttpUtility.UrlDecode(UserName).Trim();
            PaymentReceiptNumber = string.IsNullOrEmpty(PaymentReceiptNumber) ? string.Empty : HttpUtility.UrlDecode(PaymentReceiptNumber).Trim();
            ChargeCode = string.IsNullOrEmpty(ChargeCode) ? string.Empty : HttpUtility.UrlDecode(ChargeCode).Trim();

            DateTime beginDate = DateTime.Now.Date.AddDays(-30);
            DateTime endDate = DateTime.Now.Date.AddDays(1).AddSeconds(-1);
            if (BeginDate != "")
            {
                beginDate = Convert.ToDateTime(BeginDate);
            }
            if (EndDate != "")
            {
                endDate = Convert.ToDateTime(EndDate).AddDays(1).AddSeconds(-1);
            }

            int totalCount;
            model.PageSize = Config.PageSize;
            QuoteDB db = new QuoteDB(connectionString);
            DataSet ds = db.GetQuoteList(autIntNo, ChargeCode, UserName, QuoteReferenceNumber, PaymentReceiptNumber, beginDate, endDate, page + 1, model.PageSize, out totalCount);

            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            #region Quote Header

                            QuoteEnquiryModel item = new QuoteEnquiryModel();
                            item.QuHeIntNo = Convert.ToString(dt.Rows[i]["QuHeIntNo"]);
                            item.QuHeReferenceNumber = dt.Rows[i]["QuHeReferenceNumber"] == null ? "" : Convert.ToString(dt.Rows[i]["QuHeReferenceNumber"]);
                            item.QuHeSurName = dt.Rows[i]["QuHeSurName"] == null ? "" : Convert.ToString(dt.Rows[i]["QuHeSurName"]);
                            item.QuHeInitials = dt.Rows[i]["QuHeInitials"] == null ? "" : Convert.ToString(dt.Rows[i]["QuHeInitials"]);
                            item.QuHeIDNumber = dt.Rows[i]["QuHeIDNumber"] == null ? "" : Convert.ToString(dt.Rows[i]["QuHeIDNumber"]);
                            item.QuHeDate = Convert.ToString(dt.Rows[i]["QuHeDate"]) == "" ? "" : Convert.ToDateTime(dt.Rows[i]["QuHeDate"]).ToString("yyyy/MM/dd");
                            item.QuHeAddress1 = Convert.ToString(dt.Rows[i]["QuHeAddress1"]);
                            item.QuHeAddress2 = Convert.ToString(dt.Rows[i]["QuHeAddress2"]);
                            item.QuHeAddress3 = Convert.ToString(dt.Rows[i]["QuHeAddress3"]);
                            item.QuHeAddress4 = Convert.ToString(dt.Rows[i]["QuHeAddress4"]);
                            item.QuHeAddress5 = Convert.ToString(dt.Rows[i]["QuHeAddress5"]);
                            item.ReceiptNo = Convert.ToString(dt.Rows[i]["RctNumber"]);
                            item.ReceiptAmount = double.Parse(dt.Rows[i]["Total"].ToString()).ToString("f2");
                            item.RctIntNo = Convert.ToInt32(dt.Rows[i]["RctIntNo"]);
                            item.Reversed = Convert.ToBoolean(dt.Rows[i]["Reversed"]);
                            model.QuoteEnquiryList.Add(item);

                            #endregion

                            #region Quote Detail

                            if (ds.Tables.Count > 1)
                            {
                                DataRow[] drQuoteDetails = ds.Tables[1].Select(String.Format("QuHeIntNo={0} AND RctIntNo={1}", item.QuHeIntNo, item.RctIntNo));
                                if (drQuoteDetails != null && drQuoteDetails.Length > 0)
                                {
                                    for (int j = 0; j < drQuoteDetails.Length; j++)
                                    {
                                        QuoteDetailModel itemDetial = new QuoteDetailModel();
                                        itemDetial.QuDeIntNo = Convert.ToString(drQuoteDetails[j]["QuDeIntNo"]);
                                        itemDetial.NTCCode = Convert.ToString(drQuoteDetails[j]["NTCCode"]);
                                        itemDetial.NTCDescr = Convert.ToString(drQuoteDetails[j]["NTCDescr"]);
                                        itemDetial.NTCComment = Convert.ToString(drQuoteDetails[j]["NTCComment"]);
                                        itemDetial.QuDeChargeQuantity = Convert.ToString(drQuoteDetails[j]["QuDeChargeQuantity"]);
                                        itemDetial.MeasureUnit = Convert.ToString(drQuoteDetails[j]["MeasureUnit"]);
                                        itemDetial.NTCStandardAmount = double.Parse(drQuoteDetails[j]["NTCStandardAmount"].ToString()).ToString("f2");
                                        itemDetial.QuDeActualAmount = double.Parse(drQuoteDetails[j]["QuDeActualAmount"].ToString()).ToString("f2");
                                        itemDetial.QuDeGrossAmount = double.Parse(drQuoteDetails[j]["QuDeGrossAmount"].ToString()).ToString("f2");
                                        item.QuoteDetailList.Add(itemDetial);
                                    }
                                }
                            }

                            #endregion

                            #region Receipt Detail

                            if (ds.Tables.Count > 2)
                            {
                                DataRow[] drReceiptDetails = ds.Tables[2].Select(String.Format("QuHeIntNo={0} AND RctIntNo={1}", item.QuHeIntNo, item.RctIntNo));
                                if (drReceiptDetails != null && drReceiptDetails.Length > 0)
                                {
                                    for (int x = 0; x < drReceiptDetails.Length; x++)
                                    {
                                        ReceiptDetailModel itemDetial = new ReceiptDetailModel();
                                        itemDetial.RTIntNo = Convert.ToString(drReceiptDetails[x]["RTIntNo"]);
                                        itemDetial.RTAmount = double.Parse(drReceiptDetails[x]["RTAmount"].ToString()).ToString("f2");
                                        itemDetial.RTGrossAmount = Convert.ToDouble(drReceiptDetails[x]["RTAmount"]);
                                        itemDetial.RTFineAmount = double.Parse(drReceiptDetails[x]["RTFineAmount"].ToString()).ToString("f2");
                                        itemDetial.RTCashType = GetCashTypeName(Convert.ToString(drReceiptDetails[x]["RTCashType"]));
                                        itemDetial.RctNumber = Convert.ToString(drReceiptDetails[x]["RctNumber"]);
                                        itemDetial.RTDetails = GetRTDetails(Convert.ToString(drReceiptDetails[x]["RTCashType"]), drReceiptDetails[x]);
                                        item.ReceiptDetailList.Add(itemDetial);
                                    }
                                }
                            }

                            #endregion
                        }
                    }
                    else
                    {
                        model.ErrorMsg = QuoteEnquiryListResx.lblErrorText2;
                    }

                }
            }

            model.TotalCount = Convert.ToInt32(totalCount);

            return model;
        }

        CashBoxSelectModel BindCashBoxSelectModel(int pageIndex)
        {
            //Config.PageSize
            CashBoxSelectModel model = new CashBoxSelectModel() { Page = pageIndex + 1, PageSize = 5 };
            CashboxDB db = new CashboxDB(connectionString);
            int totalCount = 0;
            DataSet ds = db.GetCashboxListDS(this.autIntNo, model.PageSize, model.Page, out totalCount);
            model.TotalCount = totalCount;

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                model.CashBox.Add(new CashBoxEntity()
                {
                    CBIntNo = Convert.ToInt32(dr["CBIntNo"]),
                    CBName = dr["CBName"].ToString(),
                    CBStartAmount = Convert.ToDouble(dr["CBStartAmount"])
                });
            }

            return model;
        }

        public string GetRTDetails(string ReceiptType, DataRow dr)
        {
            var descr = "";
            switch (ReceiptType)
            {
                case "[":
                    descr = "";
                    break;
                case "]":
                    if (!string.IsNullOrEmpty(Convert.ToString(dr["ChqDate"])))
                    {
                        descr = "Cheque Number: " + Convert.ToString(dr["ChqNo"]) + "<br />Cheque Drawer: " + Convert.ToString(dr["ChqPayee"]) + "<br />Cheque Bank: " + Convert.ToString(dr["ChqBank"]);
                        descr += "<br />Cheque Date: " + Convert.ToDateTime(Convert.ToString(dr["ChqDate"])).ToString("yyyy/MM/dd");
                    }
                    break;
                case ")":
                    if (!string.IsNullOrEmpty(Convert.ToString(dr["CardExpiryDate"])))
                    {
                        descr = "Card Issuer: " + Convert.ToString(dr["CardIssuer"]) + "<br />Name on Card: " + Convert.ToString(dr["CardHolder"]);
                        descr += "<br />Card Expiry: " + Convert.ToDateTime(Convert.ToString(dr["CardExpiryDate"])).ToString("yyyy/MM/dd");
                    }
                    break;
                case "{":
                    if (!string.IsNullOrEmpty(Convert.ToString(dr["DebitCardExpiryDate"])))
                    {
                        descr = "Card Issuer: " + Convert.ToString(dr["DebitCardIssuer"]) + "<br />Name on Card: " + Convert.ToString(dr["DebitCardHolder"]);
                        descr += "<br />Card Expiry: " + Convert.ToDateTime(Convert.ToString(dr["DebitCardExpiryDate"])).ToString("yyyy/MM/dd");
                    }
                    break;
                case "(":
                    descr = "";//"Description:" + Reference;
                    break;
                case "}":
                    if (!string.IsNullOrEmpty(Convert.ToString(dr["PostalOrderNo"])))
                        descr = "Postal Order Number: " + Convert.ToString(dr["PostalOrderNo"]);
                    break;

            }

            return descr;
        }

        public string GetCashTypeName(string CashType)
        {
            string cashTypeName = "";
            switch (CashType)
            {
                case "[":
                    cashTypeName = QuoteEnquiryListResx.lblPaymentTypeItems0;//Cash
                    break;
                case "]":
                    cashTypeName = QuoteEnquiryListResx.lblPaymentTypeItems1;//Cheque
                    break;
                case ")":
                    cashTypeName = QuoteEnquiryListResx.lblPaymentTypeItems2;//Credit Card
                    break;
                case "{":
                    cashTypeName = QuoteEnquiryListResx.lblPaymentTypeItems3;//Debit Card
                    break;
                case "}":
                    cashTypeName = QuoteEnquiryListResx.lblPaymentTypeItems4;//Postal Order
                    break;
                case "(":
                    cashTypeName = QuoteEnquiryListResx.lblPaymentTypeItems5;//Bank Payment
                    break;
                case "*":
                    cashTypeName = QuoteEnquiryListResx.lblPaymentTypeItems7;//Other
                    break;

            }

            return cashTypeName;
        }

    }
}
