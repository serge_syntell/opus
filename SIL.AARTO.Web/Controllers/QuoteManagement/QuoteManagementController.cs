﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.ViewModels.QuoteManagement;

namespace SIL.AARTO.Web.Controllers.QuoteManagement
{
    [AARTOErrorLog]
    public partial class QuoteManagementController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AdminReversalForNTC(int page = 0, string QuoteReferenceNumber = "",
            string UserName = "", string PaymentReceiptNumber = "", string ChargeCode = "",
            string BeginDate = "", string EndDate = "")
        {
            UserLoginInfo userInfo = Session["UserLoginInfo"] as UserLoginInfo;
            if (userInfo == null)
            {
                return RedirectToAction("login", "account");
            }
            if (!userInfo.UserRoles.Contains((int)SIL.AARTO.DAL.Entities.AartoUserRoleList.AdminReversalForNTC))
            {
                Response.Write(SIL.AARTO.Web.Resource.QuoteManagement.AdminReversalForNTC.msg_NoPermissionAccess);
                Response.End();
                return View();
            }

            QuoteEnquiryManagementModel model = InitialQuoteEnqiuryModel(page, QuoteReferenceNumber, UserName, PaymentReceiptNumber, ChargeCode, BeginDate, EndDate);

            return View(model);

            //return RedirectToAction("QuoteEnquiryList", new
            //{
            //    page = page,
            //    QuoteReferenceNumber = QuoteReferenceNumber,
            //    UserName = UserName,
            //    PaymentReceiptNumber = PaymentReceiptNumber,
            //    BeginDate = BeginDate,
            //    EndDate = EndDate
            //});
        }
    }
}
