using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using SIL.AARTO.BLL.OfficerError.Model;
using SIL.AARTO.BLL.Utility.AartoDocType;
using System.Collections;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.Web.Controllers.OfficerErrors
{
    [AARTOErrorLog, LanguageFilter]
    public partial class ViewOfficerErrorsController : Controller
    {
        //
        // GET: /ViewOfficerErrors/

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult ViewOfficerErrors()
        {
            ViewOfficerErrorsModel viewOfficerErrorsModel = new ViewOfficerErrorsModel();

            InitModel(viewOfficerErrorsModel);

            return View(viewOfficerErrorsModel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult ViewOfficerErrors(FormCollection formCollection)
        {
            ViewOfficerErrorsModel viewOfficerErrorsModel = new ViewOfficerErrorsModel();
            UpdateModel<ViewOfficerErrorsModel>(viewOfficerErrorsModel);
            InitModel(viewOfficerErrorsModel);
            if (!viewOfficerErrorsModel.IsValid)
            {
                ModelState.AddModelLocalizationError(this, viewOfficerErrorsModel.GetRuleViolations());
            }
            else
            {
                viewOfficerErrorsModel.IsInSession = true;

                Session["ViewOfficerErrorsModel"] = viewOfficerErrorsModel;

                //return Redirect("/ViewOfficerErrorsReport.aspx");
            }
            return View(viewOfficerErrorsModel);
        }

        private void InitModel(ViewOfficerErrorsModel viewOfficerErrorsModel)
        {
            List<AartoDocTypeEntity> AatoHandWrittenDocTypeList =
                AartoDocumentTypeManager.GetAatoHandWrittenDocTypeList();

            viewOfficerErrorsModel.DocTypeList = new SelectList(AatoHandWrittenDocTypeList as IEnumerable<AartoDocTypeEntity>,
                "AaDocTypeID", "AaDocTypeName", viewOfficerErrorsModel.AaDocTypeID);

            List<TrafficOfficerEntity> trafficOfficerList = new UserManager().GetAllTrafficOffericer();
            foreach (TrafficOfficerEntity trafficOfficer in trafficOfficerList)
            {
                if (trafficOfficer.ToNo == "0")
                {
                    trafficOfficer.ToDesc = GetResourcesInClassLibraries.GetResourcesValueByCustomPath(@"Views\ViewOfficerErrors\App_LocalResources\ViewOfficerErrors.aspx", "ddlOfficer.Item");
                    break;
                }
            }

            viewOfficerErrorsModel.OfficerList = new SelectList(trafficOfficerList as IEnumerable<TrafficOfficerEntity>,
                "ToNo", "ToDesc", viewOfficerErrorsModel.OfficerNo);
            viewOfficerErrorsModel.IsInSession = false;

        }
    }
}
