﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels;
using SIL.AARTO.BLL;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.AARTONoticeClock;
using SIL.AARTO.Web.ViewModels.ScannedDigitaldocument;
using System.Web.Configuration;
using System.IO;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.ScannedDigitaldocument;
using System.Net;
using System.Drawing;

namespace SIL.AARTO.Web.Controllers.ScannedDigitaldocument
{
    public class ScannedDigitaldocumentController : Controller
    {
        private DigitaldocumentManager noticeManage = new DigitaldocumentManager();
        
       
        public ActionResult ScannedDigitaldocumentList()
        {
            Int32 record_count=0,page_index=0,page_size=10,autIntNo=0;
            if (!Int32.TryParse((Session["autIntNo"]??string.Empty).ToString(), out autIntNo))
            {
                return View(new List<NoticeModel>());
            }

            Int32.TryParse(Request["page"],out page_index);
            if(!Int32.TryParse(Request["pagesize"],out page_size)
                &&!Int32.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["PageSize"],out page_size))
            {
               page_size=10;
            }

            var noticeList = noticeManage.GetNoticeList(new Notice { AutIntNo = autIntNo, NotTicketNo = Request.QueryString["SearchStr"] }, page_index, page_size, out record_count);
            OffenceType[] offenceTypeList = noticeManage.GetOffenceTypeList(null);
            List<NoticeModel> viewNoticeList = new List<NoticeModel>(noticeList.Length);
            foreach (var notice in noticeList)
            {
                viewNoticeList.Add(new NoticeModel { NotIntNo=notice.NotIntNo, AutIntNo = notice.AutIntNo, NotRegNo = notice.NotRegNo, NotLocDescr = notice.NotLocDescr, NotTicketNo = notice.NotTicketNo, NotDateCOO = notice.NotDateCoo, NotOffenceType = (offenceTypeList.FirstOrDefault(c => c.OcTcode.Equals(notice.NotOffenceType)) ?? new OffenceType()).OcType });
            }
            var documentTypeList = noticeManage.GetDocumentImageTypeList().Where(c => c.AaDocImgTypeId == (Int32)AartoDocumentImageTypeList.ChangeOfOffender
            ||c.AaDocImgTypeId==(Int32)AartoDocumentImageTypeList.PresentationOfDocument
            ||c.AaDocImgTypeId==(Int32)AartoDocumentImageTypeList.Representation).ToArray();

            //Jerry 2015-01-06 change
            //var imageFileServer=noticeManage.GetImageFileServer("ExternalDocuments")??new ImageFileServer();
            var imageFileServer = noticeManage.GetExternalDocumentsImageFileServer() ?? new ImageFileServer();

            TempData["documentType"] = new SelectList(documentTypeList, "AaDocImgTypeID", "AaDocImgTypeDescription");
            TempData["userName"] = ((UserLoginInfo)Session["UserLoginInfo"]??new UserLoginInfo()).UserName;
            TempData["RecordCount"] = record_count;
            TempData["PageSize"] = page_size;
            TempData["SearchStr"] = Request.QueryString["SearchStr"];
            TempData["rootPath"] = Path.Combine(@"\\", imageFileServer.ImageMachineName,imageFileServer.ImageShareName).Replace("\\",@"\\");
            ViewData["authId"] = autIntNo;
            return View(viewNoticeList);
            
        }

        public JsonResult GetNoticeAndDocument(Int32 notIntNo)
        {
            var notice=noticeManage.GetNotice(notIntNo);

            if (notice == null)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            OffenceType[] offenceTypeList = noticeManage.GetOffenceTypeList(null);
            
            var documetnImageList = noticeManage.GetDocumentImageByNotice(notIntNo);
            List<object> attachment = new List<object>();
            foreach (var doc in documetnImageList)
            {
                attachment.Add(new { documentIntNo = doc.AaDocImgId, documentName = Path.GetFileName(doc.AaDocImgPath), documentUrl = doc.AaDocImgPath, docUserName = doc.LastUser, docUploadDate =doc.AaDocImgDateLoaded,documentType=doc.AaDocImgTypeId});
            }
         
            var entity = new NoticeModel
            {
                NotIntNo = notice.NotIntNo,
                NotTicketNo = notice.NotTicketNo,
                NotRegNo = notice.NotRegNo,
                
                NotOffenceType = (offenceTypeList.FirstOrDefault(c => c.OcTcode.Equals(notice.NotOffenceType)) 
                ?? new OffenceType { }).OcType,
                Attachment = attachment
            };

            return Json(entity, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetFile(int fileId)
        {
            //Jerry 2015-01-06 change
            //var fileServer= noticeManage.GetImageFileServer("ExternalDocuments");
            var fileServer = noticeManage.GetExternalDocumentsImageFileServer();

            if (fileServer == null)
            {
                return null;
            }

            var image = noticeManage.GetDocumentImage(fileId);
            if (image == null)
            {
                return null;
            }
            if (image.AaDocImgPath.StartsWith("\\"))
            {
                image.AaDocImgPath = image.AaDocImgPath.Remove(0, 1);
            }

            var path = Path.Combine(@"\\",fileServer.ImageMachineName,fileServer.ImageShareName,image.AaDocImgPath);
            if (!System.IO.File.Exists(path))
            {
                Response.StatusCode = 404;
                Response.StatusDescription = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.FileDoesNotFound;
                return null;
            }


            try
            {
                Bitmap.FromFile(path);
                return File(path, "image/jpeg");
            }
            catch (IOException ex)
            {
                return null;
            }
            catch (ArgumentException ea)
            {
                return File(path, "application/stream",Path.GetFileName(path));
            }
            catch (Exception e)
            {
                return File(path, "application/stream", Path.GetFileName(path));
            }


        }

        [HttpPost]
        public JsonResult DeleteDocument(Int32 imgIntNo)
        {
            //Jerry 2015-01-06 change
            //var fileServer = noticeManage.GetImageFileServer("ExternalDocuments");
            var fileServer = noticeManage.GetExternalDocumentsImageFileServer();
            var documentImage = noticeManage.GetDocumentImage(imgIntNo);

            if (fileServer != null && documentImage != null)
            {
                if (documentImage.AaDocImgPath.StartsWith(@"\"))
                {
                    documentImage.AaDocImgPath=documentImage.AaDocImgPath.Remove(0, 1);
                }
                string path = Path.Combine(@"\\", fileServer.ImageMachineName, fileServer.ImageShareName, documentImage.AaDocImgPath);
                if(System.IO.File.Exists(path))
                System.IO.File.Delete(path);
            }

            noticeManage.DeleteDocumentImage(imgIntNo);
            return Json(new { state = "true", result = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.deleteImageSuccess });
        }

        public JsonResult CheckFileName(string fileName)
        {
            if(string.IsNullOrEmpty(fileName))
            {
                return Json(new { state = "false", result = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            int notIntNo = 0;
            if (!Int32.TryParse(Request.Form["notIntNo"], out notIntNo))
            {
                Response.StatusCode = 500;
                Response.StatusDescription = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.NoticeNumberError;
                return Json(new { state = "false", result = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.NoticeNumberError }, JsonRequestBehavior.AllowGet);
            }

            //Jerry 2015-01-06 change
            //var fileServer = noticeManage.GetImageFileServer("ExternalDocuments");
            var fileServer = noticeManage.GetExternalDocumentsImageFileServer();

            if (fileServer == null)
            {
                Response.StatusCode = 500;
                Response.StatusDescription = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.NoFileServer;
                return Json(new { state = "false", result = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.NoFileServer }, JsonRequestBehavior.AllowGet);
            }

            string path = Path.Combine(@"\\", fileServer.ImageMachineName,  fileServer.ImageShareName, DateTime.Now.ToString("yyyy-MM-dd"), notIntNo.ToString(),fileName);
            if (System.IO.File.Exists(path))
            {
                return Json(new { state = "true", result = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.FileExist }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { state = "false", result = string.Empty },JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult SaveNoticeAndDocument()
        {
            int notIntNo=0,maxFileSize=0,imageType=0;
            string[] allowFileType = null;
            UserLoginInfo userInfo=null;
            //2015-02-25 Heidi added try catch for capture upload error message(bontq1854)
            try
            {
                
                if (!Int32.TryParse(Request.Form["notIntNo"], out notIntNo))
                {
                    Response.StatusCode = 500;
                    Response.StatusDescription = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.NoticeNumberError;
                    return Json(new { state = "false", result = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.NoticeNumberError });
                }

                if (!Int32.TryParse(Request.Form["imageType"], out imageType))
                {
                    Response.StatusCode = 500;
                    Response.StatusDescription = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.imageTypeNotExist;
                    return Json(new { state = "false", result = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.imageTypeNotExist });
                }



                if ((userInfo = (Session["UserLoginInfo"] as UserLoginInfo)) == null)
                {
                    Response.StatusCode = 500;
                    Response.StatusDescription = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.NotLogon;
                    return Json(new { state = "false", result = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.NotLogon });
                }
                //todo:
                //Jerry 2015-01-06 change
                //var fileServer = noticeManage.GetImageFileServer("ExternalDocuments");
                var fileServer = noticeManage.GetExternalDocumentsImageFileServer();

                if (fileServer == null)
                {
                    Response.StatusCode = 500;
                    Response.StatusDescription = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.NoFileServer;
                    return Json(new { state = "false", result = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.NoFileServer });
                }

                if (string.IsNullOrEmpty(fileServer.ImageMachineName))
                {
                    Response.StatusCode = 500;
                    Response.StatusDescription = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.NotLogon;
                    return Json(new { state = "false", result = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.NoFileServer });
                }

                var documentType = noticeManage.GetExternalDocumentDocumentType();
                if (documentType == null)
                {
                    Response.StatusCode = 500;
                    Response.StatusDescription = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.NotDocumentType;
                    return Json(new { state = "false", result = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.NotDocumentType });
                }

                Int32.TryParse(WebConfigurationManager.AppSettings["AllowUploadFileSize"], out maxFileSize);
                allowFileType = (WebConfigurationManager.AppSettings["AllowUploadFileType"] ?? string.Empty).Split(',');

                byte[] buffer = new byte[2048];
                int readLength = 0;
                string filePath = string.Empty;
                List<long> imageIntNoList = new List<long>();
                string rootPath = Path.Combine(@"\\", fileServer.ImageMachineName, fileServer.ImageShareName);
                DirectoryInfo directory = new DirectoryInfo(rootPath);
                if (!directory.Exists)
                {
                    //System.Security.AccessControl.AuthorizationRuleCollection roles= directory.GetAccessControl(System.Security.AccessControl.AccessControlSections.All).GetAccessRules(true,true,typeof(System.Security.Principal.NTAccount));
                    //foreach(var role in roles)
                    //{

                    //    //AppDomain.CurrentDomain.ApplicationIdentity.FullName
                    //}
                }

                foreach (string key in Request.Files.AllKeys)
                {
                    HttpPostedFileWrapper file = Request.Files[key] as HttpPostedFileWrapper;
                    if (file.ContentLength > maxFileSize)
                    {
                        Response.StatusCode = 500;
                        Response.StatusDescription = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.OutFileSize;
                        return Json(new { state = "false", result = string.Format(Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.OutFileSize, maxFileSize) });
                    }

                    if (allowFileType == null
                        || allowFileType.Where(c => file.ContentType.StartsWith(c)).Count() < 1)
                    {
                        Response.StatusCode = 500;
                        Response.StatusDescription = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.NotBeAllowFileType;
                        return Json(new { state = "false", result = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.NotBeAllowFileType });
                    }

                    //filePath = WebConfigurationManager.AppSettings["UploadFileDirectory"];
                    //if (filePath.StartsWith("/"))
                    //{
                    //    filePath = filePath.Remove(0, 1);
                    //}

                    filePath = Path.Combine(rootPath, DateTime.Now.ToString("yyyy-MM-dd"), notIntNo.ToString(),
                       Path.GetFileName(file.FileName)).Replace("/", "\\");
                    if (!Directory.Exists(Path.GetDirectoryName(filePath)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(filePath));
                    }

                    if (System.IO.File.Exists(filePath))
                    {
                        Response.StatusCode = 500;
                        Response.StatusDescription = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.FileExist;
                        return Json(new { state = "false", result = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.FileExist });
                    }

                    Stream stream = file.InputStream;
                    FileStream fs = System.IO.File.Create(filePath);
                    while ((readLength = stream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        fs.Write(buffer, 0, readLength);
                    }
                    fs.Close();
                    fs.Dispose();
                    stream.Close();

                    AartoNoticeDocument noticeDocument = noticeManage.GetNoticeDocumentByNotIntId(notIntNo);
                    if (noticeDocument == null)
                    {
                        noticeDocument = new AartoNoticeDocument();
                        noticeDocument.AaNotDocId = noticeManage.AddNoticeDocumentAndRetrunId(new AartoNoticeDocument
                        {
                            AaNoticeDocNo = "MVC_NotTicketNo",
                            NotIntNo = notIntNo,
                            AaDocTypeId = documentType.AaDocTypeId,
                            AaDocStatusId = (noticeManage.GetDocumentStatusList().FirstOrDefault() ?? new AartoDocumentStatus()).AaDocStatusId,
                            IfsIntNo = fileServer.IfsIntNo,
                            LastUser = userInfo.UserName
                        });
                    }

                    if (noticeDocument.AaNotDocId < 1)
                    {
                        Response.StatusCode = 500;
                        Response.StatusDescription = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.AddNoticeDocumentFailure;
                        return Json(new { state = "false", result = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.AddNoticeDocumentFailure });
                    }


                    var imageEntity = noticeManage.AddDocumentImage(new AartoDocumentImage
                       {
                           AaDocId = noticeDocument.AaNotDocId,
                           AaDocSourceTableId = (int)AartoDocumentSourceTableList.AARTONoticeDocument,
                           AaDocImgOrder = 1,
                           AaDocImgTypeId = imageType,
                           AaDocImgPath = filePath.Replace(rootPath, string.Empty),
                           AaDocImgDateLoaded = DateTime.Now,
                           LastUser = userInfo.UserName
                       });


                    if (imageEntity == null || imageEntity.AaDocImgId < 1)
                    {
                        Response.StatusCode = 500;
                        Response.StatusDescription = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.AddNoticeImageFailure;
                        return Json(new { state = "false", result = Resource.ScannedDigitaldocument.ScannedDigitaldocumentManage.AddNoticeImageFailure });
                    }

                    imageIntNoList.Add(imageEntity.AaDocImgId);

                }
                var result = new { state = "true", imageIntNoList = string.Join(",", imageIntNoList), result = Path.Combine("/", filePath.Replace(Server.MapPath("~/").Replace("\\", "/"), string.Empty)).Replace("\\", "/") };
                return Json(result);
            }
            catch (Exception ex)
            {
                var result = new { state = "false", result = ex.Message};
                Response.StatusCode = 500;
                return Json(result);
            }
        }

        public JsonResult GetNotice(string ticketNo)
        {
            var notice = noticeManage.GetNotice(ticketNo);
            return Json(new NoticeModel {NotIntNo=notice.NotIntNo, NotTicketNo=notice.NotTicketNo,NotRegNo=notice.NotRegNo,NotOffenceType=notice.NotOffenceType }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ScannedDigitalDocumentViewer(int notIntNo)
        {
            //Jerry 2015-01-06 change
            //var imageServer = noticeManage.GetImageFileServer("ExternalDocuments");
            var imageServer = noticeManage.GetExternalDocumentsImageFileServer();

            var list = noticeManage.GetDocumentImageByNotice(notIntNo);
           
            List<AartoDocumentImageModel> imageList=new List<AartoDocumentImageModel>();
            foreach(var l in list)
            {
                AartoDocumentImageModel model = new AartoDocumentImageModel 
                {
                    AaDocImgID = l.AaDocImgId,
                    AaDocImgPath=l.AaDocImgPath
                };
                if (l.AaDocImgPath.StartsWith("\\"))
                {
                    l.AaDocImgPath = l.AaDocImgPath.Remove(0, 1);
                }
                var path=Path.Combine(@"\\",imageServer.ImageMachineName,imageServer.ImageShareName ,
                   l.AaDocImgPath);
                if (System.IO.File.Exists(path))
                {
                    try 
                    {
                        Bitmap.FromFile(path);
                        model.IsImage = true;
                    }
                    catch 
                    {

                    }
                }
                imageList.Add(model);
            }
            return View(imageList);
        }
    }
}
