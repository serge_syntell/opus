﻿using Stalberg.TMS;
using System.Web.Services;
using System.Web.Services.Protocols;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using SIL.AARTO.BLL.Utility;
using System.IO;
using ceTe.DynamicPDF.Imaging;
using System;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.Web.Controllers.Handwritten
{
    public partial class HandwrittenController : Controller
    {
        SIL.AARTO.BLL.Utility.UserLoginInfo userDetails = new BLL.Utility.UserLoginInfo();
        string login = "";
        int autIntNo = 0;
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        string autName = "";
        string statusName = "";
        HwoImportedWithErrorsStatusService hiwssService = new HwoImportedWithErrorsStatusService();
        public ActionResult ViewHWOImportedWithErrors(string BeginDate="", string EndDate="", string Status="")
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
           
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            login = userDetails.UserName;
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            AuthorityService autService = new AuthorityService();
            SIL.AARTO.DAL.Entities.Authority autEntity = autService.GetByAutIntNo(autIntNo);
            if (autEntity != null)
            {
                autName = autEntity.AutName;
            }

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            DateTime beginDate = DateTime.Now.Date.AddDays(-30);
            DateTime endDate = DateTime.Now.Date.AddDays(1).AddSeconds(-1);
            if (BeginDate != "")
            {
                beginDate = Convert.ToDateTime(BeginDate);
            }
            if (EndDate != "")
            {
                endDate = Convert.ToDateTime(EndDate).AddDays(1).AddSeconds(-1);
            }

            int hwoImportedStatus = (int)(HwoImportedWithErrorsStatusList.Loaded);
            int.TryParse(Status, out hwoImportedStatus);
          
            if (hwoImportedStatus > 0)
            { 
                HwoImportedWithErrorsStatus hiwssEntity=hiwssService.GetByHiwesIntNo(hwoImportedStatus);
                if (hiwssEntity != null)
                {
                    statusName = hiwssEntity.HiwesName;
                }
            }

            var path = HttpContext.Request.PhysicalApplicationPath + @"\Reports\HWOImportedWithErrors.dplx";
            DocumentLayout doc = new DocumentLayout(path);
            
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl_AutName = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAutName");
                lbl_AutName.LayingOut += new LayingOutEventHandler(lbl_AutName2);
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl_Status = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblStatus");
                lbl_Status.LayingOut += new LayingOutEventHandler(lbl_Status2);

                
             
                StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
                query.ConnectionString =connectionString;
                ParameterDictionary parameters = new ParameterDictionary();
                parameters.Add("BeginDate", beginDate);
                parameters.Add("EndDate", endDate);
                parameters.Add("Status", hwoImportedStatus);
                parameters.Add("AutIntNo", autIntNo);
                Document report = doc.Run(parameters);
                byte[] buffer = report.Draw();
                return File(report.Draw(), "application/pdf");

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                Response.End();
                return View();
            }
        }

        public void lbl_AutName2(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRN = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;

                lblRN.Text = autName;
                
            }
            catch { }
        }

        public void lbl_Status2(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRN = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;

                lblRN.Text = statusName;

            }
            catch { }
        }

    }
}
