﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.Handwritten;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.Web.Controllers.Handwritten
{
    public partial class HandwrittenController : Controller
    {
       public ActionResult Report_HWOImportedWithErrors()
        {
            Report_HWOImportedWithErrorsModel model = new Report_HWOImportedWithErrorsModel();
            model.HWOImportedStatusList = GetHWOImportedStatusList();
            return View(model);
        }

       public List<SelectListItem> GetHWOImportedStatusList()
       {
           List<SelectListItem> itemList = new List<SelectListItem>();
            TList<HwoImportedWithErrorsStatus> statusList= new HwoImportedWithErrorsStatusService().GetAll();
            foreach (var item in statusList)
           {
               itemList.Add(new SelectListItem { Text = item.HiwesName , Value = item.HiwesIntNo.ToString()});
           }
           return itemList;

       }

    }
}
