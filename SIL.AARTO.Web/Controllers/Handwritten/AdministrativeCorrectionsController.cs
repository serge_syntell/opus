﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.Web.ViewModels.Handwritten;
using Stalberg.TMS;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.BLL.Utility;
using System.Transactions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Printing;
using System.Globalization;
using SIL.AARTO.DAL.Data;
using SIL.QueueLibrary;
using SIL.AARTO.Web.Resource.Handwritten;

namespace SIL.AARTO.Web.Controllers.Handwritten
{
    public partial class HandwrittenController : Controller
    {
        #region Variable definition
        NoticeDB noticedb;
        CourtDB courtdb;
        CashReceiptDB cashReceiptDB;
        Notice noticeEntity;
        NoticeService noticeService = new NoticeService();
        Charge chargeEntity;
        Court courtEntity;
        ChargeService chargeService = new ChargeService();
        SumChargeService sumChargeService = new SumChargeService();
        ReceiptService receiptService = new ReceiptService();
        NoticeSummons noticeSummonsEntity;
        Summons summonsEntity;
        List<Charge> chargeList = new List<Charge>();
        List<SumCharge> sumChargeList = new List<SumCharge>();
        HwoImportedWithErrors hwoImportedWithErrorsEntity;
        HwoImportedWithErrorsService hwoImportedWithErrorsService = new HwoImportedWithErrorsService();
        NoticeSummonsService noticeSummonsService = new NoticeSummonsService();
        SummonsService summonsService = new SummonsService();
        CourtService courtService = new CourtService();
        CourtRoomService courtRoomService = new CourtRoomService();
        CourtRoom courtRoomEntity;
        CourtDates courtDates;
        CourtDatesService courtDateService = new CourtDatesService();
        AdminCorrectionForPaymentsReceivedService adminCorrectionService = new AdminCorrectionForPaymentsReceivedService();
        EvidencePackService evidencePackService = new EvidencePackService();
        List<EvidencePack> evidencePackList;
        QueueItem item = new QueueItem();
        QueueItemProcessor queueProcessor = new QueueItemProcessor();
        SIL.DMS.DAL.Services.BatchDocumentService batchDocumentService = new SIL.DMS.DAL.Services.BatchDocumentService();
        SIL.DMS.DAL.Services.BatchService batchService = new SIL.DMS.DAL.Services.BatchService();
        int _noOfDaysForPayment = 0;
        #endregion

        public ActionResult AdministrativeCorrections(int page = 0,string NoticeNumber = "")
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            Session["currPageIndex_AdministrativeCorrections"] = page;
            return View(InitMode(page, AutIntNo, NoticeNumber));

        }

        private AdministrativeCorrectionList InitMode(int page,int autIntNo,string noticeNumber)
        {
            AdministrativeCorrectionList model = new AdministrativeCorrectionList();
            model.PageSize = Config.PageSize;
            int totalCount;
            AARTO.DAL.Services.NoticeService noticeService = new SIL.AARTO.DAL.Services.NoticeService();
            noticedb = new NoticeDB(connectionString);

            noticeNumber = string.IsNullOrEmpty(noticeNumber) ? "" : HttpUtility.UrlDecode(noticeNumber).Trim();

            DataSet ds = noticedb.GetAdministrativeCorrectionsList(autIntNo,noticeNumber, page + 1, model.PageSize, out totalCount);

            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            AdministrativeCorrectionModel item = new AdministrativeCorrectionModel();
                            item.HIWEIntNo = dt.Rows[i]["HIWEIntNo"].ToString().Trim();
                            item.NoticeNumber = dt.Rows[i]["NotTicketNo"].ToString().Trim();
                            item.OffenceDate = Convert.ToString(dt.Rows[i]["OffenceDate"]) == "" ? "" : Convert.ToDateTime(dt.Rows[i]["OffenceDate"]).ToString("yyyy/MM/dd");
                            item.ReceiptDate = Convert.ToString(dt.Rows[i]["ReceiptDate"]) == "" ? "" : Convert.ToDateTime(dt.Rows[i]["ReceiptDate"]).ToString("yyyy/MM/dd");
                            item.DMSFineAmount = double.Parse(dt.Rows[i]["DMSAmount"].ToString()).ToString("f2");
                            item.ReceiptFineAmount = double.Parse(dt.Rows[i]["ReceiptAmount"].ToString()).ToString("f2");
                            item.DMSCourt = dt.Rows[i]["DMSCourt"].ToString();
                            item.ReceiptCourt = dt.Rows[i]["ReceiptCourt"].ToString();
                            item.DMSCourtIntNo = dt.Rows[i]["DMSCrtIntNo"].ToString();
                            item.ReceiptCourtIntNo = dt.Rows[i]["ReceiptCrtIntNo"].ToString();
                            item.LoadedDate = Convert.ToString(dt.Rows[i]["HWECreateDate"]) == "" ? "" : Convert.ToDateTime(dt.Rows[i]["HWECreateDate"]).ToString("yyyy/MM/dd");
                            item.Status = dt.Rows[i]["Status"].ToString();
                            item.DMSImportedCounter = dt.Rows[i]["DMSImportedCounter"].ToString();
                            model.AdministrativeCorrectionModels.Add(item);
                        }
                    }
                   
                }
            }
            model.TotalCount = Convert.ToInt32(totalCount);
            model.PageIndex = page;
            //model.AutIntNo = autIntNo;
            model.CourtList = GetCourtSelectlist();
            return model;
        }

        [HttpPost]
        public JsonResult ConfirmFix(int HIWEIntNo, string CourtFlag, string FineAmountFlag, string CorrectCourtIntNo, string NoticeNumber)
        {
            SIL.AARTO.BLL.Utility.UserLoginInfo userDetails = new BLL.Utility.UserLoginInfo();
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            string login = userDetails.UserName;
            int userIntNo = userDetails.UserIntNo;
            int autIntNo = AutIntNo;
            decimal correctfineAmout = 0;
            int correctCourtIntNo = 0;
            int errorCode = 0;
            string errorMsg = "";
            string ReturnToDmsMsgFlag="";

            NoticeNumber = string.IsNullOrEmpty(NoticeNumber) ? "" : HttpUtility.UrlDecode(NoticeNumber).Trim();

            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
            try
            {
                #region AdministrativeCorrection

                hwoImportedWithErrorsEntity = hwoImportedWithErrorsService.GetByHiweIntNo(HIWEIntNo);
                if (hwoImportedWithErrorsEntity == null)
                {
                    //this.ResourceValue("AdministrativeCorrections", "lblErrorMsg3")
                    return Json(new { result = -3, errorlist = new { errorTip = AdministrativeCorrections_cshtml.lblErrorMsg3 } });
                    
                }

                noticeEntity = noticeService.GetByNotIntNo(hwoImportedWithErrorsEntity.NotIntNo);

                if (noticeEntity ==null)
                {
                    //this.ResourceValue("AdministrativeCorrections", "lblErrorMsg4")
                    return Json(new { result = -4, errorlist = new { errorTip = AdministrativeCorrections_cshtml.lblErrorMsg4 } });
                }

                //if (noticeEntity.NoticeStatus != 955)
                //{
                //    return Json(new { result = -28, errorlist = new { errorTip = this.ResourceValue("AdministrativeCorrections", "lblErrorMsg28") } });
                //}

                if (noticeEntity.NotFilmType != "H" && noticeEntity.NotFilmType != "M")
                {
                    // this.ResourceValue("AdministrativeCorrections", "lblErrorMsg25")
                    return Json(new { result = -25, errorlist = new { errorTip = AdministrativeCorrections_cshtml.lblErrorMsg25 } });
                }

                int.TryParse(CorrectCourtIntNo, out correctCourtIntNo);

                if (string.IsNullOrEmpty(FineAmountFlag) && string.IsNullOrEmpty(CourtFlag) && correctCourtIntNo == 0)
                {
                    //this.ResourceValue("AdministrativeCorrections", "lblErrorMsg26")
                    return Json(new { result = -26, errorlist = new { errorTip = AdministrativeCorrections_cshtml.lblErrorMsg26 } });
                }

                if (hwoImportedWithErrorsEntity.DmsAmount != hwoImportedWithErrorsEntity.ReceiptAmount)
                {
                    if (string.IsNullOrEmpty(FineAmountFlag) || (FineAmountFlag != "DMS" && FineAmountFlag != "Receipt"))
                    {
                        //this.ResourceValue("AdministrativeCorrections", "lblErrorMsg6")
                        return Json(new { result = -6, errorlist = new { errorTip =AdministrativeCorrections_cshtml.lblErrorMsg6  } });
                    }
                }

                if ((hwoImportedWithErrorsEntity.DmsAmount == hwoImportedWithErrorsEntity.ReceiptAmount) && (hwoImportedWithErrorsEntity.DmsAmount == 0))
                {
                    //this.ResourceValue("AdministrativeCorrections", "lblErrorMsg27")
                    return Json(new { result = -27, errorlist = new { errorTip = AdministrativeCorrections_cshtml.lblErrorMsg27 } });
                }
              
                noticeSummonsEntity = noticeSummonsService.GetByNotIntNo(noticeEntity.NotIntNo).FirstOrDefault();
                courtEntity = courtService.GetByCrtIntNo(correctCourtIntNo);

                if (FineAmountFlag == "DMS")
                {
                    correctfineAmout = hwoImportedWithErrorsEntity.DmsAmount;
                }
                else if (FineAmountFlag == "Receipt")
                {
                    correctfineAmout = hwoImportedWithErrorsEntity.ReceiptAmount;
                }
                else if (hwoImportedWithErrorsEntity.DmsAmount == hwoImportedWithErrorsEntity.ReceiptAmount)
                {
                    correctfineAmout = hwoImportedWithErrorsEntity.DmsAmount;
                }

                using (TransactionScope scope = new TransactionScope())
                {
                    if (!string.IsNullOrEmpty(FineAmountFlag))
                    {
                        if (FineAmountFlag == "DMS")
                        {
                            #region fine amout difference

                            if (noticeEntity.NotFilmType == "H")
                            {
                                #region deal with charge fine amount

                                TList<Charge> charges = chargeService.GetByNotIntNo(noticeEntity.NotIntNo);
                                if (charges != null && charges.Count > 0)
                                {
                                    chargeList = charges.Where(c => c.ChgIsMain == true).ToList();
                                    if (chargeList != null && chargeList.Count > 0)
                                    {
                                        decimal fineAmoutAvg = correctfineAmout / chargeList.Count;
                                        foreach (var item in chargeList)
                                        {
                                            item.ChgFineAmount = (float)fineAmoutAvg;
                                            chargeService.Update(item);
                                        }
                                    }
                                }
                                #region update HwoImportedWithErrorsEntity Status
                                hwoImportedWithErrorsEntity.ImportedWithErrorsStatus = (int)HwoImportedWithErrorsStatusList.Corrected;
                                hwoImportedWithErrorsService.Update(hwoImportedWithErrorsEntity);
                                #endregion

                                #region added AdminCorrectionAudit

                                AdminCorrectionForPaymentsReceived acaEntity = new AdminCorrectionForPaymentsReceived();
                                acaEntity.ChangedAmount = (float)(hwoImportedWithErrorsEntity.DmsAmount - hwoImportedWithErrorsEntity.ReceiptAmount);
                                acaEntity.Comment = AdministrativeCorrections_cshtml.AdminCorrectionAuditDescr1; //this.ResourceValue("AdministrativeCorrections", "AdminCorrectionAuditDescr1");
                                acaEntity.LastUser = LastUser;
                                acaEntity.NotIntNo = noticeEntity.NotIntNo;
                                acaEntity.AcfprCreateDate = DateTime.Now;
                                adminCorrectionService.Save(acaEntity);
                                #endregion

                                #endregion
                            }
                            else if (noticeEntity.NotFilmType == "M")
                            {
                                #region deal with sumcharge fine amount

                                TList<SumCharge> sumcharges = sumChargeService.GetBySumIntNo(noticeSummonsEntity.SumIntNo);
                                if (sumcharges != null && sumcharges.Count > 0)
                                {
                                    sumChargeList = sumcharges.Where(c => c.SchIsMain == true).ToList();

                                    if (sumChargeList != null && sumChargeList.Count > 0)
                                    {
                                        decimal fineAmoutAvg = correctfineAmout / sumChargeList.Count;
                                        foreach (var item in sumChargeList)
                                        {
                                            item.SchFineAmount = fineAmoutAvg;
                                            sumChargeService.Update(item);
                                        }
                                    }

                                }

                                TList<Charge> charges = chargeService.GetByNotIntNo(noticeEntity.NotIntNo);
                                if (charges != null && charges.Count > 0)
                                {
                                    chargeList = charges.Where(c => c.ChgIsMain == true).ToList();
                                    if (chargeList != null && chargeList.Count > 0)
                                    {
                                        decimal fineAmoutAvg = correctfineAmout / chargeList.Count;
                                        foreach (var item in chargeList)
                                        {
                                            item.ChgFineAmount = (float)fineAmoutAvg;
                                            chargeService.Update(item);
                                        }
                                    }
                                }

                                #region update HwoImportedWithErrorsEntity Status
                                hwoImportedWithErrorsEntity.ImportedWithErrorsStatus = (int)HwoImportedWithErrorsStatusList.Corrected;
                                hwoImportedWithErrorsService.Update(hwoImportedWithErrorsEntity);
                                #endregion

                                #region added AdminCorrectionAudit
                                AdminCorrectionForPaymentsReceived acaEntity = new AdminCorrectionForPaymentsReceived();
                                acaEntity.ChangedAmount = (float)(hwoImportedWithErrorsEntity.DmsAmount - hwoImportedWithErrorsEntity.ReceiptAmount);
                                acaEntity.Comment = AdministrativeCorrections_cshtml.AdminCorrectionAuditDescr1; //this.ResourceValue("AdministrativeCorrections", "AdminCorrectionAuditDescr1");
                                acaEntity.LastUser = LastUser;
                                acaEntity.NotIntNo = noticeEntity.NotIntNo;
                                acaEntity.AcfprCreateDate = DateTime.Now;
                                adminCorrectionService.Save(acaEntity);

                                #endregion

                                #endregion
                            }

                            #endregion
                        }

                    }

                    if (!string.IsNullOrEmpty(CourtFlag))
                    {
                        #region court difference

                        if (CourtFlag == "DMS")
                        {
                            #region Receipt reversed

                            errorCode = ReceiptReverse(hwoImportedWithErrorsEntity, out errorMsg);
                            if (errorCode != 0)
                            {
                                return Json(new { result = errorCode, errorlist = new { errorTip = errorMsg } });
                            }

                            #endregion

                            #region update Court Details

                            errorCode = UpdateCourtDetails(hwoImportedWithErrorsEntity, out errorMsg);
                            if (errorCode != 0)
                            {
                                return Json(new { result = errorCode, errorlist = new { errorTip = errorMsg } });
                            }

                            #endregion

                            if (!string.IsNullOrEmpty(FineAmountFlag) && FineAmountFlag == "Receipt")
                            {
                                //Return To DMS
                                #region Return To DMS

                                errorCode = ReturnToDMS(hwoImportedWithErrorsEntity, "FineAmount", "", out errorMsg);
                                if (errorCode != 0)
                                {
                                    return Json(new { result = errorCode, errorlist = new { errorTip = errorMsg } });
                                }

                                #endregion
                            }
                            else if ((!string.IsNullOrEmpty(FineAmountFlag) && FineAmountFlag == "DMS") || (string.IsNullOrEmpty(FineAmountFlag)))
                            {
                                #region pushQueue
                                PushQueue(hwoImportedWithErrorsEntity);
                                #endregion
                            }


                            #region add new Receipt

                            errorCode = AddNewReceipt(hwoImportedWithErrorsEntity, hwoImportedWithErrorsEntity.ReceiptAmount, userIntNo, out errorMsg);
                            if (errorCode != 0)
                            {
                                return Json(new { result = errorCode, errorlist = new { errorTip = errorMsg } });
                            }

                            #endregion
                           
                            #region update HwoImportedWithErrorsEntity Status
                            hwoImportedWithErrorsEntity.ImportedWithErrorsStatus = (int)HwoImportedWithErrorsStatusList.Corrected;
                            hwoImportedWithErrorsService.Update(hwoImportedWithErrorsEntity);
                            #endregion

                            #region Add Evidence Pack

                            if (!string.IsNullOrEmpty(FineAmountFlag))
                            {
                                if (!string.IsNullOrEmpty(FineAmountFlag) && FineAmountFlag == "DMS")
                                {
                                    #region EvidencePack
                                    AddEvidencePack(noticeEntity.NotIntNo, AdministrativeCorrections_cshtml.lblAC_CourtchangedAndProcessedAdjustFineAmount);//this.ResourceValue("AdministrativeCorrections", "lblAC_CourtchangedAndProcessedAdjustFineAmount")
                                    #endregion
                                }
                            }
                            else
                            {
                                #region EvidencePack
                                AddEvidencePack(noticeEntity.NotIntNo, AdministrativeCorrections_cshtml.lblAC_Courtchanged);//this.ResourceValue("AdministrativeCorrections", "lblAC_Courtchanged")
                                #endregion
                            }
                            #endregion


                        }
                        else if (CourtFlag == "Receipt")
                        {
                            //Return To DMS
                            #region Return To DMS

                            if (!string.IsNullOrEmpty(FineAmountFlag))
                            {
                                if (FineAmountFlag != "DMS")
                                {
                                    ReturnToDmsMsgFlag = "CourtAndFineAmount";
                                }
                            }
                            else
                            {
                                ReturnToDmsMsgFlag = "Court";
                            }

                            errorCode = ReturnToDMS(hwoImportedWithErrorsEntity, ReturnToDmsMsgFlag, "", out errorMsg);
                            if (errorCode != 0)
                            {
                                return Json(new { result = errorCode, errorlist = new { errorTip = errorMsg } });
                            }

                            #endregion

                            #region update HwoImportedWithErrorsEntity Status

                            hwoImportedWithErrorsEntity.ImportedWithErrorsStatus = (int)HwoImportedWithErrorsStatusList.Corrected;
                            hwoImportedWithErrorsService.Update(hwoImportedWithErrorsEntity);

                            #endregion

                            #region Add Evidence Pack

                            if (!string.IsNullOrEmpty(FineAmountFlag))
                            {
                                if (!string.IsNullOrEmpty(FineAmountFlag) && FineAmountFlag == "DMS")
                                {
                                    #region EvidencePack
                                    AddEvidencePack(noticeEntity.NotIntNo, AdministrativeCorrections_cshtml.lblAC_ProcessedAdjustFineAmount);//this.ResourceValue("AdministrativeCorrections", "lblAC_ProcessedAdjustFineAmount")
                                    #endregion
                                }
                            }
                            

                            #endregion


                        }

                        #endregion
                    }
                    else if (string.IsNullOrEmpty(CourtFlag)&&correctCourtIntNo>0)
                    {

                        #region Receipt reversed

                        errorCode = ReceiptReverse(hwoImportedWithErrorsEntity, out errorMsg);
                        if (errorCode != 0)
                        {
                            return Json(new { result = errorCode, errorlist = new { errorTip = errorMsg } });
                        }

                        #endregion

                        #region update Court Details

                        errorCode = UpdateCourtDetails(hwoImportedWithErrorsEntity, out errorMsg);
                        if (errorCode != 0)
                        {
                            return Json(new { result = errorCode, errorlist = new { errorTip = errorMsg } });
                        }

                        #endregion

                        //Return To DMS
                        #region Return To DMS

                        if (!string.IsNullOrEmpty(FineAmountFlag))
                        {
                            if (FineAmountFlag != "DMS")
                            {
                                ReturnToDmsMsgFlag = "CourtAndFineAmount";
                            }
                        }
                        else
                        {
                            ReturnToDmsMsgFlag = "Court";
                        }

                        errorCode = ReturnToDMS(hwoImportedWithErrorsEntity, ReturnToDmsMsgFlag, "", out errorMsg);
                        if (errorCode != 0)
                        {
                            return Json(new { result = errorCode, errorlist = new { errorTip = errorMsg } });
                        }

                        #endregion

                        #region add new Receipt

                        errorCode = AddNewReceipt(hwoImportedWithErrorsEntity, hwoImportedWithErrorsEntity.ReceiptAmount, userIntNo, out errorMsg);
                        if (errorCode != 0)
                        {
                            return Json(new { result = errorCode, errorlist = new { errorTip = errorMsg } });
                        }

                        #endregion

                        #region update HwoImportedWithErrorsEntity Status

                        hwoImportedWithErrorsEntity.ImportedWithErrorsStatus = (int)HwoImportedWithErrorsStatusList.Corrected;
                        hwoImportedWithErrorsService.Update(hwoImportedWithErrorsEntity);

                        #endregion

                        #region Add Evidence Pack
                        if (!string.IsNullOrEmpty(FineAmountFlag))
                        {
                            if (!string.IsNullOrEmpty(FineAmountFlag) && FineAmountFlag == "DMS")
                            {
                                #region EvidencePack
                                AddEvidencePack(noticeEntity.NotIntNo, AdministrativeCorrections_cshtml.lblAC_CourtchangedAndProcessedAdjustFineAmount);//this.ResourceValue("AdministrativeCorrections", "lblAC_CourtchangedAndProcessedAdjustFineAmount")
                                #endregion
                            }
                        }
                        else
                        {
                            #region EvidencePack
                            AddEvidencePack(noticeEntity.NotIntNo, AdministrativeCorrections_cshtml.lblAC_Courtchanged);//this.ResourceValue("AdministrativeCorrections", "lblAC_Courtchanged")
                            #endregion
                        }
                        #endregion


                    }
                    else if (string.IsNullOrEmpty(CourtFlag) && correctCourtIntNo==0)
                    {
                        if (!string.IsNullOrEmpty(FineAmountFlag))
                        {
                            if (FineAmountFlag == "Receipt")
                            {
                                //Return To DMS
                                #region Return To DMS

                                errorCode = ReturnToDMS(hwoImportedWithErrorsEntity, "FineAmount", "", out errorMsg);
                                if (errorCode != 0)
                                {
                                    return Json(new { result = errorCode, errorlist = new { errorTip = errorMsg } });
                                }

                                #endregion
                            }
                            else if (FineAmountFlag == "DMS")
                            {
                                #region pushQueue
                                PushQueue(hwoImportedWithErrorsEntity);
                                #endregion
                            }

                            if (FineAmountFlag == "DMS")
                            {
                                #region EvidencePack
                                AddEvidencePack(noticeEntity.NotIntNo, AdministrativeCorrections_cshtml.lblAC_ProcessedAdjustFineAmount);//this.ResourceValue("AdministrativeCorrections", "lblAC_ProcessedAdjustFineAmount")
                                #endregion
                            }

                            #region update HwoImportedWithErrorsEntity Status

                            hwoImportedWithErrorsEntity.ImportedWithErrorsStatus = (int)HwoImportedWithErrorsStatusList.Corrected;
                            hwoImportedWithErrorsService.Update(hwoImportedWithErrorsEntity);

                            #endregion
                        }
                    }
                    
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.AdministrativeCorrection, PunchAction.Add);

                    scope.Complete();
                }

                #endregion

                #region Set CurrPage Index

                NoticeNumber = string.IsNullOrEmpty(NoticeNumber) ? string.Empty : HttpUtility.UrlDecode(NoticeNumber).Trim();
                int pageSize = Config.PageSize;
                int totalCount = 0;
                noticedb = new NoticeDB(connectionString);
                DataSet ds = noticedb.GetAdministrativeCorrectionsList(autIntNo, NoticeNumber, 1, pageSize, out totalCount);

                int pageCount = totalCount / pageSize;
                int temp = totalCount % pageSize;
                if (temp > 0)
                {
                    pageCount = pageCount + 1;
                }
                int oldCurrpageIndex = (int)Session["currPageIndex_AdministrativeCorrections"];
                int currpageIndex = 0;

                if (pageCount == 1 || pageCount == 0)
                {
                    currpageIndex = 0;
                }
                else
                {

                    if (pageCount >= (oldCurrpageIndex + 1))
                    {
                        currpageIndex = oldCurrpageIndex;
                    }
                    else
                    {
                        currpageIndex = oldCurrpageIndex - 1;
                    }
                }

                #endregion

                return Json(new
                {
                    result = 1,
                    pageIndex = new
                    {
                        currpageIndex = currpageIndex
                    }
                });

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    result = -1,
                    errorlist = new
                    {
                        errorTip = ex.Message
                    }
                });
            }
        }

        [HttpPost]
        public JsonResult GetOfficerError(string NoticeNumber="")
        {
            NoticeNumber = string.IsNullOrEmpty(NoticeNumber) ? "" : HttpUtility.UrlDecode(NoticeNumber).Trim();

            string officerErrors = "";
            SIL.AARTO.BLL.Utility.UserLoginInfo userDetails = new BLL.Utility.UserLoginInfo();
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            string login = userDetails.UserName;

            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

          try{

                noticeEntity = noticeService.GetByNotTicketNo(NoticeNumber).FirstOrDefault();
                if (noticeEntity == null)
                {
                    return Json(new { result = -4, errorlist = new { errorTip = AdministrativeCorrections_cshtml.lblErrorMsg4 } });//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg4")
                }
                noticedb = new NoticeDB(connectionString);
                DataSet ds = noticedb.GetOfficerErrorByNotIntNo(noticeEntity.NotIntNo);

                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (i == dt.Rows.Count - 1)
                            {
                                officerErrors += dt.Rows[i]["AaOfErDescription"].ToString();
                            }
                            else
                            {
                                officerErrors += dt.Rows[i]["AaOfErDescription"].ToString() + ";";
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    result = -1,
                    errorlist = new
                    {
                        errorTip = ex.Message
                    }
                });
            }

          return Json(new { result = 1, errorlist = new { errorTip = officerErrors } });


        }

        [HttpPost]
        public JsonResult ReturnToDMS(int HIWEIntNo,string Reason,string NoticeNumber)
        {
            SIL.AARTO.BLL.Utility.UserLoginInfo userDetails = new BLL.Utility.UserLoginInfo();
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            string login = userDetails.UserName;
            int userIntNo = userDetails.UserIntNo;

            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            NoticeNumber = string.IsNullOrEmpty(NoticeNumber) ? "" : HttpUtility.UrlDecode(NoticeNumber).Trim();

            hwoImportedWithErrorsEntity = hwoImportedWithErrorsService.GetByHiweIntNo(HIWEIntNo);
            if (hwoImportedWithErrorsEntity == null)
            {
                return Json(new { result = -3, errorlist = new { errorTip = AdministrativeCorrections_cshtml.lblErrorMsg3 } });//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg3")
            }
            noticeEntity = noticeService.GetByNotIntNo(hwoImportedWithErrorsEntity.NotIntNo);
            if (noticeEntity == null)
            {
                return Json(new { result = -4, errorlist = new { errorTip = AdministrativeCorrections_cshtml.lblErrorMsg4 } });//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg4")
            }

            if (noticeEntity.NotFilmType != "H" && noticeEntity.NotFilmType != "M")
            {
                return Json(new { result = -25, errorlist = new { errorTip = AdministrativeCorrections_cshtml.lblErrorMsg25 } });//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg25") 
            }

            if (hwoImportedWithErrorsEntity.DmsImportedCounter.HasValue && hwoImportedWithErrorsEntity.DmsImportedCounter > 3)
            {
                return Json(new { result = -20, errorlist = new { errorTip = AdministrativeCorrections_cshtml.lblErrorMsg20 } });//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg20")
            }

             try
             {

                 #region Return To DMS
                 int errorCode = 0;
                 string errorMsg = "";

                 using (TransactionScope scope = new TransactionScope())
                 {

                     errorCode = ReturnToDMS(hwoImportedWithErrorsEntity, "", Reason, out errorMsg);
                     if (errorCode != 0)
                     {
                         return Json(new { result = errorCode, errorlist = new { errorTip = errorMsg } });
                     }

                     #region update HwoImportedWithErrorsEntity Status
                     hwoImportedWithErrorsEntity.ImportedWithErrorsStatus = (int)HwoImportedWithErrorsStatusList.Corrected;
                     hwoImportedWithErrorsService.Update(hwoImportedWithErrorsEntity);
                     #endregion

                     scope.Complete();
                 }

                 #endregion

                 #region Set CurrPage Index
               
                int pageSize = Config.PageSize;
                int totalCount = 0;
                noticedb = new NoticeDB(connectionString);
                DataSet ds = noticedb.GetAdministrativeCorrectionsList(AutIntNo, NoticeNumber, 1, pageSize, out totalCount);

                int pageCount = totalCount / pageSize;
                int temp = totalCount % pageSize;
                if (temp > 0)
                {
                    pageCount = pageCount + 1;
                }
                int oldCurrpageIndex = (int)Session["currPageIndex_AdministrativeCorrections"];
                int currpageIndex = 0;

                if (pageCount == 1 || pageCount == 0)
                {
                    currpageIndex = 0;
                }
                else
                {

                    if (pageCount >= (oldCurrpageIndex + 1))
                    {
                        currpageIndex = oldCurrpageIndex;
                    }
                    else
                    {
                        currpageIndex = oldCurrpageIndex - 1;
                    }
                }

                #endregion


                return Json(new
                {
                    result = 1,
                    pageIndex = new
                    {
                        currpageIndex = currpageIndex
                    }
                });

            }
             catch (Exception ex)
             {
                 return Json(new
                 {
                     result = -1,
                     errorlist = new
                     {
                         errorTip = ex.Message
                     }
                 });
             }
           
        }

        public void PushQueue(HwoImportedWithErrors hwoImportedWithErrorsEntity)
        {

            SIL.DMS.DAL.Entities.BatchDocument batchDoc = batchDocumentService.GetByBaDoId(hwoImportedWithErrorsEntity.BaDoId);

            if (batchDoc != null)
            {

                SIL.DMS.DAL.Entities.Batch batch = batchService.GetByBatchId(batchDoc.BatchId);

                batchDoc.BaDoStId = (int)SIL.DMS.DAL.Entities.BatchDocumentStatusList.QAFinished;
                batchDoc.IsBackFromAarto = true;
                batchDoc.LastUser = LastUser;
                batchDocumentService.Update(batchDoc);

                item.Group = batch.ClientId;
                item.LastUser = LastUser;
                item.Body = batchDoc.BaDoId;
                var delayHours = new SIL.DMS.DAL.Services.GlobalParameterService().GetByGlPaName(SIL.DMS.DAL.Entities.GlobalParameterList.DelayActionDate_InHours.ToString()).GlPaNumericValue;
                item.ActDate = DateTime.Now.AddHours(Convert.ToDouble(delayHours));
                item.QueueType = SIL.ServiceQueueLibrary.DAL.Entities.ServiceQueueTypeList.HandwrittenImporter;
                queueProcessor.Send(item);
               
            }
        }

        public int UpdateCourtDetails(HwoImportedWithErrors hwoImportedWithErrorsEntity,out string errorMsg)
        {
            #region update court details
            int errorCode = 0;
            errorMsg = "";

            noticeEntity = noticeService.GetByNotIntNo(hwoImportedWithErrorsEntity.NotIntNo);
            noticeSummonsEntity = noticeSummonsService.GetByNotIntNo(noticeEntity.NotIntNo).FirstOrDefault();

            if (noticeEntity.NotFilmType == "H")
            {
                //notice
                if (courtEntity != null)
                {
                    noticeEntity = noticeService.GetByNotIntNo(hwoImportedWithErrorsEntity.NotIntNo);
                    noticeEntity.NotCourtNo = courtEntity.CrtNo;
                    noticeEntity.NotCourtName = courtEntity.CrtName;
                    noticeService.Update(noticeEntity);
                }
            }
            else if (noticeEntity.NotFilmType == "M")
            {
                //summons
                summonsEntity = summonsService.GetBySumIntNo(noticeSummonsEntity.SumIntNo);

                if (courtEntity != null)
                {
                    IList<CourtRoom> courtRoomList = courtRoomService.GetByCrtIntNo(courtEntity.CrtIntNo).Where(c => c.CrtRactive == "Y").ToList();
                    if (courtRoomList != null && courtRoomList.Count > 0)
                    {
                        courtRoomEntity = courtRoomList[0];
                    }
                    if (courtRoomEntity == null)
                    {
                        errorCode = -21;
                        errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg21;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg21");
                        return errorCode;
                    }
                    if (summonsEntity.SumCourtDate.HasValue)
                    {
                        IList<CourtDates> CourtDateList = courtDateService.GetByCrtRintNo(courtRoomEntity.CrtRintNo).ToList();

                        courtDates = CourtDateList.Where(c => c.Cdate.Date == summonsEntity.SumCourtDate.Value.Date && c.AutIntNo == AutIntNo).FirstOrDefault();

                        if (courtDates == null)
                        {
                            courtDates = new CourtDates();
                            courtDates.CrtRintNo = courtRoomEntity.CrtRintNo;
                            courtDates.Cdate = Convert.ToDateTime(summonsEntity.SumCourtDate).Date;
                            courtDates.AutIntNo = AutIntNo;
                            courtDates.CdNoOfCases = 0;
                            courtDates.LastUser = LastUser;
                            courtDates.OgIntNo = (int)OriginGroupList.ALL;
                        }
                    }

                    summonsEntity.CrtIntNo = courtEntity.CrtIntNo;
                    summonsEntity.CrtRintNo = courtRoomEntity.CrtRintNo;
                    summonsService.Update(summonsEntity);

                   
                    noticeEntity.NotCourtNo = courtEntity.CrtNo;
                    noticeEntity.NotCourtName = courtEntity.CrtName;
                    noticeService.Update(noticeEntity);

                }
            }

            return errorCode;

            #endregion
        }

        public int ReturnToDMS(HwoImportedWithErrors hwoImportedWithErrorsEntity,string ReasonFlag,string Reason,out string errorMsg)
        {
            int errorCode = 0;
            errorMsg = "";
            string errorReason = "";

            if (ReasonFlag == "Court")
            {
                errorReason = AdministrativeCorrections_cshtml.IncorrectCourt;//this.ResourceValue("AdministrativeCorrections", "IncorrectCourt");
            }
            else if (ReasonFlag == "FineAmount")
            {
                errorReason = AdministrativeCorrections_cshtml.IncorrectAmount;//this.ResourceValue("AdministrativeCorrections", "IncorrectAmount");
            }
            else if (ReasonFlag == "CourtAndFineAmount")
            {
                errorReason = AdministrativeCorrections_cshtml.IncorrectCourtAndAmount;//this.ResourceValue("AdministrativeCorrections", "IncorrectCourtAndAmount");
            }
            else
            {
                errorReason = string.IsNullOrEmpty(Reason) ? "" : Reason;
            }

            try
            {
              
               noticeEntity = noticeService.GetByNotIntNo(hwoImportedWithErrorsEntity.NotIntNo);
               noticeSummonsEntity = noticeSummonsService.GetByNotIntNo(noticeEntity.NotIntNo).FirstOrDefault();

                if (noticeEntity.NotFilmType == "H")
                {
                    //notice
                    #region return to DMS

                    if (hwoImportedWithErrorsEntity.DmsImportedCounter.HasValue && hwoImportedWithErrorsEntity.DmsImportedCounter > 3)
                    {
                        errorCode = -20;
                        errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg20;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg20");
                        return errorCode;
                    }

                    var checkRep = CheckRepresentationBeforeCorrection(noticeEntity.NotTicketNo, true);
                    if (checkRep < 0)
                    {
                        if (checkRep == -10)
                        {
                            errorCode = -10;
                            errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg10;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg10");
                            return errorCode;
                        }
                        if (checkRep == -11)
                        {
                            errorCode = -11;
                            errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg11;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg11");
                            return errorCode;
                        }
                    }
                    if (noticeEntity.NoticeStatus == (int)ChargeStatusList.DMSDocumentCorrection)
                    {
                        errorCode = -12;
                        errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg12;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg12");
                        return errorCode;
                    }
                    else
                    {
                        //noticeEntity.NoticeStatus = (int)ChargeStatusList.DMSDocumentCorrection;
                        noticeEntity.LastUser = LastUser;
                        noticeEntity.IsHwoCorrection = true;

                        noticeService.Update(noticeEntity);

                        #region EvidencePack
                        AddEvidencePack(noticeEntity.NotIntNo, AdministrativeCorrections_cshtml.lblAC_ReturnedToDMS);//this.ResourceValue("AdministrativeCorrections", "lblAC_ReturnedToDMS")
                        #endregion

                        TList<Charge> chargeEntityList = chargeService.GetByNotIntNo(noticeEntity.NotIntNo);
                        foreach (var charge in chargeEntityList)
                        {
                            //charge.ChargeStatus = (int)ChargeStatusList.DMSDocumentCorrection;
                            charge.LastUser = LastUser;
                            chargeService.Update(charge);
                        }

                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.CorrectionsOfHandwritten, PunchAction.Add);

                        if (string.IsNullOrEmpty(hwoImportedWithErrorsEntity.BaDoId) || string.IsNullOrEmpty(hwoImportedWithErrorsEntity.BatchId))
                        {
                            errorCode = -22;
                            errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg22;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg22");
                            return errorCode;
                        }
                        else
                        {
                            //update DMS 
                            UpdateDMS(hwoImportedWithErrorsEntity.BatchId.Trim(), hwoImportedWithErrorsEntity.BaDoId.Trim(), errorReason);
                        }

                    }

                    #endregion
                }
                else if (noticeEntity.NotFilmType == "M")
                {
                    //summons
                    #region return to DMS

                    if (hwoImportedWithErrorsEntity.DmsImportedCounter.HasValue && hwoImportedWithErrorsEntity.DmsImportedCounter > 3)
                    {
                        errorCode = -20;
                        errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg20;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg20");
                        return errorCode;
                    }

                    summonsEntity = summonsService.GetBySumIntNo(noticeSummonsEntity.SumIntNo);

                    var checkRep = CheckRepresentationBeforeCorrection(noticeEntity.NotTicketNo, false);
                    if (checkRep < 0)
                    {
                        if (checkRep == -10)
                        {
                            errorCode = -10;
                            errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg10;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg10");
                            return errorCode;
                        }
                        if (checkRep == -11)
                        {
                            errorCode = -11;
                            errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg11;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg11");
                            return errorCode;
                        }
                    }

                    if (summonsEntity.SummonsStatus == (int)ChargeStatusList.DMSDocumentCorrection)
                    {
                        errorCode = -12;
                        errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg12;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg12");
                        return errorCode;
                    }
                    else
                    {
                        //summonsEntity.SummonsStatus = (int)ChargeStatusList.DMSDocumentCorrection;
                        summonsEntity.LastUser = LastUser;
                        summonsService.Update(summonsEntity);

                        #region EvidencePack
                        AddEvidencePack(noticeEntity.NotIntNo, AdministrativeCorrections_cshtml.lblAC_ReturnedToDMS);//this.ResourceValue("AdministrativeCorrections", "lblAC_ReturnedToDMS")
                        #endregion

                        TList<SumCharge> sumChargeList = sumChargeService.GetBySumIntNo(summonsEntity.SumIntNo);
                        foreach (var sumCharge in sumChargeList)
                        {
                            //sumCharge.SumChargeStatus = (int)ChargeStatusList.DMSDocumentCorrection;
                            sumCharge.LastUser = LastUser;
                            sumChargeService.Update(sumCharge);
                        }

                        //noticeEntity.NoticeStatus = (int)ChargeStatusList.DMSDocumentCorrection;
                        noticeEntity.LastUser = LastUser;
                        noticeEntity.IsHwoCorrection = true;
                        noticeService.Update(noticeEntity);

                        TList<Charge> chargeEntityList = chargeService.GetByNotIntNo(noticeEntity.NotIntNo);
                        foreach (var charge in chargeEntityList)
                        {
                           //charge.ChargeStatus = (int)ChargeStatusList.DMSDocumentCorrection;
                            charge.LastUser = LastUser;
                            chargeService.Update(charge);
                        }

                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.CorrectionsOfHandwritten, PunchAction.Add);
                        if (string.IsNullOrEmpty(hwoImportedWithErrorsEntity.BaDoId) || string.IsNullOrEmpty(hwoImportedWithErrorsEntity.BatchId))
                        {
                            errorCode = -22;
                            errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg22;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg22");
                            return errorCode;
                        }
                        else
                        {
                            //update DMS 
                            UpdateDMS(hwoImportedWithErrorsEntity.BatchId.Trim(), hwoImportedWithErrorsEntity.BaDoId.Trim(), errorReason);
                        }

                    }

                    #endregion
                }

                return errorCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int ReceiptReverse(HwoImportedWithErrors hwoImportedWithErrorsEntity, out string errorMsg)
        {
            int errorCode = 0;
            errorMsg = "";
            string receiptReversedReason = AdministrativeCorrections_cshtml.ReceiptReversedReason;//this.ResourceValue("AdministrativeCorrections", "ReceiptReversedReason");
            try
            {

                noticeEntity = noticeService.GetByNotIntNo(hwoImportedWithErrorsEntity.NotIntNo);

                #region Receipt reversed

                cashReceiptDB = new CashReceiptDB(connectionString);
                string reversed = "";
                int rctIntNo = cashReceiptDB.GetReceiptByNotIntNo(noticeEntity.NotIntNo, ref reversed);
                if (rctIntNo == 0)
                {
                    errorCode = -24;
                    errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg24;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg24");
                    return errorCode;
                }
                if (reversed == "Y")
                {
                    errorCode = -23;
                    errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg23;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg23");
                    return errorCode;
                }

                KeyValuePair<string, int> value = cashReceiptDB.ProcessAdminReceiptReversal(rctIntNo, LastUser, receiptReversedReason, AutIntNo);
                if (value.Key.Length > 0 && value.Value <= 0)
                {
                    errorCode = -13;
                    errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg13;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg13");
                    return errorCode;
                    
                }
                // Jake 2013-12-02 add display error message from sp
                if (value.Key == "" && value.Value == -20)
                {
                    errorCode = -14;
                    errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg14;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg14");
                    return errorCode;
                }

                #endregion

                return errorCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int AddNewReceipt(HwoImportedWithErrors hwoImportedWithErrorsEntity, decimal correctfineAmout,int userIntNo,out string errorMsg)
        { 
            int errorCode = 0;
            errorMsg = "";
            DateTime ReceiptDate = DateTime.Now;
            decimal totalAmount = 0;
            decimal contemptCourtAmount = 0;

            try
            {
                noticeEntity = noticeService.GetByNotIntNo(hwoImportedWithErrorsEntity.NotIntNo);

                #region add new Receipt
                GetNoOfDaysForPayment(AutIntNo);
                totalAmount = correctfineAmout;
                //contemptCourtAmount=
                ReceiptTransaction receipt = GenerateReceipt_AdministrativeCorrection(contemptCourtAmount, totalAmount,ReceiptDate, AutIntNo);
                int trhIntNo = 0;
                string errorMessage = "";
                bool header = false;
                trhIntNo = cashReceiptDB.AddReceiptToPaymentList(receipt, ReceiptDate, trhIntNo, ref errorMessage, header, LastUser, userIntNo);
                if (trhIntNo > 0)
                {
                    DataSet dsReceiptPayments = cashReceiptDB.GetReceiptPaymentListDS(trhIntNo);
                    int errorFlag = AddNoticesForPayment_AdministrativeCorrection(noticeEntity, totalAmount, chargeService, trhIntNo);
                    if (errorFlag < 0)
                    {
                        if (errorFlag == -1)
                        {
                            errorCode = -16;
                            errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg16;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg16");
                            return errorCode;
                        }
                        if (errorFlag == -1)
                        {
                            errorCode = -17;
                            errorMsg = AdministrativeCorrections_cshtml.lblErrorMsg17;//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg17");
                            return errorCode;
                        }

                    }
                    else
                    {
                        header = true;
                        errorMessage = "";
                        int updTRHIntNo = cashReceiptDB.AddReceiptToPaymentList(receipt, ReceiptDate,
                                          trhIntNo, ref errorMessage, header, LastUser, userIntNo);
                        if (updTRHIntNo < 0)
                        {
                            errorCode = -15;
                            errorMsg = string.Format(AdministrativeCorrections_cshtml.lblErrorMsg15, errorMessage); //this.ResourceValue("AdministrativeCorrections", "lblErrorMsg15", errorMessage);
                            return errorCode;
                        }
                        else
                        {
                            errorMessage = "";
                            // Payment
                            List<ReceiptList> receipts = new List<ReceiptList>();
                            errorMessage = "";
                            receipts = cashReceiptDB.CreateReceiptFromMultiplePayments(trhIntNo, ref errorMessage, _noOfDaysForPayment, null, false, ((int)PaymentOriginatorList.OPUS).ToString());
                            if (receipts == null || receipts[0].rctIntNo <= 0)
                            {
                                errorCode = -18;
                                errorMsg = string.Format(AdministrativeCorrections_cshtml.lblErrorMsg18, errorMessage);//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg18", errorMessage);
                                return errorCode;

                            }
                            else
                            {
                                var rec = receiptService.GetByRctIntNo(receipts[0].rctIntNo);
                                rec.PaymentOriginator = PaymentOriginatorList.OPUS.ToString();
                                //rec.ServiceProvider = noticePaymentRequest.NoticePaymentHeader.ServiceProvider;
                                //rec.PayPointDescription = noticePaymentRequest.NoticePaymentRequestBody.PayPointDescription;
                                //rec.PayPointIdentification = noticePaymentRequest.NoticePaymentRequestBody.PayPointIdentification;
                                //rec.PayPointType = noticePaymentRequest.NoticePaymentRequestBody.PayPointType;
                                //rec.ServiceProviderReference = noticePaymentRequest.NoticePaymentRequestBody.ServiceProviderReference;
                                rec.RctDate = ReceiptDate;
                                this.receiptService.Save(rec);

                                //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                                //punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.PaymentTraffic);
                            }

                        }
                    }
                }
                else
                {
                    errorCode = -15;
                    errorMsg = string.Format(AdministrativeCorrections_cshtml.lblErrorMsg15, errorMessage);//this.ResourceValue("AdministrativeCorrections", "lblErrorMsg15", errorMessage);
                    return errorCode;
                }

                #endregion

                return errorCode;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void GetNoOfDaysForPayment(int autIntNo)
        {
            AuthorityRulesDetails arule = new AuthorityRulesDetails();
            arule.AutIntNo = autIntNo;
            arule.ARCode = "4660";
            arule.LastUser = LastUser;

            DefaultAuthRules paymentRule = new DefaultAuthRules(arule, connectionString);
            KeyValuePair<int, string> payment = paymentRule.SetDefaultAuthRule();

            _noOfDaysForPayment = payment.Key;
        }

        private void AddEvidencePack(int NotIntNo,string descr)
        {
            #region insert EvidencePack

            TList<Charge> charges = chargeService.GetByNotIntNo(NotIntNo);
            List<Charge> chargeList = charges.Where(m => m.ChgSequence == 1).ToList();
            if (chargeList != null && chargeList.Count > 0)
            {
                EvidencePack ep = new EvidencePack();
                ep.ChgIntNo = chargeList[0].ChgIntNo;
                ep.EpItemDate = DateTime.Now;
                ep.EpItemDescr = descr;// this.ResourceValue("AdministrativeCorrections", "lblEvidencePackDesc");
                ep.EpSourceTable = "Notice";
                ep.EpSourceId = NotIntNo;
                ep.LastUser = LastUser;
                ep.EpTransactionDate = DateTime.Now;
                ep.EpItemDateUpdated = false;
                ep.EpatIntNo = (int)EpActionTypeList.Others;
                new EvidencePackService().Insert(ep);
            }

            #endregion
        }

        ReceiptTransaction GenerateReceipt_AdministrativeCorrection(decimal contempAmount, decimal totalAmount, DateTime ReceivedDate, int autIntNo)
        {
            ReceiptTransaction receipt = new ReceiptTransaction();
            DateTime expiryDate = new DateTime(2079, 01, 01);
            receipt = new CardReceipt();
            receipt.CashType = CashType.CreditCard;
            ((CardReceipt)receipt).CardIssuer = PaymentTypeList.CreditCard.ToString();
            ((CardReceipt)receipt).NameOnCard = "";
            ((CardReceipt)receipt).ExpiryDate = expiryDate;

            receipt.CashType = CashType.CreditCard;
            receipt.Details = "Payment";
            receipt.ReceivedDate = ReceivedDate;
            receipt.Amount = totalAmount;
            //receipt.ContemptAmount = contempAmount;
            receipt.AutIntNo = autIntNo;
            receipt.MaxStatus = 900;
            return receipt;

        }

        int AddNoticesForPayment_AdministrativeCorrection(SIL.AARTO.DAL.Entities.Notice notice, decimal totalAmount, ChargeService chargeService, int trhIntNo)
        {
            int errFlag = 0;
            string errMessage = "";
            AccountDB accountCharge = new AccountDB(connectionString);

            List<SIL.AARTO.DAL.Entities.Charge> chgList = new List<SIL.AARTO.DAL.Entities.Charge>();
            TList<Charge> charges = chargeService.GetByNotIntNo(notice.NotIntNo);
            int accIntNo = 0;

            var noticeList = new List<TempReceiptNotice>();

            if (notice != null && charges != null && charges.Count > 0)
            {
                chgList = charges.Where(c => c.ChargeStatus < (int)ChargeStatusList.Completion && c.ChgIsMain).ToList();

                foreach (SIL.AARTO.DAL.Entities.Charge chg in chgList)
                {
                    accIntNo = accountCharge.GetAccount(chg.CtIntNo);
                    if (accIntNo <= 0)
                    {
                        errFlag = -1;
                        return errFlag;
                    }

                    noticeList.Add(new TempReceiptNotice
                    {
                        NotIntNo = notice.NotIntNo,
                        ChgIntNo = chg.ChgIntNo,
                        CTIntNo = chg.CtIntNo,
                        AccIntNo = accIntNo,
                        ChgRevFineAmount = Convert.ToDecimal(chg.ChgRevFineAmount, CultureInfo.InvariantCulture),
                        ContemptAmount = chg.ChgContemptCourt,
                        AmountToPay = totalAmount,
                        SeniorOfficer = ""
                    });
                };
            }

            int updTRHIntNo = cashReceiptDB.AddNoticesToReceiptList(trhIntNo, "", 0, ref errMessage, noticeList);

            if (updTRHIntNo < 0)
            {
                errFlag = -2;
                return errFlag;
            }

            return errFlag;
        }

        public List<SelectListItem> GetCourtSelectlist()
        {
            List<SelectListItem> itemList = new List<SelectListItem>();
            courtdb = new CourtDB(connectionString);
            SqlDataReader reader = courtdb.GetAuth_CourtListByAuth(AutIntNo);
            while (reader.Read())
            {
                itemList.Add(new SelectListItem { Text = reader["CrtName"].ToString(), Value = reader["CrtIntNo"].ToString() });
            }
            itemList.Insert(0, new SelectListItem { Text = AdministrativeCorrections_cshtml.lblSelectCourt, Value = "" });//this.ResourceValue("AdministrativeCorrections", "lblSelectCourt")
            reader.Close();
            return itemList;

        }

    }
}
