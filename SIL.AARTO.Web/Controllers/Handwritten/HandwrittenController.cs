﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.ViewModels.Handwritten;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.DAL.Entities;
using System.Data;
using System.Transactions;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.Handwritten
{
    [AARTOErrorLog, LanguageFilter]
    public partial class HandwrittenController : Controller
    {
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
        //
        // GET: /Handwritten/

        public ActionResult Corrections(string searchStr = "", int page = 0, string msg = "")
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            HandwrittenCorrectionsModel model = new HandwrittenCorrectionsModel();
            if (searchStr != "")
            {
                model.SearchStr = searchStr.Trim();
                model.PageIndex = page;
                model.ErrorMsg = msg;
                InitModel(model);
            }
            else
            {
                List<HandwrittenCorrectionsListModel> correctionListModelList = new List<HandwrittenCorrectionsListModel>();
                model.PageCorrectionsList = correctionListModelList;
            }
            
            return View(model);
        }

        public JsonResult SubmitReason(string searchStr, string txtReason, string isNotice)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];

            //Jerry 2013-04-17 add transaction scope
            using (TransactionScope scope = new TransactionScope())
            {
                // Oscar 2013-05-20 added for check rep before correction
                var checkRep = CheckRepresentationBeforeCorrection(searchStr.Trim(), isNotice == "1");
                if (checkRep < 0)
                    return Json(new { result = checkRep });

                if (isNotice == "1")
                {
                    SIL.AARTO.DAL.Entities.Notice noticeEntity = new SIL.AARTO.DAL.Services.NoticeService().GetByNotTicketNo(searchStr.Trim()).FirstOrDefault();
                    if (noticeEntity != null)
                    {
                        if (noticeEntity.NoticeStatus == (int)ChargeStatusList.DMSDocumentCorrection)
                        {
                            return Json(new { result = -1 });
                        }
                        else
                        {
                            try
                            {
                                noticeEntity.NoticeStatus = (int)ChargeStatusList.DMSDocumentCorrection;
                                noticeEntity.LastUser = userInfo.UserName;
                                noticeEntity.IsHwoCorrection = true;
                                new SIL.AARTO.DAL.Services.NoticeService().Update(noticeEntity);
                                TList<Charge> chargeEntityList = new SIL.AARTO.DAL.Services.ChargeService().GetByNotIntNo(noticeEntity.NotIntNo);
                                foreach (var charge in chargeEntityList)
                                {
                                    charge.ChargeStatus = (int)ChargeStatusList.DMSDocumentCorrection;
                                    charge.LastUser = userInfo.UserName;
                                    new SIL.AARTO.DAL.Services.ChargeService().Update(charge);
                                }

                                //update DMS 
                                UpdateDMS(noticeEntity.NotFilmNo.Trim(), noticeEntity.NotFrameNo.Trim(), txtReason.Trim());
                             
                                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.CorrectionsOfHandwritten, PunchAction.Add);  

                            }
                            catch (Exception ex)
                            {
                                return Json(new { result = -3, msg = ex.Message });
                            }
                        }
                    }
                }
                else if (isNotice == "0")
                {
                    SIL.AARTO.DAL.Entities.Summons summonsEntity = new SIL.AARTO.DAL.Services.SummonsService().GetBySummonsNo(searchStr.Trim()).FirstOrDefault();
                    if (summonsEntity != null)
                    {
                        if (summonsEntity.SummonsStatus == (int)ChargeStatusList.DMSDocumentCorrection)
                        {
                            return Json(new { result = -1 });
                        }
                        else
                        {
                            try
                            {
                                summonsEntity.SummonsStatus = (int)ChargeStatusList.DMSDocumentCorrection;
                                summonsEntity.LastUser = userInfo.UserName;
                                new SIL.AARTO.DAL.Services.SummonsService().Update(summonsEntity);
                                TList<SumCharge> sumChargeList = new SIL.AARTO.DAL.Services.SumChargeService().GetBySumIntNo(summonsEntity.SumIntNo);
                                foreach (var sumCharge in sumChargeList)
                                {
                                    sumCharge.SumChargeStatus = (int)ChargeStatusList.DMSDocumentCorrection;
                                    sumCharge.LastUser = userInfo.UserName;
                                    new SIL.AARTO.DAL.Services.SumChargeService().Update(sumCharge);
                                }

                                SIL.AARTO.DAL.Entities.NoticeSummons noticeSummons = new SIL.AARTO.DAL.Services.NoticeSummonsService().GetBySumIntNo(summonsEntity.SumIntNo).FirstOrDefault();
                                if (noticeSummons != null)
                                {
                                    SIL.AARTO.DAL.Entities.Notice noticeEntity = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(noticeSummons.NotIntNo);
                                    noticeEntity.NoticeStatus = (int)ChargeStatusList.DMSDocumentCorrection;
                                    noticeEntity.LastUser = userInfo.UserName;
                                    noticeEntity.IsHwoCorrection = true;
                                    new SIL.AARTO.DAL.Services.NoticeService().Update(noticeEntity);
                                    TList<Charge> chargeEntityList = new SIL.AARTO.DAL.Services.ChargeService().GetByNotIntNo(noticeEntity.NotIntNo);
                                    foreach (var charge in chargeEntityList)
                                    {
                                        charge.ChargeStatus = (int)ChargeStatusList.DMSDocumentCorrection;
                                        charge.LastUser = userInfo.UserName;
                                        new SIL.AARTO.DAL.Services.ChargeService().Update(charge);
                                    }
                                }

                                //update DMS 
                                UpdateDMS(summonsEntity.SumFilmNo.Trim(), summonsEntity.SumFrameNo.Trim(), txtReason.Trim());
                                
                                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.CorrectionsOfHandwritten, PunchAction.Add);  
                            }
                            catch (Exception ex)
                            {
                                return Json(new { result = -3, msg = ex.Message });
                            }
                        }
                    }
                }

                scope.Complete();
            }

            return Json(new { result = 0 });
        }

        private void InitModel(HandwrittenCorrectionsModel model)
        {
            int totalCount = 0;
            model.TotalCount = totalCount;
            model.PageSize = Config.PageSize;

            List<HandwrittenCorrectionsListModel> correctionListModelList = new List<HandwrittenCorrectionsListModel>();
            HandwrittenCorrectionsListModel correctionListModel;

            IDataReader reader= new SIL.AARTO.DAL.Services.NoticeService().GetForHandwrittenCorrection(model.SearchStr);
            while (reader.Read())
            {
                correctionListModel = new HandwrittenCorrectionsListModel();
                correctionListModel.FilmNo = reader["BatchID"].ToString();
                correctionListModel.FrameNo = reader["DocumentID"].ToString();
                correctionListModel.NoticeTicketNoOrSummonsNo = reader["SearchNum"].ToString();
                model.IsNotice = reader["IsNotice"].ToString();
                correctionListModelList.Add(correctionListModel);
            }
            //if (noticeList.Count <=0)
            //{
            //    TList<SIL.AARTO.DAL.Entities.Summons> summonsList = new SIL.AARTO.DAL.Services.SummonsService().GetPaged(" SummonsNo = " + model.SearchStr + " And SummonsStatus < 710" + " And SumType = 'S56'", "SumIntNo", model.PageIndex, model.PageSize, out totalCount);
            //    foreach (var summons in summonsList)
            //    {
            //        correctionListModel = new HandwrittenCorrectionsListModel();
            //        correctionListModel.FilmNo = summons.SumFilmNo;
            //        correctionListModel.FrameNo = summons.SumFrameNo;
            //        correctionListModel.NoticeTicketNoOrSummonsNo = summons.SummonsNo;

            //        correctionListModelList.Add(correctionListModel);
            //    }
            //}
            model.TotalCount = totalCount;
            model.PageCorrectionsList = correctionListModelList;
            if (correctionListModelList.Count <= 0)
            {
                model.ErrorMsg = SIL.AARTO.Web.Resource.Handwritten.Corrections.NoDataMsg;
            }
            else
            {
                model.ErrorMsg = "";
            }
        }

        private void UpdateDMS(string batchID, string batchDocID, string reasonMsg)
        {
            //Insert err msg data into DMS
            SIL.DMS.DAL.Entities.DocumentErrorMessage docErrMsg = new DMS.DAL.Entities.DocumentErrorMessage();
            docErrMsg.BatchId = batchID;
            docErrMsg.BaDoId = batchDocID;
            // Paole 20121031  for display the category of error message on DMS QC page, temporary proposal， not checked by DEV
            docErrMsg.DoErMeCode = "HW Correction reason";
            docErrMsg.DoErMessage = reasonMsg;
            docErrMsg.DoErMeDateCreated = DateTime.Now;
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            docErrMsg.LastUser = userInfo.UserName;
            new SIL.DMS.DAL.Services.DocumentErrorMessageService().Save(docErrMsg);

            //update document table
            SIL.DMS.DAL.Entities.BatchDocument batchDoc = new SIL.DMS.DAL.Services.BatchDocumentService().GetByBaDoId(batchDocID);
            batchDoc.BaDoStId = (int)SIL.DMS.DAL.Entities.BatchDocumentStatusList.DocumentReturnedToQC;
            batchDoc.IsBackFromAarto = true;
            // 2013-07-16 add by Henry for LastUser
            batchDoc.LastUser = userInfo.UserName;
            new SIL.DMS.DAL.Services.BatchDocumentService().Update(batchDoc);
        }

        // Oscar 2013-05-20 added for check rep before correction
        int CheckRepresentationBeforeCorrection(string number, bool isNotice)
        {
            bool repLogged = false, repDecided = false;

            try
            {
                var paras = new[]
                {
                    new SqlParameter("@NotTicketNoOrSummonsNo", number),
                    new SqlParameter("@IsNotice", isNotice)
                };

                var connStr = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
                using (var conn = new SqlConnection(connStr))
                {
                    using (var cmd = new SqlCommand("CheckRepresentationBeforeCorrection", conn) { CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.AddRange(paras);
                        conn.Open();
                        using (var dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                int repCode;

                                if (dr["RepCode"] != DBNull.Value
                                    && int.TryParse(Convert.ToString(dr["RepCode"]), out repCode))
                                {
                                    if (repCode == 0 || repCode == 10)
                                        repLogged = true;
                                    else if (repCode > 0 && repCode != 99)
                                        repDecided = true;
                                }
                            }
                        }
                        cmd.Parameters.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                
            }

            if (repDecided)
                return -11;
            if (repLogged)
                return -10;
            return 0;
        }
    }
}
