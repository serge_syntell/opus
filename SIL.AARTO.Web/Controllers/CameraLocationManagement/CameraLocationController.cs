﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Configuration;
using SIL.AARTO.Web.Helpers.Paginator;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.CameraLocationManagement.Model;
using SIL.AARTO.BLL.CameraLocationManagement;
using resource=SIL.AARTO.Web.Resource.CameraLocationManagement;
using utility=SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.Printing;


namespace SIL.AARTO.Web.Controllers
{
    public class CameraLocationManagementController : Controller
    {
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        public string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as utility.UserLoginInfo).UserName : ""; }
        }
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((utility.UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
        //
        // GET: /CameraLocation/

        public ActionResult CameraLocation(CameraLocationModel model)
        {
            utility.UserLoginInfo userLofinInfo = (utility.UserLoginInfo)Session["UserLoginInfo"];
            if (userLofinInfo != null)
                InitData(model, userLofinInfo);
            else
                return RedirectToAction("login", "account");

            return View(model);
        }

        private void InitData(CameraLocationModel model, utility.UserLoginInfo userInfo)
        {
            CameraLocationBLL cl = new CameraLocationBLL();
            model.AutList = GetAutList(cl.GetAuthority());
            model.LocList = GetLocationList(cl.GetLocationEntityByAutIntNo(model.AutSearchSelected));
            model.CameraList = GetCameraSelectList(cl.GetCameras());

            SearchStruct searchStruct = new SearchStruct();
            searchStruct.AutIntNo = model.AutSearchSelected;
            searchStruct.LocIntNo = model.LocSearchSelected;
            searchStruct.CameraSerialNo = model.CameraSearch;
            searchStruct.LastUser = userInfo.UserName;
            searchStruct.SiteCode = model.SearchSiteCode;

            if (string.IsNullOrEmpty(model.LocationShowModel))
                model.LocationShowModel = "Camera";

            int total = 0;
            model.PageSize = int.Parse(ConfigurationManager.AppSettings["PageSize"]);

            if (model.LocationShowModel == "Camera")
            {
                model.Grid = cl.GetSelfPage(searchStruct, model.Page, model.PageSize, ref total);
            }
            else
            {
                model.SiteList = cl.GetLocationSitePage(searchStruct, model.Page, model.PageSize, ref total);
            }


            model.TotalCount = total;
        }

        public JsonResult SaveLocationCamera(int locIntNo,int camIntNo)
        {
            utility.UserLoginInfo userLofinInfo = (utility.UserLoginInfo)Session["UserLoginInfo"];
            string lastUser = (userLofinInfo != null ? userLofinInfo.UserName : "");
            Message message = new Message();
            CameraLocationBLL cl = new CameraLocationBLL();
            try
            {
                if (locIntNo != -1 && camIntNo != -1)
                {
                    bool IsAdd = cl.CheckIsAdd(locIntNo, camIntNo);
                    if (!IsAdd)
                    {
                        cl.SaveCameraLocation(locIntNo, camIntNo, lastUser);
                        message.Status = true;
                       
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.CameraLocation, PunchAction.Add);  

                    }
                    else
                    {
                        message.Data = resource.CameraLocation.Exists_Text;
                    }
                }
                else
                {
                    message.Data = resource.CameraLocation.MustSelect_Text;
                }
            }
            catch (Exception ex)
            {
                message.Data = ex.Message;
            }
            return Json(message);
        }
        public JsonResult SaveLocationSite(int LSIntNo, int locIntNo, string siteCode, string SiteDesc)
        {
            utility.UserLoginInfo userLofinInfo = (utility.UserLoginInfo)Session["UserLoginInfo"];
            string lastUser = (userLofinInfo != null ? userLofinInfo.UserName : "");
            Message message = new Message();
            CameraLocationBLL cl = new CameraLocationBLL();
            try
            {
                if (locIntNo == -1 || string.IsNullOrEmpty(siteCode) || string.IsNullOrEmpty(SiteDesc))
                {
                    message.Data = resource.CameraLocation.SiteMustError_Text;
                }
                else
                {
                    bool isAdd = cl.CheckIsAddSiteCode(LSIntNo, locIntNo,siteCode);
                    if (!isAdd)
                    {
                        cl.SaveLocationSite(LSIntNo, locIntNo, siteCode, SiteDesc, lastUser);
                        message.Status = true;

                        if (LSIntNo > 0)
                        {
                            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.LocationSite, PunchAction.Change);
                        }
                        else
                        {
                            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.LocationSite, PunchAction.Add);
                        }
                    }
                    else
                    {
                        message.Data = resource.CameraLocation.Exists_Text;
                    }
                }
            }
            catch (Exception ex)
            {
                message.Data = ex.Message;
            }
            return Json(message);
        }
        public JsonResult DeleteCameraLocation(int intNo)
        {
            CameraLocationBLL cl = new CameraLocationBLL();
            Message message = new Message();
            try
            {
                cl.DeleteCameraLocation(intNo);
                message.Status = true;
              
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.CameraLocation, PunchAction.Delete);
            }
            catch (Exception ex)
            {
                message.Data = ex.Message;
            }
            return Json(message);
        }
        public JsonResult GetLocationSite(int intNo)
        {
            CameraLocationBLL cl = new CameraLocationBLL();
            LocationSite site = new LocationSite();
            Message message = new Message();
            try
            {
                message.Data = CreatJSONStr(cl.GetLocationSite(intNo));
                message.Status = true;
            }
            catch (Exception ex)
            {
                message.Data = ex.Message;
            }
            return Json(message);
        }
        public string CreatJSONStr(LocationSiteExtend site)
        {
            StringBuilder sBuild = new StringBuilder();
            sBuild.Append("[");
            sBuild.Append("{" + string.Format("name:'{0}',value:\"{1}\"", "LSIntNo", site.LsIntNo) + "},");
            sBuild.Append("{" + string.Format("name:'{0}',value:\"{1}\"", "AutSelected", site.AutIntNo) + "},");
            sBuild.Append("{" + string.Format("name:'{0}',value:\"{1}\"", "LocSelected", site.LocIntNo) + "},");
            sBuild.Append("{" + string.Format("name:'{0}',value:\"{1}\"", "LSSiteCode", site.LsSiteCode) + "},");
            sBuild.Append("{" + string.Format("name:'{0}',value:\"{1}\"", "LSSiteDescription", site.LsSiteDescription) + "}");

            sBuild.Append("]");

            return sBuild.ToString();
        }
        public JsonResult DeleteLocationSite(int intNo)
        {
            CameraLocationBLL cl = new CameraLocationBLL();
            Message message = new Message();
            try
            {
                cl.DeleteLocationSite(intNo);
                message.Status = true;
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.LocationSite, PunchAction.Delete);
            }
            catch (Exception ex)
            {
                message.Data = ex.Message;
            }
            return Json(message);
        }
        public JsonResult GetLocation(int autIntNo)
        {
            CameraLocationBLL cl = new CameraLocationBLL();
            Message message = new Message();
            StringBuilder sBuilder = new StringBuilder();
            string result = string.Empty;
            try
            {
                sBuilder.Append("-1|" + resource.CameraLocation.Select_Text + "$");
                if (autIntNo > 0)
                {
                    List<LocationEntity> list = cl.GetLocationEntityByAutIntNo(autIntNo);
                    foreach (var item in list)
                    {
                        if (!string.IsNullOrEmpty(item.LocDescr))
                            sBuilder.Append(item.LocIntNo + "|" + item.LocDescr + "$");
                    }
                }
                result = sBuilder.ToString();
                message.Data = result.Substring(0, result.Length - 1);
                message.Status = true;
            }
            catch (Exception ex)
            {
                message.Data = ex.Message;
            }

            return Json(message);
        }
        public JsonResult GetCameraSerialNos(string key,int pageLength)
        {
            CameraLocationBLL cl = new CameraLocationBLL();
            Message message = new Message();
            StringBuilder sBuilder = new StringBuilder();
            string result = string.Empty;
            if (!string.IsNullOrEmpty(key))
            {
                List<Camera> list = cl.GetCameras(key,pageLength);
                sBuilder.Append("[");
                foreach (var item in list)
                {
                    sBuilder.Append("\"" + item.CamSerialNo + "\",");
                }
                if (sBuilder.Length > 1)
                    sBuilder.Remove(sBuilder.Length-1, 1);
                sBuilder.Append("]");
                message.Data = sBuilder.ToString();
                message.Status = true;
            }
            return Json(message);
        }
        public SelectList GetAutList(List<Authority> list)
        {
            SelectList selectList;
            List<SelectListItem> itemList=new List<SelectListItem>();
            itemList.Add(new SelectListItem { Text =resource.CameraLocation.Select_Text , Value = "-1" });
            foreach (Authority item in list)
            {
                itemList.Add(new SelectListItem { Text = item.AutName, Value = item.AutIntNo.ToString() });
            }
            selectList = new SelectList(itemList, "Value", "Text");
            return selectList;
        }
        public SelectList GetLocationList(List<LocationEntity> list)
        {
            SelectList selectList;
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Add(new SelectListItem { Text = resource.CameraLocation.Select_Text, Value = "-1" });
            foreach (LocationEntity item in list)
            {
                if (!string.IsNullOrEmpty(item.LocDescr))
                    itemList.Add(new SelectListItem { Text = item.LocDescr, Value = item.LocIntNo.ToString() });
            }
            selectList = new SelectList(itemList, "Value", "Text");
            return selectList;
        }
        public SelectList GetCameraSelectList(List<Camera> list)
        {
            SelectList selectList;
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Add(new SelectListItem { Text = resource.CameraLocation.Select_Text, Value = "-1" });
            foreach (var item in list)
            {
                itemList.Add(new SelectListItem { Text = item.CamSerialNo, Value = item.CamIntNo.ToString() });
            }
            selectList = new SelectList(itemList, "Value", "Text");
            return selectList;
        }

    }
}
