﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using System.IO;
using Stalberg.TMS;
using Stalberg.TMS.Data;
using System.Configuration;
using SIL.AARTO.BLL.Culture;
using Stalberg.TMS.Data.Util;
using System.Drawing.Imaging;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.CameraManagement;
using SIL.AARTO.Web.ViewModels.CameraManagement;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.CameraManagement
{
    public partial class CameraManagementController : Controller
    {
        public ActionResult Image()
        {
            try
            {
                using (Bitmap bitmap = ImagePreProcess())
                {
                    HandleImage(bitmap);
                    if (!string.IsNullOrEmpty(HttpUtility.UrlDecode(Request.QueryString["Invert"])) && HttpUtility.UrlDecode(Request.QueryString["Invert"]) == "1")
                    {
                        Invert(bitmap);
                    }
                    using (MemoryStream ms = new MemoryStream())
                    {
                        bitmap.Save(ms, ImageFormat.Jpeg);
                        return File(ms.ToArray(), "image/jpeg");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult ImagePartialView(bool showPreView)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            ImagePartialViewModel model = new ImagePartialViewModel()
            {
                UserInfo = (UserLoginInfo)Session["UserLoginInfo"],
                FilmIntNo = Convert.ToInt32(Session["filmIntNo"]),
                FrameIntNo = ((FrameList)this.Session["FrameList"]).Current,
                ImageType = "A",
                Phase = this.Session["cvPhase"] == null ? 1 : Convert.ToInt32(this.Session["cvPhase"]),
                ScanImageIntNo = 0,
                CrossHairStyle = 0
            };
            InitModel(model, showPreView);
            return PartialView();
        }

        public JsonResult SaveImageSettings(decimal contrast, decimal brightness)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 }, JsonRequestBehavior.AllowGet);
            }

            return Json(new
            {
                result = 0,
                data = new
                {
                    returnNo = SaveImageSet(contrast, brightness)
                }
            },
            JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveImageSettingsToSession(decimal contrast, decimal brightness)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 }, JsonRequestBehavior.AllowGet);
            }

            Session["SaveImageSettingsForFilm"] = true;
            Session["SavedBrightness"] = brightness;
            Session["SavedContrast"] = contrast;

            return Json(new
            {
                result = 0,
                data = new
                {
                    returnNo = Session["SaveImageSettingsForFilm"] ?? false
                }
            },
            JsonRequestBehavior.AllowGet);
        }

        public JsonResult ClearSaveImageSettingsSession()
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 }, JsonRequestBehavior.AllowGet);
            }
            Session["SaveImageSettingsForFilm"] = null;
            return Json(new { result = 0 }, JsonRequestBehavior.AllowGet);
        }


        private int SaveImageSet(decimal contrast, decimal brightness)
        {
            int isSuccess = 0;//2013-12-02 Heidi changed for add all Punch Statistics Transaction(5084)
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            isSuccess = ImageManager.UpdateImageSetting((int)Session["scImIntNo"],
                contrast,
                brightness,
                userInfo.UserName);
            if (isSuccess > 0)
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.Adjudication, PunchAction.Change);

            }
            return isSuccess;
        }

        private Bitmap ImagePreProcess()
        {
            Bitmap image = null;
            string jpegName = HttpUtility.UrlDecode(Request.QueryString["JpegName"]);
            byte[] data = null;
            if (!String.IsNullOrEmpty(Request.QueryString["IFSIntNo"]))
            {
                int ifsIntNo = Convert.ToInt32(HttpUtility.UrlDecode(Request.QueryString["IFSIntNo"]));
                // get test image from ImageFileServer
                ImageFileServerDB ifsDB = new ImageFileServerDB(Config.ConnectionString);
                ImageFileServerDetails testServer = ifsDB.GetImageFileServer(ifsIntNo);

                //if (service.RemoteConnect(testServer.ImageMachineName, testServer.ImageShareName, testServer.RemoteUserName, testServer.RemotePassword))
                //{
                string strTestImagePath = string.Format(@"\\{0}\{1}\test.jpg", testServer.ImageMachineName, testServer.ImageShareName);

                if (!System.IO.File.Exists(strTestImagePath))
                    strTestImagePath = string.Format(@"\\{0}\{1}\test.jpg", testServer.ImageMachineName, testServer.ImageShareName);

                System.Drawing.Image imageConvert = System.Drawing.Image.FromFile(strTestImagePath);
                image = new Bitmap(imageConvert);
                imageConvert.Dispose();
                //}
            }
            else
            {
                string strImgType = HttpUtility.UrlDecode(Request.QueryString["ImgType"]);
                Dictionary<string, byte[]> imageSession = (Dictionary<string, byte[]>)Session["Image" + strImgType];
                ImageProcesses img = new ImageProcesses(Config.ConnectionString);
                try
                {
                    if (imageSession != null && imageSession.Keys.Contains(jpegName))
                    {
                        data = imageSession[jpegName];
                    }
                    else
                    {
                        //data = img.ProcessImage(Convert.ToInt32(HttpUtility.UrlDecode(Request.QueryString["ScImIntNo"])), checkFileSystemForImages, Server.MapPath(imageFolder + "/" + HttpUtility.UrlDecode(Request.QueryString["FilmNo"])), jpegName, Server.MapPath("images/NoImage.jpg"));                        
                        ScanImageDB imageDB = new ScanImageDB(Config.ConnectionString);
                        int intImageNo = Convert.ToInt32(HttpUtility.UrlDecode(Request.QueryString["ScImIntNo"]));
                        ScanImageDetails imageDetail = imageDB.GetImageFullPath(intImageNo);
                        if (imageDetail.FileServer != null)
                        {
                            //if (service.RemoteConnect(imageDetail.FileServer.ImageMachineName, imageDetail.FileServer.ImageShareName, imageDetail.FileServer.RemoteUserName, imageDetail.FileServer.RemotePassword))
                            //{
                            data = img.ProcessImage(imageDetail.ImageFullPath);
                            //}
                        }

                        if (jpegName != null)
                            imageSession.Add(jpegName, data);
                    }

                    MemoryStream ms = new MemoryStream(data);
                    System.Drawing.Image imageConvert = System.Drawing.Image.FromStream(ms);
                    image = new Bitmap(imageConvert);
                    imageConvert.Dispose();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine(ex);
                    data = null;
                }
                finally
                {

                    if (data == null || data.Length == 0)
                    {
                        string strNoImagePath = AppDomain.CurrentDomain.BaseDirectory + "/Content/image/NoImage.jpg";
                        System.Drawing.Image imgNoImage = System.Drawing.Image.FromFile(strNoImagePath);
                        image = new Bitmap(imgNoImage);
                        imgNoImage.Dispose();
                    }
                }
            }
            return image;
        }

        private void HandleImage(Bitmap bitmap)
        {
            bool result = false;
            float contrast;

            if (Request.QueryString["SavedContrast"] != null && !Request.QueryString["SavedContrast"].ToString().Equals("99"))
            {
                result = float.TryParse(HttpUtility.UrlDecode(Request.QueryString["SavedContrast"]), out contrast);
                Session["SavedContrast"] = HttpUtility.UrlDecode(Request.QueryString["SavedContrast"]);
            }
            else
                result = float.TryParse(HttpUtility.UrlDecode(Request.QueryString["Contrast"]), out contrast);

            if (result)
            {
                ImageProcesses.Contrast(bitmap, contrast);
                Session["Contrast"] = string.Format("{0:D1}", contrast.ToString());
            }

            float brightnessLevel;

            if (Request.QueryString["SavedBrightness"] != null && !Request.QueryString["SavedBrightness"].ToString().Equals("99"))
            {
                result = float.TryParse(HttpUtility.UrlDecode(Request.QueryString["SavedBrightness"]), out brightnessLevel);
                Session["SavedBrightness"] = HttpUtility.UrlDecode(Request.QueryString["SavedBrightness"]);
            }
            else
                result = float.TryParse(HttpUtility.UrlDecode(Request.QueryString["Brightness"]), out brightnessLevel);

            if (result)
            {
                Session["Brightness"] = string.Format("{0:D1}", brightnessLevel.ToString());
                brightnessLevel *= 0.1f;
                ImageProcesses.BrightNess(bitmap, brightnessLevel);
            }


            if (HttpUtility.UrlDecode(Request.QueryString["ShowCrossHair"]) == "true" &&
                HttpUtility.UrlDecode(Request.QueryString["CrossStyle"]) != string.Empty)
            {
                DrawCrossHairs(bitmap, HttpUtility.UrlDecode(Request.QueryString["CrossStyle"]), HttpUtility.UrlDecode(Request.QueryString["XVal"]), HttpUtility.UrlDecode(Request.QueryString["YVal"]));
            }
        }

        private void DrawCrossHairs(Bitmap bitmap, string style, string xVal, string yVal)
        {
            int X, Y;

            if (Int32.TryParse(xVal, out X) && Int32.TryParse(yVal, out Y))
            {
                Drawing draw = new Drawing();
                draw.CrossHairs(bitmap, style, X, Y);
            }
        }

        private void Invert(Bitmap bitmap)
        {
            try
            {
                ImageAttributes imageAttributes = new ImageAttributes();
                ColorMatrix cm = new System.Drawing.Imaging.ColorMatrix(new float[][]
            {
                new float[]{-1, 1, 1, 0, 0},
                new float[]{1, -1, 1, 0, 0},
                new float[]{1, 1, -1, 0, 0},
                new float[]{0, 0, 0, 1, 0, 0},
                new float[]{0, 0, 0, 0, 1, 0},
                new float[]{0, 0, 0, 0, 0, 1}
            });


                imageAttributes.SetColorMatrix(cm);

                Graphics g = Graphics.FromImage(bitmap);
                g.DrawImage(bitmap, new Rectangle(0, 0, bitmap.Width, bitmap.Height), 0, 0, bitmap.Width, bitmap.Height,
                            GraphicsUnit.Pixel, imageAttributes);
                g.Dispose();
                imageAttributes.Dispose();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex);
            }
        }

        private void InitModel(ImagePartialViewModel model, bool showPreView)
        {
            Session["ImageA"] = new Dictionary<string, byte[]>();
            Session["ImageB"] = new Dictionary<string, byte[]>();
            Session["ImageR"] = new Dictionary<string, byte[]>();
            Session["ImageD"] = new Dictionary<string, byte[]>();
            model.AuthIntNo = (int)Session["autIntNo"];

            if ((bool)(Session["SaveImageSettingsForFilm"] ?? false))
            {
                ViewBag.Parameters = model.GetParameters(Session["SavedBrightness"].ToString(), Session["SavedContrast"].ToString(), showPreView ? 1 : 0);
            }
            else
            {
                ViewBag.Parameters = model.GetParameters(showPreView ? 1 : 0);
            }
            Session["scImIntNo"] = model.ScanImageIntNo;
        }

        //2014-02-18 Heidi added for download images(5188)
        public FileResult DownLoadImage()
        {
            try
            {
                string imagename = HttpUtility.UrlDecode(Request.QueryString["JpegName"]);
                string SelectImage = HttpUtility.UrlDecode(Request.QueryString["SelectImage"]);
                if (!string.IsNullOrEmpty(SelectImage))
                {
                    SelectImage = SelectImage.Replace("(", "").Replace(")", "");
                }
                using (Bitmap bitmap = ImagePreProcess())
                {
                    HandleImage(bitmap);
                    if (!string.IsNullOrEmpty(HttpUtility.UrlDecode(Request.QueryString["Invert"])) && HttpUtility.UrlDecode(Request.QueryString["Invert"]) == "1")
                    {
                        Invert(bitmap);
                    }
                    if (string.IsNullOrEmpty(imagename))
                    {
                        imagename = "default.jpg";
                    }
                    using (MemoryStream ms = new MemoryStream())
                    {
                        bitmap.Save(ms, ImageFormat.Jpeg);
                        return File(ms.ToArray(), "image/jpeg", imagename);
                    }
                }//
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
