﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.CameraManagement;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.Web.ViewModels.CameraManagement;
using SIL.AARTO.Web.Resource.CameraManagement;
using System.Configuration;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.Web.Controllers.CameraManagement
{
    public partial class CameraManagementController : Controller
    {
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        public string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : ""; }
        }

        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

        public ActionResult VerifyFilm()
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            FilmModel filmModel = new FilmModel();
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            ViewBag.Title = CommonResource.CamerManagementTitle;

            InitModel(filmModel);

            return View(filmModel);
        }


        public JsonResult RejectFilm()
        { 
            int frameStatus = (bool)Session["adjAt100"] ? ((bool)Session["NaTISLast"] ? 800 : 500) : 999;
            string errMessage = string.Empty;
            int filmIntNo = (int)Session["filmIntNo"];
            int asdInvalidRejIntNo = (int)(Session["asdInvalidRejIntNo"] ?? 0);
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            int success = FilmManager.RejectFilm(filmIntNo, asdInvalidRejIntNo, frameStatus, userInfo.UserName, ref errMessage);
            
            if (success > 0)
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.VerificationFilm, PunchAction.Change);

                return Json(new { result = 0, data = new { } });
            }
            else
            {
                switch (success)
                {
                    case -1:
                        errMessage = FilmController.RejectFilm_ErrMsg1;
                        break;
                    case -2:
                        errMessage = FilmController.RejectFilm_ErrMsg2;
                        break;
                    case -3:
                        errMessage = FilmController.RejectFilm_ErrMsg3;
                        break;
                }
                errMessage = string.Format(FilmController.RejectFilm_ErrMsg4, errMessage);
                return Json(new { result = 0, data = new { error = "RejectError", errorMessage = errMessage } });
            }
        }

        private void InitModel(FilmModel filmModel)
        { 
            UserLoginInfo UserInfo = (UserLoginInfo)Session["UserLoginInfo"];
            Session["asdInvalidRejIntNo"] = Session["asdInvalidRejIntNo"] ?? GetASDInvalidRejIntNo(UserInfo.UserName);

            int filmIntNo = Convert.ToInt32(Session["filmIntNo"]);
            int mtrIntNo = Convert.ToInt32(Session["mtrIntNo"]);
            string autName = Session["autName"].ToString();
            int authIntNo = (int)Session["autIntNo"];

            FrameDetails frameDetails = FrameManager.GetFrameDetailsOfFilmByFilmNo(filmIntNo);

            filmModel.SecondCameras = filmModel.Cameras = CameraManager.GetSelectList();
            filmModel.Camera = frameDetails.CamIntNo;
            filmModel.Locations = LocationManager.GetSelectList(authIntNo);
            filmModel.Location = frameDetails.LocIntNo;
            filmModel.RoadTypes = RoadTypeManager.GetSelectList();
            filmModel.RoadType = frameDetails.RdTIntNo;
            filmModel.SpeedZones = SpeedZoneManager.GetSelectList();
            filmModel.SpeedZone = frameDetails.SzIntNo;
            filmModel.TrafficOfficers = OfficerManager.GetSelectList(mtrIntNo);
            filmModel.TrafficOfficer = frameDetails.TOIntNo;
            filmModel.Courts = CourtManager.GetSelectList(authIntNo);
            filmModel.Court = frameDetails.CrtIntNo;
            //add FilmType
            Session["FilmType"] = frameDetails.FilmType.ToUpper();

            if (frameDetails.ASD2ndCameraIntNo != 0)
            {
                if (frameDetails.FilmType.ToUpper()=="U")//bus lane
                {
                    filmModel.Camera = frameDetails.CamIntNo;
                    filmModel.SecondCamera = frameDetails.ASD2ndCameraIntNo;
                    //
                    ViewBag.BtnAcceptEn = true;
                    ViewBag.BtnChangeVis = true;
                    filmModel.ISASD = "none";
                }
                else
                {

                    filmModel.ISASD = "";
                    filmModel.Camera = frameDetails.LCSCameraIntNo1;
                    filmModel.SecondCamera = frameDetails.LCSCameraIntNo2;

                    filmModel.SectionName = frameDetails.SectionName;
                    filmModel.SectionCode = frameDetails.SectionCode;
                    filmModel.SectionDistance = frameDetails.ASDSectionDistance.ToString();

                    ViewBag.BtnChangeVis = false;
                    if (frameDetails.LCSActive)
                    {
                        ViewBag.BtnAcceptEn = true;
                        ViewBag.ToolTip = string.Empty;
                    }
                    else
                    {
                        ViewBag.BtnAcceptEn = false;
                        ViewBag.ToolTip = FilmController.RejectFilm_ToolTip;
                    }
                }
            }
            else
            {
                ViewBag.BtnChangeVis = true;
                filmModel.ISASD = "none";
                ViewBag.BtnAcceptEn = true;
            }
            filmModel.FilmNo = FilmManager.GetFilmDetailsByFilmIntNo(filmIntNo).FilmNo;
            filmModel.LocalAuthority = autName;
        }

        private int GetASDInvalidRejIntNo(string userName)
        {
            RejectionDB rejDB = new RejectionDB(Config.ConnectionString);
            //add ASD speed rejection reason
            string asdInvalidSetup = FilmController.RejectFilm_ASDInvalid;

            return rejDB.UpdateRejection(0, asdInvalidSetup, "N", userName, 0, "Y");
        }

        private bool CheckIsInCameraLocation(int filmIntNo,int locIntNo)
        {
            return VerificationBinManager.CheckIsInCameraLocation(filmIntNo, locIntNo);
        }
        private bool CheckIsInLocationSite(int filmIntNo, int locIntNo)
        {
            return VerificationBinManager.CheckIsInLocationSite(filmIntNo, locIntNo);
        }
    }
}
