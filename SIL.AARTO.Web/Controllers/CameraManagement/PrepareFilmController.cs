﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility;
using Stalberg.TMS;
using System.Data.SqlClient;
using SIL.AARTO.BLL.CameraManagement;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.Web.Resource.CameraManagement;
using SIL.AARTO.Web.ViewModels.CameraManagement;
using SIL.AARTO.BLL.CameraManagement.Model;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.Web.Controllers.CameraManagement
{
    [AARTOErrorLog, LanguageFilter]
    public partial class CameraManagementController : Controller
    {
        [HttpGet]
        public ActionResult PrepareVerifyFilm()
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            ViewBag.Title = CommonResource.CamerManagementTitle;
            PreFilmModel model = new PreFilmModel();
            InitModel(model, ValidationStatus.Verification);
            ViewBag.Action = "PrepareVerifyFilm";
            return View("ShowFilm", model);
        }

        [HttpPost]
        public ActionResult PrepareVerifyFilm(PreFilmModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            InitModel(model, ValidationStatus.Verification);
            string action = BookOutNextFilm(model);
            if (action == "NO_LOCK_USER")
            {
                return RedirectToAction("login", "account");
            }
           
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.VerificationFilm, PunchAction.Change);  

            if (action == "PrepareVerifyFilm")
            {
                return View("ShowFilm", model);
            }
            return RedirectToAction(action);
        }

        [HttpGet]
        public ActionResult PrepareAdjudicateFilm()
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            ViewBag.Title = CommonResource.CamerManagementTitle;
            PreFilmModel model = new PreFilmModel();
            InitModel(model, ValidationStatus.Adjudication);
            ViewBag.Action = "PrepareAdjudicateFilm";
            return View("ShowFilm", model);
        }

        [HttpPost]
        public ActionResult PrepareAdjudicateFilm(PreFilmModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            InitModel(model, ValidationStatus.Adjudication);
            string action = BookOutNextFilm(model);
            if (action == "NO_LOCK_USER")
            {
                return RedirectToAction("login", "account");
            }
           
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.Adjudication, PunchAction.Change); 
            if (action == "PrepareAdjudicateFilm")
            {
                return View("ShowFilm", model);
            }
            return RedirectToAction(action);
        }

        [HttpGet]
        public ActionResult PrepareInvestigateFilm(bool? isFishpond)
        {//20130111 updated by Nancy for show fishpond investigation(add 'bool? isFishpond')
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            ViewBag.Title = CommonResource.CamerManagementTitle;
            PreFilmModel model = new PreFilmModel();
            InitModel(model, ValidationStatus.Investigation,isFishpond.Value);
            if (isFishpond.Value)
            {//20130111 added by Nancy for show fishpond title
                model.AuthorityText = PrepareFilmController.AuthorityText6;
            }
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            int authIntNo = (int)Session["autIntNo"];
            if ((!isFishpond.Value && !AuthorityRulesManager.GetIsOrNotShowVerificationBin(userInfo.UserName, authIntNo)) || (isFishpond.Value && !AuthorityRulesManager.GetIsOrNotEnableFishpond(userInfo.UserName, authIntNo)))
            {//20130111 updated by Nancy for check fishpond investigation
                model.AuthorityText = PrepareFilmController.AuthorityTextVerBin;
                if (isFishpond.Value)
                {//20130118 added by Nancy for show 'Rule for fishpond is not active'
                    model.AuthorityText = PrepareFilmController.AuthorityTextFishpond;
                }
                FrameList frames = (FrameList)Session["FrameList"];
                if (frames != null)
                {
                    frames.Clear();
                    model.Films.Clear();
                }
                if (model.Films != null)
                {
                    model.Films.Clear();
                }
                ViewBag.ShowOfficerMsg = false;
            }

            ViewBag.Action = "PrepareInvestigateFilm";
            return View("ShowFilm", model);
        }

        [HttpPost]
        public ActionResult PrepareInvestigateFilm(PreFilmModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            InitModel(model, ValidationStatus.Investigation);
            string action = BookOutNextFilm(model);
            if (action == "NO_LOCK_USER")
            {
                return RedirectToAction("login", "account");
            }
          
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.Investigation, PunchAction.Change); 
            if (action == "PrepareInvestigateFilm")
            {
                return View("ShowFilm", model);
            }
            return RedirectToAction(action, new { isFishpond = model.IsFishpond });
        }

        //20130111 (isFishpond)Added by Nancy for distinguish frame of verification bin from those of fishpond
        private void InitModel(PreFilmModel model, ValidationStatus validation, bool isFishpond=false)
        {
            UserLoginInfo userInfo = Session["UserLoginInfo"] as UserLoginInfo;
            int authIntNo = (int)Session["autIntNo"];
            model.InitModel(validation, userInfo, authIntNo, isFishpond);

            Session["OfficerNo"] = Session["OfficerNo"] ?? model.OfficerNo;
            Session["adjAt100"] = Session["adjAt100"] ?? model.AdjAt100;
            Session["showDiscrepanciesOnly"] = Session["showDiscrepanciesOnly"] ?? model.ShowDiscrepanciesOnly;
            Session["NoOfDays"] = Session["NoOfDays"] ?? model.NoOfDays;
            Session["AutCode"] = Session["AutCode"] ?? model.AutCode;
            Session["NaTISLast"] = Session["NaTISLast"] ?? model.NaTISLast;

            model.AutName = Session["autName"].ToString();
            Session["SaveImageSettingsForFilm"] = null;

            if (model.CheckOfficerNo(userInfo))
            {
                Session["adjOfficerName"] = Session["adjOfficerName"] ?? model.OfficerName;
            }
            else if (validation != ValidationStatus.Verification)
            {
                model.ErrorMessage = PrepareFilmController.ValidOfficerError;
            }

            if (model.Films.Count > 0)
            {
                switch (model.Validation)
                {
                    case ValidationStatus.Verification:
                        model.AuthorityText = PrepareFilmController.AuthorityText1;
                        ViewBag.ShowOfficerMsg = false;
                        break;
                    case ValidationStatus.Adjudication:
                        model.AuthorityText = PrepareFilmController.AuthorityText2;
                        ViewBag.ShowOfficerMsg = true;
                        break;
                    case ValidationStatus.Investigation:
                        model.AuthorityText = PrepareFilmController.AuthorityText5;
                        ViewBag.ShowOfficerMsg = true;
                        break;
                }
                model.AuthorityText = string.Format(model.AuthorityText, model.AutName);
                ViewBag.ShowBtnBookOut = true;
                ViewBag.ShowOfficerMsg &= model.ShowOfficerMsg;
            }
            else
            {
                model.AuthorityText = PrepareFilmController.AuthorityText3;
                switch (model.Validation)
                {
                    case ValidationStatus.Verification:
                        model.AuthorityText = string.Format(model.AuthorityText, PrepareFilmController.Verification_Text);
                        break;
                    case ValidationStatus.Adjudication:
                        model.AuthorityText = string.Format(model.AuthorityText, PrepareFilmController.Adjudication_Text);
                        break;
                    case ValidationStatus.Investigation:
                        model.AuthorityText = string.Format(model.AuthorityText, PrepareFilmController.Investigation_Text);
                        break;
                }
                model.AuthorityText = string.Format(model.AuthorityText, PrepareFilmController.AuthorityText4, model.AutName);
                ViewBag.ShowBtnBookOut = false;
                ViewBag.ShowOfficerMsg = false;
            }
        }

        private string BookOutNextFilm(PreFilmModel model)
        {
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            // release any films previously locked by this user
            model.LockFilmForUser(0, 0, userInfo);
            FilmEntity nextFilm = new FilmEntity();
            string action = string.Empty;
            switch (model.Validation)
            {
                case ValidationStatus.Verification:
                    nextFilm = FilmManager.GetNextFilmToValidate(userInfo.UserName, model.Status, model.AuthIntNo);
                    action = ProcessFrames(model, nextFilm, userInfo);
                    break;
                case ValidationStatus.Adjudication:
                    if (model.CheckOfficerNo(userInfo))
                    {
                        Session["USIntNo"] = Session["USIntNo"] ?? model.USIntNo;
                        UserManager.UserShiftEdit(model.USIntNo, model.OfficerNo, "Officer", userInfo.UserName);
                        nextFilm = FilmManager.GetNextFilmToAdjudicate(userInfo.UserName, model.Status, model.AuthIntNo);
                        action = ProcessFrames(model, nextFilm, userInfo);
                    }
                    else
                    {
                        action = "PrepareAdjudicateFilm";
                        model.ErrorMessage = PrepareFilmController.ValidOfficerError;
                    }
                    break;
                case ValidationStatus.Investigation:
                    if (model.CheckOfficerNo(userInfo))
                    {
                        nextFilm = FilmManager.GetNextFilmToInvestigate(userInfo.UserName, model.Status, model.AuthIntNo);
                        action = ProcessFrames(model, nextFilm, userInfo);
                    }
                    else
                    {
                        action = "PrepareInvestigateFilm";
                        model.ErrorMessage = PrepareFilmController.ValidOfficerError;
                    }
                    break;
            }
            return action;
        }

        private string ProcessFrames(PreFilmModel model, FilmEntity nextFilm, UserLoginInfo userInfo)
        {
            string action = string.Empty;
            int cvPhase;
            if (nextFilm == null)
            {
                switch (model.Validation)
                {
                    case ValidationStatus.Verification:
                        action = "PrepareVerifyFilm";
                        break;
                    case ValidationStatus.Adjudication:
                        action = "PrepareAdjudicateFilm";
                        break;
                    case ValidationStatus.Investigation:
                        action = "PrepareInvestigateFilm";
                        break;
                }
                return action;
            }
            else
            {
                if (!model.LockFilmForUser(nextFilm.FilmIntNo, nextFilm.RowVersion, userInfo))
                {
                    return "NO_LOCK_USER";
                }

                this.Session["FrameList"] = null;

                this.Session["FrameList"] = FrameManager.GetFrameList(nextFilm.FilmIntNo, nextFilm.FilmNo, model.AuthIntNo, userInfo, model.Status, model.Validation, model.Validation);
                this.Session["filmIntNo"] = nextFilm.FilmIntNo;

                cvPhase = nextFilm.ValidateDataDateTime == DateTime.MinValue ? 0 : 1;

                if (cvPhase == 0)
                {
                    action = "VerifyFilm";
                }
                else
                {
                    action = model.Validation == ValidationStatus.Verification ? "VerifyFrame" : //"AdjudicateFrame";
                        (model.Validation == ValidationStatus.Adjudication ? "AdjudicateFrame" : "InvestigateFrame");
                }
            }
            Session["cvPhase"] = cvPhase;
            return action;
        }
    }
}
