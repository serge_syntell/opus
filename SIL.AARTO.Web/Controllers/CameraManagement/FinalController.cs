﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility;
using Stalberg.TMS;
using SIL.AARTO.BLL.CameraManagement;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.Web.Resource.CameraManagement;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.CameraManagement
{
    public partial class CameraManagementController : Controller
    {
        public ActionResult Final()
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            ViewBag.Title = CommonResource.CamerManagementTitle;
            FrameList frames = (FrameList)Session["FrameList"];
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];

            ViewBag.BtnAccept = FinalController.BtnAccept_Text;
            ViewBag.Authority = Session["autName"];
            ViewBag.FilmIntNo = frames.FilmNumber;
            return View();
        }

        public JsonResult AcceptFinal()
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            int filmIntNo = (int)Session["filmIntNo"];
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            string errMessage = string.Empty;
            string natisLast = (bool)this.Session["NaTISLast"] ? "Y" : "N";
            int success = FilmManager.FinaliseFilmVerification(filmIntNo, 2, userInfo.UserName, ref errMessage, natisLast);      //cvPhase is 2 for Finalise


            if (success == 0)
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.VerificationFinalise, PunchAction.Change);  

                return Json(
                    new 
                    { 
                        result = 0,
                        message = FinalController.ReturnMsg_Text1
                    });
            }
            else if (success == -1)
            {
                errMessage = string.Format(FinalController.errMessage_Text1, errMessage);
                return Json(
                    new
                    {
                        result = 0,
                        message = errMessage
                    });
            }
            else
            {
                errMessage = FinalController.errMessage_Text2;
                return Json(
                    new
                    {
                        result = 0,
                        message = errMessage
                    });
            }
        }
    }
}
