﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.CameraManagement;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.Resource.CameraManagement;
using SIL.AARTO.BLL.CameraManagement;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.DAL.Entities;
using System.Configuration;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.CameraManagement
{
    public partial class CameraManagementController : Controller
    {
        public ActionResult VerificationBinList(string SearchStr, string errorMsg, int page = 0)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            VerificationBinModel model = new VerificationBinModel();
            InitModel(model, SearchStr, page);
            ViewBag.ErrorMsg = errorMsg;
            return View("ShowVeBiList", model);
        }

        [HttpPost]
        public ActionResult SaveVerficationBin(VerificationBinModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            VerificationBin veBiEntity = new VerificationBin
            {
                LastUser = userInfo.UserName,
                VeBiComment = model.Comment,
                VeBiUserInserted = userInfo.UserName,
                VeBiRegNo = model.RegNo,
                VeBiIntNo = model.VeBiIntNo,
                VeBiDateInserted = DateTime.Now
            };
            
            int reInt;
            if (model.VeBiIntNo != 0)
            {
                reInt = VerificationBinManager.Update(veBiEntity);
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.NonSummonsRegistrations, PunchAction.Change);  

            }
            else
            {
                reInt = VerificationBinManager.Add(veBiEntity);
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.NonSummonsRegistrations, PunchAction.Add); 
            }

            string errorMsg = string.Empty;
            if (reInt == -1)
            {
                errorMsg = VerificationBinController.ErrorVeBiExist;
            }
            //Linda 2012-9-12 add for list report19
            if (model.VeBiIntNo == 0 && reInt>0)
            {
                //2013-11-7 Heidi changed for add all Punch Statistics Transaction(5084)
                //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());
                //punchStatistics.PunchStatisticsTransactionAdd(Convert.ToInt32(Session["AutIntNo"]), userInfo.UserName, PunchStatisticsTranTypeList.NonSummonsRegistrations,PunchAction.Add);                   
            }
            return RedirectToAction("VerificationBinList", new { errorMsg = errorMsg });
        }

        public ActionResult DeleteVerficationBin(int VeBiIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

           int isSuccess =VerificationBinManager.Delete(VeBiIntNo);
           if (isSuccess == 1)
           {
               //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
               SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
               punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.NonSummonsRegistrations, PunchAction.Delete);  

           }
            return RedirectToAction("VerificationBinList");
        }

        private void InitModel(VerificationBinModel model, string searchStr, int pageIndex)
        {
            int totalCount = 0;
            model.PageSize = Config.PageSize;
            model.SearchStr = searchStr;
            if (string.IsNullOrEmpty(searchStr))
            {
                model.PageVerificationBins = VerificationBinManager.GetPage(pageIndex, model.PageSize, out totalCount);
            }
            else
            {
                model.PageVerificationBins = VerificationBinManager.Search(searchStr, pageIndex, model.PageSize, out totalCount);
            }
            model.TotalCount = totalCount;
        }

    }
}
