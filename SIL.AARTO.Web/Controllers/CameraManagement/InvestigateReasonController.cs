﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.Specialized;

using SIL.AARTO.BLL.CameraManagement.Model;
using SIL.AARTO.Web.ViewModels.CameraManagement;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.CameraManagement;
using SIL.AARTO.Web.Resource.CameraManagement;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Printing;


namespace SIL.AARTO.Web.Controllers.CameraManagement
{
    public partial class CameraManagementController : Controller
    {
        public ActionResult InvestigateReasonList(string SearchStr, string errorMsg, int page = 0)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            InvestigateReasonModel model = new InvestigateReasonModel();
            InitModel(model, SearchStr, page);
            model.ErrorMsg = errorMsg;
            return View("ShowInReList", model);
        }

        [HttpPost]
        public ActionResult SaveInvestigateReason(InvestigateReasonModel model)
        {
            int isSuccess = 0;
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];

            TList<InvestigateReasonLookup> languages = GetInvestigateReasonLookupsFromForm(Request.Form, model.InReIntNo, userInfo.UserName);
            InvestigateReason reason = new InvestigateReason
            {
                InReIntNo = model.InReIntNo,
                LastUser = userInfo.UserName,
                InReason = Request.Form["mul_en-US"]
            };

            string errorMsg = string.Empty;
            if (model.InReIntNo == 0)
            {
                // errorMsg = InvestigateReasonErrorMsgProcess(InvestigateReasonManager.Add(reason, languages));
                isSuccess = InvestigateReasonManager.Add(reason, languages);
                errorMsg = InvestigateReasonErrorMsgProcess(isSuccess);
                if (isSuccess == 1)
                {
                  
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.InvestigateReason, PunchAction.Add);  

                }
            }
            else
            {
                //errorMsg = InvestigateReasonErrorMsgProcess(InvestigateReasonManager.Update(reason, languages, model.InReIntNo));
                isSuccess = InvestigateReasonManager.Update(reason, languages, model.InReIntNo);
                errorMsg = InvestigateReasonErrorMsgProcess(isSuccess);
                if (isSuccess == 1)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.InvestigateReason, PunchAction.Change);  
                }
            }

            return RedirectToAction("InvestigateReasonList", new { errorMsg = errorMsg });
        }

        public ActionResult DeleteInvestigateReason(int inReIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            if (new FrameService().GetByInReIntNo(inReIntNo).Count > 0)
            {//20130105 added by Nancy for check investigate reason is used in frame
                InvestigateReasonModel model = new InvestigateReasonModel();
                return RedirectToAction("InvestigateReasonList", new { errorMsg=ShowInReList.lblError_Text });
            }

            InvestigateReasonManager.Delete(inReIntNo);
           
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.InvestigateReason, PunchAction.Delete);
            return RedirectToAction("InvestigateReasonList");
        }

        public JsonResult GetInReLangList(int inReIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
            InvestigateReasonEntity entity = new InvestigateReasonEntity(inReIntNo);
            return Json(new { result = 1, data = entity });
        }


        private void InitModel(InvestigateReasonModel model, string searchStr, int pageIndex)
        {
            int totalCount = 0;
            model.PageSize = Config.PageSize;
            model.SearchStr = searchStr;
            if (string.IsNullOrWhiteSpace(searchStr))
            {
                model.PageInvestigateReasons = InvestigateReasonManager.GetPage(pageIndex, model.PageSize, out totalCount);
            }
            else
            {
                model.PageInvestigateReasons = InvestigateReasonManager.Search(searchStr, pageIndex, model.PageSize, out totalCount);
            }
            model.TotalCount = totalCount;
            model.LookupModel = new ViewModels.LookupPartViewModel { EnUSLength = 100 };
        }

        private TList<InvestigateReasonLookup> GetInvestigateReasonLookupsFromForm(NameValueCollection form, int citIntNo, string userName)
        {
            TList<InvestigateReasonLookup> languages = new TList<InvestigateReasonLookup>();
            foreach (string key in form.AllKeys)
            {
                if (key.Substring(0, 4) == "mul_")
                {
                    languages.Add(new InvestigateReasonLookup()
                    {
                        InReason = Request.Form[key],
                        InReIntNo = citIntNo,
                        LsCode = key.Substring(4, key.Length - 4),
                        LastUser = userName
                    });
                }
            }
            return languages;
        }

        private string InvestigateReasonErrorMsgProcess(int errorCode)
        {
            string errorMsg = string.Empty;
            switch (errorCode)
            {
                case -1:
                    return InvestigateReasonController.errorMsg_DuplicateInReason;
                default:
                    return string.Empty;
            }
        }

        private class InvestigateReasonEntity
        {
            public int InReIntNo { get; set; }
            public string InReason { get; set; }
            public byte[] RowVersion { get; set; }
            public string LastUser { get; set; }

            public List<LanguageLookupEntity> LanguageList { get; set; }

            public InvestigateReasonEntity(int inReIntNo)
            {
                InvestigateReason reason = InvestigateReasonManager.GetByInReIntNo(inReIntNo);
                InReIntNo = reason.InReIntNo;
                LastUser = reason.InReason;
                LastUser = reason.LastUser;
                RowVersion = reason.RowVersion;
                LanguageList = new List<LanguageLookupEntity>();

                foreach (InvestigateReasonLookup lookup in reason.InvestigateReasonLookupCollection)
                {
                    LanguageList.Add(new LanguageLookupEntity()
                    {
                        LookupValue = lookup.InReason,
                        LsCode = lookup.LsCode,
                        LookUpId = lookup.InReIntNo.ToString()
                    });
                }

            }
        }
    }
}