﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.CameraManagement;
using Stalberg.TMS;
using SIL.AARTO.Web.ViewModels.CameraManagement;
using System.Text.RegularExpressions;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.Resource.CameraManagement;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.Web.Controllers.CameraManagement
{
    public partial class CameraManagementController : Controller
    {
        public ActionResult FrameDetail(int frameIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            FrameDetailModel model = new FrameDetailModel();
            return View(model);
        }

        public ActionResult GetFrameDetail(int frameIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -1 }, JsonRequestBehavior.AllowGet);
            }
            FrameDetailModel model = new FrameDetailModel();
            InitMode(model, frameIntNo);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateFrameDetail(FrameDetailModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new {result = -2});
            }
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            model.InitModel();
            string errorStr = string.Empty;
            try
            {
                Regex regex = new Regex("^[0-9]{4}$");
                if (!regex.IsMatch(model.OwnerPostalCode.Trim()))
                {
                    errorStr = FrameDetailsController.errorStr_Text1;
                    return Json(new { 
                        result = -1, 
                        data = new { 
                            errorMessage = errorStr , 
                            model = model, 
                            focus= "OwnerPostalCode" 
                        } 
                    });
                }

                if (!regex.IsMatch(model.DriverPostalCode.Trim()))
                {
                    errorStr = FrameDetailsController.errorStr_Text2;
                    return Json(new
                    {
                        result = -1,
                        data = new
                        {
                            errorMessage = errorStr,
                            model = model,
                            focus = "DriverPostalCode"
                        }
                    });
                }

                if (model.ProxyIntNo > 0)
                {
                    if (!regex.IsMatch(model.ProxyPostalCode.Trim()))
                    {
                        errorStr = FrameDetailsController.errorStr_Text3;
                        return Json(new
                        {
                            result = -1,
                            data = new
                            {
                                errorMessage = errorStr,
                                model = model,
                                focus = "ProxyPostalCode"
                            }
                        });
                    }

                    if (string.IsNullOrEmpty(model.ProxyID))
                    {
                        errorStr = FrameDetailsController.errorStr_Text4;
                        return Json(new
                        {
                            result = -1,
                            data = new
                            {
                                errorMessage = errorStr,
                                model = model,
                                focus = "ProxyID"
                            }
                        });
                    }

                    if (string.IsNullOrEmpty(model.ProxyName))
                    {
                        errorStr = FrameDetailsController.errorStr_Text5;
                        return Json(new
                        {
                            result = -1,
                            data = new
                            {
                                errorMessage = errorStr,
                                model = model,
                                focus = "ProxyID"
                            }
                        });
                    }
                }

                if (string.IsNullOrEmpty(model.OwnerID))
                {
                    errorStr = FrameDetailsController.errorStr_Text6;
                    return Json(new
                    {
                        result = -1,
                        data = new
                        {
                            errorMessage = errorStr,
                            model = model,
                            focus = "OwnerID"
                        }
                    });
                }

                if (string.IsNullOrEmpty(model.OwnerName))
                {
                    errorStr = FrameDetailsController.errorStr_Text7;
                    return Json(new
                    {
                        result = -1,
                        data = new
                        {
                            errorMessage = errorStr,
                            model = model,
                            focus = "OwnerName"
                        }
                    });
                }

                if (string.IsNullOrEmpty(model.DriverID))
                {
                    errorStr = FrameDetailsController.errorStr_Text8;
                    return Json(new
                    {
                        result = -1,
                        data = new
                        {
                            errorMessage = errorStr,
                            model = model,
                            focus = "DriverID"
                        }
                    });
                }

                if (string.IsNullOrEmpty(model.DriverName))
                {
                    errorStr = FrameDetailsController.errorStr_Text9;
                    return Json(new
                    {
                        result = -1,
                        data = new
                        {
                            errorMessage = errorStr,
                            model = model,
                            focus = "DriverName"
                        }
                    });
                }


                int effectNo = 0;
                int frameIntNo = ((FrameList)Session["FrameList"]).Current;

                effectNo = FrameOwnerManager.UpdateFrameOwner(
                    model.OwnerIntNo,
                    frameIntNo,
                    model.OwnerName,
                    model.OwnerInitials,
                    model.OwnerID,
                    model.OwnerPostalAdd1,
                    model.OwnerPostalAdd2,
                    model.OwnerPostalAdd3,
                    model.OwnerPostalAdd4,
                    model.OwnerPostalAdd5,
                    model.OwnerPostalCode,
                    model.OwnerStreetAdd1,
                    model.OwnerStreetAdd2,
                    model.OwnerStreetAdd3,
                    model.OwnerStreetAdd4,
                    userInfo.UserName);


                if (effectNo <= 0)
                {
                    errorStr = FrameDetailsController.errorStr_Text10;
                    return Json(new
                    {
                        result = -1,
                        data = new
                        {
                            errorMessage = errorStr,
                            model = model
                        }
                    });
                }

                if (model.ProxyIntNo > 0)
                {
                    effectNo = FrameProxyManager.UpdateFrameProxy(
                        model.ProxyIntNo, 
                        frameIntNo, 
                        model.ProxyName, 
                        model.ProxyInitials,
                        model.ProxyID,
                        model.ProxyPostalAdd1, 
                        model.ProxyPostalAdd2, 
                        model.ProxyPostalAdd3,
                        model.ProxyPostalAdd4, 
                        model.ProxyPostalAdd4,
                        model.ProxyPostalCode, 
                        model.ProxyStreetAdd1,
                        model.ProxyStreetAdd2, 
                        model.ProxyStreetAdd3,
                        model.ProxyStreetAdd4, 
                        userInfo.UserName);
                    if (effectNo <= 0)
                    {
                        errorStr = FrameDetailsController.errorStr_Text11;
                        return Json(new
                        {
                            result = -1,
                            data = new
                            {
                                errorMessage = errorStr,
                                model = model
                            }
                        });
                    }
                }

                effectNo = FrameDriverManager.UpdateFrameDriver(
                    model.DriverIntNo, 
                    frameIntNo, 
                    model.DriverName, 
                    model.DriverInitials,
                    model.DriverID, 
                    model.DriverPostalAdd1, 
                    model.DriverPostalAdd2,
                    model.DriverPostalAdd3,
                    model.DriverPostalAdd4,
                    model.DriverPostalAdd5,
                    model.DriverPostalCode,
                    model.DriverStreetAdd1,
                    model.DriverStreetAdd2,
                    model.DriverStreetAdd3, 
                    model.DriverStreetAdd4, 
                    userInfo.UserName);

                if (effectNo <= 0)
                {
                    errorStr = FrameDetailsController.errorStr_Text12;
                    return Json(new
                    {
                        result = -1,
                        data = new
                        {
                            errorMessage = errorStr,
                            model = model
                        }
                    });
                }

                if (model.ProxyIntNo > 0)
                {
                    errorStr = FrameDetailsController.errorStr_Text13;
                    return Json(new
                    {
                        result = -1,
                        data = new
                        {
                            errorMessage = errorStr,
                            model = model
                        }
                    });
                }
                else
                {
                    errorStr = FrameDetailsController.errorStr_Text14;
                }
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.Adjudication, PunchAction.Change);
            }
            catch (Exception ex)
            {
                errorStr = string.Format(FrameDetailsController.errorStr_Text15, ex.Message);
                return Json(new
                {
                    result = -1,
                    data = new
                    {
                        errorMessage = errorStr,
                        model = model
                    }
                });
            }
            return Json(new
            {
                result = 0,
                data = new
                {
                    model = model,
                    message = errorStr
                }
            });
        }

        private void InitMode(FrameDetailModel model, int frameIntNo)
        {
            FrameProxyDetails frameProxyDetails = FrameProxyManager.GetFrameProxyDetailsByFrame(frameIntNo);
            FrameDriverDetails frameDriverDetails = FrameDriverManager.GetFrameDriverDetailsByFrame(frameIntNo);
            FrameOwnerDetails frameOwnerDetails = FrameOwnerManager.GetFrameOwnerDetailsByFrame(frameIntNo);
            FrameDetails frameDetails = FrameManager.GetFrameDetails(frameIntNo);
            ViewBag.HasProx = false;

            try
            {
                if (frameDetails.FrameProxyFlag.Equals("Y"))
                {
                    //Proxy
                    model.ProxyID = frameProxyDetails.FrPrxIDNumber.Trim();
                    model.ProxyName = frameProxyDetails.FrPrxSurname.Trim();
                    model.ProxyIntNo = frameProxyDetails.FrPrxIntNo;
                    model.ProxyInitials = frameProxyDetails.FrPrxInitials;
                    model.ProxyPostalAdd1 = frameProxyDetails.FrPrxPOAdd1;
                    model.ProxyPostalAdd2 = frameProxyDetails.FrPrxPOAdd2;
                    model.ProxyPostalAdd3 = frameProxyDetails.FrPrxPOAdd3;
                    model.ProxyPostalAdd4 = frameProxyDetails.FrPrxPOAdd4;

                    model.ProxyStreetAdd1 = frameProxyDetails.FrPrxStAdd1;
                    model.ProxyStreetAdd2 = frameProxyDetails.FrPrxStAdd2;
                    model.ProxyStreetAdd3 = frameProxyDetails.FrPrxStAdd3;
                    model.ProxyStreetAdd4 = frameProxyDetails.FrPrxStAdd4;

                    model.ProxyPostalCode = frameProxyDetails.FrPrxPOCode;
                    model.HasProxy = true;
                }


                //Driver
                model.DriverID = frameDriverDetails.FrDrvIDNumber.Trim();
                model.DriverName = frameDriverDetails.FrDrvSurname.Trim();
                model.DriverIntNo = frameDriverDetails.FrDrvIntNo;
                model.DriverInitials = frameDriverDetails.FrDrvInitials;
                model.DriverPostalAdd1 = frameDriverDetails.FrDrvPOAdd1;
                model.DriverPostalAdd2 = frameDriverDetails.FrDrvPOAdd2;
                model.DriverPostalAdd3 = frameDriverDetails.FrDrvPOAdd3;
                model.DriverPostalAdd4 = frameDriverDetails.FrDrvPOAdd4;
                model.DriverPostalAdd5 = frameDriverDetails.FrDrvPOAdd5;

                model.DriverStreetAdd1 = frameDriverDetails.FrDrvStAdd1;
                model.DriverStreetAdd2 = frameDriverDetails.FrDrvStAdd2;
                model.DriverStreetAdd3 = frameDriverDetails.FrDrvStAdd3;
                model.DriverStreetAdd4 = frameDriverDetails.FrDrvStAdd4;

                model.DriverPostalCode = frameDriverDetails.FrDrvPOCode;

                //Owner
                model.OwnerID = frameOwnerDetails.FrOwnIDNumber.Trim();
                model.OwnerName = frameOwnerDetails.FrOwnSurname.Trim();
                model.OwnerIntNo = frameOwnerDetails.FrOwnIntNo;
                model.OwnerInitials = frameOwnerDetails.FrOwnInitials;
                model.OwnerPostalAdd1 = frameOwnerDetails.FrOwnPOAdd1;
                model.OwnerPostalAdd2 = frameOwnerDetails.FrOwnPOAdd2;
                model.OwnerPostalAdd3 = frameOwnerDetails.FrOwnPOAdd3;
                model.OwnerPostalAdd4 = frameOwnerDetails.FrOwnPOAdd4;
                model.OwnerPostalAdd5 = frameOwnerDetails.FrOwnPOAdd5;

                model.OwnerStreetAdd1 = frameOwnerDetails.FrOwnStAdd1;
                model.OwnerStreetAdd2 = frameOwnerDetails.FrOwnStAdd2;
                model.OwnerStreetAdd3 = frameOwnerDetails.FrOwnStAdd3;
                model.OwnerStreetAdd4 = frameOwnerDetails.FrOwnStAdd4;

                model.OwnerPostalCode = frameOwnerDetails.FrOwnPOCode;
            }
            catch
            {
            }

            string cssClass = "NormalBoldRed";

            if (frameDetails.FrameNatisIndicator.Equals("!"))
            {
                model.BtnUpdateOwnerEnable = true;
                model.BtnUpdateOwner = FrameDetailsController.BtnUpdateOwner_Text1;

                if (frameDetails.FrameNaTISErrorFieldList.ToLower().Contains("proxy id no is blank"))
                {
                    model.ProxyIDIsReadonly = false;
                    model.ProxyIDCssClass = cssClass;
                    model.BtnUpdateOwner = FrameDetailsController.BtnUpdateOwner_Text2;
                }
                else if (frameDetails.FrameNaTISErrorFieldList.ToLower().Contains("id no is blank"))
                {
                    model.OwnerIDIsReadonly = model.DriverIDIsReadonly = false;
                    model.OwnerIDCssClass = model.DriverIDCssClass = cssClass;
                }

                if (frameDetails.FrameNaTISErrorFieldList.ToLower().Contains("surname is blank"))
                {
                    if (frameDetails.FrameProxyFlag.Equals("Y"))
                    {
                        model.ProxyNameIsReadonly = false;
                        model.ProxyNameCssClass = cssClass;
                        model.BtnUpdateOwner = FrameDetailsController.BtnUpdateOwner_Text2;
                    }
                    else
                    {
                        model.DriverNameIsReadonly = model.OwnerNameIsReadonly = false;
                        model.DriverNameCssClass = model.OwnerNameCssClass = cssClass;
                    }
                }

                if (frameDetails.FrameNaTISErrorFieldList.ToLower().Contains("po addr1 is blank"))
                {
                    if (frameDetails.FrameProxyFlag.Equals("Y"))
                    {
                        model.ProxyPostalAdd1IsReadonly =
                        model.ProxyPostalAdd2IsReadonly =
                        model.ProxyPostalAdd3IsReadonly =
                        model.ProxyPostalAdd4IsReadonly = false;

                        model.ProxyPostalAdd1CssClass =
                        model.ProxyPostalAdd1CssClass =
                        model.ProxyPostalAdd2CssClass =
                        model.ProxyPostalAdd3CssClass =
                        model.ProxyPostalAdd4CssClass = cssClass;

                        model.BtnUpdateOwner = FrameDetailsController.BtnUpdateOwner_Text2;
                    }
                    else
                    {
                        model.DriverPostalAdd1IsReadonly =
                        model.DriverPostalAdd2IsReadonly =
                        model.DriverPostalAdd3IsReadonly =
                        model.DriverPostalAdd4IsReadonly =
                        model.DriverPostalAdd5IsReadonly = false;

                        model.OwnerPostalAdd1IsReadonly =
                        model.OwnerPostalAdd2IsReadonly =
                        model.OwnerPostalAdd3IsReadonly =
                        model.OwnerPostalAdd4IsReadonly =
                        model.OwnerPostalAdd5IsReadonly = false;

                        model.DriverPostalAdd1CssClass =
                        model.DriverPostalAdd2CssClass =
                        model.DriverPostalAdd3CssClass =
                        model.DriverPostalAdd4CssClass =
                        model.DriverPostalAdd5CssClass =

                        model.OwnerPostalAdd1CssClass =
                        model.OwnerPostalAdd2CssClass =
                        model.OwnerPostalAdd3CssClass =
                        model.OwnerPostalAdd4CssClass =
                        model.OwnerPostalAdd5CssClass = cssClass;
                    }
                }

                if (frameDetails.FrameNaTISErrorFieldList.ToLower().Contains("po code is blank"))
                {
                    if (frameDetails.FrameProxyFlag.Equals("Y"))
                    {
                        model.ProxyPostalCodeIsReadonly = false;
                        model.ProxyPostalCodeCssClass = "NormalBoldRed";
                        model.BtnUpdateOwner = FrameDetailsController.BtnUpdateOwner_Text2;
                    }
                    else
                    {
                        model.DriverPostalCodeIsReadonly = model.OwnerPostalCodeIsReadonly = false;
                        model.DriverPostalCodeCssClass =
                        model.OwnerPostalCodeCssClass = cssClass;
                    }
                }
            }
            else
            {
                model.BtnUpdateOwnerEnable = false;
            }

        }
    }
}
