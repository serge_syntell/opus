﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.CameraManagement;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.Web.ViewModels.CameraManagement;
using SIL.AARTO.BLL.Utility;
using SIL.QueueLibrary;
using System.Transactions;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.Web.Resource.CameraManagement;
using SIL.AARTO.BLL.Utility.Cache;
using Stalberg.TMS;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.CameraManagement.EnumLinkes;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.CameraManagement;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.CameraManagement
{
    public partial class CameraManagementController : Controller
    {
        FrameService frameService = new FrameService();

        public ActionResult VerifyFrame(bool? isPre)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            FrameList frames = (FrameList)Session["FrameList"];
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            if (isPre != null && isPre.Value)
            {
                frames.GetPreviousFrame();
            }
            else if (isPre != null && isPre.Value == false)
            {
                frames.GetNextFrame(false);
            }
            if (frames.Current <= 0)
            {
                FilmManager.LockUser(0, userInfo.UserName);
                Session.Remove("cvPhase");
                return RedirectToAction("Final");
            }

            int authIntNo = (int)(Session["autIntNo"] ?? 0);
            ViewBag.ShowVerificationBin = AuthorityRulesManager.GetIsOrNotShowVerificationBin(userInfo.UserName, authIntNo);
            ViewBag.Title = CommonResource.CamerManagementTitle;
            ViewBag.FrameIntNo = frames.Current;
            VerifyFrameModel model = new VerifyFrameModel();
            model.Navigation = string.Format("Navigating - {0}", frames);
            InitVerifyModel(model);

            Film film = GetFilmByFilmNo(frames.FilmNumber);
            if (film != null && film.FilmType == "U")
                model.IsBusLane = true;
            else
                model.IsBusLane = false;
            return View(model);
        }

        public ActionResult AdjudicateFrame(bool? isPre)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            FrameList frames = (FrameList)Session["FrameList"];
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            if (isPre != null && isPre.Value)
            {
                frames.GetPreviousFrame();
            }
            else if (isPre != null && isPre.Value == false)
            {
                frames.GetNextFrame(false);
            }
            if (frames.Current <= 0)
            {
                FilmManager.LockUser(0, userInfo.UserName);
                Session.Remove("cvPhase");
                int filmIntNo = (int)Session["filmIntNo"];
                string natisLast = (bool)this.Session["NaTISLast"] ? "Y" : "N";
                string errMessage = string.Empty;
                FilmManager.FinaliseFilmAdjudication(filmIntNo, natisLast, userInfo.UserName, ref errMessage);
                return RedirectToAction("PrepareAdjudicateFilm");
            }
            AdjudicateFrameModel model = new AdjudicateFrameModel();
            InitAdjudicateModel(model, ValidationStatus.Adjudication);
            ViewBag.Title = CommonResource.CamerManagementTitle;
            ViewBag.FrameIntNo = frames.Current;

            Film film = GetFilmByFilmNo(frames.FilmNumber);
            if (film != null && film.FilmType == "U")
                model.IsBusLane = true;
            else
                model.IsBusLane = false;

            return View(model);
        }

        public ActionResult InvestigateFrame(bool? isPre, bool? isFishpond)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            FrameList frames = (FrameList)Session["FrameList"];
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            if (isPre != null && isPre.Value)
            {
                frames.GetPreviousFrame();
            }
            else if (isPre != null && isPre.Value == false)
            {
                frames.GetNextFrame(false);
            }
            if (frames.Current <= 0)
            {
                FilmManager.LockUser(0, userInfo.UserName);
                Session.Remove("cvPhase");
                return RedirectToAction("PrepareInvestigateFilm", new { isFishpond = isFishpond.Value });
            }
            int authIntNo = (int)(Session["autIntNo"] ?? 0);

            AdjudicateFrameModel model = new AdjudicateFrameModel();
            InitAdjudicateModel(model, ValidationStatus.Investigation);
            model.IsFishpond = isFishpond.Value;
            ViewBag.Title = CommonResource.CamerManagementTitle;
            ViewBag.FrameIntNo = frames.Current;

            return View("AdjudicateFrame", model);
        }

        //20130103 added by Nancy for investigate
        public JsonResult PostFrameToInvestigation(int inReIntNo, bool isVerify)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            FrameList frames = (FrameList)Session["FrameList"];

            if (isVerify)
            {
                FrameManager.VerficationToInvestigation(frames.Current, 850, userInfo.UserName, DateTime.Now, inReIntNo);
                frames.MarkProcessed(frames.Current);
            }
            else
            {
                FrameManager.AdjudicationToInvestigation(frames.Current, 850, userInfo.UserName, DateTime.Now, inReIntNo);
                frames.MarkProcessed(frames.Current);
            }
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.Adjudication, PunchAction.Change);  

            return Json(new { result = 0 });
        }

        public JsonResult UpdateFrame(ActionParam param)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -1 });
            }
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            
            //throw new Exception("a");
            string message = string.Empty;
            try
            {
                FrameList frameList = (FrameList)Session["frameList"];
                bool isNaTISLast = (bool)Session["NaTISLast"];
                bool adjAt100 = (bool)Session["adjAt100"];
                int iDateRule = Convert.ToInt32(Session["NoOfDays"]);
                string AutCode = Session["AutCode"].ToString().Trim();

                QueueItemProcessor queProcessor = new QueueItemProcessor();
                QueueItem item = new QueueItem();
                string violationType = FrameManager.GetViolationType(frameList.Current);

                if (param.ProcessName.Equals("VerifyFrame") || param.ProcessName.Equals("ReturnToNatis"))
                {
                    FrameUpdate frameUpdate = new FrameUpdate(Config.ConnectionString, ref frameList);

                    // dls 070417 - some people are entereing spaces in the regno - need to eliminate them
                    param.Registration = param.Registration.Replace(@" ", @"").Trim().ToUpper();
                    param.UserName = userInfo.UserName;
                    //param.FrameIntNo = frameList.Current;

                    using (TransactionScope scope = new TransactionScope())
                    {
                        message = frameUpdate.Verify(param.ProcessName,
                                                    RejectionLookupCache.GetCacheLookupValue("en-US")[param.RejIntNo],
                                                    param.RejIntNo,
                                                    param.Registration,
                                                    param.VehType,
                                                    param.VehMake,
                                                    param.UserName,
                                                    param.ProcessType,
                                                    Int32.Parse(param.StatusVerified),
                                                    Int32.Parse(param.StatusRejected),
                                                    Int32.Parse(param.StatusNatis),
                                                    param.StatusInvestigate,
                                                    Int32.Parse(param.Speed1),
                                                    Int32.Parse(param.Speed2),
                                                    Convert.ToDateTime(param.OffenceDateTime),
                                                    param.FrameNo,
                                                    param.PreUpdatedRegNo,
                                                    RejectionLookupCache.GetCacheLookupValue("en-US")[param.PreRejIntNo],
                                                    isNaTISLast,
                                                    frameList.Current,
                                                    param.AllowContinue,
                                                    param.PreUpdatedAllowContinue,
                                                    Int64.Parse(param.RowVersion),
                                                    Convert.ToInt32(Session["autIntNo"]),
                                                    adjAt100);

                        //Oscar 20111130 add push queue.
                        if (string.IsNullOrEmpty(message))
                        {
                            // 2014-03-13, Oscar added RegNoHasChanged
                            FrameDB.CheckRegNoHasChanged(ref frameService, frameList.Current, param.PreUpdatedRegNo, LastUser);

                            FrameDetails frame = FrameManager.GetFrameDetails(frameList.Current);
                            QueueManager.PushQueueByFrameStatus(
                                frame.FrameStatus,
                                frameList.Current,
                                AutCode,
                                iDateRule - ((TimeSpan)(DateTime.Now - Convert.ToDateTime(param.OffenceDateTime))).Days,
                                violationType);
                        }

                        //Jerry 2013-05-17 if selected reject secondary frames, set frameStatus = 999, do't need to push queue
                        if (string.IsNullOrEmpty(message) && param.ProcessType.Equals("Reject") && param.RejectSecondaryFrames == "Yes")
                        {
                            int parentFrameIntNo = frameList.Current;
                            List<Frame> secondaryFrameEntityList = FrameManager.GetAllSecondaryFrames(parentFrameIntNo);
                            foreach (Frame frameEntity in secondaryFrameEntityList)
                            {
                                FrameDetails frameDetails = FrameManager.GetFrameDetails(frameEntity.FrameIntNo);
                                if ((frameEntity.FrameStatus > 750 && frameEntity.FrameStatus < 800) || (frameEntity.FrameStatus > 110 && frameEntity.FrameStatus < 450
                                    && (!frameEntity.FrameVerifyDateTime.HasValue || frameEntity.AllowContinue == "N")))
                                {
                                    message = VerifySecondaryFrame(param.ProcessName, RejectionLookupCache.GetCacheLookupValue("en-US")[param.RejIntNo], param.RejIntNo,
                                        param.Registration, param.VehType, param.VehMake, param.UserName, param.ProcessType, Int32.Parse(param.StatusVerified),
                                        Int32.Parse(param.StatusRejected), Int32.Parse(param.StatusNatis), param.StatusInvestigate, frameEntity.FirstSpeed, frameEntity.SecondSpeed,
                                        Convert.ToDateTime(param.OffenceDateTime), frameEntity.FrameNo, frameEntity.RegNo,
                                        RejectionLookupCache.GetCacheLookupValue("en-US")[frameEntity.RejIntNo], isNaTISLast, frameEntity.FrameIntNo, param.AllowContinue,
                                        frameEntity.AllowContinue, frameDetails.RowVersion, Convert.ToInt32(Session["autIntNo"]), adjAt100);
                                }
                            }

                        }
                       
                        if(string.IsNullOrEmpty(message))
                        {
                            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.VerificationFrame, PunchAction.Change); 
                        }
                        scope.Complete();
                    }
                    //need to reset this here to update the ones that have been processed
                    Session["frameList"] = frameList;


                }
                else if ( param.ProcessName.Equals("AdjudicateFrame") 
                    || param.ProcessName.Equals("RejectFrame") 
                    || param.ProcessName.Equals("ReturnToNatisAdj")
                    || param.ProcessName.Equals("InvestigateFrame"))
                {
                    string sessionAdjOfficerNo = Session["OfficerNo"].ToString();
                    int sessionUSIntNo = Convert.ToInt32(Session["USIntNo"]);

                    //dls 090619 - added to allow the officer to reject the frame
                    bool reject = param.ProcessName.Equals("RejectFrame") ? true : false;
                    bool sendToNatis = param.ProcessName.Equals("ReturnToNatisAdj") ? true : false;

                    FrameUpdate frameUpdate = new FrameUpdate(Config.ConnectionString, ref frameList);

                    // dls 070417 - some people are entereing spaces in the regno - need to eliminate them
                    param.Registration = param.Registration.Replace(@" ", @"").Trim().ToUpper();

                    using (TransactionScope scope = new TransactionScope())
                    {
                        message = frameUpdate.Adjudicate(param.ProcessName,
                                                        RejectionLookupCache.GetCacheLookupValue("en-US")[param.RejIntNo],
                                                        param.RejIntNo,
                                                        param.Registration,
                                                        param.ChangedMake,
                                                        param.VehType,
                                                        param.VehMake,
                                                        userInfo.UserName,
                                                        param.ProcessType,
                                                        Int32.Parse(param.StatusProsecute),
                                                        Int32.Parse(param.StatusCancel),
                                                        Int32.Parse(param.StatusNatis),
                                                        param.FrameNo,
                                                        param.PreUpdatedRegNo,
                                                        RejectionLookupCache.GetCacheLookupValue("en-US")[param.PreRejIntNo],
                                                        sessionAdjOfficerNo,
                                                        sessionUSIntNo,
                                                        frameList.Current,
                                                        param.AllowContinue,
                                                        param.PreUpdatedAllowContinue,
                                                        Int64.Parse(param.RowVersion),
                                                        isNaTISLast,
                                                        Convert.ToInt32(Session["autIntNo"]),
                                                        Convert.ToBoolean((Session["NatisData"]) ?? false),
                                                        adjAt100,
                                                        reject,
                                                        sendToNatis,
                                                        param.SaveImageSettings,
                                                        Convert.ToDecimal(param.Contrast),
                                                        Convert.ToDecimal(param.Brightness));

                        if (string.IsNullOrEmpty(message))
                        {
                            // 2014-03-13, Oscar added RegNoHasChanged
                            FrameDB.CheckRegNoHasChanged(ref frameService, frameList.Current, param.PreUpdatedRegNo, LastUser);

                            FrameDetails frame = FrameManager.GetFrameDetails(frameList.Current);
                            QueueManager.PushQueueByFrameStatus(
                                frame.FrameStatus,
                                frameList.Current,
                                AutCode,
                                iDateRule - ((TimeSpan)(DateTime.Now - Convert.ToDateTime(param.OffenceDateTime))).Days,
                                violationType);
                        }

                        //Jerry 2013-05-17 if selected reject secondary frames, set frameStatus = 999, do't need to push queue
                        if (string.IsNullOrEmpty(message) && param.ProcessType.Equals("Don't prosecute") && param.RejectSecondaryFrames == "Yes")
                        {
                            int parentFrameIntNo = frameList.Current;
                            List<Frame> secondaryFrameEntityList = FrameManager.GetAllSecondaryFrames(parentFrameIntNo);
                            foreach (Frame frameEntity in secondaryFrameEntityList)
                            {
                                FrameDetails frameDetails = FrameManager.GetFrameDetails(frameEntity.FrameIntNo);
                                if (frameEntity.FrameStatus == 700 || frameEntity.FrameStatus == 500 || frameEntity.FrameStatus == 850)
                                {
                                    message = AdjudicateSecondaryFrame(param.ProcessName, RejectionLookupCache.GetCacheLookupValue("en-US")[param.RejIntNo],
                                        param.RejIntNo, param.Registration, param.ChangedMake, param.VehType, param.VehMake, userInfo.UserName, param.ProcessType,
                                        Int32.Parse(param.StatusProsecute), Int32.Parse(param.StatusCancel), Int32.Parse(param.StatusNatis), frameEntity.FrameNo,
                                        frameEntity.RegNo, RejectionLookupCache.GetCacheLookupValue("en-US")[frameEntity.RejIntNo], sessionAdjOfficerNo,
                                        sessionUSIntNo, frameEntity.FrameIntNo, param.AllowContinue, frameEntity.AllowContinue, frameDetails.RowVersion, isNaTISLast,
                                        Convert.ToInt32(Session["autIntNo"]), Convert.ToBoolean((Session["NatisData"]) ?? false), adjAt100, reject, sendToNatis, param.SaveImageSettings,
                                        Convert.ToDecimal(param.Contrast), Convert.ToDecimal(param.Brightness));
                                }
                            }
                        }

                        if (string.IsNullOrEmpty(message))
                        {
                            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.Adjudication, PunchAction.Change);
                       }
                        scope.Complete();
                    }

                    //need to reset this here to update the ones that have been processed
                    Session["frameList"] = frameList;

                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(message))
                    message += Environment.NewLine;
                message += ex.Message;
            }

            return Json(new {result = string.IsNullOrEmpty(message) ? 0 : 1, data = new {errorMessage = message}});
        }

        public JsonResult UpdateAllFrames(string proceed, int locIntNo, int camIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = 0, data = new { logout = true } });
            }

            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            int filmIntNo = (int)Session["filmIntNo"];
            //add bus lane check
            string filmType = (Session["FilmType"] == null ? "" : (string)Session["FilmType"]);
            if (filmType == "U")
            {
                bool flag = CheckIsInCameraLocation(filmIntNo, locIntNo);
                bool flagSite = CheckIsInLocationSite(filmIntNo, locIntNo);
                if (!flag || !flagSite)
                {
                    return Json(new
                    {
                        result = 0,
                        data = new
                        {
                            error = "BusCheckFailed",
                            errorMessage = FilmController.RejectFilm_BUSInvalid
                        }
                    });
                }
            }

            int success = FrameManager.UpdateAllFramesForFilm(filmIntNo, locIntNo, camIntNo, proceed, userInfo.UserName);

            if (success == 0)
            {
                Session["frameIntNo"] = 0;
                Session["cvPhase"] = 1;

               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.VerificationFilm, PunchAction.Change);

                FrameList frames = (FrameList)this.Session["FrameList"];

                if (frames.NoFrames)
                {
                    return Json(new { result = 0, data = new { NoFrames = true } });
                }
                else if ((bool)this.Session["NaTISLast"])
                {
                    return Json(new { result = 0, data = new { IsNaTISLast = true } });
                }
                else
                {
                    return Json(new { result = 0, data = new { } });
                }
            }
            else if (success == -1)
            {
                return Json(
                    new
                    {
                        result = 0,
                        data = new
                        {
                            error = "NoUpdate",
                            errorMessage = FilmController.UpdateAllFrames_ErrMsg1
                        }
                    });
            }
            else
            {
                return Json(
                    new
                    {
                        result = 0,
                        data = new
                        {
                            error = "UpdateFailed",
                            errorMessage = FilmController.UpdateAllFrames_ErrMsg2
                        }
                    });
            }
        }



        public JsonResult GetVehicleTypeStr(int vtIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2, data = new { logout = true } });
            }
            VehicleTypeDetails vtDetail = VehicleTypeManager.GetByVtIntNo(vtIntNo);
            return Json(new{ result = 0, capVtStr = string.Format("{0}({1})", vtDetail.VTCode, vtDetail.VTDescr) });
        }


        #region Jerry 2013-04-18 add for secondary offence

        public JsonResult GetAllChildFramesSecondaryOffenceCode(string filmNo, string frameNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            string secondaryOffenceCode = string.Empty;

            secondaryOffenceCode = FrameManager.GetAllChildFramesSecondaryOffenceCode(filmNo, frameNo);

            return Json(new { result = 1, data = secondaryOffenceCode });
        }

        public JsonResult HandleFrameAboutSecondaryOffence(string filmNo, string frameNo, string checkSecondaryOffences, string uncheckSecondaryOffences, string processName)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];

            try
            {
                //handle checked secondary offence
                if (!string.IsNullOrWhiteSpace(checkSecondaryOffences))
                {
                    string[] secondaryOffenceArray = checkSecondaryOffences.Trim().Split('|');
                    foreach (string secondaryOffence in secondaryOffenceArray.ToList())
                    {
                        SIL.AARTO.DAL.Entities.SecondaryOffence secondaryOffenceEntity = new SIL.AARTO.DAL.Services.SecondaryOffenceService().GetBySeOfCode(secondaryOffence.Trim());
                        if (secondaryOffenceEntity != null)
                        {
                            FrameManager.CreateFrameAboutSecondaryOffence(filmNo, frameNo, secondaryOffenceEntity.SeOfCode,  userInfo.UserName);
                        }
                    }
                }

                //handle unchecked secondary offence
                if (!string.IsNullOrWhiteSpace(uncheckSecondaryOffences))
                {
                    string[] secondaryOffenceArray = uncheckSecondaryOffences.Trim().Split('|');
                    foreach (string secondaryOffence in secondaryOffenceArray.ToList())
                    {
                        SIL.AARTO.DAL.Entities.SecondaryOffence secondaryOffenceEntity = new SIL.AARTO.DAL.Services.SecondaryOffenceService().GetBySeOfCode(secondaryOffence.Trim());
                        if (secondaryOffenceEntity != null)
                        {
                            FrameManager.RejectFrameAboutSecondaryOffence(filmNo, frameNo, secondaryOffenceEntity.SeOfCode, userInfo.UserName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { result = -1 , data = ex.Message });
            }

            Film filmEntity = new FilmService().GetByFilmNo(filmNo);
            if (filmEntity != null)
            {
                ValidationStatus status = ValidationStatus.None;
                bool NaTISLast = AuthorityRulesManager.GetIsOrNotNaTISLast(userInfo.UserName, filmEntity.AutIntNo);
                status |= NaTISLast ? ValidationStatus.NaTISLast : ValidationStatus.None;

                ValidationStatus validation = ValidationStatus.None;
                switch (processName)
                {
                    case "VerifyFrame":
                        status = ValidationStatus.Verification;
                        validation = ValidationStatus.Verification;
                        break;
                    case "AdjudicateFrame":
                        status = ValidationStatus.Adjudication;
                        validation = ValidationStatus.Adjudication;
                        break;
                    case "InvestigateFrame":
                        status = ValidationStatus.Investigation;
                        validation = ValidationStatus.Investigation;
                        break;
                }

                this.Session["FrameList"] = FrameManager.GetFrameList(filmEntity.FilmIntNo, filmNo, filmEntity.AutIntNo, userInfo, status, validation, validation);
            }

            return Json(new { result = 1 });
        }

        #endregion

        private void InitVerifyModel(VerifyFrameModel model)
        {
            string allowContinueDisplay = "none";
            bool tableASDVisible = false;

            bool _ISASD = false;
            model.AllowContinue = true;

            int authIntNo = (int)Session["autIntNo"];
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            FrameList frames = (FrameList)Session["FrameList"];
            Session["adjAt100"] = Session["adjAt100"] ?? AuthorityRulesManager.GetIsOrNotAdjAt100(userInfo.UserName, authIntNo);
            Session["asdInvalidRejIntNo"] = Session["asdInvalidRejIntNo"] ?? RejectionManager.GetASDInvalidRejIntNo(userInfo);

            //bool isActive = AuthorityRulesManager.GetIsOrNotShowInvestigation(userInfo.UserName, authIntNo);  20130108 removed by Nancy
            bool isActive = AuthorityRulesManager.GetIsOrNotEnableFishpond(userInfo.UserName, authIntNo);//20130108 updated by Nancy for check finshpind          
            //bool isJmpd = new AuthorityService().GetByAutIntNo(authIntNo).AutTicketProcessor == "Cip_AARTO"; 20130109 removed by Nancy for check by authorityrule
            int dateRule30Days = DateRulesManager.GetNoOfDays(userInfo, authIntNo);
            DateTime offenceDate = new FrameService().GetByFrameIntNo(frames.Current).OffenceDate;
            TimeSpan ts = DateTime.Now - offenceDate;

            //ViewBag.ShowInvestigation = isActive && isJmpd && ts.Days <= dateRule30Days ? true : false; 20130109 updated by Nancy for check by authorityrule
            model.ShowInvestigation = isActive && ts.Days <= dateRule30Days ? true : false;

            model.PageName = FrameController.PageName_Text;
            model.VehicleMakers = VehicleMakeManager.GetSelectList();
            model.VehicleTypes = VehicleTypeManager.GetSelectList();
            model.RejectReasons = RejectionManager.GetSelectList();
            //model.RejectReasons = RejectionManager.GetSelectListValueWithText();
            model.InvestigateReasons = InvestigateReasonManager.GetSelectList();

            model.FilmNo = frames.FilmNumber;

            FrameDetails frameDetails = FrameManager.GetFrameDetails(frames.Current);
            model.FrameNo = frameDetails.FrameNo;
            model.Sequence = frameDetails.Sequence;
            model.RegNo = frameDetails.RegNo;
            //Jerry 2013-04-22 add IsParentFrame
            model.IsParentFrame = !frameDetails.ParentFrameIntNo.HasValue ? true : false;
            //Jerry 2013-06-05 add
            model.AllowSecondaryOffence = AuthorityRulesManager.GetIsOrNotShowSecondaryOffence(userInfo.UserName, authIntNo);

            model.NatisCodeOrError = frameDetails.NBReason;
            model.FirstSpeed = frameDetails.FirstSpeed;
            model.SecondSpeed = frameDetails.SecondSpeed;
            model.AllowContinue = true;
            model.OffenceDate = frameDetails.OffenceDate.ToString("yyyy/MM/dd");
            model.OffenceTime = frameDetails.OffenceDate.ToString("HH:mm");

            model.RejNoneValue = RejectionEnum.Init("en-US", userInfo.UserName).NONE.Value;
            model.RejectReason = frameDetails.RejIntNo;
            model.VehicleMaker = frameDetails.VMIntNo;
            model.VehicleType = frameDetails.VTIntNo;

            if (frameDetails.ASD2ndCameraIntNo > 0)
            {
                tableASDVisible = true;
                model.ASDGPSDateTime1 = frameDetails.ASDGPSDateTime1.ToString("yyyy-MM-dd HH:mm");
                model.ASDGPSDateTime2 = frameDetails.ASDGPSDateTime2.ToString("yyyy-MM-dd HH:mm");

                model.ASDLane1 = frameDetails.ASDSectionStartLane;
                model.ASDLane2 = frameDetails.ASDSectionEndLane;

                model.ErrorMessage = frameDetails.ConfirmError;
                model.AllowContinue = false;
                allowContinueDisplay = string.Empty;

                if (frameDetails.RejIntNo == (int)Session["asdInvalidRejIntNo"])
                {
                    _ISASD = true;
                }
            }
            else
            {
                tableASDVisible = false;
            }

            //xxx
            //ViewBag.ShowNatis = false;


            bool vehicleMakerVisible = false;
            bool vehicleTypeVisible = false;
            bool lblNatisVisible = false;
            bool natisRejreasonVisible = false;
            bool natisRegNoVisible = false;
            bool natisReturnCodeVisible = false;

            int natisVtIntNo = -1;
            switch (frameDetails.FrameStatus)
            {
                case "751":
                case "151":
                    vehicleMakerVisible = true;
                    vehicleTypeVisible = false;

                    lblNatisVisible = true;
                    natisRegNoVisible = false;
                    model.NatisVehicleMaker = GetVehicleMaker(frameDetails.FrameNatisVMCode);
                    break;
                case "752":
                case "152":
                    vehicleMakerVisible = false;
                    vehicleTypeVisible = true;

                    lblNatisVisible = true;
                    natisRegNoVisible = false;
                    model.NatisVehicleType = GetVehicleType(frameDetails.FrameNatisVTCode, out natisVtIntNo);
                    break;
                case "753":
                case "153":
                    vehicleMakerVisible = true;
                    vehicleTypeVisible = true;

                    lblNatisVisible = true;
                    natisRegNoVisible = false;
                    model.NatisVehicleMaker = GetVehicleMaker(frameDetails.FrameNatisVMCode);
                    model.NatisVehicleType = GetVehicleType(frameDetails.FrameNatisVTCode, out natisVtIntNo);
                    break;
                case "754":
                case "154":
                    lblNatisVisible = true;
                    natisRegNoVisible = true;
                    model.NatisRegNo = frameDetails.FrameNatisRegNo;
                    break;
                case "200":
                    vehicleMakerVisible = false;
                    vehicleTypeVisible = false;
                    lblNatisVisible = false;
                    natisRegNoVisible = false;
                    break;
                case "155":
                case "755":
                    natisReturnCodeVisible = true;
                    break;
            }
            model.NatisVtIntNo = natisVtIntNo;
            model.ErrorNatisVTNull = natisVtIntNo == 0 ? string.Format(FrameController.errorNatisVTNull, frameDetails.FrameNatisVTCode) : string.Empty;
            if (natisVtIntNo > 0 && natisVtIntNo != frameDetails.VTIntNo)
            {
                model.NatisVTString = string.Format("{0}({1})", frameDetails.FrameNatisVTCode, model.NatisVehicleType);
            }

            #region hidfield
            int statusVerified = 500;
            int statusReturnToNatis = 50;

            if ((bool)Session["NaTISLast"])
            {
                statusVerified = 800;
                statusReturnToNatis = 740;
            }
            if (frameDetails.FrameNatisRegNo.Equals(""))
            {
                statusVerified = (bool)Session["adjAt100"] == true ? statusVerified : statusReturnToNatis;
                model.BtnShowOwnerVisible = false;
            }

            if (frameDetails.AllowContinue.Equals("N"))
            {
                model.ErrorMessage = frameDetails.ConfirmError;
                model.AllowContinue = false;
                allowContinueDisplay = string.Empty;
            }

            int statusRejected = 999;
            int statusInvestigate = 850;
            ViewBag.HidParams =
                "{StatusRejected:'" + statusRejected + "'," +
                "StatusVerified:'" + statusVerified + "'," +
                "StatusReturnToNatis:'" + statusReturnToNatis + "'," +
                "StatusInvestigate:'" + statusInvestigate + "'," +
                "RowVersion:'" + frameDetails.RowVersion + "'," +
                "PreUpdatedRegNo:'" + frameDetails.RegNo + "'," +
                "FrameIntNo:'" + frames.Current + "'," +
                "PreRejIntNo:'" + frameDetails.RejIntNo + "'," +
                "PreUpdatedAllowContinue:'" + frameDetails.AllowContinue + "'}";
            #endregion

            ViewBag.NatisVehicleMakerVisible = vehicleMakerVisible;
            ViewBag.NatisVehicleTypeVisible = vehicleTypeVisible;
            ViewBag.LblNatisVisible = lblNatisVisible;
            ViewBag.NatisRegNoVisible = natisRegNoVisible;
            ViewBag.NatisRejreasonVisible = natisRejreasonVisible;
            ViewBag.NatisReturnCodeVisible = natisReturnCodeVisible;

            ViewBag.AllowContinueDisplay = allowContinueDisplay;
            ViewBag.TableASDVisible = tableASDVisible;
            ViewBag.ISASD = _ISASD;

            string cssClass = "NormalButtonRed";
            if (frameDetails.FrameNatisIndicator.Equals("!"))
            {
                model.BtnShowOwner = FrameController.btnAccept_Text;
                model.BtnShowOwnerCssClass = cssClass;
                string frameNaTISErrorFieldList = frameDetails.FrameNaTISErrorFieldList.ToLower();

                if (frameNaTISErrorFieldList.Contains(FrameNaTISErrorFieldListConst.PROXY_ID_NO_BLACK))
                {
                    model.BtnShowOwner = FrameController.BtnShowOwner_Text1;
                }

                if (((frameNaTISErrorFieldList.Contains(FrameNaTISErrorFieldListConst.SURNAME_BLACK)
                    || frameNaTISErrorFieldList.Contains(FrameNaTISErrorFieldListConst.PO_ADDR1_BLACK)
                    || frameNaTISErrorFieldList.Contains(FrameNaTISErrorFieldListConst.PO_CODE_BLACK))
                    ) && frameDetails.FrameProxyFlag.Equals("Y"))
	            {
                    model.BtnShowOwner = FrameController.BtnShowOwner_Text1;
	            }
            }
        }

        private void InitAdjudicateModel(AdjudicateFrameModel model, ValidationStatus validateStatus)
        {
            bool tableASDVisible = false;
            string allowContinueDisplay = "none";
            bool _ISASD = false;

            int authIntNo = (int)Session["autIntNo"];
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            Session["adjAt100"] = Session["adjAt100"] ?? AuthorityRulesManager.GetIsOrNotAdjAt100(userInfo.UserName, authIntNo);
            FrameList frames = (FrameList)Session["FrameList"];

            int statusVerified = 500;
            int statusProsecute = 600;
            int statusNatis = 50;
            int statusCancel = 999;

            if ((bool)Session["NaTISLast"])
            {
                statusVerified = 800;
                statusProsecute = 710;
                statusNatis = 710;
            }

            string adjOfficerNo = string.Empty;
            if (AuthorityRulesManager.GetIsOrNotShowOfficer(userInfo.UserName, authIntNo))
            {
                adjOfficerNo = Session["OfficerNo"].ToString();
            }

            Session["asdInvalidRejIntNo"] = Session["asdInvalidRejIntNo"] ?? RejectionManager.GetASDInvalidRejIntNo(userInfo);

            switch (validateStatus)
            {
                case ValidationStatus.Adjudication:
                    model.PageName = FrameController.Adjudication_PageName;
                    model.IsInvestigation = false;
                    ViewBag.ShowInvestigationReason = false;
                    //20130104 added by Nancy for Check 30 day rule
                    bool isActive = AuthorityRulesManager.GetIsOrNotEnableFishpond(userInfo.UserName, authIntNo);//20130108 updated by Nancy for check finshpind
                    int dateRule30Days = DateRulesManager.GetNoOfDays(userInfo, authIntNo);
                    DateTime offenceDate = new FrameService().GetByFrameIntNo(frames.Current).OffenceDate;
                    TimeSpan ts = DateTime.Now - offenceDate;
                    ViewBag.ShowInvestigation = isActive && ts.Days <= dateRule30Days ? true : false;
                    break;
                case ValidationStatus.Investigation:
                    model.PageName = FrameController.Investigation_PageName;
                    model.IsInvestigation = true;
                    if (AuthorityRulesManager.GetIsOrNotEnableFishpond(userInfo.UserName, authIntNo))
                    {//20130109 added by Nancy
                        ViewBag.ShowInvestigationReason = true;
                    }
                    else
                        ViewBag.ShowInvestigationReason = false;
                    ViewBag.ShowInvestigation = false;//20130104 added by Nancy for ShowInvestigation is not  null
                    break;
                default:
                    break;
            }

            model.VehicleMakers = VehicleMakeManager.GetSelectList();
            model.VehicleTypes = VehicleTypeManager.GetSelectList();
            model.RejectReasons = RejectionManager.GetSelectList();
            //model.RejectReasons = RejectionManager.GetSelectListValueWithText();
            model.InvestigateReasons = InvestigateReasonManager.GetSelectList();//20130103 added by Nancy for investigatereason
            

            model.FilmNo = frames.FilmNumber;
            model.AllowContinue = true;

            FrameDetails frameDetails = FrameManager.GetFrameDetails(frames.Current);
            model.FrameNo = frameDetails.FrameNo;
            model.Sequence = frameDetails.Sequence;
            model.RegNo = frameDetails.RegNo;
            //Jerry 2013-04-22 add IsParentFrame
            model.IsParentFrame = !frameDetails.ParentFrameIntNo.HasValue ? true : false;
            model.AllowSecondaryOffence = AuthorityRulesManager.GetIsOrNotShowSecondaryOffence(userInfo.UserName, authIntNo);

            VerificationBin verBin = VerificationBinManager.GetByRegNo(frameDetails.RegNo);
            model.VeBiComment = (verBin == null || string.IsNullOrEmpty(verBin.VeBiComment)) ? 
                FrameController.VerBinComment : 
                verBin.VeBiComment;

            model.SpeedZone = frameDetails.SpeedLimit;
            model.LocationDescr = frameDetails.LocDescr;
            model.OfficerDetails = frameDetails.OfficerDetails;

            model.FirstSpeed = frameDetails.FirstSpeed;
            model.SecondSpeed = frameDetails.SecondSpeed;

            model.OffenceDate = frameDetails.OffenceDate.ToString("yyyy/MM/dd");
            model.OffenceTime = frameDetails.OffenceDate.ToString("HH:mm");

            model.VehicleMaker = frameDetails.VMIntNo;
            model.VehicleType = frameDetails.VTIntNo;
            model.RejNoneValue = RejectionEnum.Init("en-US", userInfo.UserName).NONE.Value;
            model.RejExpiredValue = RejectionEnum.Init("en-US", userInfo.UserName).EXPIRED.Value;
            model.RejectReason = frameDetails.RejIntNo;
            model.InvestigateReason = frameDetails.InReIntNo;//20120104 added by Nancy for show investigate reason
            if (validateStatus == ValidationStatus.Investigation && frameDetails.InReIntNo == 0)
            {//20120109 added by Nancy(check whether show investigate reason in investigate)
                ViewBag.ShowInvestigationReason = false;
            }

            int natisVtIntNo = -1;
            model.NatisVehicleType = GetVehicleType(frameDetails.FrameNatisVTCode, out natisVtIntNo);
            model.NatisVtIntNo = natisVtIntNo;
            model.ErrorNatisVTNull = natisVtIntNo == 0 ? string.Format(FrameController.errorNatisVTNull, frameDetails.FrameNatisVTCode) : string.Empty;
            if (natisVtIntNo > 0 && natisVtIntNo != frameDetails.VTIntNo)
            {
                model.NatisVTString = string.Format("{0}({1})", frameDetails.FrameNatisVTCode, model.NatisVehicleType);
            }

            if (frameDetails.ASD2ndCameraIntNo > 0)
            {
                tableASDVisible = true;
                model.ASDGPSDateTime1 = frameDetails.ASDGPSDateTime1.ToString("yyyy-MM-dd HH:mm");
                model.ASDGPSDateTime2 = frameDetails.ASDGPSDateTime2.ToString("yyyy-MM-dd HH:mm");

                model.ASDLane1 = frameDetails.ASDSectionStartLane;
                model.ASDLane2 = frameDetails.ASDSectionEndLane;

                if (frameDetails.RejIntNo == (int)Session["asdInvalidRejIntNo"])
                {
                    _ISASD = true;
                }
            }
            else
            {
                tableASDVisible = false;
            }

            if ((bool)Session["adjAt100"])
            {
                model.PnlNatisDataVisible = true;
                VehicleMakeDetails vehicleMakeDetails = VehicleMakeManager.GetVehicleMakeDetailsByCode(frameDetails.FrameNatisVMCode, "N");
                model.LblNatisVehicleMakerText =
                    vehicleMakeDetails.VMDescr == null ? 
                    string.Format(FrameController.VMDescr_Text1, frameDetails.FrameNatisVMCode) :
                    string.Format(FrameController.VMDescr_Text2, vehicleMakeDetails.VMDescr);

                VehicleTypeDetails vehicleTypeDetails = VehicleTypeManager.GetVehicleTypeDetailsByCode(frameDetails.FrameNatisVTCode, "N");
                model.LblNatisVehicleTypeText =
                    vehicleTypeDetails.VTDescr == null ?
                    string.Format(FrameController.VTDescr_Text1, frameDetails.FrameNatisVTCode) :
                    string.Format(FrameController.VTDescr_Text2, vehicleTypeDetails.VTDescr);

                if (string.IsNullOrEmpty(frameDetails.FrameNatisRegNo))
                {
                    Session["NatisData"] = false;
                    model.LblRegistrationNo =FrameController.LblRegistrationNo_Text1; 
                }
                else
                {
                    Session["NatisData"] = true;
                    model.LblRegistrationNo = string.Format(FrameController.LblRegistrationNo_Text2, frameDetails.FrameNatisRegNo);
                }
            }
            //hidPreUpdatedRejReason.Value = ddlRejection.SelectedItem.Text;
            //hidPreUpdatedAllowContinue.Value = frameDet.AllowContinue;
            if (frameDetails.AllowContinue.Equals("N"))
            {
                model.ErrorMessage = frameDetails.ConfirmError;
                model.AllowContinue = false;
                allowContinueDisplay = string.Empty;
            }

            model.Navigation = string.Format(FrameController.Navigation_Text, frames);

            if (AuthorityRulesManager.GetIsOrNotShowImageSettingButton(userInfo.UserName, authIntNo))
            {
                model.SaveImageSettingsVisible = true;
                model.SaveSettingsForImageVisible = true;
            }
            else
            {
                model.SaveImageSettingsVisible = false;
                model.SaveSettingsForImageVisible = false;
            }

            //if ((bool)Session["AdjAt100"])
            //{

            //}

            ViewBag.AllowContinueDisplay = allowContinueDisplay;
            ViewBag.ISASD = _ISASD;
            ViewBag.TableASDVisible = tableASDVisible;

            string hidTypes =
            "{StatusVerified:'" + statusVerified + "'," +
            "RowVersion:'" + frameDetails.RowVersion + "'," +
            "PreUpdatedRegNo:'" + frameDetails.RegNo + "'," +
            "FrameIntNo:'" + frames.Current + "'," +
            "StatusProsecute:'" + statusProsecute + "'," +
            "StatusCancel:'" + statusCancel + "'," +
            "StatusReturnToNatis:'" + statusNatis + "'," +
            "PreRejIntNo:'" + frameDetails.RejIntNo + "'," +
            "PreUpdatedAllowContinue:'" + frameDetails.AllowContinue + "'}";
            ViewBag.HidParams = hidTypes;

        }

        private string GetVehicleMaker(string vmCode)
        {
            string desc = VehicleMakeManager.GetVehicleMakeDetailsByCode(vmCode, "N").VMDescr;
            return string.IsNullOrEmpty(desc) ? string.Format(FrameController.VMDescr_Text3,vmCode) : desc;
        }

        private string GetVehicleType(string vtCode, out int vtIntNo)
        {
            VehicleTypeDetails vtDetail = VehicleTypeManager.GetVehicleTypeDetailsByCode(vtCode, "N");
            vtIntNo = vtDetail.VTIntNo;
            return string.IsNullOrEmpty(vtDetail.VTDescr) ? string.Format(FrameController.VTDescr_Text3, vtCode) : vtDetail.VTDescr;
        }

        #region bus lane
        public Film GetFilmByFilmNo(string filmNo)
        {
            FilmService service = new FilmService();
            return service.GetByFilmNo(filmNo);

        }
        #endregion

		//Jerry 2013-05-20 verify secondary frame
        private string VerifySecondaryFrame(string process, string rejReason, int rejIntNo, string registration, string vehType,
            string vehMake, string userName, string processType, int statusVerified, int statusRejected, int statusNatis, int statusInvestigate,
            int speed1, int speed2, DateTime offenceDate, string frameNo, string preUpdatedRegNo, string preUpdatedRejReason,
            bool isNaTISLast, int frameIntNo, bool allowContinue, string preUpdatedAllowContinue, Int64 nRowVersion, int autIntNo, bool adjAt100)
        {
            string returnMessage = "";
            FrameList frameList = (FrameList)Session["frameList"];
            FrameUpdate frameUpdate = new FrameUpdate(Config.ConnectionString, ref frameList);

            returnMessage = frameUpdate.Verify(process, rejReason, rejIntNo, registration, vehType, vehMake, userName, processType, statusVerified,
                           statusRejected, statusNatis, statusInvestigate, speed1, speed2, offenceDate, frameNo, preUpdatedRegNo, preUpdatedRejReason,
                            isNaTISLast, frameIntNo, allowContinue, preUpdatedAllowContinue, nRowVersion, autIntNo, adjAt100);
            return returnMessage;
    }



		//Jerry 2013-05-20 AdjudicateSecondaryFrame frame
        private string AdjudicateSecondaryFrame(string processName, string rejReason, int rejIntNo, string registration, bool changedMake, string vehType,
            string vehMake, string userName, string processType, int statusProsecute, int statusCancel, int statusNatis,
            string frameNo, string preUpdatedRegNo, string preUpdatedRejReason, string sessionAdjOfficerNo, int sessionUSIntNo,
            int frameIntNo, bool allowContinue, string preUpdatedAllowContinue, Int64 nRowVersion, bool isNaTISLast, int autIntNo, bool bNatisData,
            bool adjAt100, bool reject, bool sendToNatis, bool saveImageSettings, decimal contrast, decimal brightness)
        {
            string returnMessage = "";
            FrameList frameList = (FrameList)Session["frameList"];
            FrameUpdate frameUpdate = new FrameUpdate(Config.ConnectionString, ref frameList);

            returnMessage = frameUpdate.Adjudicate(processName, rejReason, rejIntNo, registration, changedMake, vehType, vehMake, userName, processType,
                              statusProsecute, statusCancel, statusNatis, frameNo, preUpdatedRegNo, preUpdatedRejReason,  sessionAdjOfficerNo,  sessionUSIntNo,
                              frameIntNo,  allowContinue,  preUpdatedAllowContinue, nRowVersion,  isNaTISLast,  autIntNo,  bNatisData,  adjAt100,  reject,  sendToNatis,
                              saveImageSettings, contrast, brightness);
            return returnMessage;
}
    }
}
