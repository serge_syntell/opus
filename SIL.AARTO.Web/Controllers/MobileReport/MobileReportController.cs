﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.ViewModels.MobileReport;

namespace SIL.AARTO.Web.Controllers.MobileReport
{
     [AARTOErrorLog, LanguageFilter]
    public class MobileReportController : Controller
    {
         private SIL.MobileInterface.DAL.Services.NoticeReceivedFromMobileDeviceService noticeReceivedFromMobileDeviceService = new SIL.MobileInterface.DAL.Services.NoticeReceivedFromMobileDeviceService();
         private SIL.MobileInterface.DAL.Services.TrafficOfficerService trafficOfficerService = new SIL.MobileInterface.DAL.Services.TrafficOfficerService();
         private SIL.MobileInterface.DAL.Services.DocumentTypeIdService documentTypeIdService = new SIL.MobileInterface.DAL.Services.DocumentTypeIdService();
         private SIL.MobileInterface.DAL.Services.DocumentStatusService documentStatusService = new SIL.MobileInterface.DAL.Services.DocumentStatusService();

         string LastUser
         {
             get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
         }

         [AcceptVerbs(HttpVerbs.Get)]
         public ActionResult MobileReportList(string startDate = "", string endDate = "", int trafficeOfficerID = 0, int documentTypeID = 0, int documentStatusID = 0, int pageIndex = 0, string msg = "")
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            NoticeReceivedFromMobileDeviceModel model = new NoticeReceivedFromMobileDeviceModel();
            try
            {
                if (!string.IsNullOrEmpty(startDate))
                    model.DateStart = string.Format(startDate, "yyyy-MM-dd");
                else
                    model.DateStart = DateTime.Now.Date.ToString("yyyy-MM-dd");

                if (!string.IsNullOrEmpty(endDate))
                    model.DateEnd = string.Format(endDate, "yyyy-MM-dd");
                else
                    model.DateEnd = DateTime.Now.Date.ToString("yyyy-MM-dd");

                model.TrafficeOfficerID = trafficeOfficerID;
                model.DocumentTypeID = documentTypeID;
                model.DocumentStatusID = documentStatusID;
                model.PageIndex = pageIndex;
                model.Msg = msg;

                InitModel(model);
            }
            catch (Exception ex)
            {
                model.Msg = ex.Message;
            }

            return View(model);
        }

         [AcceptVerbs(HttpVerbs.Post)]
         public ActionResult MobileReportList(NoticeReceivedFromMobileDeviceModel model)
         {
             if (Session["UserLoginInfo"] == null)
             {
                 return RedirectToAction("login", "account");
             }

             InitModel(model);
             if (model.PageReportList.Count <= 0)
                 model.Msg = SIL.AARTO.Web.Resource.MobileReport.MobileReportList.NoData;
             else
                 model.Msg = "";

             return View(model);
         }

         private void InitModel(NoticeReceivedFromMobileDeviceModel model)
         {
             model.TotalCount = 0;
             model.PageSize = Config.PageSize;

             model.TrafficeOfficerList = GetTrafficeOfficerSelectList();
             model.DocumentStatusList = GetDocumentStatusSelectList();
             model.DocumentTypeList = GetDocumentTypeSelectList();

             List<NoticeReceivedFromMobileDeviceListModel> PageReportList = new List<NoticeReceivedFromMobileDeviceListModel>();
             NoticeReceivedFromMobileDeviceListModel noticeReceivedFromMobileDeviceListModel;

             using (IDataReader reader = noticeReceivedFromMobileDeviceService.GetMobileReportList(Convert.ToDateTime(model.DateStart), Convert.ToDateTime(model.DateEnd).AddDays(1), 
                 model.TrafficeOfficerID, model.DocumentTypeID, model.DocumentStatusID, model.PageSize, model.PageIndex))
             {
                 while (reader.Read())
                 {
                     noticeReceivedFromMobileDeviceListModel = new NoticeReceivedFromMobileDeviceListModel();
                     noticeReceivedFromMobileDeviceListModel.MoDeIMEI = reader["MoDeIMEI"].ToString();
                     noticeReceivedFromMobileDeviceListModel.OfficerCode = reader["OfficerCode"].ToString();
                     noticeReceivedFromMobileDeviceListModel.OfficerName = reader["OfficerName"].ToString();
                     noticeReceivedFromMobileDeviceListModel.OffenceDate = reader["OffenceDate"] == null ? "" : Convert.ToDateTime(reader["OffenceDate"].ToString()).ToString("yyyy-MM-dd");
                     noticeReceivedFromMobileDeviceListModel.OffenceTime = reader["OffenceDate"] == null ? "" : Convert.ToDateTime(reader["OffenceDate"].ToString()).ToString("HH:mm");
                     noticeReceivedFromMobileDeviceListModel.NoticeNumber = reader["NoticeNumber"].ToString();
                     noticeReceivedFromMobileDeviceListModel.GPS = reader["GPS"] == null ? "" : reader["GPS"].ToString();
                     noticeReceivedFromMobileDeviceListModel.TransactionUploadDate = reader["TransactionUploadDate"] == null ? "" : Convert.ToDateTime(reader["TransactionUploadDate"].ToString()).ToString("yyyy-MM-dd HH:mm");
                     noticeReceivedFromMobileDeviceListModel.DSDescription = reader["DSDescription"] == null ? "" : reader["DSDescription"].ToString();
                     noticeReceivedFromMobileDeviceListModel.DTDescription = reader["DTDescription"] == null ? "" : reader["DTDescription"].ToString();
                     noticeReceivedFromMobileDeviceListModel.IsComplete = reader["IsComplete"] == null ? "YES" : Convert.ToBoolean(reader["IsComplete"].ToString()) ? "YES" : "NO";
                     noticeReceivedFromMobileDeviceListModel.IsPrinted = reader["IsPrinted"] == null ? "NO" : Convert.ToBoolean(reader["IsPrinted"].ToString()) ? "YES" : "NO";

                     PageReportList.Add(noticeReceivedFromMobileDeviceListModel);
                 }
                 model.PageReportList = PageReportList;

                 if (reader.NextResult())
                 {
                     if (reader.Read())
                     {
                         model.TotalCount = Convert.ToInt32(reader["TotalCount"].ToString());
                     }
                 }
             }
             
         }

         private SelectList GetTrafficeOfficerSelectList()
         {
             List<SelectListItem> list = new List<SelectListItem>();
             list.Add(new SelectListItem() { Value = "0", Text = SIL.AARTO.Web.Resource.MobileReport.MobileReportList.All });
             List<SIL.MobileInterface.DAL.Entities.TrafficOfficer> trafficOfficerList = trafficOfficerService.GetAll().OrderBy(to => to.TrOfInitials).ToList();
             foreach (SIL.MobileInterface.DAL.Entities.TrafficOfficer trafficOfficer in trafficOfficerList)
             {
                 list.Add(new SelectListItem() { Value = (trafficOfficer.TrafficOfficerId).ToString(), Text = trafficOfficer.TrOfInitials + " , " + trafficOfficer.TrOfSurname });
             }

             return new SelectList(list, "Value", "Text");
         }

         private SelectList GetDocumentTypeSelectList()
         {
             List<SelectListItem> list = new List<SelectListItem>();
             list.Add(new SelectListItem() { Value = "0", Text = SIL.AARTO.Web.Resource.MobileReport.MobileReportList.All });

             List<SIL.MobileInterface.DAL.Entities.DocumentTypeId> documentTypeList = documentTypeIdService.GetAll().OrderBy(dt => dt.DocumentTypeId).ToList();
             foreach (SIL.MobileInterface.DAL.Entities.DocumentTypeId documentType in documentTypeList)
             {
                 list.Add(new SelectListItem() { Value = (documentType.DocumentTypeId).ToString(), Text = documentType.DtDescription });
             }

             return new SelectList(list, "Value", "Text");
         }

         private SelectList GetDocumentStatusSelectList()
         {
             List<SelectListItem> list = new List<SelectListItem>();
             list.Add(new SelectListItem() { Value = "0", Text = SIL.AARTO.Web.Resource.MobileReport.MobileReportList.All });

             List<SIL.MobileInterface.DAL.Entities.DocumentStatus> documentStatusList = documentStatusService.GetAll().OrderBy(ds => ds.DsCode).ToList();
             foreach (SIL.MobileInterface.DAL.Entities.DocumentStatus documentStatus in documentStatusList)
             {
                 list.Add(new SelectListItem() { Value = (documentStatus.DocumentStatusId).ToString(), Text = documentStatus.DsDescription });
             }

             return new SelectList(list, "Value", "Text");
         }

         [AcceptVerbs(HttpVerbs.Get)]
         public ActionResult ExportReport(string startDate = "", string endDate = "", int trafficeOfficerID = 0, int documentTypeID = 0, int documentStatusID = 0)
         {
             if (Session["UserLoginInfo"] == null)
             {
                 return RedirectToAction("login", "account");
             }

             NoticeReceivedFromMobileDeviceModel model = new NoticeReceivedFromMobileDeviceModel();
             try
             {
                 if (!string.IsNullOrEmpty(startDate))
                     model.DateStart = string.Format(startDate, "yyyy-MM-dd");
                 else
                     model.DateStart = DateTime.Now.Date.ToString("yyyy-MM-dd");

                 if (!string.IsNullOrEmpty(endDate))
                     model.DateEnd = string.Format(endDate, "yyyy-MM-dd");
                 else
                     model.DateEnd = DateTime.Now.Date.ToString("yyyy-MM-dd");

                 model.TrafficeOfficerID = trafficeOfficerID;
                 model.DocumentTypeID = documentTypeID;
                 model.DocumentStatusID = documentStatusID;

                 DataTable table = GetDataTableData(model);
                 if (table != null && table.Rows.Count > 0)
                 {
                     StringWriter sw = new StringWriter();
                     sw.WriteLine("Device" + "\t" + "Officer Code" + "\t" + "Officer Name" + "\t" + "Offence Date" + "\t" + "Offence Time" + "\t" + 
                         "Notice Number" + "\t" + "GPS" + "\t" + "Transaction Upload Date" + "\t" + "Document Status" + "\t" + "Document Type" + "\t" + 
                         "Is Complete" + "\t" + "Is Printed");

                     foreach (DataRow dr in table.Rows)
                     {
                         sw.WriteLine(dr["MoDeIMEI"] + "\t" + dr["OfficerCode"] + "\t" + dr["OfficerName"] + "\t" + dr["OffenceDate"] + "\t" + dr["OffenceTime"] + "\t" +
                             dr["NoticeNumber"] + "\t" + dr["GPS"] + "\t" + dr["TransactionUploadDate"] + "\t" + dr["DSDescription"] + "\t" + dr["DTDescription"] + "\t" +
                             dr["IsComplete"] + "\t" + dr["IsPrinted"]);
                     }
                     sw.Close();
                     Response.AddHeader("Content-Disposition", "attachment; filename=MobileReport.xls");
                     Response.ContentType = "application/ms-excel";
                     Response.ContentEncoding = System.Text.Encoding.UTF8;
                     Response.Write(sw);
                     Response.End();
                 }
                 else
                 {
                     Response.Write(SIL.AARTO.Web.Resource.MobileReport.MobileReportList.NoData);
                     Response.Flush();
                     Response.End();
                 }
                 
             }
             catch (Exception ex)
             {
                 model.Msg = ex.Message;
             }

             return View();
         }

         private DataTable GetDataTableData(NoticeReceivedFromMobileDeviceModel model)
         {
             DataTable table = CreateTable();
             
             using (IDataReader reader = noticeReceivedFromMobileDeviceService.GetMobileReportList(Convert.ToDateTime(model.DateStart).AddDays(-1), Convert.ToDateTime(model.DateEnd).AddDays(1),
                 model.TrafficeOfficerID, model.DocumentTypeID, model.DocumentStatusID, 0, 0))
             {
                 while (reader.Read())
                 {
                     DataRow row = table.NewRow();
                     row["MoDeIMEI"] = reader["MoDeIMEI"].ToString();
                     row["OfficerCode"] = reader["OfficerCode"].ToString();
                     row["OfficerName"] = reader["OfficerName"].ToString();
                     row["OffenceDate"] = reader["OffenceDate"] == null ? "" : Convert.ToDateTime(reader["OffenceDate"].ToString()).ToString("yyyy-MM-dd");
                     row["OffenceTime"] = reader["OffenceDate"] == null ? "" : Convert.ToDateTime(reader["OffenceDate"].ToString()).ToString("HH:mm");
                     row["NoticeNumber"] = reader["NoticeNumber"].ToString();
                     row["GPS"]  = reader["GPS"] == null ? "" : reader["GPS"].ToString();
                     row["TransactionUploadDate"] = reader["TransactionUploadDate"] == null ? "" : Convert.ToDateTime(reader["TransactionUploadDate"].ToString()).ToString("yyyy-MM-dd HH:mm");
                     row["DSDescription"] = reader["DSDescription"] == null ? "" : reader["DSDescription"].ToString();
                     row["DTDescription"] = reader["DTDescription"] == null ? "" : reader["DTDescription"].ToString();
                     row["IsComplete"] = reader["IsComplete"] == null ? "YES" : Convert.ToBoolean(reader["IsComplete"].ToString()) ? "YES" : "NO";
                     row["IsPrinted"] = reader["IsPrinted"] == null ? "NO" : Convert.ToBoolean(reader["IsPrinted"].ToString()) ? "YES" : "NO";

                     table.Rows.Add(row);
                 }
                 
             }

             return table;
         }

         private DataTable CreateTable()
         {
             DataTable table = new DataTable();
             DataColumn column = new DataColumn();
             column.DataType = System.Type.GetType("System.String");
             column.ColumnName = "MoDeIMEI";
             table.Columns.Add(column);

             column = new DataColumn();
             column.DataType = System.Type.GetType("System.String");
             column.ColumnName = "OfficerCode";
             table.Columns.Add(column);

             column = new DataColumn();
             column.DataType = System.Type.GetType("System.String");
             column.ColumnName = "OfficerName";
             table.Columns.Add(column);

             column = new DataColumn();
             column.DataType = System.Type.GetType("System.String");
             column.ColumnName = "OffenceDate";
             table.Columns.Add(column);

             column = new DataColumn();
             column.DataType = System.Type.GetType("System.String");
             column.ColumnName = "OffenceTime";
             table.Columns.Add(column);

             column = new DataColumn();
             column.DataType = System.Type.GetType("System.String");
             column.ColumnName = "NoticeNumber";
             table.Columns.Add(column);

             column = new DataColumn();
             column.DataType = System.Type.GetType("System.String");
             column.ColumnName = "GPS";
             table.Columns.Add(column);

             column = new DataColumn();
             column.DataType = System.Type.GetType("System.String");
             column.ColumnName = "TransactionUploadDate";
             table.Columns.Add(column);

             column = new DataColumn();
             column.DataType = System.Type.GetType("System.String");
             column.ColumnName = "DSDescription";
             table.Columns.Add(column);

             column = new DataColumn();
             column.DataType = System.Type.GetType("System.String");
             column.ColumnName = "DTDescription";
             table.Columns.Add(column);

             column = new DataColumn();
             column.DataType = System.Type.GetType("System.String");
             column.ColumnName = "IsComplete";
             table.Columns.Add(column);

             column = new DataColumn();
             column.DataType = System.Type.GetType("System.String");
             column.ColumnName = "IsPrinted";
             table.Columns.Add(column);

             return table;
         }
    }
}
