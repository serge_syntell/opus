﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.ReportManagement;
using System.Data;
using System.Text;
using System.IO;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.Web.ViewModels.ReportManagement;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using System.Configuration;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers
{
    public partial class ReportManagementController : Controller
    {
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

        public ActionResult NoticeStatusList()
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            NoticeStatusDateModel model = new NoticeStatusDateModel();
            model.NoticeStatusList = TmpNoticeStatusReportManager.GetAllNoticeStatusReport();
            TList<TmpNoticeReportDate> ranageDates = TmpNoticeReportDateManager.GetNoticeReportDate();
            if (ranageDates.Count > 0)
            {
                model.StartDate = ranageDates[0].StartDate.ToString("yyyy/MM/dd");
                model.EndDate = ranageDates[0].EndDate.ToString("yyyy/MM/dd");
                //model.GeneratedDate = ranageDates[0].GenerateReportDate.HasValue ?
                //    ((DateTime)ranageDates[0].GenerateReportDate).ToString("yyyy/MM/dd") : "Not Generated!";
                //model.IsGenerated = ranageDates[0].GenerateReportDate.HasValue;
                model.GeneratedDate = ranageDates[0].CreateDate.HasValue ?
                    ((DateTime)ranageDates[0].CreateDate).ToString("yyyy/MM/dd") : "Not Generated!";
                model.IsGenerated = ranageDates[0].CreateDate.HasValue;
            }
            return View(model);
        }

        public FileResult NoticeStatusCSV()
        {
            TList<TmpNoticeStatusReport> noticeStatusList = TmpNoticeStatusReportManager.GetAllNoticeStatusReport();
            StringBuilder sb = new StringBuilder();
            sb.Append("Report Date")
              .Append(",Film Type")
              .Append(",Awaitinge NaTIS Export Count")
              .Append(",Awaitinge NaTIS Export Value")
              .Append(",Awaitinge NaTIS Return Count")
              .Append(",Awaitinge NaTIS Return Value")
              .Append(",Awaiting Verification Count")
              .Append(",Awaiting Verification Value")
              .Append(",Awaiting Adjudication Count")
              .Append(",Awaiting Adjudication Value")
              .Append(",Awaiting 1stNotice Generation Count")
              .Append(",Awaiting 1stNotice Generation Value")
              .Append(",Awaiting 1stNotice Printing Count")
              .Append(",Awaiting 1stNotice Printing Value")
              .Append(",Awaiting 2ndNotice Generation Count")
              .Append(",Awaiting 2ndNotice Generation Value")
              .Append(",Awaiting 2ndNotice Printing Count")
              .Append(",Awaiting 2ndNotice Printing Value")
              .Append(",Awaiting Data Wash Count")
              .Append(",Awaiting Data Wash Value")
              .Append(",Awaiting Summons Generation Count")
              .Append(",Awaiting Summons Generation Value")
              .Append(",Awaiting Summons Server Allocation Count")
              .Append(",Awaiting Summons Server Allocation Value")
              .Append(",Awaiting Court Date Count")
              .Append(",Awaiting Court Date Value")
              .Append(",Awaiting S54Printing Count")
              .Append(",Awaiting S54Printing Value")
              .Append(",Awaiting RepResults Count")
              .Append(",Awaiting RepResults Value")
              .Append(",Awaiting Case Results Count")
              .Append(",Awaiting Case Results Value")
              .Append(",Awaiting WOAprinting Count")
              .Append(",Awaiting WOAprinting Value")
              .Append(",Paid Count")
              .Append(",Paid Value")
              .Append(",Withdrawnor Cancelled Count")
              .Append(",Withdrawnor Cancelled Value")
              .Append(",Reduced Count")
              .Append(",Reduced Value")
              .Append(",Total Remaining Outstanding Count")
              .Append(",Total Remaining Outstanding Value")
              .Append("\r\n");
            foreach (TmpNoticeStatusReport row in noticeStatusList)
            {
                sb.Append(string.Format("{0:yyyy/MM/dd}", row.ReportDate))
                  .Append(',').Append(row.FilmType)
                  .Append(',').Append(row.AwaitingeNaTisExportCount)
                  .Append(',').Append(row.AwaitingeNaTisExportValue)
                  .Append(',').Append(row.AwaitingeNaTisReturnCount)
                  .Append(',').Append(row.AwaitingeNaTisReturnValue)
                  .Append(',').Append(row.AwaitingVerificationCount)
                  .Append(',').Append(row.AwaitingVerificationValue)
                  .Append(',').Append(row.AwaitingAdjudicationCount)
                  .Append(',').Append(row.AwaitingAdjudicationValue)
                  .Append(',').Append(row.Awaiting1stNoticeGenerationCount)
                  .Append(',').Append(row.Awaiting1stNoticeGenerationValue)
                  .Append(',').Append(row.Awaiting1stNoticePrintingCount)
                  .Append(',').Append(row.Awaiting1stNoticePrintingValue)
                  .Append(',').Append(row.Awaiting2ndNoticeGenerationCount)
                  .Append(',').Append(row.Awaiting2ndNoticeGenerationValue)
                  .Append(',').Append(row.Awaiting2ndNoticePrintingCount)
                  .Append(',').Append(row.Awaiting2ndNoticePrintingValue)
                  .Append(',').Append(row.AwaitingDataWashCount)
                  .Append(',').Append(row.AwaitingDataWashValue)
                  .Append(',').Append(row.AwaitingSummonsGenerationCount)
                  .Append(',').Append(row.AwaitingSummonsGenerationValue)
                  .Append(',').Append(row.AwaitingSummonsServerAllocationCount)
                  .Append(',').Append(row.AwaitingSummonsServerAllocationValue)
                  .Append(',').Append(row.AwaitingCourtDateCount)
                  .Append(',').Append(row.AwaitingCourtDateValue)
                  .Append(',').Append(row.Awaitings54PrintingCount)
                  .Append(',').Append(row.Awaitings54PrintingValue)
                  .Append(',').Append(row.AwaitingRepResultsCount)
                  .Append(',').Append(row.AwaitingRepResultsValue)
                  .Append(',').Append(row.AwaitingCaseResultsCount)
                  .Append(',').Append(row.AwaitingCaseResultsValue)
                  .Append(',').Append(row.AwaitingWoAprintingCount)
                  .Append(',').Append(row.AwaitingWoAprintingValue)
                  .Append(',').Append(row.PaidCount)
                  .Append(',').Append(row.PaidValue)
                  .Append(',').Append(row.WithdrawnorCancelledCount)
                  .Append(',').Append(row.WithdrawnorCancelledValue)
                  .Append(',').Append(row.ReducedCount)
                  .Append(',').Append(row.ReducedValue)
                  .Append(',').Append(row.TotalRemainingOutstandingCount)
                  .Append(',').Append(row.TotalRemainingOutstandingValue)
                  .Append("\r\n");
            }
           
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            //punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.NoticeStatusReport, PunchAction.Other);  

            return File(Encoding.Default.GetBytes(sb.ToString()), "text/csv", "NoticeStatusReport.csv");
        }

        public JsonResult SaveReportDate(DateTime startDate, DateTime endDate)
        {
            TmpNoticeReportDate tmpDate = TmpNoticeReportDateManager.SetNoticeReportDate(startDate, endDate, ((UserLoginInfo)Session["UserLoginInfo"]).UserName);
            if (tmpDate != null)
            {
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.NoticeStatusReport, PunchAction.Change); 
                return Json(new { result = 0 }, "application/json");

            }
            else
            {
                return Json(new { result = -1, errorMsg = "Unexpected error occur!" }, "application/json");
            }
        }
    }
}
