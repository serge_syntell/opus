﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.ReportManagement;
using SIL.AARTO.Web.ViewModels.ReportManagement;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.Web.Resource.ReportManagement;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers
{
    [AARTOErrorLog, LanguageFilter]
    public partial class ReportManagementController : Controller
    {
        public ActionResult ShowReportEmailList(int reportCode = 0, int page = 0)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            return View(InitMode(reportCode,page));
        }

        public JsonResult SaveReportEmail(ReportEmailModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
            if (Save(model) == -1)
            {
                return Json(new { result = -1, errorMsg = ShowReportEmailListRes.ErrorMsg1 });
            }
            return Json(new { result = 0 });
        }

        public JsonResult DeleteReportEmail(long reaIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
            ReportEmailAddressManager.DeleteEmail(reaIntNo);
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.ReportEmailAddressManagement, PunchAction.Delete);
            return Json(new { result = 0 });
        }

        public JsonResult GetReportEmail(long reaIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

            ReportEmailAddress emailAddress = ReportEmailAddressManager.GetByReaIntNo(reaIntNo);

            return Json(new 
                { result = 0, emailAddress = new 
                    { 
                        ReaIntNo = emailAddress.ReaIntNo,
                        ReportCode = emailAddress.ReportCode,
                        ReaSurname = emailAddress.ReaSurname,
                        ReaIntitials = emailAddress.ReaIntitials,
                        ReaEmailAddress = emailAddress.ReaEmailAddress
                    } 
                });
        }

        private int Save(ReportEmailModel model)
        {
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            int errorInt;
            ReportEmailAddress emailAddress = new ReportEmailAddress
            {
                ReaIntNo = model.ReaIntNo,
                ReaSurname = model.Surname,
                ReaIntitials = model.Initials,
                ReaEmailAddress = model.EmailAddress,
                ReaDateCreated = DateTime.Now,
                ReaDateLastUpdated = DateTime.Now,
                ReportCode = model.InReportCode,
                LastUser = userInfo.UserName
            };

            if (model.ReaIntNo > 0)
            {
                errorInt = ReportEmailAddressManager.UpdateEmail(emailAddress);
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.ReportEmailAddressManagement, PunchAction.Change);
            }
            else
            {
                errorInt = ReportEmailAddressManager.AddEmail(emailAddress);
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.ReportEmailAddressManagement, PunchAction.Add);
            }
            return errorInt;
        }

        private ReportEmailModel InitMode(int reportCode, int page)
        {
            ReportEmailModel model = new ReportEmailModel();
            model.PageSize = Config.PageSize;
            model.ReportCodes = ReportConfigCodeManager.GetSelectList();
            reportCode = reportCode == 0 ? int.Parse(model.ReportCodes.First().Value) : reportCode;
            int totalCount;
            model.ReportEmails = ReportEmailAddressManager.GetPageByReportCode(reportCode, page, model.PageSize, out totalCount);
            model.TotalCount = totalCount;
            //model.ErrorMsg = message;
            model.PageIndex = page;
            return model;
        }
    }
}
