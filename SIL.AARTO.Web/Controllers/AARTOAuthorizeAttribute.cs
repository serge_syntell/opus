﻿using System.Web.Mvc;

namespace SIL.AARTO.Web.Controllers
{
    public class AARTOAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            if (filterContext.HttpContext.Session == null
                || filterContext.HttpContext.Session["UserLoginInfo"] == null)
            {
                GoToLogin(filterContext);
            }
        }

        void GoToLogin(AuthorizationContext filterContext)
        {
            var virtualPath = UrlHelper.GenerateContentUrl("~/Account/Login", filterContext.HttpContext);
            var hostName = filterContext.HttpContext.Request.ServerVariables["HTTP_HOST"];
            var js = string.Format("<script type='text/javascript'>window.top.location.href='http://{0}{1}';</script>", hostName, virtualPath);
            filterContext.HttpContext.Response.Write(js);
            filterContext.HttpContext.Response.End();
        }
    }
}
