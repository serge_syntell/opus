﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.ENatisErrorManagement;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource.ENatisErrorManagement;
using Stalberg.TMS;
using System.Configuration;
using System.Transactions;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.ENatisErrorManagement
{
    [AARTOErrorLog, LanguageFilter]
    public class ENatisErrorManagementController : Controller
    {
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
        //
        // GET: /ENatisError/

        public ActionResult ENatisErrorList(int page = 0, string searchStr="")
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            return View(InitMode(page, searchStr));
        }
        private ENatisErrorLogModel InitMode(int page, string searchStr)
        {
            ENatisErrorLogModel model = new ENatisErrorLogModel();
            model.PageSize = Config.PageSize;
            AARTO.DAL.Services.EnatisErrorLogService enatisErrorLogMgr = new SIL.AARTO.DAL.Services.EnatisErrorLogService();
            AARTO.DAL.Services.ViewEnatisErrorLogNoticeService viewEnatisErrorLogNoticeMgr = new SIL.AARTO.DAL.Services.ViewEnatisErrorLogNoticeService();

            DateTime eNELDate = new DateTime(1, 1, 1);
            DateTime eNELDatetemp = new DateTime(1, 1, 1);
            if (searchStr != "")
            {
                DateTime.TryParse(searchStr.Trim(), out eNELDate);
            }

            int totalCount;
            string where = " ENELHandleStatus=1 ";
          
            if (eNELDate!=eNELDatetemp)
            {
               // where += " and ENELDate ='" + eNELDate + "' ";
                where += " and  datename (yyyy,'" + eNELDate + "')=datename (yyyy,ENELDate) and datename(mm,'" + eNELDate + "')=datename (mm,ENELDate) and  datename(dd,'" + eNELDate + "')=datename (dd,ENELDate) ";
            }

            IList<AARTO.DAL.Entities.ViewEnatisErrorLogNotice> enatisErrorLogs = viewEnatisErrorLogNoticeMgr.GetPaged(where, "", page, model.PageSize, out totalCount);

            model.EnatisErrorLogs = enatisErrorLogs;
            
            model.TotalCount = totalCount;
          
            if (enatisErrorLogs != null && enatisErrorLogs.Count > 0)
            { }
            else
            {
                if (page == 0)
                {
                    model.ErrorMsg = "No related data or data has been processed！";
                }
                else if (page > 0)
                {
                    model.ErrorMsg = "No related data or You have dealt with some of the data , the remaining data is not more than " + page + " pages,You can click on the 'search' button again to see whether there are remaining data to be processed ?";

                }
            }
            model.PageIndex = page;
            return model;
        }

        public JsonResult GetOperReject(long eNELIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
            SIL.AARTO.DAL.Services.ChargeService aARTOChargeService = new AARTO.DAL.Services.ChargeService();
            AARTO.DAL.Services.EnatisErrorLogService enatisErrorLogMgr = new SIL.AARTO.DAL.Services.EnatisErrorLogService();
            EnatisErrorLog enatisErrorLog = enatisErrorLogMgr.GetByEnelIntNo(eNELIntNo);

            try
            {
                if (enatisErrorLog != null)
                {
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        AARTO.DAL.Services.NoticeService noticeMgr = new SIL.AARTO.DAL.Services.NoticeService();
                        Notice noticeModel = noticeMgr.GetByNotIntNo(Convert.ToInt32(enatisErrorLog.NotIntNo));
                        if (noticeModel != null)
                        {
                            noticeModel.NoticeStatus = 1100;//Rejected
                            noticeMgr.Update(noticeModel);

                            // update charge table ChargeStatus=1100
                            SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.Charge> chargeModelList = aARTOChargeService.GetByNotIntNo(noticeModel.NotIntNo);
                            if (chargeModelList != null && chargeModelList.Count > 0)
                            {
                                foreach (SIL.AARTO.DAL.Entities.Charge item in chargeModelList)
                                {
                                    item.ChargeStatus = 1100;
                                    aARTOChargeService.Update(item);
                                }

                            }


                            enatisErrorLog.EnelHandleStatus = 2;//reject
                            enatisErrorLog.EnelHandleDate = DateTime.Now;
                            enatisErrorLogMgr.Update(enatisErrorLog);

                            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.EnatisErrorLog, PunchAction.Change);  

                            scope.Complete();
                            return Json(new { result = 0 });
                        }
                        else
                        { return Json(new { result = -4 }); }

                    }

                }
            }
            catch (Exception e)
            {
                
                return Json(new { result = -1 });
            }


            return Json(new { result = -1 });

        }

        public JsonResult GetOperResend(long eNELIntNo)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
            AARTO.DAL.Services.EnatisErrorLogService enatisErrorLogMgr = new SIL.AARTO.DAL.Services.EnatisErrorLogService();
            EnatisErrorLog enatisErrorLog = enatisErrorLogMgr.GetByEnelIntNo(eNELIntNo);

            try
            {

                if (enatisErrorLog != null)
                {
                    string connstr = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString();
                    eNatisDB eNatisDB = new Stalberg.TMS.eNatisDB(connstr);
                    string error = "";
                    int notIntNo = 0;
                    if (enatisErrorLog.NotIntNo != null)
                    {
                        notIntNo = Convert.ToInt32(enatisErrorLog.NotIntNo);
                    }


                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        bool isSuccess = eNatisDB.SendToNTIDatabase(notIntNo, (Session["UserLoginInfo"] as UserLoginInfo).UserName, out error);
                        if (isSuccess)
                        {
                            enatisErrorLog.EnelHandleStatus = 3;//resend
                            enatisErrorLog.EnelHandleDate = DateTime.Now;
                            enatisErrorLogMgr.Update(enatisErrorLog);
                           
                            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                            punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.EnatisErrorLog, PunchAction.Change); 

                            scope.Complete();

                            return Json(new { result = 0 });
                        }
                        else
                        {
                            return Json(new
                            {
                                result = -3,
                                errorlist = new
                                {
                                    errorTip = error
                                }
                            });

                        }
                    }

                }
                else
                {
                    return Json(new { result = -1 });
                }
            }
            catch
            {
                return Json(new { result = -1 });
            }

            return Json(new { result = -1 });
        }



    }
}
