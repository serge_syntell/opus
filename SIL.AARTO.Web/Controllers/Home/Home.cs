using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using SIL.AARTO.BLL.Account;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Culture;
using System.Threading;

namespace SIL.AARTO.Web.Controllers.Home
{
    [AARTOErrorLog, LanguageFilter]
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        [Authorize]
        public ActionResult Index()
        {
            //throw new Exception("Error Message");
            if (Session["UserLoginInfo"] == null)
            {
                Redirect("~/Account/Login");
            }
            if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
            {
                ViewData["ReturnUrl"] = Request.QueryString["ReturnUrl"];
            }
            return View();
        }
        [Authorize]
        public ActionResult Left()
        {
            return View();
        }
        [Authorize]
        public ActionResult Middle()
        {
            return View();
        }

        [Authorize]
        public ActionResult Main()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Top()
        {
            UserManager userManager = new SIL.AARTO.BLL.Admin.UserManager();
            List<AuthorityEntity> autList = userManager.GetAllAuthority();
            int autIntNo = 0;
            if (Session["UserLoginInfo"] != null)
                autIntNo = ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo;
            SelectList list = new SelectList(autList as IEnumerable, "AutIntNo", "AutDesc", autIntNo);

            ViewData["AutList"] = list;

            ViewBag.Languages = System.Web.Helpers.Json.Encode(Config.SupportedLanguages);

            //ViewBag.CurrentLang = (string)(Session["CurrentLanguage"] ?? Config.DefaultLanguage);
            ViewBag.CurrentLang = Thread.CurrentThread.CurrentCulture.Name;

            ViewData["EnvironmentType"] = SystemParameter.GetSPByEnvironmentType("EnvironmentType");

            if (Request.QueryString["Login"] != null && Request.QueryString["Login"].Equals("logout"))
            {
                ViewData["SignOut"] = true;

            }

            //add by Rachel 20140818
            string version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            ViewData["VersionNumber"] = version;
            //end add by Rachel 20140818
            return View();
        }

        public JsonResult ChangeLanguage(string lsCode)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
            UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
            userInfo.CurrentLanguage = lsCode;
            Session["UserLoginInfo"] = userInfo;
            //Session["CurrentLanguage"] = lsCode;
            return Json(new { result = 0 });
        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult Top(FormCollection form)
        {
            if (Session["UserLoginInfo"] != null)
            {
                UserLoginInfo userInfo = ((UserLoginInfo)Session["UserLoginInfo"]);
                int userIntNo = userInfo.UserIntNo;
                int nUSIntNo = Convert.ToInt32(Session["USIntNo"]);
                string conStr = System.Configuration.ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString();
                Stalberg.TMS.UserDB db = new Stalberg.TMS.UserDB(conStr);

                // LMZ 14-03-2007 - used to logout on UserShift table
                //this will always be 0 as the session from TMS is not passed through to here.
                nUSIntNo = db.UserShiftEdit(nUSIntNo, "", "Logout", userInfo.UserName);
               
                Stalberg.TMS.UserDetails ud = (Stalberg.TMS.UserDetails)db.GetUserDetails(userIntNo);
                Stalberg.TMS.FilmDB film = new Stalberg.TMS.FilmDB(conStr);
                film.LockUser(0, ud.UserLoginName); 

                Session.RemoveAll();
                FormsAuthentication.SignOut();
            }

            int autIntNo = 0;
            UserManager userManager = new SIL.AARTO.BLL.Admin.UserManager();
            List<AuthorityEntity> autList = userManager.GetAllAuthority();
            if (Session["UserLoginInfo"] != null)
                autIntNo = ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo;
            SelectList list = new SelectList(autList as IEnumerable, "AutIntNo", "AutDesc", autIntNo);
            string tmsDomain = string.Empty; ;
            string Ip = GetServerIp();
            string HostName = Request.ServerVariables["SERVER_NAME"];
            if (String.IsNullOrEmpty(Ip))
            {
                tmsDomain = System.Configuration.ConfigurationManager.AppSettings["TMSDomainURL"];
            }
            else if (System.Configuration.ConfigurationManager.AppSettings["AARTODomainURL"].IndexOf(Ip) >= 0
                                || System.Configuration.ConfigurationManager.AppSettings["AARTODomainURL"].IndexOf(HostName) >= 0)
            {
                tmsDomain = System.Configuration.ConfigurationManager.AppSettings["TMSDomainURL"];
            }
            else if (System.Configuration.ConfigurationManager.AppSettings["AARTODomainExternalURL"].IndexOf(Ip) >= 0
                || System.Configuration.ConfigurationManager.AppSettings["AARTODomainExternalURL"].IndexOf(HostName) >= 0)
            {
                tmsDomain = System.Configuration.ConfigurationManager.AppSettings["TMSDomainExternalURL"];
            }
            else
            {
                tmsDomain = System.Configuration.ConfigurationManager.AppSettings["TMSDomainURL"];
            }

            return Redirect(tmsDomain + "/Login.aspx?Login=logout");
             

        }

        [AcceptVerbs(HttpVerbs.Post), Authorize]
        public ActionResult GetEnquiryUrl()
        {
            //int pageID = UserLoginManager.GetPageByUrl("/ViewOffence_Search.aspx").AaPageId;
            Message message = new Message();
            //message.Text = "/AutoLogin.aspx?PageID=" + pageID;
            message.Text = "/Enquiry/EnquiryList";
            message.Status = true;
            return Json(message);
        }

        [Authorize]
        public ActionResult SetDefaultAuth(int autIntNo)
        {
            Message message = new Message();
            if (autIntNo != 0)
            {
                Session["DefaultAuthority"] = autIntNo;
                Session["autIntNo"] = autIntNo;
                //****** please can you add code here to get the Authority name for the selected AutIntNo - it is needed in the Session object for use on labels
                SIL.AARTO.DAL.Entities.Authority authority = SIL.AARTO.BLL.Admin.AuthorityManager.GetAuthorityByAuthIntNo(autIntNo);
                if (authority != null)
                {
                    Session["autName"] = authority.AutName;
                    message.Status = true;
                }
                //2012-6-18, Henry add for show Natis or not
                UserLoginInfo userInfo = (UserLoginInfo)Session["UserLoginInfo"];
                Session["adjAt100"] = SIL.AARTO.BLL.CameraManagement.AuthorityRulesManager.GetIsOrNotAdjAt100(userInfo.UserName, autIntNo);
            }

            return Json(message);
        }

        public ActionResult ApplicationError()
        {
            if (Session["ErrorMessage"] != null)
            {
                ViewData["ErrorMessage"] = Session["ErrorMessage"].ToString();
            }
            return View();
        }

        private string GetServerIp()
        {
            string serverIP = string.Empty;
            try
            {

                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    serverIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                }
                else
                {
                    if (Request.ServerVariables["REMOTE_ADDR"] != null)
                    {
                        serverIP = Request.ServerVariables["REMOTE_ADDR"];
                    }
                }
            }
            catch (Exception ex)
            {
                // string msg = ex.Message;
                throw ex;
            }
            return serverIP;
        }

    }
}
