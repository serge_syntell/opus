﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.ViewModels.AppConfig;
using Stalberg.TMS.Data;
using SIL.AARTO.BLL.Utility;
using System.Configuration;


namespace SIL.AARTO.Web.Controllers.ApplicationConfiguration
{
    public class AppConfigController : Controller
    {

        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }
     
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
        public AppConfigViewModel appConfigViewModel   =  new AppConfigViewModel ( );
    
       /// <summary>
       /// //
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
       /// 
       
        [HttpGet]
        public ActionResult Edit()
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction ( "login", "account" );
            }

            AppConfigDB appConfigDB = new AppConfigDB ( connectionString );
            AppConfigDetail appConfigDetail = appConfigDB.GetAppConfigDetails ();

            if (appConfigDetail != null)
            {
                appConfigViewModel.BackupDestinationPath = appConfigDetail.BackupDestinationPath;
                appConfigViewModel.ConvertedDestination = appConfigDetail.ConvertedDestination;
                appConfigViewModel.email = appConfigDetail.Email;
                appConfigViewModel.ftpExportPath = appConfigDetail.FtpExportPath;
                appConfigViewModel.ftpExportServer = appConfigDetail.FtpExportServer;
                appConfigViewModel.ftpHostIP = appConfigDetail.FtpHostIP;
                appConfigViewModel.ftpHostPass = appConfigDetail.FtpHostPass;
                appConfigViewModel.ftpHostUser = appConfigDetail.FtpHostUser;
                appConfigViewModel.ftpHostPath = appConfigDetail.FtpHostPath;
                appConfigViewModel.MakeBackup = appConfigDetail.MakeBackup;
                appConfigViewModel.mode = appConfigDetail.Mode;
                appConfigViewModel.NotificationRecipient = appConfigDetail.NotificationRecipient;
                appConfigViewModel.SendEmailNotifications = appConfigDetail.SendEmailNotifications;
                appConfigViewModel.smtp = appConfigDetail.Smtp;
                appConfigViewModel.LogFileDestination = appConfigDetail.LogFileDestination;
                appConfigViewModel.imageFolder = appConfigDetail.ImageFolder;
                appConfigViewModel.Info = appConfigDetail.Info;
                appConfigViewModel.Error = appConfigDetail.Error;
                appConfigViewModel.Debug = appConfigDetail.Debug;
                appConfigViewModel.CopyDestinationPath = appConfigDetail.CopyDestinationPath;
                appConfigViewModel.AppConfigNO = appConfigDetail.AppConfigNO;
                appConfigViewModel.ftpReceivedPath = appConfigDetail.FtpReceivedPath;
                appConfigViewModel.ftpProcess = appConfigDetail.FtpProcess;
                appConfigViewModel.ftpHostServer = appConfigDetail.FtpHostServer;
                appConfigViewModel.Port = appConfigDetail.Port;
                appConfigViewModel.From = appConfigDetail.EmailFrom;
                appConfigViewModel.SmtpUser = appConfigDetail.SmtpUser;
                appConfigViewModel.SmtpPassword = appConfigDetail.SmtpPassword;
            }
            
            return View ( appConfigViewModel );
        }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="appConfig"></param>
       /// <returns></returns>
        [HttpPost]
        public ActionResult Edit ( AppConfigViewModel appConfig )
        {
            try
            {
                AppConfigDB appConfigDB = new AppConfigDB ( connectionString );
                int SaveAppConfig = appConfigDB.UpdateAppConfig (appConfig.CopyDestinationPath,
                                         appConfig.MakeBackup, appConfig.BackupDestinationPath,
                                         appConfig.LogFileDestination, appConfig.ConvertedDestination
                                        ,appConfig.SendEmailNotifications, appConfig.NotificationRecipient,
                                         appConfig.Info,appConfig.Error,appConfig.Debug,appConfig.Port,appConfig.From,
                                         appConfig.ftpProcess, appConfig.ftpExportServer, appConfig.ftpExportPath, 
                                         appConfig.ftpReceivedPath, appConfig.ftpHostServer, appConfig.ftpHostIP, 
                                         appConfig.ftpHostUser, appConfig.ftpHostPass, appConfig.ftpHostPath, 
                                         appConfig.email, appConfig.smtp, appConfig.mode, appConfig.imageFolder,
                                         appConfigViewModel.AppConfigNO, appConfig.SmtpUser, appConfig.SmtpPassword );
                return RedirectToAction("Edit");
            }
            catch
            {
                return View ("~/Views/Shared/Error.cshtml");
            }
        }

       
    }
}
