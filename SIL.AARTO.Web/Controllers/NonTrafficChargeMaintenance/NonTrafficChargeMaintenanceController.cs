﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Helpers.Paginator;
using SIL.AARTO.Web.Resource.NonTrafficChargeMaintenance;
using SIL.AARTO.Web.ViewModels.NonTrafficChargeMaintenance;
using Stalberg.TMS.Data;
namespace SIL.AARTO.Web.Controllers.NonTrafficChargeMaintenance
{
    [AARTOErrorLog]
    public partial class NonTrafficChargeMaintenanceController : Controller
    {
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
        readonly string cultureName = Thread.CurrentThread.CurrentCulture.Name;
        AuthorityService anthorityService = new AuthorityService();

        UnitOfMeasureService uomService = new UnitOfMeasureService();
        NonTrafficChargeService ntcService = new NonTrafficChargeService();
        NonTrafficChargeDB nonTrafficeChargeDB;
        SIL.AARTO.DAL.Entities.NonTrafficCharge ntc;
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }

        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }

        public ActionResult NonTrafficChargeList(int Page = 0, string SearchNTCCode = "", string SearchNTCDescr = "", string SearchUOMIntNo = "")
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            SearchNTCCode = string.IsNullOrEmpty(SearchNTCCode) ? "": HttpUtility.UrlDecode(SearchNTCCode).Trim();
            SearchNTCDescr = string.IsNullOrEmpty(SearchNTCDescr) ? "" : HttpUtility.UrlDecode(SearchNTCDescr).Trim();

            NonTrafficChargeMaintenanceModel model = new NonTrafficChargeMaintenanceModel();
            int totalCount = 0;
            model.PageSize = Config.PageSize;
            model.TotalCount = 0;
            model.SearchUnitOfMeasureList = GetUnitOfMeasureSelectlist();
            model.UnitOfMeasureList = GetUnitOfMeasureSelectlist();
            nonTrafficeChargeDB = new NonTrafficChargeDB(connectionString);

            int _searchUOMIntNo = 0;
            int.TryParse(SearchUOMIntNo, out _searchUOMIntNo);

            string _SearchNTCCode = "";
            if (!string.IsNullOrEmpty(SearchNTCCode))
            {
                _SearchNTCCode = SearchNTCCode.Trim();
            }
            string _SearchNTCDescr = "";
            if (!string.IsNullOrEmpty(SearchNTCDescr))
            {
                _SearchNTCDescr = SearchNTCDescr.Trim();
            }


            DataSet ds = nonTrafficeChargeDB.GetNonTrafficChargeList(AutIntNo, _SearchNTCCode,_SearchNTCDescr, _searchUOMIntNo, Page + 1, model.PageSize, out totalCount);

            model.URLPara = "SearchNTCCode=" + SearchNTCCode + "&SearchUOMIntNo=" + SearchUOMIntNo;

            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            NonTrafficChargeModel item = new NonTrafficChargeModel();
                            item.NTCIntNo = dt.Rows[i]["NTCIntNo"] == null ? 0 : Convert.ToInt32(dt.Rows[i]["NTCIntNo"]);
                            item.AutIntNo = dt.Rows[i]["AutIntNo"] == null ? 0 : Convert.ToInt32(dt.Rows[i]["AutIntNo"]);
                            item.NTCCode = dt.Rows[i]["NTCCode"] == null ? "" : Convert.ToString(dt.Rows[i]["NTCCode"]);
                            item.NTCDescr = dt.Rows[i]["NTCDescr"] == null ? "" : Convert.ToString(dt.Rows[i]["NTCDescr"]);
                            item.NTCComment = dt.Rows[i]["NTCComment"] == null ? "" : Convert.ToString(dt.Rows[i]["NTCComment"]);
                            item.UOMIntNo = dt.Rows[i]["UOMIntNo"] == null ? "" : Convert.ToString(dt.Rows[i]["UOMIntNo"]);
                            item.UOMDescr = dt.Rows[i]["UOMDescr"] == null ? "" : Convert.ToString(dt.Rows[i]["UOMDescr"]);
                            item.NTCStandardAmount = double.Parse(dt.Rows[i]["NTCStandardAmount"].ToString()).ToString("f2");
                            model.NonTrafficChargeList.Add(item);
                        }
                    }
                    
                }
            }

            model.TotalCount = totalCount;
            model.Page = Page;
            if (totalCount == 0)
            {
                model.ErrorMsg = NonTrafficChargeMaintenanceResx.ErrorMsg1;
            }

            return View(model);
        }

        [HttpPost]
        public JsonResult SaveNonTrafficCharge(string NTCIntNo, string NTCCode, string NTCDescr, string NTCComment,
                                               string NTCStandardAmount,string UOMIntNo)
        {
            SIL.AARTO.BLL.Utility.UserLoginInfo userDetails = new BLL.Utility.UserLoginInfo();
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            string login = userDetails.UserName;
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);

            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }

           int ntcIntNo = 0;
            int.TryParse(NTCIntNo, out ntcIntNo);

            if (ntcIntNo == 0)
            {
                SIL.AARTO.DAL.Entities.NonTrafficCharge ntcEntity = ntcService.GetByNtcCode(NTCCode).FirstOrDefault();

                if (ntcEntity != null)
                {
                    return Json(new
                    {
                        result = -4,
                        errorlist = new
                        {
                            errorTip = string.Format(NonTrafficChargeMaintenanceResx.lblErrorMsg4, NTCCode)
                        }
                    });
                }
            }
            else if (ntcIntNo>0)
            {
                ntc = ntcService.GetAll().Where(c=>c.NtcCode==NTCCode&&c.NtcIntNo!=ntcIntNo).FirstOrDefault();

                if (ntc != null)
                {
                    return Json(new
                    {
                        result = -4,
                        errorlist = new
                        {
                            errorTip = string.Format(NonTrafficChargeMaintenanceResx.lblErrorMsg4, NTCCode)
                        }
                    });
                }
            }


            double ntcStandardAmount = 0;
            bool validStandardAmount = double.TryParse(NTCStandardAmount, NumberStyles.Currency, CultureInfo.InvariantCulture, out ntcStandardAmount);

            if (!validStandardAmount)
            {
                return Json(new
                {
                    result = -3,
                    errorlist = new
                    {
                        errorTip = NonTrafficChargeMaintenanceResx.lblErrorMsg3
                    }
                });
            }

            if (ntcStandardAmount <= 0) // || ntcStandardAmount > 10000)
            {
                return Json(new
                {
                    result = -4,
                    errorlist = new
                    {
                        errorTip = NonTrafficChargeMaintenanceResx.lblErrorMsg6
                    }
                });
            }


            NTCDescr = string.IsNullOrEmpty(NTCDescr) ? string.Empty : HttpUtility.UrlDecode(NTCDescr).Trim();
            NTCComment = string.IsNullOrEmpty(NTCComment) ? string.Empty : HttpUtility.UrlDecode(NTCComment).Trim();

            try
            {

                if (ntcIntNo == 0)
                {
                    SIL.AARTO.DAL.Entities.NonTrafficCharge ntcAdd = new SIL.AARTO.DAL.Entities.NonTrafficCharge();
                    ntcAdd.AutIntNo = AutIntNo;
                    ntcAdd.NtcCode = NTCCode.Trim();
                    ntcAdd.NtcDescr = NTCDescr.Trim();
                    ntcAdd.NtcComment = NTCComment.Trim();
                    ntcAdd.NtcDateAdded = DateTime.Now;
                    ntcAdd.NtcStandardAmount =(float)ntcStandardAmount;
                    ntcAdd.UomIntNo = Convert.ToInt32(UOMIntNo);
                    ntcAdd.LastUser = LastUser;
                    ntcAdd = ntcService.Save(ntcAdd);
                }
                else
                {
                    ntc = ntcService.GetByNtcIntNo(ntcIntNo);
                    if (ntc != null)
                    {
                        ntc.NtcCode = NTCCode.Trim();
                        ntc.NtcDescr = NTCDescr.Trim();
                        ntc.NtcComment = NTCComment.Trim();
                        ntc.UomIntNo = Convert.ToInt32(UOMIntNo);
                        ntc.NtcStandardAmount = (float)ntcStandardAmount;
                        ntc = ntcService.Save(ntc);
                    }
                }
              
                return Json(new
                {
                    result = 1,
                    errorlist = new
                    {
                        errorTip = ""
                    }
                });

            }
            catch (Exception e)
            {
                return Json(new
                {
                    result = -1,
                    errorlist = new
                    {
                        errorTip = e.Message
                    }
                });
            }

           

           
        
        }

        [HttpPost]
        public ActionResult GetNonTrafficCharge(int NTCIntNo)
        {
            SIL.AARTO.DAL.Entities.NonTrafficCharge ntc = ntcService.GetByNtcIntNo(NTCIntNo);
            SIL.AARTO.Web.ViewModels.NonTrafficChargeMaintenance.NonTrafficChargeModel entity =
                new NonTrafficChargeModel();

            if (ntc != null)
            {
                entity.NTCIntNo = ntc.NtcIntNo;
                entity.NTCCode = ntc.NtcCode;
                entity.NTCDescr = ntc.NtcDescr;
                entity.NTCComment = ntc.NtcComment;
                entity.NTCStandardAmount = double.Parse(ntc.NtcStandardAmount.ToString()).ToString("f2");
                entity.UOMIntNo = ntc.UomIntNo.ToString();
                
            }

            return Json(entity);
        }

        [HttpPost]
        public ActionResult NonTrafficChargeDelete(int NTCIntNo)
        {
            SIL.AARTO.BLL.Utility.UserLoginInfo userDetails = new BLL.Utility.UserLoginInfo();
            userDetails = (UserLoginInfo)Session["UserLoginInfo"];
            string login = userDetails.UserName;
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);

            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { result = -2 });
            }
            try
            {
                bool Status = true;
                SIL.AARTO.DAL.Entities.NonTrafficCharge ntc = ntcService.GetByNtcIntNo(NTCIntNo);
                if (ntc != null)
                {
                    Status = ntcService.Delete(ntc);
                }

                return Json(new
                {
                    result =1,
                    errorlist = new
                    {
                        errorTip =""
                    }
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    result = -1,
                    errorlist = new
                    {
                        errorTip = NonTrafficChargeMaintenanceResx.lblErrorMsg5
                    }
                });
            }
        }

        public List<SelectListItem> GetAuthoritySelectList()
        {
            var lookups = AuthorityLookupCache.GetCacheLookupValue(this.cultureName);
            var authorityList = anthorityService.GetAll()
                .Where(a => lookups.Keys.Contains(a.AutIntNo))
                .OrderBy(a => a.AutName)
                .Select(a => new SelectListItem
                {
                    Text = lookups[a.AutIntNo],
                    Value = Convert.ToString(a.AutIntNo),
                    Selected = a.AutIntNo == AutIntNo
                }).ToList();

            return authorityList;
        }

        public List<SelectListItem> GetUnitOfMeasureSelectlist()
        {
            List<SelectListItem> itemList = new List<SelectListItem>();

            TList<UnitOfMeasure> uomList=uomService.GetAll();

            if (uomList != null && uomList.Count > 0)
            {
                foreach (var item in uomList)
                {
                    itemList.Add(new SelectListItem { Text = item.UomName, Value = item.UomIntNo.ToString() });
                }
            }

            itemList.Insert(0, new SelectListItem { Text = NonTrafficChargeMaintenanceResx.PleaseSelectMeasureUnit, Value = "" });

            return itemList;

        }

    }
}
