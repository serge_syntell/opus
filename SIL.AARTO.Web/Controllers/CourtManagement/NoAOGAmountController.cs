﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.CourtManagement;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Helpers;
using SIL.AARTO.Web.Resource.CourtManagement;
using SIL.AARTO.Web.ViewModels.CourtManagement;

namespace SIL.AARTO.Web.Controllers.CourtManagement
{
    [AARTOAuthorize, AARTOErrorLog]
    public partial class CourtManagementController : Controller
    {
        private readonly string connStr = System.Configuration.ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
        private static NoAOGAmountSettingManager manager = null;
        // GET: /NoAOGAmount/
        [HttpGet]
        public ActionResult NoAOGAmountSetting()
        {
            NoAOGAmountModel model = new NoAOGAmountModel();

            //if (!String.IsNullOrEmpty(Request.QueryString["Page"]))
            //{
            //    model.PageIndex = Convert.ToInt32(Request.QueryString["Page"]);
            //}
            //if (!string.IsNullOrEmpty(Request.QueryString["CrtIntNo"]))
            //{
            //    model.CrtIntNo = Convert.ToInt32(Request.QueryString["CrtIntNo"]);
            //}
            //if (!string.IsNullOrEmpty(Request.QueryString["CRIntNo"]))
            //{
            //    model.CRIntNo = Convert.ToInt32(Request.QueryString["CRIntNo"]);
            //}
            if (!string.IsNullOrEmpty(Request.QueryString["Number"]))
            {
                model.Number = Request.QueryString["Number"];
            }
            InitModel(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult NoAOGAmountSetting(NoAOGAmountModel model)
        {
            if (!string.IsNullOrEmpty(model.Number))
            {
                model.Number = model.Number.Replace("/", "").Replace("-", "");
            }
            else
            {
                model.Number = "";
            }
            model.Result = manager.GetResultForNoAOGAmountSetting(model.Number);
            model.NoDateFound = (model.Result == null || model.Result.Count == 0);
            if (model.NoDateFound == false)
                model.CrtIntNo = model.Result[0].CrtIntNo;
            InitModel(model);
            return View(model);
        }

        [HttpGet]
        public ActionResult NoAOGAmountSettingReversal()
        {
            NoAOGAmountModel model = new NoAOGAmountModel();
            //if (!String.IsNullOrEmpty(Request.QueryString["Page"]))
            //{
            //    model.PageIndex = Convert.ToInt32(Request.QueryString["Page"]);
            //}
            //if (!string.IsNullOrEmpty(Request.QueryString["CrtIntNo"]))
            //{
            //    model.CrtIntNo = Convert.ToInt32(Request.QueryString["CrtIntNo"]);
            //}
            //if (!string.IsNullOrEmpty(Request.QueryString["CRIntNo"]))
            //{
            //    model.CRIntNo = Convert.ToInt32(Request.QueryString["CRIntNo"]);
            //}
            if (!string.IsNullOrEmpty(Request.QueryString["Number"]))
            {
                model.Number = Request.QueryString["Number"];
            }
            InitModel(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult NoAOGAmountSettingReversal(NoAOGAmountModel model)
        {
            if (!string.IsNullOrEmpty(model.Number))
            {
                model.Number = model.Number.Replace("/", "").Replace("-", "");
            }

            model.Result = manager.GetResultForNoAOGMountReversal(model.Number);
            model.NoDateFound = (model.Result == null || model.Result.Count == 0);
            if (model.NoDateFound == false)
                model.CrtIntNo = model.Result[0].CrtIntNo;
            InitModel(model);
            //model.TotalCount = totalCount;
            return View(model);
        }

        [HttpPost]
        public ActionResult GetNoAOGCharges(int id)
        {
            if (manager == null) manager = new NoAOGAmountSettingManager(this.connStr);

            return Json(manager.GetNoAOGCharges(id));
        }

        public ActionResult GetNoAOGChargesForReverse(int id)
        {
            if (manager == null) manager = new NoAOGAmountSettingManager(this.connStr);

            return Json(manager.GetNoAOGChargesForReverse(id));
        }

        [HttpPost]
        public ActionResult SetAmount(NoAOGChageJsonEntity entity)
        {
            string msg = string.Empty;
            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { Status = false, Timeout = true });
            }

            if (entity.ChargeList == null || entity.ChargeList.Count == 0)
            {
                return Json(new { Status = false });
            }

            UserLoginInfo userInfo = ((UserLoginInfo)Session["UserLoginInfo"]);

            int returnValue = manager.SetAmountForNoAOG(entity, userInfo.UserName);
            switch (returnValue)
            {
                case -1:
                    msg = NoAOGAmountSetting_cshtml.ErrorMessage1;
                    break;
                case -2:
                    msg = NoAOGAmountSetting_cshtml.ErrorMessage2;
                    break;
                case -3:
                    msg = NoAOGAmountSetting_cshtml.ErrorMessage3;
                    break;
                case -4:
                    msg = NoAOGAmountSetting_cshtml.ErrorMessage4;
                    break;
                case -5:
                    msg = NoAOGAmountSetting_cshtml.ErrorMessage5;
                    break;
                case -6:
                    msg = NoAOGAmountSetting_cshtml.ErrorMessage6;
                    break;
                case -7:
                    msg = NoAOGAmountSetting_cshtml.ErrorMessage7;
                    break;
                case 1:
                    msg = NoAOGAmountSetting_cshtml.SuccessfulMessage;
                    break;
            }
            return Json(new { Status = returnValue > 0, Message = msg, Timeout = false });
        }

        [HttpPost]
        public ActionResult ReverseAmount(int id)
        {
            string msg = string.Empty;

            string lastUser = string.Empty;
            if (Session["UserLoginInfo"] == null)
            {
                return Json(new { Status = false, Timeout = true });
            }

            UserLoginInfo userInfo = ((UserLoginInfo)Session["UserLoginInfo"]);

            int returnValue = 0;
            if (manager == null)
                manager = new NoAOGAmountSettingManager(this.connStr);

            returnValue = manager.ReverseAmountForNoAOG(id, userInfo.UserName);
            switch (returnValue)
            {
                case -1:
                    msg = NoAOGAmountSettingReversal_cshtml.ErroeMessage1;
                    break;
                case -2:
                    msg = NoAOGAmountSettingReversal_cshtml.ErroeMessage2;
                    break;
                case -3:
                    msg = NoAOGAmountSettingReversal_cshtml.ErroeMessage3;
                    break;
                case -4:
                    msg = NoAOGAmountSettingReversal_cshtml.ErroeMessage4;
                    break;
                case -5:
                    msg = NoAOGAmountSettingReversal_cshtml.ErroeMessage5;
                    break;
                case -6:
                    msg = NoAOGAmountSettingReversal_cshtml.ErroeMessage6;
                    break;
                case 1:
                    msg = NoAOGAmountSettingReversal_cshtml.SuccessfulMessage;
                    break;
            }


            return Json(new { Status = returnValue > 0, Message = msg, Timeout = false });
        }

        void InitModel(NoAOGAmountModel model)
        {
            //model.PageIndex = 0;
            //if (!String.IsNullOrEmpty(Request.QueryString["PageIndex"]))
            //{
            //    model.PageIndex = Convert.ToInt32(Request.QueryString["PageIndex"]);
            //}

            if (manager == null)
                manager = new NoAOGAmountSettingManager(connStr);

            //List<CourtRoom> courtRooms = CourtRoomCache.GetAvailableCourtRoom(model.CrtIntNo);
            //foreach (CourtRoom r in courtRooms)
            //{
            //    r.CrtRoomName = r.CrtRoomNo + " (" + r.CrtRoomName + ")";
            //}
            //courtRooms.Insert(0, new CourtRoom() { CrtRintNo = 0, CrtRoomName = SIL.AARTO.Web.Resource.CourtManagement.NoAOGAmountSetting_cshtml.msgSelectCourtRoom });
            //model.CourtRooms = new SelectList(courtRooms, "CrtRIntNo", "CrtRoomName", model.CRIntNo);

            //model.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

            model.CourtMagList = new SelectList(manager.GetMagistrate(model.CrtIntNo), "CoMaIntNo", "MagistrateName", model.CoMaIntNo);
        }

    }
}
