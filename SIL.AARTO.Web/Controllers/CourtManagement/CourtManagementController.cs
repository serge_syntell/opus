﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.IO;
using System.Transactions;
using System.Configuration;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.Web.ViewModels.CourtManagement;
using Stalberg.TMS.Data;
using SIL.AARTO.Web.Resource.CourtManagement;

using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.Web.Controllers.CourtManagement
{
    public partial class CourtManagementController : Controller
    {
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }

        public ActionResult CourtMagistrateList(CourtManagementModel model)
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }

            int total = 0;
            model.PageSize = Config.PageSize;
            model.TotalCount = 0;
            model.SearchCourtList = GetCourtList();
            model.CourtList = model.SearchCourtList;

            TList<SIL.AARTO.DAL.Entities.CourtMagistrate> list = new TList<SIL.AARTO.DAL.Entities.CourtMagistrate>();
            if (model.SearchCourt == 0)
            {
                if (model.SearchCourtList.Count() > 1)
                {
                    string[] crtIntNoList = new string[model.SearchCourtList.Count() - 1];
                    for (int i = 1; i < model.SearchCourtList.Count(); i++)
                    {
                        crtIntNoList[i - 1] = model.SearchCourtList.ToList()[i].Value;
                    }
                    CourtMagistrateQuery query = new CourtMagistrateQuery();
                    query.AppendIn(CourtMagistrateColumn.CrtIntNo, crtIntNoList);
                    list = new CourtMagistrateService().Find(query as IFilterParameterCollection, "CrtIntNo", model.Page * model.PageSize, model.PageSize, out total);
                }
            }
            else
            {
                list = new CourtMagistrateService().GetByCrtIntNo(model.SearchCourt, model.Page * model.PageSize, model.PageSize, out total);
            }

            list.OrderBy(m => m.CrtIntNo);

            for (int i = 0; i < list.Count; i++)
            {
                Court court = new CourtService().GetByCrtIntNo(list[i].CrtIntNo);

                SIL.AARTO.Web.ViewModels.CourtManagement.CourtMagistrate CoMa = new ViewModels.CourtManagement.CourtMagistrate();
                CoMa.CoMaIntNo = list[i].CoMaIntNo;
                CoMa.CourtName = court.CrtName;
                CoMa.CourtNo = court.CrtNo;
                CoMa.MagistrateName = list[i].MagistrateName;

                model.CourtMagistrateList.Add(CoMa);
            }

            model.CourtMagistrateList.OrderBy(m => m.CourtName);

            model.TotalCount = total;
            if (total == 0)
            {
                model.ErrorMsg = CourtManage_cshtml.ErrorMsg1;
            }

            return View(model);
        }

        public SelectList GetCourtList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            SelectListItem select = new SelectListItem()
            {
                Text = "",
                Value = ""
            };
            list.Add(select);

            Stalberg.TMS.CourtDB db = new Stalberg.TMS.CourtDB(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());
            IDataReader reader = db.GetAuth_CourtListByAuth(int.Parse(Session["DefaultAuthority"].ToString()));
            while (reader.Read())
            {
                SelectListItem select1 = new SelectListItem()
                {
                    Text = reader["CrtDetails"].ToString(),
                    Value = reader["CrtIntNo"].ToString()
                };
                list.Add(select1);
            }
            reader.Close();

            return new SelectList(list, "Value", "Text");
        }

        [HttpPost]
        public ActionResult GetCourtMagistrate(int CoMaIntNo)
        {
            SIL.AARTO.DAL.Entities.CourtMagistrate coma = new CourtMagistrateService().GetByCoMaIntNo(CoMaIntNo);
            SIL.AARTO.Web.ViewModels.CourtManagement.CourtMagistrate entity = new ViewModels.CourtManagement.CourtMagistrate();

            if (coma != null)
            {
                entity.CoMaIntNo = coma.CoMaIntNo;
                entity.CrtIntNo = coma.CrtIntNo;
                entity.MagistrateName = coma.MagistrateName;
            }

            return Json(entity);
        }

        public ActionResult SaveCourtMagistrate(CourtManagementModel model)
        {
            if (model.CoMaIntNo == 0)
            {
                SIL.AARTO.DAL.Entities.CourtMagistrate coma = new SIL.AARTO.DAL.Entities.CourtMagistrate();
                coma.CrtIntNo = int.Parse(model.CrtIntNo);
                coma.MagistrateName = model.MagistrateName.Trim();
                coma.LastUser = this.LastUser;
                coma = new CourtMagistrateService().Save(coma);
            }
            else
            {
                SIL.AARTO.DAL.Entities.CourtMagistrate coma = new CourtMagistrateService().GetByCoMaIntNo(model.CoMaIntNo);
                if (coma != null)
                {
                    coma.CrtIntNo = int.Parse(model.CrtIntNo);
                    coma.MagistrateName = model.MagistrateName.Trim();
                    coma.LastUser = this.LastUser;
                    coma = new CourtMagistrateService().Save(coma);
                }
            }
            return RedirectToAction("CourtMagistrateList");
        }

        public ActionResult CourtMagistrateDelete(int CoMaIntNo)
        {
            bool Status = true;
            SIL.AARTO.DAL.Entities.CourtMagistrate coma = new CourtMagistrateService().GetByCoMaIntNo(CoMaIntNo);
            if (coma != null)
            {
                new CourtMagistrateService().Delete(coma);
            }

            return Json(new { Status = Status});
        }
    }
}
