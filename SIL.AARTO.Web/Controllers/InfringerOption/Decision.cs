using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Collections;
using System.Text;

using SIL.AARTO.BLL.InfringerOption.Model;
using SIL.AARTO.BLL.InfringerOption;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using System.Configuration;
using SIL.AARTO.BLL.Utility.Printing;
namespace SIL.AARTO.Web.Controllers.InfringerOption
{
    [AARTOErrorLog, LanguageFilter]
    public partial class InfringerOptionController : Controller
    {
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }
        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

        [AcceptVerbs(HttpVerbs.Get)]
        //[Authorize]
        public ActionResult Decision()
        {
            int docTypeID = -1;
            int repID = DecisionManager.GetTopOneRepresentationForDecideWithLocked(out docTypeID);
            return Direct(repID, docTypeID, null);
        }
        private ActionResult Direct(int repID, int docTypeID, int? a14RepID)
        {
            if (repID != -1)
            {
                string directUrl = "";
                //return Redirect("~/InfringerOption/AARTO08Decision/" + repID);
                switch (docTypeID)
                {
                    case (int)AartoDocumentTypeList.AARTO04:
                        directUrl = "~/InfringerOption/AARTO04Decision";
                        break;
                    case (int)AartoDocumentTypeList.AARTO07:
                        directUrl = "~/InfringerOption/AARTO07Decision";
                        break;
                    case (int)AartoDocumentTypeList.AARTO08:
                        directUrl = "~/InfringerOption/AARTO08Decision";
                        break;
                    case -1:
                        break;
                }
                if (repID != 0)
                    directUrl += "?id=" + repID;
                if (a14RepID != null) directUrl += "&a14RepID=" + a14RepID;
                //if (a14RepID != null) directUrl += "/" + repID + "/" + a14RepID;
                if (directUrl != "") return Redirect(directUrl);
            }
            return View();
        }

        #region AARTO04
        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult AARTO04Decision()
        {
            int id = -1, a14RepID = -1;
            if (Request.QueryString["id"] != null) id = Convert.ToInt32(Request.QueryString["id"]);
            if (Request.QueryString["a14RepID"] != null) id = Convert.ToInt32(Request.QueryString["id"]);
            if (id == -1) return Redirect("~/InfringerOption/Decision");
            DecisionModel model = DecisionManager.GetDecideModelByRepID(id, (int)AartoDocumentTypeList.AARTO04);
            model.A14RepID = a14RepID;
            FillDataIntoSelectList(model);
            FillTypeOfAccountList(model);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult AARTO04Decision(DecisionModel model, FormCollection collection)
        {
            if (collection["btnDecideSuccessful"] != null)
            {
                model.DecideResult = (int)RepresentationCodeList.AARTO_RepresentationSuccessful;
                return DoDecide(model);
            }
            else if (collection["btnDecideUnSuccessful"] != null)
            {
                model.DecideResult = (int)RepresentationCodeList.AARTO_RepresentationUnsuccessful;
                return DoDecide(model);
            }
            else
                return null;
        }
        #endregion

        #region AARTO07
        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult AARTO07Decision()
        {
            int id = -1, a14RepID = -1;
            if (Request.QueryString["id"] != null) id = Convert.ToInt32(Request.QueryString["id"]);
            if (Request.QueryString["a14RepID"] != null) id = Convert.ToInt32(Request.QueryString["id"]);
            if (id == -1) return Redirect("~/InfringerOption/Decision");
            DecisionModel model = DecisionManager.GetDecideModelByRepID(id, (int)AartoDocumentTypeList.AARTO07);
            model.A14RepID = a14RepID;
            FillDataIntoSelectList(model);
            return View(model);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        //[Authorize]
        public ActionResult AARTO07Decision(DecisionModel model, FormCollection collection)
        {
            if (collection["btnDecideSuccessful"] != null)
            {
                model.DecideResult = (int)RepresentationCodeList.AARTO_RepresentationSuccessful;
                return DoDecide(model);
            }
            else if (collection["btnDecideUnSuccessful"] != null)
            {
                model.DecideResult = (int)RepresentationCodeList.AARTO_RepresentationUnsuccessful;
                return DoDecide(model);
            }
            else
                return null;
        }
        #endregion

        #region AARTO08
        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult AARTO08Decision()
        {
            int id = -1, a14RepID = -1;
            if (Request.QueryString["id"] != null) id = Convert.ToInt32(Request.QueryString["id"]);
            if (Request.QueryString["a14RepID"] != null) a14RepID = Convert.ToInt32(Request.QueryString["a14RepID"]);
            if (id == -1) return Redirect("~/InfringerOption/Decision");
            DecisionModel model = DecisionManager.GetDecideModelByRepID(id, (int)AartoDocumentTypeList.AARTO08);
            model.A14RepID = a14RepID;
            FillDataIntoSelectList(model);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult AARTO08Decision(DecisionModel model, FormCollection collection)
        {
            /*if adjudicated unsucessfully AaRepUnsuccessfulReason should be needed.*/
            if (collection["btnDecideSuccessful"] != null)
            {
                model.DecideResult = (int)RepresentationCodeList.AARTO_RepresentationSuccessful;
                return DoDecide(model);
            }
            else if (collection["btnDecideUnSuccessful"] != null)
            {
                model.DecideResult = (int)RepresentationCodeList.AARTO_RepresentationUnsuccessful;
                return DoDecide(model);
            }
            else if (collection["btnDecideAdviseElectCourt"] != null)
            {
                model.DecideResult = (int)RepresentationCodeList.AARTO_RepresentationAdviseElectCourt;
                return DoDecide(model);
            }
            else
                return null;
        }
        #endregion

        #region AARTO14
        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult AARTO14Decision()
        {
            int id = -1;
            if (Request.QueryString["id"] != null) id = Convert.ToInt32(Request.QueryString["id"]);
            if (id == -1) id = DecisionManager.GetTopOneRevocationOfEnforcementOrderForDecideWithLocked();
            DecisionModel model = new DecisionModel();
            if (id != -1)
            {
                model = DecisionManager.GetDecideModelByRepID((int)id, (int)AartoDocumentTypeList.AARTO14);

                #region RVReasons
                model.RVReasons = new List<bool>(8);
                model.RVReasons.Add(model.Rep.AaEnfOrderRevReason.ToLower().IndexOf(RVOfEOReasonsType.HavePaid.ToLower()) != -1);
                model.RVReasons.Add(model.Rep.AaEnfOrderRevReason.ToLower().IndexOf(RVOfEOReasonsType.HaveSubmittedA04.ToLower()) != -1);
                model.RVReasons.Add(model.Rep.AaEnfOrderRevReason.ToLower().IndexOf(RVOfEOReasonsType.HaveSubmittedA08.ToLower()) != -1);
                model.RVReasons.Add(model.Rep.AaEnfOrderRevReason.ToLower().IndexOf(RVOfEOReasonsType.HaveSubmittedA07.ToLower()) != -1);
                model.RVReasons.Add(model.Rep.AaEnfOrderRevReason.ToLower().IndexOf(RVOfEOReasonsType.HaveSubmittedA10.ToLower()) != -1);
                model.RVReasons.Add(model.Rep.AaEnfOrderRevReason.ToLower().IndexOf(RVOfEOReasonsType.HaveAppearedInCourt.ToLower()) != -1);
                model.RVReasons.Add(model.Rep.AaEnfOrderRevReason.ToLower().IndexOf(RVOfEOReasonsType.CannotAppearedInCourt.ToLower()) != -1);
                model.RVReasons.Add(model.Rep.AaEnfOrderRevReason.ToLower().IndexOf(RVOfEOReasonsType.OtherReasons.ToLower()) != -1);
                model.EvidencePack = GetEvidencePackContentByList(DecisionManager.GetEvidencePackListByChgIntNo(model.ChgIntNo));
                #endregion
                if (model.AaClockTypeID == (int)AartoClockTypeList.EnforcementOrder)
                {
                    //get other unAdjudicated rep
                    int docTypeID = -1;
                    int repID = DecisionManager.GetTopOneRepresentationForRevocationOfEnforcementOrder(model.ChgIntNo, out docTypeID);
                    if (repID != -1)
                        return Direct(repID, docTypeID, id);
                    else
                    {
                        //adjudicate two options
                    }
                }
                else
                {

                }
                FillDataIntoSelectList(model);
                FillCourtList(model);
                return View(model);
            }
            else
            {
                model.RepID = id;
                model.DocumentImageUrlList = "";
                return View(model);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult AARTO14Decision(DecisionModel model, FormCollection collection)
        {
            if (collection["btnDecideSuccessful"] != null)
            {
                model.DecideResult = (int)RepresentationCodeList.AARTO_RepresentationSuccessful;
                #region RVReasons
                model.RVReasons = new List<bool>(8);
                model.RVReasons.Add((collection["chkReasonsOfA"].ToString().Replace("false", "") != ""));
                model.RVReasons.Add((collection["chkReasonsOfB"].ToString().Replace("false", "") != ""));
                model.RVReasons.Add((collection["chkReasonsOfC"].ToString().Replace("false", "") != ""));
                model.RVReasons.Add((collection["chkReasonsOfD"].ToString().Replace("false", "") != ""));
                model.RVReasons.Add((collection["chkReasonsOfE"].ToString().Replace("false", "") != ""));
                model.RVReasons.Add((collection["chkReasonsOfF"].ToString().Replace("false", "") != ""));
                model.RVReasons.Add((collection["chkReasonsOfG"].ToString().Replace("false", "") != ""));
                model.RVReasons.Add((collection["chkReasonsOfH"].ToString().Replace("false", "") != ""));
                #endregion
                return DoDecide(model);
            }
            else if (collection["btnDecideUnSuccessful"] != null)
            {
                model.DecideResult = (int)RepresentationCodeList.AARTO_RepresentationUnsuccessful;
                return DoDecide(model);
            }
            else
                return null;
        }
        #endregion
        private ActionResult DoDecide(DecisionModel model)
        {
            model.LastUser = SIL.AARTO.BLL.Utility.Session.UserName;
            if (DecisionManager.DecideRepresentation(model))
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(AutIntNo, LastUser, PunchStatisticsTranTypeList.Decision, PunchAction.Add);  

                switch (model.AaDocTypeID)
                {
                    case (int)AartoDocumentTypeList.AARTO04:
                    case (int)AartoDocumentTypeList.AARTO07:
                    case (int)AartoDocumentTypeList.AARTO08:
                        if (model.A14RepID != -1)
                            return Redirect("~/InfringerOption/AARTO14Decision/?id=" + model.A14RepID);
                        else
                            return Redirect("~/InfringerOption/Decision");
                    case (int)AartoDocumentTypeList.AARTO14:
                        return Redirect("~/InfringerOption/AARTO14Decision");
                    default:
                        return null;
                }
            }
            else
            {
                return null;
            }
        }
        #region fill data
        private void FillTypeOfAccountList(DecisionModel model)
        {
            List<SelectListItem> typeList = new List<SelectListItem>();
            SelectListItem listItem;
            listItem = new SelectListItem();
            listItem.Text = "Cheque";
            listItem.Value = "C";
            typeList.Add(listItem);
            listItem = new SelectListItem();
            listItem.Text = "Savings";
            listItem.Value = "S";
            typeList.Add(listItem);
            listItem = new SelectListItem();
            listItem.Text = "Transmission";
            listItem.Value = "T";
            typeList.Add(listItem);
            model.TypeOfAccountList = new SelectList(typeList as IEnumerable, "Value", "Text", model.Rep.AaRepMethodOfPayment);
        }
        private void FillDataIntoSelectList(DecisionModel model)
        {
            model.OrganisationTypeList = new SelectList(DecisionManager.GetOrganisationTypeList() as IEnumerable,
                "AaOrTypeID", "AaOrTypeName", model.Rep.AaRepOrgTypeId);

            model.IDTypeList = new SelectList(DecisionManager.GetIDTypeList() as IEnumerable,
                "AaIDTypeID", "AaIDTypeName", model.Rep.AaRepInfIdTypeId);

            model.LicenceCodeList = new SelectList(DecisionManager.GetLicenceCodeList() as IEnumerable,
                "AaLiCodeID", "AaLiCodeName", model.Rep.AaRepInfLiCodeId);

            model.LearnersCodeList = new SelectList(DecisionManager.GetLearnerCodeList() as IEnumerable,
                "AaLeCodeID", "AaLeCodeName", model.Rep.AaRepInfLeCodeId);

            model.PrDPCodeList = new SelectList(DecisionManager.GetPrDpCodeList() as IEnumerable,
                "AaPrDPCodeID", "AaPrDPCodeName", model.Rep.AaRepInfPrDpCodeId);

            model.VehicleMakeList = new SelectList(DecisionManager.GetVehicleMakeList() as IEnumerable,
                "VMIntNo", "VMDescr", model.Rep.VmIntNo);

            model.VehicleTypeList = new SelectList(DecisionManager.GetVehicleTypeList() as IEnumerable,
                "VTIntNo", "VTDescr", model.Rep.VtIntNo);

            model.VehicleColourList = new SelectList(DecisionManager.GetVehicleColourList() as IEnumerable,
                "VCIntNo", "VCDescr", model.Rep.VcIntNo);

        }

        private void FillCourtList(DecisionModel model)
        {
            model.CourtList = new SelectList(DecisionManager.GetCourtList() as IEnumerable,
                "CrtIntNo", "CrtName", model.Rep.CrtIntNo);
        }
        private string GetEvidencePackContentByList(TList<EvidencePack> list)
        {
            StringBuilder sb = new StringBuilder();
            foreach (EvidencePack vp in list)
            {
                sb.Append("[" + vp.EpItemDate);
                sb.Append("]    " + vp.EpItemDescr);
                sb.Append("\r\n");
            }
            return sb.ToString();
        }
        #endregion
    }
}
