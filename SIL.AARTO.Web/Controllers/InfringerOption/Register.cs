using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using SIL.AARTO.BLL.Utility.DMS;
using SIL.AARTO.BLL.InfringerOption;
using SIL.AARTO.BLL.InfringerOption.Model;
using System.Net;
using System.Text;
using System.IO;
namespace SIL.AARTO.Web.Controllers.InfringerOption
{
    public partial class InfringerOptionController : Controller
    {


        //
        // GET: /Register/
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Register()
        {

            RegisterModel registerModel = new RegisterModel();
            registerModel = RegisterManager.GetRegisterModel();
            return View(registerModel);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Register(RegisterModel model)
        {
            return View();
        }

    }
}
