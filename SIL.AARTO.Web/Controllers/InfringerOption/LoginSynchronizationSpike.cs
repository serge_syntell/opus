using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using SIL.AARTO.BLL.InfringerOption.Model;
using System.Text;

namespace SIL.AARTO.Web.Controllers.InfringerOption
{
    public partial class InfringerOptionController : Controller
    {
        //
        // GET: /LoginSynchronizationSpike/
        //[AcceptVerbs(HttpVerbs.Get)]
        //[Authorize]
        public ActionResult ReadCookie()
        {
            return View();
        }
        public ActionResult OpenCookie()
        {
            OpenCookieModel openCookieModel = new OpenCookieModel();
            SetSharedCookie();
            HttpCookie sharedCookie = System.Web.HttpContext.Current.Request.Cookies["LoginSynchronizationSpike"];
            if (sharedCookie == null)
            {
                openCookieModel.CookieScriptString = "";
            }
            else
            {
                openCookieModel.CookieScriptString = "document.write(" + sharedCookie.Value.ToString() + ")";
            }
            //Response.Write(sharedCookie == null ? "" : "document.write(" + sharedCookie.Value.ToString() + ")");

            //Response.Write(" <script src=\"Representation/SetSharedCookie\"></script>");


            return View(openCookieModel);
        }
        public ActionResult GetCrossDomainCookieInfo()
        {
            System.Net.HttpWebRequest req;
            System.Net.HttpWebResponse res;
            req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://localhost:49586/Representation/OpenCookie");
            res = (System.Net.HttpWebResponse)req.GetResponse();
            System.IO.StreamReader strm = new System.IO.StreamReader(res.GetResponseStream(), Encoding.GetEncoding("utf-8"));
            string docment = strm.ReadToEnd();
            res.Close();
            strm.Close();
            return View();
           // return docment;
        }
        private void SetSharedCookie()
        {
            HttpCookie sharedCookie = System.Web.HttpContext.Current.Request.Cookies["LoginSynchronizationSpike"];
            //if (sharedCookie == null)
            // {
            sharedCookie = new HttpCookie("LoginSynchronizationSpike");
            System.Web.HttpContext.Current.Response.AppendCookie(sharedCookie);
            //When a user logins AARTO website(domain a) successfully,his userID will be added to the above-mentioned cookie to be shared by domain b application later.
            // I will set the value of userID as 1 by means of hardcoding to go on with my spike at the moment.
            sharedCookie.Values.Add("userID", "1");
            sharedCookie.Expires = DateTime.Now.AddMonths(1);
            System.Web.HttpContext.Current.Response.SetCookie(sharedCookie);


            //}
        }
    }
}
