﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.GeneralLetters;
using SIL.AARTO.BLL.Utility;
using SIL.ServiceBase;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.Web.Controllers.GeneralLetters
{
    [AARTOErrorLog]
    public class GeneralLettersController : Controller
    {
        string LastUser
        {
            get { return Session["UserLoginInfo"] != null ? (this.Session["UserLoginInfo"] as UserLoginInfo).UserName : null; }
        }
        public int AutIntNo
        {
            get { return Session["autIntNo"] != null ? Convert.ToInt32(Session["autIntNo"]) : Session["UserLoginInfo"] != null ? ((UserLoginInfo)Session["UserLoginInfo"]).AuthIntNo : 0; }
        }

        public static string connectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

        //
        // GET: /GeneralLetter/
        public ActionResult PrintGeneralLetters(string LetterTemplate = "", string TicketSequenceNo = "", string OffenderName = "", string OffenderAddress = "", string ErrorMsg = "")
        {
            if (Session["UserLoginInfo"] == null)
            {
                return RedirectToAction("login", "account");
            }
            GeneralLetterEntity model = new GeneralLetterEntity();
            GeneralLetterManager manager = new GeneralLetterManager();
            try
            {                
                model.ErrorMsg = ErrorMsg;
                model.LetterTemplates = manager.GetGeneralLettersTemplateList();
                int autIntNo = Convert.ToInt32(Session["autIntNo"]);
                model.AutIntNo = autIntNo;
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.LastUser, PunchStatisticsTranTypeList.PrintGeneralLetters, PunchAction.Other); 

            }
            catch (Exception ex)
            {
                model.ErrorMsg = ex.Message;
            }
            return View(model);
        }

        [HttpPost]
        public JsonResult GetOffender(string ticketSequenceNo)
        {
            GeneralLetterManager manager = new GeneralLetterManager();
            GeneralLetterEntity model = new GeneralLetterEntity() ;
            try
            {
                model = manager.GetOffenderDetail(ticketSequenceNo);
            }
            catch (Exception ex)
            {
                model.ErrorMsg = ex.Message;
            }
            return Json(model);
        }

        public ActionResult Print(GeneralLetterEntity model)
        {
            try
            {

                string tempFile = HttpContext.Request.PhysicalApplicationPath + @"Reports\GeneralLetter_" + model.LetterTemplate + @".dplx";
                string DocumentTypeId = "GeneralLetters";
                GeneralLetterBase generalLetterBase = GeneralLetterFactory<GeneralLetterManager>.GetDAL(DocumentTypeId);
                byte[] buffer = generalLetterBase.SetPDFBody(model, tempFile);// File(report.Draw(), "application/pdf");
                generalLetterBase.AddPrintGeneralLettersHistory(model, LastUser);
                return File(buffer, "application/pdf");
            }
            catch (Exception ex)
            {
                return File(ex.Message, "text/html");
            }
        }
    }
}
