<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.Admin.Model.UserRoleModel>" %>

<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<%@ Import Namespace="SIL.AARTO.BLL.Utility" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% using( Html.BeginForm("UserRoleManage","Admin",FormMethod.Post,new {id="formUserRoleManage"} ))%>
    <%{ %>
    <h2>
        </h2>
    <p>
       </p>
 <table align=center cellpadding="1px" cellspacing="1px" border="0px">
            <tr>
                <td align=center colspan=5 height=77 class="ContentHead">
                   <% =Html.Resource("lblPageTitle.Text") %> 
                </td>


            </tr>
                        <tr>
                <td align=center colspan=5 height=77  class="userNamestyle"><% =Html.Resource("lblUserName.Text") %>
                 <% =Html.Encode(Model.UserName) %>
                </td>


            </tr>
        <tr>
            <td><span class="userNamestyle"><% =Html.Resource("lblCurrentUserRole.Text")%><br /><br /></span>
                <% =Html.DropDownList("UserRoles", Model.UserRoleList, new { id = "ddlUserRoles", multiple = "mutiple",@class="roleManageDropdownlist" })%>
            </td>
            <td>
                <input type="button" id="btnAddRole" value='<% =Html.Resource("btnAddRole.Value") %>'   class="NormalButton"/><input type="button" id="btnRemoveRole" value=">>"   class="NormalButton"/>
            </td>
            <td>
                <span class="userNamestyle"><% =Html.Resource("lblAvailableUserRole.Text")%><br /><br /></span>
                <% =Html.DropDownList("Roles", Model.NotUseRoleList, new { id = "ddlRoles", multiple = "mutiple",@class="roleManageDropdownlist" })%>
            </td>
            <td>
                <input type="button" id="btnPreMenu" value="<% =Html.Resource("btnPreMenu.Value") %>"   class="NormalButton"/>
            </td>
            <td>
                <ul id="ul_tree_view">
                    <% List<int> userRoles = new List<int>(); %>
                    <% SIL.AARTO.BLL.Utility.UserMenu.UserMenuEntityCollection collection = ViewData["MenuCollection"] as SIL.AARTO.BLL.Utility.UserMenu.UserMenuEntityCollection;%>
                    <% if (collection != null) %>
                    <%{ %>
                    <% = Html.CreatePreMenuForUser(collection, new object[] { "cc" })%>
                    <%} %>
                </ul>
            </td>
        </tr>
        <tr><td colspan="5" align="center"><input type="button" id="btnClose" onclick="window.close()"  class="NormalButton" value="<% =Html.Resource("btnColse.Value")%>" /></td></tr>
    </table>
    <input type="hidden" id="hidUserIntNo" value="<% =Model.UserIntNo %>" />
    <input type="hidden" id="hidUrl" value="<% =Request.RawUrl %>" />
    <%} %>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#ul_tree_view").treeview({});

        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
<link href='<% =Url.Content("~/Content/Css/UserMenu/UserMenu.css" )%>' rel="stylesheet" type="text/css" />
    <script src='<% =Url.Content("~/Scripts/Library/jquery.treeview.js")%>' type="text/javascript"></script>
    <link href='<% =Url.Content("~/Content/Css/ST_Odyssey.css")%>' rel="stylesheet" type="text/css" />
    <script src='<% =Url.Content("~/Scripts/Admin/UserManage.js")%>' type="text/javascript"></script>

</asp:Content>
