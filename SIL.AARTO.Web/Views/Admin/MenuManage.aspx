<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.Admin.Model.MenuModel>" %>


<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
    <div>
        <table cellpadding="1" cellspacing="1" border="0" align=center>
            <tr>
                <td align="center" colspan="3" height="77" class="ContentHead">
                   <%= Html.Resource("lblPageTitle.Text")%>
                </td>


            </tr>
            <tr>
                <td align="left"  valign="top">
                    <% 
                        SIL.AARTO.BLL.Utility.UserLoginInfo userLoginInfo;
                        if (Session["UserLoginInfo"] != null)
                        {
                            userLoginInfo = (SIL.AARTO.BLL.Utility.UserLoginInfo)Session["UserLoginInfo"];
                            ViewData["lastUser"] = userLoginInfo.UserName;
                            //Test
                            //userLoginInfo.UserRoles.Add(1);

                            string correntLsCode = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
                    %>

                    <ul id="ul_tree_view" style=" height:350px;">
                        <% =Html.CreateFullMenuForAdmin(new SIL.AARTO.BLL.Utility.UserMenuManager().GetUserMenuCollectionUserRolesFromDB(userLoginInfo.UserRoles, correntLsCode, 4), new object[] { "ss" })%>
                        <% }
                        %>
                    </ul>
                    
                    
                </td>
               
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
               <td align="center"> 
               
                    <table>
                      <tr>
                         <td  class="NormalBold"><%= Html.Resource("lblSelectMenu.Text")%></td>
                         <td><textarea class="inputWidthOne" type="text" size=2 id="txtCurrentMenu"></textarea></td>
                      </tr>
                       <tr>
                         <td  class="NormalBold"></td>
                         <td id="divButton">
                          <input type="button" id="btnAddMenu" class="NormalButton" value="<%= Html.Resource("btnMenuAdd.Value")%>" />
                          <input type="button" id="btnEdit" class="NormalButton" value="<%= Html.Resource("btnMenuEdit.Value")%>" />
                          <input type="button" id="btnDelete" class="NormalButton" value="<%= Html.Resource("btnMenuDelete.Value")%>" />
                         </td>
                      </tr>
                    </table>
                    
                    <table id="divAddMenu" style="display: none;">
                       <tr>
                          <td  class="NormalBold"><%= Html.Resource("lblMenuAddName.Text")%>   </td>
                          <td><textarea class="inputWidthOne" type="text" size=2 id="txtMenuItem"></textarea></td>
                       </tr>
                       <tr>
                       <td class="NormalBold" colspan="2">
                               <table cellspacing="1" cellpadding="0" border="0" width="360" align="center" bgcolor="#000000">
                                      <tr bgcolor='#FFFFFF'>
                                        <td  class="NormalBold"><%=Html.Resource("lblTranslaction.Text")%></td>
                                        <td> <div id="divMenulistTran"></div></td>
                                      </tr>
                               </table>
                            </td> 
                       </tr>
                       <tr>
                          <td  class="NormalBold"><%= Html.Resource("lblMenuAddDesctiption.Text")%>  </td>
                          <td><textarea class="inputWidthOne" type="text" size=2 id="txtMenuDesc"></textarea></td>
                       </tr>
                       <tr>
                       <td class="NormalBold" colspan="2">
                               <table cellspacing="1" cellpadding="0" border="0" width="360" align="center" bgcolor="#000000">
                                      <tr bgcolor='#FFFFFF'>
                                        <td  class="NormalBold"><%=Html.Resource("lblTranslaction.Text")%></td>
                                        <td> <div id="divMenulistDescTran"></div></td>
                                      </tr>
                               </table>
                            </td> 
                       </tr>
                        <tr>
                          <td  class="NormalBold"><%= Html.Resource("lblMenuAddOrderNo.Text")%> </td>
                          <td><input class="inputWidthOne" type="text" id="txtOrderNo" /></td>
                       </tr>
                       
                       <tr>
                          <td  class="NormalBold"></td>
                          <td> <input type="button" id="btnAddBaseMenu" class="NormalButton" value='<%= Html.Resource("btnAddBaseMenu.Value")%>' />
                        <input type="button" id="btnAddSave" class="NormalButton"  value='<%= Html.Resource("btnAddSave.Value")%>' /></td>
                       </tr>
                    </table>
                
                  <table id="divEditMenu" style="display: none;">
                     <tr>
                          <td  class="NormalBold"><%= Html.Resource("lblMenuEditName.Text")%>  </td>
                          <td> <input class="inputWidthOne" type="text" id="txtEditMenuItem" /></td>
                       </tr>
                       <tr>
                       <td class="NormalBold" colspan="2">
                               <table cellspacing="1" cellpadding="0" border="0" width="360" align="center" bgcolor="#000000">
                                      <tr bgcolor='#FFFFFF'>
                                        <td  class="NormalBold"><%=Html.Resource("lblTranslaction.Text")%></td>
                                        <td> <div id="divMenulistTranEdit"></div></td>
                                      </tr>
                               </table>
                            </td> 
                       </tr>
                                            <tr>
                          <td  class="NormalBold"><%= Html.Resource("lblMenuEditDesctiption.Text")%> </td>
                          <td> <input class="inputWidthOne" type="text" id="txtEditMenuDesc" /></td>
                       </tr>
                       <tr>
                       <td class="NormalBold" colspan="2">
                               <table cellspacing="1" cellpadding="0" border="0" width="360" align="center" bgcolor="#000000">
                                      <tr bgcolor='#FFFFFF'>
                                        <td  class="NormalBold"><%=Html.Resource("lblTranslaction.Text")%></td>
                                        <td> <div id="divMenulistDescTranEdit"></div></td>
                                      </tr>
                               </table>
                            </td> 
                       </tr>
                                            <tr>
                          <td  class="NormalBold"><%= Html.Resource("lblMenuEditOrderNo.Text")%></td>
                          <td><input class="inputWidthOne" type="text" id="txtEditOrderNo" /></td>
                       </tr>
                       
                       <tr>
                       <td  class="NormalBold">
                       </td>
                       <td> <input type="button" id="btnEditSave" class="NormalButton" value='<%= Html.Resource("btnEditSave.Value")%>'  /></td>
                       </tr>
                  </table>

                        </td>
            </tr>
        </table>
        <br />
        <br />
        <br />
    </div>
    <input type="hidden" id="txtUrl" />
    <% =Html.JavascriptTag("var lastUser = '" + ViewData["lastUser"] + "';")%>
    </form>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    
    <% =Html.JavascriptTag("var language = '" + ViewData["language"] + "';")%>
    <% =Html.JavascriptTag("var JsMenuNameIsRequired = '" + Html.Resource("JsMenuNameIsRequired")  +"';" )%>
    <% =Html.JavascriptTag("var JsMenuOrderNoIsRequired = '" + Html.Resource("JsMenuOrderNoIsRequired") +"';" ) %>
    <% =Html.JavascriptTag("var JsSelectANode = '" + Html.Resource("JsSelectANode") +"';" ) %>
    <% =Html.JavascriptTag("var JsSelectNodeHaveChidens = '" + Html.Resource("JsSelectNodeHaveChidens") +"';" ) %>
    <% =Html.JavascriptTag("var JsConfirmDeleteNode = '" + Html.Resource("JsConfirmDeleteNode") + "';")%>
    <% =Html.JavascriptTag("var JsInputANumber = '" + Html.Resource("JsInputANumber") + "';")%>
    
    
    <link href='<% =Url.Content("~/Content/Css/UserMenu/UserMenu.css")%>' rel="stylesheet" type="text/css" />
    <%--<script src="../../Scripts/Library/jquery-1.4.2.min.js" type="text/javascript"></script>--%>

    <script src='<% =Url.Content("~/Scripts/Library/jquery.treeview.js")%>' type="text/javascript"></script>

    <link href='<% =Url.Content("~/Content/Css/ST_Odyssey.css")%>' rel="stylesheet" type="text/css" />
    <script src='<% =Url.Content("~/Scripts/Admin/MenuManage.js")%>' type="text/javascript"></script>
    

</asp:Content>
