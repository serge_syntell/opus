<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.Admin.Model.MenuModel>" %>

<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td class="ContentHead" colspan="3" align="center">
                    <% =Html.Resource("lblPageTitle.Text") %>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 50px;">
                    <% =Html.Resource("lblMenu.Text") %>
                </td>
                <td>
                </td>
                <td>
                    <% =Html.Resource("lblPageList.Text") %>
                </td>
            </tr>
            <tr>
                <td>
                    <% 
                        SIL.AARTO.BLL.Utility.UserLoginInfo userLoginInfo;
                        if (Session["UserLoginInfo"] != null)
                        {
                            userLoginInfo = (SIL.AARTO.BLL.Utility.UserLoginInfo)Session["UserLoginInfo"];

                            //Test
                            //userLoginInfo.UserRoles.Add(1);

                            string correntLsCode = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
                    %>
                    <ul id="ul_tree_view">
                        <% =Html.CreateFullMenuForAdmin(new SIL.AARTO.BLL.Utility.UserMenuManager().GetUserMenuCollectionUserRolesFromDB(userLoginInfo.UserRoles, correntLsCode, 4), new object[] { "ss" })%>
                        <% }
                        %>
                    </ul>
                </td>
                <td valign="middle">
                    <table>
                        <tr>
                            <td class="NormalBold">
                                <% =Html.Resource("lblCurrentMenu.Text") %>
                            </td>
                            <td>
                                <textarea class="inputWidthOne" type="text" size="2" id="txtCurrentMenu" readonly="readonly"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="NormalBold">
                                <% =Html.Resource("lblPageUrl.Text") %>
                            </td>
                            <td>
                                <textarea class="inputWidthOne" type="text" size="2" id="txtUrl"  readonly="readonly"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="NormalBold">
                                <% =Html.Resource("lblPageName.Text") %>
                            </td>
                            <td>
                                <textarea class="inputWidthOne" type="text" size="2" id="txtPageName" readonly="readonly"></textarea>
                            </td>
                        </tr>
                                                
                         <tr>
                            <td class="NormalBold" colspan="2">
                               <table cellspacing="1" cellpadding="0" border="0" width="360" align="center" bgcolor="#000000">
                                      <tr bgcolor='#FFFFFF'>
                                      <td><%=Html.Resource("lblTranslaction.Text")%></td>
                                      <td><div id="divTranName"></div></td>
                                      </tr>
                               </table>
                            </td>                           
                        </tr>
                        
                        <tr>
                            <td class="NormalBold">
                                <% =Html.Resource("lblPageDescription.Text") %>
                            </td>
                            <td>
                                <textarea class="inputWidthOne" type="text" size="2" id="txtPageDesc" readonly="readonly"></textarea>
                            </td>
                        </tr>

                         <tr>
                            <td class="NormalBold" colspan="2">
                                <table cellspacing="1" cellpadding="0" border="0" width="360" align="center" bgcolor="#000000">
                                      <tr bgcolor='#FFFFFF'>
                                      <td><%=Html.Resource("lblTranslaction.Text")%></td>
                                      <td><div id="divTranDesc"></div></td>
                                      </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                            </td>
                            <td>
                                <input type="button" value='<% =Html.Resource("lblPageAdd.Text") %>' id="btnAddUrl"
                                    class="NormalButton" />
                                <input type="button" id="btnRemoveUrl" value='<% =Html.Resource("lblPageReomve.Text") %>'
                                    class="NormalButton" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="middle">
                    <% =Html.DropDownList("MenuPageUrl", Model.PageList, new { multiple = "multiple", id = "lbPageList", @class = "pageList" })%>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <% =Html.JavascriptTag("var language = '" + ViewData["language"] + "';")%>
    <%-- <% =Html.JavascriptTag("var JsSelectNodeToDelete = '" + Html.Resource("JsSelectNodeToDelete") + "';")%>--%>
    <% =Html.JavascriptTag("var JsSelectAPage = '" + Html.Resource("JsSelectAPage") + "';")%>
    <% =Html.JavascriptTag("var JsAssignUrlUnSucessful = '" + Html.Resource("JsAssignUrlUnSucessful") + "';")%>
    <% =Html.JavascriptTag("var JsSelectAMenu = '" + Html.Resource("JsSelectAMenu") + "';")%>
    <% =Html.JavascriptTag("var JsNoPageRemove = '" + Html.Resource("JsNoPageRemove") + "';")%>
    <% =Html.JavascriptTag("var JsRemovePageUnSucessful = '" + Html.Resource("JsRemovePageUnSucessful") + "';")%>
    <%-- <% =Html.JavascriptTag("var JsConformDeleteNode = '" + Html.Resource("JsConformDeleteNode") + "';")%>--%>
    <script src='<% =Url.Content("~/Scripts/Library/jquery-1.3.2.min.js") %>' type="text/javascript"></script>
    <link href='<% =Url.Content("~/Content/Css/UserMenu/UserMenu.css")%>' rel="stylesheet" type="text/css" />
   
    <script src='<% =Url.Content("~/Scripts/Library/jquery.treeview.js")%>' type="text/javascript"> </script>
    <link href='<% =Url.Content("~/Content/Css/ST_Odyssey.css")%>' rel="stylesheet" type="text/css" />

    <script src='<% =Url.Content("~/Scripts/Admin/MenuUrlManage.js")%>' type="text/javascript"></script>

</asp:Content>
