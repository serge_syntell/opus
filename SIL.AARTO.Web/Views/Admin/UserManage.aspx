<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.Admin.Model.UserModel>" %>

<%@ Import Namespace="SIL.AARTO.DAL.Entities" %>
<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<%@ Import Namespace="SIL.AARTO.Web.Helpers.Paginator" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("UserManage", "Admin", FormMethod.Post, new { id = "formUserManage" })) %>
    <%{ %>
    <table cellspacing="0" cellpadding="4" align="center">
        <tr>
            <td class="ContentHead">
                <%=Html.Resource("lblPageTitle.Text")%>
            </td>
        </tr>
        <tr>
            <td height="50" valign="middle">
                <%=Html.Resource("lblSearch.Text")%><% =Html.TextBox("SearchKey", Model.SearchKey)%>
                <input type="submit" id="btnSearch" name="btnSearch" value='<% =Html.Resource("btnSearch.Value") %> '
                    class="NormalButton" />
            </td>
        </tr>
        <tr>
            <td height="50" valign="middle">
                <input type="button" id="btnAddUser" value="<% =Html.Resource("btnAdd.Value") %>"
                    class="NormalButton" />
            </td>
        </tr>
    </table>
    <table align="center" cellspacing="0" cellpadding="4" border="1" style="border-color: Black;
        font-size: 8pt; border-collapse: collapse;">
        <tr class="CartListHead">
            <td>
                <% =Html.Resource("lblHeadUserName.Text")%>
            </td>
            <td>
                <% =Html.Resource("lblHeadUserFName.Text")%>
            </td>
            <td>
                <% =Html.Resource("lblHeadUserLoginName.Text")%>
            </td>
            <td>
                <% =Html.Resource("lblUserEmail.Text")%>
            </td>
            <td>
                <% =Html.Resource("lblOperation.Text")%>
            </td>
        </tr>
        <tbody>
            <% var index = 0; %>
            <% foreach (User user in Model.Users) %>
            <%{ %>
            <% if (index % 2 == 0) %>
            <%{ %>
            <tr class="CartListItem">
                <td>
                    <% =Html.Encode(user.UserSname)%>
                </td>
                <td>
                    <% =Html.Encode(user.UserFname)%>
                </td>
                <td>
                    <% =Html.Encode(user.UserLoginName)%>
                </td>
                <td>
                    <% =Html.Encode(user.UserEmail)%>
                </td>
                <td>
                    <a href="#" onclick='OpenWin(<% =user.UserIntNo %>);'><%=Html.Resource("UserRole")%> </a>&nbsp;|&nbsp;<a href="#" onclick='EditUser(<% =user.UserIntNo %>);'>
                        <% =Html.Resource("lblOperationText.Text")%>
                    </a>&nbsp;|&nbsp
                    <a href="#" onclick="ResetPassword(<% =user.UserIntNo %>);"><%= String.IsNullOrEmpty( user.UserEmail)?Html.Resource("lblResetPassword.Text"):Html.Resource("lblEmailPassword.Text")%></a>
                </td>
            </tr>
            <%} %>
            <% else %>
            <% { %>
            <tr class="CartListItemAlt">
                <td>
                    <% =Html.Encode(user.UserSname)%>
                </td>
                <td>
                    <% =Html.Encode(user.UserFname)%>
                </td>
                <td>
                    <% =Html.Encode(user.UserLoginName)%>
                </td>
                <td>
                    <% =Html.Encode(user.UserEmail)%>
                </td>
                <td>
                    <a href="#" onclick='OpenWin(<% =user.UserIntNo %>);'><%=Html.Resource("UserRole")%> </a>&nbsp;|&nbsp;<a
                        href="#" onclick='EditUser(<% =user.UserIntNo %>);'>
                        <% =Html.Resource("lblOperationText.Text")%></a>
                        &nbsp;|&nbsp
                    <a href="#" onclick="ResetPassword(<% =user.UserIntNo %>);"><%= String.IsNullOrEmpty( user.UserEmail)?Html.Resource("lblResetPassword.Text"):Html.Resource("lblEmailPassword.Text")%></a>
                </td>
            </tr>
            <%} %>
            <% index++; %>
            <%} %>
            <tr class="CartListHead">
                <td colspan="5" align="right">
                
                    <%= Html.GridPager(new SIL.AARTO.Web.Helpers.Paginator.GridPagerProperties { PageKey = "Page", PageSize = int.Parse(ViewData["pageSize"].ToString()), RecordCount = Model.TotalCount, parameters = Model.URLPara })%>
                </td>
            </tr>
        </tbody>
    </table>
    <br />
    <table id="tbUserEditPanel" align="center" cellspacing="0" cellpadding="4" border="0"
        style="display: none;">
        <tr>
            <td class="SubContentHead" colspan="2">
                <%=Html.Resource("lblAddUserTitle.Text")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <% =Html.Resource("lblAuthority.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("UserDefaultAuthIntNo", Model.AuthorityList, new { id = "ddlUserAuthority" })%>
                <%= Html.ValidationLocalizationError("UserDefaultAuthIntNo")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <% =Html.Resource("lblUserLoginName.Text")%>
            </td>
            <td>
                <% =Html.TextBox("UserLoginName", Model.UserLoginName, new { id = "txtAddUserLoginName", @class = "inputWidthOne" })%>
                <%= Html.ValidationLocalizationError("UserLoginName")%>
            </td>
        </tr>
        <%--<tr>
            <td>
                User Password:
            </td>
            <td>
                <% =Html.TextBox("UserPassword", Model.UserPassword, new { id = "txtAddUserLoginName" })%>
                <%= Html.ValidationLocalizationError("UserPassword")%>
            </td>
        </tr>--%>
        <tr>
            <td class="NormalBold">
                <% =Html.Resource("lblUserSurname.Text")%>
            </td>
            <td>
                <% =Html.TextBox("UserSName", Model.UserSName, new { id = "txtAddUserSName", @class = "inputWidthOne" })%>
                <%= Html.ValidationLocalizationError("UserSName")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <% =Html.Resource("lblUserInitial.Text")%>
            </td>
            <td>
                <% =Html.TextBox("UserInit", Model.UserInit, new { id = "txtAddUserInit", @class = "inputWidthOne" })%>
                <%= Html.ValidationLocalizationError("UserInit")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <% =Html.Resource("lblUserFirstName.Text")%>
            </td>
            <td>
                <% =Html.TextBox("UserFName", Model.UserFName, new { id = "txtAddUserFName", @class = "inputWidthOne" })%>
                <%= Html.ValidationLocalizationError("UserFName")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <% =Html.Resource("lblUserEmailAddress.Text")%>
            </td>
            <td>
                <% =Html.TextBox("UserEmail", Model.UserEmail, new { id = "txtAddUserEmail", @class = "inputWidthOne" })%>
                <%= Html.ValidationLocalizationError("UserEmail")%>
            </td>
        </tr>
        <%-- <tr>
            <td  class="NormalBold">
                <% =Html.Resource("loblUserAccessLevel.Text")%> 
            </td>
            <td>
                <% =Html.TextBox("UserAccessLevel", Model.UserAccessLevel, new { id = "txtAddUserAccessLevel", @class = "inputWidthOne" })%>
                <%= Html.ValidationLocalizationError("UserAccessLevel")%>
            </td>
        </tr>--%>
        <tr>
            <td class="NormalBold">
                <% =Html.Resource("lblTrafficOfficer.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("TrafficOfficerIntNo", Model.TrafficOfficerList, new { id = "ddlTrafficOfficer" })%>
            </td>
        </tr>

        <tr>
            <td class="NormalBold">
                <% =Html.Resource("DefaultLanguage.Text")%>
            </td>
            <td>
             <%= Html.DropDownList("LanguageSlectorListIntNo", Model.LanguageSlectorList, new {id="ddlLanguageSlector" })%>

            </td>
        </tr>

        <tr>
            <td>
            </td>
            <td align="right">
                <input type="submit" name="btnSaveUser" id="btnSaveUser" class="NormalButton" value="<% =Html.Resource("btnSave.Value") %>" />
                <%--<input type="submit" name="btnDeleteUser" id="btnDeleteUser" value="Delete User" />--%>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <% if (Model.HasErrors) %>
    <%{ %>

    <script type="text/javascript">
        $("#tbUserEditPanel").show();
    </script>

    <%} %>
    <input type="hidden" name="UserIntNo" id="hidUserIntNo" value="<% =Model.UserIntNo %>" />
    <input type="hidden" id="hidUrl" value="<% =Request.RawUrl %>" />
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
  <% =Html.JavascriptTag("var JsResetPassword = '" + Html.Resource("lblResetPasswordSuccessed") + "';")%>
    <link href='<% =Url.Content("~/Content/Css/ST_Odyssey.css")%>' rel="stylesheet" type="text/css" />

    <script src='<% =Url.Content("~/Scripts/Admin/UserManage.js")%>' type="text/javascript"></script>

</asp:Content>
