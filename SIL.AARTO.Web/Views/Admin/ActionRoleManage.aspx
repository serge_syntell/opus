<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.Admin.Model.ActionRoleModel>" %>
<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("ActionRoleManage", "Admin", FormMethod.Post, new { id = "formActionRoleManage" })) %>
    <%{ %>

    <table  align=center>
    <tr>
                <td align="center" colspan="6" class="ContentHead" style="height:77px">
                    <%= Html.Resource("lblPageTitle.Text")%>  
                </td>


            </tr>
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblMenuList.Text")%>  
            </td>
            <td>
                <% =Html.DropDownList("MenuList", Model.MenuList, new { id = "ddlUserMenus" })%>
            </td>
            <td>
                <%--<% =Html.TextBox("MenuDescription") %><br />--%>
            </td>
            <td>
                <% =Html.DropDownList("SelectRoleList", Model.ActionRoleList, new { id = "ddlSelectRoleList", multiple = "mutiple", @class = "roleManageDropdownlist" })%>
            </td>
            <td>
                <input type="button" id="btnAddRole" value="<% =Html.Resource("btnRemove.Value") %>"  class="NormalButton"/>
                <input type="button" id="btnRemoveRole" value="<% =Html.Resource("btnAdd.Value") %>" class="NormalButton" />
            </td>
            <td>
                <% =Html.DropDownList("RoleList", Model.RoleList, new { id = "ddlRoleList", multiple = "mutiple", @class = "roleManageDropdownlist" })%>
            </td>
        </tr>
      
    </table>
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <link href='<%=Url.Content("~/Content/Css/ST_Odyssey.css")%>' rel="stylesheet" type="text/css" />
    <script src='<%=Url.Content("~/Scripts/Admin/ActionRoleManage.js")%>' type="text/javascript"></script>

</asp:Content>
