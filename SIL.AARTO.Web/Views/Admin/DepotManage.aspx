<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.Admin.Model.DepotModel>" %>

<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<%@ Import Namespace="SIL.AARTO.DAL.Entities" %>
<%@ Import Namespace="SIL.AARTO.Web.Helpers.Paginator" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("DepotManage", "Admin", FormMethod.Post, new { id = "formDepotManage" }))
       { %>
    <table align="center">
        <tr>
            <td colspan="2" class="ContentHead" style="height: 77px">
                <%= Html.Resource("lblPageTitle.Text")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <% =Html.Resource("lblMetro.Text")%>
            </td>
            <td class="NormalBold">
                <% =Html.DropDownList("MetroSearchIntNo", Model.MetroSearchList)%>
            </td>
            <th>
            </th>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <br />
                <input type="submit" class="NormalButton" value='<% =Html.Resource("btnSearch.Value") %>' />
                <input type="button" class="NormalButton" id="btnAdd" value='<% =Html.Resource("linkAdd.Text") %>' />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table cellspacing="0" cellpadding="4" border="1" align="center" style="border-color: Black;
        font-size: 8pt; border-collapse: collapse;">
        <tr class="CartListHead">
            <th>
                <% =Html.Resource("lblDepotCode.Text")%>
            </th>
            <th>
                <% =Html.Resource("lblDepotDescr.Text")%>
            </th>
            <th>
            </th>
        </tr>
        <% foreach (AartobmDepot depot in Model.DeoptList) %>
        <%{ %>
        <tr class="CartListItemAlt">
            <td>
                <% = Html.Encode(depot.AaBmDepotCode)%>
            </td>
            <td>
                <% =Html.Encode(depot.AaBmDepotDescription)%>
            </td>
            <td>
                <a href="#" onclick="ShowEditPanel(<%= Html.Encode(depot.AaBmDepotId)%>);">
                    <% =Html.Resource("linkEdit.Text")%></a>
            </td>
        </tr>
        <%} %>
        <tr class="CartListHead">
            <td colspan="5" align="right">
                <%= Html.GridPager(new SIL.AARTO.Web.Helpers.Paginator.GridPagerProperties { PageKey = "Page", PageSize = Model.PageSize, RecordCount = Model.TotalCount })%>
            </td>
        </tr>
    </table>
    <table id="MetroEditPanel" style="display: none" align="center">
        <tr>
            <td class="NormalBold">
                <% =Html.Resource("lblMetro.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("MetroIntNo", Model.MetroList, new { id = "ddpMetro" })%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <% =Html.Resource("lblDepotCode.Text")%>
            </td>
            <td>
                <% =Html.TextBox("DepotCode", Model.DepotCode, new { id = "txtDepotCode" })%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <% =Html.Resource("lblDepotDescr.Text")%>
            </td>
            <td>
                <% =Html.TextArea("DepotDescr", Model.DepotDescr, new { id = "txtDepotDesc" })%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold" colspan="2">
                <table cellspacing="1" cellpadding="0" border="0" width="360" align="center" bgcolor="#000000">
                        <tr bgcolor='#FFFFFF'>
                        <td  class="NormalBold"><%=Html.Resource("lblTranslaction.Text")%></td>
                        <td> <div id="divDepotlistDescTranEdit"></div></td>
                        </tr>
                </table>
            </td> 
        </tr>
        <tr>
            <td colspan="2">
                <input type="button" class="NormalButton" id='btnSave' value='<%=Html.Resource("btnSave.Value") %>' />
            </td>
        </tr>
    </table>
    <input type="hidden" id="hidUrl" value="<% =Request.RawUrl %>" />
    <% =Html.Hidden("hidDepId", "0", new { id = "hidDepId" })%>
    <% =Html.JavascriptTag(String.Format("var jsDepotCode ='{0}';", Html.Resource("lblDepot.Text")))%>
    <% =Html.JavascriptTag(String.Format("var txtDepotDesc ='{0}';", Html.Resource("lblDepotDescription.Text")))%>
    <% =Html.JavascriptTag(String.Format("var jsSelectMetro ='{0}';", Html.Resource("lblSelectMetro.Text")))%>
    <% =Html.JavascriptTag(String.Format("var jsDepotDescription ='{0}';", Html.Resource("lblDepotDescription.Text")))%>
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">

    <script src='<% =Url.Content("~/Scripts/Admin/DepotManage.js") %>' type="text/javascript"></script>

</asp:Content>
