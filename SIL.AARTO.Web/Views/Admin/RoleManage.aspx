<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.Admin.Model.RoleModel>" %>

<%@ Import Namespace="SIL.AARTO.DAL.Entities" %>
<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
    var mulLangStr = {
        RoleNoDescription: '<%=Html.Resource("RoleNoDescription") %>',
        RoleNoName: '<%=Html.Resource("RoleNoName") %>'
    };
</script>
    <% using (Html.BeginForm("RoleManage", "Admin", FormMethod.Post, new { id = "formRoleManage" })) %>
    <%{ %>
    <table cellspacing="0" cellpadding="4" align="center">
        <tr>
            <td class="ContentHead">
               <%=Html.Resource("lblPageTitle.Text") %>
            </td>
        </tr>
        <tr>
            <td height="50" valign="middle">
               <%-- <input type="button" id="btnAddRole" value=" <%=Html.Resource("btnAddRole.Value") %>" class="NormalButton" />--%>
            </td>
        </tr>
    </table>
    <table align="center" cellspacing="0" cellpadding="4" border="1" style="border-color: Black;
        font-size: 8pt; border-collapse: collapse;">
        <tr class="CartListHead">
            <td>
                 <%=Html.Resource("lblHeadRoleName.Text") %> 
            </td>
            <td>
                 <%=Html.Resource("lblHeadRoleDescription.Text")%> 
            </td>
            <td>
                 <%=Html.Resource("lblHeadOperation.Text")%> 
            </td>
        </tr>
        <% var index = 0; %>
        <% foreach (AartoUserRole userRole in Model.UserRoleList) %>
        <%{ %>
        <% if (index % 2 == 0) %>
        <%{ %>
        <tr class="CartListItem">
            <td>
                <% =Html.Encode(userRole.AaUserRoleName) %>
            </td>
            <td>
                <% =Html.Encode(userRole.AaUserRoleDescription) %>
            </td>
            <td>
                <a onclick="EditUserRole('<% =userRole.AaUserRoleId %>');"> <%=Html.Resource("lblHeadOperation.Text")%></a>
            </td>
        </tr>
        <%} %>
        <% else %>
        <% { %>
        <tr class="CartListItemAlt">
            <td>
                <% =Html.Encode(userRole.AaUserRoleName) %>
            </td>
            <td>
                <% =Html.Encode(userRole.AaUserRoleDescription) %>
            </td>
            <td>
                <a onclick="EditUserRole('<% =userRole.AaUserRoleId %>');"> <%=Html.Resource("lblHeadOperation.Text")%></a>
            </td>
        </tr>
        <%} %>
        <% index++; %>
        <%} %>
        <tr class="CartListHead">
            <td colspan="5">
                &nbsp;
            </td>
        </tr>
    </table>
    <br />
    <table align="center" cellspacing="0" cellpadding="4" border="0"   id="divRolePanel" style="display: none;">
    <tr><td  class="NormalBold">
   
             <%=Html.Resource("lblRoleName.Text") %></td><td><% =Html.TextBox("RoleName", Model.RoleName, new { id="txtRoleName",readOnly="True",@class="inputWidthOne"})%></td>
            </tr>
            <tr>
        <td  class="NormalBold">
            <%=Html.Resource("lblRoleDescription.Text")%></td><td><% =Html.TextArea("RoleDescription", Model.RoleDescription, new { id = "txtRoleDescription",@class="inputWidthOne" })%></td>
            </tr>
            <tr>
                       <td class="NormalBold" colspan="2">
                               <table cellspacing="1" cellpadding="0" border="0" width="360" align="center" bgcolor="#000000">
                                      <tr bgcolor='#FFFFFF'>
                                        <td  class="NormalBold"><%=Html.Resource("lblTranslaction.Text")%></td>
                                        <td> <div id="divUserRolelistDescTranEdit"></div></td>
                                      </tr>
                               </table>
                            </td> 
                       </tr>
            <tr>
        <td></td>
        <td>
            <input type="button" name="btnSave" id="btnSave" value='<%=Html.Resource("btnSave.Value")%>'  class="NormalButton"/>
            <input type="button" id="btnDeleteRole" value='<%=Html.Resource("btnDelete.Value")%>'  class="NormalButton"/>
        </td>
</tr></table>
    <input type="hidden" id="hidRoleID" name="RoleID" value="<% =Model.RoleID %>" />
    <input type="hidden" id="hidUrl" value="<% =Request.RawUrl %>" />
 <br /> <br />
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <link href='<% =Url.Content("~/Content/Css/ST_Odyssey.css")%>' rel="stylesheet" type="text/css" />

    <script src='<% =Url.Content("~/Scripts/Admin/RoleManage.js")%>' type="text/javascript"></script>

</asp:Content>
