<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Content.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.BookManagement.Model.TransferBookModel>" %>

<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("TransferBooks", "BookManagement", FormMethod.Post, new { id = "formTransferBook" })) %>
    <%{ %>
    <% bool option = true; %>
    <% if (ViewData["DepotOption"] == null) %>
    <%{ %>
    <%   ViewData["DepotOption"] = true; %>
    <%} %>
    <%option = (bool)ViewData["DepotOption"];
    %>
    <table cellspacing="0" cellpadding="4" align="center">
        <tr>
            <td class="ContentHead">
                <% =Html.Resource("PageTitle.Text")%>
            </td>
        </tr>
    </table>
    <table cellspacing="0" align="center">
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblTransferOption.Text")%>
            </td>
            <td>
                <% =Html.RadioButton("Option", "Depot", option, new { id = "chbDepot" })%>&nbsp;<%= Html.Resource("lblOptionDepot.Text")%>&nbsp;&nbsp;&nbsp;&nbsp;
               <% =Html.RadioButton("Option", "Officer", !option, new { id = "chbOfficer" })%>&nbsp;<%= Html.Resource("lblOptionOfficer.Text")%>
            </td>
        </tr>
        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblMetro.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("MetroIntNo", Model.MetroList, new { id = "ddpMetroList" })%>
                <% =Html.ValidationMessage("MetroIntNo")%>
            </td>
        </tr>
        
        <tr id="trFromDepot">
            <td class="NormalBold">
                <%= Html.Resource("lblDepot.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("DepotFromIntNo", Model.DepotFromList, new { id = "ddpDepotFromList" })%>
                <% =Html.ValidationMessage("DepotFromIntNo")%>
            </td>
        </tr>
        <tr id="trFromOfficer">
            <td class="NormalBold">
                <%= Html.Resource("lblOfficer.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("TOFromIntNo", Model.OfficerFromList, new { id = "ddpOfficerFromList" })%>
                <% =Html.ValidationMessage("TOFromIntNo")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblDocumentRange.Text")%>
            </td>
            <td>
                <% =Html.TextBox("StartNo", Model.StartNo)%>-<% =Html.TextBox("EndNo", Model.EndNo)%>
                <% =Html.ValidationMessage("StartNo")%><% =Html.ValidationMessage("EndNo")%>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table align="center">
        <tr>
            <td>
                <input type="submit" class="NormalButton" runat="server" id="btnSearch" value="<%$Resources:SearchBook.Text %>" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table  id="tbIssueBookList" cellspacing="0" cellpadding="4" border="1" align="center" style="border-color: Black; font-size: 8pt; border-collapse: collapse;">
        <tr class="CartListHead">
           <th>
                <%= Html.Resource("lblHeadBookNo.Text")%>
            </th>
            <th>
                 <%= Html.Resource("lblHeadBookStartNo.Text")%>  
            </th>
            <th>
                 <%= Html.Resource("lblHeadBookEndNo.Text")%>  
            </th>
            <th>
                <%= Html.Resource("lblHeadBookType.Text")%>
            </th>
            <th>
                <%= Html.Resource("lblHeadBookPrefix.Text")%>
            </th>
            <th><a id="aselectAll" style="cursor:pointer"><%= Html.Resource("lblSelectAll.Text")%></a>
            <a id="aclearALL" style="cursor:pointer"> <% =Html.Resource("lblClearALL.Text") %></a> 
            </th>
        </tr>
        <% foreach (SIL.AARTO.DAL.Entities.AartobmBook book in Model.BookList) %>
        <%{ %>
        <tr class="CartListItemAlt">
            <td>
                <% =Html.Encode(book.AaBmBookNo)%>
            </td>
            <td>
                <% =Html.Encode(book.AaBmBookStartingNo) %>
            </td>
            <td>
                <% =Html.Encode(book.AaBmBookEndingNo) %>
            </td>
             <td>
                <% =Html.Encode(book.AaBmBookTypeIdSource == null ? "" : book.AaBmBookTypeIdSource.AaBmBookTypeName)%>
            </td>
            <td>
                <% =Html.Encode(book.NphIntNoSource == null ? "" : book.NphIntNoSource.Nprefix)%>
            </td>
            <td>
                <% =Html.CheckBox("CheckBox",false,new {id= book.AaBmBookId}) %>
            </td>
        </tr>
        <%} %>
    </table>
        <br />
    <br />
    <table align="center">
        <tr id="trToDepot">
            <td class="NormalBold">
                <%= Html.Resource("lblDepot.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("DepotToIntNo", Model.DepotToList, new { id = "ddpDepotToList" })%>
                <% =Html.ValidationMessage("DepotToIntNo")%>
            </td>
        </tr>
        <tr id="trToOfficer">
            <td class="NormalBold">
                <%= Html.Resource("lblOfficer.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("TOToIntNo", Model.OfficerToList, new { id = "ddpOfficerToList" })%>
                <% =Html.ValidationMessage("TOToIntNo")%>
            </td>
        </tr>
    </table>
        <br />
    <br />
    <table align="center">
        <tr>
            <td>
                <input type="button" class="NormalButton" id="btnTransfer" value="<% =Html.Resource("btnTransfer.Value") %>" />
                <%-- <input type="button" class="NormalButton" id="btnPrintNote" value="<% =Html.Resource("btnPrintNotes.Value") %>" />--%>
            </td>
        </tr>
    </table>

    <script type="text/javascript">
        var optionType;
    </script>

    <% if (option) %>
    <%{ %>

    <script type="text/javascript">
        $("#trToOfficer").hide();
        $("#trFromOfficer").hide();
        $("#trFromDepot").show();
        $("#trToDepot").show();
        optionType = "DEPOT";
    </script>

    <%} %>
    <% else %>
    <%{ %>

    <script type="text/javascript">
        $("#trToOfficer").show();
        $("#trFromOfficer").show();
        $("#trFromDepot").hide();
        $("#trToDepot").hide();
        optionType = "OFFICER";
    </script>

    <%} %>
    <% =Html.JavascriptTag(String.Format("var confirmMessage ='{0}';",Html.Resource("ConfirmMessage"))) %>
    <% =Html.JavascriptTag(String.Format("var selectDepot ='{0}';", Html.Resource("NoSelectDepot")))%>
    <% =Html.JavascriptTag(String.Format("var selectMetro ='{0}';", Html.Resource("NoSelectMetro")))%>
    <% =Html.JavascriptTag(String.Format("var selectOfficer ='{0}';", Html.Resource("NoSelectOfficer")))%>
    <% =Html.JavascriptTag(String.Format("var differentDepot ='{0}';", Html.Resource("DifferentDepot")))%>
    <% =Html.JavascriptTag(String.Format("var differentOfficer ='{0}';", Html.Resource("DifferentOfficer")))%>
    <% =Html.JavascriptTag(String.Format("var selectBooks ='{0}';", Html.Resource("NoSelectBooks")))%>
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">

    <script src='<% =Url.Content("~/Scripts/BookManagement/TransferBooks.js")%>' type="text/javascript"></script>

</asp:Content>
