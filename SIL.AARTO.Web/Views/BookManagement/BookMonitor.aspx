<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Content.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.BookManagement.Model.BookMonitorModel>" %>

<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("BookMonitor", "BookManagement", FormMethod.Post, new { id = "formBookMonitor" })) %>
    <%{ %>
    <table cellspacing="0" cellpadding="4" align="center">
        <tr>
            <td class="ContentHead">
                <% =Html.Resource("PageTitle.Text")%>
            </td>
        </tr>
    </table>
    <table cellspacing="0" align="center">
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblMetro.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("MetrIntNo", Model.MetroList, new { id = "ddpMetroList" })%>
                <% =Html.ValidationMessage("MetrIntNo")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblDepot.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("DepotIntNo", Model.DepotList, new { id = "ddpDepotList" })%>
                <% =Html.ValidationMessage("DepotIntNo")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblOfficer.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("OfficerIntNo", Model.OfficerList, new { id = "ddpOfficerList" })%>
                <% =Html.ValidationMessage("OfficerIntNo")%>
            </td>
        </tr>
         <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblBooksType.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("BookTypeId", Model.BookTypeList, new { id = "ddpBookTypeId" })%>
                <% =Html.ValidationMessage("BookTypeId")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblBookStatus.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("Status", Model.BookStatusList, new { id = "ddpBookStatusList" })%>
                <% =Html.ValidationMessage("Status")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblDocumentRange.Text")%>
            </td>
            <td>
                <% =Html.TextBox("StartNo", Model.StartNo)%>-<% =Html.TextBox("EndNo", Model.EndNo)%>
                <% =Html.ValidationMessage("StartNo")%><% =Html.ValidationMessage("EndNo")%>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table align="center">
        <tr>
            <td>
                <input type="submit" class="NormalButton" id="btnSearch" runat="server" value="<%$Resources:SearchBook.Text %>" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table cellspacing="0" cellpadding="4" border="1" align="center" style="border-color: Black;
        font-size: 8pt; border-collapse: collapse;">
        <tr class="CartListHead">
            <th>
                <%= Html.Resource("lblHeadBookNo.Text")%>
            </th>
            <th>
                <%= Html.Resource("lblHeadBookStartNo.Text")%>
            </th>
            <th>
                <%= Html.Resource("lblHeadBookEndNo.Text")%>
            </th>
            <th>
                <%= Html.Resource("lblHeadBookType.Text")%>
            </th>
            <th>
                <%= Html.Resource("lblHeadBookPrefix.Text")%>
            </th>
            <th>
                <%= Html.Resource("lblHeadBookStatus.Text")%>
            </th>
            <th>
                <%= Html.Resource("lblHeadBookMetro.Text")%>
            </th>
            <th>
                <%= Html.Resource("lblHeadBookDepot.Text")%>
            </th>
            <th>
            </th>
        </tr>
        <% foreach (SIL.AARTO.DAL.Entities.AartobmBook book in Model.BookList) %>
        <%{ %>
        <tr class="CartListItemAlt">
            <td>
                <% =Html.Encode( book.AaBmBookNo)%>
            </td>
            <td>
                <% =Html.Encode(book.AaBmBookStartingNo) %>
            </td>
            <td>
                <% =Html.Encode(book.AaBmBookEndingNo) %>
            </td>
            <td>
                <% =Html.Encode(book.AaBmBookTypeIdSource == null ? "" : book.AaBmBookTypeIdSource.AaBmBookTypeName)%>
            </td>
            <td>
                <% =Html.Encode(book.NphIntNoSource == null ? "" : book.NphIntNoSource.Nprefix)%>
            </td>
            <td>
                <% =Html.Encode(book.AaBmBookStatusIdSource.AaBmStatusDescription?? "")%>
            </td>
            <td>
                <% if (book.MtrIntNoSource != null) %>
                <%{ %>
                <% =Html.Encode(book.MtrIntNoSource.MtrName) %>
                <%} %>
            </td>
            <td>
                <% if (book.AaBmDepotIdSource != null) %>
                <%{ %>
                <% =Html.Encode(book.AaBmDepotIdSource.AaBmDepotDescription) %>
                <%} %>
            </td>
            <td>
                <a href='<% =Url.Content("~/BookManagement/DocumentMonitor?bookId=" + book.AaBmBookId) %>'
                    target="_blank">
                    <%= Html.Resource("lblDocumentLink.Text")%></a>
            </td>
        </tr>
        <%} %>
    </table>
    <%} %>
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <script src='<% =Url.Content("~/Scripts/BookManagement/BookMonitor.js")%>' type="text/javascript"></script>
</asp:Content>
