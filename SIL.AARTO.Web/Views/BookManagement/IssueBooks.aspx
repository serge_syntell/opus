<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Content.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.BookManagement.Model.IssueBooksModel>" %>

<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("IssueBooks", "BookManagement", FormMethod.Post, new { id = "formIssueBook" })) %>
    <%{ %>
    <table cellspacing="0" cellpadding="4" align="center">
        <tr>
            <td class="ContentHead" style="height:77px">
                <% =Html.Resource("PageTitle.Text")%>
            </td>
        </tr>
    </table>
    <table cellspacing="0" align="center">
        <tr >
            <td class="NormalBold">
                <%= Html.Resource("lblMetro.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("MetroIntNo", Model.MetroList, new { id = "ddpMetroList" })%>
                <% =Html.ValidationMessage("MetroIntNo")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblDepot.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("DepotIntNo", Model.DepotList, new { id = "ddpDepotList" })%>
                <% =Html.ValidationMessage("DepotIntNo")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblDocumentRange.Text")%>
            </td>
            <td>
                <% =Html.TextBox("StartNo", Model.StartNo)%>-<% =Html.TextBox("EndNo", Model.EndNo)%> 
                <% =Html.ValidationMessage("StartNo")%><% =Html.ValidationMessage("EndNo")%>
            </td>
        </tr>
        
        
    </table>
    <br />
    <br />
    <table align="center"><tr><td><input type="submit" class="NormalButton" runat="server" id="btnSearch" value="<%$Resources:SearchBook.Text %>" /></td></tr></table>
    <br />
    <br />
    <table cellspacing="0" cellpadding="4" border="1" align="center" style="border-color: Black; font-size: 8pt; border-collapse: collapse;">
        <tr class="CartListHead">
           <th>
                <%= Html.Resource("lblHeadBookNo.Text")%>
            </th>
            <th>
                 <%= Html.Resource("lblHeadBookStartNo.Text")%>  
            </th>
            <th>
                 <%= Html.Resource("lblHeadBookEndNo.Text")%>  
            </th>
             <th>
                <%= Html.Resource("lblHeadBookType.Text")%>
            </th>
            <th>
                <%= Html.Resource("lblHeadBookPrefix.Text")%>
            </th>
            <th><a id="aselectAll" style="cursor:pointer"><%= Html.Resource("lblSelectAll.Text")%></a>
            <a id="aclearALL" style="cursor:pointer"> <% =Html.Resource("lblClearALL.Text") %></a> 
            </th>
        </tr>
        <% foreach (SIL.AARTO.DAL.Entities.AartobmBook book in Model.BookList) %>
        <%{ %>
        <tr class="CartListItemAlt">
            <td>
                <% =Html.Encode( book.AaBmBookNo)%>
            </td>
            <td>
                <% =Html.Encode(book.AaBmBookStartingNo) %>
            </td>
            <td>
                <% =Html.Encode(book.AaBmBookEndingNo) %>
            </td>
             <td>
                <% =Html.Encode(book.AaBmBookTypeIdSource == null ? "" : book.AaBmBookTypeIdSource.AaBmBookTypeName)%>
            </td>
            <td>
                <% =Html.Encode(book.NphIntNoSource == null ? "" : book.NphIntNoSource.Nprefix)%>
            </td>
            <td>
                <% =Html.CheckBox("CheckBox",false,new {id= book.AaBmBookId}) %>
            </td>
        </tr>
        <%} %>
    </table>
    <br />
    <table  align="center">
    <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblOfficer.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("TOIntNo", Model.OfficerList, new { id = "ddpOfficerList" })%>
                <% =Html.ValidationMessage("TOIntNo")%>
            </td>
        </tr>
    </table>
    <br /><br />
    <table align="center">
        <tr>
            <td>
                <input type="button"  class="NormalButton" id="btnIssue" value="<% =Html.Resource("btnIssue.Value") %>" />
               <%-- <input type="button" id="btnPrintNote" value="<% =Html.Resource("btnPrintNotes.Value") %>" />--%>
            </td>
        </tr>
    </table>
    <% =Html.JavascriptTag(String.Format("var confirmMessage ='{0}';",Html.Resource("ConfirmMessage"))) %>
    <% =Html.JavascriptTag(String.Format("var selectDepot ='{0}';", Html.Resource("NoSelectDepot")))%>
    <% =Html.JavascriptTag(String.Format("var selectMetro ='{0}';", Html.Resource("NoSelectMetro")))%>
    <% =Html.JavascriptTag(String.Format("var selectOfficer ='{0}';", Html.Resource("NoSelectOfficer")))%>
    <% =Html.JavascriptTag(String.Format("var selectBooks ='{0}';", Html.Resource("NoSelectBooks")))%>
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <script src='<% =Url.Content("~/Scripts/BookManagement/IssueBooks.js")%>' type="text/javascript"></script>
</asp:Content>
