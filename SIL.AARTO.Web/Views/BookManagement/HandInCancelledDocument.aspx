<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Content.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.BookManagement.Model.HandInDocumentModel>" %>

<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<%@ Import Namespace="SIL.AARTO.BLL.Utility" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table cellspacing="0" cellpadding="4" align="center">
        <tr>
            <td class="ContentHead">
                <% =Html.Resource("PageTitle.Text")%>
            </td>
        </tr>
    </table>
    <p align="center" style="height:10px">
        <span id="spMessage" style="font-size: 12px; color: Red;"></span>
    </p>
    <div id="inputDiv" align="center" style="width:100%; font-weight:bold;margin-top:30px;">
        <p>
            <%= Html.Resource("lblDocumentNo.Text")%>&nbsp;
            <input id="prefixTxt" style="width:50px" onkeyup="AutoQueue()"/>
            <input id="docNoTxt" style="width:60px" />
            <% =Html.TextBox("authNoTxt", Model.AuthNo, new { id = "authNoTxt", style = "width:84px" })%>
            <% =Html.CheckBox("chbAffidavit1", false, new { id = "chbAffidavit1" })%>
            <% =Html.Resource("lblAffidavit.Text")%>
        </p>
        <p> <% =Html.Resource("lblCommentForSupervisor")%></p>
    </div>
    <div id="messageDiv" style="display:none; margin-top:20px;" align="center">
        <p><span id="fullDocumentNo" style="font-size: 18px; color: Red; font-weight:bold; "></span></p>
        <p><span style="font-size: 12px; color: Red; width:320px;"><%=Html.Resource("PopupMessage") %></span></p>
        <p>
        <span>
            <input type="button" id="yesButton" value="<%= Html.Resource("Yes") %>" style="width:40px; text-align:center;" />
            <input type="button" id="noButton" value="<%= Html.Resource("No") %>" style="width:40px; margin-left:10px;text-align:center;" />
        </span>
        </p>
    </div>
    <div id="searchDiv" style="margin-top:20px;" align="center">
        <span><input type="button" id="searchButton" class="NormalButton" name="searchButton" value="<% =Html.Resource("Search") %>" /></span>
        <span><input type="button" id="btnReverse" class="NormalButton" name="btnReverse" value="<% =Html.Resource("Reverse") %>" /></span>
    </div>
    <div id="submitDiv" style="display:none">
    <table cellspacing="0" align="center">
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblDocumentNo.Text")%>
            </td>
            <td>
                <% =Html.TextBox("DocumentNo", Model.DocumentNo, new { id = "txtDocumentNo" })%>
                <% =Html.ValidationMessage("DocumentNo")%>
                <% UserLoginInfo userLofinInfo = (UserLoginInfo)Session["UserLoginInfo"]; %>
                <% if (userLofinInfo == null) %>
                <%{ %>
                <script type="text/javascript">
                    window.parent.parent.location = "/Account/Login";
                </script>
                <%} %>
                <%else %>
                <%{ %>
                <% if (userLofinInfo.UserRoles.Contains((int)SIL.AARTO.DAL.Entities.AartoUserRoleList.Supervisor)) %>
                <%{ %>
                <% if (ViewData["showCheckBox"] != null && (bool)ViewData["showCheckBox"]) %>
                <%{ %>
                <% =Html.CheckBox("chbAffidavit", false, new { id = "chbAffidavit" })%>
                <% =Html.Resource("lblAffidavit.Text")%><br />
                <br />
                <% =Html.Resource("lblCommentForSupervisor")%>
                <%} %>
                <%} %>
                <% else %>
                <%{ %><br />
                <br />
                <% =Html.Resource("lblCommentForClerk")%>
                <%} %>
                <%} %>
            </td>
        </tr>
    </table>
    <%-- <table id="tbDocumentInfo" cellspacing="0" align="center">
        <tr>
            <td>
                <%= Html.Resource("lblDocumentNo.Text")%>
            </td>
            <td>
             
            </td>
        </tr>
    </table>--%>
    <br />
    <br />
    <table align="center">
        <tr>
            <td>
                <input type="button" id="btnHandIn" class="NormalButton" name="btnHandIn" value="<% =Html.Resource("btnHandInCancelledDocument.Value") %>" />
                <input id="checkNumTxt" />
                <%=Html.TextBox("Rule_9300", Model.Rule_9300, new { id = "Rule_9300" })%>
            </td>
        </tr>
    </table>
    </div>
<input type="hidden" id="hidOperation" value="" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <% =Html.JavascriptTag(String.Format("var documentIsRequired ='{0}';", Html.Resource("DocumentIsRequired")))%>
    <%--<% =Html.JavascriptTag(String.Format("var successMessage ='{0}';", Html.Resource("SuccessMessage")))%>--%>
     <% =Html.JavascriptTag(String.Format("var operationDocumentFailed ='{0}';", Html.Resource("OperationDocumentFailed")))%>
    <script src='<% =Url.Content("~/Scripts/BookManagement/HandInCancelledDocument.js?ff="+DateTime.Now)%>'
        type="text/javascript"></script>
</asp:Content>
