<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Content.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.BookManagement.Model.ReceiveBooksModel>" %>

<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
    <script src='<% =Url.Content("~/Scripts/BookManagement/ReceiveBook.js")%>' type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("ReceiveBooks", "BookManagement", FormMethod.Post, new { id = "formReceiveBook" })) %>
    <%{ %>
    <table cellspacing="0" cellpadding="4" align="center">
        <tr>
            <td class="ContentHead" style="height: 77px">
                <% =Html.Resource("PageTitle.Text")%>
            </td>
        </tr>
    </table>
    <p align="center">
        <span style="font-size: 12px; color: Red;">
            <% =Html.Encode(String.Format("{0}", ViewData["Message"]))%>
        </span>
    </p>
    <table cellspacing="0" align="center">
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblPrinter.Text")%>
            </td>
            <td>
                <% =Html.TextBox("PrintingCompany", Model.PrintingCompany, new { id = "txtPrintingCompany" })%>
                <% =Html.ValidationMessage("PrintingCompany")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblBooksNum.Text")%>
            </td>
            <td>
                <% =Html.TextBox("BooksNum", Model.BooksNum, new { id="txtNumberOfBooks"})%>
                <% =Html.ValidationMessage("BooksNum")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblMetro.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("MetrIntNo", Model.MetroList, new { id = "ddpMetroList" })%>
                <% =Html.ValidationMessage("MtrIntNo")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblDepot.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("DepotIntNo", Model.DepotList, new { id = "ddpDepotList" })%>
                <% =Html.ValidationMessage("PrinterName")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblBooksType.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("BookTypeId", Model.BookTypeList, new { id = "ddpBookTypeId" })%>
                <% =Html.ValidationMessage("BookTypeId")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblPrefix.Text")%>
            </td>
            <td>
                <% =Html.DropDownList("NPHIntNo", Model.NotPrefixList, new { id = "ddpNotPrefix" })%>
                <% =Html.ValidationMessage("NPHIntNo")%>
            </td>
        </tr>
        <tr>
            <td class="NormalBold">
                <%= Html.Resource("lblBookRange.Text")%>
            </td>
            <td>
                <% =Html.TextBox("BookStartNo", Model.BookStartNo, new { id = "txtBookStartNo" })%>
                -
                <% =Html.TextBox("BookEndNo", Model.BookEndNo, new { id="txtBookEndNo",  Readonly="readonly"})%>
                <% =Html.ValidationMessage("BookEndNo")%><% =Html.ValidationMessage("BookStartNo")%>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table align="center">
        <tr>
            <td>
                <input type="button" id="btnReceive" class="NormalButton" name="btnReceive" value="<% =Html.Resource("btnReceive.Value") %>" />
                <%--<input type="button" id ="btnPrintNote" class="NormalButton" name="btnPrintNote" value="<% =Html.Resource("btnPrintNotes.Value") %>" />--%>
            </td>
        </tr>
    </table>
    <% =Html.JavascriptTag(String.Format("var confirmMessage ='{0}';",Html.Resource("ConfirmMessage"))) %>
    <% =Html.JavascriptTag(String.Format("var selectDepot ='{0}';", Html.Resource("NoSelectDepot")))%>
    <% =Html.JavascriptTag(String.Format("var jsPrintingCompany ='{0}';", Html.Resource("PrintintCompanyIsRequired")))%>
    <% =Html.JavascriptTag(String.Format("var jsBoookNum ='{0}';", Html.Resource("NumberOfBooksIsRequired")))%>
    <% =Html.JavascriptTag(String.Format("var jsBookStartNo ='{0}';", Html.Resource("BookStartNoIsRequired")))%>
    <% =Html.JavascriptTag(String.Format("var jsBookEndNo ='{0}';", Html.Resource("BookEndNoIsRequired")))%>
    <% =Html.JavascriptTag(String.Format("var jsIncorrectBoookNum ='{0}';", Html.Resource("NoOfBooksIsIncorrect")))%>
    <% =Html.JavascriptTag(String.Format("var jsIncorrectBookStartNo ='{0}';", Html.Resource("BookStartNoIsIncorrect")))%>
    <% =Html.JavascriptTag(String.Format("var JsSelectBookType ='{0}';", Html.Resource("NoSelectBookType")))%>
    <% =Html.JavascriptTag(String.Format("var JsSelectPrefix ='{0}';", Html.Resource("NotPrefixIsRequired")))%>
    <% =Html.JavascriptTag(String.Format("var JsBookStartNoIsNull ='{0}';", Html.Resource("BookStartNoIsNull")))%>
    <% if (ViewData["Successed"] != null && (bool)ViewData["Successed"]) %>
    <%{ %>
    <script type="text/javascript">
        $("#txtPrintingCompany").val('');
        $("#txtNumberOfBooks").val('');
        $("#ddpBookTypeId").val(0);
        $("#txtBookStartNo").val('');
        $("#txtBookEndNo").val('');
        $("#ddpMetroList").val(0);
        $("#ddpDepotList").val(0);
    </script>
    <%} %>
    <%else
       { %>
    <% if (Model.NPHIntNo > 0) %>
    <%{ %>
    <% =Html.JavascriptTag(String.Format("var selectedPrefix ='{0}';", Model.NPHIntNo.Value))%>
    <%} %>
    <%else
       { %>
    <% =Html.JavascriptTag(String.Format("var selectedPrefix ='0';"))%>
    <%} %>
    <script type="text/javascript">

        $.post(_ROOTPATH + "/BookManagement/GetPrefixByType",
              {
                  bookTypeId: $("#ddpBookTypeId").val(),
                  metroIntNo: $("#ddpMetroList").val()
              },
               function (prefixItems) {
                   $("#ddpNotPrefix option[value!=0]").remove();
                   $.each(prefixItems, function (i, prefix) { if (prefix.NPHIntNo != 0) { $("<option value=" + prefix.NPHIntNo + ">" + prefix.NotPrefix + "</option> ").appendTo($("#ddpNotPrefix")); } });
                   $("#ddpNotPrefix").val(selectedPrefix);
               }, 'json');
    </script>
    <%} %>
    <% if (Session["PrintData"] != null) %>
    <%{ %>
    <script type="text/javascript">
        window.open(_ROOTPATH + "/ReportView.aspx");
    </script>
    <%} %>
    <%} %>
</asp:Content>
