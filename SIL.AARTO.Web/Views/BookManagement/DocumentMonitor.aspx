<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Content.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.BookManagement.Model.DocumentMonitorModel>" %>

<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table cellspacing="0" cellpadding="4" align="center">
        <tr>
            <td class="ContentHead">
                <% =Html.Resource("PageTitle.Text")%>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="4" border="1" align="center" style="border-color: Black;
        font-size: 8pt; border-collapse: collapse;">
        <tr>
            <td class="SubContentHead" colspan="6">
                <% =Html.Resource("lblBookNo.Text")%>
                <% =Html.Encode(Model.BookNo) %>
            </td>
        </tr>
        <tr class="CartListHead">
            <th>
                <% =Html.Resource("lblHeadDocumentNo.Text")%>
            </th>
            <th>
                <% =Html.Resource("lblHeadDocumentStatus.Text")%>
            </th>
            <th>
                <% =Html.Resource("lblHeadMetro.Text")%>
            </th>
            <th>
                <% =Html.Resource("lblHeadDepot.Text")%>
            </th>
            <th>
                <% =Html.Resource("lblHeadOfficer.Text")%>
            </th>
            <th>
                <% =Html.Resource("lblHeadDocumentCreateDate.Text")%>
            </th>
        </tr>
        <% foreach (SIL.AARTO.DAL.Entities.AartobmDocument document in Model.DocumentList) %>
        <%{ %>
        <tr class="CartListItemAlt">
            <td>
                <% =Html.Encode(document.AaBmDocNo)%>
            </td>
            <td>
                <% =Html.Encode(document.AaBmDocStatusIdSource.AaBmStatusDescription?? "")%>
            </td>
            <td>
                <% if (document.MtrIntNoSource != null) %>
                <%{ %>
                <% =Html.Encode(document.MtrIntNoSource.MtrName)%>
                <%} %>
            </td>
            <td>
                <% if (document.AaBmDepotIdSource != null) %>
                <% {%>
                <% =Html.Encode(document.AaBmDepotIdSource.AaBmDepotDescription)%>
                <%} %>
            </td>
            <td>
                <% if (document.ToIntNoSource != null) %>
                <%{ %>
                <% =Html.Encode(document.ToIntNoSource.TosName)%>
                <%} %>
            </td>
            <td>
                <% =Html.Encode(String.Format("{0:yyyy/MM/dd}",document.AaBmDocCreatedDate))%>
            </td>
        </tr>
        <%} %>
        <tr >
            <td colspan="6" align="right" class="SubContentHead">
                <input type="button" value='<% =Html.Resource("btnClose.Value")%>' onclick="window.close();"
                    class="NormalButton" />
            </td>
        </tr>
    </table>
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
