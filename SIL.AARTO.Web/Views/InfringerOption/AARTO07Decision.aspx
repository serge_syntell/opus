<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.InfringerOption.Model.DecisionModel>" %>

<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<%@ Import Namespace="SIL.AARTO.DAL.Entities" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        var imageUrlString = "<%= Model.DocumentImageUrlList %>";
        //$("#divCommon").attr("disabled", "disabled");
        $("#div07").attr("disabled", "disabled");
    </script>

    <script src='<% =Url.Content("~/Scripts/InfringerOption/AARTODecision.js")%>' type="text/javascript"></script>

    <div class="ContentHead" style="text-align: center;">
        <%= Html.Resource("titleOfPage.Text")%></div>
    <% using (Html.BeginForm())
       { %>
    <div style="margin-left: auto; margin-right: auto; width: 800px;">
        <span class="NormalBold">
            <%= Html.Resource("lblDisplayForm.Text")%></span><input type="radio" name="DisplayOption"
                value="1" id="rdbDisplayForm" />
        <span class="NormalBold">
            <%= Html.Resource("lblDisplayImage.Text")%></span><input type="radio" name="DisplayOption"
                value="0" checked="checked" id="rdbDisplayImage" />
    </div>
    <div id="divPic" style="margin-left: auto; margin-right: auto; width: 800px;">
        <div style="float: right">
            <a id="lkPre" class="page">
                <%= Html.Resource("btnPreviousPage.Text")%></a>&nbsp;&nbsp;&nbsp;&nbsp; <a id="lkNext"
                    class="page">
                    <%= Html.Resource("btnNextPage.Text")%></a></div>
    <img id="imgDocument" class="width_800" src="" alt="" />
    </div>
    <div id="divForm" style="margin-top: 25px;">
        <div id="divCommon" style="margin-left: auto; margin-right: auto; width: 800px;">
            <% Html.RenderPartial("CommonRepInfo"); %>
        </div>
        <div id="div07" style="margin-left: auto; margin-right: auto; width: 800px;">
            <fieldset>
                <legend>
                    <%= Html.Resource("NominatedDriverParticular.legend")%></legend>
                <div class="div_h" style="">
                    <div class="left_title NormalBold">
                        <label id="lblNewSurname" for="txtNewSurname">
                            <%= Html.Resource("lblSurname.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfSurname", Model.Rep.AaRepNewInfSurname, new { id = "txtNewSurname" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewFirstNames" for="txtNewFirstNames">
                            <%= Html.Resource("lblFirstNames.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfFirstName", Model.Rep.AaRepNewInfFirstName, new { id = "txtNewFirstNames" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewInitials" for="txtNewInitials">
                            <%= Html.Resource("lblInitials.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfInitials", Model.Rep.AaRepNewInfInitials, new { id = "txtNewInitials" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewGender" for="rdbNewGender">
                            <%= Html.Resource("lblGender.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input_box">
                        <%= Html.RadioButton("AaRepNewInfGender","M", Model.Rep.AaRepNewInfGender=="M", new { id = "rdbNewGenderM" })%><%= Html.Resource("lblMale.Text")%>
                    </div>
                    <div class="right_input_box">
                        <%= Html.RadioButton("AaRepNewInfGender","F", Model.Rep.AaRepNewInfGender=="F", new { id = "rdbNewGenderF" })%><%= Html.Resource("lblFemale.Text")%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewDOB" for="txtNewDOB">
                            <%= Html.Resource("lblDOB.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfDob", Model.Rep.AaRepNewInfDob, new { id = "txtNewDOB" })%>
                        <%= Html.Resource("lblDateFormat.Text")%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewIDType" for="ddlNewIDType">
                            <%= Html.Resource("lblIDType.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.DropDownList("AaRepNewInfIdTypeId", Model.IDTypeList, new { id = "ddlNewIDType" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewCountryOfIssue" for="txtNewCountryOfIssue">
                            <%= Html.Resource("lblCountryOfIssue.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfDob", Model.Rep.AaRepNewInfIdIssueCountryName, new { id = "txtNewCountryOfIssue" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewLicenceCode" for="ddlNewLicenceCode">
                            <%= Html.Resource("lblLicenceCode.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input_wide">
                        <%= Html.DropDownList("AaRepNewInfLiCodeId", Model.LicenceCodeList, new { id = "ddlNewLicenceCode" })%>
                        <%= Html.TextBox("AaRepNewInfForeignCode", Model.Rep.AaRepNewInfForeignCode, new { id = "txtNewForeignCode" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewLearnersCode" for="ddlNewLearnersCode">
                            <%= Html.Resource("lblLearnersCode.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.DropDownList("AaRepNewInfLeCodeId", Model.LearnersCodeList, new { id = "ddlNewLearnersCode" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewPrDPCode" for="ddlNewPrDPCode">
                            <%= Html.Resource("lblPrDPCode.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.DropDownList("AaRepNewInfPrDpCodeId", Model.PrDPCodeList, new { id = "ddlNewPrDPCode" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewHomeTel" for="txtNewHomeTel">
                            <%= Html.Resource("lblHomeTel.Text")%>
                        </label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfHomeTel", Model.Rep.AaRepNewInfHomeTel, new { id = "txtNewHomeTel" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewWorkTel" for="txtNewWorkTel">
                            <%= Html.Resource("lblWorkTel.Text")%>
                        </label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfWorkTel", Model.Rep.AaRepNewInfWorkTel, new { id = "txtNewWorkTel" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewFax" for="txtNewFax">
                            <%= Html.Resource("lblFax.Text")%>
                        </label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfFax", Model.Rep.AaRepNewInfFax, new { id = "txtNewFax" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewCell" for="txtNewCell">
                            <%= Html.Resource("lblCell.Text")%>
                        </label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfCell", Model.Rep.AaRepNewInfCell, new { id = "txtNewCell" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewEmail" for="txtNewEmail">
                            <%= Html.Resource("lblEmail.Text")%>
                        </label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfEmail", Model.Rep.AaRepNewInfEmail, new { id = "txtNewEmail" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewStreetAddress1" for="txtNewStreetAddress1">
                            <%= Html.Resource("lblStreetAddress1.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfStrAdd1", Model.Rep.AaRepNewInfStrAdd1, new { id = "txtNewStreetAddress1" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewStreetAddress2" for="txtNewStreetAddress">
                            &nbsp;</label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfStrAdd1",Model.Rep.AaRepNewInfStrAdd2, new { id = "txtNewStreetAddress2" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewPostalAddress1" for="txtNewPostalAddress1">
                            <%= Html.Resource("lblPostalAddress1.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfPosAdd1", Model.Rep.AaRepNewInfPosAdd1, new { id = "txtNewPostalAddress1" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewPostalAddress2" for="txtNewPostalAddress2">
                            &nbsp;</label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfPosAdd2", Model.Rep.AaRepNewInfPosAdd2, new { id = "txtNewPostalAddress2" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewPostCode" for="txtNewPostCode">
                            <%= Html.Resource("lblPostCode.Text")%>
                        </label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfPostalCode", Model.Rep.AaRepNewInfPostalCode, new { id = "txtNewPostCode" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewEmployerName" for="txtNewEmployerName">
                            <%= Html.Resource("lblEmployerName.Text")%>
                        </label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfEmployerName", Model.Rep.AaRepNewInfEmployerName, new { id = "txtNewEmployerName" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewEmployerAdd1" for="txtNewEmployerAdd1">
                            <%= Html.Resource("lblEmployerAdd1.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfEmployerAdd1", Model.Rep.AaRepNewInfEmployerAdd1, new { id = "txtNewEmployerAdd1" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNewEmployerAdd2" for="txtNewEmployerAdd2">
                            &nbsp;</label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNewInfPosAdd2", Model.Rep.AaRepNewInfEmployerAdd2, new { id = "txtNewEmployerAdd2" })%>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div style="padding: 50px 0px 50px 0px; margin: 0px auto 0px auto; width: 300px;">
        <%if (Model.PostCodeIsValid)
          { %>
        <button name="btnDecideSuccessful" onclick="return confirm('<%= Html.Resource("hintDecideConfrim.Text")%>');"
            type="submit" value="" class="NormalButton">
            <%= Html.Resource("btnDecideSuccessful.Text")%></button>
        <%} %>
        <button name="btnDecideUnSuccessful" onclick="return confirm('<%= Html.Resource("hintDecideConfrim.Text")%>');"
            type="submit" value="" class="NormalButton">
            <%= Html.Resource("btnDecideUnSuccessful.Text")%></button>
    </div>
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <link href='<% =Url.Content("~/Content/Css/ST_Odyssey.css")%>' rel="stylesheet" type="text/css" />
</asp:Content>
