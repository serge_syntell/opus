<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.InfringerOption.Model.DecisionModel>" %>

<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<%@ Import Namespace="SIL.AARTO.DAL.Entities" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        var imageUrlString = "<%= Model.DocumentImageUrlList %>";
        //$("#divCommon").attr("disabled", "disabled");
        $("#div08").attr("disabled", "disabled");
    </script>

    <script src='<% =Url.Content("~/Scripts/InfringerOption/AARTODecision.js")%>' type="text/javascript"></script>

   <div class="ContentHead" style=" text-align:center;">
        <%= Html.Resource("titleOfPage.Text")%></div>
    <% using (Html.BeginForm())
       { %>
    <div style="margin-left: auto; margin-right: auto; width: 800px;">
        <span class="NormalBold"><%= Html.Resource("lblDisplayForm.Text")%></span><input type="radio" name="DisplayOption" value="1" id="rdbDisplayForm" />
        <span class="NormalBold"><%= Html.Resource("lblDisplayImage.Text")%></span><input type="radio" name="DisplayOption" value="0" checked="checked" id="rdbDisplayImage" />
    </div>
    <div id="divPic" style="margin-left: auto; margin-right: auto; width: 800px;">
        <div style=" float:right">
            <a id="lkPre" class="page"><%= Html.Resource("btnPreviousPage.Text")%></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a id="lkNext" class="page"><%= Html.Resource("btnNextPage.Text")%></a></div>
        <img id="imgDocument" class="width_800" src="" alt="" />
    </div>
    <div id="divForm">
        <div id="divCommon" style="margin-left: auto; margin-right: auto; width: 800px;">
            <% Html.RenderPartial("CommonRepInfo"); %>
        </div>
        <div id="div08" style="margin-left: auto; margin-right: auto; width: 800px;">
            <fieldset>
                <legend>
                    <%=Html.Resource("RepresentationParticular.legend")%>
                </legend>
                <div style=" height:300px;">
                    <div class="left_title">
                        <label id="lblRepresentationDetail" for="txtRepresentationDetail">
                            <%= Html.Resource("lblRepresentationDetail.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextArea("AaRepDetails", Model.Rep.AaRepDetails, new { id = "txtRepresentationDetail" })%>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div style="padding:50px 0px 50px 0px; margin:0px auto 0px auto; width:800px; text-align:center;">
        <%if (Model.PostCodeIsValid)
          { %>
        <button name="btnDecideSuccessful" onclick="return confirm('<%= Html.Resource("hintDecideConfrim.Text")%>');"
            type="submit" value="" class="NormalButton">
            <%= Html.Resource("btnDecideSuccessful.Text")%></button>
        <%} %>
        <button name="btnDecideUnSuccessful" onclick="return confirm('<%= Html.Resource("hintDecideConfrim.Text")%>');"
            type="submit" value="" class="NormalButton">
            <%= Html.Resource("btnDecideUnSuccessful.Text")%></button>
        <button name="btnDecideAdviseElectCourt" onclick="return confirm('<%= Html.Resource("hintDecideConfrim.Text")%>');"
            type="submit" value="" class="NormalButton">
            <%= Html.Resource("btnDecideAdviseElectCourt.Text")%></button>
    </div>
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <link href='<% =Url.Content("~/Content/Css/ST_Odyssey.css")%>' rel="stylesheet" type="text/css" />
    <style type="text/css">
        #txtRepresentationDetail
        {
            width: 350px;
            height: 200px;
            overflow: auto;
        }
    </style>
</asp:Content>
