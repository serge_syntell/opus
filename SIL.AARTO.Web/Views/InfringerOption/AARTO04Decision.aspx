<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.InfringerOption.Model.DecisionModel>" %>

<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<%@ Import Namespace="SIL.AARTO.DAL.Entities" %>
<%@ Import Namespace="SIL.AARTO.BLL.InfringerOption" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        var imageUrlString = "<%= Model.DocumentImageUrlList %>";
        //$("#divCommon").attr("disabled", "disabled");
        $("#div04").attr("disabled", "disabled");
    </script>
    <script src='<% =Url.Content("~/Scripts/InfringerOption/AARTODecision.js")%>' type="text/javascript"></script>
    <div class="ContentHead" style=" text-align:center;">
        <%= Html.Resource("titleOfPage.Text")%></div>
    <% using (Html.BeginForm())
       { %>
    <div style="margin-left: auto; margin-right: auto; width: 800px;">
        <span class="NormalBold"><%= Html.Resource("lblDisplayForm.Text")%></span><input type="radio" name="DisplayOption" value="1" id="rdbDisplayForm" />
        <span class="NormalBold"><%= Html.Resource("lblDisplayImage.Text")%></span><input type="radio" name="DisplayOption" value="0" checked="checked" id="rdbDisplayImage" />
    </div>
    <div id="divPic" style="margin-left: auto; margin-right: auto; width: 800px;">
        <div style=" float:right;"><a id="lkPre" class="page"><%= Html.Resource("btnPreviousPage.Text")%></a>
        &nbsp;&nbsp;&nbsp;&nbsp;<a id="lkNext" class="page"><%= Html.Resource("btnNextPage.Text")%></a></div>
        <img id="imgDocument" class="width_800" src="" alt="" />
    </div>
    <div id="divForm">
        <div id="divCommon" style="margin-left: auto; margin-right: auto; width: 800px;">
            <% Html.RenderPartial("CommonRepInfo"); %>
        </div>
        <div id="div04" style="margin-left: auto; margin-right: auto; width: 800px;">
            <fieldset>
                <legend>
                    <%= Html.Resource("PaymentOptionsRequestedParticulars.legend")%></legend>
                <div style="height:50px;">
                    <div class="left_title NormalBold">
                        <label id="lblPenaltyAmount" for="txtPenaltyAmount">
                            <%= Html.Resource("lblPenaltyAmount.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input" style="height:30px; padding-top:20px">
                        <%= Html.TextBox("AaRepPenAmount",Model.Rep.AaRepPenAmount, new { id = "txtPenaltyAmount" })%>
                    </div>
                </div>
                <div style="height:60px;">
                    <div class="left_title NormalBold">
                        <label id="lblNumOfMonth" for="txtNumOfMonth">
                            <%= Html.Resource("lblNumOfMonth.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNumOfMonth", Model.Rep.AaRepNumOfMonth, new { id = "txtNumOfMonth" })%>
                    </div>
                </div>
                <div style=" text-align:left; height:80px; line-height:80px; padding-left:30px;">
                    <label id="Label1" for="chkMethodOfPaymentD">
                        <%= Html.Resource("lblMethodOfPaymentTitle.Text")%></label>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblMethodOfPaymentD" for="chkMethodOfPaymentD">
                            <%= Html.Resource("lblMethodOfPaymentD.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input_box">
                        <%= Html.RadioButton("AaRepMethodOfPayment", PaymentMethodType.BankDebitOrder, Model.Rep.AaRepMethodOfPayment == null ? true : Model.Rep.AaRepMethodOfPayment.ToLower() == PaymentMethodType.BankDebitOrder.ToLower(), new { id = "chkMethodOfPaymentD" })%>
                    </div>
                     <div class="right_input_box">
                        <%= Html.RadioButton("AaRepMethodOfPayment", PaymentMethodType.MonthlyDeposits, Model.Rep.AaRepMethodOfPayment == null ? true : Model.Rep.AaRepMethodOfPayment.ToLower() == PaymentMethodType.MonthlyDeposits.ToLower(), new { id = "chkMethodOfPaymentM" })%>
                    </div>
                </div>
                <%
                    if (Model.Rep.AaRepMethodOfPayment != null && Model.Rep.AaRepMethodOfPayment.ToLower().Equals("d"))
                    {
                %>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNameOfBank" for="txtNameOfBank">
                            <%= Html.Resource("lblNameOfBank.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNameOfBank", Model.Rep.AaRepNameOfBank, new { id = "txtNameOfBank" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNameOfBranch" for="txtNameOfBranch">
                            <%= Html.Resource("lblNameOfBranch.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNameOfBranch", Model.Rep.AaRepNameOfBranch, new { id = "txtNameOfBranch" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblNameOfAccountHolder" for="txtNameOfAccountHolder">
                            <%= Html.Resource("lblNameOfAccountHolder.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepNameOfAccountHolder", Model.Rep.AaRepNameOfAccountHolder, new { id = "txtNameOfAccountHolder" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblBankAccountNum" for="txtBankAccountNum">
                            <%= Html.Resource("lblBankAccountNum.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepBankAccountNum", Model.Rep.AaRepBankAccountNum, new { id = "txtBankAccountNum" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblBranchCode" for="txtBranchCode">
                            <%= Html.Resource("lblBranchCode.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.TextBox("AaRepBranchCode", Model.Rep.AaRepBranchCode, new { id = "txtBranchCode" })%>
                    </div>
                </div>
                <div class="div_h">
                    <div class="left_title NormalBold">
                        <label id="lblTypeOfAccount" for="ddlTypeOfAccount">
                            <%= Html.Resource("lblTypeOfAccount.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <%= Html.DropDownList("AaRepTypeOfAccount", Model.TypeOfAccountList, new { id = "ddlTypeOfAccount" })%>
                    </div>
                </div>
                <br />
                <br />
                <br />
                <br />
                <%
                    }
                %>
            </fieldset>
        </div>
    </div>
   <div style="padding:50px 0px 50px 0px; margin:0px auto 0px auto; width:300px;">
        <%if (Model.PostCodeIsValid)
          { %>
        <button name="btnDecideSuccessful" onclick="return confirm('<%= Html.Resource("hintDecideConfrim.Text")%>');"
            type="submit" value="" class="NormalButton">
            <%= Html.Resource("btnDecideSuccessful.Text")%></button>
        <%} %>
        <button name="btnDecideUnSuccessful" onclick="return confirm('<%= Html.Resource("hintDecideConfrim.Text")%>');"
            type="submit" value="" class="NormalButton">
            <%= Html.Resource("btnDecideUnSuccessful.Text")%></button>
    </div>
    <%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <link href='<% =Url.Content("~/Content/Css/ST_Odyssey.css")%>' rel="stylesheet" type="text/css" />
</asp:Content>
