<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.InfringerOption.Model.DecisionModel>" %>

<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<%@ Import Namespace="SIL.AARTO.BLL.InfringerOption" %>
<%@ Import Namespace="SIL.AARTO.DAL.Entities" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        var imageUrlString = "<%= Model.DocumentImageUrlList %>";
        //$("#divCommon").attr("disabled", "disabled");
        $("#div14").attr("disabled", "disabled");
    </script>

    <script src='<% =Url.Content("~/Scripts/InfringerOption/AARTODecision.js")%>' type="text/javascript"></script>

    <% using (Html.BeginForm())
       { if (Model.RepID == -1){ %>
        <div class="ContentHead"><%= Html.Resource("lblNoInfringerOptionsForDecide.Text")%></div>
        <%}else
        { %>
    <div class="ContentHead" style="text-align: center;">
        <%= Html.Resource("titleOfPage.Text")%></div>
    <div style="margin:0px auto; width: 300px; height: 300px; text-align:center; font-weight:bold;">
        <span>
            <%= Html.Resource("lblEvidencePack.Text")%></span><br /><br />
        <%=Html.TextArea("EvidencePack", Model.EvidencePack, new { id = "txtEvidencePack", style = "width:300px;height:200px;" })%>
    </div>
    <div style="margin-left: auto; margin-right: auto; width: 800px;">
        <span class="NormalBold">
            <%= Html.Resource("lblDisplayForm.Text")%></span><input type="radio" name="DisplayOption"
                value="1" id="rdbDisplayForm" />
        <span class="NormalBold">
            <%= Html.Resource("lblDisplayImage.Text")%></span><input type="radio" name="DisplayOption"
                value="0" checked="checked" id="rdbDisplayImage" />
    </div>
    <div id="divPic" style="margin-left: auto; margin-right: auto; width: 800px;">
        <div style="float: right">
            <a id="lkPre" class="page">
                <%= Html.Resource("btnPreviousPage.Text")%></a>&nbsp;&nbsp;&nbsp;&nbsp; <a id="lkNext"
                    class="page">
                    <%= Html.Resource("btnNextPage.Text")%></a></div>
        <img id="imgDocument" class="width_800" src="" alt="" />
    </div>
    <div id="divForm">
        <div id="divCommon" style="margin-left: auto; margin-right: auto; width: 800px;">
            <% Html.RenderPartial("CommonRepInfo"); %>
        </div>
        <div id="div14" style="margin-left: auto; margin-right: auto; width: 800px;">
            <fieldset>
                <legend>
                    <%=Html.Resource("Reasons.legend")%>
                </legend>
                <div>
                    <div class="left_title NormalBold">
                        <%= Html.CheckBox("chkReasonsOfA", Model.RVReasons[0], new { id = "chkReasonsOfA", value = 1 })%>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <label id="lblReasonsOfA" for="chkReasonsOfA">
                            <%= Html.Resource("lblReasonsOfA.Text")%></label>
                    </div>
                </div>
                <div class="clear" style=" padding-bottom:8px;"></div>
                <div>
                    <div class="left_title NormalBold">
                        <%= Html.CheckBox("chkReasonsOfB", Model.RVReasons[1], new { id = "chkReasonsOfB", value = 1 })%>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <label id="lblReasonsOfB" for="chkReasonsOfB">
                            <%= Html.Resource("lblReasonsOfB.Text")%></label>
                    </div>
                </div>
                <div class="clear" style=" padding-bottom:8px;"></div>
                <div>
                    <div class="left_title NormalBold">
                        <%= Html.CheckBox("chkReasonsOfC", Model.RVReasons[2], new { id = "chkReasonsOfC", value = 1 })%>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <label id="lblReasonsOfC" for="chkReasonsOfC">
                            <%= Html.Resource("lblReasonsOfC.Text")%></label>
                    </div>
                </div>
                <div class="clear" style=" padding-bottom:8px;"></div>
                <div>
                    <div class="left_title NormalBold">
                        <%= Html.CheckBox("chkReasonsOfD", Model.RVReasons[3], new { id = "chkReasonsOfD", value = 1 })%>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <label id="lblReasonsOfD" for="chkReasonsOfD">
                            <%= Html.Resource("lblReasonsOfD.Text")%></label>
                    </div>
                </div>
                <div class="clear" style=" padding-bottom:8px;"></div>
                <div>
                    <div class="left_title NormalBold">
                        <%= Html.CheckBox("chkReasonsOfE", Model.RVReasons[4], new { id = "chkReasonsOfE", value = 1 })%>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <label id="lblReasonsOfE" for="chkReasonsOfE">
                            <%= Html.Resource("lblReasonsOfE.Text")%></label>
                    </div>
                </div>
                <div class="clear" style=" padding-bottom:8px;"></div>
                <div>
                    <div class="left_title NormalBold">
                        <%= Html.CheckBox("chkReasonsOfF", Model.RVReasons[5], new { id = "chkReasonsOfF", value = 1 })%>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <label id="lblReasonsOfF" for="chkReasonsOfF">
                            <%= Html.Resource("lblReasonsOfF.Text")%></label>
                    </div>
                </div>
                <div class="clear" style=" padding-bottom:8px;"></div>
                <div >
                    <div class="left_title NormalBold">
                       <label id="lblCourt" for="ddlCourt">
                            <%= Html.Resource("lblCourt.Text")%></label>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input"> <%= Html.DropDownList("CrtIntNo", Model.CourtList, new { id = "ddlCourt" })%>
                        
                    </div>
                </div>
                <div class="clear" style=" padding-bottom:15px;"></div>
                <div>
                    <div class="left_title NormalBold">
                       &nbsp;
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                     <%= Html.TextBox("AaEnfOrderRevAppDate", Model.Rep.AaEnfOrderRevAppDate, new { id = "txtCourtDate" })%>
                        <label id="lblCourtDate" for="txtCourtDate">
                            <%= Html.Resource("lblCourtDate.Text")%></label></div>
                </div>
                <div class="clear" style=" padding-bottom:8px;"></div>
                <div >
                    <div class="left_title NormalBold">
                        <%= Html.CheckBox("chkReasonsOfG", Model.RVReasons[6], new { id = "chkReasonsOfG", value = 1 })%>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <label id="lblReasonsOfG" for="chkReasonsOfG">
                            <%= Html.Resource("lblReasonsOfG.Text")%></label>
                    </div>
                </div>
                <div class="clear" style=" padding-bottom:8px;"></div>
                <div>
                    <div class="left_title NormalBold">
                        <%= Html.CheckBox("chkReasonsOfH", Model.RVReasons[7], new { id = "chkReasonsOfH", value = 1 })%>
                    </div>
                    <div class="star">
                        &nbsp;</div>
                    <div class="right_input">
                        <label id="lblReasonsOfH" for="chkReasonsOfH">
                            <%= Html.Resource("lblReasonsOfH.Text")%></label>
                    </div>
                </div>
                <div class="clear" style=" padding-bottom:30px;"></div>
            </fieldset>
        </div>
    </div>
    <div style="padding: 50px 0px 50px 0px; margin: 0px auto 0px auto; width: 400px;">
        <button name="btnDecideSuccessful" onclick="return confirm('<%= Html.Resource("hintDecideConfrim.Text")%>');"
            type="submit" value="" class="NormalButton">
            <%= Html.Resource("btnDecideSuccessful.Text")%></button>
        <%if (Model.AaClockTypeID == (int)AartoClockTypeList.EnforcementOrder)
          { %>
        <button name="btnDecideUnSuccessful" onclick="return confirm('<%= Html.Resource("hintDecideConfrim.Text")%>');"
            type="submit" value="" visible="true" class="NormalButton">
            <%= Html.Resource("btnDecideUnSuccessful.Text")%></button>
        <%} %>
    </div>
        <%}
    }%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <link href='<% =Url.Content("~/Content/Css/ST_Odyssey.css")%>' rel="stylesheet" type="text/css" />
</asp:Content>
