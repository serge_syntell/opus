<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SIL.AARTO.BLL.InfringerOption.Model.DecisionModel>" %>
<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<%= Html.Hidden("RepID", Model.RepID, new { id = "hidAaRepId" })%>
<%= Html.Hidden("A14RepID", Model.A14RepID, new { id = "hidA14RepID" })%>
<%= Html.Hidden("AaDocTypeID", Model.AaDocTypeID, new { id = "hidAaDocTypeID" }) %>
<%= Html.Hidden("AutIntNo", Model.AutIntNo, new { id = "hidAutIntNo" }) %>
<%= Html.Hidden("NotIntNo", Model.NotIntNo, new { id = "hidNotIntNo" }) %>
<%= Html.Hidden("ChgIntNo", Model.ChgIntNo, new { id = "hidChgIntNo" }) %>
<fieldset>
    <legend>
        <%= Html.Resource("InfringerParticulars.legend")%></legend>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblOrganisationName" for="txtOrganisationName">
                <%= Html.Resource("lblOrganisationName.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepOrganisationName", Model.Rep.AaRepOrganisationName, new { id = "txtOrganisationName" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblRegistrationNumber" for="txtRegistrationNumber">
                <%= Html.Resource("lblRegistrationNumber.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepRegistrationNo", Model.Rep.AaRepRegistrationNo, new { id = "txtRegistrationNumber" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblOrganisationType" for="ddlOrganisationType">
                <%= Html.Resource("lblOrganisationType.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input_wide">
            <%= Html.DropDownList("AaRepOrgTypeId",Model.OrganisationTypeList, new { id = "ddlOrganisationType" })%>
            <%= Html.TextBox("AaRepOtherOrgType", Model.Rep.AaRepOtherOrgType, new { id = "txtOtherOrgType" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblSurname" for="txtSurname">
                <%= Html.Resource("lblSurname.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfSurname", Model.Rep.AaRepInfSurname, new { id = "txtSurname" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblFirstNames" for="txtFirstNames">
                <%= Html.Resource("lblFirstNames.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfFirstName", Model.Rep.AaRepInfFirstName, new { id = "txtFirstNames" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblInitials" for="txtInitials">
                <%= Html.Resource("lblInitials.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfInitials", Model.Rep.AaRepInfInitials, new { id = "txtInitials" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblGender" for="rdbGender">
                <%= Html.Resource("lblGender.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input_box">
            <%= Html.RadioButton("AaRepInfGender", "M", Model.Rep.AaRepInfGender == "M", new { id = "rdbGender"})%><%= Html.Resource("lblMale.Text")%>    
        </div>
        <div class="right_input_box">   
             <%= Html.RadioButton("AaRepInfGender", "F", Model.Rep.AaRepInfGender == "F", new { id = "rdbGender"})%><%= Html.Resource("lblFemale.Text")%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblDOB" for="txtDOB">
                <%= Html.Resource("lblDOB.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfDob", Model.Rep.AaRepInfDob, new { id = "txtDOB" })%>
            <%= Html.Resource("lblDateFormat.Text")%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblIDType" for="txtIDType">
                <%= Html.Resource("lblIDType.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.DropDownList("AaRepInfIdTypeId", Model.IDTypeList, new { id = "ddlIDType" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblIdNo" for="txtIdNo">
                <%= Html.Resource("lblIdNo.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfIdNo", Model.Rep.AaRepInfIdNo, new { id = "txtIdNo" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblCountryOfIssue" for="txtCountryOfIssue">
                <%= Html.Resource("lblCountryOfIssue.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfDob", Model.Rep.AaRepInfIdIssueCountryName, new { id = "txtCountryOfIssue" })%>
            
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblLicenceCode" for="ddlLicenceCode">
                <%= Html.Resource("lblLicenceCode.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input_wide">
            <%= Html.DropDownList("AaRepInfLiCodeId", Model.LicenceCodeList, new { id = "ddlLicenceCode" })%>
            <%= Html.TextBox("AaRepInfForeignCode", Model.Rep.AaRepInfForeignCode, new { id = "txtForeignCode" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblLearnersCode" for="ddlLearnersCode">
                <%= Html.Resource("lblLearnersCode.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.DropDownList("AaRepInfLeCodeId",Model.LearnersCodeList, new { id = "ddlLearnersCode" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblPrDPCode" for="ddlPrDPCode">
                <%= Html.Resource("lblPrDPCode.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.DropDownList("AaRepInfPrDpCodeId", Model.PrDPCodeList, new { id = "ddlPrDPCode" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblStreetAddress1" for="txtStreetAddress1">
                <%= Html.Resource("lblStreetAddress1.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input NormalBold">
            <%= Html.TextBox("AaRepInfStrAdd1", Model.Rep.AaRepInfStrAdd1, new { id = "txtStreetAddress1" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblStreetAddress2" for="txtStreetAddress">
                &nbsp;</label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfStrAdd1",Model.Rep.AaRepInfStrAdd2, new { id = "txtStreetAddress2" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblStreetAddress3" for="txtStreetAddress3">
                &nbsp;</label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfStrAdd1", Model.Rep.AaRepInfStrAdd3, new { id = "txtStreetAddress3" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblPostalAddress1" for="txtPostalAddress1">
                <%= Html.Resource("lblPostalAddress1.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfPosAdd1", Model.Rep.AaRepInfPosAdd1, new { id = "txtPostalAddress1" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblPostalAddress2" for="txtPostalAddress2">
                &nbsp;</label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfPosAdd2", Model.Rep.AaRepInfPosAdd2, new { id = "txtPostalAddress2" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblPostalAddress3" for="txtPostalAddress3">
                &nbsp;</label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfPosAdd3", Model.Rep.AaRepInfPosAdd3, new { id = "txtPostalAddress3" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblPostCode" for="txtPostCode">
                <%= Html.Resource("lblPostCode.Text")%>
            </label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfPostalCode", Model.Rep.AaRepInfPostalCode, new { id = "txtPostCode" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblHomeTel" for="txtHomeTel">
                <%= Html.Resource("lblHomeTel.Text")%>
            </label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfHomeTel", Model.Rep.AaRepInfHomeTel, new { id = "txtHomeTel" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblWorkTel" for="txtWorkTel">
                <%= Html.Resource("lblWorkTel.Text")%>
            </label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfWorkTel", Model.Rep.AaRepInfWorkTel, new { id = "txtWorkTel" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblFax" for="txtFax">
                <%= Html.Resource("lblFax.Text")%>
            </label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfFax", Model.Rep.AaRepInfFax, new { id = "txtFax" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblCell" for="txtCell">
                <%= Html.Resource("lblCell.Text")%>
            </label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfCell", Model.Rep.AaRepInfCell, new { id = "txtCell" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblEmail" for="txtEmail">
                <%= Html.Resource("lblEmail.Text")%>
            </label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfEmail", Model.Rep.AaRepInfEmail, new { id = "txtEmail" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblEmployerName" for="txtEmployerName">
                <%= Html.Resource("lblEmployerName.Text")%>
            </label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfEmployerName", Model.Rep.AaRepInfEmployerName, new { id = "txtEmployerName" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblEmployerAdd1" for="txtEmployerAdd1">
                <%= Html.Resource("lblEmployerAdd1.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfEmployerAdd1", Model.Rep.AaRepInfEmployerAdd1, new { id = "txtEmployerAdd1" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblEmployerAdd2" for="txtEmployerAdd2">
                &nbsp;</label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepInfPosAdd2", Model.Rep.AaRepInfEmployerAdd2, new { id = "txtEmployerAdd2" })%>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>
        <%= Html.Resource("MotorVehicleParticulars.legend")%></legend>
    <div class="div_h" style=" height:40px;">
    
        <div class="left_title NormalBold">
            <label id="lblVehLicence" for="txtVehLicence">
                <%= Html.Resource("lblVehLicence.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepVehLicence", Model.Rep.AaRepVehLicence, new { id = "txtVehLicence" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblVehLicenceDiscNo" for="txtVehLicenceDiscNo">
                <%= Html.Resource("lblVehLicenceDiscNo.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepVehLicenceDiscNo", Model.Rep.AaRepVehLicenceDiscNo, new { id = "txtVehLicenceDiscNo" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblOperatorCardNo" for="txtOperatorCardNo">
                <%= Html.Resource("lblOperatorCardNo.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepOperatorCardNo", Model.Rep.AaRepOperatorCardNo, new { id = "txtOperatorCardNo" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblVehicleType" for="ddlVehicleType">
                <%= Html.Resource("lblVehicleType.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input_wide">
            <%= Html.DropDownList("VtIntNo", Model.VehicleTypeList, new { id = "ddlVehTypeDescription" })%>
            <%= Html.TextBox("AaRepVehTypeDescription", Model.Rep.AaRepVehTypeDescription, new { id = "txtVehTypeDescription" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblVeGvm" for="txtVeGvm">
                <%= Html.Resource("lblVeGvm.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepVeGvm", Model.Rep.AaRepVeGvm, new { id = "txtVeGvm" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblVehicleMake" for="ddlVehicleMake">
                <%= Html.Resource("lblVehicleMake.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input_wide">
            <%= Html.DropDownList("VmIntNo", Model.VehicleMakeList, new { id = "ddlVehicleMake" })%>
            <%= Html.TextBox("AaRepVehMakeDescription", Model.Rep.AaRepVehMakeDescription, new { id = "txtVehMakeDescription" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblVehSeriesModel" for="txtVehSeriesModel">
                <%= Html.Resource("lblVehSeriesModel.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("AaRepVehLicence", Model.Rep.AaRepVehSeriesModel, new { id = "txtVehSeriesModel" })%>
        </div>
    </div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblVehicleColour" for="ddlVehicleColour">
                <%= Html.Resource("lblVehicleColour.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input_wide">
            <%= Html.DropDownList("VcIntNo", Model.VehicleColourList, new { id = "ddlVehicleColour" })%>
            <%= Html.TextBox("AaRepVehColour", Model.Rep.AaRepVehColour, new { id = "txtVehColour" })%>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>
        <%= Html.Resource("InfringementNotice.legend")%></legend>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblNoticeTicketNumber" for="txtNoticeTicketNumber">
                <%= Html.Resource("lblNoticeTicketNumber.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("NoticeTicketNumber", Model.NoticeTicketNumber, new { id = "txtNoticeTicketNumber" })%>
        </div>
    </div>

    <div style="height:45px;">
        <div class="left_title NormalBold">
            <label id="lblInfringementDate" for="txtInfringementDate">
                <%= Html.Resource("lblInfringementDate.Text")%></label>
        </div>
        <div class="star">&nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("InfringementDate", Model.InfringementDate, new { id = "txtInfringementDate" })%>
            <%= Html.Resource("lblDateFormat.Text")%>
        </div>
    </div>
     <div style=" clear:both;"></div>
    <div class="div_h">
        <div class="left_title NormalBold">
            <label id="lblIssuingAuthority" for="txtIssuingAuthority">
                <%= Html.Resource("lblIssuingAuthority.Text")%></label>
        </div>
        <div class="star">
            &nbsp;</div>
        <div class="right_input">
            <%= Html.TextBox("IssuingAuthority", Model.IssuingAuthority, new { id = "txtIssuingAuthority" })%>
        </div>
    </div>
</fieldset>
