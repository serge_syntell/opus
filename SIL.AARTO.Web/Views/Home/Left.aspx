<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<%@ Import Namespace="SIL.AARTO.BLL.Utility" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head >
    <title>Left</title>
     <script type="text/javascript" src='<% =Url.Content("~/RootPathHandler.ashx") %>'></script>
    <link href='<%=Url.Content("~/Content/Css/UserMenu/UserMenu.css")%>' rel="stylesheet" type="text/css" />
    <script src='<%=Url.Content("~/Scripts/Library/jquery-1.4.2.min.js")%>' type="text/javascript"></script>
    <script src='<%=Url.Content("~/Scripts/Library/jquery.treeview.js")%>' type="text/javascript"></script>
    <%--<script src='<%=Url.Content("~/Scripts/Library/jquery.tooltip.js") %>' type="text/javascript"></script>--%>
    <script src='<%=Url.Content("~/Scripts/Home/UserMenu.js")%>' type="text/javascript"></script>
   <%-- <script src='<%=Url.Content("~/Scripts/Tooltip/Tooltip.js")%>' type="text/javascript"></script>--%>
</head>
<style type="text/css">
body,div
{
	scrollbar-face-color: #444F7B; 
	scrollbar-shadow-color: #444F7B; 
	scrollbar-highlight-color: #444F7B; 
	scrollbar-3dlight-color: #6977B0; 
	scrollbar-darkshadow-color: #000; 
	scrollbar-track-color: #444F7B; 
	scrollbar-arrow-color: #6977B0;
	
	}
</style>
<body>
    
    <input type="hidden" id="hidNavigationMenu" />
    <div class="left_frame">
        <div class="left_frame_footlogo">
            <br />
            <div id="menu_content">
                <% 
                    if (Session["UserLoginInfo"] != null)
                    {
                        UserLoginInfo userLoginInfo = (UserLoginInfo)Session["UserLoginInfo"];
                        string lsCode = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
                      
                %>
                        <% = Html.NavigationUserMenu(new SIL.AARTO.BLL.Utility.UserMenuManager().GetUserMenuCollectionUserRolesFromDB(userLoginInfo.UserRoles, lsCode, 4), new object[] { "contentFrame" })%>
                   
                   <% }
                %>
               
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".menu_child_bg").each(function() {
                $(this).children("ul:first").treeview({
                    collapsed: true,
                    animated: "medium",
                    control: "#sidetreecontrol",
                    persist: "location"
                });
            });
        });

        function getUrl(obj, page) {
            var url = _ROOTPATH + page;
            $(obj).attr('href', url);
        }   
 
  </script>
</body>
</html>
