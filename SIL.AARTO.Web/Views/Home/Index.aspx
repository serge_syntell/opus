<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="System.Web.Mvc" %>
<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<%@ Import Namespace="SIL.AARTO.BLL.Utility" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head >
    <title>Opus-AARTO</title>
    <link rel="shortcut icon"  href='<%=Url.Content("~/Content/Image/Icon/favicon.ico")%>' />
    <script type="text/javascript" src='<% =Url.Content("~/RootPathHandler.ashx") %>'></script>
</head>

<% 
    if (Session["UserLoginInfo"] == null)
    {
%>

<script language="javascript">
    window.top.location.href = _ROOTPATH + "/Account/Login";</script>
<% 
    }
%>

<frameset name="Frame" id="frame" cols="*" rows="109,*" border="0">
    <frame  src="top" id="topFrame" name="topFrame" border="0"  noresize  scrolling="auto"/>
    <frameset name ="mainFrame" rows="*" cols="190,8,*" border="0">
        <frame  src="Left" id="leftFrame" name ="leftFrame" border="0" noresize scrolling="auto"/>
        <frame src="Middle" id="moddleFrame" name ="moddleFrame" border="0" noresize  scrolling="no"/>
        <%if (ViewData["ReturnUrl"] != null) %>
        <%{ %>
        <% =Html.JavascriptTag("var returnUrl =" +ViewData["ReturnUrl"].ToString() +";" )%>
        <frame src='<% =ViewData["ReturnUrl"].ToString()%>' id="contentFrame" name="contentFrame" border="0"  noresize  scrolling="auto"/>
        <%} %>
        <% else %>
        <%{ %>
        <frame src="Main" id="contentFrame" name="contentFrame" border="0"  noresize  scrolling="auto"/>
        <%} %>
    </frameset>
</frameset>
 
</html>
