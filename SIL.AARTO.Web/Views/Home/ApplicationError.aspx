<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Application Error</title>
    <link href='<%=Url.Content("~/Content/Css/Error.css")%>' rel="stylesheet" type="text/css" />
</head>
<body bottommargin="0" leftmargin="0" background="backgrounds/bg-white.JPG" topmargin="0"
    rightmargin="0">
    <form name="Form1" method="post" action="Error.aspx?aspxerrorpath=%2fTMS%2fRoadType.aspx"
    id="Form1">
    <div>
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUIMjQxNDE1MTkPZBYCZg9kFgwCAw8PFgIeBFRleHQFFkFuIGVycm9yIGhhcyBvY2N1cnJlZCFkZAIHDw8WAh8ABSFVbmtub3duIHNlcnZlciB0YWcgJ2hkcjE6SGVhZGVyJy5kZAIJDw8WAh4HVmlzaWJsZWhkZAILDw8WAh8BaGRkAg0PDxYCHwFoZGQCDw8PFgIeC05hdmlnYXRlVXJsBSFodHRwOi8vbG9jYWxob3N0OjQ5NTg2L0hvbWUvSW5kZXhkZGS3zrxXem94eg0evLW8A/7gHcaOqg==" />
    </div>
    <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
            </td>
        </tr>
    </table>
    <div class="errorDiv">
        <div class="floatLeft">
            <img src='<%=Url.Content("~/Content/Image/Icon/ErrorPage.gif") %>' />
        </div>
        <div class="floatLeft infoBg">
            <table border="0" width="100%">
                <tr>
                    <td>
                        <span id="lblPageName" class="ContentHead">
                            <% =Html.Resource("ErrorMessage.Text") %></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--<span id="Label2"><% =Html.Resource("lblError.Text") %></span>&nbsp;--%><span id="lblError" class="NormalRed">
                            <% if (ViewData["ErrorMessage"] != null)%>
                            <%{ %>
                            <% =ViewData["ErrorMessage"].ToString()%>
                            <%} %>
                            <% else if (Application["ErrorMessage"] != null) %>
                            <%{ %>
                            <% =Application["ErrorMessage"].ToString()%>
                            <%} %>
                            <% Application["ErrorMessage"] = null; %>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td valign="middle">
                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <a id="linkToHome" href='<% =Url.Content("~/Home/Index") %>'  target="_blank"><% =Html.Resource("HomeLink.Text")%></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
<%--<body>
    <div class="errorDiv">
        <div class="floatLeft">
            <img src='<%=Url.Content("~/Content/Image/Icon/ErrorPage.gif") %>' /></div>
        <div class="floatLeft infoBg">
            <table>
                <tr>
                    <td align="left">
                        <% =Html.Resource("ErrorMessage.Text") %>
                        <% if (ViewData["ErrorMessage"] != null)%>
                        <%{ %>
                        <p>
                            <% =ViewData["ErrorMessage"].ToString()%>
                        </p>
                        <%} %>
                        <% else if (Application["ErrorMessage"] != null) %>
                        <%{ %>
                        <p>
                            <% =Application["ErrorMessage"].ToString()%>
                        </p>
                        <%} %>
                        <p>rrrrrrrrrrrrrrrrrrrrrr</p>
                        <% Application["ErrorMessage"] = null; %>
                        <a  href='<% =Url.Content("~/Home/Index") %>'  target="_blank"><% =Html.Resource("HomeLink.Text")%></a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>--%>