<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head >
    <title>Moddle</title>
    <link href='<%=Url.Content("~/Content/Css/UserMenu/UserMenu.css")%>' rel="stylesheet" type="text/css" />
    <script src='<%=Url.Content("~/Scripts/Home/UserMenu.js")%>' type="text/javascript"></script>

</head>
<body>
    <div class="slidersbackground">
        <div class="sliders" id="middleDiv">
            <div class="img">
                <img id="showframe" onclick="oa_tool()" src='<%=Url.Content("~/Content/Image/Menu/Menu_frame_arrow_close.gif")%>' /></div>
        </div>
    </div>
</body>
</html>
