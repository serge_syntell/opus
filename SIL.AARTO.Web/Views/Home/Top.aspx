<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="SIL.AARTO.BLL.Utility" %>
<%@ Import Namespace="SIL.AARTO.BLL.Admin" %>
<%@ import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<%@ Import Namespace="SIL.AARTO.DAL.Entities" %>

<link href='<%=Url.Content("~/Content/Css/ST_Odyssey.css")%>' rel="stylesheet" type="text/css" />
<script src='<%=Url.Content("~/Scripts/Library/jquery-1.4.2.min.js")%>' type="text/javascript"></script>
<script src='<%=Url.Content("~/Scripts/Home/Top.js")%>' type="text/javascript"></script>
<script type="text/javascript" src='<% =Url.Content("~/RootPathHandler.ashx") %>'></script>
<script type="text/javascript" src='<% =Url.Content("~/Scripts/jquery.cookie.js")%>'></script>
<script type="text/javascript" src='<% =Url.Content("~/Scripts/json.js")%>'></script>
<style>
    body
    {
        background-color: #6977B0;
    }
</style>
<% using (Html.BeginForm("Top", "Home", FormMethod.Post, new { id = "formTop" })) %>
<% {%>
        <% if (ViewData["SignOut"] != null && (bool)ViewData["SignOut"] == true)%>
            <%{ %>
            <script type="text/javascript">
                window.parent.location.href = _ROOTPATH+"/Account/Login";
            </script>
            <% }%>    
<table cellspacing="0" cellpadding="0" width="100%" border="0" class="HomeHead">
    <tr>
        <td valign="middle" align="left" colspan="2">
            <table>
                <tr>
                    <td>
                        <img src='<%=Url.Content("~/Content/Image/Home/Opus_logo.png")%>' /><br />
                        <input type="button" onclick="goHome();" value="<% =Html.Resource("lblHome.Text") %>" name="btnLogout"  class="logout"/>
                        <input type="button"  value="<% =Html.Resource("lblEnquiries.Text") %>" id="btnEnquiry" name="btnEnquiry" class ="logout" />                        
                        <input type="submit" value="<% =Html.Resource("lblLogout.Text") %>" name="btnLogout"  class="logout"/>
                        <% SelectList autSelectList = ViewData["AutList"] as SelectList;  %>
                        <span class="logout" >&nbsp;&nbsp;<% =Html.Resource("lblCurrentAuthority.Text")%>&nbsp;&nbsp;</span><% =Html.DropDownList("Authority", autSelectList, new { id="ddlAuthority"})%>
                        <span class="logout" >Version Number: <% =ViewData["VersionNumber"]%></span>
                    </td>
                </tr>
            </table>
        </td>
        <td align="right" class="HomeHead" valign="top">
            <label id="lblEnvironment">
            <%
                var environment = string.Empty;
                if  (ViewData["EnvironmentType"]!=null)
                {
                    if(ViewData["EnvironmentType"].ToString().Equals("D"))
                    {
                        environment = "Development Environment";
                    }
                }
                 %>
                 <%   =environment%>
            </label>
            <table width="100%">
                <tr>
                    <td align="right">
                        <%--The Contravention Solution --%>
                        <br/>
                        <% if (Session["UserLoginInfo"] != null) %>
                        <%{ %>
                        <%    UserLoginInfo userLoginInfo = (UserLoginInfo)Session["UserLoginInfo"];%>
                        <% UserManager manager = new UserManager(); %>
                        <% AuthorityEntity aut = manager.GetAuthorityByAutIntNo(userLoginInfo.AuthIntNo); %>
                        <% var user = string.Empty; %>
                        <% if (aut != null) user = aut.AutName; %>
                        <% user += GetLocalResourceObject("loginAs") +" "+ userLoginInfo.UserName; %> 
                        <% = user %>
                        <%} %>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <span><%=Html.Resource("spanLanguage") %>&nbsp;:</span>
                        <script type="text/javascript">

                            var userLanguageCookieName = "AARTOUserLanguage";

                            var supportedLanguages = JSON.parse('<%= ViewBag.Languages %>');
                            var userLanguage = '<%= ViewBag.CurrentLang %>';

                            var cookieOption = { path: "/" };
                            document.write("<select id=\"selectLanguages\" onchange=\"onLanguageChanged()\">");
                            $.each(supportedLanguages, function(key, value) {
                                if (key == userLanguage)
                                    document.write("<option selected=\"selected\" value=\"" + key + "\">" + value + "</option>");
                                else
                                    document.write("<option value=\"" + key + "\">" + value + "</option>");
                            });
                            document.write("</select>");

                            function onLanguageChanged() {
                                var ddl = document.getElementById("selectLanguages");
                                $(function () {
                                    $.ajax({
                                        url: "<%=Url.Action("ChangeLanguage","Home") %>",
                                        data: { lsCode: ddl.value },
                                        type: "POST",
                                        success: function (data) {
                                            if (data.result == -2) {
                                                goHome();
                                            }
                                            $.cookie(userLanguageCookieName, ddl.options[ddl.selectedIndex].value, cookieOption);
                                            goHome();
                                        }
                                    });
                                });
                            }
                        </script>
                    </td>
                </tr>
            </table>
            
        </td>
    </tr>
</table>
<% =Html.JavascriptTag(String.Format("var JsSelectAuthority ='{0}';", Html.Resource("JsSelectAuthority")))%>
<% =Html.JavascriptTag(String.Format("var JsSetAuthorityFailure ='{0}';", Html.Resource("JsSetAuthorityFailure")))%>
<%} %>

<script language="javascript" type="text/javascript">

    // LMZ 18-06-2007 added to close child and parent windows
    function goHome() {
        window.parent.location.href = _ROOTPATH+"/Home/Index";
    }
    function closeWindow() {
        //window.navigate('Login.aspx?Login=logout');
        window.parent.close();
        window.close();
    }

 
</script>

