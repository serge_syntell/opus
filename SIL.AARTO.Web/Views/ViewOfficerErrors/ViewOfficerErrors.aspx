<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.OfficerError.Model.ViewOfficerErrorsModel>" %>
<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<%@ Import Namespace="SIL.AARTO.BLL.Utility" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("ViewOfficerErrors", "ViewOfficerErrors", FormMethod.Post, new { id = "formViewOfficerErrors" }))
      {  %>
         <table cellspacing="0" cellpadding="4" align=center>
     <tr>
     <td class="ContentHead"><%=Html.Resource("PageName.Text")%></td></tr></table>
        <table cellspacing="0" align=center>
    <tr>
        <td><%= Html.Resource("DocumentType.Text")%></td>
        <td><%= Html.DropDownList("AaDocTypeID", Model.DocTypeList, new { id = "ddlDocType" }) %></td>
    </tr>   
    <tr>
        <td><%= Html.Resource("DateStart.Text")%></td>
        <td><input type="text" name="DateStart" id="txtDateStart" value="<%= Model.DateStart %>" /><%= Html.ValidationLocalizationError("DateStart")%></td>
    </tr>
    <tr>
        <td><%= Html.Resource("DateEnd.Text")%></td>
        <td><input type="text" name="DateEnd" id="txtDateEnd" value="<%= Model.DateEnd %>" /><%= Html.ValidationLocalizationError("DateEnd")%></td>
    </tr>
    <tr>
        <td><%= Html.Resource("Officer.Text")%></td>
        <td><%= Html.DropDownList("OfficerNo", Model.OfficerList, new { id = "ddlOfficer" })%></td>
    </tr> 
    <tr>
        <td colspan="2" height="60" valign=bottom align=center><input type="submit" runat="server" value="<%$Resources:ReprotSubmit.Text %>" id ="btnReport" class="NormalButton" name="btnReport" /></td>
    </tr>

    </table>
    
    
    <%} %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
<link href='<% =Url.Content("~/Scripts/Datepicker/css/ui.all.css")%>' rel="stylesheet" type="text/css"
        media="all" />
    <link href='<% =Url.Content("~/Scripts/Datepicker/css/demos.css")%>' rel="stylesheet" type="text/css"
        media="all" />
    <script src='<% =Url.Content("~/Scripts/Datepicker/js/ui.core.js")%>' type="text/javascript"></script>
    <script src='<% =Url.Content("~/Scripts/Datepicker/js/ui.datepicker.js")%>' type="text/javascript"></script>
    <script src='<% =Url.Content("~/Scripts/Library/jquery.treeview.js")%>' type="text/javascript"></script>
    <link href='<% =Url.Content("~/Content/Css/ST_Odyssey.css")%>' rel="stylesheet" type="text/css" />
    <script src='<% =Url.Content("~/Scripts/Admin/UserManage.js")%>' type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        var JQueryDateFormat = "<%= ConvertDateTime.JQueryDateFormat%>";
    </script>
    <script src='<% =Url.Content("~/Scripts/ViewOfficerError/ViewOfficerErrors.js")%>' type="text/javascript"></script>
    <% =Html.JavascriptTag("var IsInSession = '" + Model.IsInSession + "';")%>
</asp:Content>
