<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.Account.Model.UserLoginModel>" %>

<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<link href='<% =Url.Content("~/Content/Css/ST_Odyssey.css")%>' rel="stylesheet" type="text/css" />
<script src='<% =Url.Content("~/Scripts/Library/jquery-1.4.2.min.js")%>' type="text/javascript"></script>
<script src='<% =Url.Content("~/Scripts/Library/jquery.tooltip.js")%>' type="text/javascript"></script>
<script type="text/javascript" src='<% =Url.Content("~/RootPathHandler.ashx") %>'></script>
<script src='<% =Url.Content("~/Scripts/Account/UserLogin.js")%>' type="text/javascript"></script>
<script src='<% =Url.Content("~/Scripts/Tooltip/Tooltip.js")%>' type="text/javascript"></script>
<script src='<% =Url.Content("~/Scripts/jquery-ui-1.8.11.min.js")%>' type="text/javascript"></script>

<title>Opus-AARTO</title>
<link rel="shortcut icon" href='<% =Url.Content("~/Content/Image/Icon/favicon.ico")%>' />
<link rel="stylesheet" type="text/css" href='<% =Url.Content("~/Content/themes/base/jquery.ui.base.css")%>' />
<link rel="stylesheet" type="text/css" href='<% =Url.Content("~/Content/themes/base/jquery.ui.theme.css")%>' />

<% using (Html.BeginForm("Login", "Account", FormMethod.Post, new { id = "formUserLogin" }))
   { %>
<style type="text/css">
    body
    {
        margin: 0px 0px 0px 0px;
        background-color: #6977b0;
        font-size: 12px !important;
        color: #fff;
    }
    td
    {
        font-size: 12px !important;
        color: #fff;
    }
    .logoTable
    {
        margin: 80px 0px 0px 80px;
    }
    .btnLogin
    {
        width: 80px;
        font-weight: bold;
        margin-top: 15px;
    }
    #userName, #userPassword
    {
        width: 250px;
    }
    .ui-dialog .ui-dialog-titlebar-close
    {display:none;}
</style>
<body>
    <center>
        <form id="form1" runat="server">
        <table style="text-align: center;">
            <tr>
                <td valign="middle" align="left" class="loginBackground">
                    <br />
                    <br />
                    <%-- <p>
            Local Authority:<br />
            <% =Html.DropDownList("LAuthorityIntNo", Model.LocalAuthoryList)%>
            <%= Html.ValidationLocalizationError("LocalAuthority")%>
            </p>--%>
                    <table class="logoTable">
                        <tr>
                            <td>
                                <% =Html.Resource("lblUserName.Text")%>
                            </td>
                            <td>
                                <% =Html.TextBox("UserLoginName", Model.UserLoginName, new { id = "userName", })%>
                                <%= Html.ValidationLocalizationError("UserLoginName")%>
                                <%--<img src="../../Content/Image/Icon/Error.gif" id="ImgUserNameError" alt="" />--%><label
                                    id="lblUserName" class="error"></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <% =Html.Resource("lblPassword.Text") %>
                            </td>
                            <td>
                                <% =Html.Password("UserPassword", Model.UserPassword, new { id="userPassword"})%>
                                <div style="position: absolute; top: 0px; right: 0px; color: #ff0000">
                                    <%= Html.ValidationLocalizationError("UserPassword")%>
                                </div>
                                <%--<img src="../../Content/Image/Icon/Error.gif" id="ImgPasswordError" alt="" />--%><label
                                    id="lblPassword" class="error"></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <input type="submit" style="float:left" value="<% =Html.Resource("btnLogin.Value") %>" id="btnLogin"
                                    class="btnLogin" name="Login"/>
                                <% //=Html.ActionLink("Reset Password", "RequestReset", "Account")%>
                                &nbsp;&nbsp;
                                 <input type="button" style="margin-top: 15px; float:right" onclick="window.location = '../Account/RequestReset'" value="Reset Password" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">
                    <table width="100%">
                        <tr>
                            <td>
                                <label id="lnkLastUpdated">
                                    <% =Html.Resource("lblLastUpdateText")%><%= SIL.AARTO.BLL.Utility.ProjectLastUpdated.AARTOWebApplication.ToString("yyyy-MM-dd") %></asp:Label>
                            </td>
                            <td align="right">
                                <label id="Label2">
                                    <%= Html.Resource("lbCommend.Text")%></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!--Block begin-->
        <div id="faceboxInfo">
             <span style="font-weight:bold; color:Red"><% =Html.Resource("JsBlockLoginOneTimes")%></span>
         </div>
         <input id="hidIsLogin" type="hidden" />
         <input id="hidURL" type="hidden" />
        <!--Block end-->
        </form>
    </center>
</body>
<% =Html.JavascriptTag("var returnUrl ='" + Request.QueryString["ReturnUrl"] +"';")%>
<%} %>
