<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.Report.Model.ReportModel>" %>

<%@ Import Namespace="SIL.AARTO.DAL.Entities" %>
<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<% using (Html.BeginForm("ReportSelector", "Report", FormMethod.Post, new { id = "formReportSelector" })) 
    { %>
         <table class=logoTable  border="1" align="center"><tr>
         <td>
           <% =Html.Resource("lblSelectReport.Text")%>
         </td>
         <td>
           <% =Html.DropDownList("RcIntNo", Model.ReportTypeList, new { id = "ddlSelectReport" })%>
         </td>
         <td>
           <input type="submit" name="button" value="<% =Html.Resource("btnSelectReport.Value") %>"
           id="btnSelectReport" class="btnAdd"  />
         </td>
         </tr>
         </table>
         
         
         <fieldset>
         <legend style="color:#333333;font-size:0.9em;font-weight:bold; line-height:30px;height: 30px;width: 85px;"><%=Html.Resource("ReportFilters")%></legend>
            <% =Html.DisplayFilters(Model)%>
          </fieldset>
          
          <table border="0" align="center">
           <tr>
           <td>
                <input type="submit" name="button" value="<% =Html.Resource("btSearch.Value") %>" id="btSearch" class="btnAdd"  />
           </td>
           </tr>
           </table>


    <%} %>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <link href='<% =Url.Content("~/Content/Css/ST_Odyssey.css")%>' rel="stylesheet" type="text/css" />
    <script src='<% =Url.Content("~/Scripts/Report/ReportSelector.js")%>' type="text/javascript"></script>
    <link href='<% =Url.Content("~/Scripts/Datepicker/css/ui.all.css")%>' rel="stylesheet" type="text/css" media="all" />
    <link href='<% =Url.Content("~/Scripts/Datepicker/css/demos.css")%>' rel="stylesheet" type="text/css" media="all" />
    <script src='<% =Url.Content("~/Scripts/Datepicker/js/ui.core.js")%>' type="text/javascript"></script>
    <script src='<% =Url.Content("~/Scripts/Datepicker/js/ui.datepicker.js")%>' type="text/javascript"></script>
</asp:Content>
