<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.Report.Model.ReportModel>" %>

<%@ Import Namespace="SIL.AARTO.DAL.Entities" %>
<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<% using (Html.BeginForm("ReportViewer", "Report", FormMethod.Post, new { id = "formReportViewer" })) 
    { %>
        <br />
        <br />
         <table class=logoTable  width="80%" style=" border-width:1" align="center">
          <tr>
          
          <td width="15%">
           <% =Html.Resource("lblReportNameText.Text")%>
         </td>
         <td width="20%">
           <% =Model.ReportName%>
         </td>
         <td width="15%">
           <% =Html.Resource("lblPageNumberText.Text")%>
         </td>
         <td width="50%">
           <% =Model.PageNumber%>/<% =Model.TotalPageCount%>
         </td>
         </tr>
         <tr>
          <td>
           <% =Html.Resource("lblReportCodeText.Text")%>
         </td>
         <td>
           <% =Model.ReportCode%>
         </td>
         <td>
           <% =Html.Resource("lblLACodeAndNameText.Text")%>
         </td>
         <td>
           <% =Model.LANameAndCode%>
         </td>
         
         </tr>
         <tr valign="top">
          <td>
           <% =Html.Resource("lblDateTimeRequestedText.Text")%>
         </td>
         <td>
           <% =DateTime.Now.ToString("yyyy/MM/dd")%>
         </td>
         <td>
           <% =Html.Resource("lblParametersText.Text")%>
         </td>
         <td>
           <% =Model.Parameters%>
         </td>
         </tr>
         </table>
          <fieldset>
         <legend style="color:#333333;font-size:0.9em;font-weight:bold; line-height:30px;height: 30px;width: 90px;"><%=Html.Resource("ReturnResults")%></legend>
            <% =Html.DisplayResult(Model)%>
            <br />
         </fieldset>
           <br />
           <table border="0" align="center">
           <tr>
           <td>
                <input type="submit" name="button" value="<% =Html.Resource("btBack.Value") %>" id="btBack" class="btnAdd"  />
           </td>
           <td>
                <input type="submit" name="button" value="<% =Html.Resource("btRefresh.Value") %>" id="btRefresh" class="btnAdd"  />
           </td>
           <td>
               <input type="button" name="button" value="<% =Html.Resource("btExportToPDF.Value") %>" id="btExportToPDF" class="btnAdd"  />
           </td>
           <td>
               <input type="button" name="button" value="<% =Html.Resource("btExportToASCII.Value") %>" id="btExportToASCII" class="btnAdd"  />
           </td>
           <% =Html.ApplicationInstalled("Word.Application")%>
               <input type="button" name="button" value="<% =Html.Resource("btExportToWord.Value") %>" id="btExportToWord" class="btnAdd"  />
           <% = "</td>"%>       
           <% =Html.ApplicationInstalled("Excel.Application")%>
               <input type="button" name="button" value="<% =Html.Resource("btExportToExcel.Value") %>" id="btExportToExcel" class="btnAdd"  />
           <% = "</td>"%>
           </tr>
           <tr>
           <td colspan="5" align="center"><label id="TxtMessage" style="color:Red"></label></td></tr>
           </table>
    <%} %>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <link href='<% =Url.Content("~/Content/Css/ST_Odyssey.css")%>' rel="stylesheet" type="text/css" />
    <script src='<% =Url.Content("~/Scripts/Report/ReportViewer.js")%>' type="text/javascript"></script>
    <link href='<% =Url.Content("~/Scripts/ColourPicker/css/layout.css")%>' rel="stylesheet" type="text/css" />
</asp:Content>
