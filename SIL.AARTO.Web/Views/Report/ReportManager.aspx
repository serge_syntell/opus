<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Admin.Master" Inherits="System.Web.Mvc.ViewPage<SIL.AARTO.BLL.Report.Model.ReportModel>" %>

<%@ Import Namespace="SIL.AARTO.DAL.Entities" %>
<%@ Import Namespace="SIL.AARTO.Web.Helpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <% using (Html.BeginForm("ReportManager", "Report", FormMethod.Post, new { id = "formReportManager" })) 
    { %>
          <table class=logoTable  border="0">
          <tr>
         <td>
           <% =Html.Resource("lblSelectReport.Text")%>
         </td>
         <td>
           <% =Html.DropDownList2("RcIntNo", Model.ReportTypeList, new { id = "ddlSelectReport" })%>
         </td>
         <td>
           <input type="submit" name="button" value="<% =Html.Resource("btnSelectReport.Value") %>"
           id="btnSelectReport" class="btnAdd"  />
         </td>
          <td>
           <input type="submit" name="button" value="<% =Html.Resource("btnDeleteReport.Value") %>"
           id="btnDeleteReport" class="btnAdd"  />
         </td>
         </tr>
          <tr>
          <td>
           <% =Html.Resource("lblReportNameText.Text")%>
         </td>
         <td>
           <% =Html.TextBox2("ReportName", Model.ReportName, new { id = "tbReportName", style = "width:100%" })%>
         </td>
         </tr>
        <tr>
          <td>
           <% =Html.Resource("lblReportCodeText.Text")%>
         </td>
         <td>
           <% =Html.TextBox2("ReportCode", Model.ReportCode, new { id = "tbReportCode", MaxLength = "5", style = "width:100%" })%>
         </td>
         </tr>
         
         <tr>
         <td>
           <% =Html.Resource("lblLocalAuthority.Text")%>
         </td>
         <td>
           <% =Html.DropDownList2("AutIntNo", Model.LocalAuthorityList, new { id = "ddlLocalAuthoritys" })%>
         </td>
         </tr>
         <tr>
         <td>
           <% =Html.Resource("lblSetStoredProc.Text")%>
         </td>
         <td>
           <% =Html.DropDownList2("RspIntNo", Model.StoredProcNameList, new { id = "ddlStoredProcNameList" })%>
         </td>
         <td>
           <input type="submit" name="button" value="<% =Html.Resource("btnSetStoredProc.Value") %>" id="btnSetStoredProc" class="btnAdd"  />                      
         </td>
         </tr>
        </table>
         <br />
         <table class=logoTable  border="0"><tr>
         <td style="width:113px">
           <% =Html.Resource("lblAddNewFilters.Text")%>
         </td>
         <td>
            <% if (Model.ReortFiltersList != null) %><%--2013-12-11 Heidi added for fixed if Model.ReortFiltersList is null it throw Exception (5084)--%>
            <%{ %>
           <% =Html.DropDownList("string", Model.ReortFiltersList, new { id = "ddlReortFilters" })%>
           <%} %>
         </td>
         <td>
           <input type="submit" name="button" value="<% =Html.Resource("btnAddNewFilters.Value") %>"
           id="btnAddFilter" class="btnAdd"  />
         </td>
         </tr>
         </table>
         
         <fieldset>
         <legend style="color:#333333;font-size:0.9em;font-weight:bold; line-height:30px;height: 30px;width: 85px;"><%=Html.Resource("ReportFilters")%></legend>
            <% =Html.DisplayFilters(Model)%>
         </fieldset>
         

         <br />
         <table class=logoTable  border="1"><tr>
         <td style="width:120px">
           <% =Html.Resource("lblHeadColumnText.Text")%>
         </td>
         <td>
           <% =Html.TextBox2("HeadColumn", Model.HeadColumn, new { id = "tbHeadColumn" })%>
         </td>
         <td style="width:140px">
           <% =Html.Resource("lblHeadColumnDataField.Text")%>
         </td>
         <td>
           <% =Html.DropDownList2("DataFieldName", Model.DataFieldList, new { id = "ddlDataFieldList",  style="width:120px"})%>
         </td>
         <td style="width:120px">
           <% =Html.Resource("lblHeadColumnColor.Text")%>
         </td>
         <td>
           <div id="HeadColumnColorSelector" class="colorSelector"><div style="background-color: #00ff00" class="colorSelectordiv"></div></div>
         </td>
         <td>
           <% =Html.Resource("lblIsSortable.Text")%>
         </td>
         <td>
           <% =Html.CheckBox("ColumnIsSortable", Model.ColumnIsSortable, new { id = "cbIsSortable" })%>
         </td>
         <td>
           <input type="submit" name="button" value="<% =Html.Resource("btnAddHeadColumn.Value") %>"
           id="btnAddHeadColumn" class="btnAdd"  />
         </td>
         </tr>
         </table>
         <br />
         
         <table class=logoTable  border="1"><tr>
         <td style="width:120px">
           <% =Html.Resource("lblHeadGroupColumnText.Text")%>
         </td>
         <td>
           <% =Html.TextBox2("HeadGroupColumn", Model.HeadGroupColumnText, new { id = "tbHeadGroupColumn" })%>
         </td>
         <td style="width:140px">
           <% =Html.Resource("lblHeadGroupColumnsCount.Text")%>
         </td>
         <td>
           <% =Html.TextBox2("HeadGroupColumnsCount", Model.HeadGroupColumnsCount, new { id = "tbHeadGroupColumnsCount",  style="width:120px"})%>
         </td>
         <td style="width:120px">
           <% =Html.Resource("lblHeadGroupColumnColor.Text")%>
         </td>
         <td>
           <div id="HeadGroupColumnColorSelector" class="colorSelector"><div style="background-color: #00ff00" class="colorSelectordiv"></div></div>
         </td>
         <td>
           <input type="submit" name="button" value="<% =Html.Resource("btnAddHeadGroupColumn.Value") %>"
           id="btnAddHeadGroupColumn" class="btnAdd"  />
         </td>
         </tr>
         </table>
         
         <br />
        
         
         <table class=logoTable  border="0" width="600px">
         <tr>
         <td style="width:55px;"><% =Html.Resource("lblAutomaticGenerate.Text")%>
         </td>
         <td style="width:30px">
            <% =Html.CheckBox2("AutomaticGenerate", Model.AutomaticGenerate, new { id = "cbIsAutomaticGenerate" })%>
         </td>
         <td style="width:50px; text-align:left">
            <% =Html.Resource("lblAutomaticGenerateInterval.Text")%>
         </td>
         <td style="width:140px; text-align:left">
            <% =Html.TextBox2("AutomaticGenerateInterval", Model.AutomaticGenerateInterval, new { id = "tbAutomaticGenerateInterval" })%>
         </td>
         <td style="width:100px; text-align:left">
            <% =Html.DropDownList2("AutomaticGenerateIntervalType", Model.AutomaticGenerateIntervalTypeList, new { id = "ddlAutomaticGenerateIntervalTypeList" , style = "width:100px"})%>
         </td>
         <td style="width:100px; text-align:left">
            <% =Html.DropDownList2("AutomaticGenerateFormat", Model.AutomaticGenerateFormatList, new { id = "ddlAutomaticGenerateFormatList", style = "width:100px" })%>
         </td>
         </tr>
         </table>
         <br />
         
         <table class=logoTable  border="0" width="800px">
         <tr>
         <td><% =Html.Resource("lblRemark.Text")%>
         </td>
         </tr>
         <tr>
         <td>
         <% =Html.TextArea("Remark", Model.Remark, new { id = "tbRemark", style = "width:100%; height:80px" })%>
         <input type="submit" name="button" value="<% =Html.Resource("btnApplyRemark.Value") %>" id="Submit1" class="btnAdd"  />
         </td>
         </tr>
         </table>
         
         
         <br />
         <table class=logoTable  width="80%" style=" border-width:1">
          <tr>
          
          <td width="15%">
           <% =Html.Resource("lblReportNameText.Text")%>
         </td>
         <td width="20%">
           <% =Model.ReportName%>
         </td>
         <td width="15%">
           <% =Html.Resource("lblPageNumberText.Text")%>
         </td>
         <td width="50%">
           <% =Model.PageNumber%>/<% =Model.TotalPageCount%>
         </td>
         </tr>
         
         <tr>
          <td>
           <% =Html.Resource("lblReportCodeText.Text")%>
         </td>
         <td>
           <% =Model.ReportCode%>
         </td>
         <td>
           <% =Html.Resource("lblLACodeAndNameText.Text")%>
         </td>
         <td>
           <% =Model.LANameAndCode%>
         </td>
         </tr>
         <tr valign="top">
          <td>
           <% =Html.Resource("lblDateTimeRequestedText.Text")%>
         </td>
         <td>
           <% =DateTime.Now.ToString("yyyy/MM/dd")%>
         </td>
         <td>
           <% =Html.Resource("lblParametersText.Text")%>
         </td>
         <td>
           <% =Model.Parameters%>
         </td>
         </tr>
         </table>
         
         
           <br />
           
          <fieldset>
         <legend style="color:#333333;font-size:0.9em;font-weight:bold; line-height:30px;height: 30px;width: 90px;" ><%=Html.Resource("ReturnResults")%></legend>
            <% =Html.DisplayResult(Model)%>
         </fieldset>
           <br />
           <table border="0" align="center">
           <tr>
           <td>
                <input type="submit" name="button" value="<% =Html.Resource("btRefresh.Value") %>" id="btRefresh" class="btnAdd"  />
                &nbsp;
           </td>
           <td>
                <input type="submit" name="button" value="<% =Html.Resource("btResetXML.Value") %>" id="btResetXML" class="btnAdd"  />
           </td>
           <td>
                <input type="submit" name="button" value="<% =Html.Resource("btSaveReport.Value") %>" id="btSaveReport" class="btnAdd"  />
           </td>
          <!--  <td>
               <input type="button" name="button" value="<% =Html.Resource("btExportToPDF.Value") %>" id="btExportToPDF" class="btnAdd"  />
           </td>
           <td>
               <input type="button" name="button" value="<% =Html.Resource("btExportToWord.Value") %>" id="btExportToWord" class="btnAdd"  />
           </td>           
           <td>
               <input type="button" name="button" value="<% =Html.Resource("btExportToExcel.Value") %>" id="btExportToExcel" class="btnAdd"  />
           </td>
           <td>
               <input type="button" name="button" value="<% =Html.Resource("btExportToASCII.Value") %>" id="btExportToASCII" class="btnAdd"  />
           </td>
            -->
           </tr>
           </table>
         <% =Html.TextBox("HeadGroupColumnColour", Model.HeadGroupColumnColour, new { id = "HeadGroupColumnColour", style = "visibility:hidden" })%>
         <% =Html.TextBox("HeadColumnColour", Model.HeadColumnColour, new { id = "HeadColumnColour", style = "visibility:hidden" })%>
         <% =Html.TextBox2("StoredProcName", Model.StoredProcName, new { id = "StoredProcName", style = "visibility:hidden" })%>  
    <%} %>
    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <link href='<% =Url.Content("~/Scripts/ColourPicker/css/colorpicker.css")%>' rel="stylesheet" type="text/css" />
    <link href='<% =Url.Content("~/Scripts/ColourPicker/css/layout.css")%>' rel="stylesheet" type="text/css" />
    <link href='<% =Url.Content("~/Content/Css/ST_Odyssey.css")%>' rel="stylesheet" type="text/css" />
    <script src='<% =Url.Content("~/Scripts/Report/ReportManager.js")%>' type="text/javascript"></script>
    <link href='<% =Url.Content("~/Scripts/Datepicker/css/ui.all.css")%>' rel="stylesheet" type="text/css" media="all" />
    <link href='<% =Url.Content("~/Scripts/Datepicker/css/demos.css")%>' rel="stylesheet" type="text/css" media="all" />
    <script src='<% =Url.Content("~/Scripts/Datepicker/js/ui.core.js")%>' type="text/javascript"></script>
    <script src='<% =Url.Content("~/Scripts/Datepicker/js/ui.datepicker.js")%>' type="text/javascript"></script>
    <script src='<% =Url.Content("~/Scripts/ColourPicker/js/colorpicker.js")%>' type="text/javascript"></script>
</asp:Content>