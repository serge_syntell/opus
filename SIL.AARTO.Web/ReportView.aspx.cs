﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SIL.AARTO.Web
{
    public partial class ReportView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["PrintData"] != null)
            {
                byte[] buffer = (byte[])Session["PrintData"];
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(buffer);
                Session["PrintData"] = null;
                Response.End();

                
            }
        }
    }
}
