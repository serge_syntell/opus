﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Web.Mvc;
using System.Collections.Generic;

using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.UserMenu;


namespace SIL.AARTO.Web.Helpers
{
    public static class UserMenuHelper
    {
        private const string strDIV = "&lt;";
        private const string endDIV = "&gt;";
        private const string parentNoChildClassName = "menu_none";
        private const string childClassName = "menu_child_bg";
        private const string parentExpClassName = "menu_bgover";
        private const string parentCollClassName = "menu_bgout";
        private const string divTag = "div";
        private const string ulTag = "ul";
        private const string liTag = "li";

        public static StringBuilder NavigationUserMenu(this HtmlHelper helper,
            UserMenuEntityCollection collection, object[] arg)
        {

            XmlDocument doc = new XmlDocument();

            GenerateTreeView(collection, doc, arg);

            StringBuilder sb = new StringBuilder();

            sb.Append(doc.InnerXml);

            sb.Replace(strDIV, "<");
            sb.Replace(endDIV, ">");
           
            return sb;
        }

        public static StringBuilder CreatePreMenuForUser(this HtmlHelper helper,
            UserMenuEntityCollection collection, object[] arg)
        {
            XmlDocument doc = new XmlDocument();

            XmlElement xmlParentNode = doc.CreateElement(divTag);
            doc.AppendChild(xmlParentNode);

            GeneratePreTreeViewForUser(collection, xmlParentNode, doc, arg);

            StringBuilder sb = new StringBuilder();

            sb.Append(doc.InnerXml);

            sb.Replace(strDIV, "<");
            sb.Replace(endDIV, ">");

  
            return sb;
        }

        public static StringBuilder CreateFullMenuForAdmin(this HtmlHelper helper,
            UserMenuEntityCollection collection, object[] arg)
        {
            XmlDocument doc = new XmlDocument();
            
            XmlElement xmlParentNode = doc.CreateElement(divTag);
            doc.AppendChild(xmlParentNode);

            GenerateTreeViewForAdmin(collection, xmlParentNode, doc, arg);

            StringBuilder sb = new StringBuilder();

            sb.Append(doc.InnerXml);

            sb.Replace(strDIV, "<");
            sb.Replace(endDIV, ">");

            return sb;
        }

        public static StringBuilder CreateFullMenuForAdmin(this Controller controller,
            UserMenuEntityCollection collection, object[] arg)
        {
            XmlDocument doc = new XmlDocument();

            XmlElement xmlParentNode = doc.CreateElement(divTag);
            doc.AppendChild(xmlParentNode);

            GenerateTreeViewForAdmin(collection, xmlParentNode, doc, arg);

            StringBuilder sb = new StringBuilder();

            sb.Append(doc.InnerXml);

            sb.Replace(strDIV, "<");
            sb.Replace(endDIV, ">");

            return sb;
        }

        private static void GenerateTreeView(UserMenuEntityCollection collection,
              XmlDocument doc, params object[] arg)
        {

            UserMenuEntityCollection proccessCollection = new UserMenuEntityCollection();
            foreach (UserMenuEntity menu in collection)
            {
                if (menu.Level == 1)
                {
                    proccessCollection.Add(menu);
                }
            }

            XmlElement xmlParentNode = doc.CreateElement(divTag);
            doc.AppendChild(xmlParentNode);

            int index = 1;
            string className = parentExpClassName;
            string display = "";
            bool boolHasChild;
            string tag = string.Empty;

            tag = arg == null ? "_self" : arg[0].ToString();

            foreach (UserMenuEntity parentMenu in proccessCollection)
            {
                if (!parentMenu.IsVisable)
                {
                    continue;
                }

                boolHasChild = true;

                XmlElement xmlElement = doc.CreateElement(divTag);
                XmlElement xmlChildElement = doc.CreateElement(divTag);

                //if (index != 1)
                //{
                    className = parentCollClassName;
                    display = "display:none";
                //}
                if (parentMenu.UserMenuList.Count == 0)
                {
                    boolHasChild = false;
                    className = parentNoChildClassName;
                    xmlChildElement.SetAttribute("onclick", String.Format("changeMenu(this,{0})", parentMenu.Level));
                    if (parentMenu.IsNewWindow.HasValue && parentMenu.IsNewWindow.Value == true)
                    {
                        tag = "_blank";
                    }
                    if (!parentMenu.IsAarto.Value)
                    {
                        xmlChildElement.InnerText = String.Format("<a onclick=getUrl(this,'/AutoLogin.aspx?PageID={0}'); target='{1}'><span>{2}</span></a>",
                              parentMenu.PageID, tag, parentMenu.AMLMenuItemName);
                    }
                    else
                    {
                        xmlChildElement.InnerText = String.Format("<a onclick=getUrl(this,'{0}');  target='{1}'><span>{2}</span></a>",
                            parentMenu.AaMePageURL,  tag, parentMenu.AMLMenuItemName);
                    }
                }
                else
                {
                    xmlChildElement.SetAttribute("onclick", String.Format("slideDisplay(this)"));
                    xmlChildElement.InnerText = "<span>" + parentMenu.AMLMenuItemName + "</span>";
                }
                xmlChildElement.SetAttribute("class", className);
                xmlElement.AppendChild(xmlChildElement);

                index++;
                if (boolHasChild)
                {
                    XmlElement xmlElementUlParent = doc.CreateElement(divTag);
                    xmlElementUlParent.SetAttribute("class", childClassName);
                    xmlElementUlParent.SetAttribute("style", display);

                    XmlElement xmlNode = doc.CreateElement(ulTag);
                    xmlNode.SetAttribute("id", "tree_" + (index).ToString());

                    xmlElementUlParent.AppendChild(xmlNode);
                    xmlElement.AppendChild(xmlElementUlParent);

                    GenerateChildTreeView(parentMenu.UserMenuList, xmlNode, doc, arg);
                }

                xmlParentNode.AppendChild(xmlElement);
            }

        }

        private static void GenerateChildTreeView(UserMenuEntityCollection collection,
            XmlElement xmlNode, XmlDocument doc, params object[] arg)
        {
            foreach (UserMenuEntity userMenu in collection)
            {
                if (!userMenu.IsVisable)
                {
                    continue;
                }
                XmlElement xmlElement = null;
                xmlElement = doc.CreateElement(liTag);

                if (userMenu.UserMenuList.Count > 0)
                {
                    xmlElement.InnerText = "<span>" + userMenu.AMLMenuItemName + "</span>";

                    XmlElement xmlUlElement = doc.CreateElement(ulTag);
                    xmlElement.AppendChild(xmlUlElement);

                    xmlNode.AppendChild(xmlElement);

                    GenerateChildTreeView(userMenu.UserMenuList, xmlUlElement, doc, arg);

                }
                else
                {
                    string tag = arg == null ? "_self" : arg[0].ToString();

                    xmlElement.SetAttribute("onclick", String.Format("changeMenu(this,{0})", userMenu.Level));

                    if (userMenu.IsNewWindow.HasValue && userMenu.IsNewWindow.Value == true)
                    {
                        tag = "_blank";
                    }
                    if (!userMenu.IsAarto.Value)
                    {
                        xmlElement.InnerText = String.Format("<a onclick=getUrl(this,'/AutoLogin.aspx?PageID={0}');target='{1}'><span>{2}</span></a>",
                             userMenu.PageID, tag, userMenu.AMLMenuItemName);
                    }
                    else
                    {
                        xmlElement.InnerText = String.Format("<a  onclick=getUrl(this,'{0}');  target='{1}'><span>{2}</span></a>",
                             userMenu.AaMePageURL, tag, userMenu.AMLMenuItemName);
                    }
                    xmlNode.AppendChild(xmlElement);

                }

            }
        }

        private static void GenerateTreeViewForAdmin(UserMenuEntityCollection collection,
          XmlElement xmlNode, XmlDocument doc, params object[] arg)
        {
            foreach (UserMenuEntity userMenu in collection)
            {
                XmlElement xmlElement = null;
                xmlElement = doc.CreateElement(liTag);

                //xmlElement.SetAttribute("onclick", String.Format("getMenuID(this,{0})", userMenu.AaMeID));
                xmlElement.InnerText = String.Format("<a onclick='getMenuID(this,{0});' id={0}>{1}</a>",
                      userMenu.AaMeID, userMenu.AMLMenuItemName);

                if (userMenu.UserMenuList.Count > 0)
                {

                    //xmlElement.InnerText =String.Format("<span id={0}>{1}</span>",userMenu.AaMeID, userMenu.AMLMenuItemName);

                    XmlElement xmlUlElement = doc.CreateElement(ulTag);
                    
                    xmlElement.AppendChild(xmlUlElement);

                    xmlNode.AppendChild(xmlElement);

                    GenerateTreeViewForAdmin(userMenu.UserMenuList, xmlUlElement, doc, arg);

                }
                else
                {
                    //string tag = arg == null ? "_self" : arg[0].ToString();

                    //xmlElement.SetAttribute("onclick", String.Format("changeMenu(this,{0})", userMenu.Level));

                    //xmlElement.InnerText = String.Format("<span onclick='getMenuID(this,{0});' id={0}>{1}</span>",
                    //     userMenu.AaMeID, userMenu.AMLMenuItemName);

                    xmlNode.AppendChild(xmlElement);

                }

            }
        }

        private static void GeneratePreTreeViewForUser(UserMenuEntityCollection collection,
         XmlElement xmlNode, XmlDocument doc, params object[] arg)
        {
            foreach (UserMenuEntity userMenu in collection)
            {
                if (!userMenu.IsVisable)
                {
                    continue;
                }
                XmlElement xmlElement = null;
                xmlElement = doc.CreateElement(liTag);

                //xmlElement.SetAttribute("onclick", String.Format("getMenuID(this,{0})", userMenu.AaMeID));
                xmlElement.InnerText = String.Format("<a id={0}>{1}</a>",
                      userMenu.AaMeID, userMenu.AMLMenuItemName);

                if (userMenu.UserMenuList.Count > 0)
                {

                    //xmlElement.InnerText =String.Format("<span id={0}>{1}</span>",userMenu.AaMeID, userMenu.AMLMenuItemName);

                    XmlElement xmlUlElement = doc.CreateElement(ulTag);

                    xmlElement.AppendChild(xmlUlElement);

                    xmlNode.AppendChild(xmlElement);

                    GeneratePreTreeViewForUser(userMenu.UserMenuList, xmlUlElement, doc, arg);

                }
                else
                {
                    xmlNode.AppendChild(xmlElement);

                }

            }
        }
    }
}
