﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIL.AARTO.BLL.Utility;
using System.Web.Mvc;

namespace SIL.AARTO.Web.Helpers
{
    public static class ControllerHelpers
    {
        public static void AddRuleViolations(this ModelStateDictionary modelState, IEnumerable<RuleViolation> errors)
        {
            foreach (RuleViolation issue in errors)
            {
                modelState.AddModelError(issue.ModelName, issue.ResourceName);
            }
        }

        public static string GetBaseurl(this Controller controller)
        {
            return string.Format("http://{0}/", controller.Request.Url.Authority.TrimEnd('/'));
        }
    }
}
