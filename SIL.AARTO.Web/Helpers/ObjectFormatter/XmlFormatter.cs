﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace SIL.AARTO.Web.Helpers.ObjectFormatter
{
    class XmlFormatter
    {
        public static byte[] Serialize<T>(T obj, bool throwError = false)
            where T : class
        {
            try
            {
                byte[] result;
                using (var input = new MemoryStream())
                {
                    var ns = new XmlSerializerNamespaces();
                    ns.Add(string.Empty, string.Empty);
                    new XmlSerializer(typeof(T)).Serialize(input, obj, ns);
                    result = input.ToArray();
                }
                return result;
            }
            catch (Exception e)
            {
                if (throwError)
                    throw;
                return null;
            }
        }

        public static T Deserialize<T>(byte[] buffer, bool throwError = false)
            where T : class
        {
            try
            {
                T result;
                using (var input = new MemoryStream(buffer))
                    result = (T)new XmlSerializer(typeof(T)).Deserialize(input);
                return result;
            }
            catch (Exception e)
            {
                if (throwError)
                    throw;
                return null;
            }
        }
    }
}
