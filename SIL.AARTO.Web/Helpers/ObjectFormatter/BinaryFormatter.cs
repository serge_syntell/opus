﻿using System;
using System.IO;

namespace SIL.AARTO.Web.Helpers.ObjectFormatter
{
    class BinaryFormatter
    {
        public static byte[] Serialize<T>(T obj, bool throwError = false)
            where T : class
        {
            try
            {
                byte[] result;
                using (var input = new MemoryStream())
                {
                    new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter().Serialize(input, obj);
                    result = input.ToArray();
                }
                return result;
            }
            catch (Exception e)
            {
                if (throwError)
                    throw;
                return null;
            }
        }

        public static T Deserialize<T>(byte[] buffer, bool throwError = false)
            where T : class
        {
            try
            {
                T reslut;
                using (var input = new MemoryStream(buffer))
                {
                    reslut = (T)new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter().Deserialize(input);
                }
                return reslut;
            }
            catch (Exception e)
            {
                if (throwError)
                    throw;
                return null;
            }
        }
    }
}
