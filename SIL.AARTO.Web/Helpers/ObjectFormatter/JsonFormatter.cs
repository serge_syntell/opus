﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;

namespace SIL.AARTO.Web.Helpers.ObjectFormatter
{
    class JsonFormatter
    {
        public static byte[] Serialize<T>(T obj, bool throwError = false)
            where T : class
        {
            try
            {
                byte[] result;
                using (var input = new MemoryStream())
                {
                    new DataContractJsonSerializer(typeof(T)).WriteObject(input, obj);
                    result = input.ToArray();
                }
                return result;
            }
            catch (Exception e)
            {
                if (throwError)
                    throw;
                return null;
            }
        }

        public static T Deserialize<T>(byte[] buffer, bool throwError = false)
            where T : class
        {
            try
            {
                T result;
                using (var input = new MemoryStream(buffer))
                    result = (T)new DataContractJsonSerializer(typeof(T)).ReadObject(input);
                return result;
            }
            catch (Exception e)
            {
                if (throwError)
                    throw;
                return null;
            }
        }

        public string ObjectToJson(object obj, bool throwError = false)
        {
            try
            {
                string result;
                using (var ms = new MemoryStream())
                {
                    new DataContractJsonSerializer(obj.GetType()).WriteObject(ms, obj);
                    using (var sr = new StreamReader(ms))
                        result = sr.ReadToEnd();
                }
                return result;
            }
            catch (Exception e)
            {
                if (throwError)
                    throw;
                return null;
            }
        }
    }
}
