﻿using System;
using System.IO;
using System.Web.UI;

namespace SIL.AARTO.Web.Helpers.ObjectFormatter
{
    class ViewStateFormatter
    {
        public static byte[] Serialize<T>(T obj, bool throwError = false)
            where T : class
        {
            try
            {
                byte[] result;
                using (var input = new MemoryStream())
                {
                    new LosFormatter().Serialize(input, obj);
                    result = input.ToArray();
                }
                return result;
            }
            catch (Exception e)
            {
                if (throwError)
                    throw;
                return null;
            }
        }

        public static T Deserialize<T>(byte[] buffer, bool throwError = false)
            where T : class
        {
            try
            {
                T result;
                using (var input = new MemoryStream(buffer))
                    result = (T)new LosFormatter().Deserialize(input);
                return result;
            }
            catch (Exception e)
            {
                if (throwError)
                    throw;
                return null;
            }
        }
    }
}
