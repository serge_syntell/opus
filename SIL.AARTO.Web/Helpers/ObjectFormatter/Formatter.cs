﻿using System;
using System.IO;

namespace SIL.AARTO.Web.Helpers.ObjectFormatter
{
    public class Formatter
    {
        public static string ObjectToString<T>(T obj, FormatterType type, bool throwError = false)
            where T : class
        {
            try
            {
                byte[] buffer = null;
                switch (type)
                {
                    case FormatterType.Json:
                        buffer = JsonFormatter.Serialize(obj, throwError);
                        break;
                    case FormatterType.Xml:
                        buffer = XmlFormatter.Serialize(obj, throwError);
                        break;
                    case FormatterType.Binary:
                        buffer = BinaryFormatter.Serialize(obj, throwError);
                        break;
                    case FormatterType.ViewState:
                        buffer = ViewStateFormatter.Serialize(obj, throwError);
                        break;
                }

                var small = Compresser.Compress(buffer);
                return Convert.ToBase64String(small);
            }
            catch (Exception e)
            {
                if (throwError)
                    throw;
                return null;
            }
        }

        public static T StringToObject<T>(string str, FormatterType type, bool throwError = false)
            where T : class
        {
            try
            {
                var small = Convert.FromBase64String(str);
                var buffer = Compresser.Decompress(small);

                T output = null;
                switch (type)
                {
                    case FormatterType.Json:
                        output = JsonFormatter.Deserialize<T>(buffer, throwError);
                        break;
                    case FormatterType.Xml:
                        output = XmlFormatter.Deserialize<T>(buffer, throwError);
                        break;
                    case FormatterType.Binary:
                        output = BinaryFormatter.Deserialize<T>(buffer, throwError);
                        break;
                    case FormatterType.ViewState:
                        output = ViewStateFormatter.Deserialize<T>(buffer, throwError);
                        break;
                }
                return output;
            }
            catch (Exception e)
            {
                if (throwError)
                    throw;
                return null;
            }
        }
    }

    public enum FormatterType
    {
        Json = 1,
        Xml = 2,
        Binary = 3,
        ViewState = 4
    }
}
