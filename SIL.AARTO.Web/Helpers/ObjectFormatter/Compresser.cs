﻿using System;
using System.IO;
using System.IO.Compression;

namespace SIL.AARTO.Web.Helpers.ObjectFormatter
{
    class Compresser
    {
        public static byte[] Compress(byte[] big, bool throwError = false)
        {
            try
            {
                byte[] result;
                using (var input = new MemoryStream())
                {
                    using (var zip = new GZipStream(input, CompressionMode.Compress))
                        zip.Write(big, 0, big.Length);
                    result = input.ToArray();
                }
                return result;
            }
            catch (Exception e)
            {
                if (throwError)
                    throw;
                return null;
            }
        }

        public static byte[] Decompress(byte[] small, bool throwError = false)
        {
            try
            {
                byte[] result;
                using (var output = new MemoryStream())
                {
                    using (var input = new MemoryStream(small))
                    {
                        using (var zip = new GZipStream(input, CompressionMode.Decompress))
                        {
                            var big = new byte[1024];
                            int offset;
                            while ((offset = zip.Read(big, 0, big.Length)) > 0)
                                output.Write(big, 0, offset);
                        }
                    }
                    output.Position = 0;
                    result = output.ToArray();
                }
                return result;
            }
            catch (Exception e)
            {
                if (throwError)
                    throw;
                return null;
            }
        }
    }
}
