﻿using System.Globalization;
using System.Web;
using System.Web.Compilation;
using System.Web.Mvc;
using System.Web.Routing;
using System;
using System.Resources;
using System.IO;
using System.Text;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.Web.Resource;

namespace SIL.AARTO.Web.Helpers
{

    public static class LocalizationHelper
    {
        #region Constant variables

        private const string USER_LANGUAGE = "UserLanguage";
        private const string APP_LOCAL_RESOURCES = "App_LocalResources";
        private const string APP_LOCAL_RESOURCES2 = "SIL.AARTO.Web.Resource";
        private const string APP_GLOBAL_RESOURCES = "App_GlobalResources";
        private const string EXTENTION_ASCX = ".ascx";
        private const string EXTENTION_RESX = ".resx";

        //private const string GET_SUBURB_BY_CITYID = "GetSuburbByCityID";
        //private const string GET_CITY_BY_STATEID = "GetCityByStateID";

        #endregion

        #region public method for resource

        /// <summary>
        /// Get Resource value for view page
        /// </summary>
        /// <param name="htmlhelper"></param>
        /// <param name="expression"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Resource(this HtmlHelper htmlhelper, string expression, params object[] args)
        {
            string virtualPath = GetVirtualPath(htmlhelper);
            return GetResourceString(htmlhelper.ViewContext.HttpContext, expression, virtualPath, args);
        }
        /// <summary>
        /// Get Resource value for MVC controller
        /// </summary>
        /// <param name="controller">MVC controller</param>
        /// <param name="expression"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Resource(this Controller controller, string expression, params object[] args)
        {
            StringBuilder sbPath = new StringBuilder();

            sbPath.Append("~/Views/");
            sbPath.Append(controller.RouteData.Values["controller"].ToString())
                .Append("/")
                .Append(controller.RouteData.Values["action"].ToString());

            string virtualPath = sbPath.ToString();
            string viewPath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + virtualPath.Replace("~", "").Replace("/", @"\");
            if (File.Exists(viewPath + ".aspx"))
            {
                sbPath.Append(".aspx");
            }
            else
            {
                sbPath.Append(".cshtml");
            }
            return GetLangString(controller.HttpContext, expression, sbPath.ToString(), args);
        }

        public static string ResourceValue(this Controller controller, string action, string expression, params object[] args)
        {
            StringBuilder sbPath = new StringBuilder();

            sbPath.Append("~/Views/");
            sbPath.Append(controller.RouteData.Values["controller"].ToString())
                .Append("/")
                .Append(action);

            string virtualPath = sbPath.ToString();
            string viewPath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + virtualPath.Replace("~", "").Replace("/", @"\");
            if (File.Exists(viewPath + ".aspx"))
            {
                sbPath.Append(".aspx");
            }
            else
            {
                sbPath.Append(".cshtml");
            }
            return GetLangString(controller.HttpContext, expression, sbPath.ToString(), args);
        }
        /// <summary>
        /// Get Resource value for User Control
        /// </summary>
        /// <param name="htmlhelper"></param>
        /// <param name="currentControl"></param>
        /// <param name="expression"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Resource(this HtmlHelper htmlhelper, ViewUserControl currentControl, string expression, params object[] args)
        {
            return GetResourceString(htmlhelper.ViewContext.HttpContext, expression, GetUserControlPath(currentControl), args);
        }

        /// <summary>
        /// Get  value for javascript calling from user control
        /// </summary>
        /// <param name="htmlhelper"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string ResourceForJavaScript(this HtmlHelper htmlhelper, ViewUserControl currentControl, string expression, params object[] args)
        {
            string langstr = GetResourceString(htmlhelper.ViewContext.HttpContext, expression, GetUserControlPath(currentControl), args);

            return string.Format("var {0} = '{1}';", expression, langstr);
        }

        /// <summary>
        /// Get value for javascript calling from view
        /// </summary>
        /// <param name="htmlhelper"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string ResourceForJavaScript(this HtmlHelper htmlhelper, string expression, params object[] args)
        {
            string langstr = GetLangString(htmlhelper.ViewContext.HttpContext, expression, GetVirtualPath(htmlhelper), args);

            return string.Format("var {0} = '{1}';", expression, langstr);
        }

        /// <summary>
        /// Get golbal resource value with key
        /// </summary>
        /// <param name="controler"></param>
        /// <param name="expression">the key value</param>
        /// <param name="globalResource">global resource file name</param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Resource(this Controller controller, string expression, string globalResource, params object[] args)
        {
            return GetMessageFromGlobalResource(controller.HttpContext, expression, globalResource, controller.HttpContext.Request.ApplicationPath, args);
        }

        /// <summary>
        /// Get golbal resource value with key
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="virtualPath"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Resource(string expression, string fileName, string virtualPath, params object[] args)
        {

            return GetMessageFromGlobalResource(System.Web.HttpContext.Current, expression, fileName, virtualPath, args);
        }


        #endregion

        #region detail method how to get resource value

        /// <summary>
        ///  the detail method how to get resource value
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="expression"></param>
        /// <param name="virtualPath"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private static string GetResourceString(HttpContextBase httpContext, string expression, string virtualPath, object[] args)
        {
            /*if (!httpContext.Request.IsAuthenticated)
            {
                string[] language = httpContext.Request.UserLanguages;

                //System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(Language.ConvertLanguageCode(language));
                System.Globalization.CultureInfo ci = Language.EnvironmentCulture;
                
                System.Threading.Thread.CurrentThread.CurrentCulture = ci;
                System.Threading.Thread.CurrentThread.CurrentUICulture = ci;
            }
            else
            {
                //get the language from db and set the culture(performance issue:retrieve the language from session)

            }*/

            ExpressionBuilderContext context = new ExpressionBuilderContext(virtualPath);
            ResourceExpressionBuilder builder = new ResourceExpressionBuilder();
            ResourceExpressionFields fields = (ResourceExpressionFields)builder.ParseExpression(expression, typeof(string), context);

            if (!string.IsNullOrEmpty(fields.ClassKey))
                return string.Format((string)httpContext.GetGlobalResourceObject(fields.ClassKey, fields.ResourceKey, CultureInfo.CurrentUICulture), args);

            return string.Format((string)httpContext.GetLocalResourceObject(virtualPath, fields.ResourceKey, CultureInfo.CurrentUICulture), args);
        }


        /// <summary>
        ///  Get view page path
        /// </summary>
        /// <param name="htmlhelper"></param>
        /// <returns></returns>
        private static string GetVirtualPath(HtmlHelper htmlhelper)
        {
            string virtualPath = null;
            BuildManagerCompiledView view = htmlhelper.ViewContext.View as BuildManagerCompiledView;
            if (view != null)
            {
                virtualPath = view.ViewPath;
            }

            return virtualPath;
        }

        /// <summary>
        /// Get the current User Control path
        /// </summary>
        /// <param name="currentControl"></param>
        /// <returns></returns>
        private static string GetUserControlPath(System.Web.Mvc.ViewUserControl currentControl)
        {
            if (currentControl != null)
            {
                return currentControl.AppRelativeVirtualPath;
            }
            else
            {
                return "";
            }
        }

        #endregion



        #region another detail method how to get resource value
        /// <summary>
        /// the commom method for get value of key
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="key"></param>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        private static string GetLangString(HttpContextBase httpContext, string key, string virtualPath, object[] args)
        {
            string currentLanguage = string.Empty;
            if (!httpContext.Request.IsAuthenticated)
            {
                //currentLanguage = Language.ConvertLanguageCode(httpContext.Request.UserLanguages);
                currentLanguage = Language.EnvironmentLanguage;
            }
            else
            {
                if (httpContext.Session[USER_LANGUAGE] == null)
                {
                    string correntLsCode = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
                    currentLanguage = correntLsCode;
                }
                else
                {
                    currentLanguage = httpContext.Session[USER_LANGUAGE] as string;
                }
            }

            return GetKeyValue(key, currentLanguage, virtualPath, args);
        }

        // Jake 2013-04-16  Modified this function , if local resource does not exists, find resource in SIL.AARTO.Web.Resource
        /// <summary>
        /// get the value for the current key and language
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="currentLanguage"></param>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        private static string GetKeyValue(string key, string currentLanguage, string virtualPath, object[] args)
        {
            string physicalPath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath;

            //"App_LocalResources"
            string[] strArray = virtualPath.Split(new char[] { '/' });


           
            //string[] strFileTypeArray = virtualPath.Split(new char[] { '.' });
            //string fileType = strFileTypeArray[strFileTypeArray.Length - 1];

            string localResourcePath = string.Empty;
            string remoteResourcePath = string.Empty;

            localResourcePath = virtualPath.Replace(strArray[strArray.Length - 1], "") + APP_LOCAL_RESOURCES + "/";
            localResourcePath = physicalPath + localResourcePath.Replace("~", "").Replace("/", "\\");


            remoteResourcePath = physicalPath.Substring(0, physicalPath.LastIndexOf('\\'));
            remoteResourcePath = remoteResourcePath.Substring(0, remoteResourcePath.LastIndexOf('\\')) + "\\" + APP_LOCAL_RESOURCES2 + "\\" + strArray[strArray.Length - 2] + "\\";


            string fileName;

            fileName = strArray[strArray.Length - 1].Replace(EXTENTION_ASCX, "") + "." + currentLanguage + EXTENTION_RESX;

            if (!File.Exists(localResourcePath + fileName) && !File.Exists(remoteResourcePath + fileName))
                fileName = strArray[strArray.Length - 1].Replace(EXTENTION_ASCX, "") + EXTENTION_RESX;

            string specificPath = File.Exists(localResourcePath + fileName) ? localResourcePath : remoteResourcePath;


            //virtualPath = virtualPath.Replace(strArray[strArray.Length - 1], "");
            //virtualPath = virtualPath + APP_LOCAL_RESOURCES + "/";
            //virtualPath = physicalPath + virtualPath.Replace("~", "").Replace("/", "\\");


            //if (fileType == "aspx")
            //{
            //    virtualPath = virtualPath.Replace(strArray[strArray.Length - 1], "");
            //    virtualPath = virtualPath + APP_LOCAL_RESOURCES + "/";
            //    virtualPath = physicalPath + virtualPath.Replace("~", "").Replace("/", "\\");
            //}
            //else
            //{
            //    physicalPath = physicalPath.Substring(0, physicalPath.LastIndexOf('\\'));
            //    virtualPath = physicalPath.Substring(0, physicalPath.LastIndexOf('\\')) + "\\" + APP_LOCAL_RESOURCES2 + "\\" + strArray[strArray.Length - 2] + "\\";
            //}

            //string fileName;

            //fileName = strArray[strArray.Length - 1].Replace(EXTENTION_ASCX, "") + "." + currentLanguage + EXTENTION_RESX;

            //if (!File.Exists(virtualPath + fileName))
            //{
            //    fileName = strArray[strArray.Length - 1].Replace(EXTENTION_ASCX, "") + EXTENTION_RESX;
            //}
            //ResXResourceReader reader = new ResXResourceReader(virtualPath + fileName);

            ResXResourceReader reader = new ResXResourceReader(specificPath + fileName);

            System.Collections.IDictionaryEnumerator enumerator = reader.GetEnumerator();

            string strReturn = "";
            while (enumerator.MoveNext())
            {
                string keyCurrent = (string)enumerator.Key;
                if (key.ToLower().Equals(keyCurrent.ToLower().Trim()))
                {
                    strReturn = enumerator.Value as string;
                    strReturn = string.Format(strReturn, args);
                    break;
                }

            }
            return strReturn;
        }

        /// <summary>
        /// Get user error message with a key
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="key"></param>
        /// <param name="virtualPath"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private static string GetMessageFromGlobalResource(HttpContextBase httpContext, string key, string fileName, string virtualPath, object[] args)
        {
            try
            {
                string currentLanguage = string.Empty;
                if (!httpContext.Request.IsAuthenticated)
                {
                    //currentLanguage = Language.ConvertLanguageCode(httpContext.Request.UserLanguages);
                    currentLanguage = Language.EnvironmentLanguage;
                }
                else
                {
                    if (httpContext.Session[USER_LANGUAGE] == null)
                    {
                        string correntLsCode = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
                        currentLanguage = correntLsCode;
                    }
                    else
                    {
                        currentLanguage = httpContext.Session[USER_LANGUAGE] as string;
                    }
                }

                StringBuilder fullPath = new StringBuilder();
                fullPath.Append(System.Web.HttpContext.Current.Request.PhysicalApplicationPath);
                fullPath.Append(virtualPath.Replace("/", @"\"));
                fullPath.Append(APP_GLOBAL_RESOURCES + @"\");
                fullPath.Append(fileName + "_" + currentLanguage + EXTENTION_RESX);

                ResXResourceReader reader = new ResXResourceReader(fullPath.ToString());

                System.Collections.IDictionaryEnumerator enumerator = reader.GetEnumerator();

                string strReturn = string.Empty;
                while (enumerator.MoveNext())
                {
                    string keyCurrent = (string)enumerator.Key;
                    if (key.ToLower().Equals(keyCurrent.ToLower().Trim()))
                    {
                        strReturn = enumerator.Value as string;
                        strReturn = string.Format(strReturn, args);
                        break;
                    }

                }
                reader.Close();
                return strReturn;
            }
            catch
            {
                return string.Empty;
            }
        }

        private static string GetMessageFromGlobalResource(HttpContext httpContext, string key, string fileName, string virtualPath, object[] args)
        {
            try
            {
                string currentLanguage = string.Empty;
                if (!httpContext.Request.IsAuthenticated)
                {
                    //currentLanguage = Language.ConvertLanguageCode(httpContext.Request.UserLanguages);
                    currentLanguage = Language.EnvironmentLanguage;
                }
                else
                {
                    if (httpContext.Session[USER_LANGUAGE] == null)
                    {
                        string correntLsCode = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
                        currentLanguage = correntLsCode;
                    }
                    else
                    {
                        currentLanguage = httpContext.Session[USER_LANGUAGE] as string;
                    }
                }

                StringBuilder fullPath = new StringBuilder();
                fullPath.Append(System.Web.HttpContext.Current.Request.PhysicalApplicationPath);
                fullPath.Append(virtualPath.Replace("/", @"\"));
                fullPath.Append(APP_GLOBAL_RESOURCES + @"\");
                fullPath.Append(fileName + "_" + currentLanguage + EXTENTION_RESX);

                ResXResourceReader reader = new ResXResourceReader(fullPath.ToString());

                System.Collections.IDictionaryEnumerator enumerator = reader.GetEnumerator();

                string strReturn = string.Empty;
                while (enumerator.MoveNext())
                {
                    string keyCurrent = (string)enumerator.Key;
                    if (key.ToLower().Equals(keyCurrent.ToLower().Trim()))
                    {
                        strReturn = enumerator.Value as string;
                        strReturn = string.Format(strReturn, args);
                        break;
                    }

                }
                reader.Close();
                return strReturn;
            }
            catch
            {
                return string.Empty;
            }
        }
        #endregion
    }

}
