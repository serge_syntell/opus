﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Extensions;

namespace SIL.AARTO.Web.Helpers.MvcWidget
{
    abstract class MvcWidgetMaker
    {
        protected HtmlHelper htmlHelper;
        protected string id;
        protected string id_Container;
        protected string jsName_Response;
        protected string jsName_Trigger;

        protected MvcWidgetMaker(HtmlHelper htmlHelper, string jsName_Trigger, string jsName_Response, string id)
        {
            this.htmlHelper = htmlHelper;
            this.jsName_Trigger = jsName_Trigger;
            this.jsName_Response = jsName_Response;
            this.id = id;
            this.id_Container = id + "_Container";
        }

        #region abstract

        protected abstract string ControllerName { get; }
        protected virtual string ActionName
        {
            get { return "Trigger"; }
        }

        protected abstract string AdditionalPostObject();

        #endregion

        protected virtual string BuildContainer()
        {
            var main = new TagBuilder("div");
            main.GenerateId(this.id_Container);
            return main.ToString();
        }

        protected virtual string BuildScript()
        {
            var sb = new StringBuilder();
            sb.AppendLine2(@"
                function {0}(key) {{
                    var container = $('#{5}');
                    container.empty();
                    $.post('{1}', {{
                        Key : key,
                        ControlId : '{2}',
                        JsName_Response : '{3}',
                        {4}
                    }}, function(rsp){{
                        if (typeof rsp == 'string' && (rsp.length <= 0 || rsp[0] != '{{'))
                            container.html(rsp);
                    }});
                }}
            ", this.jsName_Trigger,
                new UrlHelper(this.htmlHelper.ViewContext.RequestContext).Action(ActionName, ControllerName),
                this.id,
                this.jsName_Response,
                AdditionalPostObject(),
                this.id_Container);

            var main = new TagBuilder("script");
            main.MergeAttribute("type", "text/javascript");
            main.InnerHtml = sb.ToString();
            return main.ToString();
        }

        public virtual IHtmlString Build()
        {
            var sb = new StringBuilder();
            sb.AppendLine(BuildContainer());
            sb.AppendLine(BuildScript());
            var main = new TagBuilder("div")
            {
                InnerHtml = sb.ToString()
            };
            main.GenerateId(this.id);
            return this.htmlHelper.Raw(main.ToString());
        }
    }
}
