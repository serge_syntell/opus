﻿using SIL.AARTO.BLL.Model;

namespace SIL.AARTO.Web.Helpers.MvcWidget
{
    public abstract class MvcWidgetModel : MessageResult
    {
        public string Key { get; set; }
        public string ControlId { get; set; }
        public string JsName_Response { get; set; }
    }
}
