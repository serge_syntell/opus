﻿using System.Web.Mvc;

namespace SIL.AARTO.Web.Helpers.MvcWidget
{
    public abstract class MvcWidgetController<T> : Controller
        where T : MvcWidgetModel
    {
        public abstract ActionResult Trigger(T model);
    }
}
