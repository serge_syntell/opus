﻿using System.Collections.Generic;
using SIL.AARTO.Web.Helpers.MvcWidget;

namespace SIL.AARTO.Web.Helpers.DuplicateWOASelection
{
    public class DuplicateWOASelectionModel : MvcWidgetModel
    {
        public bool AutoRedirectSingleResult { get; set; }
        public bool AutoHideOnSelected { get; set; }
        public List<DuplicateWOA> WOAs { get; set; }
    }

    public class DuplicateWOA
    {
        public int WOAIntNo { get; set; }
        public string WOANumber { get; set; }
        public string NotTicketNo { get; set; }
        public string SummonsNo { get; set; }
        public string AccFullName { get; set; }
    }
}
