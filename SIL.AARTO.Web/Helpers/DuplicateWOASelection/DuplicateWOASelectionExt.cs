﻿using System;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.Web.Helpers.MvcWidget;

namespace SIL.AARTO.Web.Helpers.DuplicateWOASelection
{
    public static class DuplicateWOASelectionExt
    {
        public static IHtmlString DuplicateWOASelection(this HtmlHelper htmlHelper, string jsName_Trigger, string jsName_Response, bool autoRedirectSingleResult = true, bool autoHideOnSelected = true, string id = "DWS")
        {
            return new DuplicateWOASelectionMaker(htmlHelper, jsName_Trigger, jsName_Response, autoRedirectSingleResult, autoHideOnSelected, id).Build();
        }
    }

    class DuplicateWOASelectionMaker : MvcWidgetMaker
    {
        readonly bool autoHideOnSelected;
        readonly bool autoRedirectSingleResult;

        public DuplicateWOASelectionMaker(HtmlHelper htmlHelper, string jsName_Trigger, string jsName_Response, bool autoRedirectSingleResult, bool autoHideOnSelected, string id) : base(htmlHelper, jsName_Trigger, jsName_Response, id)
        {
            if (string.IsNullOrWhiteSpace(jsName_Trigger))
                throw new ArgumentNullException("jsName_Trigger");
            if (string.IsNullOrWhiteSpace(jsName_Response))
                throw new ArgumentNullException("jsName_Response");
            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentNullException("id");

            this.autoRedirectSingleResult = autoRedirectSingleResult;
            this.autoHideOnSelected = autoHideOnSelected;
            this.id = id;
        }

        protected override string ControllerName
        {
            get { return "DuplicateWOASelection"; }
        }

        protected override string AdditionalPostObject()
        {
            return string.Format("AutoRedirectSingleResult:{0}, AutoHideOnSelected:{1}",
                this.autoRedirectSingleResult.ToString().ToLower(),
                this.autoHideOnSelected.ToString().ToLower());
        }
    }
}
