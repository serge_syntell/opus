﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Mvc;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.Web.Helpers.MvcWidget;
using SIL.ServiceBase;

namespace SIL.AARTO.Web.Helpers.DuplicateWOASelection
{
    public class DuplicateWOASelectionController : MvcWidgetController<DuplicateWOASelectionModel>
    {
        public override ActionResult Trigger(DuplicateWOASelectionModel model)
        {
            model.WOAs = new List<DuplicateWOA>();
            var serviceDB = new ServiceDB(Config.ConnectionString);
            serviceDB.ExecuteReader("DuplicateWOASelectionSearch", new[]
            {
                new SqlParameter("@WOANumber", model.Key)
            }, dr =>
            {
                while (dr.Read())
                {
                    model.WOAs.Add(new DuplicateWOA
                    {
                        WOAIntNo = ServiceDB.GetDataReaderValue<int>(dr, "WOAIntNo"),
                        WOANumber = ServiceDB.GetDataReaderValue<string>(dr, "WOANumber"),
                        NotTicketNo = ServiceDB.GetDataReaderValue<string>(dr, "NotTicketNo"),
                        SummonsNo = ServiceDB.GetDataReaderValue<string>(dr, "SummonsNo"),
                        AccFullName = ServiceDB.GetDataReaderValue<string>(dr, "AccFullName"),
                    });
                }
            });
            return PartialView(@"~\Helpers\DuplicateWOASelection\DuplicateWOASelection.cshtml", model);
        }
    }
}
