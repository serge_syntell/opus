﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace System.Web.Mvc
{
    public class ReportResult:ActionResult
    {
        public ReportResult(byte[] data, string mineType)
        {
            this.Data = data;
            this.MineType = mineType;
        }

        public byte[] Data { get; set; }
        public string MineType { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            if (Data == null)
            {
                new EmptyResult().ExecuteResult(context);
                return;
            }
            context.HttpContext.Response.ContentType = MineType;
            context.HttpContext.Response.BinaryWrite(Data);

            //using (MemoryStream ms = new MemoryStream(Data))
            //{
            //    ms.Position = 0;
            //    using (StreamReader sr = new StreamReader(ms))
            //    {
                    
            //        context.HttpContext.Response.Output.Write(sr.ReadToEnd());
            //    }
            //}
        }
    }
}