﻿using System;
using System.Collections.Generic;

namespace SIL.AARTO.Web.Helpers.TicketNumberSearch
{
    [Serializable]
    public class TicketNumberSearchModel
    {
        public int AutIntNo { get; set; }
        public string SelectedTicketNo { get; set; }

        public string NoticePrefix { get; set; }
        public string Number { get; set; }
        public string AutCode { get; set; }
        public string CDV { get; set; }
        public List<TicketNumberDetail> Results { get; set; }

        public bool IsSuccessful { get; set; }
        public string Message { get; set; }
    }

    [Serializable]
    public class TicketNumberDetail
    {
        public TicketNumberDetail() {}

        public TicketNumberDetail(string ticketNo, string regNo, string name)
        {
            NotTicketNo = ticketNo;
            NotRegNo = regNo;
            Name = name;
        }

        public string NotTicketNo { get; set; }
        public string NotRegNo { get; set; }
        public string Name { get; set; }
    }
}
