﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Controllers;
using SIL.AARTO.Web.Helpers.ObjectFormatter;
using Stalberg.TMS;

namespace SIL.AARTO.Web.Helpers.TicketNumberSearch
{
    [AARTOAuthorize]
    public class TicketNumberSearchController : Controller
    {
        #region Cache

        PageCache cache;

        #region bind

        const string cacheKey = "CacheStr";
        const FormatterType formatterType = FormatterType.Xml;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            object value;
            if (filterContext.ActionParameters.TryGetValue(cacheKey, out value))
                BindRequestCache(Convert.ToString(value));
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var result = filterContext.Result as JsonResult;
            if (result != null)
                BindResponseCache(result);

            base.OnActionExecuted(filterContext);
        }

        void BindRequestCache(string cacheStr)
        {
            if (!string.IsNullOrWhiteSpace(cacheStr))
                this.cache = Formatter.StringToObject<PageCache>(cacheStr, formatterType);
            if (this.cache == null)
                this.cache = new PageCache();
        }

        void BindResponseCache(JsonResult json)
        {
            var jsFormatter = new JavaScriptSerializer();
            var original = jsFormatter.Serialize(json.Data);

            original = string.IsNullOrWhiteSpace(original) ? "{}" : original.Trim();
            var end = original.LastIndexOf("}", StringComparison.Ordinal);
            if (end >= 0)
            {
                var value = Formatter.ObjectToString(this.cache, formatterType);
                var data = string.Format("{0}\"{1}\":\"{2}\"", original.Length > 2 ? "," : "", cacheKey, value);
                var newJson = original.Insert(end, data);
                var newObj = jsFormatter.DeserializeObject(newJson);
                json.Data = newObj;
            }
        }

        #endregion

        [Serializable]
        public class PageCache
        {
            public int AutIntNo { get; set; }
            public string TicketProcessor { get; set; }
            public string AutNo { get; set; }
            public int? EnatisAuthorityNumber { get; set; }
            public string AR_4905 { get; set; }
            public int AR_9300 { get; set; }
        }

        #endregion

        readonly AuthorityRuleInfo authorityRuleInfo = new AuthorityRuleInfo();
        readonly AuthorityService authorityService = new AuthorityService();
        readonly string connectionStr = Config.ConnectionString;
        readonly NoticePrefixService noticePrefixService = new NoticePrefixService();
        readonly NoticeTypeService noticeTypeService = new NoticeTypeService();
        int cdv;
        string noticeNumber;
        int number;

        int processorSign
        {
            get
            {
                return "TMS".Equals(this.cache.TicketProcessor, StringComparison.OrdinalIgnoreCase)
                    ? 1 : 2;
            }
        }

        TicketNumberSearchModel ResetModel(TicketNumberSearchModel model)
        {
            model = model ?? new TicketNumberSearchModel();
            model.SelectedTicketNo = null;
            model.IsSuccessful = true;
            model.Message = string.Empty;
            return model;
        }

        TicketNumberSearchModel CheckTicketProcessor(TicketNumberSearchModel model)
        {
            if (model.AutIntNo > 0
                && !model.AutIntNo.Equals(this.cache.AutIntNo))
            {
                var authority = this.authorityService.GetByAutIntNo(model.AutIntNo);
                this.cache.AutIntNo = authority != null ? model.AutIntNo : 0;
                this.cache.TicketProcessor = authority != null && !string.IsNullOrWhiteSpace(authority.AutTicketProcessor)
                    ? authority.AutTicketProcessor.Trim() : string.Empty;
                this.cache.AutNo = authority != null && !string.IsNullOrWhiteSpace(authority.AutNo) ?
                    authority.AutNo.Trim() : string.Empty;
                this.cache.EnatisAuthorityNumber = authority != null ? authority.EnatisAuthorityNumber : (int?)null;

                var rule = this.authorityRuleInfo.GetAuthorityRulesInfoByWFRFANameAutoIntNo(this.cache.AutIntNo, "4905");
                this.cache.AR_4905 = rule != null && !string.IsNullOrWhiteSpace(rule.ARString) ? rule.ARString.Trim() : string.Empty;

                rule = this.authorityRuleInfo.GetAuthorityRulesInfoByWFRFANameAutoIntNo(this.cache.AutIntNo, "9300");
                this.cache.AR_9300 = rule != null ? rule.ARNumeric : 6;
            }

            if (string.IsNullOrWhiteSpace(this.cache.TicketProcessor))
                this.cache.TicketProcessor = Session["TicketProcessor"] != null ? Session["TicketProcessor"].ToString().Trim() : "TMS";

            return model;
        }

        TicketNumberSearchModel SetPrefix(TicketNumberSearchModel model)
        {
            do
            {
                if (model.AutIntNo <= 0) break;

                if (processorSign == 1)
                    model.AutCode = this.cache.AutNo;
                else
                    model.Number = this.cache.EnatisAuthorityNumber.HasValue
                        ? Convert.ToString(this.cache.EnatisAuthorityNumber) : string.Empty;

                var noticeType = this.noticeTypeService.GetByNtCode("6");
                if (noticeType == null) break;

                var noticePrefix = this.noticePrefixService.Find(string.Format("NTIntNo={0} AND AutIntNo={1}", noticeType.NtIntNo, this.cache.AutIntNo)).FirstOrDefault();
                if (noticePrefix == null) break;

                if (this.cache.AR_4905.Equals("N", StringComparison.OrdinalIgnoreCase))
                    model.NoticePrefix = noticePrefix.Nprefix;
            } while (false);

            return model;
        }

        TicketNumberSearchModel Validation(TicketNumberSearchModel model)
        {
            model.IsSuccessful = false;

            do
            {
                if (string.IsNullOrWhiteSpace(model.NoticePrefix))
                {
                    model.Message = string.Format(processorSign == 1
                        ? TicketNumberSearch.PrefixPortionCannotBeEmpty : TicketNumberSearch.NoticePrefixPortionCannotBeEmpty,
                        this.cache.TicketProcessor);
                    break;
                }
                model.NoticePrefix = model.NoticePrefix.Trim();

                if (string.IsNullOrWhiteSpace(model.Number))
                {
                    model.Message = string.Format(processorSign == 1 ?
                        TicketNumberSearch.SequencePortionCannotBeEmpty : TicketNumberSearch.eNatisAuthorityNumberCannotBeEmpty,
                        this.cache.TicketProcessor);
                    break;
                }
                model.Number = model.Number.Trim();

                if (string.IsNullOrWhiteSpace(model.AutCode))
                {
                    model.Message = string.Format(processorSign == 1 ?
                        TicketNumberSearch.AuthorityNumberPortionCannotBeEmpty : TicketNumberSearch.SequencePortionCannotBeEmpty,
                        this.cache.TicketProcessor);
                    break;
                }
                model.AutCode = model.AutCode.Trim();

                if (!int.TryParse(processorSign == 1
                    ? model.Number : model.AutCode, out this.number))
                {
                    model.Message = TicketNumberSearch.SequencePortionIsNaN;
                    break;
                }

                const char zero = '0';
                if (processorSign == 1)
                {
                    model.Number = model.Number.PadLeft(this.cache.AR_9300, zero);

                    this.cdv = TicketNumber.GetCdvOfTicketNumber(this.number, model.NoticePrefix, model.AutCode);
                    if (this.cdv == -1)
                    {
                        model.Message = TicketNumberSearch.SecondCharacterOfPrefixPortionMustBeNumeric;
                        break;
                    }
                    model.CDV = Convert.ToString(this.cdv);

                    this.noticeNumber = string.Join("/", model.NoticePrefix, model.Number, model.AutCode, model.CDV);
                }
                else
                {
                    model.AutCode = model.AutCode.PadLeft(9, zero);
                    this.noticeNumber = string.Join("-", model.NoticePrefix, model.Number, model.AutCode, string.Empty);

                    this.cdv = Verhoeff.CalculateCheckDigit(this.noticeNumber.Replace("-", string.Empty));
                    model.CDV = Convert.ToString(this.cdv);

                    this.noticeNumber += model.CDV;
                }

                Session["MinimalCapture_TicketNo"] = this.noticeNumber;

                model.IsSuccessful = true;
            } while (false);

            return model;
        }


        [HttpPost]
        public JsonResult SetAutIntNo(int autIntNo, string CacheStr)
        {
            var model = new TicketNumberSearchModel
            {
                AutIntNo = autIntNo
            };
            try
            {
                model = ResetModel(model);
                model = SetPrefix(CheckTicketProcessor(model));
            }
            catch (Exception ex)
            {
                model.IsSuccessful = false;
                model.Message = ex.Message;
            }

            return Json(model);
        }

        [HttpPost]
        public JsonResult Search(string NoticePrefix, string Number, string AutCode, string CDV, int AutIntNo, string CacheStr)
        {
            var model = new TicketNumberSearchModel
            {
                NoticePrefix = NoticePrefix,
                Number = Number,
                AutCode = AutCode,
                CDV = CDV,
                AutIntNo = AutIntNo
            };

            try
            {
                model = ResetModel(model);
                if (model.Results != null)
                {
                    model.Results.Clear();
                    model.Results = null;
                }

                model = Validation(CheckTicketProcessor(model));

                do
                {
                    if (!model.IsSuccessful) break;

                    var results = new List<TicketNumberDetail>();

                    var noticeDB = new NoticeDB(this.connectionStr);
                    using (var reader = processorSign == 1
                        ? noticeDB.TicketNumberSearch(processorSign, this.number, model.AutCode, model.NoticePrefix, 0)
                        : noticeDB.TicketNumberSearch(processorSign, this.number, string.Empty, model.NoticePrefix, Convert.ToInt32(model.Number)))
                    {
                        while (reader.Read())
                        {
                            var ticketNo = Convert.ToString(reader["NotTicketNo"]);
                            var regNo = Convert.ToString(reader["NotRegNo"]);
                            var name = Convert.ToString(reader["Name"]);
                            results.Add(new TicketNumberDetail(ticketNo, regNo, name));
                        }
                    }

                    if (results.Count <= 0)
                    {
                        model.SelectedTicketNo = string.Empty;
                        model.IsSuccessful = false;
                        model.Message = string.Format(TicketNumberSearch.TicketNumberNotFound, this.noticeNumber);
                        break;
                    }

                    if (results.Count == 1)
                        model.SelectedTicketNo = results[0].NotTicketNo;
                    else
                        model.Results = new List<TicketNumberDetail>(results);
                } while (false);
            }
            catch (Exception ex)
            {
                model.IsSuccessful = false;
                model.Message = ex.Message;
            }

            return Json(model);
        }
    }
}
