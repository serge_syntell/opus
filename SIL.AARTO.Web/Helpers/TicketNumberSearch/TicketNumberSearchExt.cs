﻿using System;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SIL.AARTO.Web.Helpers.TicketNumberSearch
{
    public static class TicketNumberSearchExt
    {
        /// <summary>
        ///     <para>1. JsFuncSetAutIntNo and JsFuncGetTicketNo is required.</para>
        ///     <para>2. JsFuncSetAutIntNo: Use this function to set AutIntNo for this control.</para>
        ///     <para> ----> sample: SetAutIntNo(autIntNo).</para>
        ///     <para>3. JsFuncGetTicketNo: Write this function in your code, it will be called when callback.</para>
        ///     <para> ----> sample: function GetTicketNo(ticketNo) { }</para>
        /// </summary>
        public static IHtmlString TicketNumberSearch(this HtmlHelper htmlHelper, UrlHelper urlHelper, string jsFunSetAutIntNo, string jsFunGetTicketNo, TicketNumberSearchParameters parameters = null, string id = "TNS")
        {
            var sb = new StringBuilder().AppendLine();

            var maker = new TicketNumberSearchMaker(urlHelper, jsFunSetAutIntNo, jsFunGetTicketNo, parameters ?? new TicketNumberSearchParameters(), id);
            sb.AppendLine(maker.Build());

            return htmlHelper.Raw(sb.ToString());
        }
    }

    public class TicketNumberSearchParameters
    {
        public TicketNumberSearchParameters()
        {
            //Width = "330px";
            CssNormal = "Normal";
            CssNormalButton = "NormalButton";
            CssGrid = "Normal";
            CssGridHeader = "CartListHead";
            CssGridFooter = "CartListHead";
            CssGridRow = "CartListItem";
            CssGridRowAlt = "CartListItemAlt";
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string Width { get; set; }
        public string CssNormal { get; set; }
        public string CssNormalButton { get; set; }
        public string CssGrid { get; set; }
        public string CssGridHeader { get; set; }
        public string CssGridFooter { get; set; }
        public string CssGridRow { get; set; }
        public string CssGridRowAlt { get; set; }
    }

    class TicketNumberSearchMaker
    {
        readonly string id;
        readonly string jsFunGetTicketNo;
        readonly string jsFunSetAutIntNo;
        readonly TicketNumberSearchParameters parameters;
        readonly UrlHelper urlHelper;
        readonly int? width;
        readonly string widthSuffix;

        #region names

        string name_NoticePrefix
        {
            get { return "NoticePrefix"; }
        }
        string name_Number
        {
            get { return "Number"; }
        }
        string name_AutCode
        {
            get { return "AutCode"; }
        }
        string name_CDV
        {
            get { return "CDV"; }
        }
        string name_AutIntNo
        {
            get { return "AutIntNo"; }
        }
        string name_CacheStr
        {
            get { return "CacheStr"; }
        }

        #endregion

        #region controllerIDs

        string divContainer
        {
            get { return string.Format("{0}_divTicketNumberSearch", this.id); }
        }
        string btnSearch
        {
            get { return string.Format("{0}_btnSearch", this.id); }
        }
        string grdTickNo
        {
            get { return string.Format("{0}_grdTickNo", this.id); }
        }
        string grdTickNoList
        {
            get { return string.Format("{0}_grdTickNoList", this.id); }
        }
        string lblError
        {
            get { return string.Format("{0}_lblError", this.id); }
        }

        #endregion

        #region jsFuncNames

        string fnSetContent
        {
            get { return string.Format("{0}_SetContent", this.id); }
        }
        string fnBuildModel
        {
            get { return string.Format("{0}_BuildModel", this.id); }
        }
        string fnBindModel
        {
            get { return string.Format("{0}_BindModel", this.id); }
        }
        string fnBindList
        {
            get { return string.Format("{0}_BindList", this.id); }
        }
        string fnSetErrorMsg
        {
            get { return string.Format("{0}_SetErrorMsg", this.id); }
        }
        //Jerry 2013-11-18 add
        string fnAutoQueue
        {
            get { return string.Format("{0}_AutoQueue", this.id); }
        }

        #endregion

        public TicketNumberSearchMaker(UrlHelper urlHelper, string jsFunSetAutIntNo, string jsFunGetTicketNo, TicketNumberSearchParameters parameters, string id)
        {
            if (urlHelper == null)
                throw new ArgumentNullException("urlHelper");
            this.urlHelper = urlHelper;

            if (string.IsNullOrWhiteSpace(jsFunSetAutIntNo))
                throw new ArgumentNullException("jsFunSetAutIntNo");
            this.jsFunSetAutIntNo = jsFunSetAutIntNo;

            if (string.IsNullOrWhiteSpace(jsFunGetTicketNo))
                throw new ArgumentNullException("jsFunGetTicketNo");
            this.jsFunGetTicketNo = jsFunGetTicketNo;

            if (parameters == null)
                throw new ArgumentNullException("parameters");
            this.parameters = parameters;

            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentNullException("id");
            this.id = id;

            if (!string.IsNullOrWhiteSpace(this.parameters.Width))
            {
                this.parameters.Width = this.parameters.Width.Trim();
                string w;
                if (this.parameters.Width.EndsWith("%"))
                {
                    w = this.parameters.Width.Remove(this.parameters.Width.Length - 1, 1);
                    this.widthSuffix = "%";
                }
                else if (this.parameters.Width.EndsWith("px", StringComparison.OrdinalIgnoreCase))
                {
                    w = this.parameters.Width.Remove(this.parameters.Width.Length - 2, 2);
                    this.widthSuffix = "px";
                }
                else
                {
                    w = this.parameters.Width;
                    this.widthSuffix = "px";
                }
                int n;
                if (int.TryParse(w, out n))
                    this.width = n;
            }
        }

        string GetWidth(int percentage)
        {
            if (!this.width.HasValue) return string.Empty;

            var widthStr = string.Format("width: {0}{1};", this.widthSuffix == "%" ? percentage : (percentage*this.width/100), this.widthSuffix);
            return widthStr;
        }

        string BuildSearcher()
        {
            var sb = new StringBuilder().AppendLine();
            var td = new TagBuilder("td");

            var input = new TagBuilder("input");
            input.AddCssClass(this.parameters.CssNormal);
            input.MergeAttribute("type", "text");
            input.MergeAttribute("tabIndex", "1");
            input.MergeAttribute("style", "width: 50px");
            input.MergeAttribute("data-tns-group", this.id);
            input.MergeAttribute("data-tns-name", name_NoticePrefix);
            input.MergeAttribute("onkeyup", fnAutoQueue + "()"); //Jerry 2013-11-18 add
            td.InnerHtml = input.ToString();
            sb.AppendLine(td.ToString());

            input = new TagBuilder("input");
            input.AddCssClass(this.parameters.CssNormal);
            input.MergeAttribute("type", "text");
            input.MergeAttribute("tabIndex", "2");
            input.MergeAttribute("style", "width: 70px");
            input.MergeAttribute("data-tns-group", this.id);
            input.MergeAttribute("data-tns-name", name_Number);
            td.InnerHtml = input.ToString();
            sb.AppendLine(td.ToString());

            input = new TagBuilder("input");
            input.AddCssClass(this.parameters.CssNormal);
            input.MergeAttribute("type", "text");
            input.MergeAttribute("tabIndex", "3");
            input.MergeAttribute("style", "width: 80px");
            input.MergeAttribute("data-tns-group", this.id);
            input.MergeAttribute("data-tns-name", name_AutCode);
            td.InnerHtml = input.ToString();
            sb.AppendLine(td.ToString());

            input = new TagBuilder("input");
            input.AddCssClass(this.parameters.CssNormal);
            input.MergeAttribute("type", "text");
            input.MergeAttribute("tabIndex", "4");
            input.MergeAttribute("style", "width: 50px");
            input.MergeAttribute("disabled", "disabled");
            input.MergeAttribute("data-tns-group", this.id);
            input.MergeAttribute("data-tns-name", name_CDV);
            td.InnerHtml = input.ToString();
            sb.AppendLine(td.ToString());

            input = new TagBuilder("input");
            input.GenerateId(btnSearch);
            input.AddCssClass(this.parameters.CssNormalButton);
            input.MergeAttribute("type", "button");
            input.MergeAttribute("tabIndex", "5");
            input.MergeAttribute("style", "width: 80px");
            input.MergeAttribute("value", TicketNumberSearch.Search);
            td.InnerHtml = input.ToString();

            input = new TagBuilder("input");
            input.MergeAttribute("type", "hidden");
            input.MergeAttribute("data-tns-group", this.id);
            input.MergeAttribute("data-tns-name", name_AutIntNo);
            td.InnerHtml += input.ToString();

            input = new TagBuilder("input");
            input.MergeAttribute("type", "hidden");
            input.MergeAttribute("data-tns-group", this.id);
            input.MergeAttribute("data-tns-name", name_CacheStr);
            td.InnerHtml += input.ToString();

            sb.AppendLine(td.ToString());

            var tr = new TagBuilder("tr")
            {
                InnerHtml = sb.ToString()
            };

            var main = new TagBuilder("table")
            {
                InnerHtml = tr.ToString()
            };
            main.MergeAttribute("cellspacing", "0");
            main.MergeAttribute("cellpadding", "3");
            main.MergeAttribute("border", "0");

            return main.ToString();
        }

        string BuildGrid()
        {
            var sb = new StringBuilder().AppendLine();

            var td = new TagBuilder("td")
            {
                InnerHtml = TicketNumberSearch.TicketNumber
            };
            td.MergeAttribute("style", "min-width: 120px;", true);
            sb.AppendLine(td.ToString());

            td.InnerHtml = TicketNumberSearch.Registration;
            td.MergeAttribute("style", "min-width: 80px;", true);
            sb.AppendLine(td.ToString());

            td.InnerHtml = TicketNumberSearch.Name;
            td.MergeAttribute("style", "min-width: 80px;", true);
            sb.AppendLine(td.ToString());

            td.InnerHtml = TicketNumberSearch.Select;
            td.MergeAttribute("style", "min-width: 60px;", true);
            sb.AppendLine(td.ToString());

            var tr = new TagBuilder("tr")
            {
                InnerHtml = sb.ToString()
            };
            tr.MergeAttribute("style", "text-align: center;");

            var thead = new TagBuilder("thead")
            {
                InnerHtml = tr.ToString()
            };
            thead.AddCssClass(this.parameters.CssGridHeader);

            var tbody = new TagBuilder("tbody");
            tbody.GenerateId(grdTickNoList);

            td.Attributes.Clear();
            td.MergeAttribute("colspan", "4");
            td.InnerHtml = " ";
            tr.InnerHtml = td.ToString();
            var tfoot = new TagBuilder("tfoot")
            {
                InnerHtml = tr.ToString()
            };
            tfoot.AddCssClass(this.parameters.CssGridFooter);

            var main = new TagBuilder("table")
            {
                InnerHtml = sb.Clear()
                    .AppendLine(thead.ToString())
                    .AppendLine(tbody.ToString())
                    .Append(tfoot)
                    .ToString()
            };
            main.GenerateId(grdTickNo);
            main.MergeAttribute("style", "display: none; " + GetWidth(100));
            main.MergeAttribute("cellspacing", "0");
            main.MergeAttribute("cellpadding", "3");
            main.MergeAttribute("border", "0");
            return main.ToString();
        }

        string BuildLabelError()
        {
            var sb = new StringBuilder().AppendLine();

            var error = new TagBuilder("span");
            error.GenerateId(lblError);
            error.AddCssClass(this.parameters.CssNormal);
            error.MergeAttribute("style", "color: red; display: none");
            sb.AppendLine(error.ToString());

            return sb.ToString();
        }

        string BuildJs()
        {
            var sb = new StringBuilder().AppendLine();

            sb.AppendLine(string.Format(@"
            $('#{0}').keypress(function(e) {{
                switch (e.which) {{
                    case 13:
                        $('#{1}').click();
                        break;
                }}
            }});
            ", divContainer, btnSearch));

            sb.AppendLine(string.Format(@"
            function {0}(autIntNo) {{
                if (typeof autIntNo == 'undefined' || !(autIntNo > 0)) return;
                var txtAutIntNo = $('input[data-tns-group=""{1}""][data-tns-name=""{2}""]');
                if (typeof txtAutIntNo == 'undefined' || txtAutIntNo.length <= 0) return;
                txtAutIntNo.val(autIntNo);
                var txtCache = $('input[data-tns-group=""{3}""][data-tns-name=""{4}""]');
                if (typeof txtCache == 'undefined' || txtCache.length <= 0) return;
                var cache = txtCache.val();
                $.post('{5}', {{autIntNo:autIntNo,cacheStr:cache}}, function(rsp) {{
                    {6}(rsp);
                }});
            }}
            ", this.jsFunSetAutIntNo,
                this.id, name_AutIntNo,
                this.id, name_CacheStr,
                this.urlHelper.Action("SetAutIntNo", "TicketNumberSearch"), fnBindModel));

            sb.AppendLine("$(function() {");
            sb.AppendLine(string.Format(@"
            $('#{0}').click(function() {{
                var para = {1}();
                if (typeof para == 'undefined') return;
                $.post('{2}', para, function(rsp) {{
                    {3}(rsp);
                }});
            }});
            ", btnSearch, fnBuildModel, this.urlHelper.Action("Search", "TicketNumberSearch"), fnBindModel));
            sb.AppendLine("});");

            sb.AppendLine(string.Format(@"
            function {0}() {{
                var para = '';
                $('input[data-tns-group=""{1}""]').each(function() {{
                    var name = $(this).data('tns-name');
                    if (typeof name == 'undefined' || name.length <= 0) return true;
                    var value = $(this).val();
                    if (typeof value == 'undefined') value = '';
                    para += name + '=' + encodeURIComponent(value) + '&';
                }});
                if (typeof para != 'undefined' && para.length > 0) para = para.substr(0, para.length - 1);
                return para;
            }} 
            ", fnBuildModel, this.id));

            sb.AppendLine(string.Format(@"
            function {0}(rsp) {{
                {1}();
                if (typeof rsp == 'undefined') return;
                if (!rsp.IsSuccessful) {{
                    {2}(rsp.Message);
                }} else {{
                    {3}('{4}', rsp.NoticePrefix);
                    {5}('{6}', rsp.Number);
                    {7}('{8}', rsp.AutCode);
                    {9}('{10}', rsp.CDV);
                    {11}('{12}', rsp.AutIntNo);
                    {13}('{14}', rsp.CacheStr);
                    {15}(rsp.Results);
                }}
                if (typeof rsp.SelectedTicketNo != 'undefined' && rsp.SelectedTicketNo != null)
                    {16}(rsp.SelectedTicketNo);
            }};
            ", fnBindModel, fnSetErrorMsg, fnSetErrorMsg,
                fnSetContent, name_NoticePrefix,
                fnSetContent, name_Number,
                fnSetContent, name_AutCode,
                fnSetContent, name_CDV,
                fnSetContent, name_AutIntNo,
                fnSetContent, name_CacheStr,
                fnBindList, this.jsFunGetTicketNo));

            sb.AppendLine(string.Format(@"
            function {0}(list) {{
                var table = $('#{1}');
                var tbody = $('#{2}');
                if (typeof table == 'undefined' || typeof tbody == 'undefined') return;
                tbody.empty();
                table.css('display', 'none');
                if (typeof list == 'undefined' || list == null || list.length <= 0) return;
                for (var i = 0; i < list.length; i++) {{
                    var ticketNo = list[i].NotTicketNo;
                    var a = '<a href=""javascript:void(0)"" onclick=""{3}(\'' + ticketNo + '\');{4}()"" >{5}</a>';
                    var cc = i%2 == 0 ? '{6}' : '{7}';
                    var tr = '<tr class=""' + cc + '"" style=""text-align: center;"" ><td>' + ticketNo + '</td><td>' + list[i].NotRegNo + '</td><td>' + list[i].Name + '</td><td>' + a + '</td></tr>\ ';
                    tbody.append(tr);
                }}
                table.css('display', 'block');
            }}
            ", fnBindList, grdTickNo, grdTickNoList,
                this.jsFunGetTicketNo, fnBindList, TicketNumberSearch.Select,
                this.parameters.CssGridRow, this.parameters.CssGridRowAlt));

            sb.AppendLine(string.Format(@"
            function {0}(name, value) {{
                var ctrl = $('input[data-tns-group=""{1}""][data-tns-name=""' + name + '""]');
                if (typeof ctrl == 'undefined' || ctrl.length <= 0) return;
                ctrl.val(value);
            }}
            ", fnSetContent, this.id));

            sb.AppendLine(string.Format(@"
            function {0}(msg) {{
                var error = $('#{1}');
                if (typeof error == 'undefined') return;
                error.text('');
                error.css('display', 'none');
                if (typeof msg == 'undefined' || msg.length <= 0) return;
                error.text(msg);
                error.css('display', 'block');
            }};
            ", fnSetErrorMsg, lblError));

            sb.AppendLine(string.Format(@"
                function {0}(){{
                    var nPrefix = $('input[data-tns-group=""{1}""][data-tns-name=""{2}""]').val();
                    if (nPrefix.toString().length >= 2) {{
                        $('input[data-tns-group=""{3}""][data-tns-name=""{4}""]').focus();
                    }}
                }};
            ", fnAutoQueue, this.id, name_NoticePrefix, this.id, name_Number));

            var main = new TagBuilder("script");
            main.MergeAttribute("type", "text/javascript");
            main.InnerHtml = sb.ToString();
            return main.ToString();
        }

        public string Build()
        {
            var sb = new StringBuilder().AppendLine();
            sb.AppendLine(BuildSearcher())
                .AppendLine(BuildGrid())
                .AppendLine(BuildLabelError())
                .AppendLine(BuildJs());
            var main = new TagBuilder("div")
            {
                InnerHtml = sb.ToString()
            };
            main.GenerateId(divContainer);
            main.MergeAttribute("style", GetWidth(100));
            return main.ToString();
        }
    }
}
