﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;

namespace SIL.AARTO.Web.Helpers
{
    public static class ValidationExtensionsByResourceFile
    {
        public const string ERROR_IMAGE = "../Content/Image/Icon/Error.gif";

        public static string ValidationMessageByResourceFile(this HtmlHelper htmlHelper, string modelName)
        {
            return ValidationMessageByResourceFile(htmlHelper, modelName, (object)null /* htmlAttributes */);
        }

        public static string ValidationMessageByResourceFile(this HtmlHelper htmlHelper, string modelName, object htmlAttributes)
        {
            return ValidationMessageByResourceFile(htmlHelper, modelName, new RouteValueDictionary(htmlAttributes));
        }
        public static string ValidationMessageByResourceFile(this HtmlHelper htmlHelper
            , string modelName, IDictionary<string, object> htmlAttributes)
        {
            if (modelName == null)
            {
                throw new ArgumentNullException("modelName");
            }

            if (!htmlHelper.ViewData.ModelState.ContainsKey(modelName))
            {
                return null;
            }

            ModelState modelState = htmlHelper.ViewData.ModelState[modelName];
            ModelErrorCollection modelErrors = (modelState == null) ? null : modelState.Errors;
            ModelError modelError = ((modelErrors == null) || (modelErrors.Count == 0)) ? null : modelErrors[0];

            if (modelError == null)
            {
                return null;
            }

            //TagBuilder builder = new TagBuilder("span");
            //builder.MergeAttributes(htmlAttributes);
            //builder.MergeAttribute("class", HtmlHelper.ValidationMessageCssClassName);

            string resourceErrorMsg = htmlHelper.Resource(modelError.ErrorMessage);

            //builder.SetInnerText(resourceErrorMsg);

            //return builder.ToString(TagRenderMode.Normal);

            var errorMessage = "<img src='" + ERROR_IMAGE + "' title='" + resourceErrorMsg + "'/>";
            return errorMessage;
        }
    }
}
