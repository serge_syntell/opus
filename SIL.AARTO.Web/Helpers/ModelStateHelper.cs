﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Text;
using System.Web.Mvc.Html;


using SIL.AARTO.BLL.Utility;


namespace SIL.AARTO.Web.Helpers
{
    public static class ModelStateHelper
    {
        private const string ERROR_IMAGE = "../Content/Image/Icon/Error.gif";
        private const string NOERROR_IMAGE = "../Content/Image/Icon/Success.gif";

        public static void AddModelLocalizationError(this ModelStateDictionary modelState,Controller controller, IEnumerable<BLL.Utility.RuleViolation> errors)
        {
            foreach (RuleViolation issue in errors)
            {
                modelState.AddModelError(issue.ModelName, controller.Resource(issue.ResourceName, issue.Args));
            }
        }

        public static void AddModelError(this ModelStateDictionary modelState, Controller controller, IEnumerable<BLL.Utility.RuleViolation> errors)
        {
            foreach (RuleViolation issue in errors)
            {
                modelState.AddModelError(issue.ModelName, issue.ResourceName);
            }
        }

        public static string ValidationLocalizationError(this HtmlHelper htmlHelper, string modelName)
        {
            string errorMessage = "";
            if (htmlHelper.ViewData.ModelState.Count > 0)
            {
                if (htmlHelper.ViewData.ModelState[modelName]!=null && htmlHelper.ViewData.ModelState[modelName].Errors.Count > 0)
                {
                    errorMessage = htmlHelper.ViewData.ModelState[modelName].Errors[0].ErrorMessage;

                    errorMessage = "<img src='" + ERROR_IMAGE + "' title='" + errorMessage + "'/>";
                }
                //else
                //{
                //    errorMessage = "<img src='" + NOERROR_IMAGE + "' />";
                //}
            }

            return errorMessage;
        }
   }
}
