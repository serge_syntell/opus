﻿/*
 * Author: Jake
 * Created Date: 2009-07-08
 * File Name: EmailHelper.cs
 * Copyright: 
 * Purpose: Output Javascript Tag on client
 * 
 **************************************
 * Modification History:
 */

using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace SIL.AARTO.Web.Helpers
{
    public static  class JavaScriptTagHelper
    {
        public static string JavascriptTag(this HtmlHelper helper, string text)
        {
            return String.Format("<script type='text/javascript'>{0}</script>",text);
        }
    }
}
