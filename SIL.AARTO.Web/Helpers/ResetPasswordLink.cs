﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIL.AARTO.BLL.Utility.ReportExtension;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Account.Model;
using SIL.AARTO.BLL.Account;
using SIL.AARTO.BLL.Enum;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.Web.Helpers
{
    public static class ResetPasswordLink
    {
        public static bool CreateAndSendResetLink(int userId, string userEmail, string BaseUrl)
        {
            try
            {
                var linkGuid = Guid.NewGuid();

                // create link with with guid at end
                string resetLink = BaseUrl + "/Account/Reset?ID=" + linkGuid.ToString();

                // insert link and activuty into UserAccountActivity table
                //UserLoginModel um = new UserLoginModel();
                //um.UserLoginName = user.UserLoginName;
                //um.UserPassword = user.UserPassword;

                if (UserLoginManager.CreateAccountActivity(userId, UserAccountActivity.PasswordReset, linkGuid))
                {
                    // email the link to the user in question
                    EmailHelper email = new EmailHelper();

                    email.MailBox = new System.Net.Mail.MailMessage("OpusAccounts@syntell.co.za", userEmail, "OPUS password reset request", resetLink);
                    email.MailBox.IsBodyHtml = true;

                    email.Send();
                    return true;
                }
            }
            catch(Exception ex)
            {
                EntLibLogger.WriteLog(LogCategory.Exception, null, 
                    string.Format("Error sending link to new password Message:{0} Stack:{1} ",ex.Message,ex.StackTrace));
            }

            return false;
        }

        public static void SubmitResetPassword(UserLoginModel usermodel)
        {
            try
            {
                //CreateAndSendResetLink(usermodel,"");

            }
            catch(Exception ex)
            {
                
            }
        }
    }
}