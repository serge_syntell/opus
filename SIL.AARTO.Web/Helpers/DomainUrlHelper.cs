﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.UserAutoLogin;

namespace SIL.AARTO.Web.Helpers
{
    public class DomainUrlHelper
    {
        #region Consts

        public const string TMSDomainKey = "TMSDomain";
        const string AARTODomainURLKey = "AARTODomainURL";
        const string AARTODomainExternalURLKey = "AARTODomainExternalURL";
        const string TMSDomainURLKey = "TMSDomainURL";
        const string TMSDomainExternalURLKey = "TMSDomainExternalURL";
        const int RequestTimeout = 600000;
        const int SessionTimeout = 20;
        const int Retry = 3;
        const int Wait = 1000;

        #endregion

        static readonly AartoPageListService pageService = new AartoPageListService();
        static readonly AartoAndTMSAutoLogin loginService = new AartoAndTMSAutoLogin();

        public static string GetServerIp()
        {
            if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                return HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] != null)
                return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            return string.Empty;
        }

        public static string GetHostName()
        {
            return HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
        }

        public static string GetTMSDomain()
        {
            var now = DateTime.Now;
            string tmsDomain = null;
            var domain = HttpContext.Current.Session[TMSDomainKey] as DomainUrl;

            if (domain != null
                && !string.IsNullOrWhiteSpace(tmsDomain = domain.Url)
                && now.Ticks < domain.Expires)
                return tmsDomain;
            HttpContext.Current.Session.Remove(TMSDomainKey);

            var ip = GetServerIp();
            var host = GetHostName();

            if (string.IsNullOrWhiteSpace(ip)
                || ConfigurationManager.AppSettings[AARTODomainURLKey].IndexOf(ip, StringComparison.OrdinalIgnoreCase) >= 0
                || ConfigurationManager.AppSettings[AARTODomainURLKey].IndexOf(host, StringComparison.OrdinalIgnoreCase) >= 0)
                tmsDomain = ConfigurationManager.AppSettings[TMSDomainURLKey];
            else if (ConfigurationManager.AppSettings[AARTODomainExternalURLKey].IndexOf(ip, StringComparison.OrdinalIgnoreCase) >= 0
                || ConfigurationManager.AppSettings[AARTODomainExternalURLKey].IndexOf(host, StringComparison.OrdinalIgnoreCase) >= 0)
                tmsDomain = ConfigurationManager.AppSettings[TMSDomainExternalURLKey];

            if (!string.IsNullOrWhiteSpace(tmsDomain) && tmsDomain.EndsWith("/"))
                tmsDomain = tmsDomain.Remove(tmsDomain.Length - 1, 1);

            if (!SimulateLoginTMS(tmsDomain)) return string.Empty;

            HttpContext.Current.Session[TMSDomainKey] = new DomainUrl
            {
                Url = tmsDomain,
                Expires = now.AddMinutes(Math.Min(SessionTimeout,
                    ((SessionStateSection)WebConfigurationManager
                        .OpenWebConfiguration("~/Web.config")
                        .GetSection("system.web/sessionState"))
                        .Timeout.TotalMinutes)).Ticks
            };
            return tmsDomain;
        }

        public static bool SimulateLoginTMS(string tmsDomain, string pageName = "/Default.aspx")
        {
            if (string.IsNullOrWhiteSpace(tmsDomain)) return false;

            var user = HttpContext.Current.Session["UserLoginInfo"] as UserLoginInfo;
            if (user == null) return false;

            string key;
            var login = loginService.CreateUserKeyByUserIDAndAutID(user.UserIntNo, user.AuthIntNo);
            if (login == null || string.IsNullOrWhiteSpace(key = login.AaLogSynSecretKey)) return false;

            var autIntNo = Convert.ToString(HttpContext.Current.Session["DefaultAuthority"]);
            if (string.IsNullOrWhiteSpace(autIntNo)) return false;

            var autName = Convert.ToString(HttpContext.Current.Session["autName"]);
            if (string.IsNullOrWhiteSpace(autName)) return false;

            var query = new AartoPageListQuery();
            query.Append(AartoPageListColumn.IsAarto, false.ToString());
            query.Append(AartoPageListColumn.AaPageUrl, pageName);
            var page = pageService.Find(query).FirstOrDefault();
            if (page == null) return false;

            var url = string.Format("{0}/AutoLogin.aspx?Key={1}&PageID={2}&AutIntNo={3}&AuthName={4}",
                tmsDomain, key, page.AaPageId, autIntNo, autName);

            var match = new Regex(@"(?'pageName'\w*.aspx)").Match(pageName);
            if (!match.Success) return false;
            var name = match.Groups["pageName"].Value;

            for (var i = 0; i < Retry; i++)
            {
                if (OpenUrl(url, hwr =>
                {
                    using (var sr = new StreamReader(hwr.GetResponseStream()))
                        return sr.ReadToEnd().IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0;
                }))
                    return true;
                Thread.Sleep(Wait);
            }
            return false;
        }

        static bool OpenUrl(string url, Func<HttpWebResponse, bool> onResponse = null)
        {
            if (string.IsNullOrWhiteSpace(url))
                return false;
            try
            {
                var uri = new Uri(url, UriKind.RelativeOrAbsolute);

                #region post

                //var index = uri.AbsoluteUri.IndexOf("?", StringComparison.OrdinalIgnoreCase);
                //var page = index >= 0
                //    ? uri.AbsoluteUri.Substring(0, index) : uri.AbsoluteUri;
                //var data = !string.IsNullOrWhiteSpace(uri.Query) && uri.Query.Length > 1
                //    ? Encoding.UTF8.GetBytes(uri.Query.Remove(0, 1)) : new byte[0];

                #endregion

                var request = (HttpWebRequest)WebRequest.Create(uri);
                request.Headers.Add("Cookie", HttpContext.Current.Request.Headers["Cookie"]);
                request.Timeout = RequestTimeout;

                #region post

                //if (data.Length > 0)
                //{
                //    request.Method = "POST";
                //    request.ContentType = "application/x-www-form-urlencoded";
                //    request.ContentLength = data.Length;
                //    using (var reqStream = request.GetRequestStream())
                //        reqStream.Write(data, 0, data.Length);
                //}

                #endregion

                using (var response = (HttpWebResponse)request.GetResponse())
                    return onResponse == null || onResponse(response);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }

    [Serializable]
    public class DomainUrl
    {
        public string Url { get; set; }
        public long Expires { get; set; }
    }
}
