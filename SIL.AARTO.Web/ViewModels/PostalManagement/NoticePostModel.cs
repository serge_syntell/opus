﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIL.AARTO.Web.ViewModels.PostalManagement
{
    public class NoticePostModelList
    {
        public NoticePostModelList()
        {
            NoticePostModels = new List<NoticePostModel>();
        }
        public string SearchStr { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public string ErrorMsg { get; set; }
        public int PageIndex { get; set; }
        public SelectList PrintNamePrefixs1 { get; set; }
        public SelectList PrintNamePrefixs2 { get; set; }
        public IList<NoticePostModel> NoticePostModels { get; set; }

    }

    public class NoticePostModel
    {
        public string PrintFileName { get; set; }
        public string FirstPostedDate { get; set; }
        public string PrintDate { get; set; }
        public int NumberOfNotices { get; set; }
        public string IsFirst { get; set; }

    }


    public class PrintFileModelList
    {
        public PrintFileModelList()
        {
            PrintFileModels = new List<PrintFileModel>();
        }
        public string SearchStr { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public string ErrorMsg { get; set; }
        public int PageIndex { get; set; }
        public List<SelectListItem> AuthorityList { get; set; }
        public List<SelectListItem> DocumentTypeList { get; set; }
        public IList<PrintFileModel> PrintFileModels { get; set; }
        public int AutIntNo { get; set; }
        public string DocumentType { get; set; }

    }

    public class PrintFileModel
    {
        public string PrintFileName { get; set; }
        public int NumberOfNotices { get; set; }
        public string PrintDate { get; set; }
    }

    public class PrintFileJsonEntity
    {
        public int page { get; set; }
        public int AutIntNo { get; set; }
        public string DocumentType { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
        public string PrintFileName { get; set; }
        public List<string> PFList { get; set; }
    }
}