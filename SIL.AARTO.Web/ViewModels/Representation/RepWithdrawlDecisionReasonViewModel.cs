﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIL.AARTO.Web.ViewModels.Representation
{
    public class RepWithdrawlDecisionReasonViewModel 
    {

        public int RWRIntNo { get; set; }
        public string RWRName { get; set; }
        public string RWRDescription { get; set; }
        public bool IsEnabled { get; set; }
    }
}