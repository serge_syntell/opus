﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SIL.AARTO.Web.Resource.StreetCoding;

namespace SIL.AARTO.Web.ViewModels.StreetCoding
{
    public class AreaCodeGroupManageModel
    {
        public AreaCodeGroupManageModel()
        {
            EntityList = new List<AreaCodeGroupEntity>();
        }

        public AreaCodeGroupEntity Entity { get; set; }
        public int? EntityId { get; set; }
        public byte[] EntityVersion { get; set; }
        public PageAction? Action { get; set; }
        public bool EditorVisible { get; set; }

        public List<AreaCodeGroupEntity> EntityList { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
    }

    public class AreaCodeGroupEntity
    {
        public int ACGIntNo { get; set; }

        [Required(ErrorMessageResourceType = typeof(AreaCodeGroupManage_cshtml), ErrorMessageResourceName = "PleaseEnterGroupName")]
        public string GroupName { get; set; }

        [Required(ErrorMessageResourceType = typeof(AreaCodeGroupManage_cshtml), ErrorMessageResourceName = "PleaseEnterCodeStart")]
        [Remote("CheckCodeStart", "StreetCoding", HttpMethod = "POST", ErrorMessageResourceType = typeof(AreaCodeGroupManage_cshtml), ErrorMessageResourceName = "PleaseEnterValidAreaCode")]
        public string CodeStart { get; set; }

        [Required(ErrorMessageResourceType = typeof(AreaCodeGroupManage_cshtml), ErrorMessageResourceName = "PleaseEnterCodeEnd")]
        [Remote("CheckCodeEnd", "StreetCoding", HttpMethod = "POST", ErrorMessageResourceType = typeof(AreaCodeGroupManage_cshtml), ErrorMessageResourceName = "PleaseEnterValidAreaCode")]
        public string CodeEnd { get; set; }

        public byte[] RowVersion { get; set; }
    }

    public enum PageAction
    {
        Save,
        Delete
    }

    public enum DBAction
    {
        Insert,
        Update,
        Delete
    }
}
