﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIL.AARTO.Web.ViewModels.MobileReport
{
    public class NoticeReceivedFromMobileDeviceModel
    {
        //pagination
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int TotalCount { get; set; }
        public string Msg { get; set; }

        //search condition
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public int TrafficeOfficerID { get; set; }
        public int DocumentTypeID { get; set; }
        public int DocumentStatusID { get; set; }

        //DropDownList
        public SelectList TrafficeOfficerList { get; set; }
        public SelectList DocumentTypeList { get; set; }
        public SelectList DocumentStatusList { get; set; }

        public List<NoticeReceivedFromMobileDeviceListModel> PageReportList { get; set; }
    }

    public class NoticeReceivedFromMobileDeviceListModel
    {
        public string MoDeIMEI { get; set; }
        public string OfficerCode { get; set; }
        public string OfficerName { get; set; }
        public string OffenceDate { get; set; }
        public string OffenceTime { get; set; }
        public string NoticeNumber { get; set; }
        public string GPS { get; set; }
        public string TransactionUploadDate { get; set; }
        public string DSDescription { get; set; }
        public string DTDescription { get; set; }
        public string IsComplete { get; set; }
        public string IsPrinted { get; set; }
    }
}