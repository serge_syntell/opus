﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.Web.Resource.Admin;

namespace SIL.AARTO.Web.ViewModels.ChangeOfOwner
{
    public class AuthorisationModel
    {
        [Required(ErrorMessage = "*")]
        public int SearchCCIIntNo { get; set; }
        [Required(ErrorMessage = "*")]
        public int SearchCCPIntNo { get; set; }
        public int ShowAffidavit { get; set; }
        public int IsExistAffidavit { get; set; }
        public int ISAccepted { get; set; }

        public SelectList SearchCompanyList { get; set; }
        public SelectList SearchProxyList { get; set; }

        public string ErrorMsg { get; set; }

        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public string URLPara { get; set; }
        public string LastUser { get; set; }
        public int Page { get; set; }

        public List<AuthorisationDetail> AuthorisationDetail { get; set; }

        public AuthorisationModel()
        {
            AuthorisationDetail = new List<AuthorisationDetail>();
        }

    }

    public class AuthorisationDetail
    {
        public string CFCAIntNo { get; set; }
        public string NotIntNo { get; set; }
        public string NotTicketNo { get; set; }
        public string NotRegNo { get; set; }
        public string NotOffenceDate { get; set; }
        public string CompanyName { get; set; }
        public string ProxyIDNumber { get; set; }

        // 2014-05-16, Oscar added
        public string DrvIDNumber { get; set; }

        public string DrvForenames { get; set; }
        public string PhysicalAddress1 { get; set; }
        public string CACDescription { get; set; }
    }
}