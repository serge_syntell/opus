﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIL.AARTO.Web.ViewModels.SummonsManagement
{
    public class StagnantSummonsModel
    {
        public StagnantSummonsModel() { ProcessNumber = 1000; }

        public string NotTicketNo { get; set; }
        public string SummonsNo { get; set; }
        public int CrtIntNo { get; set; }
        public int CrtRIntNo { get; set; }
        public int ProcessNumber { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public SelectList Courts { get; set; }
        public SelectList CourtRooms { get; set; }

    }
}