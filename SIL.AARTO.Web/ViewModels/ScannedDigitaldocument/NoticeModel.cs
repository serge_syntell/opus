﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIL.AARTO.Web.ViewModels.ScannedDigitaldocument
{
    public class NoticeModel
    {     
      	/// <summary>
		/// NotIntNo
        /// </summary>		
		private int _notintno;
        public int NotIntNo
        {
            get{ return _notintno; }
            set{ _notintno = value; }
        }        
		/// <summary>
		/// NotOffenceType
        /// </summary>		
		private string _notoffencetype;
        public string NotOffenceType
        {
            get{ return _notoffencetype; }
            set{ _notoffencetype = value; }
        }        
		/// <summary>
		/// NotTicketNo
        /// </summary>		
		private string _notticketno;
        public string NotTicketNo
        {
            get{ return _notticketno; }
            set{ _notticketno = value; }
        }        
		/// <summary>
		/// NotLocDescr
        /// </summary>		
		private string _notlocdescr;
        public string NotLocDescr
        {
            get{ return _notlocdescr; }
            set{ _notlocdescr = value; }
        }        
		/// <summary>
		/// NotLocCode
        /// </summary>		
		private string _notloccode;
        public string NotLocCode
        {
            get{ return _notloccode; }
            set{ _notloccode = value; }
        }        
		/// <summary>
		/// NotSpeedLimit
        /// </summary>		
		private int _notspeedlimit;
        public int NotSpeedLimit
        {
            get{ return _notspeedlimit; }
            set{ _notspeedlimit = value; }
        }        
		/// <summary>
		/// NotLaneNo
        /// </summary>		
		private string _notlaneno;
        public string NotLaneNo
        {
            get{ return _notlaneno; }
            set{ _notlaneno = value; }
        }        
		/// <summary>
		/// NotCameraID
        /// </summary>		
		private string _notcameraid;
        public string NotCameraID
        {
            get{ return _notcameraid; }
            set{ _notcameraid = value; }
        }        
		/// <summary>
		/// NotOfficerSName
        /// </summary>		
		private string _notofficersname;
        public string NotOfficerSName
        {
            get{ return _notofficersname; }
            set{ _notofficersname = value; }
        }        
		/// <summary>
		/// NotOfficerInit
        /// </summary>		
		private string _notofficerinit;
        public string NotOfficerInit
        {
            get{ return _notofficerinit; }
            set{ _notofficerinit = value; }
        }        
		/// <summary>
		/// NotOfficerNo
        /// </summary>		
		private string _notofficerno;
        public string NotOfficerNo
        {
            get{ return _notofficerno; }
            set{ _notofficerno = value; }
        }        
		/// <summary>
		/// NotOfficerGroup
        /// </summary>		
		private string _notofficergroup;
        public string NotOfficerGroup
        {
            get{ return _notofficergroup; }
            set{ _notofficergroup = value; }
        }        
		/// <summary>
		/// NotNaTISIndicator
        /// </summary>		
		private string _notnatisindicator;
        public string NotNaTISIndicator
        {
            get{ return _notnatisindicator; }
            set{ _notnatisindicator = value; }
        }        
		/// <summary>
		/// NotNaTISErrorFieldList
        /// </summary>		
		private string _notnatiserrorfieldlist;
        public string NotNaTISErrorFieldList
        {
            get{ return _notnatiserrorfieldlist; }
            set{ _notnatiserrorfieldlist = value; }
        }        
		/// <summary>
		/// NotNaTISRegNo
        /// </summary>		
		private string _notnatisregno;
        public string NotNaTISRegNo
        {
            get{ return _notnatisregno; }
            set{ _notnatisregno = value; }
        }        
		/// <summary>
		/// NotRegNo
        /// </summary>		
		private string _notregno;
        public string NotRegNo
        {
            get{ return _notregno; }
            set{ _notregno = value; }
        }        
		/// <summary>
		/// OrigRegNo
        /// </summary>		
		private string _origregno;
        public string OrigRegNo
        {
            get{ return _origregno; }
            set{ _origregno = value; }
        }        
		/// <summary>
		/// NotVehicleRegisterNo
        /// </summary>		
		private string _notvehicleregisterno;
        public string NotVehicleRegisterNo
        {
            get{ return _notvehicleregisterno; }
            set{ _notvehicleregisterno = value; }
        }        
		/// <summary>
		/// NotVehicleLicenceExpiry
        /// </summary>		
		private DateTime _notvehiclelicenceexpiry;
        public DateTime NotVehicleLicenceExpiry
        {
            get{ return _notvehiclelicenceexpiry; }
            set{ _notvehiclelicenceexpiry = value; }
        }        
		/// <summary>
		/// NotDateCOO
        /// </summary>		
		private DateTime? _notdatecoo;
        public DateTime? NotDateCOO
        {
            get{ return _notdatecoo; }
            set{ _notdatecoo = value; }
        }        
		/// <summary>
		/// NotClearanceCert
        /// </summary>		
		private string _notclearancecert;
        public string NotClearanceCert
        {
            get{ return _notclearancecert; }
            set{ _notclearancecert = value; }
        }        
		/// <summary>
		/// NotRegisterAuth
        /// </summary>		
		private string _notregisterauth;
        public string NotRegisterAuth
        {
            get{ return _notregisterauth; }
            set{ _notregisterauth = value; }
        }        
		/// <summary>
		/// NotSpeed1
        /// </summary>		
		private int _notspeed1;
        public int NotSpeed1
        {
            get{ return _notspeed1; }
            set{ _notspeed1 = value; }
        }        
		/// <summary>
		/// NotSpeed2
        /// </summary>		
		private int _notspeed2;
        public int NotSpeed2
        {
            get{ return _notspeed2; }
            set{ _notspeed2 = value; }
        }        
		/// <summary>
		/// NotVehicleDescr
        /// </summary>		
		private string _notvehicledescr;
        public string NotVehicleDescr
        {
            get{ return _notvehicledescr; }
            set{ _notvehicledescr = value; }
        }        
		/// <summary>
		/// NotVehicleCat
        /// </summary>		
		private string _notvehiclecat;
        public string NotVehicleCat
        {
            get{ return _notvehiclecat; }
            set{ _notvehiclecat = value; }
        }        
		/// <summary>
		/// NotNaTISVMCode
        /// </summary>		
		private string _notnatisvmcode;
        public string NotNaTISVMCode
        {
            get{ return _notnatisvmcode; }
            set{ _notnatisvmcode = value; }
        }        
		/// <summary>
		/// NotVehicleMakeCode
        /// </summary>		
		private string _notvehiclemakecode;
        public string NotVehicleMakeCode
        {
            get{ return _notvehiclemakecode; }
            set{ _notvehiclemakecode = value; }
        }        
		/// <summary>
		/// NotVehicleMake
        /// </summary>		
		private string _notvehiclemake;
        public string NotVehicleMake
        {
            get{ return _notvehiclemake; }
            set{ _notvehiclemake = value; }
        }        
		/// <summary>
		/// NotNaTISVTCode
        /// </summary>		
		private string _notnatisvtcode;
        public string NotNaTISVTCode
        {
            get{ return _notnatisvtcode; }
            set{ _notnatisvtcode = value; }
        }        
		/// <summary>
		/// NotVehicleTypeCode
        /// </summary>		
		private string _notvehicletypecode;
        public string NotVehicleTypeCode
        {
            get{ return _notvehicletypecode; }
            set{ _notvehicletypecode = value; }
        }        
		/// <summary>
		/// NotVehicleType
        /// </summary>		
		private string _notvehicletype;
        public string NotVehicleType
        {
            get{ return _notvehicletype; }
            set{ _notvehicletype = value; }
        }        
		/// <summary>
		/// NotVehicleUsage
        /// </summary>		
		private string _notvehicleusage;
        public string NotVehicleUsage
        {
            get{ return _notvehicleusage; }
            set{ _notvehicleusage = value; }
        }        
		/// <summary>
		/// NotVehicleColour
        /// </summary>		
		private string _notvehiclecolour;
        public string NotVehicleColour
        {
            get{ return _notvehiclecolour; }
            set{ _notvehiclecolour = value; }
        }        
		/// <summary>
		/// NotVINorChassis
        /// </summary>		
		private string _notvinorchassis;
        public string NotVINorChassis
        {
            get{ return _notvinorchassis; }
            set{ _notvinorchassis = value; }
        }        
		/// <summary>
		/// NotEngine
        /// </summary>		
		private string _notengine;
        public string NotEngine
        {
            get{ return _notengine; }
            set{ _notengine = value; }
        }        
		/// <summary>
		/// NotStatutoryOwner
        /// </summary>		
		private string _notstatutoryowner;
        public string NotStatutoryOwner
        {
            get{ return _notstatutoryowner; }
            set{ _notstatutoryowner = value; }
        }        
		/// <summary>
		/// NotNatureOfPerson
        /// </summary>		
		private string _notnatureofperson;
        public string NotNatureOfPerson
        {
            get{ return _notnatureofperson; }
            set{ _notnatureofperson = value; }
        }        
		/// <summary>
		/// NotOffenderType
        /// </summary>		
		private string _notoffendertype;
        public string NotOffenderType
        {
            get{ return _notoffendertype; }
            set{ _notoffendertype = value; }
        }        
		/// <summary>
		/// NotProxyFlag
        /// </summary>		
		private string _notproxyflag;
        public string NotProxyFlag
        {
            get{ return _notproxyflag; }
            set{ _notproxyflag = value; }
        }        
		/// <summary>
		/// NotFilmNo
        /// </summary>		
		private string _notfilmno;
        public string NotFilmNo
        {
            get{ return _notfilmno; }
            set{ _notfilmno = value; }
        }        
		/// <summary>
		/// NotFrameNo
        /// </summary>		
		private string _notframeno;
        public string NotFrameNo
        {
            get{ return _notframeno; }
            set{ _notframeno = value; }
        }        
		/// <summary>
		/// NotRefNo
        /// </summary>		
		private string _notrefno;
        public string NotRefNo
        {
            get{ return _notrefno; }
            set{ _notrefno = value; }
        }        
		/// <summary>
		/// NotSeqNo
        /// </summary>		
		private string _notseqno;
        public string NotSeqNo
        {
            get{ return _notseqno; }
            set{ _notseqno = value; }
        }        
		/// <summary>
		/// NotOffenceDate
        /// </summary>		
		private DateTime _notoffencedate;
        public DateTime NotOffenceDate
        {
            get{ return _notoffencedate; }
            set{ _notoffencedate = value; }
        }        
		/// <summary>
		/// NotPosted1stNoticeDate
        /// </summary>		
		private DateTime _notposted1stnoticedate;
        public DateTime NotPosted1stNoticeDate
        {
            get{ return _notposted1stnoticedate; }
            set{ _notposted1stnoticedate = value; }
        }        
		/// <summary>
		/// NotPosted2ndNoticeDate
        /// </summary>		
		private DateTime _notposted2ndnoticedate;
        public DateTime NotPosted2ndNoticeDate
        {
            get{ return _notposted2ndnoticedate; }
            set{ _notposted2ndnoticedate = value; }
        }        
		/// <summary>
		/// NotPostedSummonsDate
        /// </summary>		
		private DateTime _notpostedsummonsdate;
        public DateTime NotPostedSummonsDate
        {
            get{ return _notpostedsummonsdate; }
            set{ _notpostedsummonsdate = value; }
        }        
		/// <summary>
		/// NotSource
        /// </summary>		
		private string _notsource;
        public string NotSource
        {
            get{ return _notsource; }
            set{ _notsource = value; }
        }        
		/// <summary>
		/// NotRdTypeCode
        /// </summary>		
		private int _notrdtypecode;
        public int NotRdTypeCode
        {
            get{ return _notrdtypecode; }
            set{ _notrdtypecode = value; }
        }        
		/// <summary>
		/// NotRdTypeDescr
        /// </summary>		
		private string _notrdtypedescr;
        public string NotRdTypeDescr
        {
            get{ return _notrdtypedescr; }
            set{ _notrdtypedescr = value; }
        }        
		/// <summary>
		/// NotTravelDirection
        /// </summary>		
		private string _nottraveldirection;
        public string NotTravelDirection
        {
            get{ return _nottraveldirection; }
            set{ _nottraveldirection = value; }
        }        
		/// <summary>
		/// NotCourtNo
        /// </summary>		
		private string _notcourtno;
        public string NotCourtNo
        {
            get{ return _notcourtno; }
            set{ _notcourtno = value; }
        }        
		/// <summary>
		/// NotCourtName
        /// </summary>		
		private string _notcourtname;
        public string NotCourtName
        {
            get{ return _notcourtname; }
            set{ _notcourtname = value; }
        }        
		/// <summary>
		/// NotElapsedTime
        /// </summary>		
		private string _notelapsedtime;
        public string NotElapsedTime
        {
            get{ return _notelapsedtime; }
            set{ _notelapsedtime = value; }
        }        
		/// <summary>
		/// NotPaymentDate
        /// </summary>		
		private DateTime _notpaymentdate;
        public DateTime NotPaymentDate
        {
            get{ return _notpaymentdate; }
            set{ _notpaymentdate = value; }
        }        
		/// <summary>
		/// NotIssue1stNoticeDate
        /// </summary>		
		private DateTime _notissue1stnoticedate;
        public DateTime NotIssue1stNoticeDate
        {
            get{ return _notissue1stnoticedate; }
            set{ _notissue1stnoticedate = value; }
        }        
		/// <summary>
		/// NotIssue2ndNoticeDate
        /// </summary>		
		private DateTime _notissue2ndnoticedate;
        public DateTime NotIssue2ndNoticeDate
        {
            get{ return _notissue2ndnoticedate; }
            set{ _notissue2ndnoticedate = value; }
        }        
		/// <summary>
		/// NotPrint1stNoticeDate
        /// </summary>		
		private DateTime _notprint1stnoticedate;
        public DateTime NotPrint1stNoticeDate
        {
            get{ return _notprint1stnoticedate; }
            set{ _notprint1stnoticedate = value; }
        }        
		/// <summary>
		/// NotPrint2ndNoticeDate
        /// </summary>		
		private DateTime _notprint2ndnoticedate;
        public DateTime NotPrint2ndNoticeDate
        {
            get{ return _notprint2ndnoticedate; }
            set{ _notprint2ndnoticedate = value; }
        }        
		/// <summary>
		/// NotPrintSummonsDate
        /// </summary>		
		private DateTime _notprintsummonsdate;
        public DateTime NotPrintSummonsDate
        {
            get{ return _notprintsummonsdate; }
            set{ _notprintsummonsdate = value; }
        }        
		/// <summary>
		/// LastUser
        /// </summary>		
		private string _lastuser;
        public string LastUser
        {
            get{ return _lastuser; }
            set{ _lastuser = value; }
        }        
		/// <summary>
		/// RowVersion
        /// </summary>		
		private DateTime _rowversion;
        public DateTime RowVersion
        {
            get{ return _rowversion; }
            set{ _rowversion = value; }
        }        
		/// <summary>
		/// AutIntNo
        /// </summary>		
		private int _autintno;
        public int AutIntNo
        {
            get{ return _autintno; }
            set{ _autintno = value; }
        }        
		/// <summary>
		/// NotLoadDate
        /// </summary>		
		private DateTime _notloaddate;
        public DateTime NotLoadDate
        {
            get{ return _notloaddate; }
            set{ _notloaddate = value; }
        }        
		/// <summary>
		/// NotNatisINPName
        /// </summary>		
		private string _notnatisinpname;
        public string NotNatisINPName
        {
            get{ return _notnatisinpname; }
            set{ _notnatisinpname = value; }
        }        
		/// <summary>
		/// NotNatisINPDate
        /// </summary>		
		private DateTime _notnatisinpdate;
        public DateTime NotNatisINPDate
        {
            get{ return _notnatisinpdate; }
            set{ _notnatisinpdate = value; }
        }        
		/// <summary>
		/// NotNatisOUTName
        /// </summary>		
		private string _notnatisoutname;
        public string NotNatisOUTName
        {
            get{ return _notnatisoutname; }
            set{ _notnatisoutname = value; }
        }        
		/// <summary>
		/// NotNatisOUTDate
        /// </summary>		
		private DateTime _notnatisoutdate;
        public DateTime NotNatisOUTDate
        {
            get{ return _notnatisoutdate; }
            set{ _notnatisoutdate = value; }
        }        
		/// <summary>
		/// NotCiprusPrintReqName
        /// </summary>		
		private string _notciprusprintreqname;
        public string NotCiprusPrintReqName
        {
            get{ return _notciprusprintreqname; }
            set{ _notciprusprintreqname = value; }
        }        
		/// <summary>
		/// NotCiprusPrintReqDate
        /// </summary>		
		private DateTime _notciprusprintreqdate;
        public DateTime NotCiprusPrintReqDate
        {
            get{ return _notciprusprintreqdate; }
            set{ _notciprusprintreqdate = value; }
        }        
		/// <summary>
		/// ParkMeterNo
        /// </summary>		
		private string _parkmeterno;
        public string ParkMeterNo
        {
            get{ return _parkmeterno; }
            set{ _parkmeterno = value; }
        }        
		/// <summary>
		/// AutName
        /// </summary>		
		private string _autname;
        public string AutName
        {
            get{ return _autname; }
            set{ _autname = value; }
        }        
		/// <summary>
		/// IssuedBy
        /// </summary>		
		private string _issuedby;
        public string IssuedBy
        {
            get{ return _issuedby; }
            set{ _issuedby = value; }
        }        
		/// <summary>
		/// CrtPaymentAddr1
        /// </summary>		
		private string _crtpaymentaddr1;
        public string CrtPaymentAddr1
        {
            get{ return _crtpaymentaddr1; }
            set{ _crtpaymentaddr1 = value; }
        }        
		/// <summary>
		/// CrtPaymentAddr2
        /// </summary>		
		private string _crtpaymentaddr2;
        public string CrtPaymentAddr2
        {
            get{ return _crtpaymentaddr2; }
            set{ _crtpaymentaddr2 = value; }
        }        
		/// <summary>
		/// CrtPaymentAddr3
        /// </summary>		
		private string _crtpaymentaddr3;
        public string CrtPaymentAddr3
        {
            get{ return _crtpaymentaddr3; }
            set{ _crtpaymentaddr3 = value; }
        }        
		/// <summary>
		/// CrtPaymentAddr4
        /// </summary>		
		private string _crtpaymentaddr4;
        public string CrtPaymentAddr4
        {
            get{ return _crtpaymentaddr4; }
            set{ _crtpaymentaddr4 = value; }
        }        
		/// <summary>
		/// NotCiprusPrintConfirmSendDate
        /// </summary>		
		private DateTime _notciprusprintconfirmsenddate;
        public DateTime NotCiprusPrintConfirmSendDate
        {
            get{ return _notciprusprintconfirmsenddate; }
            set{ _notciprusprintconfirmsenddate = value; }
        }        
		/// <summary>
		/// NotDocType
        /// </summary>		
		private string _notdoctype;
        public string NotDocType
        {
            get{ return _notdoctype; }
            set{ _notdoctype = value; }
        }        
		/// <summary>
		/// NotSummonsPrintFileName
        /// </summary>		
		private string _notsummonsprintfilename;
        public string NotSummonsPrintFileName
        {
            get{ return _notsummonsprintfilename; }
            set{ _notsummonsprintfilename = value; }
        }        
		/// <summary>
		/// NotSendTo
        /// </summary>		
		private string _notsendto;
        public string NotSendTo
        {
            get{ return _notsendto; }
            set{ _notsendto = value; }
        }        
		/// <summary>
		/// NotCamSerialNo
        /// </summary>		
		private string _notcamserialno;
        public string NotCamSerialNo
        {
            get{ return _notcamserialno; }
            set{ _notcamserialno = value; }
        }        
		/// <summary>
		/// NotEasyPayNumber
        /// </summary>		
		private string _noteasypaynumber;
        public string NotEasyPayNumber
        {
            get{ return _noteasypaynumber; }
            set{ _noteasypaynumber = value; }
        }        
		/// <summary>
		/// ExportFlag
        /// </summary>		
		private string _exportflag;
        public string ExportFlag
        {
            get{ return _exportflag; }
            set{ _exportflag = value; }
        }        
		/// <summary>
		/// Not2ndNoticePrintFile
        /// </summary>		
		private string _not2ndnoticeprintfile;
        public string Not2ndNoticePrintFile
        {
            get{ return _not2ndnoticeprintfile; }
            set{ _not2ndnoticeprintfile = value; }
        }        
		/// <summary>
		/// NotCiprusPIContraventionFile
        /// </summary>		
		private string _notcipruspicontraventionfile;
        public string NotCiprusPIContraventionFile
        {
            get{ return _notcipruspicontraventionfile; }
            set{ _notcipruspicontraventionfile = value; }
        }        
		/// <summary>
		/// NotCiprusPIContraventionDate
        /// </summary>		
		private DateTime _notcipruspicontraventiondate;
        public DateTime NotCiprusPIContraventionDate
        {
            get{ return _notcipruspicontraventiondate; }
            set{ _notcipruspicontraventiondate = value; }
        }        
		/// <summary>
		/// NotCiprusPIProcessCode
        /// </summary>		
		private int _notcipruspiprocesscode;
        public int NotCiprusPIProcessCode
        {
            get{ return _notcipruspiprocesscode; }
            set{ _notcipruspiprocesscode = value; }
        }        
		/// <summary>
		/// NotCiprusPISeverityCode
        /// </summary>		
		private int _notcipruspiseveritycode;
        public int NotCiprusPISeverityCode
        {
            get{ return _notcipruspiseveritycode; }
            set{ _notcipruspiseveritycode = value; }
        }        
		/// <summary>
		/// NotCiprusPIProcessDescr
        /// </summary>		
		private string _notcipruspiprocessdescr;
        public string NotCiprusPIProcessDescr
        {
            get{ return _notcipruspiprocessdescr; }
            set{ _notcipruspiprocessdescr = value; }
        }        
		/// <summary>
		/// NotNatisProxyIndicator
        /// </summary>		
		private string _notnatisproxyindicator;
        public string NotNatisProxyIndicator
        {
            get{ return _notnatisproxyindicator; }
            set{ _notnatisproxyindicator = value; }
        }        
		/// <summary>
		/// NotNewOffender
        /// </summary>		
		private string _notnewoffender;
        public string NotNewOffender
        {
            get{ return _notnewoffender; }
            set{ _notnewoffender = value; }
        }        
		/// <summary>
		/// NotPosted1stNoticeUser
        /// </summary>		
		private string _notposted1stnoticeuser;
        public string NotPosted1stNoticeUser
        {
            get{ return _notposted1stnoticeuser; }
            set{ _notposted1stnoticeuser = value; }
        }        
		/// <summary>
		/// NotPosted2ndNoticeUser
        /// </summary>		
		private string _notposted2ndnoticeuser;
        public string NotPosted2ndNoticeUser
        {
            get{ return _notposted2ndnoticeuser; }
            set{ _notposted2ndnoticeuser = value; }
        }        
		/// <summary>
		/// NotPosted1stNoticeCaptureDate
        /// </summary>		
		private DateTime _notposted1stnoticecapturedate;
        public DateTime NotPosted1stNoticeCaptureDate
        {
            get{ return _notposted1stnoticecapturedate; }
            set{ _notposted1stnoticecapturedate = value; }
        }        
		/// <summary>
		/// NotPosted2ndNoticeCaptureDate
        /// </summary>		
		private DateTime _notposted2ndnoticecapturedate;
        public DateTime NotPosted2ndNoticeCaptureDate
        {
            get{ return _notposted2ndnoticecapturedate; }
            set{ _notposted2ndnoticecapturedate = value; }
        }        
		/// <summary>
		/// NotPostedSummonsCaptureDate
        /// </summary>		
		private DateTime _notpostedsummonscapturedate;
        public DateTime NotPostedSummonsCaptureDate
        {
            get{ return _notpostedsummonscapturedate; }
            set{ _notpostedsummonscapturedate = value; }
        }        
		/// <summary>
		/// PayfineExtracted
        /// </summary>		
		private string _payfineextracted;
        public string PayfineExtracted
        {
            get{ return _payfineextracted; }
            set{ _payfineextracted = value; }
        }        
		/// <summary>
		/// NewOffenderPrintFileName
        /// </summary>		
		private string _newoffenderprintfilename;
        public string NewOffenderPrintFileName
        {
            get{ return _newoffenderprintfilename; }
            set{ _newoffenderprintfilename = value; }
        }        
		/// <summary>
		/// NewOffenderPrintedDate
        /// </summary>		
		private DateTime _newoffenderprinteddate;
        public DateTime NewOffenderPrintedDate
        {
            get{ return _newoffenderprinteddate; }
            set{ _newoffenderprinteddate = value; }
        }        
		/// <summary>
		/// NewOffenderPostedDate
        /// </summary>		
		private DateTime _newoffenderposteddate;
        public DateTime NewOffenderPostedDate
        {
            get{ return _newoffenderposteddate; }
            set{ _newoffenderposteddate = value; }
        }        
		/// <summary>
		/// NewOffenderPrintedUser
        /// </summary>		
		private string _newoffenderprinteduser;
        public string NewOffenderPrintedUser
        {
            get{ return _newoffenderprinteduser; }
            set{ _newoffenderprinteduser = value; }
        }        
		/// <summary>
		/// NewOffenderPostedUser
        /// </summary>		
		private string _newoffenderposteduser;
        public string NewOffenderPostedUser
        {
            get{ return _newoffenderposteduser; }
            set{ _newoffenderposteduser = value; }
        }        
		/// <summary>
		/// Not2ndPaymentDate
        /// </summary>		
		private DateTime _not2ndpaymentdate;
        public DateTime Not2ndPaymentDate
        {
            get{ return _not2ndpaymentdate; }
            set{ _not2ndpaymentdate = value; }
        }        
		/// <summary>
		/// N = None, D = Duplicate number plate, I = Incorrect number plate, L = Natis on Previous / Licence not updated, X = no details supplied
        /// </summary>		
		private string _notnewregnotype;
        public string NotNewRegNoType
        {
            get{ return _notnewregnotype; }
            set{ _notnewregnotype = value; }
        }        
		/// <summary>
		/// NewRegNoPrintFileName
        /// </summary>		
		private string _newregnoprintfilename;
        public string NewRegNoPrintFileName
        {
            get{ return _newregnoprintfilename; }
            set{ _newregnoprintfilename = value; }
        }        
		/// <summary>
		/// NewRegNoPrintedDate
        /// </summary>		
		private DateTime _newregnoprinteddate;
        public DateTime NewRegNoPrintedDate
        {
            get{ return _newregnoprinteddate; }
            set{ _newregnoprinteddate = value; }
        }        
		/// <summary>
		/// NewRegNoPostedDate
        /// </summary>		
		private DateTime _newregnoposteddate;
        public DateTime NewRegNoPostedDate
        {
            get{ return _newregnoposteddate; }
            set{ _newregnoposteddate = value; }
        }        
		/// <summary>
		/// NewRegNoPrintedUser
        /// </summary>		
		private string _newregnoprinteduser;
        public string NewRegNoPrintedUser
        {
            get{ return _newregnoprinteduser; }
            set{ _newregnoprinteduser = value; }
        }        
		/// <summary>
		/// NewRegNoPostedUser
        /// </summary>		
		private string _newregnoposteduser;
        public string NewRegNoPostedUser
        {
            get{ return _newregnoposteduser; }
            set{ _newregnoposteduser = value; }
        }        
		/// <summary>
		/// NewVehicleMakeCode
        /// </summary>		
		private string _newvehiclemakecode;
        public string NewVehicleMakeCode
        {
            get{ return _newvehiclemakecode; }
            set{ _newvehiclemakecode = value; }
        }        
		/// <summary>
		/// NewVehicleMake
        /// </summary>		
		private string _newvehiclemake;
        public string NewVehicleMake
        {
            get{ return _newvehiclemake; }
            set{ _newvehiclemake = value; }
        }        
		/// <summary>
		/// NewVehicleTypeCode
        /// </summary>		
		private string _newvehicletypecode;
        public string NewVehicleTypeCode
        {
            get{ return _newvehicletypecode; }
            set{ _newvehicletypecode = value; }
        }        
		/// <summary>
		/// NewVehicleType
        /// </summary>		
		private string _newvehicletype;
        public string NewVehicleType
        {
            get{ return _newvehicletype; }
            set{ _newvehicletype = value; }
        }        
		/// <summary>
		/// NewVehicleColour
        /// </summary>		
		private string _newvehiclecolour;
        public string NewVehicleColour
        {
            get{ return _newvehiclecolour; }
            set{ _newvehiclecolour = value; }
        }        
		/// <summary>
		/// NewVehicleColourDescr
        /// </summary>		
		private string _newvehiclecolourdescr;
        public string NewVehicleColourDescr
        {
            get{ return _newvehiclecolourdescr; }
            set{ _newvehiclecolourdescr = value; }
        }        
		/// <summary>
		/// NewRegNoDetails
        /// </summary>		
		private string _newregnodetails;
        public string NewRegNoDetails
        {
            get{ return _newregnodetails; }
            set{ _newregnodetails = value; }
        }        
		/// <summary>
		/// Date_CIPRUS_Succ_Loadedfile
        /// </summary>		
		private DateTime _date_ciprus_succ_loadedfile;
        public DateTime Date_CIPRUS_Succ_Loadedfile
        {
            get{ return _date_ciprus_succ_loadedfile; }
            set{ _date_ciprus_succ_loadedfile = value; }
        }        
		/// <summary>
		/// Not1stBase64String
        /// </summary>		
		private string _not1stbase64string;
        public string Not1stBase64String
        {
            get{ return _not1stbase64string; }
            set{ _not1stbase64string = value; }
        }        
		/// <summary>
		/// Not2ndBase64String
        /// </summary>		
		private string _not2ndbase64string;
        public string Not2ndBase64String
        {
            get{ return _not2ndbase64string; }
            set{ _not2ndbase64string = value; }
        }        
		/// <summary>
		/// NotTicketProcessor
        /// </summary>		
		private string _notticketprocessor;
        public string NotTicketProcessor
        {
            get{ return _notticketprocessor; }
            set{ _notticketprocessor = value; }
        }        
		/// <summary>
		/// NotUniqueDocID
        /// </summary>		
		private string _notuniquedocid;
        public string NotUniqueDocID
        {
            get{ return _notuniquedocid; }
            set{ _notuniquedocid = value; }
        }        
		/// <summary>
		/// NotOffenceTypeOld
        /// </summary>		
		private string _notoffencetypeold;
        public string NotOffenceTypeOld
        {
            get{ return _notoffencetypeold; }
            set{ _notoffencetypeold = value; }
        }        
		/// <summary>
		/// NotExpireDate
        /// </summary>		
		private DateTime _notexpiredate;
        public DateTime NotExpireDate
        {
            get{ return _notexpiredate; }
            set{ _notexpiredate = value; }
        }        
		/// <summary>
		/// AdjOfficerNo
        /// </summary>		
		private string _adjofficerno;
        public string AdjOfficerNo
        {
            get{ return _adjofficerno; }
            set{ _adjofficerno = value; }
        }        
		/// <summary>
		/// AdjudicationOfficerInfrastructureNo
        /// </summary>		
		private string _adjudicationofficerinfrastructureno;
        public string AdjudicationOfficerInfrastructureNo
        {
            get{ return _adjudicationofficerinfrastructureno; }
            set{ _adjudicationofficerinfrastructureno = value; }
        }        
		/// <summary>
		/// AdjudicationOfficerSurName
        /// </summary>		
		private string _adjudicationofficersurname;
        public string AdjudicationOfficerSurName
        {
            get{ return _adjudicationofficersurname; }
            set{ _adjudicationofficersurname = value; }
        }        
		/// <summary>
		/// AdjudicationOfficerInitials
        /// </summary>		
		private string _adjudicationofficerinitials;
        public string AdjudicationOfficerInitials
        {
            get{ return _adjudicationofficerinitials; }
            set{ _adjudicationofficerinitials = value; }
        }        
		/// <summary>
		/// eNaTISGuid
        /// </summary>		
		private Guid _enatisguid;
        public Guid eNaTISGuid
        {
            get{ return _enatisguid; }
            set{ _enatisguid = value; }
        }        
		/// <summary>
		/// NotOriginalPrintDate
        /// </summary>		
		private DateTime _notoriginalprintdate;
        public DateTime NotOriginalPrintDate
        {
            get{ return _notoriginalprintdate; }
            set{ _notoriginalprintdate = value; }
        }        
		/// <summary>
		/// RequestSentToNTIDate
        /// </summary>		
		private DateTime _requestsenttontidate;
        public DateTime RequestSentToNTIDate
        {
            get{ return _requestsenttontidate; }
            set{ _requestsenttontidate = value; }
        }        
		/// <summary>
		/// ResponseReceivedFromNTIDate
        /// </summary>		
		private DateTime _responsereceivedfromntidate;
        public DateTime ResponseReceivedFromNTIDate
        {
            get{ return _responsereceivedfromntidate; }
            set{ _responsereceivedfromntidate = value; }
        }        
		/// <summary>
		/// NotLoctionInfringement
        /// </summary>		
		private string _notloctioninfringement;
        public string NotLoctionInfringement
        {
            get{ return _notloctioninfringement; }
            set{ _notloctioninfringement = value; }
        }        
		/// <summary>
		/// OperatorCardNo
        /// </summary>		
		private string _operatorcardno;
        public string OperatorCardNo
        {
            get{ return _operatorcardno; }
            set{ _operatorcardno = value; }
        }        
		/// <summary>
		/// NotIsOfficerError
        /// </summary>		
		private bool _notisofficererror;
        public bool NotIsOfficerError
        {
            get{ return _notisofficererror; }
            set{ _notisofficererror = value; }
        }        
		/// <summary>
		/// NotVehicleGVM
        /// </summary>		
		private int _notvehiclegvm;
        public int NotVehicleGVM
        {
            get{ return _notvehiclegvm; }
            set{ _notvehiclegvm = value; }
        }        
		/// <summary>
		/// NotFilmType
        /// </summary>		
		private string _notfilmtype;
        public string NotFilmType
        {
            get{ return _notfilmtype; }
            set{ _notfilmtype = value; }
        }        
		/// <summary>
		/// NotDMSCaptureClerk
        /// </summary>		
		private string _notdmscaptureclerk;
        public string NotDMSCaptureClerk
        {
            get{ return _notdmscaptureclerk; }
            set{ _notdmscaptureclerk = value; }
        }        
		/// <summary>
		/// NotDMSCaptureDate
        /// </summary>		
		private DateTime _notdmscapturedate;
        public DateTime NotDMSCaptureDate
        {
            get{ return _notdmscapturedate; }
            set{ _notdmscapturedate = value; }
        }        
		/// <summary>
		/// NotFEDFFileName
        /// </summary>		
		private string _notfedffilename;
        public string NotFEDFFileName
        {
            get{ return _notfedffilename; }
            set{ _notfedffilename = value; }
        }        
		/// <summary>
		/// NotSequenceNo
        /// </summary>		
		private long _notsequenceno;
        public long NotSequenceNo
        {
            get{ return _notsequenceno; }
            set{ _notsequenceno = value; }
        }        
		/// <summary>
		/// NotPrefix
        /// </summary>		
		private string _notprefix;
        public string NotPrefix
        {
            get{ return _notprefix; }
            set{ _notprefix = value; }
        }        
		/// <summary>
		/// NoticeStatus
        /// </summary>		
		private int _noticestatus;
        public int NoticeStatus
        {
            get{ return _noticestatus; }
            set{ _noticestatus = value; }
        }        
		/// <summary>
		/// NotPrevNoticeStatus
        /// </summary>		
		private int _notprevnoticestatus;
        public int NotPrevNoticeStatus
        {
            get{ return _notprevnoticestatus; }
            set{ _notprevnoticestatus = value; }
        }        
		/// <summary>
		/// OffenderDetailsChanged
        /// </summary>		
		private bool _offenderdetailschanged;
        public bool OffenderDetailsChanged
        {
            get{ return _offenderdetailschanged; }
            set{ _offenderdetailschanged = value; }
        }        
		/// <summary>
		/// NotManualCapture
        /// </summary>		
		private int _notmanualcapture;
        public int NotManualCapture
        {
            get{ return _notmanualcapture; }
            set{ _notmanualcapture = value; }
        }        
		/// <summary>
		/// IsVideo
        /// </summary>		
		private bool _isvideo;
        public bool IsVideo
        {
            get{ return _isvideo; }
            set{ _isvideo = value; }
        }        
		/// <summary>
		/// NotTicketNo_Enquiry
        /// </summary>		
		private string _notticketno_enquiry;
        public string NotTicketNo_Enquiry
        {
            get{ return _notticketno_enquiry; }
            set{ _notticketno_enquiry = value; }
        }        
		/// <summary>
		/// NotStatisticsCode
        /// </summary>		
		private string _notstatisticscode;
        public string NotStatisticsCode
        {
            get{ return _notstatisticscode; }
            set{ _notstatisticscode = value; }
        }        
		/// <summary>
		/// IsHWOCorrection
        /// </summary>		
		private bool _ishwocorrection;
        public bool IsHWOCorrection
        {
            get{ return _ishwocorrection; }
            set{ _ishwocorrection = value; }
        }        
		/// <summary>
		/// LoSuIntNo
        /// </summary>		
		private int _losuintno;
        public int LoSuIntNo
        {
            get{ return _losuintno; }
            set{ _losuintno = value; }
        }        
		/// <summary>
		/// AutoSendToDMSCounter
        /// </summary>		
		private int _autosendtodmscounter;
        public int AutoSendToDMSCounter
        {
            get{ return _autosendtodmscounter; }
            set{ _autosendtodmscounter = value; }
        }        
		/// <summary>
		/// NotScanDate
        /// </summary>		
		private DateTime _notscandate;
        public DateTime NotScanDate
        {
            get{ return _notscandate; }
            set{ _notscandate = value; }
        }        
		/// <summary>
		/// IsNoAOG
        /// </summary>		
		private bool _isnoaog;
        public bool IsNoAOG
        {
            get{ return _isnoaog; }
            set{ _isnoaog = value; }
        }        
		/// <summary>
		/// IsSection35
        /// </summary>		
		private bool _issection35;
        public bool IsSection35
        {
            get{ return _issection35; }
            set{ _issection35 = value; }
        }        
		/// <summary>
		/// RepCount
        /// </summary>		
		private int _repcount;
        public int RepCount
        {
            get{ return _repcount; }
            set{ _repcount = value; }
        }        
		/// <summary>
		/// PendingNewOffender
        /// </summary>		
		private bool _pendingnewoffender;
        public bool PendingNewOffender
        {
            get{ return _pendingnewoffender; }
            set{ _pendingnewoffender = value; }
        }

        public object Attachment { get; set; }
    }
}