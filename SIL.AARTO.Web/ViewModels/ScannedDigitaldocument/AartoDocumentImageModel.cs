﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIL.AARTO.Web.ViewModels.ScannedDigitaldocument
{
    public class AartoDocumentImageModel
    {
      public long AaDocImgID{get;set;}
      public long AaDocID{get;set;}
      public Int32 AaDocSourceTableID{get;set;}
      public Int32 AaDocImgOrder{get;set;}
      public string AaDocImgPath{get;set;}
      public DateTime AaDocImgDateLoaded{get;set;}
      public string LastUser{get;set;}
      public string AaDocImgTypeID { get; set; }
      public bool IsImage { get; set; }
    }
}