﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIL.AARTO.Web.ViewModels.QuoteManagement
{

    public class QuoteEnquiryManagementModel
    {
        public List<QuoteEnquiryModel> QuoteEnquiryList { get; set; }
        
        public string SearchAutIntNo { get; set; }
        public List<SelectListItem> SearchAuthorityList { get; set; }

        public string SearchUOMIntNo { get; set; }
        public List<SelectListItem> SearchUnitOfMeasureList { get; set; }

        public string SearchNTCCode { get; set; }

        public List<SelectListItem> UnitOfMeasureList { get; set; }

        public bool ShowReversalNTCButton { get; set; }
        

        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public string URLPara { get; set; }
        public string LastUser { get; set; }
        public int Page { get; set; }

        public string ErrorMsg { get; set; }


        public QuoteEnquiryManagementModel()
        {
            QuoteEnquiryList = new List<QuoteEnquiryModel>();
        }

    }

    public class QuoteEnquiryModel
    {
        public List<QuoteDetailModel> QuoteDetailList { get; set; }
        public List<ReceiptDetailModel> ReceiptDetailList { get; set; }

        public string QuHeIntNo { get; set; }
        public string QuHeReferenceNumber { get; set; }
        public string QuHeSurName { get; set; }
        public string QuHeInitials { get; set; }
        public string QuHeIDNumber { get; set; }
        public string QuHeDate { get; set; }
        public string QuHeAddress1 { get; set; }
        public string QuHeAddress2 { get; set; }
        public string QuHeAddress3 { get; set; }
        public string QuHeAddress4 { get; set; }
        public string QuHeAddress5 { get; set; }
        public string ReceiptAmount { get; set; }
        public string ReceiptNo { get; set; }
        public int RctIntNo { get; set; }
        public bool Reversed { get; set; } 

        public QuoteEnquiryModel()
        {
            QuoteDetailList = new List<QuoteDetailModel>();
            ReceiptDetailList = new List<ReceiptDetailModel>();
        }
    }

    public class CashBoxSelectModel {
        public CashBoxSelectModel() {
            CashBox = new List<CashBoxEntity>();
        }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public string URLPara { get; set; }
        public string LastUser { get; set; }
        public int Page { get; set; }

        public List<CashBoxEntity> CashBox { get; set; }
    }

    public class CashBoxEntity
    {
        public int CBIntNo { get; set; }
        public string CBName{get;set;}
        public double CBStartAmount { get; set; }
    }

    public class QuoteDetailModel
    {
        public string QuDeIntNo { get; set; }
        public string NTCCode { get; set; }
        public string NTCDescr { get; set; }
        public string NTCComment { get; set; }
        public string QuDeChargeQuantity { get; set; }
        public string MeasureUnit { get; set; }
        public string NTCStandardAmount { get; set; }
        public string QuDeActualAmount { get; set; }
        public string QuDeGrossAmount { get; set; }
        
    }

    public class ReceiptDetailModel
    {
        public string RTIntNo { get; set; }
        public string RTAmount { get; set; }
        public double RTGrossAmount { get; set; }
        public string RTFineAmount { get; set; }
        public string RTCashType { get; set; }
        public string NTCDescr { get; set; }
        public string RctNumber { get; set; }
        public string RTDetails { get; set; }
        
    }

   
}