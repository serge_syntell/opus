﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SIL.AARTO.Web.Resource.Admin;

namespace SIL.AARTO.Web.ViewModels.Admin
{
    public class FieldSheetReferenceModel
    {
        public string SearchFilmNo { get; set; }

        public bool IsHaveDate { get; set; }
        public string FilmIntNo { get; set; }
        public string FilmNo { get; set; }
        public string LocationCode { get; set; }
        public string LocationDescription { get; set; }
        public string OfficerNumber { get; set; }
        public string OfficerFullName { get; set; }
        public string OffenceDate { get; set; }
        public string ReferenceNumber { get; set; }

        public string ErrorMsg { get; set; }
    }
}