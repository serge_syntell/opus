﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SIL.AARTO.Web.Resource.Admin;

namespace SIL.AARTO.Web.ViewModels.Admin
{
    public class ChangeOfOwnerAutomationModel
    {
        public ChangeOfOwnerAutomationModel()
        {
            CompanyInfoList = new List<COOCompanyInformationEntity>();
            CompanyProxyList = new List<COOCompanyProxyEntity>();
        }

        public List<COOCompanyInformationEntity> CompanyInfoList { get; set; }
        public List<COOCompanyProxyEntity> CompanyProxyList { get; set; }

        public int SelectCCIIntNo { get; set; }
        public string SelectCompanyName { get; set; }
        public string SearchStr { get; set; }

        public int CCIIntNo { get; set; }
        [Required(ErrorMessage = "*")]
        public string CompanyName { get; set; }
        [Required(ErrorMessage = "*")]
        public string CompanyCode { get; set; }
        [Required(ErrorMessage = "*")]
        public string RegistrationNumber { get; set; }
        [Required(ErrorMessage = "*")]
        public string Telephone { get; set; }
        [Required(ErrorMessage = "*")]
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$", ErrorMessageResourceType = typeof(ChangeOfOwnerAutomation_cshtml), ErrorMessageResourceName = "EmailAddress_NotAvailable")]
        public string Email { get; set; }
        [Required(ErrorMessage = "*")]
        public string PhysicalAddress { get; set; }
        [Required(ErrorMessage = "*")]
        public string PostalAddress { get; set; }

        public int CCPIntNo { get; set; }
        public int CCIIntNo_PK { get; set; }
        [Required(ErrorMessage = "*")]
        [RegularExpression(@"^\d{13}$", ErrorMessageResourceType = typeof(ChangeOfOwnerAutomation_cshtml), ErrorMessageResourceName = "Number_NotAvailable")]
        public string CCPNumber { get; set; }
        [RegularExpression(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,50}$", ErrorMessageResourceType = typeof(ChangeOfOwnerAutomation_cshtml), ErrorMessageResourceName = "Password_NotAvailable")]
        public string CCPPassword { get; set; }
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$", ErrorMessageResourceType = typeof(ChangeOfOwnerAutomation_cshtml), ErrorMessageResourceName = "EmailAddress_NotAvailable")]
        public string CCPEmailAddress { get; set; }
        public string CCPAuthorisedDate { get; set; }
        public bool IsRevoked { get; set; }

        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public int Page { get; set; }
        public string ErrorMsg { get; set; }
    }

    public class COOCompanyInformationEntity : ViewModelBase
    {
        public int CCIIntNo { get; set; }
        public string CCICompanyName { get; set; }
        public string CCIRegistrationNumber { get; set; }
        public string CCITelephone { get; set; }
        public string CCIEmail { get; set; }
        public string CCIPostalAddress { get; set; }
        public string CCIPhysicalAddress { get; set; }
        public string CCICompanyCode { get; set; }
    }

    public class COOCompanyProxyEntity : ViewModelBase
    {
        public int CCPIntNo { get; set; }
        public int CCIIntNo { get; set; }
        public string CCPAuthorisedDate { get; set; }public string CCPEmailAddress { get; set; }
        public string CCPNumber { get; set; }
        public string CCPPassword { get; set; }
        public string CCPRevokedDate { get; set; }
        public bool IsRevoked { get; set; }
    }

}