﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.Web.ViewModels.Admin
{
    public class CourtDatesWarrantModel
    {
        public int SearchAuthority { get; set; }
        public int SearchCourt { get; set; }
        public int SearchCourtRoom { get; set; }
        public int OriginGroup { get; set; }

        public SelectList AuthorityList { get; set; }
        public SelectList CourtList { get; set; }
        public SelectList CourtRoomList { get; set; }
        public SelectList OriginGroupList { get; set; }

        public bool FirstLoad { get; set; }

        public List<CourtDatesForWarrant> CourtDates { get; set; }

    }
}