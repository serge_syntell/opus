﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SIL.AARTO.Web.Resource.Admin;

namespace SIL.AARTO.Web.ViewModels.Admin
{
    public class RepresentationAuthorisationUserModel
    {
        public RepresentationAuthorisationUserModel()
        {
            RauList = new List<RepresentationAuthorisationUserEntity>();
        }

        public RepresentationAuthorisationUserEntity RauEntity { get; set; }
        public List<RepresentationAuthorisationUserEntity> RauList { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
    }

    public class RepresentationAuthorisationUserEntity
    {
        public int Order { get; set; }
        public int RauIntNo { get; set; }
        [Required]
        [Remote("CheckRauName", "Admin", AdditionalFields = "RauIntNo", HttpMethod = "POST", ErrorMessageResourceType = typeof(RepresentationAuthorisationUser_cshtml), ErrorMessageResourceName = "NameExists")]
        public string RauName { get; set; }
        public string RauForceNumber { get; set; }
        public string LastUser { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
