﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.CameraManagement;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.Web.Resource.Admin;

namespace SIL.AARTO.Web.ViewModels.Admin
{
    public class OfficerEntity
    {
        public int ToIntNo { get; set; }
        public string TOIdNumber {get;set;}
        public string TOSName{get;set;}
        public string TOInit{get;set;}
        public string TONo{get;set;}
        public string TOGroup{get;set;}
        public string Senior { get; set; }
        public bool IsActive { get; set; }
    }

    public class TrafficOfficerModel
    {
        public SelectList Metros { get; set; }
        public int Metro { get; set; }
        public List<OfficerEntity> TrafficOfficers { get; set; }

        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int TotalCount { get; set; }
        public string ErrorMsg { get; set; }
        public string ToIdNumberSearch { get; set; }
        public string SurnameSearch { get; set; }

        public int ToIntNo { get; set; }
        public int MtrIntNo { get; set; }
        [Required(ErrorMessage = "*")]
        [StringLength(35, ErrorMessageResourceName = "ReqAddTOSName_ErrorMessage", ErrorMessageResourceType = typeof(TrafficOfficerRes))]
        public string TOSName { get; set; }
        //[Required(ErrorMessage = "*")]
        public string TOInit { get; set; }
        [Required(ErrorMessage = "*")]
        public string ToIDNumber { get; set; }
        //[Required(ErrorMessage = "*")]
        //[RegularExpression(@"^\s*[-\+]?\d+\s*$", ErrorMessageResourceName = "ReqAddTOGroup_ErrorMessage", ErrorMessageResourceType = typeof(TrafficOfficerRes))]
        //public string TOGroup { get; set; }
        //[Required(ErrorMessage = "*")]
        public string TONo { get; set; }
        //[Required(ErrorMessage = "*")]
        public string TOInfrastructureNumber { get; set; }

        public SelectList OfficerGroups { get; set; }
        public int OfGrIntNo { get; set; }

        public bool Senior { get; set; }
    }
}