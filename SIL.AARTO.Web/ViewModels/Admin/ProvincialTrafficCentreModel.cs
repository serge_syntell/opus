﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.Web.ViewModels.Admin
{
    public class ProvincialTrafficCentreModel
    {
        public int PTCIntNo { get; set; }

        [StringLength(50, ErrorMessage = "Max length 50")]
        [Required(ErrorMessage = "*")]
        public string PTCCode { get; set; }

        [StringLength(100, ErrorMessage = "Max length 100")]
        [Required(ErrorMessage = "*")]
        public string PTCDescription { get; set; }

        public string LastUser { get; set; }

        public string ErrorMsg { get; set; }

        public string SearchStr { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public TList<SIL.AARTO.DAL.Entities.ProvincialTrafficCentre> ProvincialTrafficCentreList { get; set; }
    }
}