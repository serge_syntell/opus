﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.CameraManagement;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.Web.Resource.Admin;

namespace SIL.AARTO.Web.ViewModels.Admin
{
    public class OfficerGroupModel
    {
        public List<OfficerGroup> OfficerGroups { get; set; }

        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int TotalCount { get; set; }
        public string ErrorMsg { get; set; }
        public string OfGrDescrSearch { get; set; }

        public int OfGrIntNo { get; set; }
        [Required(ErrorMessage = "*")]
        [StringLength(25, ErrorMessageResourceName = "LengthOfGrCode_ErrorMessage", ErrorMessageResourceType = typeof(OfficerGroupRes))]
        public string OfGrCode { get; set; }
        [Required(ErrorMessage = "*")]
        public string OfGrDescr { get; set; }
    }
}