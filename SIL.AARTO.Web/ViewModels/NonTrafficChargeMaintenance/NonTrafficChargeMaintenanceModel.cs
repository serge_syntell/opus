﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIL.AARTO.Web.ViewModels.NonTrafficChargeMaintenance
{
    public class NonTrafficChargeMaintenanceModel
    {
        public List<NonTrafficChargeModel> NonTrafficChargeList { get; set; }

        public int NTCIntNo { get; set; }

        [Required(ErrorMessage = "*")]
        public string NTCCode { get; set; }

        [Required(ErrorMessage = "*")]
        public string NTCDescr { get; set; }

        [Required(ErrorMessage = "*")]
        public string NTCComment { get; set; }

        [Required(ErrorMessage = "*")]
        public string NTCStandardAmount { get; set; }

        [Required(ErrorMessage = "*")]
        public string UOMIntNo { get; set; }

        public string SearchAutIntNo { get; set; }
        public List<SelectListItem> SearchAuthorityList { get; set; }

        public string SearchUOMIntNo { get; set; }
        public List<SelectListItem> SearchUnitOfMeasureList { get; set; }

        public string SearchNTCCode { get; set; }

        public List<SelectListItem> UnitOfMeasureList { get; set; }

        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public string URLPara { get; set; }
        public string LastUser { get; set; }
        public int Page { get; set; }

        public string ErrorMsg { get; set; }


        public NonTrafficChargeMaintenanceModel()
        {
            NonTrafficChargeList = new List<NonTrafficChargeModel>();
        }

    }

    public class NonTrafficChargeModel
    {
        public int NTCIntNo { get; set; }
        public int AutIntNo { get; set; }
        public string NTCCode { get; set; }
        public string NTCDescr { get; set; }
        public string NTCComment { get; set; }
        public string NTCStandardAmount { get; set; }
        public string UOMIntNo { get; set; }
        public string UOMDescr { get; set; }
    }
}