﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.Web.ViewModels.SecondaryOffence
{
    public class SecondaryOffenceModel
    {
        public int OffenceGroup { get; set; }
        public SelectList OffenceGroupList { get; set; }

        public int Offence { get; set; }
        public SelectList OffenceList { get; set; }

        public int ChangeOffenceGroup { get; set; }
        public SelectList ChangeOffenceGroupList { get; set; }

        public int ChangeOffence { get; set; }
        public SelectList ChangeOffenceList { get; set; }

        public int SeOfIntNo { get; set; }
        public string SeOfCode { get; set; }
        public string ErrorMsg { get; set; }

        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public int PageIndex { get; set; }

        public LookupPartViewModel LookupModel { get; set; }

        public TList<SIL.AARTO.DAL.Entities.SecondaryOffence> PageSecondaryOffence { get; set; }
    }
}