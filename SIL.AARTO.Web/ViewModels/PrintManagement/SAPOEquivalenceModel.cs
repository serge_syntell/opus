﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIL.AARTO.Web.ViewModels.PrintManagement
{
    public class SAPOEquivalenceModel
    {
        public int SEIntNo { get; set; }
        public int RTIntNo { get; set; }
        public string OPUSLable { get; set; }
        public string OPUSDataType { get; set; }
        public int OPUSDataLength { get; set; }
        public string SAPOLable { get; set; }
        public string SAPODataType { get; set; }
        public int SAPODataLength { get; set; }
        public string SEDescription { get; set; }
        public string LastUser { get; set; }
        public string rowversion { get; set; }

        public SelectList NPCDataTypeList { get; set; }

        public SelectList NPCSAPODataTypeList { get; set; }

        
        public string SearchStr { get; set; }
    }
}