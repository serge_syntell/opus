﻿using SIL.AARTO.Web.Resource.PrintManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.WebPages.Html;
using SIL.AARTO.DAL.Entities;
using System.Web.Mvc;

namespace SIL.AARTO.Web.ViewModels.PrintManagement
{
    public class SAPOPrintingControlModel
    {
        public Int32 SPPCIntNo { get; set; }

        public Int32 SPRTIntNo { get; set; }
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(SIL.AARTO.Web.Resource.PrintManagement.SAPOPrintingControl))]
        [StringLength(10, ErrorMessageResourceName = "MaxLeng_10", ErrorMessageResourceType = typeof(SIL.AARTO.Web.Resource.PrintManagement.SAPOPrintingControl))]
        public string SPPCOption { get; set; }
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(SIL.AARTO.Web.Resource.PrintManagement.SAPOPrintingControl))]
        [StringLength(10, ErrorMessageResourceName = "MaxLeng_20", ErrorMessageResourceType = typeof(SIL.AARTO.Web.Resource.PrintManagement.SAPOPrintingControl))]
        public string SPPCTemplate { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(SIL.AARTO.Web.Resource.PrintManagement.SAPOPrintingControl))]
        //[StringLength(10, ErrorMessageResourceName = "MaxLeng_20", ErrorMessageResourceType = typeof(SIL.AARTO.Web.Resource.PrintManagement.SAPOPrintingControl))]
        public bool SPPCPrintMethod { get; set; }
        public string LastUser { get; set; }

        private long RowVersion_Long { get; set; }
        public virtual byte[] RowVersion
        {
            get { return BitConverter.GetBytes(RowVersion_Long).Reverse().ToArray(); }
            set { RowVersion_Long = Convert.ToInt64(BitConverter.ToString(value).Replace("-", string.Empty), 16); }
        }

        public SelectList SPPCPrintMethodList { get; set; }

        public string SPRTReportName { get; set; }


    }
}