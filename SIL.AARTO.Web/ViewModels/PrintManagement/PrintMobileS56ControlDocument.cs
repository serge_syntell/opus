﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIL.AARTO.Web.ViewModels.PrintManagement
{
    public class PrintMobileS56ControlDocumentModel
    {
        //pagination
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int TotalCount { get; set; }
        public string Msg { get; set; }
        public string HasConfirmed { get; set; }

        //search condition
        public bool CheckedCompletedFiles { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }

        public List<PrintMobileS56ControlDocumentListModel> PagePrintFileList { get; set; }
    }

    public class PrintMobileS56ControlDocumentListModel
    {
        public string MobileControlDocumentGeneratedDate { get; set; }
        public string MobileControlDocumentPrintedDate { get; set; }
        public string MobileControlDocumentName { get; set; }
    }
}