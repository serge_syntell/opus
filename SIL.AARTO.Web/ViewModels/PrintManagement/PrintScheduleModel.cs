﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIL.AARTO.Web.ViewModels.PrintManagement
{
    public class PrintScheduleModel
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public int SortExpr { get; set; }
        public int PostTotalCount { get; set; }

        public bool IsShow { get; set; }
        public string ErrorMsg { get; set; }
        public string ErrorMsgPost { get; set; }//2014-01-13 added by Nancy for the posted list
        //2014-01-10 added by Nancy for report type
        public string rdoType { get; set; }

        public List<PrintScheduleDetail> PrintList { get; set; }
        public List<PrintScheduleDetail> PostList { get; set; }

        public PrintScheduleModel()
        {
            PrintList = new List<PrintScheduleDetail>();
            PostList = new List<PrintScheduleDetail>();
        }
    }

    public class PrintScheduleDetail
    {
        public string PFNIntNo { get; set; }
        public string PrintFileName { get; set; }
        public string DocumentCount { get; set; }
        public string BatchGeneratedDate { get; set; }
        public string BatDocOldestOffDate { get; set; }
        public string ProdSchePriCapDate { get; set; }
    }
}