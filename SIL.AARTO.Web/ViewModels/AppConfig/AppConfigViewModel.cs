﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIL.AARTO.Web.ViewModels.AppConfig
{

    public class AppConfigViewModel
    {
        public int AppConfigNO { get; set; }
        public string CopyDestinationPath { get; set; }
        public string BackupDestinationPath { get; set; }
        public string LogFileDestination { get; set; }
        public string ConvertedDestination { get; set; }
        public bool SendEmailNotifications { get; set; }
        public string NotificationRecipient { get; set; }
        public int  Port { get; set; }
        public string From { get; set; }
        public bool Info { get; set; }
        public bool Error { get; set; }
        public bool Debug { get; set; }
        public bool MakeBackup { get; set; }
       
        public string ftpProcess { get; set; }
        public string ftpExportServer { get; set; }
        public string ftpExportPath { get; set; }
        public string ftpReceivedPath { get; set; }
        public string ftpHostServer { get; set; }
        public string ftpHostIP { get; set; }
        public string ftpHostUser { get; set; }
        public string ftpHostPass { get; set; }
        public string ftpHostPath { get; set; }
        public string email { get; set; }
        public string smtp { get; set; }
        public string mode { get; set; }         //centralised OR mdb
        public string imageFolder { get; set; }  //new setting to store images in file system
        public string SmtpUser { get; set; }
        public string SmtpPassword { get; set; }

    }
}