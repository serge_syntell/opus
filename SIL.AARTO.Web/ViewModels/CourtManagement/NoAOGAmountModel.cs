﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.CourtManagement;

namespace SIL.AARTO.Web.ViewModels.CourtManagement
{

    public class NoAOGAmountModel
    {
        [UIHint("CourtDropdown")]
        public int CrtIntNo { get; set; }

        [UIHint("CourtRoomDropdown")]
        public int CRIntNo { get; set; }

        public int CoMaIntNo { get; set; }

        public SelectList CourtMagList { get; set; }

        public string Number { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string URLPara { get; set; }
        public int TotalCount { get; set; }

        public bool NoDateFound { get; set; }

        public List<NoAOGAmountSettingEntity> Result { get; set; }

        public System.Web.Mvc.SelectList CourtRooms { get; set; }
    }
}