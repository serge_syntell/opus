﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.Web.Resource.Admin;

namespace SIL.AARTO.Web.ViewModels.CourtManagement
{
    public class CourtManagementModel
    {
        public int SearchCourt { get; set; }
        public SelectList SearchCourtList { get; set; }

        public List<CourtMagistrate> CourtMagistrateList { get; set; }

        public int CoMaIntNo { get; set; }
        [Required(ErrorMessage = "*")]
        public string CrtIntNo { get; set; }
        public SelectList CourtList { get; set; }
        [Required(ErrorMessage = "*")]
        public string MagistrateName { get; set; }

        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public string URLPara { get; set; }
        public string LastUser { get; set; }
        public int Page { get; set; }

        public string ErrorMsg { get; set; }

        public CourtManagementModel()
        {
            CourtMagistrateList = new List<CourtMagistrate>();
        }
    }

    public class CourtMagistrate
    {
        public int CoMaIntNo { get; set; }
        public int CrtIntNo { get; set; }
        public string CourtName { get; set; }
        public string CourtNo { get; set; }
        public string MagistrateName { get; set; }
    }
}