﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace SIL.AARTO.Web.ViewModels.VideoOffenceCapture
{
    public class HeaderModel
    {
        public long VCHIntNo { get; set; }

        [Required(ErrorMessage = "*")]
        public string TapeCassetteNo { get; set; }

        [Required(ErrorMessage = "*")]
        public string Date { get; set; }
        //[Required(ErrorMessage = "*")]
        public string Location { get; set; }

        public SelectList Courts { get; set; }
        [Required(ErrorMessage = "*")]
        public int Court { get; set; }
        public string CrtNo { get; set; }

        //2013-12-12 Heidi added (5149)
        [Required(ErrorMessage = "*")]
        public int CrtIntNo { get; set; }

        public int LocIntNo { get; set; }

        public SelectList LocationSuburbs { get; set; }
       
        public string LocationSuburb { get; set; }
        [Required(ErrorMessage = "*")]
        public int LoSuIntNo { get; set; }

        //2013-12-12 Heidi added (5149)
        public SelectList RailwayLocations { get; set; }
        public string RailwayLocation { get; set; }
        //2013-12-12 Heidi added (5149)
        public bool IsRailwayCrossing { get; set; }
        //2013-12-12 Heidi added (5149)
        public int OldLocIntNo { get; set; }

        public SelectList Authorities { get; set; }
        [Required(ErrorMessage="*")]
        public int Authority { get; set; }

        public SelectList Offences { get; set; }
        [Required(ErrorMessage = "*")]
        public int Offence { get; set; }

        public SelectList Officers { get; set; }
        [Required(ErrorMessage = "*")]
        public int Officer { get; set; }

        public SelectList NSTypes { get; set; }
        [Required(ErrorMessage = "*")]
        public int NSType { get; set; }

        public string OffenceCode { set; get; }
        public float Fine { get; set; }
        public string OfficerName { get; set; }
        public string OfficerCode { get; set; }

        [Required(ErrorMessage = "*")]
        public string StartTime { set; get; }
        [Required(ErrorMessage = "*")]
        public string EndTime { set; get; }

        [Required(ErrorMessage = "*")]
        public string CompiledBy { set; get; }
        [Required(ErrorMessage = "*")]
        public string CompiledDate { set; get; }
        [Required(ErrorMessage = "*")]
        public string CheckedBy { set; get; }
        [Required(ErrorMessage = "*")]
        public string CheckedDate { set; get; }

        [Required(ErrorMessage = "*")]
        public string CapturedBy { get; set; }
        [Required(ErrorMessage = "*")]
        public string CapturedDate { get; set; }

        public string ErrorMsg { get; set; }
        public string Language { get; set; }
    }
}