﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.DAL.Entities;
using System.Web.Mvc;

namespace SIL.AARTO.Web.ViewModels.VideoOffenceCapture
{
    public class DetailModel
    {
        public HeaderMsg Header { get; set; }

        public long VcdIntNo { get; set; }
        public long VchIntNo { get; set; }

        [Required(ErrorMessage="*")]
        public string OffTime { get; set; }
        [Required(ErrorMessage = "*")]
        [StringLength(10,MinimumLength = 2, ErrorMessage = "2 ~ 10 letters")]
        [RegularExpression(".*[^0].*", ErrorMessage = "*")]
        public string RegNo { get; set; }

        public SelectList Colours { get; set; }
        [Required(ErrorMessage = "*")]
        public int Colour { get; set; }
        public string VcCode { get; set; }

        public SelectList Makers { get; set; }
        [Required(ErrorMessage = "*")]
        public int Maker { get; set; }
        public string VmCode { get; set; }

        public SelectList Types { get; set; }
        [Required(ErrorMessage = "*")]
        public int Type { get; set; }
        public string VtCode { get; set; }

        [Required(ErrorMessage = "*")]
        public string TicketNo { get; set; }

        public string ErrorMsg { get; set; }
        public string Language { get; set; }

        public List<TableRowDetail> Details { get; set; }
    }

    [Serializable]
    public class HeaderMsg
    {
        public long VCHIntNo { get; set; }
        public string OfficerCode { set; get; }
        public string OfficerName { get; set; }
        public string StartTime { set; get; }
        public string EndTime { set; get; }
        public string Location { get; set; }
        public string TapeCassetteNo { get; set; }
        public string Date { get; set; }
        public string Offence { get; set; }
        public string NSTypeDesc { get; set; }
    }

    public class TableRowDetail
    {
        public int RowNo { get; set; }
        public long VcdIntNo {get;set;}
        public DateTime VCDTime { get; set; }
        public string VCDRegNo { get; set; }
        public string VechileColour { get; set; }
        public string VechileMaker { get; set; }
        public string VechileType { get; set; }
        public string VTNoticeTicketNo { set; get; }
    }

}