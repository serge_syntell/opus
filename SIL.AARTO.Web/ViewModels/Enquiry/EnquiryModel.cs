﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIL.AARTO.DAL.Entities;
using System.ComponentModel.DataAnnotations;


namespace SIL.AARTO.Web.ViewModels.Enquiry
{
    [Serializable]
    public class EnquiryModel
    {
        public string RegNo { get; set; }
        public string IDNo { get; set; }
        public string TicketNo { get; set; }
        public string SummonsNO { get; set; }
        public string CaseNO { get; set; }
        public string WOANO { get; set; }
        public string Initials { get; set; }
        public string Name { get; set; }
        public string Since { get; set; }

        public string Prefix { get; set; }
        public string Sequence { get; set; }
        public string AuthCode { get; set; }
        public string CDV { get; set; }
        public string EasyPayNo { get; set; }
        public string NoticeNumber { get; set; }

        public string searchField { get; set; }
        public string searchValue { get; set; }
        public string ErrorMsg { get; set; }
        public string LookUpErrorMsg { get; set; }

        public bool IsShowCSCode { get; set; }
        public int minStatus { get; set; }

        public bool IsCofCT { get; set; }
        public string SelectSubmit { get; set; }

        public string setControlFocus { get; set; }

        public string SinceText { get; set; }//2013.3.16
        public string searchField2 { get; set; }//2013.3.16
        public string NoticeNoLookupHasError { get; set; }//2013.3.16

        public string AuthorityName { get; set; }//2014-05-08 Heidi added(5249)

        public bool isShowResult { get; set; }
        public bool isShowBook { get; set; }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public string NoticeNoLookupSubmitToController { get; set; }
        public string NoticeNoLookupSubmitToAction { get; set; }

        public bool btnAllDetailsEnabled { get; set; }

       // public NoticeNoLookup NoticeNoLookupModel { get; set; }

        public List<NoticeDetail> NoticeList { get; set; }
        public List<Book> BookList { get; set; }
        public List<Header> HeaderList { get; set; }

        public string originalTicketNo { get; set; }

        //Jerry 2014-08-25 add for 5303 S56 Mobile
        public bool HasMobile { get; set; }
        public bool HasMobleS56 { get; set; }

        public EnquiryModel()
        {
            NoticeList = new List<NoticeDetail>();
            BookList = new List<Book>();
            HeaderList = new List<Header>();
        }
    }

    public enum NoticeSource
    {
        unknown = -1,
        camera
    }

    [Serializable]
    public class NoticeDetail
    {
        public string NotIntNo { get; set; }
        public string NotTicketNo { get; set; }
        public string NotOffenceDate { get; set; }
        public string NotRegNo { get; set; }
        public string AutName { get; set; }
        public string ChgOffenceCode { get; set; }
        public string PaymentDate { get; set; }
        public string CSCode { get; set; }
        public string CSDescr { get; set; }
        public string ChgRevFineAmount { get; set; }
        public string ChgContemptCourt { get; set; }
        public string Offender { get; set; }
        public string Representation { get; set; }
        public string LetterTo { get; set; }
        public string FixOfficerError { get; set; }
        public string HasComment { get; set; }
        public bool IsEnable { get; set; }
        public string IDNumber { get; set; } //Heidi 2013-08-27 added for search ID Number,WOANumber,WOAStatus
        public string WOANumber { get; set; }
        public string WOAServedStatus { get; set; }
        public string WOAFineAmount { get; set; }//Heidi 2013-10-21 added WOAFineAmount,WOAAddAmount,NoticeStatus,WOAStatus for woaEnquiry page display
        public string WOAAddAmount { get; set; }
        public string NoticeStatus { get; set; }
        public string WOAStatus { get; set; }
        public string WoABookedOutToOfficer { get; set; } //Heidi 2013-11-18 added WoABookedOutToOfficer for woaEnquiry page display
        public string NotCourtName { get; set; } //Heidi 2014-05-08 added CourtName,CourtDate for display on enquiry page(5249)
        public string SumCourtDate { get; set; }
        public bool IsMobile { get; set; } //Jerry 2014-08-25 add for 5303 S56 Mobile
        public string NotFilmType { get; set; }//Jerry 2014-08-25 add for 5303 S56 Mobile
        public string MobileControlDocumentGeneratedDate { get; set; }//Jerry 2014-09-18 add for 5303 S56 Mobile
        public bool HasDocument { get; set; }
        public NoticeSource? Source { get; set; }
    }

    [Serializable]
    public class Book
    {
        public string AaBMDocNo { get; set; }
        public string AaBmIsCancelled { get; set; }
        public string DocumentStatus { get; set; }
        public string AaBMDepotDescription { get; set; }
        public string TOSName { get; set; }
        public string AaBMBookNo { get; set; }
        public string AaBMBookTypeName { get; set; }
        public string BookStatus { get; set; }
       
    }

    [Serializable]
    public class Header
    {
        public string NotTicketNo { get; set; }
        public string NotTicketNo_Enquiry { get; set; }
        public string NotRegNo { get; set; }
        public string Name { get; set; }
        public int NotIntNo { get; set; }
    }

   
}