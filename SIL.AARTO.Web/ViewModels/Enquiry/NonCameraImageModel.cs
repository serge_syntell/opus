﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIL.AARTO.Web.ViewModels.Enquiry
{
    public class NonCameraImageModel
    {
        public int NotIntNo { get; set; }
        public NoticeSource? Source { get; set; }
        public string ImageUrls { get; set; }
    }
}