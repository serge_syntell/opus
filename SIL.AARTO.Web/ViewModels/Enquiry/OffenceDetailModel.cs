﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIL.AARTO.DAL.Entities;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SIL.AARTO.Web.ViewModels.Enquiry
{
    public class OffenceDetailModel
    {
        public string Title { get; set; }
        public string Navigation { get; set; }

        public string NotFilmNo { get; set; }
        public string TicketNo { get; set; }
        public string FilmFrameNo { get; set; }
        public string RegistrationNo { get; set; }
        public string OffenceDate { get; set; }
        public string Speed { get; set; }
        public string Speed2 { get; set; }
        public string Offencedesc { get; set; }
        public string Location { get; set; }
        public string OwnerName { get; set; }
        public string OwnerID { get; set; }
        public string DriverName { get; set; }
        public string DriverID { get; set; }
        public string ProxyName { get; set; }
        public string ProxyID { get; set; }
        public string OwnerPostAddr { get; set; }
        public string DriverPostAddr { get; set; }
        public string OwnerStrAddr { get; set; }
        public string DriverStrAddr { get; set; }
        public string FineAmount { get; set; }
        public int FrameIntNo { get; set; }
        public int FilmIntNo { get; set; }
        public int ScanImageIntNo { get; set; }
        public string ImageType { get; set; }
        public int Phase { get; set; }

        public string searchField { get; set; }
        public string searchValue { get; set; }
        public string Since { get; set; }
        public int Count { get; set; }

        public string PrintVersionNavigateUrl { get; set; }
        public string NotProxyFlag { get; set; }
        public bool tblOwnerDetails { get; set; }

        public string Error { get; set; }

        public int imageAW { get; set; }
        public int imageAH { get; set; }
        public int imageBW { get; set; }
        public int imageBH { get; set; }
        public int imageRW { get; set; }
        public int imageRH { get; set; }

        public string imageAUrl { get; set; }
        public string imageBUrl { get; set; }
        public string imageRegUrl { get; set; }

        public bool isShowImgAreaA { get; set; }
        public bool isShowImgAreaB { get; set; }
        public bool isShowRegArea { get; set; }

        public string noticePara { get; set; }
        public string autintnoPara { get; set; }
        

        public SelectList ScanImageList { get; set; }

        public string backgroundImage { get; set; }
        public string styleSheet { get; set; }
        public string PageTitle { get; set; }
    }
}