﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIL.AARTO.Web.ViewModels.Enquiry
{
    public class MobileSignatureModel
    {
        public int NotIntNo { get; set; }
        public string OffenderSignatureImageURL { get; set; }
        public string TrafficOfficerSignatureImageURL { get; set; }
    }
}