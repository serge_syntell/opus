﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.Web.ViewModels.Enquiry
{
    public class NoticeCommentModel
    {
        public NoticeCommentModel()
        {
            NoticeCommentList = new List<NoticeCommentEntity>();
            NoticeComment = new NoticeCommentEntity();
        }

        public int NotIntNo { get; set; }
        public string NotTicketNo { get; set; }
        public DateTime? NotOffenceDate { get; set; }
        public List<NoticeCommentEntity> NoticeCommentList { get; set; }
        public NoticeCommentEntity NoticeComment { get; set; }
        public int PageIndex { get; set; }
        public int TotalCount { get; set; }
    }

    public class NoticeCommentEntity
    {
        public string LastUser { get; set; }
        [Required(ErrorMessage = "*")]
        public string NcComment { get; set; }
        public DateTime NcCreateDate { get; set; }
        public long NcIntNo { get; set; }
        public DateTime NcUpdateDate { get; set; }
        public int NotIntNo { get; set; }
        public byte[] RowVersion { get; set; }
        public string RowVersionString { get; set; }
        public EntityState EntityState { get; set; }

        public int OrderNo { get; set; }
        public bool EntityStatus { get; set; }
        public string EntityMessage { get; set; }
    }
}