﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.CameraManagement;

namespace SIL.AARTO.Web.ViewModels
{
    public class LookupPartViewModel
    {
        private TList<LanguageSelector> languageSelectors;
        public TList<LanguageSelector> LanguageSelectors
        {
            get
            {
                if (languageSelectors == null)
                {
                    languageSelectors = LanguageSelectorManager.GetAllLanSelector();
                }
                return languageSelectors;
            }
        }

        public int EnUSLength { get; set; }
    }
}