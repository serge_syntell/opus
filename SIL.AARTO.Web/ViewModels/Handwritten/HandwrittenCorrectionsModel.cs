﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIL.AARTO.Web.ViewModels.Handwritten
{
    public class HandwrittenCorrectionsModel
    {
        public string SearchStr { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int TotalCount { get; set; }
        public string ErrorMsg { get; set; }
        public string IsNotice { get; set; }

        public List<HandwrittenCorrectionsListModel> PageCorrectionsList { get; set; }
    }

    public class HandwrittenCorrectionsListModel
    {
        public string NoticeTicketNoOrSummonsNo { get; set; }
        public string FilmNo { get; set; }
        public string FrameNo { get; set; }
        public string DocumentErrMsg { get; set; }
    }

    public class AdministrativeCorrectionList
    {
        public AdministrativeCorrectionList()
        {
            AdministrativeCorrectionModels = new List<AdministrativeCorrectionModel>();
        }
        public string SearchStr { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public string ErrorMsg { get; set; }
        public int PageIndex { get; set; }
        public IList<AdministrativeCorrectionModel> AdministrativeCorrectionModels { get; set; }
        public List<SelectListItem> CourtList { get; set; }
        public int AutIntNo { get; set; }
        public int CurrentPage { get; set; }

    }

    public class AdministrativeCorrectionModel
    {
        public string HIWEIntNo { get; set; }
        public string NoticeNumber { get; set; }
        public string OffenceDate { get; set; }
        public string ReceiptDate { get; set; }
        public string DMSFineAmount { get; set; }
        public string ReceiptFineAmount { get; set; }
        public string DMSCourt { get; set; }
        public string ReceiptCourt { get; set; }
        public string LoadedDate { get; set; }
        public string Status { get; set; }
        public string DMSCourtIntNo { get; set; }
        public string ReceiptCourtIntNo { get; set; }
        public string OfficerError { get; set; }
        public string DMSImportedCounter { get; set;}
    }

    public class Report_HWOImportedWithErrorsModel
    {
        public List<SelectListItem> HWOImportedStatusList { get; set; }
    }

}