﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIL.AARTO.DAL.Entities;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.Web.Resource.ReportManagement;
using System.Web.Mvc;

namespace SIL.AARTO.Web.ViewModels.ReportManagement
{
    public class ReportEmailModel
    {
        public long ReaIntNo { get; set; }

        public SelectList ReportCodes { get; set; }
        public int ReportCode { get; set; }
        public int InReportCode { get; set; }
        
        [Required(ErrorMessage = "*")]
        [StringLength(35, ErrorMessageResourceName = "MaxLengthSurname", ErrorMessageResourceType = typeof(ShowReportEmailListRes))]
        public string Surname { get; set; }
        [Required(ErrorMessage = "*")]
        [StringLength(5, ErrorMessageResourceName = "MaxLengthInitials", ErrorMessageResourceType = typeof(ShowReportEmailListRes))]
        public string Initials { get; set; }
        [Required(ErrorMessage="*")]
        public string EmailAddress { get; set; }

        public TList<ReportEmailAddress> ReportEmails { get; set; }

        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public string ErrorMsg { get; set; }

        public int PageIndex { get; set; }
    }
}