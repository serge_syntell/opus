﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.Web.ViewModels.ReportManagement
{
    public class NoticeStatusDateModel
    {
        [Required(ErrorMessage = "*")]
        public string StartDate { get; set; }
        [Required(ErrorMessage = "*")]
        public string EndDate { get; set; }

        public string GeneratedDate { get; set; }
        public bool IsGenerated { get; set; }

        public string ErrorMsg { get; set; }
        public TList<TmpNoticeStatusReport> NoticeStatusList { get; set; }
    }
}