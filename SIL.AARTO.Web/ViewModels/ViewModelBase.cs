﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIL.AARTO.Web.ViewModels
{
    public class ViewModelBase
    {
        public string LastUser { get; set; }
        public byte[] RowVersion { get; set; }
        public string RowVersionString { get; set; }
        public int OrderNo { get; set; }
        public bool EntityStatus { get; set; }
        public string EntityMessage { get; set; }
    }
}