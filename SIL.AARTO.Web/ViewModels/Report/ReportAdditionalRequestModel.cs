﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SIL.AARTO.Web.Resource.Report;

namespace SIL.AARTO.Web.ViewModels.Report
{
    public class ReportAdditionalRequestModel
    {
        public ReportAdditionalRequestModel()
        {
            RarEntity = new ReportAdditionalRequestEntity();
            RarList = new List<ReportAdditionalRequestEntity>();
            ReportList = new List<SelectListItem>();
        }

        public ReportAdditionalRequestEntity RarEntity { get; set; }
        public List<ReportAdditionalRequestEntity> RarList { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public int? CurrentId { get; set; }
        public byte[] CurrentRowVersion { get; set; }
        public string Action { get; set; }

        public List<SelectListItem> ReportList { get; set; }
    }

    public class ReportAdditionalRequestEntity
    {
        public bool Visible { get; set; }
        public int Order { get; set; }

        public int RarIntNo { get; set; }

        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(AdditionalRequest_cshtml), ErrorMessageResourceName = "PleaseSelectAReport")]
        public int RcIntNo { get; set; }

        public string RequestedUser { get; set; }

        [Required(ErrorMessageResourceType = typeof(AdditionalRequest_cshtml), ErrorMessageResourceName = "PleaseSetStartDate")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? DateFrom { get; set; }

        [Required(ErrorMessageResourceType = typeof(AdditionalRequest_cshtml), ErrorMessageResourceName = "PleaseSetEndDate")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateTo { get; set; }

        [Required(ErrorMessageResourceType = typeof(AdditionalRequest_cshtml), ErrorMessageResourceName = "PleaseSetActionDate")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? RarActionDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm:ss}")]
        public DateTime? RequestedDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm:ss}")]
        public DateTime? PrintedDate { get; set; }
        public string LastUser { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
