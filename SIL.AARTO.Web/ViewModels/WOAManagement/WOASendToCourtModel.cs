﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.WOAManagement;

namespace SIL.AARTO.Web.ViewModels.WOAManagement
{
    public class WOASendToCourtModel
    {
        public string WoaNumber { get; set; }
        public int? AutIntNo { get; set; }
        public SelectList SearchCourts { get; set; }
        public int SearchCrtIntNo { get; set; }
        public SelectList SearchCourtRooms { get; set; }
        public int SearchCrtRIntNo { get; set; }
        public SelectList SearchCourtDates { get; set; }
        public int SearchCDIntNo { get; set; }
        public string SearchCourtDate { get; set; }
        public bool NoDateFound { get; set; }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public string URLPara { get; set; }

        public List<WoaPrintFileNameEntity> WOAPrintFileNames { get; set; }
    }
}