﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.WOAManagement;

namespace SIL.AARTO.Web.ViewModels.WOAManagement
{
    public class WOABookOutModel
    {

        // Jake 2013-08-28 added new search field IdNumber 
        public WOABookOutModel()
        {
            WOAList = new List<WOASearchDAO>();
            ShowWoaInfo = false;
        }
        public SelectList Authorities { get; set; }
        public SelectList TrafficOfficers { get; set; }

        [UIHint("AuthorityDropdown")]
        [Required(ErrorMessage = "*")]
        public int Authority { get; set; }

        //[Required(ErrorMessage = "*")]
        public string WOANumber { get; set; }

        //[Required(ErrorMessage = "Id number is required")]
        [RegularExpression("^[a-z0-9A-Z]{13}$", ErrorMessage = "IDNumber must be alphanumeric and 13 digits")]
        public string IdNumber { get; set; }

        public int TOIntNo { get; set; }

        public int MetroIntNo { get; set; }

        public List<WOASearchDAO> WOAList { get; set; }

        public bool ShowWoaInfo { get; set; }

        public string ErrorMsg { get; set; }

        public string HidWoaNumber { get; set; }

        // 2014-07-22, Oscar added rowversion.
        public string RowVersionStrings { get; set; }
    }

    public class WOACaptureModel
    {
        [Required(ErrorMessage = "*")]
        public string WOANumber { get; set; }

        public string AccusedName { get; set; }

        public string IDNumber { get; set; }

        //[Required(ErrorMessage="The Service Status field is required")]
        //[Range(1, 6)]
        public int? ServedStatus { get; set; }

        public string NewCourtDate { get; set; }

        public string ErrorMsg { get; set; }

        public string ServedStatusTitle { get; set; }

        public string CrtName { get; set; }
        public string CrtRoom { get; set; }

        public List<WOAReverseResultEntity> WOAResult { get; set; }
    }

    public class WOAReleasedOrCancelledModel
    {
        public string WOANumber { get; set; }
        public string AccusedName { get; set; }
        public string IDNumber { get; set; }
        public string CourtName { get; set; }
        public string CourtRoomNo { get; set; }
        public string NewCourtDate { get; set; }
        public int? ServedStatus { get; set; }

        public int? CourtFrom { get; set; }

        public SelectList CourtFromList { get; set; }

        public int CourtTo { get; set; }
        public string CourtToDesc { get; set; }

        public double? BailAmount { get; set; }
        public string BailReceiptNumber { get; set; }

        public DateTime? BailPaidDate { get; set; }

        public string ErrorMsg { get; set; }

        public int WoaIntNo { get; set; }
    }

    public class WOAReverseResultEntity{
        public string WoaNumber{get;set;}
        public string NotTicketNo { get; set; }
        public string SummonsNo { get; set; }
        public string AccFullName { get; set; }
        public int WoaIntNo{get;set;}
    }

    public class WOACaptureReverseModel
    {
        [Required]
        public string WOANumber { get; set; }

        public List<WOAReverseResultEntity> WOAResult { get; set; }

        public string Msg { get; set; }
    }
}