﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.WOAManagement;
using SIL.AARTO.Web.Views.WOAManagement.App_LocalResources;

namespace SIL.AARTO.Web.ViewModels.WOAManagement
{
    // Jake 2013-08-28 added Court ,CourtRoom,CourtDates
    public class WoaAuthorisationModel
    {
        public SelectList AuthorisationTypeList { get; set; }
        public int AuthorisationType { get; set; }
        public SelectList Authories { get; set; }
        public SelectList Courts { get; set; }
        public int CrtIntNo { get; set; }
        public int? AutIntNo { get; set; }
        public string WoaNumber { get; set; }
        public int? PFNIntNo { get; set; }
        public string WoaIntNo { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public string URLPara { get; set; }

        // Jake 2013-08-28 add new fields
        public SelectList SearchCourts { get; set; }

        //[Required(ErrorMessageResourceName = "msgRejectReasonIsRequired", ErrorMessageResourceType = typeof(WoaAuthorisation_cshtml))]
        //[Required(ErrorMessage = "Court is required")]
        public int SearchCrtIntNo { get; set; }

        public SelectList SearchCourtRooms { get; set; }
        //[Required(ErrorMessage = "Court room is required")]
        public int SearchCrtRIntNo { get; set; }
        public SelectList SearchCourtDates { get; set; }
        //[Required(ErrorMessage = "Court date is required")]
        public int SearchCDIntNo { get; set; }
        public string SearchCourtDate { get; set; }
        public List<WoaPrintFileNameEntity> WoaPrintFileNameList { get; set; }
        public List<WoaAuthorisationEntity> WoaAuthorisationList { get; set; }
        public string ErrorMessage { get; set; }

        public WoaAuthorisationModel()
        {
            WoaPrintFileNameList = new List<WoaPrintFileNameEntity>();
            WoaAuthorisationList = new List<WoaAuthorisationEntity>();
        }

        public string GenerateHtml(string fieldName, string idTag, string value, bool checkedFlag, bool checkBoxDisabled, bool disabled)
        {
            string html = string.Empty;
            string date = "";
            if (!String.IsNullOrEmpty(value))
                date = Convert.ToDateTime(value).ToString("yyyy-MM-dd");

            html = "<input type =\"checkbox\"" + (checkedFlag ? "checked=\"checked\"" : "") + (checkBoxDisabled ? " disabled=\"disabled\"" : "") + " name=\"cbx_" + fieldName +
                "\" value=\"" + String.Format("{0}", idTag) + "\" />&nbsp;<input class=\"input\" type=\"text\"" + (disabled ? " disabled=\"disabled\"" : "") + " name=\"txt_" + fieldName +
                "\" id=\"txt_" + fieldName + "_" + idTag + "\" value=\"" + date + "\" />";

            return html;
        }

        public string GenerateRejectHtml(string fieldName, string idTag, string value, string rejReason, int status, bool checkedFlag, bool checkBoxDisabled, bool disabled)
        {
            //string html = GenerateHtml(fieldName + "Rej", idTag, value, checkedFlag, checkBoxDisabled, disabled);
            //html += "&nbsp;<input class=\"inputRej\" type=\"text\" id=\"txtReject_" + idTag + "\"" + (disabled ? " disabled=\"disabled\"" : "") + " name=\"txt_" + fieldName + "RejReason\" value=\"" + rejReason + "\" />";
            //html += "<input type=\"hidden\" name=\"hid_NoticeStatus\" value=\"" + status + "\"/>";

            string html = "<input type=\"hidden\" name=\"hid_NoticeStatus\" value=\"" + status + "\"/>";
            return html;
        }
    }



    //public class JsonAuthorisationEntity {
    //    public WoaAuthorisationEntity[] Request { get; set; }
    //}


}