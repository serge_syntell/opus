﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIL.AARTO.DAL.Entities;
namespace SIL.AARTO.Web.ViewModels.ENatisErrorManagement
{
    public class ENatisErrorLogModel
    {
        public string SearchStr { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public string ErrorMsg { get; set; }
        public int PageIndex { get; set; }
        public IList<ViewEnatisErrorLogNotice> EnatisErrorLogs { get; set; }

       
    }

  
}