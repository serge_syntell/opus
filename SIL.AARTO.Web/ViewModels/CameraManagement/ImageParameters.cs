﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.Web.ViewModels.CameraManagement
{
    public class ImageParameters
    {
        public List<string> ImageUrls { get; set; }
        public string ImageList { get; set; }
        public string OriginalSize { get; set; }
        public bool ShowCrossHair { get; set; }
        public string XValue { get; set; }
        public string YValue { get; set; }
        public string SavedBrightness { get; set; }
        public string SavedContrast { get; set; }
        public string ShowPreView { get; set; }
        public int CrossStyle { get; set; }

        public ImageParameters()
        {
            ImageUrls = new List<string>();
            ImageList = string.Empty;
            OriginalSize = string.Empty;
            XValue = string.Empty;
            YValue = string.Empty;
            SavedBrightness = string.Empty;
            SavedContrast = string.Empty;
            ShowPreView = string.Empty;
        }
    }
}
