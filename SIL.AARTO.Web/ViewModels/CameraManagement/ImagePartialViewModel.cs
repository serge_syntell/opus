﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Stalberg.TMS;
using SIL.AARTO.BLL.Utility;
using System.Data;
using System.Web;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.CameraManagement;

namespace SIL.AARTO.Web.ViewModels.CameraManagement
{
    public class ImagePartialViewModel
    {
        public const string DEFAULT_BRIGHTNESS = "0.0";
        public const string DEFAULT_CONTRAST = "1.0";

        public UserLoginInfo UserInfo { get; set; }
        public int AuthIntNo { get; set; }
        public int FilmIntNo { get; set; }
        public int FrameIntNo { get; set; }
        public int ScanImageIntNo { get; set; }
        public string ImageType { get; set; }
        public int Phase { get; set; }
        public int CrossHairStyle { get; set; }

        private ScanImageDetails siDetail = new ScanImageDetails();
        private string FilmNo
        {
            get
            {
                return FilmManager.GetFilmDetailsByFilmIntNo(FilmIntNo).FilmNo;
            }
        }
        private bool OriginalSize
        {
            get
            {
                return AuthorityRulesManager.GetIsOrNotDisplayCrossHair(UserInfo.UserName, AuthIntNo) && ShowCrossHair;
            }
        }

        public ImagePartialViewModel()
        {
            ImageType = "A";
            Phase = 0;
        }
        public bool ShowCrossHair
        {
            get
            {
                siDetail = ScanImageManager.GetScanImageDetails(ScanImageIntNo);
                if (Phase != 0
                    && (AuthorityRulesManager.GetIsOrNotDisplayCrossHair(UserInfo.UserName, AuthIntNo) || 
                    AuthorityRulesManager.GetIsOrNotShowCrossHairs(UserInfo.UserName, AuthIntNo))
                    && siDetail.XValue > 0
                    && siDetail.YValue > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #region Load images

        private string ImageList = "";
        private List<string> ImageURLs = new List<string>();
        /// <summary>
        /// Load images based on film or frame, then bind to dropdownlist
        /// </summary>
        private void LoadFilmImages()
        {
            ScanImageDB scan = new ScanImageDB(Config.ConnectionString);
            
            DataSet scanDS;

            switch (Phase)
            {
                case 0:
                    scanDS = scan.GetScanImageListDS(FilmIntNo, 0, "A");
                    break;
                default:
                    scanDS = scan.GetScanImageListDS(0, FrameIntNo, "%");
                    break;
            }

            ImageList = "";
            if (scanDS.Tables[0].Rows.Count <= 0)
            {
                GetImage("", 0, "", 0, 0, decimal.Parse(DEFAULT_CONTRAST), decimal.Parse(DEFAULT_BRIGHTNESS), 3);
                GetImage("", 0, "", 0, 0, decimal.Parse(DEFAULT_CONTRAST), decimal.Parse(DEFAULT_BRIGHTNESS), 1);
            }
            else
            {
                foreach (DataRow dr in scanDS.Tables[0].Rows)
                {
                    string contrast = (dr["Contrast"]).ToString();
                    string brightness = (dr["Brightness"]).ToString();
                    int scImPrintVal = int.Parse(dr["scImPrintVal"].ToString());

                    ImageList += dr["ImageDetails"].ToString() + "&&" + dr["ScImIntNo"].ToString() + "@@@";

                    GetImage("",
                        Convert.ToInt32(dr["ScImIntNo"]),
                        dr["ImageDetails"].ToString(), 0, 0,
                        decimal.Parse(string.IsNullOrEmpty(contrast) ? DEFAULT_CONTRAST : contrast),
                        decimal.Parse(string.IsNullOrEmpty(brightness) ? DEFAULT_BRIGHTNESS : brightness),
                        scImPrintVal
                        );
                }
            }
        }

        /// <summary>
        /// WebImageViewer get the image from the file sytem if the image is stored on the hard disk or from the database
        /// If it is loaded from database and it is allowed to save back to hard disk, then save it in the corrsponding cached folder.
        /// If the image stored in DB is empty, load an "NoImage.jpg" to the WebImageViewer
        /// </summary>
        /// <param name="image"></param>
        /// <param name="scanImage"></param>
        /// <param name="imageDetails"></param>
        protected void GetImage(string ImageName, int scanImage, string imageDetails, int width, int height, decimal contrast, decimal brightness, int scImPrintVal)
        {
            string jpegName = "";

            if (!imageDetails.Equals(""))
                jpegName = GetJpegName(imageDetails);

            if (imageDetails.ToLower().Contains("main "))
            {
                ImageType = "A";
                ScanImageIntNo = scanImage;
            }
            else if (imageDetails.ToLower().Contains("2nd "))
                ImageType = "B";
            else if (imageDetails.ToLower().Contains("registration "))
                ImageType = "R";
            else if (imageDetails.ToLower().Contains("driver "))
                ImageType = "D";

            //For ASD
            if (imageDetails.ToLower().Contains("main ") && imageDetails.ToLower().Contains("_001.j"))
            {
                ImageType = "A";
                ScanImageIntNo = scanImage;
            }
            else if (imageDetails.ToLower().Contains("main ") && imageDetails.ToLower().Contains("_002.j"))
            {
                ImageType = "B";
                ScanImageIntNo = scanImage;
            }

            string strUrl = "";

            strUrl = "?FilmNo=" + HttpUtility.UrlEncode(FilmNo)
                + "&ScImIntNo=" + HttpUtility.UrlEncode(Convert.ToString(scanImage))
                + "&JpegName=" + HttpUtility.UrlEncode(jpegName)
                + "&ImgType=" + HttpUtility.UrlEncode(ImageType)
                + "&Contrast=" + HttpUtility.UrlEncode(contrast.ToString())
                + "&Brightness=" + HttpUtility.UrlEncode(brightness.ToString())
                + "&ScImPrintVal=" + HttpUtility.UrlEncode(scImPrintVal.ToString());

            ImageURLs.Add(strUrl);
        }
        #endregion

        protected string GetJpegName(string imageDetails)
        {
            if (imageDetails.Length > 0)
            {
                string jpegDetails = imageDetails;
                int posStart = jpegDetails.IndexOf('(') + 1;
                int posEnd = jpegDetails.IndexOf(')') - 1;

                string jpegName = "";

                if (posStart > 1 && posEnd > 0)
                    jpegName = jpegDetails.Substring(posStart, (posEnd - posStart) + 1);

                return jpegName;
            }
            else
                return string.Empty;
        }


        public string GetParameters(string savedBrightness, string savedContrast, int showPreView)
        {
            LoadFilmImages();
            ImageParameters parameters = new ImageParameters()
            {
                ImageUrls = ImageURLs,
                ImageList = ImageList,
                OriginalSize = OriginalSize.ToString(),
                ShowCrossHair = ShowCrossHair,
                XValue = siDetail.XValue.ToString(),
                YValue = siDetail.YValue.ToString(),
                CrossStyle = CrossHairStyle,
                //SavedBrightness = DEFAULT_BRIGHTNESS,
                //SavedContrast = DEFAULT_CONTRAST,
                ShowPreView = showPreView.ToString()
            };

            if (savedBrightness != null && savedContrast != null)
            {
                parameters.SavedBrightness = savedBrightness;
                parameters.SavedContrast = savedContrast;
            }
            return System.Web.Helpers.Json.Encode(parameters);
        }

        public string GetParameters(int showPreView)
        {
            return GetParameters(null, null, showPreView);
        }
    }
}
