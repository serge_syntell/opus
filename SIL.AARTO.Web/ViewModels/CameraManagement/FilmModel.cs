﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace SIL.AARTO.Web.ViewModels.CameraManagement
{
    public class FilmModel
    {
        public string ISASD { set; get; }
        public string ErrorMsg { get; set; }
        public string LocalAuthority { get; set; }
        public string FilmNo { set; get; }
        public string SectionName { set; get; }
        public string SectionCode { set; get; }
        public string SectionDistance { set; get; }


        public int Location { set; get; }
        public SelectList Locations { set; get; }
        public int Camera { set; get; }
        public SelectList Cameras { set; get; }
        public int SecondCamera { set; get; }
        public SelectList SecondCameras { set; get; }
        public int TrafficOfficer { set; get; }
        public SelectList TrafficOfficers { set; get; }
        public int RoadType { set; get; }
        public SelectList RoadTypes { set; get; }
        public int Court { set; get; }
        public SelectList Courts { set; get; }
        public int SpeedZone { set; get; }
        public SelectList SpeedZones { set; get; }

        public FilmModel()
        {
            Locations =
            Cameras =
            SecondCameras =
            TrafficOfficers =
            RoadTypes =
            Courts =
            SpeedZones =
            new SelectList(new List<SelectListItem>());
        }
    }
}
