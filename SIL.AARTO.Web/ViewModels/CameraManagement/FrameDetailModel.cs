﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SIL.AARTO.Web.ViewModels.CameraManagement
{
    public class FrameDetailModel
    {
        //Owner
        [Range(0, 9999999999)]
        public int OwnerIntNo { get; set; }
        [StringLength(100)]
        public string OwnerName { get; set; }
        public bool OwnerNameIsReadonly { get; set; }
        public string OwnerNameCssClass { get; set; }
        [StringLength(10)]
        public string OwnerInitials { get; set; }
        public bool OwnerInitialsIsReadonly { get; set; }
        public string OwnerInitialsCssClass { get; set; }
        [StringLength(25)]
        public string OwnerID { get; set; }
        public bool OwnerIDIsReadonly { get; set; }
        public string OwnerIDCssClass { get; set; }
        [StringLength(100)]
        public string OwnerPostalAdd1 { get; set; }
        public string OwnerPostalAdd1CssClass { get; set; }
        public bool OwnerPostalAdd1IsReadonly { get; set; }
        [StringLength(100)]
        public string OwnerPostalAdd2 { get; set; }
        public string OwnerPostalAdd2CssClass { get; set; }
        public bool OwnerPostalAdd2IsReadonly { get; set; }
        [StringLength(100)]
        public string OwnerPostalAdd3 { get; set; }
        public string OwnerPostalAdd3CssClass { get; set; }
        public bool OwnerPostalAdd3IsReadonly { get; set; }
        [StringLength(100)]
        public string OwnerPostalAdd4 { get; set; }
        public string OwnerPostalAdd4CssClass { get; set; }
        public bool OwnerPostalAdd4IsReadonly { get; set; }
        [StringLength(100)]
        public string OwnerPostalAdd5 { get; set; }
        public string OwnerPostalAdd5CssClass { get; set; }
        public bool OwnerPostalAdd5IsReadonly { get; set; }

        public string OwnerStreetAdd1 { get; set; }
        public string OwnerStreetAdd1CssClass { get; set; }
        public bool OwnerStreetAdd1IsReadonly { get; set; }
        public string OwnerStreetAdd2 { get; set; }
        public string OwnerStreetAdd2CssClass { get; set; }
        public bool OwnerStreetAdd2IsReadonly { get; set; }
        public string OwnerStreetAdd3 { get; set; }
        public string OwnerStreetAdd3CssClass { get; set; }
        public bool OwnerStreetAdd3IsReadonly { get; set; }
        public string OwnerStreetAdd4 { get; set; }
        public string OwnerStreetAdd4CssClass { get; set; }
        public bool OwnerStreetAdd4IsReadonly { get; set; }
        [StringLength(10)]
        public string OwnerPostalCode { get; set; }
        public string OwnerPostalCodeCssClass { get; set; }
        public bool OwnerPostalCodeIsReadonly { get; set; }


        //Driver
        [Range(0, 9999999999)]
        public int DriverIntNo { get; set; }
        [StringLength(100)]
        public string DriverName { get; set; }
        public bool DriverNameIsReadonly { get; set; }
        public string DriverNameCssClass { get; set; }
        [StringLength(10)]
        public string DriverInitials { get; set; }
        public bool DriverInitialsIsReadonly { get; set; }
        public string DriverInitialsCssClass { get; set; }
        [StringLength(25)]
        public string DriverID { get; set; }
        public bool DriverIDIsReadonly { get; set; }
        public string DriverIDCssClass { get; set; }
        [StringLength(100)]
        public string DriverPostalAdd1 { get; set; }
        public string DriverPostalAdd1CssClass { get; set; }
        public bool DriverPostalAdd1IsReadonly { get; set; }
        [StringLength(100)]
        public string DriverPostalAdd2 { get; set; }
        public string DriverPostalAdd2CssClass { get; set; }
        public bool DriverPostalAdd2IsReadonly { get; set; }
        [StringLength(100)]
        public string DriverPostalAdd3 { get; set; }
        public string DriverPostalAdd3CssClass { get; set; }
        public bool DriverPostalAdd3IsReadonly { get; set; }
        [StringLength(100)]
        public string DriverPostalAdd4 { get; set; }
        public string DriverPostalAdd4CssClass { get; set; }
        public bool DriverPostalAdd4IsReadonly { get; set; }
        [StringLength(100)]
        public string DriverPostalAdd5 { get; set; }
        public string DriverPostalAdd5CssClass { get; set; }
        public bool DriverPostalAdd5IsReadonly { get; set; }

        public string DriverStreetAdd1 { get; set; }
        public string DriverStreetAdd1CssClass { get; set; }
        public bool DriverStreetAdd1IsReadonly { get; set; }
        public string DriverStreetAdd2 { get; set; }
        public string DriverStreetAdd2CssClass { get; set; }
        public bool DriverStreetAdd2IsReadonly { get; set; }
        public string DriverStreetAdd3 { get; set; }
        public string DriverStreetAdd3CssClass { get; set; }
        public bool DriverStreetAdd3IsReadonly { get; set; }
        public string DriverStreetAdd4 { get; set; }
        public string DriverStreetAdd4CssClass { get; set; }
        public bool DriverStreetAdd4IsReadonly { get; set; }
        [StringLength(10)]
        public string DriverPostalCode { get; set; }
        public string DriverPostalCodeCssClass { get; set; }
        public bool DriverPostalCodeIsReadonly { get; set; }


        //Proxy
        public bool HasProxy { get; set; }
        [Range(0, 9999999999)]
        public int ProxyIntNo { get; set; }
        [StringLength(100)]
        public string ProxyName { get; set; }
        public bool ProxyNameIsReadonly { get; set; }
        public string ProxyNameCssClass { get; set; }
        [StringLength(10)]
        public string ProxyInitials { get; set; }
        public bool ProxyInitialsIsReadonly { get; set; }
        public string ProxyInitialsCssClass { get; set; }
        [StringLength(25)]
        public string ProxyID { get; set; }
        public bool ProxyIDIsReadonly { get; set; }
        public string ProxyIDCssClass { get; set; }
        [StringLength(100)]
        public string ProxyPostalAdd1 { get; set; }
        public string ProxyPostalAdd1CssClass { get; set; }
        public bool ProxyPostalAdd1IsReadonly { get; set; }
        [StringLength(100)]
        public string ProxyPostalAdd2 { get; set; }
        public string ProxyPostalAdd2CssClass { get; set; }
        public bool ProxyPostalAdd2IsReadonly { get; set; }
        [StringLength(100)]
        public string ProxyPostalAdd3 { get; set; }
        public string ProxyPostalAdd3CssClass { get; set; }
        public bool ProxyPostalAdd3IsReadonly { get; set; }
        [StringLength(100)]
        public string ProxyPostalAdd4 { get; set; }
        public string ProxyPostalAdd4CssClass { get; set; }
        public bool ProxyPostalAdd4IsReadonly { get; set; }

        public string ProxyStreetAdd1 { get; set; }
        public string ProxyStreetAdd1CssClass { get; set; }
        public bool ProxyStreetAdd1IsReadonly { get; set; }
        public string ProxyStreetAdd2 { get; set; }
        public string ProxyStreetAdd2CssClass { get; set; }
        public bool ProxyStreetAdd2IsReadonly { get; set; }
        public string ProxyStreetAdd3 { get; set; }
        public string ProxyStreetAdd3CssClass { get; set; }
        public bool ProxyStreetAdd3IsReadonly { get; set; }
        public string ProxyStreetAdd4 { get; set; }
        public string ProxyStreetAdd4CssClass { get; set; }
        public bool ProxyStreetAdd4IsReadonly { get; set; }
        [StringLength(10)]
        public string ProxyPostalCode { get; set; }
        public string ProxyPostalCodeCssClass { get; set; }
        public bool ProxyPostalCodeIsReadonly { get; set; }

        public string BtnUpdateOwner { get; set; }
        public string BtnUpdateOwnerCssClass { get; set; }
        public bool BtnUpdateOwnerEnable { get; set; }

        public FrameDetailModel()
        {
            BtnUpdateOwnerEnable = false;
            OwnerNameIsReadonly =
            OwnerIDIsReadonly =
            OwnerPostalAdd1IsReadonly =
            OwnerPostalAdd2IsReadonly =
            OwnerPostalAdd3IsReadonly =
            OwnerPostalAdd4IsReadonly =
            OwnerPostalAdd5IsReadonly =
            OwnerStreetAdd1IsReadonly =
            OwnerStreetAdd2IsReadonly =
            OwnerStreetAdd3IsReadonly =
            OwnerStreetAdd4IsReadonly =
            OwnerPostalCodeIsReadonly = 
            OwnerInitialsIsReadonly = true;

            OwnerNameCssClass =
            OwnerIDCssClass =
            OwnerPostalAdd1CssClass =
            OwnerPostalAdd2CssClass =
            OwnerPostalAdd3CssClass =
            OwnerPostalAdd4CssClass =
            OwnerPostalAdd5CssClass =
            OwnerStreetAdd1CssClass =
            OwnerStreetAdd2CssClass =
            OwnerStreetAdd3CssClass =
            OwnerStreetAdd4CssClass =
            OwnerPostalCodeCssClass =
            OwnerInitialsCssClass = "NormalBold";


            DriverNameIsReadonly =
            DriverIDIsReadonly =
            DriverPostalAdd1IsReadonly =
            DriverPostalAdd2IsReadonly =
            DriverPostalAdd3IsReadonly =
            DriverPostalAdd4IsReadonly =
            DriverPostalAdd5IsReadonly =
            DriverStreetAdd1IsReadonly =
            DriverStreetAdd2IsReadonly =
            DriverStreetAdd3IsReadonly =
            DriverStreetAdd4IsReadonly =
            DriverPostalCodeIsReadonly = 
            DriverInitialsIsReadonly = true;

            DriverNameCssClass =
            DriverIDCssClass =
            DriverPostalAdd1CssClass =
            DriverPostalAdd2CssClass =
            DriverPostalAdd3CssClass =
            DriverPostalAdd4CssClass =
            DriverPostalAdd5CssClass =
            DriverStreetAdd1CssClass =
            DriverStreetAdd2CssClass =
            DriverStreetAdd3CssClass =
            DriverStreetAdd4CssClass =
            DriverPostalCodeCssClass =
            DriverInitialsCssClass = "NormalBold";

            HasProxy = false;
            ProxyNameIsReadonly =
            ProxyIDIsReadonly =
            ProxyPostalAdd1IsReadonly =
            ProxyPostalAdd2IsReadonly =
            ProxyPostalAdd3IsReadonly =
            ProxyPostalAdd4IsReadonly =
            ProxyStreetAdd1IsReadonly =
            ProxyStreetAdd2IsReadonly =
            ProxyStreetAdd3IsReadonly =
            ProxyStreetAdd4IsReadonly =
            ProxyPostalCodeIsReadonly = 
            ProxyInitialsIsReadonly = true;

            ProxyNameCssClass =
            ProxyIDCssClass =
            ProxyPostalAdd1CssClass =
            ProxyPostalAdd2CssClass =
            ProxyPostalAdd3CssClass =
            ProxyPostalAdd4CssClass =
            ProxyStreetAdd1CssClass =
            ProxyStreetAdd2CssClass =
            ProxyStreetAdd3CssClass =
            ProxyStreetAdd4CssClass =
            ProxyPostalCodeCssClass =
            ProxyInitialsCssClass = "NormalBold";
        }

        public void InitModel()
        {
            //Proxy
            ProxyID = ProxyID ?? string.Empty;
            ProxyName = ProxyName ?? string.Empty;
            ProxyInitials = ProxyInitials ?? string.Empty;
            ProxyPostalAdd1 = ProxyPostalAdd1 ?? string.Empty;
            ProxyPostalAdd2 = ProxyPostalAdd2 ?? string.Empty;
            ProxyPostalAdd3 = ProxyPostalAdd3 ?? string.Empty;
            ProxyPostalAdd4 = ProxyPostalAdd4 ?? string.Empty;
            ProxyStreetAdd1 = ProxyStreetAdd1 ?? string.Empty;
            ProxyStreetAdd2 = ProxyStreetAdd2 ?? string.Empty;
            ProxyStreetAdd3 = ProxyStreetAdd3 ?? string.Empty;
            ProxyStreetAdd4 = ProxyStreetAdd4 ?? string.Empty;
            ProxyPostalCode = ProxyPostalCode ?? string.Empty;

            //Driver
            DriverID = DriverID ?? string.Empty;
            DriverName = DriverName ?? string.Empty;
            DriverInitials = DriverInitials ?? string.Empty;
            DriverPostalAdd1 = DriverPostalAdd1 ?? string.Empty;
            DriverPostalAdd2 = DriverPostalAdd2 ?? string.Empty;
            DriverPostalAdd3 = DriverPostalAdd3 ?? string.Empty;
            DriverPostalAdd4 = DriverPostalAdd4 ?? string.Empty;
            DriverPostalAdd5 = DriverPostalAdd5 ?? string.Empty;
            DriverStreetAdd1 = DriverStreetAdd1 ?? string.Empty;
            DriverStreetAdd2 = DriverStreetAdd2 ?? string.Empty;
            DriverStreetAdd3 = DriverStreetAdd3 ?? string.Empty;
            DriverStreetAdd4 = DriverStreetAdd4 ?? string.Empty;
            DriverPostalCode = DriverPostalCode ?? string.Empty;

            //Owner
            OwnerID = OwnerID ?? string.Empty;
            OwnerName = OwnerName ?? string.Empty;
            OwnerInitials = OwnerInitials ?? string.Empty;
            OwnerPostalAdd1 = OwnerPostalAdd1 ?? string.Empty;
            OwnerPostalAdd2 = OwnerPostalAdd2 ?? string.Empty;
            OwnerPostalAdd3 = OwnerPostalAdd3 ?? string.Empty;
            OwnerPostalAdd4 = OwnerPostalAdd4 ?? string.Empty;
            OwnerPostalAdd5 = OwnerPostalAdd5 ?? string.Empty;
            OwnerStreetAdd1 = OwnerStreetAdd1 ?? string.Empty;
            OwnerStreetAdd2 = OwnerStreetAdd2 ?? string.Empty;
            OwnerStreetAdd3 = OwnerStreetAdd3 ?? string.Empty;
            OwnerStreetAdd4 = OwnerStreetAdd4 ?? string.Empty;
            OwnerPostalCode = OwnerPostalCode ?? string.Empty;
        }

    }
}