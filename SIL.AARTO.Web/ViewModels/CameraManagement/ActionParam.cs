﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Stalberg.TMS
{
    /// <summary>
    /// Summary description for VerificationParam
    /// </summary>
    public class ActionParam
    {
        public bool SaveImageSettings { get; set; }
        public string Contrast { get; set; }
        public string Brightness { get; set; }

        public string ProcessName { get; set; }
        public string RejReason { get; set; }
        public int RejIntNo { get; set; }
        public string Registration { get; set; }
        public string VehMake { get; set; }
        public string VehType { get; set; }
        public string UserName { get; set; }
        public string ProcessType { get; set; }
        public string FrameNo { get; set; }
        public string StatusVerified { get; set; }
        public string StatusRejected { get; set; }
        public string StatusNatis { get; set; }
        public string OffenceDateTime { get; set; }
        public string Speed1 { get; set; }
        public string Speed2 { get; set; }
        public string FrameIntNo { get; set; }
        public string PreUpdatedRegNo { get; set; }
        public string PreUpdatedRejReason { get; set; }
        public int PreRejIntNo { get; set; }
        public bool AllowContinue { get; set; }
        public string PreUpdatedAllowContinue { get; set; }
        public string RowVersion { get; set; }
        public bool ChangedMake { get; set; }
        public string StatusProsecute { get; set; }
        public string StatusCancel { get; set; }

        public bool IsBatchApproved { get; set; }
        public int InReIntNo { get; set; }
        public int StatusInvestigate { get; set; }
        public string VeBiComment { get; set; }

        //Jerry 2013-05-16 add
        public string RejectSecondaryFrames { get; set; }
    }
}