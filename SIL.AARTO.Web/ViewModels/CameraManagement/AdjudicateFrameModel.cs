﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.Web.Resource.CameraManagement;
using SIL.AARTO.BLL.CameraManagement;
using Stalberg.TMS;

namespace SIL.AARTO.Web.ViewModels.CameraManagement
{
    public class AdjudicateFrameModel
    {
        public string Title { get; set; }
        public string Navigation { get; set; }

        public string PageName { get; set; }
        public string FilmNo { get; set; }
        [StringLength(10)]
        public string FrameNo { get; set; }
        [StringLength(10)]
        public string Sequence { get; set; }
        public string ErrorMessage { get; set; }

        //Jerry 2013-04-22 add IsParentFrame
        public bool IsParentFrame { get; set; }
        //Jerry 2013-06-05 add
        public bool AllowSecondaryOffence { get; set; }

        public bool AllowContinue { get; set; }
        public bool SaveImageSettings { get; set; }//adjudicate
        public bool IsInvestigation { get; set; }

        [Range(0, 9999999999)]
        public int FirstSpeed { get; set; }
        [Range(0, 9999999999)]
        public int SecondSpeed { get; set; }

        [StringLength(10)]
        public string OffenceTime { get; set; }
        public string OffenceDate { get; set; }

        public int VehicleMaker { get; set; }
        public SelectList VehicleMakers { get; set; }
        public int VehicleType { get; set; }
        public SelectList VehicleTypes { get; set; }
        [StringLength(10)]
        public string RegNo { get; set; }
        public int RejNoneValue { get; set; }
        public int RejExpiredValue { get; set; }

        private int rejectReason;
        public int RejectReason
        {
            get
            {
                return rejectReason;
            }
            set
            {
                rejectReason = value;
                //RejectionDetails rejDetails = RejectionManager.GetRejectionDetailsByRejIntNo(int.Parse(rejectReason));
                //rejectReason = rejectReason + '|' + rejDetails.RejReason;
                //if (rejDetails != null && rejDetails.RejReason.ToLower() == "none")
                if (rejectReason == RejNoneValue)
                {
                    BtnAcceptValue = ViewModelAdjudicateFrameModel.Prosecute;
                    BtnAcceptName = "Prosecute";
                    DdlRejectionDisplay = "none";
                    LblRejectionDisplay = "none";
                    //BtnShowOwnerVisible = true;
                }
                else
                {
                    BtnAcceptValue = ViewModelAdjudicateFrameModel.DoNotProsecute;
                    BtnAcceptName = "Don't prosecute";
                    DdlRejectionDisplay = string.Empty;
                    LblRejectionDisplay = string.Empty;
                    //BtnShowOwnerVisible = false;
                }
            }
        }
        public SelectList RejectReasons { get; set; }

        //20130103 added by Nancy for investigatereason
        public SelectList InvestigateReasons { get; set; }
        [Required(ErrorMessage = "*")]
        public int InvestigateReason { get; set; }

        public string VeBiComment { get; set; }
        public bool ShowVeBiComment { get; set; }

        public string NatisVehicleMaker { get; set; }
        public string NatisVehicleType { get; set; }
        public string NatisRegNo { get; set; }
        public string NatisRejReason { get; set; }

        public string NatisCodeOrError { set; get; }
        public string ASDGPSDateTime1 { set; get; }
        public string ASDGPSDateTime2 { set; get; }
        public int ASDLane1 { get; set; }
        public int ASDLane2 { get; set; }

        public string BtnAcceptValue { get; set; }
        public string BtnAcceptName { get; set; }
        public string DdlRejectionDisplay { get; set; }
        public string LblRejectionDisplay { get; set; }
        //public bool BtnShowOwnerVisible { get; set; }

        public string BtnShowOwner { get; set; }
        public string BtnShowOwnerCssClass { get; set; }

        public AdjudicateFrameModel()
        {
            VehicleMakers =
            VehicleTypes =
            RejectReasons =
            new SelectList(new List<SelectListItem>());
        }
        
        public string LocationDescr { get; set; }
        public string OfficerDetails { get; set; }
        public int SpeedZone { get; set; }
        public int LowerSpeed { get { return FirstSpeed < SecondSpeed ? FirstSpeed : SecondSpeed; } }
        public bool OverSpeedLimit40 { get { return LowerSpeed > SpeedZone + 40; } }

        public string LblNatisVehicleMakerText { get; set; }
        public string LblNatisVehicleTypeText { get; set; }
        public string LblRegistrationNo { get; set; }

        public bool PnlNatisDataVisible { get; set; }
        public bool SaveImageSettingsVisible { get; set; }
        public bool SaveSettingsForImageVisible { get; set; }

        public bool IsBusLane { get; set; }

        public int NatisVtIntNo { get; set; }
        public string ErrorNatisVTNull { get; set; }
        public string NatisVTString { get; set; }

        public bool IsFishpond { get; set; }
    }
}