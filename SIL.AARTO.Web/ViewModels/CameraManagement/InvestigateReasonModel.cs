﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.Web.ViewModels.CameraManagement
{
    public class InvestigateReasonModel
    {
        public int InReIntNo { get; set; }
        public string ErrorMsg { get; set; }

        public string SearchStr { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public LookupPartViewModel LookupModel { get; set; }

        public TList<InvestigateReason> PageInvestigateReasons { get; set; }
    }
}