﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.Web.Resource.CameraManagement;
using Stalberg.TMS;
using SIL.AARTO.BLL.CameraManagement;

namespace SIL.AARTO.Web.ViewModels.CameraManagement
{
    public class VerifyFrameModel
    {
        public string Title { get; set; }
        public bool ShowInvestigation { get; set; }
        public string Navigation { get; set; }

        public string PageName { get; set; }
        public string FilmNo { get; set; }
        [StringLength(10)]
        public string FrameNo { get; set; }
        [StringLength(10)]
        public string Sequence { get; set; }
        public string ErrorMessage { get; set; }

        //Jerry 2013-04-22 add IsParentFrame
        public bool IsParentFrame { get; set; }
        //Jerry 2013-06-05 add
        public bool AllowSecondaryOffence { get; set; }

        public bool AllowContinue { get; set; }

        [Range(0, 9999999999)]
        public int FirstSpeed { get; set; }
        [Range(0, 9999999999)]
        public int SecondSpeed { get; set; }

        [StringLength(10)]
        public string OffenceTime { get; set; }
        public string OffenceDate { get; set; }

        public int VehicleMaker { get; set; }
        public SelectList VehicleMakers { get; set; }
        public int VehicleType { get; set; }
        public SelectList VehicleTypes { get; set; }

        [Required(ErrorMessage = "*")]
        public int InvestigateReason { get; set; }
        public SelectList InvestigateReasons { get; set; }
        [StringLength(10)]
        public string RegNo { get; set; }
        public int RejNoneValue { get; set; }

        private int rejectReason;
        public int RejectReason
        {
            get
            {
                return rejectReason;
            }
            set
            {
                rejectReason = value;
                //RejectionDetails rejDetails = RejectionManager.GetRejectionDetailsByRejIntNo(int.Parse(rejectReason));
                //rejectReason = rejectReason + '|' + rejDetails.RejReason;
                //if (rejDetails != null && rejDetails.RejReason.ToLower() == "none")
                if(rejectReason == RejNoneValue)
                {
                    BtnAcceptValue = ViewModelVerifyFrameModel.Accept;
                    BtnAcceptName = "Accept";
                    DdlRejectionDisplay = "none";
                    LblRejectionDisplay = "none";
                    BtnShowOwnerVisible = true;
                }
                else
                {
                    BtnAcceptValue = ViewModelVerifyFrameModel.Reject;
                    BtnAcceptName = "Reject";
                    DdlRejectionDisplay = string.Empty;
                    LblRejectionDisplay = string.Empty;
                    BtnShowOwnerVisible = false;
                }
            }
        }
        public SelectList RejectReasons { get; set; }

        public string NatisVehicleMaker { get; set; }
        public string NatisVehicleType { get; set; }
        public string NatisRegNo { get; set; }
        public string NatisRejReason { get; set; }
        public int NatisVtIntNo { get; set; }

        public string NatisCodeOrError { set; get; }
        public string ASDGPSDateTime1 { set; get; }
        public string ASDGPSDateTime2 { set; get; }
        public int ASDLane1 { get; set; }
        public int ASDLane2 { get; set; }

        public string BtnAcceptValue { get; set; }
        public string BtnAcceptName { get; set; }
        public string DdlRejectionDisplay { get; set; }
        public string LblRejectionDisplay { get; set; }
        public bool BtnShowOwnerVisible { get; set; }

        public string BtnShowOwner { get; set; }
        public string BtnShowOwnerCssClass { get; set; }

        public VerifyFrameModel()
        {
            VehicleMakers =
            VehicleTypes =
            RejectReasons =
            new SelectList(new List<SelectListItem>());
        }
        public bool IsBusLane { get; set; }

        public string NatisVTString { get; set; }
        public string ErrorNatisVTNull { get; set; }
    }
}