﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;
using System.Configuration;
using SIL.AARTO.BLL.Utility;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.CameraManagement.Model;
using SIL.AARTO.BLL.CameraManagement;

namespace SIL.AARTO.Web.ViewModels.CameraManagement
{
    public class PreFilmModel
    {
        public PreFilmModel()
        {
            AutName = string.Empty;
            OfficerName = string.Empty;
            Films = new List<FilmEntity>();
        }

        public string AuthorityText { set; get; }
        public int AuthIntNo { set; get; }
        public string OfficerName { set; get; }
        public string ErrorMessage { set; get; }
        public string AutName { set; get; }

        public bool ShowOfficerMsg { get; set; }
        public string OfficerNo { get; set; }
        public bool AdjAt100 { get; set; }
        public bool ShowDiscrepanciesOnly { get; set; }
        public int NoOfDays { get; set; }
        public string AutCode { get; set; }
        public int USIntNo { get; set; }
        public ValidationStatus Status { get; set; }
        public List<FilmEntity> Films { get; set; }

        private ValidationStatus status = ValidationStatus.None;
        public ValidationStatus Validation { set; get; }
        public bool NaTISLast { get; set; }

        public int PageSize { get; set; }

        //20130111 (isFishpond)Added by Nancy for distinguish frame of verification bin from those of fishpond
        public void InitModel(ValidationStatus validation, UserLoginInfo userInfo, int authIntNo, bool isFishpond = false)
        {
            Validation = validation;
            status = validation;
            AuthIntNo = authIntNo;
            ShowOfficerMsg = AuthorityRulesManager.GetIsOrNotShowOfficer(userInfo.UserName, authIntNo);
            OfficerNo = UserManager.GetOfficerNo(userInfo);
            AdjAt100 = AuthorityRulesManager.GetIsOrNotAdjAt100(userInfo.UserName, authIntNo);
            ShowDiscrepanciesOnly = AuthorityRulesManager.GetIsOrNotShowDiscrepanciesOnly(userInfo.UserName, authIntNo);
            NoOfDays = DateRulesManager.GetNoOfDays(userInfo, authIntNo);
            AutCode = AuthorityManager.GetAutCode(authIntNo);
            USIntNo = UserManager.GetUSIntNo(userInfo);
            PageSize = Config.PageSize;
            Status = GetStatus(userInfo, authIntNo);
            if (OfficerNo == "0")
            {
                ErrorMessage = "There is an exception when trying to get officer number for current login user.";
                return;
            }
            Films = FilmManager.GetListFilms(authIntNo, Status, isFishpond);
        }

        public bool CheckOfficerNo(UserLoginInfo userInfo)
        {
            string toName = string.Empty;

            if (OfficerNo.Equals("") || OfficerNo.Equals("0"))
            {
                return false;
            }

            int toIntNo = (new TrafficOfficerDB(Config.ConnectionString)).ValidateTrafficOfficer(userInfo.AuthIntNo, OfficerNo, ref toName);

            if (toIntNo < 1)
            {
                return false;
            }
            else
            {
                OfficerName = toName;
                return true;
            }
        }

        public bool LockFilmForUser(int filmIntNo, Int64 rowversion, UserLoginInfo userInfo)
        {
            Int32 updFilmIntNo = FilmManager.LockUser(filmIntNo, userInfo.UserName, rowversion);
            if (updFilmIntNo <= 0)
            {
                ErrorMessage = "Unable to book film out - please try again";
                return false;
            }
            return true;
        }

        private ValidationStatus GetStatus(UserLoginInfo userInfo, int authIntNo)
        {
            NaTISLast = AuthorityRulesManager.GetIsOrNotNaTISLast(userInfo.UserName, authIntNo);
            status |= NaTISLast ? ValidationStatus.NaTISLast : ValidationStatus.None;
            return status;
        }

        public bool IsFishpond { get; set; }
    }
}
