﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIL.AARTO.BLL.CameraManagement.Model;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.Web.ViewModels.CameraManagement
{
    public class VerificationBinModel
    {
        
        public int VeBiIntNo { get; set; }
        [StringLength(10, ErrorMessage="Max length 10")]
        [Required(ErrorMessage="*")]
        public string RegNo { get; set; }
        [Required(ErrorMessage = "*")]
        [StringLength(2000, ErrorMessage="Max length 2000")]
        public string Comment { get; set; }
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage="Max length 50")]
        public string UserInsert { get; set; }

        public string SearchStr { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public TList<VerificationBin> PageVerificationBins { get; set; }
    }
}