﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIL.AARTO.Web.ViewModels.CameraManagement
{
    public class NatisPopupPartialViewModel
    {
        public string NatisVTString { get; set; }
        public string ErrorNatisVTNull { get; set; }
    }
}