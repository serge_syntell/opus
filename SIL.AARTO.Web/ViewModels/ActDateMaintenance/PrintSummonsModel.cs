﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SIL.AARTO.BLL.ActDateMaintenance;

namespace SIL.AARTO.Web.ViewModels.ActDateMaintenance
{

    public class PrintSummonsModel
    {
        [UIHint("CourtDropdown")]
        public int CrtIntNo { get; set; }

        public DateTime? CourtDateFrom { get; set; }
        public DateTime? CourtDateTo { get; set; }
        public List<PrintFileEntity> Result { get; set; }

        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public string URLPara { get; set; }
        public bool FirstLoad { get; set; }
        public int PageIndex { get; set; }

        public string GenerateHtml(string actDate)
        {
            string html = "<span name=\"sp_ActionDate\">" + actDate + "</span><input type=\"text\" name=\"actdate\" value=\"" + actDate + "\" style=\"display:none;\"/>";

            return html;
        }
    }
}