﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.Web.ViewModels.Infringement
{

    public class JsonRequest {
        public List<string> Resubmissions { get; set; }
    }
      
    public class ResubmissionModel
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public string NotTicketNo { get; set; }
        public string URLPara { get; set; }
        public TList<CisInfringementResubmission> ResubmissionList { get; set; }

    }
}