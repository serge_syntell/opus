﻿/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />
$(document).ready(function() {
    $("#ddlUserMenus").change(function() {
    $.post(_ROOTPATH + "/Admin/GetActionRolesByActID", { actID: $("#ddlUserMenus").val() },
        function(message) {
            if (message.Status == true) {
                if (message.Text != '' && message.Text != null) {
                    var roleArr = message.Text.split(';');
                    $("#ddlSelectRoleList option").remove();
                    $.each(roleArr, function(index, role) {
                        $("<option value=" + roleArr[index].split(':')[0] + ">" + roleArr[index].split(':')[1] + "</option>").appendTo($("#ddlSelectRoleList"));
                    });
                }
                else {
                    $("#ddlSelectRoleList option").remove();
                }
            }
        },
        'json');

        $.post(_ROOTPATH + "/Admin/GetActionRolesNotUseByActID", { actID: $("#ddlUserMenus").val() },
        function(message) {
            if (message.Status == true) {
                if (message.Text != '') {
                    var roleArr = message.Text.split(';');
                    $("#ddlRoleList option").remove();
                    $.each(roleArr, function(index, role) {
                        $("<option value=" + roleArr[index].split(':')[0] + ">" + roleArr[index].split(':')[1] + "</option>").appendTo($("#ddlRoleList"));
                    });
                }
            }
        }, 'json');

    });


    $("#btnAddRole").click(function() {
        var userRoles = '';
        $("#ddlRoleList option:selected").each(function(index) {
            userRoles += $("#ddlRoleList option:selected")[index].value + ";";
        });
        $.post(_ROOTPATH + "/Admin/AddRoleToAction", { actID: $("#ddlUserMenus").val(), userRoles: $("#ddlRoleList option:selected").val() },
        function(message) {
            if (message.Status == true) {
                $("#ddlRoleList option:selected").appendTo($("#ddlSelectRoleList"));
                $("#ddlRoleList option : selected").remove();
            }
        }, 'json');
    });

    $("#btnRemoveRole").click(function() {
        var userRoles = '';
        $("#ddlSelectRoleList option:selected").each(function(index) {
            userRoles += $("#ddlSelectRoleList option:selected")[index].value + ";";
        });
        $.post(_ROOTPATH + "/Admin/ReomveRoleFromAction", { actID: $("#ddlUserMenus").val(), userRoles: $("#ddlSelectRoleList option:selected").val() },
        function(message) {
            if (message.Status == true) {
                $("#ddlSelectRoleList option:selected").appendTo($("#ddlRoleList"));
                $("#ddlSelectRoleList option : selected").remove();
            }
        }, 'json');
    });

});