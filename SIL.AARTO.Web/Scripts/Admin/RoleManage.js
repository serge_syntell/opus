﻿var divRoleDescStr;

/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />
$(document).ready(function() {
    $("#btnAddRole").click(function() {
        $("#divRolePanel").show();
        $("#txtRoleName").val('');
        $("#txtRoleDescription").val('');
        $("#hidRoleID").val('');
        $("#btnDeleteRole").hide();
    });

    $("#btnDeleteRole").click(function() {
        if ($("#hidRoleID").val() == '') {
            return;
        }
        $.post(_ROOTPATH + "/Admin/DeleteRoleByID", { roleID: $("#hidRoleID").val() },
    function(message) {
        if (message.Status == true) {
            $("#formRoleManage").attr("action", $("#hidUrl").val());
            $("#formRoleManage").submit();
        }
    }, 'json');

    });

$("#btnSave").click(function () {
        if ($("#txtRoleName").val() == '') {
            alert(mulLangStr.RoleNoName);
            return;
        }
        if ($("#txtRoleDescription").val() == '') {
            alert(mulLangStr.RoleNoDescription);
            return;
        }
       //Heidi 2014-03-25 added for maintaining multiple languages(5141)
        var validLookUp = 0;
        $(".table_lookup input").each(function () {
            // if ($.trim($(this).parent().prev().text()).toUpperCase() == "ENGLISH") {
            if ($.trim($(this).next().attr("class")) == "en-US") {
                if ($.trim($(this).val()) == "") {
                    $(this).next().show();
                    validLookUp = +1;
                }
                else {
                    $(this).next().hide();
                }
            }
        });
        if (validLookUp > 0) {
            return false;
        }
        $.post(_ROOTPATH + "/Admin/GetLanguageCountUserRole", {},
           function (message) {
               var roleArry;
               roleArry = message.Text.split(';');
               roleListArry = "";
               for (var i = 0; i < roleArry.length; i += 2) {
                   roleListArry += roleArry[i + 1] + ";";
                   roleListArry += $("#role_" + roleArry[i + 1]).val() + ";";
               }
               
               $.post(_ROOTPATH + "/Admin/EditeUserRole",
                   { userRoleID: $("#hidRoleID").val(),
                       userRoleListStr: roleListArry
                   },
                   function (message) {
                       //if (message.Status == true) {
                       //Heidi 2014-04-28 moved for fixing a problem that can not add multilanguage(5141)
                           $("#formRoleManage").attr("action", $("#hidUrl").val());
                           $("#formRoleManage").submit();
                       //}
                   }, 'json');
               }, 'json');

        //$("#formRoleManage").attr("action", $("#hidUrl").val());
        //$("#formRoleManage").submit();

    });
});

function EditUserRole(roleID) {
    if (roleID == '' || roleID == 0) {
        return;
    }
    $.post(_ROOTPATH + "/Admin/GetUserRoleByID", { roleID: roleID },
    function (message) {
        if (message.Status == true) {
            $("#txtRoleName").val(message.Text.split(';')[0]);
            $("#txtRoleDescription").val(message.Text.split(';')[1]);
            //Heidi 2014-03-27 added class='table_lookup' for maintaining multiple languages(5141)
            for (var i = 2; i < message.Text.split(';').length; i += 3) {
                if (i == 2) {
                    divRoleDescStr = "<table border='0' class='table_lookup' style='background-color:Silver;width:350px;border-collapse:collapse;'>";
                }
                divRoleDescStr += "<tr><td>" + message.Text.split(';')[i + 1] + "</td><td>";

                if (i == message.Text.split(';').length - 3) {
                    divRoleDescStr += "<input type='text' id='role_" + message.Text.split(';')[i] + "'  value='" + message.Text.split(';')[i + 2] + "'><span class='" + message.Text.split(';')[i] + "' title='Required Field' style='color:red;padding-left:4px;display:none' >*</span></td></tr></table>";
                } else {
                    divRoleDescStr += "<input type='text' id='role_" + message.Text.split(';')[i] + "'  value='" + message.Text.split(';')[i + 2] + "'><span class='" + message.Text.split(';')[i] + "' title='Required Field' style='color:red;padding-left:4px;display:none' >*</span></td></tr>";
                }
            }
            $("#divUserRolelistDescTranEdit").html(divRoleDescStr);

            $("#hidRoleID").val(roleID);
            $("#divRolePanel").show();
            $("#btnDeleteRole").show();
        }
    }, 'json')
}