﻿/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />
var objMenu;
var menuID = 0;
var menuListArry;
var divMenulistTranStr;
var divMenulistDescTranStr;

$(document).ready(function () {
    $("#ul_tree_view").treeview({});

    $("#btnAddMenu").click(function () {
        //Heidi 2014-03-27 added class='table_lookup' for maintaining multiple languages(5141)
        $.post(_ROOTPATH + "/Admin/GetLanguageCount", {},
           function (message) {
               var menuArry;
               menuArry = message.Text.split(';');

               for (var i = 0; i < menuArry.length; i += 2) {
                   if (i == 0) { 
                       divMenulistTranStr = "<table border='0' class='table_lookup' style='background-color:Silver;width:350px;border-collapse:collapse;'>";
                       divMenulistDescTranStr = "<table border='0' class='table_lookup' style='background-color:Silver;width:350px;border-collapse:collapse;'>";
                   }
                   divMenulistTranStr += "<tr><td>" + menuArry[i] + "</td><td>";
                   divMenulistDescTranStr += "<tr><td>" + menuArry[i] + "</td><td>";

                   if (i == menuArry.length - 1) {
                       divMenulistTranStr += "<input type='text' id='mul_" + menuArry[i + 1] + "' ><span class='" + menuArry[i + 1] + "' title='Required Field' style='color:red;padding-left:4px;display:none' >*</span></td></tr></table>";
                       divMenulistDescTranStr += "<input type='text' id='mulDesc_" + menuArry[i + 1] + "' ><span class='" + menuArry[i + 1] + "' title='Required Field' style='color:red;padding-left:4px;display:none' >*</span></td></tr></table>";
                   } else {
                       divMenulistTranStr += "<input type='text' id='mul_" + menuArry[i + 1] + "' ><span class='" + menuArry[i + 1] + "' title='Required Field' style='color:red;padding-left:4px;display:none' >*</span></td></tr>";
                       divMenulistDescTranStr += "<input type='text' id='mulDesc_" + menuArry[i + 1] + "' ><span class='" + menuArry[i + 1] + "' title='Required Field' style='color:red;padding-left:4px;display:none' >*</span></td></tr>";
                   }
               }
               $("#divMenulistTran").html(divMenulistTranStr);
               $("#divMenulistDescTran").html(divMenulistDescTranStr);
           }, 'json');

        $("#divEditMenu").hide();
        $("#divAddMenu").show();
    });

    $("#btnAddBaseMenu").click(function () {

        menuID = 0;
        if ($("#txtMenuItem").val() == '') {
            alert(JsMenuNameIsRequired);
            //alert("Menu name is required!");
            return;
        }

        if ($("#txtOrderNo").val() == '') {
            alert(JsMenuOrderNoIsRequired);
            //alert("Menu Order No is required!");
            return;
        }
        var patrn = /^[0-9]*[1-9][0-9]*$/;
        if (!patrn.exec($("#txtOrderNo").val())) {
            alert(JsInputANumber);
            return;
        }

        //Heidi 2014-03-25 added for maintaining multiple languages(5141)
        var validLookUp = 0;
        $(".table_lookup input").each(function () {
            // if ($.trim($(this).parent().prev().text()).toUpperCase() == "ENGLISH") {
            if ($.trim($(this).next().attr("class")) == "en-US") {
                if ($.trim($(this).val()) == "") {
                    $(this).next().show();
                    validLookUp = +1;
                }
                else {
                    $(this).next().hide();
                }
            }
        });
        if (validLookUp > 0) {
            return false;
        }
       
        $.post(_ROOTPATH + "/Admin/GetLanguageCount", {},
           function (message) {
               var menuArry;
               menuArry = message.Text.split(';');
               menuListArry = "";
               for (var i = 0; i < menuArry.length; i += 2) {
                   menuListArry += menuArry[i + 1] + ";";
                   menuListArry += $("#mul_" + menuArry[i + 1]).val() + ";";
                   menuListArry += $("#mulDesc_" + menuArry[i + 1]).val() + ";";
               }
               
               $.post(_ROOTPATH + "/Admin/CreateMenu",
                   { parentMenuID: menuID,
                       menuName: $("#txtMenuItem").val(),
                       menuDesc: $("#txtMenuDesc").val(),
                       orderNo: $("#txtOrderNo").val(),
                       language: language, //$("#txtLanguage").val()
                       lastUser: lastUser,
                       menuListStr: menuListArry
                   },
                   function (message) {
                       if (message.Status == true) {
                           $("#ul_tree_view").children().remove();
                           $(message.Text).appendTo($("#ul_tree_view"));
                           $("#ul_tree_view").treeview({
                           });
                           $("#divAddMenu").hide();
                           objMenu = undefined;
                       }

                   }, 'json');
           }, 'json');



    });


    $("#btnDelete").click(function () {
        if (objMenu == undefined) {
            alert(JsSelectANode);
            //alert("Please select a node which you will delete!");
            return;
        }
        if ($(objMenu).siblings().length > 0) {
            alert(JsSelectNodeHaveChidens);
            //alert("The menu you selected have childens!");
            return;
        }
        if (menuID != 0) {
            //if (window.confirm("Are you sure to delete this node?")) {
            if (window.confirm(JsConfirmDeleteNode)) {
                $.post(_ROOTPATH + "/Admin/DeleteMenu",
                { menuID: menuID },
                function (message) {
                    if (message.Status == true) {
                        $("#ul_tree_view").children().remove();
                        $(message.Text).appendTo($("#ul_tree_view"));
                        $("#ul_tree_view").treeview({
                        });

                        objMenu = undefined;
                    }
                },
                'json');
            }
        }

    });

    $("#btnEdit").click(function () {
        $.post(_ROOTPATH + "/Admin/GetUserMenuEntityByMenuID", { menuID: menuID, language: language },
    function (message) {
        if (message.Status == true) {
            var menuArry = message.Text.split(';');
            $("#txtEditMenuItem").val(menuArry[0]);
            $("#txtEditMenuDesc").val(menuArry[1]);
            $("#txtEditOrderNo").val(menuArry[2]);
            //Heidi 2014-03-27 added class='table_lookup' for maintaining multiple languages(5141)
            for (var i = 3; i < menuArry.length; i += 4) {
                if (i == 3) {
                    divMenulistTranStr = "<table border='0' class='table_lookup' style='background-color:Silver;width:350px;border-collapse:collapse;'>";
                    divMenulistDescTranStr = "<table border='0' class='table_lookup' style='background-color:Silver;width:350px;border-collapse:collapse;'>";
                }
                divMenulistTranStr += "<tr><td>" + menuArry[i + 1] + "</td><td>";
                divMenulistDescTranStr += "<tr><td>" + menuArry[i + 1] + "</td><td>";

                if (i == menuArry.length - 4) {
                    divMenulistTranStr += "<input type='text' id='mul_" + menuArry[i] + "' value='" + menuArry[i + 2] + "'><span class='" + menuArry[i] + "' title='Required Field' style='color:red;padding-left:4px;display:none' >*</span></td></tr></table>";
                    divMenulistDescTranStr += "<input type='text' id='mulDesc_" + menuArry[i] + "' value='" + menuArry[i + 3] + "' ><span class='" + menuArry[i] + "' title='Required Field' style='color:red;padding-left:4px;display:none' >*</span></td></tr></table>";
                } else {
                    divMenulistTranStr += "<input type='text' id='mul_" + menuArry[i] + "' value='" + menuArry[i + 2] + "'><span class='" + menuArry[i] + "' title='Required Field' style='color:red;padding-left:4px;display:none' >*</span></td></tr>";
                    divMenulistDescTranStr += "<input type='text' id='mulDesc_" + menuArry[i] + "' value='" + menuArry[i + 3] + "' ><span class='" + menuArry[i] + "' title='Required Field' style='color:red;padding-left:4px;display:none' >*</span></td></tr>";
                }

            }
            $("#divMenulistTranEdit").html(divMenulistTranStr);
            $("#divMenulistDescTranEdit").html(divMenulistDescTranStr);

            $("#divEditMenu").show();
        }
    }, 'json');

        $("#divAddMenu").hide();
    });

    $("#btnAddSave").click(function () {
        if ($("#txtMenuItem").val() == '') {

            alert(JsMenuNameIsRequired);
            //alert("Menu name is required!");
            return;
        }
        if ($("#txtOrderNo").val() == '') {
            alert(JsMenuOrderNoIsRequired);
            //alert("Menu Order No is required!");
            return;
        }
        var patrn = /^[0-9]*[1-9][0-9]*$/;
        if (!patrn.exec($("#txtOrderNo").val())) {
            alert(JsInputANumber);
            return;
        }

        //Heidi 2014-03-25 added for maintaining multiple languages(5141)
        var validLookUp = 0;
        $(".table_lookup input").each(function () {
            // if ($.trim($(this).parent().prev().text()).toUpperCase() == "ENGLISH") {
            if ($.trim($(this).next().attr("class")) == "en-US") {
                if ($.trim($(this).val()) == "") {
                    $(this).next().show();
                    validLookUp = +1;
                }
                else {
                    $(this).next().hide();
                }
            }
        });
        if (validLookUp > 0) {
            return false;
        }

        $.post(_ROOTPATH + "/Admin/GetLanguageCount", {},
           function (message) {
               var menuArry;
               menuArry = message.Text.split(';');
               menuListArry = "";
               for (var i = 0; i < menuArry.length; i += 2) {
                   menuListArry += menuArry[i + 1] + ";";
                   menuListArry += $("#mul_" + menuArry[i + 1]).val() + ";";
                   menuListArry += $("#mulDesc_" + menuArry[i + 1]).val() + ";";
               }
               $.post(_ROOTPATH + "/Admin/CreateMenu",
                   { parentMenuID: menuID,
                       menuName: $("#txtMenuItem").val(),
                       menuDesc: $("#txtMenuDesc").val(),
                       orderNo: $("#txtOrderNo").val(),
                       language: language, //$("#txtLanguage").val() 
                       lastUser: lastUser,
                       menuListStr: menuListArry
                   },
                   function (message) {
                       if (message.Status == true) {
                           $("#ul_tree_view").children().remove();
                           $(message.Text).appendTo($("#ul_tree_view"));
                           $("#ul_tree_view").treeview({
                           });
                           $("#divAddMenu").hide();
                           objMenu = undefined;
                       }

                   }, 'json');
           }, 'json');


    });
    $("#btnEditSave").click(function () {

        if ($("#txtEditMenuItem").val() == '') {
            alert(JsMenuNameIsRequired);
            return;
        }
        if ($("#txtEditOrderNo").val() == '') {
            alert(JsMenuOrderNoIsRequired);
            return;
        }
        var patrn = /^[0-9]*[1-9][0-9]*$/;
        if (!patrn.exec($("#txtEditOrderNo").val())) {
            alert(JsInputANumber);
            return;
        }

        //Heidi 2014-03-25 added for maintaining multiple languages(5141)
        var validLookUp = 0;
        $(".table_lookup input").each(function () {
            // if ($.trim($(this).parent().prev().text()).toUpperCase() == "ENGLISH") {
            if ($.trim($(this).next().attr("class")) == "en-US") {
                if ($.trim($(this).val()) == "") {
                    $(this).next().show();
                    validLookUp = +1;
                }
                else {
                    $(this).next().hide();
                }
            }
        });
        if (validLookUp > 0) {
            return false;
        }

        $.post(_ROOTPATH + "/Admin/GetLanguageCount", {},
           function (message) {
               var menuArry;
               menuArry = message.Text.split(';');
               menuListArry = "";
               for (var i = 0; i < menuArry.length; i += 2) {
                   menuListArry += menuArry[i + 1] + ";";
                   menuListArry += $("#mul_" + menuArry[i + 1]).val() + ";";
                   menuListArry += $("#mulDesc_" + menuArry[i + 1]).val() + ";";
               }
               $.post(_ROOTPATH + "/Admin/EditMenu",
                   { menuID: menuID,
                       menuName: $("#txtEditMenuItem").val(),
                       menuDesc: $("#txtEditMenuDesc").val(),
                       orderNo: $("#txtEditOrderNo").val(),
                       language: language, //$("#txtEditLanguage").val()
                       lastUser: lastUser,
                       menuListStr: menuListArry
                   },
                   function (message) {
                       if (message.Status == true) {
                           $("#ul_tree_view").children().remove();
                           $(message.Text).appendTo($("#ul_tree_view"));
                           $("#ul_tree_view").treeview({
                           });
                           $("#divEditMenu").hide();
                           objMenu = undefined;
                       }

                   }, 'json');
           }, 'json');


    });

})

function getMenuID(obj, pmenuID) {
    if (pmenuID != 0) {
        menuID = pmenuID;
        $("#txtCurrentMenu").val($(obj).text());
        $.post(_ROOTPATH + "/Admin/GetMenuByID", { menuID: pmenuID },
        function(message) {
            $("#txtUrl").val('');
            if (message.Status == true) {
                $("#txtUrl").val(message.Text);
                $("#divAddMenu").hide();
                $("#divEditMenu").hide();
            }
        }, 'json');
        objMenu = obj;
    }
}



//var branches;

//if (objMenu != undefined) {
//    var obj = $(objMenu).parent();
//    var hasUL = false;
//    $(objMenu).siblings().each(function(index, oo) {

//        obj = oo;
//        hasUL = true;
//    });
//    // query if it's child has ul 
//    //var s = $(obj).children("ul")
//    if (hasUL == true) {
//        branches = $("<li><span onclick='getMenuID(this," + message.Text + ");'>" + $("#txtMenuItem").val() + "</span>" +
//				"</li>").appendTo($(obj));
//    }
//    else {
//        branches = $("<ul><li><span onclick='getMenuID(this," + message.Text + ");'>" + $("#txtMenuItem").val() + "</span>" +
//				"</li></ul>").appendTo($(obj));
//    }

//    objMenu = undefined;

//}
//else {
//    branches = $("<li class='open'><span  onclick='getMenuID(this," + message.Text + ");'>" + $("#txtMenuItem").val() + "</span>" +
//				"</li>").appendTo($("#ul_tree_view"));
//}


//$("#ul_tree_view").treeview({
//    add: branches
//});
