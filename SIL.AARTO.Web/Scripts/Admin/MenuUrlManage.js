﻿/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />
var objMenu;
var menuID = 0;
//var language = 'en-us';
//var lastUser = 'Jake';
var divMenulistTranStr;
var divMenulistDescTranStr;

$(document).ready(function () {
    $("#txtUrl").attr("disabled", true);
    $("#ul_tree_view").treeview({

    });

    $("#btnAddUrl").click(function () {
        if (menuID == 0) {
            alert(JsSelectAMenu);
            return;
        }
        if ($("#lbPageList").val() == null) {
            alert(JsSelectAPage);
            return;
        }
        $.post(_ROOTPATH + "/Admin/CheckMenuByID", { menuID: menuID },
        function (message) {
            if (message.Status == true) {
                setTimeout(
                $.post(_ROOTPATH + "/Admin/AssignMenuUrl", { menuID: menuID, pageID: $("#lbPageList").val() },
                function (message) {
                    if (message.Status == false) {
                        alert(JsAssignUrlUnSucessful);
                    }
                    else {
                        $("#lbPageList option:selected").remove();
                    }
                }, 'json'), 400);
            }
            else {
                alert(JsAssignUrlUnSucessful)
            }
        }, 'json');

    });

    $("#btnRemoveUrl").click(function () {
        if (menuID == 0) {
            alert(JsSelectAMenu);
            return;
        }
        if ($("#txtUrl").val() == "" || $("#txtUrl").val() == null) {
            alert(JsNoPageRemove);
        }
        $.post(_ROOTPATH + "/Admin/RemoveMenuUrlByID", { menuID: menuID },
    function (message) {
        if (message.Status == false) {
            alert(JsRemovePageUnSucessful);
        }
        else {
            $("#txtUrl").val("");
            setTimeout(
                $.post(_ROOTPATH + "/Admin/GetPageListNotUse", "",
                function (menuList) {
                    if (menuID != null) {
                        $("#lbPageList option").remove();
                        $.each(menuList, function (index, menuUrl) {
                            $("<option value=" + menuUrl.MenuPageID + ">" + menuUrl.MenuPageUrl + "</option>").appendTo($("#lbPageList"));
                        });
                    }
                }
                , 'json')
            , 400);
        }
    }, 'json')
    });

    $("#lbPageList").click(function () {
        var pageID = $("#lbPageList option:selected").val();
        if (pageID != null) {
            $.post(_ROOTPATH + "/Admin/GetPageDetail", { pageID: pageID },
            function (message) {
                var pageArry;
                pageArry = message.Text.split(';');

                $("#txtPageName").val(pageArry[0]);
                $("#txtPageDesc").val(pageArry[1]);
                $("#txtUrl").val(pageArry[2]);


                for (var i = 3; i < pageArry.length; i += 3) {
                    if (i == 3) {
                        divMenulistTranStr = "<table border='0' style='background-color:Silver;width:350px;border-collapse:collapse;'>";
                        divMenulistDescTranStr = "<table border='0' style='background-color:Silver;width:350px;border-collapse:collapse;'>";
                    }
                    divMenulistTranStr += "<tr><td>" + pageArry[i] + "</td><td>";
                    divMenulistDescTranStr += "<tr><td>" + pageArry[i] + "</td><td>";

                    if (i == pageArry.length - 3) {
                        divMenulistTranStr += "<input type='text' readonly='true'  value='" + pageArry[i + 1] + "'></td></tr></table>";
                        divMenulistDescTranStr += "<input type='text' readonly='true'   value='" + pageArry[i + 2] + "' ></td></tr></table>";
                    } else {
                        divMenulistTranStr += "<input type='text' readonly='true'   value='" + pageArry[i + 1] + "'></td></tr>";
                        divMenulistDescTranStr += "<input type='text' readonly='true'   value='" + pageArry[i + 2] + "' ></td></tr>";
                    }

                }
                $("#divTranName").html(divMenulistTranStr);
                $("#divTranDesc").html(divMenulistTranStr);

            }, 'json');
        }
        //$("#txtUrl").val($("#lbPageList option:selected").text());
    });
    $("#lbPageList").change(function () {
        var pageID = $("#lbPageList option:selected").val();
        if (pageID != null) {
            $.post(_ROOTPATH + "/Admin/GetPageDetail", { pageID: pageID },
            function (message) {
                if (message.Status == false) {
                    var pageArry;
                    pageArry = message.Text.split(';');
                    //                    if (pageArry.length == 3) {
                    $("#txtPageName").val(pageArry[0]);
                    $("#txtPageDesc").val(pageArry[1]);
                    $("#txtUrl").val(pageArry[2]);

                    //                    }
                }

            }, 'json');
        }
    });

    //$("#btnDelete").click(function() {
    //    if (objMenu == undefined) {
    //        alert(JsSelectNodeToDelete);
    //        return;
    //    }
    //    if (menuID != 0) {
    //        if (window.confirm(JsConformDeleteNode)) {
    //            $.post("/Admin/DeleteMenu",
    //                { menuID: menuID },
    //                function(message) {
    //                    if (message.Status == true) {
    //                        $("#ul_tree_view").children().remove();
    //                        $(message.Text).appendTo($("#ul_tree_view"));
    //                        $("#ul_tree_view").treeview({
    //                    });
    //                    objMenu = undefined;
    //                }
    //            },
    //                'json');
    //        }
    //    }

    //});

})

function getMenuID(obj, pmenuID) {
    if (pmenuID != 0) {
        menuID = pmenuID;
        $("#txtCurrentMenu").val($(obj).text());
        $.post(_ROOTPATH + "/Admin/GetMenuByID", { menuID: pmenuID },
        function(message) {
            $("#txtUrl").val('');
            $("#txtPageName").val('');
            $("#txtPageDesc").val('');
            if (message.Status == true) {
                var pageArry;
                pageArry = message.Text.split(';');
                if (pageArry.length >= 3) {
                    $("#txtPageName").val(pageArry[0]);
                    $("#txtPageDesc").val(pageArry[1]);
                    $("#txtUrl").val(pageArry[2]);
                    
                    for (var i = 3; i < pageArry.length; i += 3) {                       
                        if (i == 3) {
                            divMenulistTranStr = "<table border='0' style='background-color:Silver;width:350px;border-collapse:collapse;'>";
                            divMenulistDescTranStr = "<table border='0' style='background-color:Silver;width:350px;border-collapse:collapse;'>";
                        }
                        divMenulistTranStr += "<tr><td>" + pageArry[i] + "</td><td>";
                        divMenulistDescTranStr += "<tr><td>" + pageArry[i] + "</td><td>";


                        if (i == pageArry.length - 3) {
                            divMenulistTranStr += "<input type='text' readonly='true'   value='" + pageArry[i + 1] + "'></td></tr></table>";
                            divMenulistDescTranStr += "<input type='text' readonly='true'   value='" + pageArry[i + 2] + "' ></td></tr></table>";
                        } else {
                            divMenulistTranStr += "<input type='text' readonly='true'   value='" + pageArry[i + 1] + "'></td></tr>";
                            divMenulistDescTranStr += "<input type='text' readonly='true'   value='" + pageArry[i + 2] + "' ></td></tr>";
                        }
                    }
                    $("#divTranName").html(divMenulistTranStr);
                    $("#divTranDesc").html(divMenulistTranStr);
                }

            }
        }, 'json');
        objMenu = obj;
    }
}

