﻿/// <reference path="../jquery-1.4.4-vsdoc.js" />
$(function () {

    var newdate = new Date();
    var newtimems = new Date().getTime() + (1 * 24 * 60 * 60 * 1000);
    newdate.setTime(newtimems);

    $("#txtCourtDate").datepicker({ dateFormat: 'yy-mm-dd', dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true, minDate: newdate });

    $("#SearchAuthority").change(function () {

        $.post(_ROOTPATH + "/Admin/GetCourts",
            {
                autIntNo: $(this).val()
            }, function (items) {
                if (items != null && items != undefined) {
                    $("#SearchCourt option[value!=0]").remove();
                    $.each(items, function (index, item) {
                        $("<option value='" + item.Value + "'>" + item.Text + "</option>").appendTo($("#SearchCourt"));
                    });
                }
            }, 'json');
    });

    $("#SearchCourt").change(function () {
        $.post(_ROOTPATH + "/WOAManagement/GetCourtRoomByCourt",
            {
                crtIntNo: $(this).val()
            }, function (items) {
                if (items != null && items != undefined) {
                    $("#SearchCourtRoom option[value!=0]").remove();
                    $.each(items, function (index, item) {
                        $("<option value='" + item.CrtRIntNo + "'>" + item.CrtRoomName + "</option>").appendTo($("#SearchCourtRoom"));
                    });
                }
            }, 'json');
    });

    $("#btnCreate").click(function () {
        if ($("#SearchAuthority").val() == "0") {
            alert("Authority is required");
            return false;
        }
        if ($("#SearchCourt").val() == "0") {
            alert("Court is required");
            return false;
        }
        if ($("#SearchCourtRoom").val() == "0") {
            alert("Court Room is required");
            return false;
        }

        if ($("#OriginGroup option").length == 0) {

            $.post(_ROOTPATH + "/Admin/GetOriginGroup",
                {
                    crtIntNo: $("#SearchCourt").val()
                },
                function (items) {
                    $.each(items, function (index, item) {
                        $("<option value='" + item.Value + "'>" + item.Text + "</option>").appendTo($("#OriginGroup"));
                    });
                    showEdit(0);

                }, "json");

            return;
        } else
            showEdit(0);
    });

    $("#btnSearch").click(function () {
        if ($("#SearchAuthority").val() == "0") {
            alert("Authority is required");
            return false;
        }
        if ($("#SearchCourt").val() == "0") {
            alert("Court is required");
            return false;
        }
        if ($("#SearchCourtRoom").val() == "0") {
            alert("Court Room is required");
            return false;
        }
    });
})

function showEdit(id) {
    $("#txtNoofCases").focus();

    if (id > 0) {
        $.post(_ROOTPATH + "/Admin/GetCourtDateForWarrant",
            {
                id: id
            }, function (item) {
                if (item != null && item != undefined) {
                    if (item.Status == true) {
                        $("#txtCourtDate").val(item.CdateWar);
                        $("#txtNoofCases").val(item.CdNoOfCases);
                        $("#OriginGroup").val(item.OGIntNo);

                        showDialog(id);
                    }
                    else {
                        alert(item.Message);
                        return;
                    }
                }
            }, 'json');
    }
    else {
        $("#txtCourtDate").val('');
        $("#txtNoofCases").val('');
        $("#OriginGroup").val('0');
        showDialog(0);
    }

}

function showDialog(id) {
    $("#pnlCourtDateEdit").dialog({
        position: "center", resizable: false, width: 430, modal: true,
        buttons: {
            "Submit": function () {

                if ($("#txtNoofCases").val() == "") {
                    alert("No of cases is required");
                    return;
                }
                var reg = /^[1-9]([0-9])*$/; // new RegExp("^[1-9]\d*$");
                //var re = new RegExp("^\d*$", "g");
                if (!reg.test($("#txtNoofCases").val()) || $("#txtNoofCases").val() > 32767) {
                    alert("Invalid value for No of cases");
                    return;
                }


                $.post(_ROOTPATH + "/Admin/SubmitCourtDatesForWarrant",
                    {
                        id: id,
                        autIntNo: $("#SearchAuthority").val(),
                        crtRIntNo: $("#SearchCourtRoom").val(),
                        cDate: $("#txtCourtDate").val(),
                        noofCases: $("#txtNoofCases").val(),
                        orginGroup: $("#OriginGroup").val()
                    },
                    function (msg) {
                        if (msg != undefined && msg != null) {
                            if (msg.Status == true) {
                                document.forms[0].submit();
                            }
                            else {
                                if (msg.Text != null && msg.Text != '') {
                                    alert(msg.Text);
                                }
                            }
                        }
                    }, "json");
            },
            "Cancel": function () {
                $("#pnlCourtDateEdit").dialog('close');
            }
        }
    });
}