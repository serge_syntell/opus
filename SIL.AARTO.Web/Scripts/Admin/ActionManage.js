﻿/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />
$(document).ready(function() {
    $("#btnSaveAction").click(function() {
    $.post(_ROOTPATH + "/Admin/EditAction", { actID: $("#hidActionID").val(), meID: $("#ddlMenu").val() },
    function(message) {
        if (message.Status == true) {
            $("#formActionManage").attr("action", $("#hidUrl").val());
            $("#formActionManage").submit();
        }
    }, 'json');
    });
});

function editAction(actID) {
    $.post(_ROOTPATH + "/Admin/GetActionEntity", { actID: actID },
    function(message) {
        if (message.Status == true) {
            var actionArry = message.Text.split(';');
            $("#txtActionName").val(actionArry[0]);
            $("#ddlMenu").val(actionArry[1]);
            $("#hidActionID").val(actID);
            $("#divActionEdit").show();
        }
    }, 'json');
    
}