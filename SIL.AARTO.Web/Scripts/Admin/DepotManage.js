﻿/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />
var divDepotDescStr = "";
$(document).ready(function () {

    $("#btnAdd").click(function () {
        $("#txtDepotDesc").val('');
        $("#txtDepotCode").val('');
        $("#ddpMetro").val("0")
        $("#MetroEditPanel").show();
       //Heidi 2014-03-25 added for maintaining multiple languages(5141)
       $.post(_ROOTPATH + "/Admin/GetLanguageCountDepot", {},
       function (message) {
           var menuArry;
           menuArry = message.Text.split(';');
           for (var i = 0; i < menuArry.length; i += 2) {
               if (i == 0) {
                   divDepotDescStr = "<table border='0' class='table_lookup' style='background-color:Silver;width:350px;border-collapse:collapse;'>";
               }
               divDepotDescStr += "<tr><td>" + menuArry[i] + "</td><td>";

               if (i == menuArry.length - 1) {
                   divDepotDescStr += "<input type='text' id='depotlookup_" + menuArry[i + 1] + "' ><span class='" + menuArry[i + 1] + "' title='Required Field' style='color:red;padding-left:4px;display:none' >*</span></td></tr></table>";
               } else {
                   divDepotDescStr += "<input type='text' id='depotlookup_" + menuArry[i + 1] + "' ><span class='" + menuArry[i + 1] + "' title='Required Field' style='color:red;padding-left:4px;display:none' >*</span></td></tr>";
               }
           }
           $("#divDepotlistDescTranEdit").html(divDepotDescStr);
       }, 'json');

      

    });

    $("#btnSave").click(function () {
        if ($("#ddpMetro").val() == '0') {
            alert(jsSelectMetro);
            $("#ddpMetro").focus();
            return;
        }
        if ($("#txtDepotCode").val() == "") {
            alert(jsDepotCode);
            $("#txtDepotCode").focus();
            return;
        }
        if ($("#txtDepotDesc").val() == "") {
            alert(jsDepotDescription);
            $("#txtDepotDesc").focus();
            return;
        }
        //Heidi 2014-03-25 added for maintaining multiple languages(5141)
        var validLookUp = 0;
        $(".table_lookup input").each(function () {
            // if ($.trim($(this).parent().prev().text()).toUpperCase() == "ENGLISH") {
            if ($.trim($(this).next().attr("class")) == "en-US") {
                if ($.trim($(this).val()) == "") {
                    $(this).next().show();
                    validLookUp = +1;
                }
                else {
                    $(this).next().hide();
                }
            }
        });
        if (validLookUp>0)
        {
            return false;
        }
        //Heidi 2014-03-25 added for maintaining multiple languages(5141)
        $.post(_ROOTPATH + "/Admin/GetLanguageCountDepot", {},
         function (message) {
             var depotLookUpArry;
             depotLookUpArry = message.Text.split(';');
             depotLookUpListArry = "";
             for (var i = 0; i < depotLookUpArry.length; i += 2) {
                 depotLookUpListArry += depotLookUpArry[i + 1] + ";";
                 depotLookUpListArry += $("#depotlookup_" + depotLookUpArry[i + 1]).val() + ";";
             }
             $.post(_ROOTPATH + "/Admin/SaveDepot",
                 {
                     depId: $("#hidDepId").val(),
                     mtrIntNo: $("#ddpMetro").val(),
                     depCode: $("#txtDepotCode").val(),
                     depDescr: $("#txtDepotDesc").val(),
                     depotListStr: depotLookUpListArry
                 },
                 function (message) {
                     if (message.Status == true) {
                         $("#formDepotManage").attr("action", $("#hidUrl").val());
                         $("#formDepotManage").submit();
                         $("#MetroEditPanel").hide();
                     }
                 }, 'json');
         }, 'json');
    });
});

function ShowEditPanel(depId) {
    if (depId != '' && depId != 0) {

        $.post(_ROOTPATH + "/Admin/GetDepot",
        {
            depId: depId
        },
        function (message) {

            if (message.Status == true) {
                var depArr = message.Text.split(':');

                $("#txtDepotCode").val(depArr[1]);
                $("#txtDepotDesc").val(depArr[2]);
                $("#ddpMetro").val(depArr[0]);

                   //Heidi 2014-03-25 added for maintaining multiple languages(5141)
                    $.post(_ROOTPATH + "/Admin/GetDepotLookUpList",
                   {
                       depId: depId
                   },
                   function (message) {
                       if (message.Status == true) {
                           for (var i = 0; i < message.Text.split(';').length; i += 3) {
                               if (i == 0) {
                                   divDepotDescStr = "<table class='table_lookup' border='0' style='background-color:Silver;width:350px;border-collapse:collapse;'>";
                               }
                               divDepotDescStr += "<tr><td>" + message.Text.split(';')[i + 1] + "</td><td>";

                               if (i == message.Text.split(';').length - 3) {
                                   divDepotDescStr += "<input type='text' id='depotlookup_" + message.Text.split(';')[i] + "'  value='" + message.Text.split(';')[i + 2] + "'><span class='" + message.Text.split(';')[i] + "' title='Required Field' style='color:red;padding-left:4px;display:none' >*</span></td></tr></table>";
                               } else {
                                   divDepotDescStr += "<input type='text' id='depotlookup_" + message.Text.split(';')[i] + "'  value='" + message.Text.split(';')[i + 2] + "'><span class='" + message.Text.split(';')[i] + "' title='Required Field' style='color:red;padding-left:4px;display:none' >*</span></td></tr>";
                               }
                           }
                           $("#divDepotlistDescTranEdit").html(divDepotDescStr);
                       }
                   }, 'json');

                $("#hidDepId").val(depId);
                $("#MetroEditPanel").show();

            }
        }, 'json');
    }
}