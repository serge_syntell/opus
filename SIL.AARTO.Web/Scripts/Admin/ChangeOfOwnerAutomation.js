﻿var urlCOOPrefix = _ROOTPATH + "/Admin/";

function IsNullOrEmpty(dataStr, nullValue) {
    if (typeof (dataStr) == "undefined"
        || dataStr == null
            || dataStr.length == 0
                || dataStr == nullValue)
        return true;
    else
        return false;
}

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return null;
}

$(document).ready(function () {
    Initialization();
});

function Initialization() {
    $("#InfoTable").hide();
    $("#ProxyTable").hide();
    $("#btnNewInfo").click(function () {
        $("#lblError").html("");
        $("#InfoTable").show();
        $("#InfoTitleTable").hide();
        $("#ProxyTable").hide();
        $("#ProxyGrid").hide();

        $("#CCIIntNo").val("0");
        $("#CompanyName").val("");
        $("#CompanyName").val("");
        $("#CompanyCode").val("");
        $("#RegistrationNumber").val("");
        $("#Telephone").val("");
        $("#Email").val("");
        $("#PhysicalAddress").val("");
        $("#PostalAddress").val("");
    });

    $("#btnCheckProxy").click(function () {
        if ($.trim($("#CCPNumber").val()) != "") {
            var urlPost = urlCOOPrefix + "CheckProxy";
            $.post(urlPost, { CCPNumber: $("#CCPNumber").val(), CCIIntNo: $("#CCIIntNo_PK").val() }, function (data) {
                if (data.Status) {
                    if (data.Count > 0) {
                        $("#CCPNumber").attr("disabled", true);
                        $("#btnCheckProxy").attr("disabled", true);
                        $("#IsCheckNumber").val("1");
                    }
                    else {
                        $("#CCPNumber").attr("disabled", true);
                        $("#btnCheckProxy").attr("disabled", true);
                        $("#PasswordTable").show();
                        $("#IsChangePassword").hide();
                        $("#IsChangePasswordSpan").hide();
                        $("#EmailTable").show();
                        $("#IsCheckNumber").val("1");
                    }
                }
                else {
                    $("#lblError").html(data.ErrorMsg);
                }
            }, "json");
        }
    });

    $("#IsChangePassword").click(function () {
        if ($("#IsChangePassword").is(":checked")) {
            $("#CCPPassword").show();
            $("#CCPPassword").val("");
        }
        else {
            $("#CCPPassword").hide();
            $("#PasswordSpan").html("");
            $("#CCPPassword").val("");
        }
    });
}

function COOIGet(cciIntNo) {
    $("#lblError").html("");
    $("#InfoTable").show();
    $("#InfoTitleTable").hide();
    $("#ProxyTable").hide();
    $("#ProxyGrid").hide();

    $("#CCIIntNo").val("0");
    $("#CompanyName").val("");
    $("#CompanyName").val("");
    $("#CompanyCode").val("");
    $("#RegistrationNumber").val("");
    $("#Telephone").val("");
    $("#Email").val("");
    $("#PhysicalAddress").val("");
    $("#PostalAddress").val("");

    var urlPost = urlCOOPrefix + "COOIGet";
    $.post(urlPost, { CCIIntNo: cciIntNo }, function (data) {
        $("#CCIIntNo").val(data.CCIIntNo);
        $("#CompanyName").val(data.CCICompanyName);
        $("#CompanyCode").val(data.CCICompanyCode);
        $("#RegistrationNumber").val(data.CCIRegistrationNumber);
        $("#Telephone").val(data.CCITelephone);
        $("#Email").val(data.CCIEmail);
        $("#PhysicalAddress").val(data.CCIPhysicalAddress);
        $("#PostalAddress").val(data.CCIPostalAddress);
    }, "json");
}

function COOIGetProxies(cciIntNo) {
    var Page = getQueryString("Page");
    var SearchStr = getQueryString("SearchStr");
    var urlRequest = urlCOOPrefix + String.format("ChangeOfOwnerAutomationSetup?Page={0}&SearchStr={1}&SelectCCIIntNo={2}", Page, SearchStr, cciIntNo);
    window.location.href = urlRequest;
}

function COOIAddProxy(cciIntNo) {
    $("#lblError").html("");
    $("#InfoTable").hide();
    $("#InfoTitleTable").hide();
    $("#ProxyTable").show();
    $("#ProxyGrid").hide();

    $("#CCPIntNo").val("0");
    $("#CCIIntNo_PK").val(cciIntNo);
    $("#CCPNumber").val("");
    $("#CCPPassword").val("");
    $("#CCPEmailAddress").val("");
    $("#IsRevoked").attr("checked", false);
    $("#IsChangePassword").attr("checked", false);

    $("#CCPNumber").attr("disabled", false);
    $("#IsCheckNumber").val("0");
    $("#btnCheckProxy").attr("disabled", false);
    $("#btnCheckProxy").show();
    $("#PasswordTable").hide();
    $("#EmailTable").hide();
}

function COOIDelete(cciIntNo) {
    $("#lblError").html("");
    var urlPost = urlCOOPrefix + "COOIDelete";
    var Page = getQueryString("Page");
    var SearchStr = getQueryString("SearchStr");
    var SelectCCIIntNo = getQueryString("SelectCCIIntNo");
    var urlRequest = urlCOOPrefix + String.format("ChangeOfOwnerAutomationSetup?Page={0}&SearchStr={1}&SelectCCIIntNo={2}", Page, SearchStr, SelectCCIIntNo);

    $.post(urlPost, { CCIIntNo: cciIntNo }, function (data) {
        if (data.Status) {
            window.location.href = urlRequest;
        } else {
            $("#lblError").html(data.ErrorMsg);
        }
    }, "json");
}

function COOPGet(ccpIntNo) {
    $("#lblError").html("");
    $("#InfoTable").hide();
    $("#InfoTitleTable").hide();
    $("#ProxyTable").show();
    $("#ProxyGrid").hide();

    $("#CCPIntNo").val(ccpIntNo);
    $("#CCPNumber").val("");
    $("#CCPPassword").val("");
    $("#CCPPassword").hide();
    $("#CCPEmailAddress").val("");
    $("#IsRevoked").attr("checked", false);
    $("#IsChangePassword").attr("checked", false);
    $("#IsChangePassword").show();

    $("#CCPNumber").attr("disabled", true);
    $("#IsCheckNumber").val("1");
    $("#btnCheckProxy").hide();
    $("#PasswordTable").show();
    $("#EmailTable").show();

    var urlPost = urlCOOPrefix + "COOPGet";
    $.post(urlPost, { CCPIntNo: ccpIntNo }, function (data) {
        $("#CCPIntNo").val(data.CCPIntNo);
        $("#CCPNumber").val(data.CCPNumber);
        $("#CCPEmailAddress").val(data.CCPEmailAddress);
        $("#IsRevoked").attr("checked", data.IsRevoked);
    }, "json");
}

function COOPSave(tip) {
    $("#lblError").html("");

    if ($("#IsCheckNumber").val() == "0") {
        alert(tip);
        return;
    }
    if ($("#IsChangePassword").attr("checked") && $("#CCPPassword").val() == "") {
        $("#CCPPassword").focus();
        return;
    }

    if (!$("#ProxyForm").valid())
        return;

    var urlPost = urlCOOPrefix + "COOPSave";

    var Page = getQueryString("Page");
    var SearchStr = getQueryString("SearchStr");
    var SelectCCIIntNo = getQueryString("SelectCCIIntNo");
    var urlRequest = urlCOOPrefix + String.format("ChangeOfOwnerAutomationSetup?Page={0}&SearchStr={1}&SelectCCIIntNo={2}", Page, SearchStr, SelectCCIIntNo);
    $.post(urlPost,
    {
        CCPIntNo: $("#CCPIntNo").val(),
        CCIIntNo_PK: $("#CCIIntNo_PK").val(),
        CCPNumber: $("#CCPNumber").val(),
        CCPPassword: $("#CCPPassword").val(),
        CCPEmailAddress: $("#CCPEmailAddress").val(),
        IsRevoked: $("#IsRevoked").is(":checked")
    },
    function (data) {
        if (data.Status) {
            window.location.href = urlRequest;
        } else {
            $("#lblError").html(data.ErrorMsg);
        }
    }, "json");
}

function COOPDelete(ccpIntNo) {
    $("#lblError").html("");
    var urlPost = urlCOOPrefix + "COOPDelete";
    var Page = getQueryString("Page");
    var SearchStr = getQueryString("SearchStr");
    var SelectCCIIntNo = getQueryString("SelectCCIIntNo");
    var urlRequest = urlCOOPrefix + String.format("ChangeOfOwnerAutomationSetup?Page={0}&SearchStr={1}&SelectCCIIntNo={2}", Page, SearchStr, SelectCCIIntNo);

    $.post(urlPost, { CCPIntNo: ccpIntNo }, function (data) {
        if (data.Status) {
            window.location.href = urlRequest;
        } else {
            $("#lblError").html(data.ErrorMsg);
        }
    }, "json");
}