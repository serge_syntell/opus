﻿/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />
$(document).ready(function () {
    $("#btnAddRole").click(function () {
        var userRoles = '';
        $("#ddlRoles option:selected").each(function (index) {
            userRoles += $("#ddlRoles option:selected")[index].value + ";";
        });
        $.post(_ROOTPATH + "/Admin/AddRoleToUser", { userIntNo: $("#hidUserIntNo").val(), roles: userRoles },
            function (message) {
                if (message.Status == true) {
                    $("#ddlRoles option:selected").appendTo($("#ddlUserRoles"));
                    $("#ddlRoles option : selected").remove();
                }
            }, 'json');
    });

    $("#btnRemoveRole").click(function () {
        var userRoles = '';
        $("#ddlUserRoles option:selected").each(function (index) {
            userRoles += $("#ddlUserRoles option:selected")[index].value + ";";
        });
        $.post(_ROOTPATH + "/Admin/RemoveRoleFormUser", { userIntNo: $("#hidUserIntNo").val(), roles: userRoles },
            function (message) {
                if (message.Status == true) {
                    $("#ddlUserRoles option:selected").appendTo($("#ddlRoles"));
                    $("#ddlUserRoles option : selected").remove();
                }
            }, 'json');
    });
    $("#btnPreMenu").click(function () {
        $("#formUserRoleManage").attr("action", $("#hidUrl").val());
        $("#formUserRoleManage").submit();
    });

    $("#btnAddUser").click(function () {
        $("#hidUserIntNo").val("");
        $("#ddlUserAuthority").val("");
        $("#txtAddUserSName").val("");
        $("#txtAddUserInit").val("");
        $("#txtAddUserFName").val("");
        $("#txtAddUserLoginName").val("");
        $("#txtAddUserEmail").val("");
        // $("#txtAddUserAccessLevel").val("");
        $("#ddlTrafficOfficer").val("");
        $("#ddlLanguageSlector").val("");
        $("#tbUserEditPanel").show();
    });

    //    $("#btnDeleteUser").click(function() {
    //        var userID = $("#hidUserIntNo").val();
    //        if (userID == '' || userID == 0) {
    //            alert("Please select a User!");
    //            return;
    //        }
    //        $.post("/Admin/DeleteUser", { userID: userID },
    //        function(message) {
    //            if (message.Status == true) {
    //                $("#formUserManage").attr("action", $("#hidUrl").val());
    //                
    //            }
    //        }
    //        , 'json')
    //    });

    $("#btnSaveUser").click(function () {
        $("#formUserManage").attr("action", $("#hidUrl").val());
        //$("#formUserManage").submit();
        return;
    });

});

function OpenWin(userID) {
    window.open(_ROOTPATH + "/Admin/UserRoleManage?UserIntNo=" + userID);
}

function EditUser(userID) {
    if (userID == '' || userID == 0) {
        alert("Please select a User!");
        return;
    }
    $.post(_ROOTPATH + "/Admin/GetUserDetail", { userID: userID },
        function (message) {
            if (message.Status == true) {
                var officerArry = message.Text.split(';');
                $("#ddlUserAuthority").val(officerArry[0]);
                $("#ddlLanguageSlector").val(officerArry[8]);
                $("#txtAddUserSName").val(officerArry[1]);
                $("#txtAddUserInit").val(officerArry[2]);
                $("#txtAddUserFName").val(officerArry[3]);
                $("#txtAddUserLoginName").val(officerArry[4]);
                $("#txtAddUserEmail").val(officerArry[5]);
                if (officerArry[7] == '') {
                    officerArry[7] = '0';
                }
                $("#ddlTrafficOfficer").val(officerArry[7]);

                $("#hidUserIntNo").val(userID);
                $("#tbUserEditPanel").show();
            }
        }, 'json');
}

function ResetPassword(userId) {
    $.post(_ROOTPATH + "/Admin/ResetPassword", { userId: userId },
     function (message) {
         alert(message.Text);
    }, 'json');
}