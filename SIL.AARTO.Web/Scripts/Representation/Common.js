﻿/// <reference path="~/Scripts/jquery-1.7.2.min.js" />
/// <reference path="~/Scripts/Library/jquery-1.4.2.min.js" />
/// <reference path="~/Scripts/jquery.blockUI.js" />
$.valHooks.textarea = {
    get: function(elem) {
        return elem.value.replace(/\r?\n/g, "\r\n");
    }
};

$.prototype.Load2 = function(url, data, onResponded, text, opacity) {
    var container = this;
    if (IsNullOrEmpty(container)) return;
    if (typeof text != "string") text = TextLoading;
    BlockPost(url, data, function(rsp) {
        if (typeof rsp == "string" && (rsp.length <= 0 || rsp[0] != "{"))
            container.empty().html(rsp);
        if (typeof onResponded == "function")
            onResponded(rsp);
    }, text, opacity);
};

//$.blockUI.defaults.fadeIn = 0;

$(function() {
    $.ajaxSetup({
        timeout: 600000
    });
});

//#region base functions

function IsNullOrEmpty(obj) {
    return typeof obj == "undefined"
        || obj == null
        || (typeof obj == "string" && obj == "")
        || obj.length <= 0;
}

function ToString(value, defaultValue) {
    return typeof value == "string"
        ? value
        : (!IsNullOrEmpty(value)
            ? value.toString()
            : (typeof defaultValue != "string"
                ? ""
                : defaultValue));
}

function ToInt(value, defaultValue) {
    var num = parseInt(value, 10);
    return !isNaN(num)
        ? num
        : (typeof defaultValue != "number"
            ? 0
            : parseInt(defaultValue, 10));
}

function ToFloat(value, defaultValue) {
    var num = parseFloat(value);
    return !isNaN(num)
        ? num
        : (typeof defaultValue != "number"
            ? 0
            : parseFloat(defaultValue));
}

function ToBoolean(value, defaultValue) {
    var type = typeof value;
    return type == "boolean"
        ? value
        : (type == "number"
            ? value != 0
            : (type == "string"
                ? value.toLowerCase() == "true" || value == "1"
                : typeof defaultValue == "boolean" && defaultValue));
}

function GetTicks() {
    return ((new Date()).getTime() * 10000) + 621355968000000000;
}

var testMSCount = 0;

function TestMS(onTesting, enableTest, alertTime) {
    if (typeof enableTest != "boolean") enableTest = true;
    if (!enableTest) return 0;
    if (typeof alertTime != "boolean") alertTime = true;
    var dt1 = new Date();
    try {
        if (typeof onTesting == "function") onTesting();
    } catch (e) {
        alert(e);
    }
    var dt2 = new Date();
    var result = dt2.getTime() - dt1.getTime();
    if (alertTime) $("body").append('<div>' + (++testMSCount) + ": " + result + 'ms</div>');
    return result;
}

//#endregion base functions

//#region Global Vars

var LoadingGif = "";
var TextLoading = "Loading...";
var TextProcessing = "Processing...";
var TextProcessingReport = "Processing Report...";
var SubmitText = "";
var KeepBlock = false;

//#endregion

function HiddenValue(name, value) {
    if (typeof name != "string") return;
    var hidden = $("input[type=hidden][name=" + name + "]");
    if (typeof value == "undefined")
        return hidden.val();
    else
        hidden.val(value);
}

function LoadingText(text) {
    if (IsNullOrEmpty(LoadingGif)) LoadingGif = "";
    return '<div style="text-align:center; vertical-align:middle; margin: auto"><h3><img src="' + LoadingGif + '" />&nbsp;&nbsp;' + text + '</h3></div>';
}

function BlockLoading(text, next, opacity) {
    if (typeof opacity != "number") opacity = 0.1;
    if (opacity >= 0) {
        $.blockUI({
            overlayCSS: { opacity: opacity },
            message: LoadingText(ToString(text))
        });
    }
    if (typeof next == "function")
        next();
}

function BlockPost(url, data, callback, text, opacity) {
    if (typeof url != "string" || url.length <= 1) return;
    BlockLoading(text, function() {
        $.post(url, data, function(rsp) {
            if (typeof callback == "function")
                callback(rsp);
        });
    }, opacity);
}

function BindCheckbox(id, text, idLabel) {
    var obj = $('#' + id);
    var lblStr = '<a href="javascript:void(0)"><label'
        + ((typeof idLabel != "string") ? '' : ' id="' + idLabel + '"')
        + ' for="' + id + '">' + text + '</label></a>';
    obj.after(lblStr);
    return obj;
}

function BindRadio(id, text) {
    var obj = $('#' + id);
    var lblStr = '<label for="' + id + '">' + text + '</label>';
    obj.after(lblStr);
    return obj;
}

function BindRadioGroup(groupName, disabled, idValue) {
    var group = $("[name=" + groupName + "]:radio");
    if (IsNullOrEmpty(group)) return;
    if (typeof disabled != "boolean") disabled = false;
    var hid = $("#" + idValue);
    if (IsNullOrEmpty(hid)) return;
    group.attr("disabled", disabled);
    hid.change(function() {
        var value = $(this).val();
        if (IsNullOrEmpty(value)) return;
        group.attr("checked", function() {
            return $(this).val() == value;
        });
    }).change();
    if (!disabled) {
        group.change(function() {
            var rad = $(this);
            if (rad.is(":checked"))
                hid.val(rad.val());
        }).change();
    }
}

function BindCheckboxClass(idCheckbox, idLabel) {
    var chk = $("#" + idCheckbox);
    $("#" + idLabel).attr("class", chk.is(":checked") ? 'subContentEnabled' : 'subContentDisabled');
    return chk;
}

function BindSubContent(idCheckbox, text, idContent, fnOnChange) {
    var lblId = "lbl_" + idCheckbox;
    BindCheckbox(idCheckbox, text, lblId);
    var chk = BindCheckboxClass(idCheckbox, lblId);
    chk.change(function() {
        var isChecked = BindCheckboxClass(idCheckbox, lblId).is(':checked');
        if (typeof fnOnChange == 'function') {
            fnOnChange(isChecked, idContent);
        } else
            ControlVisible(idContent, isChecked, true);
    });
    return chk;
}

/// arrayOptions must be a Text-Value collection;
/// mode = 0: clear options; 1: clear options except first one; 2: don't clear options;
function SelectPushOptions(idSelect, arrayOptions, selectedValue, mode) {
    var select = $("#" + idSelect);
    if (IsNullOrEmpty(idSelect)
        || !$.isArray(arrayOptions))
        return select;
    selectedValue = ToString(selectedValue);
    mode = ToInt(mode);
    if (mode < 0 || mode > 2) mode = 0;
    var options = select.children("option");
    switch (mode) {
        case 0:
            options.remove();
            break;
        case 1:
            options.not(":first").remove();
            break;
    }
    var newOptions = [select.html()];
    var selected = 0;
    for (var i = 0; i < arrayOptions.length; i++) {
        var value = ToString(arrayOptions[i].Value);
        if (selected == 0 && value == selectedValue)
            selected = 1;

        newOptions.push('<option value="' +
            value + (selected == 1 ? '" selected="selected' : '') + '">' +
            arrayOptions[i].Text + '</option>');

        if (selected == 1)
            selected = 2;
    }
    select.html(newOptions.join("")).change();
    return select;
}

function EnableIt(id, isEnabled, checkIgnore) {
    var target = $("#" + id);
    if (IsNullOrEmpty(target))
        return target;

    isEnabled = ToBoolean(isEnabled);
    if (ToBoolean(checkIgnore) && ToBoolean(target.data("ignored")))
        return target;

    target.data("ignored", true);
    if (target.is(":input")) {
        if (target.is("select")) {
            target.attr("disabled", !isEnabled).css("cursor", isEnabled ? "" : "not-allowed");
            var idHidden = target.data("idHidden");
            if (IsNullOrEmpty(idHidden))
                idHidden = id + "_" + GetTicks();
            if (!isEnabled) {
                var name = target.attr("name");
                if (!IsNullOrEmpty(name)) {
                    target.data("originalName", name)
                        .data("idHidden", idHidden)
                        .removeAttr("name")
                        .after('<input id="' + idHidden + '" name="' + name + '" type="hidden" value="">');
                    $("#" + idHidden).val(target.val());
                }
            } else {
                $("#" + target.data("idHidden")).remove();
                var originalName = target.data("originalName");
                if (!IsNullOrEmpty(originalName))
                    target.attr("name", originalName);
            }
        } else if (target.is(":button")) {
            target.attr("disabled", !isEnabled).css("cursor", isEnabled ? "" : "not-allowed");
        } else {
            target.attr("readonly", !isEnabled).css("cursor", isEnabled ? "" : "not-allowed");
        }
    } else {
        if (!isEnabled)
            target.block({
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.5,
                    cursor: 'not-allowed'
                },
                message: null
            });
        else
            target.unblock();
    }
    return target;
}

function ControlVisible(idControl, isVisible, isAnimation) {
    var ctrl = $("#" + idControl);
    if (IsNullOrEmpty(ctrl)) return ctrl;
    isAnimation = ToBoolean(isAnimation);
    isVisible = ToBoolean(isVisible);
    if (isAnimation) {
        if (isVisible)
            ctrl.slideDown();
        else
            ctrl.slideUp();
    } else
        ctrl.css("display", isVisible ? "" : "none");
    return ctrl;
}

function SameWidth(parent, children) {
    var main = $("#" + parent);
    if (IsNullOrEmpty(main) || IsNullOrEmpty(children)) return;
    var width = main.outerWidth();
    if (typeof children == "string")
        $("#" + children).css("width", width);
    else if ($.isArray(children))
        for (var i = 0; i < children.length; i++) {
            var child = children[i];
            if (typeof child != "string") continue;
            $("#" + child).css("width", width + "px");
        }
}