﻿var loadingGif = "";
var loadingText = "";
var woaCourDates = [];
var msgInvalidWOANumber = "";
var msgInvalidNewWOACourtDate = "";

function loadingGraphic(text) {
    loadingGif = loadingGif || "";
    text = text || "";
    return '<div style="text-align:center; vertical-align:middle; margin: auto"><h3><img src="' + loadingGif + '" />&nbsp;&nbsp;' + text + '</h3></div>';
}

function loadingBlock(text, opacity) {
    if (typeof opacity != "number" || opacity < 0)
        opacity = 0.1;
    $.blockUI({
        overlayCSS: { opacity: opacity },
        message: loadingGraphic(text)
    });
}

function isHtml(html) {
    return typeof html == "string" && (html.length <= 0 || html[0] != "{");
}

$(document)
    .ajaxStart(function() {
        clearMessage();
        loadingBlock(loadingText);
    })
    .ajaxSuccess(function(e, r, s) {
        var data;
        try {
            if (typeof r.responseText != "string"
                || r.responseText.length <= 0
                || r.responseText[0] != "{"
                || !(data = $.parseJSON(r.responseText))
                || !data.IsMessageResult)
                return;

            if (data.Message)
                writeMessage(data.Message, false);
            if (data.Error)
                writeMessage(data.Error, true);

        } catch (ex) {
            //console.debug(ex);
        }
    })
    .ajaxError(function(e, r, s) {
        writeMessage(r.status + " - " + r.statusText + " (" + s.url + ")", true);
    })
    .ajaxStop(function() {
        $.unblockUI();
    });

$(function() {
    $.ajaxSetup({
        timeout: 600000
    });
    $('#btnSearch').click(woaSearch);
    initTd();
});

function initTd() {
    $('table').not('[id=DWS_Table2]').find('td')
        .filter(":even").attr("class", "NormalBold").css("width", "160px").css("height", "16px")
        .end().filter(":odd").css("width", "200px").css("height", "16px")
        .children("input:text").attr("class", "Normal").css("width", "180px")
        .end().children("select:first-child").css("width", "184px");
    $("td, input").css("font-family", "Verdana");
}

function clearMessage() {
    $('#divMessage > div').css("display", "none").empty();
}

function writeMessage(message, isError) {
    message = message || '';
    isError = isError || false;
    if (!message) return;
    $(!isError ? '#lblMessage' : '#lblError').css("display", "").html(message);
}

function woaSearch() {
    $('#divWOAContent').empty();
    var woaNumber = $('#txtSearch').val() || "";
    if (!woaNumber) {
        clearMessage();
        writeMessage(msgInvalidWOANumber, true);
        return;
    }

    loadingText = "Searching...";
    searchWOAs(woaNumber);
}

function getWOA(woaIntNo) {
    var container = $('#divWOAContent');
    container.empty();
    var url = _ROOTPATH + "/WOAManagement/WOASelfPlacementSearch";
    loadingText = "Searching...";
    $.post(url, {
        woaIntNo: woaIntNo
    }, function (rsp) {
        if (isHtml(rsp))
            container.html(rsp);
    });
}

function woaUpdate() {
    var dateStr = $("#WOANewCourtDate").val() || "";

    clearMessage();
    if (!isValidCourtDate(new Date(dateStr))) {
        writeMessage(msgInvalidNewWOACourtDate, true);
        return;
    }
    var url = _ROOTPATH + "/WOAManagement/WOASelfPlacementUpdate";
    var data = $('#btnUpdate').closest('form').serialize();
    loadingText = "Updating...";
    $.post(url, data, function(rsp) {
        if (rsp.Status)
            $('#divWOAContent').empty();
    });
}

function woaReverse() {
    clearMessage();
    var url = _ROOTPATH + "/WOAManagement/WOASelfPlacementReverse";
    var data = $('#btnReverse').closest('form').serialize();
    loadingText = "Reversing...";
    $.post(url, data, function (rsp) {
        if (rsp.Status)
            $('#divWOAContent').empty();
    });
}

function isValidCourtDate(date) {
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var dateStr = date.getFullYear() + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? '0' : '') + day;
    return woaCourDates.indexOf(dateStr) >= 0;
}

function beforeShowDay(date) {
    return [isValidCourtDate(date), ''];
}