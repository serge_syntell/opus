﻿/// <reference path="../jquery-1.4.4-vsdoc.js" />
$(function () {

    var currentDate = new Date();

    $("#txtSendToCourtDate").datepicker({ dateFormat: 'yy-mm-dd', dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true, maxDate: new Date() });

    $("#SearchCrtIntNo").change(function () {

        $.post(_ROOTPATH + "/WOAManagement/GetCourtRoomByCourt", { crtIntNo: $(this).val() },
            function (items) {
                $("#SearchCrtRIntNo option[value!=0]").remove();
                $.each(items, function (index, item) {
                    $("<option value='" + item.CrtRIntNo + "'>" + item.CrtRoomName + "</option>").appendTo($("#SearchCrtRIntNo"));
                });
            }, "json");
    });



    $("#SearchCrtRIntNo").change(function () {

        $.post(_ROOTPATH + "/WOAManagement/GetCourtDateByCrtrIntNo", { crtRIntNo: $(this).val() },
            function (items) {
                $("#SearchCourtDate option[value!=0]").remove();
                $.each(items, function (index, item) {
                    $("<option value='" + item.CDIntNo + "'>" + item.CDate + "</option>").appendTo($("#SearchCourtDate"));
                });
            }, "json");
    });

    $("#btnAll").click(function () {
        $("input[name='cbx_woaNumber']").each(function (index, item) {

            $(item).attr('checked', true);
        });
    });
    $("#btnUndo").click(function () {
        $("input[name='cbx_woaNumber']").each(function (index, item) {

            $(item).attr('checked', false);
        });
    });

    $("#btnSubmit").click(function () {

        if ($("#SearchCourtDate").val() == "0") {
            alert("Court date is required");
            return false;
        }
    });
})

function openDiv(pfnIntNo, fileName) {
    $("#tbWOA tr:gt(0)").remove();
    $("#btnAll").focus();
    $("#divSendToCourt").attr("title", fileName);
    $.post(_ROOTPATH + "/WOAManagement/GetWOaByPfnIntNo", { pfnIntNo: pfnIntNo },
        function (items) {
            if (items != undefined && items.length > 0) {
                $.each(items, function (index, item) {
                    $("<tr class='CartListItem'><td>" + item.WoaNumber + "</td><td>" + item.SummonsNo + "</td><td>"+item.AccFullName+"</td><td><input type='checkbox' name='cbx_woaNumber' id='" + item.WoaIntNo + "'/></td></tr>").appendTo($("#tbWOA"));
                });

                $("#divSendToCourt").dialog({
                    position: "center", resizable: false, width: 430, height: 400, modal: true,
                    buttons: [{
                        text: "Submit",
                        id: "btnSubmit_J",
                        click: function () {
                            $("#btnSubmit_J").attr("disabled", true);
                            var checkedLength = $("input[name='cbx_woaNumber']:checked").length;
                            if (checkedLength == 0) {
                                alert("None WOA item selected,Please select at least one WOA");
                                $("#btnSubmit_J").attr("disabled", false);
                                return;
                            }
                            if ($("#txtSendToCourtDate").val() == '') {
                                alert("The date of Send to court is required");
                                $("#btnSubmit_J").attr("disabled", false);
                                return;
                            }
                            var crtIntNo = $("#SearchCrtIntNo").val();
                            var crtRIntNo = $("#SearchCrtRIntNo").val();
                            var courtDate = $("#SearchCourtDate option[selected]").text();
                            var cdIntNo = $("#SearchCourtDate").val();
                            var url = _ROOTPATH + "/WOAManagement/SendToCourt?Page=" + $("#Page").val();


                            var checkBoxLength = $("input[name='cbx_woaNumber']").length;
                            if (checkBoxLength == checkedLength) {

                                $.post(_ROOTPATH + "/WOAManagement/SubmitSendToCourtByPFNIntNo", { pfnIntNo: pfnIntNo, date: $("#txtSendToCourtDate").val() },
                                    function (msg) {
                                        if (msg.Status == false && msg != null && msg != undefined && msg.Text != '' && msg.Text != null && msg.Text != undefined) {
                                            $("#btnSubmit_J").attr("disabled", false);
                                            alert(msg.Text);
                                            return;
                                        }
                                        if (crtIntNo != "") {
                                            url += "&CrtIntNo=" + crtIntNo;
                                        }
                                        if (crtRIntNo != "") {
                                            url += "&CrtRIntNo=" + crtRIntNo;
                                        }
                                        if (courtDate != "") {
                                            url += "&CDate=" + courtDate;
                                        }
                                        if (cdIntNo != "") {
                                            url += "&CDIntNo=" + cdIntNo;
                                        }
                                        document.location = url;
                                    }, 'json');

                            } else {
                                var para = new Object();
                                var arra = new Array();
                                para.CrtIntNo = $("#SearchCrtIntNo").val();
                                para.CourtDate = $("#txtSendToCourtDate").val();
                                var i = 0;
                                $("input[name='cbx_woaNumber']:checked").each(function (index, item) {
                                    arra[i++] = item.id;
                                });
                                para.WoaIntNoList = arra;
                                $.ajax({
                                    type: 'POST',
                                    url: _ROOTPATH + "/WOAManagement/SubmitSendToCourt",
                                    dataType: "json",
                                    traditional: false,
                                    contentType: 'application/json',
                                    data: JSON.stringify(para),
                                    success: function (msg) {
                                        if (msg != undefined && msg.Text != '' && msg.Text != null) {
                                            alert(msg.Text);
                                        }

                                        if (crtIntNo != "") {
                                            url += "&CrtIntNo=" + crtIntNo;
                                        }
                                        if (crtRIntNo != "") {
                                            url += "&CrtRIntNo=" + crtRIntNo;
                                        }
                                        if (courtDate != "") {
                                            url += "&CDate=" + courtDate;
                                        }
                                        if (cdIntNo != "") {
                                            url += "&CDIntNo=" + cdIntNo;
                                        }
                                        document.location = url;
                                        //$("#form1").submit();
                                    },
                                    error: function (mgs) {

                                        if (msg != undefined && msg.Text != '' && msg.Text != null) {
                                            alert(msg.Text);
                                        }
                                        $("#btnSubmit_J").attr("disabled", false);
                                    }
                                });
                            }
                        },
                    },
                    {
                        text: "Cancel",
                        click: function () {
                            $("#divSendToCourt").dialog('close');
                        }
                    }]
                });

            } else {
                alert("No available data found, please refersh your page and try it again");
                return;
            }
        }, 'json');

}