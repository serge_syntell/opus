﻿/// <reference path="../jquery-1.4.4-vsdoc.js" />
$(document).ready(function () {

    var currentDate = new Date();

    $("input[name='txt_SentToCourt']").datepicker({ dateFormat: 'yy-mm-dd', dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true, maxDate: new Date() });
    $("input[name='txt_ReturnFromCourt']").datepicker({ dateFormat: 'yy-mm-dd', dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true, maxDate: new Date() });
    $("input[name='txt_ReturnFromCourtRej']").datepicker({ dateFormat: 'yy-mm-dd', dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true, maxDate: new Date() });


    $("input[name='cbx_SentToCourt']").change(function () {
        var checked = $(this).attr('checked');

        var objRFCParent = $(this).parent().siblings().eq(4);
        var objRFCRejParent = $(this).parent().siblings().eq(5);

        var cbxRTC = objRFCParent.children("[name='cbx_ReturnFromCourt']");
        var cbxRTCRej = objRFCRejParent.children("[name='cbx_ReturnFromCourtRej']");

        var rfcTextbox = objRFCParent.children("[name='txt_ReturnFromCourt']");
        var txtSendToCourt = $(this).siblings("[name='txt_SentToCourt']");
        var rejDateTextbox = objRFCRejParent.children("[name='txt_ReturnFromCourtRej']");
        var rejTextbox = objRFCRejParent.children("[name='txt_ReturnFromCourtRejReason']");

        var status = objRFCRejParent.children("[name='hid_NoticeStatus']").val();

        if (checked) {

            var formatDate = currentDate.format('yyyy-MM-dd');

            if (txtSendToCourt.attr("disabled")) {
                txtSendToCourt.attr("disabled", false);
            }
            txtSendToCourt.val(formatDate);

            if (status == 832) {
                disabledControl(cbxRTC, false);
                disabledControl(cbxRTCRej, false);

                disabledControl(rfcTextbox, true);
                disabledControl(rejDateTextbox, true);
                disabledControl(rejTextbox, true);
            }
            else if (status == 840) { }
            else if (status == 875) {
            }

        }
        else {

            if (status == 832) {

                disabledControl(cbxRTC, true);
                disabledControl(cbxRTCRej, true);

                cleanTextBox(rejDateTextbox);
                cleanTextBox(rfcTextbox);
                cleanTextBox(rejTextbox);

            }
            else if (status == 840) { }
            else if (status == 875) {
            }

            $(this).siblings("[name='txt_SentToCourt']").val('');
            if (txtSendToCourt.attr("disabled") == false) {
                txtSendToCourt.attr("disabled", true);
            }

        }

    });

    $("input[name='cbx_ReturnFromCourt']").change(function () {

        var checked = $(this).attr('checked');
        var objSTCParent = $(this).parent().siblings().eq(4);
        var objRFCRejParent = $(this).parent().siblings().eq(5);
        var rejCheckbox = objRFCRejParent.children("[name='cbx_ReturnFromCourtRej']");
        var rejDateTextbox = objRFCRejParent.children("[name='txt_ReturnFromCourtRej']");
        var rejTextbox = objRFCRejParent.children("[name='txt_ReturnFromCourtRejReason']");
        // jake 2013-10-11 modfied
        //var stcCheckBox = objSTCParent.children("[name='cbx_SentToCourt']");
        //var stcTextBox = objSTCParent.children("[name='txt_SentToCourt']");
        var status = objRFCRejParent.children("[name='hid_NoticeStatus']").val();

        if (checked) {
            $(this).siblings("[name='txt_ReturnFromCourt']").attr('disabled', false);
            $(this).siblings("[name='txt_ReturnFromCourt']").val('');

            var formatDate = currentDate.format('yyyy-MM-dd');
            $(this).siblings("[name='txt_ReturnFromCourt']").val(formatDate);

            // jake 2013-10-11 modfied
            //if (stcCheckBox.attr('disabled') == false) {
            //    stcCheckBox.attr('disabled', 'disabled');
            //    stcTextBox.attr('disabled', 'disabled');
            //}

            if (rejCheckbox.attr('disabled') == false) {
                rejCheckbox.attr('disabled', 'disabled');
                rejCheckbox.attr('checked', false);

                //rejDate.attr('disabled', 'disabled');
                //rejDate.val('');

                //rejReason.attr('disabled', 'disabled');
                //rejReason.val('');
            }
        }
        else {

            if (status == 832) {
                // jake 2013-10-11 modfied
                //stcCheckBox.attr('disabled', false);
                //stcTextBox.attr('disabled', false);

                rejCheckbox.attr('disabled', false);
                rejDateTextbox.attr('disabled', true);
                rejTextbox.attr('disabled', true);
            }
            else if (status == 840) {

                //stcCheckBox.attr('disabled', true);
                //stcTextBox.attr('disabled', true);

                rejCheckbox.attr('disabled', true);
                rejDateTextbox.attr('disabled', true);
                rejTextbox.attr('disabled', true);
            }

            $(this).siblings("[name='txt_ReturnFromCourt']").attr('disabled', true);
            $(this).siblings("[name='txt_ReturnFromCourt']").val('');

        }
    });

    $("input[name='cbx_ReturnFromCourtRej']").change(function () {
        var objSTCParent = $(this).parent().siblings().eq(4);
        var objRFCParent = $(this).parent().siblings().eq(5);
        var rfcCheckbox = objRFCParent.children("[name='cbx_ReturnFromCourt']");
        var rfcTextBox = objRFCParent.children("[name='txt_ReturnFromCourt']");

        var rejTextBox = $(this).siblings("[name='txt_ReturnFromCourtRejReason']");
        var rejDateTextBox = $(this).siblings("[name='txt_ReturnFromCourtRej']");

        // jake 2013-10-11 modfied
        //var stcCheckbox = objSTCParent.children("[name='cbx_SentToCourt']");
        //var stcTextBox = objSTCParent.children("[name='txt_SentToCourt']");

        var status = $(this).siblings("[name='hid_NoticeStatus']").val();

        var checked = $(this).attr('checked');
        if (checked) {

            var formatDate = currentDate.format('yyyy-MM-dd');
            rejDateTextBox.val(formatDate);
            if (rejTextBox.val() == "")
                rejTextBox.val(REJ_REASON);

            if (rfcCheckbox.attr('disabled') == false) {
                rfcCheckbox.attr('disabled', true);
                rfcTextBox.attr('disabled', true);
                rfcTextBox.val('');
            }
            // jake 2013-10-11 modfied
            //if (stcCheckbox.attr('disabled') == false) {
            //    stcCheckbox.attr('disabled', true);

            //}

            rejTextBox.attr('disabled', false);
            rejDateTextBox.attr('disabled', false);

            if (status == 832) {
                // jake 2013-10-11 modfied
                //disabledControl(stcCheckbox, true);
                //disabledControl(stcTextBox, true);
                disabledControl(rfcCheckbox, true);
                disabledControl(rfcTextBox, true);
            }
            else if (status == 840) { }
            else if (status == 875) { }

        }
        else {
            if (status == 832) {
                // jake 2013-10-11 modfied
                //disabledControl(stcCheckbox, false);
                //disabledControl(stcTextBox, false);
                disabledControl(rfcCheckbox, false);

                disabledControl(rfcTextBox, true);

            }
            else if (status == 875) {
                if (rfcCheckbox.attr('disabled') == false) {
                    rfcCheckbox.attr('disabled', true);
                }
            }
            else {
                if (rfcCheckbox.attr('disabled')) {
                    rfcCheckbox.attr('disabled', false);
                }
            }


            rejTextBox.attr('disabled', true);

            rejDateTextBox.attr('disabled', true);
            rejTextBox.val('');
            rejDateTextBox.val('');
        }


    });



    //Jake 2013-08-28 add js to process new layout for this page

    $("#SearchCrtIntNo").change(function () {

        $.post(_ROOTPATH + "/WOAManagement/GetCourtRoomByCourt", { crtIntNo: $(this).val() },
            function (items) {
                $("#SearchCrtRIntNo option[value!=0]").remove();
                $.each(items, function (index, item) {
                    $("<option value='" + item.CrtRIntNo + "'>" + item.CrtRoomName + "</option>").appendTo($("#SearchCrtRIntNo"));
                });
            }, "json");
    });



    $("#SearchCrtRIntNo").change(function () {

        $.post(_ROOTPATH + "/WOAManagement/GetCourtDateByCrtrIntNo", { crtRIntNo: $(this).val() },
            function (items) {
                $("#SearchCDIntNo option[value!=0]").remove();
                $.each(items, function (index, item) {
                    $("<option value='" + item.CDIntNo + "'>" + item.CDate + "</option>").appendTo($("#SearchCDIntNo"));
                });
            }, "json");
    });


    $("#btnSelect").click(function () {
        return ValidateSearchCondition();
    });

    $("#btnWoaAtCourt").click(function () {
        var validated = ValidateSearchCondition();
        if ($("#SearchCDIntNo").val() == "0") {
            alert("Court date is required");
            return;
        }
        if (validated)
            window.open(_ROOTPATH + "/WOAManagement/WoaAtCourt?crtIntNo=" + $("#SearchCrtIntNo").val() + "&cdIntNo=" +$("#SearchCDIntNo").val());

    });

    $('#progressBar').dialog({
        autoOpen: false,
        //bgiframe: true,
        position: "center",
        resizable: false,
        width: 320,
        modal: true
    });


    $("#btnSubmit").click(function () {

        var index = 0;
        var request = "";
        $("input[name='cbx_SentToCourt']").each(function (index, item) {
            //alert($(item).val());
            var crtIntNo = $("#SearchCrtIntNo").val();
            var crtrIntNo = $("#SearchCrtRIntNo").val();
            var cdIntNo = $("#SearchCDIntNo").val();
            var obj = new Object();
            obj.WoaIntNo = $(item).siblings("[name='cbx_SentToCourt']").context.value;
            obj.Status = $(item).parent().siblings().eq(5).children("[name='hid_NoticeStatus']").val();
            obj.SentToCourt = $(item).siblings("[name='txt_SentToCourt']").val();
            obj.ReturnFromCourt = $(item).parent().siblings().eq(4).children("[name='txt_ReturnFromCourt']").val();
            obj.RejectDate = $(item).parent().siblings().eq(5).children("[name='txt_ReturnFromCourtRej']").val();
            obj.RejectReason = $(item).parent().siblings().eq(5).children("[name='txt_ReturnFromCourtRejReason']").val();

            if (index == 0) {
                request += "para[" + index + "].WoaIntNo=" + obj.WoaIntNo +
                    "&para[" + index + "].Status=" + obj.Status +
                    "&para[" + index + "].SentToCourt=" + obj.SentToCourt +
                    "&para[" + index + "].ReturnFromCourt=" + obj.ReturnFromCourt +
                    "&para[" + index + "].RejectDate=" + //obj.RejectDate +
                    "&para[" + index + "].RejectReason=" + //obj.RejectReason +
                    "&para[" + index + "].CrtIntNo=" + crtIntNo +
                    "&para[" + index + "].CrtRIntNo=" + crtrIntNo +
                    "&para[" + index + "].CDIntNo=" + cdIntNo;
            }
            else {
                request += "&para[" + index + "].WoaIntNo=" + obj.WoaIntNo +
                   "&para[" + index + "].Status=" + obj.Status +
                   "&para[" + index + "].SentToCourt=" + obj.SentToCourt +
                   "&para[" + index + "].ReturnFromCourt=" + obj.ReturnFromCourt +
                   "&para[" + index + "].RejectDate=" + //obj.RejectDate +
                   "&para[" + index + "].RejectReason=" + //obj.RejectReason +
                   "&para[" + index + "].CrtIntNo=" + crtIntNo +
                   "&para[" + index + "].CrtRIntNo=" + crtrIntNo +
                   "&para[" + index + "].CDIntNo=" + cdIntNo;
            }
            index++;

        });


        $.blockUI({ message: $('#progressBar') });

        $.post(_ROOTPATH + "/WOAManagement/SubmitAuthorisation", request,
           function (msgs) {
               $.unblockUI();
               if (msgs != null && msgs != undefined) {
                   var message = "";
                   $.each(msgs, function (index, item) {
                       if (item.Status == false) {
                           message += item.Text + "\r\n";
                       }
                   });

                   // $("#progressBar").dialog('close');

                   if (message != "") {
                       alert(message);
                   }

                   var crtIntNo = $("#SearchCrtIntNo").val();
                   var crtrIntNo = $("#SearchCrtRIntNo").val();
                   var cdIntNo = $("#SearchCDIntNo").val();
                   var woaNumber = $("#WoaNumber").val();
                   //$("#form").attr('action', document.location);
                   //$("#form").submit();
                   var url = _ROOTPATH + "/WOAManagement/WoaAuthorisation?Page=" + $("#PageIndex").val() + "&WoaNumber=" + woaNumber + "&SearchCrtIntNo=" + crtIntNo + "&SearchCrtRIntNo=" + crtrIntNo + "&SearchCDIntNo=" + cdIntNo;
                   document.location = url;
               }
           }, "json");

    });


    Date.prototype.format = function (format) {
        var o = {
            "M+": this.getMonth() + 1, //month
            "d+": this.getDate(), //day
            "h+": this.getHours(), //hour
            "m+": this.getMinutes(), //minute
            "s+": this.getSeconds(), //second
            "q+": Math.floor((this.getMonth() + 3) / 3), //quarter
            "S": this.getMilliseconds() //millisecond
        }
        if (/(y+)/.test(format)) format = format.replace(RegExp.$1,
        (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o) if (new RegExp("(" + k + ")").test(format))
            format = format.replace(RegExp.$1,
            RegExp.$1.length == 1 ? o[k] :
            ("00" + o[k]).substr(("" + o[k]).length));
        return format;
    }

})

function disabledControl(obj, flag) {
    $(obj).attr('disabled', flag);
}

function cleanTextBox(obj) {
    $(obj).val('');
}


function ValidateSearchCondition() {
    if ($("#SearchCrtIntNo").val() == "0") { alert("Court is required"); $("#SearchCrtIntNo").focus(); return false; }
    if ($("#SearchCrtRIntNo").val() == "0") { alert("Court room is required"); $("#SearchCrtRIntNo").focus(); return false; }
    if ($("#WoaNumber").val() == '') {
        if ($("#SearchCDIntNo").val() == "0") { alert("Court date is required"); $("#SearchCDIntNo").focus(); return false; }
    }
    return true;
}