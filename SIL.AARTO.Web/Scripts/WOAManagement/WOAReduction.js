﻿var loadingGif = "";
var loadingText = "";
var msgInvalidWOANumber = "";
var msgGreaterThanZero = "";
var msgLessThan0 = "";
var msgGreaterThan10000 = "";
var msgSelectOfficial = "";
var msgFineAmountHasIncreased = "";
var msgContemptAmountHasIncreased = "";
var msgContinue = "";
var msgSelectAuthority = "";
var msgInvalidDateRange = "";

function loadingGraphic(text) {
    loadingGif = loadingGif || "";
    text = text || "";
    return '<div style="text-align:center; vertical-align:middle; margin: auto"><h3><img src="' + loadingGif + '" />&nbsp;&nbsp;' + text + '</h3></div>';
}

function loadingBlock(text, opacity) {
    if (typeof opacity != "number" || opacity < 0)
        opacity = 0.1;
    $.blockUI({
        overlayCSS: { opacity: opacity },
        message: loadingGraphic(text)
    });
}

function toFloat(value, defaultValue) {
    var num = parseFloat(value);
    return !isNaN(num)
        ? num
        : (typeof defaultValue != "number"
            ? 0
            : parseFloat(defaultValue));
}

function isHtml(html) {
    return typeof html == "string" && (html.length <= 0 || html[0] != "{");
}

$(document)
    .ajaxStart(function() {
        clearMessage();
        loadingBlock(loadingText);
    })
    .ajaxSuccess(function(e, r, s) {
        var data;
        try {
            if (typeof r.responseText != "string"
                || r.responseText.length <= 0
                || r.responseText[0] != "{"
                || !(data = $.parseJSON(r.responseText))
                || !data.IsMessageResult)
                return;

            if (data.Message)
                writeMessage(data.Message, false);
            if (data.Error)
                writeMessage(data.Error, true);

        } catch (ex) {
            //console.debug(ex);
        }
    })
    .ajaxError(function(e, r, s) {
        writeMessage(r.status + " - " + r.statusText + " (" + s.url + ")", true);
    })
    .ajaxStop(function() {
        $.unblockUI();
    });

$(function() {
    $.ajaxSetup({
        timeout: 600000
    });
    $("#tabs").tabs();
    $("#ui-id-2").click(loadTabReport);
    $('#btnSearch').click(woaSearch);
    initTd();
});

function initTd() {
    $('table').not('[id=DWS_Table2]').find('td')
        .filter(":even").attr("class", "NormalBold").css("width", "160px").css("height", "16px")
        .end().filter(":odd").css("width", "200px").css("height", "16px")
        .children("input:text").attr("class", "Normal").css("width", "180px")
        .end().children("select:first-child").css("width", "184px");
    $("td, input").css("font-family", "Verdana");
}

function clearMessage() {
    $('#divMessage > div').css("display", "none").empty();
}

function writeMessage(message, isError) {
    message = message || '';
    isError = isError || false;
    if (!message) return;
    $(!isError ? '#lblMessage' : '#lblError').css("display", "").html(message);
}

function woaSearch() {
    $('#divWOAContent').empty();
    var woaNumber = $('#txtSearch').val() || "";
    if (!woaNumber) {
        clearMessage();
        writeMessage(msgInvalidWOANumber, true);
        return;
    }

    loadingText = "Searching...";
    searchWOAs(woaNumber);
}

function getWOA(woaIntNo) {
    var container = $('#divWOAContent');
    container.empty();
    var url = _ROOTPATH + "/WOAManagement/WOAReductionSearch";
    loadingText = "Searching...";
    $.post(url, {
        woaIntNo: woaIntNo
    }, function (rsp) {
        if (isHtml(rsp))
            container.html(rsp);
    });
}

function woaUpdate() {
    var fineAmountOld = toFloat($('#TotalFineAmountOld').val());
    var contemptAmountOld = toFloat($('#TotalContemptAmountOld').val());
    var fineAmountNew = toFloat($('#TotalFineAmountNew').val());
    var contemptAmountNew = toFloat($('#TotalContemptAmountNew').val());

    clearMessage();
    if (fineAmountNew + contemptAmountNew <= 0) {
        writeMessage(msgGreaterThanZero, true);
        return;
    }
    if (fineAmountNew < 0 || contemptAmountNew < 0) {
        writeMessage(msgLessThan0, true);
        return;
    }
    if (!$('#CoMaIntNo').val()) {
        writeMessage(msgSelectOfficial, true);
        return;
    }
    var increased = [];
    if (fineAmountNew > fineAmountOld)
        increased.push(msgFineAmountHasIncreased);
    if (contemptAmountNew > contemptAmountOld)
        increased.push(msgContemptAmountHasIncreased);
    if (increased.length > 0) {
        var msg = increased.join('\n');
        msg += '\n' + msgContinue;
        if (!confirm(msg))
            return;
    }
    if (fineAmountOld != fineAmountNew && fineAmountNew > 10000
        || contemptAmountOld != contemptAmountNew && contemptAmountNew > 10000) {
        if (!confirm(msgGreaterThan10000 + '\n' + msgContinue))
            return;
    }
    var url = _ROOTPATH + "/WOAManagement/WOAReductionUpdate";
    var data = $('#btnUpdate').closest('form').serialize();
    loadingText = "Updating...";
    $.post(url, data, function(rsp) {
        if (rsp.Status)
            $('#divWOAContent').empty();
    });
}

function loadTabReport() {
    var container = $("#tabReport");
    if (container.html())
        return;
    var url = _ROOTPATH + "/WOAManagement/WOAReductionReport";
    loadingText = "";
    $.post(url, null, function(rsp) {
        if (isHtml(rsp))
            container.html(rsp);
    });
}

function authorityChanged() {
    var autIntNo = $(this).val() || 0;
    loadCourtList(autIntNo);
}

function loadCourtList(autIntNo) {
    var select = $("#CrtIntNo");
    select.children("option").not(":first").remove();
    if (!autIntNo) return;
    var url = _ROOTPATH + "/WOAManagement/GetCourtList";
    loadingText = "";
    $.post(url, {
        autIntNo: autIntNo
    }, function(rsp) {
        if (rsp && typeof rsp == "object") {
            var options = [select.html()];
            for (var i = 0; i < rsp.length; i++)
                options.push('<option value="' + rsp[i].Value + '">' + rsp[i].Text + '</option>');
            select.html(options.join(""));
        }
    });
}

function createReport() {
    var autIntNo = $("#AutIntNo").val() || 0;
    var dateFrom = $("#DateFrom").val();
    var dateTo = $("#DateTo").val();

    clearMessage();
    if (!autIntNo) {
        writeMessage(msgSelectAuthority, true);
        return;
    }
    if (!dateFrom
        || !dateTo
        || dateTo < dateFrom) {
        writeMessage(msgInvalidDateRange, true);
        return;
    }
    var url = _ROOTPATH + "/WOAManagement/WOAReductionCreateReport";
    var data = $('#btnReport').closest('form').serialize();
    loadingText = "Generating...";
    $.post(url, data, function(rsp) {
        if (rsp.Status) {
            window.open(url + "/?" + data + "&DKey=" + rsp.Body);
        }
    });
}

function onDatePickerSelect(date, inst) {
    switch (inst.id) {
        case "DateFrom":
            $("#DateTo").datepicker("option", "minDate", date);
            break;
        case "DateTo":
            $("#DateFrom").datepicker("option", "maxDate", date);
            break;
    }
}

function integerOnly(e) {
    var code = e.which;
    return code >= 48 && code <= 57 || code == 8;
}