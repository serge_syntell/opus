﻿
$(document).ready(function () {
    $("#btnSearch").click(function () {
        var regNo = $("#SearchRegistrationNumber").val();
        window.location.href = _ROOTPATH + "/TraceAndTrack/TraceAndTrackInfo?regNo=" + regNo;
    });
    $("#btnAddSt").click(function () {
        ClearAllContorls();
        $("#actionMode").val("add");
        $("#mainEdit").show();
    });
    $("#btnSearch").click(function () {
        $("#mainEdit").hide();
    });

        $("#TTStAdd1").blur(function () {
            copyStreetToPo("#TTStAdd1", "#TTPoAdd1");
        });
        $("#TTStAdd2").blur(function () {
            copyStreetToPo("#TTStAdd2", "#TTPoAdd2");
        });
        $("#TTStAdd3").blur(function () {
            copyStreetToPo("#TTStAdd3", "#TTPoAdd3");
        });
        $("#TTStAdd4").blur(function () {
            copyStreetToPo("#TTStAdd4", "#TTPoAdd4");
        });
        $("#TTStCode").blur(function () {
            copyStreetToPo("#TTStCode", "#TTPoCode");
        });
        var copyStreetToPo = function (src, des) {
            if ($("#cbCopyAddress").attr("checked")) {
                $(des).val($(src).val());
            }
        };
    //first clear values
    ClearAllContorls();
})

//
function IsExitsTheData() {
    var mode = $("#actionMode").val();
    var RegistrationNo = $("#RegistrationNumber").val();
    if ($("form").valid()) {
        $.post(_ROOTPATH + "/TraceAndTrack/IsExitsTheData",
        {
            regNo: RegistrationNo
        },
        function (message) {
            if (message.Status && mode == "add") {
                $("#errorMsg").text(message.Text);
                $("#errorMsg").show();
            } else {
                $("#errorMsg").hide();
                $("#btnSaveSt").click();
            }
        },
        "json");
    }
}
//edit
function EditInfo(regNo) {
    if (regNo != "") {
        ClearAllContorls();
        $("#actionMode").val("update");
        $.post(_ROOTPATH + "/TraceAndTrack/GetTheTraceEdit",
        {
            regNo: regNo
        },
        function (message) {
            if (message.length > 0) {
                SetControlsValue(message);
                $("#mainEdit").show();
            }
        }, "json");
    }
}
//set the controls value
function SetControlsValue(dataObj) {
    var jsonObj = eval("("+dataObj+")");
    $.each(jsonObj, function (index, item) {
        var name = item.name;
        var getValue = item.value;
        if ($("#" + name)) {
            $("#" + name).val(getValue);
        }
    });
}
//delete the data
function DeleInfo(obj, regNo) {
    if (window.confirm("Are you sure?")) {
        $.post(_ROOTPATH+"/TraceAndTrack/DeleInfo",
        {
            regNo: regNo
        },
        function (message) {
            if (message.Status) {
                $(obj.parentElement.parentElement).remove();
            } else {
                alert("The operation failed!");
            }
        },
        "json");
    }
}
//clear all control value
function ClearAllContorls() {
    var allInputs = $(":input");
    $.each(allInputs, function (index, item) {
        if (IsNeedClear(item))
        item.value = "";
    });
}
//check clear items
function IsNeedClear(obj) {
    var flag = true;
    if (obj.type == "button")
        flag = false;
    else if (obj.type == "submit")
        flag = false;
    else if (obj.id == "SearchRegistrationNumber")
        flag = false;
    return flag;
}