﻿/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />


function checkDocumentNo() {

    var prefix = $("#prefixTxt");
    var docNo = $("#docNoTxt");
    var authNo = $("#authNoTxt");
    var zeroLength = $("#Rule_9300").val();
    $("#spMessage").html("");
    //check null
    if (CheckEmpty(prefix, docNo, authNo)) {
        $.post(_ROOTPATH + "/BookManagement/GetTheCDV",
                {
                    Prefix: prefix.val(),
                    DocumentNo: docNo.val(),
                    AuthrityNo: authNo.val()
                },
                function (message) {
                    if (message.Status == true) {
                        $("#searchDiv").hide();
                        $("#messageDiv").show();
                        $("#checkNumTxt").val(message.Text);
                        $("#fullDocumentNo").html(prefix.val() + "/" + setZeroString(docNo.val(), zeroLength) + "/" + authNo.val() + "/" + message.Text);
                    } else {
                        $("#spMessage").html(message.Text);
                    }
                },
                'json');
        prefix.focus();
    }
    $("#fullDocumentNo").html(prefix.val() + "/" + setZeroString(docNo.val(), zeroLength) + "/" + authNo.val() + "/");
}

$(document).ready(function () {
    $("#prefixTxt").focus();


    //search button
    $("#searchButton").click(function () {
        checkDocumentNo();
        $("#hidOperation").val("H"); // H = handle in document
    });

    $("#btnReverse").click(function () {
        checkDocumentNo();
        $("#hidOperation").val("R"); // H = handle in document

    });

    //yes button
    $("#yesButton").click(
        function () {
            var prefix = $("#prefixTxt");
            var docNo = $("#docNoTxt");
            var authNo = $("#authNoTxt");
            $("#txtDocumentNo").val(prefix.val() + "/" + docNo.val() + "/" + authNo.val());
            $("#messageDiv").hide();
            $("#btnHandIn").click();
            $("#searchDiv").show();
        }
    );
    //no button
    $("#noButton").click(
        function () {
            $("#messageDiv").hide();
            $("#searchDiv").show();
        }
    );

    $("#btnHandIn").click(function () {
        $("#spMessage").html('');
        var prefix = $("#prefixTxt");
        var docNo = $("#docNoTxt");
        var authNo = $("#authNoTxt");
        var zeroLength = $("#Rule_9300").val();
        var operation = $("#hidOperation").val();
        if ($("#txtDocumentNo").val() == '') {
            alert(documentIsRequired);
            //$("#txtDocumentNo").focus();
            return;
        }

        var url = _ROOTPATH + "/BookManagement/HandInDocument";
        if (operation == "R") {
            url = _ROOTPATH + "/BookManagement/ReverseHandledInDocument";
        }

        $.post(url,
        {
            notIntNo: $("#txtDocumentNo").val()
        },
        function (message) {
            if (message.Status == true) {
                //alert(message.Text);
                //$("#spMessage").html(message.Text);
                var fullNumber = prefix.val() + "/" + setZeroString(docNo.val(), zeroLength) + "/" + authNo.val() + "/" + $("#checkNumTxt").val(); ;
                $("#spMessage").html("Operation successful, Notice Number:" + fullNumber);
                //$("#txtDocumentNo").val('');
                //$("#txtDocumentNo").focus();
                setContorlsEmpty();
            }
            else {
                if (message.Text != '') {
                    $("#spMessage").html(message.Text);
                }
            }

        },
        'json');
    });
});

function CheckEmpty(prefix, docNo, authNo) {
    var flag = true;
    var message = "";
    if (prefix.val() == "") {
        flag = false;
        message = "Prefix";
        prefix.focus();
    }
    else if (docNo.val() == "") {
        flag = false;
        message = "Document Number";
        docNo.focus();
    }
    else if (authNo.val() == "") {
        flag = false;
        message = "Authority Code";
        authNo.focus();
    }
    if (!flag)
        $("#spMessage").html(message + " can not be empty !");
    return flag;
}
function setContorlsEmpty() {
    $("prefixTxt").val("");
    $("docNoTxt").val("");
    $("authCodeTxt").val("");
    $("#txtDocumentNo").val("");
}
function setZeroString(str, len) {
    var length = parseInt(len);
    var strLong = str.length;
    var strZero = "";
    for (var i = 0; i < (length - strLong); i++) {
        strZero += "0";
    }
    return strZero + str;
}
//20130111 added by Nancy for moving to the sequence automatically  
function AutoQueue() {
    var NPrefix = $("#prefixTxt").val();
    if (NPrefix.toString().length >= 2) {
        $("#docNoTxt").focus();
    }
}