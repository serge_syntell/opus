﻿/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />
$(document).ready(function() {

    $("#ddpMetroList").change(function() {
        $("#ddpDepotFromList option[value!=0]").remove();
        $("#ddpDepotToList option[value!=0]").remove();
        if ($("#ddpMetroList").val() != '0') {
            $.post(_ROOTPATH + "/BookManagement/GetDepotByMetrIntNo",
            {
                metroIntNo: $("#ddpMetroList").val()
            },
            function(depotList) {
                //alert(depotList);
                //                $("#ddpDepotFromList option[value!=0]").remove();
                //                $("#ddpDepotToList option[value!=0]").remove();
                $.each(depotList, function(i, depot) {
                    if (depot.DepotIntNo != 0) {
                        $("<option value=" + depot.DepotIntNo + ">" + depot.DepotDescription + "</option>").appendTo($("#ddpDepotFromList"));
                    }
                });
                $.each(depotList, function(i, depot) {
                    if (depot.DepotIntNo != 0) {
                        $("<option value=" + depot.DepotIntNo + ">" + depot.DepotDescription + "</option>").appendTo($("#ddpDepotToList"));
                    }
                });
            }, 'json');
            $.post(_ROOTPATH + "/BookManagement/GetOfficerByMetrIntNo",
            {
                metroIntNo: $("#ddpMetroList").val()
            },
            function(depotList) {
                //alert(depotList);
                $("#ddpOfficerFromList option[value!=0]").remove();
                $("#ddpOfficerToList option[value!=0]").remove();
                $.each(depotList, function(i, officer) {
                    if (officer.TOIntNo != 0) {
                        $("<option value=" + officer.TOIntNo + ">" + officer.TOName + "</option>").appendTo($("#ddpOfficerFromList"));
                    }
                });
                $.each(depotList, function(i, officer) {
                    if (officer.TOIntNo != 0) {
                        $("<option value=" + officer.TOIntNo + ">" + officer.TOName + "</option>").appendTo($("#ddpOfficerToList"));
                    }
                });
            }, 'json');
        }
    });

    //    $("#aselectAll").click(function() {
    //        alert("a");
    //        $(":checkbox").each(function(i, item) {
    //            if ($(item).attr('checked')) {
    //                //alert(item.id);
    //                $(item).attr('checked', true);
    //            }
    //            else {
    //                $(item).attr('checked', false);
    //            }
    //        });

    //    });

    $("#ddpOfficerFromList").change(function() {

        $(".CartListItemAlt").remove();
    });
    $("#ddpDepotFromList").change(function() {

        $(".CartListItemAlt").remove();
    });

    $("#aselectAll").click(function() {

        $(":checkbox").each(function(i, item) {
            $(item).attr('checked', true);
        });

    });

    $("#aclearALL").click(function() {

        $(":checkbox").each(function(i, item) {
            $(item).attr('checked', false);
        });

    });

    $("#btnTransfer").click(function() {

        if ($("#ddpMetroList").val() == '0') {
            //alert("No metro has been selected");
            alert(selectMetro);
            $("#ddpMetroList").focus();
            return;
        }

        if (optionType == "DEPOT") {
            if ($("#ddpDepotToList").val() == '0') {
                //alert("No depot has been selected");
                alert(selectDepot);
                $("#ddpDepotToList").focus();
                return;
            }
            if ($("#ddpDepotToList").val() == $("#ddpDepotFromList").val()) {
                //alert("From Depot and To depot must be different");
                alert(differentDepot);
                return;
            }
        }
        else {
            if ($("#ddpOfficerToList").val() == '0') {
                //alert("No officer has been selected");
                alert(selectOfficer);
                $("#ddpOfficerToList").focus();
                return;
            }
            if ($("#ddpOfficerToList").val() == $("#ddpOfficerFromList").val()) {
                //alert("From Officer and To officer must be different");
                alert(differentOfficer);
                return;
            }
        }
        var bookID = '';
        $(":checkbox").each(function(i, item) {
            if ($(item).attr('checked')) {
                //alert(item.id);
                bookID = bookID + item.id + ";";
            }
        });

        if (bookID == '') {
            //alert("No books has been selected");
            alert(selectBooks);
            return;
        }
        else {
            $.post(_ROOTPATH + '/BookManagement/Transfer',
            {
                depotFromIntNo: $("#ddpDepotFromList").val(),
                depotToIntNo: $("#ddpDepotToList").val(),
                metroIntNo: $("#ddpMetroList").val(),
                officerFromIntNo: $("#ddpOfficerFromList").val(),
                officerToIntNo: $("#ddpOfficerToList").val(),
                bookId: bookID
            },
            function(message) {
                if (message.Status == true) {
                    $("#formTransferBook").submit();
                    window.open(_ROOTPATH + message.Text);
                }
                else {
                    if (message.Text != '') {
                        alert(message.Text);
                    }
                }
            }, 'json');
        }

    });


    $("#chbDepot").click(function() {
        $("#chbOfficer").attr("checked", false);
        $("#trToOfficer").hide();
        $("#ddpOfficerToList").val("0");
        $("#ddpOfficerFromList").val("0");
        $("#trFromOfficer").hide();
        $("#trToDepot").show();
        $("#trFromDepot").show();
        $(".CartListItemAlt").remove();
        optionType = "DEPOT";
    });

    $("#chbOfficer").click(function() {
        $("#chbDepot").attr("checked", false);
        $("#trToOfficer").show();
        $("#trFromOfficer").show();
        $("#trToDepot").hide();
        $("#trFromDepot").hide();
        $("#ddpDepotFromList").val("0");
        $("#ddpDepotToList").val("0");
        $(".CartListItemAlt").remove();
        optionType = "OFFICER";
    });
});