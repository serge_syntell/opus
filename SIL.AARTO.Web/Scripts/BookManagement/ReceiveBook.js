﻿/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />
$(document).ready(function () {
    $("#ddpMetroList").change(function () {
        $("#ddpDepotList option[value!=0]").remove();
        if ($("#ddpMetroList").val() != '0') {
            $.post(_ROOTPATH + "/BookManagement/GetDepotByMetrIntNo",
            {
                metroIntNo: $("#ddpMetroList").val()
            },
            function (depotList) {
                //alert(depotList);

                $.each(depotList, function (i, depot) {
                    if (depot.DepotIntNo != 0) {
                        $("<option value=" + depot.DepotIntNo + ">" + depot.DepotDescription + "</option>").appendTo($("#ddpDepotList"));
                    }
                });
            }, 'json');
        }

        if ($("#ddpMetroList").val() == '0') return;

        if ($("#ddpBookTypeId").val() == '0') return;

        $.post(_ROOTPATH + "/BookManagement/GetPrefixByType",
        {
            bookTypeId: $("#ddpBookTypeId").val(),
            metroIntNo: $("#ddpMetroList").val()
        },
        function (prefixItems) {
            $("#ddpNotPrefix option[value!=0]").remove();
            $.each(prefixItems, function (i, prefix) {
                if (prefix.NPHIntNo != 0) {
                    $("<option value=" + prefix.NPHIntNo + ">" + prefix.NotPrefix + "</option>").appendTo($("#ddpNotPrefix"));
                }
            });
        }, 'json');

    });

    $("#ddpBookTypeId").change(function () {
        $("#txtBookStartNo").val('');
        $("#txtBookEndNo").val('');

        if ($("#ddpMetroList").val() == '0') return;

        if ($("#ddpBookTypeId").val() == '0') return;

        $.post(_ROOTPATH + "/BookManagement/GetPrefixByType",
        {
            bookTypeId: $("#ddpBookTypeId").val(),
            metroIntNo: $("#ddpMetroList").val()
        },
        function (prefixItems) {
            $("#ddpNotPrefix option[value!=0]").remove();
            $.each(prefixItems, function (i, prefix) {
                if (prefix.NPHIntNo != 0) {
                    $("<option value=" + prefix.NPHIntNo + ">" + prefix.NotPrefix + "</option>").appendTo($("#ddpNotPrefix"));
                }
            });
        }, 'json');
    });

    $("#txtBookStartNo").change(function () {
        reg = /\d$/;
        if (!reg.test($("#txtNumberOfBooks").val())) {
            alert(jsIncorrectBoookNum);
            $("#txtNumberOfBooks").focus();
            return;
        }
        if (!reg.test($("#txtBookStartNo").val())) {
            alert(jsIncorrectBookStartNo);
            $("#txtBookStartNo").focus();
            return;
        }
        if ($("#ddpBookTypeId").val() == '0') {
            alert(JsSelectBookType);
            $("#ddpBookTypeId").focus();
            return false;
        }
        $.post(_ROOTPATH + "/BookManagement/GetBookEndNo",
        {
            bookTypeId: $("#ddpBookTypeId").val(),
            bookNum: $("#txtNumberOfBooks").val(),
            bookStartNo: $("#txtBookStartNo").val()
        },
        function (message) {
            if (message.Status == true) {
                $("#txtBookEndNo").val(message.Text);
            }
        }, 'json');

    });

    $("#txtNumberOfBooks").change(function () {
        reg = /\d$/;
        if (!reg.test($("#txtNumberOfBooks").val())) {
            alert(jsIncorrectBoookNum);
            $("#txtNumberOfBooks").focus();
            return;
        }

        //Heidi 2014-10-16 added for fixing AARTOErrorLog shows issue.(5367)
        var txtBookStartNoValue = $("#txtBookStartNo").val();
        if (txtBookStartNoValue != "") {
            $.post(_ROOTPATH + "/BookManagement/GetBookEndNo",
            {
                bookTypeId: $("#ddpBookTypeId").val(),
                bookNum: $("#txtNumberOfBooks").val(),
                bookStartNo: $("#txtBookStartNo").val()

            },
            function (message) {
                if (message.Status == true) {
                    $("#txtBookEndNo").val(message.Text);
                }
            }, 'json');
        }

    });

    $("#btnReceive").click(function () {
        if ($("#txtPrintingCompany").val() == '') {
            alert(jsPrintingCompany);
            $("#txtPrintingCompany").focus();
            return false;
        }
        if ($("#ddpBookTypeId").val() == '0') {
            alert(JsSelectBookType);
            $("#ddpBookTypeId").focus();
            return false;
        }
        if ($("#txtNumberOfBooks").val() == '') {
            alert(jsBoookNum);
            $("#txtNumberOfBooks").focus();
            return false;
        }
        if ($("#ddpDepotList").val() == '0') {
            alert(selectDepot);
            $("#ddpDepotList").focus();
            return false;
        }
        if ($("#ddpNotPrefix").val() == '0') {
            alert(JsSelectPrefix);
            $("#ddpNotPrefix").focus();
            return false;
        }
        if ($("#txtBookStartNo").val() == '') {
            alert(jsBookStartNo);
            return false;
        }
        if ($("#txtBookEndNo").val() == '') {
            alert(jsBookEndNo);
            $("#txtBookEndNo").focus();
            return false;
        }
 
        if (window.confirm(confirmMessage)) {
            $("#formReceiveBook").submit();
        }
        else {
            return false;
        }
    });

});