﻿/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />
$(document).ready(function() {
    $("#ddpMetroList").change(function() {
        $("#ddpDepotList option[value!=0]").remove();
        $("#ddpOfficerList option[value!=0]").remove();
        if ($("#ddpMetroList").val() != '0') {
            $.post(_ROOTPATH + "/BookManagement/GetDepotByMetrIntNo",
            {
                metroIntNo: $("#ddpMetroList").val()
            },
            function(depotList) {
                //alert(depotList);

                $.each(depotList, function(i, depot) {
                    if (depot.DepotIntNo != 0) {
                        $("<option value=" + depot.DepotIntNo + ">" + depot.DepotDescription + "</option>").appendTo($("#ddpDepotList"));
                    }
                });
            }, 'json');
            $.post(_ROOTPATH + "/BookManagement/GetOfficerByMetrIntNo",
            {
                metroIntNo: $("#ddpMetroList").val()
            },
            function(depotList) {
                //alert(depotList);


                $.each(depotList, function(i, officer) {
                    if (officer.TOIntNo != 0) {
                        $("<option value=" + officer.TOIntNo + ">" + officer.TOName + "</option>").appendTo($("#ddpOfficerList"));
                    }
                });
            }, 'json');
        }
    });

});