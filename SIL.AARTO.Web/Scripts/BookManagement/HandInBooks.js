﻿/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />
$(document).ready(function() {

    $("#ddpMetroList").change(function() {

        if ($("#ddpMetroList").val() != '0') {
            $.post(_ROOTPATH + "/BookManagement/GetDepotByMetrIntNo",
            {
                metroIntNo: $("#ddpMetroList").val()
            },
            function(depotList) {

                $("#ddpDepotList option[value!=0]").remove();
                $.each(depotList, function(i, depot) {
                    if (depot.DepotIntNo != 0) {
                        $("<option value=" + depot.DepotIntNo + ">" + depot.DepotDescription + "</option>").appendTo($("#ddpDepotList"));
                    }
                });

            }, 'json');
            $.post(_ROOTPATH + "/BookManagement/GetOfficerByMetrIntNo",
            {
                metroIntNo: $("#ddpMetroList").val()
            },
            function(depotList) {

                $("#ddpOfficerList option[value!=0]").remove();

                $.each(depotList, function(i, officer) {
                    if (officer.TOIntNo != 0) {
                        $("<option value=" + officer.TOIntNo + ">" + officer.TOName + "</option>").appendTo($("#ddpOfficerList"));
                    }
                });

            }, 'json');
        }
    });

    $("#aselectAll").click(function() {

        $(":checkbox").each(function(i, item) {
            $(item).attr('checked', true);
        });

    });

    $("#aclearALL").click(function() {

        $(":checkbox").each(function(i, item) {
            $(item).attr('checked', false);
        });

    });

    $("#btnHandIn").click(function() {
        if ($("#ddpDepotList").val() == '0') {
            //alert("No depot has been selected");
            alert(selectDepot);
            return;
        }
        if ($("#ddpMetroList").val() == '0') {
            //alert("No metro has been selected");
            alert(selectMetro);
            return;
        }
        if ($("#ddpOfficerList").val() == '0') {
            //alert("No officer has been selected");
            alert(selectOfficer);
            return;
        }
        var bookID = '';
        $(":checkbox").each(function(i, item) {
            if ($(item).attr('checked')) {
                //alert(item.id);
                bookID = bookID + item.id + ";";
            }
        });

        if (bookID == '') {
            //alert("No books has been selected");
            alert(selectBooks);
            return;
        }
        else {
            $.post(_ROOTPATH + '/BookManagement/HandIn',
            {
                depotIntNo: $("#ddpDepotList").val(),
                metroIntNo: $("#ddpMetroList").val(),
                officerIntNo: $("#ddpOfficerList").val(),
                bookId: bookID,
                type: 'H'
            },
            function(message) {
                if (message.Status == true) {
                    $("#formHandInBook").submit();
                    
                }
            }, 'json');
        }

    });

    $("#btnReturnInComplete").click(function() {

        if ($("#ddpDepotList").val() == '0') {
            //alert("No depot has been selected");
            alert(selectDepot);
            $("#ddpDepotList").focus();
            return;
        }
        if ($("#ddpMetroList").val() == '0') {
            //alert("No metro has been selected");
            alert(selectMetro);
            $("#ddpMetroList").focus();
            return;
        }
        if ($("#ddpOfficerList").val() == '0') {
            //alert("No officer has been selected");
            alert(selectOfficer);
            $("#ddpOfficerList").focus();
            return;
        }
        var bookID = '';
        $(":checkbox").each(function(i, item) {
            if ($(item).attr('checked')) {
                //alert(item.id);
                bookID = bookID + item.id + ";";
            }
        });

        if (bookID == '') {
            //alert("No books has been selected");
            alert(selectBooks);

            return;
        }
        else {
            $.post(_ROOTPATH + '/BookManagement/HandIn',
            {
                depotIntNo: $("#ddpDepotList").val(),
                metroIntNo: $("#ddpMetroList").val(),
                officerIntNo: $("#ddpOfficerList").val(),
                bookId: bookID,
                type: 'R'
            },
            function(message) {
                if (message.Status == true) {
                    $("#formHandInBook").submit();
                    //window.open(message.Text);
                }
            }, 'json');
        }

    });

});