﻿/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />
$(document).ready(function() {
    $("#btnEnquiry").click(function() {
        $.post(_ROOTPATH + "/Home/GetEnquiryUrl", {},
        function(message) {
            if (message.Status == true) {
                //window.open(_ROOTPATH + "/Home/Index?ReturnUrl='" + _ROOTPATH + message.Text + "'");
                window.open(_ROOTPATH + message.Text);
            }
        }, 'json');
    })
    $("#ddlAuthority").change(function() {
        if ($("#ddlAuthority").val() == '0') {
            alert(JsSelectAuthority);
            return;
        }
        $.post(_ROOTPATH + "/Home/SetDefaultAuth", { autIntNo: $("#ddlAuthority").val() },
        function(message) {
            if (message.Status != true) {
                alert(JsSetAuthorityFailure);
            }
        }, 'json');

    });
});