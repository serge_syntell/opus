﻿
function slideDisplay(obj) {
    //Jerry 2014-12-03 change it by Serge raised
    //$(obj).siblings("div:last-child").slideDown();
    //$(obj).removeClass("menu_bgout");
    //$(obj).addClass("menu_bgover");
    ////$(obj).parent().slblings().children("div:last-child").slideUp();
    var cssName = $(obj).attr("class");
    if (cssName == "menu_bgout") {
        $(obj).siblings("div:last-child").slideDown();
        $(obj).removeClass("menu_bgout");
        $(obj).addClass("menu_bgover");
    }
    else if (cssName == "menu_bgover") {
        $(obj).siblings("div:last-child").slideDown();
        $(obj).removeClass("menu_bgover");
        $(obj).addClass("menu_bgout");
        $(obj).next().attr("style", "display: none;");
    }

    var parentObj;
    $(obj).parent("div").each(function() {
        parentObj = $(this)[0];
        //$(parentObj).children("div: first-child").removeClass("menu_bgover");
        //$(parentObj).children("div: first-child").addClass("menu_bgout");
        $(parentObj).siblings().find(".menu_child_bg").siblings().removeClass("menu_bgover");
        $(parentObj).siblings().find(".menu_child_bg").siblings().addClass("menu_bgout");
        $(parentObj).siblings().find(".menu_child_bg").slideUp();
    });
}

function slideDisplay_D(obj) {
    $(obj).removeClass("menu_bg_out").addClass("menu_bg_over");
    $(obj).siblings().removeClass("menu_bg_over").addClass("menu_bg_out");
    //$(obj).children("div").removeClass("dropdown_menu_bg_out").addClass("dropdown_menu_bg_over");
    $(obj).children("div").slideDown();
    $(obj).siblings().children("div").slideUp();


}

function oa_tool() {
    var frameshow = document.getElementById("showframe");
    var fs = top.document.getElementsByName("mainframe");
    if (fs[0].cols == "190,8,*") {
        frameshow.src = "../../Content/Image/Menu/Menu_frame_arrow_open.gif";
        fs[0].cols = "0,8,*";
    } else {
    frameshow.src = "../../Content/Image/Menu/Menu_frame_arrow_close.gif";
        fs[0].cols = "190,8,*";
    }
}

function changeMenu(obj, level) {
    var displayValue;
    //alert($(obj).parent().parent().find("span:first").html());
    switch (level) {
        case 1:
            displayValue = $(obj).find("span:first").html();
            break;
        case 2:
            displayValue = $(obj).parent().parent().parent().find("span:first").html()
                            + "&nbsp;&nbsp;»&nbsp;&nbsp;"
                            + $(obj).find("span:first").html();
            break;
        case 3:
            displayValue = $(obj).parent().parent().parent().parent().parent().find("span:first").html()
                            + "&nbsp;&nbsp;»&nbsp;&nbsp;"
                            + $(obj).parent().parent().find("span:first").html()
                            + "&nbsp;&nbsp;»&nbsp;&nbsp;"
                            + $(obj).find("span:first").html();

            break;
        default:
            //divObj = $(obj).parent.parent.parent.parent.parent.parent;
            goto(3);
            break;
    }
    $("#hidNavigationMenu").val(displayValue);
}
