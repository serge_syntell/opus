var responseMessage = null;


/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />
$(document).ready(function () {
    //    $("#ImgUserNameError").css("display", "none");
    //    $("#ImgPasswordError").css("display", "none");

    $("#btnReset").click(function () {

        $("#lblPasswordRetype").text('');

        if ($("#Password1").val() != $("#PasswordRetype").val()) {
            $("#lblPasswordRetype").text('Password don\'t match!');
            return false;
        }


        $.post(_ROOTPATH + "/Account/Reset",
        { Password1: $("#Password1").val(), PasswordRetype: $("#PasswordRetype").val() },
        function (message) {
            responseMessage = message;

            $("#faceboxInfo").html(replaceAll("\n", "<br />", message.Text));
            $("#faceboxInfo").dialog("open");
            $("#hidIsLogin").val("0");

        }, "json");
        return false;
    });

    $('#faceboxInfo').dialog({
        autoOpen: false,
        width: 600,
        height: 200,
        position: "center",
        resizable: false,
        modal: true,
        buttons: {
            "Ok": function () {
                if (responseMessage != null && responseMessage.Status == true) {
                    $(this).dialog("close");
                    window.parent.location.href = _ROOTPATH + "/Account/Login";
                }
                else {
                    $(this).dialog("close");
                }
            }
        }
    });

});


function replaceAll(find, replace, str) {
    return str.replace(new RegExp(find, 'g'), replace);
}

