﻿/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />
$(document).ready(function () {
    //    $("#ImgUserNameError").css("display", "none");
    //    $("#ImgPasswordError").css("display", "none");

    $("#btnReset").click(function () {

        $("#lblUserName").text('');
        $("#lblEmail").text('');


        if ($("#userName").val() == '') {
            //alert("User login name is required!");
            $("#lblUserName").text('User login name is required!');
            return false;
        }
        if ($("#userEmail").val() == '') {
            //alert("Password is required!");
            $("#lblEmail").text('Email is required!');
            return false;
        }

        $.post(_ROOTPATH + "/Account/RequestReset",
        { UserLoginName: $("#userName").val(), UserEmail: $("#userEmail").val(), returnUrl: returnUrl },
        function (message) {
            if (message.Status == true) {

                window.parent.location.href = _ROOTPATH + "/Account/Login";

                //if (message.User == "Block") {
                //    $("#faceboxInfo").dialog("open");
                //    $("#hidIsLogin").val("1");
                //    $("#hidURL").val(_ROOTPATH + message.Text);
                //}
                //else {
                //    window.parent.location.href = _ROOTPATH + message.Text;
                //}
                //window.opener = null;
                //window.open(_ROOTPATH + message.Text, 'main', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,fullscreen=0,left=0,top=0,height=980,width=1270');

                ////window.close();

            }
            else {
                $("#faceboxInfo").text(message.Text);
                $("#faceboxInfo").dialog("open");
                $("#hidIsLogin").val("0");

                //var colon = message.Text.indexOf(":");
                //var type = message.Text.substr(0, colon);
                //if (type == "userName") {
                //    //$("#ImgUserNameError").css("display", "inline");
                //    //$("#ImgPasswordError").css("display", "none");
                //    //$("#ImgUserNameError").attr("tooltipText", message.Text.substr(colon + 1));
                //    $("#lblUserName").text(message.Text.substr(colon + 1));
                //}
                //else if (type == "password") {
                //    //$("#ImgPasswordError").css("display", "inline");
                //    //$("#ImgUserNameError").css("display", "none");
                //    // $("#ImgPasswordError").attr("tooltipText", message.Text.substr(colon + 1));
                //    $("#lblPassword").text(message.Text.substr(colon + 1));
                //}
                //else if (type == "Block") {
                //    //$("#ImgPasswordError").css("display", "inline");
                //    //$("#ImgUserNameError").css("display", "none");
                //    // $("#ImgPasswordError").attr("tooltipText", message.Text.substr(colon + 1));
                //    $("#faceboxInfo").text(message.Text.substr(colon + 1));
                //    $("#faceboxInfo").dialog("open");
                //    $("#hidIsLogin").val("0");
                //}
            }
        }, "json");
        return false;
    });

    $('#faceboxInfo').dialog({
        autoOpen: false, //如果设置为true，则默认页面加载完毕后，就自动弹出对话框；相反则处理hidden状态。 
        //bgiframe: true, //解决ie6中遮罩层盖不住select的问题  
        width: 600,
        height: 200,
        position: "center",
        resizable: false,
        modal: true, //这个就是遮罩效果   
        buttons: {
            "Ok": function () {
                if ($("#hidIsLogin").val() == "1") {
                    $(this).dialog("close");
                    window.parent.location.href = $("#hidURL").val();
                }
                else {
                    $(this).dialog("close");
                }
            }
        }
    });

})

