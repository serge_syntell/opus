﻿

function setDateTextBox(txtId) {
    $('#' + txtId).datepicker(
	                { changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd', defaultDate: ' ' }
                        );
}

$(function() {
    setDateTextBox('txtDateStart');
    setDateTextBox('txtDateEnd');
});

$(function() {

if (IsInSession.toLowerCase() == 'true') {
    window.open(_ROOTPATH + "/ViewOfficerErrorsReport.aspx");
    }

});