﻿var MAIN_DIV_WIDTH = 657;
var MAIN_DIV_HEIGHT = 420;
var MAIN_IMAGE_WIDTH;
var MAIN_IMAGE_HEIGHT;
var MAIN_DIV_X;
var MAIN_DIV_Y;
var MIN_WIDTH = 40;
var STEP = 50;          //10 - dls 090720 - increased the size of the STEP so that the user can Zoom faster
var DEFAULT_BRIGHTNESS = "0.0";
var DEFAULT_CONTRAST = "1.0";
var Percent = 0.2;
var EnableZoomOut = false;
var urls;
var OriginalSize = false;
var ShowCrossHair = false;
var XValue;
var YValue;
var CrossStyle;
var MainSrc;
var ShowSide;
var SavedContrast;
var SavedBrightness;
var Contrast;
var Brightness;
var ScImPrintVal;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///Load2
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function CheckedSaveImageSettings(checked) {
    if (checked) {
        $("#SaveImageSettings").attr("checked", true);
        $("#labelForSISet").css("background-color", "red").css("color", "white");
        $("#lblError").text(lblError);
    }
    else {
        $("#SaveImageSettings").attr("checked", false);
        $("#labelForSISet").css("background-color", "").css("color", "black");
        $("#lblError").text("");
    }
}
function LoadImage(parameters) {
    document.getElementById('mainImageDiv').style.display = "none";
    document.getElementById('regDiv').style.display = "none";
    document.getElementById('pre1Div').style.display = "none";
    document.getElementById('pre2Div').style.display = "none";
    
    document.getElementById('btnCrossHairs').style.visibility = 'hidden';
    document.getElementById('btnCrossHairsHide').style.visibility = 'hidden';
    document.getElementById('btnCrossHairs').style.display = 'none';
    document.getElementById('btnCrossHairsHide').style.display = 'none';
//    document.getElementById('btnCrossHairs').style.width = 0;
//    document.getElementById('btnCrossHairsHide').style.width = 0;

    ShowSide = parameters.ShowPreView;

    SavedContrast = "";
    SavedBrightness = "";
    
    if(document.getElementById('lblImageSettingsSaved'))
        document.getElementById('lblImageSettingsSaved').style.visibility = 'hidden';
    
    urls = parameters.ImageUrls;
    for (i = urls.length -1; i >= 0; i--) {
        var url = urls[i];
        var ImgType = GetQueryStringParameter("ImgType", url);
        ScImPrintVal = GetQueryStringParameter("ScImPrintVal", url);
        Contrast = GetQueryStringParameter("Contrast", url);
        Brightness = GetQueryStringParameter("Brightness", url);
        switch (ImgType) {
            case "A":
                document.getElementById('mainImageDiv').style.display = "";
                MainSrc = url;
                if (ScImPrintVal == "1") {
                    SavedContrast = parameters.SavedContrast;
                    SavedBrightness = parameters.SavedBrightness;
                    if (SavedContrast && SavedBrightness) {
                        CheckedSaveImageSettings(true);
                    }
                    else {
                        CheckedSaveImageSettings(false);
                    }
                }
                break;
            case "R":
                if (ShowSide == "1") {
                    document.getElementById('regDiv').style.display = "";
                    document.getElementById('regImage').src = url;
                }
                break;
            case "B":
            case "D":
                if (ShowSide == "1") {
                    document.getElementById('pre2Div').style.display = "";
                    document.getElementById('pre2Image').src = url;
                }
                break;
        }
    }

    if (ShowSide == "1") {
        document.getElementById('pre1Div').style.display = "";
        document.getElementById('pre1Image').src = MainSrc;
    }

    if (SavedBrightness != "") {
        if (document.getElementById('lblImageSettingsSaved'))
            document.getElementById('lblImageSettingsSaved').style.visibility = '';

        SetQueryStringParameter("SavedBrightness", SavedBrightness);
        InitSliderB(SavedBrightness);
    }
    else {
        if (document.getElementById('lblImageSettingsSaved'))
            document.getElementById('lblImageSettingsSaved').style.visibility = 'hidden';

        //SetQueryStringParameter("Brightness", Brightness);
        InitSliderB(Brightness);
    }

    if (SavedContrast != "") {
        SetQueryStringParameter("SavedContrast", SavedContrast);
        InitSliderC(SavedContrast);
    }
    else {
        //SetQueryStringParameter("Contrast",Contrast);
        InitSliderC(Contrast);
    }

    SetDropDownList(parameters.ImageList);
    if (parameters.OriginalSize == true)
        OriginalSize = true;
    if (parameters.ShowCrossHair == true)
        ShowCrossHair = true;
    XValue = parameters.XValue;
    YValue = parameters.YValue;
    CrossStyle = parameters.CrossStyle;

    if (ShowCrossHair) {
        CrossHairs();
        SetQueryStringParameter("ShowCrossHair", ShowCrossHair);
        document.getElementById('btnCrossHairs').style.visibility = '';
        document.getElementById('btnCrossHairsHide').style.visibility = '';
        document.getElementById('btnCrossHairs').style.display = '';
        document.getElementById('btnCrossHairsHide').style.display = '';
        document.getElementById('btnCrossHairs').style.width = 150;
        document.getElementById('btnCrossHairsHide').style.width = 150;
    }
    pageLoad();
}


var image;
function pageLoad() {
    $("#lblFeedback").text(lblFeedback1);
    ReleaseControl();
    
    if (document.getElementById('mainImageDiv').lastChild.nodeName == "IMG") {
        document.getElementById('mainImageDiv').removeChild(document.getElementById('mainImageDiv').lastChild);
    }

    $("#btnZoom").bind("click", EnableZoom);
    $("#btnRbZoom").bind("click", EnableRubberbandingZoom);
    $("#btnCenter").bind("click", SetCenter);
    $("#mainImageDiv").bind("contextmenu", function(e) { return false; });

    var imageSource = document.createElement("img");

    if (document.getElementById('mainImageDiv').lastChild.nodeName != "IMG") {
        document.getElementById('mainImageDiv').appendChild(imageSource);
    }

    imageSource.src = MainSrc;
    imageSource.id = mainImageID;
    imageSource.style.position = "absolute";

    image = document.getElementById(mainImageID);
    MainSrc = image.src; 
    
    var mainDiv = document.getElementById('mainImageDiv');
    MAIN_DIV_WIDTH = mainDiv.clientWidth;
    MAIN_DIV_HEIGHT = mainDiv.clientHeight;
    MAIN_DIV_X = mainDiv.offsetLeft;
    MAIN_DIV_Y = mainDiv.offsetTop;

    image.style.visibility = 'hidden';
    HandleImage();
    imageSource = null;
    image.onmousedown = ReturnFalse;

    InitSliderB($("#brightness").val());
    InitSliderC($("#contrast").val());
    bindMouseEvent();
}

function ReturnFalse() {
    return false;
}

function HandleImage() {
    if (image.complete) {
        $("#lblFeedback").text(lblFeedback2);
        MAIN_IMAGE_WIDTH = image.width;
        MAIN_IMAGE_HEIGHT = image.height;

        ResetRbZoom();
        RbZoomInit();
        ResetZoom();
        SetImageFit();
        image.style.visibility = '';
    }
    else
        setTimeout("HandleImage()", 500);
}

//SetQueryStringParameter("Invert", "");

function pageUnload() {
    ReleaseControl();
}

function ReleaseControl() {
    $("#btnZoom").unbind();
    $("#btnRbZoom").unbind();
    $("#btnCenter").unbind();
    $("#" + mainImageID).unbind("click");
    $("#" + mainImageID).css = null;
    $("#" + mainImageID).unbind('mouseup', up);
    $("#" + mainImageID).unbind('onmousedown', ReturnFalse);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///Image postion
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function SetImageFit() {
    //var image = document.getElementById(mainImageID);
    //var mainDiv = document.getElementById('mainImageDiv');

    if (!OriginalSize) {
        BestFit();
    }
    else {
        image.width = MAIN_IMAGE_WIDTH;
        image.height = MAIN_IMAGE_HEIGHT;
        image.style.left = Math.round((MAIN_DIV_WIDTH - MAIN_IMAGE_WIDTH) / 2) + "px";
        image.style.top = Math.round((MAIN_DIV_HEIGHT - MAIN_IMAGE_HEIGHT) / 2) + "px";
    }
}

function BestFit() {
    //var image = document.getElementById(mainImageID);
    var XYRatio = MAIN_IMAGE_WIDTH / MAIN_IMAGE_HEIGHT;

    if (MAIN_DIV_HEIGHT * XYRatio > MAIN_DIV_WIDTH) {
        image.width = MAIN_DIV_WIDTH;
        image.height = MAIN_DIV_WIDTH / XYRatio;
    }
    else {
        image.width = MAIN_DIV_HEIGHT * XYRatio;
        image.height = MAIN_DIV_HEIGHT;
    }

    image.style.left = Math.round((MAIN_DIV_WIDTH - image.width) / 2) + "px";
    image.style.top = Math.round((MAIN_DIV_HEIGHT - image.height) / 2) + "px";
}

function BestFitForPre(image, url) {
    var WIDTH = GetQueryStringParameter("Width", url);
    var HEIGHT = GetQueryStringParameter("Height", url);

    var XYRatio = WIDTH / HEIGHT;

    if (100 * XYRatio > 160) {
        image.width = 160;
        image.height = 160 / XYRatio;
    }
    else {
        image.width = 100 * XYRatio;
        image.height = 100;
    }

    image.style.left = Math.round((160 - image.width) / 2) + "px";
    image.style.top = Math.round((100 - image.height) / 2) + "px";
}

function SetCenter() {
    image.style.left = Math.round((MAIN_DIV_WIDTH - image.width) / 2) + "px";
    image.style.top = Math.round((MAIN_DIV_HEIGHT - image.height) / 2) + "px";
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///Zoom
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function EnableZoom() {
    ResetRbZoom();
    ResetZoom();
    jQuery.each(jQuery.browser, function(i) {
        if ($.browser.msie) {
            $("#" + mainImageID).css({ 'cursor': 'images/ZoomIn.cur' });
        }
        if ($.browser.mozilla) {
            $("#" + mainImageID).css({ 'cursor': '-moz-zoom-in' });
        }
    });

    $("#" + mainImageID).click(ZoomIn);
    EnableZoomOut = true;
    $("#lblFeedback").text(lblFeedback3);
}

function EnableRubberbandingZoom() {
    ResetZoom();
    ResetRbZoom();
    BestFit();

    $("#" + mainImageID).css({ 'cursor': 'default' });
    $(document).mouseup(up);
    //$("#lblFeedback").text("Click left button, use rubber band select a area to zoom in.");
    $("#lblFeedback").text(lblFeedback4);
    RbZoomInit();
    select = true;
}

function ZoomIn() {
    //var image = document.getElementById(mainImageID);
    var originalW =  image.width;
    var originalH =  image.height;

    image.width *= (1 + Percent);
    image.height = Math.round(image.width * ((MAIN_IMAGE_HEIGHT) / (MAIN_IMAGE_WIDTH)));

    image.style.left = (image.style.left.replace("px", "") - (image.width - originalW) / 2) + "px";
    image.style.top = (image.style.top.replace("px", "") - (image.height - originalH) / 2) + "px";
}
function ZoomOut() {
    if (EnableZoomOut) {
        //var image = document.getElementById(mainImageID);
        if (image.width > STEP) {
            var originalW = image.width;
            var originalH = image.height;
            
            image.width *= (1 - Percent);
            image.height = Math.round(image.width * ((MAIN_IMAGE_HEIGHT) / (MAIN_IMAGE_WIDTH)));

            image.style.left = (image.style.left.replace("px", "") - (image.width - originalW) / 2) + "px";
            image.style.top = (image.style.top.replace("px", "") - (image.height - originalH) / 2) + "px";
        }
        else {
            alert(alert1);
        }
    }    
}
function ResetZoom() {
//    image.width = MAIN_IMAGE_WIDTH;
//    image.height = MAIN_IMAGE_HEIGHT;

//    SetCenter();
    $("#" + mainImageID).css({ 'cursor': 'default' });
    $("#" + mainImageID).unbind('click', ZoomIn);
    EnableZoomOut = false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///Rubber Zoom
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function ResetRbZoom() {
    $("#mainImageDiv").unbind('mousedown',down);
    //$("#mainImageDiv").unbind('mousemove', move);
    $("#mainImageDiv").unbind('mouseup', up);
    $("#" + mainImageID).unbind('mouseup', up);
    $(document).unbind('mouseup', up);
    select = false;
}

var select = false;
var rect = document.getElementById("rect");
var downX = 0;
var downY = 0;
var mouseX2 = downX;
var mouseY2 = downY;

function RbZoomInit() {
    select = false;
    rect = document.getElementById("rect");
    downX = 0;
    downY = 0;
    
    mouseX2 = downX;
    mouseY2 = downY;

    rect.style.width = "0px";
    rect.style.height = "0px";
    rect.style.visibility = 'hidden';
    rect.style.zIndex = 1000;
    rect.style.position = "absolute";
    rect.style.backgroundColor = "#80FF80";
    
}

var Rectmove = false;
function down(evt) {
    if (select) {
        rect.style.visibility = 'visible';
        downX = document.documentElement.scrollLeft + evt.clientX - MAIN_DIV_X;
        downY = document.documentElement.scrollTop + evt.clientY - MAIN_DIV_Y;

        rect.style.width = "0px";
        rect.style.height = "0px";

        rect.style.left = downX + "px";
        rect.style.top = downY + "px";
        Rectmove = true;
    }
}

function up(event) {
    if (rect.style.width == "0px" || !select) {
        return;
    }
    AreaZoom();
    rect.style.width = "0px";
    rect.style.height = "0px";
    rect.style.visibility = "hidden";
    Rectmove = false; 
}

function moveRect(event) {
    if (select && Rectmove) {
        mouseX2 = document.documentElement.scrollLeft + event.clientX - MAIN_DIV_X;
        mouseY2 = document.documentElement.scrollTop + event.clientY - MAIN_DIV_Y;

        rect.style.width = Math.abs(mouseX2 - downX) + "px";
        rect.style.height = Math.abs(mouseY2 - downY) + "px";

        rect.style.visibility = "visible";
        rect.style.left = Math.min(downX, mouseX2) + "px";
        rect.style.top = Math.min(downY, mouseY2) + "px";
    }
}

function AreaZoom() {
    var zommPercent = Math.min(MAIN_DIV_WIDTH / rect.style.width.replace("px", ""), MAIN_DIV_HEIGHT / rect.style.height.replace("px", ""));
    if (zommPercent > 100 || image.width * zommPercent > 100000) {
        alert(alert1);
        return;
    }

    image.width = image.width * zommPercent;
    image.height = image.height * zommPercent;

    image.style.left = (-1 * rect.style.left.replace("px", "") * zommPercent + image.offsetLeft * zommPercent) + "px";
    image.style.top = (-1 * rect.style.top.replace("px", "") * zommPercent + image.offsetTop * zommPercent) + "px";
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///Image operation
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function Reset(Original) {
    //alert(document.getElementById("sliderExtender"));
    if (Original)
        OriginalSize = true;
    else
        OriginalSize = false;

    try {
            SetQueryStringParameter("Invert", "");
            InitSliderB(DEFAULT_BRIGHTNESS);
            InitSliderC(DEFAULT_CONTRAST);

            SetQueryStringParameter("Contrast", DEFAULT_CONTRAST);
            SetQueryStringParameter("Brightness", DEFAULT_BRIGHTNESS);

            SetQueryStringParameter("SavedContrast", DEFAULT_CONTRAST);
            SetQueryStringParameter("SavedBrightness", DEFAULT_BRIGHTNESS);
            
            if (!OriginalSize)
                SetQueryStringParameter("NoCache", Math.random());
    }
    catch (ex) {
        alert(ex.message);
    }
    pageLoad();
}

function OriginalImage() {
    Reset("1");
}

function Invert() {
    SetQueryStringParameter("Invert", "1");
    pageLoad();
}

function CrossHairs()
{
    SetQueryStringParameter("XVal", XValue);
    SetQueryStringParameter("YVal", YValue);
    SetQueryStringParameter("CrossStyle", CrossStyle);
    pageLoad();
}

function HideCrossHairs()
{
    SetQueryStringParameter("CrossStyle", "");
    pageLoad();
}

function btnContrast_Click()
{
    SetQueryStringParameter("Contrast", document.getElementById("contrast").value);
    SetQueryStringParameter("SavedContrast", "99");
    SetQueryStringParameter("NoCaching", Math.random());
    pageLoad();
}

function btnBrightness_Click()
{
    SetQueryStringParameter("Brightness", document.getElementById("brightness").value);
    SetQueryStringParameter("SavedBrightness", "99");
    SetQueryStringParameter("NoCaching", Math.random());
    pageLoad();
}

function SetQueryStringParameter(paramName, value) {
    try {
        var parameter = "&" + paramName + "=";

        var ipos = MainSrc.indexOf(parameter);
        if (ipos > 0) {
            var end = MainSrc.indexOf("&", ipos + parameter.length);
            if (end < 0)
                MainSrc = MainSrc.substring(0, ipos) + parameter + value;
            else
                MainSrc = MainSrc.substring(0, ipos) + parameter + value + MainSrc.substring(end, MainSrc.length);
        }
        else
            MainSrc += parameter + value;
    }
    catch (ex) { }
 }

 function GetQueryStringParameter(paramName, url) {
     var parameter = "&" + paramName + "=";
     var value = "";

     var ipos = url.indexOf(parameter);
     if (ipos > 0) {
         var end = url.indexOf("&", ipos + parameter.length);
         if (end < 0)
             value = url.substring(ipos + parameter.length, url.length); 
         else
             value = url.substring(ipos + parameter.length, end);
    }
    return value;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///DropDownList operation
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function SetDropDownList(ddlItems) {
    var ItemsSplit = ddlItems.split("@@@");
    var ddl = document.getElementById('ddlScan');
    ddl.options.length = 0;
    for (i = 0; i < ItemsSplit.length; i++) {
        if(ItemsSplit[i] != null && ItemsSplit[i].length > 0)
        {
            var Itmes = ItemsSplit[i].split("&&");
            if (Itmes != null && Itmes.length == 2)
            {
                var optNew = document.createElement('OPTION');
                optNew.text = Itmes[0];
                optNew.value = Itmes[1];
                if (optNew.value == 0)
                optNew.Selected = true;

               try
               {
                    ddl.options.add(optNew); //IE
               }
               catch (ex) {
                    ddl.options.add(optNew, null); //not IE
                }
             }
        }
    }
}

function DropDownListSelect() {
    var ddl = document.getElementById('ddlScan');
    var selectValue = ddl.options[ddl.selectedIndex].value;

    for (i = 0; i < urls.length; i++) {
        var url = urls[i];
        var ImgNo = GetQueryStringParameter("ScImIntNo", url);
        if (ImgNo == selectValue) {
            MainSrc = url;

            var ImgType = GetQueryStringParameter("ImgType", MainSrc);
            if(ImgType == 'A')
                document.getElementById('pre1Image').src = MainSrc;

            SetQueryStringParameter("Contrast", document.getElementById("contrast").value);
            SetQueryStringParameter("Brightness", document.getElementById("brightness").value);
        }
    }

    pageLoad();
}

function btnNext_Click() {
    var ddl = document.getElementById('ddlScan');
    if (ddl.selectedIndex < ddl.options.length - 1)
        ddl.selectedIndex += 1;
    DropDownListSelect();   
}

function btnPrevious_Click() {
    var ddl = document.getElementById('ddlScan');
    if (ddl.selectedIndex > 0)
        ddl.selectedIndex -= 1;
    DropDownListSelect(); 
}

function btnFirst_Click() {
    var ddl = document.getElementById('ddlScan');
    ddl.selectedIndex = 0;
    DropDownListSelect();
}

function btnLast_Click() {
    var ddl = document.getElementById('ddlScan');
    ddl.selectedIndex = ddl.options.length - 1;
    DropDownListSelect(); 
}

function ddlScan_SelectedIndexChanged() {
    DropDownListSelect();
}

function rdoPreviewA_CheckedChanged() {
    MainSrc = document.getElementById('pre1Image').src;
    pageLoad();
}


function rdoPreviewB_CheckedChanged() {
    MainSrc = document.getElementById('pre2Image').src;
    pageLoad();
}


//////////////////////////////////////////////////////////Drag    releasable drag
var dragapproved = false;
var x = 0, y = 0, temp1 = 0, temp2 = 0
function drags(event) {
    if (!(document.all || document.getElementsByTagName("*"))) {
        return;
    }

    var srcElement = event.srcElement || event.target;
    if (srcElement.id ==  mainImageID)
    {
        dragapproved = true;
        temp1 = image.style.left.replace("px", "");
        temp2 = image.style.top.replace("px", "");
        x = event.clientX;
        y = event.clientY;
    }
}


function mouseup(e) {
    var evt = e || event;
    Handlemouseup(evt);
}

function mousedown(e) {
    var evt = e || event;
    Handlemousedown(evt);
}

function move(e) {
    var evt = e || event;
    Handlemousemove(evt);
}

$(function () {
    bindMouseEvent();
});
function bindMouseEvent() {
    $("#" + mainImageID).mousemove(move);
    document.onmousedown = mousedown;
    document.onmouseup = mouseup;
}
function Handlemousedown(event) {
    var srcElement = event.srcElement || event.target;
    if (srcElement.id !=  mainImageID)
        return;

    if (event.button == 2) {
        ZoomOut();
        down(event);
    }
    else {
        drags(event);
    }
}

function Handlemouseup(event) {
    dragapproved = false;
}

function Handlemousemove(event) {
    var srcElement = event.srcElement || event.target;
    if (srcElement.id !=  mainImageID && srcElement.id != "rect")
        return;

    moveRect(event);
    if (event.button <= 1 && dragapproved && image) {
        image.style.left = (parseInt(temp1) + parseInt(event.clientX - x)) + "px";
        image.style.top = (parseInt(temp2) + parseInt(event.clientY - y)) + "px";
        return false;
    }
}

function debug(msg) {
    $("#lblFeedback").text(msg);
}
