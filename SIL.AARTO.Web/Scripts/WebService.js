﻿function Action(processName, processType, url, rejectSecondaryFrames)
{
    if(document.getElementById("lblError") != null)
        document.getElementById("lblError").innerHTML = "";
        
    document.getElementById("btnAccept").disabled = true;
    if (document.getElementById("lnkPrevious") != null) 
    {
        document.getElementById("lnkPrevious").disabled = true;
    }
    if (document.getElementById("lnkNext") != null) 
    {
        document.getElementById("lnkNext").disabled = true;
    }
    if (document.getElementById("investigate") != null) {
        document.getElementById("investigate").disabled = true;
    }

    //var param = new Stalberg.TMS.ActionParam();
    //var rejIntNo = $("#RejectReason>option:selected").val();
    var param = {
        ProcessName: processName,
        RejReason: $("#RejectReason>option:selected").text(),
        //RejIntNo: rejIntNo.substr(0, rejIntNo.indexOf('|')),
        RejIntNo: $("#RejectReason>option:selected").val(),
	    Registration: $("#RegNo").val(),
	    ChangedMake: document.getElementById("VehicleType").disabled,
	    VehMake: $("#VehicleMaker").val(),
        VehType: $("#VehicleType").val(),
        //param.UserName = document.getElementById("hidUserName").value;
        ProcessType: processType,
        FrameNo: $("#FrameNo").val(),

        StatusNatis: hideField.StatusReturnToNatis,
        StatusVerified: hideField.StatusVerified,
        StatusRejected: hideField.StatusRejected,
        StatusInvestigate: hideField.StatusInvestigate,

        FrameIntNo: $("#FrameIntNo").val(),
        PreUpdatedRegNo: hideField.PreUpdatedRegNo,
        PreRejIntNo: hideField.PreRejIntNo,
        AllowContinue: document.getElementById("AllowContinue").checked,
        PreUpdatedAllowContinue : hideField.PreUpdatedAllowContinue,
        RowVersion: hideField.RowVersion,
        StatusProsecute : hideField.StatusProsecute,
        StatusCancel : hideField.StatusCancel,
        OffenceDateTime: $("#OffenceDate").val() + " " + $("#OffenceTime").val(),
        Speed1 : $("#FirstSpeed").val(),
        Speed2: $("#SecondSpeed").val(),

        SaveImageSettings: document.getElementById("chkSaveImageSettings") ?
            document.getElementById("chkSaveImageSettings").checked : null,

        InReIntNo: $("#InvestigateReason") ? $("#InvestigateReason>option:selected").val() : null,
        VeBiComment: $("#VeBiComment") ? $("#VeBiComment").val() : null,

        Contrast : $("#contrast").val(),
        Brightness: $("#brightness").val(),
        //Jerry 2013-05-16 add
        RejectSecondaryFrames: rejectSecondaryFrames
    };
        
    $.ajax({
        url: url,
        dataType: "json",
        processData: true,
        type: "POST",
        data: param,
        success: function (msg) {
            if (msg.result == 0) {
                OnRemoteInvoke(processName);
            }
            else {
                SucceededCallback(msg.data.errorMessage);
            }
        },
        faild: function (msg) {
            alert(msg);
        }
    });
}

function OnRemoteInvoke(processName) {
    var update = processName;

    if (update == "AdjudicateFrame"
    || update == "VerifyFrame"
    || update == "ReturnToNatis"
    || update == "RejectFrame"
    || update == "InvestigateFrame"
    || update == "ReturnToNatisAdj") {
        setTimeout(enableButtons, 3000);
        document.getElementById("lblMessage").style.display = "";

        var msg = "";
        switch (update) {
            case "AdjudicateFrame":
            case "InvestigateFrame":
            case "VerifyFrame":
            case "RejectFrame":
                msg = formatStr(msg_Text1, document.getElementById("FrameNo").value);
                break;
            case "ReturnToNatis":
            case "ReturnToNatisAdj":
                msg = formatStr(msg_Text2, document.getElementById("FrameNo").value);
                break;
        }
        document.getElementById("lblMessage").innerHTML = msg;

        //dls 070416 - another aattempt to stop them moving before update is complete
        if (document.getElementById("lnkPrevious") != null) {
            document.getElementById("lnkPrevious").disabled = false;
        }

        if (document.getElementById("lnkNext") != null) {
            document.getElementById("lnkNext").disabled = false;
        }

        //Jerry 2013-11-01 add
        switch (update) {
            case "AdjudicateFrame":
            //case "InvestigateFrame":
            case "VerifyFrame":
            case "RejectFrame":
            case "ReturnToNatis":
            case "ReturnToNatisAdj":
                var nextHref = $("#lnkNext").attr("href");
                window.location = nextHref;
                break;
        }
    }
}

function SucceededCallback(result)
{
    if (result != "") {
        alert(result);
        document.getElementById("btnAccept").disabled = false;
        document.getElementById("lnkPrevious").disabled = false;
        document.getElementById("lnkNext").disabled = false;
    }
    if (result == "Frame cannot be adjudicated - there is no corresponding offence code for the values on the frame") {
        document.getElementById("lblMessage").innerHTML = lblMessage;
        document.getElementById("btnAccept").disabled = true;
    }
}

function formatStr() {
    var ary = [];
    for (i = 1; i < arguments.length; i++) {
        ary.push(arguments[i]);
    }
    return arguments[0].replace(/\{(\d+)\}/g, function (m, i) {
        return ary[i];
    });
}

