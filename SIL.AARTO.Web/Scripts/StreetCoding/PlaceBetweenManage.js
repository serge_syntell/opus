﻿/// <reference path="../jquery-1.4.4-vsdoc.js" />

// Validate selected two streets is whether equal
$.validator.unobtrusive.adapters.add(
    'valuenotequal', ['propertytested'], function (options) {
        options.rules['valuenotequal'] = options.params;
        options.messages['valuenotequal'] = options.message;
    });
$.validator.addMethod("valuenotequal", function (value, element, params) {
    var parts = element.name.split(".");
    var prefix = "";
    if (parts.length > 1)
        prefix = parts[0] + ".";
    var anotherStValue = $('select[name="' + prefix + params.propertytested + '"]').val();
    if (!value || !anotherStValue) {
        alert(value + '-' + prefix + '-' + params.propertytested);
        return true;
    }
    return anotherStValue != value;
});

String.prototype.format = function () {
    var args = arguments;
    return this.replace(/\{(\d+)\}/g,
        function (m, i) {
            return args[i];
        });
}

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}

var placebetw_strTip = null;
$(document).ready(function () {

    var search_autId = getQueryString('a');
    var search_subId = getQueryString('sb');
    var search_st1Id = getQueryString('st1');
    var search_st2Id = getQueryString('st2');
    var search_st3Id = getQueryString('st3');
    if (search_autId) {
        pb_autId = search_autId;
    }

    if (pb_autId) {
        $("#SearchAutIntNo").val(pb_autId);
        setSubVal(pb_autId, search_subId, "#SearchLoSuIntNo", 0, "", search_st1Id, "#SearchStreet1", search_st2Id, "#SearchStreet2", search_st3Id, "#SearchStreet3");
        $("#AutIntNo").val(pb_autId);
        setSubVal(pb_autId, 0, "#LoSuIntNo", 0, "#CrtIntNo", 0, "#PBStreet1", 0, "#PBStreet2", 0, "#PBStreet3");
    }

    $('#SearchAutIntNo').change(function () {
        var autId = $("#SearchAutIntNo").val();
        var subId = $("#SearchLoSuIntNo").val();
        if (autId == 0) {
            clearSearchDpdVal();
            return;
        }
        setSubVal(autId, 0, "#SearchLoSuIntNo", 0, "", 0, "#SearchStreet1", 0, "#SearchStreet2", 0, "#SearchStreet3");
    });

    $('#SearchLoSuIntNo').change(function () {
        var autId = $("#SearchAutIntNo").val();
        var subId = $("#SearchLoSuIntNo").val();
        if (subId == 0) {
            clearStDpd("#SearchStreet1", "#SearchStreet2", "#SearchStreet3");
            return;
        }
        setStVal(autId, subId, 0, "#SearchStreet1", 0, "#SearchStreet2", 0, "#SearchStreet3");
    });

    $('#AutIntNo').change(function () {
        var autId = $("#AutIntNo").val();
        var subId = $("#LoSuIntNo").val();
        if (autId == 0) {
            clearEditDpdVal();
            return;
        }
        setSubVal(autId, 0, "#LoSuIntNo", 0, "#CrtIntNo", 0, "#PBStreet1", 0, "#PBStreet2", 0, "#PBStreet3");
    });

    $('#LoSuIntNo').change(function () {
        var autId = $("#AutIntNo").val();
        var subId = $("#LoSuIntNo").val();
        if (subId == 0) {
            clearStDpd("#PBStreet1", "#PBStreet2", "#PBStreet3");
            return;
        }
        setStVal(autId, subId, 0, "#PBStreet1", 0, "#PBStreet2", 0, "#PBStreet3");
    });

    $("#btnAddPB").click(function () {
        if ($("#tblEditPB").css("display") == 'none') {
            $(".error_fontsize12").show();
            $("#tblEditPB").show();
        } else {
            if ($("#hidPBId").val() == '0') {
                $(".error_fontsize12").hide();
                $("#tblEditPB").hide();
            } else {
                $("#errorMsg").text('');
                $("#AutIntNo").val(pb_autId);
                if (pb_autId != 0) setSubVal(pb_autId, 0, "#LoSuIntNo", 0, "#CrtIntNo", 0, "#PBStreet1", 0, "#PBStreet2", 0, "#PBStreet3");
                clearEditDpdVal();
                $("#hidPBId").val("0");
                $("#hidAutId").val("0");
                $("#hidSubId").val("0");
                $("#hidCrtId").val("0");
                $("#hidSt1Id, #hidSt2Id, #hidSt3Id").val("0");
                $("#PBGPSXCoord, #PBGPSYCoord").val("");
            }
        }
    });

    $("#btnSearch").click(function () {
        window.location.href = _ROOTPATH + '/StreetCoding/PlaceBetweenManage?s=1&a=' +
            $("#SearchAutIntNo").val() + '&sb=' + $("#SearchLoSuIntNo").val() + '&st1=' + $("#SearchStreet1").val() + '&st2=' + $("#SearchStreet2").val() + '&st3=' + $("#SearchStreet3").val();
    });

    $("#PBStreet1, #PBStreet2, #PBStreet3").change(function () {
        $("#StSelectTip").text('');
        $("form").valid();
        var losu = $("#LoSuIntNo").val();
        var st1 = $("#PBStreet1").val();
        var st2 = $("#PBStreet2").val();
        var st3 = $("#PBStreet3").val();
        if (st1 == 0 || st2 == 0 || st3 == 0 || st1 == st2 || st1 == st3 || st2 == st3) return;
        if (placebetw_strTip != null) {
            $("#StSelectTip").text(placebetw_strTip.format($("#PBStreet1 option:selected").text(), $("#PBStreet2 option:selected").text(), $("#PBStreet3 option:selected").text(), $("#LoSuIntNo option:selected").text()));
            return;
        }
        $.post(_ROOTPATH + "/StreetCoding/GetBetwStSelectTip",
            {
                st1: st1,
                st2: st2,
                st3: st3
            },
            function (message) {
                if (message.Status == true) {
                    placebetw_strTip = message.Text;
                    $("#StSelectTip").text(placebetw_strTip.format($("#PBStreet1 option:selected").text(), $("#PBStreet2 option:selected").text(), $("#PBStreet3 option:selected").text(), $("#LoSuIntNo option:selected").text()));
                }
            }, 'json');
    });

    $("#btnSavePB").click(function () {
        $("#errorMsg").text('');
        var st1 = $("#PBStreet1").val();
        var st2 = $("#PBStreet2").val();
        var st3 = $("#PBStreet3").val();

        if ($("form").valid()) {
            $.post(_ROOTPATH + "/StreetCoding/SavePB",
            {
                pbId: $("#hidPBId").val(),
                sub: $("#LoSuIntNo").val(),
                st1: st1,
                st2: st2,
                st3: st3,
                xCoord: $("#PBGPSXCoord").val(),
                yCoord: $("#PBGPSYCoord").val(),
                crtId: $("#CrtIntNo").val(),
                st1Before: $("#hidSt1Id").val(),
                st2Before: $("#hidSt2Id").val(),
                st3Before: $("#hidSt3Id").val(),
                autIntNo: $("#AutIntNo").val()//2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            },
            function (message) {
                if (message.Status == true) {
                    window.location.href = _ROOTPATH + '/StreetCoding/PlaceBetweenManage?s=1&a=' +
            $("#SearchAutIntNo").val() + '&sb=' + $("#SearchLoSuIntNo").val() + '&st1=' + $("#SearchStreet1").val() + '&st2=' + $("#SearchStreet2").val() + '&st3=' + $("#SearchStreet3").val();
                } else {
                    $("#errorMsg").text(message.Text);
                }
            }, 'json');
        }
    });

});

function clearStDpd(st1Obj, st2Obj, st3Obj) {
    var seleStr = $(st1Obj).find("option").eq(0);
    $(st1Obj + "," + st2Obj + "," + st3Obj).find("option").remove();
    seleStr.appendTo($(st1Obj + "," + st2Obj + "," + st3Obj));
}

function clearSearchDpdVal() {
    var seleStr = $("#SearchLoSuIntNo").find("option").eq(0);
    $("#SearchLoSuIntNo").find("option").remove();
    seleStr.appendTo($("#SearchLoSuIntNo"));
    seleStr = $("#SearchStreet1").find("option").eq(0);
    $("#SearchStreet1, #SearchStreet2, #SearchStreet3").find("option").remove();
    seleStr.appendTo($("#SearchStreet1, #SearchStreet2, #SearchStreet3"));
}

function clearEditDpdVal() {
    var seleStr = $("#LoSuIntNo").find("option").eq(0);
    $("#LoSuIntNo").find("option").remove();
    seleStr.appendTo($("#LoSuIntNo"));
    seleStr = $("#CrtIntNo").find("option").eq(0);
    $("#CrtIntNo").find("option").remove();
    seleStr.appendTo($("#CrtIntNo"));
    seleStr = $("#PBStreet1").find("option").eq(0);
    $("#PBStreet1, #PBStreet2, #PBStreet3").find("option").remove();
    seleStr.appendTo($("#PBStreet1, #PBStreet2, #PBStreet3"));
}

function setSubVal(autId, subId, subObjId, crtId, crtObjId, st1, st1ObjId, st2, st2ObjId, st3, st3ObjId) {
    $.post(_ROOTPATH + "/StreetCoding/GetSubByAutIntNo",
        {
            autId: autId
        },
        function (data) {
            if (data != null) {
                $(subObjId).find("option").remove();
                $.each(data, function (i, item) {
                    $("<option></option>").val(item["Value"]).text(item["Text"]).appendTo($(subObjId));
                });
                if (subId != 0) $(subObjId).val(subId);
                if (crtObjId != "")
                    setCrtVal(autId, subId, crtId, crtObjId, st1, st1ObjId, st2, st2ObjId, st3, st3ObjId);
                else
                    setStVal(autId, subId, st1, st1ObjId, st2, st2ObjId, st3, st3ObjId);
            }
        }, 'json');
}

function setCrtVal(autId, subId, crtId, crtObjId, st1, st1ObjId, st2, st2ObjId, st3, st3ObjId) {
    $.post(_ROOTPATH + "/Admin/GetCrtByAutIntNo",
        {
            autId: autId
        },
        function (data) {
            if (data != null) {
                $(crtObjId).find("option").remove();
                $.each(data, function (i, item) {
                    $("<option></option>").val(item["Value"]).text(item["Text"]).appendTo($(crtObjId));
                });
                if (crtId != 0) $(crtObjId).val(crtId);
                setStVal(autId, subId, st1, st1ObjId, st2, st2ObjId, st3, st3ObjId);
            }
        }, 'json');
}

function setStVal(autId, subId, st1, st1ObjId, st2, st2ObjId, st3, st3ObjId) {
    if (subId == 0 || isNaN(subId) || !subId) {
        clearStDpd(st1ObjId, st2ObjId, st3ObjId);
        return;
    }
    $.post(_ROOTPATH + "/StreetCoding/GetStByAutLoSu",
        {
            autId: autId,
            subId: subId
        },
        function (data) {
            if (data != null) {
                $(st1ObjId + ", " + st2ObjId + ", " + st3ObjId).find("option").remove();
                $.each(data, function (i, item) {
                    $("<option></option>").val(item["Value"]).text(item["Text"]).appendTo($(st1ObjId + ", " + st2ObjId + ", " + st3ObjId));
                });
                $(st1ObjId).val(st1);
                $(st2ObjId).val(st2);
                $(st3ObjId).val(st3);
            }
        }, 'json');
}

function EditPB(pbId) {
    if (pbId != 0) {
        $.post(_ROOTPATH + "/StreetCoding/GetPB",
        {
            pbId: pbId
        },
        function (message) {
            if (message.Status == true) {
                var stArr = message.Text.toString().split(',');
                setSubVal(stArr[1], stArr[2], "#LoSuIntNo", stArr[3], "#CrtIntNo", stArr[4], "#PBStreet1", stArr[5], "#PBStreet2", stArr[6], "#PBStreet3");
                $("#hidPBId").val(stArr[0]);
                $("#AutIntNo, #hidAutId").val(stArr[1]);
                $("#hidSubId").val(stArr[2]);
                $("#hidCrtId").val(stArr[3]);
                $("#hidSt1Id").val(stArr[4]);
                $("#hidSt2Id").val(stArr[5]);
                $("#hidSt3Id").val(stArr[6]);
                $("#PBGPSXCoord").val(stArr[7]);
                $("#PBGPSYCoord").val(stArr[8]);
                $("#tblEditPB").show();
            }
        }, 'json');
    }
}

function DelePB(pbId) {
    $("#errorMsg").text('');
    if (!isNaN(pbId)) {
        $.post(_ROOTPATH + "/StreetCoding/DelePB",
        {
            pbId: pbId,
            autIntNo: $("#SearchAutIntNo").val()//2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        },
        function (message) {
            if (message.Status == true) {
                window.location.href = _ROOTPATH + '/StreetCoding/PlaceBetweenManage?s=1&a=' +
            $("#SearchAutIntNo").val() + '&sb=' + $("#SearchLoSuIntNo").val() + '&st1=' + $("#SearchStreet1").val() + '&st2=' + $("#SearchStreet2").val() + '&st3=' + $("#SearchStreet3").val();
            } else {
                $("#errorMsg").text(message.Text);
            }
        }, 'json');
    }
}