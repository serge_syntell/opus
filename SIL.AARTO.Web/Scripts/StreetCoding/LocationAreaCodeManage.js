﻿/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}

var ac_validator;
$(document).ready(function () {

    ac_validator = $("form").validate();
    var searchKey = getQueryString('s');
    if (searchKey) $("#SearchKey").val(searchKey);

    $("#btnAddLAC").click(function () {
        if ($("#tbLACEdit").css("display") == 'none') {
            $("#errorMsg").show();
            $("#tbLACEdit").show();
        } else {
            if ($("#hidLacCode").val() == '') {
                $("#errorMsg").hide();
                $("#tbLACEdit").hide();
            } else {
                $("#errorMsg").text('');
                $("#hidLacCode").val("");
                $("#txtLacCode").val("");
                $("#txtLacDescr").val("");
            }
        }
    });

    $("#btnSearch").click(function () {
        window.location.href = _ROOTPATH + '/StreetCoding/LocationAreaCodeManage?s=' +
            $("#SearchKey").val();
    });

    $("#btnSaveLAC").click(function () {
        $("#errorMsg").text('');
        if ($("form").valid()) {
            $.post(_ROOTPATH + "/StreetCoding/SaveLAC",
                {
                    lacCodeBefore: $("#hidLacCode").val(),
                    lacCodeNow: $("#txtLacCode").val(),
                    lacDescr: $("#txtLacDescr").val()
                },
                function (message) {
                    if (message.Status == true) {
                        window.location.href = _ROOTPATH + '/StreetCoding/LocationAreaCodeManage?s=' +
                            $("#SearchKey").val();
                    } else {
                        ac_validator.showErrors({ "LACCode": message.Text });
                    }
                });
        }
    });


});


function EditLAC(lacCode) {
    $("#errorMsg").text('');
    if (lacCode != '') {
        $.post(_ROOTPATH + "/StreetCoding/GetLAC",
        {
            lacCode: lacCode
        },
        function (message) {
            if (message.Status == true) {
                $("#hidLacCode").val(lacCode);
                $("#txtLacCode").val(lacCode);
                $("#txtLacDescr").val(message.Text.toString());
                $("#tbLACEdit").show();
            }
        }, 'json');  
    }
}

function DeleLAC(lacCode) {
    $("#errorMsg").text('');
    if (lacCode != '') {
        $.post(_ROOTPATH + "/StreetCoding/DeleLAC",
        {
            lacCode: lacCode
        },
        function (message) {
            if (message.Status == true) {
                window.location.href = _ROOTPATH + '/StreetCoding/LocationAreaCodeManage?s=' +
                    $("#SearchKey").val();
            } else {
                $("#errorMsg").text(message.Text);
            }
        }, 'json');
    }
}