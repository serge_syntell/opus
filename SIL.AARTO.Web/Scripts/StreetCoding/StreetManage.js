﻿/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}

$(document).ready(function () {

    var search_autId = getQueryString('a');
    var search_subId = getQueryString('sb');
    var search_key = getQueryString('s');
    if (search_autId) {
        st_autId = search_autId;
        $("#SearchKey").val(search_key);
    }

    if (st_autId) {
        $("#SearchAutIntNo").val(st_autId);
        setSubDpdVal(st_autId, "#SearchSubIntNo", search_subId);
        $("#AutIntNo").val(st_autId);
        setSubDpdVal(st_autId, "#LoSuIntNo", 0);
    }

    $('#SearchAutIntNo').change(function () {
        var autId = $("#SearchAutIntNo").val();
        if (autId == 0) {
            var seleStr = $("#SearchLoSuIntNo").find("option").eq(0);
            $("#SearchLoSuIntNo").find("option").remove();
            seleStr.appendTo($("#SearchLoSuIntNo"));
            return;
        }
        setSubDpdVal(autId, "#SearchSubIntNo", 0)
    });

    $('#AutIntNo').change(function () {
        var autId = $("#AutIntNo").val();
        if (autId == 0) {
            var seleStr = $("#LoSuIntNo").find("option").eq(0);
            $("#LoSuIntNo").find("option").remove();
            seleStr.appendTo($("#LoSuIntNo"));
            return;
        }
        setSubDpdVal(autId, "#LoSuIntNo", 0)
    });

    $("#btnAddSt").click(function () {
        if ($("#tbStreetEdit").css("display") == 'none') {
            $("#errorMsg").show();
            $("#tbStreetEdit").show();
        } else {
            if ($("#hidStId").val() == '0') {
                $("#errorMsg").hide();
                $("#tbStreetEdit").hide();
            } else {
                $("#errorMsg").text('');
                var seleStr = $("#LoSuIntNo").find("option").eq(0);
                $("#LoSuIntNo").find("option").remove();
                seleStr.appendTo($("#LoSuIntNo"));
                $("#hidAutId").val("0");
                $("#hidSubId").val("0");
                $("#hidStId").val("0");
                $("#AutIntNo").val(st_autId);
                if (st_autId != 0) setSubDpdVal(st_autId, "#LoSuIntNo", 0)
                $("#txtStName").val('');
            }
        }
    });

    $("#btnSearch").click(function () {
        window.location.href = _ROOTPATH + '/StreetCoding/StreetManage?a=' +
            $("#SearchAutIntNo").val() + '&sb=' + $("#SearchSubIntNo").val() + '&s=' + $("#SearchKey").val();
    });

    $("#btnSaveSt").click(function () {
        $("#errorMsg").text('');
        if ($("form").valid()) {
            var stIntNo = $("#hidStId").val() == '' ? 0 : $("#hidStId").val();
            var autIntNoBefore = $("#hidAutId").val() == '' ? 0 : $("#hidAutId").val();
            var subIntNoBefore = $("#hidSubId").val() == '' ? 0 : $("#hidSubId").val();

            $.post(_ROOTPATH + "/StreetCoding/SaveStreet",
            {
                autIntNo: $("#AutIntNo").val(),
                subIntNo: $("#LoSuIntNo").val(),
                stIntNo: stIntNo,
                stName: $("#txtStName").val(),
                autIntNoBefore: autIntNoBefore,
                subIntNoBefore: subIntNoBefore
            },
            function (message) {
                if (message.Status == true) {
                    window.location.href = _ROOTPATH + '/StreetCoding/StreetManage?a=' +
                        $("#SearchAutIntNo").val() + '&s=' + $("#SearchKey").val();
                } else {
                    $("#errorMsg").text(message.Text);
                }
            }, 'json');
        }
    });

});


function setSubDpdVal(autId, subObjId, subId) {
    $.post(_ROOTPATH + "/StreetCoding/GetSubByAutIntNo",
        {
            autId: autId
        },
        function (data) {
            if (data != null) {
                $(subObjId).find("option").remove();
                $.each(data, function (i, item) {
                    $("<option></option>").val(item["Value"]).text(item["Text"]).appendTo($(subObjId));
                });
                if (subId != 0) $(subObjId).val(subId);
            }
        }, 'json');
}

function EditStreet(subId, stId) {
    if (stId != '' && stId != 0) {

        $.post(_ROOTPATH + "/StreetCoding/GetStreet",
        {
            subId: subId,
            stId: stId
        },
        function (message) {
            if (message.Status == true) {
                var stArr = message.Text.toString().split(',');
                $("#AutIntNo").val(stArr[0]);
                setSubDpdVal(stArr[0], "#LoSuIntNo", stArr[1]);
                $("#txtStName").val(stArr[2]);
                $("#hidStId").val(stId);
                $("#hidAutId").val(stArr[0]);
                $("#hidSubId").val(stArr[1]);
                $("#tbStreetEdit").show();
            }
        }, 'json');
        
    }
}

function DeleSt(subId, stId) {
    $("#errorMsg").text('');
    if (!isNaN(stId)) {
        $.post(_ROOTPATH + "/StreetCoding/DeleSt",
        {
            loSuIntNo: subId,
            strIntNo: stId,
            autIntNo: $("#SearchAutIntNo").val()//2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        },
        function (message) {
            if (message.Status == true) {
                window.location.href = _ROOTPATH + '/StreetCoding/StreetManage?a=' +
                    $("#SearchAutIntNo").val() + '&s=' + $("#SearchKey").val();
            } else {
                $("#errorMsg").text(message.Text);
            }
        }, 'json');
    }
}