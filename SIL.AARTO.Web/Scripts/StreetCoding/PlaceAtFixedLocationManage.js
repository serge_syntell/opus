﻿/// <reference path="../jquery-1.4.4-vsdoc.js" />

String.prototype.format = function () {
    var args = arguments;
    return this.replace(/\{(\d+)\}/g,
        function (m, i) {
            return args[i];
        });
}

String.prototype.trim = function () {  
    return this.replace(/(^\s*)|(\s*$)/g, "");
}

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}

var fixloc_strTip = null;
$(document).ready(function () {

    var search_autId = getQueryString('a');
    var search_subId = getQueryString('sb');
    var search_stId = getQueryString('st');
    var search_key = getQueryString('s');
    if (search_autId) {
        paf_autId = search_autId;
        $("#SearchKey").val(search_key);
    }

    if (paf_autId) {
        $("#SearchAutIntNo").val(paf_autId);
        setSubVal(paf_autId, search_subId, "#SearchLoSuIntNo", 0, "", search_stId, "#SearchStreet", null);
        $("#AutIntNo").val(paf_autId);
        setSubVal(paf_autId, 0, "#LoSuIntNo", 0, "#CrtIntNo", 0, "#PAFLStreet1", null);
    }

    $('#SearchAutIntNo').change(function () {
        var autId = $("#SearchAutIntNo").val();
        var subId = $("#SearchLoSuIntNo").val();
        if (autId == 0) {
            clearSearchDpdVal();
            return;
        }
        setSubVal(autId, 0, "#SearchLoSuIntNo", 0, "", 0, "#SearchStreet", null);
    });

    $('#SearchLoSuIntNo').change(function () {
        var autId = $("#SearchAutIntNo").val();
        var subId = $("#SearchLoSuIntNo").val();
        if (subId == 0) {
            clearStDpd("#SearchStreet");
            return;
        }
        setStVal(autId, subId, 0, "#SearchStreet", null);
    });

    $('#AutIntNo').change(function () {
        var autId = $("#AutIntNo").val();
        var subId = $("#LoSuIntNo").val();
        if (autId == 0) {
            clearEditDpdVal();
            return;
        }
        setSubVal(autId, 0, "#LoSuIntNo", 0, "#CrtIntNo", 0, "#PAFLStreet1", showFixLocTip);
    });

    $('#LoSuIntNo').change(function () {
        var autId = $("#AutIntNo").val();
        var subId = $("#LoSuIntNo").val();
        if (subId == 0) {
            clearStDpd("#PAFLStreet1");
            return;
        }
        setStVal(autId, subId, 0, "#PAFLStreet1", showFixLocTip);
    });

    $("#btnAddFixLoc").click(function () {
        if ($("#tblEditFixLoc").css("display") == 'none') {
            $(".error_fontsize12").show();
            $("#tblEditFixLoc").show();
        } else {
            if ($("#hidFLId").val() == '0') {
                $(".error_fontsize12").hide();
                $("#tblEditFixLoc").hide();
            } else {
                $("#errorMsg").text('');
                $("#AutIntNo").val(paf_autId);
                if (paf_autId != 0) setSubVal(paf_autId, 0, "#LoSuIntNo", 0, "#CrtIntNo", 0, "#PAFLStreet1", null);
                clearEditDpdVal();
                $("#PAFLocation").val('');
                $("#hidFLId").val("0");
                $("#hidAutId").val("0");
                $("#hidSubId").val("0");
                $("#hidCrtId").val("0");
                $("#hidStId").val("0");
                $("#hidLocName").val('');
                $("#PAFLGPSXCoord, #PAFLGPSYCoord").val("");
            }
        }
    });

    $("#btnSearch").click(function () {
        window.location.href = _ROOTPATH + '/StreetCoding/PlaceAtFixedLocationManage?s=' + $("#SearchKey").val() + '&a=' +
            $("#SearchAutIntNo").val() + '&sb=' + $("#SearchLoSuIntNo").val() + '&st=' + $("#SearchStreet").val();
    });

    // Validate form and show selected fixed location tip
    $('#CrtIntNo, #PAFLStreet1').change(function (event) {
        showFixLocTip();
    });
    $("#PAFLocation").blur(function () {
        showFixLocTip();
    });

    $("#btnSaveFixLoc").click(function () {
        $("#errorMsg").text('');
        var sub = $("#LoSuIntNo").val();
        var st = $("#PAFLStreet1").val();
        var loc = $("#PAFLocation").val().trim();
        if ($("form").valid()) {
            $.post(_ROOTPATH + "/StreetCoding/SaveFixLoc",
            {
                flId: $("#hidFLId").val(),
                subId: sub,
                stId: st,
                locName: loc,
                xCoord: $("#PAFLGPSXCoord").val(),
                yCoord: $("#PAFLGPSYCoord").val(),
                crtId: $("#CrtIntNo").val(),
                subBefore: $("#hidSubId").val(),
                stBefore: $("#hidStId").val(),
                locBefore: $("#hidLocName").val(),
                autIntNo: $("#SearchAutIntNo").val()//2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            },
            function (message) {
                if (message.Status == true) {
                    window.location.href = _ROOTPATH + '/StreetCoding/PlaceAtFixedLocationManage?s=' + $("#SearchKey").val() + '&a=' +
            $("#SearchAutIntNo").val() + '&sb=' + $("#SearchLoSuIntNo").val() + '&st=' + $("#SearchStreet").val();
                } else {
                    $("#errorMsg").text(message.Text);
                }
            }, 'json');
        }
    });

});

function clearStDpd(st1Obj) {
    var seleStr = $(st1Obj).find("option").eq(0);
    $(st1Obj).find("option").remove();
    seleStr.appendTo($(st1Obj));
}

function clearSearchDpdVal() {
    var seleStr = $("#SearchLoSuIntNo").find("option").eq(0);
    $("#SearchLoSuIntNo").find("option").remove();
    seleStr.appendTo($("#SearchLoSuIntNo"));
    seleStr = $("#SearchStreet").find("option").eq(0);
    $("#SearchStreet").find("option").remove();
    seleStr.appendTo($("#SearchStreet"));
}

function clearEditDpdVal() {
    var seleStr = $("#LoSuIntNo").find("option").eq(0);
    $("#LoSuIntNo").find("option").remove();
    seleStr.appendTo($("#LoSuIntNo"));
    seleStr = $("#CrtIntNo").find("option").eq(0);
    $("#CrtIntNo").find("option").remove();
    seleStr.appendTo($("#CrtIntNo"));
    seleStr = $("#PAFLStreet1").find("option").eq(0);
    $("#PAFLStreet1").find("option").remove();
    seleStr.appendTo($("#PAFLStreet1"));
}

function setSubVal(autId, subId, subObjId, crtId, crtObjId, st, stObjId, callbackFuc) {
    $.post(_ROOTPATH + "/StreetCoding/GetSubByAutIntNo",
        {
            autId: autId
        },
        function (data) {
            if (data != null) {
                $(subObjId).find("option").remove();
                $.each(data, function (i, item) {
                    $("<option></option>").val(item["Value"]).text(item["Text"]).appendTo($(subObjId));
                });
                if (subId != 0) $(subObjId).val(subId);
                if (crtObjId != "")
                    setCrtVal(autId, subId, crtId, crtObjId, st, stObjId, callbackFuc);
                else
                    setStVal(autId, subId, st, stObjId, callbackFuc);
            }
        }, 'json');
}

function setCrtVal(autId, subId, crtId, crtObjId, st, stObjId, callbackFuc) {
    $.post(_ROOTPATH + "/Admin/GetCrtByAutIntNo",
        {
            autId: autId
        },
        function (data) {
            if (data != null) {
                $(crtObjId).find("option").remove();
                $.each(data, function (i, item) {
                    $("<option></option>").val(item["Value"]).text(item["Text"]).appendTo($(crtObjId));
                });
                if (crtId != 0) $(crtObjId).val(crtId);
                setStVal(autId, subId, st, stObjId, callbackFuc);
            }
        }, 'json');
}

function setStVal(autId, subId, st, stObjId, callbackFuc) {
    if (subId == 0 || isNaN(subId) || !subId) {
        var seleStr = $(stObjId).find("option").eq(0);
        $(stObjId).find("option").remove();
        seleStr.appendTo($(stObjId));
        if (callbackFuc != null) callbackFuc();
        return;
    }
    $.post(_ROOTPATH + "/StreetCoding/GetStByAutLoSu",
        {
            autId: autId,
            subId: subId
        },
        function (data) {
            if (data != null) {
                $(stObjId).find("option").remove();
                $.each(data, function (i, item) {
                    $("<option></option>").val(item["Value"]).text(item["Text"]).appendTo($(stObjId));
                });
                if (st != 0) {
                    $(stObjId).val(st);
                }
                if (callbackFuc != null) callbackFuc();
            }
        }, 'json');
}

function showFixLocTip() {
    $("#FixLocSelectTip").text('');
    var aut = $("#AutIntNo").val();
    var sub = $("#LoSuIntNo").val();
    var crt = $("#CrtIntNo").val();
    var st = $("#PAFLStreet1").val();
    var loc = $("#PAFLocation").val().trim();
    if (aut == 0 || sub == 0 || st == 0 || crt == 0 || loc == '') return;
    if (fixloc_strTip != null) {
        $("#FixLocSelectTip").text(fixloc_strTip.format(loc, $("#PAFLStreet1 option:selected").text(), $("#LoSuIntNo option:selected").text()));
        return;
    }
    $.post(_ROOTPATH + "/StreetCoding/GetFixLocSelectTip",
        {
            stId: st,
            locName: loc
        },
        function (message) {
            if (message.Status == true) {
                fixloc_strTip = message.Text;
                $("#FixLocSelectTip").text(fixloc_strTip.format(loc, $("#PAFLStreet1 option:selected").text(), $("#LoSuIntNo option:selected").text()));
            }
        }, 'json');
}

function EditFixLoc(flId) {
    if (flId != 0) {
        $.post(_ROOTPATH + "/StreetCoding/GetFixLoc",
        {
            flId: flId
        },
        function (message) {
            if (message.Status == true) {
                var stArr = message.Text.toString().split(',');
                setSubVal(stArr[1], stArr[2], "#LoSuIntNo", stArr[3], "#CrtIntNo", stArr[4], "#PAFLStreet1", null);
                $("#hidFLId").val(stArr[0]);
                $("#AutIntNo, #hidAutId").val(stArr[1]);
                $("#hidSubId").val(stArr[2]);
                $("#hidCrtId").val(stArr[3]);
                $("#hidStId").val(stArr[4]);
                $("#PAFLocation, #hidLocName").val(stArr[5]);
                $("#PAFLGPSXCoord").val(stArr[6]);
                $("#PAFLGPSYCoord").val(stArr[7]);
                $("#tblEditFixLoc").show();
            }
        }, 'json');
    }
}

function DeleFixLoc(flId) {
    $("#errorMsg").text('');
    if (!isNaN(flId)) {
        $.post(_ROOTPATH + "/StreetCoding/DeleFixLoc",
        {
            flId: flId,
            autIntNo: $("#SearchAutIntNo").val()//2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        },
        function (message) {
            if (message.Status == true) {
                window.location.href = _ROOTPATH + '/StreetCoding/PlaceAtFixedLocationManage?s=' + $("#SearchKey").val() + '&a=' +
            $("#SearchAutIntNo").val() + '&sb=' + $("#SearchLoSuIntNo").val() + '&st=' + $("#SearchStreet").val();
            } else {
                $("#errorMsg").text(message.Text);
            }
        }, 'json');
    }
}