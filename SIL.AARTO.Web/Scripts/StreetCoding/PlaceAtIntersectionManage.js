﻿/// <reference path="../jquery-1.4.4-vsdoc.js" />

// Validate selected two streets is whether equal
$.validator.unobtrusive.adapters.add(
    'valuenotequal', ['propertytested'], function (options) {
        options.rules['valuenotequal'] = options.params;
        options.messages['valuenotequal'] = options.message;
});
$.validator.addMethod("valuenotequal", function (value, element, params) {
    var parts = element.name.split(".");
    var prefix = "";
    if (parts.length > 1)
        prefix = parts[0] + ".";
    var anotherStValue = $('select[name="' + prefix + params.propertytested + '"]').val();
    if (!value || !anotherStValue) {
        alert(value + '-' + prefix + '-' + params.propertytested);
        return true;
    }
    return anotherStValue != value;
});

String.prototype.format = function () {
    var args = arguments;
    return this.replace(/\{(\d+)\}/g,
        function (m, i) {
            return args[i];
        });
}

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}

var intersect_strTip = null;
$(document).ready(function () {

    var search_autId = getQueryString('a');
    var search_subId = getQueryString('sb');
    var search_st1Id = getQueryString('st1');
    var search_st2Id = getQueryString('st2');
    if (search_autId)
        pai_autId = search_autId;

    if (pai_autId) {
        $("#SearchAutIntNo").val(pai_autId);
        setSubVal(pai_autId, search_subId, "#SearchLoSuIntNo", 0, "", search_st1Id, "#SearchStreet1", search_st2Id, "#SearchStreet2");
        $("#AutIntNo").val(pai_autId);
        setSubVal(pai_autId, 0, "#LoSuIntNo", 0, "#CrtIntNo", 0, "#PAIStreet1", 0, "#PAIStreet2");
    }

    $('#SearchAutIntNo').change(function () {
        var autId = $("#SearchAutIntNo").val();
        var subId = $("#SearchLoSuIntNo").val();
        if (autId == 0) {
            clearSearchDpdVal();
            return;
        }
        setSubVal(autId, 0, "#SearchLoSuIntNo", 0, "", 0, "#SearchStreet1", 0, "#SearchStreet2");
    });

    $('#SearchLoSuIntNo').change(function () {
        var autId = $("#SearchAutIntNo").val();
        var subId = $("#SearchLoSuIntNo").val();
        if (subId == 0) {
            clearStDpd("#SearchStreet1", "#SearchStreet2");
            return;
        }
        setStVal(autId, subId, 0, "#SearchStreet1", 0, "#SearchStreet2");
    });

    $('#AutIntNo').change(function () {
        var autId = $("#AutIntNo").val();
        var subId = $("#LoSuIntNo").val();
        if (autId == 0) {
            clearEditDpdVal();
            return;
        }
        setSubVal(autId, 0, "#LoSuIntNo", 0, "#CrtIntNo", 0, "#PAIStreet1", 0, "#PAIStreet2");
    });

    $('#LoSuIntNo').change(function () {
        var autId = $("#AutIntNo").val();
        var subId = $("#LoSuIntNo").val();
        if (subId == 0) {
            clearStDpd("#PAIStreet1", "#PAIStreet2");
            return;
        }
        setStVal(autId, subId, 0, "#PAIStreet1", 0, "#PAIStreet2");
    });

    $("#btnAddPAI").click(function () {
        if ($("#tblEditPAI").css("display") == 'none') {
            $(".error_fontsize12").show();
            $("#tblEditPAI").show();
        } else {
            if ($("#hidPAIId").val() == '0') {
                $(".error_fontsize12").hide();
                $("#tblEditPAI").hide();
            } else {
                $("#errorMsg").text('');
                $("#AutIntNo").val(pai_autId);
                if (pai_autId != 0) setSubVal(pai_autId, 0, "#LoSuIntNo", 0, "#CrtIntNo", 0, "#PAIStreet1", 0, "#PAIStreet2");
                clearEditDpdVal();
                $("#hidPAIId").val("0");
                $("#hidAutId").val("0");
                $("#hidSubId").val("0");
                $("#hidCrtId").val("0");
                $("#hidSt1Id, #hidSt2Id").val("0");
                $("#PAIGPSXCoord, #PAIGPSYCoord").val("");
            }
        }
    });

    $("#btnSearch").click(function () {
        window.location.href = _ROOTPATH + '/StreetCoding/PlaceAtIntersectionManage?s=1&a=' +
            $("#SearchAutIntNo").val() + '&sb=' + $("#SearchLoSuIntNo").val() + '&st1=' + $("#SearchStreet1").val() + '&st2=' + $("#SearchStreet2").val();
    });

    $("#PAIStreet1, #PAIStreet2").change(function () {
        $("form").valid();
        $("#StSelectTip").text('');
        var losu = $("#LoSuIntNo").val();
        var st1 = $("#PAIStreet1").val();
        var st2 = $("#PAIStreet2").val();

        if (st1 == 0 || st2 == 0 || st1 == st2 || losu == 0) return;
        if (intersect_strTip != null) {
            $("#StSelectTip").text(intersect_strTip.format($("#PAIStreet1 option:selected").text(), $("#PAIStreet2 option:selected").text(), $("#LoSuIntNo option:selected").text()));
            return;
        }
        $.post(_ROOTPATH + "/StreetCoding/GetStSelectTip",
            {
                st1: st1,
                st2: st2
            },
            function (message) {
                if (message.Status == true) {
                    intersect_strTip = message.Text;
                    $("#StSelectTip").text(intersect_strTip.format($("#PAIStreet1 option:selected").text(), $("#PAIStreet2 option:selected").text(), $("#LoSuIntNo option:selected").text()));
                }
            }, 'json');
    });

    $("#btnSavePAI").click(function () {
        $("#errorMsg").text('');
        var st1 = $("#PAIStreet1").val();
        var st2 = $("#PAIStreet2").val();

        if ($("form").valid()) {
            $.post(_ROOTPATH + "/StreetCoding/SavePAI",
            {
                paiId: $("#hidPAIId").val(),
                sub: $("#LoSuIntNo").val(),
                st1: st1,
                st2: st2,
                xCoord: $("#PAIGPSXCoord").val(),
                yCoord: $("#PAIGPSYCoord").val(),
                crtId: $("#CrtIntNo").val(),
                st1Before: $("#hidSt1Id").val(),
                st2Before: $("#hidSt2Id").val(),
                autIntNo: $("#AutIntNo").val()//2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            },
            function (message) {
                if (message.Status == true) {
                    window.location.href = _ROOTPATH + '/StreetCoding/PlaceAtIntersectionManage?s=1&a=' +
            $("#SearchAutIntNo").val() + '&sb=' + $("#SearchLoSuIntNo").val() + '&st1=' + $("#SearchStreet1").val() + '&st2=' + $("#SearchStreet2").val();
                } else {
                    $("#errorMsg").text(message.Text);
                }
            }, 'json');
        }
    });

});

function clearStDpd(st1Obj, st2Obj) {
    var seleStr = $(st1Obj).find("option").eq(0);
    $(st1Obj + "," + st2Obj).find("option").remove();
    seleStr.appendTo($(st1Obj + "," + st2Obj));
}

function clearSearchDpdVal() {
    var seleStr = $("#SearchLoSuIntNo").find("option").eq(0);
    $("#SearchLoSuIntNo").find("option").remove();
    seleStr.appendTo($("#SearchLoSuIntNo"));
    seleStr = $("#SearchStreet1").find("option").eq(0);
    $("#SearchStreet1, #SearchStreet2").find("option").remove();
    seleStr.appendTo($("#SearchStreet1, #SearchStreet2"));
}

function clearEditDpdVal() {
    var seleStr = $("#LoSuIntNo").find("option").eq(0);
    $("#LoSuIntNo").find("option").remove();
    seleStr.appendTo($("#LoSuIntNo"));
    seleStr = $("#CrtIntNo").find("option").eq(0);
    $("#CrtIntNo").find("option").remove();
    seleStr.appendTo($("#CrtIntNo"));
    seleStr = $("#PAIStreet1").find("option").eq(0);
    $("#PAIStreet1, #PAIStreet2").find("option").remove();
    seleStr.appendTo($("#PAIStreet1, #PAIStreet2"));
}

function setSubVal(autId, subId, subObjId, crtId, crtObjId, st1, st1ObjId, st2, st2ObjId) {
    $.post(_ROOTPATH + "/StreetCoding/GetSubByAutIntNo",
        {
            autId: autId
        },
        function (data) {
            if (data != null) {
                $(subObjId).find("option").remove();
                $.each(data, function (i, item) {
                    $("<option></option>").val(item["Value"]).text(item["Text"]).appendTo($(subObjId));
                });
                if (subId != 0) $(subObjId).val(subId);
                if (crtObjId != "")
                    setCrtVal(autId, subId, crtId, crtObjId, st1, st1ObjId, st2, st2ObjId);
                else 
                    setStVal(autId, subId, st1, st1ObjId, st2, st2ObjId);
            }
        }, 'json');
}

function setCrtVal(autId, subId, crtId, crtObjId, st1, st1ObjId, st2, st2ObjId) {
    $.post(_ROOTPATH + "/Admin/GetCrtByAutIntNo",
        {
            autId: autId
        },
        function (data) {
            if (data != null) {
                $(crtObjId).find("option").remove();
                $.each(data, function (i, item) {
                    $("<option></option>").val(item["Value"]).text(item["Text"]).appendTo($(crtObjId));
                });
                if (crtId != 0) $(crtObjId).val(crtId);
                setStVal(autId, subId, st1, st1ObjId, st2, st2ObjId);
            }
        }, 'json');
}

function setStVal(autId, subId, st1, st1ObjId, st2, st2ObjId) {
    if (subId == 0 || isNaN(subId) || !subId) {
        clearStDpd(st1ObjId, st2ObjId);
        return;
    }
    $.post(_ROOTPATH + "/StreetCoding/GetStByAutLoSu",
        {
            autId: autId,
            subId: subId
        },
        function (data) {
            if (data != null) {
                $(st1ObjId + ", " + st2ObjId).find("option").remove();
                $.each(data, function (i, item) {
                    $("<option></option>").val(item["Value"]).text(item["Text"]).appendTo($(st1ObjId + ", " + st2ObjId));
                });
                $(st1ObjId).val(st1);
                $(st2ObjId).val(st2);
            }
        }, 'json');
}

function EditPAI(paiId) {
    if (paiId != 0) {
        $.post(_ROOTPATH + "/StreetCoding/GetPAI",
        {
            paiId: paiId
        },
        function (message) {
            if (message.Status == true) {
                var stArr = message.Text.toString().split(',');
                setSubVal(stArr[1], stArr[2], "#LoSuIntNo", stArr[3], "#CrtIntNo", stArr[4], "#PAIStreet1", stArr[5], "#PAIStreet2");
                $("#hidPAIId").val(stArr[0]);
                $("#AutIntNo, #hidAutId").val(stArr[1]);
                $("#hidSubId").val(stArr[2]);
                $("#hidCrtId").val(stArr[3]);
                $("#hidSt1Id").val(stArr[4]);
                $("#hidSt2Id").val(stArr[5]);
                $("#PAIGPSXCoord").val(stArr[6]);
                $("#PAIGPSYCoord").val(stArr[7]);
                $("#tblEditPAI").show();
            }
        }, 'json');
    }
}

function DelePAI(paiId) {
    $("#errorMsg").text('');
    if (!isNaN(paiId)) {
        $.post(_ROOTPATH + "/StreetCoding/DelePAI",
        {
            paiId: paiId,
            autIntNo: $("#SearchAutIntNo").val()//2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        },
        function (message) {
            if (message.Status == true) {
                window.location.href = _ROOTPATH + '/StreetCoding/PlaceAtIntersectionManage?s=1&a=' +
                    $("#SearchAutIntNo").val() + '&sb=' + $("#SearchLoSuIntNo").val() + '&st1=' + $("#SearchStreet1").val() + '&st2=' + $("#SearchStreet2").val();
            } else {
                $("#errorMsg").text(message.Text);
            }
        }, 'json');
    }
}