﻿/// <reference path="../Library/jquery-1.3.2.min-vsdoc.js" />

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}

$(document).ready(function () {

    var search_autId = getQueryString('a');
    var search_key = getQueryString('s');
    if (search_autId) {
        losu_autId = search_autId;
        $("#SearchKey").val(search_key);
    }

    if (losu_autId) {
        $("#SearchAutIntNo").val(losu_autId);
        $("#AutIntNo").val(losu_autId);
    }

    $("#btnAddSub").click(function () {
        if ($("#tbSubEdit").css("display") == 'none') {
            $("#errorMsg").show();
            $("#tbSubEdit").show();
        } else {
            if ($("#hidSubId").val() == '0') {
                $("#errorMsg").hide();
                $("#tbSubEdit").hide();
            } else {
                $("#SearchAutIntNo, #AutIntNo").val(losu_autId);
                $("#errorMsg").text('');
                $("#hidAutId").val(0);
                $("#hidAreaCode").val("0");
                $("#hidSubId").val("0");
                $("#dpdAreaCode")[0].options(0).selected = true;
                $("#txtSubDescr").val("");
            }
        }
    });

    $("#btnSearch").click(function () {
        window.location.href = _ROOTPATH + '/StreetCoding/LocationSuburbManage?a=' +
            $("#SearchAutIntNo").val() + '&s=' + $("#SearchKey").val();
    });

    $("#btnSaveSub").click(function () {
        $("#errorMsg").text('');
        if ($("form").valid()) {
            var subId = $("#hidSubId").val();
            var autIdBefore = $("#hidAutId").val();
            $.post(_ROOTPATH + "/StreetCoding/SaveLocSub",
                {
                    autIntNo: $("#AutIntNo").val(),
                    lacCode: $("#dpdAreaCode").val(),
                    loSuIntNo: subId,
                    loSuDescr: $("#txtSubDescr").val(),
                    autIntNoBefore: autIdBefore,
                    lacCodeBefore: $("#hidAreaCode").val()
                },
                function (message) {
                    if (message.Status == true) {
                        window.location.href = _ROOTPATH + '/StreetCoding/LocationSuburbManage?a=' +
                            $("#SearchAutIntNo").val() + '&s=' + $("#SearchKey").val();
                    } else {
                        $("#errorMsg").text(message.Text);
                    }
                }, 'json');
        }
    });

});


function EditSub(subId, laccode) {
    $("#errorMsg").text('');
    if (!isNaN(subId)) {
        $.post(_ROOTPATH + "/StreetCoding/GetLocSub",
        {
            loSuIntNo: subId
        },
        function (message) {
            if (message.Status == true) {
                var subArr = message.Text.toString().split(',');
                $("#AutIntNo, #hidAutId").val(subArr[0]);
                $("#dpdAreaCode, #hidAreaCode").val(laccode);
                $("#txtSubDescr").val(subArr[1]);
                $("#hidSubId").val(subId);
                $("#tbSubEdit").show();
            }
        }, 'json');
    }
}

function DeleSub(subId, laccode) {
    $("#errorMsg").text('');
    if (!isNaN(subId)) {
        $.post(_ROOTPATH + "/StreetCoding/DeleLocSub",
        {
            loSuIntNo: subId,
            lacCode: laccode,
            autIntNo: $("#SearchAutIntNo").val()//2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        },
        function (message) {
            if (message.Status == true) {
                window.location.href = _ROOTPATH + '/StreetCoding/LocationSuburbManage?a=' +
                    $("#SearchAutIntNo").val() + '&s=' + $("#SearchKey").val();
            } else {
                $("#errorMsg").text(message.Text);
            }
        }, 'json');
    }
}