﻿$(document).ready(function () {
    $("#CrtIntNo").change(function () {

        $.post(_ROOTPATH + "/WOAManagement/GetCourtRoomByCourt", { crtIntNo: $(this).val() },
            function (items) {
                $("#CRIntNo option[value!='']").remove();
                $.each(items, function (index, item) {
                    $("<option value='" + item.CrtRIntNo + "'>" + item.CrtRoomName + "</option>").appendTo($("#CRIntNo"));
                });
            }, "json");
    });
})

function openAmountPnl(id, noticeNumber) {
    $("#divAmountSetting").attr("title", noticeNumber);
    $("#tabDetail tr:gt(0)").remove();
    $.post(_ROOTPATH + "/CourtManagement/GetNoAOGCharges",
        {
            id: id
        }, function (items) {
            if (items != undefined && items.length > 0) {
                $.each(items, function (index, item) {
                    $("<tr class='CartListItem' ><td>" + item.ChgType
                        + "</td><td style='text-align:right'>" + item.Sequence
                        + "</td><td><input type='text' style='width:80px;' value='N/A' name='txtFineAmount' id='" + item.ChgIntNo
                        + "'/></td><td>" + item.OffCode
                        + "</td><td>" + item.OffDescr +
                        "</td></tr>").appendTo($("#tabDetail"));
                });
                $("#txtComment").val('');

                $("#divAmountSetting").dialog({
                    position: "center", resizable: false, modal: true, width: 520,
                    buttons: [{
                        text: "Save",
                        id: "btnSave",
                        click: function () {
                            var flag = true;
                            var jsonList = new Array();
                            var obj = new Object();
                            //2015-01-26 jake modified regular expression
                            //var reg = /"^(?!^0\.0+$)([1-9]\d{0,3}|0)(\.\d{0,2})?$"/;
                            var reg = /^(([0](\.[1-9]{1,2}))|([1-9]\d{0,3})(\.[0-9]{1,2})?)$/;

                            $("input[name='txtFineAmount']").each(function (index, item) {
                                if ($(item).val() == "" || $(item).val() == 'N/A') {
                                    alert("Amount is required");
                                    $(item).focus();
                                    flag = false;
                                    return flag;
                                }
                                if (!reg.test($(item).val())) {
                                    alert("Please enter a correct amount.");
                                    $(item).focus();
                                    flag = false;
                                    return flag;
                                }

                                if ($(item).val() == "999999") {
                                    alert("Please enter a valid amount.");
                                    $(item).focus();
                                    flag = false;
                                    return flag;
                                }

                                obj.ChgIntNo = item.id;
                                obj.FineAmount = $(item).val();
                                jsonList.push(obj);
                            });

                            if ($("#CoMaIntNo").val() == "" || $("#CoMaIntNo").val() == "0") {
                                alert("Public prosecutor is required");
                                $("#CoMaIntNo").focus();
                                flag = false;
                                return flag;
                            }

                            if ($("#txtComment").val() == "") {
                                alert("Comment is required");
                                $("#txtComment").focus();
                                flag = false;
                                return;
                            }

                            if (flag == false) {
                                return flag;
                            }

                            var jsonObj = new Object();
                            jsonObj.SumIntNo = id;
                            jsonObj.ChargeList = jsonList;
                            jsonObj.Comment = $("#txtComment").val();
                            jsonObj.CoMaIntNo = $("#CoMaIntNo").val();
                            $("#btnSave").attr("disabled", true);

                            $.ajax({
                                type: 'POST',
                                url: _ROOTPATH + "/CourtManagement/SetAmount",
                                dataType: "json",
                                traditional: false,
                                contentType: 'application/json',
                                data: JSON.stringify(jsonObj),
                                success: function (msg) {

                                    if (msg.Timeout == true) {
                                        window.top.location = _ROOTPATH + "/Account/Login";
                                        return;
                                    }
                                    if (msg.Status) {
                                        if (msg.Message != '') {
                                            alert(msg.Message);
                                        }
                                        $("#divAmountSetting").dialog('close');
                                        $("#btnSave").attr("disabled", false);
                                    } else {
                                        if (msg.Message != '') {
                                            alert(msg.Message);
                                            $("#btnSave").attr("disabled", false);
                                            return;
                                        }
                                    }
                                    $("#divAmountSetting").dialog('close');
                                    var url = _ROOTPATH + "/CourtManagement/NoAOGAmountSetting?";

                                    //if ($("#CrtIntNo").val() != "") {
                                    //    url += "&CrtIntNo=" + $("#CrtIntNo").val();
                                    //}
                                    //if ($("#CRIntNo").val() != "") {
                                    //    url += "&CRIntNo=" + $("#CRIntNo").val();
                                    //}
                                    if ($("#Number").val() != "") {
                                        url += "Number=" + $("#Number").val();
                                    }
                                    document.location = url;
                                },
                                error: function (mgs) {
                                    if (msg.Timeout == true) {
                                        window.top.location = _ROOTPATH + "/Account/Login";
                                        return;
                                    }
                                    if (msg.Status) {
                                        if (msg.Message != '') {
                                            alert(msg.Message);
                                        }
                                        $("#divAmountSetting").dialog('close');
                                    } else {
                                        if (msg.Message != '') {
                                            alert(msg.Message);
                                            return;
                                        }
                                    }
                                }
                            });

                        }
                    },
                        {
                            text: "Cancel",
                            click: function () {
                                $("#divAmountSetting").dialog('close');
                            }
                        }]

                });

            } else {
                alert("No available data found");
            }
        }, 'json');



}

function confirmReverse(id, noticeNumber) {
    $("#divAmountSettingReverse").attr("title", noticeNumber);
    $("#tabDetail tr:gt(0)").remove();
    $.post(_ROOTPATH + "/CourtManagement/GetNoAOGChargesForReverse",
       {
           id: id
       }, function (item) {
           if (item != undefined) {
               if (item.ChargeList != undefined && item.ChargeList.length > 0) {
                   $.each(item.ChargeList, function (index, item) {
                       $("<tr class='CartListItem' ><td>" + item.ChgType
                           + "</td><td style='text-align:right'>" + item.Sequence
                           + "</td><td style='text-align:right'>" + item.FormatedAmount + "</td><td>" + item.OffCode
                           + "</td><td>" + item.OffDescr +
                           "</td></tr>").appendTo($("#tabDetail"));
                   });
               }
               $("#txtComment").val(item.Comment);
               $("#txtComment").attr("disabled", true);
               $("#CoMaIntNo").val(item.CoMaIntNo);
               $("#CoMaIntNo").attr("disabled", true);



               $("#divAmountSettingReverse").dialog({
                   position: "center", resizable: false, width: 520, modal: true,
                   buttons: [{
                       text: "Reverse",
                       id: "btnReverse",
                       click: function () {
                           $("#btnReverse").attr("disabled", true);
                           //post data
                           $.post(_ROOTPATH + "/CourtManagement/ReverseAmount",
                               {
                                   id: id
                               },
                               function (msg) {
                                   if (msg.Timeout == true) {
                                       window.top.location = _ROOTPATH + "/Account/Login";
                                       return;
                                   }
                                   if (msg.Status) {
                                       if (msg.Message != '') {
                                           alert(msg.Message);
                                       }
                                       $("#divAmountSettingReverse").dialog('close');

                                       var url = _ROOTPATH + "/CourtManagement/NoAOGAmountSettingReversal?";

                                       //if ($("#CrtIntNo").val() != "") {
                                       //    url += "&CrtIntNo=" + $("#CrtIntNo").val();
                                       //}
                                       //if ($("#CRIntNo").val() != "") {
                                       //    url += "&CRIntNo=" + $("#CRIntNo").val();
                                       //}
                                       if ($("#Number").val() != "") {
                                           url += "Number=" + $("#Number").val();
                                       }
                                       document.location = url;

                                   } else {
                                       if (msg.Message != '') {
                                           alert(msg.Message);
                                           return;
                                       }
                                   }
                                   $("#btnReverse").attr("disabled", false);
                               }, 'json');
                       }
                   },
                       {
                           text: "Cancel",
                           click: function () {
                               $("#divAmountSettingReverse").dialog('close');
                           }
                       }]

               });


           }
       }, 'json');


}
