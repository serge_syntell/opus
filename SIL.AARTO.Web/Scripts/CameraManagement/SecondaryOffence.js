﻿
$(document).ready(function () {

    if (IsParentFrame == 'True' && allowSecondaryOffence == 'True') {
        $("#secondaryOffenceMessagePanel").css("display", "none");

        //get all secondary offences code to display in txt_SecondaryOffenceCodes
        var para = { filmNo: FilmNo, frameNo: FrameNo };
        $.post(_ROOTPATH + "/CameraManagement/GetAllChildFramesSecondaryOffenceCode",
            para,
            function (data) {
                if (data.result == -2) {
                    window.location = _ROOTPATH + "/Account/Login";
                }

                $("#txt_SecondaryOffenceCodes").val(data.data);

                var chkSecondaryOffenceValue = $("#txt_SecondaryOffenceCodes").val();
                if (chkSecondaryOffenceValue.length > 0) {
                    $("#btn_SecondaryOffence").val("Modify Secondary Offence");
                }
                else {
                    $("#btn_SecondaryOffence").val("Add Secondary Offence");
                }

                //display add/modify button, txt_SecondaryOffenceCodes panel
                $("#secondaryOffenceButtonPanel").css("display", "block");

            },
            'json');
    }
     else {

        if (IsParentFrame != 'True') {
            var para = { filmNo: FilmNo, frameNo: FrameNo };
            $.post(_ROOTPATH + "/SecondaryOffence/GetSecondaryOffence",
            para,
            function (data) {
                if (data.result == -2) {
                    window.location = _ROOTPATH + "/Account/Login";
                }

                $("#secondaryOffenceMessagePanel").css("display", "block");
                $("#secondaryOffenceButtonPanel").css("display", "none");

                if (data.result == -1) {
                    $("#lblScondaryOffenceValue").text('wrong filmNo or frameNo');
                    return;
                }

                $("#lblScondaryOffenceValue").text(data.data.SeOfDescription);
            },
            'json');
        }
    }

    $("#btn_SecondaryOffence").click(function () {
        var para = {};
        $.post(_ROOTPATH + "/SecondaryOffence/GetAllByLanguage",
                    para,
                    function (data) {

                        if (data.result == -2) {
                            window.location = _ROOTPATH + "/Account/Login";
                        }

                        var chkSecondaryOffenceValue = "";
                        var secondaryOffenceArray = "";
                        chkSecondaryOffenceValue = $("#txt_SecondaryOffenceCodes").val();
                        if (chkSecondaryOffenceValue.length > 0) {
                            secondaryOffenceArray = chkSecondaryOffenceValue.split("|");
                        }

                        var checkString = "<div>";
                        var count = 1;
                        $.each(data.data, function (j) {
                            var isCheck = false;
                            if (secondaryOffenceArray.length > 0) {
                                for (var i = 0; i < secondaryOffenceArray.length; i++) {
                                    if (secondaryOffenceArray[i] == data.data[j].SeOfCode) {
                                        isCheck = true;
                                    }
                                }
                            }

                            if (data.data[j].IsAutomatic && !isCheck)//if not exists 78011(Vehicle license expired) child frame, exclude it.
                                return true;

                            if (count % 3 != 0) {
                                if (isCheck == true) {
                                    checkString = checkString + "&nbsp;<input type='checkbox' name='chkSecondaryOffence' value= " + data.data[j].SeOfCode + " checked= 'checked' />";
                                }
                                else {
                                    checkString = checkString + "&nbsp;<input type='checkbox' name='chkSecondaryOffence' value= " + data.data[j].SeOfCode + " />";
                                }

                                checkString = checkString + "&nbsp;<span>" + data.data[j].SeOfDescription + "</span>";
                            }
                            else {
                                checkString = checkString + "<span style='width: 150px;'>&nbsp;&nbsp;</span>";
                                if (isCheck == true) {
                                    checkString = checkString + "&nbsp;<input type='checkbox' name='chkSecondaryOffence' value= " + data.data[j].SeOfCode + " checked= 'checked' />";
                                }
                                else {
                                    checkString = checkString + "&nbsp;<input type='checkbox' name='chkSecondaryOffence' value= " + data.data[j].SeOfCode + " />";
                                }
                                checkString = checkString + "&nbsp;<span>" + data.data[j].SeOfDescription + "</span>";
                                checkString = checkString + "</div>"
                            }
                            count++;
                        });
                        $("#divSecondaryOffenceGroup").text("");
                        $("#divSecondaryOffenceGroup").append(checkString);

                        $("#PanelForSecondaryOffencePopup").dialog("open");
                    },
                'json');
    });

    $("#btnOk").click(function () {
        var chkSecondaryOffenceValue = "";
        var unChkSecondaryOffenceValue = "";

        $("input[name='chkSecondaryOffence']").each(function (i) {
            if ($(this).attr("checked")) {
                chkSecondaryOffenceValue = chkSecondaryOffenceValue + $(this).val() + "|";
            }
            else {
                unChkSecondaryOffenceValue = unChkSecondaryOffenceValue + $(this).val() + "|";
            }
        })

        //checked secondary offence
        chkSecondaryOffenceValue = chkSecondaryOffenceValue.substring(0, chkSecondaryOffenceValue.length - 1);
        $("#txt_SecondaryOffenceCodes").val(chkSecondaryOffenceValue);

        //unchecked secondary offence
        unChkSecondaryOffenceValue = unChkSecondaryOffenceValue.substring(0, unChkSecondaryOffenceValue.length - 1);

        //handle frame about secondary offence   
        var para1 = { filmNo: FilmNo, frameNo: FrameNo, checkSecondaryOffences: chkSecondaryOffenceValue, uncheckSecondaryOffences: unChkSecondaryOffenceValue, processName: processName };
        $.post(_ROOTPATH + "/CameraManagement/HandleFrameAboutSecondaryOffence",
                para1,
                function (data) {
                    if (data.result == -2) {
                        window.location = _ROOTPATH + "/Account/Login";
                    }

                    if (data.result == -1) {
                        alert(data.data);
                    }

                    if (data.result == 1) {
                        if (chkSecondaryOffenceValue.length > 0) {
                            $("#btn_SecondaryOffence").val("Modify Secondary Offence");
                        }
                        else {
                            $("#btn_SecondaryOffence").val("Add Secondary Offence");
                        }

                        $("#PanelForSecondaryOffencePopup").dialog("close");
                        //window.location.reload();
                    }
                },
            'json');

        
    });

    $("#btnCancel").click(function () {

        $("#PanelForSecondaryOffencePopup").dialog("close");
    });

    $("#btnRejectYes").click(function () {
        //Reject frames include secondary frames
        var btnAcceptNameValue = $("#btnAccept").attr("name");
        Action(processName, btnAcceptNameValue, _ROOTPATH + "/CameraManagement/UpdateFrame", 'Yes');

        $("#PanelForRejectPopup").dialog("close");
    });

    $("#btnRejectNo").click(function () {
        //Just reject parent frame
        var btnAcceptNameValue = $("#btnAccept").attr("name");
        Action(processName, btnAcceptNameValue, _ROOTPATH + "/CameraManagement/UpdateFrame", 'No');
        $("#PanelForRejectPopup").dialog("close");
    });

    $('#PanelForSecondaryOffencePopup').dialog({
        autoOpen: false,  
        //bgiframe: true, 
        width: 500,
        height: 160,
        position: "center",
        resizable: false,
        modal: true,   
        closeOnEscape: false,
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); }
    });

    $('#PanelForRejectPopup').dialog({
        autoOpen: false,  
        //bgiframe: true, 
        width: 500,
        height: 160,
        position: "center",
        resizable: false,
        modal: true,   
        closeOnEscape: false,
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); }
    });

})