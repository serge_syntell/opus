﻿/// <reference path="../jquery-1.4.4-vsdoc.js" />
$(document).ready(function () {
    $("input[name='cbx_notice']").change(function () {
        //var notIntNo = $(this).siblings().eq(0).val();

        //var checked = false;
        //if ($(this).attr("checked")) {
        //    checked = true;
        //}

        //$("#tb_resubmission_" + notIntNo + " input:checkbox").each(function (index, item) {
        //    $(item).attr("checked", checked);

        //    //$(item).attr('checked', $(this).attr('checked'));
        //});

    });


    $("input[name='cbx_resubmissionError']").change(function () {
        var checked = $(this).attr("checked");
        var notIntNo = $(this).siblings().eq(0).val().split('_')[0];

        //if (checked) {
        //    if ($("#cbx_" + notIntNo).attr("checked")) {
        //    } else {
        //        $("#cbx_" + notIntNo).attr("checked", true);
        //    }
        //} else {

        //    var checkedLen = $("#tb_resubmission_" + notIntNo + " :checked").length;

        //    if (checkedLen == 0) {
        //        $("#cbx_" + notIntNo).attr("checked", false);
        //    }
        //}
        var checkedLen = $("#tb_resubmission_" + notIntNo + " :checked").length;
        var checkboxLen = $("#tb_resubmission_" + notIntNo + " input[type='checkbox']").length;
        //alert(checkedLen);
        if (checkedLen == checkboxLen) {
            $("#cbx_" + notIntNo).attr("disabled", false);
        }
    });

    $("#btnResubmit").click(function () {

        var notcheckedLen = $("input[name='cbx_notice']:checked").length;
        if (notcheckedLen == 0) {
            alert("None infringement selected,Please select at least one infringement number");
            return;
        }

        //var checkedLen = $("input[name='cbx_resubmissionError']:checked").length;
        //if (checkedLen == 0) {
        //    alert("None item selected,Please select at least one item");
        //    return;
        //}
        var resubmission = new Object();
        var requestArra = new Array();
        var index = 0;

        $.blockUI({ message: $("#progressBar") });

        $("input[name='cbx_notice']:checked").each(function (i, not) {
            var notIntNo = $(not).siblings().eq(0).val();
            $("#tb_resubmission_" + notIntNo + " input[name='cbx_resubmissionError']:checked").each(function (y, item) {
                requestArra[index] = $(item).siblings().eq(0).val();
                index++;
            });

        });


        //$("input[name='cbx_resubmissionError']:checked").each(function (index, item) {
        //    requestArra[index] = $(item).siblings().eq(0).val();
        //    index++;
        //});

        resubmission.Resubmissions = requestArra;


        $.ajax({
            type: 'POST',
            url: _ROOTPATH + "/Infringement/PostResubmissions",
            dataType: "json",
            traditional: false,
            contentType: 'application/json',
            data: JSON.stringify(resubmission),
            success: function (msg) {
                $.unblockUI();
                if (msg != undefined && msg.Text != '' && msg.Text != null) {
                    alert(msg.Text);
                }
                var noticeNumber = $("#NotTicketNo").val();
                var url = _ROOTPATH + "/Infringement/Resubmission?Page=" + $("#Page").val();
                if (noticeNumber != "") {
                    url += "&NotTicketNo=" + $("#NotTicketNo").val();
                }
                document.location = url;
                //$("#form1").submit();
            },
            error: function (mgs) {
                $.unblockUI();
                if (msg != undefined && msg.Text != '' && msg.Text != null) {
                    alert(msg.Text);
                }
            }
        });

    });
})

function showResubmissionError(obj, id) {
    if ($("#tb_resubmission_" + id).css("display") == 'none') {
        $("#tb_resubmission_" + id).slideDown();
        $(obj).attr('src', _ROOTPATH + "/Content/Image/scroll_arrow_up.gif");
    }
    else {
        $("#tb_resubmission_" + id).slideUp("slow", function () {
            $(obj).attr('src', _ROOTPATH + "/Content/Image/scroll_arrow_down.gif");
        });

    }
}