﻿$(document).ready(function() {
    $("#btAddCourt").click(function() {
    $.post(_ROOTPATH + "/Report/AddCourt", { Courts: $("#lbCourtsList").val() },
        function(message) {
            if (message.Status == true) {
                $("#lbCourtsList option:selected").appendTo("#lbSelectedCourtsList");
            }
        }, 'json');
    });

    $("#btRemoveCourt").click(function() {
    $.post(_ROOTPATH + "/Report/RemoveCourt", { Courts: $("#lbSelectedCourtsList").val() },
        function(message) {
            if (message.Status == true) {
                $("#lbSelectedCourtsList option:selected").appendTo("#lbCourtsList");
            }
        }, 'json');
    });

    $("#btAddAllCourt").click(function() {
    $.post(_ROOTPATH + "/Report/AddCourt", { Courts: "ALL" },
        function(message) {
            if (message.Status == true) {
                $("#lbCourtsList option").appendTo("#lbSelectedCourtsList");
            }
        }, 'json');
    });

    $("#btRemoveAllCourt").click(function() {
    $.post(_ROOTPATH + "/Report/RemoveCourt", { Courts: "ALL" },
        function(message) {
            if (message.Status == true) {
                $("#lbSelectedCourtsList option").appendTo("#lbCourtsList");
            }
        }, 'json');
    });
})


function setDateTextBox(txtId) {
    $('#' + txtId).datepicker(
	                { changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd', defaultDate: '' }
                        );
}




$(function() {
    setDateTextBox('txtDateStart');
    setDateTextBox('txtDateEnd');
});


//$("#ddlReortFilters").val()

