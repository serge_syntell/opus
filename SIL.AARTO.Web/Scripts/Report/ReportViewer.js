﻿function ShowPage(pageId) {
    $.post(_ROOTPATH + "/Report/ShowPage", { PageNumber: pageId, SortColumn: "" },
    function(message) {
        if (message.Status == true) {
            $("#btRefresh").click();
        }
    }, 'json');
}

function Sort(SortColumn) {
    $.post(_ROOTPATH + "/Report/ShowPage", { PageNumber: 1, SortColumn: SortColumn },
    function(message) {
    if (message.Status == true) {
        $("#btRefresh").click();
        }
    }, 'json');
}




$(document).ready(function() {
$("#btExportToExcel").click(function() {
    $('#TxtMessage').html('Waiting...');
    $.post(_ROOTPATH + "/Report/ExportReport", { SaveType: "Excel" },
        function(message) {
            $('#TxtMessage').html('');
            if (message.Status == true) {
                alert(message.Text);
                window.open(message.Text);
            }
            else if (message.Status == false) {
                alert(message.Text);
            }
        }, 'json');
    });


    $("#btExportToWord").click(function() {
    $('#TxtMessage').html('Waiting...');
    $.post(_ROOTPATH + "/Report/ExportReport", { SaveType: "Word" },
        function(message) {
            $('#TxtMessage').html('');
            if (message.Status == true) {
                alert(message.Text);
                window.open(message.Text);
            }
            else if (message.Status == false) {
                alert(message.Text);
            }
        }, 'json');
    });


    $("#btExportToPDF").click(function() {
    $('#TxtMessage').html('Waiting...');
    $.post(_ROOTPATH + "/Report/ExportReport", { SaveType: "PDF" },
        function(message) {
            $('#TxtMessage').html('');
            if (message.Status == true) {
                alert(message.Text);
                window.open(message.Text);
            }
            else if (message.Status == false) {
                alert(message.Text);
            }
        }, 'json');
    });


    $("#btExportToASCII").click(function() {
    $('#TxtMessage').html('Waiting...');
    $.post(_ROOTPATH + "/Report/ExportReport", { SaveType: "ASCII" },
        function(message) {
            $('#TxtMessage').html('');
            if (message.Status == true) {
                alert(message.Text);
                window.open(message.Text);
            }
            else if (message.Status == false) {
                alert(message.Text);
            }
        }, 'json');
    });
})