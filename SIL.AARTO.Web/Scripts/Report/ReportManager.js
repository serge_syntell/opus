﻿$(document).ready(function () {
    //    $("#btnAddFilter").click(function() {
    //    $.post("/Report/ReportManager", { OperateType: "AddReortFilters", FilterName: $("#ddlReortFilters").val() },
    //            function(message) {
    //            if (message.Status == true) {
    //                }
    //            }, 'json');
    //    });

    //    $("#btnAddHeadGroupColumn").click(function() {
    //    $.post("/Report/ReportManager", { OperateType: "AddReortHeadGroupColumn" },
    //        function(message) {
    //            if (message.Status == true) {
    //            }
    //        }, 'json');
    //    });


    //    $("#btnAddHeadColumn").click(function() {
    //        $.post("/Report/AddReortHeadColumn", { ColumnText: $("#tbHeadColumn").val(), DataField: $("#tbHeadColumnDataField").val(), ColumnFontColor: $("#ddlHeadColumnFontColor").val(),
    //            Sort: document.getElementById("cbIsSortable").checked
    //        },
    //        function(message) {
    //            if (message.Status == true) {
    //            }
    //        }, 'json');
    //    });

    //    $("#btResetXML").click(function() {
    //        $.post("/Report/ResetXML", {},
    //        function(message) {
    //            if (message.Status == true) {
    //            }
    //        }, 'json');
    //    });

    //    $("#btExportToWord").click(function() {
    //        $.post("/Report/ExportToWord", { control: $("#Content1") },
    //        function(message) {
    //            if (message.Status == true) {
    //            }
    //        }, 'json');
    //    });

    $("#btAddCourt").click(function () {
        $.post(_ROOTPATH + "/Report/AddCourt", { Courts: $("#lbCourtsList").val() },
        function (message) {
            if (message.Status == true) {
                $("#lbCourtsList option:selected").appendTo("#lbSelectedCourtsList");
            }
        }, 'json');
    });

    $("#btRemoveCourt").click(function () {
        $.post(_ROOTPATH + "/Report/RemoveCourt", { Courts: $("#lbSelectedCourtsList").val() },
        function (message) {
            if (message.Status == true) {
                $("#lbSelectedCourtsList option:selected").appendTo("#lbCourtsList");
            }
        }, 'json');
    });

    $("#btAddAllCourt").click(function () {
        $.post(_ROOTPATH + "/Report/AddCourt", { Courts: "ALL" },
        function (message) {
            if (message.Status == true) {
                $("#lbCourtsList option").appendTo("#lbSelectedCourtsList");
            }
        }, 'json');
    });

    $("#btRemoveAllCourt").click(function () {
        $.post(_ROOTPATH + "/Report/RemoveCourt", { Courts: "ALL" },
        function (message) {
            if (message.Status == true) {
                $("#lbSelectedCourtsList option").appendTo("#lbCourtsList");
            }
        }, 'json');
    });

    $("#ddlAutomaticGenerateIntervalTypeList").change(function () {
        var select = $("#ddlAutomaticGenerateIntervalTypeList option:selected").val();
        if (select == "Daily" || select == "Weekly" || select == "Monthly") {
            $("#tbAutomaticGenerateInterval").hide();
        }
        else {
            $("#tbAutomaticGenerateInterval").show();
        }
    });

    var select = $("#ddlAutomaticGenerateIntervalTypeList option:selected").val();
    if (select == "Daily" || select == "Weekly" || select == "Monthly") {
        $("#tbAutomaticGenerateInterval").hide();
    }
    else {
        $("#tbAutomaticGenerateInterval").show();
    }



    //    $("#btSaveReport").click(function() {
    //    $.post(_ROOTPATH + "/Report/SaveReport", { ReportName: $("#tbReportName").val(), ReportCode: $("#tbReportCode").val(), AutIntNo: $("#ddlLocalAuthoritys option:selected").val(), RspIntNo: $("#ddlStoredProcNameList option:selected").val(),
    //    AutomaticGenerate: $("#cbIsAutomaticGenerate").val(), AutomaticGenerateInterval: $("#tbAutomaticGenerateInterval").val(), AutomaticGenerateIntervalType: $("#ddlAutomaticGenerateIntervalTypeList").val(), AutomaticGenerateIntervalType: $("#ddlAutomaticGenerateFormatList").val()
    //    },
    //        function(message) {
    //            if (message.Status == true) {
    //                alert(message.Text);
    //            }
    //        }, 'json');
    //    });
})


function ShowPage(pageId) {
    $.post("/Report/ShowPage", { PageNumber: pageId, SortColumn: ""},
    function(message) {
    if (message.Status == true) {
        $("#btRefresh").click();
        }
    }, 'json');
}

function Sort(SortColumn) {
    $.post("/Report/ShowPage", { PageNumber: 1, SortColumn: SortColumn },
    function(message) {
    if (message.Status == true) {
        $("#btRefresh").click();
        }
    }, 'json');

    $("#btRefresh").click();
}


function setDateTextBox(txtId) {
    $('#' + txtId).datepicker(
	                { changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd', defaultDate: '' }
                        );
	            }

function setColorSelector(txtId, txtValue, colorValue) {
    $('#' + txtId).ColorPicker({
	                    color: colorValue,
	                    onShow: function(colpkr) {
	                        $(colpkr).fadeIn(500);
	                        return false;
	                    },
	                    onHide: function(colpkr) {
	                        $(colpkr).fadeOut(500);
	                        return false;
	                    },
	                    onChange: function(hsb, hex, rgb) {
	                    $('#' + txtId + ' div').css('backgroundColor', '#' + hex);
	                    $('#' + txtValue).val('#' + hex);
	                    }
	                });
	                $('#' + txtValue).val(colorValue);
	                $('#' + txtId + ' div').css('backgroundColor', colorValue);
}



$(function() {
    setDateTextBox('txtDateStart');
    setDateTextBox('txtDateEnd');

    setColorSelector('HeadColumnColorSelector', 'HeadColumnColour', '#000000');
    setColorSelector('HeadGroupColumnColorSelector', 'HeadGroupColumnColour', '#000000');

});


    //$("#ddlReortFilters").val()

