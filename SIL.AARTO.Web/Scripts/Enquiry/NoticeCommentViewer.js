﻿var urlEnquiryPrefix = _ROOTPATH + "/Enquiry/";
//var urlEnquiryPrefix = "/Enquiry/";


function IsNullOrEmpty(dataStr) {
    if (dataStr == null || dataStr == "" || dataStr == undefined || dataStr == "undefined") {
        return true;
    } else {
        return false;
    }
}

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return "";
}


$(document).ready(function () {

    $("a").css("text-decoration", "none").focus(function () {
        this.blur();
    });

    $("#tbdGrid tr:nth-child(2n-1)").addClass("CartListItem");
    $("#tbdGrid tr:nth-child(2n)").addClass("CartListItemAlt");

    $("#btnNew").attr("disabled", false);
    $("#tbdEdit").hide();
    $("#btnUpdate").hide();
    $("#btnAdd").show();

    $("#btnNew").click(function () {
        $("#errorMsg").text('');
        $("#tbdEdit").show();
        $("#btnUpdate").hide();
        $("#btnAdd").show();
        $("#lblEditTitle").text(msgAddComment);
        $("#txtComment").val("");
        $("#btnNew").attr("disabled", true);
    });

    $("#btnPrint").click(function () {
        NCPrint();
    });

});


function NCGet(order, ncIntNo) {
    var urlPost = urlEnquiryPrefix + "NCGet";
    if (IsNullOrEmpty(order) || IsNullOrEmpty(ncIntNo)) return;
    $("#btnNew").attr("disabled", false);
    $("#tbdEdit").show();
    $("#btnAdd").hide();
    $("#btnUpdate").show();
    $("#lblEditTitle").text(msgUpdateComment + order);
    $("#hidOldComment").val("");
    $("#txtComment").val("");
    $("#errorMsg").text('');

    $.post(urlPost, { NcIntNo: ncIntNo }, function(message) {
        if (message.EntityStatus) {
            $("#hidOldComment").val(message.NcComment);
            $("#txtComment").val(message.NcComment);
            $("#btnUpdate").unbind('click');
            $("#btnUpdate").bind('click', function() {
                NCSave(message.NcIntNo, message.RowVersionString);
            });
        }
        $("#errorMsg").text(message.EntityMessage);
    }, 'json');
}

function NCSave(ncIntNo, rowVersion, gotoPage) {
    var id = getQueryString("id");
    var page = getQueryString("page");
    var urlPost = urlEnquiryPrefix + "NCSave";
    var urlRequest = urlEnquiryPrefix + "NoticeCommentViewer?id=" + id + "&page=" + (IsNullOrEmpty(gotoPage) ? page : gotoPage).toString();
    $("#errorMsg").text('');

    if ($("form").valid()) {
        if ($.trim($("#txtComment").val()) == "") {
            $("#errorMsg").text("Please input comment");
            return;
        }

        if (!IsNullOrEmpty(ncIntNo) && ncIntNo > 0 && $("#hidOldComment").val() == $("#txtComment").val()) {
            $("#errorMsg").text(msgSaveSuccessful);
            return;
        }
        $.post(urlPost, { NotIntNo: id, NcIntNo: ncIntNo, NcComment: $("#txtComment").val(), RowVersionString: rowVersion }, function(message) {
            if (message.EntityStatus) {
                window.location.href = urlRequest;
            }
            $("#errorMsg").text(message.EntityMessage);
        }, 'json');
    }
}

function NCDelete(ncIntNo, rowVersion) {
    var id = getQueryString("id");
    var page = getQueryString("page");
    var urlPost = urlEnquiryPrefix + "NCDelete";
    var urlRequest = urlEnquiryPrefix + "NoticeCommentViewer?id=" + id + "&page=" + page;
    $("#errorMsg").text("");

    if (!IsNullOrEmpty(ncIntNo)) {
        $.post(urlPost, { NcIntNo: ncIntNo, RowVersionString: rowVersion }, function(message) {
            if (message.EntityStatus) {
                window.location.href = urlRequest;
            }
            $("#errorMsg").text(message.EntityMessage);
        }, 'json');
    }
}

function NCPrint() {
    var id = getQueryString("id");
    var urlPost = urlEnquiryPrefix + "NCPrint?id=" + id;
    $("#errorMsg").text("");

    window.open(urlPost, "_blank");
}

//2013-12-09 added by Nancy
function Close() {
    window.close();
}