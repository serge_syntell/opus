﻿   function Close() {
        window.close();
    } 

    function submitAction(obj, controller, action) {
        $("#NoticeNoLookupHasError").val("");
        if ($("#Since").val() != "") {
            $("#SinceText").val($.trim($("#Since").val()));
        } 
        if (controller != "" && action != "") {
            $("#mainform").attr("action", _ROOTPATH+"/" + controller + "/" + action + "");
            $(obj).submit();
        }

    }
    function SelectSubmit2(obj) {
        $("#NoticeNoLookupHasError").val("");
        var nottickno = $.trim($(obj).parent().prev().prev().prev().text());
        $("#searchField").val("TicketNo");
        $("#TicketNo").val(nottickno);
        $("#mainform").attr("action",_ROOTPATH+ "/" + $("#NoticeNoLookupSubmitToController").val() + "/" + $("#NoticeNoLookupSubmitToAction").val() + "?selectSubmit=1");

        $(obj).submit();

    }
    $(function () {
        $("#Since").datepicker({ dateFormat: 'yy-mm-dd' });

        if ($("#SinceText").val() != "") {
        }
        else {
            $("#Since").val("");
        }

        $("#RegNo").focus(function () {
            $("#searchField").val("RegNo");
            $(".Panel1").attr("class", "Panel1 DisableTextBox2");
            $("#RegNo").removeClass("DisableTextBox2");
            $("#RegNo").addClass("Normal");

        });
        $("#IDNo").focus(function () {
            $("#searchField").val("IDNo");
            $(".Panel1").attr("class", "Panel1 DisableTextBox2");
            $("#IDNo").removeClass("DisableTextBox2");
            $("#IDNo").addClass("Normal");
        });
        $("#TicketNo").focus(function () {
            $("#searchField").val("TicketNo");
            $(".Panel1").attr("class", "Panel1 DisableTextBox2");
            $("#TicketNo").removeClass("DisableTextBox2");
            $("#TicketNo").addClass("Normal");
        });
        $("#SummonsNO").focus(function () {
            $("#searchField").val("SummonsNO");
            $(".Panel1").attr("class", "Panel1 DisableTextBox2");
            $("#SummonsNO").removeClass("DisableTextBox2");
            $("#SummonsNO").addClass("Normal");
        });
        $("#CaseNO").focus(function () {
            $("#searchField").val("CaseNO");
            $(".Panel1").attr("class", "Panel1 DisableTextBox2");
            $("#CaseNO").removeClass("DisableTextBox2");
            $("#CaseNO").addClass("Normal");
        });
        $("#WOANO").focus(function () {
            $("#searchField").val("WOANO");
            $(".Panel1").attr("class", "Panel1 DisableTextBox2");
            $("#WOANO").removeClass("DisableTextBox2");
            $("#WOANO").addClass("Normal");
        });
        $("#Initials").focus(function () {
            $("#searchField").val("Names");
            $(".Panel1").attr("class", "Panel1 DisableTextBox2");
            $("#Initials").removeClass("DisableTextBox2");
            $("#Initials").addClass("Normal");
        });
        $("#Name").focus(function () {
            $("#searchField").val("Names");
            $(".Panel1").attr("class", "Panel1 DisableTextBox2");
            $("#Name").removeClass("DisableTextBox2");
            $("#Name").addClass("Normal");
        });

        $("#EasyPayNo").focus(function () {
            $("#searchField2").val("EasyPayNo"); 
            $(".Panel1").attr("class", "Panel1 DisableTextBox2");
            $("#EasyPayNo").removeClass("DisableTextBox2");
            $("#EasyPayNo").addClass("Normal")
        });
        $("#Prefix").focus(function () {
            $("#searchField2").val("TickSequenceNo"); 
            $(".Panel1").attr("class", "Panel1 DisableTextBox2");
            $("#Prefix").removeClass("DisableTextBox2");
            $("#Prefix").addClass("Normal")
        });
        $("#Sequence").focus(function () {
            $("#searchField2").val("TickSequenceNo"); 
            $(".Panel1").attr("class", "Panel1 DisableTextBox2");
            $("#Sequence").removeClass("DisableTextBox2");
            $("#Sequence").addClass("Normal")
        });
        $("#AuthCode").focus(function () {
            $("#searchField2").val("TickSequenceNo"); 
            $(".Panel1").attr("class", "Panel1 DisableTextBox2");
            $("#AuthCode").removeClass("DisableTextBox2");
            $("#AuthCode").addClass("Normal")
        });

        $(".CartListHead th").css("height", "50px");
        //$(".CartListHead th:nth-child(10)").css({ "background-image": "url(../Content/Image/Camera.png)", "background-repeat": "no-repeat", "background-position": "center" });
        //$(".CartListHead th:nth-child(11)").css({ "background-image": "url(../Content/Image/Acrobat.png)", "background-repeat": "no-repeat", "background-position": "center" });

        if ($("#NoticeNoLookupHasError").val() == "1") { 
            if ($("#searchField2").val() == "TickSequenceNo") {
                if ($("#setControlFocus").val() == "txtNumber") {
                    $("#Sequence").focus();
                }
                else if ($("#setControlFocus").val() == "txtAuthCode") {
                    $("#AuthCode").focus();
                }
                else {
                    $("#Prefix").focus();
                }
            }
            else if ($("#searchField2").val() == "EasyPayNo") {
                $("#EasyPayNo").focus();
            }
        }
        else {
            if ($("#searchField").val() == "RegNo") {
                $("#RegNo").focus();
            }
            else if ($("#searchField").val() == "IDNo") {
                $("#IDNo").focus();
            }
            else if ($("#searchField").val() == "TicketNo") {
                $("#TicketNo").focus();
            }
            else if ($("#searchField").val() == "SummonsNO") {
                $("#SummonsNO").focus();
            }
            else if ($("#searchField").val() == "CaseNO") {
                $("#CaseNO").focus();
            }
            else if ($("#searchField").val() == "WOANO") {
                $("#WOANO").focus();
            }
            else if ($("#searchField").val() == "Names") {
                $("#Initials").focus();
            }
            else {
                $("#RegNo").focus();
            }
        }

//        $(".Panel1").each(function () {
//            if ($(this).focus()) {
//                $(".Panel1").attr("class", "Panel1 DisableTextBox2");
//                $(this).removeClass("DisableTextBox2");
//                $(this).addClass("Normal");
//            }
//        })

    });

    function OpenViewHistoricalSummons_Report(searchField, searchValue, Since) {
        window.open("ViewHistoricalSummons_Report?searchField=" + searchField + "&searchValue=" + searchValue + "&Since=" + Since, "_blank");

    }
