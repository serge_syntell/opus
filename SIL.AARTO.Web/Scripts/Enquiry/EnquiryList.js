﻿   function Close() {
        window.close();
    } 

    function submitAction(obj, controller, action) {
        $("#NoticeNoLookupHasError").val("");
        if ($("#Since").val() != "") {
            $("#SinceText").val($.trim($("#Since").val()));
        } 
        if (controller != "" && action != "") {
            $("#mainform").attr("action", _ROOTPATH+"/" + controller + "/" + action + "");
            $("#mainform").submit();
        }
    }
    
    function SelectSubmit2(obj) {
        $("#NoticeNoLookupHasError").val("");
        var nottickno = $.trim($(obj).parent().prev().prev().prev().text());
        $("#searchField").val("TicketNo");
        $("#SelectSubmit").val("1");
        $("#TicketNo").val(nottickno);
        $("#mainform").attr("action", _ROOTPATH+"/" + $("#NoticeNoLookupSubmitToController").val() + "/" + $("#NoticeNoLookupSubmitToAction").val() + "?selectSubmit=1");
        $("#mainform").submit();
    }
    $(function () {
        $("#Since").datepicker({ dateFormat: 'yy-mm-dd' });
        
        if ($("#SinceText").val() != "") {
        }
        else {
            $("#Since").val("");
        }
        //2013-07-18 modify by Henry for fix click enter no Lookup
        $("input.Panel1").focus(function () {
            var searchField = $(this).attr("Name");
            if (searchField == "Initials" || searchField == "Name") {
                searchField = "Names";
            }
            $("#searchField").val(searchField)
            $(".Panel1").attr("class", "Panel1 DisableTextBox2");
            $(this).removeClass("DisableTextBox2");
            $(this).addClass("Normal");
            lookupOnFocus = false;
            if (searchField == "Prefix" || searchField == "Sequence" || searchField == "AuthCode" || searchField == "EasyPayNo") {
                lookupOnFocus = true;
            }
        });

      
        $(".CartListHead th").css("height", "50px");
        if ($("#NoticeNoLookupHasError").val() == "1") { 
            if ($("#searchField2").val() == "TickSequenceNo") {
                if ($("#setControlFocus").val() == "txtNumber") {
                    $("#Sequence").focus();
                }
                else if ($("#setControlFocus").val() == "txtAuthCode") {
                    $("#AuthCode").focus();
                }
                else if ($("#setControlFocus").val() == "txtPrefix") {
                    $("#Prefix").focus();
                }
                else {
                    $("#Prefix").focus();
                }
            }
            else if ($("#searchField2").val() == "EasyPayNo") {
                $("#EasyPayNo").focus();
            }
        }
        else {
            if ($("#searchField").val() == "RegNo") {
                $("#RegNo").focus();
            }
            else if ($("#searchField").val() == "IDNo") {
                $("#IDNo").focus();
            }
            else if ($("#searchField").val() == "TicketNo") {
                $("#TicketNo").focus();
            }
            else if ($("#searchField").val() == "SummonsNO") {
                $("#SummonsNO").focus();
            }
            else if ($("#searchField").val() == "CaseNO") {
                $("#CaseNO").focus();
            }
            else if ($("#searchField").val() == "WOANO") {
                $("#WOANO").focus();
            }
            else if ($("#searchField").val() == "Names") {
                $("#Initials").focus();
            }
            else {
                $("#RegNo").focus();
            }
        }
//        $(".Panel1").each(function () {
//            if ($(obj).focus()) {
//                $(".Panel1").attr("class", "Panel1 DisableTextBox2");
//                $(obj).removeClass("DisableTextBox2");
//                $(obj).addClass("Normal");
//            }
        //        })

    });

 


    function OpenNotes(notIntNo) {
        window.open("NoticeCommentViewer?id=" + notIntNo, "_blank");
    }

    function OpenReport(notIntNo) {
        window.open("ViewOffenceDetailReport?NotIntNo=" + notIntNo, "_blank");
    }
    function OpenReportEnquiry(notIntNo) {
        window.open("ViewOffenceDetailReportEnquiry?NotIntNo=" + notIntNo, "_blank");
    }
    function OpenReportHistory(notIntNo) {
        window.open("ViewOffenceDetailReportHistory?NotIntNo=" + notIntNo, "_blank");
    }


    function OpenLetter(RepIntNo, LetterTo, TicketNo) {
        window.open("RepresentationLetterViewer?RepIntNo=" + RepIntNo + "&LetterTo=" + LetterTo + "&TicketNo=" + TicketNo, "_blank");
    }

    function OpenCamera(notIntNo, searchField, searchValue, Since) {
        window.open("ViewOffenceDetail?NotIntNo=" + notIntNo + "&searchField=" + searchField + "&searchValue=" + searchValue + "&Since=" + Since + "&isPre=true", "_blank");
        //alert("ViewOffenceDetail?NotIntNo=" + notIntNo + "&searchField=" + searchField + "&searchValue=" + searchValue + "&Since=" + Since + "&isPre=true");
    }

    function OpenDocument(notIntNo, source) {
        if (source == null || source < 0)
            window.open("NonCamera?NotIntNo=" + notIntNo, "_blank");
        else 
            window.open("CamaraDocument?notIntNo=" + notIntNo + "&source=" + source, "_blank");
    }

    function OpenSummaryReport(searchField, searchValue, Since) {
        window.open("ViewOffenceReport?searchField=" + searchField + "&searchValue=" + searchValue + "&Since=" + Since, "_blank");

    }
    function OpenOutstandingFinesReportViewer(searchField, searchValue, Since) {
        window.open("OutstandingFinesReportViewer?searchField=" + searchField + "&searchValue=" + searchValue + "&Since=" + Since, "_blank");
    }

    function OpenViewOffenceDetailAllReport(searchField, searchValue, Since) {
        window.open("ViewOffenceDetailAllReport?searchField=" + searchField + "&searchValue=" + searchValue + "&Since=" + Since, "_blank");
    }
    function OpenViewOffenceDetailAllReport_CT(searchField, searchValue, Since) {
        window.open("ViewOffenceDetailAllReport_CT?searchField=" + searchField + "&searchValue=" + searchValue + "&Since=" + Since, "_blank");
    }

    //Jerry 2014-08-07 add
    function OpenSignature(notIntNo) {
        window.open("ViewSignature?NotIntNo=" + notIntNo, "_blank");
    }

    function OpenControlDocument(notIntNo) {
        window.open("ViewControlDocument?NotIntNo=" + notIntNo, "_blank");
    }


    function SumNoSearch(sumNo, Since) {
        location.href = "EnquiryDetailList?sumNo=" + sumNo + "&Since=" + Since;
    }

    function OutStandingReport(searchField, searchValue, Since) {
        window.open("OutstandingReport?searchField=" + searchField + "&searchValue=" + searchValue + "&Since=" + Since, "_blank");
    }
    function SummonsDetailReport(searchField, searchValue, Since) {
        window.open("ReportSummonsDetail?searchField=" + searchField + "&searchValue=" + searchValue + "&Since=" + Since, "_blank");
    }
