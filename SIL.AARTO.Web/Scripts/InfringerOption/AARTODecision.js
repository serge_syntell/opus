﻿
var imageUrlList;
var nowImageIndex;
$(document).ready(function() {
    imageUrlList = imageUrlString.split("|");
    nowImageIndex = -1;
    if (imageUrlList.length > 0) {
        ShowImageByIndex(0);
        ShowImage(true);
    }
    else {
        ShowImage(false);
    }
    $("#rdbDisplayForm").click(function() { ShowImage(!$("#rdbDisplayForm").attr("checked")); });
    $("#rdbDisplayImage").click(function() { ShowImage(!$("#rdbDisplayForm").attr("checked")); });
    $("#lkPre").click(function() {
        ShowImageByIndex(nowImageIndex - 1 > 0 ? nowImageIndex - 1 : 0);
    });
    $("#lkNext").click(function() {
        ShowImageByIndex(nowImageIndex < imageUrlList.length - 1 ? nowImageIndex + 1 : nowImageIndex);
    });
});
function ShowImageByIndex() {
    if (nowImageIndex != arguments[0]) {
        $("#imgDocument").attr("src", imageUrlList[arguments[0]]);
        nowImageIndex = arguments[0];
    }
}
function ShowImage() {
    $("#divForm").css("display", arguments[0] ? "none" : "");
    $("#divPic").css("display", arguments[0] ? "" : "none");
}