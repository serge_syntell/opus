﻿using System;
using Stalberg.TMS.RoadblockExtractor.Components;
using Stalberg.TMS.RoadblockExtractor.Objects;
using Stalberg.TMS.Data;
using System.Collections.Generic;
using System.IO;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.EntLib;

namespace Stalberg.TMS.RoadblockExtractor
{
    /// <summary>
    /// The main class of the application
    /// </summary>
    public class Beginnings
    {
        #region Fields
        private static RBDExtractParameters parameters;

        internal static LogWriter writer;
        
        // Constants
        internal const string APP_NAME = "RoadBlockDataExtract";

        #endregion

        /// <summary>
        /// Initializes the <see cref="Beginnings"/> class.
        /// </summary>
        static Beginnings()
        {
            // Create the Log Writer
            Beginnings.writer = new LogWriter();

            // Create a new parameters class
            Beginnings.parameters = new RBDExtractParameters();
        }

        
        /// <summary>
        /// The Main entry point of the application
        /// </summary>
        /// <param name="args">Any command line parameters passed into the application</param>
        static void Main(string[] args)
        {
            // Check Last updated Version            
            string errorMessage;

            try
            {
                // Setup the Log Writer
                string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
                Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);

                // 2010/10/11 jerry add 
                if (!CheckVersionManager.CheckVersion(AartoProjectList.AARTORoadblockExtractor, out errorMessage))
                {
                    Console.WriteLine(errorMessage);
                    EntLibLogger.WriteLog(LogCategory.General, "", errorMessage);
                    return;
                }

                Beginnings.writer.WriteLine("Ticket Processor External Interfaces - started at: " + DateTime.Now.ToString());

                //bool fatalError = false;

                bool loadFailed = false;
                string lastUpdated = ProjectLastUpdated.AARTORoadblockExtractor.ToShortDateString();

                Beginnings.writer.WriteLine("Last Updated on: " + lastUpdated);

                //************************************************************************************************************************
                //          Get configuration data
                //************************************************************************************************************************

                GetData data = new GetData();
                loadFailed = data.GetStartValues(Beginnings.parameters);
                if (loadFailed == true)
                {
                    writer.WriteLine("Failed to load XML or FTP parameters at: " + DateTime.Now.ToString());
                    return;
                }

                //************************************************************************************************************************
                //          Create entry in process log
                //************************************************************************************************************************

                int nPLIntNo = 0;
                ProcessLogDB db = new ProcessLogDB(Beginnings.parameters.ConnectionString);
                try
                {
                    nPLIntNo = db.AddProcessLog(nPLIntNo, "RoadblockDataExtract", DateTime.Now, DateTime.Now, "RoadblockDataExtract");
                }
                catch (Exception e)
                {
                    writer.WriteLine("Failed to write to process log table " + e.Message);
                    writer.WriteLine("");
                }

                //************************************************************************************************************************
                //          Check whether another instance is already running
                //************************************************************************************************************************

                Singleton single = new Singleton("RoadblockDataExtract");
                single.OtherProcessesToCheckFor(new string[] { "TMS_ViolationLoader", 
                "TMS_CD_Loader", 
                "TMS_3P_Loader", 
                "Stalberg.Payfine.Extract",
                "PFB_Loader",
                "Stalberg.ThaboExporter",
                "RoadblockDataExtract"});
                if (single.Check())
                {
                    writer.WriteLine(single.Message);
                    writer.WriteLine(string.Format("An instance of {1} application is already running - processing aborted {0}.", DateTime.Now.ToString(), parameters.ProcessName));
                    writer.Close();
                    
                    return;
                }

                //************************************************************************************************************************
                //          Check whether ALL image servers are accessible 
                //************************************************************************************************************************

                ImageFileServerDB imageDB = new ImageFileServerDB(parameters.ConnectionString);
                List<ImageFileServerDetails> imageServers = imageDB.GetAllImageFileServers();
                if (imageServers.Count == 0)
                {
                    writer.WriteLine("Image file server not found. Please check image file servers in database." + DateTime.Now.ToString());
                    return;
                }

                if (imageServers.Count > 0)
                {
                    foreach (ImageFileServerDetails server in imageServers)
                    {
                        //bool isConnected = Stalberg.TMS.Data.Util.RemoteManager.RemoteConnect(
                        //                    server.ImageMachineName,
                        //                    server.ImageShareName,
                        //                    server.RemoteUserName,
                        //                    server.RemotePassword);
                        //if (isConnected == false)
                        //{
                        //    writer.WriteLine("Image file server " + server.ImageMachineName + "\\" + server.ImageShareName + " connected failed, please check the access authority.");
                        //    return;
                        //}
                        //else
                        //{
                            // Check the test image "test.jpg"
                            string strTestImagePath = string.Format(@"\\{0}\{1}\test.gif", server.ImageMachineName, server.ImageShareName);
                            string strTestImagePath2 = string.Format(@"\\{0}\{1}\test.jpg", server.ImageMachineName, server.ImageShareName);

                            if (File.Exists(strTestImagePath) == false && File.Exists(strTestImagePath2) == false)
                            {
                                writer.WriteLine("Test Image " + strTestImagePath + " not exist, please check the remote file server's configuration.");
                                return;
                            }
                        //}
                    }
                }

                //************************************************************************************************************************
                //     1.     Remove all of the expired ( older than 7 days till the date time of app execution) road block,expired road block 
                //            false images and expired( older than 7 days till the date time of app execution) road block flase number plate files 
                //************************************************************************************************************************
                GeneralFunctions gFunction = new GeneralFunctions();
                gFunction.RemoveFiles(Beginnings.parameters.RBDExtract.RoadBlockFilesFolder);

                //************************************************************************************************************************
                //     2.     RBDExtract_AARTO - Road block data extract  
                //************************************************************************************************************************
                RBDExtract rbdExtract = new RBDExtract(Beginnings.parameters);
                rbdExtract.RBDExtract_Processing();

                //************************************************************************************************************************
                //     3.     RBFNPExtract_AARTO - Road block false number plate data extract  
                //************************************************************************************************************************
                RBFNPExtract rbfnpExtract = new RBFNPExtract(Beginnings.parameters);
                rbfnpExtract.RBFNPExtract_Processing();

                //************************************************************************************************************************
                //     99.     Mail the log file
                //************************************************************************************************************************

                // Mail the log file
                try
                {
                    Beginnings.parameters.SendMail(true);
                }
                catch (Exception emailEx)
                {
                    string msg = emailEx.Message;
                }

                try
                {
                    db.UpdateProcessLog(nPLIntNo, "RoadblockDataExtract", DateTime.Now, "RoadblockDataExtract");
                }
                catch (Exception processLogEx)
                {
                    string msg = processLogEx.Message;
                }
            }
            catch (Exception ex)
            {
                Beginnings.writer.WriteLine(ex.Message);
                Beginnings.writer.WriteLine(ex.StackTrace);
                EntLibLogger.WriteErrorLog(ex.GetBaseException(),LogCategory.Exception, AartoProjectList.AARTORoadblockExtractor.ToString());
            }
        }
        
    }
}
