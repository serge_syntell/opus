﻿using System;
using System.Data.SqlClient;
using System.IO;
using Stalberg.TMS.RoadblockExtractor.Components;
using System.Text.RegularExpressions;
using Stalberg.TMS;
using System.Collections;
using System.Net.Mail;
using Stalberg.TMS.RoadblockExtractor.Objects;
using System.Data;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using Stalberg.TMS.Data;
using Stalberg.TMS.RoadblockExtractor.Utilities;
using System.Drawing;
using Stalberg.TMS_TPExInt.Components;

namespace Stalberg.TMS.RoadblockExtractor
{
    //image file name info
    public struct imageDetail
    {
        public Int32 scImIntNoA;
        public Int32 scImIntNoB;
        public string filmNo;
        public string frameNo;
        public string imageNoA;
        public string imageNoB;
    }

    /// <summary>
    /// Contains all the logic for processing Civitas data.
    /// </summary>
    internal class RBFNPExtract
    {
        // Fields
        private RBDExtractParameters parameters;

        //DLS - removed: not used anywhere
        //private ResponseProcessCodeList responseCodeList = new ResponseProcessCodeList();
        private List<imageDetail> imageFileInfos = new List<imageDetail>();
        private GeneralFunctions gFunction;
        //private Civitas_Functions cp;

        // Constants
        private const string ROADBLOCKFALSENUMBERPLATE_PROCESSOR_RBDE = "RBFNPE";               //"RBFNPE_Aarto";
        private const string ROADBLOCKFALSENUMBERPLATE_FILE_PREFIX = "Falseplate_SI";


        /// <summary>
        /// Initializes a new instance of the <see cref="CivitasProcessing"/> class.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        public RBFNPExtract(RBDExtractParameters parameters)
        {
            this.parameters = parameters;
            gFunction = new GeneralFunctions();
            //this.cp = new Civitas_Functions(this.parameters);
        }

        internal void RBFNPExtract_Processing()
        {
            if (!parameters.RBDExtract.RoadBlockFilesFolder.Equals(""))
            {
                // Save the road block files to the folders specified in sysparam.xml file 
                this.CreateRBFNPExtractFiles();

                //switching on/off the generation of images for false number plates
                if (parameters.RBDExtract.GenerateFalseNumberImages)
                this.CreateScImageFiles();
            }
            return;
        }



        private SqlDataReader GetRoadBlockFalseNumberPlateData(string mtrCode, out SqlConnection connection)
        {
            //get road block extract data 
            RoadBlockFalseNumberPlateRecord rbfnpRecord = new RoadBlockFalseNumberPlateRecord(this.parameters.ConnectionString);
            //SqlDataReader reader = rbfnpRecord.GetRoadBlockFalseNumberPlateData(arString,mtrCode, out connection);
            SqlDataReader reader = rbfnpRecord.GetRoadBlockFalseNumberPlateData(mtrCode, out connection);
            return reader;
        }


        private void CreateScImageFiles()
        {
            Beginnings.writer.WriteLine("Starting creation of image files authorities at: " + DateTime.Now.ToString());

            //save to folders
            foreach (imageDetail imageFileInfo in imageFileInfos)
            {
                if (imageFileInfo.scImIntNoA == 0)
                    continue;

                SaveImage(imageFileInfo, "A");

                if (imageFileInfo.scImIntNoB == 0)
                    continue;

                SaveImage(imageFileInfo, "B");               
            }

            Beginnings.writer.WriteLine("Completed creation of image files authorities at: " + DateTime.Now.ToString());
        }

        private void SaveImage(imageDetail imageFileInfo, string imageType)
        {
            string exportFolder = this.parameters.RBDExtract.RoadBlockFilesFolder;

            string errMessage = "";

            //string vName = exportFolder + "\\" + imageFileInfo.filmNo + "_" + imageFileInfo.frameNo + "_" + imageFileInfo.imageNoA + ".jpg";
            string vName = exportFolder + "\\" + imageFileInfo.filmNo + "_" + imageFileInfo.frameNo + "_" + imageType + ".jpg";

            if (!File.Exists(vName))
            {
                Int32 scImIntNo = (imageType == "A") ? imageFileInfo.scImIntNoA : imageFileInfo.scImIntNoB;

                //SqlDataReader reader = this.GetImageFileData(Convert.ToInt32(imageFileInfo.scImIntNoA), ref errMessage);
                SqlDataReader reader = this.GetImageFileData(Convert.ToInt32(scImIntNo), ref errMessage);

                if (reader == null)
                {
                    Beginnings.writer.WriteLine("Get image files- error: " + errMessage);
                }
                else if (!reader.HasRows)
                {
                    Beginnings.writer.WriteLine("No available Image files");
                }
                else
                {
                    while (reader.Read())
                    {
                        try
                        {
                            // David Lin 20100329 Remove images from database
                            //FT 091209 Check DBNull
                            //if (reader["ScanImage"] == DBNull.Value)
                            //{
                            //    Beginnings.writer.WriteLine("Empty image data of filmNo:" + imageFileInfo.filmNo + " frameNo:" + imageFileInfo.frameNo);
                            //    continue;
                            //}
                            //byte[] imageFile = (byte[])reader["ScanImage"];
                            ScanImageDB imgDB = new ScanImageDB(this.parameters.ConnectionString);
                            byte[] imageFile = imgDB.GetScanImageDataFromRemoteServer(Convert.ToInt32(reader["ScImIntNo"]));
                            if (imageFile == null)
                            {
                                 Beginnings.writer.WriteLine("Empty image data of filmNo:" + imageFileInfo.filmNo + " frameNo:" + imageFileInfo.frameNo);
                                 continue;
                            }
                            MemoryStream ms_vImage = new MemoryStream(imageFile);
                            Image vImage = new Bitmap(ms_vImage);
                            vImage.Save(vName, System.Drawing.Imaging.ImageFormat.Jpeg);
                        }
                        catch (Exception e)
                        {
                            Beginnings.writer.WriteLine("Image file generation failed for " + imageFileInfo.filmNo + "_" + imageFileInfo.frameNo + "_" + imageType + ".jpg - Error: " + e.Message);
                        }
                    }
                }
                reader.Dispose();
            }
        }

        private void CreateRBFNPExtractFiles()
        {
            string errMessage = "";
            int prevAutIntNo = 0;
            int countRoadBlockFalseNumberFiles = 0;
            string fileName = "";
            imageFileInfos.Clear();

            Beginnings.writer.WriteLine("Starting creation of road block false number plate extract files for " + ROADBLOCKFALSENUMBERPLATE_PROCESSOR_RBDE + " authorities at: " + DateTime.Now.ToString());

            //save to folders
            string exportFolder = this.parameters.RBDExtract.RoadBlockFilesFolder;

           
            SqlDataReader reader = this.GetAuthListForCreatingRoadBlockDataExtract(ref errMessage);

            if (reader == null)
            {
                Beginnings.writer.WriteLine("GetAuthList - error: " + errMessage);
                reader.Dispose();
                return;
            }
            else if (!reader.HasRows)
            {
                Beginnings.writer.WriteLine("GetAuthList - no authority data for the metro");
            }
            else
            {
                while (reader.Read())
                {
                    int autIntNo = Convert.ToInt32(reader["AutIntNo"]);
                    string mtrCode = reader["MtrCode"].ToString();
                    //string autCode = reader["AutCode"].ToString();

                    int noOfRecords = 0;

                    bool bGenerate = gFunction.IsGenerateRBDFile(prevAutIntNo, reader, autIntNo, "2620", this.parameters, 1);

                    if (bGenerate)
                    {
                        Beginnings.writer.WriteLine("Create Road Block False Number Plate Files started for metro: " + reader["MtrName"].ToString());

                        bool failed = this.WriteRoadBlockFile(mtrCode, autIntNo, ref noOfRecords, exportFolder, ref fileName);

                        if (!failed)
                        {
                            countRoadBlockFalseNumberFiles++;
                            //string exportFullPath = exportFolder + fileName;
                            //if (!Directory.Exists(exportFolder))
                            //    Directory.CreateDirectory(exportFolder);

                        }

                        if (noOfRecords == 0)
                        {
                            Beginnings.writer.WriteLine("No records for Road Block False Number Plate Files for metro: " + reader["MtrName"].ToString());
                        }
                    }

                    prevAutIntNo = autIntNo;
                }

                Beginnings.writer.WriteLine("Created " + countRoadBlockFalseNumberFiles.ToString() + " Road block false number plate files");

                Beginnings.writer.WriteLine("Completed creation of road block false number plate files for " + ROADBLOCKFALSENUMBERPLATE_PROCESSOR_RBDE + " authorities data at: " + DateTime.Now.ToString());
            }

            reader.Dispose();

            return;

        }

        internal bool WriteRoadBlockFile(string mtrCode, int autIntNo, ref int noOfRecords, string roadBlockfileFolder, ref string fileName)
        {
           
            bool failed = false;
            SqlConnection connection = new SqlConnection();
            
            //not necessary to check Aarto rule for falso no. plate extract
            //string rbdRec = string.Empty;
            ////check rule for each metro
            //AuthorityRulesDetails ard = new AuthorityRulesDetails();
            //ard.AutIntNo = autIntNo;
            //ard.ARCode = "2600";
            //ard.LastUser = parameters.ProcessName;

            //DefaultAuthRules ar = new DefaultAuthRules(ard, parameters.ConnectionString);
            //KeyValuePair<int, string> rbde = ar.SetDefaultAuthRule();

            // Initialise and create a Character Translator for Civitas data
            CharacterTranslator.Initialise(this.parameters.ConnectionString);
            CharacterTranslator translator = new CharacterTranslator();
            RoadBlockFalseNumberPlateData recRBFNPD = new RoadBlockFalseNumberPlateData();
            //SqlDataReader reader = this.GetRoadBlockFalseNumberPlateData(rbde.Value, mtrCode, out connection);
            SqlDataReader reader = this.GetRoadBlockFalseNumberPlateData(mtrCode, out connection);

            // Check for empty recordset

            // create the csv file
            if (reader.HasRows)
            {
                if (!Directory.Exists(roadBlockfileFolder))
                    Directory.CreateDirectory(roadBlockfileFolder);

                string fileGenerateDate = DateTime.Now.ToString("dd-MM-yyyy");
                string exportFullPath = roadBlockfileFolder + "\\" + ROADBLOCKFALSENUMBERPLATE_FILE_PREFIX + "_" + mtrCode.Trim() + "_" + fileGenerateDate + ".csv";
                fileName = Path.GetFileName(exportFullPath);

                FileStream fs = new FileStream(exportFullPath, FileMode.Create, FileAccess.Write);
                //FT 091209 add using for StreamWriter
                using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.ASCII))
                {
                    string strTemp;
                    //string strName;


                    try
                    {
                        while (reader.Read())
                        {
                            //upload the value from database to the road block DTO
                            recRBFNPD.filmNo = gFunction.GetValue(reader["Film Number"], 0);
                            recRBFNPD.frameNo = gFunction.GetValue(reader["Frame Number"], 0);
                            recRBFNPD.registrationNo = gFunction.GetValue(reader["Registration number"], 0);
                            strTemp = gFunction.GetValue(reader["Location description"], 0);
                            //if (!translator.TryParseAddress(strTemp, out strName))
                            //{
                            //    Beginnings.writer.WriteLine("There was an error translating the Location descriptionn. It was {0}, {1} was sent to Civitas.", strTemp, strName);
                            //}
                            //recRBFNPD.locationDesc = strName;
                            //strName = "";

                            recRBFNPD.locationDesc = gFunction.GetValue(reader["Location description"], 0);
                            recRBFNPD.offenceDateTime = gFunction.GetValue(reader["Offence Date and Time"], 3);
                            recRBFNPD.offenceVehicleType = gFunction.GetValue(reader["Vehicle Type on Offence"], 0);
                            recRBFNPD.offenceVehicleDesc = gFunction.GetValue(reader["Vehicle description on offence"], 0);
                            recRBFNPD.natisVehicleType = gFunction.GetValue(reader["Vehicle type on NATIS"], 0);
                            recRBFNPD.natisVehicleDesc = gFunction.GetValue(reader["Vehicle description on NATIS"], 0);
                            recRBFNPD.natisVehicleLicence = gFunction.GetValue(reader["Vehicle Licence on NATIS"], 2);
                            recRBFNPD.natisVehicleLicenceExpire = gFunction.GetValue(reader["Vehicle Licence Expire on NATIS"], 2);
                            recRBFNPD.natisVINNo = gFunction.GetValue(reader["VIN Number on NATIS"], 0);
                            recRBFNPD.natisEngineNo = gFunction.GetValue(reader["Engine Number on NATIS"], 0);
                            recRBFNPD.natisVehicleColour = gFunction.GetValue(reader["Vehicle Colour on NATIS"], 0);
                            recRBFNPD.recType = gFunction.GetValue(reader["Type of record"], 0);
                            recRBFNPD.offenceDesc = gFunction.GetValue(reader["Offence description"], 0);
                            recRBFNPD.imageNoA = gFunction.GetValue(reader["ImageNumberA"], 0);
                            recRBFNPD.imageNoB = gFunction.GetValue(reader["ImageNumberB"], 0);

                            //dls 090911 - need to handle A and B scans for the same frame
                            imageDetail imageDetail = new imageDetail();

                            imageDetail.scImIntNoA = Convert.ToInt32(reader["ScImIntNoA"]);
                            imageDetail.scImIntNoB = Convert.ToInt32(reader["ScImIntNoB"]);
                            imageDetail.filmNo = recRBFNPD.filmNo;
                            imageDetail.frameNo = recRBFNPD.frameNo;
                            imageDetail.imageNoA = recRBFNPD.imageNoA;
                            imageDetail.imageNoB = recRBFNPD.imageNoB;
                            imageFileInfos.Add(imageDetail);

                            String strRecord = recRBFNPD.Write();
                            sw.Write(strRecord);
                            noOfRecords++;
                            if (noOfRecords <= 0)
                                failed = false;
                        }
                    }
                    catch (Exception e)
                    {
                        Beginnings.writer.WriteLine("RoadBlockFalseNumberPlateFile failed " + e.Message + " " + exportFullPath + " " + DateTime.Now.ToString());
                        failed = true;
                        sw.Close();
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                            connection.Dispose();
                    }

                    if (!failed)
                    {
                        if (noOfRecords > 0)
                        {
                            //FT 091209	Add the record count
                            sw.Write(noOfRecords.ToString());
                            //Road block file is generated successfully,set AutRoadblockExtractDate to the current time.
                            string AutRoadBlockFalseNumberPlateExtractDate = DateTime.Now.ToString();
                            Beginnings.writer.WriteLine("Authority road block false number plate extract date is " + AutRoadBlockFalseNumberPlateExtractDate + " for metro code " + mtrCode);
                            int affectedCount = this.UpdateAutRoadBlockFalseNumberPlateExtractDate(AutRoadBlockFalseNumberPlateExtractDate, mtrCode);
                            if (affectedCount < 0)
                                Beginnings.writer.WriteLine("Authority road block false number plate extract date update failed for metro code " + mtrCode);
                            else if (affectedCount == 0)
                                Beginnings.writer.WriteLine("No available authority for the metro code " + mtrCode + " to be updated.");
                            else
                                Beginnings.writer.WriteLine("Authority road block false number plate extract date updated for metro code " + mtrCode);
                            //Update batch with contravention date
                        }
                    }
                    else
                    {
                        failed = true;
                    }
                }
            }
            else
                failed = true;



            return failed;
        }

        internal SqlDataReader GetAuthListForCreatingRoadBlockDataExtract(ref string errMessage)
        {
            TMSData list = new TMSData(this.parameters.ConnectionString);
            SqlDataReader authList = list.GetAuthListForCreatingRoadBlockDataExtract(ref errMessage);
            return authList;
        }

        internal SqlDataReader GetImageFileData(int scImIntNo,ref string errMessage)
        {
            TMSData list = new TMSData(this.parameters.ConnectionString);
            SqlDataReader imageFileData = list.GetImageFileData(scImIntNo,ref errMessage);
            return imageFileData;
        }

        internal int UpdateAutRoadBlockFalseNumberPlateExtractDate(string AutRoadBlockFalseNumberPlateExtractDate, string mtrCode)
        {
            TMSData db = new TMSData(this.parameters.ConnectionString);
            int affectedCount = db.UpdateAutRoadBlockFalseNumberPlateExtractDate(AutRoadBlockFalseNumberPlateExtractDate, mtrCode, Beginnings.APP_NAME);
            return affectedCount;
        }

    }
}
