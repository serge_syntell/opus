﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;

namespace Stalberg.TMS.RoadblockExtractor.Objects
{
    /// <summary>
    /// Contains all the TPExInt program parameter data
    /// </summary>
    internal class RBDExtractParameters
    {
        // Fields
        private string connectionString = string.Empty;
        private string processName = string.Empty;
        private string systemEmail = string.Empty;
        private string hostServer = string.Empty;
        private string smtp = string.Empty;

        private RBDExtract rbdExtract;


        /// <summary>
        /// Initializes a new instance of the <see cref="TPExIntParameters"/> class.
        /// </summary>
        public RBDExtractParameters()
        {

            this.rbdExtract = new RBDExtract();
        }

        #region Properties



        public RBDExtract RBDExtract
        {
            get { return this.rbdExtract; }
        }

      

        /// <summary>
        /// Gets or sets the name of the process.
        /// </summary>
        /// <value>The name of the process.</value>
        public string ProcessName
        {
            get { return processName; }
            set { processName = value; }
        }

        /// <summary>
        /// Gets or sets the database connection string.
        /// </summary>
        /// <value>The connection string.</value>
        public string ConnectionString
        {
            get { return connectionString; }
            set { connectionString = value; }
        }

        public string SystemEmail
        {
            get { return systemEmail; }
            set { systemEmail = value; }
        }

        public string SMTP
        {
            get { return smtp; }
            set { smtp = value; }
        }

        public string HostServer
        {
            get { return hostServer; }
            set { hostServer = value; }
        }

        public void SendMail(bool closeLogWriter)
        {
            string message = "On the " + DateTime.Now.ToString() + " the following log file was created by the " + this.processName + " process:\n\n"
                   + "File name: " + Beginnings.writer.FileName + "\n\n"
                   + "The file is attached to this email.\n\n"
                   + "Please check the file status.\n\n"
                   + "Thank you very much.\n\n"
                   + "Regards\n"
                   + "TMS System Administrator";

            if (closeLogWriter)
                Beginnings.writer.Close();

            List<string> fileAttachments = new List<string>();
            fileAttachments.Add(Beginnings.writer.FileName);

            this.SendMail(message, this.processName + " log file", fileAttachments, closeLogWriter);

        }

        public void SendMail(string message, string subject, List<string> fileAttachments, bool closeLogWriter)
        {
            try
            {
                this.SendMail(this.SystemEmail, subject, message, fileAttachments);

                if (closeLogWriter)
                {
                    // Create a local log file confirming that the log was sent
                    Beginnings.writer = new LogWriter(Beginnings.writer.FileName);
                    Beginnings.writer.WriteLine(string.Format("Main: emailed '{0}' to {1} at: {2}.", subject, this.systemEmail, DateTime.Now));
                }

            }
            catch (Exception emailEx)
            {
                //Beginnings.writer.WriteLine("Main: Failed to send email file " + emailEx.Message);
            }
        }

        public void SendMail(string email, string subject, string message, List<string> attachments)
        {
            try
            {
                // Create the mail
                MailAddress from = new MailAddress(this.SystemEmail);
                MailAddress to = new MailAddress(email);

                MailMessage mail = new MailMessage(from, to);
                mail.Subject = this.HostServer.ToUpper() + " " + subject;
                mail.Body = message;
                mail.BodyEncoding = System.Text.Encoding.UTF8;

                foreach (string fileName in attachments)
                {
                    Attachment myAttachment = new Attachment(fileName);
                    mail.Attachments.Add(myAttachment);
                }

                // Connect to the server
                try
                {
                    SmtpClient mailClient = new SmtpClient();
                    mailClient.Host = this.SMTP;
                    mailClient.UseDefaultCredentials = true;
                    mailClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;

                    mailClient.Send(mail);
                }
                catch (Exception smtpEx)
                {
                    Beginnings.writer.WriteLine("Main: Failed to send email file " + smtpEx.Message);
                }
                finally
                {
                    foreach (Attachment at in mail.Attachments)
                    {
                        at.ContentStream.Close();
                    }
                    mail.Attachments.Dispose();
                }
            }
            catch (Exception emailEx)
            {
                Beginnings.writer.WriteLine("Main: Failed to send email file " + emailEx.Message);
            }
        }
        #endregion


    }
}
