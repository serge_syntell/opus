﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stalberg.TMS.RoadblockExtractor.Objects
{
    /// <summary>
    /// Represents the details that a client needs to know about Thabo
    /// </summary>
    internal class RBDExtract
    {
        // Fields
        private string roadBlockFilesFolder = string.Empty;
        private bool generateFalseNumberImages;
        

        /// <summary>
        /// Initializes a new instance of the <see cref="CivitasPI"/> class.
        /// </summary>
        public RBDExtract()
        {
        }

        /// <summary>
        /// Gets or sets the  files folder.
        /// </summary>
        /// <value>The Road black files folder.</value>
        public string RoadBlockFilesFolder
        {
            get { return this.roadBlockFilesFolder; }
            set { this.roadBlockFilesFolder = value; }
        }

        /// <summary>
        ///  Gets or sets GenerateFalseNumberImages.
        /// </summary>
        public bool GenerateFalseNumberImages
        {
            get { return generateFalseNumberImages; }
            set { generateFalseNumberImages = value; }
        }

       
    }
}
