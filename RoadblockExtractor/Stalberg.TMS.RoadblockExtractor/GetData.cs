﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using Stalberg.TMS.RoadblockExtractor.Components;
using System.Text.RegularExpressions;
using Stalberg.TMS;
using System.Collections;
using Stalberg.TMS.RoadblockExtractor.Objects;

namespace Stalberg.TMS.RoadblockExtractor
{
    /// <summary>
    /// Contains all the methods to get the data
    /// </summary>
    internal class GetData
    {
        // Fields


        /// <summary>
        /// Initializes a new instance of the <see cref="GetData"/> class.
        /// </summary>
        public GetData()
        {
        }

        /// <summary>
        /// Gets the start values.
        /// </summary>
        /// <param name="connStr">The conn STR.</param>
        /// <param name="procStr">The proc STR.</param>
        /// <param name="localFtpParm">The local FTP parm.</param>
        /// <param name="writer">The writer.</param>
        /// <returns></returns>
        public bool GetStartValues(RBDExtractParameters parameters)
        {
            bool failed = false;
            // get the environmental parameters
            // xml file to get server name & FTP process name
            this.GetServerXML(parameters);
            if (parameters.ConnectionString.Length == 0)
            {
                Beginnings.writer.WriteLine("GetServerXML failed at: " + DateTime.Now.ToString());
                failed = true;
                return failed;
            }
            return failed;
        }

        /// <summary>
        /// Gets the server XML.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        public void GetServerXML(RBDExtractParameters parameters)
        {
            //load xml file with system parameters
            string currentDir = Directory.GetCurrentDirectory();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(currentDir + "\\SysParam.xml");

            // Assign the contents of the child nodes to application variables
            XmlNode n = null;
            if (xmlDoc.DocumentElement.HasChildNodes)
            {
                foreach (XmlNode node in xmlDoc.DocumentElement.ChildNodes)
                {
                    if (node.NodeType != XmlNodeType.Element)
                        continue;

                    switch (node.Name)
                    {
                        case "constr":
                            parameters.ConnectionString = node.InnerText;
                            break;

                        case "procstr":
                            parameters.ProcessName = node.InnerText; 
                            break;

                        case "smtp":
                            parameters.SMTP = node.InnerText;
                            break;

                        case "hostServer":
                            parameters.HostServer = node.InnerText;
                            break;

                        case "systemEmail":
                            parameters.SystemEmail = node.InnerText;
                            break;

                        case "rbdExtract":
                            n = xmlDoc.SelectSingleNode("/codes/rbdExtract/roadBlackFilesFolder");
                            if (n != null)
                                parameters.RBDExtract.RoadBlockFilesFolder = n.InnerText;

                            n = xmlDoc.SelectSingleNode("/codes/rbdExtract/generateFalseNumberImages");
                            if (n != null)
                                parameters.RBDExtract.GenerateFalseNumberImages = n.InnerText.Trim().ToLower() == "true";
                            break;
                        
                    }
                }

               
            }
        }

    }
}
