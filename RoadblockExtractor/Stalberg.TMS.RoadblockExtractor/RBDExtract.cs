﻿using System;
using System.Data.SqlClient;
using System.IO;
using Stalberg.TMS.RoadblockExtractor.Components;
using System.Text.RegularExpressions;
using Stalberg.TMS;
using System.Collections;
using System.Net.Mail;
using Stalberg.TMS.RoadblockExtractor.Objects;
using System.Data;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using Stalberg.TMS.Data;
using Stalberg.TMS.RoadblockExtractor.Utilities;
using Stalberg.TMS_TPExInt.Components;


namespace Stalberg.TMS.RoadblockExtractor
{
    /// <summary>
    /// Contains all the logic for processing Civitas data.
    /// </summary>
    internal class RBDExtract
    {
        // Fields
        private RBDExtractParameters parameters;

        //private ResponseProcessCodeList responseCodeList = new ResponseProcessCodeList();
        //private Civitas_Functions cp;

        // Constants
        private const string ROADBLOCK_PROCESSOR_RBDE = "RDBE";           //"RDBE_Aarto";
        private const string ROADBLOCK_FILE_PREFIX = "ade";
        private GeneralFunctions gFunction;

        /// <summary>
        /// Initializes a new instance of the <see cref="CivitasProcessing"/> class.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        public RBDExtract(RBDExtractParameters parameters)
        {
            this.parameters = parameters;
            gFunction = new GeneralFunctions();

            //this.cp = new Civitas_Functions(this.parameters);
        }

        internal void RBDExtract_Processing()
        {
            if (!parameters.RBDExtract.RoadBlockFilesFolder.Equals(""))
            {
                // Save the road block files to the folders specified in sysparam.xml file 
                this.CreateRBDExtractFiles();
            }
            return;
        }


        private SqlDataReader GetRoadBlockExtractData(string arString,string mtrCode,out SqlConnection connection)
        {
            //get road block extract data 
            RoadBlockRecord roadBlockRecord = new RoadBlockRecord(this.parameters.ConnectionString);
            SqlDataReader reader = roadBlockRecord.GetRoadBlockData(arString, mtrCode, out connection);
            return reader;
        }

        private void CreateRBDExtractFiles()
        {
            string errMessage = "";
            int prevAutIntNo = 0;
            int countRoadBlockFiles = 0;
            string fileName = "";

            Beginnings.writer.WriteLine("Starting creation of road block extract files for " + ROADBLOCK_PROCESSOR_RBDE + " authorities at: " + DateTime.Now.ToString());

            //save to folders
            string exportFolder = this.parameters.RBDExtract.RoadBlockFilesFolder;

            SqlDataReader reader = this.GetAuthListForCreatingRoadBlockDataExtract(ref errMessage);

            if (reader == null)
            {
                Beginnings.writer.WriteLine("GetAuthList - error: " + errMessage);
                reader.Dispose();
                return;
            }
            else if (!reader.HasRows)
            {
                Beginnings.writer.WriteLine("GetAuthList - no authority data for the metro");
            }
            else
            {
                while (reader.Read())
                {
                    int autIntNo = Convert.ToInt32(reader["AutIntNo"]);
                    string mtrCode = reader["MtrCode"].ToString();
                    //string autCode = reader["AutCode"].ToString();
                    string autNo = reader["autNo"].ToString();

                    int noOfRecords = 0;

                    //Beginnings.writer.WriteLine("Create Road Block Files started for metro: " + reader["MtrName"].ToString());
                    bool bGenerate = gFunction.IsGenerateRBDFile(prevAutIntNo, reader, autIntNo, "2610", this.parameters, 0);

                    if (bGenerate)
                    {
                        Beginnings.writer.WriteLine("Create Road Block Files started for metro: " + reader["MtrName"].ToString());

                        bool failed  = this.WriteRoadBlockFile(mtrCode,autNo,autIntNo, ref noOfRecords, exportFolder, ref fileName);

                        if (!failed)
                        {
                            countRoadBlockFiles++;
                            //string exportFullPath = exportFolder + "\\" + fileName;
                            //if (!Directory.Exists(exportFolder))
                            //    Directory.CreateDirectory(exportFolder);

                        }

                        if (noOfRecords == 0)
                        {
                            Beginnings.writer.WriteLine("No records for Road Block Files for metro: " + reader["MtrName"].ToString());
                        }

                    }

                    prevAutIntNo = autIntNo;
                }

                Beginnings.writer.WriteLine("Created " + countRoadBlockFiles.ToString() + " Road block files");

                Beginnings.writer.WriteLine("Completed creation of road block extract files for " + ROADBLOCK_PROCESSOR_RBDE + " authorities data at: " + DateTime.Now.ToString());
            }

            reader.Dispose();

            return;
        }

       

        internal bool WriteRoadBlockFile(string mtrCode,string autNo,int autIntNo, ref int noOfRecords, string roadBlockfileFolder, ref string fileName)
        {
            bool failed = false;
            SqlConnection connection = new SqlConnection();
            string rbdRec = string.Empty;

            //check rule for each metro
            AuthorityRulesDetails ard = new AuthorityRulesDetails();
            ard.AutIntNo = autIntNo;
            ard.ARCode = "2600";
            ard.LastUser = parameters.ProcessName;

            DefaultAuthRules ar = new DefaultAuthRules(ard, parameters.ConnectionString);
            KeyValuePair<int, string> rbde = ar.SetDefaultAuthRule();
           

            // Initialise and create a Character Translator for Civitas data
            CharacterTranslator.Initialise(this.parameters.ConnectionString);
            CharacterTranslator translator = new CharacterTranslator();
            RoadBlockData recRBD = new RoadBlockData();
            SqlDataReader reader = this.GetRoadBlockExtractData(rbde.Value,mtrCode, out connection);
            
            // Check for empty recordset
            

            // create the csv file
            if (reader.HasRows)
            {
                if (!Directory.Exists(roadBlockfileFolder))
                    Directory.CreateDirectory(roadBlockfileFolder);

                string fileGenerateDate = DateTime.Now.ToString("yyyyMMdd");
                string exportFullPath = roadBlockfileFolder + "\\" + ROADBLOCK_FILE_PREFIX + "_" + mtrCode.Trim() + "_" + fileGenerateDate+".txt";
                fileName = Path.GetFileName(exportFullPath);

                FileStream fs = new FileStream(exportFullPath, FileMode.Create, FileAccess.Write);
                //FT 091209 add using for StreamWriter
                using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.ASCII))
                {
                    string strTemp;
                    string strName;

                    //dls 2011-12-14 - change request from M Roets (Roadblock Data extract of active offences Ref 007)
                    try
                    {
                        while (reader.Read())
                        {
                            //upload the value from database to the road block DTO
                            recRBD.recType = "02";
                            recRBD.filter1 = "00";                          //dls 2011-12-14   string.Empty;
                            recRBD.localauthCode = autNo.Trim().Substring(0,3);            //dls 2011-12-14autCode.Trim();
                            recRBD.filter2 = "00";                          //dls 2011-12-14 string.Empty;
                            recRBD.filter3 = "0";                           //dls 2011-12-14 string.Empty;
                            recRBD.noticeNo = gFunction.GetValue(reader["NotTicketNo"], 0).Replace("/", "").Replace("-", "");
                            recRBD.recIdentifier = gFunction.GetValue(reader["CSRoadblockIdentifier"], 0);
                            recRBD.filmNo = gFunction.GetValue(reader["NotFilmNo"], 0);
                            recRBD.frameNo = gFunction.GetValue(reader["NotFrameNo"], 0);
                            recRBD.OffenderIdentityNo = gFunction.GetValue(reader["Offender Identity number"], 0);
                            strTemp = gFunction.GetValue(reader["Offender Surname"], 0);
                            if (!translator.TryParseName(strTemp, out strName))
                            {
                                Beginnings.writer.WriteLine("There was an error translating the Offender Surname. It was {0}, {1} was sent to Civitas.", strTemp, strName);
                            }
                            recRBD.surname = strName;
                            strName = "";

                            strTemp = gFunction.GetValue(reader["Offender Initials"], 0);
                            if (!translator.TryParseName(strTemp, out strName))
                            {
                                Beginnings.writer.WriteLine("There was an error translating the Offender Initials. It was {0}, {1} was sent to Civitas.", strTemp, strName);
                            }
                            recRBD.initials = strName;
                            strName = "";

                            recRBD.vehicleNo = gFunction.GetValue(reader["Vehicle Registration number"], 0);
                            recRBD.offenceDate = gFunction.GetValue(reader["DateTime of offence"], 0);
                            recRBD.offenceTime = gFunction.GetValue(reader["DateTime of offence"], 1); ;
                            recRBD.firstSpeedReading = gFunction.GetValue(reader["First Speed reading"], 0);

                            //strTemp = gFunction.GetValue(reader["Location where offender offended"], 0).Replace("/", " ");
                            //if (!translator.TryParseName(strTemp, out strName))
                            //{
                            //    Beginnings.writer.WriteLine("There was an error translating the Location where offender offended. It was {0}, {1} was sent to Civitas.", strTemp, strName);
                            //}
                            //recRBD.locationOffended = strName;
                            recRBD.locationOffended = gFunction.GetValue(reader["Location where offender offended"], 0);
                            strName = "";

                            recRBD.courtCode = gFunction.GetValue(reader["Court code"], 0);
                            recRBD.courtDate = gFunction.GetValue(reader["Court date"], 0);
                            recRBD.caseNo = gFunction.GetValue(reader["Case number"], 0).Replace("/", "");
                            recRBD.summonsNo = gFunction.GetValue(reader["Summons number"], 0).Replace("/", "");
                            recRBD.summonsIssueDate = gFunction.GetValue(reader["Summons issue date"], 0);
                            recRBD.woaNo = gFunction.GetValue(reader["Warrant of Arrest number"], 0).Replace("/", "");
                            recRBD.woaPrintedDate = gFunction.GetValue(reader["Warrant of Arrest date printed"], 0);
                            recRBD.representation = gFunction.GetValue(reader["Representation"], 0);
                            recRBD.offenceCode1 = gFunction.GetValue(reader["Offence code"], 0);
                            recRBD.chargeType1 = gFunction.GetValue(reader["Type of charge"], 0);
                            recRBD.fineAmount1 = gFunction.GetValue(reader["Fine amount(cents)"], 0);
                            recRBD.chargeResult1 = string.Empty;
                            recRBD.offenceCode2 = string.Empty;
                            recRBD.chargeType2 = string.Empty;
                            recRBD.fineAmount2 = string.Empty;
                            recRBD.chargeResult2 = string.Empty;
                            recRBD.offenceCode3 = string.Empty;
                            recRBD.chargeType3 = string.Empty;
                            recRBD.fineAmount3 = string.Empty;
                            recRBD.chargeResult3 = string.Empty;
                            recRBD.offenceCode4 = string.Empty;
                            recRBD.chargeType4 = string.Empty;
                            recRBD.fineAmount4 = string.Empty;
                            recRBD.chargeResult4 = string.Empty;
                            recRBD.offenceCode5 = string.Empty;
                            recRBD.chargeType5 = string.Empty;
                            recRBD.fineAmount5 = string.Empty;
                            recRBD.chargeResult5 = string.Empty;
                            sw.Write(recRBD.Write());
                            noOfRecords++;
                            if (noOfRecords <= 0)
                                failed = false;
                        }
                    }
                    catch (Exception e)
                    {
                        Beginnings.writer.WriteLine("RoadBlockFile failed " + e.Message + " " + exportFullPath + " " + DateTime.Now.ToString());
                        failed = true;
                        sw.Close();
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                            connection.Dispose();
                    }

                    if (!failed)
                    {
                        if (noOfRecords > 0)
                        {
                            char cTemp = Convert.ToChar("0");

                            // Write out the up control record - add 1 to the counter to include the 54
                            sw.Write("99" + "000" + (noOfRecords + 1).ToString().PadLeft(8, cTemp));
                            sw.Close();
                            fs.Close();

                            //road block file is generated successfully,set AutRoadblockExtractDate to the current time.
                            string AutRoadblockExtractDate = DateTime.Now.ToString();
                            Beginnings.writer.WriteLine("Authority road block extract date is " + AutRoadblockExtractDate + " for metro code " + mtrCode);
                            int affectedCount = this.UpdateAutRoadblockExtractDate(AutRoadblockExtractDate, mtrCode);
                            if (affectedCount < 0)
                                Beginnings.writer.WriteLine("Authority road block extract date update failed for metro code " + mtrCode);
                            else if (affectedCount == 0)
                                Beginnings.writer.WriteLine("No available authority for the metro code " + mtrCode + " to be updated.");
                            else
                                Beginnings.writer.WriteLine("Authority road block extract date has been updated for metro code " + mtrCode);
                            // Update batch with contravention date
                        }
                    }
                    else
                    {
                        failed = true;
                    }
                }
            }
            else
                failed = true;

            
            
            return failed;
        }

        internal SqlDataReader GetAuthListForCreatingRoadBlockDataExtract(ref string errMessage)
        {
            TMSData list = new TMSData(this.parameters.ConnectionString);
            SqlDataReader authList = list.GetAuthListForCreatingRoadBlockDataExtract(ref errMessage);
            return authList;
        }

        

        internal int UpdateAutRoadblockExtractDate(string AutRoadblockExtractDate, string mtrCode)
        {
            TMSData db = new TMSData(this.parameters.ConnectionString);
            int affectedCount = db.UpdateAutRoadblockExtractDate(AutRoadblockExtractDate, mtrCode, Beginnings.APP_NAME);
            return affectedCount;
        }

    }
}
