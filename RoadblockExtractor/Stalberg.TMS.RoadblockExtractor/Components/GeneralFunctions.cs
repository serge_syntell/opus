﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Drawing;
using EnterpriseDT.Net.Ftp;
using EnterpriseDT.Util.Debug;
using Stalberg.TMS;
using Stalberg.TMS_TPExInt.Objects;
using System.Collections.Generic;
using Stalberg.TMS.Data.Util;
using Stalberg.TMS.RoadblockExtractor.Objects;
using System.Globalization;

namespace Stalberg.TMS.RoadblockExtractor.Components
{
    /// <summary>
    /// Represents a group of General Functions for the road block file generation.
    /// </summary>
    internal class GeneralFunctions
    {
        public GeneralFunctions()
        {
        }

        /// <summary>
        /// Convert the format of value to the designated format.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="iFlag">Format 1:Hour+Month 2:Year/Month/Day Others:Default</param>
        /// <returns>return the value in form of designated format.</returns>
        internal string GetValue(object value, int iFlag)
        {
            if (value.GetType() == typeof(DateTime))
            {
                if (iFlag == 1)
                    return (value == DBNull.Value ? "" : Convert.ToDateTime(value).ToString("HHmm"));
                else if(iFlag == 2)
                    return (value == DBNull.Value ? "" : Convert.ToDateTime(value).ToString("yyyy/MM/dd"));
                else if (iFlag == 3)
                    //FT 091209 We don’t store the seconds in the OffenceDate
                    return (value == DBNull.Value ? "" : Convert.ToDateTime(value).ToString("dd/MM/yyyy HH:mm"));
                else
                    return (value == DBNull.Value ? "" : Convert.ToDateTime(value).ToString("yyyyMMdd"));
            }
            else
                //dls 090914 - can't do a replace on "-" - it is needed for the addresses - only remove on the notices, so add inline
                //return (value == DBNull.Value ? string.Empty : value.ToString().Replace(",", " ").Replace("\"", "").Replace("'", "").Replace("/", "").Replace("-", ""));
                return (value == DBNull.Value ? string.Empty : value.ToString().Replace(",", " ").Replace("\"", "").Replace("'", "").Replace(System.Environment.NewLine, " ").Trim());
        }

        internal bool IsGenerateRBDFile(int prevAutIntNo, SqlDataReader reader, int autIntNo, string AuthRuleCode, RBDExtractParameters parameters,int flag)
        {
            //QA - dls 090902 - bad use of variable name
            bool generateFile=false;
            DateTime day = DateTime.MinValue;
            AuthorityRulesDB authRules = new AuthorityRulesDB(parameters.ConnectionString);
            if (prevAutIntNo != autIntNo)
            {
                //QA - dls 090902 - this rule is in the wrong place

                AuthorityRulesDetails ard = new AuthorityRulesDetails();
                ard.AutIntNo = autIntNo;
                ard.ARCode = AuthRuleCode;
                ard.LastUser = parameters.ProcessName;

                DefaultAuthRules ar = new DefaultAuthRules(ard, parameters.ConnectionString);
                KeyValuePair<int, string>  rbde = ar.SetDefaultAuthRule();

                int intHours = Convert.ToInt32(rbde.Key);
                if (AuthRuleCode == "2610")
                {
                    if (flag == 0 && reader["AutRoadblockExtractDate"] != DBNull.Value)
                        day = Convert.ToDateTime(reader["AutRoadblockExtractDate"]);
                }
                else
                {
                    if (reader["AutRoadBlockExtractFalseNumberPlateDate"] != DBNull.Value)
                        day = Convert.ToDateTime(reader["AutRoadBlockExtractFalseNumberPlateDate"]);
                }

                //QA - dls 090902 - if day is still = DateTime.MinValue (null dates - Extract has never been run) then this will never be set
                if (day.AddHours(24).Date <= DateTime.Now.Date )//&& DateTime.Now.Hour == intHours)
                    generateFile = true;
                else
                    generateFile = false;
          
            }
            return generateFile;
        }


        internal void RemoveFiles(string exportFolder)
        {
            //Delete all files with the roadblock extract signature older than 7 days from the FTP folder
            try
            {
                if (Directory.Exists(exportFolder))
                {
                    string[] files = Directory.GetFiles(exportFolder);
                    foreach (string file in files)
                    {
                        if (Path.GetExtension(file) != ".jpg")
                        {
                            string fileName = Path.GetFileNameWithoutExtension(file);
                            string[] array = fileName.Split('_');
                            DateTime dt = new DateTime();
                            if (array.Length == 3 && DateTime.TryParse(array[2].Insert(4, "-").Insert(7, "-"), out dt) && dt.AddDays(7) < DateTime.Now)
                            {
                                File.Delete(file);
                            }
                            else if (array.Length == 4 && DateTime.TryParse(array[3], CultureInfo.CreateSpecificCulture("fr-FR"), DateTimeStyles.None, out dt) && dt.AddDays(7) < DateTime.Now)
                            {
                                File.Delete(file);
                            }
                        }
                        else
                        {
                            FileInfo imageFileInfo = new FileInfo(file);
                            if(imageFileInfo.CreationTime.AddDays(7) < DateTime.Now)
                                File.Delete(file);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Beginnings.writer.WriteLine("Delete expired files failed! " + ex.Message);
            }

        }

    }
}

