﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Stalberg.Payfine
{
    public class RemoteManager
    {
        public RemoteManager()
        {

        }

        public static bool RemoteConnect(string remoteHost, string shareName, string userName, string passWord)
        {
            string StrConnectCommand = string.Format(@"NET USE \\{0}\{1} /User:{2} {3} /Persistent:Yes", remoteHost, shareName, userName, passWord);
            bool connectResult = RemoteCommand(StrConnectCommand);
            if (connectResult == false)
            {
                // if some error occour run disconnect and reconnect again only one time
                bool disConnectResult = RemoteCommand(@"net use \\" + remoteHost + @"\" + shareName + " /Delete");
                if (disConnectResult)
                {
                    connectResult = RemoteCommand(StrConnectCommand);
                }
            }
            return connectResult;
        }

        public static bool RemoteCommand(string commandString)
        {
            bool flag = false;
            Process proc = new Process();
            try
            {
                proc.StartInfo.FileName = "cmd.exe";
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardInput = true;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.RedirectStandardError = true;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.StandardInput.WriteLine(commandString);
                proc.StandardInput.WriteLine("exit");
                while (!proc.HasExited)
                {
                    proc.WaitForExit(1000);
                }

                string errormsg = proc.StandardError.ReadToEnd();
                proc.StandardError.Close();
                if (String.IsNullOrEmpty(errormsg))
                {
                    flag = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                proc.Close();
                proc.Dispose();
            }
            return flag;
        }
    }
}
