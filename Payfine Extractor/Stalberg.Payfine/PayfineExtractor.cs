using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using NLog;
using Stalberg.Payfine.Objects;

namespace Stalberg.Payfine
{
    /// <summary>
    /// Represents an object that will process the extraction of data from TOMS for Payfine
    /// </summary>
    internal class PayfineExtractor
    {
        // Fields
        private Logger logger = null;
        private DbProvider db = null;
        private Mode mode = Mode.None;

        /// <summary>
        /// Initializes a new instance of the <see cref="PayfineExtractor"/> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="db">The db.</param>
        /// <param name="mode">The mode that the application is running in.</param>
        public PayfineExtractor(Logger logger, DbProvider db, Mode mode)
        {
            this.mode = mode;
            this.logger = logger;
            this.db = db;
        }

        /// <summary>
        /// Performs the PayFine data extract from TOMS process
        /// </summary>
        public void Extract()
        {
            // Get a List the Films for extract
            List<Film> films = this.db.GetFilmsForExtract();
            if (films.Count == 0)
            {
                this.logger.Info("There were no films to extract");
                return;
            }

            // Create PS1 files
            string outputPath = ConfigurationManager.AppSettings["OutputFolder"];
            PS1 newFile;
            List<PS1> files = new List<PS1>();

            foreach (Film film in films)
            {
                newFile = new PS1(film, this.logger);
                newFile.FileName = Path.Combine(outputPath, string.Format("{1}\\{0}_{1}_{2}{3}",
                    ConfigurationManager.AppSettings["Source"],
                    film.FilmNumber,
                    DateTime.Now.ToString("yyyyMMddHHmm"),
                    PS1.EXTENSION));
                bool prevFileExists = newFile.CreateFolder();
                files.Add(newFile);

                bool failed = false;

                try
                {
                    // Get Image data for the PS1
                    if (!newFile.GetFrames(db))
                    {
                        this.logger.Error("There was a problem getting frames for {0}", newFile.Film.FilmNumber);
                        failed = true;
                    }

                    // Output the PS1 files
                    if (!failed)
                        failed = newFile.Write();

                    if (!failed)
                    {
                        // Update the database
                        db.MarkFilmAsExtracted(newFile.Film);
                        this.logger.Info("Marking film {0} as extracted", newFile.Film.FilmNumber);
                    }

                }
                catch (Exception ex)
                {
                    this.logger.Error("Error in film {0}.\n{1}", newFile.Film.FilmNumber, ex.Message);
                    failed = true;
                }

                if (failed)
                {
                    if (prevFileExists)
                    {
                        //in JMPD - there may be more than one .ps1 file, that was created from Notice for each film
                        //only delete the current file .ps1 and no images
                        try
                        {
                            if (File.Exists(newFile.FileName))
                            {
                                File.Delete(newFile.FileName);
                                this.logger.Info("Deleted file {0}", newFile.FileName);
                            }
                        }
                        catch (Exception ex)
                        {
                            this.logger.Error("Unable to delete .ps1 file {0}\n{1}", newFile.Film.FilmNumber, ex.Message);
                        }
                    }
                    else
                    {
                        //delete the folder and its contents
                        newFile.DeleteFolder();
                    }
                }
            }

        }

    }
}
