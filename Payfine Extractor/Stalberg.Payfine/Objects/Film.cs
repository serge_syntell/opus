using System;
using System.Collections.Generic;
using System.Text;

namespace Stalberg.Payfine.Objects
{
    /// <summary>
    /// Represents a film from TOMS
    /// </summary>
    internal class Film
    {
        // Fields
        private bool isValid = true;
        private int id = 0;
        private string filmNo = string.Empty;
        private string authorityName = string.Empty;
        private string location = string.Empty;
        private List<Frame> frames ;

        /// <summary>
        /// Initializes a new instance of the <see cref="Film"/> class.
        /// </summary>
        public Film()
        {
            this.frames = new List<Frame>();
        }

        /// <summary>
        /// Gets a value indicating whether this instance is valid.
        /// </summary>
        /// <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
        public bool IsValid
        {
            get { return this.isValid; }
        }

        /// <summary>
        /// Sets the state of the film to be invalid.
        /// </summary>
        public void SetInvalid()
        {
            this.isValid = false;
        }

        /// <summary>
        /// Gets the film's frames.
        /// </summary>
        /// <value>The frames.</value>
        public List<Frame> Frames
        {
            get { return this.frames; }
        }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>The location.</value>
        public string Location
        {
            get { return this.location; }
            set { this.location = value; }
        }

        /// <summary>
        /// Gets or sets the name of the authority.
        /// </summary>
        /// <value>The name of the authority.</value>
        public string AuthorityName
        {
            get { return this.authorityName; }
            set { this.authorityName = value; }
        }

        /// <summary>
        /// Gets or sets the film number.
        /// </summary>
        /// <value>The film number.</value>
        public string FilmNumber
        {
            get { return this.filmNo; }
            set { this.filmNo = value; }
        }

        /// <summary>
        /// Gets or sets the database ID.
        /// </summary>
        /// <value>The ID.</value>
        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            return this.filmNo;
        }

    }
}
