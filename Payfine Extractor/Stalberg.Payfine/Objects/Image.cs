using System;
using System.Collections.Generic;
using System.Text;

namespace Stalberg.Payfine.Objects
{
    /// <summary>
    /// Represents an object that contains information about an image for Payfine extraction
    /// </summary>
    internal class Image
    {
        // Fields
        private int scanIntNo;
        private string name = string.Empty;
        private int x;
        private int y;
        private byte[] buffer = null;
        private string sequence = string.Empty;
        private Frame frame;

        /// <summary>
        /// Initializes a new instance of the <see cref="Image"/> class.
        /// </summary>
        public Image(Frame frame)
        {
            this.frame = frame;
        }

        /// <summary>
        /// Gets the frame.
        /// </summary>
        /// <value>The frame.</value>
        public Frame Frame
        {
            get { return this.frame; }
        }
        
        /// <summary>
        /// Gets or sets the sequence.
        /// </summary>
        /// <value>The sequence.</value>
        public string Sequence
        {
            get { return this.sequence; }
            set { this.sequence = value.Trim(); }
        }

        /// <summary>
        /// Gets or sets the image buffer.
        /// </summary>
        /// <value>The image buffer.</value>
        public byte[] ImageBuffer
        {
            get { return this.buffer; }
            set { this.buffer = value; }
        }

        /// <summary>
        /// Gets or sets the crosshair Y coordinate.
        /// </summary>
        /// <value>The Y.</value>
        public int Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        /// <summary>
        /// Gets or sets the crosshair X coordinate.
        /// </summary>
        /// <value>The X.</value>
        public int X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// Gets or sets the scan int no.
        /// </summary>
        /// <value>The scan int no.</value>
        public int ScanIntNo
        {
            get { return this.scanIntNo; }
            set { this.scanIntNo = value; }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            return this.name;
        }

    }
}
