using System;

namespace Stalberg.Payfine.Objects
{
    /// <summary>
    /// Lists the modes that the Payfine Extractor can run in
    /// </summary>
    internal enum Mode
    {
        /// <summary>
        /// No mode has yet been set.
        /// </summary>
        None = 0,
        /// <summary>
        /// The program will use the Indaba protocol for transferring files.
        /// </summary>
        Indaba = 1,
        /// <summary>
        /// The program will not transfer files, but allow them to be written to CD/DVD.
        /// </summary>
        DVD = 2
    }
}