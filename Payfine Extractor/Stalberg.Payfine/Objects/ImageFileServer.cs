﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stalberg.Payfine.Objects
{
    public class ImageFileServer
    {
        public int IFSIntNo { get; set; }
        public string ImageMachineName { get; set; }
        public string ImageDrive { get; set; }
        public string ImageRootFolder { get; set; }
        public string ImageShareName { get; set; }
        //public string RemoteUserName { get; set; }
        //public string RemotePassword { get; set; }
        public bool IsDefault { get; set; }
    }
}
