using System;

namespace Stalberg.Payfine.Objects
{
    /// <summary>
    /// Lists the possible Data sources for the Payfine Extractor
    /// </summary>
    internal enum DataSource
    {
        /// <summary>
        /// The data source has not yet been set.
        /// </summary>
        None = 0,
        /// <summary>
        /// Use the Traffic database for TOMS as the data source for extraction.
        /// </summary>
        Traffic = 1,
        /// <summary>
        /// Use the TMS / Opus database as the source for extraction.
        /// </summary>
        Opus = 2
    }
}