using System;
using System.Collections.Generic;
using System.Text;
//using Atalasoft.Imaging;
//using Atalasoft.Imaging.Codec;
//using Atalasoft.Imaging.Codec.Jpeg2000;
using System.IO;
using System.Drawing;
using System.Threading;

//dls 090604 - remove all references to Atalasoft and JP2000 conversion

namespace Stalberg.Payfine.Objects
{
    /// <summary>
    /// Represents the frame details for a Payfine extraction
    /// </summary>
    internal class Frame
    {
        // Fields
        private int frameIntNo = 0;
        private string regNo = string.Empty;
        private string frameNo = string.Empty;
        private string offenceType = string.Empty;
        private DateTime offenceDate;
        private string ticketNo = string.Empty;
        private int speedLimit;
        private int speed1;
        private int speed2;
        private Image mainImage = null;
        private Image secondImage = null;
        private Image driverImage = null;
        private Image regImage = null;
        private string sequence = string.Empty;
        private Film film;
        private int iFSIntNo = 0;
        private string frameImagePath = string.Empty;

        /// <summary>
        /// Gets or sets the IFSIntNo.
        /// </summary>
        public int IFSIntNo
        {
            get { return this.iFSIntNo; }
            set { this.iFSIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the FrameImagePath.
        /// </summary>
        public string FrameImagePath
        {
            get { return this.frameImagePath; }
            set { this.frameImagePath = value; }
        }
         
        //private static AtalaImage img = null;
        //private static JpegEncoder jpgCodec = new JpegEncoder();

        //private static Image img = null;

        /// <summary>
        /// Initializes the <see cref="Frame"/> class.
        /// </summary>
        static Frame()
        {
            //RegisteredDecoders.Decoders.Add(new Jp2Decoder());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Frame"/> class.
        /// </summary>
        public Frame(Film film)
        {
            this.film = film;

            this.mainImage = new Image(this);
            this.secondImage = new Image(this);
            this.driverImage = new Image(this);
            this.regImage = new Image(this);

        }

        /// <summary>
        /// Gets the film.
        /// </summary>
        /// <value>The film.</value>
        public Film Film
        {
            get { return this.film; }
        }

        /// <summary>
        /// Gets or sets the frame sequence.
        /// </summary>
        /// <value>The sequence.</value>
        public string Sequence
        {
            get { return this.sequence; }
            set { this.sequence = value; }
        }

        /// <summary>
        /// Gets or sets the second image.
        /// </summary>
        /// <value>The second image.</value>
        public Image SecondImage
        {
            get { return this.secondImage; }
            set { this.secondImage = value; }
        }

        /// <summary>
        /// Gets or sets the reg image.
        /// </summary>
        /// <value>The reg image.</value>
        public Image RegImage
        {
            get { return this.regImage; }
            set { this.regImage = value; }
        }

        /// <summary>
        /// Gets or sets the driver image.
        /// </summary>
        /// <value>The driver image.</value>
        public Image DriverImage
        {
            get { return this.driverImage; }
            set { this.driverImage = value; }
        }

        /// <summary>
        /// Gets or sets the main image.
        /// </summary>
        /// <value>The main image.</value>
        public Image MainImage
        {
            get { return this.mainImage; }
            set { this.mainImage = value; }
        }

        /// <summary>
        /// Gets or sets the second speed.
        /// </summary>
        /// <value>The speed2.</value>
        public int Speed2
        {
            get { return this.speed2; }
            set { this.speed2 = value; }
        }

        /// <summary>
        /// Gets or sets the first speed.
        /// </summary>
        /// <value>The speed1.</value>
        public int Speed1
        {
            get { return this.speed1; }
            set { this.speed1 = value; }
        }

        /// <summary>
        /// Gets or sets the speed limit.
        /// </summary>
        /// <value>The speed limit.</value>
        public int SpeedLimit
        {
            get { return this.speedLimit; }
            set { this.speedLimit = value; }
        }

        /// <summary>
        /// Gets or sets the ticket number.
        /// </summary>
        /// <value>The ticket number.</value>
        public string TicketNumber
        {
            get { return this.ticketNo; }
            set { this.ticketNo = value; }
        }

        /// <summary>
        /// Gets or sets the offence date.
        /// </summary>
        /// <value>The offence date.</value>
        public DateTime OffenceDate
        {
            get { return this.offenceDate; }
            set { this.offenceDate = value; }
        }

        /// <summary>
        /// Gets or sets the offence type letter.
        /// </summary>
        /// <value>The type of the offence.</value>
        public string OffenceType
        {
            get { return this.offenceType; }
            set { this.offenceType = value; }
        }

        /// <summary>
        /// Gets or sets the frame number.
        /// </summary>
        /// <value>The frame number.</value>
        public string FrameNumber
        {
            get { return this.frameNo; }
            set { this.frameNo = value; }
        }

        /// <summary>
        /// Gets or sets the registration number.
        /// </summary>
        /// <value>The registration number.</value>
        public string RegistrationNumber
        {
            get { return this.regNo; }
            set { this.regNo = value; }
        }

        /// <summary>
        /// Gets or sets the database ID of the frame.
        /// </summary>
        /// <value>The ID.</value>
        public int ID
        {
            get { return this.frameIntNo; }
            set { this.frameIntNo = value; }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            return this.frameNo;
        }

        /// <summary>
        /// Saves the images to the file system.
        /// </summary>
        /// <param name="outputPath">The output path.</param>
        public void SaveImages(string outputPath)
        {
            //dls 070928 - only write the images to disk if the imageBuffer is not null
            if (this.mainImage.Name.Length > 0)
            {
                if (this.mainImage.ImageBuffer != null)
                {
                    Save(Path.Combine(outputPath, this.mainImage.Name), this.mainImage.ImageBuffer);
                    this.mainImage.ImageBuffer = null;
                }
            }

            if (this.secondImage.Name.Length > 0)
            {
                if (this.secondImage.ImageBuffer != null)
                {
                    Save(Path.Combine(outputPath, this.secondImage.Name), this.secondImage.ImageBuffer);
                    this.secondImage.ImageBuffer = null;
                }
            }

            if (this.regImage.Name.Length > 0)
            {
                if (this.regImage.ImageBuffer != null)
                {
                    Save(Path.Combine(outputPath, this.regImage.Name), this.regImage.ImageBuffer);
                    this.regImage.ImageBuffer = null;
                }
            }

            if (this.driverImage.Name.Length > 0)
            {
                if (this.driverImage.ImageBuffer != null)
                {
                    Save(Path.Combine(outputPath, this.driverImage.Name), this.driverImage.ImageBuffer);
                    this.driverImage.ImageBuffer = null;
                }
            }
        }

        public void Save(string path, byte[] buffer)
        {
            // Save the image to the file system without checking it
            FileStream fs = new FileStream(path, FileMode.Create);
            fs.Write(buffer, 0, buffer.Length);
            fs.Close();
            Thread.Sleep(5);
        }

        //public void SaveImages(string outputPath)
        //{
        //    //dls 070928 - only write the images to disk if the imageBuffer is not null
        //    if (this.mainImage.Name.Length > 0)
        //    {
        //        if (this.mainImage.ImageBuffer != null)
        //        {
        //            Frame.img = AtalaImage.FromByteArray(this.mainImage.ImageBuffer);
        //            Frame.img.Save(Path.Combine(outputPath, this.mainImage.Name), jpgCodec, null);
        //            this.mainImage.ImageBuffer = null;
        //            Frame.img.Dispose();
        //        }
        //    }

        //    if (this.secondImage.Name.Length > 0)
        //    {
        //        if (this.secondImage.ImageBuffer != null)
        //        {
        //            Frame.img = AtalaImage.FromByteArray(this.secondImage.ImageBuffer);
        //            Frame.img.Save(Path.Combine(outputPath, this.secondImage.Name), jpgCodec, null);
        //            this.secondImage.ImageBuffer = null;
        //            Frame.img.Dispose();
        //        }
        //    }

        //    if (this.regImage.Name.Length > 0)
        //    {
        //        if (this.regImage.ImageBuffer != null)
        //        {
        //            Frame.img = AtalaImage.FromByteArray(this.regImage.ImageBuffer);
        //            Frame.img.Save(Path.Combine(outputPath, this.regImage.Name), jpgCodec, null);
        //            this.regImage.ImageBuffer = null;
        //            Frame.img.Dispose();
        //        }
        //    }

        //    if (this.driverImage.Name.Length > 0)
        //    {
        //        if (this.driverImage.ImageBuffer != null)
        //        {
        //            Frame.img = AtalaImage.FromByteArray(this.driverImage.ImageBuffer);
        //            Frame.img.Save(Path.Combine(outputPath, this.driverImage.Name), jpgCodec, null);
        //            this.driverImage.ImageBuffer = null;
        //            Frame.img.Dispose();
        //        }
        //    }
        //}


    }
}
