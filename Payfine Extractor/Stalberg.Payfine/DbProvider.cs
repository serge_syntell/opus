using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using NLog;
using Stalberg.Payfine.Objects;

namespace Stalberg.Payfine
{
    /// <summary>
    /// Represents a the database provider for the application
    /// </summary>
    internal class DbProvider
    {
        // Fields
        private SqlConnection con = null;
        private Logger logger = null;

        private static DataSource dataSource = DataSource.None;

        private const string DRIVER_FILE_EXT = "_DRV.J";
        private const string REGNO_FILE_EXT = "_REGNO.JPG";
        private const string REG_FILE_EXT = "_REG.JPG";
        private const string WET_FILM_FIRST_IMAGE = "_17.J";
        private const string DIG_FILM_FIRST_IMAGE = "001.J";

        //private ImageFileServer imageFileServer = null;

        /// <summary>
        /// Gets or sets the data source for the database provider.
        /// </summary>
        /// <value>The data source.</value>
        public static DataSource DataSource
        {
            get { return DbProvider.dataSource; }
            set { DbProvider.dataSource = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DbProvider"/> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        public DbProvider(Logger logger)
        {
            this.logger = logger;

            if (DbProvider.dataSource == DataSource.None && ConfigurationManager.AppSettings["DataSource"] != null)
            {
                string sourceName = ConfigurationManager.AppSettings["DataSource"];
                try
                {
                    DbProvider.dataSource = (DataSource)Enum.Parse(typeof(DataSource), sourceName, true);
                }
                catch
                {
                    this.logger.Error("The data source {0} is not recognised, the default (Traffic) will be used instead.", sourceName);
                }
                this.logger.Info("Data Source = {0}", DbProvider.dataSource);
            }

            string connectionString = string.Empty;
            switch (DbProvider.dataSource)
            {
                case DataSource.Traffic:
                    connectionString = ConfigurationManager.ConnectionStrings["Traffic"].ConnectionString;
                    break;

                case DataSource.Opus:
                    connectionString = ConfigurationManager.ConnectionStrings["Opus"].ConnectionString;
                    break;
            }

            try
            {
                con = new SqlConnection(connectionString);
                this.con.Open();
            }
            catch (Exception ex)
            {
                this.logger.Error("There was a problem opening the SQL Connection.\n{0}", ex);
            }
            finally
            {
                if (this.con != null)
                    this.con.Close();
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is connected.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is connected; otherwise, <c>false</c>.
        /// </value>
        public bool IsConnected
        {
            get { return (this.con != null && DbProvider.dataSource != DataSource.None); }
        }

        private T SafeRead<T>(SqlDataReader reader, string fieldName)
        {
            if (reader[fieldName] == DBNull.Value)
                return default(T);

            return (T)reader[fieldName];
        }

        /// <summary>
        /// Gets a list films that need to be extracted for Payfine.
        /// </summary>
        /// <returns></returns>
        public List<Film> GetFilmsForExtract()
        {
            List<Film> films = new List<Film>();
            SqlCommand com = new SqlCommand("PayfineFilmsForExtract", this.con);
            com.CommandTimeout = 60;
            com.CommandType = CommandType.StoredProcedure;

            try
            {
                Film film = null;
                this.con.Open();
                SqlDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    film = new Film();
                    film.ID = (int)reader["FilmIntNo"];
                    film.FilmNumber = (string)reader["FilmNo"];
                    film.AuthorityName = (string)reader["AuthName"];
                    film.Location = (string)reader["LocDescr"];

                    films.Add(film);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                this.logger.Error("SQL Error in PayfineFilmsForExtract.\n{0}", ex);
            }
            finally
            {
                this.con.Close();
            }

            return films;
        }

        public ImageFileServer SetImageFileServerSettings(int ifsIntNo)
        {
            // dl 2010/03/19 get the imagefilesever where to read image after remove images from database             
            ImageFileServer imageFileServer = null;
            SqlCommand com = new SqlCommand("ImageFileServersListGetByID", this.con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@IFSIntNo", SqlDbType.Int).Value = ifsIntNo;
            try
            {
                this.con.Open();
                SqlDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    imageFileServer = new ImageFileServer();
                    imageFileServer.IFSIntNo = Convert.ToInt32(reader["IFSIntNo"]);
                    imageFileServer.ImageDrive = reader["ImageDrive"].ToString();
                    imageFileServer.ImageMachineName = reader["ImageMachineName"].ToString();
                    imageFileServer.ImageRootFolder = reader["ImageRootFolder"].ToString();
                    imageFileServer.ImageShareName = reader["ImageShareName"].ToString();
                    //imageFileServer.RemotePassword = reader["RemotePassword"].ToString();
                    //imageFileServer.RemoteUserName = reader["RemoteUserName"].ToString();
                    imageFileServer.IsDefault = (bool)reader["IsDefault"];
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                this.logger.Error("SQL Error in ImageFileServersListGetByID.\n{0}", ex);
            }

            if (imageFileServer == null)
            {
                this.logger.Error("ImageFileServer not exist.\n");
                return null;
            }

            this.con.Close();
            return imageFileServer;

        }
        /// <summary>
        /// Processes the images for the supplied film, populating the list of image names.
        /// </summary>
        /// <param name="frame">The frame.</param>
        public bool ProcessImages(Frame frame, ImageFileServer imageFileServer)
        {
            bool response = true;

            //dls 091009 - have added an ORDER BY clause into the SP on OPUS to make sure the images are coming out in the right order
            SqlCommand com = new SqlCommand("PayfineGetImagesForFilm", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@FrameIntNo", SqlDbType.Int, 4).Value = frame.ID;

            try
            {
                Image image = null;

                this.con.Open();
                SqlDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    // Check if this is an image we want
                    image = new Image(frame);
                    image.ScanIntNo = (int)reader["ScImIntNo"];
                    image.Name = (string)reader["JPegName"];
                    image.X = (int)reader["XValue"];
                    image.Y = (int)reader["YValue"];
                    image.Sequence = (string)reader["ScImType"];

                    // DL 2009/03/19 remove images from database, read image data from remote file system
                    if (DbProvider.dataSource == Objects.DataSource.Traffic)
                    {
                        if (reader["ScanImage"] != System.DBNull.Value)
                            image.ImageBuffer = (byte[])reader["ScanImage"];
                    }
                    else
                    {
                        //dls 2010-03-26 - when extracting images from Traffic DB, the images will not be on the file server
                        if (imageFileServer == null || frame.IFSIntNo == 0)
                        {
                            if (reader["ScanImage"] != System.DBNull.Value)
                                image.ImageBuffer = (byte[])reader["ScanImage"];
                        }
                        else
                        {
                            string strFullImagePath = @"\\" + imageFileServer.ImageMachineName;
                            strFullImagePath += @"\" + imageFileServer.ImageShareName;
                            strFullImagePath += @"\" + frame.FrameImagePath;
                            strFullImagePath += @"\" + image.Name;

                            using (FileStream fs = File.OpenRead(strFullImagePath))
                            {
                                image.ImageBuffer = new byte[fs.Length];
                                fs.Read(image.ImageBuffer, 0, image.ImageBuffer.Length);
                            }
                        }
                    }

                    this.logger.Debug(image.Name);

                    if (image.Sequence.Equals("D") || image.Name.ToUpper().IndexOf(DRIVER_FILE_EXT) > -1)
                    {
                        frame.DriverImage = image;
                        continue;
                    }

                    if (image.Sequence.Equals("R") ||
                        image.Name.ToUpper().IndexOf(REGNO_FILE_EXT) > -1 ||
                        image.Name.ToUpper().IndexOf(REG_FILE_EXT) > -1)
                    {
                        frame.RegImage = image;
                        continue;
                    }

                    //dls 091009 - must not overwrite the image if it has already been updated - check that it is still null
                    if ((image.Sequence.Equals("A") || image.Sequence.Equals("X") ) && (
                        image.Name.ToUpper().IndexOf(WET_FILM_FIRST_IMAGE) > -1 ||
                        image.Name.ToUpper().IndexOf(DIG_FILM_FIRST_IMAGE) > -1)
                        && frame.MainImage.ImageBuffer == null)
                    {
                        frame.MainImage = image;
                        continue;
                    }

                    //dls 091009 - must not overwrite the image if it has already been updated - check that it is still null
                    if (image.Sequence.Equals("B") && (
                        image.Name.ToUpper().IndexOf(WET_FILM_FIRST_IMAGE) > -1 ||
                        image.Name.ToUpper().IndexOf(DIG_FILM_FIRST_IMAGE) > -1)
                         && frame.SecondImage.ImageBuffer == null)
                    {
                        frame.SecondImage = image;
                        continue;
                    }

                    //dls 091009 - must not overwrite the image if it has already been updated - check that it is still null
                    if (frame.MainImage.ImageBuffer == null)
                    {
                        frame.MainImage = image;
                        continue;
                    }
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                this.logger.Error("SQL Error in PayfineGetImagesForFilm.\n{0}", ex);
                response = false;
            }
            finally
            {
                this.con.Close();
            }

            return response;
        }

        /// <summary>
        /// Gets the frames for the supplied film.
        /// </summary>
        /// <param name="film">The film.</param>
        public bool GetFramesForFilm(Film film)
        {
            bool response = true;

            SqlCommand com = new SqlCommand("PayfineGetFramesForFilm", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@FilmIntNo", SqlDbType.Int, 4).Value = film.ID;

            try
            {
                Frame frame = null;
                this.con.Open();
                SqlDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    frame = new Frame(film);
                    frame.ID = (int)reader["FrameIntNo"];
                    frame.RegistrationNumber = (string)reader["RegNo"];
                    frame.FrameNumber = (string)reader["FrameNo"];
                    frame.OffenceType = (string)reader["OffenceLetter"];
                    frame.OffenceDate = (DateTime)reader["OffenceDate"];
                    frame.TicketNumber = (string)reader["TicketNo"];
                    frame.SpeedLimit = (int)reader["Speed"];
                    frame.Speed1 = (int)reader["FirstSpeed"];
                    frame.Speed2 = (int)reader["SecondSpeed"];
                    frame.Sequence = ((string)reader["Sequence"]).ToUpper();

                    //dls 2010-03-26 - need to remember to set the Traffic script to initialise these columns to 0 and string.emptry
                    // DL 2009/03/19 read images from file system, load ImageFileServerInfo

                    if (DbProvider.dataSource == Objects.DataSource.Opus)
                    {
                        frame.IFSIntNo = (int)reader["IFSIntNo"];
                        frame.FrameImagePath = reader["FrameImagePath"].ToString();
                    }
                    film.Frames.Add(frame);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                this.logger.Error("SQL Error in PayfineGetFramesForFilm.\n{0}", ex);
                response = false;
            }
            finally
            {
                this.con.Close();
            }

            return response;
        }

        public void MarkFilmAsExtracted(Film film)
        {
            SqlCommand com = new SqlCommand("PayfineUpdateFilm", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@FilmIntNo", SqlDbType.Int, 4).Value = film.ID;

            try
            {
                this.con.Open();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                this.logger.Error("SQL Error in PayfineUpdateFilm.\n{0}", ex);
            }
            finally
            {
                this.con.Close();
            }

        }

    }
}
