using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using NLog;
using Stalberg.Payfine.Objects;
using System.Configuration;

namespace Stalberg.Payfine
{
    /// <summary>
    /// Represents a PS1 extract file for Payfine
    /// </summary>
    internal class PS1
    {
        // Fields
        private string fileName = string.Empty;
        private string filmNo = string.Empty;
        private Film film = null;
        private string imageFolderName = string.Empty;
        private Logger logger = null;

        private static readonly string source;
        private static bool alwaysAddRegImageName;

        private const char DELIMITER = ',';
        private const string DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
        private const string IMAGE_FOLDER_NAME = "Images";

        internal const string EXTENSION = ".ps1";
        private const int VERSION = 1;

        /// <summary>
        /// Initializes the <see cref="PS1"/> class.
        /// </summary>
        static PS1()
        {
            PS1.source = ConfigurationManager.AppSettings["Source"];
            try
            {
                bool.TryParse(ConfigurationManager.AppSettings["AlwaysAddRegImageName"], out PS1.alwaysAddRegImageName);
            }
            catch { }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PS1"/> class.
        /// </summary>
        public PS1(Film film, Logger logger)
        {
            this.film = film;
            this.logger = logger;
        }

        /// <summary>
        /// Gets the film that this PS1 object is writing.
        /// </summary>
        /// <value>The film.</value>
        public Film Film
        {
            get { return this.film; }
        }

        /// <summary>
        /// Gets or sets the name of the PS1 file.
        /// </summary>
        /// <value>The name of the file.</value>
        public string FileName
        {
            get { return this.fileName; }
            set { this.fileName = value; }
        }

        /// <summary>
        /// Writes out the PS1 file contents, logging any errors that occur.
        /// </summary>
        public bool Write()
        {
            bool failed = false;

            FileInfo file = new FileInfo(this.fileName);
            FileStream fs = file.Open(FileMode.Create, FileAccess.Write);
            StreamWriter writer = new StreamWriter(fs, Encoding.Default);

            try
            {
                // Write the Header
                this.WriteHeader(writer);

                // Write the Data rows
                foreach (Frame frame in this.film.Frames)
                    this.WriteData(frame, writer);
            }
            catch (Exception e)
            {
                this.logger.Error("Error in file {0}.\n{1}", this.fileName, e.Message);
                failed = true;
            }
            finally
            {
                writer.Close();
                fs.Close();
                fs.Dispose();
            }

            return failed;
        }

        private void WriteData(Frame frame, StreamWriter writer)
        {
            try
            {
                writer.Write(HandleDataField(frame.RegistrationNumber));
                writer.Write(DELIMITER);
                writer.Write(HandleDataField(this.film.FilmNumber));
                writer.Write(DELIMITER);
                writer.Write(HandleDataField(frame.FrameNumber));
                writer.Write(DELIMITER);
                writer.Write(HandleDataField(frame.Sequence));
                writer.Write(DELIMITER);
                writer.Write(HandleDataField(frame.OffenceType));
                writer.Write(DELIMITER);
                writer.Write(HandleDataField(frame.OffenceDate.ToString(DATE_FORMAT)));
                writer.Write(DELIMITER);
                writer.Write(HandleDataField(frame.TicketNumber));
                writer.Write(DELIMITER);
                writer.Write(HandleDataField(this.film.AuthorityName));
                writer.Write(DELIMITER);
                writer.Write(HandleDataField(this.film.Location));
                writer.Write(DELIMITER);
                writer.Write(frame.SpeedLimit);
                writer.Write(DELIMITER);
                writer.Write(frame.Speed1);
                writer.Write(DELIMITER);
                writer.Write(frame.Speed2);
                writer.Write(DELIMITER);
                writer.Write(frame.MainImage.X);
                writer.Write(DELIMITER);
                writer.Write(frame.MainImage.Y);
                writer.Write(DELIMITER);
                writer.Write(HandleDataField(frame.MainImage.Name));
                writer.Write(DELIMITER);
                writer.Write(frame.SecondImage.X);
                writer.Write(DELIMITER);
                writer.Write(frame.SecondImage.Y);
                writer.Write(DELIMITER);
                writer.Write(HandleDataField(frame.SecondImage.Name));
                writer.Write(DELIMITER);
                writer.Write(HandleDataField(this.CheckImageName(frame, true)));
                writer.Write(DELIMITER);
                writer.Write(HandleDataField(this.CheckImageName(frame, false)));

                writer.WriteLine();
            }
            catch (Exception ex)
            {
                this.logger.Error("Error writing a data row of the PS1 File for frame {1}.\n{0}", ex, frame.FrameNumber);
            }
        }

        private string CheckImageName(Frame frame, bool isDriver)
        {
            if (isDriver)
            {
                if (frame.DriverImage.Name.Length > 0 || !PS1.alwaysAddRegImageName)
                    return frame.DriverImage.Name;
                else
                    return string.Format("{0}{1}_DRV.jpg", frame.Film.FilmNumber, frame.FrameNumber);
            }
            else
            {
                if (frame.RegImage.Name.Length > 0 || !PS1.alwaysAddRegImageName)
                    return frame.RegImage.Name;
                else
                    return string.Format("{0}{1}_REG.jpg", frame.Film.FilmNumber, frame.FrameNumber);
            }
        }

        private void WriteHeader(StreamWriter writer)
        {
            try
            {
                writer.Write(PS1.VERSION);
                writer.Write(DELIMITER);
                writer.Write(HandleDataField(PS1.source));
                writer.Write(DELIMITER);
                writer.Write(HandleDataField(this.film.FilmNumber));
                writer.Write(DELIMITER);
                writer.WriteLine(DateTime.Now.ToString(DATE_FORMAT));
            }
            catch (Exception ex)
            {
                this.logger.Error("Error writing the header of the PS1 File.\n{0}", ex);
            }
        }

        /// <summary>
        /// Creates the output folder for the PS1 file.
        /// </summary>
        //public void CreateFolder()
        public bool CreateFolder()
        {
            bool fileAlreadyExists = false;

            if (this.fileName.Length == 0)
                return false;

            string folderName = Path.GetDirectoryName(this.fileName);
            DirectoryInfo dir = new DirectoryInfo(folderName);
            if (!dir.Exists)
                dir.Create();
            else
            {
                foreach (FileInfo file in dir.GetFiles())
                {
                    if (file.Extension.Equals(EXTENSION))
                    {
                        fileAlreadyExists = true;
                        break;
                    }
                }
            }

            this.imageFolderName = Path.Combine(folderName, IMAGE_FOLDER_NAME);
            dir = new DirectoryInfo(this.imageFolderName);
            if (!dir.Exists)
                dir.Create();

            if (!fileAlreadyExists)
            {
                this.logger.Info("Created folders for {0}", this.film.FilmNumber);
            }

            return fileAlreadyExists;
        }

        public void DeleteFolder()
        {
            if (this.fileName.Length == 0)
                return;

            string folderName = Path.GetDirectoryName(this.fileName);
            DirectoryInfo dir = new DirectoryInfo(folderName);
            if (dir.Exists)
                dir.Delete(true);

            this.logger.Info("Deleted folders for {0}", this.film.FilmNumber);
        }

        /// <summary>
        /// Gets the frames.
        /// </summary>
        /// <param name="db">The db.</param>
        public bool GetFrames(DbProvider db)
        {
            bool response = true;
            this.logger.Info("Getting frames for {0}", this.film.FilmNumber);
            int ifsIntNo = 0;

            ImageFileServer ifs = new ImageFileServer();

            if (!db.GetFramesForFilm(this.film))
            {
                this.film.SetInvalid();
                return false;
            }

            foreach (Frame frame in this.film.Frames)
            {
                this.logger.Info("Getting images for Frame {0}", frame.FrameNumber);

                if (DbProvider.DataSource == DataSource.Opus)
                {

                    if (ifsIntNo != frame.IFSIntNo)
                    {
                        ifsIntNo = frame.IFSIntNo;
                        ifs = db.SetImageFileServerSettings(ifsIntNo);
                    }
                }

                if (!db.ProcessImages(frame, ifs))
                {
                    this.film.SetInvalid();
                    response = false;
                    return response;
                }
                else
                {
                    frame.SaveImages(this.imageFolderName);
                }
            }

            //if (!this.film.IsValid)
            //    response = false;

            return response;
        }

        private string HandleDataField(string strIn)
        {
            return strIn.Replace(',', ';');
        }

    }
}
