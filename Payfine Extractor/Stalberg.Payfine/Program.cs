using System;
using System.Configuration;
using System.IO;
using System.Text;
using NLog;
using Stalberg.Indaba;
using Stalberg.Payfine.Objects;
using Stalberg.TMS_3P_Loader.Components;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.EntLib;

namespace Stalberg.Payfine
{
    /// <summary>
    /// The class that contains the main entry point of the application
    /// </summary>
    public class Program
    {
        // Fields
        private static bool doNothing = false;
        private static bool isDebug = false;
        private static Logger logger = null;
        private static ICommunicate indaba = null;
        private static Mode mode = Mode.None;

        internal const string APP_NAME = "Payfine Extractor";
       // private const string LAST_UPDATED = ProjectLastUpdated.AARTOPayfineExtractor.ToShortDateString();

        /// <summary>
        /// Initializes the <see cref="Program"/> class.
        /// </summary>
        static Program()
        {
            // Setup the logger
            LogManager.ThrowExceptions = true;
            Program.logger = LogManager.GetLogger(APP_NAME);
            Program.logger.Info("{0}\n-----------------\nCopyright Stalberg & Associates 2006 - {2} All rights reserved\nLast Updated {1}\n", APP_NAME, ProjectLastUpdated.AARTOPayfineExtractor.ToShortDateString(), DateTime.Today.Year);
        }

        /// <summary>
        /// Mains the specified args.
        /// </summary>
        /// <param name="args">The args.</param>
        static void Main(string[] args)
        {
            // Check Last updated Version            
            string errorMessage;

            try
            {
                // Setup the Log Writer
                string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
                Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);

                // Check whether another instance is already running
                Singleton single = new Singleton("Stalberg.Payfine.Extract");
                single.OtherProcessesToCheckFor("TMS_ViolationLoader",
                    "TMS_CD_Loader",
                    "TMS_3P_Loader",
                    "TMS_TPExInt",
                    "Stalberg.ThaboExporter",
                    "RoadblockDataExtract");
                if (single.Check())
                {
                    Program.logger.Info(single.Message);
                    Program.logger.Error(string.Format("An instance of {1} application is already running - processing aborted {0}.", DateTime.Now.ToString(), APP_NAME));
                    return;
                }

                // Parse the command line
                Program.ParseCommandLineArguments(args);
                if (!Program.doNothing)
                {
                    Program.logger.Info("{0} started at {1}", APP_NAME, DateTime.Now.ToLongTimeString());

                    if (ConfigurationManager.AppSettings["Mode"] != null)
                    {
                        string modeName = ConfigurationManager.AppSettings["Mode"];
                        try
                        {
                            Program.mode = (Mode)Enum.Parse(typeof(Mode), modeName, true);
                        }
                        catch
                        {
                            Program.logger.Error("The mode {0} id not recognised", modeName);
                        }
                    }
                    if (Program.mode == Mode.None)
                    {
                        Program.logger.Error("The mode of the application has not been set.");
                        return;
                    }

                    // Setup Indaba
                    if ((Program.mode & Mode.Indaba) == Mode.Indaba)
                    {
                        Client.ApplicationName = APP_NAME;
                        Client.ConnectionString = ConfigurationManager.ConnectionStrings["Indaba"].ConnectionString;
                        Client.Initialise();

                        Program.indaba = Client.Create();
                    }

                    // Setup the database connection provider
                    DbProvider db = new DbProvider(Program.logger);
                    if (!db.IsConnected)
                        return;

                    if (DbProvider.DataSource == DataSource.Opus)
                    {
                        // 2010/10/11 jerry add 
                        if (!CheckVersionManager.CheckVersion(AartoProjectList.AARTOPayfineExtractor, out errorMessage))
                        {
                            Console.WriteLine(errorMessage);
                            EntLibLogger.WriteLog(LogCategory.General, "", errorMessage);
                            return;
                        }
                    }

                    // Create and run the PayFine extractor
                    PayfineExtractor payfine = new PayfineExtractor(Program.logger, db, Program.mode);
                    payfine.Extract();

                    // Check how the extracted data should be sent
                    switch (Program.mode)
                    {
                        case Mode.Indaba:
                            Program.logger.Info("Sending files using Indaba.");
                            Program.SendExtractWithIndaba();
                            break;
                    }

                    // Cleanup
                    Program.logger.Info("{0} finished at {1}", APP_NAME, DateTime.Now.ToLongTimeString());
                }

                // See if there's a debug statement
                if (Program.isDebug)
                {
                    Program.logger.Info("\n[ Hit <Enter> to exit. ]");
                    Console.ReadLine();
                }
            }
            catch (Exception ex)
            {
                Program.logger.Fatal("Fatal error: {0}.\n{1}", ex.Message, ex.StackTrace);
                EntLibLogger.WriteErrorLog(ex.GetBaseException(), LogCategory.Exception, AartoProjectList.AARTOPayfineExtractor.ToString());
            }
            finally
            {
                Client.Dispose();
            }
        }

        private static void SendExtractWithIndaba()
        {
            DirectoryInfo dir = null;
            string outputDir = ConfigurationManager.AppSettings["OutputFolder"];
            DirectoryInfo rootDir = new DirectoryInfo(outputDir);
            DirectoryInfo[] directories = rootDir.GetDirectories();
            for (int i = directories.Length - 1; i > -1; i--)
            {
                dir = directories[i];
                Program.SendDirectoryContentsRecursive(dir);
                dir.Delete(true);
                Program.logger.Info("Sending {0}", dir.Name);
            }
        }

        private static void SendDirectoryContentsRecursive(DirectoryInfo dir)
        {
            foreach (DirectoryInfo subDir in dir.GetDirectories())
                Program.SendDirectoryContentsRecursive(subDir);

            foreach (FileInfo file in dir.GetFiles())
                Program.indaba.Enqueue(file);
        }

        private static void ParseCommandLineArguments(string[] args)
        {
            foreach (string arg in args)
            {
                string[] split = arg.ToLower().Split(new char[] { '=' });
                switch (split[0].Trim())
                {
                    case "/indaba":
                    case "-indaba":
                        Program.mode = Program.mode | Mode.Indaba;
                        break;

                    case "?":
                    case "help":
                    case "/help":
                    case "-help":
                        Program.doNothing = true;
                        Program.ShowHelp();
                        break;

                    case "/datasource":
                        try
                        {
                            DbProvider.DataSource = (DataSource)Enum.Parse(typeof(DataSource), split[1], true);
                        }
                        catch
                        {
                            Program.logger.Error("The data source {0} is not recognised, the default (Traffic) will be used instead.", arg[1]);
                        }
                        Program.logger.Info("Data Source = {0}", DbProvider.DataSource);
                        break;

                    case "/debug":
                        Program.isDebug = true;
                        break;

                    default:
                        Program.logger.Info("'{0}' is not a recognised command line switch.", arg);
                        break;
                }
            }
        }

        private static void ShowHelp()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("/Indaba          - Run in Indaba mode (files will be queued and sent by Indaba).\n");
            sb.Append("/Debug           - Run in debug mode so that you can see more of what is happening.\n");
            sb.Append("/Help            - Displays this help file.");
            Program.logger.Info(sb.ToString());
        }

    }
}
