﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Net;
using System.Collections;
using System.IO;
using System.Xml;
using SIL.AARTO.BLL.Utility;
namespace SIL.AARTO.Web.Tests.Controllers
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        public UnitTest1()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the public class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a public class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {
            //
            // TODO: Add test logic	here
            //GenerateDdl();
            //GetRandomNum(2);
            //GetExternalDate();

        }
        public  DateTime GetExternalDate()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("http://localhost/now.xml");
            XmlElement rootElement = xmlDoc.DocumentElement;
            XmlNode datas = rootElement.SelectSingleNode(@"//Datas");
            string now = datas.Attributes["Now"].Value.Trim();
            return Convert.ToDateTime(now);
        }
        private string GetRandomNum(int ix)
        {
            string x = "";
            for (int i = 0; i < ix; i++)
            {
                Random ran = new Random();
                ran.Next(0, 9);
                x = x + ran.Next(0, 9).ToString();
            }
            return x;
        }
        private string GenerateDdl()
        {
           string responseFromServer="";
           try
           {
               Hashtable ht=new Hashtable();
               ht.Add("optionType","aartoprdpcode");
               XmlDocument doc=WebSvcCaller.QueryPostWebService("http://localhost:50670/DataService.asmx", "GetOptions", ht);
               responseFromServer = doc.InnerXml;
           }
           catch (Exception ex)
           {

           }
            return responseFromServer;
        }

    }
}
