using System;
using System.Collections.Generic;
using System.Text;

namespace Stalberg.TMS_3P_Loader.Components
{
	/// <summary>
	/// A data class that contains FTP parameter information
	/// </summary>
	internal class FtpParameters
	{
		/// <summary>
		/// The ID of the FTP
		/// </summary>
		//public int ID;
		/// <summary>
		/// The Contractor Code
		/// </summary>
		public String Contractor;
		/// <summary>
		/// The name of the process that is using the FTP
		/// </summary>
		public String Process;
		/// <summary>
		/// The name of the current machine for FTP identification purposes
		/// </summary>
		public String ExportServer;
		/// <summary>
		/// The local path that will be checked to see if there is anything to export
		/// </summary>
		public String ExportPath;
		/// <summary>
		/// The name of the FTP server
		/// </summary>
		public String HostServer;
		/// <summary>
		/// The IP address of the FTP server
		/// </summary>
		public String HostIP;
		/// <summary>
		/// The name of the user account to use on the FTP server
		/// </summary>
		public String HostUser;
		/// <summary>
		/// The user account password for the FTP server
		/// </summary>
		public String HostPassword;
		/// <summary>
		/// The path on the FTP server to switch to by default when the connection is made
		/// </summary>
		public String HostPath;
		/// <summary>
		/// The email address to send the log file to once processing completes
		/// </summary>
		public string Email;
		/// <summary>
		/// The address of the SMTP server to use to email the log
		/// </summary>
		public string SmtpServer;
	}
}
