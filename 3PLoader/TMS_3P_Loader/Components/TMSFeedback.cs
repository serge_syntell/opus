using System;
using System.Collections.Generic;
using System.Text;
using EnterpriseDT.Net.Ftp;
using Stalberg.TMS_3P_Loader.Data;
using System.IO;
using Stalberg.Indaba;

namespace Stalberg.TMS_3P_Loader.Components
{
    /// <summary>
    /// Represents a class that can check and process MS error files
    /// </summary>
    internal class TMSFeedback
    {
        // Fields
        private List<TS1File> feedbackFiles = null;
        private FTPClient ftp = null;
        private ICommunicate indaba = null;
        private bool isIndaba = false;

        // Constants
        private const string ERROR_EXTENSION = ".ERR";
        private const string OK_EXTENSION = ".OK";

        /// <summary>
        /// Initializes a new instance of the <see cref="TMSFeedback"/> class.
        /// </summary>
        public TMSFeedback(bool isIndaba)
        {
            this.isIndaba = isIndaba;
            this.feedbackFiles = new List<TS1File>();
        }

        /// <summary>
        /// Checks for feedback files on the FTP server.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> is any error files was found.
        /// </returns>
        internal bool CheckForFeedbackFiles()
        {
            if (this.isIndaba)
                return this.CheckIndabaFOrFeedback();
            else
                return this.CheckFtpForFeedback();
        }

        private bool CheckIndabaFOrFeedback()
        {
            bool response = false;

            this.indaba = Client.Create();
            Stream stream = null;
            byte[] buffer = null;
            TS1File ts1 = null;
            foreach (IndabaFileInfo fileInfo in this.indaba.GetFileInformation(ERROR_EXTENSION))
            {
                stream = this.indaba.Peek(fileInfo);
                buffer = new byte[stream.Length];
                stream.Read(buffer, 0, (int)stream.Length);

                ts1 = new TS1File(Encoding.Default.GetString(buffer), true);
                this.feedbackFiles.Add(ts1);

                this.indaba.Delete(fileInfo);
                response = true;
            }

            foreach (IndabaFileInfo fileInfo in this.indaba.GetFileInformation(OK_EXTENSION))
            {
                stream = this.indaba.Peek(fileInfo);
                buffer = new byte[stream.Length];
                stream.Write(buffer, 0, (int)stream.Length);

                ts1 = new TS1File(Encoding.Default.GetString(buffer), false);
                this.feedbackFiles.Add(ts1);

                this.indaba.Delete(fileInfo);
                response = true;
            }

            return response;
        }

        private bool CheckFtpForFeedback()
        {
            bool response = false;

            ftp = new FTPClient();

            try
            {
                ftp.RemoteHost = Options.ProgramOptions.FtpParameters.HostIP;
                ftp.Connect();
                ftp.Login(Options.ProgramOptions.FtpParameters.HostUser, Options.ProgramOptions.FtpParameters.HostPassword);
                ftp.TransferType = FTPTransferType.BINARY;
                ftp.ConnectMode = FTPConnectMode.PASV;

                // FBJ Added (2006-08-23): It is assumed that the host path takes the FTP client directly to its relevant folder
                //string contractorPath = Path.Combine(ftpParameters.ftpHostPath, ftpParameters.ftpContractor);
                ftp.ChDir(Options.ProgramOptions.FtpParameters.HostPath);

                foreach (string fileListing in ftp.Dir(Options.ProgramOptions.FtpParameters.HostPath, false))
                {
                    if (fileListing.ToUpper().EndsWith(ERROR_EXTENSION))
                    {
                        this.GetFeedbackFile(fileListing, true);
                        response = true;
                        continue;
                    }

                    if (fileListing.ToUpper().EndsWith(OK_EXTENSION))
                    {
                        this.GetFeedbackFile(fileListing, false);
                        response = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Beginnings.Writer.WriteError(string.Empty, ex);
            }
            finally
            {
                if (ftp.IsConnected)
                    ftp.Quit();
            }

            return response;
        }

        private void GetFeedbackFile(string fileListing, bool hasErrors)
        {
            byte[] fileContents = ftp.Get(fileListing);
            this.feedbackFiles.Add(new TS1File(Encoding.Default.GetString(fileContents), hasErrors));
            ftp.Delete(fileListing);
        }

        /// <summary>
        /// Manually checks for feedback files.
        /// </summary>
        /// <returns><c>true</c> if feedback files were found</returns>
        internal bool ManuallyCheckForFeedbackFiles()
        {
            this.ManuallyCheckDirectory(Options.ProgramOptions.FtpParameters.ExportPath);

            return (this.feedbackFiles.Count > 0);
        }

        private void ManuallyCheckDirectory(string directoryName)
        {
            DirectoryInfo dir = new DirectoryInfo(directoryName);
            if (dir.Exists)
            {
                // Check for error feedback files
                FileInfo[] files = dir.GetFiles("*" + ERROR_EXTENSION);
                foreach (FileInfo file in files)
                {
                    FileStream fs = new FileStream(file.FullName, FileMode.Open);
                    byte[] buffer = new byte[fs.Length];
                    fs.Read(buffer, 0, (int)fs.Length);
                    fs.Close();
                    string contents = Encoding.Default.GetString(buffer);
                    this.feedbackFiles.Add(new TS1File(contents, true));
                    file.Delete();
                }

                // Check for OK feedback files
                files = dir.GetFiles("*" + OK_EXTENSION);
                foreach (FileInfo file in files)
                {
                    FileStream fs = new FileStream(file.FullName, FileMode.Open);
                    byte[] buffer = new byte[fs.Length];
                    fs.Read(buffer, 0, (int)fs.Length);
                    fs.Close();
                    string contents = Encoding.Default.GetString(buffer);
                    this.feedbackFiles.Add(new TS1File(contents, false));
                    file.Delete();
                }

                // Check sub-directories
                foreach (DirectoryInfo info in dir.GetDirectories())
                    this.ManuallyCheckDirectory(info.FullName);
            }
        }

        /// <summary>
        /// Processes the feedback files.
        /// </summary>
        internal void ProcessFeedbackFiles()
        {
            foreach (TS1File file in this.feedbackFiles)
            {
                if (file.Read())
                    SqlServerDataProvider.SetFrameError(file.Film);
            }
        }

    }
}
