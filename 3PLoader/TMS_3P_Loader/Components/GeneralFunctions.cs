using System;
using System.IO;
//using Atalasoft.Imaging;
//using Atalasoft.Imaging.Codec;
//using Atalasoft.Imaging.Codec.Jpeg2000;
using EnterpriseDT.Net.Ftp;
using EnterpriseDT.Util.Debug;
using Stalberg.TMS_3P_Loader.Data;
using Stalberg.TMS_3p_Loader.Components;

//dls 090604  - remove all references to Atalasoft and JP2000 conversion
namespace Stalberg.TMS_3P_Loader.Components
{
	/// <summary>
	/// Contains General Functions for FTPing data as well as converting images to JPEG2000.
	/// </summary>
	internal class GeneralFunctions
	{
		// Fields
        string mExportFolder = string.Empty;

		internal FilmFtpCallback FilmFtpCallback = null;

        // Constants
        //internal const long MIN_COMPRESSIBLE_SIZE = 204800;

		/// <summary>
		/// Initializes a new instance of the <see cref="GeneralFunctions"/> class.
		/// </summary>
		public GeneralFunctions()
		{
		}

		#region FTP Functions

		// function to delete the rows in the table
		/// <summary>
		/// Deletes the user rows.
		/// </summary>
		/// <param name="mConnStr">The m conn STR.</param>
		/// <returns></returns>
		public int DeleteUserRows(string mConnStr)
		{
			TOMSdata userDelete = new TOMSdata(mConnStr);
			return userDelete.DeleteRowsUFS();
		}

		// function to delete the rows in the table
		/// <summary>
		/// Deletes the location rows.
		/// </summary>
		/// <param name="mConnStr">The m conn STR.</param>
		/// <param name="filmIntNo">The film int no.</param>
		/// <returns></returns>
		public int DeleteLocationRows(string mConnStr, int filmIntNo)
		{
			TOMSdata locationDelete = new TOMSdata(mConnStr);
			int result = locationDelete.DeleteRowsLocation(filmIntNo);
			return result;
		}

		// function to delete files on local machines
		/// <summary>
		/// Deletes the file.
		/// </summary>
		/// <param name="fullPathName">Full name of the path.</param>
		/// <returns></returns>
		public bool DeleteFile(string fullPathName)
		{
			bool failed;
			try
			{
				// Delete the file
				FileInfo file = new FileInfo(fullPathName);
				file.Delete();

				// If the directory is empty, delete it
				if (file.Directory.GetDirectories().Length == 0 && file.Directory.GetFiles().Length == 0)
					file.Directory.Delete();

				failed = false;
			}
			catch
			{
				failed = true;
				return failed;
			}
			return failed;
		}

		/// <summary>
		/// Copies the file.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="destinationPath">The destination path.</param>
		/// <param name="destinationFile">The destination file.</param>
		/// <returns></returns>
		public bool CopyFile(string source, string destinationPath, string destinationFile)
		{
			bool failed;
			// must first check whether the destination directory is valid
			bool checkDir = CheckFolder(destinationPath);
			if (checkDir)
			{
				failed = true;
				return failed;
			}
			else
			{
				try
				{
					// make a directory
					DirectoryInfo di = new DirectoryInfo(destinationPath);
					if (di.Exists == false)
						di.Create();
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.Assert(false, e.Message);
					failed = false;
					return failed;
				}
			}
			// now can copy
			try
			{
				string destination = destinationPath + @"\" + destinationFile;
				if (File.Exists(destination))
				{
					// do nothing
					failed = false;
				}
				else
				{
					File.Copy(source, destination);
					failed = false;
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.Assert(false, ex.Message);
				failed = true;
				return failed;
			}
			return failed;
		}

		/// <summary>
		/// Checks the folder.
		/// </summary>
		/// <param name="destinationPath">The destination path.</param>
		/// <returns></returns>
		public bool CheckFolder(string destinationPath)
		{
			bool failed = false;
			try
			{
				// must first check whether the destination directory is valid
				if (!Directory.Exists(destinationPath))
				{
					// make a directory
					DirectoryInfo di = new DirectoryInfo(destinationPath);
					if (di.Exists == false)
						di.Create();
				}
				return failed;
			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.Assert(false, e.Message);
				failed = true;
				return failed;
			}
		}

		// Function ftp file to TMS
        /// <summary>
        /// FTPs the file.
        /// </summary>
        /// <param name="ftpParameters">The FTP parameters.</param>
        /// <returns></returns>
		public bool FtpFile(FtpParameters ftpParameters)
		{
			bool failed = false;
            //string contractor = ftpParameters.Contractor;
			string host = ftpParameters.HostIP;
			string user = ftpParameters.HostUser;
			string password = ftpParameters.HostPassword;
			string hostPath = ftpParameters.HostPath;
            //string exportPath = ftpParameters.ExportPath; //not required as the folder is dynamically acquired
			// FBJ Added (2006-08-23): It is assumed that the host path takes the FTP client to the contractor's path
			string contractorPath = hostPath; // +@"\" + contractor;

			try
			{
				// FTP Client Setup
				Logger.CurrentLevel = Level.OFF;
                FTPClient ftp =  new FTPClient();
				ftp.RemoteHost = host;
				ftp.Connect();
				ftp.Login(user, password);
				// Set up passive ASCII transfers
				ftp.ConnectMode = FTPConnectMode.PASV;
				ftp.TransferType = FTPTransferType.BINARY;

				// FBJ Added (2006-08-18): Contractor must go straight to their own directory on FTP server
				//// Change directory to appropriate folder on host - root is ftpRoot
				//// needs protection against missing folder
				//try
				//{
				//    ftp.ChDir(contractorPath);
				//}
				//catch
				//{
				//    ftp.MkDir(contractorPath);
				//}

				// This where it all started
				mExportFolder = ftpParameters.ExportPath;
				if (File.Exists(mExportFolder))
					failed = true;

				else if (Directory.Exists(mExportFolder))
				{
					// This path is a directory - sent should not be processed
					this.ProcessFtpDirectory(ftpParameters, ref ftp,mExportFolder, ref contractorPath);
					//return failed;
				}
				else
					failed = true;

				// Shut down ftp client                
				ftp.Quit();  // compiler says it can't reach this code
				Beginnings.Writer.WriteError("FtpFile quit successful: " + DateTime.Now.ToString());
				return failed;
			}
			catch (Exception e)
			{
				Beginnings.Writer.WriteError("FtpFile quit failure: " + e.Message + " " + DateTime.Now.ToString());
				return failed;
			}
		}

        /// <summary>
        /// Processes the FTP directory.
        /// </summary>
        /// <param name="localFtp">The local FTP.</param>
        /// <param name="ftp">The FTP.</param>
        /// <param name="targetDirectory">The target directory.</param>
        /// <param name="ftpFolder">The FTP folder.</param>
		public void ProcessFtpDirectory(FtpParameters localFtp, ref FTPClient ftp, string targetDirectory, ref string ftpFolder)
		{
			// Recurse into subdirectories of this directory.
			string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
			foreach (string subdirectory in subdirectoryEntries)
			{
				if (!subdirectory.Contains("Sent"))
				{
					string newFolder = ftpFolder;
					ftpFolder += "\\" + subdirectory.Substring(subdirectory.LastIndexOf(@"\") + 1);
					try
					{
						ftp.ChDir(ftpFolder);
					}
					catch
					{
						ftp.MkDir(ftpFolder);
					}

					this.ProcessFtpDirectory(localFtp, ref ftp, subdirectory, ref ftpFolder);

					ftpFolder = newFolder;
					ftp.CdUp();
				}
			}

			//Process the list of files found in the directory.
			string file;
			string[] fileEntries = Directory.GetFiles(targetDirectory);
			foreach (string fileName in fileEntries)
			{
				file = Path.GetFileName(fileName);
				this.ProcessFtpFile(localFtp, ref ftp, file, fileName);
			}

			// Fire callback to check if this is a local film and needs to update its status
			if (this.FilmFtpCallback != null)
				this.FilmFtpCallback(targetDirectory);
		}

        /// <summary>
        /// Processes the FTP file.
        /// </summary>
        /// <param name="localFtp">The local FTP.</param>
        /// <param name="ftp">The FTP.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="targetDirectory">The target directory.</param>
        /// <returns></returns>
		public bool ProcessFtpFile(FtpParameters localFtp, ref FTPClient ftp, string fileName, string targetDirectory)
		{
			// multiple folders are a potential problem
			// must create folder structure on ftp site equivalent to local folder structure
			// ftproot\TMS\3rdParty\Contractor\film\etc
			bool failed = false;
            //string contractor = localFtp.Contractor;
            //string host = localFtp.HostIP;
            //string user = localFtp.HostUser;
            //string password = localFtp.HostPassword;
			string hostPath = localFtp.HostPath;
            //string exportPath = localFtp.ExportPath; //not required as the folder is dynamically acquired
			string dirName = Path.GetDirectoryName(targetDirectory);
			int lenExportPath = mExportFolder.Length;
			string stubPath = dirName.Remove(0, lenExportPath);
			// FBJ Added (2006-08-23): 
			string contractorPath = hostPath; //+@"\" + contractor;
            //string[] fileEntries = Directory.GetFiles(exportPath);
			// build remote file name
			string remoteFile;
			string remoteFullPath;
            //string path;
			//string destinationPath = "";
			string remoteFullFileName;
			string imageFile = targetDirectory;
			// change directory to appropriate folder on host - root is ftpRoot
			// needs protection against missing folder
			// can only mkdir one level at a time
			try
			{
				ftp.ChDir(contractorPath);
			}
			catch
			{
				ftp.MkDir(contractorPath);
			}

			remoteFile = Path.GetFileName(targetDirectory);
            //path = Path.GetDirectoryName(targetDirectory);
			remoteFullPath = contractorPath + @"\" + stubPath;
			remoteFullFileName = contractorPath + @"\" + stubPath + @"\" + remoteFile;
			// change directory to appropriate folder on host - root is ftpRoot
			// needs protection against missing folder
			try
			{
				ftp.ChDir(remoteFullPath);
			}
			catch
			{
				ftp.MkDir(remoteFullPath);
			}

			// Write file to remote folder
			ftp.Put(imageFile, remoteFullFileName);
			Beginnings.Writer.WriteError("FtpFile put: " + remoteFullFileName + " " + DateTime.Now.ToString());

			// FBJ Added (2006-08-18): We're not copying to a sent directory any more, just deleting
			//// copy file to sent folder - check for folder - make folder if necessary
			//// if jp2 extension then don't copy
			//if (Path.GetExtension(imageFile).ToLower() != ".jp2")
			//{
			//    destinationPath = mExportFolder + @"\Sent";
			//    bool copyFailed = CopyFile(imageFile, destinationPath, remoteFile);
			//    if (copyFailed == true)
			//    {
			//        failed = true;
			//        writer.WriteLine("FtpFile copyFailed: " + remoteFullFileName + " " + DateTime.Now.ToString());
			//        //writer.WriteLine();
			//        writer.Flush();
			//        return failed;
			//    }
			//}

			// delete file from folder
			bool deleteFailed = DeleteFile(imageFile);
			if (deleteFailed)
			{
				Beginnings.Writer.WriteError("FtpFile deleteFailed: " + remoteFullFileName + " " + DateTime.Now.ToString());
				return true;
			}

			return failed;
		}

		#endregion

        # region NormalImageProcessing
        /// <summary>
        /// Processes the image directory.
        /// </summary>
        /// <param name="targetDirectory">The target directory.</param>
		public void ProcessImageDirectory(string targetDirectory)
		{
			//Process the list of files found in the directory.
			string file;
			string[] fileEntries = Directory.GetFiles(targetDirectory);
			foreach (string fileName in fileEntries)
			{
				file = Path.GetFileName(fileName);
				this.ProcessImageFile(file, fileName);
			}

			// Recurse into subdirectories of this directory.
			string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
			foreach (string subdirectory in subdirectoryEntries)
			{
				if (!subdirectory.Contains("Sent"))
					this.ProcessImageDirectory(subdirectory);
			}
		}

        /// <summary>
        /// Processes the image file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="targetDirectory">The target directory.</param>
        /// <returns></returns>
		public bool ProcessImageFile(string fileName, string targetDirectory)
		{
			bool failed = false;
			string jpgFile;

            FileInfo file = null;
            try
            {
                file = new FileInfo(targetDirectory);

                // Is it still a JPEG
                //if (file.Extension.ToLower() == ".jpg" && file.Length > MIN_COMPRESSIBLE_SIZE)
                if (file.Extension.ToLower() == ".jpg")
                {
                    if (!(file.Name.ToLower().Contains("_reg") || file.Name.ToLower().Contains("drv")))
                    {
                        // Delete file .jpg from folder
                        jpgFile = Path.GetFileName(targetDirectory);

                        bool deleteFailed = DeleteFile(targetDirectory);
                        if (deleteFailed)
                        {
                            failed = true;
                            Beginnings.Writer.WriteError("JPG deleteFailed: " + jpgFile + " " + DateTime.Now.ToString());
                            return failed;
                        }

                        return failed;
                    }
                }

                return failed;
            }
            catch (Exception e)
            {
                Beginnings.Writer.WriteError("ProcessImageFile failed: " + e.Message + " " + DateTime.Now.ToString());
                return true;
            }
            finally
            {
                file = null;
            }
		}

#endregion

        #region JPEG Conversion - no longer in use

        /// <summary>
        /// Converts the JPG image to JP2.
        /// </summary>
        /// <returns></returns>
        //public bool ConvertJpgToJp2()
        //{
        //    bool failed = false;
        //    mExportFolder = Options.ProgramOptions.FtpParameters.ExportPath; //this where it all started
        //    // multiple folders are a potential problem
        //    if (File.Exists(mExportFolder))
        //    {
        //        // This path is a file
        //        //ProcessImageFile(ref writer, fileName, exportFolder);
        //        //// user must supply a folder to start off
        //        failed = true;
        //        return failed;
        //    }
        //    else if (Directory.Exists(mExportFolder))
        //    {
        //        // This path is a directory
        //        // \sent should not be processed
        //        this.ProcessImageDirectory(mExportFolder);
        //        return failed;
        //    }
        //    else
        //    {
        //        failed = true;
        //        return failed;
        //    }
        //}

        ///// <summary>
        ///// Processes the image file.
        ///// </summary>
        ///// <param name="fileName">Name of the file.</param>
        ///// <param name="targetDirectory">The target directory.</param>
        ///// <returns></returns>
        //public bool ProcessImageFile(string fileName, string targetDirectory)
        //{
        //    bool failed = false;
        //    //string exportPath = targetDirectory;
        //    //string destinationPath = "";
        //    //string jp2File;
        //    string jpgFile;
        //    //double compression;
        //    //string extension = "jp2";
        //    //Jp2Decoder jp2 = new Jp2Decoder();
        //    //RegisteredDecoders.Decoders.Add(jp2);

        //    // 17 images >> 5%
        //    // Reg number & Drv images leave as JPEG
        //    // all the rest 2.5%
        //    FileInfo file = null;
        //    try
        //    {
        //        file = new FileInfo(targetDirectory);

        //        // Is it still a JPEG
        //        if (file.Extension.ToLower() == ".jpg" && file.Length > MIN_COMPRESSIBLE_SIZE)
        //        {
        //            if (!(file.Name.ToLower().Contains("_reg") || file.Name.ToLower().Contains("drv")))
        //            {
        //                //if (file.Name.ToLower().Contains("_17"))
        //                //    compression = 5.0;
        //                //else
        //                //    compression = 2.5;

        //                //dls 090604  - remove all references to Atalasoft
        //                //string path = Path.GetDirectoryName(targetDirectory);
        //                //jp2File = Path.ChangeExtension(targetDirectory, ".jp2");
        //                //jpgFile = Path.GetFileName(targetDirectory);

        //                //AtalaImage myImage = new AtalaImage(targetDirectory);
        //                //myImage.Save(jp2File, new Jp2Encoder(compression), null);

        //                // FBJ Added (2006-08-21): We're not keeping backups of images any longer
        //                ////copy file.jpg to sent folder - make sure \sent is off root - not in recursive structure
        //                //destinationPath = mExportFolder + @"\Sent";
        //                //string source = targetDirectory;
        //                //bool copyFailed = CopyFile(source, destinationPath, jpgFile);
        //                //if (copyFailed == true)
        //                //{
        //                //    failed = true;
        //                //    writer.WriteLine("JPG copyFailed: " + jpgFile + " " + DateTime.Now.ToString());
        //                //    //writer.WriteLine();
        //                //    writer.Flush();
        //                //    return failed;
        //                //}

        //                // Delete file .jpg from folder

        //                jpgFile = Path.GetFileName(targetDirectory);

        //                bool deleteFailed = DeleteFile(targetDirectory);
        //                if (deleteFailed)
        //                {
        //                    failed = true;
        //                    Beginnings.Writer.WriteError("JPG deleteFailed: " + jpgFile + " " + DateTime.Now.ToString());
        //                    return failed;
        //                }

        //                //if (Options.ProgramOptions.IsDebug)
        //                //    Beginnings.Writer.WriteLine("\tConverting {0} to JPEG 2000, which was {1:N} bytes.", file.Name, file.Length);

        //                return failed;
        //            }
        //        }

        //        return failed;
        //    }
        //    catch (Exception e)
        //    {
        //        Beginnings.Writer.WriteError("JPG >> JP2 conversion failed: " + e.Message + " " + DateTime.Now.ToString());
        //        return true;
        //    }
        //    finally
        //    {
        //        file = null;
        //    }
        //}


        #endregion

	}
}
