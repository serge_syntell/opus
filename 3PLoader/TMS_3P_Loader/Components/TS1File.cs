using System;
using System.IO;
using System.Text;
using Stalberg.TMS_3P_Loader.Data;

namespace Stalberg.TMS_3P_Loader.Components
{
    /// <summary>
    /// Contains all the logic for creating TS1 files
    /// </summary>
    internal class TS1File
    {
        // Fields
        private Film film;
        private StringBuilder sb;
        private string fileName = string.Empty;
        private string ts1FileContents = string.Empty;
        private bool hasErrors = false;
        private int TS1_Version = 3;
        private string source = string.Empty;

        // Static 
        private static string outputDirectory = string.Empty;

        // Constant
        private const char DELIMITER = (char)255; // � - character
        private const char ERROR_DELIMITER = (char)254;
        internal const string TS1_EXTENSION = ".TS1";

        /// <summary>
        /// Gets or sets the output directory.
        /// </summary>
        /// <value>The output directory.</value>
        public static string OutputDirectory
        {
            get { return outputDirectory; }
            set { outputDirectory = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TS1File"/> class.
        /// </summary>
        /// <param name="film">The film to create a TS1 file for.</param>
        /// <remarks>This constructor is used for creating TS1 files for export to TMS.</remarks>
        public TS1File(Film film)
        {
            this.film = film;
            this.sb = new StringBuilder(500000);

            // Ensure that the export directory exists
            string path = Path.Combine(TS1File.outputDirectory, film.AuthorityCode + "\\" + film.FilmNumber);
            DirectoryInfo directory = new DirectoryInfo(path);
            if (!directory.Exists)
                directory.CreateSubdirectory(film.FilmNumber);

            path = Path.Combine(directory.FullName, film.FilmNumber);
            this.film.OutputDirectory = path;
            this.fileName = path + TS1_EXTENSION;
        }

        /// <summary>
        /// Gets the name of the source site that the file originated from.
        /// </summary>
        /// <value>The source.</value>
        public string Source
        {
            get { return this.source; }
        }

        /// <summary>
        /// Gets the film behind this TS1 file.
        /// </summary>
        /// <value>The film.</value>
        internal Film Film
        {
            get { return film; }
        }

        /// <summary>
        /// Gets the name of the TS1 file.
        /// </summary>
        /// <value>The name of the file.</value>
        public string FileName
        {
            get { return fileName; }
        }

        /// <summary>
        /// Gets or sets the TS1 export version number.
        /// </summary>
        /// <value>The version.</value>
        public int Version
        {
            get { return this.TS1_Version; }
            set { this.TS1_Version = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TS1File"/> class.
        /// </summary>
        /// <param name="ts1FileContents">The TS1 file contents for parsing.</param>
        /// <remarks>This constructor us used for processing files returned for feedback</remarks>
        public TS1File(string ts1FileContents, bool hasErrors)
        {
            this.ts1FileContents = ts1FileContents;
            this.hasErrors = hasErrors;
        }

        #region Reader Methods

        /// <summary>
        /// Reads only the header information from the TS1 File.
        /// </summary>
        public void ReadHeader()
        {
            if (this.ts1FileContents.Length == 0)
                throw new ApplicationException("The TS1 file has not been initialised for reading.");

            this.film = new Film();
            this.film.HasErrors = this.hasErrors;

            try
            {
                string[] lines = ts1FileContents.Split(new char[] { '\n' });
                if (lines.Length == 0)
                    throw new ApplicationException("The TS1 file contents cannot contain nothing.");

                // Read header
                this.ProcessHeader(lines[0].Trim());
            }
            catch { }
        }

        /// <summary>
        /// Reads the contents of a TS1 file and updates the database
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if there were no problems with reading the file contents
        /// </returns>
        public bool Read()
        {
            if (this.ts1FileContents.Length == 0)
                throw new ApplicationException("The TS1 file has not been initialised for reading.");

            bool response = false;

            this.film = new Film();
            this.film.HasErrors = this.hasErrors;

            try
            {
                string[] lines = ts1FileContents.Split(new char[] { '\n' });
                if (lines.Length == 0)
                    throw new ApplicationException("The TS1 file contents cannot contain nothing.");

                // Read header
                this.ProcessHeader(lines[0].Trim());

                // If there are no errors then process this as an OK
                if (!hasErrors)
                {
                    this.film.SetStatus(TMSLoaderStatus.TMSComplete);
                    // TODO: insert a comment into Film Comment dating the completion of the import 
                    // no longer necessary - comment is inserted in proc that updates the status
                    return true;
                }

                // Set the status code on the film
                this.film.SetStatus(TMSLoaderStatus.TMSErrorsReceived);

                // Check for Frame Errors
                if (lines.Length > 1 && lines[1].Trim().Length > 0)
                {
                    for (int i = 1; i < lines.Length; i++)
                    {
                        string line = lines[i].Trim();
                        if (line.Length > 0)
                            this.ProcessFrameData(line);
                    }
                }

                response = true;
            }
            catch (Exception ex)
            {
                Beginnings.Writer.WriteError(ex);
            }

            return response;
        }

        private void ProcessFrameData(string line)
        {
            string[] split = line.Split(new char[] { DELIMITER });
            if (split.Length < 46)
                throw new ApplicationException("The frame data row does not have a valid number of columns.");

            if (split[0].Trim().ToUpper() != "D")
                throw new ApplicationException(
                    "The frame data line supplied does not have the valid line marker at the beginning of the line.");

            FrameData frame = new FrameData();
            frame.Version = int.Parse(split[1].Trim());
            frame.Contractor = split[2].Trim();
            frame.FrameNo = split[6].Trim();
            frame.Sequence = split[7].Trim()[0];
            frame.RegistrationNumber = split[8].Trim();
            frame.ReferenceNo = int.Parse(split[9].Trim());
            string dateString =
                string.Format("{0}-{1}-{2} {3}:{4}", split[10].Trim(), split[11].Trim(), split[12].Trim(),
                              split[13].Trim(), split[14].Trim());
            frame.OffenceDate = DateTime.Parse(dateString);
            frame.FirstSpeed = int.Parse(split[15].Trim());
            frame.SecondSpeed = int.Parse(split[16].Trim());
            frame.ElapsedTime = split[17].Trim();
            frame.VehicleMakeCode = split[18].Trim();
            frame.VehicleMakeDescription = split[19].Trim();
            frame.VehicleTypeCode = split[20].Trim();
            frame.VehicleTypeDescription = split[21].Trim();
            frame.CourtNo = split[22].Trim();
            frame.CourtName = split[23].Trim();
            frame.OfficerNo = split[24].Trim();
            frame.OfficerGroup = split[25].Trim();
            frame.OfficerSurname = split[26].Trim();
            frame.OfficerInit = split[27].Trim();
            frame.CameraID = split[28].Trim();
            frame.CameraLocationCode = split[29].Trim();
            frame.LocationDescription = split[30].Trim();
            frame.OffenceCode = split[31].Trim();
            frame.OffenceDescription = split[32].Trim();
            frame.OffenceLetter = split[33].Trim()[0];
            frame.OffenderType = int.Parse(split[34].Trim());
            frame.FineAmount = decimal.Parse(split[35].Trim());
            frame.FineAllocation = split[36].Trim()[0];
            frame.Speed = int.Parse(split[37].Trim());
            frame.RoadTypeCode = int.Parse(split[38].Trim());
            frame.TravelDirection = split[41].Trim()[0];
            frame.RejectionReason = split[41].Trim();
            frame.Violation = split[42].Trim()[0];
            frame.ConfirmViolation = split[43].Trim()[0];
            frame.ManualView = split[44].Trim()[0];
            frame.MultipleFrames = split[45].Trim()[0];
            if (frame.Version > 1)
                frame.CameraSerial = split[46].Trim();
            // Handle an Error column only if it is present
            if (split.Length > (this.Version > 1 ? 46 : 47))
            {
                string[] errorSplit = split[(this.Version == 1 ? 46 : 47)].Split(new char[] { ERROR_DELIMITER });
                foreach (string error in errorSplit)
                {
                    if (error.Trim().Length > 0)
                        frame.Errors.Add(error.Trim());
                }
            }

            this.film.Frames.Add(frame);
        }

        private void ProcessHeader(string headerLine)
        {
            string[] split = headerLine.Split(new char[] { DELIMITER });
            if (split.Length < 19)
                throw new ApplicationException("The header row does not have a valid number of columns.");

            if (split[0].Trim().ToUpper() != "H")
                throw new ApplicationException(
                    "The header line supplied does not have the header line marker at the beginning of the line.");

            this.Version = int.Parse(split[1].Trim());
            this.film.Contractor = split[2].Trim();
            this.film.AuthorityCode = split[3].Trim();
            this.film.AuthorityName = split[4].Trim();
            this.source = this.film.AuthorityName;
            this.film.AuthorityNumber = split[5].Trim();
            this.film.CDLabel = split[6].Trim();
            this.film.FilmNumber = split[7].Trim();
            this.film.FilmDescription = split[8].Trim();
            this.film.MultipleFrames = split[9].Trim()[0];
            this.film.MultipleViolations = split[10].Trim()[0];
            this.film.LastReferenceNumber = int.Parse(split[11].Trim());
            this.film.FilmType = split[12].Trim()[0];
            string dateString =
                string.Format("{0}-{1}-{2} {3}:{4}", split[13].Trim(), split[14].Trim(), split[15].Trim(),
                              split[16].Trim(), split[17].Trim());
            this.film.FirstOffence = DateTime.Parse(dateString);
            if (split.Length > 19)
                this.film.ErrorMessage = split[19].Trim();
        }

        #endregion

        #region Writer Methods

        #region SQL Server Specific

        /// <summary>
        /// Writes the TS1 file using SQL Server as a source for additional data.
        /// </summary>
        public void WriteFromSqlServer()
        {
            // Set the source
            this.source = film.AuthorityName;

            // Create the header
            this.CreateHeader();

            // Get the data rows
            SqlServerDataProvider.GetFramesForFilm(this.film);

            // Write data rows
            this.CreateDataRows();

            // Get Images
            this.RetrieveImagesForFramesFromSqlServer();

            // Write Image Rows
            this.WriteImageRows();

            // Save the file contents
            this.CreateFile();
        }

        private void RetrieveImagesForFramesFromSqlServer()
        {
            // Retrieve image data per frame
            foreach (FrameData frame in film.Frames)
                SqlServerDataProvider.GetImageData(frame);
        }

        #endregion

        #region MDB Specific

        /// <summary>
        /// Writes The TS1 file using the data already in the Film object that has been created from the CD extract.
        /// </summary>
        public void WriteFromMdb()
        {
            // Create the header
            this.CreateHeader();

            // Write data rows
            this.CreateDataRows();

            // Write Image Rows
            this.WriteImageRows();

            // Save the file contents
            this.CreateFile();
        }

        #endregion

        private void WriteImageRows()
        {
            for (int i = 0; i < this.film.Frames.Count; i++)
            {
                FrameData frame = this.film.Frames[i];

                foreach (Image image in frame.Images)
                {
                    sb.Append('I');
                    sb.Append(DELIMITER);
                    //sb.Append(1);
                    sb.Append(this.Version);
                    sb.Append(DELIMITER);
                    sb.Append("SI");
                    sb.Append(DELIMITER);
                    sb.Append(image.ImageType);
                    sb.Append(DELIMITER);
                    sb.Append(this.film.AuthorityCode.Trim());
                    sb.Append(DELIMITER);
                    sb.Append(this.film.FilmNumber.Trim());
                    sb.Append(DELIMITER);
                    if (frame.Sequence == 'B')
                        sb.Append(this.film.Frames[i - 1].FrameNo.Trim());
                    else
                        sb.Append(frame.FrameNo.Trim());
                    sb.Append(DELIMITER);
                    sb.Append(frame.ReferenceNo);
                    sb.Append(DELIMITER);
                    sb.Append(image.Name.Trim());
                    sb.Append(DELIMITER);
                    sb.Append(image.X);
                    sb.Append(DELIMITER);
                    sb.Append(image.Y);
                    sb.Append("\r\n");
                }
            }
        }

        private void CreateDataRows()
        {
            foreach (FrameData frame in this.film.Frames)
            {
                if (frame.Sequence == 'B')
                    continue;

                sb.Append('D'); // 1
                sb.Append(DELIMITER);
                //sb.Append(film.Version); // 2         //film.Version is never set anywhere!!!!
                sb.Append(this.Version); // 2
                sb.Append(DELIMITER);
                sb.Append("SI"); // 3
                sb.Append(DELIMITER);
                sb.Append(this.film.AuthorityCode.Trim()); // 4
                sb.Append(DELIMITER);
                sb.Append(this.film.CDLabel.Trim()); // 5
                sb.Append(DELIMITER);
                sb.Append(this.film.FilmNumber.Trim()); // 6
                sb.Append(DELIMITER);
                sb.Append(frame.FrameNo.Trim()); // 7
                sb.Append(DELIMITER);
                sb.Append(frame.Sequence); // 8
                sb.Append(DELIMITER);
                sb.Append(frame.RegistrationNumber.Trim()); // 9
                sb.Append(DELIMITER);
                sb.Append(frame.ReferenceNo); // 10
                sb.Append(DELIMITER);
                sb.Append(frame.OffenceDate.Year); // 11
                sb.Append(DELIMITER);
                sb.Append(frame.OffenceDate.Month); // 12
                sb.Append(DELIMITER);
                sb.Append(frame.OffenceDate.Day); // 13
                sb.Append(DELIMITER);
                sb.Append(frame.OffenceDate.Hour); // 14
                sb.Append(DELIMITER);
                sb.Append(frame.OffenceDate.Minute); // 15
                sb.Append(DELIMITER);
                sb.Append(frame.FirstSpeed); // 16
                sb.Append(DELIMITER);
                sb.Append(frame.SecondSpeed); // 17
                sb.Append(DELIMITER);
                sb.Append(frame.ElapsedTime.Trim()); // 18
                sb.Append(DELIMITER);
                sb.Append(frame.VehicleMakeCode.Trim()); // 19
                sb.Append(DELIMITER);
                sb.Append(frame.VehicleMakeDescription.Trim()); // 20
                sb.Append(DELIMITER);
                sb.Append(frame.VehicleTypeCode.Trim()); // 21
                sb.Append(DELIMITER);
                sb.Append(frame.VehicleTypeDescription.Trim()); // 22
                sb.Append(DELIMITER);
                sb.Append(frame.CourtNo.Trim()); // 23
                sb.Append(DELIMITER);
                sb.Append(frame.CourtName.Trim()); // 24
                sb.Append(DELIMITER);
                sb.Append(frame.OfficerNo.Trim()); // 25
                sb.Append(DELIMITER);
                sb.Append(frame.OfficerGroup.Trim()); // 26
                sb.Append(DELIMITER);
                sb.Append(frame.OfficerSurname.Trim()); // 27
                sb.Append(DELIMITER);
                sb.Append(frame.OfficerInit.Trim()); // 28
                sb.Append(DELIMITER);
                sb.Append(frame.CameraID.Trim()); // 29
                sb.Append(DELIMITER);
                sb.Append(frame.CameraLocationCode.Trim()); // 30
                sb.Append(DELIMITER);
                sb.Append(frame.LocationDescription.Trim()); // 31
                sb.Append(DELIMITER);
                sb.Append(frame.OffenceCode.Trim()); // 32
                sb.Append(DELIMITER);
                sb.Append(frame.OffenceDescription.Trim()); // 33
                sb.Append(DELIMITER);
                sb.Append(frame.OffenceLetter); // 34
                sb.Append(DELIMITER);
                sb.Append(frame.OffenderType); // 35
                sb.Append(DELIMITER);
                sb.Append(frame.FineAmount); // 36
                sb.Append(DELIMITER);
                sb.Append(frame.FineAllocation); // 37
                sb.Append(DELIMITER);
                sb.Append(frame.Speed); // 38
                sb.Append(DELIMITER);
                sb.Append(frame.RoadTypeCode); // 39
                sb.Append(DELIMITER);
                sb.Append(frame.RoadTypeDescription.Trim()); // 40
                sb.Append(DELIMITER);
                sb.Append(frame.TravelDirection); // 41
                sb.Append(DELIMITER);
                sb.Append(frame.RejectionReason.Trim()); // 42
                sb.Append(DELIMITER);
                sb.Append(frame.Violation); // 43
                sb.Append(DELIMITER);
                sb.Append(frame.ConfirmViolation); // 44
                sb.Append(DELIMITER);
                sb.Append(frame.ManualView); // 45
                sb.Append(DELIMITER);
                sb.Append(frame.MultipleFrames); // 46

                if (this.Version > 1)
                {
                    sb.Append(DELIMITER);
                    sb.Append(frame.CameraSerial); // 47

                    if (this.Version > 2)
                    {
                        //FT 100430 For Average speed over distance
                        sb.Append(DELIMITER);
                        sb.Append(frame.ASDGPSDateTime1); // 48

                        sb.Append(DELIMITER);
                        sb.Append(frame.ASDGPSDateTime2); // 49

                        sb.Append(DELIMITER);
                        sb.Append(frame.ASDTimeDifference); // 50

                        sb.Append(DELIMITER);
                        sb.Append(frame.ASDSectionStartLane); // 51

                        sb.Append(DELIMITER);
                        sb.Append(frame.ASDSectionEndLane); // 52

                        sb.Append(DELIMITER);
                        sb.Append(frame.ASDSectionDistance); // 53


                        sb.Append(DELIMITER);
                        sb.Append(frame.ASD1stCamUnitID); // 54

                        sb.Append(DELIMITER);
                        sb.Append(frame.ASD2ndCamUnitID); // 55

                        sb.Append(DELIMITER);
                        sb.Append(frame.ASDCameraSerialNo1); // 56

                        sb.Append(DELIMITER);
                        sb.Append(frame.ASDCameraSerialNo2); // 57
                    }
                }
                sb.Append("\r\n");
            }
        }

        private void CreateFile()
        {
            try
            {
                // Create the file
                FileInfo file = new FileInfo(this.FileName);
                FileStream fs = file.OpenWrite();
                byte[] buffer = Encoding.UTF7.GetBytes(this.sb.ToString());
                fs.Write(buffer, 0, buffer.Length);
                fs.Close();

                // Set the file name in the DB
                switch (Options.ProgramOptions.Mode)
                {
                    case Mode.Centralised:
                        SqlServerDataProvider.SetTS1FileName(film, file.Name);
                        break;
                }
                file = null;
            }
            catch (Exception ex)
            {
                Beginnings.Writer.WriteError("There was an error saving the TS1 file.", ex);
            }
        }

        private void CreateHeader()
        {
            sb.Append('H'); // 1
            sb.Append(DELIMITER);
            sb.Append(this.Version); // 2
            sb.Append(DELIMITER);
            sb.Append("SI"); // 3
            sb.Append(DELIMITER);
            sb.Append(film.AuthorityCode.Trim()); // 4
            sb.Append(DELIMITER);
            sb.Append(film.AuthorityName.Trim()); // 5
            sb.Append(DELIMITER);
            sb.Append(film.AuthorityNumber.Trim()); // 6
            sb.Append(DELIMITER);
            sb.Append(film.CDLabel.Trim()); // 7
            sb.Append(DELIMITER);
            sb.Append(film.FilmNumber.Trim()); // 8
            sb.Append(DELIMITER);
            sb.Append(film.FilmDescription.Trim()); // 9
            sb.Append(DELIMITER);
            sb.Append(film.MultipleFrames.ToString()); // 10
            sb.Append(DELIMITER);
            sb.Append(film.MultipleViolations); // 11
            sb.Append(DELIMITER);
            sb.Append(film.LastReferenceNumber); // 12
            sb.Append(DELIMITER);
            sb.Append(film.FilmType); // 13
            sb.Append(DELIMITER);
            sb.Append(film.FirstOffence.Year); // 14
            sb.Append(DELIMITER);
            sb.Append(film.FirstOffence.Month); // 15
            sb.Append(DELIMITER);
            sb.Append(film.FirstOffence.Day); // 16
            sb.Append(DELIMITER);
            sb.Append(film.FirstOffence.Hour); // 17
            sb.Append(DELIMITER);
            sb.Append(film.FirstOffence.Minute); // 18
            sb.Append(DELIMITER);
            sb.Append('S'); // 19
            sb.Append("\r\n");
        }

        #endregion
    }
}