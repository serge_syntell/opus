using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS_3p_Loader.Components
{
	public class FtpParameters 
	{
		//added elements to struct
		public int ftpID;
        public String ftpContractor;
		public String ftpProcess;
		public String ftpExportServer;
		public String ftpExportPath;
		public String ftpHostServer;
		public String ftpHostIP;
		public String ftpHostUser;
		public String ftpHostPass;
		public String ftpHostPath;
	}

	/// <summary>
	/// Summary description for TOMSdata.
	/// </summary>
	public class TOMSdata
	{
        string mConstr = string.Empty;

		public TOMSdata(string vConstr)
		{
			mConstr = vConstr;
		}

		public FtpParameters GetFTP(string ftpProcess)
		{
			// Returns a single row
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("GetFtpParameters", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFtpProcess = new SqlParameter("@FtpProcess", SqlDbType.VarChar,20);
			parameterFtpProcess.Value = ftpProcess;
			myCommand.Parameters.Add(parameterFtpProcess);
			
			try
			{
				myConnection.Open();
				SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
				// Create Struct
				FtpParameters myFTP = new FtpParameters();

				while (result.Read())
				{
					// Populate Struct
					myFTP.ftpID = Convert.ToInt32(result["FtpIntNo"]);
					myFTP.ftpProcess = result["FtpProcess"].ToString();
					myFTP.ftpExportServer = result["FtpExportServer"].ToString();
					myFTP.ftpExportPath = result["FtpExportPath"].ToString();
					myFTP.ftpHostServer = result["FtpHostServer"].ToString();
					myFTP.ftpHostIP = result["FtpHostIP"].ToString();
					myFTP.ftpHostUser = result["FtpHostUserName"].ToString();
					myFTP.ftpHostPass = result["FtpHostUserPassword"].ToString();
					myFTP.ftpHostPath = result["FtpHostFtpPath"].ToString();
				}
				return myFTP;
			}
			catch(Exception e)
			{
				string error = e.Message;
				return null;
			}
		}

		public SqlDataReader GetUserStats()
		{
			// Returns a recordset
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("ExportUserFrameStats", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			// currently no parameters
			//			SqlParameter parameterCustomerID = new SqlParameter("@CustomerID", SqlDbType.Int, 4);
			//			parameterCustomerID.Value = customerID;
			//			myCommand.Parameters.Add(parameterCustomerID);
			try
			{
				myConnection.Open();
				SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

				return result;
			}
			catch(Exception e)
			{
				string error = e.Message;
				return null;
			}
		}

		public SqlDataReader GetFilms()
		{
			// Returns a recordset
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("Temp_Camera_Location_Stats_Films", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			try
			{
				myConnection.Open();
				SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

				return result;
			}
			catch(Exception e)
			{
				string error = e.Message;
				return null;
			}
		}

		public SqlDataReader GetLocationStats(int filmIntNo)
		{
			// Returns a recordset
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("Temp_Camera_Loc_Stats_Get", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFilmID = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
			parameterFilmID.Value = filmIntNo;
			myCommand.Parameters.Add(parameterFilmID);
			try
			{
				myConnection.Open();
				SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

				return result;
			}
			catch(Exception e)
			{
				string error = e.Message;
				return null;
			}
		}

		public int DeleteRowsUFS ()
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("UFSDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Close();
				return 0;
			}
			catch(Exception e)
			{
				string error = e.Message;
				return 1;
			}
		}

		public int DeleteRowsLocation(int filmIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("Temp_Camera_Location_Stats_Delete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFilmID = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
			parameterFilmID.Value = filmIntNo;
			parameterFilmID.Direction = ParameterDirection.Input;
			myCommand.Parameters.Add(parameterFilmID);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Close();
				return 0;
			}
			catch(Exception e)
			{
				string error = e.Message;
				return 1;
			}
		}
	}
}
