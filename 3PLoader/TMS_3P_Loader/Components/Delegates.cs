using System;
using System.Collections.Generic;
using System.Text;

namespace Stalberg.TMS_3P_Loader.Components
{
	/// <summary>
	/// The delegate that defines the method signature to call when the FTP process has completed
	/// </summary>
	/// <param name="filmNo">Tthe film number of the film that has been FTP</param>
	internal delegate void FilmFtpCallback(string filmNo);
}
