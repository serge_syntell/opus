/*
 * Created by: 
 * Created: 14 September 2006
 */

using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;

namespace Stalberg.TMS_3P_Loader.Data
{
    /// <summary>
    /// Represents an object that can open an Access MDB file and provide information from it
    /// </summary>
    internal class AccessDataProvider
    {
        // Fields
        private OleDbConnection con;
        private int version = 1;

        // Constants
        private const string USER_NAME = "Admin";
        private const string PASSWORD = "foryoureyesonly";

        /// <summary>
        /// Initializes a new instance of the <see cref="AccessDataProvider"/> class.
        /// </summary>
        public AccessDataProvider(string fileName)
        {
            string connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Persist Security Info=True;Jet OLEDB:Database Password={1};", fileName, PASSWORD);
            con = new OleDbConnection(connectionString);

            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                Beginnings.Writer.WriteError("Error opening MDB.", ex);
            }
        }

        /// <summary>
        /// Gets the MDB export version that detirmins the format of the TS1 file.
        /// </summary>
        /// <value>The version.</value>
        public int Version
        {
            get { return this.version; }
        }

        /// <summary>
        /// Disposes this Access database provider.
        /// </summary>
        public void Dispose()
        {
            if (this.con != null)
            {
                this.con.Close();
                this.con.Dispose();
            }
        }

        /// <summary>
        /// Gets the films in the Access database, adding them to the provided list.
        /// </summary>
        /// <param name="films">The films.</param>
        /// <returns>
        /// 	<c>true</c> if films were added to the list.
        /// </returns>
        public bool GetFilms(List<Film> films)
        {
            bool response = false;

            this.CheckDatabaseExportVersion();

            string sql =
                @"SELECT DISTINCT FilmNo, AuthCode, AuthName, AuthNo, FilmNo, CDLabel, FilmDescr, TruvellaFlag, 
                    MIN(OffenceDate) AS FirstOffence , MIN(ReferenceNo) AS FirstReference , MultFrames, MultipleViolations 
                 FROM InputFromTOMS 
                GROUP BY AuthCode, AuthName, AuthNo, FilmNo, CDLabel, FilmDescr, TruvellaFlag, MultFrames, MultipleViolations";
            //ORDER BY FilmNo";
            OleDbCommand com = new OleDbCommand(sql, this.con);

            try
            {
                OleDbDataReader reader = com.ExecuteReader();
                if (reader.HasRows)
                    response = true;

                bool found;
                while ((reader.Read()))
                {
                    found = false;
                    Film film = new Film();
                    film.FilmNumber = (string)reader["FilmNo"];
                    film.AuthorityCode = (string)reader["AuthCode"];
                    film.AuthorityName = (string)reader["AuthName"];
                    film.AuthorityNumber = (string)reader["AuthNo"];
                    film.CDLabel = (string)reader["CDLabel"];
                    film.Contractor = Options.ProgramOptions.FtpParameters.Contractor;
                    film.FilmDescription = (string)reader["FilmDescr"];
                    film.FilmType = ((string)reader["TruvellaFlag"])[0];
                    film.FirstOffence = (DateTime)reader["FirstOffence"];
                    film.LastReferenceNumber = (int)reader["FirstReference"];
                    film.MultipleFrames = ((string)reader["MultFrames"])[0];
                    film.MultipleViolations = ((string)reader["MultipleViolations"])[0];

                    foreach (Film film1 in films)
                    {
                        if (film1.FilmNumber.Equals(film.FilmNumber))
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        film.ExtractAllImages = Options.ProgramOptions.ExtractAllImages;
                        films.Add(film);
                    }
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Beginnings.Writer.WriteError(ex);
            }

            return response;
        }

        /// <summary>
        /// Gets the data for the provided film.
        /// </summary>
        /// <param name="film">The film.</param>
        /// <returns>
        /// 	<c>true</c> if there were errors while getting film data
        /// </returns>
        public bool GetFilmData(Film film)
        {
            bool response = true;

            string sql = string.Empty;

            //dls 090613 - add an additional check here to cound the total no. frames in a film - picked up a situation where the joins to the lookup tables
            // didn't find one of the frames

            sql = @"SELECT DISTINCT FrameNo FROM InputFromTOMS WHERE FilmNo = '" + film.FilmNumber + @"' ";
            int noOFFrames = 0;

            OleDbCommand com1 = new OleDbCommand(sql, this.con);

            try
            {
                OleDbDataReader reader = com1.ExecuteReader();

                if (!reader.HasRows)
                {
                    throw new ApplicationException(string.Format("The film {0} has returned no frames.", film.FilmNumber));
                }
                else
                {
                    while (reader.Read())
                    {
                        noOFFrames++;
                    }
                }
                reader.Close();
            }
            catch(Exception e)
            {
                Beginnings.Writer.WriteError(e.Message);
                return true;
            }

            switch (this.version)
            {
                case 1:
                    sql = @"SELECT DISTINCT CameraID, [CameraLocCode], [Violation], CourtName, [CourtNo], ElapsedTime, FineAlloc, FineAmnt, 
                        FirstSpeed, FrameNo, LocDescr, ManualView, MultFrames, i.OffenceCode, OffenceDate, OffenceDescr, OffenceLetter, 
                        o.OfficerGroup, OfficerInit,
                        i.[OfficerNo], [OfficerSName], [OffenderType], ReferenceNo, RegNo, RejReason, i.RdTypeCode, RdTypeDescr, SecondSpeed, 
                        [Sequence], i.[Speed], [TravelDirection], m.VehMCode, VehMDescr, i.VehTCode, VehTDescr, [Violation], [TruvellaFlag] 
                        FROM InputFromTOMS i, Officer o, Offence c, RoadType r, VehicleMake m, VehicleType t
                        WHERE i.OfficerNo = o.OfficerNo
                        AND c.OffenceCode = i.OffenceCode
                        AND c.VehTCode = t.VehTCode
                        AND c.RdTypeCode = r.RdTypeCode
                        AND c.Speed = i.Speed
                        AND  r.RdTypeCode = i.RdTypeCode
                        AND i.VehMCode = m.VehMcode
                        AND i.VehTCode = t.VehTCode
                        AND FilmNo = '" + film.FilmNumber + @"' 
                        ORDER BY FrameNo";
                    break;

                case 2 :
                    sql = @"SELECT DISTINCT CameraSerialNo, CameraID, [CameraLocCode], [Violation], CourtName, [CourtNo], ElapsedTime, FineAlloc, FineAmnt, 
                        FirstSpeed, FrameNo, LocDescr, ManualView, MultFrames, i.OffenceCode, OffenceDate, OffenceDescr, OffenceLetter, 
                        o.OfficerGroup, OfficerInit,
                        i.[OfficerNo], [OfficerSName], [OffenderType], ReferenceNo, RegNo, RejReason, i.RdTypeCode, RdTypeDescr, SecondSpeed, 
                        [Sequence], i.[Speed], [TravelDirection], m.VehMCode, VehMDescr, i.VehTCode, VehTDescr, [Violation], [TruvellaFlag]
                        FROM InputFromTOMS i, Officer o, Offence c, RoadType r, VehicleMake m, VehicleType t
                        WHERE i.OfficerNo = o.OfficerNo
                        AND c.OffenceCode = i.OffenceCode
                        AND c.VehTCode = t.VehTCode
                        AND c.RdTypeCode = r.RdTypeCode
                        AND c.Speed = i.Speed
                        AND  r.RdTypeCode = i.RdTypeCode
                        AND i.VehMCode = m.VehMcode
                        AND i.VehTCode = t.VehTCode
                        AND FilmNo = '" + film.FilmNumber + @"' 
                        ORDER BY FrameNo";
                    break;

                case 3:
                    sql = @"SELECT DISTINCT CameraSerialNo, CameraID, [CameraLocCode], [Violation], CourtName, [CourtNo], ElapsedTime, FineAlloc, FineAmnt, 
                        FirstSpeed, FrameNo, LocDescr, ManualView, MultFrames, i.OffenceCode, OffenceDate, OffenceDescr, OffenceLetter, 
                        o.OfficerGroup, OfficerInit,i.[OfficerNo], [OfficerSName], [OffenderType], ReferenceNo, RegNo, RejReason, i.RdTypeCode, RdTypeDescr, SecondSpeed,
                        [Sequence], i.[Speed], [TravelDirection], m.VehMCode, VehMDescr, i.VehTCode, VehTDescr, [Violation], [TruvellaFlag],
                        ASDGPSDateTime1, ASDGPSDateTime2, ASDTimeDifference, ASDSectionStartLane, ASDSectionEndLane, ASDSectionDistance,
                        ASD2ndCameraID,  ASD2ndCameraSerialNo
                        FROM InputFromTOMS i, Officer o, Offence c, RoadType r, VehicleMake m, VehicleType t
                        WHERE i.OfficerNo = o.OfficerNo
                        AND c.OffenceCode = i.OffenceCode
                        AND c.VehTCode = t.VehTCode
                        AND c.RdTypeCode = r.RdTypeCode
                        AND c.Speed = i.Speed
                        AND  r.RdTypeCode = i.RdTypeCode
                        AND i.VehMCode = m.VehMcode
                        AND i.VehTCode = t.VehTCode
                        AND FilmNo = '" + film.FilmNumber + @"' 
                        ORDER BY FrameNo";

                    break;

                default:
                    throw new ApplicationException(string.Format("Version {0} of the database is not supported by this version of {1}.", this.version, Beginnings.APP_NAME));
            }
            OleDbCommand com = new OleDbCommand(sql, this.con);

            try
            {
                FrameData frame;
                OleDbDataReader reader = com.ExecuteReader();

                if (!reader.HasRows)
                {
                    throw new ApplicationException(string.Format("The film {0} has returned no frames.", film.FilmNumber));
                }
                else
                {
                    while (reader.Read())
                    {
                        frame = new FrameData();
                        frame.CameraID = (string)reader["CameraID"];
                        frame.CameraLocationCode = (string)reader["CameraLocCode"];
                        frame.ConfirmViolation = ((string)reader["Violation"])[0];
                        frame.CourtName = (string)reader["CourtName"];
                        frame.CourtNo = (string)reader["CourtNo"];
                        frame.ElapsedTime = (string)reader["ElapsedTime"];
                        frame.FineAllocation = ((string)reader["FineAlloc"])[0];
                        frame.FineAmount = decimal.Parse(reader["FineAmnt"].ToString());
                        frame.FirstSpeed = (byte)reader["FirstSpeed"];
                        frame.FrameNo = (string)reader["FrameNo"];
                        frame.LocationDescription = (string)reader["LocDescr"];
                        frame.ManualView = ((string)reader["ManualView"])[0];
                        frame.MultipleFrames = ((string)reader["MultFrames"])[0];
                        frame.OffenceCode = (string)reader["OffenceCode"];
                        frame.OffenceDate = (DateTime)reader["OffenceDate"];
                        frame.OffenceDescription = (string)reader["OffenceDescr"];
                        frame.OffenceLetter = ((string)reader["OffenceLetter"])[0];
                        frame.OfficerGroup = (string)reader["OfficerGroup"];
                        frame.OfficerInit = (string)reader["OfficerInit"];
                        frame.OfficerNo = (string)reader["OfficerNo"];
                        frame.OfficerSurname = (string)reader["OfficerSName"];
                        frame.OffenderType = int.Parse(reader["OffenderType"].ToString());
                        frame.ReferenceNo = (int)reader["ReferenceNo"];
                        frame.RegistrationNumber = (string)reader["RegNo"];
                        frame.RejectionReason = (string)reader["RejReason"];
                        frame.RoadTypeCode = (byte)reader["RdTypeCode"];
                        frame.RoadTypeDescription = (string)reader["RdTypeDescr"];
                        frame.SecondSpeed = (byte)reader["SecondSpeed"];
                        frame.Sequence = ((string)reader["Sequence"])[0];
                        frame.Speed = (byte)reader["Speed"];
                        frame.TravelDirection = ((string)reader["TravelDirection"])[0];
                        frame.VehicleMakeCode = (string)reader["VehMCode"];
                        frame.VehicleMakeDescription = (string)reader["VehMDescr"];
                        frame.VehicleTypeCode = (string)reader["VehTCode"];
                        frame.VehicleTypeDescription = (string)reader["VehTDescr"];
                        frame.Violation = ((string)reader["Violation"])[0];
                        frame.TruvellaFlag = ((string)reader["TruvellaFlag"])[0];

                        if (this.version > 1)
                            frame.CameraSerial = (string)reader["CameraSerialNo"];

                        if (this.version > 2  && reader["TruvellaFlag"].ToString().Equals("O"))
                        {
                            //FT 100430 For Average speed over distance 
                            //frame.ASD2ndCameraIntNo = Convert.ToInt32(reader["ASD2ndCameraIntNo"]);
                            if (reader["ASDGPSDateTime1"] != DBNull.Value)
                                frame.ASDGPSDateTime1 = (DateTime)reader["ASDGPSDateTime1"];
                            if (reader["ASDGPSDateTime2"] != DBNull.Value)
                                frame.ASDGPSDateTime2 = (DateTime)reader["ASDGPSDateTime2"];

                            frame.ASDTimeDifference = Convert.ToInt32(reader["ASDTimeDifference"]);
                            frame.ASDSectionStartLane = Convert.ToInt32(reader["ASDSectionStartLane"]);
                            frame.ASDSectionEndLane = Convert.ToInt32(reader["ASDSectionEndLane"]);
                            frame.ASDSectionDistance = Convert.ToInt32(reader["ASDSectionDistance"]);

                            frame.ASD1stCamUnitID = Convert.ToString(reader["CameraID"]);
                            frame.ASD2ndCamUnitID = Convert.ToString(reader["ASD2ndCameraID"]);
                            //frame.LCSIntNo = Convert.ToInt32(reader["LCSIntNo"]);
                            frame.ASDCameraSerialNo1 = Convert.ToString(reader["CameraSerialNo"]);
                            frame.ASDCameraSerialNo2 = Convert.ToString(reader["ASD2ndCameraSerialNo"]);

                        }

                        if (this.version > 2 && reader["TruvellaFlag"].ToString().Equals("U"))
                        {
                            //FT 100430 For Average speed over distance 
                            //frame.ASD2ndCameraIntNo = Convert.ToInt32(reader["ASD2ndCameraIntNo"]);
                            if (reader["ASDGPSDateTime1"] != DBNull.Value)
                                frame.ASDGPSDateTime1 = (DateTime)reader["ASDGPSDateTime1"];
                            if (reader["ASDGPSDateTime2"] != DBNull.Value)
                                frame.ASDGPSDateTime2 = (DateTime)reader["ASDGPSDateTime2"];

                            frame.ASDTimeDifference = Convert.ToInt32(reader["ASDTimeDifference"]);
                            frame.ASDSectionStartLane = Convert.ToInt32(reader["ASDSectionStartLane"]);
                            frame.ASDSectionEndLane = Convert.ToInt32(reader["ASDSectionEndLane"]);
                            frame.ASDSectionDistance = Convert.ToInt32(reader["ASDSectionDistance"]);

                            frame.ASD1stCamUnitID = Convert.ToString(reader["CameraID"]);
                            frame.ASD2ndCamUnitID = Convert.ToString(reader["ASD2ndCameraID"]);
                            //frame.LCSIntNo = Convert.ToInt32(reader["LCSIntNo"]);
                            frame.ASDCameraSerialNo1 = Convert.ToString(reader["CameraSerialNo"]);
                            frame.ASDCameraSerialNo2 = Convert.ToString(reader["ASD2ndCameraSerialNo"]);

                        }
                        
                        frame.Parent = film;

                        film.Frames.Add(frame);
                    }
                }
                reader.Close();

                //dls 090613 - check that we have the correct no. of frames
                if (film.Frames.Count != noOFFrames)
                {
                    throw new ApplicationException(string.Format("The film {0} has {1} frames, but only {2} were returned when joining to the lookup tables", 
                        film.FilmNumber, noOFFrames.ToString(), film.Frames.Count.ToString()));
                }

                response = false;
            }
            catch (Exception ex)
            {
                Beginnings.Writer.WriteError(ex);
            }

            return response;
        }

        private void CheckDatabaseExportVersion()
        {
            System.Data.DataTable table = this.con.GetSchema("Columns", new string[] { null, null, "InputFromTOMS" }); 
            foreach (System.Data.DataRow row in table.Rows)
            {
                if (row[3].ToString().Equals("CameraSerialNo"))
                {
                    this.version = 2;
                    return;
                }
                else if (row[3].ToString().Equals("ASD2ndCameraID"))
                {
                    this.version = 3;
                    return;
                }
            }
        }

        /// <summary>
        /// Gets the image data from the current MDB for the supplied <see cref="FrameData"/>.
        /// </summary>
        /// <param name="frame">The frame.</param>
        /// <returns>
        /// 	<c>true</c> if there were problems loading the image data
        /// </returns>
        internal bool GetImages(FrameData frame)
        {
            bool response = true;

            string sql = string.Format("SELECT Sequence, JpegName, XValue, YValue FROM InputFromTOMS WHERE FrameNo = '{0}' AND FilmNo = '{1}' ORDER BY JpegName", frame.FrameNo, frame.Parent.FilmNumber);
            OleDbCommand com = new OleDbCommand(sql, this.con);

            try
            {
                OleDbDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    Image image = new Image(frame);
                    image.ImageType = ((string)reader["Sequence"])[0];
                    image.Name = (string)reader["JpegName"];
                    if (!(reader["XValue"] is DBNull))
                    {
                        image.X = (int)reader["XValue"];
                        image.Y = (int)reader["YValue"];
                    }

                    this.ProcessImage(frame, image);
                }

                reader.Close();
				
				//this only needs to be done once per frame - moved from inside the image loop
                response = this.CheckForDriverImage(frame);

                //response = false;
            }
            catch (Exception ex)
            {
                Beginnings.Writer.WriteError(ex);
                response = false;
            }

            return response;
        }

        private bool CheckForDriverImage(FrameData frame)
        {
            bool failed = false;
            string driverImageName = string.Format("{0}{1}{2}.jpg", frame.Parent.FilmNumber, frame.FrameNo, "_Drv");
            FileInfo file = new FileInfo(Path.Combine(frame.Parent.OutputDirectory, driverImageName));
            if (file.Exists)
            {
                Image driverImage = new Image(frame);
                driverImage.ImageType = 'D';
                driverImage.Name = driverImageName;

                this.ProcessImage(frame, driverImage);
            }
            //else
            //{
            //    //we are not sure whether there will be a driver image or not, so display a message
            //    Beginnings.Writer.WriteLine("\tNo Driver image found {0}", driverImageName);
            //}

            string registrationImageName = string.Format("{0}{1}{2}.jpg", frame.Parent.FilmNumber, frame.FrameNo, "_Reg");
            file = new FileInfo(Path.Combine(frame.Parent.OutputDirectory, registrationImageName));
            if (file.Exists)
            {
                Image registrationImage = new Image(frame);
                registrationImage.ImageType = 'R';
                registrationImage.Name = registrationImageName;

                this.ProcessImage(frame, registrationImage);
            }
            else 
            {
                if (frame.Violation.ToString().Equals("Y"))
                {
                    failed = true;
                }

                //we are not sure whether there will be a driver image or not, so display a message
                Beginnings.Writer.WriteLine("\tNo RegNo image found {0} {1}", registrationImageName, failed ? "for violation on frame " + frame.FrameNo : String.Empty);
                
            }

            return failed;
        }

        /// <summary>
        /// Processes the image to see if it should be saved
        /// </summary>
        /// <param name="frame">The frame.</param>
        /// <param name="image">The image.</param>
        private void ProcessImage(FrameData frame, Image image)
        {
            //FileInfo file = new FileInfo(Path.Combine(frame.Parent.OutputDirectory, image.Name));
            //if (file.Exists)
            //{
            //    file.Attributes = FileAttributes.Archive;
            //    if (image.CheckImageValidity())
            //    {
            //        file.CopyTo(Path.Combine(file.DirectoryName, frame.Parent.FilmNumber + "\\" + image.Name), true);
            //        frame.Images.Add(image);
            //        if (Options.ProgramOptions.IsDebug)
            //            Beginnings.Writer.WriteLine("\tProcessing Image from film {1} :: {0}", image.Name, frame.Parent.FilmNumber);
            //    }
                
            //    file.Delete();
            //}

            FileInfo file = new FileInfo(Path.Combine(frame.Parent.OutputDirectory, image.Name));

            if (image.CheckImageValidity())
            {
                if (file.Exists)
                {
                    file.Attributes = FileAttributes.Archive;
                    file.CopyTo(Path.Combine(file.DirectoryName, frame.Parent.FilmNumber + "\\" + image.Name), true);
                    file.Delete();
                }

                frame.Images.Add(image);

                if (Options.ProgramOptions.IsDebug)
                    Beginnings.Writer.WriteLine("\tProcessing Image from film {1} :: {0}", image.Name, frame.Parent.FilmNumber);

            }
            else
            {
                if (file.Exists)
                {
                    file.Attributes = FileAttributes.Archive;
                    file.Delete();
                }
            }
        }
    }
}