/*
 * Created by: Francis Judge
 * Created: 13 September 2006
 */

//dls 090604  - remove all references to Atalasoft and JP2000 conversion
using System;
using System.Net.Mail;
using System.Text;
using Stalberg.TMS_3P_Loader.Components;

namespace Stalberg.TMS_3P_Loader.Data
{
    /// <summary>
    /// Represents the option data of the program
    /// </summary>
    internal class Options
    {
        // Fields
        private FtpParameters ftpParameters = null;
        private string connectionString = string.Empty;
        private string procStr = string.Empty;
        private bool doNothing = false;
        private bool isDebug = false;
        private Mode mode;
        private bool extractAllImages = true;
        //private bool jp2Conversion = true;
        private string indabaSource = string.Empty;
        private string indabaConnectionString = string.Empty;

        private static Options singletonOptions;

        /// <summary>
        /// Initializes a new instance of the <see cref="Options"/> class.
        /// </summary>
        static Options()
        {
            Options.singletonOptions = new Options();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Options"/> class.
        /// </summary>
        private Options()
        {
            this.ftpParameters = new FtpParameters();
        }

        /// <summary>
        /// Gets the program options.
        /// </summary>
        /// <value>The program options.</value>
        public static Options ProgramOptions
        {
            get
            {
                return Data.Options.singletonOptions;
            }
        }

        /// <summary>
        /// Gets or sets the indaba source name.
        /// </summary>
        /// <value>The indaba source.</value>
        public string IndabaSource
        {
            get { return this.indabaSource; }
            set { this.indabaSource = value; }
        }

        /// <summary>
        /// Gets and sets the indaba connection string.
        /// </summary>
        /// <value>The indaba connection string.</value>
        public string IndabaConnectionString
        {
            get { return this.indabaConnectionString; }
            set { this.indabaConnectionString = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to perform JPEG2000 conversion.
        /// </summary>
        /// <value><c>true</c> if JP2 conversion should be performed; otherwise, <c>false</c>.</value>
        //public bool Jp2Conversion
        //{
        //    get { return jp2Conversion; }
        //    set { jp2Conversion = value; }
        //}

        /// <summary>
        /// Gets or sets the FTP parameters.
        /// </summary>
        /// <value>The FTP parameters.</value>
        public FtpParameters FtpParameters
        {
            get { return ftpParameters; }
            set { ftpParameters = value; }
        }

        /// <summary>
        /// Gets or sets the name of the process.
        /// </summary>
        /// <value>The name of the process.</value>
        public string ProcessName
        {
            get { return procStr; }
            set { procStr = value; }
        }

        /// <summary>
        /// Gets or sets the Database connection string.
        /// </summary>
        /// <value>The connection string.</value>
        public string ConnectionString
        {
            get { return connectionString; }
            set { connectionString = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to do nothing as a result of a command line argument.
        /// </summary>
        /// <value><c>true</c> if [do nothing]; otherwise, <c>false</c>.</value>
        public bool DoNothing
        {
            get { return doNothing; }
            set { doNothing = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is in debug mode.
        /// </summary>
        /// <value><c>true</c> if this instance is debug; otherwise, <c>false</c>.</value>
        public bool IsDebug
        {
            get { return isDebug; }
            set { isDebug = value; }
        }

        /// <summary>
        /// Gets or sets the mode the application is running in.
        /// </summary>
        /// <value>The mode.</value>
        public Mode Mode
        {
            get { return mode; }
            set { mode = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [extract all images].
        /// </summary>
        /// <value><c>true</c> if all images should be extracted; otherwise, <c>false</c>.</value>
        public bool ExtractAllImages
        {
            get { return extractAllImages; }
            set { extractAllImages = value; }
        }

        /// <summary>
        /// Emails the log file .
        /// </summary>
        public void EmailLogFile()
        {
            Beginnings.Writer.Close();

            try
            {
                MailAddress from = new MailAddress(this.ftpParameters.Email);
                MailAddress to = new MailAddress(this.ftpParameters.Email);

                MailMessage mail = new MailMessage(from, to);
                mail.Subject = this.ftpParameters.HostServer.ToUpper()  + " " + this.procStr + " log file";
                //mail.Body = message;
                mail.BodyEncoding = Encoding.UTF8;
                Attachment myAttachment = new Attachment(Beginnings.Writer.LogFileName);
                mail.Attachments.Add(myAttachment);

                try
                {
                    SmtpClient mailClient = new SmtpClient();
                    mailClient.Host = this.ftpParameters.SmtpServer;
                    mailClient.UseDefaultCredentials = true;
                    mailClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
                    mailClient.Send(mail);

                    Beginnings.Writer = new LogWriter(true);
                    Beginnings.Writer.WriteLine(string.Format("Main: email log file sent to {0} at: {1}", this.ftpParameters.Email, DateTime.Now));
                    Beginnings.Writer.Close();
                }
                catch (Exception smtpEx)
                {
                    Beginnings.Writer = new LogWriter(true);
                    Beginnings.Writer.WriteError("Main: Failed to send email of log file " + smtpEx);
                    Beginnings.Writer.Close();
                }
            }
            catch (Exception emailEx)
            {
                Beginnings.Writer = new LogWriter(true);
                Beginnings.Writer.WriteError("Main: Failed to send email of log file " + emailEx);
                Beginnings.Writer.Close();
            }
        }

    }
}