using System.IO;
using System.Threading;
using System.Drawing;

//using Atalasoft.Imaging;
//using Atalasoft.Imaging.Codec;
//using Atalasoft.Imaging.Codec.Jpeg2000;
//dls 090604  - remove all references to Atalasoft and JP2000 conversion

namespace Stalberg.TMS_3P_Loader.Data
{
    /// <summary>
    /// Represents the details of a scanned frame image to export
    /// </summary>
    internal class Image
    {
        // Fields
        private string name = string.Empty;
        private int x;
        private int y;
        private char type;
        private FrameData parent;

        // Static
        //private static AtalaImage img = null;
        //private static Jp2Encoder jp2CodecSmall = new Jp2Encoder(2.5);
        //private static Jp2Encoder jp2CodecLarge = new Jp2Encoder(5.0);
        //private static JpegEncoder jpgCodec = new JpegEncoder();

        private static Image img = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="Image"/> class.
        /// </summary>
        public Image(FrameData parent)
        {
            this.parent = parent;
        }

        /// <summary>
        /// Gets the parent frame.
        /// </summary>
        /// <value>The parent.</value>
        public FrameData Parent
        {
            get { return this.parent; }
        }

        /// <summary>
        /// Gets or sets the type of the image.
        /// </summary>
        /// <value>The type of the image.</value>
        public char ImageType
        {
            get { return this.type; }
            set
            {
                if (char.ToUpper(value).Equals('X'))
                    this.type = 'A';
                else
                    this.type = char.ToUpper(value);
            }
        }

        /// <summary>
        /// Gets or sets the Y value.
        /// </summary>
        /// <value>The Y.</value>
        public int Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        /// <summary>
        /// Gets or sets the X value.
        /// </summary>
        /// <value>The X.</value>
        public int X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Saves the image's picture.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="buffer">The buffer containing the image.</param>
        /// <returns><c>true</c> if the image should be added to the collection</returns>
        public bool SaveImage(string path, byte[] buffer)
        {
            // Check to see if the image is valid for export
            if (!CheckImageValidity())
                return false;

            // Save the image to the file system without checking it
            FileInfo file = new FileInfo(Path.Combine(path, this.name));

            FileStream fs = new FileStream(file.FullName, FileMode.Create);
            fs.Write(buffer, 0, buffer.Length);
            fs.Close();
            Thread.Sleep(5);

            return true;
        }

        /// <summary>
        /// Checks whether image is valid for inclusion in the extract.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if the image is valid for extraction according the the currently set rules.
        /// </returns>
        public bool CheckImageValidity()
        {
            // If this film's authority doesn't want all the images sent, then discard the irrelevant ones
            if (!this.parent.Parent.ExtractAllImages && !(this.ImageType == 'R' || this.ImageType == 'D'))
            {
                switch (char.ToUpper(this.parent.TruvellaFlag))
                {
                    case 'P':
                    case 'D':
                        if (!this.name.ToUpper().EndsWith("001.JPG"))
                            return false;
                        break;

                    case 'Y':
                    case 'N':
                        if (!this.name.ToUpper().EndsWith("_17.JPG"))
                            return false;
                        break;
                }
            }

            // If this is a B frame image and its the driver or registration number picture, discard it
            if (this.parent.Sequence == 'B' && (this.ImageType == 'R' || this.ImageType == 'D'))
                return false;

            // FBJ Added (2006-10-09): This check is wrong and the scenario is being catered for in the TS1 file writer
            // If we're reading from and MDB and this is a B frame, discard it
            //if (Options.ProgramOptions.Mode == Mode.MDB && this.parent.Sequence == 'B')
            //    return false;

            // Its fine if its got this far
            return true;
        }

        //public bool SaveImage(string path, byte[] buffer)
        //{
        //    // Check to see if the image is valid for export
        //    if (!CheckImageValidity())
        //        return false;

        //    // Save the image to the file system without checking it
        //    FileInfo file = new FileInfo(Path.Combine(path, this.name));
        //    img = AtalaImage.FromByteArray(buffer);

        //    // Only convert to JPEG 2000 if its a main shot, and JP2 conversions should be performed
        //    if (this.ImageType == 'R' || this.ImageType == 'D')
        //        img.Save(file.FullName, jpgCodec, null);
        //    else if (Options.ProgramOptions.Jp2Conversion && buffer.Length > Components.GeneralFunctions.MIN_COMPRESSIBLE_SIZE)
        //    {
        //        // If its a 17 frame then use higher compression
        //        if (this.name.EndsWith("_17.JPG"))
        //            img.Save(file.FullName.Replace(file.Extension, ".jp2"), jp2CodecLarge, null);
        //        else
        //            img.Save(file.FullName.Replace(file.Extension, ".jp2"), jp2CodecSmall, null);
        //    }
        //    else
        //        img.Save(file.FullName, jpgCodec, null);

        //    img.Dispose();
        //    img = null;

        //    Thread.Sleep(5);

        //    return true;
        //}
    }
}
