using System;
using System.Collections.Generic;
using System.Text;

namespace Stalberg.TMS_3P_Loader.Data
{
	/// <summary>
	/// Lists the states that a Film can be in during its export lifecycle from Traffic into TMS
	/// </summary>
	internal enum TMSLoaderStatus
	{
		CDComplete = 100,
		TMSExtractBegun = 150,
		TS1Prepared = 200,
		TS1_FTPd = 300,
		TS1Deleted = 400,
		TMSErrorsReceived = 500,
		TMSErrorsFixed = 600,
		TMSErrorsExtractBegun = 650,
		TMSErrorsPrepared = 700,
		TMSErrors_FTPd = 800,
		TMSErrorsDeleted = 900,
		TMSComplete = 1000
	}
}
