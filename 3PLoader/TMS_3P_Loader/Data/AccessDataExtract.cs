/*
 * Created by: FBJ
 * Created: 14 September 2006
 */

using System.Collections.Generic;
using System.IO;

namespace Stalberg.TMS_3P_Loader.Data
{
    /// <summary>
    /// Represents an object that is able to extract film information from an Access database file
    /// </summary>
    internal class AccessDataExtract
    {
        // Fields
        private string exportPath;
        private List<Film> films = null;
        private List<string> mdbFileNames;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccessDataExtract"/> class.
        /// </summary>
        public AccessDataExtract()
        {
            this.exportPath = Options.ProgramOptions.FtpParameters.ExportPath;
            mdbFileNames = new List<string>();
        }

        #region Properties

        /// <summary>
        /// Gets the list of films created from the Access files found.
        /// </summary>
        /// <value>The films.</value>
        public List<Film> Films
        {
            get { return films; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds access database files in the export path.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if Access MDB files were found.
        /// </returns>
        public bool FindAccessFiles()
        {
            this.mdbFileNames.Clear();
            this.FindAccessFilesInDirectory(this.exportPath);

            return (this.mdbFileNames.Count > 0);
        }

        private void FindAccessFilesInDirectory(string directoryName)
        {
            DirectoryInfo directory = new DirectoryInfo(directoryName);
            if (directory.Exists)
            {
                foreach (FileInfo file in directory.GetFiles("*.mdb"))
                {
                    Beginnings.Writer.WriteLine("Found {0} to process.", file.FullName);
                    file.Attributes = FileAttributes.Archive;
                    this.mdbFileNames.Add(file.FullName);
                }

                foreach (DirectoryInfo info in directory.GetDirectories())
                    this.FindAccessFilesInDirectory(info.FullName);
            }
        }

        /// <summary>
        /// Creates and populates <see cref="Film"/> objects from the access files.
        /// </summary>
        public bool CreateFilmsFromAccessFiles()
        {
            bool failed = false;

            this.films = new List<Film>();

            foreach (string fileName in this.mdbFileNames)
            {
                Beginnings.Writer.WriteLine("Opening {0} for processing.", fileName);
                failed = this.OpenMdb(fileName);
                if (!failed)
                {
                    System.Threading.Thread.Sleep(50);
                    File.Delete(fileName);
                }
                else
                    break;
            }

            return failed;
        }

        private bool OpenMdb(string fileName)
        {
            bool failed = false;

            AccessDataProvider provider = new AccessDataProvider(fileName);

            // Check for films to extract
            if (provider.GetFilms(this.films))
            {
                foreach (Film film in this.films)
                {
                    // Don't reprocess films
                    if (film.IsProcessed)
                        continue;

                    film.Version = provider.Version;

                    // Set the film folder, and create the images sub-folder
                    this.CheckFilmFolders(film);

                    // Get the image data from the database
                    if (provider.GetFilmData(film))
                    {
                        Beginnings.Writer.WriteLine("There was a problem getting the data for film: {0}, from {1}.", film.FilmNumber, film.AuthorityName);
                        failed = true;
                    }
                    else
                    {
                        // Log the film processing
                        Beginnings.Writer.WriteLine("Processing film number: {0}, for {1}.", film.FilmNumber, film.AuthorityName);

                        // Save the image data
                        foreach (FrameData frame in film.Frames)
                        {
                            // Extract the image data for the frame
                            if (provider.GetImages(frame))
                            {
                                Beginnings.Writer.WriteLine("There was a problem retrieving the image information for film {0}, from {1}", film.FilmNumber, film.AuthorityName);
                                failed = true;
                                break;
                            }
                        }
                    }

                    if (!failed)
                    {
                        // Set the film as having been processed so it isn't done again
                        film.IsProcessed = true;
                    }
                }
            }

            // Cleanup the Access file provider
            provider.Dispose();

            return failed;
        }

        /// <summary>
        /// Checks that the film has all the necessary output folders.
        /// </summary>
        /// <param name="film">The film.</param>
        private void CheckFilmFolders(Film film)
        {
            string filmBasePath = Path.Combine(Options.ProgramOptions.FtpParameters.ExportPath, film.AuthorityCode + "\\" + film.FilmNumber);
            DirectoryInfo dirInfo = new DirectoryInfo(filmBasePath);
            film.OutputDirectory = dirInfo.FullName;
            dirInfo.CreateSubdirectory(film.FilmNumber);
        }

        #endregion

    }
}
