using System;
using System.Collections.Generic;
using System.IO;
using Stalberg.TMS_3P_Loader.Components;
using Stalberg.TMS_3P_Loader.Data;

namespace Stalberg.TMS_3P_Loader
{
    /// <summary>
    /// Represents a class that connects to the local database and extracts films for delivery
    /// </summary>
    internal class SqlServerDatabaseExtract
    {
        // Fields
        private List<Film> films = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlServerDatabaseExtract"/> class.
        /// </summary>
        public SqlServerDatabaseExtract()
        {
        }

        /// <summary>
        /// Gets the films for extraction.
        /// Checking the Database for Films to extract
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if there are films to extract from the database
        /// </returns>
        public bool GetFilmsForExtraction()
        {
            this.films = SqlServerDataProvider.GetFilmsForExport();

            return (this.films.Count > 0);
        }

        public int FilmCount()
        {
            return (this.films.Count);
        }

        /// <summary>
        /// Creates the TS1 file for each film.
        /// </summary>
        public void CreateTS1File()
        {
            for (int i = this.films.Count - 1; i >= 0; i--)
            {
                Film film = this.films[i];

                try
                {
                    // Create the TS file, populating the data objects
                    TS1File writer = new TS1File(film);
                    switch (Options.ProgramOptions.Mode)
                    {
                        case Mode.Centralised:
                        case Mode.Indaba:
                            writer.WriteFromSqlServer();
                            break;

                        case Mode.MDB:
                            writer.WriteFromMdb();
                            break;

                        default:
                            throw new ApplicationException("When trying to create the TS1 file the application mode was discovered not to have been set properly!");
                    }

                    // Set the TMSLoaderFlag to 200 for each film
                    film.SetStatus(TMSLoaderStatus.TS1Prepared);
                }
                catch (Exception ex)
                {
                    Beginnings.Writer.WriteError("Error extracting and creating TS1 file.", ex);

                    // Reset the status of the film if the TS1 file creation fails and remove the film from the list to prevent any further processing
                    film.SetStatus(TMSLoaderStatus.CDComplete);
                    this.films.Remove(film);
                }
            }
        }

        /// <summary>
        /// Checks whether an FTPd film is one that was extracted, and sets its TMS Loader Status flag accordingly.
        /// </summary>
        /// <param name="filmNo">The film no.</param>
        public void CheckForExtractFilmFTP(string filmNo)
        {
            if (filmNo.Trim().Length == 0)
                return;

            DirectoryInfo directory = new DirectoryInfo(filmNo);
            SqlServerDataProvider.SetFilmStatus(directory.Name, TMSLoaderStatus.TS1Deleted, string.Empty);
        }

    }
}

