using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
//using Stalberg.TMS;
//using Stalberg.TMS.Data;

namespace Stalberg.TMS_3P_Loader.Data
{
    /// <summary>
    /// Contains all the data access logic.
    /// All data is returned as objects from static factory methods on this class.
    /// </summary>
    internal static class SqlServerDataProvider
    {
        // Fields
        private static string connectionString = string.Empty;
        private static SqlConnection con = null;

        /// <summary>
        /// Initialises the Data Provider using the connection string.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if a connection to the database has been successful
        /// </returns>
        public static bool Initialise()
        {
            bool response = false;
            connectionString = Options.ProgramOptions.ConnectionString;

            try
            {
                con = new SqlConnection(connectionString);
                con.Open();
                response = true;

                // Check JP2 usage if there is a successful connection
                //SqlServerDataProvider.GetJP2Usage();
            }
            catch (SqlException ex)
            {
                Beginnings.Writer.WriteError("There was a problem opening the database connection.", ex);
            }
            finally
            {
                con.Close();
            }

            return response;
        }

        /// <summary>
        /// Disposes this data provider object.
        /// </summary>
        public static void Dispose()
        {
            if (con != null)
            {
                con.Close();
                con.Dispose();
            }
        }

        /// <summary>
        /// Gets the list of films that need to be exported.
        /// </summary>
        /// <returns>A list of films</returns>
        public static List<Film> GetFilmsForExport()
        {
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandText = "GetFilmsForExport";
            com.CommandType = CommandType.StoredProcedure;
            List<Film> films = new List<Film>();

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    Film film = new Film();
                    film.AuthorityCode = (string)reader["AuthCode"];
                    film.AuthorityNumber = (string)reader["AuthNo"];
                    film.AuthorityName = (string)reader["AuthName"];
                    film.CDLabel = (string)reader["CdLabel"];
                    film.FilmDescription = (string)reader["FilmDescr"];
                    film.FilmIntNo = (int)reader["FilmIntNo"];
                    film.FilmNumber = (string)reader["FilmNo"];
                    film.FilmType = ((string)reader["TruvellaFlag"])[0];
                    film.FirstOffence = (DateTime)reader["FirstOffenceDate"];
                    film.LastReferenceNumber = (int)reader["LastRefNo"];
                    film.MultipleFrames = ((string)reader["MultipleFrames"])[0];
                    film.MultipleViolations = ((string)reader["MultipleViolations"])[0];
                    film.ExtractAllImages = (((string)reader["ExportAllImages"]).ToUpper() == "Y") ? true : false;
                    film.SetStatus((short)reader["Status"]);

                    Beginnings.Writer.WriteError(string.Format("Found Film: {0} to extract data for.", film.FilmNumber));

                    films.Add(film);
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                Beginnings.Writer.WriteError("Error getting films for export.", ex);
            }
            finally
            {
                con.Close();
            }

            return films;
        }

        /// <summary>
        /// Gets the frames for a film.
        /// </summary>
        /// <param name="film">The film.</param>
        public static bool GetFramesForFilm(Film film)
        {
            bool response = true;
            int frameIntNo = 0;
            FrameData frame;

            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandText = "GetFramesForFilm";
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@FilmIntNo", SqlDbType.Int, 4).Value = film.FilmIntNo;

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    // Verify that each row is a unique Frame
                    if (frameIntNo != (int)reader["FrameIntNo"])
                    {
                        frame = new FrameData();
                        //frame.CameraID = (string)reader["CameraSerialNo"];
                        //dls 061120 - add camera serial no
                        frame.CameraID = (string)reader["CameraID"];
                        frame.CameraSerial = (string)reader["CameraSerialNo"];
                        frame.CameraLocationCode = (string)reader["CameraLocCode"];
                        frame.ConfirmViolation = ((string)reader["ConfirmViolation"])[0];
                        frame.CourtName = (string)reader["CourtName"];
                        frame.CourtNo = (string)reader["CourtNo"];
                        frame.ElapsedTime = (string)reader["ElapsedTime"];
                        frame.FineAllocation = ((string)reader["FineAlloc"])[0];
                        frame.FineAmount = decimal.Parse(reader["FineAmnt"].ToString());
                        frame.FirstSpeed = (int)reader["FirstSpeed"];
                        frame.FrameIntNo = (int)reader["FrameIntNo"];
                        frame.FrameNo = (string)reader["FrameNo"];
                        frame.LocationDescription = (string)reader["LocDescr"];
                        frame.ManualView = ((string)reader["ManualView"])[0];
                        frame.MultipleFrames = ((string)reader["MultFrames"])[0];
                        frame.OffenceCode = (string)reader["OffenceCode"];
                        frame.OffenceDate = (DateTime)reader["OffenceDate"];
                        frame.OffenceDescription = (string)reader["OffenceDescr"];
                        frame.OffenceLetter = ((string)reader["OffenceLetter"])[0];
                        frame.OfficerGroup = (string)reader["OfficerGroup"];
                        frame.OfficerInit = (string)reader["OfficerInit"];
                        frame.OfficerNo = (string)reader["OfficerNo"];
                        frame.OfficerSurname = (string)reader["OfficerSName"];
                        frame.OffenderType = (int)reader["OffenderType"];
                        frame.ReferenceNo = (int)reader["ReferenceNo"];
                        frame.RegistrationNumber = (string)reader["RegNo"];
                        frame.RejectionReason = (string)reader["RejReason"];
                        frame.RoadTypeCode = (int)reader["RdTypeCode"];
                        frame.RoadTypeDescription = (string)reader["RdTypeDescr"];
                        frame.SecondSpeed = (int)reader["SecondSpeed"];
                        frame.Sequence = ((string)reader["Sequence"])[0];
                        frame.Speed = (int)reader["Speed"];
                        frame.TravelDirection = ((string)reader["TravelDirection"])[0];
                        frame.VehicleMakeCode = (string)reader["VehMCode"];
                        frame.VehicleMakeDescription = (string)reader["VehMDescr"];
                        frame.VehicleTypeCode = (string)reader["VehTCode"];
                        frame.VehicleTypeDescription = (string)reader["VehTDescr"];
                        frame.Violation = ((string)reader["Violation"])[0];
                        frame.TruvellaFlag = ((string)reader["TruvellaFlag"])[0];

                        //FT 100430 For Average speed over distance 
                        if (reader["TruvellaFlag"].ToString().Equals("O"))
                        {
                            //frame.ASD2ndCameraIntNo = Convert.ToInt32(reader["ASD2ndCameraIntNo"]);
                            if (reader["ASDGPSDateTime1"] != DBNull.Value)
                                frame.ASDGPSDateTime1 = (DateTime)reader["ASDGPSDateTime1"];
                            if (reader["ASDGPSDateTime2"] != DBNull.Value)
                                frame.ASDGPSDateTime2 = (DateTime)reader["ASDGPSDateTime2"];

                            frame.ASDTimeDifference = Convert.ToInt32(reader["ASDTimeDifference"]);
                            frame.ASDSectionStartLane = Convert.ToInt32(reader["ASDSectionStartLane"]);
                            frame.ASDSectionEndLane = Convert.ToInt32(reader["ASDSectionEndLane"]);
                            frame.ASDSectionDistance = Convert.ToInt32(reader["ASDSectionDistance"]);

                            //frame.ASD1stCamUnitID = Convert.ToInt32(reader["ASD1stCamUnitID"]);
                            //frame.ASD2ndCamUnitID = Convert.ToInt32(reader["ASD2ndCamUnitID"]);
                            frame.ASD1stCamUnitID = Convert.ToString(reader["ASD1stCamUnitID"]);
                            frame.ASD2ndCamUnitID = Convert.ToString(reader["ASD2ndCamUnitID"]);
                            //frame.LCSIntNo = Convert.ToInt32(reader["LCSIntNo"]);
                            frame.ASDCameraSerialNo1 = Convert.ToString(reader["ASDCameraSerialNo1"]);
                            frame.ASDCameraSerialNo2 = Convert.ToString(reader["ASDCameraSerialNo2"]);
                        }

                        if (reader["TruvellaFlag"].ToString().Equals("U"))
                        {
                            //frame.ASD2ndCameraIntNo = Convert.ToInt32(reader["ASD2ndCameraIntNo"]);
                            if (reader["ASDGPSDateTime1"] != DBNull.Value)
                                frame.ASDGPSDateTime1 = (DateTime)reader["ASDGPSDateTime1"];
                            if (reader["ASDGPSDateTime2"] != DBNull.Value)
                                frame.ASDGPSDateTime2 = (DateTime)reader["ASDGPSDateTime2"];

                            frame.ASDTimeDifference = Convert.ToInt32(reader["ASDTimeDifference"]);
                            frame.ASDSectionStartLane = Convert.ToInt32(reader["ASDSectionStartLane"]);
                            frame.ASDSectionEndLane = Convert.ToInt32(reader["ASDSectionEndLane"]);
                            frame.ASDSectionDistance = Convert.ToInt32(reader["ASDSectionDistance"]);

                            //frame.ASD1stCamUnitID = Convert.ToInt32(reader["ASD1stCamUnitID"]);
                            //frame.ASD2ndCamUnitID = Convert.ToInt32(reader["ASD2ndCamUnitID"]);
                            frame.ASD1stCamUnitID = Convert.ToString(reader["ASD1stCamUnitID"]);
                            frame.ASD2ndCamUnitID = Convert.ToString(reader["ASD2ndCamUnitID"]);
                            //frame.LCSIntNo = Convert.ToInt32(reader["LCSIntNo"]);
                            frame.ASDCameraSerialNo1 = Convert.ToString(reader["ASDCameraSerialNo1"]);
                            frame.ASDCameraSerialNo2 = Convert.ToString(reader["ASDCameraSerialNo2"]);
                        }

                        frame.Parent = film;

                        film.Frames.Add(frame);

                        frameIntNo = frame.FrameIntNo;
                    }
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                response = false;
                Beginnings.Writer.WriteError("Error adding frames to a film", ex);
            }
            catch (Exception e)
            {
                response = false;
                Beginnings.Writer.WriteError("Exception:", e);
            }
            finally
            {
                con.Close();
            }

            return response;
        }

        /// <summary>
        /// Gets the image data from a particular scan.
        /// </summary>
        /// <param name="frame">The frame.</param>
        /// <returns></returns>
        public static bool GetImageData(FrameData frame)
        {
            bool response = true;
            byte[] buffer;

            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandText = "GetImageData";
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@FrameIntNo", SqlDbType.Int, 4).Value = frame.FrameIntNo;

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader(CommandBehavior.Default);
                while (reader.Read())
                {
                    Image image = new Image(frame);
                    image.ImageType = ((string)reader["Sequence"])[0];
                    image.Name = (string)reader["JpegName"];
                    if (!(reader["XValue"] is DBNull))
                    {
                        image.X = (int)reader["XValue"];
                        image.Y = (int)reader["YValue"];
                    }
                    buffer = (byte[])reader["SceneImage"];

                    if (image.SaveImage(frame.Parent.OutputDirectory, buffer))
                    {
                        frame.Images.Add(image);
                        Beginnings.Writer.WriteError(string.Format("Saving image data for {0}.", image.Name));
                    }
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                // FBJ Added (2006-08-22): Send film even if there are image extraction errors.
                // response = false;
                Beginnings.Writer.WriteError(ex);
                Beginnings.Writer.WriteLine("Error getting image data for one of the images in Film: {0}, Frame Number: {1}.", frame.Parent.FilmNumber, frame.FrameNo);
            }
            finally
            {
                con.Close();
            }

            return response;
        }

        /// <summary>
        /// Sets the film's TMS Loader Status.
        /// </summary>
        /// <param name="film">The film.</param>
        public static bool SetFilmStatus(Film film)
        {
            return SetFilmStatus(film.FilmNumber, film.Status, film.ErrorMessage.Trim());
        }

        /// <summary>
        /// Sets the film status in the database.
        /// </summary>
        /// <param name="filmNo">The film number.</param>
        /// <param name="status">The status.</param>
        /// <returns>
        /// 	<c>true</c> if the update was successful.
        /// </returns>
        public static bool SetFilmStatus(string filmNo, TMSLoaderStatus status, string comment)
        {
            bool response = false;

            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandText = "UpdateTMSLoaderStatus";
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@FilmNo", SqlDbType.VarChar, 10).Value = filmNo;
            com.Parameters.Add("@TMSLoaderStatus", SqlDbType.Int, 4).Value = status;
            if (comment.Length > 0)
                com.Parameters.Add("@Comment", SqlDbType.Text).Value = comment;

            SqlParameter returnParameter = com.Parameters.Add("@Return", SqlDbType.Int, 4);
            returnParameter.Direction = ParameterDirection.ReturnValue;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                if (returnParameter.Value.Equals(1))
                {
                    Beginnings.Writer.WriteError(string.Format("The status of film: {0}, was change to {1}.", filmNo, status.ToString()));
                    response = true;
                }
                else
                    Beginnings.Writer.WriteError(string.Format("There was an error saving the film status change to {0}.", status.ToString()), null);
            }
            catch (SqlException ex)
            {
                Beginnings.Writer.WriteError("There was a problem updating the film's TMS Loader Status.", ex);
            }
            finally
            {
                con.Close();
            }

            return response;
        }

        /// <summary>
        /// Sets the name of the TS1 file for the film in the database.
        /// </summary>
        /// <param name="film">The film.</param>
        /// <param name="fileName">Name of the file.</param>
        public static void SetTS1FileName(Film film, string fileName)
        {
            SqlCommand com = new SqlCommand("FilmUpdateTS1FileName", con);
            com.Parameters.Add("@FilmIntNo", SqlDbType.Int, 4).Value = film.FilmIntNo;
            com.Parameters.Add("@TS1Name", SqlDbType.VarChar, 50).Value = fileName;
            com.CommandType = CommandType.StoredProcedure;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            catch (SqlException se)
            {
                Beginnings.Writer.WriteError("There was an error setting the Film's TS1 file name.", se);
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Sets the film and its frame's comments rows.
        /// </summary>
        /// <param name="film">The film.</param>
        public static void SetFrameError(Film film)
        {
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandText = "SetFrameComment";
            com.CommandType = CommandType.StoredProcedure;
            SqlParameter frameParameter = com.Parameters.Add("@FrameNo", SqlDbType.VarChar, 4);
            SqlParameter errorParameter = com.Parameters.Add("@Comment", SqlDbType.Text);
            com.Parameters.Add("@FilmNo", SqlDbType.VarChar, 10).Value = film.FilmNumber;

            con.Open();
            foreach (FrameData frame in film.Frames)
            {
                frameParameter.Value = frame.FrameNo;

                foreach (string error in frame.Errors)
                {
                    try
                    {
                        errorParameter.Value = error;
                        com.ExecuteNonQuery();
                    }
                    catch (SqlException se)
                    {
                        Beginnings.Writer.WriteError("There was a problem writing the frame error", se);
                    }
                }

                Beginnings.Writer.WriteError(string.Format("Writing errors/comments for film: {0}", film.FilmNumber));
            }
            con.Close();
        }

        /// <summary>
        /// Gets the JPEG 2000 usage from the database, overriding what was in the sysparam XML file.
        /// </summary>
        //private static void GetJP2Usage()
        //{
        //    // Check the database parameters
        //    SysParamDetails sysParam = new SysParamDetails();
        //    sysParam.SPColumnName = "UseJPEG200Conversion";
        //    sysParam.SPDescr = "Whether or not to use JPEG2000 conversion in the application (the defailt is No).";
        //    sysParam.SPIntegerValue = 0;
        //    sysParam.SPStringValue = "No";
        //    SysParamDB db = new SysParamDB(Options.ProgramOptions.ConnectionString);
        //    Options.ProgramOptions.Jp2Conversion = db.GetSysParamWithDefaults(sysParam, Beginnings.APP_NAME);
        //}
    }
}

