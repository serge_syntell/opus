using System;
using System.Collections.Generic;

namespace Stalberg.TMS_3P_Loader.Data
{
	/// <summary>
	/// Represents a Film for export
	/// </summary>
	internal class Film
	{
		// Fields for export
		private int filmIntNo;
		private string filmNo = string.Empty;
		private string authCode = string.Empty;
		private string authName = string.Empty;
		private string authNo;
		private string cdLabel = string.Empty;
		private string filmDescription = string.Empty;
		private char multipleFrames;
		private char multipleViolations;
		private int lastRefNo;
		private char filmType;
		private DateTime firstOffence;
		private List<FrameData> frames;
		private string outputDirectory = string.Empty;
		private TMSLoaderStatus status = TMSLoaderStatus.CDComplete;
		private bool extractAllImages = true;
		// Fields for feedback
		private string errorMessage = string.Empty;
		private bool hasErrors = false;
		private int version;
		private string contractor = string.Empty;
	    // For processing
        private bool isProcessed = false;

		/// <summary>
		/// Initializes a new instance of the <see cref="Film"/> class.
		/// </summary>
		public Film()
		{
			this.Initialise();
		}

		private void Initialise()
		{
			this.frames = new List<FrameData>();
		}

		/// <summary>
		/// Gets or sets the contractor.
		/// </summary>
		/// <value>The contractor.</value>
		public string Contractor
		{
			get { return contractor; }
			set { contractor = value; }
		}

		/// <summary>
		/// Gets or sets the version.
		/// </summary>
		/// <value>The version.</value>
		public int Version
		{
			get { return version; }
			set { version = value; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance has errors.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance has errors; otherwise, <c>false</c>.
		/// </value>
		public bool HasErrors
		{
			get { return hasErrors; }
			set { hasErrors = value; }
		}

		/// <summary>
		/// Gets or sets the error message.
		/// </summary>
		/// <value>The error message.</value>
		public string ErrorMessage
		{
			get { return errorMessage; }
			set { errorMessage = value; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [extract all images].
		/// </summary>
		/// <value><c>true</c> if [extract all images]; otherwise, <c>false</c>.</value>
		public bool ExtractAllImages
		{
			get { return extractAllImages; }
			set { extractAllImages = value; }
		}

		/// <summary>
		/// Gets the films TMS Loader status.
		/// </summary>
		/// <value>The status.</value>
		public TMSLoaderStatus Status
		{
			get { return status; }
		}
		/// <summary>
		/// Sets the status.
		/// </summary>
		/// <param name="status">The status.</param>
		public void SetStatus(TMSLoaderStatus status)
		{
			this.SetStatus(status, true);
		}

		/// <summary>
		/// Sets the status.
		/// </summary>
		/// <param name="status">The status.</param>
		public void SetStatus(int status)
		{
			this.SetStatus((TMSLoaderStatus)status, false);
		}

        /// <summary>
        /// Sets the status.
        /// </summary>
        /// <param name="newStatus">The new status.</param>
        /// <param name="updateDatabase">if set to <c>true</c> [update database].</param>
		public void SetStatus(TMSLoaderStatus newStatus, bool updateDatabase)
		{
			// FBJ Added (2006-08-23): This is now done in the SQL PROC
			//// Automatically assign the resend statuses
			//if ((int)this.status >= 600 && (int)newStatus < 600)
			//    newStatus = (TMSLoaderStatus)((int)newStatus + 500);

			this.status = newStatus;
			if (updateDatabase)
				SqlServerDataProvider.SetFilmStatus(this);
		}

		/// <summary>
		/// Gets or sets the output directory for this film.
		/// </summary>
		/// <value>The output directory.</value>
		public string OutputDirectory
		{
			get { return outputDirectory; }
			set { outputDirectory = value; }
		}

		/// <summary>
		/// Gets the frames on this film.
		/// </summary>
		/// <value>The frames.</value>
		public List<FrameData> Frames
		{
			get { return this.frames; }
		}

		/// <summary>
		/// Gets or sets the first offence date on the film.
		/// </summary>
		/// <value>The first offence.</value>
		public DateTime FirstOffence
		{
			get { return firstOffence; }
			set { firstOffence = value; }
		}

		/// <summary>
		/// Gets or sets the type of the film.
		/// </summary>
		/// <value>The type of the film.</value>
		public char FilmType
		{
			get { return filmType; }
			set { filmType = value; }
		}

		/// <summary>
		/// Gets or sets the last reference number.
		/// </summary>
		/// <value>The last reference number.</value>
		public int LastReferenceNumber
		{
			get { return lastRefNo; }
			set { lastRefNo = value; }
		}

		/// <summary>
		/// Gets or sets the multiple violations.
		/// </summary>
		/// <value>The multiple violations.</value>
		public char MultipleViolations
		{
			get { return multipleViolations; }
			set { multipleViolations = value; }
		}

		/// <summary>
		/// Gets or sets the multiple frames.
		/// </summary>
		/// <value>The multiple frames.</value>
		public char MultipleFrames
		{
			get { return multipleFrames; }
			set { multipleFrames = value; }
		}

		/// <summary>
		/// Gets or sets the film description.
		/// </summary>
		/// <value>The film description.</value>
		public string FilmDescription
		{
			get { return filmDescription; }
			set { filmDescription = value; }
		}

		/// <summary>
		/// Gets or sets the CD label.
		/// </summary>
		/// <value>The CD label.</value>
		public string CDLabel
		{
			get { return cdLabel; }
			set { cdLabel = value; }
		}

		/// <summary>
		/// Gets or sets the authority int no.
		/// </summary>
		/// <value>The authority int no.</value>
		public string AuthorityNumber
		{
			get { return this.authNo; }
			set { this.authNo = value; }
		}

		/// <summary>
		/// Gets or sets the name of the authority.
		/// </summary>
		/// <value>The name of the authority.</value>
		public string AuthorityName
		{
			get { return authName; }
			set { authName = value; }
		}

		/// <summary>
		/// Gets or sets the authority code.
		/// </summary>
		/// <value>The authority code.</value>
		public string AuthorityCode
		{
			get { return authCode; }
			set { authCode = value; }
		}

		/// <summary>
		/// Gets or sets the film number.
		/// </summary>
		/// <value>The film number.</value>
		public string FilmNumber
		{
			get { return filmNo; }
			set { filmNo = value; }
		}

		/// <summary>
		/// Gets or sets the film int no.
		/// </summary>
		/// <value>The film int no.</value>
		public int FilmIntNo
		{
			get { return filmIntNo; }
			set { filmIntNo = value; }
		}

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Film"/> has been processed yet.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this <see cref="Film"/> has been processed; otherwise, <c>false</c>.
        /// </value>
	    public bool IsProcessed
	    {
	        get { return isProcessed; }
	        set { isProcessed = value; }
	    }

	    /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            return string.Format("Film No.:{0}, for {1}", this.filmNo, this.authName);
        }

    }
}
