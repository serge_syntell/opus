using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Stalberg.Indaba;
using Stalberg.TMS_3P_Loader.Components;
using Stalberg.TMS_3P_Loader.Data;

namespace Stalberg.TMS_3P_Loader
{
    /// <summary>
    /// The main class of the application, contains the main entry point
    /// </summary>
    public class Beginnings
    {
        // Fields
        private static Export export = null;
        private static SqlServerDatabaseExtract extract;

        /// <summary>
        /// The Log Writer component for the application
        /// </summary>
        internal static LogWriter Writer;
        /// <summary>
        /// NB. SET THIS VARIABLE BEFORE DOING A BUILD RELEASE !!!
        /// </summary>
        internal static readonly DateTime LAST_MODIFIED = new DateTime(2011, 08, 19, 14, 00, 00);

        // Constants
        internal const string APP_NAME = "TMS 3rd Party Loader";

        /// <summary>
        /// Static initialization of the <see cref="Beginnings"/> class.
        /// </summary>
        static Beginnings()
        {
            Console.Title = APP_NAME;

            // Setup logging
            Beginnings.Writer = new LogWriter();
            Beginnings.Writer.WriteLine(Beginnings.ApplicationTitle());
            Beginnings.Writer.WriteLine("TMS_3rd_Party loader started at: " + DateTime.Now.ToString());

            // Retrieve program options from the SysParam file
            Export.GetProgramOptions();
            Beginnings.Writer.WriteLine("Completed getting start values at: " + DateTime.Now.ToString());

            // Setup and initialise helper classes
            Beginnings.extract = new SqlServerDatabaseExtract();
            TS1File.OutputDirectory = Options.ProgramOptions.FtpParameters.ExportPath;
        }

        /// <summary>
        /// Main entry point of the application
        /// </summary>
        /// <param name="args">The args.</param>
        static void Main(string[] args)
        {
            try
            {
                // Check any command line parameters
                Beginnings.CheckCommandLineArguments(args);
                if (Options.ProgramOptions.DoNothing)
                    return;

                // Check if this is the only instance running
                Singleton singleton = new Singleton("TMS_3P_Loader");
                if (Options.ProgramOptions.Mode == Mode.MDB)
                    singleton.OtherProcessesToCheckFor(new string[] { "TMS_ViolationLoader", 
                        "TMS_CD_Loader", 
                        "TMS_TPExInt",
                        "Stalberg.Payfine.Extract",
                        "PFB_Loader",
                        "Stalberg.ThaboExporter"});
                if (singleton.Check())
                {
                    if (Options.ProgramOptions.IsDebug)
                    {
                        Console.WriteLine(singleton.Message);
                        Writer.WriteLine(singleton.Message);
                    }
                    return;
                }

                Console.WriteLine(Beginnings.ApplicationTitle());

                // Setup the Indaba Client
                if (Options.ProgramOptions.Mode == Mode.Indaba)
                {
                    Client.ApplicationName = Beginnings.APP_NAME;
                    Client.ConnectionString = Options.ProgramOptions.IndabaConnectionString;
                    Client.Initialise();
                }

                // Common Objects
                Beginnings.export = new Export();

                // Check Mode
                switch (Options.ProgramOptions.Mode)
                {
                    case Mode.Indaba:
                    case Mode.Centralised:
                        Beginnings.RunCentralised();
                        break;

                    case Mode.MDB:
                        Beginnings.RunFromMDB();
                        break;

                    case Mode.Traffic:
                        Beginnings.RunOnTraffic();
                        break;

                    default:
                        Writer.WriteLine("The application mode was not set, so no processing was performed!");
                        break;
                }
            }
            catch (Exception ex)
            {
                Writer.WriteError("There was a general error in the application.\n", ex);
            }
            finally
            {
                Options.ProgramOptions.EmailLogFile();

                if (Options.ProgramOptions.IsDebug)
                {
                    Console.WriteLine();
                    Console.WriteLine("[ Completed successfully, hit <Enter> to exit. ]");
                    Console.ReadLine();
                }
            }
        }

        /// <summary>
        /// Runs the application in de-centralised mode, providing manual feedback into the Traffic database.
        /// </summary>
        /// <remarks>
        /// 	<para><b>NB.</b> there is no checking of the FTP site.</para>
        /// 	<para><b>NB.</b> there is no data processing.</para>
        /// </remarks>
        private static void RunOnTraffic()
        {
            Beginnings.Writer.WriteConsoleProcess(string.Format("Starting the application in {0} mode.", Options.ProgramOptions.Mode), true);

            // Initialise the database connection
            if (!SqlServerDataProvider.Initialise())
            {
                Beginnings.Writer.WriteLine("The Database could not be initialised.");
                return;
            }

            // Check the export folder for feedback files
            TMSFeedback tmsFeedback = new TMSFeedback(false);
            if (tmsFeedback.ManuallyCheckForFeedbackFiles())
                tmsFeedback.ProcessFeedbackFiles();
            else
                Beginnings.Writer.WriteLine("There were no feedback files to process at {0}.", DateTime.Now);
        }

        private static void RunFromMDB()
        {
            Beginnings.Writer.WriteLine("Beginning to run {0} in MDB mode at {1}.", APP_NAME, DateTime.Now);

            // Go to the export directory and find the CD MDBs
            AccessDataExtract mdbExtract = new AccessDataExtract();
            if (!mdbExtract.FindAccessFiles())
            {
                Beginnings.Writer.WriteLine("There were no MDB files found in the export path.");
                return;
            }

            // Read the MDBs into Film objects
            bool failed = mdbExtract.CreateFilmsFromAccessFiles();

            if (!failed)
            {
                // Create the TS1 files from them
                List<TS1File> ts1Files = new List<TS1File>();
                foreach (Film film in mdbExtract.Films)
                {
                    TS1File file = new TS1File(film);
                    file.WriteFromMdb();
                    ts1Files.Add(file);

                    Beginnings.Writer.WriteLine("Creates a TS1 file for {0}.", film.FilmNumber);
                }

                //// Check for JPEG 2000 conversion
                //if (Options.ProgramOptions.Jp2Conversion)
                //{
                //    Writer.WriteConsoleProcess("JPEG 2000 Conversion", true);
                //    GeneralFunctions general = new GeneralFunctions();
                //    general.ConvertJpgToJp2();
                //    Writer.WriteConsoleProcess("JPEG 2000 Conversion", false);
                //}

                // Create the Hash files
                Beginnings.Writer.WriteConsoleProcess("Hash file Creation", true);
                string directoryName = string.Empty;
                foreach (TS1File ts1File in ts1Files)
                {
                    directoryName = Path.GetDirectoryName(ts1File.FileName);
                    Beginnings.export.CreateHashes(directoryName);

                    Beginnings.Writer.WriteLine("\tCreated a hash file for {0}.", ts1File.Film.FilmNumber);
                }

                // Cleanup
                if (directoryName.Length > 0)
                {
                    DirectoryInfo dir = new DirectoryInfo(directoryName);
                    if (dir.Parent.Exists)
                        Beginnings.export.Cleanup(dir.Parent);
                }
                Beginnings.Writer.WriteConsoleProcess("Hash file Creation", false);

                Beginnings.Writer.WriteLine(string.Format("MDB Extraction complete at {0}", DateTime.Now));
            }
        }

        private static void RunCentralised()
        {
            SqlServerDataProvider.Initialise();

            // Check the FTP Site for re-sends
            Console.WriteLine("Checking for Resends.");
            Beginnings.CheckResends();

            // Extract Syntell films from Database
            //Console.WriteLine("Checking Syntell Data in the Database Server.");
            Console.WriteLine("Checking Data in the Database Server.");
            Beginnings.ExtractSyntelData();

            if (extract.FilmCount() > 0)
            {
                // Prepare the data for FTP and send
                Console.WriteLine("Preparing and Sending Data to Destination.");
                Beginnings.SendData();
            }

            // Clean up
            SqlServerDataProvider.Dispose();
            Console.WriteLine("TMS 3P Loader Completed at {0}.", DateTime.Now.ToString("HH:mm:ss"));
        }

        private static void CheckCommandLineArguments(string[] args)
        {
            foreach (string argument in args)
            {
                switch (argument.Trim().ToUpper())
                {
                    case "/DEBUG":
                    case "-DEBUG":
                    case "DEBUG":
                        Options.ProgramOptions.IsDebug = true;
                        break;

                    case "?":
                    case "-?":
                    case "/?":
                    case "HELP":
                    case "/HELP":
                    case "-HELP":
                        WriteCommandLineParameterList();
                        break;
                }
            }
        }

        private static void WriteCommandLineParameterList()
        {
            Console.WriteLine();
            Console.WriteLine(Beginnings.ApplicationTitle());
            Console.WriteLine();
            Console.WriteLine("Command Line Arguments for 3P Loader");
            Console.WriteLine("-------------------------------------");
            Console.WriteLine();
            Console.WriteLine("{0,-15} {1}", "Debug", "Sets the internal debug flag that outputs additional information.");
            Console.WriteLine("{0,-15} {1}", "Help", "Displays this help text.");
            Console.WriteLine();
            Console.WriteLine("Command line arguments are not case sensitive.");

            Options.ProgramOptions.DoNothing = true;
        }

        private static string ApplicationTitle()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("TMS Third Party (3P) Loader.\nCopyright (c) Stalberg & Associates 2005-");
            sb.Append(DateTime.Today.Year.ToString());
            sb.Append(", All Rights Reserved.\n");
            sb.Append("Last Updated: ");
            sb.Append(LAST_MODIFIED.ToString("yyyy/MM/dd HH:mm"));
            sb.Append("\n");

            return sb.ToString();
        }

        private static void SendData()
        {
            //// Check for JPEG 2000 conversion
            //if (Options.ProgramOptions.Jp2Conversion)
            //{
            //    Writer.WriteConsoleProcess("JPEG 2000 Conversion", true);
            //    export.DoJp2Conversion();
            //    Writer.WriteError("Completed conversion at: " + DateTime.Now.ToString());
            //    Writer.WriteConsoleProcess("JPEG 2000 Conversion", false);
            //}

            // Create hash file and create hashes
            Writer.WriteConsoleProcess("Hash", true);
            export.CreateHashes(Options.ProgramOptions.FtpParameters.ExportPath);
            Writer.WriteConsoleProcess("Hash", false);

            // Check if the files need to be sent using Indaba
            export.general.FilmFtpCallback = new FilmFtpCallback(extract.CheckForExtractFilmFTP);
            if (Options.ProgramOptions.Mode == Mode.Indaba)
            {
                Writer.WriteConsoleProcess("Queueing Data in Indaba", true);
                export.EnqueueData();
                Writer.WriteConsoleProcess("Queueing Data in Indaba", false);
            }
            // Check if the data should be FTPd
            else if (Options.ProgramOptions.FtpParameters.HostServer.Length > 0)
            {
                Writer.WriteConsoleProcess("FTPing Data", true);
                export.GetSourceFolder();
                Writer.WriteError("Completed load at: " + DateTime.Now.ToString());
                Writer.WriteConsoleProcess("FTPing Data", false);
            }
        }

        private static void ExtractSyntelData()
        {
            // Check Database
            Writer.WriteConsoleProcess("Extracting Syntel Data", true);
            if (extract.GetFilmsForExtraction())
            {
                // Create file system export
                extract.CreateTS1File();
            }
            Writer.WriteConsoleProcess("Extracting Syntel Data", false);
        }

        private static void CheckResends()
        {
            bool isIndaba = (Options.ProgramOptions.Mode == Mode.Indaba);
            TMSFeedback tmsFeedback = new TMSFeedback(isIndaba);

            // Check the FTP Site for re-sends
            Writer.WriteConsoleProcess("Checking for Feedback", true);
            if (tmsFeedback.CheckForFeedbackFiles())
            {
                Writer.WriteConsoleProcess("Checking for Resends", false);

                // Update Traffic film/frame
                Writer.WriteConsoleProcess("Processing Feedback", true);
                tmsFeedback.ProcessFeedbackFiles();
                Writer.WriteConsoleProcess("Processing Feedback", false);
            }
            else
                Writer.WriteConsoleProcess("Checking for Resends", false);
        }

    }
}
