/*
 * Created by: FBJ
 * Created: 14 September 2006
 */

using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using Stalberg.TMS_3P_Loader.Data;

namespace Stalberg.TMS_3P_Loader
{
    /// <summary>
    /// Represents the log writer for the application
    /// </summary>
    internal class LogWriter
    {
        // Fields
        private StreamWriter writer;
        private string logFileName;
        private StringBuilder sb = null;
        private DateTime dt;

        // Constants
        private const string DATE_FORMAT = "yyyy-MM-dd_HH-mm-ss";

        #region Constructors
        
        /// <summary>
        /// Initializes a new instance of the <see cref="LogWriter"/> class.
        /// </summary>
        public LogWriter()
            : this(false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogWriter"/> class.
        /// </summary>
        public LogWriter(bool isFinal)
        {
            sb = new StringBuilder();

            //Setup logging
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "LogFiles");
            if (isFinal)
                logFileName = Path.Combine(path, DateTime.Now.ToString(DATE_FORMAT) + "_final.txt");
            else
                logFileName = Path.Combine(path, DateTime.Now.ToString(DATE_FORMAT) + ".txt");

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            this.writer = new StreamWriter(LogFileName);

            this.WriteLine("{0}, last modified date {1}.\n\n", Beginnings.APP_NAME, Beginnings.LAST_MODIFIED.ToString("yyyy/MM/dd HH:mm"));
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the name of the log file.
        /// </summary>
        /// <value>The name of the log file.</value>
        public string LogFileName
        {
            get { return this.logFileName; }
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Writes a line to the log file, substituting the arguments for heir place-holders in the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="arg1">The arg1.</param>
        public void WriteLine(string message, object arg1)
        {
            this.WriteLine(string.Format(message, arg1));
        }

        /// <summary>
        /// Writes a line to the log file, substituting the arguments for heir place-holders in the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        public void WriteLine(string message, object arg1, object arg2)
        {
            this.WriteLine(string.Format(message, arg1, arg2));
        }
        
        /// <summary>
        /// Writes a line to the log file, substituting the arguments for heir place-holders in the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public void WriteLine(string message, object[] args)
        {
            this.WriteLine(string.Format(message, args));
        }

        /// <summary>
        /// Writes a line to the log file.
        /// </summary>
        /// <param name="message">The message.</param>
        public void WriteLine(string message)
        {
            this.writer.WriteLine(message);
            this.writer.Flush();

            if(Options.ProgramOptions.IsDebug)
            {
                Debug.WriteLine(message);
                Console.WriteLine(message);
            }
        }

        /// <summary>
        /// Closes the log writer
        /// </summary>
        public void Close()
        {
            this.writer.Flush();
            this.writer.Close();
            this.writer.Dispose();
        }
        /// <summary>
        /// Writes the error message to the log.
        /// </summary>
        /// <param name="message">The message.</param>
        public void WriteError(string message)
        {
            this.WriteError(message, null);
        }

        /// <summary>
        /// Writes the error message to the log..
        /// </summary>
        /// <param name="ex">The ex.</param>
        public void WriteError(Exception ex)
        {
            this.WriteError(string.Empty, ex);
        }

        /// <summary>
        /// Writes the error message and the exception details to the log.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The exception.</param>
        public void WriteError(string message, Exception ex)
        {
            sb.Length = 0;
            if (ex == null)
                sb.Append(message + "\n");
            else
            {
                sb.Append(message + "\n" + ex.Message);
                if (Options.ProgramOptions.IsDebug)
                    sb.Append(ex.StackTrace);
            }

            this.WriteLine(sb.ToString());
        }

        /// <summary>
        /// Writes the console process.
        /// </summary>
        /// <param name="process">The process.</param>
        /// <param name="started">if set to <c>true</c> [started].</param>
        public void WriteConsoleProcess(string process, bool started)
        {
            this.sb.Length = 0;
            if (started)
            {
                dt = DateTime.Now;
                sb.Append(string.Format("\tStarted {0} at ", process));
            }
            else
            {
                TimeSpan ts = DateTime.Now.Subtract(dt);
                sb.Append(string.Format("\tFinished {0} in {1} at ", process, ts.ToString()));
            }

            sb.Append(DateTime.Now.ToString("HH:mm:ss"));
            this.WriteError(sb.ToString());
        }

        #endregion
    }

}