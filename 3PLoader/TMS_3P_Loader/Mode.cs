
namespace Stalberg.TMS_3P_Loader
{
    /// <summary>
    /// Lists the modes that the Third Party loader can be operating in
    /// </summary>
    internal enum Mode
    {
        /// <summary>
        /// Indicates that the application execution mode has not yet been set
        /// </summary>
        None = 0,
        /// <summary>
        /// The default mode; 3P Loader is accessing the local SQL Server and FTPing results
        /// </summary>
        Centralised = 1,
        /// <summary>
        /// 3P Loader is working in file system mode, and creating TS1 files from information in an ARS Access files
        /// </summary>
        MDB = 2,
        /// <summary>
        /// 3P loader is 
        /// </summary>
        Traffic = 3,
        /// <summary>
        /// Operates in the same way as Centralised, but uses the Indaba communication service
        /// </summary>
        Indaba = 4
    }
}
