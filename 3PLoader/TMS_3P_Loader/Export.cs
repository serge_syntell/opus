using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Stalberg.Indaba;
using Stalberg.TMS_3P_Loader.Components;
using Stalberg.TMS_3P_Loader.Data;

namespace Stalberg.TMS_3P_Loader
{
    /// <summary>
    /// Contains the methods to prepare and export the film data to TMS by FTP
    /// </summary>
    internal class Export
    {
        // Fields
        internal GeneralFunctions general = new GeneralFunctions();

        // Constants
        internal static readonly string[] HASH_INCLUSIONS = new string[] { ".JPG", ".JPEG", ".JP2", ".TS1", Hashing.HASH_EXTENSION.ToUpper() };

        /// <summary>
        /// Initializes a new instance of the <see cref="Export"/> class.
        /// </summary>
        public Export()
        {
        }

        /// <summary>
        /// Gets the server XML data.
        /// </summary>
        public static void GetProgramOptions()
        {
            //load xml file with system parameters
            string currentDir = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(currentDir, "SysParam.xml"));
            XmlNode root = xmlDoc.DocumentElement;
            XmlAttribute attribute = null;

            // Create Struct
            if (root.HasChildNodes)
            {
                foreach (XmlNode node in root.ChildNodes)
                {
                    switch (node.Name)
                    {
                        case "constr":
                            Options.ProgramOptions.ConnectionString = node.InnerText;
                            break;

                        case "procstr":
                            Options.ProgramOptions.ProcessName = node.InnerText;
                            Options.ProgramOptions.FtpParameters.Process = node.InnerText;
                            break;

                        case "contractor":
                            Options.ProgramOptions.FtpParameters.Contractor = node.InnerText;
                            break;

                        case "ftpExportServer":
                            Options.ProgramOptions.FtpParameters.ExportServer = node.InnerText;
                            break;

                        case "ftpExportPath":
                            Options.ProgramOptions.FtpParameters.ExportPath = node.InnerText;
                            break;

                        case "ftpHostServer":
                            Options.ProgramOptions.FtpParameters.HostServer = node.InnerText;
                            break;

                        case "ftpHostIP":
                            Options.ProgramOptions.FtpParameters.HostIP = node.InnerText;
                            break;

                        case "ftpHostUser":
                            Options.ProgramOptions.FtpParameters.HostUser = node.InnerText;
                            break;

                        case "ftpHostPass":
                            Options.ProgramOptions.FtpParameters.HostPassword = node.InnerText;
                            break;

                        case "ftpHostPath":
                            Options.ProgramOptions.FtpParameters.HostPath = node.InnerText;
                            break;

                        case "ftpEmail":
                            Options.ProgramOptions.FtpParameters.Email = node.InnerText;
                            break;

                        case "ftpSMTP":
                            Options.ProgramOptions.FtpParameters.SmtpServer = node.InnerText;
                            break;

                        case "mode":
                            try
                            {
                                Options.ProgramOptions.Mode =
                                    (Mode)Enum.Parse(typeof(Mode), node.InnerText, true);
                            }
                            catch (ArgumentException ex)
                            {
                                Beginnings.Writer.WriteError("There was an error parsing the application mode from the SysParam file.", ex);
                            }
                            break;

                        case "extractAllImages":
                            bool extractAllImages;
                            if (bool.TryParse(node.InnerText, out extractAllImages))
                                Options.ProgramOptions.ExtractAllImages = extractAllImages;
                            break;

                        //case "jp2Conversion":
                        //    bool jp2Conversion;
                        //    if (bool.TryParse(node.InnerText, out jp2Conversion))
                        //        Options.ProgramOptions.Jp2Conversion = jp2Conversion;
                        //    break;

                        case "indaba":
                            attribute = node.Attributes["connectionstring"];
                            if (attribute != null)
                                Options.ProgramOptions.IndabaConnectionString = attribute.Value;
                            attribute = node.Attributes["source"];
                            if (attribute != null)
                                Options.ProgramOptions.IndabaSource = attribute.Value;
                            break;

                        default:
                            Options.ProgramOptions.ConnectionString = "none";
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Does the JP2 conversion.
        /// </summary>
        //public void DoJp2Conversion()
        //{
        //    Beginnings.Writer.WriteError("Starting JP2 conversion now: " + DateTime.Now.ToString());

        //    general.ConvertJpgToJp2();
        //}

        /// <summary>
        /// Gets the source folder.
        /// </summary>
        public void GetSourceFolder()
        {
            Beginnings.Writer.WriteError("Starting FTP Process now: " + DateTime.Now.ToString());

            general.FtpFile(Options.ProgramOptions.FtpParameters);
        }

        /// <summary>
        /// Exports the data.
        /// </summary>
        [Obsolete("This functionality have been moved to the main method of Loader class.")]
        private void ExportData()
        {
            string path = Application.StartupPath + "\\LogFiles";
            string fileName = DateTime.Now.ToString() + ".txt";

            fileName = fileName.Replace("/", "-");
            fileName = fileName.Replace(":", "-");

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            StreamWriter writer = new StreamWriter(path + "\\" + fileName, false);

            //bool loadFailed = false;

            writer.WriteLine("TMS_3P_Loader started at: " + DateTime.Now.ToString());
            //writer.WriteLine();
            writer.Flush();

            //	convert the files
            //this.DoJp2Conversion();
            //writer.WriteLine("Completed JP2 conversion at: " + DateTime.Now.ToString());
            //writer.Flush();

            //	ftp the files
            this.GetSourceFolder();
            writer.WriteLine("Completed FTP to remote location at: " + DateTime.Now.ToString());
            writer.Flush();
            writer.Close();
        }

        #region Hash file creation

        /// <summary>
        /// Creates the hash files for all films awaiting export.
        /// </summary>
        /// <param name="contentDirectory">The content directory.</param>
        /// <returns>True if there were no errors</returns>
        public bool CreateHashes(string contentDirectory)
        {
            //Beginnings.Writer.WriteLine("\tEntering Hashing Object");

            DirectoryInfo directory = new DirectoryInfo(contentDirectory);
            if (!directory.Exists)
                return false;

            // Find the TS1 files and hash from there
            //Beginnings.Writer.WriteLine("\tScanning Directory: {0}", directory.Name);
            return this.SearchForTS1File(directory);
        }

        private bool SearchForTS1File(DirectoryInfo directory)
        {
            foreach (FileInfo file in directory.GetFiles("*" + TS1File.TS1_EXTENSION))
            {
                Beginnings.Writer.WriteLine("\tFound {0}", file.Name);
                if (!this.CreateHashFile(file))
                    return false;
            }

            foreach (DirectoryInfo subDirectory in directory.GetDirectories())
            {
                //Beginnings.Writer.WriteLine("\tScanning Directory: {0}", directory.Name);
                if (!this.SearchForTS1File(subDirectory))
                    return false;
            }

            return true;
        }

        private bool CreateHashFile(FileInfo ts1File)
        {
            string fileName = ts1File.FullName.Replace(ts1File.Extension, Hashing.HASH_EXTENSION);
            //Beginnings.Writer.WriteLine("\tCreating Hash File: {0}", fileName);
            if (File.Exists(fileName))
            {
                // THe hash file already exists, probably because there was a communications error and we 
                // need to re-send the file as it was not sent properly, not recreate it.
                Beginnings.Writer.WriteLine("\tThe Hash File {0} already exists.", fileName);
                return true;
            }

            Hashing hasher = new Hashing();
            bool response = hasher.CreateHashFile(fileName, ts1File.DirectoryName, Export.HASH_INCLUSIONS);
            if (!response)
            {
                // TODO: Is there anything else to do here?
            }
            Beginnings.Writer.WriteLine("\tSuccessfully Created Hash File {0}.", fileName);

            return response;
        }

        #endregion

        /// <summary>
        /// Cleans up redundant files from the specified directory.
        /// </summary>
        /// <param name="directoryName">Name of the directory.</param>
        internal void Cleanup(DirectoryInfo dir)
        {
            // Make sure it exists
            if (!dir.Exists)
                return;

            // Process all the files
            bool found;
            foreach (FileInfo file in dir.GetFiles())
            {
                // Check if it is included and should remain
                found = false;
                foreach (string inclusion in Export.HASH_INCLUSIONS)
                {
                    if (inclusion.Equals(file.Extension.ToUpper()))
                    {
                        found = true;
                        break;
                    }
                }

                if (found)
                    continue;

                // Try to delete the redundant file
                try
                {
                    file.Delete();
                }
                catch (IOException ie)
                {
                    Beginnings.Writer.WriteLine("\t\tThere was an error cleaning up '{0}' ({1}).", file.FullName, ie.Message);
                }
            }

            // Process subdirectories
            foreach (DirectoryInfo subDir in dir.GetDirectories())
                this.Cleanup(subDir);
        }

        /// <summary>
        /// Enqueues the files to be sent using Indaba rather then directly by FTP.
        /// </summary>
        internal void EnqueueData()
        {
            DirectoryInfo dir = new DirectoryInfo(Options.ProgramOptions.FtpParameters.ExportPath);
            if (!dir.Exists)
                return;

            ICommunicate indaba = Client.Create();

            this.SendRecursive(dir, indaba, string.Empty);
        }

        private void SendRecursive(DirectoryInfo dir, ICommunicate indaba, string destination)
        {
            // Check if there's a TS1 file
            FileInfo[] files = dir.GetFiles("*" + TS1File.TS1_EXTENSION);
            if (files.Length > 0)
            {
                StreamReader reader = new StreamReader(files[0].Open(FileMode.Open), Encoding.Default, true);
                string contents = reader.ReadToEnd();
                reader.Close();

                TS1File ts1 = new TS1File(contents, false);
                ts1.ReadHeader();
                destination = ts1.Film.AuthorityName;

                // Fire callback to check if this is a local film and needs to update its status
                if (general.FilmFtpCallback != null)
                    general.FilmFtpCallback(ts1.Film.FilmNumber);
            }

            // Depth based traversal
            foreach (DirectoryInfo subDir in dir.GetDirectories())
            {
                this.SendRecursive(subDir, indaba, destination);

                try
                {
                    subDir.Delete();
                }
                catch (Exception ex)
                {
                    Beginnings.Writer.WriteLine("Error deleting {0}\n{1}", subDir.FullName, ex);
                }
            }

            // Send the files 
            files = dir.GetFiles();
            for (int x = files.Length - 1; x > -1; x--)
            {
                try
                {
                    // Find out where to send the file
                    if (destination.Trim().Length == 0)
                    {
                        Beginnings.Writer.WriteLine("\tCant send {0} as it has not destination!", files[x].Name);
                        continue;
                    }

                    indaba.Source = Options.ProgramOptions.IndabaSource;
                    indaba.Destination = destination;
                    Guid id = indaba.Enqueue(files[x]);
                    if (id != Guid.Empty)
                    {
                        Beginnings.Writer.WriteLine("\tQueueing {0}", files[x].Name);
                        files[x].Delete();
                    }
                }
                catch (Exception ex)
                {
                    Beginnings.Writer.WriteLine(ex.Message);
                    break;
                }
            }
        }

    }
}
