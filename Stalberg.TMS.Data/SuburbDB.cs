using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
	/// <summary>
	/// COntains all the database code for interacting with Area Codes
	/// </summary>
    public class SuburbDetails
    {
        public int SubIntNo;
        public string AreaCode;
        public string SubDescr;
    }

    public class SuburbDB
	{
		// Fields
		private string connectionString;

		/// <summary>
		/// Initializes a new instance of the <see cref="AreaCodeDB"/> class.
		/// </summary>
		/// <param name="connectionString">The connection string.</param>
		public SuburbDB(string connectionString)
		{
			this.connectionString = connectionString;
		}

        public SuburbDetails GetSuburbDetails(int subIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SuburbDetail", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterSubIntNo = new SqlParameter("@SubIntNo", SqlDbType.Int, 4);
            parameterSubIntNo.Value = subIntNo;
            com.Parameters.Add(parameterSubIntNo);

            con.Open();
            SqlDataReader result = com.ExecuteReader(CommandBehavior.CloseConnection);

            // Create Struct
            SuburbDetails mySubDetails = new SuburbDetails();

            if (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                mySubDetails.SubIntNo = Convert.ToInt32(result["SubIntNo"]);
                mySubDetails.AreaCode = Convert.ToString(result["AreaCode"]);
                mySubDetails.SubDescr = Convert.ToString(result["SubDescr"]);
            }

            result.Close();
            return mySubDetails;
        }

		/// <summary>
		/// Gets all area codes.
		/// </summary>
		/// <returns>A SQL Data Reader</returns>
        public DataSet GetAllSuburbs()
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("GetAllSuburbs", con);
            com.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(com);

            try
            {
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                da.Dispose();
            }
		}

         //<summary>
         //Gets a filtered list of area codes.
         //</summary>
         //<param name="areaCode">The area code.</param>
         //<param name="areaDescription">The area description.</param>
         //<returns>A Data set</returns>
         
        public DataSet GetAllSuburbsForAreaDS(string areaCode)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("GetAllSuburbsForArea", con);
            com.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(com);
                com.Parameters.Add("@AreaCode", SqlDbType.VarChar, 8).Value = areaCode;
                //com.Parameters.Add("@SubDescr", SqlDbType.VarChar, 100).Value = SubDescription;

            try
            {
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                da.Dispose();
            }
        }

		/// <summary>
		/// Saves the area code's details.
		/// </summary>
		/// <param name="areaCode">The area code.</param>
		/// <param name="description">The description.</param>
		/// <returns>
		/// 	<c>true</c> if the insert/update is successful, otherwise <c>false</c>.
		/// </returns>
        public int SaveSuburb(string areaCode, string subDescr, string lastUser, int subIntNo)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("SuburbUpdate", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AreaCode", SqlDbType.VarChar, 8).Value = areaCode;
            com.Parameters.Add("@SubDescr", SqlDbType.VarChar, 100).Value = subDescr;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            // Add output parameter
            SqlParameter parameterSubIntNo = new SqlParameter("@SubIntNo", SqlDbType.Int, 4);
            parameterSubIntNo.Value = subIntNo;
            parameterSubIntNo.Direction = ParameterDirection.InputOutput;
            com.Parameters.Add(parameterSubIntNo);
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Dispose();
                int SubIntNo = (int)com.Parameters["@SubIntNo"].Value;
                return SubIntNo;
            }
            catch (Exception e)
            {
                con.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int AddSuburb(string areaCode, string subDescr, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("SuburbAdd", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;
            // Add Parameters to SPROC
            cmd.Parameters.Add("@AreaCode", SqlDbType.VarChar, 8).Value = areaCode;
            cmd.Parameters.Add("@SubDescr", SqlDbType.VarChar, 100).Value = subDescr;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            // Add output parameter
            SqlParameter parameterSubIntNo = new SqlParameter("@SubIntNo", SqlDbType.Int, 4);
            parameterSubIntNo.Value = 0;
            parameterSubIntNo.Direction = ParameterDirection.InputOutput;
            cmd.Parameters.Add(parameterSubIntNo);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Dispose();
                int SubIntNo = (int)cmd.Parameters["@SubIntNo"].Value;
                return SubIntNo;
            }
            catch (Exception e)
            {
                con.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int DeleteSuburb(int subIntNo)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("SuburbDelete", con);
            com.CommandType = CommandType.StoredProcedure;
            // Add output parameter
            SqlParameter parameterSubIntNo = new SqlParameter("@SubIntNo", SqlDbType.Int, 4);
            parameterSubIntNo.Value = subIntNo;
            parameterSubIntNo.Direction = ParameterDirection.InputOutput;
            com.Parameters.Add(parameterSubIntNo);
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Dispose();
                int sub = (int)com.Parameters["@SubIntNo"].Value;
                return sub;
            }
            catch (Exception e)
            {
                con.Dispose();
                string msg = e.Message;
                return -2;
            }
        }

        public DataSet GetFilteredSuburbCodes(string areaCode, string areaDescription)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("GetFilteredSuburbCodes", con);
            com.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(com);
            if (areaCode.Length > 0)
                com.Parameters.Add("@AreaCode", SqlDbType.VarChar, 8).Value = areaCode;
            if (areaDescription.Length > 0)
                com.Parameters.Add("@AreaDescr", SqlDbType.VarChar, 100).Value = areaDescription;
            try
            {
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                da.Dispose();
            }
        }

	}
}
