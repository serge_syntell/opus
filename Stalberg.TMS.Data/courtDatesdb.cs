using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


namespace Stalberg.TMS
{

    //*******************************************************
    //
    // CourtDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class CourtDatesDetails
    {
        public Int32 CDIntNo;
        public DateTime CDate;
        public Int16 CDNoOfCases;
        public string sS56;
        public string sS54;
        public string sCurrentTotal;
        public DateTime CDCourtRollCreatedDate; //mrs 20080116
        public Int32 SummonsPrintSequence;
        public string CDS54Reserved;
        public string CDS56Reserved;
        public string CDTotalUtilised;
        public int OGIntNo;
        public int CDMaximumNumberOfNoAOGAllowed; //jerry 2012-03-23 add
        public int CCrtMaxNumberOfS35AllowedForS54;//Jake 2013-12-23 added 
        public bool CDateLocked;// Jake 2014-02-19 added
        public DateTime? CDCourtRollCreatedDate2;   // 2014-04-11, Oscar added
    }

    //Jake 2013-08-28 added new json entity 
    public class CourtDateEntity
    {
        public int CDIntNo { get; set; }
        public string CDate { get; set; }
    }

    public partial class CourtDatesDB
    {

        string mConstr = "";

        public CourtDatesDB(string vConstr)
        {
            mConstr = vConstr;
        }

        public SqlDataReader GetCourtList()
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        public SqlDataReader GetCourtDatesList(int crtRIntNo, int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtDatesList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtRIntNo = new SqlParameter("@CrtRIntNo", SqlDbType.Int, 4);
            parameterCrtRIntNo.Value = crtRIntNo;
            myCommand.Parameters.Add(parameterCrtRIntNo);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        public SqlDataReader GetWarrantAndSummonsCountDatesList(int crtRIntNo, int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("WarrantAndSummonsCourtDatesList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtRIntNo = new SqlParameter("@CrtRIntNo", SqlDbType.Int, 4);
            parameterCrtRIntNo.Value = crtRIntNo;
            myCommand.Parameters.Add(parameterCrtRIntNo);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        public SqlDataReader GetCourtDatesListByRule(int crtRIntNo, int autIntNo, int iNoOfDays)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtDatesListWithRule", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtRIntNo = new SqlParameter("@CrtRIntNo", SqlDbType.Int, 4);
            parameterCrtRIntNo.Value = crtRIntNo;
            myCommand.Parameters.Add(parameterCrtRIntNo);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterNoOfDays = new SqlParameter("@NoOfDays", SqlDbType.Int, 4);
            parameterNoOfDays.Value = iNoOfDays;
            myCommand.Parameters.Add(parameterNoOfDays);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        public SqlDataReader GetCourtDatesAvailableList(int crtRIntNo, int autIntNo, string noticeFilmType, string needRestrictNoAOG = null, string needSeparateNoAOG = null)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtDatesAvailableList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtRIntNo = new SqlParameter("@CrtRIntNo", SqlDbType.Int, 4);
            parameterCrtRIntNo.Value = crtRIntNo;
            myCommand.Parameters.Add(parameterCrtRIntNo);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            myCommand.Parameters.Add("@NotFilmType", SqlDbType.Char, 1).Value = noticeFilmType;

            //jerry 2012-03-22 add courtRuleRestrictNoAOG parameter
            if (!string.IsNullOrEmpty(needRestrictNoAOG))
            {
                myCommand.Parameters.Add("@NeedRestrictNoAOG", SqlDbType.Char, 1).Value = needRestrictNoAOG;
            }

            //Jerry 2012-05-21 add needSeparateNoAOG parameter
            if (!string.IsNullOrEmpty(needSeparateNoAOG))
            {
                myCommand.Parameters.Add("@NeedSeparateNoAOG", SqlDbType.Char, 1).Value = needSeparateNoAOG;
            }

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        public SqlDataReader GetAuth_CourtListByAuth(int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("Auth_CourtListByAuth", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        /// <summary>
        /// 2011/11/01 jerry add OGIntNo
        /// jerry 2012-03-23 add CDMaximumNumberOfNoAOGAllowed
        /// Jake 2014-02-19 added CDateLocked
        /// </summary>
        /// <param name="cdIntNo"></param>
        /// <returns></returns>
        public CourtDatesDetails GetCourtDatesDetails(int cdIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtDatesDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCDIntNo = new SqlParameter("@CDIntNo", SqlDbType.Int, 4);
            parameterCDIntNo.Value = cdIntNo;
            myCommand.Parameters.Add(parameterCDIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            CourtDatesDetails myCourtDatesDetails = new CourtDatesDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                //myCourtDatesDetails.CDIntNo = Convert.ToInt32(result["CDIntNo"]);
                myCourtDatesDetails.CDate = Convert.ToDateTime(result["CDate"]);
                myCourtDatesDetails.CDNoOfCases = Convert.ToInt16(result["CDNoOfCases"]);
                myCourtDatesDetails.sS54 = result["CDS54Allocated"].ToString();
                myCourtDatesDetails.sS56 = result["CDS56Allocated"].ToString();
                myCourtDatesDetails.sCurrentTotal = result["CDTotalAllocated"].ToString();
                myCourtDatesDetails.SummonsPrintSequence = Convert.ToInt32(result["SummonsPrintSequence"]);
                myCourtDatesDetails.CDateLocked = Convert.ToBoolean(result["CDateLocked"]);
                if (result["CDCourtRollCreatedDate"] != System.DBNull.Value) //mrs 20080116 added
                {
                    myCourtDatesDetails.CDCourtRollCreatedDate = Convert.ToDateTime(result["CDCourtRollCreatedDate"]); //mrs 20080116
                }
                else
                {
                    myCourtDatesDetails.CDCourtRollCreatedDate = DateTime.MinValue;
                }
                //mrs 20080811 added
                myCourtDatesDetails.CDS54Reserved = result["CDS54Reserved"].ToString();
                myCourtDatesDetails.CDS56Reserved = result["CDS56Reserved"].ToString();
                myCourtDatesDetails.CDTotalUtilised = result["CDTotalUtilised"].ToString();
                myCourtDatesDetails.OGIntNo = Convert.ToInt32(result["OGIntNo"]);
                myCourtDatesDetails.CDMaximumNumberOfNoAOGAllowed = Convert.ToInt32(result["CDMaximumNumberOfNoAOGAllowed"]);
                myCourtDatesDetails.CCrtMaxNumberOfS35AllowedForS54 = Convert.ToInt32(result["CCrtMaxNumberOfS35AllowedForS54"]);
            }
            result.Close();
            return myCourtDatesDetails;
        }

        /// <summary>
        /// 2011/11/01 jerry add OGIntNo
        /// jerry 2012-03-22 add noAOGMax parameter
        /// </summary>
        /// <param name="crtRIntNo"></param>
        /// <param name="autIntNo"></param>
        /// <param name="cDate"></param>
        /// <param name="cdNoOfCases"></param>
        /// <param name="lastUser"></param>
        /// <param name="ogIntNo"></param>
        /// <returns></returns>
        public int AddCourtDates(int crtRIntNo, int autIntNo, DateTime cDate, Int16 cdNoOfCases, string lastUser, int ogIntNo, int noAOGMax, int noAOGS35Max)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtDatesAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtRIntNo = new SqlParameter("@CrtRIntNo", SqlDbType.Int, 4);
            parameterCrtRIntNo.Value = crtRIntNo;
            myCommand.Parameters.Add(parameterCrtRIntNo);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCDate = new SqlParameter("@CDate", SqlDbType.SmallDateTime);
            parameterCDate.Value = cDate;
            myCommand.Parameters.Add(parameterCDate);

            SqlParameter parameterCDNoOfCases = new SqlParameter("@CDNoOfCases", SqlDbType.SmallInt);
            parameterCDNoOfCases.Value = cdNoOfCases;
            myCommand.Parameters.Add(parameterCDNoOfCases);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int, 4);
            parameterOGIntNo.Value = ogIntNo;
            myCommand.Parameters.Add(parameterOGIntNo);

            //jerry 2012-03-22 add
            SqlParameter parameterNoAOGMax = new SqlParameter("@CDMaximumNumberOfNoAOGAllowed", SqlDbType.Int, 4);
            parameterNoAOGMax.Value = noAOGMax;
            myCommand.Parameters.Add(parameterNoAOGMax);

            // Jake 2013-12-23 added 
            SqlParameter parameterNoAOGS35Max = new SqlParameter("@CDMaximumNumberOfNoAOGS35Allowed", SqlDbType.Int, 4);
            parameterNoAOGS35Max.Value = noAOGS35Max;
            myCommand.Parameters.Add(parameterNoAOGS35Max);

            SqlParameter parameterCDIntNo = new SqlParameter("@CDIntNo", SqlDbType.Int, 4);
            parameterCDIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterCDIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                int cdIntNo = Convert.ToInt32(parameterCDIntNo.Value);
                return cdIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int AddAuth_Court(int autIntNo, int crtIntNo, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("Auth_CourtAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterACIntNo = new SqlParameter("@ACIntNo", SqlDbType.Int, 4);
            parameterACIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterACIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int acIntNo = Convert.ToInt32(parameterACIntNo.Value);

                return acIntNo;
            }
            catch (Exception e)
            {

                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public SqlDataReader GetAuth_CourtListByCourt(int crtIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("Auth_CourtListByCourt", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        //mrs 20080811 not used in tms or tpexint
        //public DataSet GetCourtDatesAvailableListDS(int crtIntNo, int autIntNo)
        //{
        //    SqlDataAdapter sqlDACourts = new SqlDataAdapter();
        //    DataSet dsCourts = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlDACourts.SelectCommand = new SqlCommand();
        //    sqlDACourts.SelectCommand.Connection = new SqlConnection(mConstr);
        //    sqlDACourts.SelectCommand.CommandText = "CourtDatesAvailableList";

        //    // Mark the Command as a SPROC
        //    sqlDACourts.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
        //    parameterCrtIntNo.Value = crtIntNo;
        //    sqlDACourts.SelectCommand.Parameters.Add(parameterCrtIntNo);

        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    sqlDACourts.SelectCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command and close the connection
        //    sqlDACourts.Fill(dsCourts);
        //    sqlDACourts.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsCourts;		
        //}

        public DataSet GetCourtDatesListDS(int crtRIntNo, int autIntNo, int pageSize, int pageIndex, out int totalCount)
        {
            SqlDataAdapter sqlDACourts = new SqlDataAdapter();
            DataSet dsCourts = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDACourts.SelectCommand = new SqlCommand();
            sqlDACourts.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDACourts.SelectCommand.CommandText = "CourtDatesList";

            // Mark the Command as a SPROC
            sqlDACourts.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterCrtRIntNo = new SqlParameter("@CrtRIntNo", SqlDbType.Int, 4);
            parameterCrtRIntNo.Value = crtRIntNo;
            sqlDACourts.SelectCommand.Parameters.Add(parameterCrtRIntNo);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDACourts.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int);
            parameterPageSize.Value = pageSize;
            sqlDACourts.SelectCommand.Parameters.Add(parameterPageSize);

            SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int);
            parameterPageIndex.Value = pageIndex;
            sqlDACourts.SelectCommand.Parameters.Add(parameterPageIndex);

            SqlParameter parameterTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            parameterTotalCount.Direction = ParameterDirection.Output;
            sqlDACourts.SelectCommand.Parameters.Add(parameterTotalCount);

            // Execute the command and close the connection
            sqlDACourts.Fill(dsCourts);
            sqlDACourts.SelectCommand.Connection.Dispose();
            totalCount = (int)(parameterTotalCount.Value == DBNull.Value ? 0 : parameterTotalCount.Value);

            // Return the dataset result
            return dsCourts;
        }

        //mrs 20080118 different signature for hiding dates that have already got a courtrollcreatedate
        public DataSet GetCourtDatesListDS(int crtRIntNo, int autIntNo, string hideCourtRollCreated, int pageSize, int pageIndex, out int totalCount)
        {
            SqlDataAdapter sqlDACourts = new SqlDataAdapter();
            DataSet dsCourts = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDACourts.SelectCommand = new SqlCommand();
            sqlDACourts.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDACourts.SelectCommand.CommandText = "CourtDatesList";

            // Mark the Command as a SPROC
            sqlDACourts.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterCrtRIntNo = new SqlParameter("@CrtRIntNo", SqlDbType.Int, 4);
            parameterCrtRIntNo.Value = crtRIntNo;
            sqlDACourts.SelectCommand.Parameters.Add(parameterCrtRIntNo);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDACourts.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCRHide = new SqlParameter("@CRHide", SqlDbType.Char, 1);
            parameterCRHide.Value = hideCourtRollCreated;
            sqlDACourts.SelectCommand.Parameters.Add(parameterCRHide);

            SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int);
            parameterPageSize.Value = pageSize;
            sqlDACourts.SelectCommand.Parameters.Add(parameterPageSize);

            SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int);
            parameterPageIndex.Value = pageIndex;
            sqlDACourts.SelectCommand.Parameters.Add(parameterPageIndex);

            SqlParameter parameterTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            parameterTotalCount.Direction = ParameterDirection.Output;
            sqlDACourts.SelectCommand.Parameters.Add(parameterTotalCount);

            // Execute the command and close the connection
            sqlDACourts.Fill(dsCourts);
            sqlDACourts.SelectCommand.Connection.Dispose();

            totalCount = (int)(parameterTotalCount.Value == DBNull.Value ? 0 : parameterTotalCount.Value);

            // Return the dataset result
            return dsCourts;
        }

        public DataSet GetAuth_CourtListByAuthDS(int autIntNo)
        {
            SqlDataAdapter sqlDACourts = new SqlDataAdapter();
            DataSet dsCourts = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDACourts.SelectCommand = new SqlCommand();
            sqlDACourts.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDACourts.SelectCommand.CommandText = "Auth_CourtListByAuth";

            // Mark the Command as a SPROC
            sqlDACourts.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDACourts.SelectCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command and close the connection
            sqlDACourts.Fill(dsCourts);
            sqlDACourts.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsCourts;
        }

        public int UpdateCourtDates(int cdIntNo, DateTime cDate, Int16 cdNoOfCases, string lastUser, int ogIntNo, int noAOGMax, int noAOGS35Max)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtDatesUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCDate = new SqlParameter("@CDate", SqlDbType.SmallDateTime);
            parameterCDate.Value = cDate;
            myCommand.Parameters.Add(parameterCDate);

            SqlParameter parameterCDNoOfCases = new SqlParameter("@CDNoOfCases", SqlDbType.SmallInt);
            parameterCDNoOfCases.Value = cdNoOfCases;
            myCommand.Parameters.Add(parameterCDNoOfCases);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int);
            parameterOGIntNo.Value = ogIntNo;
            myCommand.Parameters.Add(parameterOGIntNo);

            //jerry 2012-03-22 add
            SqlParameter parameterNoAOGMax = new SqlParameter("@CDMaximumNumberOfNoAOGAllowed", SqlDbType.Int, 4);
            parameterNoAOGMax.Value = noAOGMax;
            myCommand.Parameters.Add(parameterNoAOGMax);

            // Jake 2013-12-23 added 
            SqlParameter parameterNoAOGS35Max = new SqlParameter("@CDMaximumNumberOfNoAOGS35Allowed", SqlDbType.Int, 4);
            parameterNoAOGS35Max.Value = noAOGS35Max;
            myCommand.Parameters.Add(parameterNoAOGS35Max);

            SqlParameter parameterCDIntNo = new SqlParameter("@CDIntNo", SqlDbType.Int);
            parameterCDIntNo.Direction = ParameterDirection.InputOutput;
            parameterCDIntNo.Value = cdIntNo;
            myCommand.Parameters.Add(parameterCDIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                cdIntNo = (int)myCommand.Parameters["@CDIntNo"].Value;
                return cdIntNo;
            }
            catch (Exception e)
            {

                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        //BD 20080527 - created update procedure for adding 1 to the print sequence for summons and returning original number for current file
        // dls 080622 - need to use the Auth_Court date value here, otherwise we are setting it for all courts
        //public int UpdateSummonsPrintSequence(int CourtIntNo, DateTime CourtDate, int PrintSequence)
        //public int UpdateSummonsPrintSequence(int acIntNo, DateTime CourtDate, int PrintSequence)
        public Int32 UpdateSummonsPrintSequence(int autIntNo, int crtRIntNo, DateTime CourtDate, int PrintSequence)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("SummonsPrintSequenceUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Direction = ParameterDirection.Input;
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCrtRIntNo = new SqlParameter("@CrtRIntNo", SqlDbType.Int);
            parameterCrtRIntNo.Direction = ParameterDirection.Input;
            parameterCrtRIntNo.Value = crtRIntNo;
            myCommand.Parameters.Add(parameterCrtRIntNo);

            // Add Parameters to SPROC
            SqlParameter parameterCourtDate = new SqlParameter("@CourtDate", SqlDbType.DateTime);
            parameterCourtDate.Direction = ParameterDirection.Input;
            parameterCourtDate.Value = CourtDate;
            myCommand.Parameters.Add(parameterCourtDate);

            // Add Parameters to SPROC
            SqlParameter parameterPrintSequence = new SqlParameter("@PrintSequence", SqlDbType.Int);
            parameterPrintSequence.Direction = ParameterDirection.InputOutput;
            parameterPrintSequence.Value = PrintSequence;
            myCommand.Parameters.Add(parameterPrintSequence);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                PrintSequence = (int)myCommand.Parameters["@PrintSequence"].Value;
                return PrintSequence;
            }
            catch (Exception e)
            {

                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int UpdateCourtPartial(int crtIntNo, string crtName, string crtPaymentAddr1,
        string crtPaymentAddr2, string crtPaymentAddr3, string crtPaymentAddr4, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtUpdatepartial", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtName = new SqlParameter("@CrtName", SqlDbType.VarChar, 30);
            parameterCrtName.Value = crtName;
            myCommand.Parameters.Add(parameterCrtName);

            SqlParameter parameterCrtPaymentAddr1 = new SqlParameter("@CrtPaymentAddr1", SqlDbType.VarChar, 25);
            parameterCrtPaymentAddr1.Value = crtPaymentAddr1;
            myCommand.Parameters.Add(parameterCrtPaymentAddr1);

            SqlParameter parameterCrtPaymentAddr2 = new SqlParameter("@CrtPaymentAddr2", SqlDbType.VarChar, 25);
            parameterCrtPaymentAddr2.Value = crtPaymentAddr2;
            myCommand.Parameters.Add(parameterCrtPaymentAddr2);

            SqlParameter parameterCrtPaymentAddr3 = new SqlParameter("@CrtPaymentAddr3", SqlDbType.VarChar, 25);
            parameterCrtPaymentAddr3.Value = crtPaymentAddr3;
            myCommand.Parameters.Add(parameterCrtPaymentAddr3);

            SqlParameter parameterCrtPaymentAddr4 = new SqlParameter("@CrtPaymentAddr4", SqlDbType.VarChar, 25);
            parameterCrtPaymentAddr4.Value = crtPaymentAddr4;
            myCommand.Parameters.Add(parameterCrtPaymentAddr4);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int);
            parameterCrtIntNo.Direction = ParameterDirection.InputOutput;
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int CrtIntNo = (int)myCommand.Parameters["@CrtIntNo"].Value;
                //int menuId = (int)parameterCrtIntNo.Value;

                return CrtIntNo;
            }
            catch (Exception e)
            {

                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int UpdateAuth_Court(int acIntNo, int autIntNo, int crtIntNo, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterACIntNo = new SqlParameter("@ACIntNo", SqlDbType.Int);
            parameterACIntNo.Direction = ParameterDirection.InputOutput;
            parameterACIntNo.Value = acIntNo;
            myCommand.Parameters.Add(parameterACIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int ACIntNo = (int)myCommand.Parameters["@ACIntNo"].Value;
                //int menuId = (int)parameterCrtIntNo.Value;

                return ACIntNo;
            }
            catch (Exception e)
            {

                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int GetCourtIntNoByCourtNo(string courtNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtIntNoByCourtNo", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCourtNo = new SqlParameter("@CrtNo", SqlDbType.VarChar, 6);
            parameterCourtNo.Value = courtNo;
            myCommand.Parameters.Add(parameterCourtNo);

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterCrtIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int getCrtIntNo = Convert.ToInt32(parameterCrtIntNo.Value);

                return getCrtIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public String DeleteAuth_Court(int acIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("Auth_CourtDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterACIntNo = new SqlParameter("@ACIntNo", SqlDbType.Int, 4);
            parameterACIntNo.Value = acIntNo;
            parameterACIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterACIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int ACIntNo = (int)parameterACIntNo.Value;

                return ACIntNo.ToString();
            }
            catch
            {
                myConnection.Dispose();
                return String.Empty;
            }
        }

        public String DeleteCourt(int crtIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtIntNo;
            parameterCrtIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterCrtIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int CrtIntNo = (int)parameterCrtIntNo.Value;

                return CrtIntNo.ToString();
            }
            catch
            {
                myConnection.Dispose();
                return String.Empty;
            }
        }

        // 2014-04-11, Oscar added
        public List<CourtDatesDetails> GetCourtDatesDetailsBySumIntNo(int sumIntNo)
        {
            var result = new List<CourtDatesDetails>();
            if (sumIntNo <= 0) return result;

            var paras = new[]
            {
                new SqlParameter("@SumIntNo", sumIntNo)
            };
            using (var conn = new SqlConnection(mConstr))
            {
                using (var cmd = new SqlCommand("GetCourtDatesDetailsBySumIntNo", conn)
                {
                    CommandType = CommandType.StoredProcedure
                })
                {
                    cmd.Parameters.AddRange(paras);
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            result.Add(new CourtDatesDetails
                            {
                                CDIntNo = Helper.GetReaderValue<int>(dr, "CDIntNo"),
                                CDate = Helper.GetReaderValue<DateTime>(dr, "CDate"),
                                CDNoOfCases = Helper.GetReaderValue<short>(dr, "CDNoOfCases"),
                                sS56 = Helper.GetReaderValue<string>(dr, "CDS56Allocated"),
                                sS54 = Helper.GetReaderValue<string>(dr, "CDS54Allocated"),
                                sCurrentTotal = Helper.GetReaderValue<string>(dr, "CDTotalAllocated"),
                                CDCourtRollCreatedDate2 = Helper.GetReaderValue<DateTime?>(dr, "CDCourtRollCreatedDate"),
                                SummonsPrintSequence = Helper.GetReaderValue<int>(dr, "SummonsPrintSequence"),
                                CDS54Reserved = Helper.GetReaderValue<string>(dr, "CDS54Reserved"),
                                CDS56Reserved = Helper.GetReaderValue<string>(dr, "CDS56Reserved"),
                                CDTotalUtilised = Helper.GetReaderValue<string>(dr, "CDTotalUtilised"),
                                OGIntNo = Helper.GetReaderValue<int>(dr, "OGIntNo"),
                                CDMaximumNumberOfNoAOGAllowed = Helper.GetReaderValue<int>(dr, "CDMaximumNumberOfNoAOGAllowed"),
                                CCrtMaxNumberOfS35AllowedForS54 = Helper.GetReaderValue<int>(dr, "CCrtMaxNumberOfS35AllowedForS54"),
                                CDateLocked = Helper.GetReaderValue<bool>(dr, "CDateLocked")
                            });
                        }
                        return result;
                    }
                }
            }
        }

    }
}

