using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the data for a Summons Server
    /// </summary>
    public class SummonsServerDetails
    {
        public int SSIntNo;
        public string SSFullName;
        public string SSAddress;
        public string SSTelNo;
        public string SSEmail;
        public string SSCode;
    }

    public class AuthSSDetails
    {
        public int ASIntNo;
        public int AutIntNo;
        public int SSIntNo;
        public string AreaCode;
        public string ASSAllocateManual;
        public string ASSPriority;
        public int ASSPercentage;
    }

    /// <summary>
    /// Contains all the database code for manipulating Summons Servers
    /// </summary>
    public class SummonsServerDB
    {
        // Fields
        private readonly string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="SummonsServerDB"/> class.
        /// </summary>
        /// <param name="vConstr">The database connection string.</param>
        public SummonsServerDB(string vConstr)
        {
            connectionString = vConstr;
        }

        /// <summary>
        /// Gets the summons server details.
        /// </summary>
        /// <param name="crtIntNo">The court int no.</param>
        /// <returns></returns>
        public SummonsServerDetails GetSummonsServerDetails(int ssIntNo, int asIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("SummonsServerDetail", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP 
            if (ssIntNo > 0)
                com.Parameters.Add("@SSIntNo", SqlDbType.Int, 4).Value = ssIntNo;
            else if (asIntNo > 0)
                com.Parameters.Add("@ASIntNo", SqlDbType.Int, 4).Value = asIntNo;

            // Create CustomerDetails
            SummonsServerDetails mySummonsServerDetails = new SummonsServerDetails();

            con.Open();
            SqlDataReader result = com.ExecuteReader(CommandBehavior.CloseConnection);
            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                mySummonsServerDetails.SSIntNo = Convert.ToInt32(result["SSIntNo"].ToString());
                mySummonsServerDetails.SSFullName = result["SSFullName"].ToString();
                mySummonsServerDetails.SSAddress = result["SSAddress"].ToString();
                mySummonsServerDetails.SSTelNo = result["SSTelNo"].ToString();
                mySummonsServerDetails.SSEmail = result["SSEmail"].ToString();
                mySummonsServerDetails.SSCode = result["SSCode"].ToString();
            }
            result.Close();

            return mySummonsServerDetails;
        }

        /// <summary>
        /// Gets the summons server details.
        /// </summary>
        /// <param name="crtIntNo">The court int no.</param>
        /// <returns></returns>
        public AuthSSDetails GetASSDetails(int asIntNo)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("AuthSSDetail", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP 
            com.Parameters.Add("@ASIntNo", SqlDbType.Int, 4).Value = asIntNo;
            AuthSSDetails myASSDetails = new AuthSSDetails();

            con.Open();
            SqlDataReader result = com.ExecuteReader(CommandBehavior.CloseConnection);
            while (result.Read())
            {
                myASSDetails.ASIntNo = Convert.ToInt32(result["ASIntNo"].ToString());
                myASSDetails.SSIntNo = Convert.ToInt32(result["SSIntNo"].ToString());
                myASSDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"].ToString());
                myASSDetails.AreaCode = result["AreaCode"].ToString();
                myASSDetails.ASSAllocateManual = result["ASSAllocateManual"].ToString();
                myASSDetails.ASSPriority = result["ASSPriority"].ToString();
                myASSDetails.ASSPercentage = Convert.ToInt16(result["ASSPercentage"].ToString());
            }
            result.Close();

            return myASSDetails;
        }

        public SqlDataReader GetSummonsServerList()
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("SummonsServerList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader
            return result;
        }

        public DataSet GetSummonsServerListDS()
        {
            SqlDataAdapter sqlDASummonsServers = new SqlDataAdapter();
            DataSet dsSummonsServers = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDASummonsServers.SelectCommand = new SqlCommand();
            sqlDASummonsServers.SelectCommand.Connection = new SqlConnection(connectionString);
            sqlDASummonsServers.SelectCommand.CommandText = "SummonsServerList";
            sqlDASummonsServers.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                // Execute the command and close the connection
                sqlDASummonsServers.Fill(dsSummonsServers);
                sqlDASummonsServers.SelectCommand.Connection.Dispose();

                // Return the dataset result
                return dsSummonsServers;
            }
            finally
            {
                sqlDASummonsServers.Dispose();
            }
        }

        public DataSet GetSummonsServerListForAuthAndSSDS(int autIntNo, int ssIntNo)
        {
            SqlDataAdapter sqlDASummonsServers = new SqlDataAdapter();
            DataSet dsSummonsServers = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDASummonsServers.SelectCommand = new SqlCommand();
            sqlDASummonsServers.SelectCommand.Connection = new SqlConnection(connectionString);
            sqlDASummonsServers.SelectCommand.CommandText = "SummonsServerListForAuthAndSS";
            sqlDASummonsServers.SelectCommand.CommandType = CommandType.StoredProcedure;
            // Add Parameters to the SP 
            sqlDASummonsServers.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            sqlDASummonsServers.SelectCommand.Parameters.Add("@SSIntNo", SqlDbType.Int, 4).Value = ssIntNo;

            try
            {
                // Execute the command and close the connection
                sqlDASummonsServers.Fill(dsSummonsServers);
                sqlDASummonsServers.SelectCommand.Connection.Dispose();

                // Return the dataset result
                return dsSummonsServers;
            }
            finally
            {
                sqlDASummonsServers.Dispose();
            }
        }

        public int UpdateSummonsServer(string ssFullName, string ssAddress, string ssTelNo,
            string ssEmail, string ssCode, string lastUser, int ssIntNo)
        {
            //20080531 is ssArea used anywhere? it is not valid for this table. i replaced it with ssCode
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("SummonsServerUpdate", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            com.Parameters.Add("@SSFullName", SqlDbType.VarChar, 100).Value = ssFullName;
            com.Parameters.Add("@SSAddress", SqlDbType.VarChar, 100).Value = ssAddress;
            com.Parameters.Add("@SSTelNo", SqlDbType.VarChar, 20).Value = ssTelNo;
            com.Parameters.Add("@SSEmail", SqlDbType.VarChar, 50).Value = ssEmail;
            com.Parameters.Add("@SSCode", SqlDbType.VarChar, 5).Value = ssCode;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@SSIntNo", SqlDbType.Int, 4).Value = ssIntNo;

            try
            {
                con.Open();
                return (int)com.ExecuteScalar();
            }
            catch (SqlException se)
            {
                string msg = se.Message;
            }
            finally
            {
                con.Close();
            }

            return 0;
        }

        public int UpdateSummonsServer(string ssFullName, string ssAddress, string ssTelNo,
            string ssEmail, string ssCode, string lastUser)
        {
            //update summons server when you do not know the ssintno and ir could or could not be a new summons server
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("SummonsServerAddOrUpdate", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            com.Parameters.Add("@SSFullName", SqlDbType.VarChar, 100).Value = ssFullName;
            com.Parameters.Add("@SSAddress", SqlDbType.VarChar, 100).Value = ssAddress;
            com.Parameters.Add("@SSTelNo", SqlDbType.VarChar, 20).Value = ssTelNo;
            com.Parameters.Add("@SSEmail", SqlDbType.VarChar, 50).Value = ssEmail;
            com.Parameters.Add("@SSCode", SqlDbType.VarChar, 5).Value = ssCode;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                con.Open();
                return (int)com.ExecuteScalar();
            }
            catch (SqlException se)
            {
                string msg = se.Message;
            }
            finally
            {
                con.Close();
            }

            return 0;
        }

        public int AddAuth_SummonsServer(int autIntNo, int ssIntNo, string areaCode, string lastUser)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("Auth_SummonsServerAdd", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@SSIntNo", SqlDbType.Int).Value = ssIntNo;
            com.Parameters.Add("@AreaCode", SqlDbType.VarChar, 8).Value = areaCode;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@ASIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Dispose();
                int assIntNo = Convert.ToInt32(com.Parameters["@ASIntNo"].Value);
                return assIntNo;
            }
            catch (Exception e)
            {
                con.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int AddAllAuth_SummonsServer(int autIntNo, int ssIntNo, string lastUser)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("Auth_SummonsServerAddAll", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@SSIntNo", SqlDbType.Int).Value = ssIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@ASIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Dispose();
                int assIntNo = Convert.ToInt32(com.Parameters["@ASIntNo"].Value);
                return assIntNo;
            }
            catch (Exception e)
            {
                con.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int UpdateAuthSS(string assManual, string assPriority, int assPercentage, string lastUser, int asIntNo)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("AuthSSUpdate", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            com.Parameters.Add("@ASSAllocateManual", SqlDbType.Char, 1).Value = assManual;
            com.Parameters.Add("@ASSPriority", SqlDbType.Char, 1).Value = assPriority;
            com.Parameters.Add("@ASSPercentage", SqlDbType.TinyInt).Value = assPercentage;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            //mrs 20080618 i prefer parameters
            com.Parameters.Add("@ASIntNo", SqlDbType.Int).Value = asIntNo;
            com.Parameters["@ASIntNo"].Direction = ParameterDirection.InputOutput;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Dispose();
                int assIntNo = Convert.ToInt32(com.Parameters["@ASIntNo"].Value);
                return assIntNo;
                //con.Open();
                //return (int)com.ExecuteScalar();
            }
            catch (SqlException se)
            {
                string msg = se.Message;
            }
            finally
            {
                con.Close();
            }

            return 0;
        }

        /// <summary>
        /// Gets the summons server list by authority.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <returns></returns>
        public SqlDataReader GetAuth_SummonsServerListByAuth(int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("Auth_SummonsServerListByAuth", con);
            com.CommandType = CommandType.StoredProcedure;

            // Parameters
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            // Execute the command and return the reader
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }
        // 2013-07-19 comment by Henry for useless
        //public SqlDataReader GetAuth_SummonsServerListBySummonsServer(int ssIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("Auth_SummonsServerListBySummonsServer", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    SqlParameter parameterSSIntNo = new SqlParameter("@SSIntNo", SqlDbType.Int, 4);
        //    parameterSSIntNo.Value = ssIntNo;
        //    myCommand.Parameters.Add(parameterSSIntNo);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the data reader result
        //    return result;
        //}

        /// <summary>
        /// Gets a data set of summons server by authority.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <returns></returns>
        public DataSet GetAuth_SummonsServerListByAuthDS(int autIntNo)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            // Create Instance of Connection and Command Object
            da.SelectCommand = new SqlCommand();
            da.SelectCommand.Connection = new SqlConnection(this.connectionString);
            da.SelectCommand.CommandText = "Auth_SummonsServerListByAuth";
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Parameters
            da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            // Execute the command and close the connection
            da.Fill(ds);
            da.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return ds;
        }
        // 2013-07-19 comment by Henry for useless
        //public int UpdateSummonsServerPartial(int crtIntNo, string crtName, string crtPaymentAddr1,
        //string crtPaymentAddr2, string crtPaymentAddr3, string crtPaymentAddr4, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("SummonsServerUpdatepartial", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterCrtName = new SqlParameter("@CrtName", SqlDbType.VarChar, 30);
        //    parameterCrtName.Value = crtName;
        //    myCommand.Parameters.Add(parameterCrtName);

        //    SqlParameter parameterCrtPaymentAddr1 = new SqlParameter("@CrtPaymentAddr1", SqlDbType.VarChar, 25);
        //    parameterCrtPaymentAddr1.Value = crtPaymentAddr1;
        //    myCommand.Parameters.Add(parameterCrtPaymentAddr1);

        //    SqlParameter parameterCrtPaymentAddr2 = new SqlParameter("@CrtPaymentAddr2", SqlDbType.VarChar, 25);
        //    parameterCrtPaymentAddr2.Value = crtPaymentAddr2;
        //    myCommand.Parameters.Add(parameterCrtPaymentAddr2);

        //    SqlParameter parameterCrtPaymentAddr3 = new SqlParameter("@CrtPaymentAddr3", SqlDbType.VarChar, 25);
        //    parameterCrtPaymentAddr3.Value = crtPaymentAddr3;
        //    myCommand.Parameters.Add(parameterCrtPaymentAddr3);

        //    SqlParameter parameterCrtPaymentAddr4 = new SqlParameter("@CrtPaymentAddr4", SqlDbType.VarChar, 25);
        //    parameterCrtPaymentAddr4.Value = crtPaymentAddr4;
        //    myCommand.Parameters.Add(parameterCrtPaymentAddr4);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int);
        //    parameterCrtIntNo.Direction = ParameterDirection.InputOutput;
        //    parameterCrtIntNo.Value = crtIntNo;
        //    myCommand.Parameters.Add(parameterCrtIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int CrtIntNo = (int)myCommand.Parameters["@CrtIntNo"].Value;
        //        //int menuId = (int)parameterCrtIntNo.Value;

        //        return CrtIntNo;
        //    }
        //    catch (Exception e)
        //    {

        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        public int UpdateAuth_SummonsServer(int acIntNo, int autIntNo, int crtIntNo, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("SummonsServerUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterACIntNo = new SqlParameter("@ACIntNo", SqlDbType.Int);
            parameterACIntNo.Direction = ParameterDirection.InputOutput;
            parameterACIntNo.Value = acIntNo;
            myCommand.Parameters.Add(parameterACIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int ACIntNo = (int)myCommand.Parameters["@ACIntNo"].Value;
                //int menuId = (int)parameterCrtIntNo.Value;

                return ACIntNo;
            }
            catch (Exception e)
            {

                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// Gets the summons server int no by summons server no.
        /// </summary>
        /// <param name="SummonsServerNo">The summons server no.</param>
        /// <returns></returns>
        // 2013-07-19 comment by Henry for useless
        //public int GetSummonsServerIntNoBySummonsServerNo(string SummonsServerNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("SummonsServerIntNoBySummonsServerNo", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterSummonsServerNo = new SqlParameter("@CrtNo", SqlDbType.VarChar, 6);
        //    parameterSummonsServerNo.Value = SummonsServerNo;
        //    myCommand.Parameters.Add(parameterSummonsServerNo);

        //    SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
        //    parameterCrtIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterCrtIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int getCrtIntNo = Convert.ToInt32(parameterCrtIntNo.Value);

        //        return getCrtIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        /// <summary>
        /// Deletes the authority summons server.
        /// </summary>
        /// <param name="acIntNo">The ac int no.</param>
        /// <returns></returns>
        public int DeleteAuth_SummonsServer(int assIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("Auth_SummonsServerDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterASIntNo = new SqlParameter("@ASIntNo", SqlDbType.Int, 4);
            parameterASIntNo.Value = assIntNo;
            parameterASIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterASIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int ASIntNo = (int)parameterASIntNo.Value;

                return ASIntNo;
            }
            catch
            {
                myConnection.Dispose();
                return 0;
            }
        }

        /// <summary>
        /// Deletes the summons server.
        /// </summary>
        /// <param name="ssIntNo">The ss int no.</param>
        /// <param name="checkOnly">if set to <c>true</c> then no delete is performed, 
        /// only a check for linked authorities.</param>
        /// <returns>
        /// The number of authorities linked to the summons server;
        /// greater then zero means the delete did not take place.
        /// </returns>
        public int DeleteSummonsServer(int ssIntNo, bool checkOnly)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("SummonsServerDelete", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            com.Parameters.Add("@SSIntNo", SqlDbType.Int, 4).Value = ssIntNo;
            com.Parameters.Add("@CheckOnly", SqlDbType.Bit).Value = checkOnly;

            try
            {
                con.Open();
                return (int)com.ExecuteScalar();
            }
            catch (SqlException se)
            {
                System.Diagnostics.Debug.WriteLine(se.Message);
            }
            finally
            {
                con.Close();
            }

            return -1;
        }

        /// <summary>
        /// Unlinks the authority summons server area.
        /// </summary>
        /// <param name="asIntNo">As int no.</param>
        public void UnlinkAuthoritySummonsServerArea(int asIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("UnlinkAuthoritySummonsServerArea", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@ASIntNo", SqlDbType.Int, 4).Value = asIntNo;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Links the summons server to authority and area.
        /// </summary>
        /// <param name="authIntNo">The authority int no.</param>
        /// <param name="ssIntNo">The Summons Server int no.</param>
        /// <param name="areaCode">The area code.</param>
        /// <param name="lastUser">The last user.</param>
        public void LinkSummonsServer2AuthorityArea(int authIntNo, int ssIntNo, string areaCode, string lastUser)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("LinkSummonsServer2AuthorityArea", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AuthIntNo", SqlDbType.Int, 4).Value = authIntNo;
            com.Parameters.Add("@SSIntNo", SqlDbType.Int, 4).Value = ssIntNo;
            com.Parameters.Add("@AreaCode", SqlDbType.VarChar, 8).Value = areaCode;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Gets a data set containing the summon server staff.
        /// </summary>
        /// <param name="intNo">The int no of either the summons server (isAllStaff = true), of just an individual staff member (isAllStaff = false).</param>
        /// <param name="isAllStaff">if set to <c>true</c> then all staff for a summons server are returned.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetSummonServerStaff(int intNo, bool isAllStaff, bool showOnlyActive)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = new SqlCommand("GetSummonServerStaff", new SqlConnection(this.connectionString));
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            if (isAllStaff)
                da.SelectCommand.Parameters.Add("@SSIntNo", SqlDbType.Int, 4).Value = intNo;
            else
                da.SelectCommand.Parameters.Add("@SS_SIntNo", SqlDbType.Int, 4).Value = intNo;

            da.SelectCommand.Parameters.Add("@ShowOnlyActive", SqlDbType.Bit).Value = showOnlyActive;

            DataSet ds = new DataSet();
            da.Fill(ds);
            da.SelectCommand.Connection.Dispose();

            return ds;
        }

        /// <summary>
        /// Updates the summons server staff member details.
        /// </summary>
        /// <param name="ssIntNo">The summons server int no.</param>
        /// <param name="staffIntNo">The staff int no.</param>
        /// <param name="name">The name.</param>
        /// <param name="forceNo">The force no.</param>
        /// <param name="certificate">The certificate.</param>
        /// <param name="dt">The dt.</param>
        /// <param name="user">The user.</param>
        public void UpdateStaffDetails(int ssIntNo, int staffIntNo, string name, string forceNo, string certificate, DateTime dt, string user, bool active)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SummonsServerStaffUpdate", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@SSIntNo", SqlDbType.Int, 4).Value = ssIntNo;
            com.Parameters.Add("@SS_SIntNo", SqlDbType.Int, 4).Value = staffIntNo;
            com.Parameters.Add("@Name", SqlDbType.VarChar, 100).Value = name;
            if (forceNo.Length > 0)
                com.Parameters.Add("@ForceNo", SqlDbType.VarChar, 20).Value = forceNo;
            if (certificate.Length > 0)
                com.Parameters.Add("@Certificate", SqlDbType.VarChar, 100).Value = certificate;
            if (DateTime.Compare(dt, DateTime.MinValue) > 0)
                com.Parameters.Add("@CertDate", SqlDbType.SmallDateTime, 4).Value = dt;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = user;
            com.Parameters.Add("@Active", SqlDbType.Bit).Value = active;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Tries to delete the Summonses server staff member.
        /// </summary>
        /// <param name="staffIntNo">The staff int no.</param>
        /// <returns><c>true</c> if the delete was successful</returns>
        public bool SummonsServerStaffDelete(int staffIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SummonsServerStaffDelete", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@SS_SIntNo", SqlDbType.Int, 4).Value = staffIntNo;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            catch (SqlException se)
            {
                System.Diagnostics.Debug.WriteLine(se.Message);
                return false;
            }
            finally
            {
                con.Close();
            }

            return true;
        }

        /// <summary>
        /// Gets the summons server for area.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="areaCode">The area code.</param>
        /// <returns></returns>
        public SqlDataReader GetSummonsServerForArea(int autIntNo, string areaCode)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SummonsServerForArea", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@AreaCode", SqlDbType.VarChar, 8).Value = areaCode;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        // 2013-11-11, Oscar removed - we don't need 'Unknown' (Dawn)
        //public SummonsServerDetails GetUnknownSummonsServer()
        //{
        //    SummonsServerDetails details = new SummonsServerDetails();

        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand cmd = new SqlCommand("RoadblockUnknownSummonsServer", con);
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    try
        //    {
        //        con.Open();
        //        SqlDataReader reader = cmd.ExecuteReader();

        //        if(reader.Read())
        //        {
        //            details.SSIntNo = reader.GetInt32(0);
        //            details.SSFullName = reader.GetString(1);
        //            details.SSCode = reader.GetString(2);
        //        }
        //        reader.Close();
        //    }
        //    catch (SqlException se)
        //    {
        //        return null;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }

        //    return details;
        //}

    }
}

