using System;
using System.Data;
using System.Data.SqlClient;
using Stalberg.TMS.Data;
using System.Collections.Generic;
using Stalberg.TMS.Data.Util;

namespace Stalberg.TMS
{
    public class NoticesForReceipt
    {
        private string notTicketNo;
        private decimal notAmountPaid;
        private string notRegNo;

        public string NotTicketNo
        {
            get { return this.notTicketNo; }
            set { this.notTicketNo = value; }
        }

        public string NotRegNo
        {
            get { return this.notRegNo; }
            set { this.notRegNo = value; }
        }

        public decimal NotAmountPaid
        {
            get { return this.notAmountPaid; }
            set { this.notAmountPaid = value; }
        }
    }
    /// <summary>
    /// Represents a ticket number
    /// </summary>

    public class TicketNo
    {
        // Fields
        private int notIntNo;
        private string notTicketNo;
        private string nPrefix;
        private string sNumber;
        private string autNumber;

        /// <summary>
        /// Gets or sets the not int no.
        /// </summary>
        /// <value>The not int no.</value>
        public int NotIntNo
        {
            get { return this.notIntNo; }
            set { this.notIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the not ticket no.
        /// </summary>
        /// <value>The not ticket no.</value>
        public string NotTicketNo
        {
            get { return this.notTicketNo; }
            set { this.notTicketNo = value; }
        }

        /// <summary>
        /// Gets or sets the N prefix.
        /// </summary>
        /// <value>The N prefix.</value>
        public string NPrefix
        {
            get { return this.nPrefix; }
            set { this.nPrefix = value; }
        }

        /// <summary>
        /// Gets or sets the S number.
        /// </summary>
        /// <value>The S number.</value>
        public string SNumber
        {
            get { return this.sNumber; }
            set { this.sNumber = value; }
        }

        /// <summary>
        /// Gets or sets the authority number.
        /// </summary>
        /// <value>The aut number.</value>
        public string AutNumber
        {
            get { return this.autNumber; }
            set { this.autNumber = value; }
        }

        // 2015-02-26, Oscar added. (1868, ref 1866)
        public int ErrorCode { get; set; }
        public string Error { get; set; }
    }

    /// <summary>
    /// Run general procedures and functions
    /// </summary>
    public class NoticeNumbers
    {
        // Fields
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="NoticeNumbers"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public NoticeNumbers(string connectionString)
        {
            this.connectionString = connectionString;
        }

        //dls 071010 - added additional parameters to return prefix & seq no & autno (not used at this stage) to ticket no proc
        //public string GenerateNoticeNumber(string this.connectionString, string user, string ntCode, int autIntNo)
        /// <summary>
        /// Generates a notice number.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="ntCode">The nt code.</param>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        public TicketNo GenerateNoticeNumber(string user, string ntCode, int autIntNo)
        {
            TicketNo ticket = new TicketNo();

            // dls 070508 - Stellenbosch is using 1001 - NoticePrefix has been extended from a char(2) to a varchar(4)

            // current method is as follows
            // A group = notice type
            //		1 01-09 = suspended vehicles = A1
            //		2 10-49 = section 341 notice of intended prosecution does not lapse (20 years shelf life)= A2
            //		3 50-69 = section 56 notice to appear in court = A3
            //		4 70-79 = hand-written summons = A4
            //		5 80-89 = first information of crime = A5
            //		6 90-99 = computer printed section 341 camera A6
            //  **** Stellenbosch is using 1001 - NoticePrefix has been extended from a char(2) to a varchar(4)
            // B group = sequence number
            // C group = 3 digit authority number, made up of the 1st 3 digits of the postal code
            // D group = cdv calculation
            //		A + (2 x B) + C = D
            // 999999 must cause the system to roll over to the next prefix number allocated to the type

            try
            {
                string noticeNumber = "0/0/0/0";
                int seqLength = 6; // currently sequence is six digits long
                int ntIntNo = 0;
                string ntDescr = "Notice";
                string ntStartPrefix = "A1";
                int npIntNo = 0;
                string nPrefix = "A1";
                string autNumber = "1";
                string sNumber = "0";
                char cTemp = Convert.ToChar("0");
                string tnType = "A";
                string cdv = "XX";
                int tnIntNo = 0;

                bool cdvPadding = false;
                int cdvPaddingLength = 6;
                // Jake 2012-07-18 added check authority rule (sequeuce number length and cdv padding )
                AuthorityRulesDetails ard = new AuthorityRulesDetails();
                ard.AutIntNo = autIntNo;
                ard.ARCode = "9300";
                ard.LastUser = "";

                DefaultAuthRules authRule = new DefaultAuthRules(ard, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                if (value.Value == "Y")
                { cdvPadding = true; }

                seqLength = value.Key;

                // Jake 2012-11-19 
                if (seqLength <= 0)
                {
                    noticeNumber = "0";
                    ticket.NotTicketNo = noticeNumber;
                    ticket.ErrorCode = -1;  // 2015-02-26, Oscar added. (1868, ref 1866)
                    return ticket;
                }

                // type of notice determined by calling program (noticeType)
                // look-up starting prefix in use for this type
                Stalberg.TMS.NoticeTypeDB noticeType = new Stalberg.TMS.NoticeTypeDB(this.connectionString);

                Stalberg.TMS.NoticeTypeDetails ntDetails = noticeType.GetNoticeTypeDetailsByCode(ntCode);
                ntIntNo = ntDetails.NTIntNo;
                if (ntIntNo > 0)
                {
                    ntDescr = ntDetails.NTDescr;
                    ntStartPrefix = ntDetails.NTStartPrefix;
                }
                else
                {
                    noticeNumber = "0";
                    ticket.NotTicketNo = noticeNumber;
                    ticket.ErrorCode = -2;  // 2015-02-26, Oscar added. (1868, ref 1866)
                    return ticket;
                }
                // look-up current prefix in use for this type
                Stalberg.TMS.NoticePrefixDB noticePrefix = new Stalberg.TMS.NoticePrefixDB(this.connectionString);
                Stalberg.TMS.NoticePrefixDetails npDetails = noticePrefix.GetNoticePrefixDetailsByType(ntIntNo, autIntNo);
                npIntNo = npDetails.NPIntNo;
                if (npIntNo > 0)
                {
                    //				autIntNo = npDetails.AutIntNo;
                    nPrefix = npDetails.NPrefix;
                    npIntNo = npDetails.NPIntNo;
                }
                else
                {
                    // no authority level prefix. add now
                    Stalberg.TMS.NoticePrefixDB noticePrefixAdd = new NoticePrefixDB(this.connectionString);

                    int addNPIntNo = noticePrefixAdd.AddNoticePrefix(ntIntNo, autIntNo, ntStartPrefix, user);

                    if (addNPIntNo > 0)
                    {
                        nPrefix = ntStartPrefix;
                        npIntNo = addNPIntNo;
                    }
                    else
                    {
                        noticeNumber = "0";
                        ticket.NotTicketNo = noticeNumber;
                        ticket.ErrorCode = -3;  // 2015-02-26, Oscar added. (1868, ref 1866)
                        return ticket;
                    }
                }
                //  **** Stellenbosch is using 1001 - NoticePrefix has been extended from a char(2) to a varchar(4)

                // we have the prefix 01 - 99
                // ntCode  tran		prefix
                //	1		A1		01-09 = suspended vehicles
                //	2		A2		10-49 = section 341 notice of intended prosecution does not lapse (20 years shelf life)
                //	3		A3		50-69 = section 56 notice to appear in court
                //	4		A4		70-79 = hand-written summons
                //	5		A5		80-89 = first information of crime
                //	6		A6		90-99 = computer printed section 341 camera

                Validation validation = Validation.GetInstance(this.connectionString);

                // get the next number from tranNo and increment (in stored proc)
                tnType = "A" + ntCode.ToString();
                Stalberg.TMS.TranNoDB tranNo = new Stalberg.TMS.TranNoDB(this.connectionString);

                //dls 060522 - the stored proc requires a Max No when auto-generating no's
                int maxNo = 999999;

                // jake 2012-11-19 modified ,get maxno according authority rule  9300
                string sTemplate = "99999999999".Substring(0, seqLength);

                maxNo = Convert.ToInt32(sTemplate);

                int tNumber = tranNo.GetNextTranNoByType(tnType, autIntNo, user, maxNo);

                //tnIntNo = tranNoDetails.TNIntNo;
                if (tNumber > 0)
                {
                    sNumber = tNumber.ToString().PadLeft(seqLength, cTemp);

                    // if the returned number is 999999 and seqLength=6 then we have to reset the prefix & reset the counter

                    //string sTemplate = "99999999999".Substring(0, seqLength);

                    if (sNumber == sTemplate)
                    {
                        // number is 9999...9 - reset tran no to 1
                        Stalberg.TMS.TranNoDB tranNoUpdate = new Stalberg.TMS.TranNoDB(this.connectionString);
                        int updTranNoIntNo = tranNoUpdate.UpdateTranNumber(1, user, tnIntNo);

                        if (updTranNoIntNo.Equals("-1"))
                        {
                            noticeNumber = "0";
                            ticket.NotTicketNo = noticeNumber;
                            ticket.ErrorCode = -4;  // 2015-02-26, Oscar added. (1868, ref 1866)
                            return ticket;
                        }

                        string tempPrefix = nPrefix; // still going to use this nPrefix value
                        //tempPrefix = Convert.ToString(Convert.ToInt32(tempPrefix) + 1);

                        int currentPrefixInt = validation.GetIntValue(tempPrefix) + 1;
                        tempPrefix = validation.GetCharFromNumber(currentPrefixInt, ntIntNo);

                        Stalberg.TMS.NoticePrefixDB npUpdate = new Stalberg.TMS.NoticePrefixDB(this.connectionString);
                        int updNPIntNo = npUpdate.UpdateNoticePrefix(ntIntNo, autIntNo, tempPrefix, user, npIntNo);

                        if (updNPIntNo.Equals("-1"))
                        {
                            noticeNumber = "0";
                            ticket.NotTicketNo = noticeNumber;
                            ticket.ErrorCode = -5;  // 2015-02-26, Oscar added. (1868, ref 1866)
                            return ticket;
                        }
                    }

                    // now we need the postcode
                    Stalberg.TMS.AuthorityDB postCode = new Stalberg.TMS.AuthorityDB(this.connectionString);

                    Stalberg.TMS.AuthorityDetails pcDetails = postCode.GetAuthorityDetails(autIntNo);
                    int xxIntNo = pcDetails.AutIntNo;
                    if (autIntNo == xxIntNo)
                    {
                        //autNumber = pcDetails.AutPostCode.Substring(0,3);
                        autNumber = pcDetails.AutNo.Trim().Substring(0, 3);
                    }
                    else
                    {
                        noticeNumber = "0";
                        ticket.NotTicketNo = noticeNumber;
                        ticket.ErrorCode = -6;  // 2015-02-26, Oscar added. (1868, ref 1866)
                        return ticket;
                    }

                    // now we compute the CDV!
                    cdv = Convert.ToString(validation.GetIntValue(nPrefix) + (2 * Convert.ToInt32(sNumber)) + Convert.ToInt32(autNumber));

                    if (cdvPadding)
                    {
                        cdv = cdv.PadLeft(cdvPaddingLength, cTemp);
                    }

                    noticeNumber = nPrefix + "/" + sNumber + "/" + autNumber + "/" + cdv;
                    //return noticeNumber;
                }
                else
                {
                    noticeNumber = "0";
                    //return noticeNumber;
                    ticket.ErrorCode = -7;  // 2015-02-26, Oscar added. (1868, ref 1866)
                }

                ticket.NotTicketNo = noticeNumber;

                //take the rightmost 3 digits and pad left with zeroes
                nPrefix = nPrefix.PadLeft(3, cTemp);                            //makes sure it uias at least 3 digits
                nPrefix = nPrefix.Substring(nPrefix.Length - 3, 3);             //takes the rightmost 3 digits

                autNumber = autNumber.PadLeft(3, cTemp);                        //makes sure it uias at least 3 digits
                autNumber = autNumber.Substring(autNumber.Length - 3, 3);       //takes the rightmost 3 digit

                ticket.NPrefix = nPrefix;
                ticket.SNumber = sNumber;
                ticket.AutNumber = autNumber.PadLeft(3, cTemp);

            }
            catch (Exception ex)
            {
                // 2015-02-26, Oscar added. (1868, ref 1866)
                ticket.ErrorCode = -99;
                ticket.Error = ex.ToString();
            }

            return ticket;
        }

        /// <summary>
        /// Generates the notice number_ CPI.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="autNo">The aut no.</param>
        /// <param name="camUnitID">The cam unit ID.</param>
        /// <param name="errMessage">The err message.</param>
        /// <param name="cupIntNo">The cup int no.</param>
        /// <param name="bbbbb">The BBBBB.</param>
        /// <returns></returns>
        public string GenerateNoticeNumber_CPI(string user, int autIntNo, string autNo, string camUnitID, ref string errMessage, ref int cupIntNo, ref int bbbbb)
        {
            // current method is as follows
            // AABBBBBCCCDDDDDD
            // A group = document type - e.g. A0, F1, etc [from CameraUnitPanel next available panel
            // B group = sequence number
            // C group = 3 digit authority number, made up of the 1st 3 digits of the postal code
            // D group = 6 digit cdv calculation
            //		A + (2 x B) + C = D
            // 999999 must cause the system to roll over to the next prefix number allocated to the type

            string noticeNumber = "0/0/0/0";
            int seqLength = 5; // currently sequence is 5 digits long
            int cdvLength = 6;
            string aa = "";
            //int bbbbb = 0;      //left pad this with zeroes
            string ccc = autNo.Trim().Substring(0, 3);
            int dddddd = 0;

            string A1 = "";

            //get next available doc type and seq no. for this camera unit
            CameraUnitPanelDB panel = new CameraUnitPanelDB(this.connectionString);

            SqlDataReader reader = panel.GetNextNoFromPanel(camUnitID, autNo);

            while (reader.Read())
            {
                aa = reader["CUPDocType"].ToString();
                bbbbb = Convert.ToInt32(reader["CUPNextNo"]);
                int cupPanelNo = Convert.ToInt32(reader["MinCUPPanelNo"]);
                cupIntNo = Convert.ToInt32(reader["CUPIntNo"]);

                //civitas have a range starting with 00000
                //if (aa.Equals("00") || bbbbb == 0 || cupPanelNo == 0 || cupIntNo == 0)
                if (aa.Equals("00") || cupPanelNo == 0 || cupIntNo == 0)
                {
                    //we don't have a valid panel with available numbers
                    errMessage = "No numbers available for authority " + autNo + ", camera " + camUnitID;
                    break;
                }

                A1 = aa.Substring(0, 1);
                int a1;
                int a2 = Convert.ToInt32(aa.Substring(1, 1));

                if (int.TryParse(aa, out a1))
                    //numeric
                    dddddd = ((2 * bbbbb) + Convert.ToInt32(aa) + Convert.ToInt32(ccc));
                else
                {
                    switch (A1)
                    {
                        case "A":
                            a1 = 100;
                            break;
                        case "B":
                            a1 = 110;
                            break;
                        case "C":
                            a1 = 120;
                            break;
                        case "D":
                            a1 = 130;
                            break;
                        case "E":
                            a1 = 140;
                            break;
                        case "F":
                            a1 = 150;
                            break;
                        case "G":
                            a1 = 160;
                            break;
                        case "H":
                            a1 = 170;
                            break;
                    }

                    dddddd = ((2 * bbbbb) + a1 + a2 + Convert.ToInt32(ccc));
                }

                char pad0Char = Convert.ToChar("0");

                noticeNumber = aa + "/" + bbbbb.ToString().Trim().PadLeft(seqLength, pad0Char)
                    + "/" + ccc + "/" + dddddd.ToString().Trim().PadLeft(cdvLength, pad0Char);

                break;

            }
            reader.Dispose();

            return noticeNumber;
        }

        /// <summary>
        /// Checks a notice number from Ciprus sequence number.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <param name="autNo">The authority number.</param>
        /// <returns><c>true</c> if the notice was found</returns>
        public bool CheckNoticeFromCiprusSequence(CiprusPaymentResponseFile.Record record, string autNo)
        {
            // Create a valid ticket number from the 
            string sequence = string.Format("{0}/{1}/{2}", record.NoticeNo.Substring(0, 2), record.NoticeNo.Substring(2, 5).PadLeft(5, '0'), autNo);

            // SQL objects
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "CiprusGetNoticeFromSequence";

            cmd.Parameters.Add("@Sequence", SqlDbType.VarChar, 20).Value = sequence;

            try
            {
                int x = 0;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    x++;

                    record.NotIntNo = reader.GetInt32(0);
                    record.TicketNo = reader.GetString(1);
                    record.AutIntNo = reader.GetInt32(2);
                }
                reader.Close();

                // If there were no records, or more than on returned zero the NotIntNo as a sign that the record is invalid
                if (x != 1)
                    record.NotIntNo = 0;
                else
                    return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
            finally
            {
                con.Close();
            }

            return false;
        }

        public bool CheckNoticeFromCiprusSequence(CiprusSummonsFile.Record record, string autNo)
        {
            // Create a valid ticket number from the 
            string sequence = string.Format("{0}/{1}/{2}", record.NoticeNo.Substring(0, 2), record.NoticeNo.Substring(2, 5).PadLeft(5, '0'), autNo);

            // SQL objects
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "CiprusGetNoticeFromSequence";

            cmd.Parameters.Add("@Sequence", SqlDbType.VarChar, 20).Value = sequence;

            try
            {
                int x = 0;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    x++;

                    record.NotIntNo = reader.GetInt32(0);
                    record.TicketNo = reader.GetString(1);
                    record.AutIntNo = reader.GetInt32(2);
                }
                reader.Close();

                // If there were no records, or more than on returned zero the NotIntNo as a sign that the record is invalid
                if (x != 1)
                    record.NotIntNo = 0;
                else
                    return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
            finally
            {
                con.Close();
            }

            return false;
        }

    }
}