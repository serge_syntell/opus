﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    public partial class TransactionTypeDetails
    {//TTIntNo,TTCode,TTDescription,TTPrefix,LastUser
        public Int32 TTIntNo;
        public string TTCode;
        public string TTDescription;
        public string TTPrefix;
        public string LastUser;
    }
    public partial class TransactionTypeDB
    {
        string mConstr = "";
        public TransactionTypeDB(string vConstr)
        {
            mConstr = vConstr;
        }
        public DataSet GetTransactionTypeListDS()
        {
            SqlDataAdapter sqlDATransactionTypes = new SqlDataAdapter();
            DataSet dsTransactionTypes = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDATransactionTypes.SelectCommand = new SqlCommand();
            sqlDATransactionTypes.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDATransactionTypes.SelectCommand.CommandText = "TransactionTypeList";

            // Mark the Command as a SPROC
            sqlDATransactionTypes.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command and close the connection
            sqlDATransactionTypes.Fill(dsTransactionTypes);
            sqlDATransactionTypes.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsTransactionTypes;
        }

        public TransactionTypeDetails GetTransactionTypeDetails(int ttIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("TransactionTypeDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterTTIntNo = new SqlParameter("@TTIntNo", SqlDbType.Int);
            parameterTTIntNo.Value = ttIntNo;
            myCommand.Parameters.Add(parameterTTIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            TransactionTypeDetails myTransactionTypeDetails = new TransactionTypeDetails();

            while (result.Read())
            {//TTIntNo,TTCode,TTDescription,TTPrefix,LastUser
                // Populate Struct using Output Params from SPROC
                myTransactionTypeDetails.TTIntNo = Convert.ToInt32(result["TTIntNo"]);
                myTransactionTypeDetails.TTCode = result["TTCode"].ToString();
                myTransactionTypeDetails.TTDescription = result["TTDescription"].ToString();
                myTransactionTypeDetails.TTPrefix = result["TTPrefix"].ToString();
                myTransactionTypeDetails.LastUser = result["LastUser"].ToString();
            }
            result.Close();
            return myTransactionTypeDetails;
        }
        public int UpdateTransactionType(TransactionTypeDetails tranType)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("TransactionTypeUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            // Add Parameters to SPROC
            SqlParameter parameterTTCode = new SqlParameter("@TTCode", SqlDbType.VarChar, 30);
            parameterTTCode.Value = tranType.TTCode;
            myCommand.Parameters.Add(parameterTTCode);

            SqlParameter parameterTTDescription = new SqlParameter("@TTDescription", SqlDbType.VarChar, 100);
            parameterTTDescription.Value = tranType.TTDescription;
            myCommand.Parameters.Add(parameterTTDescription);

            SqlParameter parameterTTPrefix = new SqlParameter("@TTPrefix", SqlDbType.VarChar,3);
            parameterTTPrefix.Value = tranType.TTPrefix;
            myCommand.Parameters.Add(parameterTTPrefix);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = tranType.LastUser;
            myCommand.Parameters.Add(parameterLastUser);


            SqlParameter parameterTTIntNo = new SqlParameter("@TTIntNo", SqlDbType.Int);
            parameterTTIntNo.Value = tranType.TTIntNo;
            parameterTTIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterTTIntNo);
            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                tranType.TTIntNo = Convert.ToInt32(myCommand.Parameters["@TTIntNo"].Value);

                return tranType.TTIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }
    }
}
