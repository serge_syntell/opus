using System;
using System.Collections.Generic;
using System.Text;
using Stalberg.TMS_TPExInt.Objects;
using Stalberg.TMS;

namespace Stalberg.ThaboAggregator.Objects
{
    /// <summary>
    /// Represents an authority that has details for sending fine exchange feedback data files back to it.
    /// (i.e. it has Machine IP, User Name, and Password configured for it in Thabo)
    /// </summary>
    public class RemoteAuthority : Authority
    {
        // Fields
        private string machineName = string.Empty;
        private string autCode = string.Empty;
        private int receiverIdentifier = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="RemoteAuthority"/> class.
        /// </summary>
        public RemoteAuthority()
        {
        }

        /// <summary>
        /// Gets a value indicating whether this instance has a valid remote machine setup.
        /// </summary>
        /// <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
        public bool IsEasyPay
        {
            get { return (this.receiverIdentifier > 0); }
        }

        /// <summary>
        /// Gets or sets the unique name of the remote server.
        /// </summary>
        /// <value>The name of the machine.</value>
        public string MachineName
        {
            get { return this.machineName; }
            set { this.machineName = value; }
        }

        /// <summary>
        /// Gets or sets the EasyPay receiver Identifier.
        /// </summary>
        /// <value>The receipt ID.</value>
        public int ReceiptID
        {
            get { return this.receiverIdentifier; }
            set { this.receiverIdentifier = value; }
        }

        /// <summary>
        /// Gets or sets the authority code.
        /// </summary>
        /// <value>The authority code.</value>
        public string AuthorityCode
        {
            get { return this.autCode; }
            set { this.autCode = value; }
        }

    }
}
