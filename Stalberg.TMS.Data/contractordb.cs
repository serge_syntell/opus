using System;
using System.Data;
using System.Data.SqlClient;


namespace Stalberg.TMS
{

    //*******************************************************
    //
    // ContractorDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class ContractorDetails
    {
        public Int32 ConIntNo;
        public string ConCode;
        public string ConName;
        public string ConSupportSName;
        public string ConSupportFName;
        public string ConSupportEmail;
        public string ConFTPSite;
        public string ConFTPFolder;
        public string ConFTPLogin;
        public string ConFTPPassword;
    }

    //*******************************************************
    //
    // ContractorDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query Contractors within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class ContractorDB
    {

        string mConstr = "";

        public ContractorDB(string vConstr)
        {
            mConstr = vConstr;
        }

        //*******************************************************
        //
        // ContractorDB.GetContractorDetails() Method <a name="GetContractorDetails"></a>
        //
        // The GetContractorDetails method returns a ContractorDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public ContractorDetails GetContractorDetails(int conIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ContractorDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterConIntNo = new SqlParameter("@ConIntNo", SqlDbType.Int, 4);
            parameterConIntNo.Value = conIntNo;
            myCommand.Parameters.Add(parameterConIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            ContractorDetails myContractorDetails = new ContractorDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myContractorDetails.ConName = result["ConName"].ToString();
                myContractorDetails.ConCode = result["ConCode"].ToString();
                myContractorDetails.ConSupportSName = result["ConSupportSName"].ToString();
                myContractorDetails.ConSupportFName = result["ConSupportFName"].ToString();
                myContractorDetails.ConSupportEmail = result["ConSupportEmail"].ToString();
                myContractorDetails.ConFTPSite = result["ConFTPSite"].ToString();
                myContractorDetails.ConFTPFolder = result["ConFTPFolder"].ToString();
                myContractorDetails.ConFTPLogin = result["ConFTPLogin"].ToString();
                myContractorDetails.ConFTPPassword = result["ConFTPPassword"].ToString();
            }
            result.Close();
            return myContractorDetails;
        }

        public ContractorDetails GetContractorDetailsByConCode(string conCode)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ContractorDetailByConCode", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterConCode = new SqlParameter("@ConCode", SqlDbType.Char, 2);
            parameterConCode.Value = conCode;
            myCommand.Parameters.Add(parameterConCode);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            ContractorDetails myContractorDetails = new ContractorDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myContractorDetails.ConIntNo = Convert.ToInt32(result["ConIntNo"]);
                myContractorDetails.ConName = result["ConName"].ToString();
                myContractorDetails.ConCode = result["ConCode"].ToString();
                myContractorDetails.ConSupportSName = result["ConSupportSName"].ToString();
                myContractorDetails.ConSupportFName = result["ConSupportFName"].ToString();
                myContractorDetails.ConSupportEmail = result["ConSupportEmail"].ToString();
                myContractorDetails.ConFTPSite = result["ConFTPSite"].ToString();
                myContractorDetails.ConFTPFolder = result["ConFTPFolder"].ToString();
                myContractorDetails.ConFTPLogin = result["ConFTPLogin"].ToString();
                myContractorDetails.ConFTPPassword = result["ConFTPPassword"].ToString();
            }
            result.Close();
            return myContractorDetails;
        }

        //*******************************************************
        //
        // ContractorDB.AddContractor() Method <a name="AddContractor"></a>
        //
        // The AddContractor method inserts a new menu record
        // into the menus database.  A unique "ContractorId"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddContractor(string conCode, string conName, string conSupportSName, string conSupportFName, 
            string conSupportEmail, string conFTPSite,  string conFTPFolder, string conFTPLogin, string conFTPPassword, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ContractorAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC

            SqlParameter parameterConName = new SqlParameter("@ConName", SqlDbType.VarChar, 30);
            parameterConName.Value = conName;
            myCommand.Parameters.Add(parameterConName);

            SqlParameter parameterConSupportSName = new SqlParameter("@ConSupportSName", SqlDbType.VarChar, 20);
            parameterConSupportSName.Value = conSupportSName;
            myCommand.Parameters.Add(parameterConSupportSName);

            SqlParameter parameterConSupportFName = new SqlParameter("@ConSupportFName", SqlDbType.VarChar, 20);
            parameterConSupportFName.Value = conSupportFName;
            myCommand.Parameters.Add(parameterConSupportFName);

            SqlParameter parameterConSupportEmail = new SqlParameter("@ConSupportEmail", SqlDbType.VarChar, 100);
            parameterConSupportEmail.Value = conSupportEmail;
            myCommand.Parameters.Add(parameterConSupportEmail);

            SqlParameter parameterConFTPSite = new SqlParameter("@ConFTPSite", SqlDbType.VarChar, 100);
            parameterConFTPSite.Value = conFTPSite;
            myCommand.Parameters.Add(parameterConFTPSite);

            SqlParameter parameterConFTPFolder = new SqlParameter("@ConFTPFolder", SqlDbType.VarChar, 100);
            parameterConFTPFolder.Value = conFTPFolder;
            myCommand.Parameters.Add(parameterConFTPFolder);

            SqlParameter parameterConFTPLogin = new SqlParameter("@ConFTPLogin", SqlDbType.VarChar, 20);
            parameterConFTPLogin.Value = conFTPLogin;
            myCommand.Parameters.Add(parameterConFTPLogin);

            SqlParameter parameterConFTPPassword = new SqlParameter("@ConFTPPassword", SqlDbType.VarChar, 50);
            parameterConFTPPassword.Value = conFTPPassword;
            myCommand.Parameters.Add(parameterConFTPPassword);

            SqlParameter parameterConCode = new SqlParameter("@ConCode", SqlDbType.VarChar, 3);
            parameterConCode.Value = conCode;
            myCommand.Parameters.Add(parameterConCode);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterConIntNo = new SqlParameter("@ConIntNo", SqlDbType.Int, 4);
            parameterConIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterConIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int conIntNo = Convert.ToInt32(parameterConIntNo.Value);

                return conIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public SqlDataReader GetContractorList()
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ContractorList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }


        public DataSet GetContractorListDS()
        {
            SqlDataAdapter sqlDAContractors = new SqlDataAdapter();
            DataSet dsContractors = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAContractors.SelectCommand = new SqlCommand();
            sqlDAContractors.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAContractors.SelectCommand.CommandText = "ContractorList";

            // Mark the Command as a SPROC
            sqlDAContractors.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command and close the connection
            sqlDAContractors.Fill(dsContractors);
            sqlDAContractors.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsContractors;
        }


        public int UpdateContractor(int conIntNo, string conCode, string conName, string conSupportSName, string conSupportFName,
            string conSupportEmail, string conFTPSite, string conFTPFolder, string conFTPLogin, string conFTPPassword, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ContractorUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterConName = new SqlParameter("@ConName", SqlDbType.VarChar, 30);
            parameterConName.Value = conName;
            myCommand.Parameters.Add(parameterConName);

            SqlParameter parameterConCode = new SqlParameter("@ConCode", SqlDbType.VarChar, 3);
            parameterConCode.Value = conCode;
            myCommand.Parameters.Add(parameterConCode);

            SqlParameter parameterConSupportSName = new SqlParameter("@ConSupportSName", SqlDbType.VarChar, 20);
            parameterConSupportSName.Value = conSupportSName;
            myCommand.Parameters.Add(parameterConSupportSName);

            SqlParameter parameterConSupportFName = new SqlParameter("@ConSupportFName", SqlDbType.VarChar, 20);
            parameterConSupportFName.Value = conSupportFName;
            myCommand.Parameters.Add(parameterConSupportFName);

            SqlParameter parameterConSupportEmail = new SqlParameter("@ConSupportEmail", SqlDbType.VarChar, 100);
            parameterConSupportEmail.Value = conSupportEmail;
            myCommand.Parameters.Add(parameterConSupportEmail);

            SqlParameter parameterConFTPSite = new SqlParameter("@ConFTPSite", SqlDbType.VarChar, 100);
            parameterConFTPSite.Value = conFTPSite;
            myCommand.Parameters.Add(parameterConFTPSite);

            SqlParameter parameterConFTPFolder = new SqlParameter("@ConFTPFolder", SqlDbType.VarChar, 100);
            parameterConFTPFolder.Value = conFTPFolder;
            myCommand.Parameters.Add(parameterConFTPFolder);

            SqlParameter parameterConFTPLogin = new SqlParameter("@ConFTPLogin", SqlDbType.VarChar, 20);
            parameterConFTPLogin.Value = conFTPLogin;
            myCommand.Parameters.Add(parameterConFTPLogin);

            SqlParameter parameterConFTPPassword = new SqlParameter("@ConFTPPassword", SqlDbType.VarChar, 50);
            parameterConFTPPassword.Value = conFTPPassword;
            myCommand.Parameters.Add(parameterConFTPPassword);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterConIntNo = new SqlParameter("@ConIntNo", SqlDbType.Int);
            parameterConIntNo.Direction = ParameterDirection.InputOutput;
            parameterConIntNo.Value = conIntNo;
            myCommand.Parameters.Add(parameterConIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int ConIntNo = (int)myCommand.Parameters["@ConIntNo"].Value;
                //int menuId = (int)parameterConIntNo.Value;

                return ConIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }


        public String DeleteContractor(int conIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ContractorDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterConIntNo = new SqlParameter("@ConIntNo", SqlDbType.Int, 4);
            parameterConIntNo.Value = conIntNo;
            parameterConIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterConIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int ConIntNo = (int)parameterConIntNo.Value;

                return ConIntNo.ToString();
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return String.Empty;
            }
        }
    }
}


