﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Stalberg.TMS.Data
{
    [Serializable]
    public class JudgementSnapshot
    {
        public CourtDates CourtDates { get; set; }
        public Notice Notices { get; set; }
        public List<Notice_Summons> NoticeSummonsList { get; set; }
        public Summons Summons { get; set; }
        public List<Charge> Charges { get; set; }
        public List<Charge_SumCharge> ChargeSumCharges { get; set; }
        public List<SumCharge> SumCharges { get; set; }

        public void AddNoticeSummons(Notice_Summons noticeSummons)
        {
            if (NoticeSummonsList == null)
                NoticeSummonsList = new List<Notice_Summons>();

            if (NoticeSummonsList.Contains(noticeSummons) == false)
                NoticeSummonsList.Add(noticeSummons);
        }

        public void RemoveNoticeSummons(Notice_Summons noticeSummons)
        {
            if (NoticeSummonsList == null)
                NoticeSummonsList = new List<Notice_Summons>();

            if (NoticeSummonsList.Contains(noticeSummons))
                NoticeSummonsList.Remove(noticeSummons);
        }

        public void AddCharge(Charge charge)
        {
            if (Charges == null)
                Charges = new List<Charge>();

            if (Charges.Contains(charge) == false)
                Charges.Add(charge);
        }

        public void RemoveCharge(Charge charge)
        {
            if (Charges == null)
                Charges = new List<Charge>();

            if (Charges.Contains(charge))
                Charges.Remove(charge);
        }

        public void AddChargeSumCharge(Charge_SumCharge chargeSumCharge)
        {
            if (ChargeSumCharges == null)
                ChargeSumCharges = new List<Charge_SumCharge>();

            if (ChargeSumCharges.Contains(chargeSumCharge) == false)
                ChargeSumCharges.Add(chargeSumCharge);
        }

        public void RemoveChargeSumCharge(Charge_SumCharge chargeSumCharge)
        {
            if (ChargeSumCharges == null)
                ChargeSumCharges = new List<Charge_SumCharge>();

            if (ChargeSumCharges.Contains(chargeSumCharge))
                ChargeSumCharges.Remove(chargeSumCharge);
        }

        public void AddSumCharge(SumCharge sumCharge)
        {
            if (SumCharges == null)
                SumCharges = new List<SumCharge>();

            if (SumCharges.Contains(sumCharge) == false)
                SumCharges.Add(sumCharge);
        }

        public void RemoveSumCharge(SumCharge sumCharge)
        {
            if (SumCharges == null)
                SumCharges = new List<SumCharge>();

            if (SumCharges.Contains(sumCharge))
                SumCharges.Remove(sumCharge);
        }
    }

    [Serializable]
    public class CourtDates
    {
        public int AutIntNo { get; set; }
        public int CrtRIntNo { get; set; }
        public DateTime CDate { get; set; }
    }

    [Serializable]
    public class Summons
    {
        public int SumIntNo { get; set; }
        public string SumStatus { get; set; }

        public int SumChargeStatus { get; set; }
        public bool SumLocked { get; set; }
        public string SumCancelled { get; set; }
        public bool SumDateCancelled { get; set; }
        public DateTime? SumReasonCancelled { get; set; }
        public int SummonsStatus { get; set; }
        public DateTime? SumPaidDate { get; set; }

    }

    [Serializable]
    public class Notice
    {
        public int NotIntNo { get; set; }
        public int NoticeStatus { get; set; }
    }

    [Serializable]
    public class Notice_Summons
    {
        public int NotSumIntNo { get; set; }
        public int NotIntNo { get; set; }
        public int SumIntNo { get; set; }
        public string LastUser { get; set; }
    }

    [Serializable]
    public class Accused
    {
        public int AccIntNo { get; set; }
    }

    [Serializable]
    public class Charge
    {
        public int ChgIntNo { get; set; }
        public int NotIntNo { get; set; }
        public int CTIntNo { get; set; }
        public string ChgOffenceType { get; set; }
        public string ChgOffenceCode { get; set; }
        public int ChgFineAmount { get; set; }
        public string ChgFineAlloc { get; set; }
        public string LastUser { get; set; }
        public string ChgStatutoryRef { get; set; }
        public string ChgStatRefLine1 { get; set; }
        public string ChgStatRefLine2 { get; set; }
        public string ChgStatRefLine3 { get; set; }
        public string ChgStatRefLine4 { get; set; }
        public string ChgOffenceDescr { get; set; }
        public string ChgOffenceDescr2 { get; set; }
        public string ChgOffenceDescr3 { get; set; }
        public int ChargeStatus { get; set; }
        public DateTime? ChgPaidDate { get; set; }
        public string ChgNoAOG { get; set; }
        public decimal ChgRevFineAmount { get; set; }
        public decimal ChgRevDiscountAmount { get; set; }
        public decimal ChgRevDiscountedAmount { get; set; }
        public int PreviousStatus { get; set; }
        public int ChgContemptCourt { get; set; }
        public string ChgIssueAuthority { get; set; }
        public string ChgOfficerNatisNo { get; set; }
        public string ChgOfficerName { get; set; }
        public decimal ChgDiscountAmount { get; set; }
        public decimal ChgDiscountedAmount { get; set; }
        public string ChgType { get; set; }
        public int ChgDemeritPoints { get; set; }
        public string ChgLegislation { get; set; }
        public string ChgOffenceTypeOld { get; set; }
        public bool ChgIsMain { get; set; }
        public int ChgSequence { get; set; }
        public int? MainChargeID { get; set; }
    }

    [Serializable]
    public class Charge_SumCharge
    {
        public int ChgSChIntNo { get; set; }
        public int ChgIntNo { get; set; }
        public int SChIntNo { get; set; }
        public string LastUser { get; set; }
    }

    [Serializable]
    public class SumCharge
    {
        public int SChIntNo { get; set; }
        public int SumIntNo { get; set; }
        public string SChStatRef { get; set; }
        public string SChNumber { get; set; }
        public string SChCode { get; set; }
        public string SChType { get; set; }
        public string SChDescr { get; set; }
        public string SChOcTDescr { get; set; }
        public string SChOctDescr1 { get; set; }
        public decimal SChFineAmount { get; set; }
        public decimal SChRevAmount { get; set; }
        public DateTime? SChCourtJudgementLoaded { get; set; }
        public decimal SChSentenceAmount { get; set; }
        public decimal SChContemptAmount { get; set; }
        public decimal SChOtherAmount { get; set; }
        public decimal SChPaidAmount { get; set; }
        public int CJTIntNo { get; set; }
        public string SChVerdict { get; set; }
        public string SChSentence { get; set; }
        public string SChNoAOG { get; set; }
        public string SChWOALoaded { get; set; }
        public string LastUser { get; set; }
        public DateTime? GraceExpiryDate { get; set; }
        public string SChDescrAfr { get; set; }
        public bool SchIsMain { get; set; }
        public int SChSequence { get; set; }
        public int SumChargeStatus { get; set; }
        public int? SChPrevSumChargeStatus { get; set; }
        public int? MainSumChargeID { get; set; }
    }
}
