using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // EvidencePackDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public class EvidencePackDetails
    {
        public int nEPIntNo;
        public int nChgIntNo;
        public DateTime dtEPItemDate;
        public string sEPItemDescr;
        public string sEPSourceTable;
        public int nEPSourceID;
    }

    //*******************************************************
    //
    // EvidencePackDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query EvidencePackDetails 
    //
    //*******************************************************

    public class EvidencePackDB
    {
        // Fields
        private readonly string connectionString = "";

        public EvidencePackDB(string vConstr)
        {
            connectionString = vConstr;
        }

        //*******************************************************
        //
        // EvidencePackDB.GetDetails() Method <a name="GetDetails"></a>
        //
        // The GetDetails method returns a GetDetails
        //
        //*******************************************************
        // 2013-07-19 comment by Henry for useless
        //public EvidencePackDetails GetDetails(int nEpIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(connectionString);
        //    SqlCommand cmd = new SqlCommand("GetEvidencePackDetail", con);

        //    // Mark the Command as a SPROC
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC

        //    con.Open();
        //    SqlDataReader result = cmd.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Create CustomerDetails Struct
        //    EvidencePackDetails EPDet = new EvidencePackDetails();
        //    EPDet.nEPIntNo = 0;

        //    while (result.Read())
        //    {
        //        EPDet.nEPIntNo = Convert.ToInt32(result["EPIntNo"]);
        //        if (EPDet.nEPIntNo == -1)
        //        {
        //            result.Close();
        //            cmd.Dispose();
        //            return EPDet;
        //        }
        //        // Populate Struct using Output Params from SPROC
        //        EPDet.nChgIntNo = Convert.ToInt32(result["ChgIntNo"]);
        //        EPDet.nEPSourceID = Convert.ToInt32(result["EPSourceID"]);
        //        EPDet.sEPItemDescr = result["EPItemDescr"].ToString();
        //        EPDet.sEPSourceTable = result["EPSourceTable"].ToString();
        //        if (result["EPitemDate"] != DBNull.Value)
        //            DateTime.TryParse(result["EPitemDate"].ToString(), out EPDet.dtEPItemDate);
        //    }
        //    result.Close();
        //    cmd.Dispose();
        //    return EPDet;
        //}

        //*******************************************************
        //
        // EvidencePackDB.Add() Method <a name="Add"></a>
        //
        // The Add method inserts a new record
        //
        //*******************************************************
        public int Add(int nChgIntNo, DateTime dtInDate, string sItemDescr, string sSourceTable, int nSourceId, string sUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("EvidencePackAdd", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@ChgIntNo", SqlDbType.Int, 4).Value = nChgIntNo;
            cmd.Parameters.Add("@EPItemDate", SqlDbType.DateTime).Value = dtInDate;
            cmd.Parameters.Add("@EPItemDescr", SqlDbType.VarChar, 100).Value = sItemDescr;
            cmd.Parameters.Add("@EPSourceTable", SqlDbType.VarChar, 50).Value = sSourceTable;
            cmd.Parameters.Add("@EPSourceID", SqlDbType.Int, 4).Value = nSourceId;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = sUser;

            try
            {
                con.Open();
                int nResult = cmd.ExecuteNonQuery();
                return nResult;
            }
            finally
            {
                con.Close();
                con.Dispose();
                cmd.Dispose();
            }
        }

    }
}


