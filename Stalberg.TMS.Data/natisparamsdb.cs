using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    /// <summary>
    /// Summary description for NatisParamsDB
    /// </summary>
    public class NatisParamsDB
    {
        /// <summary>
        /// Represents the NATIS parameter data
        /// </summary>
        public class NatisParamDetails
        {
            public int nNpIntNo;
            public string sTerminalID;
            public string sSendToNatis;
            public string sSendNatisEmail;
            public string sGetFromNatis;
            public int nStartRunTime;
            public int nInterval;
            public int nNoRuns;
        }

         // Fields
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public NatisParamsDB(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Gets the notice audit results.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        public DataSet GetNatisParams(int autIntNo)
        {
            SqlDataAdapter sa = new SqlDataAdapter();
            DataSet ds = new DataSet();
            sa.SelectCommand = new SqlCommand();
            sa.SelectCommand.Connection = new SqlConnection(connectionString);
            sa.SelectCommand.CommandText = "GetNatisParams";

            // Mark the Command as a SPROC
            sa.SelectCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter prm = new SqlParameter("@AutIntNo", SqlDbType.Int);
            prm.Direction = ParameterDirection.Input;
            prm.Value = autIntNo;
            sa.SelectCommand.Parameters.Add(prm);

            // Execute the command and close the connection
            sa.Fill(ds);
            sa.SelectCommand.Connection.Dispose();

            return ds;
        }

         /// <summary>
        /// Gets the notice audit results.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        public NatisParamDetails GetNatisParamsDetails(int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("GetNatisParamsDetails", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterIntNo);
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            NatisParamDetails myDetails = new NatisParamDetails();
            myDetails.nNpIntNo  = 0;

            while (result.Read())
            {
                myDetails.nNpIntNo = Convert.ToInt32(result["NPIntNo"]);
                if (myDetails.nNpIntNo == -1)
                {
                    result.Close();
                    myCommand.Dispose();
                    return myDetails;
                }
                // Populate Struct using Output Params from SPROC
                myDetails.sTerminalID = result["NPTerminalID"].ToString();
                myDetails.sSendToNatis  = result["NPSendNatisFolder"].ToString ();
                myDetails.sSendNatisEmail  = result["NPSendNatisEmail"].ToString ();
                myDetails.sGetFromNatis  = result["NPGetNatisFolder"].ToString();
                myDetails.nStartRunTime = Convert.ToInt32( result["NPStartRunTime"].ToString());
                myDetails.nInterval  = Convert.ToInt32 (result["NPInterval"].ToString());
                myDetails.nNoRuns = Convert.ToInt32(result["NPNumberOfRuns"].ToString());
            }
            result.Close();
            myCommand.Dispose();

            return myDetails;
        }

        public int UpdateNatisParams(int nNPIntNo, string sTerminalID, string sSendNatisFolder, string sSendNatisEmail, string sGetNatisFolder, int nStartTime, int nInterval, int nNoRuns,string sCurrent,string sUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("UpdateNatisParams", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter prmNPIntNo = new SqlParameter("@NPIntNo", SqlDbType.Int, 4);
            prmNPIntNo.Direction = ParameterDirection.InputOutput;
            prmNPIntNo.Value = nNPIntNo;
            myCommand.Parameters.Add(prmNPIntNo);

            SqlParameter prmA = new SqlParameter("@NPTerminalID", SqlDbType.Char, 10);
            prmA.Value = sTerminalID;
            myCommand.Parameters.Add(prmA);

            SqlParameter prmB = new SqlParameter("@NPSendNatisFolder", SqlDbType.VarChar, 50);
            prmB.Value = sSendNatisFolder;
            myCommand.Parameters.Add(prmB);

            SqlParameter prmC = new SqlParameter("@NPSendNatisEmail", SqlDbType.VarChar, 50);
            prmC.Value = sSendNatisEmail;
            myCommand.Parameters.Add(prmC);

            SqlParameter prmD = new SqlParameter("@NPGetNatisFolder", SqlDbType.VarChar, 50);
            prmD.Value = sGetNatisFolder;
            myCommand.Parameters.Add(prmD);

            SqlParameter prmE = new SqlParameter("@NPStartRunTime", SqlDbType.Int);
            prmE.Value = nStartTime;
            myCommand.Parameters.Add(prmE);

            SqlParameter prmF = new SqlParameter("@NPInterval", SqlDbType.Int);
            prmF.Value = nInterval;
            myCommand.Parameters.Add(prmF);

            SqlParameter prmG = new SqlParameter("@NPNumberOfRuns", SqlDbType.Int);
            prmG.Value = nNoRuns ;
            myCommand.Parameters.Add(prmG);

            SqlParameter prmH = new SqlParameter("@Current", SqlDbType.VarChar, 1);
            prmH.Value = sCurrent;
            myCommand.Parameters.Add(prmH);

            SqlParameter prmI = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            prmI.Value = sUser;
            myCommand.Parameters.Add(prmI);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                int nRes = (int)myCommand.Parameters["@NPIntNo"].Value;

                return nRes;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e.Message);

                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        /// <summary>
        /// Adds the natis params.
        /// </summary>
        /// <param name="nAutIntNo">The n aut int no.</param>
        /// <param name="sTerminalID">The s terminal ID.</param>
        /// <param name="sSendNatisFolder">The s send natis folder.</param>
        /// <param name="sSendNatisEmail">The s send natis email.</param>
        /// <param name="sGetNatisFolder">The s get natis folder.</param>
        /// <param name="nStartTime">The n start time.</param>
        /// <param name="nInterval">The n interval.</param>
        /// <param name="nNoRuns">The n no runs.</param>
        /// <param name="sCurrent">The s current.</param>
        /// <param name="sUser">The s user.</param>
        /// <returns></returns>
        public int AddNatisParams(int nAutIntNo, string sTerminalID, string sSendNatisFolder, string sSendNatisEmail, string sGetNatisFolder, int nStartTime, int nInterval, int nNoRuns, string sCurrent,string sUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("AddNatisParams", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter prmAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            prmAutIntNo.Value = nAutIntNo;
            myCommand.Parameters.Add(prmAutIntNo);

            SqlParameter prmA = new SqlParameter("@NPTerminalID", SqlDbType.Char, 10);
            prmA.Value = sTerminalID;
            myCommand.Parameters.Add(prmA);

            SqlParameter prmB = new SqlParameter("@NPSendNatisFolder", SqlDbType.VarChar, 50);
            prmB.Value = sSendNatisFolder;
            myCommand.Parameters.Add(prmB);

            SqlParameter prmC = new SqlParameter("@NPSendNatisEmail", SqlDbType.VarChar, 50);
            prmC.Value = sSendNatisEmail;
            myCommand.Parameters.Add(prmC);

            SqlParameter prmD = new SqlParameter("@NPGetNatisFolder", SqlDbType.VarChar, 50);
            prmD.Value = sGetNatisFolder;
            myCommand.Parameters.Add(prmD);

            SqlParameter prmE = new SqlParameter("@NPStartRunTime", SqlDbType.Int);
            prmE.Value = nStartTime ;
            myCommand.Parameters.Add(prmE);

            SqlParameter prmF = new SqlParameter("@NPInterval", SqlDbType.Int);
            prmF.Value = nInterval;
            myCommand.Parameters.Add(prmF);

            SqlParameter prmG = new SqlParameter("@NPNumberOfRuns", SqlDbType.Int);
            prmG.Value = nNoRuns;
            myCommand.Parameters.Add(prmG);

            SqlParameter prmH = new SqlParameter("@Current", SqlDbType.VarChar, 1);
            prmH.Value = sCurrent;
            myCommand.Parameters.Add(prmH);

            SqlParameter prmI = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            prmI.Value = sUser;
            myCommand.Parameters.Add(prmI);

            SqlParameter prmNPIntNo = new SqlParameter("@NPIntNo", SqlDbType.Int, 4);
            prmNPIntNo.Direction = ParameterDirection.InputOutput;
            prmNPIntNo.Value = 0;
            myCommand.Parameters.Add(prmNPIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                int nRes = (int)myCommand.Parameters["@NPIntNo"].Value;

                return nRes;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e.Message);

                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        /// <summary>
        /// Deletes the natis params.
        /// </summary>
        /// <param name="nNPIntNo">The n NP int no.</param>
        /// <param name="nAutIntNo">The n aut int no.</param>
        /// <param name="sCurrent">The s current.</param>
        /// <returns></returns>
        // 2013-07-25 add parameter lastUser by Henry
        public int DeleteNatisParams(int nNPIntNo , int nAutIntNo,  string sCurrent, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("DeleteNatisParams", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter prmAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            prmAutIntNo.Value = nAutIntNo;
            myCommand.Parameters.Add(prmAutIntNo);

            SqlParameter prmH = new SqlParameter("@Current", SqlDbType.VarChar, 1);
            prmH.Value = sCurrent;
            myCommand.Parameters.Add(prmH);

            SqlParameter prmNPIntNo = new SqlParameter("@NPIntNo", SqlDbType.Int, 4);
            prmNPIntNo.Direction = ParameterDirection.InputOutput;
            prmNPIntNo.Value = nNPIntNo;
            myCommand.Parameters.Add(prmNPIntNo);

            SqlParameter prmLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            prmLastUser.Value = lastUser;
            myCommand.Parameters.Add(prmLastUser);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                int nRes = (int)myCommand.Parameters["@NPIntNo"].Value;

                return nRes;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e.Message);

                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

    }
}

