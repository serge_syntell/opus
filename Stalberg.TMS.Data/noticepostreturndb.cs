using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // NoticePostReturnDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************
    public class NoticePostReturnDetails
    {
        public int nNotIntNo;
        public int nPRTIntNo;
        public DateTime dtNPRRDate;
        public string sNPRRUser;
        public string sNPRRBoxNo;
        public string sNPRRStorageLocation;
        public int nNPRRIntNo;
    }

    //*******************************************************
    //
    // NoticePostReturnDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query NoticePostReturnDB within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class NoticePostReturnDB
    {

        string mConstr = "";

        public NoticePostReturnDB(string vConstr)
        {
            mConstr = vConstr;
        }

        //*******************************************************
        //
        // NoticePostReturnDB.GetNoticePostReturnDetails() Method <a name="GetNoticePostReturnDetails"></a>
        //
        // The GetNoticePostReturnDetails method returns a NoticePostReturnDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public NoticePostReturnDetails GetNoticePostReturnDetails(string sTicketNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("GetNoticePostReturnDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@TicketNo", SqlDbType.VarChar, 50);
            parameterNotIntNo.Value = sTicketNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            NoticePostReturnDetails myDetails = new NoticePostReturnDetails();
            myDetails.nNPRRIntNo = 0;

            while (result.Read())
            {
                myDetails.nNPRRIntNo = Convert.ToInt32(result["NPRRIntNo"]);
                if (myDetails.nNPRRIntNo == -1)
                {
                    result.Close();
                    myCommand.Dispose();
                    return myDetails;
                }
                // Populate Struct using Output Params from SPROC
                myDetails.nNotIntNo = Convert.ToInt32(result["NotIntNo"]);
                myDetails.nPRTIntNo = Convert.ToInt32(result["PRTIntNo"]);
                myDetails.sNPRRBoxNo = result["NPRRBoxNo"].ToString();
                myDetails.sNPRRStorageLocation = result["NPRRStorageLocation"].ToString();
                myDetails.sNPRRUser = result["NPRRUser"].ToString();
                if (result["NPRRDate"] != DBNull.Value)
                    DateTime.TryParse(result["NPRRDate"].ToString(), out myDetails.dtNPRRDate);
            }
            result.Close();
            myCommand.Dispose();
            return myDetails;
        }

        //*******************************************************
        //
        // NoticePostReturnDB.GetNoticePostReturnByValues() Method <a name="GetNoticePostReturnByValues"></a>
        //
        // The GetNoticePostReturnByValues method returns a NoticePostReturnDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public SqlDataReader GetNoticePostReturnByValues(string sTicketNo, string sRegNo, string sName, string sBoxNo, string sLocation, DateTime dtDate)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("GetNoticePostReturnByValues", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = sTicketNo;
            myCommand.Parameters.Add("@RegNo", SqlDbType.VarChar, 20).Value = sRegNo;
            myCommand.Parameters.Add("@Name", SqlDbType.VarChar, 20).Value = sName;
            myCommand.Parameters.Add("@BoxNo", SqlDbType.VarChar, 10).Value = sBoxNo;
            myCommand.Parameters.Add("@Location", SqlDbType.VarChar, 25).Value = sLocation;
            myCommand.Parameters.Add("@InDate", SqlDbType.DateTime).Value = dtDate;
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            myCommand.Dispose();

            return result;
        }

        //*******************************************************
        //
        // NoticePostReturnDB.AddPostReturn() Method <a name="AddPostReturn"></a>
        //
        // The AddPostReturn method inserts a new menu record
        // into the menus database.  A unique "AddPostReturnID"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddPostReturn(string sTicketNo, int nNotIntNo, int nPRTIntNo, DateTime dtInDate, string sUser, string sBoxNo, string sStorageLocation)
        {
            /* Edge 20130710 Comment out
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("UpdatePostInvalidFlag", myConnection);

            //// Mark the Command as a SPROC
            //myCommand.CommandType = CommandType.StoredProcedure;

            //// Added this to update the MIPoAddInvalid field - which is really setting the Post Address Invalid flag
            //// in MI5_Persona with the given ticket number
            //try
            //{
            //    myCommand.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = sTicketNo;
            //    myConnection.Open();
            //    int nResult = myCommand.ExecuteNonQuery();
            //    myCommand.Dispose();
            //    myConnection.Dispose();
            //}
            //catch (Exception ex)
            //{
            //    // Oh dear - Houston we have a problem
            //    Console.WriteLine("Error " + ex.Message);
            //    myCommand.Dispose();
            //    myConnection.Dispose();
            //}
             */

            SqlConnection newConnection = new SqlConnection(mConstr);
            //SqlCommand newCommand = new SqlCommand("UpdatePostInvalidFlag", newConnection);

            SqlCommand newCommand = new SqlCommand("NoticePostReturnAdd", newConnection);
            // Mark the Command as a SPROC
            newCommand.CommandType = CommandType.StoredProcedure;
            // Add Parameters to SPROC
            newCommand.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = sTicketNo;
            //myCommand.Parameters.Add("@NotIntNo", SqlDbType.Int).Value = nNotIntNo;
            newCommand.Parameters.Add("@PRTIntNo", SqlDbType.Int).Value = nPRTIntNo;
            newCommand.Parameters.Add("@NPRRDate", SqlDbType.SmallDateTime).Value = dtInDate;
            newCommand.Parameters.Add("@NPRRUser", SqlDbType.VarChar, 50).Value = sUser;
            newCommand.Parameters.Add("@NPRRBoxNo", SqlDbType.VarChar, 10).Value = sBoxNo;
            newCommand.Parameters.Add("@NPRRStorageLocation", SqlDbType.VarChar, 25).Value = sStorageLocation;
            SqlParameter returnIntNo = new SqlParameter("@NPRRIntNo", SqlDbType.Int, 4);
            returnIntNo.Direction = ParameterDirection.Output;
            newCommand.Parameters.Add(returnIntNo);

            try
            {
                newConnection.Open();
                newCommand.ExecuteNonQuery();
                newCommand.Dispose();
                newConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int NPRRIntNo = Convert.ToInt32(returnIntNo.Value);

                return NPRRIntNo;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e.Message);
                newConnection.Dispose();
                newCommand.Dispose();
                return 0;
            }
        }

        public int UpdatePostReturn(int nNPRRIntNo, int nNotIntNo, int nPRTIntNo, DateTime dtInDate, string sUser, string sBoxNo, string sStorageLocation)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticePostReturnUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@NPRRIntNo", SqlDbType.Int).Value = nNPRRIntNo;
            myCommand.Parameters.Add("@NotIntNo", SqlDbType.Int).Value = nNotIntNo;
            myCommand.Parameters.Add("@PRTIntNo", SqlDbType.Int).Value = nPRTIntNo;
            myCommand.Parameters.Add("@NPRRDate", SqlDbType.SmallDateTime).Value = dtInDate;
            myCommand.Parameters.Add("@NPRRUser", SqlDbType.VarChar, 50).Value = sUser;
            myCommand.Parameters.Add("@NPRRBoxNo", SqlDbType.VarChar, 10).Value = sBoxNo;
            myCommand.Parameters.Add("@NPRRStorageLocation", SqlDbType.VarChar, 25).Value = sStorageLocation;
            SqlParameter returnIntNo = new SqlParameter("@RetVal", SqlDbType.Int, 4);
            returnIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(returnIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int nReturn = Convert.ToInt32(returnIntNo.Value);

                return nReturn;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e.Message);
                myConnection.Dispose();
                return 0;
            }
        }

        public SqlDataReader GetPostReturnTypeList()
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("PostReturnTypeList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }


        public DataSet GetPostReturnTypeListDS()
        {
            SqlDataAdapter sqlDARoadTypes = new SqlDataAdapter();
            DataSet dsPostReturnTypes = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDARoadTypes.SelectCommand = new SqlCommand();
            sqlDARoadTypes.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDARoadTypes.SelectCommand.CommandText = "PostReturnTypeList";

            // Mark the Command as a SPROC
            sqlDARoadTypes.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command and close the connection
            sqlDARoadTypes.Fill(dsPostReturnTypes);
            sqlDARoadTypes.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsPostReturnTypes;
        }



        public int DeletePostReturn(int nNPRRTIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticePostReturnDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNPRRTIntNo = new SqlParameter("@NPRRIntNo", SqlDbType.Int, 4);
            parameterNPRRTIntNo.Value = nNPRRTIntNo;
            parameterNPRRTIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNPRRTIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int PRTIntNo = Convert.ToInt32(parameterNPRRTIntNo.Value);

                return PRTIntNo;
            }
            catch
            {
                myConnection.Dispose();
                return 0;
            }
        }
    }
}


