﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Stalberg.TMS_TPExInt.Objects;
using System.Data.SqlClient;
using System.Data;
using System.Xml;

namespace Stalberg.TMS
{
    public class ResponseFileData
    {
        public ResponseFileData()
        { }

        private string ticketNumber;
        private DateTime updateDate;
        private bool updateSuccess;


        public string TicketNumber
        {
            get { return ticketNumber; }
            set { ticketNumber = value; }
        }

        public DateTime UpdateDate
        {
            get { return updateDate; }
            set { updateDate = value; }
        }

        public bool UpdateSuccess
        {
            get { return updateSuccess; }
            set { updateSuccess = value; }
        }
    }

    public class ResponseFile
    {
        //fileds
        public const string RESPONSE_FILE_EXTENSION = ".irf";
        private FineExchangeDataFile fine;
        private const int VERSION = 1;
        public const string DATE_FORMAT = "yyyy-MM-dd_HH-mm-ss-ff";
        private const char DELIMITER = ',';
        public string fileName;
        private Stream m_stream = null;
        private int version;

        public EasyPayAuthority authority;
        private string BatchCode = "";
        private List<ResponseFileData> responseFileDatalist = new List<ResponseFileData>();

        /// <summary>
        /// Occurs when an email needs to be sent.
        /// </summary>
        public event EmailEventHandler SendEmail;

        private int finesCount;
        /// <summary>
        /// Gets the authority.
        /// </summary>
        /// <value>The authority.</value>
        public EasyPayAuthority Authority
        {
            get { return this.authority; }
        }

        public List<ResponseFileData> ResponseFileDatalist
        {
            get { return responseFileDatalist; }
            set { responseFileDatalist = value; }
        }

        public int FinesCount
        {
            get { return finesCount; }
            set { finesCount = value; }
        }

        //instance
        public ResponseFile(FineExchangeDataFile mfine, string strFileName)
        {
            fine = mfine;
            fileName = strFileName;
        }

        public ResponseFile(Stream stream)
        {
            stream.Seek(0, SeekOrigin.Begin);
            m_stream = stream;
        }

        public static ResponseFile ReadDataFiles(Stream stream, string name)
        {
            ResponseFile responseFile = new ResponseFile(stream);
            responseFile.fileName = name;
            try
            {
                if (responseFile.Read())
                {
                    return responseFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return null;
        }

        public string Check(string strConnectionStr)
        {
            string strMsg = "";

                AuthorityDB authorityDb = new AuthorityDB(strConnectionStr);

            SqlDataReader reader = authorityDb.GetAuthorityDetailsByAutCode(authority.AuthorityCode,ref strMsg); 
            if(reader == null || !reader.HasRows)
            {
                strMsg = "Database don't know the LA with AuthorityCode: " + authority.AuthorityCode;
                return strMsg;
            }

            if(version != VERSION)
            {
                strMsg = "Invalid version: " + version.ToString();
                return strMsg;
            }

            if(finesCount != responseFileDatalist.Count)
            {
                strMsg = "Response file data row count is incorrect. DataRow Count: " + responseFileDatalist.Count.ToString() + " finesCount: " + finesCount.ToString();
                return strMsg;
            }
            return strMsg;
        }

        public void ProceessResponseFileData(string strConnectionStr, ref List<string> failedTickets, string strLastUser)
        {
            try
            {
                this.SendEmail += new EmailEventHandler(responseFile_SendEmail);

                //Check response data
                string strMsg = Check(strConnectionStr);
                if (strMsg.Length > 0)
                {
                    this.SendEmail(this, new EmailEventArgs(string.Format("Tickets in the response file {0} processed failed. LA:{1}", fileName, authority.AuthorityCode), strMsg));
                    throw new Exception(strMsg);
                }

                SqlConnection con = new SqlConnection(strConnectionStr);
                SqlCommand com = new SqlCommand("ThaboUpdateWithResponseData", con);
                com.CommandType = CommandType.StoredProcedure;

                try
                {
                    con.Open();
                    foreach (ResponseFileData responseFileData in responseFileDatalist)
                    {
                        try
                        {
                            // Check if the Notice exists and update it if necessary, return the ID
                            com.Parameters.Clear();
                            com.Parameters.Add("@AutCode", SqlDbType.VarChar, 3).Value = authority.AuthorityCode;
                            com.Parameters.Add("@BatchCode", SqlDbType.VarChar, 20).Value = BatchCode;

                            com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = responseFileData.TicketNumber;
                            //Jerry 2014-10-08 if the update date is min value then use date now
                            //com.Parameters.Add("@UpdateDate", SqlDbType.DateTime, 8).Value = responseFileData.UpdateDate;
                            com.Parameters.Add("@UpdateDate", SqlDbType.DateTime, 8).Value = responseFileData.UpdateDate == DateTime.MinValue ? DateTime.Now : responseFileData.UpdateDate;
                            com.Parameters.Add("@UpdateSuccess", SqlDbType.Bit, 10).Value = responseFileData.UpdateSuccess;
                            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = strLastUser;

                            if (int.Parse(com.ExecuteScalar().ToString()) < 0)
                            {
                                failedTickets.Add(responseFileData.TicketNumber);
                            }
                        }
                        catch
                        {
                            failedTickets.Add(responseFileData.TicketNumber);
                        }
                    }
                }
                finally
                {
                    con.Close();
                }
                

                if (failedTickets.Count > 0)
                {
                    if (this.SendEmail != null)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (string str in failedTickets)
                        {
                            sb.Append(string.Format("Ticket Number {0} ", str));
                              sb.Append(Environment.NewLine);
                        }

                        this.SendEmail(this, new EmailEventArgs(string.Format("Tickets in the response file {0} processed failed. LA:{1}", fileName, authority.AuthorityCode), sb.ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                this.SendEmail -= new EmailEventHandler(responseFile_SendEmail);
            }
        }

        //Jerry 2012-07-11 add for service
        public void UpdateWithResponseData(string strConnectionStr, ref List<string> failedTickets, string strLastUser)
        {
            SqlConnection con = new SqlConnection(strConnectionStr);
            SqlCommand com = new SqlCommand("ThaboUpdateWithResponseData", con);
            com.CommandType = CommandType.StoredProcedure;

            try
            {
                con.Open();
                foreach (ResponseFileData responseFileData in responseFileDatalist)
                {
                    try
                    {
                        // Check if the Notice exists and update it if necessary, return the ID
                        com.Parameters.Clear();
                        com.Parameters.Add("@AutCode", SqlDbType.VarChar, 3).Value = authority.AuthorityCode;
                        com.Parameters.Add("@BatchCode", SqlDbType.VarChar, 20).Value = BatchCode;

                        com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = responseFileData.TicketNumber;
                        com.Parameters.Add("@UpdateDate", SqlDbType.DateTime, 8).Value = responseFileData.UpdateDate;
                        com.Parameters.Add("@UpdateSuccess", SqlDbType.Bit, 10).Value = responseFileData.UpdateSuccess;
                        com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = strLastUser;

                        if (int.Parse(com.ExecuteScalar().ToString()) < 0)
                        {
                            failedTickets.Add(responseFileData.TicketNumber);
                        }
                    }
                    catch
                    {
                        failedTickets.Add(responseFileData.TicketNumber);
                    }
                }
            }
            finally
            {
                con.Close();
            }
        }

        private void responseFile_SendEmail(object sender, EmailEventArgs e)
        {
            try
            {
                EmailHelper email = new EmailHelper();
                email.Subject = e.Subject;
                email.Message = e.Body;

                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(Path.Combine(Environment.CurrentDirectory, "SysParam.xml"));

                XmlNode node = xDoc.SelectSingleNode("/codes/responseFileEmail");
                if (node != null)
                    email.Eamil = node.InnerText;

                if (!email.Send())
                    throw new Exception("ERROR: an email could not be sent.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Write
        public bool CreateFile()
        {
            bool response = true;

            if (!fine.Authority.HasData)
                return response;

            // Create the writer stream
            FileInfo file = new FileInfo(fileName);
            if (!file.Directory.Exists)
                file.Directory.Create();
            using (StreamWriter sw = new StreamWriter(file.Open(FileMode.Create), Encoding.UTF8))
            {
                try
                {
                    this.WriteHeader(sw);

                    foreach (EasyPayFineData m_fine in fine.Authority.Fines)
                    {
                        this.WriteData(sw, m_fine);
                    }

                    WriteFooter(sw);
                }
                catch (Exception ex)
                {
                    response = false;
                    throw new ApplicationException("There was an error writing the response file.", ex);
                }
                finally
                {
                    sw.Close();
                    file = null;
                }
            }
            return response;

        }

        private void WriteHeader(StreamWriter writer)
        {
            writer.Write("H");
            writer.Write(DELIMITER);
            writer.Write(this.fine.Authority.Name);
            writer.Write(DELIMITER);
            writer.Write(fine.Authority.AuthorityCode);
            writer.Write(DELIMITER);
            writer.Write(fine.BatchNumber);
            writer.Write(DELIMITER);
            writer.Write(VERSION);
            writer.Write("\n");
        }


        private void WriteData(StreamWriter writer, EasyPayFineData m_fine)
        {
            writer.Write("D");
            writer.Write(DELIMITER);

            writer.Write(m_fine.TicketNumber);
            writer.Write(DELIMITER);

            writer.Write(m_fine.UpdateDate);
            writer.Write(DELIMITER);

            if (m_fine.UpdateSuccess)
                writer.Write("Y");
            else
                writer.Write("N");
            writer.Write("\n");
        }

        private void WriteFooter(StreamWriter writer)
        {
            writer.Write("E");
            writer.Write(DELIMITER);
            writer.Write(fine.Authority.Fines.Count);
        }
        #endregion

        #region Read
        private bool Read()
        {
            bool response = true;

            StreamReader reader = new StreamReader(m_stream, Encoding.UTF8);
            string fileContents = reader.ReadToEnd();
            reader.Close();
            m_stream.Close();

            try
            {
                responseFileDatalist = new List<ResponseFileData>();
                string[] lines = fileContents.Split(new char[] { '\n' });
                foreach (string line in lines)
                {
                    if (line.Trim().Length == 0)
                        continue;

                    switch (line[0])
                    {
                        case 'H':
                            this.ReadHeader(line);
                            break;

                        case 'D':
                            this.ReadData(line);
                            break;

                        case 'E':
                            this.ReadFooter(line);
                            break;

                        default:
                            throw new ApplicationException(string.Format("There was a problem processing a line in {0}, it began with {1} which is not a valid Line Identifier.", this.fileName, line[0]));
                    }
                }
            }
            catch (Exception ex)
            {
                response = false;
                throw new ApplicationException(string.Format("There was a problem reading the Response Data File {0}.\n{1}", this.fileName, ex.Message), ex);
            }

            return response;
        }

        private void ReadHeader(string line)
        {
            string[] values = line.Split(new char[] { DELIMITER });

            version = int.Parse(values[4]);

            this.authority = new EasyPayAuthority();
            this.authority.Name = values[1];
            this.authority.AuthorityCode = values[2];
            BatchCode = values[3];
        }

        private void ReadData(string line)
        {
            string[] values = line.Split(new char[] { DELIMITER });

            ResponseFileData responseFileData = new ResponseFileData();

            responseFileData.TicketNumber = values[1];
            responseFileData.UpdateDate = DateTime.Parse(values[2]);
            responseFileData.UpdateSuccess = values[3].Trim().ToUpper() == "Y";
            responseFileDatalist.Add(responseFileData);

        }

        private void ReadFooter(string line)
        {
            string[] values = line.Split(new char[] { DELIMITER });
            finesCount = int.Parse(values[1]);
        }
        #endregion
    }
}
