using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Collections;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the details of a camera unit
    /// </summary>
    public partial class CameraUnitDetails
    {
        public Int32 AutIntNo;
        public string CamUnitID;
        public string AutNo;
        public string Active;
        public string LastUser;
    }

    /// <summary>
    /// Contains all the methods that interact with the database concerning Camera Units
    /// </summary>
    public partial class CameraUnitDB
    {
        // Fields
        string connectionString = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="CameraUnitDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public CameraUnitDB(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Gets the camera unit details.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="camUnitID">The cam unit ID.</param>
        /// <returns></returns>
        public CameraUnitDetails GetCameraUnitDetails(int autIntNo, string camUnitID)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("CameraUnitDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
            parameterCamUnitID.Value = camUnitID;
            myCommand.Parameters.Add(parameterCamUnitID);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            CameraUnitDetails myCameraUnitDetails = new CameraUnitDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myCameraUnitDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
                myCameraUnitDetails.AutNo = result["AutNo"].ToString();
                myCameraUnitDetails.CamUnitID = result["CamUnitID"].ToString();
                myCameraUnitDetails.Active = result["Active"].ToString();
            }
            result.Close();
            return myCameraUnitDetails;
        }

        /// <summary>
        /// Adds the camera unit.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="camUnitID">The cam unit ID.</param>
        /// <param name="active">The active.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        public int AddCameraUnit(int autIntNo, string camUnitID, string active, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CameraUnitAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
            parameterCamUnitID.Value = camUnitID;
            myCommand.Parameters.Add(parameterCamUnitID);

            SqlParameter parameterActive = new SqlParameter("@Active", SqlDbType.VarChar, 5);
            parameterActive.Value = active;
            myCommand.Parameters.Add(parameterActive);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int, 4);
            parameterSuccess.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterSuccess);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int success = Convert.ToInt32(parameterSuccess.Value);
                return success;
            }
            catch
            {
                myConnection.Dispose();
                return -2;
            }
        }

        /// <summary>
        /// Gets the list camera unit for an LA.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <returns>A <see cref="SqlDataReader"/>.</returns>
        public SqlDataReader GetCameraUnitList(int autIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CameraUnitList", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }


        /// <summary>
        /// Gets the camera unit list DS.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        public DataSet GetCameraUnitListDS(int autIntNo)
        {
            SqlDataAdapter sqlDACameraUnits = new SqlDataAdapter();
            DataSet dsCameraUnits = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDACameraUnits.SelectCommand = new SqlCommand();
            sqlDACameraUnits.SelectCommand.Connection = new SqlConnection(connectionString);
            sqlDACameraUnits.SelectCommand.CommandText = "CameraUnitList";

            // Mark the Command as a SPROC
            sqlDACameraUnits.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDACameraUnits.SelectCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command and close the connection
            sqlDACameraUnits.Fill(dsCameraUnits);
            sqlDACameraUnits.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsCameraUnits;
        }


        /// <summary>
        /// Updates the camera unit.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="camUnitID">The cam unit ID.</param>
        /// <param name="active">The active.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        public int UpdateCameraUnit(int autIntNo, string camUnitID, string active, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CameraUnitUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
            parameterCamUnitID.Value = camUnitID;
            myCommand.Parameters.Add(parameterCamUnitID);

            SqlParameter parameterActive = new SqlParameter("@Active", SqlDbType.VarChar, 5);
            parameterActive.Value = active;
            myCommand.Parameters.Add(parameterActive);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int, 4);
            parameterSuccess.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterSuccess);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int success = (int)myCommand.Parameters["@Success"].Value;
                return success;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return -2;
            }
        }

        /// <summary>
        /// Deletes the camera unit.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="camUnitID">The cam unit ID.</param>
        /// <returns></returns>
        public int DeleteCameraUnit(int autIntNo, string camUnitID)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CameraUnitDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
            parameterCamUnitID.Value = camUnitID;
            myCommand.Parameters.Add(parameterCamUnitID);

            SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int, 4);
            parameterSuccess.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterSuccess);


            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int success = (int)parameterSuccess.Value;
                return success;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return -5;
            }
        }

    }
}

