using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{
	
	public partial class TranNoDetails 
	{
		public Int32 TNIntNo;
		public Int32 AutIntNo;
		public string TNType;
		public string TNDescr;
		public Int32 TNumber;
		public string LastUser;
	}

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public partial class TranNoDB
	{
		string mConstr = "";

			public TranNoDB (string vConstr)
			{
				mConstr = vConstr;
			}

		//*******************************************************
		//
		// The GetTranNoDetails method returns a TranNoDetails
		// struct that contains information about a specific transaction number
		//
		//*******************************************************

		public TranNoDetails GetTranNoDetails(int tnIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("TranNoDetail", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterTNIntNo = new SqlParameter("@TNIntNo", SqlDbType.Int, 4);
			parameterTNIntNo.Value = tnIntNo;
			myCommand.Parameters.Add(parameterTNIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			// Create CustomerDetails Struct
			TranNoDetails myTranNoDetails = new TranNoDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myTranNoDetails.TNIntNo = Convert.ToInt32(result["TNIntNo"]);
				myTranNoDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
				myTranNoDetails.TNType = result["TNType"].ToString();
				myTranNoDetails.TNumber = Convert.ToInt32(result["TNumber"]);
				myTranNoDetails.TNDescr = result["TNDescr"].ToString();
				myTranNoDetails.LastUser = result["LastUser"].ToString();
			}
			result.Close();
			return myTranNoDetails;
		}

        //dls 060522 - the stored proc requires a Max No when auto-generating no's
        public int GetNextTranNoByType(string tnType, int autIntNo, string lastUser, int maxNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("TranNoNextNumberByType", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterTNType = new SqlParameter("@TNType", SqlDbType.VarChar, 3);
			parameterTNType.Value = tnType;
			myCommand.Parameters.Add(parameterTNType);

			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
			parameterAutIntNo.Value = autIntNo;
			myCommand.Parameters.Add(parameterAutIntNo);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterMaxNo = new SqlParameter("@MaxNo", SqlDbType.Int, 4);
            parameterMaxNo.Value = maxNo;
            myCommand.Parameters.Add(parameterMaxNo);

			SqlParameter parameterTNumber = new SqlParameter("@TNumber", SqlDbType.Int);
			parameterTNumber.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterTNumber);
			
			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int tNumber = Convert.ToInt32(parameterTNumber.Value);

				return tNumber;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		//*******************************************************
		//
		// The AddTranNo method inserts a new transaction number record
		// into the TranNo database.  A unique "TNIntNo"
		// key is then returned from the method.  
		//
		//*******************************************************

		public int AddTranNo(int autIntNo, string tnType, int tNumber, string tnDescr, string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("TranNoAdd", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			myCommand.Parameters.Add(parameterAutIntNo);

			SqlParameter parameterTNType = new SqlParameter("@TNType", SqlDbType.VarChar, 3);
			parameterTNType.Value = tnType;
			myCommand.Parameters.Add(parameterTNType);

			SqlParameter parameterTNumber = new SqlParameter("@TNumber", SqlDbType.Int);
			parameterTNumber.Value = tNumber;
			myCommand.Parameters.Add(parameterTNumber);

			SqlParameter parameterTNDescr = new SqlParameter("@TNDescr", SqlDbType.VarChar, 50);
			parameterTNDescr.Value = tnDescr;
			myCommand.Parameters.Add(parameterTNDescr);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterTNIntNo = new SqlParameter("@TNIntNo", SqlDbType.Int, 4);
			parameterTNIntNo.Direction = ParameterDirection.Output;
			myCommand.Parameters.Add(parameterTNIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int tnIntNo = Convert.ToInt32(parameterTNIntNo.Value);

				return tnIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}
        // 2013-07-19 comment by Henry for useless
        //public SqlDataReader GetTranNoList(int autIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("TranNosList", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the datareader result
        //    return result;
        //}

		public DataSet GetTranNoListDS(int autIntNo)
		{
			SqlDataAdapter sqlDATranNos = new SqlDataAdapter();
			DataSet dsTranNos = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDATranNos.SelectCommand = new SqlCommand();
			sqlDATranNos.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDATranNos.SelectCommand.CommandText = "TranNoList";

			// Mark the Command as a SPROC
			sqlDATranNos.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			sqlDATranNos.SelectCommand.Parameters.Add(parameterAutIntNo);

			// Execute the command and close the connection
			sqlDATranNos.Fill(dsTranNos);
			sqlDATranNos.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsTranNos;		
		}

		public int UpdateTranNo(int autIntNo, string tnType, int tNumber, string tnDescr, string lastUser, int tnIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("TranNoUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			myCommand.Parameters.Add(parameterAutIntNo);

			SqlParameter parameterTNType = new SqlParameter("@TNType", SqlDbType.VarChar, 3);
			parameterTNType.Value = tnType;
			myCommand.Parameters.Add(parameterTNType);

			SqlParameter parameterTNumber = new SqlParameter("@TNumber", SqlDbType.Int);
			parameterTNumber.Value = tNumber;
			myCommand.Parameters.Add(parameterTNumber);

			SqlParameter parameterTNDescr = new SqlParameter("@TNDescr", SqlDbType.VarChar, 50);
			parameterTNDescr.Value = tnDescr;
			myCommand.Parameters.Add(parameterTNDescr);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterTNIntNo = new SqlParameter("@TNIntNo", SqlDbType.Int, 4);
			parameterTNIntNo.Value = tnIntNo;
			parameterTNIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterTNIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int TNIntNo = (int)myCommand.Parameters["@TNIntNo"].Value;

				return TNIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}
		public int UpdateTranNumber(int tNumber, string lastUser, int tnIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("TranNoUpdateNumber", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterTNumber = new SqlParameter("@TNumber", SqlDbType.Int);
			parameterTNumber.Value = tNumber;
			myCommand.Parameters.Add(parameterTNumber);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterTNIntNo = new SqlParameter("@TNIntNo", SqlDbType.Int, 4);
			parameterTNIntNo.Value = tnIntNo;
			parameterTNIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterTNIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int TNIntNo = (int)myCommand.Parameters["@TNIntNo"].Value;

				return TNIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		public String DeleteTranNo (int tnIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("TranNoDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterTNIntNo = new SqlParameter("@TNIntNo", SqlDbType.Int, 4);
			parameterTNIntNo.Value = tnIntNo;
			parameterTNIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterTNIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int userId = (int)parameterTNIntNo.Value;

				return userId.ToString();
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return String.Empty;
			}
		}
	}
}
