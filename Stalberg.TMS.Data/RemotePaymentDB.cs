using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    /// <summary>
    /// Contains all the logic and data access for interacting with the Remote Payment database table
    /// </summary>
    public class RemotePaymentDB
    {
        // Fields
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="RemotePaymentDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public RemotePaymentDB(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Checks if the payment already exists, and returns <c>true</c> if the payment can proceed.
        /// </summary>
        /// <param name="notIntNo">The not int no.</param>
        /// <returns><c>true</c> if the payment can proceed.</returns>
        public string CheckIfPaymentExists(int notIntNo)
        {
            return this.CheckIfPaymentExists(notIntNo, string.Empty);
        }

        /// <summary>
        /// Checks if the payment already exists, and returns <c>true</c> if the payment can proceed.
        /// </summary>
        /// <param name="ticketNo">The ticket no.</param>
        /// <returns><c>true</c> if the payment can proceed.</returns>
        public string CheckIfPaymentExists(string ticketNo)
        {
            return this.CheckIfPaymentExists(0, ticketNo);
        }

        private string CheckIfPaymentExists(int notIntNo, string ticketNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("RemotePaymentCheck", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = ticketNo;

            try
            {
                con.Open();
                return com.ExecuteScalar().ToString();
            }
            finally
            {
                con.Close();
                con.Dispose();
                com.Dispose();
            }
        }

        /// <summary>
        /// Gets the original fine amount for a notice.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <returns>A <see cref="decimal"/> containing the amount in the database.</returns>
        public decimal GetOriginalFineAmount(string ticketNo, int notIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("ThaboCheckNoticeAmount", con);
            com.CommandType = CommandType.StoredProcedure;

            if (ticketNo.Length > 0)
                com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = ticketNo;
            if (notIntNo > 0)
                com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;

            try
            {
                //int count = 0;
                decimal amount = 0;

                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                if (reader != null)
                {
                    // jerry 2011-1-11 changed 
                    while (reader.Read())
                    {
                        //count++;
                        amount = Convert.ToDecimal(reader["ChgRevFineAmount"]);
                    }
                    reader.Close();
                }

                return amount;
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                con.Close();
            }
           
        }

    }
}