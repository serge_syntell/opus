using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Stalberg.ThaboAggregator.Objects;
using Stalberg.TMS_TPExInt.Objects;
using System.Globalization;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents an EasyPay number
    /// </summary>
    public struct EasyPayNumber
    {
        // Fields
        private readonly int _receiverIdentifier;
        private readonly long _accountNumber;
        private readonly int _checkDigit;
        private readonly string _value;

        // Constants
        private const int EASY_PAY_CONSTANT = 9;

        /// <summary>
        /// Initializes a new instance of the <see cref="EasyPayNumber"/> class.
        /// </summary>
        /// <param name="receiverIdentifier">The receiver identifier.</param>
        /// <param name="accountNumber">The account number.</param>
        public EasyPayNumber(int receiverIdentifier, long accountNumber)
        {
            this._receiverIdentifier = receiverIdentifier;
            this._accountNumber = accountNumber;
            this._checkDigit = EasyPayNumber.CalculateCheckDigit(receiverIdentifier, accountNumber);
            this._value = string.Format("{0:0}{1:0000}{2:00000000000000}{3:0}",
                EASY_PAY_CONSTANT, this._receiverIdentifier, this._accountNumber, this._checkDigit);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EasyPayNumber"/> class.
        /// </summary>
        /// <param name="sReceiverID">The s receiver ID.</param>
        /// <param name="sNPrefix">The s N prefix.</param>
        /// <param name="sTicketSequence">The s ticket sequence.</param>
        /// <param name="sAutNo">The s aut no.</param>
        public EasyPayNumber(string sReceiverID, string sNPrefix, string sTicketSequence, string sAutNo)
        {
            this._receiverIdentifier = 0;
            this._accountNumber = 0;

            this._checkDigit = EasyPayNumber.CalculateCheckDigit(sReceiverID, sTicketSequence, sNPrefix, sAutNo);
            this._value = string.Format("{0:0}{1:0000}{2:000}{3:000000}{4:000}{5:0}",
                EASY_PAY_CONSTANT, sReceiverID, sTicketSequence, sNPrefix, sAutNo, _checkDigit);
        }

        /// <summary>
        /// Parses a string containing the digits of an easy pay number.
        /// </summary>
        /// <param name="easyPayNumber">The string representation of an easy pay number.</param>
        /// <returns>An <see cref="EasyPayNumber"/></returns>
        public static EasyPayNumber Parse(string easyPayNumber)
        {
            if (string.IsNullOrEmpty(easyPayNumber))
                throw new ApplicationException("The EasyPay number cannot be NULL or an empty string, it must be 20 characters long.");
            if (easyPayNumber.Length != 20)
                throw new ApplicationException(string.Format("The EasyPay Number is incorrect, it was {0} characters long instead of 20.", easyPayNumber.Length));

            foreach (char c in easyPayNumber)
            {
                if (!char.IsDigit(c))
                    throw new ApplicationException(string.Format("The EasyPay Number is made up entirely of digits, you cannot have a {0} in it.", c));
            }

            int epConst;
            if (!int.TryParse(easyPayNumber[0].ToString(), out epConst))
                throw new ApplicationException(string.Format("The EasyPay constant is incorrect ({0} instead of {1}).", easyPayNumber[0], EASY_PAY_CONSTANT));
            if (!epConst.Equals(EASY_PAY_CONSTANT))
                throw new ApplicationException(string.Format("The EasyPay constant is incorrect ({0} instead of {1}).", epConst, EASY_PAY_CONSTANT));

            int checkDigit = int.Parse(easyPayNumber[easyPayNumber.Length - 1].ToString());
            int receiverIdentifier = int.Parse(easyPayNumber.Substring(1, 4));
            long accountNumber = long.Parse(easyPayNumber.Substring(5, 14));

            int computedCheckDigit = EasyPayNumber.CalculateCheckDigit(receiverIdentifier, accountNumber);
            if (!checkDigit.Equals(computedCheckDigit))
                throw new ApplicationException(string.Format("The computed check digit {0} does not match the one on the EasyPay Number check digit {1}.",
                    computedCheckDigit, checkDigit));

            return new EasyPayNumber(receiverIdentifier, accountNumber);
        }

        /// <summary>
        /// Gets the easy pay constant.
        /// </summary>
        /// <value>The easy pay constant.</value>
        public int EasyPayConstant
        {
            get { return EASY_PAY_CONSTANT; }
        }

        /// <summary>
        /// Gets the check digit for the Reciever Identity and Account Number.
        /// </summary>
        /// <value>The check digit.</value>
        public int CheckDigit
        {
            get { return this._checkDigit; }
        }

        /// <summary>
        /// Gets the current system created Account Number.
        /// </summary>
        /// <value>The account number.</value>
        public long AccountNumber
        {
            get { return this._accountNumber; }
        }

        /// <summary>
        /// Gets the EasyPay Receiver Identifier.
        /// </summary>
        /// <value>The receiver identifier.</value>
        public int ReceiverIdentifier
        {
            get { return this._receiverIdentifier; }
        }

        /// <summary>
        /// Returns the fully qualified type name of this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> containing a fully qualified type name.
        /// </returns>
        public override string ToString()
        {
            return this._value;
        }

        /// <summary>
        /// Calculates the check digit part of the EasyPay number.
        /// </summary>
        /// <param name="receiverIdentifier">The receiver identifier. As supplied by EasyPay.</param>
        /// <param name="accountNumber">The account number. This is the current system's identification data that EasyPay will return on payment.</param>
        /// <returns>An <see cref="Int32"/> that is the check sim digit.</returns>
        internal static int CalculateCheckDigit(int receiverIdentifier, long accountNumber)
        {
            string checkPart = string.Format("{0:0000}{1:00000000000000}", receiverIdentifier, accountNumber);
            int total = 0;
            int current;

            // Iterate in reverse order
            for (int i = checkPart.Length - 1; i >= 0; i--)
            {
                if (i % 2 == 0) // Even - Just add the digit
                    total += int.Parse(checkPart[i].ToString());
                else // Odd - double it and remove 9 if its greater than 9, then add it to the total
                {
                    current = int.Parse(checkPart[i].ToString()) * 2;
                    if (current > 9)
                        current -= 9;
                    total += current;
                }
            }

            // Calculate the remainder
            int returnValue = 10 - (total % 10);

            return returnValue;
        }

        /// <summary>
        /// Calculates the check digit part of the EasyPay number.
        /// </summary>
        /// <param name="sReceiverID">The s receiver ID.</param>
        /// <param name="sNPrefix">The s N prefix.</param>
        /// <param name="sTicketSequence">The s ticket sequence.</param>
        /// <param name="sAutNo">The s aut no.</param>
        /// <returns>
        /// An <see cref="Int32"/> that is the check sim digit.
        /// </returns>
        /// <remarks>they no longer want the autno as part of the number because we are stuck with length 15</remarks>
        internal static int CalculateCheckDigit(string sReceiverID, string sNPrefix, string sTicketSequence, string sAutNo)
        {
            string checkPart = string.Format("{0:0000}{1:000}{2:000000}{3:000}", sReceiverID, sNPrefix, sTicketSequence, sAutNo);
            int total = 0;

            // Iterate in reverse order
            for (int i = checkPart.Length - 1; i >= 0; i--)
            {
                if (i % 2 == 0) // Even - Just add the digit
                    total += int.Parse(checkPart[i].ToString());
                else // Odd - double it and remove 9 if its greater than 9, then add it to the total
                {
                    int current = int.Parse(checkPart[i].ToString()) * 2;
                    if (current > 9)
                        current -= 9;
                    total += current;
                }
            }

            // Calculate the remainder
            int returnValue = 10 - (total % 10);

            return returnValue;
        }

    }

    /// <summary>
    /// Represent the email event arguments
    /// </summary>
    public class EmailEventArgs : EventArgs
    {
        // Fields
        private string subject;
        private string body;
        private List<string> attachments;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailEventArgs"/> class.
        /// </summary>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        public EmailEventArgs(string subject, string body)
            : this(subject, body, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailEventArgs"/> class.
        /// </summary>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="attachments">The attachments.</param>
        public EmailEventArgs(string subject, string body, params string[] attachments)
        {
            this.subject = subject;
            this.body = body;
            this.attachments = new List<string>();
            if (attachments != null)
                this.attachments.AddRange(attachments);
        }

        /// <summary>
        /// Gets the list of attachments.
        /// </summary>
        /// <value>The attachments.</value>
        public List<string> Attachments
        {
            get { return this.attachments; }
        }

        /// <summary>
        /// Gets the email body.
        /// </summary>
        /// <value>The body.</value>
        public string Body
        {
            get { return this.body; }
        }

        /// <summary>
        /// Gets the email's subject line.
        /// </summary>
        /// <value>The subject.</value>
        public string Subject
        {
            get { return this.subject; }
        }

    }

    /// <summary>
    /// The delegate for handling email events
    /// </summary>
    public delegate void EmailEventHandler(object sender, EmailEventArgs e);

    /// <summary>
    /// Contains all the database access logic for managing Easy Pay data
    /// </summary>
    public class EasyPayDB : IDisposable
    {
        // Fields
        private SqlConnection con = null;
        private string connectionString;
        private AuthorityDB auth = null;

        // Constants
        private const int CHANGE_FINE_REP_CODE = 4;
        private const int SUMMONS_REPCODE_REDUCTION = 14;

        /// <summary>
        /// Occurs when an email needs to be sent.
        /// </summary>
        public event EmailEventHandler SendEmail;

        /// <summary>
        /// Initializes a new instance of the <see cref="EasyPayDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public EasyPayDB(string connectionString)
        {
            this.connectionString = connectionString;
            this.con = new SqlConnection(connectionString);
            this.auth = new AuthorityDB(this.connectionString);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (this.con != null)
            {
                this.con.Close();
                this.con.Dispose();
            }
        }

        /// <summary>
        /// Creates the appropriate EasyPay transaction.
        /// </summary>
        /// <param name="authority">The authority.</param>
        /// <param name="notIntNo">The notice int no.</param>
        /// <param name="login">The login.</param>
        public void EasyPayTransactionVerification(EasyPayAuthority authority, int notIntNo, string login)
        {
            if (!authority.IsValid)
                return;

            // FBJ Added (2006-11-20): We are not verifying EasyPay numbers, the one on Notice is definitive
            //EasyPayNumber epNo = new EasyPayNumber(authority.ReceiptIdentification, notIntNo);
            string epNo2 = string.Empty;
            decimal amount = 0.0M;
            DateTime paymentDate = DateTime.Now;

            SqlCommand com = new SqlCommand("EasyPayNoticeDetails", this.con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;

            try
            {
                this.con.Open();
                SqlDataReader reader = com.ExecuteReader();
                if (reader != null)
                {
                    if (reader.Read())
                    {
                        if (reader["NotEasyPayNumber"] != DBNull.Value)
                            epNo2 = (string)reader["NotEasyPayNumber"];
                        if (reader["NotPaymentDate"] != DBNull.Value)
                            paymentDate = (DateTime)reader["NotPaymentDate"];
                        amount = decimal.Parse(reader["Amount"].ToString());
                    }
                    reader.Close();
                }
            }
            finally
            {
                this.con.Close();
            }

            com.CommandText = "EasyPayAddTransaction";
            com.Parameters.Add("@EasyPayNo", SqlDbType.VarChar, 20).Value = epNo2;
            com.Parameters.Add("@EPAmount", SqlDbType.SmallMoney).Value = amount;
            com.Parameters.Add("@EPExpiryDate", SqlDbType.SmallDateTime).Value = paymentDate.AddDays(authority.NumberOfDaysForExpiry);
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = login;

            try
            {
                this.con.Open();
                com.ExecuteNonQuery();
            }
            finally
            {
                this.con.Close();
            }
        }

        /// <summary>
        /// Deletes the easy pay transaction.
        /// </summary>
        /// <param name="notIntNo">The not int no.</param>
        /// <param name="login">The login.</param>
        public void DeleteEasyPayTransaction(int notIntNo, string login)
        {
            SqlCommand com = new SqlCommand("EasyPayDeleteTransaction", this.con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = login;

            try
            {
                this.con.Open();
                com.ExecuteNonQuery();
            }
            finally
            {
                this.con.Close();
            }
        }

        /// <summary>
        /// Gets a list of the authorities that have data for export to the hub.
        /// </summary>
        /// <returns>A <see cref="SqlDataReader"/></returns>
        public SqlDataReader GetAuthoritiesForExport()
        {
            SqlCommand com = new SqlCommand("EasyPayAutoritiesNeedingExport", this.con);
            com.CommandType = CommandType.StoredProcedure;

            this.con.Open();
            SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection);

            return reader;
        }

        /// <summary>
        /// Gets the export data for a particular authority.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>A <see cref="SqlDataReader"/></returns>
        // 2013-07-24 add parameter lastUser by Henry
        public SqlDataReader GetExportData(int autIntNo, string fileName, string lastUser)
        {
            SqlCommand com = new SqlCommand("EasyPayGetExportData", this.con);
            com.CommandType = CommandType.StoredProcedure;
            com.CommandTimeout = 300;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@FileName", SqlDbType.VarChar, 255).Value = fileName;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            this.con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        /// <summary>
        /// Gets the EasyPay transactions for an authority.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>A <see cref="SqlDataReader"/></returns>
        public SqlDataReader GetEasyPayTransactions(int autIntNo, string fileName)
        {
            SqlCommand cmd = new SqlCommand("EasyPayExportData", this.con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 120;

            cmd.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 255).Value = fileName;

            this.con.Open();
            return cmd.ExecuteReader(CommandBehavior.CloseConnection);
        }

        /// <summary>
        /// Processes feedback the payments.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="sb">The sb.</param>
        /// <param name="applicationName">Name of the application.</param>
        public void ProcessPayments(FineExchangeFeedbackFile file, ref StringBuilder sb, string applicationName)
        {
            FeedbackAuthority authority = (FeedbackAuthority)file.Authority;

            SqlCommand com = new SqlCommand();
            com.Connection = this.con;

            RemotePaymentDB db = new RemotePaymentDB(this.connectionString);

            // Process EasyPay payments
            //TODO remove the Easypay section at some point - everything seems to be in the Payfine section
            com.CommandType = CommandType.StoredProcedure;

            // Declare temp variables
            CashType cashType = CashType.EasyPay;
            decimal originalAmount = 0M;

            foreach (EasyPayTransaction payment in authority.EasyPayPayments)
            {
                // Check if the payment has already been recorded
                string strPaymentExists = db.CheckIfPaymentExists(payment.NoticeID);
                if (strPaymentExists == "F")
                {
                    sb.Append(string.Format("The Notice (ID: {0}) doesn't exist.\r\n", payment.NoticeID));
                    continue;
                }
                if (strPaymentExists == "N")
                {
                    sb.Append(string.Format("The Ticket ({0}) has already been paid.\r\n", payment.NoticeID));
                    continue;
                }
                else if (strPaymentExists == "D")
                {
                    sb.Append(string.Format("The Notice (ID: {0}) has been duplicately paid.\r\n", payment.NoticeID));
                    continue;
                }

                // Get original payment amount
                originalAmount = db.GetOriginalFineAmount(string.Empty, payment.NoticeID);
                if (originalAmount != payment.Amount)
                {
                    throw new NotImplementedException("The EasyPay specific payment import section in EasyPayDB has not been implemented.");

                    sb.Append(string.Format(CultureInfo.InvariantCulture,"Ticket ID: {0}, has a different fine amount (R {1:N}) in Opus to that which was paid on {2} (R {3:N}).",
                        payment.NoticeID, originalAmount, cashType, payment.Amount));

                    try
                    {
                        if (this.SendEmail != null)
                        {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.Append(string.Format("There was a payment mismatch detected by the {0} ", applicationName));
                            sb2.Append("while processing EasyPay Number:");
                            sb2.Append(payment.EasyPayNumber);
                            sb2.Append(". The amount in Opus was R ");
                            //update by Rachel 20140812 for 5337
                            //sb2.Append(originalAmount.ToString("N"));
                            sb2.Append(originalAmount.ToString("N",CultureInfo.InvariantCulture));
                            //end update by Rachel 20140812 for 5337
                            sb2.Append(", while the amount paid at ");
                            sb2.Append(cashType.ToString());
                            sb2.Append(" was R ");
                            //update by Rachel 20140812 for 5337
                            //sb2.Append(payment.Amount.ToString("N"));
                            sb2.Append(payment.Amount.ToString("N", CultureInfo.InvariantCulture));
                            //end update by Rachel 20140812 for 5337
                            sb2.Append(".\n\nThis problem needs to be rectified manually.\n\nRegards\n\n");
                            sb2.Append(applicationName);

                            this.SendEmail(this, new EmailEventArgs(string.Format("Payment discrepancy from {0}", cashType), sb2.ToString()));
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex);
                    }
                }

                // TODO: Import and Receipt EasyPay payments
            }

            //dls 090701 - moved the udpate of the SumComments to the SP SummonsChargeUpdatePayment which is called from ProcessThaboPayment
            // 20090531 tf Update column SumComments in Summons
            //SummonsDB summonsDb = new SummonsDB(this.connectionString);

            // Process PayFine payments
            foreach (FeedbackPayment payment in authority.Payments)
            {
                // Check if the payment has already been recorded
                string strPaymentExists = db.CheckIfPaymentExists(payment.TicketNo);
                if (strPaymentExists == "F")
                {
                    sb.Append(string.Format("The Ticket ({0}) doesn't exist.\r\n", payment.TicketNo));
                    continue;
                }
                if (strPaymentExists == "N")
                {
                    sb.Append(string.Format("The Ticket ({0}) has already been paid.\r\n", payment.TicketNo));
                    continue;
                }
                if (strPaymentExists == "D")
                {
                    sb.Append(string.Format("The Ticket ({0}) has been paid via a duplicate payment.\r\n", payment.TicketNo));
                    //Notice number = AutCode eg. ��ST�� + sequence number. 
                    payment.TicketNo = authority.AuthorityCode + payment.TicketNo.Substring(2, 6);

                    this.ProcessRejectedPayment(payment, authority, ref sb, applicationName);
                    continue;
                }

                // mrs 20081003 3970 we have a new payment type to handle (PEN) caused by a remote payment being received but not able to process
                if (payment.Type == "PEN")
                {
                    //process remote payment pending
                    this.ProcessPaymentPending(payment, authority, applicationName, ref sb);

                    //update by rachel 20140808 for 5337
                    //sb.Append(string.Format("The Ticket ({0}) has a remote payment of R {1:N} pending - Duplicate payment processing changes have not yet been deployed to the LA - transaction has been ignored.\r\n", payment.TicketNo, payment.Amount));
                    sb.Append(string.Format(CultureInfo.InvariantCulture,"The Ticket ({0}) has a remote payment of R {1:N} pending - Duplicate payment processing changes have not yet been deployed to the LA - transaction has been ignored.\r\n", payment.TicketNo, payment.Amount));
                    //end update by rachel 20140808 for 5337
                    continue;
                }

                // Get current fine amount in the system
                originalAmount = db.GetOriginalFineAmount(payment.TicketNo, 0);

                // Only check for mismatched payments if this is a normal payment
                if (payment.Action == ActionPayment.Normal && originalAmount != payment.Amount)
                {
                    //update by rachel 20140808 for 5337
                    //sb.Append(string.Format("Ticket ID: {0}, has a different fine amount (R {1:N}) in Opus to that which was paid at {2} (R {3:N}).",payment.TicketNo, originalAmount, cashType, payment.Amount));                    
                    sb.Append(string.Format(CultureInfo.InvariantCulture, "Ticket ID: {0}, has a different fine amount (R {1:N}) in Opus to that which was paid at {2} (R {3:N}).", payment.TicketNo, originalAmount, cashType, payment.Amount));
                    //end update by rachel 20140808 for 5337

                    try
                    {
                        if (this.SendEmail != null)
                        {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.Append(string.Format("There was a payment mismatch detected by the {0} ", applicationName));
                            sb2.Append("while processing ticket number:");
                            sb2.Append(payment.TicketNo);
                            sb2.Append(". The amount in Opus was R ");
                            //update by Rachel 20140812 for 5337
                            //sb2.Append(originalAmount.ToString("N"));
                            sb2.Append(originalAmount.ToString("N",CultureInfo.InvariantCulture));
                            //end update by Rachel 20140812 for 5337
                            sb2.Append(", while the amount paid at ");
                            sb2.Append(cashType.ToString());
                            sb2.Append(" was R ");
                            //update by Rachel 20140812 for 5337
                            //sb2.Append(payment.Amount.ToString("N"));
                            sb2.Append(payment.Amount.ToString("N",CultureInfo.InvariantCulture));
                            //end update by Rachel 20140812 for 5337
                            sb2.Append(".\n\nThis problem needs to be rectified manually.\n\nRegards\n\n");
                            sb2.Append(applicationName);

                            this.SendEmail(this, new EmailEventArgs(string.Format("Payment discrepancy from {0}", cashType), sb2.ToString()));

                            continue;
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex);
                        sb.Append(ex);
                    }
                }

                // Check the payment action and process
                switch (payment.Action)
                {
                    case ActionPayment.Normal:
                        this.ProcessNormalPayment(payment, authority, applicationName, ref sb);
                        break;

                    case ActionPayment.Rejected:
                        this.ProcessRejectedPayment(payment, authority, ref sb, applicationName);
                        break;

                    case ActionPayment.Representation:
                        //dls 081113 - if the payment has already been rep'd at the LA, but there was a delay in the reduced amount getting sent to Thabo
                        //             and the incoming rep amount = the current amount for the notice, process it as a normal payment and don't do a rep

                        if (originalAmount != payment.Amount)
                            this.ProcessRepresentationPayment(payment, authority, applicationName, originalAmount, ref sb);
                        else
                            this.ProcessNormalPayment(payment, authority, applicationName, ref sb);

                        break;

                    case ActionPayment.Hybrid:
                        this.ProcessHybridPayment(payment, authority, applicationName, originalAmount, ref sb);
                        break;
                }

                //dls 090701 - moved the udpate of the SumComments to the SP SummonsChargeUpdatePayment which is called from ProcessThaboPayment
                //summonsDb.UpdateSummonsComments(payment.TicketNo, "Case finalized �C payment of R rrrr.cc was received on yyyy/mm/dd HH:MM (receipt # 999999)", applicationName);
            }
        }

        public void ProcessHybridPayment(FeedbackPayment payment, FeedbackAuthority authority, string applicationName, decimal originalAmount, ref StringBuilder sb)
        {
            // Create a new payment for the difference 
            FeedbackPayment suspensePayment = (FeedbackPayment)payment.Clone();
            suspensePayment.Amount = payment.Amount - originalAmount;

            // Pass it to the rejection process
            //dls 081114 - surely we need to use the Suspense payment amount here and not the original amount????
            //bool failed = this.ProcessRejectedPayment(payment, authority, ref sb, applicationName);
            bool failed = this.ProcessRejectedPayment(suspensePayment, authority, ref sb, applicationName);

            //dls 081114 - cannot process the 'Normal' portion of the payment if the suspense portion has failed
            if (!failed)
            {
                // Update the payment amount to the original amount
                payment.Amount = originalAmount;

                // Process the balance as a normal payment
                this.ProcessNormalPayment(payment, authority, applicationName, ref sb);
            }
            else
            {
                sb.Append("Unable to process the notice payment portion of the hybrid payment for ticket no: ");
                sb.Append(payment.TicketNo);
                sb.Append("\r\n");
            }
        }

        public void ProcessRepresentationPayment(FeedbackPayment payment, FeedbackAuthority authority, string applicationName, decimal originalAmount, ref StringBuilder sb)
        {
            //jerry 2011-2-14 changed to apply multiple charge
            decimal representationAmount = originalAmount - payment.Amount;

            // Process the representation
            RepresentationDB db = new RepresentationDB(connectionString);

            //dls 081113 - add a new RepAction type = T that will allow status = 954 to be rep'd (currently not allowed with the RepAction = R option)
            //SqlDataReader reader = db.NewSearchRepresentations(authority.ID, payment.TicketNo, 0, "R", "CDCourtRollCreatedDate");
            string repAction = "T";

            SqlDataReader reader = db.NewSearchRepresentations(authority.ID, payment.TicketNo, 0, repAction, "CDCourtRollCreatedDate");

            if (!reader.HasRows)
            {
                sb.Append("Unable to retrieve the ticket's details for representation: ");
                sb.Append(payment.TicketNo);
                sb.Append("\r\n");
                reader.Dispose();
                return;
            }

            // Read the search details
            //reader.Read();
            while (reader.Read())//jerry 2011-2-14 changed
            {
                int chgIntNo = Helper.GetReaderValue<int>(reader, "ChgIntNo");
                int csCode = Helper.GetReaderValue<int>(reader, "RepCSCode");

                //dls 080130 - there is no rep yet, so the rep fields are null. Need to get the data from the Driver fields

                //string name = Helper.GetReaderValue<string>(reader, "RepOffenderName");
                //string address = Helper.GetReaderValue<string>(reader, "RepOffenderAddress");
                //string phone = Helper.GetReaderValue<string>(reader, "RepOffenderTelNo");

                string name = string.Empty;
                if (!reader["Name"].Equals(DBNull.Value))
                    Helper.GetReaderValue<string>(reader, "Name");

                StringBuilder sb1 = new StringBuilder();
                if (!reader["DrvPoAdd1"].Equals(DBNull.Value))
                    sb1.Append(reader["DrvPoAdd1"] + "\n");
                if (!reader["DrvPoAdd2"].Equals(DBNull.Value))
                    sb1.Append(reader["DrvPoAdd2"] + "\n");
                if (!reader["DrvPoAdd3"].Equals(DBNull.Value))
                    sb1.Append(reader["DrvPoAdd3"] + "\n");
                if (!reader["DrvPoAdd4"].Equals(DBNull.Value))
                    sb1.Append(reader["DrvPoAdd4"] + "\n");
                if (!reader["DrvPoAdd5"].Equals(DBNull.Value))
                    sb1.Append(reader["DrvPoAdd5"] + "\n");
                if (!reader["DrvPoCode"].Equals(DBNull.Value))
                    sb1.Append(reader["DrvPoCode"]);
                string address = sb1.ToString().Trim();

                string phone = string.Empty;
                bool isSummons = Helper.GetReaderValue<bool>(reader, "IsSummons");

                //jerry 2011-2-14 add
                decimal chgRevFineAmount = Helper.GetReaderValue<decimal>(reader, "ChgRevFineAmount");
                //reader.Close();
                //reader.Dispose();

                string details = "On-the-fly representation processed by the Thabo Exporter";
                string official = "Administration Department";

                // Make the Representation
                string errMsg = "";

                //dls 081113 - add a new RepAction type = T that will allow status = 954 to be rep'd (currently not allowed with the RepAction = R option)
                int repIntNo = db.NewUpdateRepresentation(0, chgIntNo, details,
                    DateTime.Now, name, address, phone, official, applicationName,
                    authority.ID, csCode, chgRevFineAmount, "N", "R", ref errMsg, isSummons);

                if (repIntNo == -2)
                {
                    sb.Append("There was a problem lodging the representation: ");
                    sb.Append(errMsg);
                    sb.Append("\r\n");

                    reader.Close();
                    reader.Dispose();
                    return;
                }
                //Jerry 2012-11-01 add
                else if (repIntNo == -11)
                {
                    sb.Append("There was a problem lodging the representation: ");
                    sb.Append(string.Format("The {0} has been corrected so you can't do any representation on it.", isSummons ? "Summons" : "Notice"));
                    sb.Append("\r\n");

                    reader.Close();
                    reader.Dispose();
                    return;
                }

                // Get the Representation details with the new Charge RowVersion
                ChargeDB chgDb = new ChargeDB(connectionString);
                ChargeDetails charge = chgDb.GetChargeDetails(chgIntNo);

                string errMessage = string.Empty;

                int noOfDaysForPayment = GetNoOfDaysForPayment(authority.ID, applicationName);

                //jerry 2011-2-14 add
                representationAmount = representationAmount - chgRevFineAmount;
                decimal resetChgAmout;
                if (representationAmount >= 0)
                {
                    resetChgAmout = 0;
                }
                else
                {
                    resetChgAmout = Math.Abs(representationAmount);
                }

                // Make the representation decision
                Int32 result = db.DecideRepresentation(repIntNo, isSummons ? SUMMONS_REPCODE_REDUCTION : CHANGE_FINE_REP_CODE, null, official, details, DateTime.Now,
                    applicationName, chgIntNo, resetChgAmout, csCode, charge.RowVersion, chgRevFineAmount, "N", details, "O", isSummons, "N", ref errMessage, noOfDaysForPayment);

                if (result < 0)
                {
                    sb.Append("There was a problem processing the representation decision for ");
                    sb.Append(payment.TicketNo);
                    sb.Append("\rError: ");
                    sb.Append(errMessage);
                    sb.Append("\r\n");

                    reader.Close();
                    reader.Dispose();
                    return;
                }

                if (representationAmount <= 0)
                {
                    break;
                }
            }

            reader.Close();
            reader.Dispose();

            // Process the payment as a normal payment
            this.ProcessNormalPayment(payment, authority, applicationName, ref sb);
        }

        private int GetNoOfDaysForPayment(int autIntNo, string applicationName)
        {
            AuthorityRulesDetails arule = new AuthorityRulesDetails();
            arule.AutIntNo = autIntNo;
            arule.ARCode = "4660";
            arule.LastUser = applicationName;

            DefaultAuthRules paymentRule = new DefaultAuthRules(arule, this.connectionString);
            KeyValuePair<int, string> payment = paymentRule.SetDefaultAuthRule();

            return payment.Key;
        }

        public bool ProcessRejectedPayment(FeedbackPayment payment, FeedbackAuthority authority, ref StringBuilder sb, string applicationName)
        {
            // dls 081114 - need to do a few extra tasks here as the Suspense Notice is created with no reference back to the original ticketNo, if we know what it is
            // So create a new proc (needs a transaction, so must go into the same proc) that will do the following:
            // 1. Create the Dummy Suspense Account
            // 2. If it really is a dummy notice or we don't recognise the number, leave it at that
            // 3. If it is a valid ticket no, but the amount was wrong
            //    a) Clear the remote payment pending flag on the Charge and set it back to the previous status
            //    b) insert the ticket no. details into the Suspense Account Dummy Notice
            // 4. Create a receipt for the Dummy Notice using the normal processing route

            bool failed = false;

            SqlCommand cmd = new SqlCommand("SuspenseAccountCreateDummyForValidNotice", this.con);
            
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = authority.ID;
            cmd.Parameters.Add("@ConCode", SqlDbType.Char, 2).Value = "SI";
            cmd.Parameters.Add("@NoticeNo", SqlDbType.VarChar, 50).Value = payment.TicketNo;
            cmd.Parameters.Add("@PaymentDate", SqlDbType.SmallDateTime, 4).Value = payment.PaymentDate;
            cmd.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = payment.Amount;
            cmd.Parameters.Add("@Bank", SqlDbType.VarChar, 30).Value = payment.PartnerID;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = applicationName;
            cmd.Parameters.Add("@CashType", SqlDbType.Char, 1).Value = Helper.GetCashTypeChar(payment.Source);

            try
            {
                this.con.Open();
                cmd.CommandTimeout = 60;

                SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                //dls 081114 - only one result set is returned here
                //reader.NextResult();

                while (reader.Read())
                {
                    int rctIntNo = (int)reader[0];
                    string errMessage = reader[1].ToString();

                    if (rctIntNo < 1)
                    {
                        sb.Append("ERROR: There was a problem creating a suspense a/c and/or receipt for ticket number: ");
                        sb.Append(payment.TicketNo);
                        sb.Append(".\nOther details:\nPayment Date:\t");
                        sb.Append(payment.PaymentDate.ToLongDateString());
                        sb.Append("\nAmount:\t");
                        //update by rachel 20140808 for 5337
                        //sb.Append(payment.Amount.ToString());
                        sb.Append(payment.Amount.ToString(CultureInfo.InvariantCulture));
                        //end update by rachel 20140808 for 5337
                        sb.Append("\nPartner ID:\t");
                        sb.Append(payment.PartnerID);
                        //dls 081114 - the receipt no is generated in the transaction, so n/a here
                        //sb.Append("\nReceipt Number:\t");         
                        //sb.Append(receiptNo);
                        sb.Append("\nCash Type:\t");
                        sb.Append(payment.Source.ToString());
                        sb.Append("\nError:\t");
                        sb.Append(errMessage);
                        sb.Append("\r\n");
                    }
                    else
                    {
                        //sb.Append("\tSuccessfully created a receipt (");
                        //sb.Append(receiptNo);
                        //sb.Append(", RctIntNo: ");
                        ////sb.Append(response.ToString());
                        //sb.Append(rctIntNo.ToString());
                        //sb.Append(") for ticket number ");
                        //sb.Append(payment.TicketNo);
                        sb.Append(errMessage);
                        sb.Append("\r\n");
                    }
                }
            }
            catch (Exception e)
            {
                sb.Append("ERROR: ");
                sb.Append(e.Message);
                sb.Append("\nERROR: There was a problem creating a suspense a/c and/or receipt for ticket number: ");
                sb.Append(payment.TicketNo);
                sb.Append(".\nOther details:\nPayment Date:\t");
                sb.Append(payment.PaymentDate.ToLongDateString());
                sb.Append("\nAmount:\t");
                //update by rachel 20140808 for 5337
                //sb.Append(payment.Amount.ToString());
                sb.Append(payment.Amount.ToString(CultureInfo.InvariantCulture));
                //end update by rachel 20140808 for 5337
                sb.Append("\nPartner ID:\t");
                sb.Append(payment.PartnerID);
                //dls 081114 - the receipt no is generated in the transaction, so n/a here
                //sb.Append("\nReceipt Number:\t");
                //sb.Append(receiptNo);
                sb.Append("\nCash Type:\t");
                sb.Append(payment.Source.ToString());
                sb.Append("\r\n");
            }
            finally
            {
                this.con.Close();
            }

            return failed;
            
        }

        public void ProcessNormalPayment(FeedbackPayment payment, FeedbackAuthority authority, string applicationName, ref StringBuilder sb)
        {            
            int maxChargeStatus = auth.CheckMaximumStatusRule(authority.ID, applicationName);

            //dls 100204 - move getting of next receipt no into the SP
            //// Create a receipt number
            //TranNoDB tranNo = new TranNoDB(this.connectionString);
            //int tNumber = tranNo.GetNextTranNoByType("REC", authority.ID, applicationName, 99999999);
            //if (tNumber <= 0)
            //{
            //    sb.Append("An invalid receipt transaction number was returned.\r\n");
            //    sb.Append(payment.TicketNo);
            //    sb.Append(".\n");
            //    return;
            //}
            ////dls 080412 - increase lenght of receipt no. to 6 digits
            //string receiptNo = string.Format("{0:000000}", tNumber);

            // Create the payment
			// mrs 20081008 new spin on the payment process - can be for a 954 = remote payment pending. The normal rules will reject this receipt. Change in proc            
			SqlCommand cmd = new SqlCommand("ProcessThaboPayment", this.con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 600; // FBJ (2010-05-23): 600 = 10 muinutes
            cmd.Parameters.Clear();

            cmd.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = authority.ID;
            cmd.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = payment.TicketNo;
            cmd.Parameters.Add("@PaymentDate", SqlDbType.SmallDateTime, 4).Value = payment.PaymentDate;
            cmd.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = payment.Amount;
            cmd.Parameters.Add("@Bank", SqlDbType.VarChar, 30).Value = payment.PartnerID;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = applicationName;
            //com.Parameters.Add("@ReceiptNo", SqlDbType.VarChar, 10).Value = receiptNo;
            cmd.Parameters.Add("@CashType", SqlDbType.Char, 1).Value = Helper.GetCashTypeChar(payment.Source);
            cmd.Parameters.Add("@MaxChargeStatus", SqlDbType.Int, 4).Value = maxChargeStatus;
            //dls 080130 - add a try catch to pick up exceptions

            try
            {
                this.con.Open();
                //int response = (int)com.ExecuteScalar();
                //if (response < 1)
                //{

                //dls 080130 - the code is returning multiple recordsets - the first related to the Authority rule, and the second one contains the RctIntNo

                //there are now additional rows from the Summons proc as well, but the authority rule has been removed. There can be up to 3 charges/sumcharge rows

                SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader != null)
                {
                    //reader.NextResult();
                    DataSet ds = new DataSet();
                    ds.Tables.Add("1");
                    ds.Tables.Add("2");
                    ds.Tables.Add("3");
                    ds.Tables.Add("4");
                    if (reader != null)
                        ds.Load(reader, LoadOption.OverwriteChanges, new string[] { "1", "2", "3", "4" });

                    //DataTable dt = ds.Tables[0];
                    //if (ds.Tables[1].Rows.Count > 0)
                    //    dt = ds.Tables[1];

                    foreach (DataTable dt in ds.Tables)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr[0].ToString().Equals("Summons"))
                                continue;

                            int rctIntNo = 0;

                            if (int.TryParse(dr[0].ToString(), out rctIntNo))
                                rctIntNo = (int)dr[0];
                            else
                                continue;

                            string errMessage = dr[1].ToString();

                            if (rctIntNo < 1)
                            {
                                sb.Append("ERROR: There was a problem creating a receipt for ticket number: ");
                                sb.Append(payment.TicketNo);
                                sb.Append(".\nOther details:\nPayment Date:\t");
                                sb.Append(payment.PaymentDate.ToLongDateString());
                                sb.Append("\nAmount:\t");
                                //update by rachel 20140808 for 5337
                                //sb.Append(payment.Amount.ToString());
                                sb.Append(payment.Amount.ToString(CultureInfo.InvariantCulture));
                                //end update by rachel 20140808 for 5337
                                sb.Append("\nPartner ID:\t");
                                sb.Append(payment.PartnerID);
                                sb.Append("\nReceipt Number:\t");
                                //sb.Append(receiptNo);
                                sb.Append("\nCash Type:\t");
                                sb.Append(payment.Source.ToString());
                                sb.Append("\nError:\t");
                                sb.Append(errMessage);
                                sb.Append("\r\n");
                            }
                            else
                            {
                                sb.Append("\tSuccessfully created a receipt (");
                                //sb.Append(receiptNo);
                                sb.Append(dr[2].ToString());
                                sb.Append(", RctIntNo: ");
                                //sb.Append(response.ToString());
                                sb.Append(rctIntNo.ToString());
                                sb.Append(") for ticket number ");
                                sb.AppendLine(payment.TicketNo);
                            }
                        }
                    }
                }

                reader.Dispose();
            }
            catch (Exception e)
            {
                sb.Append("ERROR in Normal Payment (Exception Message): ");
                sb.Append(e.Message);
                sb.Append("\nERROR: There was a problem creating a receipt for ticket number: ");
                sb.Append(payment.TicketNo);
                sb.Append(".\nOther details:\nPayment Date:\t");
                sb.Append(payment.PaymentDate.ToLongDateString());
                sb.Append("\nAmount:\t");
                //update by rachel 20140808 for 5337
                //sb.Append(payment.Amount.ToString());
                sb.Append(payment.Amount.ToString(CultureInfo.InvariantCulture));
                //end update by rachel 20140808 for 5337
                sb.Append("\nPartner ID:\t");
                sb.Append(payment.PartnerID);
                //sb.Append("\nReceipt Number:\t");
                //sb.Append(receiptNo);
                sb.Append("\nCash Type:\t");
                sb.AppendLine(payment.Source.ToString());
                sb.AppendLine(e.StackTrace);
            }
            finally
            {
                this.con.Close();
            }
        }

        public void ProcessPaymentPending(FeedbackPayment payment, FeedbackAuthority authority, string applicationName, ref StringBuilder sb)
        {
            int maxChargeStatus = auth.CheckMaximumStatusRule(authority.ID, applicationName);

            //mrs 20081007 3970 new procedure to lock notice when a funny easypay payment has been received so that no further payments can be processed
            //  no receipt created so we don't have reversal complications later

            // Create the payment pending
            SqlCommand com = new SqlCommand("ProcessThaboPaymentPending", this.con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Clear();

            com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = payment.TicketNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = applicationName;
            com.Parameters.Add("@CashType", SqlDbType.Char, 1).Value = Helper.GetCashTypeChar(payment.Source);
            com.Parameters.Add("@MaxChargeStatus", SqlDbType.Int, 4).Value = maxChargeStatus;

            try
            {
                this.con.Open();
                //dls 080130 - the code is returning multiple recordsets - the first related to the Authority rule, and the second one contains the RctIntNo
                SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection);
                reader.NextResult();

                while (reader.Read())
                {
                    int notIntNo = (int)reader[0];

                    if (notIntNo < 1)
                    {
                        sb.Append("ERROR: There was a problem processing a payment pending transaction on: ");
                        sb.Append(payment.TicketNo);
                        sb.Append("\nPartner ID:\t");
                        sb.Append(payment.PartnerID);
                        sb.Append("\nCash Type:\t");
                        sb.Append(payment.Source.ToString());
                        sb.Append("\r\n");
                    }
                    else
                    {
                        sb.Append("\tSuccessfully created a payment pending transaction (");
                        sb.Append(") for ticket number ");
                        sb.Append(payment.TicketNo);
                        sb.Append("\r\n");
                    }
                }

                reader.Dispose();
            }
            catch (Exception e)
            {
                sb.Append("ERROR: ");
                sb.Append(e.Message);
                sb.Append("\nERROR: There was a problem creating payment pending transaction for ticket number: ");
                sb.Append(payment.TicketNo);
                sb.Append(payment.Source.ToString());
                sb.Append("\r\n");
            }
            finally
            {
                this.con.Close();
            }
        }

        /// <summary>
        /// Gets the authority ID for a feedback file.
        /// </summary>
        /// <param name="file">The file.</param>
        public void GetAuthorityID(FineExchangeFeedbackFile file)
        {
            SqlCommand com = new SqlCommand("AuthorityGetIDFromCode", this.con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutCode", SqlDbType.VarChar, 3).Value = file.Authority.AuthorityCode.Trim();

            try
            {
                this.con.Open();
                SqlDataReader reader = com.ExecuteReader();
                if (reader != null)
                {
                    if (reader.Read())
                    {
                        file.Authority.ID = (int)reader["AutIntNo"];
                        file.Authority.Name = (string)reader["AutName"];
                    }
                    reader.Close();
                }
            }
            finally
            {
                this.con.Close();
                //this.con.Dispose();
            }
        }

        /// <summary>
        /// Updates the sent status of easy pay tran records that were contained in the file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        // 2013-07-24 add parameter lastUser by Henry
        public void UpdateSentStatus(string fileName, string lastUser)
        {
            SqlCommand cmd = new SqlCommand("EasyPayUpdateSentStatus", this.con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 255).Value = fileName;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            try
            {
                this.con.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                this.con.Close();
            }
        }

        /// <summary>
        /// Cleans up the expired easy pay transactions.
        /// </summary>
        /// <remarks>The connection timeout is set to 300 seconds (5 minutes)</remarks>
        public void CleanupExpiredEasyPayTransactions()
        {
            using (SqlCommand cmd = this.con.CreateCommand())
            {
                cmd.CommandTimeout = 1200; // 20 minutes, to make sure that it does clean everything up
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "EasyPayCleanupExpiredTransactions";
                cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = DateTime.Today;

                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
                finally
                {
                    con.Close();
                }
            }
        }
        /// <summary>
        /// 091201 FT Reset the sent status of easy pay tran records that were contained in the file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        // 2013-07-24 add parameter lastUser by Henry
        public void ResetSentStatus(string fileName, string lastUser)
        {
            SqlCommand cmd = new SqlCommand("EasyPayResetSentStatus", this.con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 255).Value = fileName;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                this.con.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                this.con.Close();
            }
        }

    }
}