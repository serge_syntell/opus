﻿using System;

namespace Stalberg.TMS
{
    public enum NoticeFilmType
    {
        //S341,
        //S54,
        //S56,
        H,
        M,
        Others
    }

    public class NoticeFilmTypes
    {
        public NoticeFilmTypes()
        {
        }

        public NoticeFilmType GetNoticeFilmType(object typeSource)
        {
            if (typeSource != null && typeSource != DBNull.Value)
            {
                string chargeType = Convert.ToString(typeSource).ToUpper();
                switch (chargeType)
                {
                    case "M":
                        //case "S56":
                        return NoticeFilmType.M;
                    case "H":
                        //case "S341":
                        return NoticeFilmType.H;
                    default:
                        return NoticeFilmType.Others;
                }
            }
            return NoticeFilmType.Others;
        }
    }
}


