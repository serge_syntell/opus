using System;    
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.DAL.Services;
using Stalberg.TMS.Data;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a Receipt
    /// </summary>
    public class ReceiptDetails
    {
        public int NotIntNo;
        public string NotTicketNo;
        public string NotRegNo;
        public DateTime NotOffenceDate;
        public string Owner;
        public int ChgIntNo;
        public string ChgOffenceCode;
        public string ChgOffenceDescr;
        public double ChgFineAmount;
        public int CTIntNo;
        public string ChargeStatus;
    }

    public struct NoticeHasImage
    {
        private int notIntNo;
        private bool hasImage;

        public int NotIntNo
        {
            get { return notIntNo; }
            set { notIntNo = value; }
        }

        public bool HasImage
        {
            get { return hasImage; }
            set { hasImage = value; }
        }
    }

    /// <summary>
    /// Represents a Notice
    /// </summary>
    public class NoticeDetails
    {
        public int NotIntNo;
        public int AutIntNo;
        public string NotTicketNo;
        public string NotLocDescr;
        public string NotLocCode;
        public string NotSpeedLimit;
        public string NotLaneNo;
        public string NotCameraID;
        public string NotOfficerSName;
        public string NotOfficerInit;
        public string NotOfficerNo;
        public string NotOfficerGroup;
        public string NotRegNo;
        public int NotSpeed1;
        public int NotSpeed2;
        public string NotVehicleMake;
        public string NotVehicleType;
        public string NotVehicleMakeCode;
        public string NotVehicleTypeCode;
        public string NotOffenderType;
        public string NotFilmNo;
        public string NotFrameNo;
        public string NotRefNo;
        public string NotSeqNo;
        public DateTime NotOffenceDate;
        public string NotSource;
        public string NotRdTypeCode;
        public string NotRdTypeDescr;
        public string NotCourtNo;
        public string NotCourtName;
        public string NotTravelDirection;
        public string NotElapsedTime;
        public string NotOffenceType;
        public string NotNaTISIndicator;
        public string NotNaTISErrorFieldList;
        public string NotNaTISRegNo;
        public string NotVehicleRegisterNo;
        public DateTime NotVehicleLicenceExpiry;
        public DateTime NotDateCOO;
        public string NotClearanceCert;
        public string NotRegisterAuth;
        public string NotVehicleDescr;
        public string NotVehicleCat;
        public string NotNaTISVMCode;
        public string NotNaTISVTCode;
        public string NotVehicleUsage;
        public string NotVehicleColour;
        public string NotVINorChassis;
        public string NotEngine;
        public string NotStatutoryOwner;
        public string NotNatureOfPerson;
        public string NotProxyFlag;
        public DateTime NotPosted1stNoticeDate;
        public DateTime NotPosted2ndNoticeDate;
        public DateTime NotPostedSummonsDate;
        public DateTime NotPaymentDate;
        public DateTime NotIssue1stNoticeDate;
        public DateTime NotIssue2ndNoticeDate;
        public DateTime NotPrint1stNoticeDate;
        public DateTime NotPrint2ndNoticeDate;
        public DateTime NotPrintSummonsDate;
        public string AutName;
        public string IssuedBy;
        public string CrtPaymentAddr1;
        public string CrtPaymentAddr2;
        public string CrtPaymentAddr3;
        public string CrtPaymentAddr4;
        public DateTime NotLoadDate;
        public string NotNatisINPName;
        public DateTime NotNatisINPDate;
        public string NotNatisOUTName;
        public DateTime NotNatisOUTDate;
        public string NotCiprusPrintReqName;
        public DateTime NotCiprusPrintReqDate;
        //public string EasyPayNo;
        public string ParkMeterNo;
        public string LastUser;
        public string NotSendTo;
        public string NotCamSerialNo;
        public string NotEasyPayNumber;
        public string ExportFlag;
        public string Not2ndNoticePrintFile;
        public string NotCiprusPIContraventionFile;
        public DateTime NotCiprusPIContraventionDate;
        public int NotCiprusPIProcessCode;
        public int NotCiprusPISeverityCode;
        public string NotCiprusPIProcessDescr;
        public string NotNatisProxyIndicator;
        public string NotNewOffender;
        public string NotPosted1stNoticeUser;
        public string NotPosted2ndNoticeUser;
        public string Not1stImageString;
        public string Not2ndImageString;
        public int NoticeStatus;
        public string NotFilmType;
        public bool IsVideo;
        public Int64 VehImageIntNo;
        public Int64 LicImageIntNo;
        //Tommi 2014-11-5 add 
        public DateTime SMSFor2ndNotice;
       // Heidi 2013-10-23 added NotStatisticsCode for Only search generated notice from DMS imported S341 Document
        public string NotStatisticsCode;
    }

    /// <summary>
    /// Represents the criteria of a query for Notice
    /// </summary>
    [Serializable]
    public class NoticeQueryCriteria
    {
        public int AutIntNo;
        public string ColumnName;
        public string Value;
        public DateTime DateSince;
        public int NoticeIntNo;
        public int MinStatus;

        /// <summary>
        /// Initializes a new instance of the <see cref="NoticeQueryCriteria"/> class.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="value">The value of the column.</param>
        /// <param name="dateSince">The date after which to search for notices.</param>
        public NoticeQueryCriteria(int autIntNo, string columnName, string value, string dateSince, int minStatus)
        {
            this.AutIntNo = autIntNo;
            this.ColumnName = columnName;
            //this.Value = value;

            //remove slashes and dashes from input value.
            this.Value = value.Replace("/", "").Replace("-", "");

            DateTime dt;
            if (DateTime.TryParse(dateSince, out dt))
                this.DateSince = dt;
            else
                this.DateSince = new DateTime(2000, 1, 1);

            this.MinStatus = minStatus;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NoticeQueryCriteria"/> class.
        /// </summary>
        /// <param name="notIntNo">The not int no.</param>
        public NoticeQueryCriteria(int notIntNo)
        {
            this.NoticeIntNo = notIntNo;
        }

    }

    // Lists the phase that a Notice is in
    public enum NoticeStatus
    {
        First = 0,
        Second = 1,
        NoAOG = 2,
        NewOffender = 3,
        NewRegNo = 4
    }

    /// <summary>
    /// The notice DB class contains all the database access logic for retrieving notices
    /// </summary>
    public class NoticeDB
    {
        string mConstr = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:NoticeDB"/> class.
        /// </summary>
        /// <param name="vConstr">The database connection string.</param>
        public NoticeDB(string vConstr)
        {
            mConstr = vConstr;
        }

        /// <summary>
        /// Gets the notice details.
        /// </summary>
        /// <param name="NotIntNo">The not int no.</param>
        /// <returns>a Notice data structure</returns>
        public NoticeDetails GetNoticeDetails(int NotIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = NotIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            NoticeDetails myNoticeDetails = new NoticeDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myNoticeDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
                myNoticeDetails.NotTicketNo = result["NotTicketNo"].ToString();
                myNoticeDetails.NotCameraID = result["NotCameraID"].ToString();
                myNoticeDetails.NotFilmNo = result["NotFilmNo"].ToString();
                myNoticeDetails.NotFrameNo = result["NotFrameNo"].ToString();
                myNoticeDetails.NotLaneNo = result["NotLaneNo"].ToString();
                myNoticeDetails.NotLocDescr = result["NotLocDescr"].ToString();
                myNoticeDetails.NotLocCode = result["NotLocCode"].ToString();
                myNoticeDetails.NotOffenceDate = Convert.ToDateTime(result["NotOffenceDate"]);
                myNoticeDetails.NotOffenderType = result["NotOffenderType"].ToString();
                myNoticeDetails.NotOfficerSName = result["NotOfficerSName"].ToString();
                myNoticeDetails.NotOfficerInit = result["NotOfficerInit"].ToString();
                myNoticeDetails.NotOfficerGroup = result["NotOfficerGroup"].ToString();
                myNoticeDetails.NotOfficerNo = result["NotOfficerNo"].ToString();
                myNoticeDetails.NotRefNo = result["NotRefNo"].ToString();
                myNoticeDetails.NotRegNo = result["NotRegNo"].ToString();
                myNoticeDetails.NotSpeed1 = Convert.ToInt32(result["NotSpeed1"]);
                myNoticeDetails.NotSpeed2 = Convert.ToInt32(result["NotSpeed2"]);
                myNoticeDetails.NotSeqNo = result["NotSeqNo"].ToString();
                myNoticeDetails.NotSource = result["NotSource"].ToString();
                myNoticeDetails.NotSpeedLimit = result["NotSpeedLimit"].ToString();
                myNoticeDetails.NotVehicleMake = result["NotVehicleMake"].ToString();
                myNoticeDetails.NotVehicleType = result["NotVehicleType"].ToString();
                myNoticeDetails.NotVehicleMakeCode = result["NotVehicleMakeCode"].ToString();
                myNoticeDetails.NotVehicleTypeCode = result["NotVehicleTypeCode"].ToString();
                myNoticeDetails.NotRdTypeCode = result["NotRdTypeCode"].ToString();
                myNoticeDetails.NotRdTypeDescr = result["NotRdTypeDescr"].ToString();
                myNoticeDetails.NotElapsedTime = result["NotElapsedTime"].ToString();
                myNoticeDetails.NotTravelDirection = result["NotTravelDirection"].ToString();
                myNoticeDetails.NotCourtNo = result["NotCourtNo"].ToString();
                myNoticeDetails.NotCourtName = result["NotCourtName"].ToString();
                myNoticeDetails.NotOffenceType = result["NotOffenceType"].ToString();
                myNoticeDetails.NotNaTISIndicator = result["NotNaTISIndicator"].ToString();
                myNoticeDetails.NotNaTISErrorFieldList = result["NotNaTISErrorFieldList"].ToString();
                myNoticeDetails.NotNaTISRegNo = result["NotNaTISRegNo"].ToString();
                myNoticeDetails.NotVehicleRegisterNo = result["NotVehicleRegisterNo"].ToString();
                if (result["NotVehicleLicenceExpiry"] != System.DBNull.Value)
                    myNoticeDetails.NotVehicleLicenceExpiry = Convert.ToDateTime(result["NotVehicleLicenceExpiry"]);
                if (result["NotDateCOO"] != System.DBNull.Value)
                    myNoticeDetails.NotDateCOO = Convert.ToDateTime(result["NotDateCOO"]);
                myNoticeDetails.NotClearanceCert = result["NotClearanceCert"].ToString();
                myNoticeDetails.NotRegisterAuth = result["NotRegisterAuth"].ToString();
                myNoticeDetails.NotVehicleDescr = result["NotVehicleDescr"].ToString();
                myNoticeDetails.NotVehicleCat = result["NotVehicleCat"].ToString();
                myNoticeDetails.NotNaTISVMCode = result["NotNaTISVMCode"].ToString();
                myNoticeDetails.NotNaTISVTCode = result["NotNaTISVTCode"].ToString();
                myNoticeDetails.NotVehicleUsage = result["NotVehicleUsage"].ToString();
                myNoticeDetails.NotVehicleColour = result["NotVehicleColour"].ToString();
                myNoticeDetails.NotVINorChassis = result["NotVINorChassis"].ToString();
                myNoticeDetails.NotEngine = result["NotEngine"].ToString();
                myNoticeDetails.NotStatutoryOwner = result["NotStatutoryOwner"].ToString();
                myNoticeDetails.NotNatureOfPerson = result["NotNatureOfPerson"].ToString();
                myNoticeDetails.NotProxyFlag = result["NotProxyFlag"].ToString();
                if (result["NotPosted1stNoticeDate"] != System.DBNull.Value)
                    myNoticeDetails.NotPosted1stNoticeDate = Convert.ToDateTime(result["NotPosted1stNoticeDate"]);
                if (result["NotPosted2ndNoticeDate"] != System.DBNull.Value)
                    myNoticeDetails.NotPosted2ndNoticeDate = Convert.ToDateTime(result["NotPosted2ndNoticeDate"]);
                if (result["NotPostedSummonsDate"] != System.DBNull.Value)
                    myNoticeDetails.NotPostedSummonsDate = Convert.ToDateTime(result["NotPostedSummonsDate"]);
                if (result["NotPaymentDate"] != System.DBNull.Value)
                    myNoticeDetails.NotPaymentDate = Convert.ToDateTime(result["NotPaymentDate"]);
                if (result["NotIssue1stNoticeDate"] != System.DBNull.Value)
                    myNoticeDetails.NotIssue1stNoticeDate = Convert.ToDateTime(result["NotIssue1stNoticeDate"]);
                if (result["NotIssue2ndNoticeDate"] != System.DBNull.Value)
                    myNoticeDetails.NotIssue2ndNoticeDate = Convert.ToDateTime(result["NotIssue2ndNoticeDate"]);
                if (result["NotPrint1stNoticeDate"] != System.DBNull.Value)
                    myNoticeDetails.NotPrint1stNoticeDate = Convert.ToDateTime(result["NotPrint1stNoticeDate"]);
                if (result["NotPrint2ndNoticeDate"] != System.DBNull.Value)
                    myNoticeDetails.NotPrint2ndNoticeDate = Convert.ToDateTime(result["NotPrint2ndNoticeDate"]);
                if (result["NotPrintSummonsDate"] != System.DBNull.Value)
                    myNoticeDetails.NotPrintSummonsDate = Convert.ToDateTime(result["NotPrintSummonsDate"]);
                myNoticeDetails.AutName = result["AutName"].ToString();
                myNoticeDetails.IssuedBy = result["IssuedBy"].ToString();
                myNoticeDetails.CrtPaymentAddr1 = result["CrtPaymentAddr1"].ToString();
                myNoticeDetails.CrtPaymentAddr2 = result["CrtPaymentAddr2"].ToString();
                myNoticeDetails.CrtPaymentAddr3 = result["CrtPaymentAddr3"].ToString();
                myNoticeDetails.CrtPaymentAddr4 = result["CrtPaymentAddr4"].ToString();
                if (result["NotLoadDate"] != System.DBNull.Value)
                    myNoticeDetails.NotLoadDate = Convert.ToDateTime(result["NotLoadDate"]);
                myNoticeDetails.NotNatisINPName = result["NotNatisINPName"].ToString();
                if (result["NotNatisINPDate"] != System.DBNull.Value)
                    myNoticeDetails.NotNatisINPDate = Convert.ToDateTime(result["NotNatisINPDate"]);
                myNoticeDetails.NotNatisOUTName = result["NotNatisOUTName"].ToString();
                if (result["NotNatisOUTDate"] != System.DBNull.Value)
                    myNoticeDetails.NotNatisOUTDate = Convert.ToDateTime(result["NotNatisOUTDate"]);
                myNoticeDetails.NotCiprusPrintReqName = result["NotCiprusPrintReqName"].ToString();
                if (result["NotCiprusPrintReqDate"] != System.DBNull.Value)
                    myNoticeDetails.NotCiprusPrintReqDate = Convert.ToDateTime(result["NotCiprusPrintReqDate"]);
                //myNoticeDetails.EasyPayNo = result["NotEasyPayNumber"].ToString();
                myNoticeDetails.ParkMeterNo = result["ParkMeterNo"].ToString();
                myNoticeDetails.LastUser = result["LastUser"].ToString();
                myNoticeDetails.NotSendTo = result["NotSendTo"].ToString();
                myNoticeDetails.NotCamSerialNo = result["NotCamSerialNo"].ToString();
                myNoticeDetails.NotEasyPayNumber = result["NotEasyPayNumber"].ToString();
                myNoticeDetails.ExportFlag = result["ExportFlag"].ToString();
                myNoticeDetails.Not2ndNoticePrintFile = result["Not2ndNoticePrintFile"].ToString();
                myNoticeDetails.NotCiprusPIContraventionFile = result["NotCiprusPIContraventionFile"].ToString();
                if (result["NotCiprusPIContraventionDate"] != System.DBNull.Value)
                    myNoticeDetails.NotCiprusPIContraventionDate = Convert.ToDateTime(result["NotCiprusPIContraventionDate"]);
                if (result["NotCiprusPIProcessCode"] != System.DBNull.Value)
                    myNoticeDetails.NotCiprusPIProcessCode = Convert.ToInt32(result["NotCiprusPIProcessCode"]);
                if (result["NotCiprusPIProcessCode"] != System.DBNull.Value)
                    myNoticeDetails.NotCiprusPIProcessCode = Convert.ToInt32(result["NotCiprusPIProcessCode"]);
                myNoticeDetails.NotCiprusPIProcessDescr = result["NotCiprusPIProcessDescr"].ToString();
                myNoticeDetails.NotNatisProxyIndicator = result["NotNatisProxyIndicator"].ToString();
                myNoticeDetails.NotNewOffender = result["NotNewOffender"].ToString();
                myNoticeDetails.NotPosted1stNoticeUser = result["NotPosted1stNoticeUser"].ToString();
                myNoticeDetails.NotPosted2ndNoticeUser = result["NotPosted2ndNoticeUser"].ToString();
                //BD chnage to pass back notice images in string format
                myNoticeDetails.Not1stImageString = result["Not1stImageString"].ToString();
                myNoticeDetails.Not2ndImageString = result["Not2ndImageString"].ToString();
                myNoticeDetails.NoticeStatus = Convert.ToInt32(result["NoticeStatus"]);
                if (result["SMSFor2ndNotice"] != System.DBNull.Value)
                    myNoticeDetails.SMSFor2ndNotice = Convert.ToDateTime(result["SMSFor2ndNotice"]);
                // Oscar 20120315 add NotFilmType
                if (result["NotFilmType"] != DBNull.Value)
                    myNoticeDetails.NotFilmType = result["NotFilmType"].ToString();
                myNoticeDetails.IsVideo = Convert.ToInt32(result["IsVideo"])==1?true:false;
                myNoticeDetails.NotStatisticsCode = result["NotStatisticsCode"].ToString();
            }
            if (result.NextResult())
            {
                while (result.Read())
                {
                    myNoticeDetails.VehImageIntNo = (result["VehImageIntNo"] != DBNull.Value ? Convert.ToInt64(result["VehImageIntNo"]) : 0);
                    myNoticeDetails.LicImageIntNo = (result["LicImageIntNo"] != DBNull.Value ? Convert.ToInt64(result["LicImageIntNo"]) : 0);
                }
            }
            result.Close();//jerry 2011-12-30 add
            result.Dispose();
            return myNoticeDetails;
        }
        public NoticeDetails GetNoticeByFrameIntNo(int FrameIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("GetNoticeByFrameIntNo", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@frameIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = FrameIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            NoticeDetails myNoticeDetails = null;

            while (result.Read())
            {
                myNoticeDetails = new NoticeDetails();
                // Populate Struct using Output Params from SPROC
                myNoticeDetails.NotIntNo = Convert.ToInt32(result["NotIntNo"]);
                myNoticeDetails.NoticeStatus = Convert.ToInt32(result["NoticeStatus"]);
                myNoticeDetails.NotFrameNo = result["NotFrameNo"].ToString();
                myNoticeDetails.NotFilmNo = result["NotFilmNo"].ToString();
                myNoticeDetails.IsVideo = Convert.ToInt32(result["IsVideo"]) == 1 ? true : false;
            }
            result.Close();//jerry 2011-12-30 add
            result.Dispose();
            return myNoticeDetails;
        }
        /// <summary>
        /// Get notice By FrameIntNo  create by Heidi 2013-07-17 
        /// </summary>
        /// <param name="FrameIntNo"></param>
        /// <returns></returns>
        public NoticeDetails GetNoticeDataByFrameIntNo(int FrameIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("GetNoticeDataByFrameIntNo", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@frameIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = FrameIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            NoticeDetails myNoticeDetails = null;

            while (result.Read())
            {
                myNoticeDetails = new NoticeDetails();
                // Populate Struct using Output Params from SPROC
                myNoticeDetails.NotIntNo = Convert.ToInt32(result["NotIntNo"]);
                myNoticeDetails.NoticeStatus = Convert.ToInt32(result["NoticeStatus"]);
                myNoticeDetails.NotFrameNo = result["NotFrameNo"].ToString();
                myNoticeDetails.NotFilmNo = result["NotFilmNo"].ToString();
                myNoticeDetails.IsVideo = Convert.ToInt32(result["IsVideo"]) == 1 ? true : false;
                // Heidi 2013-10-23 added NotStatisticsCode for Only search generated notice from DMS imported S341 Document
                myNoticeDetails.NotStatisticsCode = result["NotStatisticsCode"].ToString();
            }
            result.Close();//jerry 2011-12-30 add
            result.Dispose();
            return myNoticeDetails;
        }

        /// <summary>
        /// Get notice By ENatisGuid  create by Heidi 2014-01-26 
        /// </summary>
        /// <param name="FrameIntNo"></param>
        /// <returns></returns>
        public string GetNoticeDataByENatisGuid(Guid eNatisGuid)
        {
            string str = "";
            string notIntNo = "";
            string drvIDNumber = "";
            string notOffenceDate = "";
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("GetNoticeDataByENatisGuid", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterENatisGuid = new SqlParameter("@ENatisGuid", SqlDbType.UniqueIdentifier);
            parameterENatisGuid.Value = eNatisGuid;
            myCommand.Parameters.Add(parameterENatisGuid);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            while (result.Read())
            {
                notIntNo = result["NotIntNo"].ToString();
                drvIDNumber = result["DrvIDNumber"].ToString();
                if (result["NotOffenceDate"] != null)
                {
                    notOffenceDate = Convert.ToDateTime(result["NotOffenceDate"]).ToString("yyyy/MM/dd");
                }
                else
                {
                    notOffenceDate = "";
                }
                str =  drvIDNumber + "," + notOffenceDate;
               
            }
           
            result.Close();//jerry 2011-12-30 add
            result.Dispose();
            return str;
        }

        /// <summary>
        /// Gets the notice details for receipt.
        /// </summary>
        /// <param name="notIntNo">The not int no.</param>
        /// <returns></returns>
        public ReceiptDetails GetNoticeDetailsForReceipt(int notIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeDetailForReceipt", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = notIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            ReceiptDetails myReceiptDetails = new ReceiptDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myReceiptDetails.NotIntNo = Convert.ToInt32(result["NotIntNo"]);
                myReceiptDetails.NotTicketNo = result["NotTicketNo"].ToString();
                myReceiptDetails.NotRegNo = result["NotRegNo"].ToString();
                myReceiptDetails.NotOffenceDate = Convert.ToDateTime(result["NotOffenceDate"]);
                myReceiptDetails.Owner = result["Owner"].ToString();
                myReceiptDetails.ChgIntNo = Convert.ToInt32(result["ChgIntNo"]);
                myReceiptDetails.ChgOffenceCode = result["ChgOffenceCode"].ToString();
                myReceiptDetails.ChgOffenceDescr = result["ChgOffenceDescr"].ToString();
                myReceiptDetails.ChgFineAmount = Convert.ToDouble(result["ChgFineAmount"]);
                myReceiptDetails.CTIntNo = Convert.ToInt32(result["CTIntNo"]);
                myReceiptDetails.ChargeStatus = result["ChargeStatus"].ToString();
                //myReceiptDetails.CSDescr = result["CSDescr"].ToString();
            }
            result.Close();
            return myReceiptDetails;
        }

        //dls 081003 - don't think this is used, and it shouldn't be!

        //public int AddNotice(int autIntNo, string notTicketNo, string notLocDescr, string notLocCode,
        //    int notSpeedLimit, string notLaneNo, int notSpeed1, int notSpeed2, string notCameraID, string notOfficerSName,
        //    string notOfficerInit, string notOfficerNo, string notOfficerGroup, string notRegNo,
        //    string notVehicleMake, string notVehicleType, string notVehicleMakeCode, string notVehicleTypeCode, string notOffenderType,
        //    string notFilmNo, string notFrameNo, string notRefNo, string notSeqNo, DateTime notOffenceDate, string notSource,
        //    string notRdTypeCode, string notRdTypeDescr, string notTravelDirection,
        //    string notCourtNo, string notCourtName, string notElapsedTime, string notOffenceType,
        //    string notNaTISIndicator, string notNaTISErrorFieldList, string notNaTISRegNo,
        //    string notVehicleRegisterNo, string notClearanceCert, string notRegisterAuth, string notVehicleDescr,
        //    string notVehicleCat, string notNaTISVMCode, string notNaTISVTCode, string notVehicleUsage, string notVehicleColour,
        //    string notVINorChassis, string notEngine, string notStatutoryOwner, string notNatureOfPerson,
        //    string notProxyFlag, DateTime notPaymentDate, string easyPayNo, string parkMeterNo, string autName, string issuedBy, string crtPaymentAddr1,
        //    string crtPaymentAddr2, string crtPaymentAddr3, string crtPaymentAddr4,
        //    string notDocType, string lastUser, string notSendTo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("NoticeAdd", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterNotTicketNo = new SqlParameter("@NotTicketNo", SqlDbType.VarChar, 50);
        //    parameterNotTicketNo.Value = notTicketNo;
        //    myCommand.Parameters.Add(parameterNotTicketNo);

        //    SqlParameter parameterNotLocDescr = new SqlParameter("@NotLocDescr", SqlDbType.VarChar, 60);
        //    parameterNotLocDescr.Value = notLocDescr;
        //    myCommand.Parameters.Add(parameterNotLocDescr);

        //    SqlParameter parameterNotLocCode = new SqlParameter("@NotLocCode", SqlDbType.VarChar, 15);
        //    parameterNotLocCode.Value = notLocCode;
        //    myCommand.Parameters.Add(parameterNotLocCode);

        //    SqlParameter parameterNotSpeedLimit = new SqlParameter("@NotSpeedLimit", SqlDbType.Int);
        //    parameterNotSpeedLimit.Value = notSpeedLimit;
        //    myCommand.Parameters.Add(parameterNotSpeedLimit);

        //    SqlParameter parameterNotLaneNo = new SqlParameter("@NotLaneNo", SqlDbType.Char, 1);
        //    parameterNotLaneNo.Value = notLaneNo;
        //    myCommand.Parameters.Add(parameterNotLaneNo);

        //    SqlParameter parameterNotCameraID = new SqlParameter("@NotCameraID", SqlDbType.VarChar, 15);
        //    parameterNotCameraID.Value = notCameraID;
        //    myCommand.Parameters.Add(parameterNotCameraID);

        //    SqlParameter parameterNotOfficer = new SqlParameter("@NotOfficerSName", SqlDbType.VarChar, 35);
        //    parameterNotOfficer.Value = notOfficerSName;
        //    myCommand.Parameters.Add(parameterNotOfficer);

        //    SqlParameter parameterNotOfficerInit = new SqlParameter("@NotOfficerInit", SqlDbType.VarChar, 5);
        //    parameterNotOfficerInit.Value = notOfficerInit;
        //    myCommand.Parameters.Add(parameterNotOfficerInit);

        //    SqlParameter parameterNotOfficerNo = new SqlParameter("@NotOfficerNo", SqlDbType.VarChar, 10);
        //    parameterNotOfficerNo.Value = notOfficerNo;
        //    myCommand.Parameters.Add(parameterNotOfficerNo);

        //    SqlParameter parameterNotOfficerGroup = new SqlParameter("@NotOfficerGroup", SqlDbType.Char, 3);
        //    parameterNotOfficerGroup.Value = notOfficerGroup;
        //    myCommand.Parameters.Add(parameterNotOfficerGroup);

        //    SqlParameter parameterNotRegNo = new SqlParameter("@NotRegNo", SqlDbType.VarChar, 10);
        //    parameterNotRegNo.Value = notRegNo;
        //    myCommand.Parameters.Add(parameterNotRegNo);

        //    SqlParameter parameterNotSpeed1 = new SqlParameter("@NotSpeed1", SqlDbType.Int);
        //    parameterNotSpeed1.Value = notSpeed1;
        //    myCommand.Parameters.Add(parameterNotSpeed1);

        //    SqlParameter parameterNotSpeed2 = new SqlParameter("@NotSpeed2", SqlDbType.Int);
        //    parameterNotSpeed2.Value = notSpeed2;
        //    myCommand.Parameters.Add(parameterNotSpeed2);

        //    SqlParameter parameterNotVehicleMake = new SqlParameter("@NotVehicleMake", SqlDbType.VarChar, 30);
        //    parameterNotVehicleMake.Value = notVehicleMake;
        //    myCommand.Parameters.Add(parameterNotVehicleMake);

        //    SqlParameter parameterNotVehicleType = new SqlParameter("@NotVehicleType", SqlDbType.VarChar, 30);
        //    parameterNotVehicleType.Value = notVehicleType;
        //    myCommand.Parameters.Add(parameterNotVehicleType);

        //    SqlParameter parameterNotVehicleMakeCode = new SqlParameter("@NotVehicleMakeCode", SqlDbType.VarChar, 3);
        //    parameterNotVehicleMakeCode.Value = notVehicleMakeCode;
        //    myCommand.Parameters.Add(parameterNotVehicleMakeCode);

        //    SqlParameter parameterNotVehicleTypeCode = new SqlParameter("@NotVehicleTypeCode", SqlDbType.VarChar, 3);
        //    parameterNotVehicleTypeCode.Value = notVehicleTypeCode;
        //    myCommand.Parameters.Add(parameterNotVehicleTypeCode);

        //    SqlParameter parameterNotOffenderType = new SqlParameter("@NotOffenderType", SqlDbType.Char, 1);
        //    parameterNotOffenderType.Value = notOffenderType;
        //    myCommand.Parameters.Add(parameterNotOffenderType);

        //    SqlParameter parameterNotFilmNo = new SqlParameter("@NotFilmNo", SqlDbType.VarChar, 20);
        //    parameterNotFilmNo.Value = notFilmNo;
        //    myCommand.Parameters.Add(parameterNotFilmNo);

        //    SqlParameter parameterNotFrameNo = new SqlParameter("@NotFrameNo", SqlDbType.VarChar, 20);
        //    parameterNotFrameNo.Value = notFrameNo;
        //    myCommand.Parameters.Add(parameterNotFrameNo);

        //    SqlParameter parameterNotRefNo = new SqlParameter("@NotRefNo", SqlDbType.VarChar, 20);
        //    parameterNotRefNo.Value = notRefNo;
        //    myCommand.Parameters.Add(parameterNotRefNo);

        //    SqlParameter parameterNotSeqNo = new SqlParameter("@NotSeqNo", SqlDbType.Char, 1);
        //    parameterNotSeqNo.Value = notSeqNo;
        //    myCommand.Parameters.Add(parameterNotSeqNo);

        //    SqlParameter parameterNotOffenceDate = new SqlParameter("@NotOffenceDate", SqlDbType.DateTime);
        //    parameterNotOffenceDate.Value = notOffenceDate;
        //    myCommand.Parameters.Add(parameterNotOffenceDate);

        //    SqlParameter parameterNotSource = new SqlParameter("@NotSource", SqlDbType.VarChar, 10);
        //    parameterNotSource.Value = notSource;
        //    myCommand.Parameters.Add(parameterNotSource);

        //    //SqlParameter parameterNotJPegNameA = new SqlParameter("@NotJPegNameA", SqlDbType.VarChar, 30);
        //    //parameterNotJPegNameA.Value = notJPegNameA;
        //    //myCommand.Parameters.Add(parameterNotJPegNameA);

        //    //SqlParameter parameterNotJPegNameB = new SqlParameter("@NotJPegNameB", SqlDbType.VarChar, 30);
        //    //parameterNotJPegNameB.Value = notJPegNameB;
        //    //myCommand.Parameters.Add(parameterNotJPegNameB);

        //    //SqlParameter parameterNotRegNoImage = new SqlParameter("@NotRegNoImage", SqlDbType.VarChar, 30);
        //    //parameterNotRegNoImage.Value = notRegNoImage;
        //    //myCommand.Parameters.Add(parameterNotRegNoImage);

        //    SqlParameter parameterNotRdTypeCode = new SqlParameter("@NotRdTypeCode", SqlDbType.SmallInt);
        //    parameterNotRdTypeCode.Value = notRdTypeCode;
        //    myCommand.Parameters.Add(parameterNotRdTypeCode);

        //    SqlParameter parameterNotRdTypeDescr = new SqlParameter("@NotRdTypeDescr", SqlDbType.VarChar, 30);
        //    parameterNotRdTypeDescr.Value = notRdTypeDescr;
        //    myCommand.Parameters.Add(parameterNotRdTypeDescr);

        //    SqlParameter parameterNotCourtNo = new SqlParameter("@NotCourtNo", SqlDbType.VarChar, 6);
        //    parameterNotCourtNo.Value = notCourtNo;
        //    myCommand.Parameters.Add(parameterNotCourtNo);

        //    SqlParameter parameterNotCourtName = new SqlParameter("@NotCourtName", SqlDbType.VarChar, 30);
        //    parameterNotCourtName.Value = notCourtName;
        //    myCommand.Parameters.Add(parameterNotCourtName);

        //    SqlParameter parameterNotTravelDirection = new SqlParameter("@NotTravelDirection", SqlDbType.Char, 1);
        //    parameterNotTravelDirection.Value = notTravelDirection;
        //    myCommand.Parameters.Add(parameterNotTravelDirection);

        //    SqlParameter parameterNotElapsedTime = new SqlParameter("@NotElapsedTime", SqlDbType.VarChar, 5);
        //    parameterNotElapsedTime.Value = notElapsedTime;
        //    myCommand.Parameters.Add(parameterNotElapsedTime);

        //    //dls 080930 - don't think this proc is used, but for future reference ==> len of NotOffenceType increased to 2
        //    SqlParameter parameterNotOffenceType = new SqlParameter("@NotOffenceType", SqlDbType.Char, 2);
        //    parameterNotOffenceType.Value = notOffenceType;
        //    myCommand.Parameters.Add(parameterNotOffenceType);

        //    SqlParameter parameterNaTISIndicator = new SqlParameter("@NotNaTISIndicator", SqlDbType.Char, 1);
        //    parameterNaTISIndicator.Value = notNaTISIndicator;
        //    myCommand.Parameters.Add(parameterNaTISIndicator);

        //    SqlParameter parameterNotNaTISErrorFieldList = new SqlParameter("@NotNaTISErrorFieldList", SqlDbType.VarChar, 255);
        //    parameterNotNaTISErrorFieldList.Value = notNaTISErrorFieldList;
        //    myCommand.Parameters.Add(parameterNotNaTISErrorFieldList);

        //    SqlParameter parameterNotNaTISRegNo = new SqlParameter("@NotNaTISRegNo", SqlDbType.VarChar, 10);
        //    parameterNotNaTISRegNo.Value = notNaTISRegNo;
        //    myCommand.Parameters.Add(parameterNotNaTISRegNo);

        //    SqlParameter parameterNotVehicleRegisterNo = new SqlParameter("@NotVehicleRegisterNo", SqlDbType.VarChar, 10);
        //    parameterNotVehicleRegisterNo.Value = notVehicleRegisterNo;
        //    myCommand.Parameters.Add(parameterNotVehicleRegisterNo);

        //    //SqlParameter parameterNotVehicleLicenceExpiry = new SqlParameter("@NotVehicleLicenceExpiry", SqlDbType.DateTime);
        //    //parameterNotVehicleLicenceExpiry.Value = notVehicleLicenceExpiry;
        //    //myCommand.Parameters.Add(parameterNotVehicleLicenceExpiry);

        //    //SqlParameter parameterNotDateCOO = new SqlParameter("@NotDateCOO", SqlDbType.DateTime);
        //    //parameterNotDateCOO.Value = notDateCOO;
        //    //myCommand.Parameters.Add(parameterNotDateCOO);

        //    SqlParameter parameterNotClearanceCert = new SqlParameter("@NotClearanceCert", SqlDbType.VarChar, 50);
        //    parameterNotClearanceCert.Value = notClearanceCert;
        //    myCommand.Parameters.Add(parameterNotClearanceCert);

        //    SqlParameter parameterNotRegisterAuth = new SqlParameter("@NotRegisterAuth", SqlDbType.VarChar, 10);
        //    parameterNotRegisterAuth.Value = notRegisterAuth;
        //    myCommand.Parameters.Add(parameterNotRegisterAuth);

        //    SqlParameter parameterNotVehicleDescr = new SqlParameter("@NotVehicleDescr", SqlDbType.VarChar, 3);
        //    parameterNotVehicleDescr.Value = notVehicleDescr;
        //    myCommand.Parameters.Add(parameterNotVehicleDescr);

        //    SqlParameter parameterNotVehicleCat = new SqlParameter("@NotVehicleCat", SqlDbType.VarChar, 3);
        //    parameterNotVehicleCat.Value = notVehicleCat;
        //    myCommand.Parameters.Add(parameterNotVehicleCat);

        //    SqlParameter parameterNotNaTISVMCode = new SqlParameter("@NotNaTISVMCode", SqlDbType.VarChar, 3);
        //    parameterNotNaTISVMCode.Value = notNaTISVMCode;
        //    myCommand.Parameters.Add(parameterNotNaTISVMCode);

        //    SqlParameter parameterNotNaTISVTCode = new SqlParameter("@NotNaTISVTCode", SqlDbType.VarChar, 3);
        //    parameterNotNaTISVTCode.Value = notNaTISVTCode;
        //    myCommand.Parameters.Add(parameterNotNaTISVTCode);

        //    SqlParameter parameterNotVehicleUsage = new SqlParameter("@NotVehicleUsage", SqlDbType.VarChar, 3);
        //    parameterNotVehicleUsage.Value = notVehicleUsage;
        //    myCommand.Parameters.Add(parameterNotVehicleUsage);

        //    SqlParameter parameterNotVehicleColour = new SqlParameter("@NotVehicleColour", SqlDbType.VarChar, 3);
        //    parameterNotVehicleColour.Value = notVehicleColour;
        //    myCommand.Parameters.Add(parameterNotVehicleColour);

        //    SqlParameter parameterNotVINorChassis = new SqlParameter("@NotVINorChassis", SqlDbType.VarChar, 50);
        //    parameterNotVINorChassis.Value = notVINorChassis;
        //    myCommand.Parameters.Add(parameterNotVINorChassis);

        //    SqlParameter parameterNotEngine = new SqlParameter("@NotEngine", SqlDbType.VarChar, 50);
        //    parameterNotEngine.Value = notEngine;
        //    myCommand.Parameters.Add(parameterNotEngine);

        //    SqlParameter parameterNotStatutoryOwner = new SqlParameter("@NotStatutoryOwner", SqlDbType.VarChar, 3);
        //    parameterNotStatutoryOwner.Value = notStatutoryOwner;
        //    myCommand.Parameters.Add(parameterNotStatutoryOwner);

        //    SqlParameter parameterNotNatureOfPerson = new SqlParameter("@NotNatureOfPerson", SqlDbType.VarChar, 3);
        //    parameterNotNatureOfPerson.Value = notNatureOfPerson;
        //    myCommand.Parameters.Add(parameterNotNatureOfPerson);

        //    SqlParameter parameterNotProxyFlag = new SqlParameter("@NotProxyFlag", SqlDbType.Char, 1);
        //    parameterNotProxyFlag.Value = notProxyFlag;
        //    myCommand.Parameters.Add(parameterNotProxyFlag);

        //    //SqlParameter parameterNotPosted1stNoticeDate = new SqlParameter("@NotPosted1stNoticeDate", SqlDbType.DateTime);
        //    //parameterNotPosted1stNoticeDate.Value = notPosted1stNoticeDate;
        //    //myCommand.Parameters.Add(parameterNotPosted1stNoticeDate);

        //    //SqlParameter parameterNotPosted2ndNoticeDate = new SqlParameter("@NotPosted2ndNoticeDate", SqlDbType.DateTime);
        //    //parameterNotPosted2ndNoticeDate.Value = notPosted2ndNoticeDate;
        //    //myCommand.Parameters.Add(parameterNotPosted2ndNoticeDate);

        //    //SqlParameter parameterNotPostedSummonsDate = new SqlParameter("@NotPostedSummonsDate", SqlDbType.DateTime);
        //    //parameterNotPostedSummonsDate.Value = notPostedSummonsDate;
        //    //myCommand.Parameters.Add(parameterNotPostedSummonsDate);

        //    SqlParameter parameterNotPaymentDate = new SqlParameter("@NotPaymentDate", SqlDbType.DateTime);
        //    parameterNotPaymentDate.Value = notPaymentDate;
        //    myCommand.Parameters.Add(parameterNotPaymentDate);

        //    //SqlParameter parameterNotIssue1stNoticeDate = new SqlParameter("@NotIssue1stNoticeDate", SqlDbType.DateTime);
        //    //parameterNotIssue1stNoticeDate.Value = notIssue1stNoticeDate;
        //    //myCommand.Parameters.Add(parameterNotIssue1stNoticeDate);

        //    //SqlParameter parameterNotIssue2ndNoticeDate = new SqlParameter("@NotIssue2ndNoticeDate", SqlDbType.DateTime);
        //    //parameterNotIssue2ndNoticeDate.Value = notIssue2ndNoticeDate;
        //    //myCommand.Parameters.Add(parameterNotIssue2ndNoticeDate);

        //    //SqlParameter parameterNotPrint1stNoticeDate = new SqlParameter("@NotPrint1stNoticeDate", SqlDbType.DateTime);
        //    //parameterNotPrint1stNoticeDate.Value = notPrint1stNoticeDate;
        //    //myCommand.Parameters.Add(parameterNotPrint1stNoticeDate);

        //    //SqlParameter parameterNotPrint2ndNoticeDate = new SqlParameter("@NotPrint2ndNoticeDate", SqlDbType.DateTime);
        //    //parameterNotPrint2ndNoticeDate.Value = notPrint2ndNoticeDate;
        //    //myCommand.Parameters.Add(parameterNotPrint2ndNoticeDate);

        //    //SqlParameter parameterNotPrintSummonsDate = new SqlParameter("@NotPrintSummonsDate", SqlDbType.DateTime);
        //    //parameterNotPrintSummonsDate.Value = notPrintSummonsDate;
        //    //myCommand.Parameters.Add(parameterNotPrintSummonsDate);

        //    SqlParameter parameterEasyPayNo = new SqlParameter("@EasyPayNo", SqlDbType.VarChar, 20);
        //    parameterEasyPayNo.Value = easyPayNo;
        //    myCommand.Parameters.Add(parameterEasyPayNo);

        //    SqlParameter parameterParkMeterNo = new SqlParameter("@ParkMeterNo", SqlDbType.VarChar, 10);
        //    parameterParkMeterNo.Value = parkMeterNo;
        //    myCommand.Parameters.Add(parameterParkMeterNo);

        //    SqlParameter parameterAutName = new SqlParameter("@AutName", SqlDbType.VarChar, 30);
        //    parameterAutName.Value = autName;
        //    myCommand.Parameters.Add(parameterAutName);

        //    SqlParameter parameterIssuedBy = new SqlParameter("@IssuedBy", SqlDbType.VarChar, 30);
        //    parameterIssuedBy.Value = issuedBy;
        //    myCommand.Parameters.Add(parameterIssuedBy);

        //    SqlParameter parameterCrtPaymentAddr1 = new SqlParameter("@CrtPaymentAddr1", SqlDbType.VarChar, 30);
        //    parameterCrtPaymentAddr1.Value = crtPaymentAddr1;
        //    myCommand.Parameters.Add(parameterCrtPaymentAddr1);

        //    SqlParameter parameterCrtPaymentAddr2 = new SqlParameter("@CrtPaymentAddr2", SqlDbType.VarChar, 25);
        //    parameterCrtPaymentAddr2.Value = crtPaymentAddr2;
        //    myCommand.Parameters.Add(parameterCrtPaymentAddr2);

        //    SqlParameter parameterCrtPaymentAddr3 = new SqlParameter("@CrtPaymentAddr3", SqlDbType.VarChar, 25);
        //    parameterCrtPaymentAddr3.Value = crtPaymentAddr3;
        //    myCommand.Parameters.Add(parameterCrtPaymentAddr3);

        //    SqlParameter parameterCrtPaymentAddr4 = new SqlParameter("@CrtPaymentAddr4", SqlDbType.VarChar, 25);
        //    parameterCrtPaymentAddr4.Value = crtPaymentAddr4;
        //    myCommand.Parameters.Add(parameterCrtPaymentAddr4);

        //    //SqlParameter parameterNotCiprusPrintConfirmSendDate = new SqlParameter("@NotCiprusPrintConfirmSendDate", SqlDbType.DateTime);
        //    //parameterNotCiprusPrintConfirmSendDate.Value = notCiprusPrintConfirmSendDate;
        //    //myCommand.Parameters.Add(parameterNotCiprusPrintConfirmSendDate);

        //    SqlParameter parameterNotDocType = new SqlParameter("@NotDocType", SqlDbType.Char, 1);
        //    parameterNotDocType.Value = notDocType;
        //    myCommand.Parameters.Add(parameterNotDocType);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterNotSendTo = new SqlParameter("@NotSendTo", SqlDbType.Char, 1);
        //    parameterNotSendTo.Value = notSendTo;
        //    myCommand.Parameters.Add(parameterNotSendTo);

        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
        //    parameterNotIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterNotIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int NotIntNo = Convert.ToInt32(parameterNotIntNo.Value);

        //        return NotIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        public SqlDataReader GetNoticeList(int autIntNo, int status)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("SILCustom_Notice_NoticeList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
            parameterStatus.Value = status;
            myCommand.Parameters.Add(parameterStatus);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        public DataSet GetPagedNoticePostalList(int autIntNo, string NoticeType, string PrintNamePrefix, DateTime  beginDate, DateTime  endDate,string printFileName, int page, int PageSize,  out int totalCount)
        {
            #region oldcode
            //SqlDataReader result = null;
            //int total = 0;

            //// Create Instance of Connection and Command Object
            //SqlConnection myConnection = new SqlConnection(mConstr);
            //SqlCommand myCommand = new SqlCommand("SILCustom_Notice_GetPagedNoticePostalList", myConnection);

            //// Mark the Command as a SPROC
            //myCommand.CommandType = CommandType.StoredProcedure;

            //// Add Parameters to SPROC
            //SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            //parameterAutIntNo.Value = autIntNo;
            //myCommand.Parameters.Add(parameterAutIntNo);

            //SqlParameter parameterNoticeType = new SqlParameter("@NoticeType", SqlDbType.VarChar, 20);
            //parameterNoticeType.Value = NoticeType;
            //myCommand.Parameters.Add(parameterNoticeType);

            //SqlParameter parameterPrintNamePrefix = new SqlParameter("@PrintNamePrefix", SqlDbType.VarChar, 10);
            //parameterPrintNamePrefix.Value = PrintNamePrefix;
            //myCommand.Parameters.Add(parameterPrintNamePrefix);

            //SqlParameter parameterBeginDate = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            //parameterBeginDate.Value = beginDate;
            //myCommand.Parameters.Add(parameterBeginDate);

            //SqlParameter parameterEndDate = new SqlParameter("@EndDate", SqlDbType.DateTime);
            //parameterEndDate.Value = endDate;
            //myCommand.Parameters.Add(parameterEndDate);

            //SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int, 4);
            //parameterPageIndex.Value = page;
            //myCommand.Parameters.Add(parameterPageIndex);

            //SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int, 4);
            //parameterPageSize.Value = PageSize;
            //myCommand.Parameters.Add(parameterPageSize);

            //SqlParameter parameterTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int, 4);
            ////parameterTotalCount.Value = total;
            //parameterTotalCount.Direction = ParameterDirection.Output;
            //myCommand.Parameters.Add(parameterTotalCount);

            //DataSet ds = new DataSet();

            //try
            //{
            //    // Execute the command
            //    myConnection.Open();
            //    result = myCommand.ExecuteReader();

            //}
            //catch (Exception e)
            //{
            //    myConnection.Dispose();
            //    string msg = e.Message;

            //}


            #endregion 

            SqlDataAdapter sqlDANotice = new SqlDataAdapter();
            DataSet dsNotice = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDANotice.SelectCommand = new SqlCommand();
            sqlDANotice.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDANotice.SelectCommand.CommandText = "SILCustom_Notice_GetPagedNoticePostalList";

            // Mark the Command as a SPROC
            sqlDANotice.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDANotice.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterNoticeType = new SqlParameter("@NoticeType", SqlDbType.VarChar, 20);
            parameterNoticeType.Value = NoticeType;
            sqlDANotice.SelectCommand.Parameters.Add(parameterNoticeType);

            SqlParameter parameterPrintNamePrefix = new SqlParameter("@PrintNamePrefix", SqlDbType.VarChar, 10);
            parameterPrintNamePrefix.Value = PrintNamePrefix;
            sqlDANotice.SelectCommand.Parameters.Add(parameterPrintNamePrefix);

            SqlParameter parameterBeginDate = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            parameterBeginDate.Value = beginDate;
            sqlDANotice.SelectCommand.Parameters.Add(parameterBeginDate);

            SqlParameter parameterEndDate = new SqlParameter("@EndDate", SqlDbType.DateTime);
            parameterEndDate.Value = endDate;
            sqlDANotice.SelectCommand.Parameters.Add(parameterEndDate);

            //Add by Brian 2013-10-22.
            SqlParameter parameterPrintFileName= new SqlParameter("@PrintFileName", SqlDbType.VarChar, 50);
            parameterPrintFileName.Value = printFileName;
            sqlDANotice.SelectCommand.Parameters.Add(parameterPrintFileName);


            SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int, 4);
            parameterPageIndex.Value = page;
            sqlDANotice.SelectCommand.Parameters.Add(parameterPageIndex);

            SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int, 4);
            parameterPageSize.Value = PageSize;
            sqlDANotice.SelectCommand.Parameters.Add(parameterPageSize);

            SqlParameter parameterTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int, 4);
            //parameterTotalCount.Value = total;
            parameterTotalCount.Direction = ParameterDirection.Output;
            sqlDANotice.SelectCommand.Parameters.Add(parameterTotalCount);


            // Execute the command and close the connection
            sqlDANotice.Fill(dsNotice);
            sqlDANotice.SelectCommand.Connection.Dispose();

            // Return the dataset result

            totalCount = Convert.ToInt32(parameterTotalCount.Value);

            return dsNotice;



           // totalCount = 0;
            //return result;
        }

        public DataSet GetAdministrativeCorrectionsList(int autIntNo,string noticeNumber, int page, int PageSize, out int totalCount)
        {
            SqlDataAdapter sqlDANotice = new SqlDataAdapter();
            DataSet dsNotice = new DataSet();

            try
            {

                // Create Instance of Connection and Command Object
                sqlDANotice.SelectCommand = new SqlCommand();
                sqlDANotice.SelectCommand.Connection = new SqlConnection(mConstr);
                sqlDANotice.SelectCommand.CommandText = "GetPagedAdministrativeCorrectionsList";

                // Mark the Command as a SPROC
                sqlDANotice.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add Parameters to SPROC
                SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
                parameterAutIntNo.Value = autIntNo;
                sqlDANotice.SelectCommand.Parameters.Add(parameterAutIntNo);

                //Add by Brian 2013-10-22.
                SqlParameter parameterNoticeNumber = new SqlParameter("@NoticeNumber", SqlDbType.NVarChar, 50);
                parameterNoticeNumber.Value = noticeNumber;
                sqlDANotice.SelectCommand.Parameters.Add(parameterNoticeNumber);

                SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int, 4);
                parameterPageIndex.Value = page;
                sqlDANotice.SelectCommand.Parameters.Add(parameterPageIndex);

                SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int, 4);
                parameterPageSize.Value = PageSize;
                sqlDANotice.SelectCommand.Parameters.Add(parameterPageSize);

                SqlParameter parameterTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int, 4);
                //parameterTotalCount.Value = total;
                parameterTotalCount.Direction = ParameterDirection.Output;
                sqlDANotice.SelectCommand.Parameters.Add(parameterTotalCount);


                // Execute the command and close the connection
                sqlDANotice.Fill(dsNotice);
                sqlDANotice.SelectCommand.Connection.Dispose();

                // Return the dataset result

                totalCount = Convert.ToInt32(parameterTotalCount.Value);

                return dsNotice;
            }
            finally
            {
                if (sqlDANotice.SelectCommand.Connection != null)
                {
                    sqlDANotice.SelectCommand.Connection.Dispose();
                }

            }
        }

        public DataSet GetOfficerErrorByNotIntNo(int notIntNo)
        {
            SqlDataAdapter sqlDANotice = new SqlDataAdapter();
            DataSet dsNotice = new DataSet();

            try
            {
                // Create Instance of Connection and Command Object
                sqlDANotice.SelectCommand = new SqlCommand();
                sqlDANotice.SelectCommand.Connection = new SqlConnection(mConstr);
                sqlDANotice.SelectCommand.CommandText = "GetOfficerErrorByNotIntNo";

                // Mark the Command as a SPROC
                sqlDANotice.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add Parameters to SPROC
                SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
                parameterNotIntNo.Value = notIntNo;
                sqlDANotice.SelectCommand.Parameters.Add(parameterNotIntNo);

                // Execute the command and close the connection
                sqlDANotice.Fill(dsNotice);
                sqlDANotice.SelectCommand.Connection.Dispose();

                return dsNotice;
            }
            finally
            {
                if (sqlDANotice.SelectCommand.Connection != null)
                {
                    sqlDANotice.SelectCommand.Connection.Dispose();
                }

            }
        }

        public DataSet GetExpirePrintFileList(int autIntNo, string DocumentType, DateTime beginDate, DateTime endDate, string printFileName,int DR_NotOffenceDate_NotPosted1stNoticeDate_days, int page, int PageSize, out int totalCount)
        {
            SqlDataAdapter sqlDANotice = new SqlDataAdapter();
            DataSet dsNotice = new DataSet();

            try
            {

                // Create Instance of Connection and Command Object
                sqlDANotice.SelectCommand = new SqlCommand();
                sqlDANotice.SelectCommand.Connection = new SqlConnection(mConstr);
                sqlDANotice.SelectCommand.CommandText = "GetPagedExpirePrintFileList";

                // Mark the Command as a SPROC
                sqlDANotice.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add Parameters to SPROC
                SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
                parameterAutIntNo.Value = autIntNo;
                sqlDANotice.SelectCommand.Parameters.Add(parameterAutIntNo);

                SqlParameter parameterNoticeType = new SqlParameter("@DocumentType", SqlDbType.VarChar, 20);
                parameterNoticeType.Value = DocumentType;
                sqlDANotice.SelectCommand.Parameters.Add(parameterNoticeType);

                SqlParameter parameterBeginDate = new SqlParameter("@BeginDate", SqlDbType.DateTime);
                parameterBeginDate.Value = beginDate;
                sqlDANotice.SelectCommand.Parameters.Add(parameterBeginDate);

                SqlParameter parameterEndDate = new SqlParameter("@EndDate", SqlDbType.DateTime);
                parameterEndDate.Value = endDate;
                sqlDANotice.SelectCommand.Parameters.Add(parameterEndDate);

                //Add by Brian 2013-10-22.
                SqlParameter parameterPrintFileName = new SqlParameter("@PrintFileName", SqlDbType.VarChar, 50);
                parameterPrintFileName.Value = printFileName;
                sqlDANotice.SelectCommand.Parameters.Add(parameterPrintFileName);

                SqlParameter parameterDR_NotOffenceDate_NotPosted1stNoticeDate_days = new SqlParameter("@DR_NotOffenceDate_NotPosted1stNoticeDate_days", SqlDbType.Int, 4);
                parameterDR_NotOffenceDate_NotPosted1stNoticeDate_days.Value = DR_NotOffenceDate_NotPosted1stNoticeDate_days;
                sqlDANotice.SelectCommand.Parameters.Add(parameterDR_NotOffenceDate_NotPosted1stNoticeDate_days);


                SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int, 4);
                parameterPageIndex.Value = page;
                sqlDANotice.SelectCommand.Parameters.Add(parameterPageIndex);

                SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int, 4);
                parameterPageSize.Value = PageSize;
                sqlDANotice.SelectCommand.Parameters.Add(parameterPageSize);

                SqlParameter parameterTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int, 4);
                //parameterTotalCount.Value = total;
                parameterTotalCount.Direction = ParameterDirection.Output;
                sqlDANotice.SelectCommand.Parameters.Add(parameterTotalCount);


                // Execute the command and close the connection
                sqlDANotice.Fill(dsNotice);
                sqlDANotice.SelectCommand.Connection.Dispose();

                // Return the dataset result

                totalCount = Convert.ToInt32(parameterTotalCount.Value);

                return dsNotice;
            }
            finally
            {
                if (sqlDANotice.SelectCommand.Connection != null)
                {
                    sqlDANotice.SelectCommand.Connection.Dispose();
                }

            }
        }

        public DataSet GetRevertPrintFileList(int autIntNo, string DocumentType, DateTime beginDate, DateTime endDate, string printFileName, int page, int PageSize, out int totalCount)
        {
            SqlDataAdapter sqlDANotice = new SqlDataAdapter();
            DataSet dsNotice = new DataSet();

            try
            {

                // Create Instance of Connection and Command Object
                sqlDANotice.SelectCommand = new SqlCommand();
                sqlDANotice.SelectCommand.Connection = new SqlConnection(mConstr);
                sqlDANotice.SelectCommand.CommandText = "GetPagedRevertPrintFileList";

                // Mark the Command as a SPROC
                sqlDANotice.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add Parameters to SPROC
                SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
                parameterAutIntNo.Value = autIntNo;
                sqlDANotice.SelectCommand.Parameters.Add(parameterAutIntNo);

                SqlParameter parameterNoticeType = new SqlParameter("@DocumentType", SqlDbType.VarChar, 20);
                parameterNoticeType.Value = DocumentType;
                sqlDANotice.SelectCommand.Parameters.Add(parameterNoticeType);

                SqlParameter parameterBeginDate = new SqlParameter("@BeginDate", SqlDbType.DateTime);
                parameterBeginDate.Value = beginDate;
                sqlDANotice.SelectCommand.Parameters.Add(parameterBeginDate);

                SqlParameter parameterEndDate = new SqlParameter("@EndDate", SqlDbType.DateTime);
                parameterEndDate.Value = endDate;
                sqlDANotice.SelectCommand.Parameters.Add(parameterEndDate);

                //Add by Brian 2013-10-22.
                SqlParameter parameterPrintFileName = new SqlParameter("@PrintFileName", SqlDbType.VarChar, 50);
                parameterPrintFileName.Value = printFileName;
                sqlDANotice.SelectCommand.Parameters.Add(parameterPrintFileName);


                SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int, 4);
                parameterPageIndex.Value = page;
                sqlDANotice.SelectCommand.Parameters.Add(parameterPageIndex);

                SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int, 4);
                parameterPageSize.Value = PageSize;
                sqlDANotice.SelectCommand.Parameters.Add(parameterPageSize);

                SqlParameter parameterTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int, 4);
                //parameterTotalCount.Value = total;
                parameterTotalCount.Direction = ParameterDirection.Output;
                sqlDANotice.SelectCommand.Parameters.Add(parameterTotalCount);


                // Execute the command and close the connection
                sqlDANotice.Fill(dsNotice);
                sqlDANotice.SelectCommand.Connection.Dispose();

                // Return the dataset result

                totalCount = Convert.ToInt32(parameterTotalCount.Value);

                return dsNotice;
            }
            finally
            {
                if (sqlDANotice.SelectCommand.Connection != null)
                {
                    sqlDANotice.SelectCommand.Connection.Dispose();
                }

            }
        }

        //public SqlDataReader GetNoticeListBarryHack(int autIntNo, int status)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ImageListHack", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the data reader result
        //    return result;
        //}

        public SqlDataReader GetNoticeList_CPI(int autIntNo, int statusLoaded, int statusFixed)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeList_CPI", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
            parameterStatusLoaded.Value = statusLoaded;
            myCommand.Parameters.Add(parameterStatusLoaded);

            SqlParameter parameterStatusFixed = new SqlParameter("@StatusFixed", SqlDbType.Int, 4);
            parameterStatusFixed.Value = statusFixed;
            myCommand.Parameters.Add(parameterStatusFixed);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        public SqlDataReader GetNoticeViewDetails(int notIntNo, string showCrossHairs, int crossHairStyle)
        {
            // Returns a recordset
            // Create Instance of Connection and Command Object
            // 2006/01/03 no more batch
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeViewDetails", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = notIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            myCommand.Parameters.Add("@ShowCrossHairs", SqlDbType.Char, 1).Value = showCrossHairs;
            myCommand.Parameters.Add("@CrossHairStyle", SqlDbType.VarChar, 4).Value = crossHairStyle;
            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception e)
            {
                string error = e.Message;
                return null;
            }
        }

        public DataSet GetNatisFixDataDS(int autIntNo, int status)
        {
            SqlDataAdapter sqlDAFixNatis = new SqlDataAdapter();
            DataSet dsFixNatis = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAFixNatis.SelectCommand = new SqlCommand();
            sqlDAFixNatis.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAFixNatis.SelectCommand.CommandText = "NoticeFixNatis";

            // Mark the Command as a SPROC
            sqlDAFixNatis.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDAFixNatis.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
            parameterStatus.Value = status;
            sqlDAFixNatis.SelectCommand.Parameters.Add(parameterStatus);

            // Execute the command and close the connection
            sqlDAFixNatis.Fill(dsFixNatis);
            sqlDAFixNatis.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsFixNatis;
        }

        public DataSet GetNoticeCheckRLVDS(string printFile)
        {
            return GetNoticeCheckRLVDS(printFile, 0);
        }

        public DataSet GetNoticeCheckRLVDS(Int32 nrIntNo)
        {
            return GetNoticeCheckRLVDS(string.Empty, nrIntNo);
        }

        public DataSet GetNoticeCheckRLVDS(string printFile, Int32 nrIntNo)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            // Create Instance of Connection and Command Object
            da.SelectCommand = new SqlCommand();
            da.SelectCommand.Connection = new SqlConnection(mConstr);
            da.SelectCommand.CommandText = "NoticeCheckRLV";

            // Mark the Command as a SPROC
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            da.SelectCommand.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = printFile;
            da.SelectCommand.Parameters.Add("@NRIntNo", SqlDbType.Int, 4).Value = nrIntNo;

            // Execute the command and close the connection
            da.Fill(ds);
            da.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return ds;
        }

        public DataSet GetNoticeExceptionsDS(int autIntNo, int statusError)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            // Create Instance of Connection and Command Object
            da.SelectCommand = new SqlCommand();
            da.SelectCommand.Connection = new SqlConnection(mConstr);
            da.SelectCommand.CommandText = "NoticeExceptions_CPI";

            // da the Command as a SPROC
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            da.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterStatusError = new SqlParameter("@StatusError", SqlDbType.Int, 4);
            parameterStatusError.Value = statusError;
            da.SelectCommand.Parameters.Add(parameterStatusError);

            // Execute the command and close the connection
            da.Fill(ds);
            da.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return ds;
        }


        public DataSet GetNoticeListDS(int autIntNo, int status)
        {
            SqlDataAdapter sqlDANotice = new SqlDataAdapter();
            DataSet dsNotice = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDANotice.SelectCommand = new SqlCommand();
            sqlDANotice.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDANotice.SelectCommand.CommandText = "SILCustom_Notice_NoticeList";

            // Mark the Command as a SPROC
            sqlDANotice.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDANotice.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
            parameterStatus.Value = status;
            sqlDANotice.SelectCommand.Parameters.Add(parameterStatus);


            // Execute the command and close the connection
            sqlDANotice.Fill(dsNotice);
            sqlDANotice.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsNotice;
        }

        public DataSet GetNoticeList_CPI_DS(int autIntNo, int statusLoaded, int statusFixed,
            int pageSize, int pageIndex, out int totalCount) //2013-04-09 add by Henry for pagination
        {
            SqlDataAdapter sqlDANotice = new SqlDataAdapter();
            DataSet dsNotice = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDANotice.SelectCommand = new SqlCommand();
            sqlDANotice.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDANotice.SelectCommand.CommandText = "NoticeList_CPI";

            // Mark the Command as a SPROC
            sqlDANotice.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDANotice.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
            parameterStatusLoaded.Value = statusLoaded;
            sqlDANotice.SelectCommand.Parameters.Add(parameterStatusLoaded);

            SqlParameter parameterStatusFixed = new SqlParameter("@StatusFixed", SqlDbType.Int, 4);
            parameterStatusFixed.Value = statusFixed;
            sqlDANotice.SelectCommand.Parameters.Add(parameterStatusFixed);

            sqlDANotice.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            sqlDANotice.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            sqlDANotice.SelectCommand.Parameters.Add(paraTotalCount);

            // Execute the command and close the connection
            sqlDANotice.Fill(dsNotice);
            sqlDANotice.SelectCommand.Connection.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            // Return the dataset result
            return dsNotice;
        }

        /// <summary>
        /// Gets the notice search.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="colName">Name of the col.</param>
        /// <param name="colValue">The col value.</param>
        /// <returns></returns>
        public SqlDataReader GetNoticeSearch(int autIntNo, string colName, string colValue)
        {
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeSearch", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterColumnName = new SqlParameter("@ColumnName", SqlDbType.VarChar, 10);
            parameterColumnName.Value = colName;
            myCommand.Parameters.Add(parameterColumnName);

            SqlParameter parameterColumnValue = new SqlParameter("@ColumnValue", SqlDbType.VarChar, 30);
            parameterColumnValue.Value = colValue;
            myCommand.Parameters.Add(parameterColumnValue);

            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception e)
            {
                string error = e.Message;
                return null;
            }
        }
        // 2006/06/02 no longer in use
        //public DataSet GetNoticeListForUpdateDS(int autIntNo)
        //{
        //    SqlDataAdapter sqlDANotice = new SqlDataAdapter();
        //    DataSet dsNotice = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlDANotice.SelectCommand = new SqlCommand();
        //    sqlDANotice.SelectCommand.Connection = new SqlConnection(mConstr);
        //    sqlDANotice.SelectCommand.CommandText = "NoticeListForUpdate";

        //    // Mark the Command as a SPROC
        //    sqlDANotice.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    sqlDANotice.SelectCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command and close the connection
        //    sqlDANotice.Fill(dsNotice);
        //    sqlDANotice.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsNotice;
        //}

        // 2006/06/02 no longer in use
        //public DataSet GetNoticeListManualDS(int autIntNo, int csCode, string cType)
        //{
        //    SqlDataAdapter sqlDANotice = new SqlDataAdapter();
        //    DataSet dsNotice = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlDANotice.SelectCommand = new SqlCommand();
        //    sqlDANotice.SelectCommand.Connection = new SqlConnection(mConstr);
        //    sqlDANotice.SelectCommand.CommandText = "NoticeListManual";

        //    // Mark the Command as a SPROC
        //    sqlDANotice.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    sqlDANotice.SelectCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterCSCode = new SqlParameter("@CSCode", SqlDbType.Int, 4);
        //    parameterCSCode.Value = csCode;
        //    sqlDANotice.SelectCommand.Parameters.Add(parameterCSCode);

        //    SqlParameter parameterCType = new SqlParameter("@CType", SqlDbType.VarChar, 15);
        //    parameterCType.Value = csCode;
        //    sqlDANotice.SelectCommand.Parameters.Add(parameterCType);

        //    // Execute the command and close the connection
        //    sqlDANotice.Fill(dsNotice);
        //    sqlDANotice.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsNotice;
        //}

        public DataSet GetNoticeListCashDS(int autIntNo, string searchValue, string searchType)
        {
            SqlDataAdapter sqlDANotices = new SqlDataAdapter();
            DataSet dsNotices = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDANotices.SelectCommand = new SqlCommand();
            sqlDANotices.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDANotices.SelectCommand.CommandText = "NoticeListCash";

            // Mark the Command as a SPROC
            sqlDANotices.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDANotices.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterSearchValue = new SqlParameter("@SearchValue", SqlDbType.VarChar, 50);
            parameterSearchValue.Value = searchValue;
            sqlDANotices.SelectCommand.Parameters.Add(parameterSearchValue);

            SqlParameter parameterSearchType = new SqlParameter("@SearchType", SqlDbType.VarChar, 15);
            parameterSearchType.Value = searchType;
            sqlDANotices.SelectCommand.Parameters.Add(parameterSearchType);

            // Execute the command and close the connection
            sqlDANotices.Fill(dsNotices);
            sqlDANotices.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsNotices;
        }

        // 2013-07-19 comment by Henry for useless
        //public int UpdateCameraNoticeDetails(int NotIntNo, int locIntNo, int szIntNo,
        //    int notSpeed1, int notSpeed2, int toIntNo, string notRegNo,
        //    int vmIntNo, int vtIntNo, int rdTIntNo, int crtIntNo,
        //    string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("NoticeCameraUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterLocIntNo = new SqlParameter("@LocIntNo", SqlDbType.Int, 4);
        //    parameterLocIntNo.Value = locIntNo;
        //    myCommand.Parameters.Add(parameterLocIntNo);

        //    SqlParameter parameterSZIntNo = new SqlParameter("@SZIntNo", SqlDbType.Int, 4);
        //    parameterSZIntNo.Value = szIntNo;
        //    myCommand.Parameters.Add(parameterSZIntNo);

        //    SqlParameter parameterNotRegNo = new SqlParameter("@NotRegNo", SqlDbType.VarChar, 10);
        //    parameterNotRegNo.Value = notRegNo;
        //    myCommand.Parameters.Add(parameterNotRegNo);

        //    SqlParameter parameterNotSpeed1 = new SqlParameter("@NotSpeed1", SqlDbType.Int);
        //    parameterNotSpeed1.Value = notSpeed1;
        //    myCommand.Parameters.Add(parameterNotSpeed1);

        //    SqlParameter parameterNotSpeed2 = new SqlParameter("@NotSpeed2", SqlDbType.Int);
        //    parameterNotSpeed2.Value = notSpeed2;
        //    myCommand.Parameters.Add(parameterNotSpeed2);

        //    SqlParameter parameterTOIntNo = new SqlParameter("@TOIntNo", SqlDbType.Int, 4);
        //    parameterTOIntNo.Value = toIntNo;
        //    myCommand.Parameters.Add(parameterTOIntNo);

        //    SqlParameter parameterVMIntNo = new SqlParameter("@VMIntNo", SqlDbType.Int, 4);
        //    parameterVMIntNo.Value = vmIntNo;
        //    myCommand.Parameters.Add(parameterVMIntNo);

        //    SqlParameter parameterVTIntNo = new SqlParameter("@VTIntNo", SqlDbType.Int, 4);
        //    parameterVTIntNo.Value = vtIntNo;
        //    myCommand.Parameters.Add(parameterVTIntNo);

        //    SqlParameter parameterRdTIntNo = new SqlParameter("@RdTIntNo", SqlDbType.Int, 4);
        //    parameterRdTIntNo.Value = rdTIntNo;
        //    myCommand.Parameters.Add(parameterRdTIntNo);

        //    SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
        //    parameterCrtIntNo.Value = crtIntNo;
        //    myCommand.Parameters.Add(parameterCrtIntNo);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
        //    parameterNotIntNo.Value = NotIntNo;
        //    parameterNotIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterNotIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int notIntNo = (int)myCommand.Parameters["@NotIntNo"].Value;

        //        return notIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        public int CheckNoticeForPrintRequest(string printReqName, ref string error)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeCheckPrintRequest", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterPrintReqName = new SqlParameter("@PrintReqName", SqlDbType.VarChar, 50);
            parameterPrintReqName.Value = printReqName;
            myCommand.Parameters.Add(parameterPrintReqName);

            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNotIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int notIntNo = (int)myCommand.Parameters["@NotIntNo"].Value;

                return notIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                error = e.Message;
                return -2;
            }
        }

        /// <summary>
        /// Updates the notice charge status.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="printFile">The print file.</param>
        /// <param name="statusFrom">The status from.</param>
        /// <param name="statusTo">The status to.</param>
        /// <returns>The number of rows affected</returns>
        public int UpdateNoticeChargeStatus(int autIntNo, string printFile, int statusFrom, int statusTo, string type, string lastuser, ref string errMessage)
        {
            //2013-06-27 added by Nancy for getting authority rule start
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = autIntNo;
            arDetails.ARCode = "2570";
            arDetails.LastUser = lastuser;
            DefaultAuthRules authRule = new DefaultAuthRules(arDetails, mConstr);
            KeyValuePair<int, string> valueArDet = authRule.SetDefaultAuthRule();
            //2013-06-27 added by Nancy for getting authority rule end

            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("NoticeChargeUpdateStatus", con) { CommandTimeout = 0 };    // 2013-09-26, Oscar added timeout;
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@PrintFile", SqlDbType.VarChar, 50).Value = printFile;
            com.Parameters.Add("@StatusFrom", SqlDbType.Int, 4).Value = statusFrom;
            com.Parameters.Add("@StatusTo", SqlDbType.Int, 4).Value = statusTo;

            //FirstNotice, SecondNotice
            com.Parameters.Add("@Type", SqlDbType.VarChar, 20).Value = type;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastuser;
            com.Parameters.Add("@AuthRule", SqlDbType.VarChar, 3).Value = valueArDet.Value; //2013-06-27 added by Nancy for 4973

            SqlParameter parameterNoOfRows = com.Parameters.Add("@NoOfRows", SqlDbType.Int, 4);
            parameterNoOfRows.Direction = ParameterDirection.InputOutput;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                int noOfRows = (int)com.Parameters["@NoOfRows"].Value;

                return noOfRows;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                errMessage = e.Message;
                return -1;
            }
            finally
            {
                con.Dispose();
            }
        }

        public int UpdateColumnForNoticeCorrection(string table, string column, string strValue,
            int intValue, string lastUser, ref string errMessage, int notIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("NoticeColumnCorrection_CPI", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@Table", SqlDbType.VarChar, 30).Value = table;
            com.Parameters.Add("@Column", SqlDbType.VarChar, 50).Value = column;
            com.Parameters.Add("@StrValue", SqlDbType.VarChar, 100).Value = strValue;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@IntValue", SqlDbType.Int, 4).Value = intValue;

            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters["@NotIntNo"].Direction = ParameterDirection.InputOutput;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                notIntNo = (int)com.Parameters["@NotIntNo"].Value;

                return notIntNo;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        public int UpdateNoticebyColumn(string table, string column, string strValue,
            int intValue, string lastUser, ref string errMessage, int notIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("NoticeColumnCorrection", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@Table", SqlDbType.VarChar, 30).Value = table;
            com.Parameters.Add("@Column", SqlDbType.VarChar, 50).Value = column;
            com.Parameters.Add("@StrValue", SqlDbType.VarChar, 100).Value = strValue;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@IntValue", SqlDbType.Int, 4).Value = intValue;

            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters["@NotIntNo"].Direction = ParameterDirection.InputOutput;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                notIntNo = (int)com.Parameters["@NotIntNo"].Value;

                return notIntNo;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        public int UpdateCiprusAartoResponse(string printFileName,CiprusAarto61Record rec61, string processDescr,
            string lastUser, int notIntNo, int statusErrors, int statusAlloc, int statusNoAOG, ref string errMessage)
        {
            //mrs 20090205 concatenate officer name and initials
            string officerFullName = rec61.officerName + " " + rec61.officerInit;

            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("NoticeUpdateResponse_CA", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@PrintFileName", SqlDbType.VarChar, 50).Value = printFileName;
            com.Parameters.Add("@ProcessCode", SqlDbType.Int, 4).Value = int.Parse(rec61.processCode);
            com.Parameters.Add("@SeverityCode", SqlDbType.Int, 4).Value = int.Parse(rec61.severityCode);
            com.Parameters.Add("@ProcessDescr", SqlDbType.VarChar, 100).Value = processDescr;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@StatusErrors", SqlDbType.Int, 4).Value = statusErrors;
            com.Parameters.Add("@StatusAlloc", SqlDbType.Int, 4).Value = statusAlloc;
            com.Parameters.Add("@StatusNoAOG", SqlDbType.Int, 4).Value = statusNoAOG;
            com.Parameters.Add("@NotTicketNo", SqlDbType.VarChar, 50).Value = rec61.noticeNumber;
            com.Parameters.Add("@NoticeNoType", SqlDbType.Char, 1).Value = rec61.noticeNoType;
            com.Parameters.Add("@ChgIssueAuthority", SqlDbType.VarChar, 40).Value = rec61.issuingAuthority;
            com.Parameters.Add("@ChgOfficerNatisNo", SqlDbType.VarChar, 9).Value = rec61.officeNatisNo;
            com.Parameters.Add("@ChgOfficerName", SqlDbType.VarChar, 40).Value = officerFullName; // rec61.officerName;
            //Changed by Tod Zhang on the 090701
            com.Parameters.Add("@AdjOfficerInfrastructureNo", SqlDbType.VarChar, 9).Value = rec61.adjOfficerInfrastructureNo;
            com.Parameters.Add("@AdjOfficerSurname", SqlDbType.VarChar, 40).Value = rec61.adjOfficerSurname;
            com.Parameters.Add("@AdjOfficerInitials", SqlDbType.VarChar, 5).Value = rec61.adjOfficerInitials;

            com.Parameters.Add("@ChgDiscountAmount", SqlDbType.Money).Value = Convert.ToDecimal(rec61.discountAmount) / 100;  // willie fixed numbers;
            com.Parameters.Add("@ChgDiscountedAmount", SqlDbType.Money).Value = Convert.ToDecimal(rec61.discountedAmount) / 100;
            com.Parameters.Add("@ChgDemeritPoints", SqlDbType.Int, 4).Value = Int16.Parse(rec61.demeritPoints);
            com.Parameters.Add("@ChgPenaltyAmount", SqlDbType.Money).Value = Convert.ToDecimal(rec61.penaltyAmount) / 100;
            com.Parameters.Add("@ChgType", SqlDbType.VarChar, 10).Value = rec61.chargeType;
            com.Parameters.Add("@ChgLegislation", SqlDbType.Char, 1).Value = rec61.legislation;
            com.Parameters.Add("@UniqueDocID", SqlDbType.VarChar, 30).Value = rec61.uniqueDocID;

            //dls 090324 - add ciprus load date
            if (rec61.ciprusLoadDate.Length > 0)
            {
                com.Parameters.Add("@CiprusLoadDate", SqlDbType.SmallDateTime, 4).Value = Convert.ToDateTime(rec61.ciprusLoadDate);
            }

            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters["@NotIntNo"].Direction = ParameterDirection.InputOutput;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                notIntNo = (int)com.Parameters["@NotIntNo"].Value;

                return notIntNo;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                errMessage = e.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        public int UpdateCiprusCofCTNSResponse(string noticeNumber, string processDescr,
        string lastUser, int notIntNo, int statusAlloc, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("NoticeUpdateNSResponse_CC", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@StatusAlloc", SqlDbType.Int, 4).Value = statusAlloc;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters.Add("@NotTicketNo", SqlDbType.VarChar, 50).Value = noticeNumber;
            com.Parameters["@NotIntNo"].Direction = ParameterDirection.InputOutput;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                notIntNo = (int)com.Parameters["@NotIntNo"].Value;

                return notIntNo;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                errMessage = e.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        public int UpdateCiprusCofCTResponse(CiprusCofCT61Record rec61, string processDescr,
            string lastUser, int notIntNo, int statusErrors, int statusAlloc, int statusNoAOG,
            string typeOfOwner, string streetAddrLine1, string streetAddrLine2, string streetAddrLine3, string streetAddrLine4,string streetAddrLine5,  string streetPostCode,
            string postalAddrLine1, string postalAddrLine2, string postalAddrLine3, string postalAddrLine4, string postalAddrLine5,  string postalPostCode,
            ref string errMessage)
        {
            //mrs 20090205 concatenate officer name and initials
            string officerFullName = rec61.officerName + " " + rec61.officerInit;

            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("NoticeUpdateResponse_CC", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ProcessCode", SqlDbType.Int, 4).Value = int.Parse(rec61.processCode);
            com.Parameters.Add("@SeverityCode", SqlDbType.Int, 4).Value = int.Parse(rec61.severityCode);
            com.Parameters.Add("@ProcessDescr", SqlDbType.VarChar, 100).Value = processDescr;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@StatusErrors", SqlDbType.Int, 4).Value = statusErrors;
            com.Parameters.Add("@StatusAlloc", SqlDbType.Int, 4).Value = statusAlloc;
            com.Parameters.Add("@StatusNoAOG", SqlDbType.Int, 4).Value = statusNoAOG;
            com.Parameters.Add("@NotTicketNo", SqlDbType.VarChar, 50).Value = rec61.noticeNumber;
            com.Parameters.Add("@NoticeNoType", SqlDbType.Char, 1).Value = rec61.noticeNoType;
            com.Parameters.Add("@ChgIssueAuthority", SqlDbType.VarChar, 40).Value = rec61.issuingAuthority;
            com.Parameters.Add("@ChgOfficerNatisNo", SqlDbType.VarChar, 9).Value = rec61.officeNatisNo;
            com.Parameters.Add("@ChgOfficerName", SqlDbType.VarChar, 40).Value = officerFullName; // rec61.officerName;
            com.Parameters.Add("@ChgDiscountAmount", SqlDbType.Money).Value = Convert.ToDecimal(rec61.discountAmount) / 100;  // willie fixed numbers;
            com.Parameters.Add("@ChgDiscountedAmount", SqlDbType.Money).Value = Convert.ToDecimal(rec61.discountedAmount) / 100;
            com.Parameters.Add("@ChgDemeritPoints", SqlDbType.Int, 4).Value = Int16.Parse(rec61.demeritPoints);
            com.Parameters.Add("@ChgPenaltyAmount", SqlDbType.Money).Value = Convert.ToDecimal(rec61.penaltyAmount) / 100;
            com.Parameters.Add("@ChgType", SqlDbType.VarChar, 10).Value = rec61.chargeType;
            com.Parameters.Add("@ChgLegislation", SqlDbType.Char, 1).Value = rec61.legislation;
            com.Parameters.Add("@UniqueDocID", SqlDbType.VarChar, 30).Value = rec61.uniqueDocID;

            // Jake 2011-0923 added
            // add new parameter for process processcode =71, update driver, owner ,proxy addres information
            com.Parameters.Add("@StrAddrLine1", SqlDbType.VarChar, 40).Value = streetAddrLine1;
            com.Parameters.Add("@StrAddrLine2", SqlDbType.VarChar, 40).Value = streetAddrLine2;
            com.Parameters.Add("@StrAddrLine3", SqlDbType.VarChar, 40).Value = streetAddrLine3;
            com.Parameters.Add("@StrAddrLine4", SqlDbType.VarChar, 40).Value = streetAddrLine4;
            com.Parameters.Add("@StrAddrLine5", SqlDbType.VarChar, 40).Value = streetAddrLine5;
            com.Parameters.Add("@StrPostCode", SqlDbType.VarChar, 40).Value = streetPostCode;

            com.Parameters.Add("@PostalAddrLine1", SqlDbType.VarChar, 40).Value = postalAddrLine1;
            com.Parameters.Add("@PostalAddrLine2", SqlDbType.VarChar, 40).Value = postalAddrLine2;
            com.Parameters.Add("@PostalAddrLine3", SqlDbType.VarChar, 40).Value = postalAddrLine3;
            com.Parameters.Add("@PostalAddrLine4", SqlDbType.VarChar, 40).Value = postalAddrLine4;
            com.Parameters.Add("@PostalAddrLine5", SqlDbType.VarChar, 40).Value = postalAddrLine5;
            com.Parameters.Add("@PostalPostCode", SqlDbType.VarChar, 40).Value = postalPostCode;

            com.Parameters.Add("@TypeOfOwner", SqlDbType.Int).Value = Convert.ToInt32(typeOfOwner);


            //dls 090324 - add ciprus load date
            if (rec61.ciprusLoadDate.Length > 0)
                com.Parameters.Add("@CiprusLoadDate", SqlDbType.SmallDateTime, 4).Value = Convert.ToDateTime(rec61.ciprusLoadDate);

            //dls 090803 - add payment date from civitas
            if (rec61.paymentDate.Length > 0)
                com.Parameters.Add("@NotPaymentDate", SqlDbType.SmallDateTime, 4).Value = Convert.ToDateTime(rec61.paymentDate);

            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters["@NotIntNo"].Direction = ParameterDirection.InputOutput;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                notIntNo = (int)com.Parameters["@NotIntNo"].Value;

                return notIntNo;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                errMessage = e.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        public int UpdateCiprusCofCTNSResponse(CiprusCofCT66ResponseRecord rec66, string processDescr,
        string lastUser, int notIntNo, int statusErrors, int statusAlloc, int statusNoAOG, ref string errMessage)
        {
            //mrs 20090205 concatenate officer name and initials
            string officerFullName = rec66.officerName + " " + rec66.officerInit;

            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("NoticeUpdateResponse_CC", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ProcessCode", SqlDbType.Int, 4).Value = int.Parse(rec66.processCode);
            com.Parameters.Add("@SeverityCode", SqlDbType.Int, 4).Value = int.Parse(rec66.severityCode);
            com.Parameters.Add("@ProcessDescr", SqlDbType.VarChar, 100).Value = processDescr;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@StatusErrors", SqlDbType.Int, 4).Value = statusErrors;
            com.Parameters.Add("@StatusAlloc", SqlDbType.Int, 4).Value = statusAlloc;
            com.Parameters.Add("@StatusNoAOG", SqlDbType.Int, 4).Value = statusNoAOG;
            com.Parameters.Add("@NotTicketNo", SqlDbType.VarChar, 50).Value = rec66.noticeNumber;
            com.Parameters.Add("@NoticeNoType", SqlDbType.Char, 1).Value = rec66.noticeNoType;
            com.Parameters.Add("@ChgIssueAuthority", SqlDbType.VarChar, 40).Value = rec66.issuingAuthority;
            com.Parameters.Add("@ChgOfficerNatisNo", SqlDbType.VarChar, 9).Value = rec66.officeNatisNo;
            com.Parameters.Add("@ChgOfficerName", SqlDbType.VarChar, 40).Value = officerFullName; // rec61.officerName;
            com.Parameters.Add("@ChgDiscountAmount", SqlDbType.Money).Value = Convert.ToDecimal(rec66.discountAmount) / 100;  // willie fixed numbers;
            com.Parameters.Add("@ChgDiscountedAmount", SqlDbType.Money).Value = Convert.ToDecimal(rec66.discountedAmount) / 100;
            com.Parameters.Add("@ChgDemeritPoints", SqlDbType.Int, 4).Value = Int16.Parse(rec66.demeritPoints);
            com.Parameters.Add("@ChgPenaltyAmount", SqlDbType.Money).Value = Convert.ToDecimal(rec66.penaltyAmount) / 100;
            com.Parameters.Add("@ChgType", SqlDbType.VarChar, 10).Value = rec66.chargeType;
            com.Parameters.Add("@ChgLegislation", SqlDbType.Char, 1).Value = rec66.legislation;
            com.Parameters.Add("@UniqueDocID", SqlDbType.VarChar, 30).Value = rec66.uniqueDocID;

            //dls 090324 - add ciprus load date
            if (rec66.ciprusLoadDate.Length > 0)
                com.Parameters.Add("@CiprusLoadDate", SqlDbType.SmallDateTime, 4).Value = Convert.ToDateTime(rec66.ciprusLoadDate);

            //dls 090803 - add payment date from civitas
            if (rec66.paymentDate.Length > 0)
                com.Parameters.Add("@NotPaymentDate", SqlDbType.SmallDateTime, 4).Value = Convert.ToDateTime(rec66.paymentDate);

            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters["@NotIntNo"].Direction = ParameterDirection.InputOutput;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                notIntNo = (int)com.Parameters["@NotIntNo"].Value;

                return notIntNo;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                errMessage = e.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }
        // 2013-07-19 comment by Henry for useless
        //public int LoadCiprusAartoResponseRecord(string strResponseRecord, string lastUser, int notIntNo, ref string errMessage, int autIntNo, ref int chargeStatus, int noOfDays)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(mConstr);
        //    SqlCommand com = new SqlCommand("ResponseRecordMapping", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.Parameters.Add("@ResponseRecord", SqlDbType.Xml, strResponseRecord.Length).Value = strResponseRecord;
        //    //com.Parameters.Add("@CheckValue", SqlDbType.VarChar, 255).Direction = ParameterDirection.InputOutput;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    com.Parameters.Add("@NoOfDays", SqlDbType.Int, 4).Value = noOfDays;
        //    com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
        //    com.Parameters["@NotIntNo"].Direction = ParameterDirection.InputOutput;
        //    com.Parameters.Add("@ChargeStatus", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();

        //        notIntNo = (int)com.Parameters["@NotIntNo"].Value;
        //        chargeStatus = (int)com.Parameters["@ChargeStatus"].Value;

        //        //string checkValue = com.Parameters["@CheckValue"].Value.ToString();
        //        return notIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        System.Diagnostics.Debug.WriteLine(e.Message);
        //        errMessage = e.Message;
        //        return 0;
        //    }
        //    finally
        //    {
        //        con.Dispose();
        //    }
        //}
        // 2013-07-19 comment by Henry for useless
        //public int LoadCiprusAartoSAPORecord(string strSAPORecord, string BatchNo, DateTime CreatedDate, int FileNameSequenceNo, int BatchNoSequenceNo, ref string errMessage)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(mConstr);
        //    SqlCommand com = new SqlCommand("SAPORecordMapping", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.Parameters.Add("@SAPORecord", SqlDbType.Xml, strSAPORecord.Length).Value = strSAPORecord;
        //    com.Parameters.Add("@BatchNo", SqlDbType.VarChar, 20).Value = BatchNo;
        //    com.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = CreatedDate;
        //    com.Parameters.Add("@FileNameSequenceNo", SqlDbType.Int, 4).Value = FileNameSequenceNo;
        //    com.Parameters.Add("@BatchNoSequenceNo", SqlDbType.Int, 4).Value = BatchNoSequenceNo;

        //    try
        //    {
        //        con.Open();
        //        return Convert.ToInt32(com.ExecuteScalar());
        //    }
        //    catch (Exception ex)
        //    {
        //        errMessage = ex.Message;
        //        return -1;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        /// <summary>
        /// 090924 FT Update CiprusCofCT by request record
        /// </summary>
        /// <param name="strRequestRecord"></param>
        /// <param name="lastUser"></param>
        /// <param name="notIntNo"></param>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        public int UpdateCiprusCofCTRequest(string strRequestRecord, string lastUser, int notIntNo, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("NoticeUpdateRequest_CC", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@RequestRecord", SqlDbType.Xml, strRequestRecord.Length).Value = strRequestRecord;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters["@NotIntNo"].Direction = ParameterDirection.InputOutput;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                notIntNo = (int)com.Parameters["@NotIntNo"].Value;
                return notIntNo;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                errMessage = e.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        public int UpdateCiprusPIResponse(int processCode, int severityCode, string processDescr,
            string lastUser, int notIntNo, int statusErrors, int statusNoErrors)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("NoticeUpdateResponse_CPI", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ProcessCode", SqlDbType.Int, 4).Value = processCode;
            com.Parameters.Add("@SeverityCode", SqlDbType.Int, 4).Value = severityCode;
            com.Parameters.Add("@ProcessDescr", SqlDbType.VarChar, 100).Value = processDescr;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@StatusNoErrors", SqlDbType.Int, 4).Value = statusNoErrors;
            com.Parameters.Add("@StatusErrors", SqlDbType.Int, 4).Value = statusErrors;

            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters["@NotIntNo"].Direction = ParameterDirection.InputOutput;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                notIntNo = (int)com.Parameters["@NotIntNo"].Value;

                return notIntNo;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        public int UpdateNoticeChargeStatus_CPI(string autNo, string camUnitID, int notIntNo, string fileName, DateTime fileDate, int statusTo, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("NoticeChargeUpdateStatus_CPI", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutNo", SqlDbType.VarChar, 6).Value = autNo;
            com.Parameters.Add("@CamUnitID", SqlDbType.VarChar, 5).Value = camUnitID;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters.Add("@FileName", SqlDbType.VarChar, 20).Value = fileName;
            com.Parameters.Add("@FileDate", SqlDbType.SmallDateTime).Value = fileDate;
            com.Parameters.Add("@StatusTo", SqlDbType.Int, 4).Value = statusTo;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                return 0;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                System.Diagnostics.Debug.WriteLine(e.Message);
                return -1;
            }
            finally
            {
                con.Dispose();
            }
        }
        // 2013-07-19 comment by Henry for useless
        //public int UpdateNoticeChargeStatus_CA(string autNo, string camUnitID, int notIntNo, string fileName, DateTime fileDate, int statusTo, ref string errMessage)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(mConstr);
        //    SqlCommand com = new SqlCommand("NoticeChargeUpdateStatus_CPI", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@AutNo", SqlDbType.VarChar, 6).Value = autNo;
        //    com.Parameters.Add("@CamUnitID", SqlDbType.VarChar, 5).Value = camUnitID;
        //    com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
        //    com.Parameters.Add("@FileName", SqlDbType.VarChar, 20).Value = fileName;
        //    com.Parameters.Add("@FileDate", SqlDbType.SmallDateTime).Value = fileDate;
        //    com.Parameters.Add("@StatusTo", SqlDbType.Int, 4).Value = statusTo;

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();
        //        return 0;
        //    }
        //    catch (Exception e)
        //    {
        //        errMessage = e.Message;
        //        System.Diagnostics.Debug.WriteLine(e.Message);
        //        return -1;
        //    }
        //    finally
        //    {
        //        con.Dispose();
        //    }
        //}

        public int UpdateNoticeChargeStatus_CC(string autNo, string camUnitID, int notIntNo, string fileName, DateTime fileDate, int statusTo, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("NoticeChargeUpdateStatus_CPI", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutNo", SqlDbType.VarChar, 6).Value = autNo;
            com.Parameters.Add("@CamUnitID", SqlDbType.VarChar, 5).Value = camUnitID;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters.Add("@FileName", SqlDbType.VarChar, 20).Value = fileName;
            com.Parameters.Add("@FileDate", SqlDbType.SmallDateTime).Value = fileDate;
            com.Parameters.Add("@StatusTo", SqlDbType.Int, 4).Value = statusTo;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                return 0;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                System.Diagnostics.Debug.WriteLine(e.Message);
                return -1;
            }
            finally
            {
                con.Dispose();
            }
        }
        // 2013-07-19 comment by Henry for useless
        //public int UpdateNoticeReverseStatus(int autIntNo, string camUnitID, string fileName, int statusContravention, int statusTickets, int statusLoaded)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(mConstr);
        //    SqlCommand com = new SqlCommand("NoticeChargeReverseStatus", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    com.Parameters.Add("@CamUnitID", SqlDbType.VarChar, 5).Value = camUnitID;
        //    com.Parameters.Add("@FileName", SqlDbType.VarChar, 20).Value = fileName;
        //    com.Parameters.Add("@StatusTickets", SqlDbType.Int, 4).Value = statusTickets;
        //    com.Parameters.Add("@StatusLoaded", SqlDbType.Int, 4).Value = statusLoaded;
        //    com.Parameters.Add("@StatusContravention", SqlDbType.Int, 4).Value = statusContravention;

        //    SqlParameter parameterNoOfRows = com.Parameters.Add("@NoOfRows", SqlDbType.Int, 4);
        //    parameterNoOfRows.Direction = ParameterDirection.InputOutput;

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();

        //        int noOfRows = (int)com.Parameters["@NoOfRows"].Value;

        //        return noOfRows;
        //    }
        //    catch (Exception e)
        //    {
        //        System.Diagnostics.Debug.WriteLine(e.Message);
        //        return 0;
        //    }
        //    finally
        //    {
        //        con.Dispose();
        //    }
        //}
        // 2013-07-19 comment by Henry for useless
        //public int UpdateNoticeClearTicketNo_CPI(int autIntNo, string camUnitID, string fileName,
        //    int statusErrors, int statusNoErrors, int statusLoaded, int statusFixReq)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(mConstr);
        //    SqlCommand com = new SqlCommand("NoticeClearTicketNo_CPI", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    com.Parameters.Add("@CamUnitID", SqlDbType.VarChar, 5).Value = camUnitID;
        //    com.Parameters.Add("@FileName", SqlDbType.VarChar, 20).Value = fileName;
        //    com.Parameters.Add("@StatusErrors", SqlDbType.Int, 4).Value = statusErrors;
        //    com.Parameters.Add("@StatusNoErrors", SqlDbType.Int, 4).Value = statusNoErrors;
        //    com.Parameters.Add("@StatusLoaded", SqlDbType.Int, 4).Value = statusLoaded;
        //    com.Parameters.Add("@StatusFixReq", SqlDbType.Int, 4).Value = statusFixReq;

        //    SqlParameter parameterNoOfRows = com.Parameters.Add("@NoOfRows", SqlDbType.Int, 4);
        //    parameterNoOfRows.Direction = ParameterDirection.InputOutput;

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();

        //        int noOfRows = (int)com.Parameters["@NoOfRows"].Value;

        //        return noOfRows;
        //    }
        //    catch (Exception e)
        //    {
        //        System.Diagnostics.Debug.WriteLine(e.Message);
        //        return 0;
        //    }
        //    finally
        //    {
        //        con.Dispose();
        //    }
        //}
        /*
        public int AddNoticeFromCiprusPrintRequest(int autIntNo, string autName, string issuedBy, string noticeNo,
                string locDescr, string officerNo, string regNo, int speed, string vehicleMake,
                string vehicleType, string offenderType, string filmNo, string refNo, DateTime offenceDate,
                string courtNo, string companyName, string offenderSName, string offenderFName, string offenderInit,
                string postAddr1, string postAddr2, string postAddr3, string postCode, string parkMeterNo,
                string offenceCode, Double fineAmount, string easyPayNo, string printReqName,
                string docType, string offenderIDNo, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeAddFromCiprusPrintRequest", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterAutName = new SqlParameter("@AutName", SqlDbType.VarChar, 30);
            parameterAutName.Value = autName;
            myCommand.Parameters.Add(parameterAutName);

            SqlParameter parameterIssuedBy = new SqlParameter("@IssuedBy", SqlDbType.VarChar, 30);
            parameterIssuedBy.Value = issuedBy;
            myCommand.Parameters.Add(parameterIssuedBy);

            SqlParameter parameterNoticeNo = new SqlParameter("@NoticeNo", SqlDbType.VarChar, 20);
            parameterNoticeNo.Value = noticeNo;
            myCommand.Parameters.Add(parameterNoticeNo);

            SqlParameter parameterNotLocDescr = new SqlParameter("@LocDescr", SqlDbType.VarChar, 60);
            parameterNotLocDescr.Value = locDescr;
            myCommand.Parameters.Add(parameterNotLocDescr);

            SqlParameter parameterNotOfficerNo = new SqlParameter("@OfficerNo", SqlDbType.VarChar, 10);
            parameterNotOfficerNo.Value = officerNo;
            myCommand.Parameters.Add(parameterNotOfficerNo);

            SqlParameter parameterRegNo = new SqlParameter("@RegNo", SqlDbType.VarChar, 10);
            parameterRegNo.Value = regNo;
            myCommand.Parameters.Add(parameterRegNo);

            SqlParameter parameterSpeed = new SqlParameter("@Speed", SqlDbType.Int);
            parameterSpeed.Value = speed;
            myCommand.Parameters.Add(parameterSpeed);

            SqlParameter parameterVehicleMake = new SqlParameter("@VehicleMake", SqlDbType.VarChar, 30);
            parameterVehicleMake.Value = vehicleMake;
            myCommand.Parameters.Add(parameterVehicleMake);

            SqlParameter parameterVehicleType = new SqlParameter("@VehicleType", SqlDbType.VarChar, 30);
            parameterVehicleType.Value = vehicleType;
            myCommand.Parameters.Add(parameterVehicleType);

            SqlParameter parameterOffenderType = new SqlParameter("@OffenderType", SqlDbType.Char, 1);
            parameterOffenderType.Value = offenderType;
            myCommand.Parameters.Add(parameterOffenderType);

            SqlParameter parameterFilmNo = new SqlParameter("@FilmNo", SqlDbType.VarChar, 20);
            parameterFilmNo.Value = filmNo;
            myCommand.Parameters.Add(parameterFilmNo);

            SqlParameter parameterRefNo = new SqlParameter("@RefNo", SqlDbType.VarChar, 20);
            parameterRefNo.Value = refNo;
            myCommand.Parameters.Add(parameterRefNo);

            SqlParameter parameterOffenceDate = new SqlParameter("@OffenceDate", SqlDbType.DateTime);
            parameterOffenceDate.Value = offenceDate;
            myCommand.Parameters.Add(parameterOffenceDate);

            SqlParameter parameterCourtNo = new SqlParameter("@CourtNo", SqlDbType.VarChar, 6);
            parameterCourtNo.Value = courtNo;
            myCommand.Parameters.Add(parameterCourtNo);

            SqlParameter parameterCompanyName = new SqlParameter("@CompanyName", SqlDbType.VarChar, 30);
            parameterCompanyName.Value = companyName;
            myCommand.Parameters.Add(parameterCompanyName);

            SqlParameter parameterOffenderSName = new SqlParameter("@OffenderSName", SqlDbType.VarChar, 30);
            parameterOffenderSName.Value = offenderSName;
            myCommand.Parameters.Add(parameterOffenderSName);

            SqlParameter parameterOffenderFName = new SqlParameter("@OffenderFName", SqlDbType.VarChar, 40);
            parameterOffenderFName.Value = offenderFName;
            myCommand.Parameters.Add(parameterOffenderFName);

            SqlParameter parameterOffenderInit = new SqlParameter("@OffenderInit", SqlDbType.VarChar, 5);
            parameterOffenderInit.Value = offenderInit;
            myCommand.Parameters.Add(parameterOffenderInit);

            SqlParameter parameterPostAddr1 = new SqlParameter("@PostAddr1", SqlDbType.VarChar, 30);
            parameterPostAddr1.Value = postAddr1;
            myCommand.Parameters.Add(parameterPostAddr1);

            SqlParameter parameterPostAddr2 = new SqlParameter("@PostAddr2", SqlDbType.VarChar, 30);
            parameterPostAddr2.Value = postAddr2;
            myCommand.Parameters.Add(parameterPostAddr2);

            SqlParameter parameterPostAddr3 = new SqlParameter("@PostAddr3", SqlDbType.VarChar, 30);
            parameterPostAddr3.Value = postAddr3;
            myCommand.Parameters.Add(parameterPostAddr3);

            SqlParameter parameterPostCode = new SqlParameter("@PostCode", SqlDbType.VarChar, 4);
            parameterPostCode.Value = postCode;
            myCommand.Parameters.Add(parameterPostCode);

            SqlParameter parameterParkMeterNo = new SqlParameter("@ParkMeterNo", SqlDbType.VarChar, 10);
            parameterParkMeterNo.Value = parkMeterNo;
            myCommand.Parameters.Add(parameterParkMeterNo);

            SqlParameter parameterOffenceCode = new SqlParameter("@OffenceCode", SqlDbType.VarChar, 6);
            parameterOffenceCode.Value = offenceCode;
            myCommand.Parameters.Add(parameterOffenceCode);

            SqlParameter parameterFineAmount = new SqlParameter("@FineAmount", SqlDbType.Real);
            parameterFineAmount.Value = fineAmount;
            myCommand.Parameters.Add(parameterFineAmount);

            SqlParameter parameterEasyPayNo = new SqlParameter("@EasyPayNo", SqlDbType.VarChar, 30);
            parameterEasyPayNo.Value = easyPayNo;
            myCommand.Parameters.Add(parameterEasyPayNo);

            SqlParameter parameterDocType = new SqlParameter("@DocType", SqlDbType.Char, 1);
            parameterDocType.Value = docType;
            myCommand.Parameters.Add(parameterDocType);

            SqlParameter parameterOffenderIDNo = new SqlParameter("@OffenderIDNo", SqlDbType.VarChar, 25);
            parameterOffenderIDNo.Value = offenderIDNo;
            myCommand.Parameters.Add(parameterOffenderIDNo);

            SqlParameter parameterPrintReqName = new SqlParameter("@PrintReqName", SqlDbType.VarChar, 50);
            parameterPrintReqName.Value = printReqName;
            myCommand.Parameters.Add(parameterPrintReqName);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterNotIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int NotIntNo = Convert.ToInt32(parameterNotIntNo.Value);

                return NotIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }
        */
        // 2013-07-19 comment by Henry for useless
        //public int UpdateNoticeFromNATIS(string f001, string f002, string f003, string f004, DateTime f005, string f006, DateTime f007, string f008, DateTime f009, string f010,
        //    string f011, string f012, string f013, string f014, string f015, string f016, string f017, string f018, string f019, string f020,
        //    string f021, string f022, string f023, string f024, string f025, string f026, string f027, string f028, string f029, string f030,
        //    string f031, string f032, string f033, string f034, string f035, string f036, DateTime f037, string f038, string f040, string f041,
        //    string f042, string f043, string f053, string f056, string f057, string f058, string f059, string f060, string f061, string f062,
        //    string f063, string f064, string f066, string f067, string f068, string f069, DateTime f153, int autIntNo, int cs150, int cs151,
        //    int cs152, int cs153, int cs154, int cs200, int notIntNo, string outFile, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("NoticeUpdateFromNATIS", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterF001 = new SqlParameter("@F001", SqlDbType.Char, 1);
        //    parameterF001.Value = f001;
        //    myCommand.Parameters.Add(parameterF001);

        //    SqlParameter parameterF002 = new SqlParameter("@F002", SqlDbType.VarChar, 255);
        //    parameterF002.Value = f002;
        //    myCommand.Parameters.Add(parameterF002);

        //    SqlParameter parameterF003 = new SqlParameter("@F003", SqlDbType.VarChar, 10);
        //    parameterF003.Value = f003;
        //    myCommand.Parameters.Add(parameterF003);

        //    SqlParameter parameterF004 = new SqlParameter("@F004", SqlDbType.VarChar, 3);
        //    parameterF004.Value = f004;
        //    myCommand.Parameters.Add(parameterF004);

        //    SqlParameter parameterF005 = new SqlParameter("@F005", SqlDbType.SmallDateTime);
        //    parameterF005.Value = f005;
        //    myCommand.Parameters.Add(parameterF005);

        //    SqlParameter parameterF006 = new SqlParameter("@F006", SqlDbType.VarChar, 3);
        //    parameterF006.Value = f006;
        //    myCommand.Parameters.Add(parameterF006);

        //    SqlParameter parameterF007 = new SqlParameter("@F007", SqlDbType.SmallDateTime);
        //    parameterF007.Value = f007;
        //    myCommand.Parameters.Add(parameterF007);

        //    SqlParameter parameterF008 = new SqlParameter("@F008", SqlDbType.VarChar, 3);
        //    parameterF008.Value = f008;
        //    myCommand.Parameters.Add(parameterF008);

        //    SqlParameter parameterF009 = new SqlParameter("@F009", SqlDbType.SmallDateTime);
        //    parameterF009.Value = f009;
        //    myCommand.Parameters.Add(parameterF009);

        //    SqlParameter parameterF010 = new SqlParameter("@F010", SqlDbType.VarChar, 3);
        //    parameterF010.Value = f010;
        //    myCommand.Parameters.Add(parameterF010);

        //    SqlParameter parameterF011 = new SqlParameter("@F011", SqlDbType.VarChar, 3);
        //    parameterF011.Value = f011;
        //    myCommand.Parameters.Add(parameterF011);

        //    SqlParameter parameterF012 = new SqlParameter("@F012", SqlDbType.VarChar, 50);
        //    parameterF012.Value = f012;
        //    myCommand.Parameters.Add(parameterF012);

        //    SqlParameter parameterF013 = new SqlParameter("@F013", SqlDbType.VarChar, 10);
        //    parameterF013.Value = f013;
        //    myCommand.Parameters.Add(parameterF013);

        //    SqlParameter parameterF014 = new SqlParameter("@F014", SqlDbType.VarChar, 3);
        //    parameterF014.Value = f014;
        //    myCommand.Parameters.Add(parameterF014);

        //    SqlParameter parameterF015 = new SqlParameter("@F015", SqlDbType.VarChar, 25);
        //    parameterF015.Value = f015;
        //    myCommand.Parameters.Add(parameterF015);

        //    SqlParameter parameterF016 = new SqlParameter("@F016", SqlDbType.VarChar, 100);
        //    parameterF016.Value = f016;
        //    myCommand.Parameters.Add(parameterF016);

        //    SqlParameter parameterF017 = new SqlParameter("@F017", SqlDbType.VarChar, 10);
        //    parameterF017.Value = f017;
        //    myCommand.Parameters.Add(parameterF017);

        //    SqlParameter parameterF018 = new SqlParameter("@F018", SqlDbType.VarChar, 3);
        //    parameterF018.Value = f018;
        //    myCommand.Parameters.Add(parameterF018);

        //    SqlParameter parameterF019 = new SqlParameter("@F019", SqlDbType.VarChar, 100);
        //    parameterF019.Value = f019;
        //    myCommand.Parameters.Add(parameterF019);

        //    SqlParameter parameterF020 = new SqlParameter("@F020", SqlDbType.VarChar, 100);
        //    parameterF020.Value = f020;
        //    myCommand.Parameters.Add(parameterF020);

        //    SqlParameter parameterF021 = new SqlParameter("@F021", SqlDbType.VarChar, 100);
        //    parameterF021.Value = f021;
        //    myCommand.Parameters.Add(parameterF021);

        //    SqlParameter parameterF022 = new SqlParameter("@F022", SqlDbType.VarChar, 100);
        //    parameterF022.Value = f022;
        //    myCommand.Parameters.Add(parameterF022);

        //    SqlParameter parameterF023 = new SqlParameter("@F023", SqlDbType.VarChar, 100);
        //    parameterF023.Value = f023;
        //    myCommand.Parameters.Add(parameterF023);

        //    SqlParameter parameterF024 = new SqlParameter("@F024", SqlDbType.VarChar, 10);
        //    parameterF024.Value = f024;
        //    myCommand.Parameters.Add(parameterF024);

        //    SqlParameter parameterF025 = new SqlParameter("@F025", SqlDbType.VarChar, 100);
        //    parameterF025.Value = f025;
        //    myCommand.Parameters.Add(parameterF025);

        //    SqlParameter parameterF026 = new SqlParameter("@F026", SqlDbType.VarChar, 100);
        //    parameterF026.Value = f026;
        //    myCommand.Parameters.Add(parameterF026);

        //    SqlParameter parameterF027 = new SqlParameter("@F027", SqlDbType.VarChar, 100);
        //    parameterF027.Value = f027;
        //    myCommand.Parameters.Add(parameterF027);

        //    SqlParameter parameterF028 = new SqlParameter("@F028", SqlDbType.VarChar, 100);
        //    parameterF028.Value = f028;
        //    myCommand.Parameters.Add(parameterF028);

        //    SqlParameter parameterF029 = new SqlParameter("@F029", SqlDbType.VarChar, 3);
        //    parameterF029.Value = f029;
        //    myCommand.Parameters.Add(parameterF029);

        //    SqlParameter parameterF030 = new SqlParameter("@F030", SqlDbType.VarChar, 10);
        //    parameterF030.Value = f030;
        //    myCommand.Parameters.Add(parameterF030);

        //    SqlParameter parameterF031 = new SqlParameter("@F031", SqlDbType.VarChar, 3);
        //    parameterF031.Value = f031;
        //    myCommand.Parameters.Add(parameterF031);

        //    SqlParameter parameterF032 = new SqlParameter("@F032", SqlDbType.VarChar, 100);
        //    parameterF032.Value = f032;
        //    myCommand.Parameters.Add(parameterF032);

        //    SqlParameter parameterF033 = new SqlParameter("@F033", SqlDbType.VarChar, 10);
        //    parameterF033.Value = f033;
        //    myCommand.Parameters.Add(parameterF033);

        //    SqlParameter parameterF034 = new SqlParameter("@F034", SqlDbType.VarChar, 3);
        //    parameterF034.Value = f034;
        //    myCommand.Parameters.Add(parameterF034);

        //    SqlParameter parameterF035 = new SqlParameter("@F035", SqlDbType.VarChar, 10);
        //    parameterF035.Value = f035;
        //    myCommand.Parameters.Add(parameterF035);

        //    SqlParameter parameterF036 = new SqlParameter("@F036", SqlDbType.VarChar, 10);
        //    parameterF036.Value = f036;
        //    myCommand.Parameters.Add(parameterF036);

        //    SqlParameter parameterF037 = new SqlParameter("@F037", SqlDbType.SmallDateTime);
        //    parameterF037.Value = f037;
        //    myCommand.Parameters.Add(parameterF037);

        //    SqlParameter parameterF040 = new SqlParameter("@F040", SqlDbType.VarChar, 10);
        //    parameterF040.Value = f040;
        //    myCommand.Parameters.Add(parameterF040);

        //    SqlParameter parameterF041 = new SqlParameter("@F041", SqlDbType.VarChar, 3);
        //    parameterF041.Value = f041;
        //    myCommand.Parameters.Add(parameterF041);

        //    SqlParameter parameterF042 = new SqlParameter("@F042", SqlDbType.VarChar, 3);
        //    parameterF042.Value = f042;
        //    myCommand.Parameters.Add(parameterF042);

        //    SqlParameter parameterF043 = new SqlParameter("@F043", SqlDbType.VarChar, 3);
        //    parameterF043.Value = f043;
        //    myCommand.Parameters.Add(parameterF043);

        //    SqlParameter parameterF053 = new SqlParameter("@F053", SqlDbType.VarChar, 25);
        //    parameterF053.Value = f053;
        //    myCommand.Parameters.Add(parameterF053);

        //    SqlParameter parameterF056 = new SqlParameter("@F056", SqlDbType.VarChar, 3);
        //    parameterF056.Value = f056;
        //    myCommand.Parameters.Add(parameterF056);

        //    SqlParameter parameterF061 = new SqlParameter("@F061", SqlDbType.VarChar, 10);
        //    parameterF061.Value = f061;
        //    myCommand.Parameters.Add(parameterF061);

        //    SqlParameter parameterF062 = new SqlParameter("@F062", SqlDbType.VarChar, 3);
        //    parameterF062.Value = f062;
        //    myCommand.Parameters.Add(parameterF062);

        //    SqlParameter parameterF063 = new SqlParameter("@F063", SqlDbType.VarChar, 50);
        //    parameterF063.Value = f063;
        //    myCommand.Parameters.Add(parameterF063);

        //    SqlParameter parameterF064 = new SqlParameter("@F064", SqlDbType.VarChar, 3);
        //    parameterF064.Value = f064;
        //    myCommand.Parameters.Add(parameterF064);

        //    SqlParameter parameterF066 = new SqlParameter("@F066", SqlDbType.VarChar, 50);
        //    parameterF066.Value = f066;
        //    myCommand.Parameters.Add(parameterF066);

        //    SqlParameter parameterF067 = new SqlParameter("@F067", SqlDbType.VarChar, 50);
        //    parameterF067.Value = f067;
        //    myCommand.Parameters.Add(parameterF067);

        //    SqlParameter parameterF068 = new SqlParameter("@F068", SqlDbType.VarChar, 3);
        //    parameterF068.Value = f068;
        //    myCommand.Parameters.Add(parameterF068);

        //    SqlParameter parameterF069 = new SqlParameter("@F069", SqlDbType.VarChar, 3);
        //    parameterF069.Value = f069;
        //    myCommand.Parameters.Add(parameterF069);

        //    SqlParameter parameterF153 = new SqlParameter("@F153", SqlDbType.SmallDateTime);
        //    parameterF153.Value = f153;
        //    myCommand.Parameters.Add(parameterF153);

        //    SqlParameter parameterCS150 = new SqlParameter("@CS150", SqlDbType.Int);
        //    parameterCS150.Value = cs150;
        //    myCommand.Parameters.Add(parameterCS150);

        //    SqlParameter parameterCS151 = new SqlParameter("@CS151", SqlDbType.Int);
        //    parameterCS151.Value = cs151;
        //    myCommand.Parameters.Add(parameterCS151);

        //    SqlParameter parameterCS152 = new SqlParameter("@CS152", SqlDbType.Int);
        //    parameterCS152.Value = cs152;
        //    myCommand.Parameters.Add(parameterCS152);

        //    SqlParameter parameterCS153 = new SqlParameter("@CS153", SqlDbType.Int);
        //    parameterCS153.Value = cs153;
        //    myCommand.Parameters.Add(parameterCS153);

        //    SqlParameter parameterCS154 = new SqlParameter("@CS154", SqlDbType.Int);
        //    parameterCS154.Value = cs154;
        //    myCommand.Parameters.Add(parameterCS154);

        //    SqlParameter parameterCS200 = new SqlParameter("@CS200", SqlDbType.Int);
        //    parameterCS200.Value = cs200;
        //    myCommand.Parameters.Add(parameterCS200);

        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterOutFile = new SqlParameter("@OutFile", SqlDbType.VarChar, 25);
        //    parameterOutFile.Value = outFile;
        //    myCommand.Parameters.Add(parameterOutFile);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
        //    parameterNotIntNo.Value = notIntNo;
        //    parameterNotIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterNotIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        notIntNo = (int)myCommand.Parameters["@NotIntNo"].Value;

        //        return notIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        //dls 081003 - don't think this is used, and it shouldn't be!

        //public int UpdateNotice(int NotIntNo, int autIntNo, int csIntNo, string notTicketNo,
        //    string notLocDescr, string notLocCode, int notSpeedLimit, string notLaneNo,
        //    int notSpeed1, int notSpeed2, string notCameraID, string notOfficerSName,
        //    string notOfficerInit, string notOfficerNo, string notOfficerGroup, string notRegNo,
        //    string notVehicleMake, string notVehicleType,
        //    string notVehicleMakeCode, string notVehicleTypeCode, string notOffenderType,
        //    string notFilmNo, string notFrameNo, string notRefNo, string notSeqNo,
        //    DateTime notOffenceDate, string notSource, string notJPegNameA, string notJPegNameB,
        //    string notRegNoImage, string notRdTypeCode, string notRdTypeDescr, string notTravelDirection,
        //    string notCourtNo, string notCourtName, string notElapsedTime, string notOffenceType,
        //    string notNaTISIndicator, string notNaTISErrorFieldList, string notNaTISRegNo,
        //    string notVehicleRegisterNo, DateTime notVehicleLicenceExpiry, DateTime notDateCOO,
        //    string notClearanceCert, string notRegisterAuth, string notVehicleDescr, string notVehicleCat,
        //    string notNaTISVMCode, string notNaTISVTCode, string notVehicleUsage, string notVehicleColour,
        //    string notVINorChassis, string notEngine, string notStatutoryOwner, string notNatureOfPerson,
        //    string notProxyFlag, DateTime notPosted1stNoticeDate, DateTime notPosted2ndNoticeDate,
        //    DateTime notPostedSummonsDate, DateTime notPaymentDate, DateTime notIssue1stNoticeDate,
        //    DateTime notIssue2ndNoticeDate, DateTime notPrint1stNoticeDate, DateTime notPrint2ndNoticeDate,
        //    DateTime notPrintSummonsDate, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("NoticeUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterCSIntNo = new SqlParameter("@CSIntNo", SqlDbType.Int, 4);
        //    parameterCSIntNo.Value = csIntNo;
        //    myCommand.Parameters.Add(parameterCSIntNo);

        //    SqlParameter parameterNotTicketNo = new SqlParameter("@NotTicketNo", SqlDbType.VarChar, 50);
        //    parameterNotTicketNo.Value = notTicketNo;
        //    myCommand.Parameters.Add(parameterNotTicketNo);

        //    SqlParameter parameterNotLocDescr = new SqlParameter("@NotLocDescr", SqlDbType.VarChar, 60);
        //    parameterNotLocDescr.Value = notLocDescr;
        //    myCommand.Parameters.Add(parameterNotLocDescr);

        //    SqlParameter parameterNotLocCode = new SqlParameter("@NotLocCode", SqlDbType.VarChar, 15);
        //    parameterNotLocCode.Value = notLocCode;
        //    myCommand.Parameters.Add(parameterNotLocCode);

        //    SqlParameter parameterNotSpeedLimit = new SqlParameter("@NotSpeedLimit", SqlDbType.Int);
        //    parameterNotSpeedLimit.Value = notSpeedLimit;
        //    myCommand.Parameters.Add(parameterNotSpeedLimit);

        //    SqlParameter parameterNotLaneNo = new SqlParameter("@NotLaneNo", SqlDbType.Char, 1);
        //    parameterNotLaneNo.Value = notLaneNo;
        //    myCommand.Parameters.Add(parameterNotLaneNo);

        //    SqlParameter parameterNotCameraID = new SqlParameter("@NotCameraID", SqlDbType.VarChar, 15);
        //    parameterNotCameraID.Value = notCameraID;
        //    myCommand.Parameters.Add(parameterNotCameraID);

        //    SqlParameter parameterNotOfficer = new SqlParameter("@NotOfficerSName", SqlDbType.VarChar, 35);
        //    parameterNotOfficer.Value = notOfficerSName;
        //    myCommand.Parameters.Add(parameterNotOfficer);

        //    SqlParameter parameterNotOfficerInit = new SqlParameter("@NotOfficerInit", SqlDbType.VarChar, 5);
        //    parameterNotOfficerInit.Value = notOfficerInit;
        //    myCommand.Parameters.Add(parameterNotOfficerInit);

        //    SqlParameter parameterNotOfficerNo = new SqlParameter("@NotOfficerNo", SqlDbType.VarChar, 10);
        //    parameterNotOfficerNo.Value = notOfficerNo;
        //    myCommand.Parameters.Add(parameterNotOfficerNo);

        //    SqlParameter parameterNotOfficerGroup = new SqlParameter("@NotOfficerGroup", SqlDbType.Char, 3);
        //    parameterNotOfficerGroup.Value = notOfficerGroup;
        //    myCommand.Parameters.Add(parameterNotOfficerGroup);

        //    SqlParameter parameterNotRegNo = new SqlParameter("@NotRegNo", SqlDbType.VarChar, 10);
        //    parameterNotRegNo.Value = notRegNo;
        //    myCommand.Parameters.Add(parameterNotRegNo);

        //    SqlParameter parameterNotSpeed1 = new SqlParameter("@NotSpeed1", SqlDbType.Int);
        //    parameterNotSpeed1.Value = notSpeed1;
        //    myCommand.Parameters.Add(parameterNotSpeed1);

        //    SqlParameter parameterNotSpeed2 = new SqlParameter("@NotSpeed2", SqlDbType.Int);
        //    parameterNotSpeed2.Value = notSpeed2;
        //    myCommand.Parameters.Add(parameterNotSpeed2);

        //    SqlParameter parameterNotVehicleMake = new SqlParameter("@NotVehicleMake", SqlDbType.VarChar, 30);
        //    parameterNotVehicleMake.Value = notVehicleMake;
        //    myCommand.Parameters.Add(parameterNotVehicleMake);

        //    SqlParameter parameterNotVehicleType = new SqlParameter("@NotVehicleType", SqlDbType.VarChar, 30);
        //    parameterNotVehicleType.Value = notVehicleType;
        //    myCommand.Parameters.Add(parameterNotVehicleType);

        //    SqlParameter parameterNotVehicleMakeCode = new SqlParameter("@NotVehicleMakeCode", SqlDbType.VarChar, 3);
        //    parameterNotVehicleMakeCode.Value = notVehicleMakeCode;
        //    myCommand.Parameters.Add(parameterNotVehicleMakeCode);

        //    SqlParameter parameterNotVehicleTypeCode = new SqlParameter("@NotVehicleTypeCode", SqlDbType.VarChar, 3);
        //    parameterNotVehicleTypeCode.Value = notVehicleTypeCode;
        //    myCommand.Parameters.Add(parameterNotVehicleTypeCode);

        //    SqlParameter parameterNotOffenderType = new SqlParameter("@NotOffenderType", SqlDbType.Char, 1);
        //    parameterNotOffenderType.Value = notOffenderType;
        //    myCommand.Parameters.Add(parameterNotOffenderType);

        //    SqlParameter parameterNotFilmNo = new SqlParameter("@NotFilmNo", SqlDbType.VarChar, 20);
        //    parameterNotFilmNo.Value = notFilmNo;
        //    myCommand.Parameters.Add(parameterNotFilmNo);

        //    SqlParameter parameterNotFrameNo = new SqlParameter("@NotFrameNo", SqlDbType.VarChar, 20);
        //    parameterNotFrameNo.Value = notFrameNo;
        //    myCommand.Parameters.Add(parameterNotFrameNo);

        //    SqlParameter parameterNotRefNo = new SqlParameter("@NotRefNo", SqlDbType.VarChar, 20);
        //    parameterNotRefNo.Value = notRefNo;
        //    myCommand.Parameters.Add(parameterNotRefNo);

        //    SqlParameter parameterNotSeqNo = new SqlParameter("@NotSeqNo", SqlDbType.Char, 1);
        //    parameterNotSeqNo.Value = notSeqNo;
        //    myCommand.Parameters.Add(parameterNotSeqNo);

        //    SqlParameter parameterNotOffenceDate = new SqlParameter("@NotOffenceDate", SqlDbType.DateTime);
        //    parameterNotOffenceDate.Value = notOffenceDate;
        //    myCommand.Parameters.Add(parameterNotOffenceDate);

        //    SqlParameter parameterNotSource = new SqlParameter("@NotSource", SqlDbType.Char, 3);
        //    parameterNotSource.Value = notSource;
        //    myCommand.Parameters.Add(parameterNotSource);

        //    SqlParameter parameterNotJPegNameA = new SqlParameter("@NotJPegNameA", SqlDbType.VarChar, 30);
        //    parameterNotJPegNameA.Value = notJPegNameA;
        //    myCommand.Parameters.Add(parameterNotJPegNameA);

        //    SqlParameter parameterNotJPegNameB = new SqlParameter("@NotJPegNameB", SqlDbType.VarChar, 30);
        //    parameterNotJPegNameB.Value = notJPegNameB;
        //    myCommand.Parameters.Add(parameterNotJPegNameB);

        //    SqlParameter parameterNotRegNoImage = new SqlParameter("@NotRegNoImage", SqlDbType.VarChar, 30);
        //    parameterNotRegNoImage.Value = notRegNoImage;
        //    myCommand.Parameters.Add(parameterNotRegNoImage);

        //    SqlParameter parameterNotRdTypeCode = new SqlParameter("@NotRdTypeCode", SqlDbType.SmallInt);
        //    parameterNotRdTypeCode.Value = notRdTypeCode;
        //    myCommand.Parameters.Add(parameterNotRdTypeCode);

        //    SqlParameter parameterNotRdTypeDescr = new SqlParameter("@NotRdTypeDescr", SqlDbType.VarChar, 30);
        //    parameterNotRdTypeDescr.Value = notRdTypeDescr;
        //    myCommand.Parameters.Add(parameterNotRdTypeDescr);

        //    SqlParameter parameterNotCourtNo = new SqlParameter("@NotCourtNo", SqlDbType.VarChar, 6);
        //    parameterNotCourtNo.Value = notCourtNo;
        //    myCommand.Parameters.Add(parameterNotCourtNo);

        //    SqlParameter parameterNotCourtName = new SqlParameter("@NotCourtName", SqlDbType.VarChar, 30);
        //    parameterNotCourtName.Value = notCourtName;
        //    myCommand.Parameters.Add(parameterNotCourtName);

        //    SqlParameter parameterNotTravelDirection = new SqlParameter("@NotTravelDirection", SqlDbType.Char, 1);
        //    parameterNotTravelDirection.Value = notTravelDirection;
        //    myCommand.Parameters.Add(parameterNotTravelDirection);

        //    SqlParameter parameterNotElapsedTime = new SqlParameter("@NotElapsedTime", SqlDbType.VarChar, 5);
        //    parameterNotElapsedTime.Value = notElapsedTime;
        //    myCommand.Parameters.Add(parameterNotElapsedTime);

        //    //dls 080930 - don't think this proc is used, but for future reference ==> len of NotOffenceType increased to 2
        //    SqlParameter parameterNotOffenceType = new SqlParameter("@NotOffenceType", SqlDbType.Char, 2);
        //    parameterNotOffenceType.Value = notOffenceType;
        //    myCommand.Parameters.Add(parameterNotOffenceType);

        //    SqlParameter parameterNaTISIndicator = new SqlParameter("@NotNaTISIndicator", SqlDbType.Char, 1);
        //    parameterNaTISIndicator.Value = notNaTISIndicator;
        //    myCommand.Parameters.Add(parameterNaTISIndicator);

        //    SqlParameter parameterNotNaTISErrorFieldList = new SqlParameter("@NotNaTISErrorFieldList", SqlDbType.VarChar, 255);
        //    parameterNotNaTISErrorFieldList.Value = notNaTISErrorFieldList;
        //    myCommand.Parameters.Add(parameterNotNaTISErrorFieldList);

        //    SqlParameter parameterNotNaTISRegNo = new SqlParameter("@NotNaTISRegNo", SqlDbType.VarChar, 10);
        //    parameterNotNaTISRegNo.Value = notNaTISRegNo;
        //    myCommand.Parameters.Add(parameterNotNaTISRegNo);

        //    SqlParameter parameterNotVehicleRegisterNo = new SqlParameter("@NotVehicleRegisterNo", SqlDbType.VarChar, 10);
        //    parameterNotVehicleRegisterNo.Value = notVehicleRegisterNo;
        //    myCommand.Parameters.Add(parameterNotVehicleRegisterNo);

        //    SqlParameter parameterNotVehicleLicenceExpiry = new SqlParameter("@NotVehicleLicenceExpiry", SqlDbType.DateTime);
        //    parameterNotVehicleLicenceExpiry.Value = notVehicleLicenceExpiry;
        //    myCommand.Parameters.Add(parameterNotVehicleLicenceExpiry);

        //    SqlParameter parameterNotDateCOO = new SqlParameter("@NotDateCOO", SqlDbType.DateTime);
        //    parameterNotDateCOO.Value = notDateCOO;
        //    myCommand.Parameters.Add(parameterNotDateCOO);

        //    SqlParameter parameterNotClearanceCert = new SqlParameter("@NotClearanceCert", SqlDbType.VarChar, 50);
        //    parameterNotClearanceCert.Value = notClearanceCert;
        //    myCommand.Parameters.Add(parameterNotClearanceCert);

        //    SqlParameter parameterNotRegisterAuth = new SqlParameter("@NotRegisterAuth", SqlDbType.VarChar, 10);
        //    parameterNotRegisterAuth.Value = notRegisterAuth;
        //    myCommand.Parameters.Add(parameterNotRegisterAuth);

        //    SqlParameter parameterNotVehicleDescr = new SqlParameter("@NotVehicleDescr", SqlDbType.VarChar, 3);
        //    parameterNotVehicleDescr.Value = notVehicleDescr;
        //    myCommand.Parameters.Add(parameterNotVehicleDescr);

        //    SqlParameter parameterNotVehicleCat = new SqlParameter("@NotVehicleCat", SqlDbType.VarChar, 3);
        //    parameterNotVehicleCat.Value = notVehicleCat;
        //    myCommand.Parameters.Add(parameterNotVehicleCat);

        //    SqlParameter parameterNotNaTISVMCode = new SqlParameter("@NotNaTISVMCode", SqlDbType.VarChar, 3);
        //    parameterNotNaTISVMCode.Value = notNaTISVMCode;
        //    myCommand.Parameters.Add(parameterNotNaTISVMCode);

        //    SqlParameter parameterNotNaTISVTCode = new SqlParameter("@NotNaTISVTCode", SqlDbType.VarChar, 3);
        //    parameterNotNaTISVTCode.Value = notNaTISVTCode;
        //    myCommand.Parameters.Add(parameterNotNaTISVTCode);

        //    SqlParameter parameterNotVehicleUsage = new SqlParameter("@NotVehicleUsage", SqlDbType.VarChar, 3);
        //    parameterNotVehicleUsage.Value = notVehicleUsage;
        //    myCommand.Parameters.Add(parameterNotVehicleUsage);

        //    SqlParameter parameterNotVehicleColour = new SqlParameter("@NotVehicleColour", SqlDbType.VarChar, 3);
        //    parameterNotVehicleColour.Value = notVehicleColour;
        //    myCommand.Parameters.Add(parameterNotVehicleColour);

        //    SqlParameter parameterNotVINorChassis = new SqlParameter("@NotVINorChassis", SqlDbType.VarChar, 50);
        //    parameterNotVINorChassis.Value = notVINorChassis;
        //    myCommand.Parameters.Add(parameterNotVINorChassis);

        //    SqlParameter parameterNotEngine = new SqlParameter("@NotEngine", SqlDbType.VarChar, 50);
        //    parameterNotEngine.Value = notEngine;
        //    myCommand.Parameters.Add(parameterNotEngine);

        //    SqlParameter parameterNotStatutoryOwner = new SqlParameter("@NotStatutoryOwner", SqlDbType.VarChar, 3);
        //    parameterNotStatutoryOwner.Value = notStatutoryOwner;
        //    myCommand.Parameters.Add(parameterNotStatutoryOwner);

        //    SqlParameter parameterNotNatureOfPerson = new SqlParameter("@NotNatureOfPerson", SqlDbType.VarChar, 3);
        //    parameterNotNatureOfPerson.Value = notNatureOfPerson;
        //    myCommand.Parameters.Add(parameterNotNatureOfPerson);

        //    SqlParameter parameterNotProxyFlag = new SqlParameter("@NotProxyFlag", SqlDbType.Char, 1);
        //    parameterNotProxyFlag.Value = notProxyFlag;
        //    myCommand.Parameters.Add(parameterNotProxyFlag);

        //    SqlParameter parameterNotPosted1stNoticeDate = new SqlParameter("@NotPosted1stNoticeDate", SqlDbType.DateTime);
        //    parameterNotPosted1stNoticeDate.Value = notPosted1stNoticeDate;
        //    myCommand.Parameters.Add(parameterNotPosted1stNoticeDate);

        //    SqlParameter parameterNotPosted2ndNoticeDate = new SqlParameter("@NotPosted2ndNoticeDate", SqlDbType.DateTime);
        //    parameterNotPosted2ndNoticeDate.Value = notPosted2ndNoticeDate;
        //    myCommand.Parameters.Add(parameterNotPosted2ndNoticeDate);

        //    SqlParameter parameterNotPostedSummonsDate = new SqlParameter("@NotPostedSummonsDate", SqlDbType.DateTime);
        //    parameterNotPostedSummonsDate.Value = notPostedSummonsDate;
        //    myCommand.Parameters.Add(parameterNotPostedSummonsDate);

        //    SqlParameter parameterNotPaymentDate = new SqlParameter("@NotPaymentDate", SqlDbType.DateTime);
        //    parameterNotPaymentDate.Value = notPaymentDate;
        //    myCommand.Parameters.Add(parameterNotPaymentDate);

        //    SqlParameter parameterNotIssue1stNoticeDate = new SqlParameter("@NotIssue1stNoticeDate", SqlDbType.DateTime);
        //    parameterNotIssue1stNoticeDate.Value = notIssue1stNoticeDate;
        //    myCommand.Parameters.Add(parameterNotIssue1stNoticeDate);

        //    SqlParameter parameterNotIssue2ndNoticeDate = new SqlParameter("@NotIssue2ndNoticeDate", SqlDbType.DateTime);
        //    parameterNotIssue2ndNoticeDate.Value = notIssue2ndNoticeDate;
        //    myCommand.Parameters.Add(parameterNotIssue2ndNoticeDate);

        //    SqlParameter parameterNotPrint1stNoticeDate = new SqlParameter("@NotPrint1stNoticeDate", SqlDbType.DateTime);
        //    parameterNotPrint1stNoticeDate.Value = notPrint1stNoticeDate;
        //    myCommand.Parameters.Add(parameterNotPrint1stNoticeDate);

        //    SqlParameter parameterNotPrint2ndNoticeDate = new SqlParameter("@NotPrint2ndNoticeDate", SqlDbType.DateTime);
        //    parameterNotPrint2ndNoticeDate.Value = notPrint2ndNoticeDate;
        //    myCommand.Parameters.Add(parameterNotPrint2ndNoticeDate);

        //    SqlParameter parameterNotPrintSummonsDate = new SqlParameter("@NotPrintSummonsDate", SqlDbType.DateTime);
        //    parameterNotPrintSummonsDate.Value = notPrintSummonsDate;
        //    myCommand.Parameters.Add(parameterNotPrintSummonsDate);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
        //    parameterNotIntNo.Value = NotIntNo;
        //    parameterNotIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterNotIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int notIntNo = (int)myCommand.Parameters["@NotIntNo"].Value;

        //        return notIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        public int CheckNoticePrintDate(int autIntNo, string dateField)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeCheckPrintDate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterDateField = new SqlParameter("@DateField", SqlDbType.VarChar, 30);
            parameterDateField.Value = dateField;
            myCommand.Parameters.Add(parameterDateField);

            SqlParameter parameterCountNotices = new SqlParameter("@CountNotices", SqlDbType.Int, 4);
            parameterCountNotices.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterCountNotices);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int count = (int)myCommand.Parameters["@CountNotices"].Value;

                return count;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return -1;
            }
        }

        public int GetFrameForNotice(int notIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeGetFrame", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = notIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterFrameIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int frameIntNo = (int)myCommand.Parameters["@FrameIntNo"].Value;

                return frameIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        //public int UpdateNoticeTicketNo(int NotIntNo, string ticketNo, int statusTicket,
        //    int statusAmountAdded, int statusNoAOG, string lastUser)

        public int UpdateNoticeTicketNo(TicketNo ticket, int statusTicket, int statusAmountAdded, int statusNoAOG, string lastUser, out string errorMsg)
        {
            // Oscar 2013-03-15 added error message
            errorMsg = null;

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("SILCustom_Notice_NoticeUpdateTicketNo", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            myCommand.Parameters.Add("@NotTicketNo", SqlDbType.VarChar, 50).Value = ticket.NotTicketNo;
            myCommand.Parameters.Add("@StatusTicket", SqlDbType.Int, 4).Value = statusTicket;
            myCommand.Parameters.Add("@StatusAmountAdded", SqlDbType.Int, 4).Value = statusAmountAdded;
            myCommand.Parameters.Add("@StatusNoAOG", SqlDbType.Int, 4).Value = statusNoAOG;
            myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            myCommand.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = ticket.NotIntNo;
            myCommand.Parameters.Add("@NPrefix", SqlDbType.VarChar, 3).Value = ticket.NPrefix;
            myCommand.Parameters.Add("@SNumber", SqlDbType.VarChar, 6).Value = ticket.SNumber;
            myCommand.Parameters.Add("@AutNo", SqlDbType.VarChar, 3).Value = ticket.AutNumber;

            myCommand.Parameters["@NotIntNo"].Direction = ParameterDirection.InputOutput;

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int notIntNo = (int)myCommand.Parameters["@NotIntNo"].Value;

                return notIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                errorMsg = e.Message;
                return 0;
            }
        }


        public int UpdateNoticeTicketNo_CPI(int NotIntNo, string ticketNo, int statusTicket,
            int seqNo, int cupIntNo, string lastUser, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeUpdateTicketNo_CPI", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotTicketNo = new SqlParameter("@NotTicketNo", SqlDbType.VarChar, 30);
            parameterNotTicketNo.Value = ticketNo;
            myCommand.Parameters.Add(parameterNotTicketNo);

            SqlParameter parameterStatusTicket = new SqlParameter("@StatusTicket", SqlDbType.Int, 4);
            parameterStatusTicket.Value = statusTicket;
            myCommand.Parameters.Add(parameterStatusTicket);

            myCommand.Parameters.Add("@SeqNo", SqlDbType.Int, 4).Value = seqNo;
            myCommand.Parameters.Add("@CUPIntNo", SqlDbType.Int, 4).Value = cupIntNo;

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = NotIntNo;
            parameterNotIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNotIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int notIntNo = (int)myCommand.Parameters["@NotIntNo"].Value;

                return notIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                errMessage = e.Message;
                return -10;
            }
        }
        // 2013-07-19 comment by Henry for useless
        //public int UpdateNoticeFixNatis(string f001, string f002, string f003, string lastUser, int notIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("NoticeUpdateFixNatis", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterF001 = new SqlParameter("@F001", SqlDbType.VarChar, 10);
        //    parameterF001.Value = f001;
        //    myCommand.Parameters.Add(parameterF001);

        //    SqlParameter parameterF002 = new SqlParameter("@F002", SqlDbType.Char, 3);
        //    parameterF002.Value = f002;
        //    myCommand.Parameters.Add(parameterF002);

        //    SqlParameter parameterF003 = new SqlParameter("@F003", SqlDbType.Char, 3);
        //    parameterF003.Value = f003;
        //    myCommand.Parameters.Add(parameterF003);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
        //    parameterNotIntNo.Value = notIntNo;
        //    parameterNotIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterNotIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        notIntNo = (int)myCommand.Parameters["@NotIntNo"].Value;

        //        return notIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        //public int GetNoticeByJPegName(int autIntNo, string jpeg, string filmNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("NoticeByJpegName", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterNotJpegName = new SqlParameter("@NotJpegName", SqlDbType.VarChar, 30);
        //    parameterNotJpegName.Value = jpeg;
        //    myCommand.Parameters.Add(parameterNotJpegName);

        //    SqlParameter parameterFilmNo = new SqlParameter("@FilmNo", SqlDbType.VarChar, 10);
        //    parameterFilmNo.Value = filmNo;
        //    myCommand.Parameters.Add(parameterFilmNo);

        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
        //    parameterNotIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterNotIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int notIntNo = (int)myCommand.Parameters["@NotIntNo"].Value;

        //        return notIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        public int GetNoticeByNumber(int autIntNo, string ticket)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeByNumber", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterNotTicketNo = new SqlParameter("@NotTicketNo", SqlDbType.VarChar, 50);
            parameterNotTicketNo.Value = ticket;
            myCommand.Parameters.Add(parameterNotTicketNo);

            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNotIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int notIntNo = (int)myCommand.Parameters["@NotIntNo"].Value;

                return notIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int GetNoticeByFrame(int frameIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeByFrame", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@frameIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = frameIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNotIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int notIntNo = (int)myCommand.Parameters["@NotIntNo"].Value;

                return notIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public String DeleteNotice(int NotIntNo)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NoticeDelete", con);
            com.CommandType = CommandType.StoredProcedure;

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = NotIntNo;
            parameterNotIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNotIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int userId = (int)parameterNotIntNo.Value;

                return userId.ToString();
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return String.Empty;
            }
        }

        public int UpdateNoticeINP(int status, string notNatisINPName, string lastUser, int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeUpdateNatisINP", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
            parameterStatus.Value = status;
            myCommand.Parameters.Add(parameterStatus);

            SqlParameter parameterNotNatisINPName = new SqlParameter("@NotNatisINPName", SqlDbType.VarChar, 25);
            parameterNotNatisINPName.Value = notNatisINPName;
            myCommand.Parameters.Add(parameterNotNatisINPName);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            parameterAutIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterAutIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                autIntNo = (int)myCommand.Parameters["@AutIntNo"].Value;

                return autIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public SqlDataReader GetNaTISSendData(int autIntNo, int status, string lastUser)
        {
            // Returns a recordset
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeSendNaTIS", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
            parameterStatus.Value = status;
            myCommand.Parameters.Add(parameterStatus);

            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception e)
            {
                string error = e.Message;
                return null;
            }
        }

        //2013-03-14 Add by Henry for pagination
        public DataSet GetPrintrunDS(int autIntNo, string cType, int status, string showAll, DateTime dateFrom, DateTime dateTo)
        {
            int totalCount = 0;
            return GetPrintrunDS(autIntNo, cType, status, showAll, "N", dateFrom, dateTo, out totalCount);
        }

        public DataSet GetPrintrunDS(int autIntNo, string cType, int status, string showAll, DateTime dateFrom, DateTime dateTo,
            out int totalCount, int pageSize = 0, int pageIndex = 0)//2013-03-14 Add by Henry for pagination
        {
            return GetPrintrunDS(autIntNo, cType, status, showAll, "N", dateFrom, dateTo, out totalCount, pageSize, pageIndex);
        }

        public DataSet GetPrintrunDS(int autIntNo, string cType, int status, string showAll)
        {
            int totalCount = 0;
            return GetPrintrunDS(autIntNo, cType, status, showAll, "N", DateTime.MinValue, DateTime.MaxValue, out totalCount);
        }

        public DataSet GetPrintrunDS(
            int autIntNo, string cType, int status, string showAll, string showControlRegister)
        {
            int totalCount = 0;
            return GetPrintrunDS(autIntNo, cType, status, showAll, "N", DateTime.MinValue, DateTime.MaxValue, out totalCount);
        }

        //2013-03-14 Add by Henry for pagination
        public DataSet GetPrintrunDS(
            int autIntNo, string cType, int status, string showAll, string showControlRegister, DateTime dateFrom, DateTime dateTo,
            out int totalCount, int pageSize = 0, int pageIndex = 0)//2013-03-14 Add by Henry for pagination
        {
            SqlDataAdapter sqlPrintrun = new SqlDataAdapter();
            DataSet dsPrintrun = new DataSet();

            // Create Instance of Connection and Command Object
            sqlPrintrun.SelectCommand = new SqlCommand() { CommandTimeout = 0 };//2014-09-05 Heidi added for fixing time out issue.(bontq1497)
            sqlPrintrun.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlPrintrun.SelectCommand.CommandText = "NoticePrintrun";

            // Mark the Command as a SPROC
            sqlPrintrun.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            sqlPrintrun.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            sqlPrintrun.SelectCommand.Parameters.Add("@Type", SqlDbType.VarChar, 6).Value = cType;
            sqlPrintrun.SelectCommand.Parameters.Add("@StatusLoad", SqlDbType.Int, 4).Value = status;
            sqlPrintrun.SelectCommand.Parameters.Add("@ShowAll", SqlDbType.Char, 1).Value = showAll;
            sqlPrintrun.SelectCommand.Parameters.Add("@ShowControlRegister", SqlDbType.Char, 1).Value = showControlRegister;

            if (dateFrom.CompareTo(DateTime.MinValue) != 0)
                sqlPrintrun.SelectCommand.Parameters.Add("@DateFrom", SqlDbType.SmallDateTime).Value = dateFrom;
            
            if (dateTo.CompareTo(DateTime.MaxValue) != 0)
                sqlPrintrun.SelectCommand.Parameters.Add("@DateTo", SqlDbType.SmallDateTime).Value = dateTo;

            sqlPrintrun.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            sqlPrintrun.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            //2013-08-29 added by Henry for Filter by AR_5110 and AR_5120
            AuthorityRulesDB autRulDB = new AuthorityRulesDB(mConstr);
            sqlPrintrun.SelectCommand.Parameters.Add(new SqlParameter("@AR_5110", SqlDbType.NVarChar, 1)
            {
                Value = autRulDB.GetAuthorityRule(autIntNo, "5110").ARString.Trim()
            });
            sqlPrintrun.SelectCommand.Parameters.Add(new SqlParameter("@AR_5120", SqlDbType.NVarChar, 1)
            {
                Value = autRulDB.GetAuthorityRule(autIntNo, "5120").ARString.Trim()
            });

            SqlParameter sqlParam = new SqlParameter("@TotalCount", SqlDbType.Int);
            sqlParam.Direction = ParameterDirection.Output;
            sqlPrintrun.SelectCommand.Parameters.Add(sqlParam);

            // Execute the command and close the connection
            sqlPrintrun.Fill(dsPrintrun);
            sqlPrintrun.SelectCommand.Connection.Dispose();

            totalCount = (int)(sqlParam.Value == DBNull.Value ? 0 : sqlParam.Value);

            // Return the dataset result
            return dsPrintrun;
        }

        void adapter_FillError(object sender, FillErrorEventArgs e)
        {
            throw new Exception("There was an error filling the NoticeByIdNo dataset.");
        }

        /// <summary>
        /// Gets a summary of the errors for all the summons for a given authority.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetSummonsErrors(int autIntNo)
        {
            int totalCount = 0;
            return GetSummonsErrors(autIntNo, 0, 0, out totalCount);
        }

        public DataSet GetSummonsErrors(int autIntNo, int pageSize, int pageIndex, out int totalCount)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("SummonsErrorStats", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            com.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            com.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            com.Parameters.Add(paraTotalCount);

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            return ds;
        }

        /// <summary>
        /// Searches through summons issued to Summons Servers for a summons.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="summonsNo">The summons no.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet SearchServedSummons(int autIntNo, string summonsNo)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("SummonsSearch", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@SummonsNo", SqlDbType.VarChar, 50).Value = summonsNo;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);

            return ds;
        }

        ///// <summary>
        ///// Updates the summons charge status for a print file.
        ///// </summary>
        ///// <param name="autIntNo">The authority int no.</param>
        ///// <param name="printFile">The print file.</param>
        ///// <returns>The number of rows affected.</returns>
        //public int UpdateSummonsChargeStatus(int autIntNo, string printFile)
        //{
        //    SqlConnection con = new SqlConnection(mConstr);
        //    SqlCommand com = new SqlCommand("SummonsChargeUpdateStatus", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    //com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    com.Parameters.Add("@PrintFile", SqlDbType.VarChar, 50).Value = printFile;

        //    try
        //    {
        //        con.Open();
        //        return (com.ExecuteNonQuery() / 2);
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.Write(ex.Message);
        //        return -1;
        //    }
        //    finally
        //    {
        //        con.Dispose();
        //    }
        //}


        /// <summary>
        /// Updates the ciprus PI allocation.
        /// </summary>
        /// <param name="notIntNo">The not int no.</param>
        /// <param name="ticketNo">The ticket no.</param>
        /// <param name="chgFineAmount">The CHG fine amount.</param>
        /// <param name="paymentDate">The payment date.</param>
        /// <param name="easypayNo">The easypay no.</param>
        /// <param name="statusBatchOK">The status batch OK.</param>
        /// <param name="statusAlloc">The status alloc.</param>
        /// <param name="statusNoAOG">The status no AOG.</param>
        /// <param name="lastUser">The last user.</param>
        /// <param name="errMessage">The err message.</param>
        /// <returns></returns>
        // 2013-07-19 comment by Henry for useless
        //public int UpdateCiprusPIAllocation(int notIntNo, ref string ticketNo, decimal chgFineAmount,
        //    DateTime paymentDate, string easypayNo, int statusBatchOK, int statusAlloc, int statusNoAOG, string lastUser,
        //    ref string errMessage, DateTime ciprusLoadDate)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("NoticeChargeUpdateAllocation_CPI", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    myCommand.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
        //    myCommand.Parameters["@NotIntNo"].Direction = ParameterDirection.InputOutput;

        //    myCommand.Parameters.Add("@NotTicketNo", SqlDbType.VarChar, 30).Value = ticketNo;
        //    myCommand.Parameters["@NotTicketNo"].Direction = ParameterDirection.InputOutput;

        //    myCommand.Parameters.Add("@ChgFineAmount", SqlDbType.Real).Value = chgFineAmount;
        //    myCommand.Parameters.Add("@NotPaymentDate", SqlDbType.SmallDateTime).Value = paymentDate;
        //    myCommand.Parameters.Add("@NotEasyPayNumber", SqlDbType.Char, 20).Value = easypayNo;
        //    myCommand.Parameters.Add("@StatusAlloc", SqlDbType.Int, 4).Value = statusAlloc;
        //    myCommand.Parameters.Add("@StatusBatchOK", SqlDbType.Int, 4).Value = statusBatchOK;
        //    myCommand.Parameters.Add("@StatusNoAOG", SqlDbType.Int, 4).Value = statusNoAOG;
        //    myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

        //    if (ciprusLoadDate != DateTime.MinValue)
        //    {
        //        myCommand.Parameters.Add("@CiprusLoadDate", SqlDbType.SmallDateTime).Value = ciprusLoadDate;
        //    }

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        notIntNo = (int)myCommand.Parameters["@NotIntNo"].Value;
        //        ticketNo = myCommand.Parameters["@NotTicketNo"].Value.ToString();

        //        return notIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        errMessage = e.Message;
        //        return 0;
        //    }
        //}

        //dls 070106 - use in emergencies only

        // 2013-07-19 comment by Henry for useless
        //public int UpdateCiprusPIAllocationFix(int notIntNo, ref string ticketNo, decimal chgFineAmount,
        //    DateTime paymentDate, string easypayNo, int statusBatchOK, int statusAlloc, int statusNoAOG, string lastUser,
        //    ref string errMessage, DateTime ciprusLoadDate)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("NoticeChargeUpdateAllocation_CPI_Fix", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    myCommand.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
        //    myCommand.Parameters["@NotIntNo"].Direction = ParameterDirection.InputOutput;

        //    myCommand.Parameters.Add("@NotTicketNo", SqlDbType.VarChar, 30).Value = ticketNo;
        //    myCommand.Parameters["@NotTicketNo"].Direction = ParameterDirection.InputOutput;

        //    myCommand.Parameters.Add("@ChgFineAmount", SqlDbType.Real).Value = chgFineAmount;
        //    //myCommand.Parameters.Add("@NotPaymentDate", SqlDbType.SmallDateTime).Value = paymentDate;
        //    myCommand.Parameters.Add("@NotEasyPayNumber", SqlDbType.Char, 20).Value = easypayNo;
        //    myCommand.Parameters.Add("@StatusAlloc", SqlDbType.Int, 4).Value = statusAlloc;
        //    myCommand.Parameters.Add("@StatusBatchOK", SqlDbType.Int, 4).Value = statusBatchOK;
        //    myCommand.Parameters.Add("@StatusNoAOG", SqlDbType.Int, 4).Value = statusNoAOG;
        //    myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

        //    if (ciprusLoadDate != DateTime.MinValue)
        //    {
        //        myCommand.Parameters.Add("@CiprusLoadDate", SqlDbType.SmallDateTime).Value = ciprusLoadDate;
        //    }

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        notIntNo = (int)myCommand.Parameters["@NotIntNo"].Value;
        //        ticketNo = myCommand.Parameters["@NotTicketNo"].Value.ToString();

        //        return notIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        errMessage = e.Message;
        //        return 0;
        //    }
        //}

        /// <summary>
        /// Gets notices for postage in the selected autority.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <returns>A <see cref="SqlDataReader"/>.</returns>
        public DataSet GetNoticesForPostage(int autIntNo)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NoticePostalList", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        /// <summary>
        /// Sets the posted date and user for a print file name.
        /// 20090624 tf add return value for error code
        /// </summary>
        /// <param name="printFileName">Name of the print file.</param>
        /// <param name="lastUser">The last user.</param>
        /// <param name="isFirst">If set to <c>true</c> updates the first notice posted date and user.</param>
        public int SetPostedDate(string printFileName, int autIntNo, DateTime dtActualDate, string lastUser, NoticeStatus status, ref string errMessage)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            //SqlCommand com = new SqlCommand("NoticePrintSetStatus", con);
            SqlCommand com = new SqlCommand("NoticePostedSetStatus", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@PrintFileName", SqlDbType.VarChar, 50).Value = printFileName;
            com.Parameters.Add("@ActualDate", SqlDbType.SmallDateTime).Value = dtActualDate;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@Status", SqlDbType.Int, 4).Value = (int)status;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@ReturnValue", SqlDbType.Int, 4).Direction = ParameterDirection.ReturnValue;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                int i = Convert.ToInt32(com.Parameters["@ReturnValue"].Value);
                return i;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return 0;
            }
            finally
            {
                con.Close();
            }
        }

        public DataSet GetPostedNotices(string printFileName, int autIntNo, ref string errMessage)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NoticePosted_GetForQueue", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@PrintFileName", SqlDbType.VarChar, 50).Value = printFileName;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            try
            {
                con.Open();

                SqlDataAdapter da = new SqlDataAdapter(com);
                DataSet ds = new DataSet();
                da.Fill(ds);
                da.Dispose();

                return ds;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
            }
            finally
            {
                con.Dispose();
            }
            return null;
        }

        /// <summary>
        /// Gets the notice ID from a Civitas ticket number.
        /// </summary>
        /// <param name="civitasTicketNo">The civitas ticket no.</param>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns>
        /// The Notice Int No of the ticket, unless none was found (0), or multiple tickets were returned (-1).
        /// </returns>
        public string GetNoticeIDFromCivitasTicketNumber(string civitasTicketNo, int autIntNo)
        {
            string ticketNo = string.Empty;

            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NoticeGetIDFromCivitasTicketNumber", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 20).Value = civitasTicketNo;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            con.Open();
            SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection);
            int x = 0;
            while (reader.Read())
            {
                ticketNo = reader.GetString(0);
                x++;
            }
            reader.Close();
            con.Close();

            if (x > 1)
                ticketNo = string.Format("~{0} tickets were found mathing your criteria '{1}'", x, civitasTicketNo);

            return ticketNo;
        }

        /// <summary>
        /// Gets the ciprus notice batch exceptions.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="chargeStatus">The charge status.</param>
        /// <param name="cameraUnitId">The camera unit id.</param>
        /// <returns></returns>
        //public DataSet GetCiprusNoticeBatchExceptions(int autIntNo, int chargeStatus, string cameraUnitId)
        // Oscar changed for 4416
        public DataSet GetCiprusNoticeBatchExceptions(int autIntNo, int noticeStatus, string cameraUnitId,
            int pageSize, int pageIndex, out int totalCount)  //2013-04-09 add by Henry for pagination
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NoticeBatchExceptions_CPI", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@NoticeStatus", SqlDbType.Int, 4).Value = noticeStatus;
            if (!cameraUnitId.Equals("0"))
                com.Parameters.Add("@CameraUnitID", SqlDbType.VarChar, 6).Value = cameraUnitId;

            com.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            com.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            com.Parameters.Add(paraTotalCount);

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            return ds;
        }

        /// <summary>
        /// Resets the ciprus notice exceptions.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="ciprusError">The ciprus error.</param>
        /// <param name="cameraUnitID">The camera unit ID.</param>
        /// <param name="ciprusFixedStatus">The ciprus fixed status.</param>
        public int ResetCiprusNoticeExceptions(int autIntNo, int ciprusError, string cameraUnitID, int ciprusFixedStatus, int ciprusErrorStatus)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NoticeExceptionReset_CPI", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@CiprusError", SqlDbType.Int, 4).Value = ciprusError;
            com.Parameters.Add("@CameraUnitID", SqlDbType.VarChar, 6).Value = cameraUnitID;
            com.Parameters.Add("@FixedStatus", SqlDbType.Int, 4).Value = ciprusFixedStatus;
            com.Parameters.Add("@BrokenStatus", SqlDbType.Int, 4).Value = ciprusErrorStatus;

            try
            {
                con.Open();
                return com.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Finds a notice for an electronic payment.
        /// </summary>
        /// <param name="search">The search string.</param>
        /// <returns>A <see cref="SqlDataReader"/>.</returns>
        //public SqlDataReader FindNoticeForElectronicPayment(string search, int maxStatus)
        //{
        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("NoticeSearchForElectronicPayment", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@SearchString", SqlDbType.VarChar, 50).Value = search;
        //    com.Parameters.Add("@MaxChargeStatus", SqlDbType.Int, 4).Value = maxStatus;

        //    con.Open();
        //    return com.ExecuteReader(CommandBehavior.CloseConnection);
        //}

        public DataSet FindNoticeForElectronicPayment(string search, int maxStatus)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NoticeSearchForElectronicPayment", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@SearchString", SqlDbType.VarChar, 50).Value = search;
            com.Parameters.Add("@MaxChargeStatus", SqlDbType.Int, 4).Value = maxStatus;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        /// <summary>
        /// Gets a list of the new offender print file names.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="showAll">if set to <c>true</c> returns all print file names.</param>
        /// <returns></returns>
        public DataSet GetNewOffenderFiles(int autIntNo, bool showAll, int pageSize, int pageIndex, out int totalCount)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            //2014-10-08 Heidi changed for fixing time out issue(bontq1593)
            SqlCommand com = new SqlCommand("NewOffenderPrintFileList", con) { CommandTimeout = 0 };
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@AllNotices", SqlDbType.Bit, 1).Value = showAll;
            com.Parameters.Add("@PageIndex", SqlDbType.Int, 4).Value = pageIndex;
            com.Parameters.Add("@PageSize", SqlDbType.Int, 4).Value = pageSize;

            SqlParameter param = new SqlParameter("@TotalCount", SqlDbType.Int);
            param.Direction = ParameterDirection.Output;
            com.Parameters.Add(param);

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            totalCount = (int)(param.Value == DBNull.Value ? 0 : param.Value);

            return ds;
        }

        /// <summary>
        /// Gets the new offender authorities.
        /// </summary>
        /// <returns></returns>
        public SqlDataReader GetNewOffenderAuthorities()
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NewOffenderGetAuthorities", con);
            com.CommandType = CommandType.StoredProcedure;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        /// <summary>
        /// Creates the new offender notice batch.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        public int CreateNewOffenderNoticeBatch(int autIntNo, ref string errMessage, string print2ndNotices, int numberDaysFor2ndPayment)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NewOffenderCreateBatch", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@Print2ndNotices", SqlDbType.Char, 1).Value = print2ndNotices;
            com.Parameters.Add("@NumberDaysFor2ndPayment", SqlDbType.Int, 4).Value = numberDaysFor2ndPayment;
            com.Parameters.Add("@FileName", SqlDbType.VarChar, 50).Value = string.Format("RNO_{0}", DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-ff"));
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = "Create2ndNotice";

            try
            {
                con.Open();
                return Convert.ToInt32(com.ExecuteScalar());
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return -1;
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Sets the new offender printed date.
        /// </summary>
        /// <param name="printFileName">Name of the print file.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        public int SetNewOffenderPrintedDate(string printFileName, string lastUser, int autIntNo)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NewOffenderSetPrintedDate", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@FileName", SqlDbType.VarChar, 50).Value = printFileName;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            try
            {
                con.Open();
                return Convert.ToInt32(com.ExecuteScalar());
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Gets the new offender authorities.
        /// jerry 2011-1-13 changed
        /// </summary>
        /// <returns></returns>
        public SqlDataReader TicketNumberSearch(int processorSign, int notSequenceNo, string autNo, string notPrefix, int eNatisAuthorityNumber)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("TicketNumberSearch", con);
            com.CommandType = CommandType.StoredProcedure;
            //com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = nAutIntNo;
            //com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = sTicketNumber;
            com.Parameters.Add("@ProcessorSign", SqlDbType.Int, 4).Value = processorSign;
            com.Parameters.Add("@NotSequenceNo", SqlDbType.BigInt).Value = notSequenceNo;
            //com.Parameters.Add("@AutNo", SqlDbType.Char, 6).Value = autNo;
            com.Parameters.Add("@AutNo", SqlDbType.VarChar, 10).Value = autNo;          //change this to allow for up to 9 digits for when authorities are switched between different processors
            com.Parameters.Add("@NotPrefix", SqlDbType.VarChar, 4).Value = notPrefix;
            com.Parameters.Add("@ENatisAuthorityNumber", SqlDbType.Int, 4).Value = eNatisAuthorityNumber;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        /// <summary>
        /// Gets the new offender authorities.
        /// jerry 2011-1-13 changed
        /// </summary>
        /// <returns></returns>
        public DataTable  TicketNumberSearchDT(int processorSign, int notSequenceNo, string autNo, string notPrefix, int eNatisAuthorityNumber)
        {
            SqlDataAdapter sqlDANotice = new SqlDataAdapter();
            DataSet dsTicketNumberSearch = new DataSet();
            //2014-11-05 Heidi 2014-10-27 add try catch finally(5367)
            try
            {
                autNo = autNo.Replace(" ", "").Replace(" ", "");
                sqlDANotice.SelectCommand = new SqlCommand() { CommandTimeout = 0 };// Heidi 2014-10-16 added "CommandTimeout=0" for fixing time out issue.(5367)
                sqlDANotice.SelectCommand.Connection = new SqlConnection(this.mConstr);
                sqlDANotice.SelectCommand.CommandText = "TicketNumberSearch";

                sqlDANotice.SelectCommand.CommandType = CommandType.StoredProcedure;

                sqlDANotice.SelectCommand.Parameters.Add("@ProcessorSign", SqlDbType.Int, 4).Value = processorSign;
                sqlDANotice.SelectCommand.Parameters.Add("@NotSequenceNo", SqlDbType.BigInt).Value = notSequenceNo;

                sqlDANotice.SelectCommand.Parameters.Add("@AutNo", SqlDbType.VarChar, 10).Value = autNo;          //change this to allow for up to 9 digits for when authorities are switched between different processors
                sqlDANotice.SelectCommand.Parameters.Add("@NotPrefix", SqlDbType.VarChar, 4).Value = notPrefix;
                sqlDANotice.SelectCommand.Parameters.Add("@ENatisAuthorityNumber", SqlDbType.Int, 4).Value = eNatisAuthorityNumber;


                sqlDANotice.Fill(dsTicketNumberSearch);
                sqlDANotice.SelectCommand.Connection.Dispose();


                if (dsTicketNumberSearch != null && dsTicketNumberSearch.Tables.Count > 0 && dsTicketNumberSearch.Tables[0].Rows.Count > 0)
                {
                    return dsTicketNumberSearch.Tables[0];
                }

            }
            finally
            {
                if (sqlDANotice.SelectCommand.Connection != null)
                {
                    sqlDANotice.SelectCommand.Connection.Dispose();
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the new offender authorities.
        /// </summary>
        /// <returns></returns>
        public DataTable EasyPaySearch(int nAutIntNo, string sNumber)
        {
            SqlDataAdapter sqlDANotice = new SqlDataAdapter();
            DataSet dsEasyPayNumberSearch = new DataSet();

            sqlDANotice.SelectCommand = new SqlCommand();
            sqlDANotice.SelectCommand.Connection = new SqlConnection(this.mConstr);
            sqlDANotice.SelectCommand.CommandText = "EasyPayNumberSearch";

            sqlDANotice.SelectCommand.CommandType = CommandType.StoredProcedure;



            sqlDANotice.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = nAutIntNo;
            sqlDANotice.SelectCommand.Parameters.Add("@EasyPayNo", SqlDbType.VarChar, 50).Value = sNumber;

            sqlDANotice.Fill(dsEasyPayNumberSearch);
            sqlDANotice.SelectCommand.Connection.Dispose();

            if (dsEasyPayNumberSearch != null && dsEasyPayNumberSearch.Tables[0].Rows.Count > 0)
            {
                return dsEasyPayNumberSearch.Tables[0];
            }

            return null;
        }



        // 2013-07-19 comment by Henry for useless
        //public int SaveNewRegNoDetails(int notIntNo, int repIntNo, string newRegNoType, string newRegNo,
        //    int newVMIntNo, int newVTIntNo, string newVehicleColourDescr, string lastUser, ref string errMessage)
        //{
        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("NoticeUpdateNewRegNoDetails", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
        //    com.Parameters.Add("@RepIntNo", SqlDbType.Int, 4).Value = repIntNo;
        //    com.Parameters.Add("@NotNewRegNo", SqlDbType.VarChar, 10).Value = newRegNo;
        //    com.Parameters.Add("@NotNewRegNoType", SqlDbType.Char, 1).Value = newRegNoType;
        //    com.Parameters.Add("@NewVMIntNo", SqlDbType.Int, 4).Value = newVMIntNo;
        //    com.Parameters.Add("@NewVTIntNo", SqlDbType.Int, 4).Value = newVTIntNo;
        //    com.Parameters.Add("@NewVehicleColourDescr", SqlDbType.VarChar, 20).Value = newVehicleColourDescr;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();
        //        return (int)com.Parameters["@NotIntNo"].Value;
        //    }
        //    catch (Exception e)
        //    {
        //        com.Dispose();
        //        errMessage = e.Message;
        //        return 0;
        //    }
        //    finally
        //    {
        //        con.Dispose();
        //    }

        //}

        public int UpdateExpiredNotices(int autIntNo, string cameraUnitID, string lastUser, string fileName, int statusExpired, ref string errMessage, int statusErrors)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NoticeUpdateExpired_CPI", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@NotCameraID", SqlDbType.VarChar, 6).Value = cameraUnitID;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@NotCiprusPIContraventionFile", SqlDbType.VarChar, 20).Value = fileName;
            com.Parameters.Add("@StatusExpired", SqlDbType.Int, 4).Value = statusExpired;
            com.Parameters.Add("@StatusErrors", SqlDbType.Int, 4).Value = statusErrors;
            com.Parameters.Add("@CountExpired", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                return (int)com.Parameters["@CountExpired"].Value;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return -1;
            }
            finally
            {
                con.Dispose();
            }
        }

        public SqlDataReader GetNoticeListByCriteria_NotIntNoOnly(NoticeQueryCriteria criteria)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NoticeEnquiryAll_NotIntNoOnly", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = 0;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = criteria.AutIntNo;
            com.Parameters.Add("@ColName", SqlDbType.VarChar, 50).Value = criteria.ColumnName;
            com.Parameters.Add("@ColValue", SqlDbType.VarChar, 30).Value = criteria.Value;
            com.Parameters.Add("@DateSince", SqlDbType.DateTime).Value = criteria.DateSince;
            com.Parameters.Add("@MinStatus", SqlDbType.Int, 4).Value = criteria.MinStatus;
            
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public DataSet GetNoticeListByCriteria_NotIntNoOnly(string AutIntNo, string ColumnName, string ColumnValue, string DateSince, string MinStatus)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = new SqlCommand("NoticeEnquiryAll_NotIntNoOnly", new SqlConnection(this.mConstr));
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP

            da.SelectCommand.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = 0;
            da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value =AutIntNo;
            da.SelectCommand.Parameters.Add("@ColName", SqlDbType.VarChar, 50).Value =ColumnName;
            da.SelectCommand.Parameters.Add("@ColValue", SqlDbType.VarChar, 30).Value = ColumnValue;
            da.SelectCommand.Parameters.Add("@DateSince", SqlDbType.DateTime).Value =DateSince;
            da.SelectCommand.Parameters.Add("@MinStatus", SqlDbType.Int, 4).Value =MinStatus;

            // Execute the command and fill the data set
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.SelectCommand.Connection.Dispose();

            // Return the data set result
            return ds;
        }

        public SqlDataReader GetGetNonCameraImgs(int notIntNo, string mConstr)
        {
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("GetNonCameraImgs", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value =notIntNo;
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public SqlDataReader GetNoticeListByCriteria(NoticeQueryCriteria criteria)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NoticeEnquiryAll", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = 0;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = criteria.AutIntNo;
            com.Parameters.Add("@ColName", SqlDbType.VarChar, 50).Value = criteria.ColumnName;
            com.Parameters.Add("@ColValue", SqlDbType.VarChar, 30).Value = criteria.Value;
            com.Parameters.Add("@DateSince", SqlDbType.DateTime).Value = criteria.DateSince;
            com.Parameters.Add("@MinStatus", SqlDbType.Int, 4).Value = criteria.MinStatus;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public SqlDataReader GetNoticeExpiredListByAuth(int autIntNo, int noDaysForNoticeExpiry)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NoticeExpiredListByAuth", con);
            com.CommandType = CommandType.StoredProcedure;
            com.CommandTimeout = 0;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@NoDaysForNoticeExpiry", SqlDbType.Int, 4).Value = noDaysForNoticeExpiry;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return null;
            }
        }

        public int UpdateExpiredNotices(ref string errMessage, int noDaysForNoticeExpiry, int notIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeExpired", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            myCommand.CommandTimeout = 0;

            myCommand.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            myCommand.Parameters.Add("@NoDaysForNoticeExpiry", SqlDbType.Int, 4).Value = noDaysForNoticeExpiry;
            myCommand.Parameters.Add("@NoOfNotices", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();

                int noOfExpiredNotices = int.Parse(myCommand.Parameters["@NoOfNotices"].Value.ToString());
                return noOfExpiredNotices;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return -3;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        public SqlDataReader GetNoticeBeforeSummonsExpiredListByAuth(int autIntNo, int noDaysBeforeSummonsExpiry)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NoticeExpiredBeforeSummonsListByAuth", con);
            com.CommandType = CommandType.StoredProcedure;
            com.CommandTimeout = 0;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@NoDaysForNoticeExpiryBeforeSummons", SqlDbType.Int, 4).Value = noDaysBeforeSummonsExpiry;
            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return null;
            }
        }
        public int UpdateExpiredNoticesBeforeSummons(ref string errMessage, int noDaysBeforeSummonsExpiry, int notIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeExpiredBeforeSummons", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            myCommand.CommandTimeout = 0;

            myCommand.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            myCommand.Parameters.Add("@NoDaysForNoticeExpiryBeforeSummons", SqlDbType.Int, 4).Value = noDaysBeforeSummonsExpiry;
            myCommand.Parameters.Add("@NoOfNotices", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                int noOfExpiredNotices = int.Parse(myCommand.Parameters["@NoOfNotices"].Value.ToString());
                return noOfExpiredNotices;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return -3;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        public DataSet GetBMDocument(string prefix, string sequence)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand cmd = new SqlCommand("NoticeSearchBookManage", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@Prefix", SqlDbType.NVarChar, 4).Value = prefix;
            cmd.Parameters.Add("@Sequence", SqlDbType.NVarChar, 50).Value = sequence;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        // 2014-03-12, Oscar added
        public static bool SetPendingNewOffender(int notIntNo, string lastUser, bool isReset = false)
        {
            NoticeService service;
            SIL.AARTO.DAL.Entities.Notice notice;
            if (notIntNo <= 0
                || (notice = (service = new NoticeService()).GetByNotIntNo(notIntNo)) == null)
                return false;
            if ((!isReset && !notice.PendingNewOffender)
                || (isReset && notice.PendingNewOffender))
            {
                notice.PendingNewOffender = !isReset;
                notice.LastUser = lastUser;
                service.Save(notice);
            }
            return true;
        }

        //Heidi 2014-04-29 added for WithdrawSec56WithOfficerError service(5239)
        public int UpdateStatusForAutoWithdrawS56WithOfficerErrors(int notIntNo, string lastUser)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("UpdateStatusForAutoWithdrawS56WithOfficerErrors", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            try
            {
                con.Open();
                int records = Convert.ToInt32(com.ExecuteScalar());
                return records;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Dispose();
            }
        }

        public int CheckNoticeNumberAndIDNumber(int notIntNo,string IDNumber,out string message)
        {
            int flag = 1;
            message = "";
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("CheckNoticeNumberAndIDNumber", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters.Add("@IDNumber", SqlDbType.VarChar, 50).Value = IDNumber;
            try
            {
                con.Open();
                flag = Convert.ToInt32(com.ExecuteScalar());
                return flag;
            }
            catch (Exception ex)
            {
                flag = 0;
                message = ex.Message;
                return flag;
            }
            finally
            {
                con.Dispose();
            }
        }

        //Heidi 2014-04-29 added Get S56 With Officer Error Withdrawn Data(5239)
        public  DataTable GetS56WithOfficerErrorWithdrawnData(int notIntNo)
        {
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            var myConnection = new SqlConnection(this.mConstr);
            var myCommand = new SqlCommand("GetNoticeDetail_S56OfficerErrorWithdrawn", myConnection) { CommandType = CommandType.StoredProcedure };//, CommandTimeout = GetSqlCmdTimeout() 

            myCommand.Parameters.Add(new SqlParameter("@NotIntNo", SqlDbType.Int) { Value = notIntNo });
            
            var sda = new SqlDataAdapter(myCommand);
            try
            {
                myConnection.Open();
                sda.Fill(ds);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
                sda.Dispose();
            }

            return dt;
        }


		//Heidi 2014-06-06 added if isVideo=1 should be cpa5 printing
        public bool GetIsVideo(string ticketNo, string summonsNo, string Mode=null)
        {
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("NoticeGetIsVideo", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@NotTicketNo", SqlDbType.VarChar, 50).Value = ticketNo;
            com.Parameters.Add("@SummonsNo", SqlDbType.VarChar, 50).Value = summonsNo;
            com.Parameters.Add("@Mode", SqlDbType.VarChar, 5).Value = Mode;
            com.Parameters.Add("@IsVideo", SqlDbType.Bit).Direction = ParameterDirection.Output;

            // Open the connection and execute the Command
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Dispose();

                bool IsVideo = Convert.ToBoolean(com.Parameters["@IsVideo"].Value);
                return IsVideo;
            }
            catch (Exception e)
            {
                con.Dispose();
                string msg = e.Message;
                return false;
            }
        }
    

    }
}