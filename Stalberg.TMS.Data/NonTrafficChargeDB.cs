﻿using System;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.DAL.Services;
using Stalberg.TMS.Data;
using System.Collections.Generic;

namespace Stalberg.TMS.Data
{
   public class NonTrafficChargeDB
    {
        string mConstr = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:NoticeDB"/> class.
        /// </summary>
        /// <param name="vConstr">The database connection string.</param>
        public NonTrafficChargeDB(string vConstr)
        {
            mConstr = vConstr;
        }

        public DataSet GetNonTrafficChargeList(int autIntNo, string NTCCode, string NTCDescr,int UOMIntNo, int page, int PageSize, out int totalCount)
        {
            SqlDataAdapter sqlDANotice = new SqlDataAdapter();
            DataSet dsNotice = new DataSet();

            try
            {

                // Create Instance of Connection and Command Object
                sqlDANotice.SelectCommand = new SqlCommand() { CommandTimeout = 0 };
                sqlDANotice.SelectCommand.Connection = new SqlConnection(mConstr);
                sqlDANotice.SelectCommand.CommandText = "GetPagedNonTrafficChargeList";

                // Mark the Command as a SPROC
                sqlDANotice.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add Parameters to SPROC
                SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
                parameterAutIntNo.Value = autIntNo;
                sqlDANotice.SelectCommand.Parameters.Add(parameterAutIntNo);

                SqlParameter parameterNTCCode = new SqlParameter("@NTCCode", SqlDbType.NVarChar, 20);
                parameterNTCCode.Value = NTCCode;
                sqlDANotice.SelectCommand.Parameters.Add(parameterNTCCode);

                SqlParameter parameterNTCDescr = new SqlParameter("@NTCDescr", SqlDbType.NVarChar, 500);
                parameterNTCDescr.Value = NTCDescr;
                sqlDANotice.SelectCommand.Parameters.Add(parameterNTCDescr);

                SqlParameter parameterUOMIntNo = new SqlParameter("@UOMIntNo", SqlDbType.Int, 4);
                parameterUOMIntNo.Value = UOMIntNo;
                sqlDANotice.SelectCommand.Parameters.Add(parameterUOMIntNo);


                SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int, 4);
                parameterPageIndex.Value = page;
                sqlDANotice.SelectCommand.Parameters.Add(parameterPageIndex);

                SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int, 4);
                parameterPageSize.Value = PageSize;
                sqlDANotice.SelectCommand.Parameters.Add(parameterPageSize);

                SqlParameter parameterTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int, 4);
                //parameterTotalCount.Value = total;
                parameterTotalCount.Direction = ParameterDirection.Output;
                sqlDANotice.SelectCommand.Parameters.Add(parameterTotalCount);


                // Execute the command and close the connection
                sqlDANotice.Fill(dsNotice);
                sqlDANotice.SelectCommand.Connection.Dispose();

                // Return the dataset result

                totalCount = Convert.ToInt32(parameterTotalCount.Value);

                return dsNotice;
            }
            finally
            {
                if (sqlDANotice.SelectCommand.Connection != null)
                {
                    sqlDANotice.SelectCommand.Connection.Dispose();
                }

            }
        }

    }
}
