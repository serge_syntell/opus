using System;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the status of films to display 
    /// </summary>
    [Flags]
    public enum ValidationStatus : int
    {
        /// <summary>
        /// Not yet set
        /// </summary>
        None = 0,
        /// <summary>
        /// Display films for Verification 
        /// </summary>
        Verification = 1,
        /// <summary>
        /// Display films for adjudication
        /// </summary>
        Adjudication = 2,
        /// <summary>
        /// Use frame statuses for NaTIS processing being performed last
        /// </summary>
        NaTISLast = 4,
        /// <summary>
        /// Display films for Investigate
        /// </summary>
        Investigation = 8,
    }
}