using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    public class InfringementDetails
    {
        public string CertificateCode; //Field representing ‘Certificate code?= 01 - aarto, 02 for CPA 
        public string CertificateNumber; //Field representing 'Infringement certificate number' from civitas, no dashes - NotTicketNo without dashes
        public string DeliveryMethod; //Field representing 'Delivery method' if 01 then 1, if 02 then 2
        public string NoticeNo; //Field representing 'Infringement notice no.' 
        public string Surname; //Field representing 'Surname' if proxy (NotNatisProxyIndicator if greater 1 then proxy) organisation name, Offender surname - drvSurname
        public string Gender; //public string Gender; //Field representing 'Gender' blank
        public string FirstName1; //Field representing 'First name 1' blank if offender, if proxy prxSurname 
        public string FirstName2;//public string FirstName2;//Field representing 'First name 2' blank
        public string FirstName3;//public string FirstName3;//Field representing 'First name 3' blank
        public string Initials;//Field representing 'Initials' if proxy (NotNatisProxyIndicator if greater 1 then proxy) prxInitials, Offender surname - drvInitials
        public string DateOfBirth;//Field representing 'Date of birth' blank
        public string IdentificationType;//Field representing 'Identification type' lookup table: if proxy proxy ID Type, else driver ID Type
        public string IdentificationCode;
        public string TelHome;//Field representing 'Tel (home)' blank
        public string TelHomeNo;//Field representing 'Tel (home) number' blank 
        public string IdentificationNo;//Field representing 'Identification number' if proxy then proxy id, else driver id
        public string TelWork;//Field representing 'Tel (work)' blank
        public string TelWorkNo;//Field representing 'Tel (work) number' blank
        public string CountryOfIssue;//Field representing 'Country of issue' blank
        public string Fax;//Field representing 'Fax' blank
        public string FaxNumber;//Field representing 'Fax number' blank
        public string LicenceCode;//Field representing 'Licence code' blank
        public string LicenceCode2;//Field representing 'Licence code (2)' blank
        public string LicenceCode3;//Field representing 'Licence code (3)' blank
        public string Cell;//Field representing 'Cell' blank
        public string PrDP;//Field representing 'PrDP code' blank
        public string PrDP_D;//Field representing 'PrDP Code D' blank
        public string PrDP_P;//Field representing 'PrDP Code P' blank
        public string Email;//Field representing 'E-mail address' blank
        public string OperatorCardNo;//Field representing 'Operator card number' blank
        public string StreetAddress1;//Field representing 'Street address line 1' driver or proxy
        public string StreetAddress2;//Field representing 'Street address line 2' 
        public string StreetAddress3;//Field representing 'Street address line 3' 
        public string StreetAddress4;//Field representing 'Street address line 4' 
        public string StreetAddress5;//Field representing 'Street address line 5' 
        public string StreetAddressCode;//Field representing 'Street address code' 
        public string VehicleLicenseNo;//Field representing 'Vehicle licence no.' NotRegNo
        public string LicenceDiskNo;//Field representing 'Licence disc no' NotClearanceCertificate
        public string VehicleType;//Field representing 'Vehicle type' NotVehicleType
        public string PostalAddress1;//Field representing 'Post address line 1' driver or proxy
        public string PostalAddress2;//Field representing 'Post address line 2' 
        public string PostalAddress3;//Field representing 'Post address line 3' 
        public string PostalAddress4;//Field representing 'Post address line 4' 
        public string PostalAddress5;//Field representing 'Post address line 5' 
        public string PostalAddressCode;//Field representing 'Post address code' 
        public string VehicleGVM;//Field representing 'Vehicle GVM' blank
        public string VehicleMake;//Field representing 'Vehicle make' NotVehicleMake
        public string VehicleModel;//Field representing 'Vehicle model' blank 
        public string EmployerName;//Field representing 'Employer name' blank
        public string VehicleColour;//Field representing 'Vehicle colour' table called vehicle colour, VCDescription
        public string EmployerAddress1;//Field representing 'Employer address line 1' blank
        public string EmployerAddress2;//Field representing 'Employer address line 2' blank
        public string EmployerAddress3;//Field representing 'Employer address line 3' blank
        public string EmployerAddress4;//Field representing 'Employer address line 4' blank
        public string EmployerAddress5;//Field representing 'Employer address line 5' blank
        public string EmployerAddressCode;//Field representing 'Employer address code' blank
        public string Province;//Field representing 'Province' default Gauteng
        public string City;//Field representing 'City/Town'   blank
        public string Suburb;//Field representing 'Suburb' blank
        public string OffenseDate;//Field representing 'Date' OffenseDate
        public string OffenseTime;//Field representing 'Time' OffenseTime
        public string StreetA;//Field representing 'Street A' blank
        public string GeneralLocation;//Field representing 'General Location' NotLocDescription
        public string StreetB;//Field representing 'Street B' blank
        public string Route;//Field representing 'Route' blank
        public string From;//Field representing 'From' blank
        public string To;//Field representing 'To' blank
        public string GPS_X;//Field representing 'GPS co-ordinates: X' blank
        public string GPS_Y;//Field representing 'Y' blank
        public string ChargeCode;//Field representing 'Charge Code' ChargeCode
        public string ChargeDescription;//Field representing 'Description' Charge description
        public string ChargeDescription2;//Field representing 'Charge description line 2' blank
        public string ChargeDescription3;//Field representing 'Charge description line 3' blank
        public string ChargeDescription4;//Field representing 'Charge description line 4' blank
        public string SpeedReadings;//Field representing 'Speed readings' numeric speed, no fractions
        public string SpeedReadings2;//Field representing 'Speed reading (2)' 
        public string AmberTime;//Field representing 'Amber time' 0
        public string RedTime;//Field representing 'Red time' 0
        public string Type;//Field representing 'Type' if 03 then minor infringement
        public string PenaltyAmount;//Field representing 'Penalty amount' R100.00
        public string DemeritPoints;//Field representing 'Demerit points' 
        public string DiscountAmount;//Field representing 'Discount amount' 
        public string DiscountedAmount;//Field representing 'Discounted amount' 
        public string ChargeCode_alt;//Field representing 'Charge Code' blank
        public string ChargeDescription_alt;//Field representing 'Description' blank
        public string ChargeDescription2_alt;//Field representing 'Charge description line 2' blank
        public string ChargeDescription3_alt;//Field representing 'Charge description line 3' blank
        public string ChargeDescription4_alt;//Field representing 'Charge description line 4' blank
        public string Type_alt;//Field representing 'Type' blank
        public string PenaltyAmount_alt;//Field representing 'Penalty amount' blank
        public string DemeritPoints_alt;//Field representing 'Demerit points' 
        public string DiscountAmount_alt;//Field representing 'Discount amount' 
        public string DiscountedAmount_alt;//Field representing 'Discounted amount' 
        public string IssuingAuthority;//Field representing 'Issuing Authority' ChargeIssuingAuthority
        public string OfficerName;//Field representing 'Officer name ' ChgOfficerNamme
        public string InfrastructureNo;//Field representing 'Infrastructure no' ChgOfficerNatisNo
        public string ReceipientInitials;//Field representing 'Initials' offender initials either proxy or driver
        public string ReceipientSurname;//Field representing 'Surname' offender initials either proxy or driver
        public string ReceipientPostAddress1;//Field representing 'Post address line 1'  either proxy or driver
        public string ReceipientPostAddress2;//Field representing 'Post address line 2'  either proxy or driver
        public string ReceipientPostAddress3; //Field representing 'Post address line 3'  either proxy or driver
        public string ReceipientPostAddress4;//Field representing 'Post address line 4'  either proxy or driver
        public string ReceipientPostAddress5;//Field representing 'Post address line 5'  either proxy or driver
        public string ReceipientPostAddressCode;//Field representing 'Post address code' either proxy or driver
        public byte[] VehicleImage;
        public byte[] NumberPlateImage;
        public byte[] VehicleOriginalImage;
        public byte[] NumberPlateOriginalImage;
        public int ChgIntNo; //BD used for updating charge status later
        public string NotUniqueDocID;
        public int ChgLegislation;

        public string VehicleImageHash;
        public string NumberPlateImageHash;

        //Change by Tod Zhang on 2009-6-30
        public string AdjudicatorsOfficeName; //Field representing 'Adjudicators Office Name'  
        public string AdjudicatorsInfrastructureNo;//Field representing 'Adjudicators Infrastructure No'  
    }

    public class InfringementData
    {
        // Fields
        private readonly string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="InfringementData"/> class.
        /// </summary>
        /// <param name="vConstr">The v constr.</param>
        public InfringementData(string vConstr)
        {
            this.connectionString = vConstr;
        }

        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <value>The connection string.</value>
        public string ConnectionString
        {
            get { return this.connectionString; }
        }

        /// <summary>
        /// Gets the infringement details.
        /// </summary>
        /// <param name="NotIntNo">The not int no.</param>
        /// <returns>a Notice data structure</returns>
        public InfringementDetails GetInfringementDetails(int NotIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("InfringementDataList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = NotIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            InfringementDetails myInfringementDetails = new InfringementDetails();

            if (result != null)
            {
                while (result.Read())
                {
                    myInfringementDetails.CertificateCode = result["CertificateCode"].ToString();
                    myInfringementDetails.CertificateNumber = result["CertificateNUmber"].ToString();
                    myInfringementDetails.DeliveryMethod = result["DeliveryMethod"].ToString();
                    myInfringementDetails.NoticeNo = result["NotTicketNo"].ToString();
                    myInfringementDetails.Surname = result["Surname"].ToString();
                    myInfringementDetails.FirstName1 = result["FirstName1"].ToString();
                    myInfringementDetails.Initials = result["Initials"].ToString();
                    myInfringementDetails.DateOfBirth = result["DateOfBirth"].ToString();
                    myInfringementDetails.IdentificationType = result["IdentificationType"].ToString();
                    myInfringementDetails.IdentificationCode = result["IdentificationCode"].ToString();
                    myInfringementDetails.IdentificationNo = result["IdentificationNo"].ToString();
                    myInfringementDetails.StreetAddress1 = result["StreetAddress1"].ToString();
                    myInfringementDetails.StreetAddress2 = result["StreetAddress2"].ToString();
                    myInfringementDetails.StreetAddress3 = result["StreetAddress3"].ToString();
                    myInfringementDetails.StreetAddress4 = result["StreetAddress4"].ToString();
                    myInfringementDetails.StreetAddress5 = result["StreetAddress5"].ToString();
                    myInfringementDetails.StreetAddressCode = result["StreetAddressCode"].ToString();
                    myInfringementDetails.VehicleLicenseNo = result["VehicleLicenseNo"].ToString();
                    myInfringementDetails.LicenceDiskNo = result["LicenceDiskNo"].ToString();
                    myInfringementDetails.VehicleType = result["VehicleType"].ToString();
                    myInfringementDetails.PostalAddress1 = result["PostalAddress1"].ToString();
                    myInfringementDetails.PostalAddress2 = result["PostalAddress2"].ToString();
                    myInfringementDetails.PostalAddress3 = result["PostalAddress3"].ToString();
                    myInfringementDetails.PostalAddress4 = result["PostalAddress4"].ToString();
                    myInfringementDetails.PostalAddress5 = result["PostalAddress5"].ToString();
                    myInfringementDetails.PostalAddressCode = result["PostalAddressCode"].ToString();
                    myInfringementDetails.VehicleMake = result["VehicleMake"].ToString();
                    myInfringementDetails.VehicleColour = result["VehicleColour"].ToString();
                    myInfringementDetails.Province = result["Province"].ToString();
                    myInfringementDetails.City = result["City"].ToString();
                    myInfringementDetails.OffenseDate = result["OffenseDate"].ToString();
                    myInfringementDetails.OffenseTime = result["OffenseTime"].ToString();
                    myInfringementDetails.GeneralLocation = result["GeneralLocation"].ToString();
                    myInfringementDetails.ChargeCode = result["ChargeCode"].ToString();
                    myInfringementDetails.ChargeDescription = result["ChargeDescription"].ToString();
                    myInfringementDetails.SpeedReadings = result["SpeedReadings"].ToString();
                    myInfringementDetails.SpeedReadings2 = result["SpeedReadings2"].ToString();
                    myInfringementDetails.AmberTime = result["AmberTime"].ToString();
                    myInfringementDetails.RedTime = result["RedTime"].ToString();
                    myInfringementDetails.Type = result["Type"].ToString();
                    myInfringementDetails.PenaltyAmount = result["PenaltyAmount"].ToString();
                    myInfringementDetails.DemeritPoints = result["DemeritPoints"].ToString();
                    myInfringementDetails.DiscountAmount = result["DiscountAmount"].ToString();
                    myInfringementDetails.DiscountedAmount = result["DiscountedAmount"].ToString();
                    myInfringementDetails.IssuingAuthority = result["IssuingAuthority"].ToString();
                    myInfringementDetails.OfficerName = result["OfficerName"].ToString();
                    myInfringementDetails.InfrastructureNo = result["InfrastructureNo"].ToString();
                    //Change by Tod Zhang on 2009-6-30
                    myInfringementDetails.AdjudicatorsOfficeName = result["AdjOfficerName"].ToString();
                    myInfringementDetails.AdjudicatorsInfrastructureNo = result["AdjudicationOfficerInfrastructureNo"].ToString();
                    myInfringementDetails.ReceipientInitials = result["ReceipientInitials"].ToString();
                    myInfringementDetails.ReceipientSurname = result["ReceipientSurname"].ToString();
                    myInfringementDetails.ReceipientPostAddress1 = result["ReceipientPostAddress1"].ToString();
                    myInfringementDetails.ReceipientPostAddress2 = result["ReceipientPostAddress2"].ToString();
                    myInfringementDetails.ReceipientPostAddress3 = result["ReceipientPostAddress3"].ToString();
                    myInfringementDetails.ReceipientPostAddress4 = result["ReceipientPostAddress4"].ToString();
                    myInfringementDetails.ReceipientPostAddress5 = result["ReceipientPostAddress5"].ToString();
                    myInfringementDetails.ReceipientPostAddressCode = result["ReceipientPostAddressCode"].ToString();
                    myInfringementDetails.NotUniqueDocID = result["NotUniqueDocID"].ToString();

                    if (result["ChgLegislation"] != DBNull.Value)
                        myInfringementDetails.ChgLegislation = Convert.ToInt32(result["ChgLegislation"]);

                    //-- FT 020210 Remove image from Notice
					//  DLS 100212 - pelase remember that we have removed compressed images here - if we ever have to use them again, 
					//	we need to do some fancy graphic manipulation
			 		//if (!result["Not1stCompressedImage"].Equals(System.DBNull.Value))
                    //{
                    //    myInfringementDetails.VehicleImage = (byte[])result["Not1stCompressedImage"];
                    //}
                    //else
                    //{
                    //    myInfringementDetails.VehicleImage = null;
                    //}

                    //if (!result["Not2ndCompressedImage"].Equals(System.DBNull.Value))
                    //{
                    //    myInfringementDetails.NumberPlateImage = (byte[])result["Not2ndCompressedImage"];
                    //}
                    //else
                    //{
                    //    myInfringementDetails.VehicleOriginalImage = null;
                    //}

                    //-- DL 2010/03/18 Task 3673 after remove images from database, read images from remote file system.
                    int intScImIntNo = 0;
                    ScanImageDB scanImageDB = new ScanImageDB(connectionString);

                    if (!result["Not1stImage"].Equals(System.DBNull.Value))
                    {
                        //myInfringementDetails.VehicleImage = (byte[])result["Not1stImage"];
                        intScImIntNo = Convert.ToInt32(result["Not1stImage"]);
                        myInfringementDetails.VehicleOriginalImage = scanImageDB.GetScanImageDataFromRemoteServer(intScImIntNo);
                    }
                    else
                    {
                        myInfringementDetails.VehicleOriginalImage = null;
                    }

                    if (!result["Not3rdImage"].Equals(System.DBNull.Value))
                    {
                        //myInfringementDetails.NumberPlateImage = (byte[])result["Not3rdImage"];
                        intScImIntNo = Convert.ToInt32(result["Not3rdImage"]);
                        myInfringementDetails.NumberPlateOriginalImage = scanImageDB.GetScanImageDataFromRemoteServer(intScImIntNo);
                    }
                    else
                    {
                        myInfringementDetails.NumberPlateOriginalImage = null;
                    }

                    //if (!result["Not1stImage"].Equals(System.DBNull.Value))
                    //{
                    //    myInfringementDetails.VehicleOriginalImage = (byte[])result["Not1stImage"];
                    //}
                    //else
                    //{
                    //    myInfringementDetails.VehicleOriginalImage = null;
                    //}

                    //if (!result["Not3rdImage"].Equals(System.DBNull.Value))
                    //{
                    //    myInfringementDetails.NumberPlateOriginalImage = (byte[])result["Not3rdImage"];
                    //}
                    //else
                    //{
                    //    myInfringementDetails.NumberPlateOriginalImage = null;
                    //}

                    myInfringementDetails.ChgIntNo = Convert.ToInt32(result["ChgIntNo"]);

                    // Fields that are blank and being passed from DB
                    myInfringementDetails.Gender = result["Gender"].ToString();
                    myInfringementDetails.FirstName2 = result["FirstName2"].ToString();
                    myInfringementDetails.FirstName3 = result["FirstName3"].ToString();
                    myInfringementDetails.TelHome = result["TelHome"].ToString();
                    myInfringementDetails.TelHomeNo = result["TelHomeNo"].ToString();
                    myInfringementDetails.TelWork = result["TelWork"].ToString();
                    myInfringementDetails.TelWorkNo = result["TelWorkNo"].ToString();
                    myInfringementDetails.CountryOfIssue = result["CountryOfIssue"].ToString();
                    myInfringementDetails.Fax = result["Fax"].ToString();
                    myInfringementDetails.FaxNumber = result["FaxNumber"].ToString();
                    myInfringementDetails.LicenceCode = result["LicenceCode"].ToString();
                    myInfringementDetails.LicenceCode2 = result["LicenceCode2"].ToString();
                    myInfringementDetails.LicenceCode3 = result["LicenceCode3"].ToString();
                    myInfringementDetails.Cell = result["Cell"].ToString();
                    myInfringementDetails.PrDP = result["PrDP"].ToString();
                    myInfringementDetails.PrDP_D = result["PrDP_D"].ToString();
                    myInfringementDetails.PrDP_P = result["PrDP_P"].ToString();
                    myInfringementDetails.Email = result["Email"].ToString();
                    myInfringementDetails.OperatorCardNo = result["OperatorCardNo"].ToString();
                    myInfringementDetails.VehicleGVM = result["VehicleGVM"].ToString();
                    myInfringementDetails.VehicleModel = result["VehicleModel"].ToString();
                    myInfringementDetails.EmployerName = result["EmployerName"].ToString();
                    myInfringementDetails.EmployerAddress1 = result["EmployerAddress1"].ToString();
                    myInfringementDetails.EmployerAddress2 = result["EmployerAddress2"].ToString();
                    myInfringementDetails.EmployerAddress3 = result["EmployerAddress3"].ToString();
                    myInfringementDetails.EmployerAddress4 = result["EmployerAddress4"].ToString();
                    myInfringementDetails.EmployerAddress5 = result["EmployerAddress5"].ToString();
                    myInfringementDetails.EmployerAddressCode = result["EmployerAddressCode"].ToString();
                    myInfringementDetails.Suburb = result["Suburb"].ToString();
                    myInfringementDetails.StreetA = result["StreetA"].ToString();
                    myInfringementDetails.StreetB = result["StreetB"].ToString();
                    myInfringementDetails.Route = result["sRoute"].ToString();
                    myInfringementDetails.From = result["sFrom"].ToString();
                    myInfringementDetails.To = result["sTo"].ToString();
                    myInfringementDetails.GPS_X = result["GPS_X"].ToString();
                    myInfringementDetails.GPS_Y = result["GPS_Y"].ToString();
                    myInfringementDetails.ChargeDescription2 = result["ChargeDescription2"].ToString();
                    myInfringementDetails.ChargeDescription3 = result["ChargeDescription3"].ToString();
                    myInfringementDetails.ChargeDescription4 = result["ChargeDescription4"].ToString();
                    myInfringementDetails.ChargeCode_alt = result["ChargeCode_alt"].ToString();
                    myInfringementDetails.ChargeDescription_alt = result["ChargeDescription_alt"].ToString();
                    myInfringementDetails.ChargeDescription2_alt = result["ChargeDescription2_alt"].ToString();
                    myInfringementDetails.ChargeDescription3_alt = result["ChargeDescription3_alt"].ToString();
                    myInfringementDetails.ChargeDescription4_alt = result["ChargeDescription4_alt"].ToString();
                    myInfringementDetails.Type_alt = result["Type_alt"].ToString();
                    myInfringementDetails.PenaltyAmount_alt = result["PenaltyAmount_alt"].ToString();

                }
                result.Close();
            }
            return myInfringementDetails;
        }

        /// <summary>
        /// Creates the new line in the print file batch table
        /// </summary>
        /// <returns></returns>
        // 2013-07-25 comment out by Henry for useless
        //public int CreateNewPrintFileBatch(string FileName, string BatchNo, DateTime CreatedDate, int FileNameSequenceNo, int BatchNoSequenceNo, ref string errMessage)
        //{
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("NewPrintFileBatch", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@FileName", SqlDbType.VarChar, 20).Value = FileName;
        //    com.Parameters.Add("@BatchNo", SqlDbType.VarChar, 20).Value = BatchNo;
        //    com.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = CreatedDate;
        //    com.Parameters.Add("@FileNameSequenceNo", SqlDbType.Int, 4).Value = FileNameSequenceNo;
        //    com.Parameters.Add("@BatchNoSequenceNo", SqlDbType.Int, 4).Value = BatchNoSequenceNo;

        //    try
        //    {
        //        con.Open();
        //        return Convert.ToInt32(com.ExecuteScalar());
        //    }
        //    catch (Exception ex)
        //    {
        //        errMessage = ex.Message;
        //        return -1;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        /// <summary>
        /// marks the line in the print file batch table as failed
        /// </summary>
        /// <returns></returns>
        // 2013-07-19 comment by Henry for useless
        //public int FailPrintFileBatch(string FileName, string BatchNo, ref string errMessage)
        //{
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("FailPrintFileBatch", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@FileName", SqlDbType.VarChar, 20).Value = FileName;
        //    com.Parameters.Add("@BatchNo", SqlDbType.VarChar, 20).Value = BatchNo;

        //    try
        //    {
        //        con.Open();
        //        return Convert.ToInt32(com.ExecuteScalar());
        //    }
        //    catch (Exception ex)
        //    {
        //        errMessage = ex.Message;
        //        return -1;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        /// <summary>
        /// gets a list of all print files in the system
        /// </summary>
        /// <returns></returns>
        public SqlDataReader GetPrintFileBatchList()
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("PrintFileBatchList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        /// <summary>
        /// Get the max of the unique ID in the PrintFileBatch table = max of Batch No
        /// </summary>
        /// <returns></returns>
        public int PFB_MaxUniqueID(ref string errMessage)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("PFB_MaxUniqueID", con);
            com.CommandType = CommandType.StoredProcedure;

            try
            {
                con.Open();
                return Convert.ToInt32(com.ExecuteScalar());
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return -1;
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Updates the PFB max batch no.
        /// </summary>
        /// <param name="imgFileName">Name of the img file.</param>
        /// <param name="maxBatchNo">The max batch no.</param>
        // 2013-07-25 comment out by Henry for useless
        //public void PFB_UpdateMaxBatchNo(string imgFileName,int maxBatchNo)
        //{
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand cmd = new SqlCommand("PFB_UpdateMaxBatchNo", con);
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    cmd.Parameters.Add("@ImgFileName", SqlDbType.VarChar, 20).Value = imgFileName;
        //    cmd.Parameters.Add("@MaxBatchNo", SqlDbType.Int, 4).Value = maxBatchNo;

        //    try
        //    {
        //        con.Open();
        //        cmd.ExecuteNonQuery();
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.Out.WriteLine(ex);
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        /// <summary>
        /// Get the max of the Batch No in the PrintFileBatch table
        /// </summary>
        /// <returns></returns>
        public int PFB_MaxBatchNo(ref string errMessage)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("PFB_MaxBatchNo", con);
            com.CommandType = CommandType.StoredProcedure;

            try
            {
                con.Open();
                return Convert.ToInt32(com.ExecuteScalar());
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return -1;
            }
            finally
            {
                con.Close();
            }
        }

        public string CreateInfringementLine(InfringementDetails infringeDetails)
        {
            //check if the ID is an RSA ID
            string gender = "";
            string DOB = "";

            if (infringeDetails.IdentificationCode == "02")
            {
                if (IDNoCheck(infringeDetails.IdentificationNo))
                {
                    DOB = "19" + infringeDetails.IdentificationNo.Substring(0, 2) + "/" + infringeDetails.IdentificationNo.Substring(2, 2) + "/" + infringeDetails.IdentificationNo.Substring(4, 2);
                    if (Convert.ToInt32(infringeDetails.IdentificationNo.Substring(6, 1)) < 5)
                    {
                        gender = "Female";
                    }
                    else
                    {
                        gender = "Male";
                    }
                }
                else
                {
                    gender = "";
                    DOB = "";
                }

            }

            string iLine = "";
            iLine += infringeDetails.CertificateCode; //Field representing ‘Certificate code?= 01 - aarto, 02 for CPA 
            iLine += "%" + infringeDetails.NotUniqueDocID; //Has been changed to an unique document id, Field representing 'Infringement certificate number' from civitas, no dashes - NotTicketNo without dashes
            iLine += "%" + infringeDetails.DeliveryMethod; //Field representing 'Delivery method' if 01 then 1, if 02 then 2
            iLine += "%" + infringeDetails.NoticeNo; //Field representing 'Infringement notice no.' 
            iLine += "%" + infringeDetails.Surname; //Field representing 'Surname' if proxy (NotNatisProxyIndicator if greater 1 then proxy) organisation name, Offender surname - drvSurname
            iLine += "%" + gender; //iLine += "%" + infringeDetails.Gender; //Field representing 'Gender' blank
            iLine += "%" + infringeDetails.FirstName1; //Field representing 'First name 1' blank if offender, if proxy prxSurname 
            iLine += "%" + infringeDetails.FirstName2;//iLine += "%" + infringeDetails.FirstName2;//Field representing 'First name 2' blank
            iLine += "%" + infringeDetails.FirstName3;//iLine += "%" + infringeDetails.FirstName3;//Field representing 'First name 3' blank
            iLine += "%" + infringeDetails.Initials;//Field representing 'Initials' if proxy (NotNatisProxyIndicator if greater 1 then proxy) prxInitials, Offender surname - drvInitials
            iLine += "%" + DOB;//Field representing 'Date of birth' blank
            iLine += "%" + infringeDetails.IdentificationType;//Field representing 'Identification type' lookup table: if proxy proxy ID Type, else driver ID Type
            iLine += "%" + infringeDetails.TelHome;//Field representing 'Tel (home)' blank
            iLine += "%" + infringeDetails.TelHomeNo;//Field representing 'Tel (home) number' blank 
            if (infringeDetails.IdentificationCode == "04")
            {
                iLine += "%";
            }
            else
            {
                iLine += "%" + infringeDetails.IdentificationNo;//Field representing 'Identification number' if proxy then proxy id, else driver id
            }
            iLine += "%" + infringeDetails.TelWork;//Field representing 'Tel (work)' blank
            iLine += "%" + infringeDetails.TelWorkNo;//Field representing 'Tel (work) number' blank
            iLine += "%" + infringeDetails.CountryOfIssue;//Field representing 'Country of issue' set to republic of south africa
            iLine += "%" + infringeDetails.Fax;//Field representing 'Fax' blank
            iLine += "%" + infringeDetails.FaxNumber;//Field representing 'Fax number' blank
            iLine += "%" + infringeDetails.LicenceCode;//Field representing 'Licence code' blank
            iLine += "%" + infringeDetails.LicenceCode2;//Field representing 'Licence code (2)' blank
            iLine += "%" + infringeDetails.LicenceCode3;//Field representing 'Licence code (3)' blank
            iLine += "%" + infringeDetails.Cell;//Field representing 'Cell' blank
            iLine += "%" + infringeDetails.PrDP;//Field representing 'PrDP code' blank
            iLine += "%" + infringeDetails.PrDP_D;//Field representing 'PrDP Code D' blank
            iLine += "%" + infringeDetails.PrDP_P;//Field representing 'PrDP Code P' blank
            iLine += "%" + infringeDetails.Email;//Field representing 'E-mail address' blank
            iLine += "%" + infringeDetails.OperatorCardNo;//Field representing 'Operator card number' blank
            iLine += "%" + infringeDetails.StreetAddress1;//Field representing 'Street address line 1' driver or proxy
            iLine += "%" + infringeDetails.StreetAddress2;//Field representing 'Street address line 2' 
            iLine += "%" + infringeDetails.StreetAddress3;//Field representing 'Street address line 3' 
            iLine += "%" + infringeDetails.StreetAddress4;//Field representing 'Street address line 4' 
            iLine += "%" + infringeDetails.StreetAddress5;//Field representing 'Street address line 5' 
            iLine += "%" + infringeDetails.StreetAddressCode;//Field representing 'Street address code' 
            iLine += "%" + infringeDetails.VehicleLicenseNo;//Field representing 'Vehicle licence no.' NotRegNo
            iLine += "%" + infringeDetails.LicenceDiskNo;//Field representing 'Licence disc no' NotClearanceCertificate
            iLine += "%" + infringeDetails.VehicleType;//Field representing 'Vehicle type' NotVehicleType
            iLine += "%" + infringeDetails.PostalAddress1;//Field representing 'Post address line 1' driver or proxy
            iLine += "%" + infringeDetails.PostalAddress2;//Field representing 'Post address line 2' 
            iLine += "%" + infringeDetails.PostalAddress3;//Field representing 'Post address line 3' 
            iLine += "%" + infringeDetails.PostalAddress4;//Field representing 'Post address line 4' 
            iLine += "%" + infringeDetails.PostalAddress5;//Field representing 'Post address line 5' 
            iLine += "%" + infringeDetails.PostalAddressCode;//Field representing 'Post address code' 
            iLine += "%" + infringeDetails.VehicleGVM;//Field representing 'Vehicle GVM' blank
            iLine += "%" + infringeDetails.VehicleMake;//Field representing 'Vehicle make' NotVehicleMake
            iLine += "%" + infringeDetails.VehicleModel;//Field representing 'Vehicle model' blank 
            iLine += "%" + infringeDetails.EmployerName;//Field representing 'Employer name' blank
            iLine += "%" + infringeDetails.VehicleColour;//Field representing 'Vehicle colour' table called vehicle colour, VCDescription
            iLine += "%" + infringeDetails.EmployerAddress1;//Field representing 'Employer address line 1' blank
            iLine += "%" + infringeDetails.EmployerAddress2;//Field representing 'Employer address line 2' blank
            iLine += "%" + infringeDetails.EmployerAddress3;//Field representing 'Employer address line 3' blank
            iLine += "%" + infringeDetails.EmployerAddress4;//Field representing 'Employer address line 4' blank
            iLine += "%" + infringeDetails.EmployerAddress5;//Field representing 'Employer address line 5' blank
            iLine += "%" + infringeDetails.EmployerAddressCode;//Field representing 'Employer address code' blank
            iLine += "%" + infringeDetails.Province;//Field representing 'Province' default Gauteng
            iLine += "%" + infringeDetails.City;//Field representing 'City/Town'   blank
            iLine += "%" + infringeDetails.Suburb;//Field representing 'Suburb' blank
            iLine += "%" + infringeDetails.OffenseDate;//Field representing 'Date' OffenseDate
            iLine += "%" + infringeDetails.OffenseTime;//Field representing 'Time' OffenseTime
            iLine += "%" + infringeDetails.StreetA;//Field representing 'Street A' blank
            iLine += "%" + infringeDetails.GeneralLocation;//Field representing 'General Location' NotLocDescription
            iLine += "%" + infringeDetails.StreetB;//Field representing 'Street B' blank
            iLine += "%" + infringeDetails.Route;//Field representing 'Route' blank
            iLine += "%" + infringeDetails.From;//Field representing 'From' blank
            iLine += "%" + infringeDetails.To;//Field representing 'To' blank
            iLine += "%" + infringeDetails.GPS_X;//Field representing 'GPS co-ordinates: X' blank
            iLine += "%" + infringeDetails.GPS_Y;//Field representing 'Y' blank
            iLine += "%" + infringeDetails.ChargeCode;//Field representing 'Charge Code' ChargeCode
            iLine += "%" + infringeDetails.ChargeDescription;//Field representing 'Description' Charge description
            iLine += "%" + infringeDetails.ChargeDescription2;//Field representing 'Charge description line 2' blank
            iLine += "%" + infringeDetails.ChargeDescription3;//Field representing 'Charge description line 3' blank
            iLine += "%" + infringeDetails.ChargeDescription4;//Field representing 'Charge description line 4' blank
            iLine += "%" + infringeDetails.SpeedReadings;//Field representing 'Speed readings' numeric speed, no fractions
            iLine += "%" + infringeDetails.SpeedReadings2;//Field representing 'Speed reading (2)' 
            iLine += "%" + infringeDetails.AmberTime;//Field representing 'Amber time' 0
            iLine += "%" + infringeDetails.RedTime;//Field representing 'Red time' 0
            iLine += "%" + infringeDetails.Type;//Field representing 'Type' if 03 then minor infringement
            iLine += "%" + infringeDetails.PenaltyAmount;//Field representing 'Penalty amount' R100.00
            iLine += "%" + infringeDetails.DemeritPoints;//Field representing 'Demerit points' 
            iLine += "%" + infringeDetails.DiscountAmount;//Field representing 'Discount amount' 
            iLine += "%" + infringeDetails.DiscountedAmount;//Field representing 'Discounted amount' 
            iLine += "%" + infringeDetails.ChargeCode_alt;//Field representing 'Charge Code' blank
            iLine += "%" + infringeDetails.ChargeDescription_alt;//Field representing 'Description' blank
            iLine += "%" + infringeDetails.ChargeDescription2_alt;//Field representing 'Charge description line 2' blank
            iLine += "%" + infringeDetails.ChargeDescription3_alt;//Field representing 'Charge description line 3' blank
            iLine += "%" + infringeDetails.ChargeDescription4_alt;//Field representing 'Charge description line 4' blank
            iLine += "%" + infringeDetails.Type_alt;//Field representing 'Type' blank
            iLine += "%" + infringeDetails.PenaltyAmount_alt;//Field representing 'Penalty amount' blank
            iLine += "%" + infringeDetails.DemeritPoints_alt;//Field representing 'Demerit points' 
            iLine += "%" + infringeDetails.DiscountAmount_alt;//Field representing 'Discount amount' 
            iLine += "%" + infringeDetails.DiscountedAmount_alt;//Field representing 'Discounted amount' 
            iLine += "%" + infringeDetails.IssuingAuthority;//Field representing 'Issuing Authority' ChargeIssuingAuthority
            iLine += "%" + infringeDetails.OfficerName;//Field representing 'Officer name ' ChgOfficerNamme
            iLine += "%" + infringeDetails.InfrastructureNo;//Field representing 'Infrastructure no' ChgOfficerNatisNo
            //Changed by Tod Zhang on 2009-6-30
            iLine += "%" + infringeDetails.AdjudicatorsOfficeName;//Field representing 'Adjudicators Office Name' 
            iLine += "%" + infringeDetails.AdjudicatorsInfrastructureNo;//Field representing 'Adjudicators Infrastructure No' 
            iLine += "%" + infringeDetails.ReceipientInitials;//Field representing 'Initials' offender initials either proxy or driver
            iLine += "%" + infringeDetails.ReceipientSurname;//Field representing 'Surname' offender initials either proxy or driver
            iLine += "%" + infringeDetails.ReceipientPostAddress1;//Field representing 'Post address line 1'  either proxy or driver
            iLine += "%" + infringeDetails.ReceipientPostAddress2;//Field representing 'Post address line 2'  either proxy or driver
            iLine += "%" + infringeDetails.ReceipientPostAddress3; //Field representing 'Post address line 3'  either proxy or driver
            iLine += "%" + infringeDetails.ReceipientPostAddress4;
            iLine += "%" + infringeDetails.ReceipientPostAddress5;
            iLine += "%" + infringeDetails.ReceipientPostAddressCode;

            iLine += "\n";
            return iLine;
        }

        private bool IDNoCheck(string IDNo)
        {
            int d = -1;
            try
            {
                int a = 0;
                for (int i = 0; i < 6; i++)
                {
                    a += int.Parse(IDNo[2 * i].ToString());
                }
                int b = 0;
                for (int i = 0; i < 6; i++)
                {
                    b = b * 10 + int.Parse(IDNo[2 * i + 1].ToString());
                }
                b *= 2;
                int c = 0;
                do
                {
                    c += b % 10;
                    b = b / 10;
                }
                while (b > 0);
                c += a;
                d = 10 - (c % 10);
                if (d == 10) d = 0;
            }
            catch {/*ignore*/}

            if (d == -1)
                return false;

            return true;
        }
    }
}
