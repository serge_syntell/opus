﻿using System;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.DAL.Services;
using Stalberg.TMS.Data;
using System.Collections.Generic;


namespace Stalberg.TMS.Data
{
    public class QuoteDB
    {
        string mConstr = string.Empty;

        public QuoteDB(string vConstr)
        {
            mConstr = vConstr;
        }

        public DataSet GetQuoteList(int autIntNo, string NTCCode, string Name, string QuoteReferenceNumber, string RctNumber, DateTime beginDate, DateTime endDate, int page, int PageSize, out int totalCount)
        {
            SqlDataAdapter sqlDANotice = new SqlDataAdapter();
            DataSet dsNotice = new DataSet();

            try
            {

                // Create Instance of Connection and Command Object
                sqlDANotice.SelectCommand = new SqlCommand() { CommandTimeout = 0 };
                sqlDANotice.SelectCommand.Connection = new SqlConnection(mConstr);
                sqlDANotice.SelectCommand.CommandText = "GetPagedQuoteList";
                

                // Mark the Command as a SPROC
                sqlDANotice.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add Parameters to SPROC
                SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
                parameterAutIntNo.Value = autIntNo;
                sqlDANotice.SelectCommand.Parameters.Add(parameterAutIntNo);

                SqlParameter parameterNTCCode = new SqlParameter("@NTCCode", SqlDbType.NVarChar, 20);
                parameterNTCCode.Value = NTCCode;
                sqlDANotice.SelectCommand.Parameters.Add(parameterNTCCode);

                SqlParameter parameterName = new SqlParameter("@Name", SqlDbType.NVarChar,100);
                parameterName.Value = Name;
                sqlDANotice.SelectCommand.Parameters.Add(parameterName);

                SqlParameter parameterQuoteReferenceNumber = new SqlParameter("@QuoteReferenceNumber", SqlDbType.NVarChar, 50);
                parameterQuoteReferenceNumber.Value = QuoteReferenceNumber;
                sqlDANotice.SelectCommand.Parameters.Add(parameterQuoteReferenceNumber);

                SqlParameter parameterRctNumber = new SqlParameter("@RctNumber", SqlDbType.VarChar, 15);
                parameterRctNumber.Value = RctNumber;
                sqlDANotice.SelectCommand.Parameters.Add(parameterRctNumber);

                SqlParameter parameterBeginDate = new SqlParameter("@BeginDate", SqlDbType.DateTime);
                parameterBeginDate.Value = beginDate;
                sqlDANotice.SelectCommand.Parameters.Add(parameterBeginDate);

                SqlParameter parameterEndDate = new SqlParameter("@EndDate", SqlDbType.DateTime);
                parameterEndDate.Value = endDate;
                sqlDANotice.SelectCommand.Parameters.Add(parameterEndDate);


                SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int, 4);
                parameterPageIndex.Value = page;
                sqlDANotice.SelectCommand.Parameters.Add(parameterPageIndex);

                SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int, 4);
                parameterPageSize.Value = PageSize;
                sqlDANotice.SelectCommand.Parameters.Add(parameterPageSize);

                SqlParameter parameterTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int, 4);
                //parameterTotalCount.Value = total;
                parameterTotalCount.Direction = ParameterDirection.Output;
                sqlDANotice.SelectCommand.Parameters.Add(parameterTotalCount);


                // Execute the command and close the connection
                sqlDANotice.Fill(dsNotice);
                sqlDANotice.SelectCommand.Connection.Dispose();

                // Return the dataset result

                totalCount = Convert.ToInt32(parameterTotalCount.Value);

                return dsNotice;
            }
            finally
            {
                if (sqlDANotice.SelectCommand.Connection != null)
                {
                    sqlDANotice.SelectCommand.Connection.Dispose();
                }

            }
        }
    }
}
