using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for PeriodStatisticsDB
/// </summary>
namespace Stalberg.TMS
{

    public partial class PeriodStatisticsDB
    {
        string mConstr = "";

        public PeriodStatisticsDB(string vConstr)
        {
	        mConstr = vConstr;
        }

        public DataSet GetPeriodStatsDataDS(String autIntNo, string sYear, string sMonth)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            // Create Instance of Connection and Command Object
            da.SelectCommand = new SqlCommand();
            da.SelectCommand.Connection = new SqlConnection(mConstr);
            da.SelectCommand.CommandText = "GetPeriodStats";
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int).Value = int.Parse(autIntNo);
            da.SelectCommand.Parameters.Add("@InYear", SqlDbType.Int).Value = int.Parse(sYear);
            da.SelectCommand.Parameters.Add("@InMonth", SqlDbType.Int).Value = int.Parse(sMonth);

            // Execute the command and close the connection
            da.Fill(ds);
            da.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return ds;		
        }

    }
}