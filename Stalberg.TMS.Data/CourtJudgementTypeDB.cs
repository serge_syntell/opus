using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    /// <summary>
    /// Summary description for CourtJudgementTypeDB
    /// </summary>
    public class CourtJudgementTypeDB
    {
        // Fields
        private SqlConnection con;

        /// <summary>
        /// Initializes a new instance of the <see cref="CourtJudgementTypeDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public CourtJudgementTypeDB(string connectionString)
        {
            this.con = new SqlConnection(connectionString);
        }

        /// <summary>
        /// Gets the judgement list for a court.
        /// </summary>
        /// <param name="crtIntNo">The Court int no.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        // 2013-07-24 add parameter lastUser by Henry
        public DataSet GetListForCourt(int crtIntNo, string lastUser)
        {
            SqlCommand com = new SqlCommand("CourtJudgementTypeList", this.con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@CrtIntNo", SqlDbType.Int, 4).Value = crtIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        /// <summary>
        /// Saves the specified Court Judgement Type.
        /// </summary>
        /// <param name="cjt">The CJT.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        public int Save(JudgementType cjt, string lastUser, ref string errMessage)
        {
            SqlCommand com = new SqlCommand("CourtJudgementTypeSave", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@CJTIntNo", SqlDbType.Int, 4).Value = cjt.ID;
            com.Parameters.Add("@CrtIntNo", SqlDbType.Int, 4).Value = cjt.CrtIntNo;
            com.Parameters.Add("@Code", SqlDbType.TinyInt, 1).Value = cjt.Code;
            com.Parameters.Add("@Description", SqlDbType.VarChar, 7000).Value = cjt.Description;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                this.con.Open();
                cjt.ID = Convert.ToInt32(com.ExecuteScalar());

                return cjt.ID;
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return 0;
            }
            finally
            {
                this.con.Close();
            }
        }

        /// <summary>
        /// Deletes the type of the judgement.
        /// </summary>
        /// <param name="cjtIntNo">The CJT int no.</param>
        public int DeleteJudgementType(int cjtIntNo, ref string errMessage)
        {
            SqlCommand com = new SqlCommand("CourtJudgementTypeDelete", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@CJTIntNo", SqlDbType.Int, 4).Value = cjtIntNo;
            com.Parameters["@CJTIntNo"].Direction = ParameterDirection.InputOutput;

            try
            {
                this.con.Open();
                com.ExecuteNonQuery();
                return int.Parse(com.Parameters["@CJTIntNo"].Value.ToString());
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return 0;
            }
            finally
            {
                this.con.Close();
            }
        }
    
    }

    /// <summary>
    /// Represents a Court Judgement Type
    /// </summary>
    [Serializable]
    public class JudgementType
    {
        // Fields
        private byte code;
        private string description;
        private int crtIntNo;
        private int cjtIntNo;

        /// <summary>
        /// Initializes a new instance of the <see cref="CourtJudgementType"/> class.
        /// </summary>
        public JudgementType()
        {
        }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        public byte Code
        {
            get { return this.code; }
            set { this.code = value; }
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        /// <summary>
        /// Gets or sets the Court int no.
        /// </summary>
        /// <value>The CRT int no.</value>
        public int CrtIntNo
        {
            get { return this.crtIntNo; }
            set { this.crtIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        /// <value>The ID.</value>
        public int ID
        {
            get { return this.cjtIntNo; }
            set { this.cjtIntNo = value; }
        }

    }
}