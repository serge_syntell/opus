using System;
using Stalberg.TMS;

namespace Stalberg.TMS_TPExInt.Objects
{
    /// <summary>
    /// Represents an EasyPay transaction
    /// </summary>
    public class EasyPayTransaction : IComparable<EasyPayTransaction>
    {
        // Fields
        private int notIntNo;
        private string epNo = string.Empty;
        private DateTime expiryDate;
        private char epAction;
        private EasyPayAuthority authority;
        private decimal amount = 0; //20080509 used for rand amounts
        private Int32 cents = 0; //20080509 used for cents as in Easypay traffic data file
        private string regNo = string.Empty;
        private string error = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="EasyPayTransaction"/> class.
        /// </summary>
        public EasyPayTransaction(EasyPayAuthority authority)
        {
            this.authority = authority;
        }

        /// <summary>
        /// Gets or sets the error message sent in the HoldBack file.
        /// </summary>
        /// <value>The error.</value>
        public string Error
        {
            get { return this.error; }
            set { this.error = value; }
        }

        /// <summary>
        /// Gets or sets the amount of the EasyPay transaction.
        /// </summary>
        /// <value>The amount.</value>
        public decimal Amount
        {
            get { return this.amount; }
            set { this.amount = value; }
        }
        /// <summary>
        /// Gets or sets the cents of the EasyPay transaction.
        /// </summary>
        /// <value>The amount.</value>
        public Int32 Cents
        {
            get { return this.cents; }
            set { this.cents = value; }
        }

        /// <summary>
        /// Gets or sets the database notice ID.
        /// </summary>
        /// <value>The notice ID.</value>
        public int NoticeID
        {
            get { return this.notIntNo; }
            set { this.notIntNo = value; }
        }

        /// <summary>
        /// Gets the authority that this EasyPay transaction belongs to.
        /// </summary>
        /// <value>The authority.</value>
        public EasyPayAuthority Authority
        {
            get { return this.authority; }
        }

        /// <summary>
        /// Gets or sets the easy pay action.
        /// </summary>
        /// <value>The easy pay action.</value>
        public char EasyPayAction
        {
            get { return this.epAction; }
            set { this.epAction = value; }
        }

        /// <summary>
        /// Gets or sets the easy pay number.
        /// </summary>
        /// <value>The easy pay number.</value>
        public string EasyPayNumber
        {
            get { return this.epNo; }
            set { this.epNo = value; }
        }

        /// <summary>
        /// Gets or sets the expiry date.
        /// </summary>
        /// <value>The expiry date.</value>
        public DateTime ExpiryDate
        {
            get { return this.expiryDate; }
            set { this.expiryDate = value; }
        }

        /// <summary>
        /// Gets or sets the vehicle registration number.
        /// </summary>
        /// <value>The reg no.</value>
        public string RegNo
        {
            get { return this.regNo; }
            set { this.regNo = value; }
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. 
        /// The return value has the following meanings: Value Meaning 
        ///    Less than zero - This object is less than the other parameter.
        ///    Zero - This object is equal to other. 
        ///    Greater than zero - This object is greater than other.
        /// </returns>
        public int CompareTo(EasyPayTransaction other)
        {
            int response = this.EasyPayNumber.CompareTo(other.EasyPayNumber);
            if (response != 0)
                return response;

            response = this.notIntNo.CompareTo(other.NoticeID);
            if (response != 0)
                return response;

            switch (this.epAction)
            {
                case 'I':
                    switch (other.epAction)
                    {
                        case 'I':
                            return 0;
                        case 'U':
                        case 'D':
                            return -1;
                    }
                    break;

                case 'U':
                    switch (other.epAction)
                    {
                        case 'I':
                            return 1;
                        case 'U':
                            return 0;
                        case 'D':
                            return -1;
                    }
                    break;

                case 'D':
                    switch (other.epAction)
                    {
                        case 'I':
                        case 'U':
                            return 1;
                        case 'D':
                            return 0;
                    }
                    break;
            }

            return 0;
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"></see> is equal to the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <param name="obj">The <see cref="T:System.Object"></see> to compare with the current <see cref="T:System.Object"></see>.</param>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"></see> is equal to the current <see cref="T:System.Object"></see>; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            EasyPayTransaction tran = obj as EasyPayTransaction;
            return (this.EasyPayNumber == tran.EasyPayNumber && this.notIntNo == tran.NoticeID);
        }

        /// <summary>
        /// Serves as a hash function for a particular type. <see cref="M:System.Object.GetHashCode"></see> is suitable for use in hashing algorithms and data structures like a hash table.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            return string.Format("{0} - {1}", this.EasyPayNumber, this.EasyPayAction);
        }

    }
}
