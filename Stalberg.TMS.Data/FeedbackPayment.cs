using System;
using Stalberg.TMS;

namespace Stalberg.TMS_TPExInt.Objects
{
    /// <summary>
    /// Represents the contents for a feedback payment item.
    /// </summary>
    public class FeedbackPayment : ICloneable
    {
        // Fields
        private string ticketNo = string.Empty;
        private string partnerId = string.Empty;
        private CashType source = CashType.None;
        private DateTime paymentDate;
        private decimal amount = 0.0M;
        private DateTime msgDateTime;
        private byte paymentAction;
        private string type = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="FeedbackPayment"/> class.
        /// </summary>
        public FeedbackPayment()
        {
        }

        /// <summary>
        /// Gets or sets the payment action.
        /// </summary>
        /// <value>The payment action.</value>
        public byte PaymentAction
        {
            get { return this.paymentAction; }
            set { this.paymentAction = value; }
        }

        public string Type
        {
            get { return this.type; }
            set { this.type = value; }
        }

        
        /// <summary>
        /// Gets or sets the message date.
        /// </summary>
        /// <value>The message date.</value>
        public DateTime MessageDate
        {
            get { return this.msgDateTime; }
            set { this.msgDateTime = value; }
        }

        /// <summary>
        /// Gets or sets the amount of the payment.
        /// </summary>
        /// <value>The amount.</value>
        public decimal Amount
        {
            get { return this.amount; }
            set { this.amount = value; }
        }

        /// <summary>
        /// Gets or sets the payment date.
        /// </summary>
        /// <value>The payment date.</value>
        public DateTime PaymentDate
        {
            get { return this.paymentDate; }
            set { this.paymentDate = value; }
        }

        /// <summary>
        /// Sets the source as a CashType for payment processing into TMS.
        /// </summary>
        /// <param name="source">The source code from Thabo.</param>
        public void SetSource(string source)
        {
            if (source.Equals("Payfine", StringComparison.InvariantCultureIgnoreCase))
                this.source = CashType.PayFine;
            else
                this.source = CashType.EasyPay;
        }

        /// <summary>
        /// Gets the source of the payment for receipt processing.
        /// </summary>
        /// <value>The source.</value>
        public CashType Source
        {
            get { return this.source; }
        }

        /// <summary>
        /// Gets the payment action for processing the feedback.
        /// </summary>
        /// <value>The action.</value>
        public ActionPayment Action
        {
            get { return (ActionPayment)this.paymentAction; }
        }

        /// <summary>
        /// Gets or sets the partner ID.
        /// </summary>
        /// <value>The partner ID.</value>
        public string PartnerID
        {
            get { return this.partnerId; }
            set { this.partnerId = value; }
        }

        /// <summary>
        /// Gets or sets the ticket no.
        /// </summary>
        /// <value>The ticket no.</value>
        public string TicketNo
        {
            get { return this.ticketNo; }
            set { this.ticketNo = value; }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            return this.ticketNo;
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        public object Clone()
        {
            FeedbackPayment p = new FeedbackPayment();
            p.amount = this.amount;
            p.msgDateTime = this.msgDateTime;
            p.partnerId = this.partnerId;
            p.paymentAction = this.paymentAction;
            p.paymentDate = this.paymentDate;
            p.source = this.source;
            p.ticketNo = this.ticketNo;
            p.type = this.type;

            return p;
        }

    }

    /// <summary>
    /// Lists the actions that can be taken when a payment is processed into a Local Authority
    /// </summary>
    public enum ActionPayment
    {
        /// <summary>
        /// The payment can be processed normally
        /// </summary>
        Normal = 0,
        /// <summary>
        /// The payment is rejected and needs to be entered into the Syspense Account
        /// </summary>
        Rejected = 1,
        /// <summary>
        /// The payment requires a Representation to make the revised fine amount equal the amount paid
        /// </summary>
        Representation = 2,
        /// <summary>
        /// The payment was more than the fine amount and needs to be split between a payment for the 
        /// charge amount and the rest being placed in the Suspense Account.
        /// </summary>
        Hybrid = 4
    }

}
