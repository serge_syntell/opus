using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    /// <summary>
    /// Summary description for SumChargeDB
    /// </summary>
    public class SumChargeDB
    {
        // Fields
        private SqlConnection con;

        /// <summary>
        /// Initializes a new instance of the <see cref="SumChargeDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public SumChargeDB(string connectionString)
        {
            this.con = new SqlConnection(connectionString);
        }

        /// <summary>
        /// Records the judgement.
        /// </summary>
        /// <param name="schIntNo">The SCH int no.</param>
        /// <param name="verdict">The verdict.</param>
        /// <param name="sentence">The sentence.</param>
        /// <param name="sentenceAmount">The sentence amount.</param>
        /// <param name="contemptAmount">The contempt amount.</param>
        /// <param name="otherAmount">The other amount.</param>
        /// <param name="cjtIntNo">The CJT int no.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns><c>true</c> if the update is successful</returns>
        // mrs 20080214 changed internals of proc returns int, negative is a problem
        //public int RecordJudgement(string verdict, string sentence, decimal sentenceAmount, decimal contemptAmount, decimal otherAmount, int cjtIntNo, string lastUser, int schIntNo, DateTime newCourtDate, int YesDeferred, ref string errMessage)
        // Oscar 20101126 - changed the parameters
        public int RecordJudgement(string verdict, string sentence, decimal sentenceAmount, decimal contemptAmount, decimal otherAmount, int cjtIntNo, string lastUser, int schIntNo, DateTime newCourtDate, bool YesDeferred, ref string errMessage, int sumIntNo, bool forSummons)
        {
            SqlCommand com = new SqlCommand("SumChargeRecordJudgement", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@Verdict", SqlDbType.VarChar, 7000).Value = verdict;
            com.Parameters.Add("@Sentence", SqlDbType.VarChar, 7000).Value = sentence;
            com.Parameters.Add("@SentenceAmount", SqlDbType.Money, 8).Value = sentenceAmount;
            com.Parameters.Add("@ContemptAmount", SqlDbType.Money, 8).Value = contemptAmount;
            com.Parameters.Add("@OtherAmount", SqlDbType.Money, 8).Value = otherAmount;
            com.Parameters.Add("@CJTIntNo", SqlDbType.Int, 4).Value = cjtIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@SChIntNo", SqlDbType.Int, 4).Value = schIntNo;
            com.Parameters.Add("@newCourtDate", SqlDbType.SmallDateTime).Value = newCourtDate;
            com.Parameters.Add("@YesDeferred", SqlDbType.Bit).Value = YesDeferred;

            // Oscar 20101126 - added parameters
            com.Parameters.Add("@sumIntNo", SqlDbType.Int).Value = sumIntNo;
            com.Parameters.Add("@forSummons", SqlDbType.Bit).Value = forSummons;

            com.Parameters["@SChIntNo"].Direction = ParameterDirection.InputOutput;
            //com.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

            try
            {
                this.con.Open();
                int records = Convert.ToInt32(com.ExecuteScalar());
                //int records = (int)com.Parameters["@ReturnValue"].Value;
                return records;
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return -1;
            }
            finally
            {
                this.con.Close();
            }
        }

        /// <summary>
        /// Records the judgement.
        /// </summary>
        /// <param name="schIntNo">The SCH int no.</param>
        /// <param name="verdict">The verdict.</param>
        /// <param name="sentence">The sentence.</param>
        /// <param name="sentenceAmount">The sentence amount.</param>
        /// <param name="contemptAmount">The contempt amount.</param>
        /// <param name="otherAmount">The other amount.</param>
        /// <param name="cjtIntNo">The CJT int no.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns><c>true</c> if the update is successful</returns>
        // mrs 20080214 changed internals of proc returns int, negative is a problem
        //public int RecordJudgement(string verdict, string sentence, decimal sentenceAmount, decimal contemptAmount, decimal otherAmount, int cjtIntNo, string lastUser, int schIntNo, DateTime newCourtDate, int YesDeferred, ref string errMessage)
        // Oscar 20101126 - changed the parameters
        // Jake 2013-10-14 added new function from RecordJudgement
        public int RecordJudgement(string verdict, string sentence, decimal sentenceAmount,
            decimal contemptAmount, decimal otherAmount, int cjtIntNo, string lastUser,
            int schIntNo, ref string errMessage, int sumIntNo, string woaType,
            string woaNumber = "", string issueWOANotice = "N", string displayRepresentative = "N", string woaPrintFileName = "", int ctnlaIntNo = 0)
        {
            SqlCommand com = new SqlCommand("SumChargeRecordJudgement_WOA", this.con);
            com.CommandType = CommandType.StoredProcedure;
            com.CommandTimeout = 0;    //dls 2013-11-02- extend timeout

            com.Parameters.Add("@Verdict", SqlDbType.VarChar, 7000).Value = verdict;
            com.Parameters.Add("@Sentence", SqlDbType.VarChar, 7000).Value = sentence;
            com.Parameters.Add("@SentenceAmount", SqlDbType.Money, 8).Value = sentenceAmount;
            com.Parameters.Add("@ContemptAmount", SqlDbType.Money, 8).Value = contemptAmount;
            com.Parameters.Add("@OtherAmount", SqlDbType.Money, 8).Value = otherAmount;
            com.Parameters.Add("@CJTIntNo", SqlDbType.Int, 4).Value = cjtIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@SChIntNo", SqlDbType.Int, 4).Value = schIntNo;
            //com.Parameters.Add("@newCourtDate", SqlDbType.SmallDateTime).Value = newCourtDate;
            //com.Parameters.Add("@YesDeferred", SqlDbType.Bit).Value = YesDeferred;

            // Oscar 20101126 - added parameters
            com.Parameters.Add("@sumIntNo", SqlDbType.Int).Value = sumIntNo;
            //com.Parameters.Add("@forSummons", SqlDbType.Bit).Value = forSummons;

            // Jake 2013-10-15 added new parameters
            com.Parameters.Add("@WOAType", SqlDbType.Char).Value = woaType;
            com.Parameters.Add("@WOANumber", SqlDbType.NVarChar, 20).Value = woaNumber;
            com.Parameters.Add("@IssueWOANotice", SqlDbType.Char).Value = issueWOANotice;
            com.Parameters.Add("@DisplayRepresentative", SqlDbType.Char).Value = displayRepresentative;
            com.Parameters.Add("@WOAPrintFileName", SqlDbType.NVarChar, 100).Value = woaPrintFileName;
            com.Parameters.Add("@CTNLAIntNo", SqlDbType.Int).Value = ctnlaIntNo;

            com.Parameters["@SChIntNo"].Direction = ParameterDirection.InputOutput;
            //com.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

            try
            {
                this.con.Open();
                int records = Convert.ToInt32(com.ExecuteScalar());
                //int records = (int)com.Parameters["@ReturnValue"].Value;
                return records;
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return -1;
            }
            finally
            {
                this.con.Close();
            }
        }

        // Jake 2013-11-04 added override function
        public int RecordJudgement(string verdict, string sentence, decimal sentenceAmount,
            decimal contemptAmount, decimal otherAmount, int cjtIntNo, string lastUser,
            int schIntNo, DateTime newCourtDate,
            bool YesDeferred, int sumIntNo, bool forSummons, string woaType,
            string woaNumber, string issueWOANotice, string displayRepresentative, string woaPrintFileName, int ctnlaIntNo, ref string errMessage)
        {
            if (woaType == "B" || woaType == "D")
            {
                return RecordJudgement(verdict, sentence, sentenceAmount, contemptAmount, otherAmount, cjtIntNo, lastUser, schIntNo,
                    ref errMessage, sumIntNo, woaType, woaNumber, issueWOANotice, displayRepresentative, woaPrintFileName, ctnlaIntNo);
            }
            else
            {
                return RecordJudgement(verdict, sentence, sentenceAmount, contemptAmount, otherAmount, cjtIntNo, lastUser, schIntNo, newCourtDate,
                    YesDeferred, ref errMessage, sumIntNo, forSummons);
            }
        }
        /// <summary>
        /// Records the cash receipt.
        /// </summary>
        /// <param name="sentenceAmount">The sentence amount.</param>
        /// <param name="contemptAmount">The contempt amount.</param>
        /// <param name="otherAmount">The other amount.</param>
        /// <param name="cjtIntNo">The CJT int no.</param>
        /// <param name="lastUser">The last user.</param>
        /// <param name="receiptNumber">The receipt number.</param>
        /// <param name="schIntNo">The SCH int no.</param>
        /// <param name="rptDate">The receipt date</param>
        /// <param name="address1">The address1.</param>
        /// <param name="address2">The address2.</param>
        /// <param name="address3">The address3.</param>
        /// <param name="address4">The address4.</param>
        /// <param name="address5">The address5.</param>
        /// <param name="receivedFrom">The received from.</param>
        /// <param name="poCode">The po code.</param>
        /// <param name="Phone">The phone.</param>
        /// <returns>
        /// Cash receipt if success, otherwise, return the failed info
        /// </returns>
        public int RecordCashReceipt(decimal sentenceAmount, decimal contemptAmount, decimal otherAmount, int cjtIntNo, string lastUser, //string receiptNumber,
            int schIntNo, string rptDate, string address1, string address2, string address3, string address4, string address5, string receivedFrom, string poCode, string Phone,
            bool isFromCancelWarrant, ref string errMessage)
        {
            // Process the court receipt
            SqlCommand com = new SqlCommand("PaidAtCourtJudgement", this.con);
            com.CommandType = CommandType.StoredProcedure;

            //com.Parameters.Add("@ReceiptNo", SqlDbType.VarChar, 10).Value = receiptNumber;
            com.Parameters.Add("@SentenceAmount", SqlDbType.Money, 8).Value = sentenceAmount;
            com.Parameters.Add("@ContemptAmount", SqlDbType.Money, 8).Value = contemptAmount;
            com.Parameters.Add("@OtherAmount", SqlDbType.Money, 8).Value = otherAmount;
            com.Parameters.Add("@CJTIntNo", SqlDbType.Int, 4).Value = cjtIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@SChIntNo", SqlDbType.Int, 4).Value = schIntNo;
            com.Parameters.Add("@rptDate", SqlDbType.SmallDateTime).Value = rptDate;
            com.Parameters.Add("@Address1", SqlDbType.VarChar, 50).Value = address1;
            com.Parameters.Add("@Address2", SqlDbType.VarChar, 50).Value = address2;
            com.Parameters.Add("@Address3", SqlDbType.VarChar, 50).Value = address3;
            com.Parameters.Add("@Address4", SqlDbType.VarChar, 50).Value = address4;
            com.Parameters.Add("@Address5", SqlDbType.VarChar, 50).Value = address5;
            com.Parameters.Add("@ReceivedFrom", SqlDbType.VarChar, 50).Value = receivedFrom;
            com.Parameters.Add("@PoCode", SqlDbType.VarChar, 50).Value = poCode;
            com.Parameters.Add("@Phone", SqlDbType.VarChar, 50).Value = Phone;
            com.Parameters.Add("@FromCancelWarrant", SqlDbType.Bit).Value = isFromCancelWarrant;
            com.Parameters["@SChIntNo"].Direction = ParameterDirection.InputOutput;

            try
            {
                this.con.Open();

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(com);
                da.Fill(ds);
                da.Dispose();

                //dls 100105 - there are multiple recordsets returned here because of the nested SP's. Get the last one for the valid ReturnID
                return Convert.ToInt32(ds.Tables[ds.Tables.Count - 1].Rows[0][0]);
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return -1;
            }
            finally
            {
                this.con.Close();
            }
        }

        //public DataSet GetDeferredPayments(int schIntNo)
        /// /// <summary>
        /// Gets the Deferred Payments
        /// </summary>
        /// <param name="schIntNo">SumCharge Int No</param>
        public DataSet GetDeferredPayments(int sumIntNo)
        {
            SqlCommand com = new SqlCommand("DeferredPaymentsForSumCharge", this.con);
            com.CommandType = CommandType.StoredProcedure;

            //com.Parameters.Add("@SChIntNo", SqlDbType.Int, 4).Value = schIntNo;
            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;   // Oscar 20101210 changed

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        //public int AddDeferredPayment(int schIntNo, string paymentDate, string paymentAmount)
        /// /// <summary>
        /// Add a Deferred Payment
        /// </summary>
        /// <param name="schIntNo">SumCharge Int No</param>
        /// <param name="paymentDate">Payment Date</param>
        /// <param name="paymentAmount">Payment Amount</param>
        /// <param name="SCDefPayIntNo">SCDefPayIntNo</param>
        public int AddDeferredPayment(int sumIntNo, string paymentDate, string paymentAmount, string schIntNoList, string amountList)
        {
            SqlCommand com = new SqlCommand("DeferredPaymentsForSumChargeAdd", this.con);
            com.CommandType = CommandType.StoredProcedure;

            //com.Parameters.Add("@SChIntNo", SqlDbType.Int, 4).Value = schIntNo;
            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;    // Oscar 20101210 changed
            com.Parameters.Add("@PaymentDate", SqlDbType.SmallDateTime).Value = DateTime.Parse(paymentDate);
            com.Parameters.Add("@PaymentAmount", SqlDbType.Money, 8).Value = decimal.Parse(paymentAmount);
            com.Parameters.Add("@SCDefPayIntNo", SqlDbType.Int, 4).Value = 0;
            // Oscar 20101215 - added
            com.Parameters.Add("@SChIntNoList", SqlDbType.VarChar).Value = schIntNoList;
            com.Parameters.Add("@AmountList", SqlDbType.VarChar).Value = amountList;
            com.Parameters["@SCDefPayIntNo"].Direction = ParameterDirection.InputOutput;

            try
            {
                this.con.Open();
                int records = com.ExecuteNonQuery();

                return records;
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return -1;
            }
            finally
            {
                this.con.Close();
            }
        }

        //public int UpdateDeferredPayment(int scDefPayIntNo, int schIntNo, string paymentDate, string paymentAmount)
        /// /// <summary>
        /// Update a Deferred Payment
        /// </summary>
        /// <param name="schIntNo">SumCharge Int No</param>
        /// <param name="schIntNo">Payment Date</param>
        /// <param name="schIntNo">Payment Amount</param>
        /// 
        public int UpdateDeferredPayment(int sumIntNo, string oldPaymentDate, string newPaymentDate, string paymentAmount, string schIntNoList, string amountList)
        {
            SqlCommand com = new SqlCommand("DeferredPaymentsForSumChargeUpdate", this.con);
            com.CommandType = CommandType.StoredProcedure;

            //com.Parameters.Add("@SCDefPayIntNo", SqlDbType.Int, 4).Value = scDefPayIntNo;
            //com.Parameters["@SCDefPayIntNo"].Direction = ParameterDirection.InputOutput;
            //com.Parameters.Add("@SChIntNo", SqlDbType.Int, 4).Value = schIntNo;
            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;    // Oscar 20101210 changed
            //com.Parameters.Add("@PaymentDate", SqlDbType.SmallDateTime).Value = DateTime.Parse(paymentDate);
            // Oscar 20101210 changed
            com.Parameters.Add("@OldPaymentDate", SqlDbType.SmallDateTime).Value = DateTime.Parse(oldPaymentDate);
            com.Parameters.Add("@NewPaymentDate", SqlDbType.SmallDateTime).Value = DateTime.Parse(newPaymentDate);
            com.Parameters.Add("@PaymentAmount", SqlDbType.Money, 8).Value = decimal.Parse(paymentAmount);
            com.Parameters.Add("@SCDefPayIntNo", SqlDbType.Int, 4).Value = 0;
            // Oscar 20101215 - added
            com.Parameters.Add("@SChIntNoList", SqlDbType.VarChar).Value = schIntNoList;
            com.Parameters.Add("@AmountList", SqlDbType.VarChar).Value = amountList;
            com.Parameters["@SCDefPayIntNo"].Direction = ParameterDirection.InputOutput;

            try
            {
                this.con.Open();
                int records = com.ExecuteNonQuery();

                return records;
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return -1;
            }
            finally
            {
                this.con.Close();
            }
        }

        //public int DeleteDeferredPayment(int scDefPayIntNo)
        /// /// <summary>
        /// Delete a Deferred Payment
        /// </summary>
        /// <param name="schIntNo">SCDefPayIntNo</param>
        public int DeleteDeferredPayment(int sumIntNo, string paymentDate)
        {
            SqlCommand com = new SqlCommand("DeferredPaymentsForSumChargeDelete", this.con);
            com.CommandType = CommandType.StoredProcedure;

            //com.Parameters.Add("@SCDefPayIntNo", SqlDbType.Int, 4).Value = scDefPayIntNo;
            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;   // Oscar 20101210 changed
            com.Parameters.Add("@PaymentDate", SqlDbType.SmallDateTime).Value = DateTime.Parse(paymentDate);  // Oscar 20101210 added

            try
            {
                this.con.Open();
                int records = com.ExecuteNonQuery();

                return records;
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return -1;
            }
            finally
            {
                this.con.Close();
            }
        }

        /// <summary>
        /// return the summons address detail
        /// </summary>
        public DataSet GetSummonsddressDetails(int sumIntNo, string person)
        {
            // Create the data access objects
            SqlConnection con = new SqlConnection(this.con.ConnectionString);
            SqlCommand com = new SqlCommand("SummonsAddressDetails", con);
            com.CommandType = CommandType.StoredProcedure;

            // Set the parameter values
            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;
            com.Parameters.Add("@Person", SqlDbType.VarChar, 20).Value = person;

            // Fill a data set
            SqlDataAdapter adapter = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            adapter.Dispose();

            // Return the data set
            return ds;
        }

    }
}