using System;

namespace Stalberg.TMS
{
    [Serializable]
    public class SumServStatusOptions
    {
        //Fields
        private int _AutIntNo = -1;
        private DateTime _CourtDate = new DateTime(1900, 01, 01);
        private int _CrtIntNo = -1;
        private int _CrtRIntNo = -1;
        private bool _NotReturnedFromCofCourt;
        private bool _NotReturnedFromSServer;
        private string _OptionSelected = string.Empty;
        private string _ServedUnserved = string.Empty;
        private string _SummonsNo = string.Empty;
        private string _NoticeNo = string.Empty;
        private bool _WithDrawn;

        private int _OrderBy = 0;

        public string OptionSelected
        {
            get { return _OptionSelected; }
            set { _OptionSelected = value; }
        }

        public int AutIntNo
        {
            get { return _AutIntNo; }
            set { _AutIntNo = value; }
        }

        public int OrderBy
        {
            get { return _OrderBy; }
            set { _OrderBy = value; }
        }

        public int CrtIntNo
        {
            get { return _CrtIntNo; }
            set { _CrtIntNo = value; }
        }

        public int CrtRIntNo
        {
            get { return _CrtRIntNo; }
            set { _CrtRIntNo = value; }
        }

        public DateTime CourtDate
        {
            get { return _CourtDate; }
            set { _CourtDate = value; }
        }

        public string ServedUnserved
        {
            get { return _ServedUnserved; }
            set { _ServedUnserved = value; }
        }

        public bool WithDrawn
        {
            get { return _WithDrawn; }
            set { _WithDrawn = value; }
        }

        public bool NotReturnedFromCofCourt
        {
            get { return _NotReturnedFromCofCourt; }
            set { _NotReturnedFromCofCourt = value; }
        }

        public bool NotReturnedFromSServer
        {
            get { return _NotReturnedFromSServer; }
            set { _NotReturnedFromSServer = value; }
        }

        public string SummonsNo
        {
            get { return _SummonsNo; }
            set { _SummonsNo = value; }
        }

        public string NoticeNo
        {
            get { return _NoticeNo; }
            set { _NoticeNo = value; }
        }

        // Oscar 2013-05-09 added
        public int DateOption { get; set; }
        public DateTime CrtDateStart { get; set; }
        public DateTime CrtDateEnd { get; set; }
    }
}