using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


/* SD : 21.07.2008
 * Page : S56Bookdb.cs
 * Functionality : Datastructure to maintain the S56 Book information
 */

namespace Stalberg.TMS
{
    // SD: 21.07.2008
	//*******************************************************
	//
	// S56 Book Class
	//
	// A simple data class that encapsulates details about a particular loc 
	//
	//*******************************************************

	public partial class S56BookDetails
	{
        public Int32 S56BIntNo;
        public Int32 S56BTIntNo;
        public Int32 AutIntNo;
        public string  S56BNo;
        public Int32 S56BStartNo;
        public Int32 S56BEndNo;
        public Int32 S56BNoOfForms;
	}

    //*******************************************************
	//
    // S56 BookDB Class
	//
	//*******************************************************

	public partial class S56BookDB
    {
		string mConstr = "";

        public S56BookDB(string vConstr)
		{
			mConstr = vConstr;
		}

		//*******************************************************
        // SD : 21.07.2008 & 22.07.2008 
		// Functionality to add S56 Book details to database
        //*******************************************************

        public int AddS56Book(int autIntNo, int s56BTypeIntNo,  string  s56BNo, string s56BStartNo, 
            string s56BendNo, int s56BNoOfForms, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("S56BookAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterS56BTIntNo = new SqlParameter("@S56BTIntNo", SqlDbType.Int, 4);
            parameterS56BTIntNo.Value = s56BTypeIntNo;
            myCommand.Parameters.Add(parameterS56BTIntNo);
           

            SqlParameter parameterS56BNo = new SqlParameter("@S56BNo", SqlDbType.VarChar, 30);
            parameterS56BNo.Value = s56BNo;
            myCommand.Parameters.Add(parameterS56BNo);

            SqlParameter parameterS56BStartNo = new SqlParameter("@S56BStartNo", SqlDbType.Int, 4);
            parameterS56BStartNo.Value = s56BStartNo;
            myCommand.Parameters.Add(parameterS56BStartNo);

            SqlParameter parameters56BendNo = new SqlParameter("@S56BEndNo", SqlDbType.Int, 4);
            parameters56BendNo.Value = s56BendNo;
            myCommand.Parameters.Add(parameters56BendNo);

            SqlParameter parameterS56BNoOfForms = new SqlParameter("@S56BNoOfForms", SqlDbType.Int, 4);
            parameterS56BNoOfForms.Value = s56BNoOfForms;
            myCommand.Parameters.Add(parameterS56BNoOfForms);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterS56BIntNo = new SqlParameter("@S56BIntNo", SqlDbType.Int, 4);
            parameterS56BIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterS56BIntNo);

           try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the BookID using Output Param from SPROC
                int s56BIntNo = Convert.ToInt32(parameterS56BIntNo.Value);

                return s56BIntNo;
          }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }


        // SD : 22.07.2008 
        // Functionality : to fetch the S56Book information from database
        //                 based on the input parameter supplied 

        public S56BookDetails GetS56BookDetails(int s56BIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("S56BookDetail", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
            SqlParameter parameters56BIntNo = new SqlParameter("@S56BIntNo", SqlDbType.Int, 4);
            parameters56BIntNo.Value = s56BIntNo;
            myCommand.Parameters.Add(parameters56BIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            S56BookDetails myBookDetails = new S56BookDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
                myBookDetails.S56BNo = result["S56BNo"].ToString();
                myBookDetails.S56BStartNo = Convert.ToInt32(result["S56BStartNo"].ToString());
                myBookDetails.S56BEndNo = Convert.ToInt32(result["S56BEndNo"].ToString());
                myBookDetails.S56BNoOfForms = Convert.ToInt32(result["S56BNoOfForms"]);
                myBookDetails.S56BTIntNo = Convert.ToInt32(result["S56BTIntNo"]);

			}
			result.Close();
            return myBookDetails;
		}

		
        /// <summary>
        ///SD: 21.07.2008 & 22.07.2008 
        /// </summary>
        /// <param name="autIntNo"></param>
        /// <returns>Returns the dataset containing S56Books Fields</returns>
		public DataSet GetS56BookListDS (int autIntNo)
		{
			SqlDataAdapter sqlDAS56Book = new SqlDataAdapter();
            DataSet dsS56Book = new DataSet();

			// Create Instance of Connection and Command Object
            sqlDAS56Book.SelectCommand = new SqlCommand();
            sqlDAS56Book.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAS56Book.SelectCommand.CommandText = "S56BookList";

			// Mark the Command as a SPROC
            sqlDAS56Book.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo; 
            sqlDAS56Book.SelectCommand.Parameters.Add(parameterAutIntNo);

			// Execute the command and close the connection
            sqlDAS56Book.Fill(dsS56Book);
            sqlDAS56Book.SelectCommand.Connection.Dispose();

			// Return the dataset result
            return dsS56Book;
		}

        public DataSet GetS56BookListByLADS(int autIntNo)
        {
            SqlDataAdapter sqlDAS56Book = new SqlDataAdapter();
            DataSet dsS56Book = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAS56Book.SelectCommand = new SqlCommand();
            sqlDAS56Book.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAS56Book.SelectCommand.CommandText = "S56BookListByLA";

            // Mark the Command as a SPROC
            sqlDAS56Book.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDAS56Book.SelectCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command and close the connection
            sqlDAS56Book.Fill(dsS56Book);
            sqlDAS56Book.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsS56Book;
        }

        /// <summary>
        /// SD: 21.07.2008 & 22.07.2008
        /// Updates the S56 Database using input & output parameters
        /// </summary>

        //public int UpdateS56Book(int s56BIntNo, int autIntNo, int s56BTIntNo, string s56BNo, string s56BStartNo, string s56BEndNo, int s56BNoOfForms)
        public int UpdateS56Book(int s56BIntNo, int autIntNo, int s56BTIntNo, string s56BNo, int s56BStartNo, 
            int s56BEndNo, int s56BNoOfForms, string lastUser)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("S56BookUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;
			// Add Parameters to SPROC
			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			myCommand.Parameters.Add(parameterAutIntNo);
         
            SqlParameter paramerterS56BTIntNo = new SqlParameter("@S56BTIntNo", SqlDbType.Int, 4);
            paramerterS56BTIntNo.Value = s56BTIntNo;
            myCommand.Parameters.Add(paramerterS56BTIntNo);

            SqlParameter parameterS56BNo = new SqlParameter("@S56BNo", SqlDbType.VarChar, 30);
            parameterS56BNo.Value = s56BNo;
            myCommand.Parameters.Add(parameterS56BNo);

            SqlParameter parameterS56BStartNo = new SqlParameter("@S56BStartNo", SqlDbType.Int, 4);
            parameterS56BStartNo.Value = s56BStartNo;
            myCommand.Parameters.Add(parameterS56BStartNo);

            SqlParameter parameterS56BEndNo = new SqlParameter("@S56BEndNo", SqlDbType.Int, 4);
            parameterS56BEndNo.Value = s56BEndNo;
            myCommand.Parameters.Add(parameterS56BEndNo);

            SqlParameter parameterS56BNoOfForms = new SqlParameter("@S56BNoOfForms", SqlDbType.Int,4);
            parameterS56BNoOfForms.Value = s56BNoOfForms;
            myCommand.Parameters.Add(parameterS56BNoOfForms);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterS56BIntNo = new SqlParameter("@S56BIntNo", SqlDbType.Int);
            parameterS56BIntNo.Direction = ParameterDirection.InputOutput;
            parameterS56BIntNo.Value = s56BIntNo;
            myCommand.Parameters.Add(parameterS56BIntNo);

			try
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

                s56BIntNo = (int)myCommand.Parameters["@S56BIntNo"].Value;

                return s56BIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

        // SD : 21.07.2008 & 22.07.2008 
        // Deletes the S56Book record based on the BookIntNo supplied
		public int  DeleteS56Book (int s56BIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("S56BookDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
            SqlParameter parameterS56IntNo = new SqlParameter("@S56BIntNo", SqlDbType.Int, 4);
            parameterS56IntNo.Value = s56BIntNo;
            parameterS56IntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterS56IntNo);

			try
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
                s56BIntNo = (int)parameterS56IntNo.Value;

                return s56BIntNo;
			}
			catch
			{
				myConnection.Dispose();
                return 0;
			}
		}

	

	}
}

