﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    public class RoadBlockSessions
    {
        public int RBSIntNo;
        public string RBSessionID;
        public DateTime CrtTime;
        public DateTime ClsTime;
        public string IndcFlag;
        public string LocDescr;
        public string LocGps;
        public string LastUser;
        public string Comments;
    }

    public class ClerkOfCourt
    {
        public int CofCIntNo;
        public int CrtIntNo;
        public string CofCName;
        public string CofCSurName;
        public string CofCContact;
    }

    public class RoadBlockDB
    {

        // Fields
        private SqlConnection con;

        public RoadBlockDB(string connString)
        {
            this.con = new SqlConnection(connString);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strCofCIntNo">Example : '1,2,3,4,5'</param>
        /// <param name="roadBlockSession"></param>
        /// <returns></returns>
        public int AddRoadBlockSession(RoadBlockSessions roadBlockSession, int userIntNo, string strCofCIntNo)
        {
            //SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RoadBlockSessionAdd", con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterIndcFlag = new SqlParameter("@IndcFlag", SqlDbType.Char, 1);
            parameterIndcFlag.Value = roadBlockSession.IndcFlag;

            myCommand.Parameters.Add(parameterIndcFlag);

            SqlParameter parameterLocDescr = new SqlParameter("@LocDescr", SqlDbType.VarChar, 200);
            parameterLocDescr.Value = roadBlockSession.LocDescr;
            myCommand.Parameters.Add(parameterLocDescr);

            //Jerry 2015-03-04 change the length to 20
            //SqlParameter parameterLocGps = new SqlParameter("@LocGPSCoOr", SqlDbType.VarChar, 6);
            SqlParameter parameterLocGps = new SqlParameter("@LocGPSCoOr", SqlDbType.VarChar, 20);
            parameterLocGps.Value = roadBlockSession.LocGps;
            myCommand.Parameters.Add(parameterLocGps);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = roadBlockSession.LastUser;
            myCommand.Parameters.Add(parameterLastUser);

            //SqlParameter parameterClsTime = new SqlParameter("@ClsTime", SqlDbType.DateTime);
            //parameterClsTime.Value = roadBlockSession.ClsTime;
            //myCommand .Parameters.Add(parameterClsTime);

            SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int);
            parameterUserIntNo.Value = userIntNo;
            myCommand.Parameters.Add(parameterUserIntNo);

            SqlParameter parameterStrCofCIntNo = new SqlParameter("@StrCofCIntNo", SqlDbType.VarChar, 4000);
            parameterStrCofCIntNo.Value = strCofCIntNo;
            myCommand.Parameters.Add(parameterStrCofCIntNo);

            SqlParameter parameterComments = new SqlParameter("@Comments", SqlDbType.VarChar, 200);
            parameterComments.Value = roadBlockSession.Comments;
            myCommand.Parameters.Add(parameterComments);


            SqlParameter parameterRBSIntNo = new SqlParameter("@RBSIntNo", SqlDbType.Int, 4);
            parameterRBSIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterRBSIntNo);

            try
            {
                con.Open();
                myCommand.ExecuteNonQuery();
                con.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int rbsIntNo = Convert.ToInt32(parameterRBSIntNo.Value);

                return rbsIntNo;
            }
            catch (Exception e)
            {
                con.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int UpdateRoadBolckSession(RoadBlockSessions roadBlockSession, int userIntNo, string strCofCIntNo)
        {
            //SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RoadBlockSessionUpdate", con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterIndcFlag = new SqlParameter("@IndcFlag", SqlDbType.Char, 1);
            parameterIndcFlag.Value = roadBlockSession.IndcFlag;

            myCommand.Parameters.Add(parameterIndcFlag);

            SqlParameter parameterLocDescr = new SqlParameter("@LocDescr", SqlDbType.VarChar, 200);
            parameterLocDescr.Value = roadBlockSession.LocDescr;
            myCommand.Parameters.Add(parameterLocDescr);

            //Jerry 2015-03-04 change length to 20
            //SqlParameter parameterLocGps = new SqlParameter("@LocGPSCoOr", SqlDbType.VarChar, 6);
            SqlParameter parameterLocGps = new SqlParameter("@LocGPSCoOr", SqlDbType.VarChar, 20);
            parameterLocGps.Value = roadBlockSession.LocGps;
            myCommand.Parameters.Add(parameterLocGps);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = roadBlockSession.LastUser;
            myCommand.Parameters.Add(parameterLastUser);

            //SqlParameter parameterClsTime = new SqlParameter("@ClsTime", SqlDbType.DateTime);
            //parameterClsTime.Value = roadBlockSession.ClsTime;
            //myCommand.Parameters.Add(parameterClsTime);

            SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int);
            parameterUserIntNo.Value = userIntNo;
            myCommand.Parameters.Add(parameterUserIntNo);

            SqlParameter parameterStrCofCIntNo = new SqlParameter("@StrCofCIntNo", SqlDbType.VarChar, 4000);
            parameterStrCofCIntNo.Value = strCofCIntNo;
            myCommand.Parameters.Add(parameterStrCofCIntNo);

            SqlParameter parameterComments = new SqlParameter("@Comments", SqlDbType.VarChar, 200);
            parameterComments.Value = roadBlockSession.Comments;
            myCommand.Parameters.Add(parameterComments);


            SqlParameter parameterRBSIntNo = new SqlParameter("@RBSIntNo", SqlDbType.Int, 4);
            parameterRBSIntNo.Direction = ParameterDirection.InputOutput;
            parameterRBSIntNo.Value = roadBlockSession.RBSIntNo;
            myCommand.Parameters.Add(parameterRBSIntNo);

            try
            {
                con.Open();
                myCommand.ExecuteNonQuery();
                con.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int rbsIntNo = Convert.ToInt32(parameterRBSIntNo.Value);

                return rbsIntNo;
            }
            catch (Exception e)
            {
                con.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int UpdateRoadBlockStatus(int rbsIntNo, string flag, string lastUser, DateTime clsTime)
        {
            SqlCommand myCommand = new SqlCommand("RoadBlockSessionFlagUpdate", con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            //myCommand.Parameters.Add(new SqlParameter("@RBSIntNo", SqlDbType.Int, 4)).Value = rbsIntNo;

            myCommand.Parameters.Add(new SqlParameter("@IndcFlag", SqlDbType.Char, 1)).Value = flag;
            myCommand.Parameters.Add(new SqlParameter("@LastUser", SqlDbType.VarChar, 100)).Value = lastUser;
            myCommand.Parameters.Add(new SqlParameter("@ClsTime", SqlDbType.DateTime)).Value = clsTime;

            // myCommand.Parameters.Add(new SqlParameter("@RBSCofCIntNo", SqlDbType.Int, 4)).Direction = ParameterDirection.Output;

            SqlParameter parameterRBSIntNo = new SqlParameter("@RBSIntNo", SqlDbType.Int, 4);
            parameterRBSIntNo.Value = rbsIntNo;
            parameterRBSIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterRBSIntNo);

            try
            {
                con.Open();
                myCommand.ExecuteNonQuery();
                con.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int rbscofCIntNo = Convert.ToInt32(parameterRBSIntNo.Value);

                return rbscofCIntNo;
            }
            catch (Exception e)
            {
                con.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public SqlDataReader GetRoadBlockByUserIntNo(int userIntNo)
        {
            //SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RoadBlockSessionListByUserIntNo", con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            myCommand.Parameters.Add(new SqlParameter("@UserIntNo", SqlDbType.Int, 4)).Value = userIntNo;

            con.Open();
            return myCommand.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public RoadBlockSessions GetRoadBlockSessionByNo(int roadBlockIntNo)
        {
            // Create Instance of Connection and Command Object
            // SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RoadBlockSessionListByRBSNo", con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterRBSIntNo = new SqlParameter("@RBSIntNo", SqlDbType.Int, 4);
            parameterRBSIntNo.Value = roadBlockIntNo;
            myCommand.Parameters.Add(parameterRBSIntNo);
            try
            {
                // Execute the command
                con.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
                RoadBlockSessions roadBlock = new RoadBlockSessions();
                while (result.Read())
                {
                    roadBlock.RBSIntNo = roadBlockIntNo;
                    roadBlock.LocDescr = result["LocDescr"].ToString();
                    roadBlock.LocGps = result["LocGPSCoOrd"].ToString();
                    roadBlock.IndcFlag = result["IndcFlag"].ToString();
                    roadBlock.CrtTime = DateTime.Parse(result["CrtTime"].ToString());
                    //roadBlock.ClsTime  = DateTime.Parse(result["CLsTime"].ToString());
                    roadBlock.LastUser = result["LastUser"].ToString();
                    roadBlock.RBSessionID = result["RBSessionID"].ToString();
                    roadBlock.Comments = result["Comments"].ToString();
                }

                return roadBlock;
            }
            catch (SqlException ex)
            {
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public int AddRoadBlockSessionCofCourt(int rbsIntNo, int cofcIntNo, string lastUser)
        {
            //SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RoadBlockSessionAdd", con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            myCommand.Parameters.Add(new SqlParameter("@RBSIntNo", SqlDbType.Int, 4)).Value = rbsIntNo;

            myCommand.Parameters.Add(new SqlParameter("@CofCIntNo", SqlDbType.Int, 4)).Value = cofcIntNo;
            myCommand.Parameters.Add(new SqlParameter("@LastUser", SqlDbType.VarChar, 100)).Value = lastUser;

            // myCommand.Parameters.Add(new SqlParameter("@RBSCofCIntNo", SqlDbType.Int, 4)).Direction = ParameterDirection.Output;

            SqlParameter parameterRBSCofCIntNo = new SqlParameter("@RBSCofCIntNo", SqlDbType.Int, 4);
            parameterRBSCofCIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterRBSCofCIntNo);

            try
            {
                con.Open();
                myCommand.ExecuteNonQuery();
                con.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int rbscofCIntNo = Convert.ToInt32(parameterRBSCofCIntNo.Value);

                return rbscofCIntNo;
            }
            catch (Exception e)
            {
                con.Close();
                con.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// flag = 0 : Get all roadblocksession
        /// other    : Get open roadblocksession
        /// </summary>
        /// <param name="flag"></param>
        /// <returns></returns>
        // 2013-04-10 removed by Henry no use
        //public SqlDataReader GetRoadBlockSessionList(int flag)
        //{
        //    // Create Instance of Connection and Command Object
        //    // SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("RoadBlockSessionList", con);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    SqlParameter parameterFlag = new SqlParameter("@Flag", SqlDbType.Int, 4);
        //    parameterFlag.Value = flag;
        //    myCommand.Parameters.Add(parameterFlag);

        //    // Execute the command
        //    con.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the data reader result
        //    return result;
        //}

        public DataSet GetRoadBlockSessionDS(int flag, 
            int pageSize, int pageIndex, out int totalCount, params SqlParameter[] filterParams)    //2013-04-10 add by Henry for pagination
        {
            DataSet ds = new DataSet();
            SqlDataAdapter result = new SqlDataAdapter();
            result.SelectCommand = new SqlCommand("RoadBlockSessionList", con);
            result.SelectCommand.CommandType = CommandType.StoredProcedure;

            result.SelectCommand.Parameters.Add(new SqlParameter("@Flag", SqlDbType.Int, 4)).Value = flag;
            result.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            result.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            result.SelectCommand.Parameters.Add(paraTotalCount);

            result.SelectCommand.Parameters.AddRange(filterParams);

            try
            {
                con.Open();
                result.Fill(ds);
                totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);
                return ds;
            }
            catch (SqlException)
            {
                totalCount = 0;
                return ds;
            }
            finally
            {
                con.Close();
            }
        }

        public int DeleteRoadBlockSesion(int rbsIntNo)
        {
            SqlCommand myCommand = new SqlCommand("DeleteRoadBlockSession", con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            myCommand.Parameters.Add(new SqlParameter("@RBSIntNo", SqlDbType.Int)).Value = rbsIntNo;

            try
            {
                con.Open();
                int effort = myCommand.ExecuteNonQuery();
                return effort;
            }
            catch (SqlException)
            {
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
                myCommand.Dispose();
            }
        }

        public int CheckRoadBlockInSummons(int rbsIntNo)
        {
            SqlCommand myCommand = new SqlCommand("SummonsSearchWithRoadBlockSession", con);

            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterRBSIntNo = new SqlParameter("@RBSIntNo", SqlDbType.Int, 4);
            parameterRBSIntNo.Value = rbsIntNo;
            myCommand.Parameters.Add(parameterRBSIntNo);

            SqlParameter parameterSearchMode = new SqlParameter("@SearchMode", SqlDbType.Char, 1);
            parameterSearchMode.Value = "C";
            myCommand.Parameters.Add(parameterSearchMode);

            SqlParameter parameterCount = new SqlParameter("@Count", SqlDbType.Int, 4);
            parameterCount.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterCount);
            try
            {
                con.Open();
                myCommand.ExecuteNonQuery();

                int count = (int)parameterCount.Value;

                return count;

            }
            catch (SqlException ex)
            {
                return 0;

            }
            finally
            {
                con.Close();
            }
        }

       /// <summary>
        /// Gets summons / notices at a roadblock.
        /// #5165:shows the court name in the grid view. update by Teresa 2014/01/03
        /// 2014-08-14 add errMsg parameter
        /// 2014-09-05 Jerry here just searched by IDNumber
       /// </summary>
       /// <param name="rbsIntNo"></param>
       /// <param name="idNumber"></param>
       /// <param name="autIntNosXml"></param>
       /// <param name="pageSize"></param>
       /// <param name="pageIndex"></param>
       /// <param name="totalCount"></param>
       /// <param name="errMsg"></param>
       /// <param name="regNo"></param>
       /// <param name="surname"></param>
       /// <param name="initials"></param>
       /// <returns></returns>
        //public DataSet GetSumNotWithRoadBlock(int rbsIntNo, string idNumber, string autIntNosXml, int pageSize, int pageIndex, out int totalCount, ref string errMsg, string regNo = null, string surname = null, string initials = null)
        public DataSet GetSumNotWithRoadBlock(int rbsIntNo, string idNumber, string autIntNosXml, int pageSize, int pageIndex, out int totalCount, ref string errMsg)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand();
            //adapter.SelectCommand.CommandText = "NoticeSummonsSearchByID";
            // 2013-11-29, Oscar changed
            adapter.SelectCommand.CommandText = "NoticeSummonsSearchByIDRegNoOrSurnameInitials";
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Connection = this.con;

            adapter.SelectCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.VarChar, 50)).Value = idNumber;

            // 2014-09-05 Jerry here just searched by IDNumber
            // 2013-11-29, Oscar added
            //adapter.SelectCommand.Parameters.Add(new SqlParameter("@RegNo", SqlDbType.VarChar, 10)).Value = regNo ?? string.Empty;
            //adapter.SelectCommand.Parameters.Add(new SqlParameter("@Surname", SqlDbType.VarChar, 100)).Value = surname ?? string.Empty;
            //adapter.SelectCommand.Parameters.Add(new SqlParameter("@Initials", SqlDbType.VarChar, 10)).Value = initials ?? string.Empty;

            //adapter.SelectCommand.Parameters.Add(new SqlParameter("@RBSIntNo", SqlDbType.Int, 4)).Value = rbsIntNo;
            adapter.SelectCommand.Parameters.Add(new SqlParameter("@AutIntNos", SqlDbType.Xml)).Value = autIntNosXml;
            adapter.SelectCommand.Parameters.Add(new SqlParameter("@pageSize", SqlDbType.Int)).Value = pageSize;
            adapter.SelectCommand.Parameters.Add(new SqlParameter("@pageIndex", SqlDbType.Int)).Value = pageIndex;
            SqlParameter pageParameter = new SqlParameter("@TotalCount", SqlDbType.Int);
            pageParameter.Direction = ParameterDirection.Output;
            adapter.SelectCommand.Parameters.Add(pageParameter);

            try
            {
                con.Open();
                adapter.Fill(ds);

                // 2013-11-29, Oscar added
                if (ds.Tables.Count > 1)
                    ds.Tables.RemoveAt(0);

            }
            catch (SqlException se)
            {
                System.Diagnostics.Debug.WriteLine(se);
                errMsg = se.Message; //Jerry 2014-08-14 add
            }
            finally
            {
                con.Close();
            }
            //Jerry 2014-02-26 change
            //totalCount = (int)(pageParameter.Value == DBNull.Value ? 0 : pageParameter.Value);
            totalCount = (int)(pageParameter.Value == null ? 0 : pageParameter.Value);
            return ds;
        }

        public DataSet GetAllSummonsPrintFileNameBtRBSIntNo(int rbsIntNo,
            int pageSize, int pageIndex, out int totalCount) //2013-04-10 add by Henry for pagination
        {
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand("SummonsSearchWithRoadBlockSession", con);

            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterRBSIntNo = new SqlParameter("@RBSIntNo", SqlDbType.Int, 4);
            parameterRBSIntNo.Value = rbsIntNo;
            adapter.SelectCommand.Parameters.Add(parameterRBSIntNo);

            SqlParameter parameterSearchMode = new SqlParameter("@SearchMode", SqlDbType.Char, 1);
            parameterSearchMode.Value = "P";
            adapter.SelectCommand.Parameters.Add(parameterSearchMode);

            SqlParameter parameterCount = new SqlParameter("@Count", SqlDbType.Int, 4);
            parameterCount.Direction = ParameterDirection.Output;
            adapter.SelectCommand.Parameters.Add(parameterCount);

            adapter.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            adapter.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            adapter.SelectCommand.Parameters.Add(paraTotalCount);

            try
            {
                con.Open();

                adapter.Fill(ds);
                totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);
                return ds;
            }
            catch (SqlException ex)
            {
                totalCount = 0;
                return ds;
            }

            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Determines whether the roadblock session is still active
        /// </summary>
        /// <param name="rbsIntNo">The RBS int no.</param>
        /// <returns>
        /// 	<c>true</c> if [is roadblock session active] [the specified RBS int no]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsRoadblockSessionActive(int rbsIntNo)
        {
            SqlCommand cmd = new SqlCommand("RoadBlockSessionStillActive", this.con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@RBSIntNo", SqlDbType.Int, 4).Value = rbsIntNo;

            try
            {
                this.con.Open();
                bool response = (bool)cmd.ExecuteScalar();
                return response;
            }
            catch (SqlException se)
            {
                System.Diagnostics.Debug.WriteLine(se);
            }
            finally
            {
                this.con.Close();
            }

            return false;
        }

        /// <summary>
        /// Get IDNumber DataSet
        /// Jerry 2014-08-14 add errMsg parameter
        /// </summary>
        /// <param name="idNumber"></param>
        /// <param name="autIntNosXml"></param>
        /// <param name="errMsg"></param>
        /// <param name="regNo"></param>
        /// <param name="surname"></param>
        /// <param name="initials"></param>
        /// <returns></returns>
        public DataSet GetIDNumberDataSetWithRoadBlock(string idNumber, string autIntNosXml, ref string errMsg, string regNo = null, string surname = null, string initials = null)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand();
            adapter.SelectCommand.CommandText = "IDNumberSearchByIDRegNoOrSurnameInitials";
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Connection = this.con;

            adapter.SelectCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.VarChar, 50)).Value = idNumber;
            adapter.SelectCommand.Parameters.Add(new SqlParameter("@RegNo", SqlDbType.VarChar, 10)).Value = regNo ?? string.Empty;
            adapter.SelectCommand.Parameters.Add(new SqlParameter("@Surname", SqlDbType.VarChar, 100)).Value = surname ?? string.Empty;
            adapter.SelectCommand.Parameters.Add(new SqlParameter("@Initials", SqlDbType.VarChar, 10)).Value = initials ?? string.Empty;
            adapter.SelectCommand.Parameters.Add(new SqlParameter("@AutIntNos", SqlDbType.Xml)).Value = autIntNosXml;

            try
            {
                con.Open();
                adapter.Fill(ds);
                if (ds.Tables.Count > 1)
                    ds.Tables.RemoveAt(0);
            }
            catch (SqlException se)
            {
                System.Diagnostics.Debug.WriteLine(se);
                errMsg = se.Message;
            }
            finally
            {
                con.Close();
            }

            return ds;
        }
    }

    public class ClerkofCourtDB
    {
        private SqlConnection con;

        public ClerkofCourtDB(string connString)
        {
            this.con = new SqlConnection(connString);
        }

        /// <summary>
        /// AddClertOfCourt
        /// 2013-07-29 add parameter lastUser by Henry
        /// 2014-03-07 add errMsg parameter
        /// </summary>
        /// <param name="crtIntNo"></param>
        /// <param name="cofCName"></param>
        /// <param name="cofcSurName"></param>
        /// <param name="contact"></param>
        /// <param name="lastUser"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public int AddClertOfCourt(int crtIntNo, string cofCName, string cofcSurName, string contact, string lastUser, ref string errMsg)
        {
            //SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ClerkofCourtAdd", con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterCofCName = new SqlParameter("@CofCName", SqlDbType.VarChar, 50);
            parameterCofCName.Value = cofCName;
            myCommand.Parameters.Add(parameterCofCName);

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            SqlParameter parameterCofCSurName = new SqlParameter("@CofCSurName", SqlDbType.VarChar, 100);
            parameterCofCSurName.Value = cofcSurName;
            myCommand.Parameters.Add(parameterCofCSurName);

            SqlParameter parameterContact = new SqlParameter("@Contact", SqlDbType.VarChar, 100);
            parameterContact.Value = contact;
            myCommand.Parameters.Add(parameterContact);

            SqlParameter parameterCofCIntNo = new SqlParameter("@CofCIntNo", SqlDbType.Int, 4);
            parameterCofCIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterCofCIntNo);

            myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            try
            {
                con.Open();
                myCommand.ExecuteNonQuery();
                con.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int cOfCIntNo = Convert.ToInt32(parameterCofCIntNo.Value);

                return cOfCIntNo;
            }
            catch (Exception e)
            {
                con.Close();
                con.Dispose();
                errMsg = e.Message;
                return 0;
            }
        }
        // 2013-07-29 add parameter lastUser by Henry
        public int ClerkOfCourtModify(int cofcIntNo, int crtIntNo, string cofcName, string cofcSurName, string contact, string lastUser)
        {
            //ClerkOfCourtUpdate
            SqlCommand myCommand = new SqlCommand("ClerkOfCourtUpdate", con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            myCommand.Parameters.Add(new SqlParameter("@CofCIntNo", SqlDbType.Int)).Value = cofcIntNo;
            myCommand.Parameters.Add(new SqlParameter("@CrtIntNo", SqlDbType.Int)).Value = crtIntNo;
            myCommand.Parameters.Add(new SqlParameter("@CofCName", SqlDbType.VarChar, 100)).Value = cofcName;
            myCommand.Parameters.Add(new SqlParameter("@CofCSurName", SqlDbType.VarChar, 100)).Value = cofcSurName;
            myCommand.Parameters.Add(new SqlParameter("@Contact", SqlDbType.VarChar, 100)).Value = contact;
            myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            try
            {
                con.Open();

                return myCommand.ExecuteNonQuery();
            }
            catch (SqlException)
            {
                return 0;
            }
            finally
            {
                con.Close();
            }
        }

        public ClerkOfCourt GetClerkofCourtByNo(int cofcIntNo)
        {
            SqlCommand myCommand = new SqlCommand("ClerkofCourtAListByNo", con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterCofCintNo = new SqlParameter("@CofCIntNo", SqlDbType.Int);
            parameterCofCintNo.Value = cofcIntNo;

            myCommand.Parameters.Add(parameterCofCintNo);

            try
            {
                ClerkOfCourt clerk = new ClerkOfCourt();
                con.Open();
                SqlDataReader reader = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                while (reader.Read())
                {
                    clerk.CofCIntNo = Convert.ToInt32(reader["CofCIntNo"]);
                    clerk.CofCName = reader["CofCName"].ToString();
                    clerk.CrtIntNo = Convert.ToInt32(reader["CrtIntNo"]);
                    clerk.CofCContact = reader["Contact"].ToString();
                    clerk.CofCSurName = reader["CofCSurName"].ToString();
                }

                return clerk;
            }
            catch (SqlException)
            {
                return null;
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Gets the clerk of court list.
        /// </summary>
        /// <returns></returns>
        //2013-04-07 add by Henry for pagination
        public DataSet GetClerkofCourtList()
        {
            int totalCount = 0;
            return GetClerkofCourtList(0, 0, out totalCount);
        }

        public DataSet GetClerkofCourtList(int pageSize, int pageIndex, out int totalCount)
        {
            try
            {
                DataSet ds = new DataSet();
                // Create Instance of Connection and Command Object
                //SqlConnection myConnection = new SqlConnection(mConstr);
                SqlCommand myCommand = new SqlCommand("ClerkofCourtAList", con);
                myCommand.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter result = new SqlDataAdapter(myCommand);

                //2013-04-07 add by Henry for pagination
                myCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int)).Value = pageSize;
                myCommand.Parameters.Add(new SqlParameter("@PageIndex", SqlDbType.Int)).Value = pageIndex;

                SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
                paraTotalCount.Direction = ParameterDirection.Output;
                myCommand.Parameters.Add(paraTotalCount);

                con.Open();
                result.Fill(ds);
                //con.Close();
                // Mark the Command as a SPROC

                totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);
                return ds;

            }
            catch (SqlException ex)
            {
                totalCount = 0;
                return new DataSet();
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Deletes the clerk of court by int no.
        /// </summary>
        /// <param name="cofcIntNo">The clerk of court int no.</param>
        /// <returns></returns>
        public int DeleteClerkOfCourtByIntNo(int cofcIntNo)
        {
            SqlCommand cmd = new SqlCommand("ClerkofCourtDelete", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@CofCIntNo", SqlDbType.Int, 4).Value = cofcIntNo;
            try
            {
                con.Open();
                return cmd.ExecuteNonQuery();
            }
            catch (SqlException)
            {
                return 0;
            }
            finally
            {
                con.Close();
            }
        }

        public SqlDataReader GetClerkOfCourtByCourtIntNo(int crtIntNo, int rbsIntNo)
        {
            //ClerkofCourtGetByCourtIntNo

            // Create Instance of Connection and Command Object
            //SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ClerkofCourtGetByCourtIntNo", con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            myCommand.Parameters.Add(new SqlParameter("@CrtIntNo", SqlDbType.Int)).Value = crtIntNo;
            myCommand.Parameters.Add(new SqlParameter("@RBSIntNo", SqlDbType.Int)).Value = rbsIntNo;
            // Execute the command

            con.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        public SqlDataReader GetClerkofCourtByRBSNo(int rbsIntNo)
        {
            // Create Instance of Connection and Command Object
            //SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ClerkofCourtGetByRBSIntNo", con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            myCommand.Parameters.Add(new SqlParameter("@RBSIntNo", SqlDbType.Int)).Value = rbsIntNo;
            // Execute the command

            con.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        public DataSet GetAutClerkofCourt()
        {
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand();
            adapter.SelectCommand.CommandText = "AuthorityListWithClerkofCourt";
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Connection = con;
            try
            {
                con.Open();
                adapter.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                return ds;
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Check Clerk In RoadBlockSessionCofCourt row
        /// Jerry 2014-03-05 add
        /// </summary>
        /// <param name="cofCIntNo"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public int CheckClerkInRoadblockSession(int cofCIntNo, ref string errMsg)
        {
            SqlCommand myCommand = new SqlCommand("CheckClerkInRoadblockSession", con);

            myCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter parameterCofCIntNo = new SqlParameter("@CofCIntNo", SqlDbType.Int, 4);
            parameterCofCIntNo.Value = cofCIntNo;
            myCommand.Parameters.Add(parameterCofCIntNo);

            try
            {
                con.Open();
                int count = (int)myCommand.ExecuteScalar();
                return count;
            }
            catch (SqlException ex)
            {
                errMsg = ex.Message;
                return 0;
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// check the ClertOfCourt before add
        /// Jerry 2014-03-07 add
        /// </summary>
        /// <param name="crtIntNo"></param>
        /// <param name="cofCName"></param>
        /// <param name="cofcSurName"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public int CheckClertOfCourt(int crtIntNo, string cofCName, string cofcSurName, ref string errMsg)
        {
            //SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CheckClerkofCourt", con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterCofCName = new SqlParameter("@CofCName", SqlDbType.VarChar, 50);
            parameterCofCName.Value = cofCName;
            myCommand.Parameters.Add(parameterCofCName);

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            SqlParameter parameterCofCSurName = new SqlParameter("@CofCSurName", SqlDbType.VarChar, 100);
            parameterCofCSurName.Value = cofcSurName;
            myCommand.Parameters.Add(parameterCofCSurName);
            
            try
            {
                con.Open();
                int count = (int)myCommand.ExecuteScalar();
                return count;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return -1;
            }
            finally
            {
                con.Close();
            }
        }

    }

    public partial class RoadBlockSessionUserDB
    {
        //RoadBlockUserCheck

        private SqlConnection con;
        public RoadBlockSessionUserDB(string connString)
        {
            this.con = new SqlConnection(connString);
        }

        public string RoadBlockSessionUserCheck(int UserIntNo)
        {

            SqlCommand myCommand = new SqlCommand("RoadBlockUserCheck", con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int);
            parameterUserIntNo.Value = UserIntNo;
            myCommand.Parameters.Add(parameterUserIntNo);

            SqlParameter parameterRoadBlockUser = new SqlParameter("@RoadBlockUser", SqlDbType.VarChar, 50);
            parameterRoadBlockUser.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterRoadBlockUser);

            try
            {

                con.Open();
                myCommand.ExecuteNonQuery();

                string roadBlockUserFlag = parameterRoadBlockUser.Value.ToString().Trim();
                return roadBlockUserFlag;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return String.Empty;
            }
            finally
            {
                con.Close();
            }
        }
        // 2013-07-29 add parameter lastUser by Henry
        public int AddRoadBlockSessionUser(int rbsIntNo, int userIntNo, string lastUser)
        {
            //RoadBlockSessUserAdd
            SqlCommand myCommand = new SqlCommand("RoadBlockSessUserAdd", con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int);
            parameterUserIntNo.Value = userIntNo;

            myCommand.Parameters.Add(parameterUserIntNo);

            SqlParameter parameterRBSIntNo = new SqlParameter("@RBSIntNo", SqlDbType.Int);
            parameterRBSIntNo.Value = rbsIntNo;

            myCommand.Parameters.Add(parameterRBSIntNo);

            SqlParameter parameterRoadBlockUser = new SqlParameter("@RSUIntNo", SqlDbType.Int);
            parameterRoadBlockUser.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterRoadBlockUser);

            myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                con.Open();
                myCommand.ExecuteNonQuery();

                int RBSUIntNo = 0;
                RBSUIntNo = Convert.ToInt32(parameterRoadBlockUser.Value);

                return RBSUIntNo;
            }
            catch (SqlException ex)
            {

                return 0;
            }
            finally { con.Close(); }
        }

        public int CheckRoadBlockSessionUser(int rbsIntNo, int userIntNo)
        {
            //RoadBlockSessionUserCheck
            SqlCommand myCommand = new SqlCommand("RoadBlockSessionUserCheck", con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int);
            parameterUserIntNo.Value = userIntNo;

            myCommand.Parameters.Add(parameterUserIntNo);

            SqlParameter parameterRBSIntNo = new SqlParameter("@RBSIntNo", SqlDbType.Int);
            parameterRBSIntNo.Value = rbsIntNo;

            myCommand.Parameters.Add(parameterRBSIntNo);

            SqlParameter parameterRoadBlockUser = new SqlParameter("@RSUIntNo", SqlDbType.Int);
            parameterRoadBlockUser.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterRoadBlockUser);

            try
            {
                con.Open();
                myCommand.ExecuteNonQuery();

                int RBSUIntNo = 0;
                RBSUIntNo = Convert.ToInt32(parameterRoadBlockUser.Value);

                return RBSUIntNo;
            }
            catch (SqlException ex)
            {
                return 0;
            }
            finally { con.Close(); }
        }

    }

}
