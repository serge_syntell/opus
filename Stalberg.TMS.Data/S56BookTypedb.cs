using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


/* TLP : 21.11.2008
 * Page : S56BookTypedb.cs
 * Functionality : Datastructure to maintain the S56 Book Type information
 */

namespace Stalberg.TMS
{
    // TLP: 21.11.2008
	//*******************************************************
	//
	// S56 BookType Class
	//
	// A simple data class that encapsulates details about a particular book type 
	//
	//*******************************************************

	public partial class S56BookTypeDetails
	{
        public Int32 S56BTIntNo;        
        public string  S56BookType;       
	}
    
    //*******************************************************
	//
    // S56 BooktypeDB Class
	//
	//*******************************************************

    public partial class S56BookTypeDB
    {
		string mConstr = "";

        public S56BookTypeDB(string vConstr)
		{
			mConstr = vConstr;
		}

		//*******************************************************
		// Functionality to add S56 Book Type details to database
        //*******************************************************

        public int AddS56BookType(string  s56BookType, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("S56BookTypeAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@S56BookType", SqlDbType.VarChar, 30).Value = s56BookType;
            myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            myCommand.Parameters.Add("@S56BTIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
                       
           try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the BookTypeID using Output Param from SPROC
                Int32 addBTIntNo = (Int32)myCommand.Parameters["@S56BTIntNo"].Value;

                return addBTIntNo;               
               
          }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }
                
        // Functionality : to fetch the S56BookType information from database
        //                 based on the input parameter supplied 

        public S56BookTypeDetails GetS56BookTypeDetails(Int32 S56BTIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("S56BookTypeDetails", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
            SqlParameter parameterS56BTIntNo = new SqlParameter("@S56BTIntNo", SqlDbType.Int, 4);
            parameterS56BTIntNo.Value = S56BTIntNo;
            myCommand.Parameters.Add(parameterS56BTIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            S56BookTypeDetails myBookTypeDetails = new S56BookTypeDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
                myBookTypeDetails.S56BookType = result["S56BookType"].ToString();
            }
			result.Close();
            return myBookTypeDetails;
		}

		
        /// <summary>
        /// <returns>Returns the dataset containing S56BookType Fields
        /// 
		public DataSet GetS56BookTypeListDS ()
		{
			SqlDataAdapter sqlDAS56BookType = new SqlDataAdapter();
            DataSet dsS56BookType = new DataSet();

			// Create Instance of Connection and Command Object
            sqlDAS56BookType.SelectCommand = new SqlCommand();
            sqlDAS56BookType.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAS56BookType.SelectCommand.CommandText = "S56BookTypeList";

			// Mark the Command as a SPROC
            sqlDAS56BookType.SelectCommand.CommandType = CommandType.StoredProcedure;

          	// Execute the command and close the connection
            sqlDAS56BookType.Fill(dsS56BookType);
            sqlDAS56BookType.SelectCommand.Connection.Dispose();

			// Return the dataset result
            return dsS56BookType;
		}
		
        /// <summary>
        /// Updates the S56 BookType table using input & output parameters        
        ///
        public int UpdateS56BookType(string s56BookType, string lastUser, Int32 s56BTIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("S56BookTypeUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
            myCommand.Parameters.Add("@S56BookType", SqlDbType.VarChar, 30).Value = s56BookType;
            myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            myCommand.Parameters.Add("@S56BTIntNo", SqlDbType.Int, 4).Value = s56BTIntNo;
            myCommand.Parameters["@S56BTIntNo"].Direction = ParameterDirection.InputOutput;
             
			try
			{
                myConnection.Open();
                // Calculate the Book Type ID using Output Param from SPROC
                //s56BTIntNo = myCommand.ExecuteNonQuery();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the BookTypeID using Output Param from SPROC
                s56BTIntNo = (Int32)myCommand.Parameters["@S56BTIntNo"].Value;

                return s56BTIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

       
        // Deletes the S56BookType record based on the BookTypeIntNo supplied
		public int  DeleteS56BookType (Int32 s56BTIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("S56BookTypeDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
            myCommand.Parameters.Add("@S56BTIntNo", SqlDbType.Int, 4).Value = s56BTIntNo;
            myCommand.Parameters["@S56BTIntNo"].Direction = ParameterDirection.InputOutput;

			try
			{
				myConnection.Open();
                // Calculate the Book Type ID using Output Param from SPROC
				//s56BTIntNo = myCommand.ExecuteNonQuery();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the BookTypeID using Output Param from SPROC
                s56BTIntNo = (Int32)myCommand.Parameters["@S56BTIntNo"].Value;

                return s56BTIntNo;
			}
			
            catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

	

	}
}

