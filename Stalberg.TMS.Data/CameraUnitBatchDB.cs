using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the details of a camera unit batch
    /// </summary>
    public partial class CameraUnitBatchDetails
    {
        public Int32 CUTIntNo;
        public Int32 CUPIntNo;
        public string LastUser;
        public string CUBFileName;
        public Int32 CUBStartNo;
        public DateTime CUBContraventionDate;
        public DateTime CUBResponseDate;
        public DateTime CUBAllocDate;
        public Int32 CUBNoOfRecords;
        public Int32 CUBNoOfSuccess;
        public Int32 CUBNoOfFailed;
        public string CUBReasonFailed;
    }

    /// <summary>
    /// Contains all the methods for a Camera Unit batch to communicate with the database
    /// </summary>
    public partial class CameraUnitBatchDB
    {
        // Fields
        string connectionString = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="CameraUnitBatchDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public CameraUnitBatchDB(string connectionString)
        {
            this.connectionString = connectionString;
        }

        //*******************************************************
        //
        // The GetCameraUnitBatchDetails method returns a CameraUnitBatchDetails
        // struct that contains information about a specific transaction number
        //
        //*******************************************************

        public CameraUnitBatchDetails GetCameraUnitBatchDetails(int cubIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CameraUnitBatchDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCUBIntNo = new SqlParameter("@CUBIntNo", SqlDbType.Int, 4);
            parameterCUBIntNo.Value = cubIntNo;
            myCommand.Parameters.Add(parameterCUBIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            CameraUnitBatchDetails myCameraUnitBatchDetails = new CameraUnitBatchDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myCameraUnitBatchDetails.CUTIntNo = Convert.ToInt32(result["CUTIntNo"]);
                myCameraUnitBatchDetails.CUPIntNo = Convert.ToInt32(result["CUPIntNo"]);
                myCameraUnitBatchDetails.CUBFileName = result["CUBFileName"].ToString();
                myCameraUnitBatchDetails.CUBStartNo = Convert.ToInt32(result["CUBStartNo"]);
                myCameraUnitBatchDetails.CUBContraventionDate = Convert.ToDateTime(result["CUBContraventionDate"]);
                myCameraUnitBatchDetails.CUBResponseDate = Convert.ToDateTime(result["CUBResponseDate"]);
                myCameraUnitBatchDetails.CUBAllocDate = Convert.ToDateTime(result["CUBAllocDate"]);
                myCameraUnitBatchDetails.CUBNoOfRecords = Convert.ToInt32(result["CUBNoOfRecords"]);
                myCameraUnitBatchDetails.CUBNoOfSuccess = Convert.ToInt32(result["CUBNoOfSuccess"]);
                myCameraUnitBatchDetails.CUBNoOfFailed = Convert.ToInt32(result["CUBNoOfFailed"]);
                myCameraUnitBatchDetails.CUBReasonFailed = result["CUBReasonFailed"].ToString();
                myCameraUnitBatchDetails.LastUser = result["LastUser"].ToString();
            }
            result.Close();
            return myCameraUnitBatchDetails;
        }

        public SqlDataReader CameraUnitBatchCheckResponse(string autNo, string camUnitID)
        {
            return CameraUnitBatchCheckResponse(autNo, camUnitID, string.Empty);
        }

        //dls 091022 - pass in the actual file name for coct
        public SqlDataReader CameraUnitBatchCheckResponse(string autNo, string camUnitID, string infringementFile)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CameraUnitBatchCheckResponse", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutNo = new SqlParameter("@AutNo", SqlDbType.VarChar, 6);
            parameterAutNo.Value = autNo;
            myCommand.Parameters.Add(parameterAutNo);

            SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
            parameterCamUnitID.Value = camUnitID;
            myCommand.Parameters.Add(parameterCamUnitID);

            SqlParameter parameterInfringementFile = new SqlParameter("@InfringementFile", SqlDbType.VarChar, 30);
            parameterInfringementFile.Value = infringementFile;
            myCommand.Parameters.Add(parameterInfringementFile);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        public SqlDataReader CameraUnitBatchDetails(string autNo, string camUnitID, string cubFileName)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CameraUnitBatchDetails", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutNo = new SqlParameter("@AutNo", SqlDbType.VarChar, 6);
            parameterAutNo.Value = autNo;
            myCommand.Parameters.Add(parameterAutNo);

            SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
            parameterCamUnitID.Value = camUnitID;
            myCommand.Parameters.Add(parameterCamUnitID);

            SqlParameter parameterCUBFileName = new SqlParameter("@CUBFileName", SqlDbType.VarChar, 20);
            parameterCUBFileName.Value = cubFileName;
            myCommand.Parameters.Add(parameterCUBFileName);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        //*******************************************************
        //
        // The AddCameraUnitBatch method inserts a new transaction number record
        // into the CameraUnitBatch database.  A unique "CUTIntNo"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddCameraUnitBatchWithRequest(string autNo, string camUnitID, int cubStartNo,
            string cubFileName, string lastUser, int cupIntNo, string cubRequestPanels)
        {
            //dls 080121 - add setting of new flag CUBRequestPanels: default = N, only set to Y if creating a manual contravention file

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CameraUnitBatchAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@AutNo", SqlDbType.VarChar, 6).Value = autNo;
            myCommand.Parameters.Add("@CamUnitID", SqlDbType.VarChar, 5).Value = camUnitID;
            myCommand.Parameters.Add("@CUPIntNo", SqlDbType.Int, 4).Value = cupIntNo;
            myCommand.Parameters.Add("@CUBStartNo", SqlDbType.Int, 4).Value = cubStartNo;
            myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            myCommand.Parameters.Add("@CUBFileName", SqlDbType.VarChar, 50).Value = cubFileName;
            myCommand.Parameters.Add("@CUBIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
            myCommand.Parameters.Add("@CUBRequestPanels", SqlDbType.Char, 1).Value = cubRequestPanels;

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int cubIntNo = Convert.ToInt32(myCommand.Parameters["@CUBIntNo"].Value);

                return cubIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int AddCameraUnitBatch(string autNo, string camUnitID, int cubStartNo,
            string cubFileName, string lastUser, int cupIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CameraUnitBatchAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutNo = new SqlParameter("@AutNo", SqlDbType.VarChar, 6);
            parameterAutNo.Value = autNo;
            myCommand.Parameters.Add(parameterAutNo);

            SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
            parameterCamUnitID.Value = camUnitID;
            myCommand.Parameters.Add(parameterCamUnitID);

            SqlParameter parameterCUPIntNo = new SqlParameter("@CUPIntNo", SqlDbType.Int, 4);
            parameterCUPIntNo.Value = cupIntNo;
            myCommand.Parameters.Add(parameterCUPIntNo);

            SqlParameter parameterCUBStartNo = new SqlParameter("@CUBStartNo", SqlDbType.Int, 4);
            parameterCUBStartNo.Value = cubStartNo;
            myCommand.Parameters.Add(parameterCUBStartNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterCUBFileName = new SqlParameter("@CUBFileName", SqlDbType.VarChar, 50);
            parameterCUBFileName.Value = cubFileName;
            myCommand.Parameters.Add(parameterCUBFileName);

            SqlParameter parameterCUTIntNo = new SqlParameter("@CUBIntNo", SqlDbType.Int, 4);
            parameterCUTIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterCUTIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int cubIntNo = Convert.ToInt32(parameterCUTIntNo.Value);

                return cubIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int AddCameraUnitBatch_CA(string autNo, string camUnitID, string cubFileName, string lastUser)
        {
            return AddCameraUnitBatch_CA(autNo, camUnitID, cubFileName, lastUser, false);
        }

        public int AddCameraUnitBatch_CA(string autNo, string camUnitID, string cubFileName, string lastUser, bool reloadProcess)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CameraUnitBatchAdd_CA", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutNo = new SqlParameter("@AutNo", SqlDbType.VarChar, 6);
            parameterAutNo.Value = autNo;
            myCommand.Parameters.Add(parameterAutNo);

            SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
            parameterCamUnitID.Value = camUnitID;
            myCommand.Parameters.Add(parameterCamUnitID);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterCUBFileName = new SqlParameter("@CUBFileName", SqlDbType.VarChar, 50);
            parameterCUBFileName.Value = cubFileName;
            myCommand.Parameters.Add(parameterCUBFileName);

            SqlParameter parameterCUBReloadProcess = new SqlParameter("@CUBReloadProcess", SqlDbType.Bit);
            parameterCUBReloadProcess.Value = reloadProcess;
            myCommand.Parameters.Add(parameterCUBReloadProcess);

            SqlParameter parameterCUTIntNo = new SqlParameter("@CUBIntNo", SqlDbType.Int, 4);
            parameterCUTIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterCUTIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int cubIntNo = Convert.ToInt32(parameterCUTIntNo.Value);

                return cubIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int AddCameraUnitBatch_CC(string autNo, string camUnitID, string cubFileName, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CameraUnitBatchAdd_CC", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutNo = new SqlParameter("@AutNo", SqlDbType.VarChar, 6);
            parameterAutNo.Value = autNo;
            myCommand.Parameters.Add(parameterAutNo);

            SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
            parameterCamUnitID.Value = camUnitID;
            myCommand.Parameters.Add(parameterCamUnitID);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterCUBFileName = new SqlParameter("@CUBFileName", SqlDbType.VarChar, 50);
            parameterCUBFileName.Value = cubFileName;
            myCommand.Parameters.Add(parameterCUBFileName);

            SqlParameter parameterCUTIntNo = new SqlParameter("@CUBIntNo", SqlDbType.Int, 4);
            parameterCUTIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterCUTIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int cubIntNo = Convert.ToInt32(parameterCUTIntNo.Value);

                return cubIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        // 2013-07-23 comment by Henry for useless
        //public int UpdateCameraUnitBatchColumn(string autNo, string camUnitID, string cubFileName, string lastUser,
        //    int cubIntNo, string colValue, string colName, ref string errMessage)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("CameraUnitBatchUpdateColumn", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutNo = new SqlParameter("@AutNo", SqlDbType.VarChar, 6);
        //    parameterAutNo.Value = autNo;
        //    myCommand.Parameters.Add(parameterAutNo);

        //    SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
        //    parameterCamUnitID.Value = camUnitID;
        //    myCommand.Parameters.Add(parameterCamUnitID);

        //    SqlParameter parameterColValue = new SqlParameter("@ColValue", SqlDbType.VarChar, 255);
        //    parameterColValue.Value = colValue;
        //    myCommand.Parameters.Add(parameterColValue);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterColName = new SqlParameter("@ColName", SqlDbType.VarChar, 50);
        //    parameterColName.Value = colName;
        //    myCommand.Parameters.Add(parameterColName);

        //    SqlParameter parameterCUBFileName = new SqlParameter("@CUBFileName", SqlDbType.VarChar, 50);
        //    parameterCUBFileName.Value = cubFileName;
        //    myCommand.Parameters.Add(parameterCUBFileName);

        //    SqlParameter parameterCUBIntNo = new SqlParameter("@CUBIntNo", SqlDbType.Int, 4);
        //    parameterCUBIntNo.Direction = ParameterDirection.Output;
        //    parameterCUBIntNo.Value = cubIntNo;
        //    myCommand.Parameters.Add(parameterCUBIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();

        //        cubIntNo = Convert.ToInt32(parameterCUBIntNo.Value);

        //        return cubIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        errMessage = e.Message;
        //        return 0;
        //    }
        //    finally
        //    {
        //        myConnection.Dispose();
        //    }
        //}

        public int UpdateCameraUnitBatch(string autNo, string camUnitID, string cubFileName, string lastUser,
            DateTime cubResponseDate, int cubNoOfFailed, int cubNoOfSuccess, ref string errMessage)
        {
            return UpdateCameraUnitBatch(autNo, camUnitID, cubFileName, lastUser, cubResponseDate, cubNoOfFailed, cubNoOfSuccess, ref errMessage, false);
        }

        public int UpdateCameraUnitBatch(string autNo, string camUnitID, string cubFileName, string lastUser,
            DateTime cubResponseDate, int cubNoOfFailed, int cubNoOfSuccess, ref string errMessage, bool reloadProcess)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CameraUnitBatchUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutNo = new SqlParameter("@AutNo", SqlDbType.VarChar, 6);
            parameterAutNo.Value = autNo;
            myCommand.Parameters.Add(parameterAutNo);

            SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
            parameterCamUnitID.Value = camUnitID;
            myCommand.Parameters.Add(parameterCamUnitID);

            SqlParameter parameterCUBResponseDate = new SqlParameter("@CUBResponseDate", SqlDbType.SmallDateTime);
            parameterCUBResponseDate.Value = cubResponseDate;
            myCommand.Parameters.Add(parameterCUBResponseDate);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterCUBNoOfFailed = new SqlParameter("@CUBNoOfFailed", SqlDbType.Int, 4);
            parameterCUBNoOfFailed.Value = cubNoOfFailed;
            myCommand.Parameters.Add(parameterCUBNoOfFailed);

            SqlParameter parameterCUBNoOfSuccess = new SqlParameter("@CUBNoOfSuccess", SqlDbType.Int, 4);
            parameterCUBNoOfSuccess.Value = cubNoOfSuccess;
            myCommand.Parameters.Add(parameterCUBNoOfSuccess);

            SqlParameter parameterCUBFileName = new SqlParameter("@CUBFileName", SqlDbType.VarChar, 50);
            parameterCUBFileName.Value = cubFileName;
            myCommand.Parameters.Add(parameterCUBFileName);

            SqlParameter parameterCUBReloadProcess = new SqlParameter("@CUBReloadProcess", SqlDbType.Bit);
            parameterCUBReloadProcess.Value = reloadProcess;
            myCommand.Parameters.Add(parameterCUBReloadProcess);

            SqlParameter parameterCUBIntNo = new SqlParameter("@CUBIntNo", SqlDbType.Int, 4);
            parameterCUBIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterCUBIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();

                int cubIntNo = Convert.ToInt32(parameterCUBIntNo.Value);

                return cubIntNo;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        //public SqlDataReader GetCameraUnitBatchList(int autIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("CameraUnitBatchsList", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the datareader result
        //    return result;
        //}

        //public DataSet GetCameraUnitBatchListDS(int autIntNo)
        //{
        //    SqlDataAdapter sqlDACameraUnitBatchs = new SqlDataAdapter();
        //    DataSet dsCameraUnitBatchs = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlDACameraUnitBatchs.SelectCommand = new SqlCommand();
        //    sqlDACameraUnitBatchs.SelectCommand.Connection = new SqlConnection(connectionString);			
        //    sqlDACameraUnitBatchs.SelectCommand.CommandText = "CameraUnitBatchList";

        //    // Mark the Command as a SPROC
        //    sqlDACameraUnitBatchs.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    sqlDACameraUnitBatchs.SelectCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command and close the connection
        //    sqlDACameraUnitBatchs.Fill(dsCameraUnitBatchs);
        //    sqlDACameraUnitBatchs.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsCameraUnitBatchs;		
        //}

        //public int UpdateCameraUnitBatch(int autNo, string camUnitID, int cutNextNo, string lastUser) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("CameraUnitBatchUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutNo = new SqlParameter("@AutNo", SqlDbType.VarChar, 6);
        //    parameterAutNo.Value = autNo;
        //    myCommand.Parameters.Add(parameterAutNo);

        //    SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
        //    parameterCamUnitID.Value = camUnitID;
        //    myCommand.Parameters.Add(parameterCamUnitID);

        //    SqlParameter parameterCutNextNo = new SqlParameter("@CutNextNo", SqlDbType.Int, 4);
        //    parameterCutNextNo.Value = cutNextNo;
        //    myCommand.Parameters.Add(parameterCutNextNo);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterCUTIntNo = new SqlParameter("@CUTIntNo", SqlDbType.Int, 4);
        //    parameterCUTIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterCUTIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int CUTIntNo = (int)myCommand.Parameters["@CUTIntNo"].Value;

        //        return CUTIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        //public int UpdateTranNumber(int cutNextNo, string lastUser, int cutIntNo) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("CameraUnitBatchUpdateNumber", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterCutNextNo = new SqlParameter("@CutNextNo", SqlDbType.Int, 4);
        //    parameterCutNextNo.Value = cutNextNo;
        //    myCommand.Parameters.Add(parameterCutNextNo);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterCUTIntNo = new SqlParameter("@CUTIntNo", SqlDbType.Int, 4);
        //    parameterCUTIntNo.Value = cutIntNo;
        //    parameterCUTIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterCUTIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int CUTIntNo = (int)myCommand.Parameters["@CUTIntNo"].Value;

        //        return CUTIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        //public String DeleteCameraUnitBatch (int cutIntNo)
        //{

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("CameraUnitBatchDelete", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterCUTIntNo = new SqlParameter("@CUTIntNo", SqlDbType.Int, 4);
        //    parameterCUTIntNo.Value = cutIntNo;
        //    parameterCUTIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterCUTIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int userId = (int)parameterCUTIntNo.Value;

        //        return userId.ToString();
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return String.Empty;
        //    }
        //}

        //internal int GetNextCameraUnitBatch(string autNo, string camUnitID, int maxNo, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("CameraUnitBatchNextNumber", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
        //    parameterCamUnitID.Value = camUnitID;
        //    myCommand.Parameters.Add(parameterCamUnitID);

        //    SqlParameter parameterAutNo = new SqlParameter("@AutNo", SqlDbType.VarChar, 6);
        //    parameterAutNo.Value = autNo;
        //    myCommand.Parameters.Add(parameterAutNo);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterMaxNo = new SqlParameter("@MaxNo", SqlDbType.Int, 4);
        //    parameterMaxNo.Value = maxNo;
        //    myCommand.Parameters.Add(parameterMaxNo);

        //    SqlParameter parameterTNumber = new SqlParameter("@CUTNextNo", SqlDbType.Int);
        //    parameterTNumber.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterTNumber);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int cutNextNo = Convert.ToInt32(parameterTNumber.Value);

        //        return cutNextNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        /// <summary>
        /// Gets the contravention file list.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="cameraUnitID">The camera unit ID.</param>
        /// <param name="onlyOutstanding">if set to <c>true</c> [only outstanding].</param>
        /// <returns>A <see cref="DataSet"/>.</returns>
        public DataSet GetContraventionFileList(int autIntNo, string cameraUnitID, bool onlyOutstanding, int pageSize, int pageIndex, out int totalCount)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CameraUnitBatchEnquiry", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            if (cameraUnitID.Length > 0)
                com.Parameters.Add("@CamUnitID", SqlDbType.VarChar, 6).Value = cameraUnitID;
            com.Parameters.Add("@Outstanding", SqlDbType.Bit, 1).Value = onlyOutstanding;

            // 2013-03-28 Henry add for pagination
            com.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            com.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            com.Parameters.Add(paraTotalCount);

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            return ds;
        }

    }
}
