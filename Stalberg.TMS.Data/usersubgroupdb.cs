using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


namespace Stalberg.TMS
{

    //*******************************************************
    //
    // UserSubGroupDetails Class
    //
    // A simple data class that encapsulates details about a particular user 
    //
    //*******************************************************

    public partial class UserSubGroupDetails 
	{
		public Int32 UGIntNo;
        public string USGName;
		public string USGComment;
    }

    //*******************************************************
    //
    // UserSubGroupDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query UserSubGroups within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class UserSubGroupDB {

		string mConstr = "";

		public UserSubGroupDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // UserSubGroupDB.GetUserSubGroupDetails() Method <a name="GetUserSubGroupDetails"></a>
        //
        // The GetUserSubGroupDetails method returns a UserSubGroupDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public UserSubGroupDetails GetUserSubGroupDetails(int usgIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("UserSubGroupDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterUSGIntNo = new SqlParameter("@USGIntNo", SqlDbType.Int, 4);
            parameterUSGIntNo.Value = usgIntNo;
            myCommand.Parameters.Add(parameterUSGIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			UserSubGroupDetails myUserSubGroupDetails = new UserSubGroupDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myUserSubGroupDetails.UGIntNo = Convert.ToInt32(result["UGIntNo"]);
				myUserSubGroupDetails.USGName = result["USGName"].ToString();
				myUserSubGroupDetails.USGComment = result["USGComment"].ToString();
			}

			result.Close();
            return myUserSubGroupDetails;
        }

        //*******************************************************
        //
        // UserSubGroupsDB.AddUser() Method <a name="AddUser"></a>
        //
        // The AddUser method inserts a new user record
        // into the userGroups database.  A unique "UserId"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddUserSubGroup
			(int ugIntNo, string usgName, string usgComment) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("UserSubGroupAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
			SqlParameter parameterUGIntNo = new SqlParameter("@UGIntNo", SqlDbType.Int,4);
			parameterUGIntNo.Value = ugIntNo;
			myCommand.Parameters.Add(parameterUGIntNo);

			SqlParameter parameterUSGName = new SqlParameter("@USGName", SqlDbType.VarChar, 50);
			parameterUSGName.Value = usgName;
			myCommand.Parameters.Add(parameterUSGName);

			SqlParameter parameterUSGComment = new SqlParameter("@USGComment", SqlDbType.Text);
			parameterUSGComment.Value = usgComment;
			myCommand.Parameters.Add(parameterUSGComment);

			SqlParameter parameterUSGIntNo = new SqlParameter("@USGIntNo", SqlDbType.Int, 4);
            parameterUSGIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterUSGIntNo);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int addUSGIntNo = Convert.ToInt32(parameterUSGIntNo.Value);

                return addUSGIntNo;
            }
            catch {
				myConnection.Dispose();
                return 0;
            }
        }

 
		public SqlDataReader GetUserSubGroupList(int ugIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("UserSubGroupList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterUGIntNo = new SqlParameter("@UGIntNo", SqlDbType.Int, 4);
			parameterUGIntNo.Value = ugIntNo;
			myCommand.Parameters.Add(parameterUGIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		public SqlDataReader GetUserGroupName(int usgIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("GetUserGroupName", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterUSGIntNo = new SqlParameter("@USGIntNo", SqlDbType.Int, 4);
			parameterUSGIntNo.Value = usgIntNo;
			myCommand.Parameters.Add(parameterUSGIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		public DataSet GetUserSubGroupListDS(int ugIntNo)
		{
			SqlDataAdapter sqlDAUserSubGroups = new SqlDataAdapter();
			DataSet dsUserSubGroups = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAUserSubGroups.SelectCommand = new SqlCommand();
			sqlDAUserSubGroups.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAUserSubGroups.SelectCommand.CommandText = "UserSubGroupList";

			// Mark the Command as a SPROC
			sqlDAUserSubGroups.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterUGIntNo = new SqlParameter("@UGIntNo", SqlDbType.Int, 4);
			parameterUGIntNo.Value = ugIntNo;
			sqlDAUserSubGroups.SelectCommand.Parameters.Add(parameterUGIntNo);

			// Execute the command and close the connection
			sqlDAUserSubGroups.Fill(dsUserSubGroups);
			sqlDAUserSubGroups.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsUserSubGroups;		
		}

		public int UpdateUserSubGroup

			//password only updated via change password
			(int usgIntNo, int ugIntNo, string usgName, string usgComment) 
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("UserSubGroupUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterUGIntNo = new SqlParameter("@UGIntNo", SqlDbType.Int,4);
			parameterUGIntNo.Value = ugIntNo;
			myCommand.Parameters.Add(parameterUGIntNo);

			SqlParameter parameterUSGName = new SqlParameter("@USGName", SqlDbType.VarChar, 50);
			parameterUSGName.Value = usgName;
			myCommand.Parameters.Add(parameterUSGName);

			SqlParameter parameterUSGComment = new SqlParameter("@USGComment", SqlDbType.Text);
			parameterUSGComment.Value = usgComment;
			myCommand.Parameters.Add(parameterUSGComment);

			SqlParameter parameterUSGIntNo = new SqlParameter("@USGIntNo", SqlDbType.Int, 4);
			parameterUSGIntNo.Direction = ParameterDirection.InputOutput;
			parameterUSGIntNo.Value = usgIntNo;
			myCommand.Parameters.Add(parameterUSGIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int updUSGIntNo = (int)myCommand.Parameters["@USGIntNo"].Value;

				return updUSGIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		public String DeleteUserSubGroup (int usgIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("UserSubGroupDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterUSGIntNo = new SqlParameter("@USGIntNo", SqlDbType.Int, 4);
			parameterUSGIntNo.Value = usgIntNo;
			parameterUSGIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterUSGIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int delUSGIntNo = (int)parameterUSGIntNo.Value;

				return delUSGIntNo.ToString();
			}
			catch 
			{
				myConnection.Dispose();
				return String.Empty;
			}
		}
	}
}

