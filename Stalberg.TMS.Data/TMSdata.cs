using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS_TPExInt.Components
{
    /// <summary>
    /// Represents duplicate notice information
    /// </summary>
    public class DuplicateNotice
    {
        // Fields
        private string filmNo = string.Empty;
        private string frameNo = string.Empty;
        private int frameIntNo = 0;
        private int notIntNo = 0;
        private string ticketNo = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="DuplicateNotice"/> class.
        /// </summary>
        public DuplicateNotice()
        {
        }

        /// <summary>
        /// Gets or sets the frame int no.
        /// </summary>
        /// <value>The frame int no.</value>
        public int FrameIntNo
        {
            get { return this.frameIntNo; }
            set { this.frameIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the frame number.
        /// </summary>
        /// <value>The frame number.</value>
        public string FrameNumber
        {
            get { return this.frameNo; }
            set { this.frameNo = value; }
        }

        /// <summary>
        /// Gets or sets the film number.
        /// </summary>
        /// <value>The film number.</value>
        public string FilmNumber
        {
            get { return this.filmNo; }
            set { this.filmNo = value; }
        }

        /// <summary>
        /// Gets or sets the not int no.
        /// </summary>
        /// <value>The not int no.</value>
        public int NotIntNo
        {
            get { return this.notIntNo; }
            set { this.notIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the ticket number.
        /// </summary>
        /// <value>The ticket number.</value>
        public string TicketNumber
        {
            get { return this.ticketNo; }
            set { this.ticketNo = value; }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            return string.Format("Film: {0} - Frame: {1} ({2}) [ Notice: '{3}' ({4}) ]", this.filmNo, this.frameNo, this.frameIntNo, this.ticketNo, this.notIntNo);
        }

    }

    /// <summary>
    /// Contains methods to manipulate TMS Data.
    /// </summary>
    public class TMSData
    {
        // Fields
        private string connectionString;

        // Constants
        private readonly DateTime NO_DATE = new DateTime(1900, 1, 1);

        /// <summary>
        /// Initializes a new instance of the <see cref="TMSData"/> class.
        /// </summary>
        /// <param name="vConstr">The v constr.</param>
        public TMSData(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Represents a field pair
        /// </summary>
        public struct FieldPair
        {
            public string fieldNo, fieldValue;

            /// <summary>
            /// Initializes a new instance of the <see cref="FieldPair"/> class.
            /// </summary>
            /// <param name="p1">The p1.</param>
            /// <param name="p2">The p2.</param>
            public FieldPair(string p1, string p2)
            {
                fieldNo = p1;
                fieldValue = p2;
            }
        }

        /// <summary>
        /// Gets the films.
        /// </summary>
        /// <param name="processor">The processor.</param>
        /// <returns>A <see cref="SqlDataReader"/>.</returns>
        public SqlDataReader GetFilms(string processor)
        {
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("GetFilmsForTicketProcessor", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterProcessor = new SqlParameter("@Processor", SqlDbType.VarChar, 10);
            parameterProcessor.Value = processor;
            myCommand.Parameters.Add(parameterProcessor);

            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception e)
            {
                string error = e.Message;
                return null;
            }
        }

        /// <summary>
        /// Gets the print file list.
        /// </summary>
        /// <param name="processor">The processor.</param>
        /// <returns></returns>
        public SqlDataReader GetPrintFileList(string processor)
        {
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("GetPrintFilesForTicketProcessor", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterProcessor = new SqlParameter("@AutTicketProcessor", SqlDbType.VarChar, 10);
            parameterProcessor.Value = processor;
            myCommand.Parameters.Add(parameterProcessor);

            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception e)
            {
                string error = e.Message;
                return null;
            }
        }

        /// <summary>
        /// Gets the auth list by processor and status.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="processor">The processor.</param>
        /// <param name="statusProsecute">The status prosecute.</param>
        /// <param name="statusTickets">The status tickets.</param>
        /// <param name="statusNoticeCreated">The status notice created.</param>
        /// <param name="errMessage">The err message.</param>
        /// <returns></returns>
        public SqlDataReader GetAuthListByProcessorAndStatus(int autIntNo, string processor, int statusProsecute, int statusTickets, int statusNoticeCreated, ref string errMessage)
        {
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("AuthByTicketProcessorFrameStatus", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterProcessor = new SqlParameter("@Processor", SqlDbType.VarChar, 10);
            parameterProcessor.Value = processor;
            myCommand.Parameters.Add(parameterProcessor);

            SqlParameter parameterFrameStatus = new SqlParameter("@StatusProsecute", SqlDbType.Int, 4);
            parameterFrameStatus.Value = statusProsecute;
            myCommand.Parameters.Add(parameterFrameStatus);

            SqlParameter parameterStatusTickets = new SqlParameter("@StatusTickets", SqlDbType.Int, 4);
            parameterStatusTickets.Value = statusTickets;
            myCommand.Parameters.Add(parameterStatusTickets);

            SqlParameter parameterStatusNoticeCreated = new SqlParameter("@StatusNoticeCreated", SqlDbType.Int, 4);
            parameterStatusNoticeCreated.Value = statusNoticeCreated;
            myCommand.Parameters.Add(parameterStatusNoticeCreated);

            //SqlParameter parameterStatusAmountAdded = new SqlParameter("@StatusAmountAdded", SqlDbType.Int, 4);
            //parameterStatusAmountAdded.Value = statusAmountAdded;
            //myCommand.Parameters.Add(parameterStatusAmountAdded);

            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return null;
            }
        }

        /// <summary>
        /// Gets the auth list by processor printing.
        /// </summary>
        /// <param name="processor">The processor.</param>
        /// <param name="statusAlloc">The status alloc.</param>
        /// <param name="statusNoticeIssued">The status notice issued.</param>
        /// <returns></returns>
        public SqlDataReader GetAuthListByProcessorPrinting(string processor, int statusAlloc, int statusNoticeIssued, ref string errMessage)
        {
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("AuthByTicketProcessorPrinting", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterProcessor = new SqlParameter("@Processor", SqlDbType.VarChar, 10);
            parameterProcessor.Value = processor;
            myCommand.Parameters.Add(parameterProcessor);

            SqlParameter parameterStatusAlloc = new SqlParameter("@StatusAlloc", SqlDbType.Int, 4);
            parameterStatusAlloc.Value = statusAlloc;
            myCommand.Parameters.Add(parameterStatusAlloc);

            SqlParameter parameterStatusNoticeIssued = new SqlParameter("@StatusNoticeIssued", SqlDbType.Int, 4);
            parameterStatusNoticeIssued.Value = statusNoticeIssued;
            myCommand.Parameters.Add(parameterStatusNoticeIssued);

            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return null;
            }
        }

        /// <summary>
        /// Gets the auth list for processing notices_ CPI.
        /// </summary>
        /// <param name="processor">The processor.</param>
        /// <param name="statusProsecute">The status prosecute.</param>
        /// <param name="statusLoaded">The status loaded.</param>
        /// <param name="statusNoticeCreated">The status notice created.</param>
        /// <param name="statusNoticePrinted">The status notice printed.</param>
        /// <returns></returns>
        public SqlDataReader GetAuthListForProcessingNotices_CPI(string processor, int statusProsecute,
            int statusLoaded, int statusNoticeCreated, int statusNoticePrinted, ref string errMessage)
        {
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("AuthListForProcessingNotices_CPI", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterProcessor = new SqlParameter("@Processor", SqlDbType.VarChar, 10);
            parameterProcessor.Value = processor;
            myCommand.Parameters.Add(parameterProcessor);

            SqlParameter parameterFrameStatus = new SqlParameter("@StatusProsecute", SqlDbType.Int, 4);
            parameterFrameStatus.Value = statusProsecute;
            myCommand.Parameters.Add(parameterFrameStatus);

            SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
            parameterStatusLoaded.Value = statusLoaded;
            myCommand.Parameters.Add(parameterStatusLoaded);

            SqlParameter parameterStatusNoticeCreated = new SqlParameter("@StatusNoticeCreated", SqlDbType.Int, 4);
            parameterStatusNoticeCreated.Value = statusNoticeCreated;
            myCommand.Parameters.Add(parameterStatusNoticeCreated);

            SqlParameter parameterStatusNoticePrinted = new SqlParameter("@StatusNoticePrinted", SqlDbType.Int, 4);
            parameterStatusNoticePrinted.Value = statusNoticePrinted;
            myCommand.Parameters.Add(parameterStatusNoticePrinted);

            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception e)
            {
                errMessage = e.ToString();
                System.Diagnostics.Debug.WriteLine(false, e.Message);
                return null;
            }
        }

        public SqlDataReader GetAuthListForProcessingNotices(string processor, int statusProsecute,
           int statusLoaded, int statusNoticeCreated, int statusNoticePrinted, ref string errMessage)
        {
            var con = new SqlConnection(this.connectionString);
            var cmd = new SqlCommand("AuthListForProcessingNotices", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 300;

            // Add Parameters to SPROC
            SqlParameter parameterProcessor = new SqlParameter("@Processor", SqlDbType.VarChar, 10);
            parameterProcessor.Value = processor;
            cmd.Parameters.Add(parameterProcessor);

            SqlParameter parameterFrameStatus = new SqlParameter("@StatusProsecute", SqlDbType.Int, 4);
            parameterFrameStatus.Value = statusProsecute;
            cmd.Parameters.Add(parameterFrameStatus);

            SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
            parameterStatusLoaded.Value = statusLoaded;
            cmd.Parameters.Add(parameterStatusLoaded);

            SqlParameter parameterStatusNoticeCreated = new SqlParameter("@StatusNoticeCreated", SqlDbType.Int, 4);
            parameterStatusNoticeCreated.Value = statusNoticeCreated;
            cmd.Parameters.Add(parameterStatusNoticeCreated);

            SqlParameter parameterStatusNoticePrinted = new SqlParameter("@StatusNoticePrinted", SqlDbType.Int, 4);
            parameterStatusNoticePrinted.Value = statusNoticePrinted;
            cmd.Parameters.Add(parameterStatusNoticePrinted);

            try
            {
                con.Open();
                SqlDataReader result = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(false, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the auth list for2nd notices.
        /// </summary>
        /// <param name="status1stNoticePrinted">The status1st notice printed.</param>
        /// <returns>A <see cref="SqlDataReader"/>.</returns>
        //public SqlDataReader GetAuthListFor2ndNotices(int status1stNoticePrinted, ref string errMessage)
        public SqlDataReader GetAuthListFor2ndNotices(int status1stNoticePosted, ref string errMessage)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("AuthListFor2ndNotice", con);
            com.CommandType = CommandType.StoredProcedure;

            //this parameter is confusing, because it is passing in the POSTED date, not thet PRINTED date - renamed
            //com.Parameters.Add("@Status1stNoticePrinted", SqlDbType.Int, 4).Value = status1stNoticePrinted;
            com.Parameters.Add("@Status1stNoticePosted", SqlDbType.Int, 4).Value = status1stNoticePosted;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                System.Diagnostics.Debug.Assert(false, e.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the list of authorities ho have summons to process.
        /// </summary>
        /// <returns>A <see cref="T:SqlDataReader"/></returns>
        public SqlDataReader GetAuthListForSummons(string processor, int autIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("AuthListForSummons", con);
            com.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterProcessor = new SqlParameter("@Processor", SqlDbType.VarChar, 10);
            parameterProcessor.Value = processor;
            com.Parameters.Add(parameterProcessor);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            com.Parameters.Add(parameterAutIntNo);

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Assert(false, e.Message);
                return null;
            }
        }

        /// <summary>
        /// BD - Gets the list of authorities
        /// </summary>
        /// <returns>A <see cref="SqlDataReader"/></returns>
        public SqlDataReader GetAuthList()
        {
            return this.GetAuthList(string.Empty);
        }


        /// <summary>
        /// Gets the auth list.
        /// </summary>
        /// <param name="ticketProcessor">The ticket processor.</param>
        /// <returns></returns>
        public SqlDataReader GetAuthList(string ticketProcessor)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("AuthorityList", con);
            com.Parameters.Add("@MtrIntNo", SqlDbType.Int, 4).Value = 0;
            com.Parameters.Add("@OrderBy", SqlDbType.VarChar, 10).Value = "AutCode";
            com.Parameters.Add("@Processor", SqlDbType.VarChar, 10).Value = ticketProcessor;
            com.CommandType = CommandType.StoredProcedure;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Assert(false, e.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the type of the next tran no by.
        /// </summary>
        /// <param name="tnType">Type of the tn.</param>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="lastUser">The last user.</param>
        /// <param name="maxNo">The max no.</param>
        /// <returns></returns>
        public int GetNextTranNoByType(string tnType, int autIntNo, string lastUser, int maxNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("TranNoNextNumberByType", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterTNType = new SqlParameter("@TNType", SqlDbType.VarChar, 3);
            parameterTNType.Value = tnType;
            myCommand.Parameters.Add(parameterTNType);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterMaxNo = new SqlParameter("@MaxNo", SqlDbType.Int);
            parameterMaxNo.Value = maxNo;
            myCommand.Parameters.Add(parameterMaxNo);

            SqlParameter parameterTNumber = new SqlParameter("@TNumber", SqlDbType.Int);
            parameterTNumber.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterTNumber);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int tNumber = Convert.ToInt32(parameterTNumber.Value);

                return tNumber;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// Updates the tran number.
        /// </summary>
        /// <param name="tNumber">The t number.</param>
        /// <param name="lastUser">The last user.</param>
        /// <param name="tnType">Type of the tn.</param>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="tnIntNo">The tn int no.</param>
        /// <returns></returns>
        public int UpdateTranNumber(int tNumber, string lastUser, string tnType, int autIntNo, int tnIntNo)
        {
            //@TNumber int, 
            //@LastUser varchar(50),
            //@TNType char(3),
            //@AutIntNo int, 
            //@TNIntNo int OUTPUT 
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("TranNoUpdateNumber", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterTNumber = new SqlParameter("@TNumber", SqlDbType.Int);
            parameterTNumber.Value = tNumber;
            myCommand.Parameters.Add(parameterTNumber);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterTNType = new SqlParameter("@TNType", SqlDbType.Char, 3);
            parameterTNType.Value = tnType;
            myCommand.Parameters.Add(parameterTNType);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterTNIntNo = new SqlParameter("@TNIntNo", SqlDbType.Int, 4);
            parameterTNIntNo.Value = tnIntNo;
            parameterTNIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterTNIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int TNIntNo = (int)myCommand.Parameters["@TNIntNo"].Value;

                return TNIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// Gets the film int no from frame.
        /// </summary>
        /// <param name="frameIntNo">The frame int no.</param>
        /// <returns></returns>
        public int GetFilmIntNoFromFrame(int frameIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("FrameGetFilmIntNo", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int);
            parameterFrameIntNo.Value = frameIntNo;
            myCommand.Parameters.Add(parameterFrameIntNo);

            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int);
            parameterFilmIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterFilmIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                int filmIntNo = Convert.ToInt32(parameterFilmIntNo.Value);
                return filmIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return -5;
            }
        }

        /// <summary>
        /// Gets the camera from frame.
        /// </summary>
        /// <param name="filmIntNo">The film int no.</param>
        /// <returns></returns>
        // 2013-07-19 comment by Henry for useless
        //public string GetCameraFromFrame(int filmIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(this.connectionString);
        //    SqlCommand myCommand = new SqlCommand("FilmGetCamera", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int);
        //    parameterFilmIntNo.Value = filmIntNo;
        //    myCommand.Parameters.Add(parameterFilmIntNo);

        //    SqlParameter parameterCamera = new SqlParameter("@CameraID", SqlDbType.VarChar, 3);
        //    parameterCamera.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterCamera);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        string camera = Convert.ToString(parameterCamera.Value);

        //        return camera;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return null;
        //    }
        //}

        /// <summary>
        /// Gets the aut int from authority from aut no.
        /// </summary>
        /// <param name="autNo">The aut no.</param>
        /// <returns></returns>
        public int GetAutIntFromAuthorityFromAutNo(string autNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("AuthorityGetAutIntNo", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC

            SqlParameter parameterAutNo = new SqlParameter("@AutNo", SqlDbType.VarChar, 6);
            parameterAutNo.Value = autNo;
            myCommand.Parameters.Add(parameterAutNo);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterAutIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int autIntNo = Convert.ToInt32(parameterAutIntNo.Value);

                return autIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return -99;
            }
        }

        /// <summary>
        /// Updates the frame with response.
        /// </summary>
        /// <param name="responseDate">The response date.</param>
        /// <param name="statusCode">The status code.</param>
        /// <param name="statusDescr">The status descr.</param>
        /// <param name="lastUser">The last user.</param>
        /// <param name="frameIntNo">The frame int no.</param>
        /// <returns></returns>
        public int UpdateFrameWithResponse(DateTime responseDate, int statusCode, string statusDescr, string lastUser, int frameIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("FrameUpdateCiprusResponse", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterResponseDate = new SqlParameter("@ResponseDate", SqlDbType.SmallDateTime);
            parameterResponseDate.Value = responseDate;
            myCommand.Parameters.Add(parameterResponseDate);

            SqlParameter parameterStatusCode = new SqlParameter("@StatusCode", SqlDbType.SmallInt);
            parameterStatusCode.Value = statusCode;
            myCommand.Parameters.Add(parameterStatusCode);

            SqlParameter parameterStatusDescr = new SqlParameter("@StatusDescr", SqlDbType.VarChar, 100);
            parameterStatusDescr.Value = statusDescr;
            myCommand.Parameters.Add(parameterStatusDescr);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Value = frameIntNo;
            parameterFrameIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterFrameIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                frameIntNo = (int)myCommand.Parameters["@FrameIntNo"].Value;

                return frameIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return -1;
            }
        }

        /// <summary>
        /// Updates the film with response.
        /// </summary>
        /// <param name="responseDate">The response date.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="filmStatus">The film status.</param>
        /// <param name="lastUser">The last user.</param>
        /// <param name="filmIntNo">The film int no.</param>
        /// <returns></returns>
        public int UpdateFilmWithResponse(DateTime responseDate, string fileName, int filmStatus, string lastUser, int filmIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("FilmUpdateCiprusResponse", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterResponseDate = new SqlParameter("@ResponseDate", SqlDbType.SmallDateTime);
            parameterResponseDate.Value = responseDate;
            myCommand.Parameters.Add(parameterResponseDate);

            SqlParameter parameterFileName = new SqlParameter("@FileName", SqlDbType.VarChar, 50);
            parameterFileName.Value = fileName;
            myCommand.Parameters.Add(parameterFileName);

            SqlParameter parameterFilmStatus = new SqlParameter("@FilmStatus", SqlDbType.SmallInt);
            parameterFilmStatus.Value = filmStatus;
            myCommand.Parameters.Add(parameterFilmStatus);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Value = filmIntNo;
            parameterFilmIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterFilmIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                filmIntNo = (int)myCommand.Parameters["@FilmIntNo"].Value;

                return filmIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return -1;
            }
        }

        /// <summary>
        /// Updates the film with create TPE.
        /// </summary>
        /// <param name="lastUser">The last user.</param>
        /// <param name="filmIntNo">The film int no.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public int UpdateFilmWithCreateTPE(string lastUser, int filmIntNo, string fileName)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("FilmUpdateCreateTPE", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFileName = new SqlParameter("@FileName", SqlDbType.VarChar, 50);
            parameterFileName.Value = fileName;
            myCommand.Parameters.Add(parameterFileName);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Value = filmIntNo;
            parameterFilmIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterFilmIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                filmIntNo = (int)myCommand.Parameters["@FilmIntNo"].Value;

                return filmIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return -1;
            }
        }

        /// <summary>
        /// Updates the notice with print confirm sent to ticket processor.
        /// </summary>
        /// <param name="lastUser">The last user.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public string UpdateNoticeWithPrintConfirmSentToTicketProcessor(string lastUser, string fileName)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("NoticeUpdatePrintConfirmSend", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterFileName = new SqlParameter("@FileName", SqlDbType.VarChar, 50);
            parameterFileName.Value = fileName;
            parameterFileName.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterFileName);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                fileName = myCommand.Parameters["@FileName"].Value.ToString();

                return fileName;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return "bad";
            }
        }

        //public int UpdateCameraFromCiprus(int autIntNo, string camID, string camSerialNo, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(this.connectionString);
        //    SqlCommand myCommand = new SqlCommand("CameraInsertUpdateFromCiprus", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterCamID = new SqlParameter("@CamID", SqlDbType.VarChar, 5);
        //    parameterCamID.Value = camID;
        //    myCommand.Parameters.Add(parameterCamID);

        //    SqlParameter parameterCamSerialNo = new SqlParameter("@CamSerialNo", SqlDbType.VarChar, 18);
        //    parameterCamSerialNo.Value = camSerialNo;
        //    myCommand.Parameters.Add(parameterCamSerialNo);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterCamIntNo = new SqlParameter("@CamIntNo", SqlDbType.Int);
        //    parameterCamIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterCamIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int camIntNo = Convert.ToInt32(parameterCamIntNo.Value);

        //        return camIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return -99;
        //    }
        //}
        /*
        /// <summary>
        /// Updates the location from ciprus.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="locCameraCode">The loc camera code.</param>
        /// <param name="locCode">The loc code.</param>
        /// <param name="locStreetCode">The loc street code.</param>
        /// <param name="locStreetName">Name of the loc street.</param>
        /// <param name="locDescr">The loc descr.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        public int UpdateLocationFromCiprus(int autIntNo, string locCameraCode, string locCode, string locStreetCode, string locStreetName, string locDescr, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("LocationInsertUpdateFromCiprus", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterLocCameraCode = new SqlParameter("@LocCameraCode", SqlDbType.VarChar, 12);
            parameterLocCameraCode.Value = locCameraCode;
            myCommand.Parameters.Add(parameterLocCameraCode);

            SqlParameter parameterLocCode = new SqlParameter("@LocCode", SqlDbType.VarChar, 21);
            parameterLocCode.Value = locCode;
            myCommand.Parameters.Add(parameterLocCode);

            SqlParameter parameterLocStreetCode = new SqlParameter("@LocStreetCode", SqlDbType.VarChar, 5);
            parameterLocStreetCode.Value = locStreetCode;
            myCommand.Parameters.Add(parameterLocStreetCode);

            SqlParameter parameterLocStreetName = new SqlParameter("@LocStreetName", SqlDbType.VarChar, 30);
            parameterLocStreetName.Value = locStreetName;
            myCommand.Parameters.Add(parameterLocStreetName);

            SqlParameter parameterLocDescr = new SqlParameter("@LocDescr", SqlDbType.VarChar, 60);
            parameterLocDescr.Value = locDescr;
            myCommand.Parameters.Add(parameterLocDescr);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterLocIntNo = new SqlParameter("@LocIntNo", SqlDbType.Int);
            parameterLocIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterLocIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int locIntNo = Convert.ToInt32(parameterLocIntNo.Value);

                return locIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return -99;
            }
        }
        */
        /// <summary>
        /// Updates the traffic officer from ciprus.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="toNo">To no.</param>
        /// <param name="toSName">Name of to S.</param>
        /// <param name="toInit">To init.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        public int UpdateTrafficOfficerFromCiprus(int autIntNo, string toNo, string toSName, string toInit, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("TrafficOfficerInsertUpdateFromCiprus", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterTONo = new SqlParameter("@TONo", SqlDbType.VarChar, 10);
            parameterTONo.Value = toNo;
            myCommand.Parameters.Add(parameterTONo);

            SqlParameter parameterTOSName = new SqlParameter("@TOSName", SqlDbType.VarChar, 21);
            parameterTOSName.Value = toSName;
            myCommand.Parameters.Add(parameterTOSName);

            SqlParameter parameterTOInit = new SqlParameter("@TOInit", SqlDbType.VarChar, 5);
            parameterTOInit.Value = toInit;
            myCommand.Parameters.Add(parameterTOInit);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterTOIntNo = new SqlParameter("@TOIntNo", SqlDbType.Int);
            parameterTOIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterTOIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int toIntNo = Convert.ToInt32(parameterTOIntNo.Value);

                return toIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return -99;
            }
        }

        /// <summary>
        /// Updates the court from ciprus.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="crtNo">The CRT no.</param>
        /// <param name="crtName">Name of the CRT.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        // 2013-07-19 comment by Henry for useless
        //public int UpdateCourtFromCiprus(int autIntNo, string crtNo, string crtName, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(this.connectionString);
        //    SqlCommand myCommand = new SqlCommand("CourtInsertUpdateFromCiprus", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterCrtNo = new SqlParameter("@CrtNo", SqlDbType.VarChar, 6);
        //    parameterCrtNo.Value = crtNo;
        //    myCommand.Parameters.Add(parameterCrtNo);

        //    SqlParameter parameterCrtName = new SqlParameter("@CrtName", SqlDbType.VarChar, 30);
        //    parameterCrtName.Value = crtName;
        //    myCommand.Parameters.Add(parameterCrtName);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int);
        //    parameterCrtIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterCrtIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int crtIntNo = Convert.ToInt32(parameterCrtIntNo.Value);

        //        return crtIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return -99;
        //    }
        //}

        /// <summary>
        /// Updates the offence from ciprus.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="offStatutoryRef">The off statutory ref.</param>
        /// <param name="offDescr">The off descr.</param>
        /// <param name="offData">The off data.</param>
        /// <param name="offCode">The off code.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        // 2013-07-19 comment by Henry for useless
        //public int UpdateOffenceFromCiprus(int autIntNo, string offStatutoryRef, string offDescr, string offData, string offCode, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(this.connectionString);
        //    SqlCommand myCommand = new SqlCommand("OffenceInsertUpdateFromCiprus", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterStatutoryRef = new SqlParameter("@OffStatutoryRef", SqlDbType.VarChar, 255);
        //    parameterStatutoryRef.Value = offStatutoryRef;
        //    myCommand.Parameters.Add(parameterStatutoryRef);

        //    SqlParameter parameterDescription = new SqlParameter("@OffDescr", SqlDbType.VarChar, 255);
        //    parameterDescription.Value = offDescr;
        //    myCommand.Parameters.Add(parameterDescription);

        //    SqlParameter parameterData = new SqlParameter("@OffData", SqlDbType.VarChar, 3);
        //    parameterData.Value = offData;
        //    myCommand.Parameters.Add(parameterData);

        //    SqlParameter parameterOffCode = new SqlParameter("@OffCode", SqlDbType.VarChar, 6);
        //    parameterOffCode.Value = offCode;
        //    myCommand.Parameters.Add(parameterOffCode);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterOffIntNo = new SqlParameter("@OffIntNo", SqlDbType.Int);
        //    parameterOffIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterOffIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int offIntNo = Convert.ToInt32(parameterOffIntNo.Value);

        //        return offIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return -99;
        //    }
        //}

        /// <summary>
        /// Updates the vehicle make from ciprus.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="vmCode">The vm code.</param>
        /// <param name="vmDescr">The vm descr.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        public int UpdateVehicleMakeFromCiprus(int autIntNo, string vmCode, string vmDescr, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("VehicleMakeInsertUpdateFromCiprus", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterVMCode = new SqlParameter("@VMCode", SqlDbType.VarChar, 3);
            parameterVMCode.Value = vmCode;
            myCommand.Parameters.Add(parameterVMCode);

            SqlParameter parameterVMDescr = new SqlParameter("@VMDescr", SqlDbType.VarChar, 30);
            parameterVMDescr.Value = vmDescr;
            myCommand.Parameters.Add(parameterVMDescr);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterVMIntNo = new SqlParameter("@VMIntNo", SqlDbType.Int);
            parameterVMIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterVMIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int vmIntNo = Convert.ToInt32(parameterVMIntNo.Value);

                return vmIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return -99;
            }
        }

        /// <summary>
        /// Updates the vehicle type from ciprus.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="vtCode">The vt code.</param>
        /// <param name="vtDescr">The vt descr.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        public int UpdateVehicleTypeFromCiprus(int autIntNo, string vtCode, string vtDescr, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("VehicleTypeInsertUpdateFromCiprus", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterVTCode = new SqlParameter("@VTCode", SqlDbType.VarChar, 3);
            parameterVTCode.Value = vtCode;
            myCommand.Parameters.Add(parameterVTCode);

            SqlParameter parameterVTDescr = new SqlParameter("@VTDescr", SqlDbType.VarChar, 30);
            parameterVTDescr.Value = vtDescr;
            myCommand.Parameters.Add(parameterVTDescr);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterVTIntNo = new SqlParameter("@VTIntNo", SqlDbType.Int);
            parameterVTIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterVTIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int vtIntNo = Convert.ToInt32(parameterVTIntNo.Value);

                return vtIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return -99;
            }
        }


        /// <summary>
        /// Adds the notice charge from frame_ CPI.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="statusLoaded">The status loaded.</param>
        /// <param name="statusExpired">The status expired.</param>
        /// <param name="statusNotice">The status notice.</param>
        /// <param name="cType">Type of the c.</param>
        /// <param name="lastUser">The last user.</param>
        /// <param name="noOfRecords">The no of records.</param>
        /// <param name="errorMsg">The error MSG.</param>
        /// <returns></returns>
        //public int AddNoticeChargeFromFrame_CPI(int autIntNo, int statusLoaded, int statusExpired, int statusNotice,
        //    string cType, string lastUser, ref int noOfRecords, ref string errorMsg, List<DuplicateNotice> noticeAlready)
        //{

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(this.connectionString);
        //    SqlCommand myCommand = new SqlCommand("NoticeChargeAddFromFrame_CPI", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;
        //    myCommand.CommandTimeout = 0;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
        //    parameterStatusLoaded.Value = statusLoaded;
        //    myCommand.Parameters.Add(parameterStatusLoaded);

        //    SqlParameter parameterStatusExpired = new SqlParameter("@StatusExpired", SqlDbType.Int, 4);
        //    parameterStatusExpired.Value = statusExpired;
        //    myCommand.Parameters.Add(parameterStatusExpired);

        //    SqlParameter parameterStatusNotice = new SqlParameter("@StatusNotice", SqlDbType.Int, 4);
        //    parameterStatusNotice.Value = statusNotice;
        //    myCommand.Parameters.Add(parameterStatusNotice);

        //    SqlParameter parameterCType = new SqlParameter("@CType", SqlDbType.VarChar, 15);
        //    parameterCType.Value = cType;
        //    myCommand.Parameters.Add(parameterCType);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int);
        //    parameterSuccess.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterSuccess);

        //    SqlParameter parameterNoOfRecords = new SqlParameter("@NoOfRecords", SqlDbType.Int);
        //    parameterNoOfRecords.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterNoOfRecords);

        //    try
        //    {
        //        myConnection.Open();
        //        SqlDataReader reader = myCommand.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            DuplicateNotice notice = new DuplicateNotice();
        //            notice.FilmNumber = (string)reader["FilmNo"];
        //            notice.FrameIntNo = (int)reader["FrameIntNo"];
        //            notice.FrameNumber = (string)reader["FrameNo"];
        //            notice.NotIntNo = (int)reader["NotIntNo"];
        //            if (reader["NotTicketNo"] != DBNull.Value)
        //                notice.TicketNumber = (string)reader["NotTicketNo"];

        //            noticeAlready.Add(notice);
        //        }
        //        reader.Close();

        //        noOfRecords = Convert.ToInt32(parameterNoOfRecords.Value);
        //        int success = Convert.ToInt32(parameterSuccess.Value);
        //        return success;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        errorMsg = e.Message;
        //        return -1;
        //    }
        //    finally
        //    {
        //        myConnection.Dispose();
        //    }
        //}


        // 2013-07-26 comment out by Henry for useless
        //public int AddNoticeChargeFromFrame_CA(int autIntNo, int statusLoaded, int statusExpired, int statusNotice,
        //   string cType, string lastUser, ref int noOfRecords, ref string errorMsg, List<DuplicateNotice> noticeAlready, string natisLast,
        //   string violationCutOff, int noOfDaysIssue, int noOfDaysPayment)
        //{

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(this.connectionString);
        //    SqlCommand myCommand = new SqlCommand("NoticeChargeAddFromFrame_CA", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;
        //    myCommand.CommandTimeout = 0;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
        //    parameterStatusLoaded.Value = statusLoaded;
        //    myCommand.Parameters.Add(parameterStatusLoaded);

        //    SqlParameter parameterStatusExpired = new SqlParameter("@StatusExpired", SqlDbType.Int, 4);
        //    parameterStatusExpired.Value = statusExpired;
        //    myCommand.Parameters.Add(parameterStatusExpired);

        //    SqlParameter parameterStatusNotice = new SqlParameter("@StatusNotice", SqlDbType.Int, 4);
        //    parameterStatusNotice.Value = statusNotice;
        //    myCommand.Parameters.Add(parameterStatusNotice);

        //    SqlParameter parameterCType = new SqlParameter("@CType", SqlDbType.VarChar, 15);
        //    parameterCType.Value = cType;
        //    myCommand.Parameters.Add(parameterCType);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int);
        //    parameterSuccess.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterSuccess);

        //    SqlParameter parameterNoOfRecords = new SqlParameter("@NoOfRecords", SqlDbType.Int);
        //    parameterNoOfRecords.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterNoOfRecords);

        //    SqlParameter parameterNatisLast = new SqlParameter("@NatisLast", SqlDbType.Char, 1);
        //    parameterNatisLast.Value = natisLast;
        //    myCommand.Parameters.Add(parameterNatisLast);

        //    myCommand.Parameters.Add("@ViolationCutOff", SqlDbType.Char, 1).Value = violationCutOff;
        //    myCommand.Parameters.Add("@NoOfDaysIssue", SqlDbType.Int, 4).Value = noOfDaysIssue;
        //    myCommand.Parameters.Add("@NoOfDaysPayment", SqlDbType.Int, 4).Value = noOfDaysPayment;

        //    try
        //    {
        //        myConnection.Open();
        //        SqlDataReader reader = myCommand.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            DuplicateNotice notice = new DuplicateNotice();
        //            notice.FilmNumber = (string)reader["FilmNo"];
        //            notice.FrameIntNo = (int)reader["FrameIntNo"];
        //            notice.FrameNumber = (string)reader["FrameNo"];
        //            notice.NotIntNo = (int)reader["NotIntNo"];
        //            if (reader["NotTicketNo"] != DBNull.Value)
        //                notice.TicketNumber = (string)reader["NotTicketNo"];

        //            noticeAlready.Add(notice);
        //        }
        //        reader.Close();

        //        noOfRecords = Convert.ToInt32(parameterNoOfRecords.Value);
        //        int success = Convert.ToInt32(parameterSuccess.Value);
        //        return success;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        errorMsg = e.Message;
        //        return -1;
        //    }
        //    finally
        //    {
        //        myConnection.Dispose();
        //    }
        //}

        public int AddNoticeChargeFromFrame_CC(int autIntNo, int statusLoaded, int statusExpired, int statusNotice,
            string cType, string lastUser, ref int noOfRecords, ref string errorMsg, List<DuplicateNotice> noticeAlready, string natisLast,
            string violationCutOff, int noOfDaysIssue, int noOfDaysPayment)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("NoticeChargeAddFromFrame_CC", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            myCommand.CommandTimeout = 0;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
            parameterStatusLoaded.Value = statusLoaded;
            myCommand.Parameters.Add(parameterStatusLoaded);

            SqlParameter parameterStatusExpired = new SqlParameter("@StatusExpired", SqlDbType.Int, 4);
            parameterStatusExpired.Value = statusExpired;
            myCommand.Parameters.Add(parameterStatusExpired);

            SqlParameter parameterStatusNotice = new SqlParameter("@StatusNotice", SqlDbType.Int, 4);
            parameterStatusNotice.Value = statusNotice;
            myCommand.Parameters.Add(parameterStatusNotice);

            SqlParameter parameterCType = new SqlParameter("@CType", SqlDbType.VarChar, 15);
            parameterCType.Value = cType;
            myCommand.Parameters.Add(parameterCType);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int);
            parameterSuccess.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterSuccess);

            SqlParameter parameterNoOfRecords = new SqlParameter("@NoOfRecords", SqlDbType.Int);
            parameterNoOfRecords.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterNoOfRecords);

            SqlParameter parameterNatisLast = new SqlParameter("@NatisLast", SqlDbType.Char, 1);
            parameterNatisLast.Value = natisLast;
            myCommand.Parameters.Add(parameterNatisLast);

            myCommand.Parameters.Add("@ViolationCutOff", SqlDbType.Char, 1).Value = violationCutOff;
            myCommand.Parameters.Add("@NoOfDaysIssue", SqlDbType.Int, 4).Value = noOfDaysIssue;
            myCommand.Parameters.Add("@NoOfDaysPayment", SqlDbType.Int, 4).Value = noOfDaysPayment;

            try
            {
                myConnection.Open();
                SqlDataReader reader = myCommand.ExecuteReader();
                while (reader.Read())
                {
                    DuplicateNotice notice = new DuplicateNotice();
                    notice.FilmNumber = (string)reader["FilmNo"];
                    notice.FrameIntNo = (int)reader["FrameIntNo"];
                    notice.FrameNumber = (string)reader["FrameNo"];
                    notice.NotIntNo = (int)reader["NotIntNo"];
                    if (reader["NotTicketNo"] != DBNull.Value)
                        notice.TicketNumber = (string)reader["NotTicketNo"];

                    noticeAlready.Add(notice);
                }
                reader.Close();

                noOfRecords = Convert.ToInt32(parameterNoOfRecords.Value);
                int success = Convert.ToInt32(parameterSuccess.Value);
                return success;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                errorMsg = e.Message;
                return -1;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        /// <summary>
        /// Adds the notice charge from frame.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="statusLoaded">The status loaded.</param>
        /// <param name="statusNoFine">The status no fine.</param>
        /// <param name="statusExpired">The status expired.</param>
        /// <param name="statusNotice">The status notice.</param>
        /// <param name="cType">Type of the c.</param>
        /// <param name="lastUser">The last user.</param>
        /// <param name="printFileName">Name of the print file.</param>
        /// <param name="noOfRecords">The no of records.</param>
        /// <param name="errorMsg">The error MSG.</param>
        /// <param name="statusAmountAdded">The status amount added.</param>
        /// <param name="noOfFrames">The no of frames.</param>
        /// <param name="natisLast">The natis last.</param>
        /// <param name="duplicates">The duplicates.</param>
        /// <returns></returns>
        // 2013-07-26 comment out by Henry for useless
        //public int AddNoticeChargeFromFrame(int autIntNo, int statusLoaded, int statusNoFine, int statusExpired, int statusNotice,
        //     string cType, string lastUser, string printFileName, ref int noOfRecords, ref string errorMsg, int statusAmountAdded,
        //     ref int noOfFrames, string natisLast, List<DuplicateNotice> duplicates,
        //    string violationCutOff, int noOfDaysIssue, int noOfDaysPayment)
        //{

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(this.connectionString);
        //    SqlCommand myCommand = new SqlCommand("NoticeChargeAddFromFrame", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;
        //    myCommand.CommandTimeout = 0;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
        //    parameterStatusLoaded.Value = statusLoaded;
        //    myCommand.Parameters.Add(parameterStatusLoaded);

        //    SqlParameter parameterStatusNoFine = new SqlParameter("@StatusNoFine", SqlDbType.Int, 4);
        //    parameterStatusNoFine.Value = statusNoFine;
        //    myCommand.Parameters.Add(parameterStatusNoFine);

        //    SqlParameter parameterStatusExpired = new SqlParameter("@StatusExpired", SqlDbType.Int, 4);
        //    parameterStatusExpired.Value = statusExpired;
        //    myCommand.Parameters.Add(parameterStatusExpired);

        //    SqlParameter parameterStatusAmountAdded = new SqlParameter("@StatusAmountAdded", SqlDbType.Int, 4);
        //    parameterStatusAmountAdded.Value = statusAmountAdded;
        //    myCommand.Parameters.Add(parameterStatusAmountAdded);

        //    SqlParameter parameterStatusNotice = new SqlParameter("@StatusNotice", SqlDbType.Int, 4);
        //    parameterStatusNotice.Value = statusNotice;
        //    myCommand.Parameters.Add(parameterStatusNotice);

        //    SqlParameter parameterCType = new SqlParameter("@CType", SqlDbType.VarChar, 15);
        //    parameterCType.Value = cType;
        //    myCommand.Parameters.Add(parameterCType);

        //    SqlParameter parameterPrintFileName = new SqlParameter("@PrintFileName", SqlDbType.VarChar, 50);
        //    parameterPrintFileName.Value = printFileName;
        //    myCommand.Parameters.Add(parameterPrintFileName);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int);
        //    parameterSuccess.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterSuccess);

        //    SqlParameter parameterNoOfRecords = new SqlParameter("@NoOfRecords", SqlDbType.Int);
        //    parameterNoOfRecords.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterNoOfRecords);

        //    SqlParameter parameterNoOfFrames = new SqlParameter("@NoOfFrames", SqlDbType.Int);
        //    parameterNoOfFrames.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterNoOfFrames);

        //    SqlParameter parameterNatisLast = new SqlParameter("@NatisLast", SqlDbType.Char, 1);
        //    parameterNatisLast.Value = natisLast;
        //    myCommand.Parameters.Add(parameterNatisLast);

        //    myCommand.Parameters.Add("@ViolationCutOff", SqlDbType.Char, 1).Value = violationCutOff;
        //    myCommand.Parameters.Add("@NoOfDaysIssue", SqlDbType.Int, 4).Value = noOfDaysIssue;
        //    myCommand.Parameters.Add("@NoOfDaysPayment", SqlDbType.Int, 4).Value = noOfDaysPayment;

        //    //SqlParameter parameterErrorMsg = new SqlParameter("@ErrorMsg", SqlDbType.VarChar, 255);
        //    //parameterErrorMsg.Direction = ParameterDirection.Output;
        //    //myCommand.Parameters.Add(parameterErrorMsg);

        //    try
        //    {
        //        myConnection.Open();
        //        SqlDataReader reader = myCommand.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            DuplicateNotice notice = new DuplicateNotice();
        //            notice.FilmNumber = (string)reader["FilmNo"];
        //            notice.FrameIntNo = (int)reader["FrameIntNo"];
        //            notice.FrameNumber = (string)reader["FrameNo"];
        //            notice.NotIntNo = (int)reader["NotIntNo"];
        //            notice.TicketNumber = reader["NotTicketNo"] != System.DBNull.Value ? (string)reader["NotTicketNo"] : string.Empty;

        //            duplicates.Add(notice);
        //        }
        //        reader.Close();

        //        noOfRecords = Convert.ToInt32(parameterNoOfRecords.Value);
        //        noOfFrames = Convert.ToInt32(parameterNoOfFrames.Value);
        //        int success = Convert.ToInt32(parameterSuccess.Value);
        //        return success;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        errorMsg = e.Message;
        //        return -1;
        //    }
        //    finally
        //    {
        //        myConnection.Dispose();
        //    }
        //}
       

        public int AddNoticeChargeFromFrame_JA(int autIntNo, int statusLoaded, int statusExpired, int statusNotice,
             string cType, string lastUser, string printFileName, ref int noOfRecords, ref string errorMsg,
             ref int noOfFrames, string natisLast, List<DuplicateNotice> duplicates)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("NoticeChargeAddFromFrame_JA", myConnection);

            //BD - connection time out
            myCommand.CommandTimeout = 60;

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
            parameterStatusLoaded.Value = statusLoaded;
            myCommand.Parameters.Add(parameterStatusLoaded);

            //SqlParameter parameterStatusNoFine = new SqlParameter("@StatusNoFine", SqlDbType.Int, 4);
            //parameterStatusNoFine.Value = statusNoFine;
            //myCommand.Parameters.Add(parameterStatusNoFine);

            SqlParameter parameterStatusExpired = new SqlParameter("@StatusExpired", SqlDbType.Int, 4);
            parameterStatusExpired.Value = statusExpired;
            myCommand.Parameters.Add(parameterStatusExpired);

            //SqlParameter parameterStatusAmountAdded = new SqlParameter("@StatusAmountAdded", SqlDbType.Int, 4);
            //parameterStatusAmountAdded.Value = statusAmountAdded;
            //myCommand.Parameters.Add(parameterStatusAmountAdded);

            SqlParameter parameterStatusNotice = new SqlParameter("@StatusNotice", SqlDbType.Int, 4);
            parameterStatusNotice.Value = statusNotice;
            myCommand.Parameters.Add(parameterStatusNotice);

            SqlParameter parameterCType = new SqlParameter("@CType", SqlDbType.VarChar, 15);
            parameterCType.Value = cType;
            myCommand.Parameters.Add(parameterCType);

            SqlParameter parameterPrintFileName = new SqlParameter("@PrintFileName", SqlDbType.VarChar, 50);
            parameterPrintFileName.Value = printFileName;
            myCommand.Parameters.Add(parameterPrintFileName);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int);
            parameterSuccess.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterSuccess);

            SqlParameter parameterNoOfRecords = new SqlParameter("@NoOfRecords", SqlDbType.Int);
            parameterNoOfRecords.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterNoOfRecords);

            SqlParameter parameterNoOfFrames = new SqlParameter("@NoOfFrames", SqlDbType.Int);
            parameterNoOfFrames.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterNoOfFrames);

            SqlParameter parameterNatisLast = new SqlParameter("@NatisLast", SqlDbType.Char, 1);
            parameterNatisLast.Value = natisLast;
            myCommand.Parameters.Add(parameterNatisLast);


            //SqlParameter parameterErrorMsg = new SqlParameter("@ErrorMsg", SqlDbType.VarChar, 255);
            //parameterErrorMsg.Direction = ParameterDirection.Output;
            //myCommand.Parameters.Add(parameterErrorMsg);

            try
            {
                myConnection.Open();
                SqlDataReader reader = myCommand.ExecuteReader();
                while (reader.Read())
                {
                    DuplicateNotice notice = new DuplicateNotice();
                    notice.FilmNumber = (string)reader["FilmNo"];
                    notice.FrameIntNo = (int)reader["FrameIntNo"];
                    notice.FrameNumber = (string)reader["FrameNo"];
                    notice.NotIntNo = (int)reader["NotIntNo"];
                    notice.TicketNumber = (string)reader["NotTicketNo"];

                    duplicates.Add(notice);
                }
                reader.Close();

                noOfRecords = Convert.ToInt32(parameterNoOfRecords.Value);
                noOfFrames = Convert.ToInt32(parameterNoOfFrames.Value);
                int success = Convert.ToInt32(parameterSuccess.Value);
                return success;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                errorMsg = e.Message;
                return -1;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        /// <summary>
        /// Issues the notice_ CPI.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="statusAlloc">The status alloc.</param>
        /// <param name="statusIssued">The status issued.</param>
        /// <param name="statusExpired">The status expired.</param>
        /// <param name="lastUser">The last user.</param>
        /// <param name="printFileName">Name of the print file.</param>
        /// <param name="noOfRecords">The no of records.</param>
        /// <param name="errorMsg">The error MSG.</param>
        /// <param name="noOfNotices">The no of notices.</param>
        /// <returns></returns>
        // 2013-07-19 comment by Henry for useless
        //public int IssueNotice_CPI(int autIntNo, int statusAlloc, int statusIssued, int statusExpired,
        //    string lastUser, string printFileName, ref int noOfRecords, ref string errorMsg, ref int noOfNotices)
        //{

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(this.connectionString);
        //    SqlCommand myCommand = new SqlCommand("NoticeChargeIssue_CPI", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterStatusAlloc = new SqlParameter("@StatusAlloc", SqlDbType.Int, 4);
        //    parameterStatusAlloc.Value = statusAlloc;
        //    myCommand.Parameters.Add(parameterStatusAlloc);

        //    SqlParameter parameterStatusIssued = new SqlParameter("@StatusIssued", SqlDbType.Int, 4);
        //    parameterStatusIssued.Value = statusIssued;
        //    myCommand.Parameters.Add(parameterStatusIssued);

        //    SqlParameter parameterStatusExpired = new SqlParameter("@StatusExpired", SqlDbType.Int, 4);
        //    parameterStatusExpired.Value = statusExpired;
        //    myCommand.Parameters.Add(parameterStatusExpired);

        //    SqlParameter parameterPrintFileName = new SqlParameter("@PrintFileName", SqlDbType.VarChar, 50);
        //    parameterPrintFileName.Value = printFileName;
        //    myCommand.Parameters.Add(parameterPrintFileName);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterNoOfRecords = new SqlParameter("@NoOfRecords", SqlDbType.Int);
        //    parameterNoOfRecords.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterNoOfRecords);

        //    SqlParameter parameterNoOfNotices = new SqlParameter("@NoOfNotices", SqlDbType.Int);
        //    parameterNoOfNotices.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterNoOfNotices);

        //    SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int);
        //    parameterSuccess.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterSuccess);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int success = Convert.ToInt32(parameterSuccess.Value);

        //        noOfRecords = Convert.ToInt32(parameterNoOfRecords.Value);
        //        noOfNotices = Convert.ToInt32(parameterNoOfNotices.Value);
        //        return success;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        errorMsg = e.Message;
        //        return -2;
        //    }
        //}

        /// 2013-07-26 comment out by Henry for useless
        //dls 090123 - added for JMPD
        //public int IssueNotice_CA(int autIntNo, int statusAlloc, int statusIssued, int statusExpired,
        //    string lastUser, string printFileName, ref int noOfRecords, ref string errorMsg, ref int noOfNotices, string violationCutOff)
        //{

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(this.connectionString);
        //    SqlCommand myCommand = new SqlCommand("NoticeChargeIssue_CA", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterStatusAlloc = new SqlParameter("@StatusAlloc", SqlDbType.Int, 4);
        //    parameterStatusAlloc.Value = statusAlloc;
        //    myCommand.Parameters.Add(parameterStatusAlloc);

        //    SqlParameter parameterStatusIssued = new SqlParameter("@StatusIssued", SqlDbType.Int, 4);
        //    parameterStatusIssued.Value = statusIssued;
        //    myCommand.Parameters.Add(parameterStatusIssued);

        //    SqlParameter parameterStatusExpired = new SqlParameter("@StatusExpired", SqlDbType.Int, 4);
        //    parameterStatusExpired.Value = statusExpired;
        //    myCommand.Parameters.Add(parameterStatusExpired);

        //    SqlParameter parameterPrintFileName = new SqlParameter("@PrintFileName", SqlDbType.VarChar, 50);
        //    parameterPrintFileName.Value = printFileName;
        //    myCommand.Parameters.Add(parameterPrintFileName);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterNoOfRecords = new SqlParameter("@NoOfRecords", SqlDbType.Int);
        //    parameterNoOfRecords.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterNoOfRecords);

        //    SqlParameter parameterNoOfNotices = new SqlParameter("@NoOfNotices", SqlDbType.Int);
        //    parameterNoOfNotices.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterNoOfNotices);

        //    SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int);
        //    parameterSuccess.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterSuccess);

        //    SqlParameter parameterViolationCutOff = new SqlParameter("@ViolationCutOff", SqlDbType.Char, 1);
        //    parameterViolationCutOff.Value = violationCutOff;
        //    myCommand.Parameters.Add(parameterViolationCutOff);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int success = Convert.ToInt32(parameterSuccess.Value);

        //        noOfRecords = Convert.ToInt32(parameterNoOfRecords.Value);
        //        noOfNotices = Convert.ToInt32(parameterNoOfNotices.Value);
        //        return success;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        errorMsg = e.Message;
        //        return -2;
        //    }
        //}

        //dls 090123 - added for CofCT
        public int IssueNotice_CC(int autIntNo, int statusAlloc, int statusIssued, int statusExpired,
            string lastUser, string printFileName, ref int noOfRecords, ref string errorMsg, ref int noOfNotices, string violationCutOff)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("NoticeChargeIssue_CC", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterStatusAlloc = new SqlParameter("@StatusAlloc", SqlDbType.Int, 4);
            parameterStatusAlloc.Value = statusAlloc;
            myCommand.Parameters.Add(parameterStatusAlloc);

            SqlParameter parameterStatusIssued = new SqlParameter("@StatusIssued", SqlDbType.Int, 4);
            parameterStatusIssued.Value = statusIssued;
            myCommand.Parameters.Add(parameterStatusIssued);

            SqlParameter parameterStatusExpired = new SqlParameter("@StatusExpired", SqlDbType.Int, 4);
            parameterStatusExpired.Value = statusExpired;
            myCommand.Parameters.Add(parameterStatusExpired);

            SqlParameter parameterPrintFileName = new SqlParameter("@PrintFileName", SqlDbType.VarChar, 50);
            parameterPrintFileName.Value = printFileName;
            myCommand.Parameters.Add(parameterPrintFileName);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterNoOfRecords = new SqlParameter("@NoOfRecords", SqlDbType.Int);
            parameterNoOfRecords.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterNoOfRecords);

            SqlParameter parameterNoOfNotices = new SqlParameter("@NoOfNotices", SqlDbType.Int);
            parameterNoOfNotices.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterNoOfNotices);

            SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int);
            parameterSuccess.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterSuccess);

            SqlParameter parameterViolationCutOff = new SqlParameter("@ViolationCutOff", SqlDbType.Char, 1);
            parameterViolationCutOff.Value = violationCutOff;
            myCommand.Parameters.Add(parameterViolationCutOff);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int success = Convert.ToInt32(parameterSuccess.Value);

                noOfRecords = Convert.ToInt32(parameterNoOfRecords.Value);
                noOfNotices = Convert.ToInt32(parameterNoOfNotices.Value);
                return success;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                errorMsg = e.Message;
                return -2;
            }
        }

        /// <summary>
        /// Creates the second notice.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="status1stNoticePosted">The status1st notice posted.</param>
        /// <param name="status2ndNoticeCreated">The status2nd notice created.</param>
        /// <param name="lastUser">The last user.</param>
        /// <param name="printFileName">Name of the print file.</param>
        /// <param name="noOfRecords">The no of records.</param>
        /// <param name="errorMsg">The error MSG.</param>
        /// <param name="noOfNotice">The no of notice.</param>
        /// <param name="noOfDaysTo2ndNotice">The no of days to 2nd notice.</param>
        /// <returns></returns>
        public int CreateSecondNotice(int autIntNo, int status1stNoticePosted, int status2ndNoticeCreated,
             string lastUser, string printFileName, ref int noOfRecords, ref string errorMsg, ref int noOfNotice, int noOfDaysTo2ndNotice, int noOfDaysTo2ndPayment)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("NoticeCreate2ndNotice", con);
            com.CommandType = CommandType.StoredProcedure;
            com.CommandTimeout = 600;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@Status1stNoticePosted", SqlDbType.Int, 4).Value = status1stNoticePosted;
            com.Parameters.Add("@Status2ndNoticeCreated", SqlDbType.Int, 4).Value = status2ndNoticeCreated;
            com.Parameters.Add("@PrintFileName", SqlDbType.VarChar, 50).Value = printFileName;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@NoOfDays2ndNotice", SqlDbType.Int, 4).Value = noOfDaysTo2ndNotice;
            com.Parameters.Add("@Not2ndPayDays", SqlDbType.Int, 4).Value = noOfDaysTo2ndPayment;

            SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int);
            parameterSuccess.Direction = ParameterDirection.Output;
            com.Parameters.Add(parameterSuccess);

            SqlParameter parameterNoOfRecords = new SqlParameter("@NoOfRecords", SqlDbType.Int);
            parameterNoOfRecords.Direction = ParameterDirection.Output;
            com.Parameters.Add(parameterNoOfRecords);

            SqlParameter parameterNoOfNotices = new SqlParameter("@NoOfNotices", SqlDbType.Int);
            parameterNoOfNotices.Direction = ParameterDirection.Output;
            com.Parameters.Add(parameterNoOfNotices);

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                noOfRecords = Convert.ToInt32(parameterNoOfRecords.Value);
                noOfNotice = Convert.ToInt32(parameterNoOfRecords.Value);

                return Convert.ToInt32(parameterSuccess.Value);
            }
            catch (Exception ex)
            {
                com.Dispose();
                errorMsg = ex.Message;
                return -1;
            }
            finally
            {
                con.Dispose();
            }
        }

        //Fred add for create SecondNotice with windows service
        public int CreateSecondNotice_WS(int autIntNo, int notIntNo, int status1stNoticePosted, int status2ndNoticeCreated,
             string lastUser, ref string printFileName, ref string errorMsg, int noOfDaysTo2ndNotice, int noOfDaysTo2ndPayment, string dataWashingActived, int dataWashingRecycleMonth)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("NoticeCreate2ndNotice_WS", con);
            com.CommandType = CommandType.StoredProcedure;
            com.CommandTimeout = 600;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters.Add("@Status1stNoticePosted", SqlDbType.Int, 4).Value = status1stNoticePosted;
            com.Parameters.Add("@Status2ndNoticeCreated", SqlDbType.Int, 4).Value = status2ndNoticeCreated;
            //com.Parameters.Add("@PrintFileName", SqlDbType.VarChar, 50).Value = printFileName;
            //Oscar 20120320 changed for output print file name
            SqlParameter paraPrintFile = new SqlParameter("@PrintFileName", printFileName) { Direction = ParameterDirection.InputOutput, Size = 100 };
            com.Parameters.Add(paraPrintFile);

            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@NoOfDays2ndNotice", SqlDbType.Int, 4).Value = noOfDaysTo2ndNotice;
            com.Parameters.Add("@Not2ndPayDays", SqlDbType.Int, 4).Value = noOfDaysTo2ndPayment;
            com.Parameters.Add("@DataWashingActived", SqlDbType.Char, 1).Value = dataWashingActived;
            com.Parameters.Add("@DataWashingRecycleMonth", SqlDbType.Int, 4).Value = dataWashingRecycleMonth;

            SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int);
            parameterSuccess.Direction = ParameterDirection.Output;
            com.Parameters.Add(parameterSuccess);

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                //Oscar 20120320 added for output print file name
                if (paraPrintFile.Value != null) printFileName = paraPrintFile.Value.ToString();

                return Convert.ToInt32(parameterSuccess.Value);
            }
            catch (Exception ex)
            {
                com.Dispose();
                errorMsg = ex.Message;
                return -1;
            }
            finally
            {
                con.Dispose();
            }
        }

        /// <summary>
        /// Gets the summons list by authority.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="noDaysFromNotPaymentDate">The no days from not payment date.</param>
        /// <returns>A <see cref="T:SqlDataReader"/></returns>
        /// 
        //dls 070707 - wrong date was being used - need to used NotPosted1stNoticeDate not NotPaymentDate as start date
        // 2013-07-26 comment out by Henry for useless
        //public SqlDataReader GetSummonsListByAuth(int autIntNo, int noDaysNotPosted1stNoticeDate, string sFlag, ref string error, string processor)
        //{
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("SummonsCreateListByAuth", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.CommandTimeout = 0;

        //    com.Parameters.Add("@Processor", SqlDbType.VarChar, 10).Value = processor;
        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    com.Parameters.Add("@NoDaysFromNotPosted1stNoticeDate", SqlDbType.Int, 4).Value = noDaysNotPosted1stNoticeDate;
        //    // CPA5 = N and CPA6 = Y
        //    com.Parameters.Add("@Flag", SqlDbType.Char, 1).Value = sFlag;

        //    try
        //    {
        //        con.Open();
        //        return com.ExecuteReader(CommandBehavior.CloseConnection);
        //    }
        //    catch (Exception e)
        //    {
        //        error = e.Message;
        //        return null;
        //    }
        //}

        /// <summary>
        /// Gets the post code for a summons charge .
        /// </summary>
        /// <param name="notIntNo">The not int no.</param>
        /// <returns>A <see cref="T:SqlDataReader"/></returns>
        public SqlDataReader GetSummonsChargePostCode(int notIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SummonsChargePostCode", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                string error = e.Message;
                return null;
            }
        }

        /// <summary>
        /// Gets a court date for a summons.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="crtIntNo">The Court int no.</param>
        /// <param name="firstNoticeDate">The first notice date.</param>
        /// <param name="daysToCourt">The days to court.</param>
        /// <param name="lastUser">The last user.</param>
        /// <param name="acIntNo">The authority court int no.</param>
        /// <returns>A <see cref="T:DateTime"/></returns>
        /// 
        //dls 070707 - Summons Issue date will always be today (if summons issue is successful. Do not need to use
        // 1st notice posted date in calculations. Proc will use todays' date + no. of days from summons issue date --> court date 
        //public DateTime GetSummonsCourtDate(int autIntNo, int crtIntNo, DateTime firstNoticeDate, int daysToCourt, string lastUser, ref int acIntNo)
        //public DateTime GetSummonsCourtDate(int autIntNo, int crtRIntNo, int daysToCourt, double dPercentage, string lastUser, ref int acIntNo)
        //jerry 2011-11-15 add noticeFilmType parameter
        public DateTime GetSummonsCourtDate(int autIntNo, int crtIntNo, int daysToCourt, double dPercentage, string lastUser, string noticeFilmType,
            ref int crtRIntNo, string needRestrictNoAOG = null, string needSeparateNoAOG = null, string isSection35="N")
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SummonsCourtDate", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@CrtIntNo", SqlDbType.Int, 4).Value = crtIntNo;
            //com.Parameters.Add("@FirstNoticeDate", SqlDbType.SmallDateTime).Value = firstNoticeDate;
            com.Parameters.Add("@DaysToCourt", SqlDbType.SmallInt).Value = daysToCourt;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@Percentage", SqlDbType.Real).Value = dPercentage;

            // jerry 2011-11-15 add noticeFilmType parameter
            com.Parameters.Add("@NotFilmType", SqlDbType.Char, 1).Value = noticeFilmType;

            //jerry 2012-03-22 add courtRuleRestrictNoAOG parameter
            com.Parameters.Add("@NeedRestrictNoAOG", SqlDbType.Char, 1).Value = needRestrictNoAOG;

            //Jerry 2012-05-21 add needSeparateNoAOG parameter
            com.Parameters.Add("@NeedSeparateNoAOG", SqlDbType.Char, 1).Value = needSeparateNoAOG;

            SqlParameter parameterCourtDate = com.Parameters.Add("@CourtDate", SqlDbType.SmallDateTime);
            parameterCourtDate.Direction = ParameterDirection.Output;

            SqlParameter parameterCrtRIntNo = com.Parameters.Add("@CrtRIntNo", SqlDbType.Int, 4);
            parameterCrtRIntNo.Direction = ParameterDirection.Output;

            //Jake 2013-12-13 added new parameter
            com.Parameters.Add("@IsS35NoAOG", SqlDbType.Char, 1).Value = isSection35;
            try
            {
                con.Open();
                com.ExecuteNonQuery();

                crtRIntNo = Convert.ToInt32(parameterCrtRIntNo.Value);
                if (parameterCourtDate.Value == DBNull.Value)
                    return NO_DATE;
                else
                    return (DateTime)parameterCourtDate.Value;
                //return (DateTime)parameterCourtDate.Value;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return NO_DATE;
            }
            finally
            {
                con.Dispose();
            }
        }

        /// <summary>
        /// Gets a summons server for an Authority and post code.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="postCode">The post code.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns>A <see cref="T:SqlDataReader"/></returns>
        public SqlDataReader GetSummonsServerByPostCode(int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SummonsServerByPostCode", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SPR
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("There was an error retrieving summons servers for Authority {0} ({1}).", autIntNo, ex.Message));
                return null;
            }
        }

        /// <summary>
        /// Gets the case number for a summons.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        // 2013-07-19 comment by Henry for useless
        //public string GetSummonsCaseNo(int autIntNo, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("CaseRegisterGetCaseNo", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to the SP
        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

        //    SqlParameter parameterCaseNo = com.Parameters.Add("@CaseNo", SqlDbType.VarChar, 20);
        //    parameterCaseNo.Direction = ParameterDirection.Output;

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();

        //        return Convert.ToString(parameterCaseNo.Value);
        //    }
        //    catch (Exception e)
        //    {
        //        string msg = e.Message;
        //        return string.Empty;
        //    }
        //    finally
        //    {
        //        con.Dispose();
        //    }
        //}

        /// <summary>
        /// Gets the natis authorities.
        /// </summary>
        /// <param name="startStatus">The start status.</param>
        /// <param name="endStatus">The end status.</param>
        /// <param name="type">The type.</param>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        public SqlDataReader GetNatisAuthorities(int startStatus, int endStatus, string type, int autIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("GetNatisAuthData", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            SqlParameter parameterStartStatus = new SqlParameter("@StartStatus", SqlDbType.Int, 4);
            parameterStartStatus.Value = startStatus;
            com.Parameters.Add(parameterStartStatus);

            SqlParameter parameterEndStatus = new SqlParameter("@EndStatus", SqlDbType.Int, 4);
            parameterEndStatus.Value = endStatus;
            com.Parameters.Add(parameterEndStatus);

            SqlParameter parameterType = new SqlParameter("@Type", SqlDbType.VarChar, 10);
            parameterType.Value = type;
            com.Parameters.Add(parameterType);

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Assert(false, e.Message);
                return null;
            }
        }

        //this is called by Single Summons and TPExtInt summon batch process - it sets the values for the roadblock summons to default values
        public int AddSummons(int notIntNo, string sumStatus, string notTicketNo, string proxyFlag,
            DateTime sumCourtDate, DateTime dtLastAOG, DateTime dtSumServeBy, string lastUser,
            int asIntNo, string sumLocation, Int32 crtIntNo, Int32 autIntNo, Int32 crtRIntNo,
            string summonGenerateRule, ref string errMessage, int? rbsIntNo, List<string> chgIntNoList,
            List<string> chgRowVersionList, int batchSize, string createDateString,
            out string printFileName, string dataWashingActived, int dataWashingRecycleMonth,
            bool DisplayAccusedCompany, bool allocatedByCourt = false, string s35NoAOG = "N")
        {
            return AddSummons(notIntNo, sumStatus, notTicketNo, proxyFlag, sumCourtDate, dtLastAOG,
                dtSumServeBy, lastUser, asIntNo, sumLocation, crtIntNo, autIntNo, crtRIntNo,
                summonGenerateRule, ref  errMessage, rbsIntNo, 0, null, null, 0, 0, 1,
                chgIntNoList, chgRowVersionList, batchSize, createDateString, out printFileName,
                dataWashingActived, dataWashingRecycleMonth, DisplayAccusedCompany, allocatedByCourt, s35NoAOG);
        }

        /// <summary>
        /// Inserts into summons , sumcharge , summons_charge and accused tables
        /// </summary>
        /// <param name="notSummonsPrintFileName">Name of the not summons print file.</param>
        /// <param name="chgIntNo">The CHG int no.</param>
        /// <param name="acIntNo">The ac int no.</param>
        /// <param name="crCaseNo">The cr case no.</param>
        /// <param name="crCourtDate">The cr court date.</param>
        /// <param name="crFineAmount">The cr fine amount.</param>
        /// <param name="crSummonsNo">The cr summons no.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        public int AddSummons(int notIntNo, string sumStatus, string notTicketNo, string proxyFlag,
            DateTime sumCourtDate, DateTime dtLastAOG, DateTime dtSumServeBy, string lastUser,
            int asIntNo, string sumLocation, Int32 crtIntNo, Int32 autIntNo, Int32 crtRIntNo,
            string summonGenerateRule, ref string errMessage, int? rbsIntNo, int TOIntNo,
            string OfficerNo, string OfficerName, int SumIntNo, int ssIntNo, int isBatchPrint,
            List<string> chgIntNoList, List<string> chgRowVersionList, int batchSize,
            string createDateString, out string printFileName, string dataWashingActived,
            int dataWashingRecycleMonth, bool DisplayAccusedCompany, bool allocatedByCourt = false, string s35NoAOG = "N")
        {
            printFileName = string.Empty;   // Oscar 20120307 change for get print file name
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SummonsAdd", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            //com.Parameters.Add("@ChgIntNo", SqlDbType.Int, 4).Value = chgIntNo;
            com.Parameters.Add("@chgIntNoList", SqlDbType.VarChar).Value = string.Join(",", chgIntNoList.ToArray());  //Oscar 20101111 - changed
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters.Add("@NotTicketNo", SqlDbType.VarChar, 50).Value = notTicketNo;
            com.Parameters.Add("@ProxyFlag", SqlDbType.Char, 1).Value = proxyFlag;
            //com.Parameters.Add("@Stationery", SqlDbType.VarChar, 10).Value = stationery;
            com.Parameters.Add("@SumStatus", SqlDbType.VarChar, 15).Value = sumStatus;
            com.Parameters.Add("@CourtDate", SqlDbType.SmallDateTime).Value = sumCourtDate;
            com.Parameters.Add("@SumLastAOGDate", SqlDbType.SmallDateTime).Value = dtLastAOG;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@SumServeByDate", SqlDbType.SmallDateTime).Value = dtSumServeBy;
            com.Parameters.Add("@ASIntNo", SqlDbType.Int, 4).Value = asIntNo;
            com.Parameters.Add("@SumLocationGenerated", SqlDbType.VarChar, 50).Value = sumLocation;
            //com.Parameters.Add("@ChargeRowVersion", SqlDbType.BigInt, 8).Value = chargeRowVersion;
            com.Parameters.Add("@chgRowVersionList", SqlDbType.VarChar).Value = string.Join(",", chgRowVersionList.ToArray());  //Oscar 20101111 - changed
            com.Parameters.Add("@CrtIntNo", SqlDbType.Int, 4).Value = crtIntNo;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@CrtRIntNo", SqlDbType.Int, 4).Value = crtRIntNo;
            com.Parameters.Add("@SummonGenerateRule", SqlDbType.Char, 1).Value = summonGenerateRule;
            com.Parameters.Add("@RBSIntNo", SqlDbType.Int, 4).Value = (rbsIntNo.HasValue && rbsIntNo.Value > 0) ? rbsIntNo : null;
            com.Parameters.Add("@BatchSize", SqlDbType.Int, 4).Value = batchSize;
            com.Parameters.Add("@CreateDateString", SqlDbType.VarChar, 20).Value = createDateString;
            com.Parameters.Add("@DataWashingActived", SqlDbType.Char, 1).Value = dataWashingActived;
            com.Parameters.Add("@DataWashingRecycleMonth", SqlDbType.Int, 4).Value = dataWashingRecycleMonth;
            com.Parameters.Add("@DisplayRepresentative", SqlDbType.Bit).Value = DisplayAccusedCompany;
            com.Parameters.Add("@S35NoAOG", SqlDbType.Char, 1).Value = s35NoAOG;

            if (OfficerNo != null)
            {
                if (TOIntNo != 0)
                    com.Parameters.Add("@TOIntNo", SqlDbType.Int, 4).Value = TOIntNo;

                com.Parameters.Add("@OfficerNo", SqlDbType.VarChar, 20).Value = OfficerNo;
                com.Parameters.Add("@OfficerName", SqlDbType.VarChar, 100).Value = OfficerName;
                com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = SumIntNo;
                com.Parameters.Add("@SSIntNo", SqlDbType.Int, 4).Value = ssIntNo;
                com.Parameters.Add("@isBatchPrint", SqlDbType.Bit, 1).Value = isBatchPrint;
            }

            // Oscar 2013-05-07 added
            com.Parameters.AddWithValue("@AllocatedByCourt", allocatedByCourt);

            try
            {
                con.Open();

                // Oscar 20120307 change for get print file name
                //return (int)com.ExecuteScalar();
                int result = -30;
                using (SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dr.Read())
                    {
                        if (!dr.IsDBNull(0))
                            int.TryParse(dr[0].ToString(), out result);
                        // 2013-11-08, Oscar add "dr.FieldCount > 1" checking
                        if (dr.FieldCount > 1)
                        {
                            if (!dr.IsDBNull(1))
                                printFileName = dr[1].ToString();
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                errMessage = ex.Message;
                return -30;
            }
            finally
            {
                con.Dispose();
            }
        }

        #region Oscar 20101115 - disabled, no longer used
        ////public int AddSummons(string notSummonsPrintFileName, int notIntNo, int chgIntNo, string sumStatus,
        ////DateTime sumCourtDate, string summonsNo, DateTime dtLastAOG, DateTime dtSumServeBy, string lastUser,
        ////int asIntNo, string sumLocation, Int64 chargeRowVersion, Int32 crtIntNo, Int32 autIntNo, Int32 crtRIntNo)
        //// Oscar 20101112 - changed parameters
        //public int AddSummons(string notSummonsPrintFileName, int notIntNo, string sumStatus, DateTime sumCourtDate, string summonsNo, DateTime dtLastAOG, DateTime dtSumServeBy, string lastUser, int asIntNo, string sumLocation, Int32 crtIntNo, Int32 autIntNo, Int32 crtRIntNo, List<string> chgIntNoList, List<string> chgRowVersionList)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("SummonsAdd", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to the SP
        //    com.Parameters.Add("@NotSummonsPrintFileName", SqlDbType.VarChar, 50).Value = notSummonsPrintFileName;
        //    //com.Parameters.Add("@ChgIntNo", SqlDbType.Int, 4).Value = chgIntNo;
        //    com.Parameters.Add("@chgIntNoList", SqlDbType.VarChar).Value = string.Join(",", chgIntNoList.ToArray());  //Oscar 20101111 - changed
        //    com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
        //    com.Parameters.Add("@SumStatus", SqlDbType.VarChar, 15).Value = sumStatus;
        //    com.Parameters.Add("@CourtDate", SqlDbType.SmallDateTime).Value = sumCourtDate;
        //    com.Parameters.Add("@SumLastAOGDate", SqlDbType.SmallDateTime).Value = dtLastAOG;
        //    com.Parameters.Add("@SummonsNo", SqlDbType.VarChar, 50).Value = summonsNo;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
        //    com.Parameters.Add("@SumServeByDate", SqlDbType.SmallDateTime).Value = dtSumServeBy;
        //    com.Parameters.Add("@ASIntNo", SqlDbType.Int, 4).Value = asIntNo;
        //    com.Parameters.Add("@SumLocationGenerated", SqlDbType.VarChar, 50).Value = sumLocation;
        //    //com.Parameters.Add("@ChargeRowVersion", SqlDbType.BigInt, 8).Value = chargeRowVersion;
        //    com.Parameters.Add("@chgRowVersionList", SqlDbType.VarChar).Value = string.Join(",", chgRowVersionList.ToArray());  //Oscar 20101111 - changed
        //    com.Parameters.Add("@CrtIntNo", SqlDbType.Int, 4).Value = crtIntNo;
        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    com.Parameters.Add("@CrtRIntNo", SqlDbType.Int, 4).Value = crtRIntNo;

        //    try
        //    {
        //        con.Open();

        //        return (int)com.ExecuteScalar();
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex.Message);
        //        return -1;
        //    }
        //    finally
        //    {
        //        con.Dispose();
        //    }
        //}
        #endregion

        /// <summary>
        /// Sets the status of a charge during Summons processing.
        /// </summary>
        /// <param name="chargeStatus">The charge status.</param>
        /// <param name="notIntNo">The not int no.</param>
        public void SetChargeStatus(int chargeStatus, Int32 chgIntNo, string lastUser, ref Int64 chargeRowVersion)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("UpdateChargeStatus", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@ChgIntNo", SqlDbType.Int, 4).Value = chgIntNo;
            com.Parameters.Add("@ChargeStatus", SqlDbType.Int, 4).Value = chargeStatus;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@ChargeRowVersion", SqlDbType.BigInt, 8).Value = chargeRowVersion;

            com.Parameters["@ChargeRowVersion"].Direction = ParameterDirection.InputOutput;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                chargeRowVersion = Convert.ToInt64(com.Parameters["@ChargeRowVersion"].Value);
            }
            finally
            {
                con.Close();
            }
        }

        public SqlDataReader GetAuthListForContraventions_CPI(string processor, int statusLoaded, int statusFixed, ref string errMessage)
        {
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("AuthListForContraventions_CPI", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterProcessor = new SqlParameter("@Processor", SqlDbType.VarChar, 10);
            parameterProcessor.Value = processor;
            myCommand.Parameters.Add(parameterProcessor);

            SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
            parameterStatusLoaded.Value = statusLoaded;
            myCommand.Parameters.Add(parameterStatusLoaded);

            SqlParameter parameterStatusFixed = new SqlParameter("@StatusFixed", SqlDbType.Int, 4);
            parameterStatusFixed.Value = statusFixed;
            myCommand.Parameters.Add(parameterStatusFixed);

            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                System.Diagnostics.Debug.Assert(false, e.Message);
                return null;
            }
        }

        public SqlDataReader GetAuthListForInfringements_CA(string processor, int statusLoaded, int statusFixed, ref string errMessage)
        {
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("AuthListForInfringements", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterProcessor = new SqlParameter("@Processor", SqlDbType.VarChar, 10);
            parameterProcessor.Value = processor;
            myCommand.Parameters.Add(parameterProcessor);

            SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
            parameterStatusLoaded.Value = statusLoaded;
            myCommand.Parameters.Add(parameterStatusLoaded);

            SqlParameter parameterStatusFixed = new SqlParameter("@StatusFixed", SqlDbType.Int, 4);
            parameterStatusFixed.Value = statusFixed;
            myCommand.Parameters.Add(parameterStatusFixed);

            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                System.Diagnostics.Debug.Assert(false, e.Message);
                return null;
            }
        }

        public SqlDataReader GetAuthListForInfringements_CC(string processor, int statusLoaded, int statusFixed, ref string errMessage)
        {
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("AuthListForInfringements", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterProcessor = new SqlParameter("@Processor", SqlDbType.VarChar, 10);
            parameterProcessor.Value = processor;
            myCommand.Parameters.Add(parameterProcessor);

            SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
            parameterStatusLoaded.Value = statusLoaded;
            myCommand.Parameters.Add(parameterStatusLoaded);

            SqlParameter parameterStatusFixed = new SqlParameter("@StatusFixed", SqlDbType.Int, 4);
            parameterStatusFixed.Value = statusFixed;
            myCommand.Parameters.Add(parameterStatusFixed);

            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                System.Diagnostics.Debug.Assert(false, e.Message);
                return null;
            }
        }

        public SqlDataReader GetAuthForInfringements(string authCode, string processor, int statusLoaded, int statusFixed, ref string errMessage)
        {
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("AuthForInfringements_WS", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAuthCode = new SqlParameter("@AuthCode", SqlDbType.Char, 3);
            parameterAuthCode.Value = authCode;
            myCommand.Parameters.Add(parameterAuthCode);

            SqlParameter parameterProcessor = new SqlParameter("@Processor", SqlDbType.VarChar, 10);
            parameterProcessor.Value = processor;
            myCommand.Parameters.Add(parameterProcessor);

            SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
            parameterStatusLoaded.Value = statusLoaded;
            myCommand.Parameters.Add(parameterStatusLoaded);

            SqlParameter parameterStatusFixed = new SqlParameter("@StatusFixed", SqlDbType.Int, 4);
            parameterStatusFixed.Value = statusFixed;
            myCommand.Parameters.Add(parameterStatusFixed);

            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                System.Diagnostics.Debug.Assert(false, e.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the auth list for notice exceptions.
        /// </summary>
        /// <param name="processor">The processor.</param>
        /// <param name="statusError">The status error.</param>
        /// <returns></returns>
        public SqlDataReader GetAuthListForNoticeExceptions(string processor, int statusError, ref string errMessage)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("AuthListForNoticeExceptions", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@Processor", SqlDbType.VarChar, 10).Value = processor;
            com.Parameters.Add("@StatusError", SqlDbType.Int, 4).Value = statusError;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                System.Diagnostics.Debug.Assert(false, e.Message);
                return null;
            }
        }

        /// <summary>
        /// Checks the summons offender details.
        /// </summary>
        /// <param name="notIntNo">The notice int no.</param>
        /// <returns>A <see cref="SqlDataReader"/>.</returns>
        public SqlDataReader CheckSummonsOffenderDetails(int notIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SummonsGetOffenderDetails", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        /// <summary>
        /// Summonses the set invalid offender details for summons.
        /// </summary>
        /// <param name="chgIntNo">The charge int no.</param>
        public Int32 SummonsSetInvalidOffenderDetailsForSummons(int chgIntNo, string lastUser, ref Int64 chargeRowVersion)  // 2013-07-24 add parameter lastUser by Henry
        {
            //dls 090605 - this needs to return the updated row version so that the summons can proceed
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("ChargeUpdateStatus", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@ChgIntNo", SqlDbType.Int, 4).Value = chgIntNo;
            com.Parameters.Add("@Status", SqlDbType.Int, 4).Value = 590;
            com.Parameters.Add("@ChargeRowVersion", SqlDbType.BigInt, 8).Value = chargeRowVersion;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            com.Parameters["@ChgIntNo"].Direction = ParameterDirection.InputOutput;
            com.Parameters["@ChargeRowVersion"].Direction = ParameterDirection.InputOutput;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                chgIntNo = Int32.Parse(com.Parameters["@ChgIntNo"].Value.ToString());
                chargeRowVersion = Int64.Parse(com.Parameters["@ChargeRowVersion"].Value.ToString());
                return chgIntNo;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return -1;
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Gets the list of authorities ho have summons to process.
        /// </summary>
        /// <returns>A <see cref="T:SqlDataReader"/></returns>
        public SqlDataReader GetAuthListForSummonsServed(string processor)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("AuthListForServedSummons", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@Processor", SqlDbType.VarChar, 10).Value = processor;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Assert(false, e.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the list of authorities ho have summons to process.
        /// </summary>
        /// <returns>A <see cref="T:SqlDataReader"/></returns>
        public SqlDataReader GetAuthListForSummonsExpired(string processor)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("AuthListForSummonsExpire", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@Processor", SqlDbType.VarChar, 10).Value = processor;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Assert(false, e.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the summonses that have been served.
        /// jerry 2011-11-08 add courtRule parameter
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        // 2013-07-26 comment out by Henry for useless
        //public int CaseNoAllocate(int autIntNo, string caseNumberGenerateRule, int iCourtRoomNo, DateTime dtCourtDate, string courtRule)
        //{
        //    int nRes = -1;
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("CaseNoAllocate", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    com.Parameters.Add("@CaseNumberGenerateRule", SqlDbType.Char, 1).Value = caseNumberGenerateRule;
        //    com.Parameters.Add("@CrtRIntNo", SqlDbType.Int, 4).Value = iCourtRoomNo;
        //    com.Parameters.Add("@CourtDate", SqlDbType.DateTime, 4).Value = dtCourtDate;
        //    //jerry 2011-11-08 add
        //    com.Parameters.Add("@CourtRule", SqlDbType.Char, 1).Value = courtRule;
        //    try
        //    {
        //        con.Open();
        //        SqlDataReader res = com.ExecuteReader(CommandBehavior.CloseConnection);
        //        if (res.HasRows)
        //            while (res.Read())
        //                nRes = Convert.ToInt32(res[0].ToString());
        //        res.Close();
        //        return nRes;
        //    }
        //    catch (Exception e)
        //    {
        //        System.Diagnostics.Debug.Assert(false, e.Message);
        //        return -1;
        //    }
        //}

        /// <summary>
        /// Gets the court rooms for each authority.
        /// 20090525
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        public SqlDataReader GetCourtRoomsForAut(int autIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CourtRoomsByAuthority", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Assert(false, e.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the summonses that have been served.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        //public int UpdateSummonsCaseNo(int nSumIntNo,string sCaseNo)
        //{
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("SummonsCaseNoUpdate", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    SqlParameter prm = new SqlParameter("@SumIntNo", SqlDbType.Int,4);
        //    prm.Direction = ParameterDirection.InputOutput ;
        //    prm.Value = nSumIntNo;
        //    com.Parameters.Add(prm);

        //    SqlParameter prmCaseNo = new SqlParameter("@SumCaseNo", SqlDbType.VarChar ,50);
        //    prmCaseNo.Value = sCaseNo;
        //    com.Parameters.Add(prmCaseNo);

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();
        //        con.Dispose();
        //        int tNumber = Convert.ToInt32(prm.Value);
        //        return tNumber;
        //    }
        //    catch (Exception e)
        //    {
        //        con.Dispose();
        //        System.Diagnostics.Debug.Assert(false, e.Message);
        //        return -1;
        //    }
        //}


        //jerry 2011-11-16 add CrtIntNo parameter
        public SqlDataReader GetWOAListForAuth(int autIntNo, int noOfDays, string processor, int CrtIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("WOAListForAuthority", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@Processor", SqlDbType.VarChar, 10).Value = processor;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@NoOfDays", SqlDbType.Int, 4).Value = noOfDays;
            //jerry 2011-11-16 add CrtIntNo parameter
            com.Parameters.Add("@CrtIntNo", SqlDbType.Int, 4).Value = CrtIntNo;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Assert(false, e.Message);
                return null;
            }
        }

        /// <summary>
        /// jerry 2011-11-16 add
        /// </summary>
        /// <param name="autIntNo"></param>
        /// <param name="noOfDays"></param>
        /// <param name="processor"></param>
        /// <returns></returns>
        public SqlDataReader GetCourtListForWOA(int autIntNo, int noOfDays, string processor)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CourtListForWOA", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@Processor", SqlDbType.VarChar, 10).Value = processor;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@NoOfDays", SqlDbType.Int, 4).Value = noOfDays;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Assert(false, e.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the summons (not expired) list by authority.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns>A <see cref="T:SqlDataReader"/></returns>
        /// 
        /// dls 090323 - add no of days so that we restrict the list to those that are ready to be expired
        public SqlDataReader GetSummonsExpiredListByAuth(int autIntNo, int nDays)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SummonsCreateExpiredListByAuth", con);
            com.CommandType = CommandType.StoredProcedure;
            com.CommandTimeout = 0;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@NoOfDays", SqlDbType.Int, 4).Value = nDays;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the summons (not expired) list by authority.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns>A <see cref="T:SqlDataReader"/></returns>
        // 2013-07-26 comment out by Henry for useless
        //public int UpdateSummonsExpired(int nSumIntNo, int nDays)
        //{
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("SummonsExpired", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.CommandTimeout = 0;

        //    SqlParameter prm = new SqlParameter("@SumIntNo", SqlDbType.Int, 4);
        //    prm.Direction = ParameterDirection.InputOutput;
        //    prm.Value = nSumIntNo;
        //    com.Parameters.Add(prm);

        //    com.Parameters.Add("@NoDays", SqlDbType.Int, 4).Value = nDays;

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();
        //        con.Dispose();
        //        int tNumber = Convert.ToInt32(prm.Value);
        //        return tNumber;
        //    }
        //    catch (Exception e)
        //    {
        //        con.Dispose();
        //        Console.Write(e.Message);
        //        return -1;
        //    }
        //}

        // 2013-07-26 comment out by Henry for useless
        //dle 2010-03-19 - temp filler
        //public int CreateEasyPaySecondNotice(int autIntNo, string lastUser, string printFileName, ref int noOfRecords,
        //    ref string errorMsg, ref int noOfNotice, int noOfDaysTo2ndPayment, string nPrefix, string autNo, int noDaysSummons)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("NoticeCreateEasyPay2ndNotice", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    com.Parameters.Add("@PrintFileName", SqlDbType.VarChar, 50).Value = printFileName;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
        //    com.Parameters.Add("@Not2ndPayDays", SqlDbType.Int, 4).Value = noOfDaysTo2ndPayment;
        //    com.Parameters.Add("@NPrefix", SqlDbType.VarChar, 3).Value = nPrefix;
        //    com.Parameters.Add("@NoDaysSummons", SqlDbType.Int, 4).Value = noDaysSummons;

        //    SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int);
        //    parameterSuccess.Direction = ParameterDirection.Output;
        //    com.Parameters.Add(parameterSuccess);

        //    SqlParameter parameterNoOfRecords = new SqlParameter("@NoOfRecords", SqlDbType.Int);
        //    parameterNoOfRecords.Direction = ParameterDirection.Output;
        //    com.Parameters.Add(parameterNoOfRecords);

        //    SqlParameter parameterNoOfNotices = new SqlParameter("@NoOfNotices", SqlDbType.Int);
        //    parameterNoOfNotices.Direction = ParameterDirection.Output;
        //    com.Parameters.Add(parameterNoOfNotices);

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();

        //        noOfRecords = Convert.ToInt32(parameterNoOfRecords.Value);
        //        noOfNotice = Convert.ToInt32(parameterNoOfRecords.Value);

        //        return Convert.ToInt32(parameterSuccess.Value);
        //    }
        //    catch (Exception ex)
        //    {
        //        com.Dispose();
        //        errorMsg = ex.Message;
        //        return -1;
        //    }
        //    finally
        //    {
        //        con.Dispose();
        //    }
        //}

        //Add by Tod Zhang on 090707 to extract road block

        /// <summary>
        /// Gets the auth list for processing road block file.
        /// </summary>
        public SqlDataReader GetAuthListForCreatingRoadBlockDataExtract(ref string errMessage)
        {
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("AuthListForRoadBlockExtract", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                myConnection.Open();
                SqlDataReader reader = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return reader;
            }
            catch (Exception e)
            {
                if (myConnection.State != ConnectionState.Closed)
                    myConnection.Dispose();
                System.Diagnostics.Debug.Assert(false, e.Message);
                return null;
            }
        }

        //Add by Tod Zhang on 090707 to update AutRoadblockExtractDate
        /// <summary>
        /// update AutRoadblockExtractDate.
        /// </summary>
        // 2013-07-26 add parameter lastUser by Henry
        public int UpdateAutRoadblockExtractDate(string AutRoadblockExtractDate, string mtrCode, string lastUser)
        {
            try
            {
                using (SqlConnection myConnection = new SqlConnection(this.connectionString))
                {
                    SqlCommand myCommand = new SqlCommand("AuthRoadblockExtractDateUpdate", myConnection);
                    myCommand.CommandType = CommandType.StoredProcedure;
                    int affectedCount = 0;

                    myCommand.Parameters.Add("@AutRoadblockExtractDate", SqlDbType.VarChar, 50).Value = AutRoadblockExtractDate;
                    myCommand.Parameters.Add("@mtrCode", SqlDbType.Char, 3).Value = mtrCode;
                    myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

                    myConnection.Open();
                    affectedCount = myCommand.ExecuteNonQuery();

                    return affectedCount;
                }
            }
            catch (Exception e)
            {
                return -1;
            }
        }


        //Add by Tod Zhang on 090709 to update authority road block false number plate extract date
        /// <summary>
        /// update authority road block false number plate extract date
        /// </summary>
        // 2013-07-26 add parameter lastUser by Henry
        public int UpdateAutRoadBlockFalseNumberPlateExtractDate(string AutRoadBlockFalseNumberPlateExtractDate, string mtrCode, string lastUser)
        {
            try
            {
                using (SqlConnection myConnection = new SqlConnection(this.connectionString))
                {
                    SqlCommand myCommand = new SqlCommand("AuthRoadBlockFalseNumberPlateExtractDateUpdate", myConnection);
                    myCommand.CommandType = CommandType.StoredProcedure;
                    int affectedCount = 0;

                    myCommand.Parameters.Add("@AutRoadBlockExtractFalseNumberPlateDate", SqlDbType.VarChar, 50).Value = AutRoadBlockFalseNumberPlateExtractDate;
                    myCommand.Parameters.Add("@mtrCode", SqlDbType.Char, 3).Value = mtrCode;
                    myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

                    myConnection.Open();
                    affectedCount = myCommand.ExecuteNonQuery();

                    return affectedCount;
                }
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        //Add by Tod Zhang on 090713 to extract image file

        /// <summary>
        /// Gets the image file data.
        /// </summary>
        public SqlDataReader GetImageFileData(int scImIntNo, ref string errMessage)
        {
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("ScanImage_GetImage", myConnection);
            myCommand.Parameters.Add("@ScImIntNo", SqlDbType.Int).Value = scImIntNo;
            myCommand.CommandType = CommandType.StoredProcedure;

            try
            {

                myConnection.Open();
                SqlDataReader reader = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return reader;
            }
            catch (Exception e)
            {
                if (myConnection.State != ConnectionState.Closed)
                    myConnection.Dispose();
                System.Diagnostics.Debug.Assert(false, e.Message);
                return null;
            }


        }


        //Add by Tod Zhang on 090923 to check whether or not there is any duplicate print file in the database
        // FT 090930 modify
        /// <summary>
        /// Check Duplicate Print File in database
        /// </summary>
        public int CheckDuplicatePrintFile(string localAutcode, string branchCode, string documentType, string sequenceNumber, ref string errMessage)
        {
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            //FT modify SP name
            SqlCommand myCommand = new SqlCommand("RequestFileDuplicateCheck", myConnection);

            myCommand.Parameters.Add("@LocalAutcode", SqlDbType.VarChar, 6).Value = localAutcode;
            myCommand.Parameters.Add("@BranchCode", SqlDbType.VarChar, 2).Value = branchCode;
            myCommand.Parameters.Add("@DocumentType", SqlDbType.VarChar, 2).Value = documentType;
            myCommand.Parameters.Add("@SequenceNumber", SqlDbType.VarChar, 4).Value = sequenceNumber;
            myCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                myConnection.Open();
                SqlDataReader reader = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                return (int)reader["NoDuplicate"];
            }
            catch (Exception e)
            {
                errMessage = e.Message;

                System.Diagnostics.Debug.Assert(false, errMessage);

                return -2;
            }
            finally
            {
                if (myConnection.State != ConnectionState.Closed)
                    myConnection.Dispose();
            }
        }

        // FT 090930 Add RequestPrintFile
        public int AddRequestPrintFile(string fileName, string branchCode, string documentType, string sequenceNumber,
            int autIntNo, ref string errMessage, int noOfRecords, int noOfRecordsFailed, string lastUser)
        {
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("RequestFileAdd", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            myCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            myCommand.Parameters.Add("@FileName", SqlDbType.VarChar, 20).Value = fileName;
            myCommand.Parameters.Add("@BranchCode", SqlDbType.VarChar, 2).Value = branchCode;
            myCommand.Parameters.Add("@DocumentType", SqlDbType.VarChar, 2).Value = documentType;
            myCommand.Parameters.Add("@SequenceNumber", SqlDbType.VarChar, 4).Value = sequenceNumber;
            myCommand.Parameters.Add("@NoOfRecords", SqlDbType.Int, 4).Value = noOfRecords;
            myCommand.Parameters.Add("@NoOfRecordsFailed", SqlDbType.Int, 4).Value = noOfRecordsFailed;
            myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {

                myConnection.Open();
                SqlDataReader reader = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return 1;
            }
            catch (Exception e)
            {
                if (myConnection.State != ConnectionState.Closed)
                    myConnection.Dispose();

                errMessage = e.Message;

                System.Diagnostics.Debug.Assert(false, errMessage);
                return -1;
            }


        }

    }
}
