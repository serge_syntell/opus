using System;
using System.Collections.Generic;
using System.Text;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents an Authority that can issue Summons
    /// </summary>
    public class Authority : AuthorityBase
    {
        // Fields
        //changed areacode from int to varchar(8)
        //private Dictionary<short, int> areaCodes;
        private Dictionary<string, int> areaCodes; //contents =<areacode,asintno>
        private bool mustHaveFullNames = false;
        private string issueWOANotice = "N";
        private string ticketProcessor = string.Empty;
        private bool useOriginalFineAmnt = true;
        private bool displayAccusedCompany = true;
        private string summonsPrintStationeryType = "CPA5";
        private int noOfDaysSummonsIssue = 0;
        private int noOfDaysCourtDate = 0;
        private int noOfDaysLastAOGDate = 0;
        private int noOfDaysSumServByDate = 0;
        private int minAmountForSummonsProcessing = 0;
        private int summonsPrintFileBatchSize = 200;

        //dls 080121 - added for use with Civitas Processing
        //dls 080617 - also use for representations
        public Authority(int autIntNo, string autName)
        {
            base.ID = autIntNo;
            base.Name = autName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Authority"/> class.
        /// </summary>
        public Authority()
        {
            //areacode changed from int to varchar(8)
            //this.areaCodes = new Dictionary<short, int>();
            this.areaCodes = new Dictionary<string, int>();
        }

        /// <summary>
        /// Gets or sets the area codes.
        /// </summary>
        /// <value>The area codes.</value>
        public Dictionary<string, int> AreaCodes //was <short, int>
        {
            get { return this.areaCodes; }
            set { this.areaCodes = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this authority requires full names for summons issue.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [must have full names for summons issue]; otherwise, <c>false</c>.
        /// </value>
        public bool MustHaveFullNamesForSummonsIssue
        {
            get { return this.mustHaveFullNames; }
            set { this.mustHaveFullNames = value; }
        }

        public string IssueWOANotice
        {
            get { return this.issueWOANotice; }
            set { this.issueWOANotice = value; }
        }

        public string TicketProcessor
        {
            get { return this.ticketProcessor; }
            set { this.ticketProcessor = value; }
        }

        public string SummonsPrintStationeryType
        {
            get { return this.summonsPrintStationeryType; }
            set { this.summonsPrintStationeryType = value; }
        }

        public bool UseOriginalFineAmnt
        {
            get { return this.useOriginalFineAmnt; }
            set { this.useOriginalFineAmnt = value; }
        }

        public bool DisplayAccusedCompany
        {
            get { return this.displayAccusedCompany; }
            set { this.displayAccusedCompany = value; }
        }

        public int NoOfDaysSummonsIssue
        {
            get { return this.noOfDaysSummonsIssue; }
            set { this.noOfDaysSummonsIssue = value; }
        }

        public int MinAmountForSummonsProcessing
        {
            get { return this.minAmountForSummonsProcessing; }
            set { this.minAmountForSummonsProcessing = value; }
        }

        public int SummonsPrintFileBatchSize
        {
            get { return this.summonsPrintFileBatchSize; }
            set { this.summonsPrintFileBatchSize = value; }
        }

        public int NoOfDaysSumServByDate
        {
            get { return this.noOfDaysSumServByDate; }
            set { this.noOfDaysSumServByDate = value; }
        }

        public int NoOfDaysLastAOGDate
        {
            get { return this.noOfDaysLastAOGDate; }
            set { this.noOfDaysLastAOGDate = value; }
        }

        public int NoOfDaysCourtDate
        {
            get { return this.noOfDaysCourtDate; }
            set { this.noOfDaysCourtDate = value; }
        }

    }
}
