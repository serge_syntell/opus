using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // OffenderTypeDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class OffenderTypeDetails 
	{
        public string OTCode;
		public string OTDescription;
    }

    //*******************************************************
    //
    // OffenderTypeDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query OffenderTypes within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class OffenderTypeDB {

		string mConstr = "";

		public OffenderTypeDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // OffenderTypeDB.GetOffenderTypeDetails() Method <a name="GetOffenderTypeDetails"></a>
        //
        // The GetOffenderTypeDetails method returns a OffenderTypeDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public OffenderTypeDetails GetOffenderTypeDetails(int otIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenderTypeDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterOTIntNo = new SqlParameter("@OTIntNo", SqlDbType.Int, 4);
            parameterOTIntNo.Value = otIntNo;
            myCommand.Parameters.Add(parameterOTIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			OffenderTypeDetails myOffenderTypeDetails = new OffenderTypeDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myOffenderTypeDetails.OTCode = result["OTCode"].ToString();
				myOffenderTypeDetails.OTDescription = result["OTDescription"].ToString();
			}
			result.Close();
            return myOffenderTypeDetails;
        }

		public OffenderTypeDetails GetOffenderTypeDetailsByCode(string otCode) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OffenderTypeDetailByCode", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterOTCode = new SqlParameter("@OTCode", SqlDbType.Char,1 );
			parameterOTCode.Value = otCode;
			myCommand.Parameters.Add(parameterOTCode);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			// Create CustomerDetails Struct
			OffenderTypeDetails myOffenderTypeDetails = new OffenderTypeDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myOffenderTypeDetails.OTCode = result["OTCode"].ToString();
				myOffenderTypeDetails.OTDescription = result["OTDescription"].ToString();
			}
			result.Close();
			return myOffenderTypeDetails;
		}

        //*******************************************************
        //
        // OffenderTypeDB.AddOffenderType() Method <a name="AddOffenderType"></a>
        //
        // The AddOffenderType method inserts a new menu record
        // into the menus database.  A unique "OffenderTypeId"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddOffenderType (string otCode, string otDescr,	 string lastUser) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenderTypeAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
			SqlParameter parameterOTCode = new SqlParameter("@OTCode", SqlDbType.VarChar, 20);
			parameterOTCode.Value = otCode;
			myCommand.Parameters.Add(parameterOTCode);

            SqlParameter parameterOTDescription = new SqlParameter("@OTDescription", SqlDbType.VarChar, 50);
			parameterOTDescription.Value = otDescr;
			myCommand.Parameters.Add(parameterOTDescription);
			
			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterOTIntNo = new SqlParameter("@OTIntNo", SqlDbType.Int, 4);
            parameterOTIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterOTIntNo);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int otIntNo = Convert.ToInt32(parameterOTIntNo.Value);

                return otIntNo;
            }
            catch (Exception e)
			{
				string msg = e.Message;
				myConnection.Dispose();
                return 0;
            }
        }

		public SqlDataReader GetOffenderTypeList()
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OffenderTypeList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		
		public DataSet GetOffenderTypeListDS()
		{
			SqlDataAdapter sqlDAOffenderTypes = new SqlDataAdapter();
			DataSet dsOffenderTypes = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAOffenderTypes.SelectCommand = new SqlCommand();
			sqlDAOffenderTypes.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAOffenderTypes.SelectCommand.CommandText = "OffenderTypeList";

			// Mark the Command as a SPROC
			sqlDAOffenderTypes.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Execute the command and close the connection
			sqlDAOffenderTypes.Fill(dsOffenderTypes);
			sqlDAOffenderTypes.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsOffenderTypes;		
		}

		
		public int UpdateOffenderType (int otIntNo, string otCode, string otDescr, string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OffenderTypeUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterOTCode = new SqlParameter("@OTCode", SqlDbType.VarChar, 20);
			parameterOTCode.Value = otCode;
			myCommand.Parameters.Add(parameterOTCode);

            SqlParameter parameterOTDescription = new SqlParameter("@OTDescription", SqlDbType.VarChar, 50);
			parameterOTDescription.Value = otDescr;
			myCommand.Parameters.Add(parameterOTDescription);
			
			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);
			
			SqlParameter parameterOTIntNo = new SqlParameter("@OTIntNo", SqlDbType.Int);
			parameterOTIntNo.Direction = ParameterDirection.InputOutput;
			parameterOTIntNo.Value = otIntNo;
			myCommand.Parameters.Add(parameterOTIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int OTIntNo = (int)myCommand.Parameters["@OTIntNo"].Value;
				//int menuId = (int)parameterOTIntNo.Value;

				return OTIntNo;
			}
			catch (Exception e)
			{
				string msg = e.Message;
				myConnection.Dispose();
				return 0;
			}
		}

		
		public String DeleteOffenderType (int otIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OffenderTypeDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterOTIntNo = new SqlParameter("@OTIntNo", SqlDbType.Int, 4);
			parameterOTIntNo.Value = otIntNo;
			parameterOTIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterOTIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int OTIntNo = (int)parameterOTIntNo.Value;

				return OTIntNo.ToString();
			}
			catch 
			{
				myConnection.Dispose();
				return String.Empty;
			}
		}
	}
}

