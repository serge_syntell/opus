using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


/* SD : 23.07.2008
 * Page : S56Bookissuedb.cs
 * Functionality : Datastructure to maintain the S56 Book Issue information
 */

namespace Stalberg.TMS
{
    // SD: 23.07.2008
    //*******************************************************
    //
    // S56 Book Issue Class
    //
    // A simple data class that encapsulates details about a particular loc 
    //
    //*******************************************************

    public partial class S56BookIssueDetails
    {
        public Int32 S56BIntNo;
        public Int32 TOIntNo;
        public Int32 S56BIIntNo;
        public Int32 S56BIStartNo;
        public Int32 S56BIEndNo;
        public DateTime S56BIIssueDate;
        public Int32 S56BILastNoUsed;
        public DateTime S56BILastUsedDate;
    }

    //*******************************************************
    //
    // S56 BookIssueDB Class
    //
    //*******************************************************

    public partial class S56BookIssueDB
    {
        string mConstr = "";

        public S56BookIssueDB(string vConstr)
        {
            mConstr = vConstr;
        }


        //dls 081125 - need a stored proc that gets the book issue details based on the last issue
        //can supply either the S56BIntNo (Book) or S56BIIntNo (Book Issue)

        public DataSet GetLastIssuedDetails(Int32 s56BIntNo, Int32 s56BIIntNo)
        {
            SqlDataAdapter sqlDAS56BookIssue = new SqlDataAdapter();
            DataSet dsS56BookIssue = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAS56BookIssue.SelectCommand = new SqlCommand();
            sqlDAS56BookIssue.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAS56BookIssue.SelectCommand.CommandText = "S56_GetLastIssuedDetails";

            // Mark the Command as a SPROC
            sqlDAS56BookIssue.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterS56BIntNo = new SqlParameter("@S56BIntNo", SqlDbType.Int, 4);
            parameterS56BIntNo.Value = s56BIntNo;
            sqlDAS56BookIssue.SelectCommand.Parameters.Add(parameterS56BIntNo);

            SqlParameter parameterS56BIIntNo = new SqlParameter("@S56BIIntNo", SqlDbType.Int, 4);
            parameterS56BIIntNo.Value = s56BIIntNo;
            sqlDAS56BookIssue.SelectCommand.Parameters.Add(parameterS56BIIntNo);

            // Execute the command and close the connection
            sqlDAS56BookIssue.Fill(dsS56BookIssue);
            sqlDAS56BookIssue.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsS56BookIssue;
        }

        public DataSet GetBookIssuedDetails(Int32 s56BIIntNo)
        {
            SqlDataAdapter sqlDAS56BookIssue = new SqlDataAdapter();
            DataSet dsS56BookIssue = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAS56BookIssue.SelectCommand = new SqlCommand();
            sqlDAS56BookIssue.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAS56BookIssue.SelectCommand.CommandText = "S56_BookIssueDetail";

            // Mark the Command as a SPROC
            sqlDAS56BookIssue.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterS56BIIntNo = new SqlParameter("@S56BIIntNo", SqlDbType.Int, 4);
            parameterS56BIIntNo.Value = s56BIIntNo;
            sqlDAS56BookIssue.SelectCommand.Parameters.Add(parameterS56BIIntNo);

            // Execute the command and close the connection
            sqlDAS56BookIssue.Fill(dsS56BookIssue);
            sqlDAS56BookIssue.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsS56BookIssue;
        }

        public DataSet GetBookIssuedList(Int32 autIntNo, Int32 s56BIntNo)
        {
            return GetBookIssuedList(autIntNo, s56BIntNo, 0);
        }

        public DataSet GetBookIssuedList(Int32 autIntNo, Int32 s56BIntNo, Int32 toIntNo)
        {
            SqlDataAdapter sqlDAS56BookIssue = new SqlDataAdapter();
            DataSet dsS56BookIssue = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAS56BookIssue.SelectCommand = new SqlCommand();
            sqlDAS56BookIssue.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAS56BookIssue.SelectCommand.CommandText = "S56_GetDetailsToGrid";

            // Mark the Command as a SPROC
            sqlDAS56BookIssue.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterS56BIntNo = new SqlParameter("@S56BIntNo", SqlDbType.Int, 4);
            parameterS56BIntNo.Value = s56BIntNo;
            sqlDAS56BookIssue.SelectCommand.Parameters.Add(parameterS56BIntNo);

            SqlParameter parameterTOIntNo = new SqlParameter("@TOIntNo", SqlDbType.Int, 4);
            parameterTOIntNo.Value = toIntNo;
            sqlDAS56BookIssue.SelectCommand.Parameters.Add(parameterTOIntNo);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDAS56BookIssue.SelectCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command and close the connection
            sqlDAS56BookIssue.Fill(dsS56BookIssue);
            sqlDAS56BookIssue.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsS56BookIssue;
        }

        //dls 081122 - change Start & End No's to numeric values
        // last used and last used date are not captured here
        // SD :24.07.2008
        // Functionality: A method used to Issue S56Book to Traffic officer  int s56BIntNo, int toIntNo,

        //public void AddS56BookIssue(int s56BIntNo,int toIntNo, DateTime s56BIIssueDate, string s56BIStartNo, string s56BIEndNo, 
        //                            string s56BILastNoUsed, DateTime s56BILastUsedDate)
        public int AddS56BookIssue(int s56BIntNo, int toIntNo, DateTime s56BIIssueDate, int s56BIStartNo,
            int s56BIEndNo, string lastUser, string s56Court, string s56CrtRoom, DateTime s56CrtDate, int noOfDaysSummonsToCDate,
            ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("S56_InsertBookIssue", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            //  Add Parameters to SPROC
            SqlParameter parameterS56BIntNo = new SqlParameter("@S56BIntNo", SqlDbType.Int, 4);
            parameterS56BIntNo.Value = s56BIntNo;
            myCommand.Parameters.Add(parameterS56BIntNo);

            SqlParameter parameterTOINo = new SqlParameter("@TOIntNo", SqlDbType.Int, 4);
            parameterTOINo.Value = toIntNo;
            myCommand.Parameters.Add(parameterTOINo);

            SqlParameter parameterS56BIDate = new SqlParameter("@S56BIIssueDate", SqlDbType.DateTime);
            parameterS56BIDate.Value = s56BIIssueDate;
            myCommand.Parameters.Add(parameterS56BIDate);

            SqlParameter parameterS56BStartNo = new SqlParameter("@S56BIStartNo", SqlDbType.Int, 4);
            parameterS56BStartNo.Value = s56BIStartNo;
            myCommand.Parameters.Add(parameterS56BStartNo);

            SqlParameter parameters56BendNo = new SqlParameter("@S56BIEndNo", SqlDbType.Int, 4);
            parameters56BendNo.Value = s56BIEndNo;
            myCommand.Parameters.Add(parameters56BendNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterCDS56Court = new SqlParameter("@S56BUCourtName", SqlDbType.VarChar, 50);
            parameterCDS56Court.Value = s56Court;
            myCommand.Parameters.Add(parameterCDS56Court);

            SqlParameter parameterCDS56CourtRoom = new SqlParameter("@S56BUCourtRoom", SqlDbType.VarChar, 50);
            parameterCDS56CourtRoom.Value = s56CrtRoom;
            myCommand.Parameters.Add(parameterCDS56CourtRoom);

            SqlParameter parameterS56CrtDate = new SqlParameter("@S56BUCourtDate", SqlDbType.DateTime);
            parameterS56CrtDate.Value = s56CrtDate;
            myCommand.Parameters.Add(parameterS56CrtDate);

            SqlParameter parameterNoOfDaysSummonsToCDate = new SqlParameter("@NoOfDaysSummonsToCDate", SqlDbType.Int, 4);
            parameterNoOfDaysSummonsToCDate.Value = noOfDaysSummonsToCDate;
            myCommand.Parameters.Add(parameterNoOfDaysSummonsToCDate);

            SqlParameter parameterS56BIIntNo = new SqlParameter("@S56BIIntNo", SqlDbType.Int, 4);
            parameterS56BIIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterS56BIIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the BookID using Output Param from SPROC
                int s56BIIntNo = Convert.ToInt32(parameterS56BIIntNo.Value);

                return s56BIIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                errMessage = e.Message;
                return 0;
            }
        }


        //    /// Court Allocation
        //    /// Date: 18.08.2008


        //    //public SqlDataReader PopulateBookIssuedList(int autIntNo)
        //    //{
        //    //    // Create Instance of Connection and Command Object
        //    //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    //    SqlCommand myCommand = new SqlCommand("S56_GetBookIssueByAutId", myConnection);

        //    //    // Mark the Command as a SPROC
        //    //    myCommand.CommandType = CommandType.StoredProcedure;

        //    //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    //    parameterAutIntNo.Value = autIntNo;
        //    //    myCommand.Parameters.Add(parameterAutIntNo);

        //    //    // Execute the command
        //    //    myConnection.Open();
        //    //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    //    // Return the data reader 
        //    //    return result;
        //    //}

        //    public S56BookIssueDetails GetBookIssueDetailsCourtAllocation(int s56BIntNo)
        //    {
        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(mConstr);
        //        //SqlCommand myCommand = new SqlCommand("S56_BookIssueDetail", myConnection);
        //        SqlCommand myCommand = new SqlCommand("S56_GetBookIssueDetailsCourtAllocation", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        // Add Parameters to SPROC
        //        SqlParameter parameters56BIntNo = new SqlParameter("@S56BIntNo", SqlDbType.Int, 4);
        //        parameters56BIntNo.Value = s56BIntNo;
        //        myCommand.Parameters.Add(parameters56BIntNo);

        //        myConnection.Open();
        //        SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
        //        S56BookIssueDetails myBookDetails = new S56BookIssueDetails();

        //        while (result.Read())
        //        {
        //            //myBookDetails.TODescr = result["TODescr"].ToString();
        //            //myBookDetails.S56BNoOfForms = Convert.ToInt32(result["S56BNoOfForms"]);
        //            //myBookDetails.S56BILastNoUsed = Convert.ToInt32(result["S56BILastNoUsed"].ToString());
        //            //myBookDetails.S56BIIssueDate = Convert.ToDateTime(result["S56BIIssueDate"]);
        //            //myBookDetails.S56BILastUsedDate = Convert.ToDateTime(result["S56BILastUsedDate"]);
        //        }
        //        result.Close();
        //        return myBookDetails;
        //    }

        //    public DataSet GetDetailsCADS(int crtIntNo, DateTime cDate, int  crtRIntNo)
        //    {
        //        //int iXofBrckt = crtRoom.IndexOf("(");
        //        //int ixofBrckt2 = crtRoom.IndexOf(")");
        //        //crtRoom = crtRoom.Substring(iXofBrckt + 1, ixofBrckt2-3);

        //        SqlDataAdapter sqlDAS56BookIssue = new SqlDataAdapter();
        //        DataSet dsS56BookIssue = new DataSet();

        //        // Create Instance of Connection and Command Object
        //        sqlDAS56BookIssue.SelectCommand = new SqlCommand();
        //        sqlDAS56BookIssue.SelectCommand.Connection = new SqlConnection(mConstr);
        //        sqlDAS56BookIssue.SelectCommand.CommandText = "S56_GetCourtDatesAfter65Days";

        //        // Mark the Command as a SPROC
        //        sqlDAS56BookIssue.SelectCommand.CommandType = CommandType.StoredProcedure;

        //        SqlParameter parameterCrtId = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
        //        parameterCrtId.Value = crtIntNo;
        //        sqlDAS56BookIssue.SelectCommand.Parameters.Add(parameterCrtId);


        //        SqlParameter parameterCDate = new SqlParameter("@CDate", SqlDbType.DateTime);
        //        parameterCDate.Value = cDate;
        //        sqlDAS56BookIssue.SelectCommand.Parameters.Add(parameterCDate);


        //        SqlParameter parameterCrtRoomId = new SqlParameter("@CrtRIntNo", SqlDbType.Int, 4);
        //        parameterCrtRoomId.Value = crtRIntNo;
        //        sqlDAS56BookIssue.SelectCommand.Parameters.Add(parameterCrtRoomId);

        //        // Execute the command and close the connection
        //        sqlDAS56BookIssue.Fill(dsS56BookIssue);
        //        sqlDAS56BookIssue.SelectCommand.Connection.Dispose();

        //        // Return the dataset result
        //        return dsS56BookIssue;
        //    }

        //    public S56BookIssueDetails GetCourtDetails(int cDIntNo)
        //    {
        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(mConstr);
        //        SqlCommand myCommand = new SqlCommand("S56_GetCourtDetailsByCourtIntNo", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        // Add Parameters to SPROC
        //        SqlParameter parametersCDIntNo = new SqlParameter("@CDIntNo", SqlDbType.Int, 4);
        //        parametersCDIntNo.Value = cDIntNo;
        //        myCommand.Parameters.Add(parametersCDIntNo);

        //        myConnection.Open();
        //        SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
        //        S56BookIssueDetails myBookDetails = new S56BookIssueDetails();

        //        while (result.Read())
        //        {
        //            //// Populate Struct using Output Params from SPROC
        //            //myBookDetails.CDate = Convert.ToDateTime( result["CDate"]);
        //            //myBookDetails.CDS56Reserved =Convert.ToInt16(result["CDS56Reserved"]);
        //            //myBookDetails.CDS56Allocated = Convert.ToInt16(result["CDS56Allocated"]);
        //            //myBookDetails.CDTotalUtilised = Convert.ToInt16(result["CDTotalUtilised"]);

        //        }
        //        result.Close();
        //        return myBookDetails;
        //    }

        //    public int UpdateCourtDates(int cDIntNo, Int16 cDS56Reserved)//
        //    {
        //        SqlConnection myConnection = new SqlConnection(mConstr);
        //        SqlCommand myCommand = new SqlCommand("S56_UpdateCourtDates", myConnection);

        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        SqlParameter parameterCDS56Reserved = new SqlParameter("@CDS56Reserved", SqlDbType.SmallInt);
        //        parameterCDS56Reserved.Value = cDS56Reserved;
        //        myCommand.Parameters.Add(parameterCDS56Reserved);

        //        SqlParameter parameterCDIntNo = new SqlParameter("@CDIntNo", SqlDbType.Int, 4);
        //        parameterCDIntNo.Direction = ParameterDirection.InputOutput;
        //        parameterCDIntNo.Value = cDIntNo;
        //        myCommand.Parameters.Add(parameterCDIntNo);


        //        try
        //        {
        //            myConnection.Open();
        //            myCommand.ExecuteNonQuery();
        //            myConnection.Dispose();

        //            cDIntNo = (int)myCommand.Parameters["@CDIntNo"].Value;
        //            return cDIntNo;

        //        }
        //        catch (Exception ex)
        //        {
        //            myConnection.Dispose();
        //            string msg = ex.Message;
        //            return 0;
        //        }
        //    }



        //    public void UpdateS56BookDates(string s56Court, string s56CrtRoom, DateTime s56CrtDate, DateTime s56SummonDate, int cDIntNo, int s56BIntNo)//
        //    {
        //        SqlConnection myConnection = new SqlConnection(mConstr);
        //        SqlCommand myCommand = new SqlCommand("S56_UpdateBookUsageCourtDates", myConnection);

        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        SqlParameter parameterCDS56Court = new SqlParameter("@S56BUCourtName", SqlDbType.VarChar,50);
        //        parameterCDS56Court.Value = s56Court;
        //        myCommand.Parameters.Add(parameterCDS56Court);

        //        SqlParameter parameterCDS56CourtRoom = new SqlParameter("@S56BUCourtRoom", SqlDbType.VarChar, 50);
        //        parameterCDS56CourtRoom.Value = s56CrtRoom;
        //        myCommand.Parameters.Add(parameterCDS56CourtRoom);

        //        SqlParameter parameterS56CrtDate = new SqlParameter("@S56BUCourtDate", SqlDbType.DateTime);
        //        parameterS56CrtDate.Value = s56CrtDate;
        //        myCommand.Parameters.Add(parameterS56CrtDate);

        //        SqlParameter parameterS56SummonDate = new SqlParameter("@S56BUSummonsCaptureDate", SqlDbType.DateTime);
        //        parameterS56SummonDate.Value = s56SummonDate;
        //        myCommand.Parameters.Add(parameterS56SummonDate);

        //        SqlParameter parameterCDIntNo = new SqlParameter("@CDIntNo", SqlDbType.Int, 4);
        //        //parameterCDIntNo.Direction = ParameterDirection.InputOutput;
        //        parameterCDIntNo.Value = cDIntNo;
        //        myCommand.Parameters.Add(parameterCDIntNo);

        //        SqlParameter parameterS56BIntNo = new SqlParameter("@S56BIntNo", SqlDbType.Int, 4);
        //        parameterS56BIntNo.Value = s56BIntNo;
        //        myCommand.Parameters.Add(parameterS56BIntNo);

        //        //SqlParameter parameterCDIntNo = new SqlParameter("@CDIntNo", SqlDbType.Int, 4);
        //        //parameterCDIntNo.Direction = ParameterDirection.Output;
        //        //parameterCDIntNo.Value = cDIntNo;
        //        //myCommand.Parameters.Add(parameterCDIntNo);

        //        try
        //        {
        //            myConnection.Open();
        //            myCommand.ExecuteNonQuery();
        //            myConnection.Dispose();

        //            //cDIntNo = (int)myCommand.Parameters["@CDIntNo"].Value;
        //            //return 0;

        //        }
        //        catch (Exception ex)
        //        {
        //            myConnection.Dispose();
        //            string msg = ex.Message;
        //            //return 0;
        //        }
        //    }



        //    public int AddS56BookUsage(int cDIntNo, int s56BIntNo)
        //    {
        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(mConstr);
        //        SqlCommand myCommand = new SqlCommand("S56_InsertBookUsageCDIntNo", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        // Add Parameters to SPROC
        //        SqlParameter parameterCDIntNo = new SqlParameter("@CDIntNo", SqlDbType.Int, 4);
        //        parameterCDIntNo.Value = cDIntNo;
        //        myCommand.Parameters.Add(parameterCDIntNo);

        //        SqlParameter parameterS56BIntNo = new SqlParameter("@S56BIntNo", SqlDbType.Int, 4);
        //        parameterS56BIntNo.Value = s56BIntNo;
        //        parameterS56BIntNo.Direction = ParameterDirection.InputOutput;
        //        myCommand.Parameters.Add(parameterS56BIntNo);

        //        try
        //        {
        //            myConnection.Open();
        //            myCommand.ExecuteNonQuery();
        //            myConnection.Dispose();

        //            s56BIntNo = (int)myCommand.Parameters["@S56BIntNo"].Value;
        //            return s56BIntNo;
        //        }
        //        catch (Exception e)
        //        {
        //            myConnection.Dispose();
        //            string msg = e.Message;
        //            return 0;
        //        }
        //    }


        //    public S56BookIssueDetails GetBookUsageDetails(int s56BIntNo)
        //    {
        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(mConstr);
        //        SqlCommand myCommand = new SqlCommand("S56_GetTotalIssuedFromS56BookUsage", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        // Add Parameters to SPROC
        //        SqlParameter parametersS56BIntNo = new SqlParameter("@S56BIntNo", SqlDbType.Int, 4);
        //        parametersS56BIntNo.Value = s56BIntNo;
        //        myCommand.Parameters.Add(parametersS56BIntNo);

        //        //SqlParameter parameterS56Status = new SqlParameter("@S56BUStatus", SqlDbType.Char, 1);
        //        //parameterS56Status.Value = s56Status;
        //        //myCommand.Parameters.Add(parameterS56Status);

        //        myConnection.Open();
        //        SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
        //        S56BookIssueDetails myBookDetails = new S56BookIssueDetails();

        //        while (result.Read())
        //        {
        //            // Populate Struct using Output Params from SPROC

        //           // myBookDetails.S56BUTotalIssued = Convert.ToInt16(result["TotalIssued"]);
        //        }
        //        result.Close();
        //        return myBookDetails;
        //    }

        public DataSet GetS56BookIssueListByLADS(int autIntNo)
        {
            SqlDataAdapter sqlDAS56BookIssue = new SqlDataAdapter();
            DataSet dsS56BookIssue = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAS56BookIssue.SelectCommand = new SqlCommand();
            sqlDAS56BookIssue.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAS56BookIssue.SelectCommand.CommandText = "S56_GetBookIssueByAutId";

            // Mark the Command as a SPROC
            sqlDAS56BookIssue.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDAS56BookIssue.SelectCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command and close the connection
            sqlDAS56BookIssue.Fill(dsS56BookIssue);
            sqlDAS56BookIssue.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsS56BookIssue;
        }

        public S56BookIssueDetails GetS56BookIssueDetailsDS(int editS56BIIntNo)
        {
            throw new NotImplementedException();
        }
    }
}

