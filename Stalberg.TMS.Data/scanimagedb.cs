using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Stalberg.TMS.Data;


namespace Stalberg.TMS
{

    //*******************************************************
    //
    // ScanImageDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class ScanList
    {
        public Int32 ScImIntNo;
        public string ImageDetails;
    }

    public partial class ScanImageDetails
    {
        public Int64 ScImIntNo;
        public Int64 FrameIntNo;
        public string ScImType; 
        public string JPegName;
        public int ScImPrintVal;
        public int XValue; 
		public int YValue;
        public byte[] ImageBytes;
        public ImageFileServerDetails FileServer;
        public string ImageFullPath;
    }

    //*******************************************************
    //
    // ScanImageDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query ScanImages within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class ScanImageDB
    {

        string mConstr = "";

        public ScanImageDB(string vConstr)
        {
            mConstr = vConstr;
        }

        //*******************************************************
        //
        // ScanImageDB.GetScanImageDetails() Method <a name="GetScanImageDetails"></a>
        //
        // The GetScanImageDetails method returns a ScanImageDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public ScanImageDetails GetScanImageDetails(int scImIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ScanImageDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterScImIntNo = new SqlParameter("@ScImIntNo", SqlDbType.Int, 4);
            parameterScImIntNo.Value = scImIntNo;
            myCommand.Parameters.Add(parameterScImIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            ScanImageDetails myScanImageDetails = new ScanImageDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myScanImageDetails.ScImIntNo = scImIntNo; 
                myScanImageDetails.FrameIntNo = Convert.ToInt32(result["FrameIntNo"]);
                myScanImageDetails.ScImType = result["ScImType"].ToString();
                myScanImageDetails.JPegName = result["JPegName"].ToString();
                myScanImageDetails.ScImPrintVal = Convert.ToInt32(result["ScImPrintVal"]);
                myScanImageDetails.XValue = Convert.ToInt32(result["XValue"]);
                myScanImageDetails.YValue  = Convert.ToInt32(result["YValue"]);       
            }
            result.Close();
            return myScanImageDetails;
        }

        //*******************************************************
        //
        // ScanImageDB.AddScanImage() Method <a name="AddScanImage"></a>
        //
        // The AddScanImage method inserts a new menu record
        // into the menus database.  A unique "ScanImageId"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddScanImage(int frameIntNo, string scImType,
            string jpegName, int scImPrintVal, int xValue, int yValue, string lastUser, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ScanImageAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Value = frameIntNo;
            myCommand.Parameters.Add(parameterFrameIntNo);

            SqlParameter parameterScImType = new SqlParameter("@ScImType", SqlDbType.Char, 1);
            parameterScImType.Value = scImType;
            myCommand.Parameters.Add(parameterScImType);

            SqlParameter parameterJPegName = new SqlParameter("@JPegName", SqlDbType.VarChar, 100);
            parameterJPegName.Value = jpegName;
            myCommand.Parameters.Add(parameterJPegName);

            SqlParameter parameterScImPrintVal = new SqlParameter("@ScImPrintVal", SqlDbType.Int);
            parameterScImPrintVal.Value = scImPrintVal;
            myCommand.Parameters.Add(parameterScImPrintVal);

            SqlParameter parameterXValue = new SqlParameter("@XValue", SqlDbType.Int);
            parameterXValue.Value = xValue;
            myCommand.Parameters.Add(parameterXValue);

            SqlParameter parameterYValue = new SqlParameter("@YValue", SqlDbType.Int, 4);
            parameterYValue.Value = yValue;
            myCommand.Parameters.Add(parameterYValue);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterScImIntNo = new SqlParameter("@ScImIntNo", SqlDbType.Int, 4);
            parameterScImIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterScImIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int scImIntNo = Convert.ToInt32(parameterScImIntNo.Value);

                return scImIntNo;
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                myConnection.Dispose();
                return 0;
            }
        }

        public Int32 CheckImageSize(Int32 scImIntNo, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ScanImageSize", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@ScImIntNo", SqlDbType.Int, 4).Value = scImIntNo;
            myCommand.Parameters.Add("@ImageSize", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                return Convert.ToInt32( myCommand.Parameters["@ImageSize"].Value);
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                myConnection.Dispose();
                return 0;
            }
        }

        public SqlDataReader GetScanImageList(int filmIntNo, int frameIntNo, string scImType)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ScanImageList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Value = filmIntNo;
            myCommand.Parameters.Add(parameterFilmIntNo);

            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Value = frameIntNo;
            myCommand.Parameters.Add(parameterFrameIntNo);

            SqlParameter parameterScImType = new SqlParameter("@ScImType", SqlDbType.Char, 1);
            parameterScImType.Value = scImType;
            myCommand.Parameters.Add(parameterScImType);

            //SqlParameter parameterJPegName = new SqlParameter("@JPegName", SqlDbType.VarChar, 20);
            //parameterJPegName.Value = jPegName;
            //myCommand.Parameters.Add(parameterJPegName);


            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        public SqlDataReader GetScanImagesForNotice(int notIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ScanImagesForNotice", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = notIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        public DataSet GetScanImagesForNoticeDS(int notIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlDataAdapter sqlDAScanImages = new SqlDataAdapter();
            DataSet dsScanImages = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAScanImages.SelectCommand = new SqlCommand();
            sqlDAScanImages.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAScanImages.SelectCommand.CommandText = "ScanImagesForNotice";

            // Mark the Command as a SPROC
            sqlDAScanImages.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = notIntNo;
            sqlDAScanImages.SelectCommand.Parameters.Add(parameterNotIntNo);

            // Execute the command and close the connection
            sqlDAScanImages.Fill(dsScanImages);
            sqlDAScanImages.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsScanImages;
        }

        public DataSet GetScanImageListDS(int frameIntNo, string scImType)
        {
            SqlDataAdapter sqlDAScanImages = new SqlDataAdapter();
            DataSet dsScanImages = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAScanImages.SelectCommand = new SqlCommand();
            sqlDAScanImages.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAScanImages.SelectCommand.CommandText = "ScanImageList";

            // Mark the Command as a SPROC
            sqlDAScanImages.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Value = frameIntNo;
            sqlDAScanImages.SelectCommand.Parameters.Add(parameterFrameIntNo);

            SqlParameter parameterScImType = new SqlParameter("@ScImType", SqlDbType.Char, 1);
            parameterScImType.Value = scImType;
            sqlDAScanImages.SelectCommand.Parameters.Add(parameterScImType);

            // Execute the command and close the connection
            sqlDAScanImages.Fill(dsScanImages);
            sqlDAScanImages.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsScanImages;
        }

        public DataSet GetScanImageListDS(int filmIntNo, int frameIntNo, string scImType)
        {
            SqlDataAdapter sqlDAScanImages = new SqlDataAdapter();
            DataSet dsScanImages = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAScanImages.SelectCommand = new SqlCommand();
            sqlDAScanImages.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAScanImages.SelectCommand.CommandText = "ScanImageList";

            // Mark the Command as a SPROC
            sqlDAScanImages.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Value = filmIntNo;
            sqlDAScanImages.SelectCommand.Parameters.Add(parameterFilmIntNo);

            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Value = frameIntNo;
            sqlDAScanImages.SelectCommand.Parameters.Add(parameterFrameIntNo);

            SqlParameter parameterScImType = new SqlParameter("@ScImType", SqlDbType.Char, 1);
            parameterScImType.Value = scImType;
            sqlDAScanImages.SelectCommand.Parameters.Add(parameterScImType);

            //SqlParameter parameterJPegName = new SqlParameter("@JPegName", SqlDbType.VarChar, 20);
            //parameterJPegName.Value = jPegName;
            //sqlDAScanImages.SelectCommand.Parameters.Add(parameterJPegName);

            // Execute the command and close the connection
            sqlDAScanImages.Fill(dsScanImages);
            sqlDAScanImages.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsScanImages;
        }

        public int UpdateScanImage(int scImIntNo, int frameIntNo, string scImType,
            string jpegName, int scImPrintVal, int xValue, int yValue, string lastUser, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ScanImageUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Value = frameIntNo;
            myCommand.Parameters.Add(parameterFrameIntNo);

            SqlParameter parameterScImType = new SqlParameter("@ScImType", SqlDbType.Char, 1);
            parameterScImType.Value = scImType;
            myCommand.Parameters.Add(parameterScImType);

            SqlParameter parameterJPegName = new SqlParameter("@JPegName", SqlDbType.VarChar, 100);
            parameterJPegName.Value = jpegName;
            myCommand.Parameters.Add(parameterJPegName);

            SqlParameter parameterScImPrintVal = new SqlParameter("@ScImPrintVal", SqlDbType.Int);
            parameterScImPrintVal.Value = scImPrintVal;
            myCommand.Parameters.Add(parameterScImPrintVal);

            SqlParameter parameterXValue = new SqlParameter("@XValue", SqlDbType.Int);
            parameterXValue.Value = xValue;
            myCommand.Parameters.Add(parameterXValue);

            SqlParameter parameterYValue = new SqlParameter("@YValue", SqlDbType.Int, 4);
            parameterYValue.Value = yValue;
            myCommand.Parameters.Add(parameterYValue);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterScImIntNo = new SqlParameter("@ScImIntNo", SqlDbType.Int);
            parameterScImIntNo.Direction = ParameterDirection.InputOutput;
            parameterScImIntNo.Value = scImIntNo;
            myCommand.Parameters.Add(parameterScImIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int ScImIntNo = (int)myCommand.Parameters["@ScImIntNo"].Value;
                //int menuId = (int)parameterScImIntNo.Value;

                return ScImIntNo;
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                myConnection.Dispose();
                return 0;
            }
        }


        public String DeleteScanImage(int scImIntNo, ref string errMessage)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ScanImageDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterScImIntNo = new SqlParameter("@ScImIntNo", SqlDbType.Int, 4);
            parameterScImIntNo.Value = scImIntNo;
            parameterScImIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterScImIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int ScImIntNo = (int)parameterScImIntNo.Value;

                return ScImIntNo.ToString();
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                errMessage = e.Message;
                return String.Empty;
            }
        }


        #region [Remove Images from Database]

        /// <summary>
        /// Get ScanImage details list for remove image by film
        /// </summary>
        /// <param name="frameIntNo">frame integer number</param>
        /// <returns>List of ScanImageDetails</returns>
        public List<ScanImageDetails> GetScanImageDetailsListForRomveImage(int frameIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ScanImageListGetDataForRemoveImages", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@FrameIntNo", SqlDbType.Int, 4).Value = frameIntNo;

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            List<ScanImageDetails> list = new List<ScanImageDetails>();

            while (result.Read())
            {
                ScanImageDetails myScanImageDetails = new ScanImageDetails();

                // Populate Struct using Output Params from SPROC
                myScanImageDetails.ScImIntNo = Convert.ToInt32(result["ScImIntNo"]);
                myScanImageDetails.ImageBytes = (result["ScanImage"] == DBNull.Value) ? null : (byte[])(result["ScanImage"]);
                myScanImageDetails.JPegName = result["JPegName"].ToString();

                list.Add(myScanImageDetails);
            }
            result.Close();
            return list;
        }

        /// <summary>
        /// update ScanImage value after remove all images by frame
        /// </summary>
        /// <param name="ScImIntNo"></param>
        /// <param name="exceptionMessage">out the database exception message</param>
        /// <returns></returns>
        // 2013-07-19 comment by Henry for useless
        //public int UpdateScanImageRemoved(int scImIntNo, out string exceptionMessage)
        //{
        //    exceptionMessage = string.Empty;
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ScanImageUpdateRemoved", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    myCommand.Parameters.Add("@ScImIntNo", SqlDbType.Int).Value = scImIntNo;

        //    try
        //    {
        //        myConnection.Open();
        //        int result = myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        return result;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        exceptionMessage = e.Message;
        //        return -1;
        //    }
        //}

        /// <summary>
        /// Update Frame Info After Remove Image for violation_loader
        /// </summary>
        /// <param name="scImIntNo"></param>
        /// <param name="iFSIntNo"></param>
        /// <param name="imagePath"></param>
        /// <param name="exceptionMessage"></param>
        /// <returns></returns>
        // 2013-07-26 add parameter lastUser by Henry
        public int UpdateFrameInfoAfterRemoveImage(int scImIntNo, int iFSIntNo, string imagePath, out string exceptionMessage, string lastUser)
        {
            exceptionMessage = string.Empty;
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("UpdateFrameInfoAfterRemoveImage", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@ScImIntNo", SqlDbType.Int, 4).Value = scImIntNo;
            myCommand.Parameters.Add("@IFSIntNo", SqlDbType.Int, 4).Value = iFSIntNo;
            myCommand.Parameters.Add("@ImagePath", SqlDbType.VarChar, 255).Value = imagePath;
            myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            try
            {
                myConnection.Open();
                int result = myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                return result;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                exceptionMessage = e.Message;
                return -1;
            }
        }

        /// <summary>
        /// Update Notice and Charge when missing images
        /// </summary>
        /// <param name="scImIntNo"></param>
        /// <param name="exceptionMessage"></param>
        /// <returns></returns>
        public int UpdateNoticeChargeMissImage(int scImIntNo, out string exceptionMessage, string lastUser)
        {
            exceptionMessage = string.Empty;
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeChargeUpdateMissImage", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@ScImIntNo", SqlDbType.Int).Value = scImIntNo;
            myCommand.Parameters.Add("@Lastuser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                myConnection.Open();
                int result = myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                return result;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                exceptionMessage = e.Message;
                return -1;
            }
        }


        #endregion

        /// <summary>
        /// Get Image full path by ScImIntNo
        /// </summary>
        /// <param name="scImIntNo"></param>
        /// <returns>ScanImageDetails instance contains full path and ImageFileServer info</returns>
        public ScanImageDetails GetImageFullPath(Int64 scImIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ScanImageGetFullPath", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@ScImIntNo", SqlDbType.Int).Value = scImIntNo;         

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            ScanImageDetails myScanImageDetails = new ScanImageDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myScanImageDetails.ScImIntNo = scImIntNo;

                myScanImageDetails.FileServer = new ImageFileServerDetails();
                myScanImageDetails.FileServer.IFSIntNo          = Convert.ToInt32(result["IFSIntNo"]);
                myScanImageDetails.FileServer.ImageMachineName  = result["ImageMachineName"].ToString();
                myScanImageDetails.FileServer.ImageShareName    = result["ImageShareName"].ToString();
                //myScanImageDetails.FileServer.RemoteUserName    = result["RemoteUserName"].ToString();
                //myScanImageDetails.FileServer.RemotePassword    = result["RemotePassword"].ToString();

                myScanImageDetails.FrameIntNo   = Convert.ToInt32(result["FrameIntNo"]);                
                myScanImageDetails.JPegName     = result["JPegName"].ToString();

                myScanImageDetails.ImageFullPath = @"\\" + myScanImageDetails.FileServer.ImageMachineName;
                myScanImageDetails.ImageFullPath += @"\" + myScanImageDetails.FileServer.ImageShareName;
                myScanImageDetails.ImageFullPath += @"\" + result["FrameImagePath"].ToString();
                myScanImageDetails.ImageFullPath += @"\" + myScanImageDetails.JPegName;                
            }
            result.Close();
            return myScanImageDetails;
        }

        public byte[] GetScanImageDataFromRemoteServer(int scImIntNo)
        {
            byte[] data = null;
            ScanImageDetails imageDetails = GetImageFullPath(scImIntNo);
            if (imageDetails.FileServer != null)
            {
                // DL 20100325 check ALL image servers accessible in the beginning. Remove check for the image when the notice is created.

                //bool isConnected = RemoteManager.RemoteConnect(
                //    imageDetails.FileServer.ImageMachineName,
                //    imageDetails.FileServer.ImageShareName,
                //    imageDetails.FileServer.RemoteUserName,
                //    imageDetails.FileServer.RemotePassword);

                //if (isConnected)
                //{
                    ImageProcesses imageProcesses = new ImageProcesses(mConstr);
                    data = imageProcesses.ProcessImage(imageDetails.ImageFullPath);                    
                //}
            }
            return data;
        }

        public byte[] GetScanImageDataFromRemoteServer(int scImIntNo, out ScanImageDetails imageDetails) {
            byte[] data = null;
            imageDetails = GetImageFullPath(scImIntNo);
            if (imageDetails.FileServer != null)
            {
                // DL 20100325 check ALL image servers accessible in the beginning. Remove check for the image when the notice is created.

                //bool isConnected = RemoteManager.RemoteConnect(
                //    imageDetails.FileServer.ImageMachineName,
                //    imageDetails.FileServer.ImageShareName,
                //    imageDetails.FileServer.RemoteUserName,
                //    imageDetails.FileServer.RemotePassword);

                //if (isConnected)
                //{
                ImageProcesses imageProcesses = new ImageProcesses(mConstr);
                data = imageProcesses.ProcessImage(imageDetails.ImageFullPath);
                //}
            }
            return data;
        }
    }
}


