using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    public partial class RegionDetails 
	{
		public Int32 RegIntNo;
        public string RegCode;
		public string RegName;
        public string RegTel;
    }

    public partial class RegionDB {

		string mConstr = "";

		public RegionDB (string vConstr)
		{
			mConstr = vConstr;
		}

        public RegionDetails GetRegionDetails(int regIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RegionDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterRegIntNo = new SqlParameter("@RegIntNo", SqlDbType.Int, 4);
            parameterRegIntNo.Value = regIntNo;
            myCommand.Parameters.Add(parameterRegIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			RegionDetails myRegionDetails = new RegionDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myRegionDetails.RegCode = result["RegCode"].ToString();
				myRegionDetails.RegName = result["RegName"].ToString();
                myRegionDetails.RegTel = result["RegTel"].ToString();
            }

			result.Close();
            return myRegionDetails;
        }

        public int AddRegion(string regCode, string regName, string regTel, string user) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RegionAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
			SqlParameter parameterRegCode = new SqlParameter("@RegCode", SqlDbType.Char, 3);
			parameterRegCode.Value = regCode;
			myCommand.Parameters.Add(parameterRegCode);
			
			SqlParameter parameterRegName = new SqlParameter("@RegName", SqlDbType.VarChar, 100);
			parameterRegName.Value = regName;
			myCommand.Parameters.Add(parameterRegName);

            SqlParameter parameterRegTel = new SqlParameter("@RegTel", SqlDbType.VarChar, 30);
            parameterRegTel.Value = regTel;
            myCommand.Parameters.Add(parameterRegTel);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar,50);
            parameterLastUser.Value = user;
            myCommand.Parameters.Add(parameterLastUser);
			
			SqlParameter parameterRegIntNo = new SqlParameter("@RegIntNo", SqlDbType.Int, 4);
            parameterRegIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterRegIntNo);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int addRegIntNo = Convert.ToInt32(parameterRegIntNo.Value);

                return addRegIntNo;
            }
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
        }
 
		public SqlDataReader GetRegionList(string orderBy,string lsCode)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("RegionList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterOrderBy = new SqlParameter("@OrderBy", SqlDbType.VarChar, 10);
			parameterOrderBy.Value = orderBy;
			myCommand.Parameters.Add(parameterOrderBy);

            SqlParameter parameterLsCode = new SqlParameter("@LsCode", SqlDbType.VarChar, 10);
            parameterLsCode.Value = lsCode;
            myCommand.Parameters.Add(parameterLsCode);
			
			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		public DataSet GetRegionListDS(string orderBy,string lsCode)
		{
			SqlDataAdapter sqlDARegion = new SqlDataAdapter();
			DataSet dsRegion = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDARegion.SelectCommand = new SqlCommand();
			sqlDARegion.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDARegion.SelectCommand.CommandText = "RegionList";

			// Mark the Command as a SPROC
			sqlDARegion.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterOrderBy = new SqlParameter("@OrderBy", SqlDbType.VarChar, 10);
			parameterOrderBy.Value = orderBy;
			sqlDARegion.SelectCommand.Parameters.Add(parameterOrderBy);

            SqlParameter parameterLsCode = new SqlParameter("@LsCode", SqlDbType.VarChar, 10);
            parameterLsCode.Value = lsCode;
            sqlDARegion.SelectCommand.Parameters.Add(parameterLsCode);

			// Execute the command and close the connection
			sqlDARegion.Fill(dsRegion);
			sqlDARegion.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsRegion;		
		}

        public int UpdateRegion(string regCode, string regName, string regTel, string user, int regIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("RegionUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC

			SqlParameter parameterRegCode = new SqlParameter("@RegCode", SqlDbType.Char, 3);
			parameterRegCode.Value = regCode;
			myCommand.Parameters.Add(parameterRegCode);

			SqlParameter parameterRegName = new SqlParameter("@RegName", SqlDbType.VarChar, 100);
			parameterRegName.Value = regName;
			myCommand.Parameters.Add(parameterRegName);

            SqlParameter parameterRegTel = new SqlParameter("@RegTel", SqlDbType.VarChar, 30);
            parameterRegTel.Value = regTel;
            myCommand.Parameters.Add(parameterRegTel);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = user;
            myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterRegIntNo = new SqlParameter("@RegIntNo", SqlDbType.Int, 4);
			parameterRegIntNo.Direction = ParameterDirection.InputOutput;
			parameterRegIntNo.Value = regIntNo;
			myCommand.Parameters.Add(parameterRegIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int updRegIntNo = (int)myCommand.Parameters["@RegIntNo"].Value;

				return updRegIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		public int DeleteRegion (int regIntNo, ref string errMessage)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("RegionDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterRegIntNo = new SqlParameter("@RegIntNo", SqlDbType.Int, 4);
			parameterRegIntNo.Value = regIntNo;
			parameterRegIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterRegIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int delRegIntNo = (int)parameterRegIntNo.Value;

				return delRegIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				errMessage = e.Message;
				return 0;
			}
		}
	}
}

