using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the details of a driver
    /// </summary>
    public class DriverDetails
    {
        // 2013-12-12, Oscar Changed Fields to Properties, and added Constructor
        public DriverDetails()
        {
            foreach (var pi in GetType().GetProperties())
            {
                if (pi.PropertyType == typeof(string))
                    pi.SetValue(this, string.Empty, null);
                else if (pi.PropertyType == typeof(DateTime))
                    pi.SetValue(this, DateTime.Now, null);
            }
        }

        public int DrvIntNo { get; set; }
        public int NotIntNo { get; set; }
        public string DrvSurname { get; set; }
        public string DrvInitials { get; set; }
        public string DrvForeNames { get; set; }
        public string DrvFullName { get; set; }
        public string DrvIDType { get; set; }
        public string DrvIDNumber { get; set; }
        public string DrvNationality { get; set; }
        public string DrvAge { get; set; }
        public string DrvPOAdd1 { get; set; }
        public string DrvPOAdd2 { get; set; }
        public string DrvPOAdd3 { get; set; }
        public string DrvPOAdd4 { get; set; }
        public string DrvPOAdd5 { get; set; }
        public string DrvPOCode { get; set; }
        public string DrvStAdd1 { get; set; }
        public string DrvStAdd2 { get; set; }
        public string DrvStAdd3 { get; set; }
        public string DrvStAdd4 { get; set; }
        public string DrvStCode { get; set; }
        public DateTime DrvDateCOA { get; set; }
        public string DrvLicenceCode { get; set; }
        public string DrvLicencePlace { get; set; }
        public string LastUser { get; set; }
        public string DrvCellNo { get; set; }
        public string DrvWorkNo { get; set; }
        public string DrvHomeNo { get; set; }
        public string DrvEmail { get; set; }
    }

    /// <summary>
    /// Contains all the database access for the Driver table
    /// </summary>
    public partial class DriverDB
    {
        // Fields
        private string mConstr = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="DriverDB"/> class.
        /// </summary>
        /// <param name="vConstr">The v constr.</param>
        public DriverDB(string vConstr)
        {
            this.mConstr = vConstr;
        }

        //*******************************************************
        //
        // The GetDriverDetails method returns a DriverDetails
        // struct that contains information about a specific transaction number
        //
        //*******************************************************

        public DriverDetails GetDriverDetails(int DrvIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("DriverDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterDrvIntNo = new SqlParameter("@DrvIntNo", SqlDbType.Int, 4);
            parameterDrvIntNo.Value = DrvIntNo;
            myCommand.Parameters.Add(parameterDrvIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            DriverDetails myDriverDetails = new DriverDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myDriverDetails.NotIntNo = Convert.ToInt32(result["NotIntNo"]);
                myDriverDetails.DrvIntNo = Convert.ToInt32(result["DrvIntNo"]);
                myDriverDetails.DrvSurname = result["DrvSurname"].ToString();
                myDriverDetails.DrvInitials = result["DrvInitials"].ToString();
                myDriverDetails.DrvIDType = result["DrvIDType"].ToString();
                myDriverDetails.DrvIDNumber = result["DrvIDNumber"].ToString();
                myDriverDetails.DrvNationality = result["DrvNationality"].ToString();
                myDriverDetails.DrvAge = result["DrvAge"].ToString();
                myDriverDetails.DrvPOAdd1 = result["DrvPoAdd1"].ToString();
                myDriverDetails.DrvPOAdd2 = result["DrvPoAdd2"].ToString();
                myDriverDetails.DrvPOAdd3 = result["DrvPoAdd3"].ToString();
                myDriverDetails.DrvPOAdd4 = result["DrvPoAdd4"].ToString();
                myDriverDetails.DrvPOAdd5 = result["DrvPoAdd5"].ToString();
                myDriverDetails.DrvPOCode = result["DrvPoCode"].ToString();
                myDriverDetails.DrvStAdd1 = result["DrvStAdd1"].ToString();
                myDriverDetails.DrvStAdd2 = result["DrvStAdd2"].ToString();
                myDriverDetails.DrvStAdd3 = result["DrvStAdd3"].ToString();
                myDriverDetails.DrvStAdd4 = result["DrvStAdd4"].ToString();
                myDriverDetails.DrvStCode = result["DrvStCode"].ToString();
                if (result["DrvDateCOA"] != System.DBNull.Value)
                    myDriverDetails.DrvDateCOA = Convert.ToDateTime(result["DrvDateCOA"]);
                myDriverDetails.DrvLicenceCode = result["DrvLicenceCode"].ToString();
                myDriverDetails.DrvLicencePlace = result["DrvLicencePlace"].ToString();
                myDriverDetails.LastUser = result["LastUser"].ToString();
                myDriverDetails.DrvForeNames = result["DrvForeNames"].ToString();
                myDriverDetails.DrvFullName = result["DrvFullName"].ToString();
                myDriverDetails.DrvCellNo = result["DrvCellNo"].ToString();
                myDriverDetails.DrvWorkNo = result["DrvWorkNo"].ToString();
                myDriverDetails.DrvHomeNo = result["DrvHomeNo"].ToString();
            }

            result.Close();
            return myDriverDetails;

        }

        public DriverDetails GetDriverDetailsByNotice(int NotIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("DriverDetailByNotice", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = NotIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            DriverDetails myDriverDetails = new DriverDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myDriverDetails.NotIntNo = Convert.ToInt32(result["NotIntNo"]);
                myDriverDetails.DrvIntNo = Convert.ToInt32(result["DrvIntNo"]);
                myDriverDetails.DrvSurname = result["DrvSurname"].ToString();
                myDriverDetails.DrvInitials = result["DrvInitials"].ToString();
                myDriverDetails.DrvIDType = result["DrvIDType"].ToString();
                myDriverDetails.DrvIDNumber = result["DrvIDNumber"].ToString();
                myDriverDetails.DrvNationality = result["DrvNationality"].ToString();
                myDriverDetails.DrvAge = result["DrvAge"].ToString();
                myDriverDetails.DrvPOAdd1 = result["DrvPoAdd1"].ToString();
                myDriverDetails.DrvPOAdd2 = result["DrvPoAdd2"].ToString();
                myDriverDetails.DrvPOAdd3 = result["DrvPoAdd3"].ToString();
                myDriverDetails.DrvPOAdd4 = result["DrvPoAdd4"].ToString();
                myDriverDetails.DrvPOAdd5 = result["DrvPoAdd5"].ToString();
                myDriverDetails.DrvPOCode = result["DrvPoCode"].ToString();
                myDriverDetails.DrvStAdd1 = result["DrvStAdd1"].ToString();
                myDriverDetails.DrvStAdd2 = result["DrvStAdd2"].ToString();
                myDriverDetails.DrvStAdd3 = result["DrvStAdd3"].ToString();
                myDriverDetails.DrvStAdd4 = result["DrvStAdd4"].ToString();
                myDriverDetails.DrvStCode = result["DrvStCode"].ToString();
                if (result["DrvDateCOA"] != System.DBNull.Value)
                    myDriverDetails.DrvDateCOA = Convert.ToDateTime(result["DrvDateCOA"]);
                myDriverDetails.DrvLicenceCode = result["DrvLicenceCode"].ToString();
                myDriverDetails.DrvLicencePlace = result["DrvLicencePlace"].ToString();
                myDriverDetails.LastUser = result["LastUser"].ToString();
                myDriverDetails.DrvForeNames = result["DrvForeNames"].ToString();
                myDriverDetails.DrvFullName = result["DrvFullName"].ToString();
                myDriverDetails.DrvCellNo = result["DrvCellNo"].ToString();
                myDriverDetails.DrvWorkNo = result["DrvWorkNo"].ToString();
                myDriverDetails.DrvHomeNo = result["DrvHomeNo"].ToString();
            }

            result.Close();
            return myDriverDetails;

        }

        //*******************************************************
        //
        // The AddDriver method inserts a new transaction number record
        // into the Driver database.  A unique "DrvIntNo"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddDriver(int notIntNo, string drvSurname, string drvInitials,
            string drvIDType, string drvIDNumber, string drvNationality,
            string drvAge, string drvPoAdd1, string drvPoAdd2, string drvPoAdd3,
            string drvPoAdd4, string drvPoAdd5, string drvPoCode, string drvStAdd1,
            string drvStAdd2, string drvStAdd3, string drvStAdd4, string drvStCode,
            DateTime drvDateCOA, string drvLicenceCode, string drvLicencePlace, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("DriverAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = notIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            SqlParameter parameterDrvSurname = new SqlParameter("@DrvSurname", SqlDbType.VarChar, 100);
            parameterDrvSurname.Value = drvSurname;
            myCommand.Parameters.Add(parameterDrvSurname);

            SqlParameter parameterDrvInitials = new SqlParameter("@DrvInitials", SqlDbType.VarChar, 10);
            parameterDrvInitials.Value = drvInitials;
            myCommand.Parameters.Add(parameterDrvInitials);

            SqlParameter parameterDrvIDType = new SqlParameter("@DrvIDType", SqlDbType.VarChar, 3);
            parameterDrvIDType.Value = drvIDType;
            myCommand.Parameters.Add(parameterDrvIDType);

            SqlParameter parameterDrvIDNumber = new SqlParameter("@DrvIDNumber", SqlDbType.VarChar, 25);
            parameterDrvIDNumber.Value = drvIDNumber;
            myCommand.Parameters.Add(parameterDrvIDNumber);

            SqlParameter parameterDrvNationality = new SqlParameter("@DrvNationality", SqlDbType.VarChar, 3);
            parameterDrvNationality.Value = drvNationality;
            myCommand.Parameters.Add(parameterDrvNationality);

            SqlParameter parameterDrvAge = new SqlParameter("@DrvAge", SqlDbType.VarChar, 3);
            parameterDrvAge.Value = drvAge;
            myCommand.Parameters.Add(parameterDrvAge);

            SqlParameter parameterDrvPoAdd1 = new SqlParameter("@DrvPoAdd1", SqlDbType.VarChar, 100);
            parameterDrvPoAdd1.Value = drvPoAdd1;
            myCommand.Parameters.Add(parameterDrvPoAdd1);

            SqlParameter parameterDrvPoAdd2 = new SqlParameter("@DrvPoAdd2", SqlDbType.VarChar, 100);
            parameterDrvPoAdd2.Value = drvPoAdd2;
            myCommand.Parameters.Add(parameterDrvPoAdd2);

            SqlParameter parameterDrvPoAdd3 = new SqlParameter("@DrvPoAdd3", SqlDbType.VarChar, 100);
            parameterDrvPoAdd3.Value = drvPoAdd3;
            myCommand.Parameters.Add(parameterDrvPoAdd3);

            SqlParameter parameterDrvPoAdd4 = new SqlParameter("@DrvPoAdd4", SqlDbType.VarChar, 100);
            parameterDrvPoAdd4.Value = drvPoAdd4;
            myCommand.Parameters.Add(parameterDrvPoAdd4);

            SqlParameter parameterDrvPoAdd5 = new SqlParameter("@DrvPoAdd5", SqlDbType.VarChar, 100);
            parameterDrvPoAdd5.Value = drvPoAdd5;
            myCommand.Parameters.Add(parameterDrvPoAdd5);

            SqlParameter parameterDrvPoCode = new SqlParameter("@DrvPoCode", SqlDbType.VarChar, 10);
            parameterDrvPoCode.Value = drvPoCode;
            myCommand.Parameters.Add(parameterDrvPoCode);

            SqlParameter parameterDrvStAdd1 = new SqlParameter("@DrvStAdd1", SqlDbType.VarChar, 100);
            parameterDrvStAdd1.Value = drvStAdd1;
            myCommand.Parameters.Add(parameterDrvStAdd1);

            SqlParameter parameterDrvStAdd2 = new SqlParameter("@DrvStAdd2", SqlDbType.VarChar, 100);
            parameterDrvStAdd2.Value = drvStAdd2;
            myCommand.Parameters.Add(parameterDrvStAdd2);

            SqlParameter parameterDrvStAdd3 = new SqlParameter("@DrvStAdd3", SqlDbType.VarChar, 100);
            parameterDrvStAdd3.Value = drvStAdd3;
            myCommand.Parameters.Add(parameterDrvStAdd3);

            SqlParameter parameterDrvStAdd4 = new SqlParameter("@DrvStAdd4", SqlDbType.VarChar, 100);
            parameterDrvStAdd4.Value = drvStAdd4;
            myCommand.Parameters.Add(parameterDrvStAdd4);

            SqlParameter parameterDrv = new SqlParameter("@DrvStCode", SqlDbType.VarChar, 10);
            parameterDrv.Value = drvStCode;
            myCommand.Parameters.Add(parameterDrv);

            SqlParameter parameterDrvDateCOA = new SqlParameter("@DrvDateCOA", SqlDbType.SmallDateTime);
            parameterDrvDateCOA.Value = drvDateCOA;
            myCommand.Parameters.Add(parameterDrvDateCOA);

            SqlParameter parameterDrvLicenceCode = new SqlParameter("@DrvLicenceCode", SqlDbType.VarChar, 3);
            parameterDrvLicenceCode.Value = drvLicenceCode;
            myCommand.Parameters.Add(parameterDrvLicenceCode);

            SqlParameter parameterDrvLicencePlace = new SqlParameter("@DrvLicencePlace", SqlDbType.VarChar, 50);
            parameterDrvLicencePlace.Value = drvLicencePlace;
            myCommand.Parameters.Add(parameterDrvLicencePlace);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterDrvIntNo = new SqlParameter("@DrvIntNo", SqlDbType.Int, 4);
            parameterDrvIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterDrvIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int DrvIntNo = Convert.ToInt32(parameterDrvIntNo.Value);

                return DrvIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public SqlDataReader GetDriverList(int notIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("DriverList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = notIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }


        public DataSet GetDriverListDS(int notIntNo)
        {
            SqlDataAdapter sqlDADrivers = new SqlDataAdapter();
            DataSet dsDrivers = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDADrivers.SelectCommand = new SqlCommand();
            sqlDADrivers.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDADrivers.SelectCommand.CommandText = "DriverList";

            // Mark the Command as a SPROC
            sqlDADrivers.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = notIntNo;
            sqlDADrivers.SelectCommand.Parameters.Add(parameterNotIntNo);

            // Execute the command and close the connection
            sqlDADrivers.Fill(dsDrivers);
            sqlDADrivers.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsDrivers;
        }

        // 2013-07-19 comment by Henry for useless
        //public int UpdateDriver(int drvIntNo, int notIntNo, string drvSurname, string drvInitials,
        //    string drvIDType, string drvIDNumber, string drvNationality,
        //    string drvAge, string drvPoAdd1, string drvPoAdd2, string drvPoAdd3,
        //    string drvPoAdd4, string drvPoAdd5, string drvPoCode, string drvStAdd1,
        //    string drvStAdd2, string drvStAdd3, string drvStAdd4, string drvStCode,
        //    string drvDateCOA, string drvLicenceCode, string drvLicencePlace, string lastUser, string drvForeNames)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("DriverUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
        //    parameterNotIntNo.Value = notIntNo;
        //    myCommand.Parameters.Add(parameterNotIntNo);

        //    SqlParameter parameterDrvSurname = new SqlParameter("@DrvSurname", SqlDbType.VarChar, 100);
        //    parameterDrvSurname.Value = drvSurname;
        //    myCommand.Parameters.Add(parameterDrvSurname);

        //    SqlParameter parameterDrvInitials = new SqlParameter("@DrvInitials", SqlDbType.VarChar, 10);
        //    parameterDrvInitials.Value = drvInitials;
        //    myCommand.Parameters.Add(parameterDrvInitials);

        //    SqlParameter parameterDrvIDType = new SqlParameter("@DrvIDType", SqlDbType.VarChar, 3);
        //    parameterDrvIDType.Value = drvIDType;
        //    myCommand.Parameters.Add(parameterDrvIDType);

        //    SqlParameter parameterDrvIDNumber = new SqlParameter("@DrvIDNumber", SqlDbType.VarChar, 25);
        //    parameterDrvIDNumber.Value = drvIDNumber;
        //    myCommand.Parameters.Add(parameterDrvIDNumber);

        //    SqlParameter parameterDrvForeNames = new SqlParameter("@DrvForeNames", SqlDbType.VarChar, 100);
        //    parameterDrvForeNames.Value = drvForeNames;
        //    myCommand.Parameters.Add(parameterDrvForeNames);

        //    SqlParameter parameterDrvNationality = new SqlParameter("@DrvNationality", SqlDbType.VarChar, 3);
        //    parameterDrvNationality.Value = drvNationality;
        //    myCommand.Parameters.Add(parameterDrvNationality);

        //    SqlParameter parameterDrvAge = new SqlParameter("@DrvAge", SqlDbType.VarChar, 3);
        //    parameterDrvAge.Value = drvAge;
        //    myCommand.Parameters.Add(parameterDrvAge);

        //    SqlParameter parameterDrvPoAdd1 = new SqlParameter("@DrvPoAdd1", SqlDbType.VarChar, 100);
        //    parameterDrvPoAdd1.Value = drvPoAdd1;
        //    myCommand.Parameters.Add(parameterDrvPoAdd1);

        //    SqlParameter parameterDrvPoAdd2 = new SqlParameter("@DrvPoAdd2", SqlDbType.VarChar, 100);
        //    parameterDrvPoAdd2.Value = drvPoAdd2;
        //    myCommand.Parameters.Add(parameterDrvPoAdd2);

        //    SqlParameter parameterDrvPoAdd3 = new SqlParameter("@DrvPoAdd3", SqlDbType.VarChar, 100);
        //    parameterDrvPoAdd3.Value = drvPoAdd3;
        //    myCommand.Parameters.Add(parameterDrvPoAdd3);

        //    SqlParameter parameterDrvPoAdd4 = new SqlParameter("@DrvPoAdd4", SqlDbType.VarChar, 100);
        //    parameterDrvPoAdd4.Value = drvPoAdd4;
        //    myCommand.Parameters.Add(parameterDrvPoAdd4);

        //    SqlParameter parameterDrvPoAdd5 = new SqlParameter("@DrvPoAdd5", SqlDbType.VarChar, 100);
        //    parameterDrvPoAdd5.Value = drvPoAdd5;
        //    myCommand.Parameters.Add(parameterDrvPoAdd5);

        //    SqlParameter parameterDrvPoCode = new SqlParameter("@DrvPoCode", SqlDbType.VarChar, 10);
        //    parameterDrvPoCode.Value = drvPoCode;
        //    myCommand.Parameters.Add(parameterDrvPoCode);

        //    SqlParameter parameterDrvStAdd1 = new SqlParameter("@DrvStAdd1", SqlDbType.VarChar, 100);
        //    parameterDrvStAdd1.Value = drvStAdd1;
        //    myCommand.Parameters.Add(parameterDrvStAdd1);

        //    SqlParameter parameterDrvStAdd2 = new SqlParameter("@DrvStAdd2", SqlDbType.VarChar, 100);
        //    parameterDrvStAdd2.Value = drvStAdd2;
        //    myCommand.Parameters.Add(parameterDrvStAdd2);

        //    SqlParameter parameterDrvStAdd3 = new SqlParameter("@DrvStAdd3", SqlDbType.VarChar, 100);
        //    parameterDrvStAdd3.Value = drvStAdd3;
        //    myCommand.Parameters.Add(parameterDrvStAdd3);

        //    SqlParameter parameterDrvStAdd4 = new SqlParameter("@DrvStAdd4", SqlDbType.VarChar, 100);
        //    parameterDrvStAdd4.Value = drvStAdd4;
        //    myCommand.Parameters.Add(parameterDrvStAdd4);

        //    SqlParameter parameterDrv = new SqlParameter("@DrvStCode", SqlDbType.VarChar, 10);
        //    parameterDrv.Value = drvStCode;
        //    myCommand.Parameters.Add(parameterDrv);

        //    SqlParameter parameterDrvDateCOA = new SqlParameter("@DrvDateCOA", SqlDbType.VarChar, 10);
        //    parameterDrvDateCOA.Value = drvDateCOA;
        //    myCommand.Parameters.Add(parameterDrvDateCOA);

        //    SqlParameter parameterDrvLicenceCode = new SqlParameter("@DrvLicenceCode", SqlDbType.VarChar, 3);
        //    parameterDrvLicenceCode.Value = drvLicenceCode;
        //    myCommand.Parameters.Add(parameterDrvLicenceCode);

        //    SqlParameter parameterDrvLicencePlace = new SqlParameter("@DrvLicencePlace", SqlDbType.VarChar, 50);
        //    parameterDrvLicencePlace.Value = drvLicencePlace;
        //    myCommand.Parameters.Add(parameterDrvLicencePlace);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterDrvIntNo = new SqlParameter("@DrvIntNo", SqlDbType.Int, 4);
        //    parameterDrvIntNo.Value = drvIntNo;
        //    parameterDrvIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterDrvIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int DrvIntNo = (int)myCommand.Parameters["@DrvIntNo"].Value;

        //        return DrvIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        /// <summary>
        /// Adds the new driver details and links them to the notice.
        /// </summary>
        /// <param name="driver">The driver.</param>
        // 2013-07-19 comment by Henry for useless
        //public void AddNewDriver(DriverDetails driver, int notIntNo)
        //{
        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("RepresentationNewDriver", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@DrvIntNo", SqlDbType.Int, 4).Value = driver.DrvIntNo;
        //    com.Parameters.Add("NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
        //    com.Parameters.Add("@Surname", SqlDbType.VarChar, 100).Value = driver.DrvSurname;
        //    com.Parameters.Add("@Forenames", SqlDbType.VarChar, 100).Value = driver.DrvForeNames;
        //    com.Parameters.Add("@Initials", SqlDbType.VarChar, 10).Value = driver.DrvInitials;
        //    com.Parameters.Add("@IDType", SqlDbType.VarChar, 3).Value = driver.DrvIDType;
        //    com.Parameters.Add("@IDNumber", SqlDbType.VarChar, 25).Value = driver.DrvIDNumber;
        //    com.Parameters.Add("@Nationality", SqlDbType.VarChar, 3).Value = driver.DrvNationality;
        //    com.Parameters.Add("@Age", SqlDbType.VarChar, 3).Value = driver.DrvAge;
        //    com.Parameters.Add("@PO1", SqlDbType.VarChar, 100).Value = driver.DrvPOAdd1;
        //    com.Parameters.Add("@PO2", SqlDbType.VarChar, 100).Value = driver.DrvPOAdd2;
        //    com.Parameters.Add("@PO3", SqlDbType.VarChar, 100).Value = driver.DrvPOAdd3;
        //    com.Parameters.Add("@PO4", SqlDbType.VarChar, 100).Value = driver.DrvPOAdd4;
        //    com.Parameters.Add("@PO5", SqlDbType.VarChar, 100).Value = driver.DrvPOAdd5;
        //    com.Parameters.Add("@POCode", SqlDbType.VarChar, 10).Value = driver.DrvPOCode;
        //    com.Parameters.Add("@Street1", SqlDbType.VarChar, 100).Value = driver.DrvStAdd1;
        //    com.Parameters.Add("@Street2", SqlDbType.VarChar, 100).Value = driver.DrvStAdd2;
        //    com.Parameters.Add("@Street3", SqlDbType.VarChar, 100).Value = driver.DrvStAdd3;
        //    com.Parameters.Add("@Street4", SqlDbType.VarChar, 100).Value = driver.DrvStAdd4;
        //    com.Parameters.Add("@StreetCode", SqlDbType.VarChar, 100).Value = driver.DrvStCode;
        //    com.Parameters.Add("@DrvWorkNo", SqlDbType.VarChar, 20).Value = driver.DrvWorkNo;
        //    com.Parameters.Add("@DrvCellNo", SqlDbType.VarChar, 20).Value = driver.DrvCellNo;
        //    com.Parameters.Add("@DrvHomeNo", SqlDbType.VarChar, 20).Value = driver.DrvHomeNo;

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();
        //    }
        //    finally
        //    {
        //        con.Close();
        //        con.Dispose();
        //        com.Dispose();
        //    }
        //}

    }
}
