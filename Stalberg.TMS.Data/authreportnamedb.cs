using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // AuthReportNameDetails Class
    //
    // A simple data class that encapsulates details about a particular loc 
    //
    //*******************************************************

    public partial class AuthReportNameDetails 
	{
		public Int32 ARNIntNo;
		public Int32 AutIntNo;
		public string ARNReportPage; 
		public string ARNFunction;
        public string ARNTemplate;
    }

    public partial class ReportAndTemplateNames
	{
		public List<string>  reports;     
        public List<string> templates;
    }

    //*******************************************************
    //
    // AuthReportNameDB Class
    //
    //*******************************************************

    public partial class AuthReportNameDB {

		string mConstr = "";

		public AuthReportNameDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        //*******************************************************

        public AuthReportNameDetails GetAuthReportNameDetails(int arnIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthReportNameDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterARNIntNo = new SqlParameter("@ARNIntNo", SqlDbType.Int, 4);
            parameterARNIntNo.Value = arnIntNo;
            myCommand.Parameters.Add(parameterARNIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			AuthReportNameDetails myAuthReportNameDetails = new AuthReportNameDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myAuthReportNameDetails.ARNIntNo = Convert.ToInt32(result["ARNIntNo"]);
				myAuthReportNameDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
				myAuthReportNameDetails.ARNReportPage = result["ARNReportPage"].ToString();
				myAuthReportNameDetails.ARNFunction = result["ARNFunction"].ToString();
                myAuthReportNameDetails.ARNTemplate = result["ARNTemplate"].ToString();
			}
			result.Close();
            return myAuthReportNameDetails;
        }

        public int AddAuthReportName (int autIntNo, string arnReportPage, string arnFunction, string lastUser, string arnTemplate) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthReportNameAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			myCommand.Parameters.Add(parameterAutIntNo);

			SqlParameter parameterARNReportPage = new SqlParameter("@ARNReportPage", SqlDbType.VarChar, 100);
			parameterARNReportPage.Value = arnReportPage;
			myCommand.Parameters.Add(parameterARNReportPage);

			SqlParameter parameterARNFunction = new SqlParameter("@ARNFunction", SqlDbType.VarChar, 30);
			parameterARNFunction.Value = arnFunction;
			myCommand.Parameters.Add(parameterARNFunction);

            SqlParameter parameterARNTemplate = new SqlParameter("@ARNTemplate", SqlDbType.VarChar, 100);
            parameterARNTemplate.Value = arnTemplate;
            myCommand.Parameters.Add(parameterARNTemplate);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);
			
			SqlParameter parameterARNIntNo = new SqlParameter("@ARNIntNo", SqlDbType.Int, 4);
            parameterARNIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterARNIntNo);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int arnIntNo = Convert.ToInt32(parameterARNIntNo.Value);

                return arnIntNo;
            }
            catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
                return 0;
            }
        }

        public string GetAuthReportName(int autIntNo, string arnFunction)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthReportNameDetailByFunction", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterARNFunction = new SqlParameter("@ARNFunction", SqlDbType.VarChar, 30);
            parameterARNFunction.Value = arnFunction;
            myCommand.Parameters.Add(parameterARNFunction);

            SqlParameter parameterARNReportPage = new SqlParameter("@ARNReportPage", SqlDbType.VarChar, 100);
            parameterARNReportPage.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterARNReportPage);

            try
            {
                myConnection.Open( );
                myCommand.ExecuteNonQuery( );

                string arnReportPage = parameterARNReportPage.Value.ToString( );

                return arnReportPage;
            }
            catch(Exception e)
            {
                string msg = e.Message;
                return "";
            }
            finally
            {
                myConnection.Close( );
                myConnection.Dispose( );
            }
        }

        public string GetAuthReportNameTemplate(int autIntNo, string arnFunction)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthReportTemplateByFunction", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterARNFunction = new SqlParameter("@ARNFunction", SqlDbType.VarChar, 30);
            parameterARNFunction.Value = arnFunction;
            myCommand.Parameters.Add(parameterARNFunction);

            SqlParameter parameterARNTemplate = new SqlParameter("@ARNTemplate", SqlDbType.VarChar, 100);
            parameterARNTemplate.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterARNTemplate);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                string arnTemplate = parameterARNTemplate.Value.ToString();

                return arnTemplate;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return "";
            }
        }
		
        //public SqlDataReader GetAuthReportNameList(int autIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("AuthReportNameList", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);
			
        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the datareader result
        //    return result;
        //}

        public SqlDataReader GetAuthReportNameTemplateList(int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthReportNameTemplateList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        public int CopyAutReportNames(int fromAutIntNo, int autIntNo, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthReportNamesCopy", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFromAutIntNo = new SqlParameter("@FromAutIntNo", SqlDbType.Int, 4);
            parameterFromAutIntNo.Value = fromAutIntNo;
            myCommand.Parameters.Add(parameterFromAutIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            parameterAutIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterAutIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                autIntNo = Convert.ToInt32(parameterAutIntNo.Value);

                return autIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

		// 20050715 converted to arnount
		public DataSet GetAuthReportNameListDS(int autIntNo, int pageSize, int pageIndex, out int totalCount)
		{
			SqlDataAdapter sqlDAAuthReportNames = new SqlDataAdapter();
			DataSet dsAuthReportNames = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAAuthReportNames.SelectCommand = new SqlCommand();
			sqlDAAuthReportNames.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAAuthReportNames.SelectCommand.CommandText = "AuthReportNameList";

			// Mark the Command as a SPROC
			sqlDAAuthReportNames.SelectCommand.CommandType = CommandType.StoredProcedure;

			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			sqlDAAuthReportNames.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int, 4);
            parameterPageSize.Value = pageSize;
            sqlDAAuthReportNames.SelectCommand.Parameters.Add(parameterPageSize);

            SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int, 4);
            parameterPageIndex.Value = pageIndex;
            sqlDAAuthReportNames.SelectCommand.Parameters.Add(parameterPageIndex);

            SqlParameter parameterTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int, 4);
            parameterTotalCount.Direction = ParameterDirection.Output;
            sqlDAAuthReportNames.SelectCommand.Parameters.Add(parameterTotalCount);

			// Execute the command and close the connection
			sqlDAAuthReportNames.Fill(dsAuthReportNames);
			sqlDAAuthReportNames.SelectCommand.Connection.Dispose();

            totalCount = (int)(parameterTotalCount.Value == DBNull.Value ? 0 : parameterTotalCount.Value);

			// Return the dataset result
			return dsAuthReportNames;		
		}

		// 20050715 converted to arnount
		public int UpdateAuthReportName (int arnIntNo, int autIntNo, string arnReportPage, string arnFunction, string lastUser,string temple) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("AuthReportNameUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			myCommand.Parameters.Add(parameterAutIntNo);

			SqlParameter parameterARNReportPage = new SqlParameter("@ARNReportPage", SqlDbType.VarChar, 100);
			parameterARNReportPage.Value = arnReportPage;
			myCommand.Parameters.Add(parameterARNReportPage);

			SqlParameter parameterARNFunction = new SqlParameter("@ARNFunction", SqlDbType.VarChar, 30);
			parameterARNFunction.Value = arnFunction;
			myCommand.Parameters.Add(parameterARNFunction);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterARNIntNo = new SqlParameter("@ARNIntNo", SqlDbType.Int);
			parameterARNIntNo.Direction = ParameterDirection.InputOutput;
			parameterARNIntNo.Value = arnIntNo;
			myCommand.Parameters.Add(parameterARNIntNo);

            SqlParameter parameterARNTemplate = new SqlParameter("@ARNTemplate", SqlDbType.VarChar,100);
            parameterARNTemplate.Value = temple;
            myCommand.Parameters.Add(parameterARNTemplate);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				arnIntNo = (int)myCommand.Parameters["@ARNIntNo"].Value;

				return arnIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

        // 20050715 converted to arnount
        public int UpdateAuthReportTemplate(int arnIntNo, int autIntNo, string arnTemplate, string arnFunction, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthReportTemplateUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterARNTemplate = new SqlParameter("@ARNTemplate", SqlDbType.VarChar, 100);
            parameterARNTemplate.Value = arnTemplate;
            myCommand.Parameters.Add(parameterARNTemplate);

            SqlParameter parameterARNFunction = new SqlParameter("@ARNFunction", SqlDbType.VarChar, 30);
            parameterARNFunction.Value = arnFunction;
            myCommand.Parameters.Add(parameterARNFunction);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterARNIntNo = new SqlParameter("@ARNIntNo", SqlDbType.Int);
            parameterARNIntNo.Direction = ParameterDirection.InputOutput;
            parameterARNIntNo.Value = arnIntNo;
            myCommand.Parameters.Add(parameterARNIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                arnIntNo = (int)myCommand.Parameters["@ARNIntNo"].Value;

                return arnIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }
		
		public String DeleteAuthReportName (int arnIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("AuthReportNameDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterARNIntNo = new SqlParameter("@ARNIntNo", SqlDbType.Int, 4);
			parameterARNIntNo.Value = arnIntNo;
			parameterARNIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterARNIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				arnIntNo = (int)parameterARNIntNo.Value;

				return arnIntNo.ToString();
			}
			catch (Exception e)
			{
				myConnection.Dispose();
                string msg = e.Message;
				return String.Empty;
			}
		}
        
        public ReportAndTemplateNames ReportList(string sAuthCode ,String sPath) 
        {
            ReportAndTemplateNames obj = new ReportAndTemplateNames();
            List<string> reports = new List<string>();
            List<string> templates = new List<string>(); 
            string sFileName = string.Empty;
            DirectoryInfo dir = new DirectoryInfo(sPath);
            string sCriteria = "Template";
            // for now we only want the template files - change this or add the sAuthCode to change search criteria
            FileInfo[] fi = dir.GetFiles("*" + sCriteria + "*");

            for (int n = 0; n < fi.Length; n++)
            {
                try
                {
                    sFileName = fi[n].FullName;
                    if (sFileName.ToLower().IndexOf("template") >= 0)
                    {
                        templates.Add(sFileName);
                    }
                    else
                    {
                        reports.Add(sFileName);
                    }
                    
                }
                catch (Exception ex)
                {
                    string sError = ex.Message;
                }
            }
            obj.reports = reports;
            obj.templates = templates;

            return obj;
        }
        
    }
}

