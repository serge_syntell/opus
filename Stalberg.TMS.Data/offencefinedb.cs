using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // OffenceFineDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class OffenceFineDetails 
	{
		public Int32 OffIntNo; 
		public Int32 AutIntNo; 
		public DateTime OFEffectiveDate;
		public double OFFineAmount;       
    }

    //*******************************************************
    //
    // OffenceFineDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query OffenceFines within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class OffenceFineDB {

		string mConstr = "";

		public OffenceFineDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // OffenceFineDB.GetOffenceFineDetails() Method <a name="GetOffenceFineDetails"></a>
        //
        // The GetOffenceFineDetails method returns a OffenceFineDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public OffenceFineDetails GetOffenceFineDetails(int ofIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenceFineDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterOFIntNo = new SqlParameter("@OFIntNo", SqlDbType.Int, 4);
            parameterOFIntNo.Value = ofIntNo;
            myCommand.Parameters.Add(parameterOFIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			OffenceFineDetails myOffenceFineDetails = new OffenceFineDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myOffenceFineDetails.OffIntNo = Convert.ToInt32(result["OffIntNo"]);
				myOffenceFineDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
				myOffenceFineDetails.OFEffectiveDate = Convert.ToDateTime(result["OFEffectiveDate"]);
				myOffenceFineDetails.OFFineAmount = Convert.ToDouble(result["OFFineAmount"]);
			}
			result.Close();
            return myOffenceFineDetails;
        }

        //*******************************************************
        //
        // OffenceFineDB.AddOffenceFine() Method <a name="AddOffenceFine"></a>
        //
        // The AddOffenceFine method inserts a new menu record
        // into the menus database.  A unique "OffenceFineId"
        // key is then returned from the method.  
        //
        //*******************************************************
        // 201307-25 add paramenter lastuser by Henry 
        public int AddOffenceFine (int offIntNo, int autIntNo, 
			DateTime ofEffectiveDate, double ofFineAmount, string lastUser)
			
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenceFineAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
			SqlParameter parameterOffIntNo = new SqlParameter("@OffIntNo", SqlDbType.Int, 4);
			parameterOffIntNo.Value = offIntNo;
			myCommand.Parameters.Add(parameterOffIntNo);

			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			myCommand.Parameters.Add(parameterAutIntNo);

			SqlParameter parameterOFEffectiveDate = new SqlParameter("@OFEffectiveDate", SqlDbType.SmallDateTime);
			parameterOFEffectiveDate.Value = ofEffectiveDate;
			myCommand.Parameters.Add(parameterOFEffectiveDate);

			SqlParameter parameterOFFineAmount = new SqlParameter("@OFFineAmount", SqlDbType.Real);
			parameterOFFineAmount.Value = ofFineAmount;
			myCommand.Parameters.Add(parameterOFFineAmount);

			SqlParameter parameterOFIntNo = new SqlParameter("@OFIntNo", SqlDbType.Int, 4);
            parameterOFIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterOFIntNo);

            // 2013-09-02 add by Henry
            myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int ofIntNo = Convert.ToInt32(parameterOFIntNo.Value);

                return ofIntNo;
            }
            catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
                return 0;
            }
        }

		public SqlDataReader GetOffenceFineByGroupList(int autIntNo, int ogIntNo, string search)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OffenceFineListByGroup", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int, 4);
			parameterOGIntNo.Value = ogIntNo;
			myCommand.Parameters.Add(parameterOGIntNo);

			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			myCommand.Parameters.Add(parameterAutIntNo);

			SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
			parameterSearch.Value = search;
			myCommand.Parameters.Add(parameterSearch);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		public SqlDataReader GetOffenceFineByOffenceList(int autIntNo, int offIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OffenceFineListByOffence", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			SqlParameter parameterOffIntNo = new SqlParameter("@OffIntNo", SqlDbType.Int, 4);
			parameterOffIntNo.Value = offIntNo;
			myCommand.Parameters.Add(parameterOffIntNo);

			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			myCommand.Parameters.Add(parameterAutIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		
		public DataSet GetOffenceFineListByGroupDS(int autIntNo, int ogIntNo, string search)
		{
			SqlDataAdapter sqlDAOffenceFines = new SqlDataAdapter();
			DataSet dsOffenceFines = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAOffenceFines.SelectCommand = new SqlCommand();
			sqlDAOffenceFines.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAOffenceFines.SelectCommand.CommandText = "OffenceFineListByGroup";

			// Mark the Command as a SPROC
			sqlDAOffenceFines.SelectCommand.CommandType = CommandType.StoredProcedure;

			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			sqlDAOffenceFines.SelectCommand.Parameters.Add(parameterAutIntNo);

			SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int, 4);
			parameterOGIntNo.Value = ogIntNo;
			sqlDAOffenceFines.SelectCommand.Parameters.Add(parameterOGIntNo);

			SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
			parameterSearch.Value = search;
			sqlDAOffenceFines.SelectCommand.Parameters.Add(parameterSearch);

			// Execute the command and close the connection
			sqlDAOffenceFines.Fill(dsOffenceFines);
			sqlDAOffenceFines.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsOffenceFines;		
		}

		public DataSet GetOffenceFineListByOffenceDS(int autIntNo, int offIntNo)
		{
			SqlDataAdapter sqlDAOffenceFines = new SqlDataAdapter();
			DataSet dsOffenceFines = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAOffenceFines.SelectCommand = new SqlCommand();
			sqlDAOffenceFines.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAOffenceFines.SelectCommand.CommandText = "OffenceFineListByOffence";

			// Mark the Command as a SPROC
			sqlDAOffenceFines.SelectCommand.CommandType = CommandType.StoredProcedure;

			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			sqlDAOffenceFines.SelectCommand.Parameters.Add(parameterAutIntNo);

			SqlParameter parameterOffIntNo = new SqlParameter("@OffIntNo", SqlDbType.Int, 4);
			parameterOffIntNo.Value = offIntNo;
			sqlDAOffenceFines.SelectCommand.Parameters.Add(parameterOffIntNo);

			// Execute the command and close the connection
			sqlDAOffenceFines.Fill(dsOffenceFines);
			sqlDAOffenceFines.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsOffenceFines;		
		}

		
		public int UpdateOffenceFine (int ofIntNo, 
			//int offIntNo, int autIntNo, 
			DateTime ofEffectiveDate, double ofFineAmount, string lastUser) 
		{
			//removing authority and Offence from the update stmt - they should not change!!!
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OffenceFineUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
//			SqlParameter parameterOffIntNo = new SqlParameter("@OffIntNo", SqlDbType.Int, 4);
//			parameterOffIntNo.Value = offIntNo;
//			myCommand.Parameters.Add(parameterOffIntNo);
//
//			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
//			parameterAutIntNo.Value = autIntNo;
//			myCommand.Parameters.Add(parameterAutIntNo);
			
			SqlParameter parameterOFEffectiveDate = new SqlParameter("@OFEffectiveDate", SqlDbType.SmallDateTime);
			parameterOFEffectiveDate.Value = ofEffectiveDate;
			myCommand.Parameters.Add(parameterOFEffectiveDate);

			SqlParameter parameterOFFineAmount = new SqlParameter("@OFFineAmount", SqlDbType.Real);
			parameterOFFineAmount.Value = ofFineAmount;
			myCommand.Parameters.Add(parameterOFFineAmount);

			SqlParameter parameterOFIntNo = new SqlParameter("@OFIntNo", SqlDbType.Int);
			parameterOFIntNo.Direction = ParameterDirection.InputOutput;
			parameterOFIntNo.Value = ofIntNo;
			myCommand.Parameters.Add(parameterOFIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int OFIntNo = (int)myCommand.Parameters["@OFIntNo"].Value;
				//int menuId = (int)parameterOFIntNo.Value;

				return OFIntNo;
			}
			catch 
			{
				myConnection.Dispose();
				return 0;
			}
		}

		
		public String DeleteOffenceFine (int ofIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OffenceFineDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterOFIntNo = new SqlParameter("@OFIntNo", SqlDbType.Int, 4);
			parameterOFIntNo.Value = ofIntNo;
			parameterOFIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterOFIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int OFIntNo = (int)parameterOFIntNo.Value;

				return OFIntNo.ToString();
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return String.Empty;
			}
		}

        // added LMZ to cater for call to stored proc UpdateOffenceFineByDate - 2007-01-11
        //public String UpdateSpecial(string sAutNo, string sEffectiveDate, string sUpdateToDate , string sOGCode)
        // 2013-07-25 add parameter lastUser by Henry
        //2013-12-18 Heidi added successRecordCount parameter for add all Punch Statistics Transaction(5084)
        //public String UpdateSpecial(string sAutNo, string sEffectiveDate, string sUpdateToDate, int ogIntNo, string lastUser)
        public String UpdateSpecial(string sAutNo, string sEffectiveDate, string sUpdateToDate, int ogIntNo, string lastUser, out int successRecordCount)
        {
            successRecordCount = 0;
            try
            {
                DateTime dtEffectiveDate;
                DateTime dtUpdateToDate;
                DateTime.TryParse(sEffectiveDate, out dtEffectiveDate);
                DateTime.TryParse(sUpdateToDate, out dtUpdateToDate);

                SqlConnection con = new SqlConnection(mConstr);
                SqlCommand com = new SqlCommand("OffenceFineUpdateByDate", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("@AutIntNo", SqlDbType.Int).Value = int.Parse(sAutNo);
                //com.Parameters.Add("@InCode", SqlDbType.VarChar, 6).Value = sOGCode;
                com.Parameters.Add("@OGIntNo", SqlDbType.Int).Value = ogIntNo;
                com.Parameters.Add("@InDate", SqlDbType.SmallDateTime).Value = dtEffectiveDate;
                com.Parameters.Add("@InUpdateTo", SqlDbType.SmallDateTime).Value = dtUpdateToDate;
                com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

                int nValue;
                try
                {
                    con.Open();
                    nValue = com.ExecuteNonQuery();
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                    com.Dispose();
                }
                
                var refHelper = Stalberg.TMS.Data.Util.RefHelper.GetSingleton();
                string resultStr = string.Format(refHelper.GetResource("OffenceFine.aspx", "resultStr", "Updated {0} rows succesfully"), nValue);
                successRecordCount = nValue;
                return resultStr ;
            }
            catch (Exception e)
            {
                return "Unable to update offence fine table " + e.Message;
            }
        }

	}
}

