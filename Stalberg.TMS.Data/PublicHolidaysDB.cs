using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the data of a public holiday from the database.
    /// </summary>
    public class PublicHoliday
    {
        // Fields
        private int id;
        private DateTime? date;
        private string description = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="PublicHoliday"/> class.
        /// </summary>
        public PublicHoliday()
        {
            this.id = -1;
            this.date = new Nullable<DateTime>();
            this.description = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PublicHoliday"/> class.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="dt">The dt.</param>
        public PublicHoliday(int id, DateTime dt)
        {
            this.id = id;
            this.date = dt;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PublicHoliday"/> class.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="dt">The dt.</param>
        /// <param name="description">The description of the holiday.</param>
        public PublicHoliday(int id, DateTime dt, string description)
        {
            this.id = id;
            this.date = dt;
            this.description = description;
        }

        /// <summary>
        /// Gets or sets the dataabse ID of the holiday.
        /// </summary>
        /// <value>The ID.</value>
        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Gets or sets the date of the holiday.
        /// </summary>
        /// <value>The date.</value>
        public DateTime? Date
        {
            get { return this.date; }
            set { this.date = value; }
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is new.
        /// </summary>
        /// <value><c>true</c> if this instance is new; otherwise, <c>false</c>.</value>
        public bool IsNew
        {
            get { return (this.id == -1); }
        }

    }

    /// <summary>
    /// Represents an object that can interact wuth the database concerning Public Holidays.
    /// </summary>
    public class PublicHolidaysDB
    {
        // Fields
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="PublicHolidaysDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public PublicHolidaysDB(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Gets a list of holidays for a sepcific year.
        /// </summary>
        /// <param name="year">The year to gt holidays for.</param>
        /// <returns>A List of holidays</returns>
        public List<PublicHoliday> GetHolidays(int year)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("PublicHolidaysForYear", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@Year", SqlDbType.Int, 4).Value = year;

            List<PublicHoliday> holidays = new List<PublicHoliday>();

            con.Open();
            SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection);
            while (reader.Read())
            {
                PublicHoliday holiday = new PublicHoliday((int)reader["PHIntNo"], (DateTime)reader["PHDate"]);
                if (reader["PHDescr"] != DBNull.Value)
                    holiday.Description = (string)reader["PHDescr"];

                holidays.Add(holiday);
            }
            reader.Close();
            con.Close();

            return holidays;
        }


        /// <summary>
        /// Gets a holiday for a specific day.
        /// </summary>
        /// <param name="dt">The date to check on.</param>
        /// <returns>A <see cref="PublicHoliday"/></returns>
        public PublicHoliday GetHoliday(DateTime dt)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("PublicHolidayByDate", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@Date", SqlDbType.SmallDateTime, 4).Value = dt;

            PublicHoliday holiday = new PublicHoliday();

            con.Open();
            SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection);
            while (reader.Read())
            {
                holiday = new PublicHoliday((int)reader["PHIntNo"], (DateTime)reader["PHDate"]);
                if (reader["PHDescr"] != DBNull.Value)
                    holiday.Description = (string)reader["PHDescr"];
            }
            reader.Close();
            con.Close();

            return holiday;
        }
        // 2013-07-29 add parameter lastUser by Henry
        public void UpdateHoliday(PublicHoliday holiday, string lastUser)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("PublicHolidayUpdate", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ID", SqlDbType.Int, 4).Value = holiday.ID;
            com.Parameters.Add("@Date", SqlDbType.SmallDateTime, 4).Value = holiday.Date.Value;
            if (holiday.Description.Length > 0)
                com.Parameters.Add("@Descr", SqlDbType.VarChar, 100).Value = holiday.Description;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;    // 2013-07-29 add by Henry
            try
            {
                con.Open();
                holiday.ID = (int)com.ExecuteScalar();
            }
            finally
            {
                con.Close();
            }
            
        }

        /// <summary>
        /// Deletes the holiday from the list.
        /// </summary>
        /// <param name="id">The database id of the holiday.</param>
        /// <returns></returns>
        public bool DeleteHoliday(int id)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("PublicHolidayDelete", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ID", SqlDbType.Int, 4).Value = id;

            try
            {
                con.Open();
                return (com.ExecuteNonQuery() == 1);
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Copies the holidays from one year to another.
        /// </summary>
        /// <param name="fromYear">Copy holidays from this year.</param>
        /// <param name="toYear">Paste them to this year.</param>
        // 2013-07-29 add parameter lastUser by Henry
        public void CopyHolidays(int fromYear, int toYear, string lastUser)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("PublicHolidayCopyPaste", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@From", SqlDbType.Int, 4).Value = fromYear;
            com.Parameters.Add("@To", SqlDbType.Int, 4).Value = toYear;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Gets the next working day after the one supplied if it is a weekend or a public holiday.
        /// </summary>
        /// <param name="date">The date to check.</param>
        /// <returns>A <see cref="DateTime"/></returns>
        public DateTime GetNextWorkingDay(DateTime date)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SELECT dbo.fnNextWorkingDay(@Date);", con);
            com.CommandType = CommandType.Text;
            com.Parameters.Add("@Date", SqlDbType.SmallDateTime, 4).Value = date;

            try
            {
                con.Open();
                return (DateTime)com.ExecuteScalar();
            }
            finally
            {
                con.Close();
            }
        }

        public string CheckWeekDayOrPublicHoliday(DateTime date)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SELECT dbo.fnCheckWorkingDay(@Date);", con);
            com.CommandType = CommandType.Text;
            com.Parameters.Add("@Date", SqlDbType.SmallDateTime, 4).Value = date;

            try
            {
                con.Open();
                return (string)com.ExecuteScalar();

            }
            finally
            {
                con.Close();
            }
        }

    }
}
