using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the details of a court transaction
    /// </summary>
    public class CourtTranDetails
    {
        // Fields
        private int crtIntNo = -1;
        private int ctIntNo = -1;
        private string type = string.Empty;
        private int currentNumber = 0;
        private string description = string.Empty;
        //Jerry 2012-05-07 add
        private int ctYear = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="CourtTranDetails"/> class.
        /// </summary>
        public CourtTranDetails()
        {
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        /// <summary>
        /// Gets or sets the current number.
        /// </summary>
        /// <value>The current number.</value>
        public int CurrentNumber
        {
            get { return this.currentNumber; }
            set { this.currentNumber = value; }
        }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        public string Type
        {
            get { return this.type; }
            set { this.type = value; }
        }

        /// <summary>
        /// Gets or sets the court transaction int no.
        /// </summary>
        /// <value>The court transaction int no.</value>
        public int CtIntNo
        {
            get { return this.ctIntNo; }
            set { this.ctIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the authority court int no.
        /// </summary>
        /// <value>The authority court int no.</value>
        public int CourtIntNo
        {
            get { return this.crtIntNo; }
            set { this.crtIntNo = value; }

        }

        //Jerry 2012-05-07 add
        public int CTYear
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Represents an object that interacts with the database around court case number transactions.
    /// </summary>
    public class CourtTranDB
    {
        // Fields
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="CourtTranDB"/> class.
        /// </summary>
        /// <param name="connectionString">The database connection string.</param>
        public CourtTranDB(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Gets the next case number for a court.
        /// </summary>
        /// <param name="crtIntNo">The Court int no.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns>
        /// The next case number for the court.
        /// </returns>
        // 2013-07-19 comment by Henry for useless
        //public int GetNextCaseNumberForCourt(int crtIntNo, string lastUser)
        //{
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("CourtTranNext", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.Parameters.Add("@CrtIntNo", SqlDbType.Int, 4).Value = crtIntNo;
        //    com.Parameters.Add("@CTType", SqlDbType.VarChar, 3).Value = "CAS";
        //    com.Parameters.Add("@CTDescr", SqlDbType.VarChar, 50).Value = "Case Number, per court, for served summons.";
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

        //    try
        //    {
        //        con.Open();
        //        return (int)com.ExecuteScalar();
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        /// <summary>
        /// Gets the next case number for a court.
        /// </summary>
        /// <param name="crtIntNo">The Court int no.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns>
        /// The next case number for the court.
        /// </returns>
        public string GetNextSummonsNumber(string sTicketNo, string lastUser, ref string errMessage, Int32 crtIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SummonsTranNext", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = sTicketNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@CTType", SqlDbType.VarChar, 3).Value = "SUM";
            com.Parameters.Add("@CTDescr", SqlDbType.VarChar, 50).Value = "Summons sequence number";
            com.Parameters.Add("@CrtIntNo", SqlDbType.Int, 4).Value = crtIntNo;
            try
            {
                con.Open();
                return (string)com.ExecuteScalar();
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return "-1";
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Gets the court transaction list for an authority.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetCourtTranListDS(int crtIntNo, int pageSize, int pageIndex, out int totalCount)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CourtTranList", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@CrtIntNo", SqlDbType.Int, 4).Value = crtIntNo;

            com.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            com.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            com.Parameters.Add(paraTotalCount);

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            return ds;
        }

        /// <summary>
        /// Gets the details of a Court transaction.
        /// </summary>
        /// <param name="ctIntNo">The court transaction int no.</param>
        /// <returns>A <see cref="CourtTranDetails"/></returns>
        public CourtTranDetails GetDetails(int ctIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CourtTranDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@CTIntNo", SqlDbType.Int, 4).Value = ctIntNo;

            try
            {
                CourtTranDetails detail = new CourtTranDetails();

                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                if (reader.Read())
                {
                    detail.CourtIntNo = (int)reader["CrtIntNo"];
                    detail.CtIntNo = (int)reader["CTIntNo"];
                    detail.CurrentNumber = (int)reader["CTNumber"];
                    detail.Description = (string)reader["CTDescr"];
                    detail.Type = (string)reader["CTType"];
                    //Jerry 2012-05-07 add
                    detail.CTYear = Convert.ToInt32(reader["CTYear"]);
                }
                reader.Close();

                return detail;
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Updates the court transaction details.
        /// </summary>
        /// <param name="details">The details.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns>
        /// 	<c>true</c> if the update was successfull, otherwise <c>false</c>
        /// </returns>
        public bool UpdateCourtTranDetails(CourtTranDetails details, string lastUser)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CourtTranDetailsUpdate", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@CTIntNo", SqlDbType.Int, 4).Value = details.CtIntNo;
            com.Parameters.Add("@CrtIntNo", SqlDbType.Int, 4).Value = details.CourtIntNo;
            com.Parameters.Add("@CTType", SqlDbType.VarChar, 3).Value = details.Type;
            com.Parameters.Add("@CTNumber", SqlDbType.Int, 4).Value = details.CurrentNumber;
            com.Parameters.Add("@CTDescr", SqlDbType.VarChar, 50).Value = details.Description;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            //Jerry 2012-05-15 add
            com.Parameters.Add("@CTYear", SqlDbType.Int, 4).Value = details.CTYear;

            try
            {
                con.Open();
                int rowsAffected = com.ExecuteNonQuery();
                return (rowsAffected == 1);
            }
            catch
            {
                return false;
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Captures the summons served data.
        /// </summary>
        /// <param name="crIntNo">The case register int no.</param>
        /// <param name="sssIntNo">The Summons Server Staff member int no.</param>
        /// <param name="dateServed">The date served.</param>
        /// <param name="method">The method.</param>
        /// <param name="caseNo">The case no.</param>
        /// <param name="lastUser">The last user.</param>
        //public bool CaptureSummonsServed(int crIntNo, int sssIntNo, DateTime dateServed, char method, string caseNo, string lastUser)
        //{
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("CaseRegisterUpdate", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@CRIntNo", SqlDbType.Int, 4).Value = crIntNo;
        //    com.Parameters.Add("@SS_SIntNo", SqlDbType.Int, 4).Value = sssIntNo;
        //    com.Parameters.Add("@DateServed", SqlDbType.SmallDateTime, 4).Value = dateServed;
        //    com.Parameters.Add("@DeliveryMethod", SqlDbType.Char, 1).Value = method;
        //    com.Parameters.Add("@CaseNo", SqlDbType.VarChar, 20).Value = caseNo;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

    }
}
