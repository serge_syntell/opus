using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Globalization;

namespace Stalberg.TMS
{
    /// <summary>
    /// A simple data class that encapsulates details about a particular menu 
    /// </summary>
    public class CourtRoomDetails
    {
        public string CrtName;
        public string CrtNo;
        public string CrtAddress;
        public string CrtPaymentAddr1;
        public string CrtPaymentAddr2;
        public string CrtPaymentAddr3;
        public string CrtPaymentAddr4;
        public string CrtPrefix;
        public int CrtRNextCaseNo;
    }

    /// <summary>
    /// Business/Data Logic Class that encapsulates all data
    /// logic necessary to add/login/query Courts within
    /// the Commerce Starter Kit Customer database.
    /// </summary>
    public class CourtRoomDB
    {
        // Fields
        string connectionString = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="CourtDB"/> class.
        /// </summary>
        /// <param name="vConstr">The v constr.</param>
        public CourtRoomDB(string vConstr)
        {
            connectionString = vConstr;
        }

        //*******************************************************
        //
        // CourtDB.GetCourtDetails() Method <a name="GetCourtDetails"></a>
        //
        // The GetCourtDetails method returns a CourtDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************
        public SqlDataReader GetCourtRoomDetails(int crtRIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CourtRoomDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtRIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtRIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            return result;
        }

        public SqlDataReader GetActiveCourtRooms(int crtIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CourtRoomActive", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@CrtIntNo", SqlDbType.Int).Value = crtIntNo;

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            return result;
        }

        //*******************************************************
        //
        // CourtRoomDB.AddCourtRoom() Method <a name="AddCourtRoom"></a>
        //
        // The AddCourtRoom method inserts a new menu record
        // into the menus database.  A unique "CourtId"
        // key is then returned from the method.  
        //
        //*******************************************************
        /// <summary>
        /// Updates the court room.
        /// </summary>
        /// <returns></returns>
        public int AddCourtRoom(int nCrtIntNo, string sRoomNo, string sRoomName, string sPrefix, int nCaseNo, int nWoaNo, string sPresidingOfficer, string sProsecutor, string sClerk, string sInterpreter,
            string sActive, string sLastUser, string crtRStatusFlag="R")
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("CourtRoomAdd", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            cmd.Parameters.Add("@CrtIntNo", SqlDbType.Int).Value = nCrtIntNo;
            //cmd.Parameters.Add("@CrtRoomNo", SqlDbType.Char, 1).Value = sRoomNo;
            //2010-9-8 jerry changed CourtRoom.CrtRoomNo to nvarchar(5)
            cmd.Parameters.Add("@CrtRoomNo", SqlDbType.NVarChar, 5).Value = sRoomNo;
            cmd.Parameters.Add("@CrtRoomName", SqlDbType.VarChar, 50).Value = sRoomName;
            cmd.Parameters.Add("@CrtRPrefix", SqlDbType.NVarChar, 5).Value = sPrefix;
            cmd.Parameters.Add("@CrtRNextCaseNo", SqlDbType.Int).Value = nCaseNo;
            cmd.Parameters.Add("@CrtRNextWOANo", SqlDbType.Int).Value = nWoaNo;
            cmd.Parameters.Add("@CrtRPresidingOfficer", SqlDbType.VarChar, 50).Value = sPresidingOfficer;
            cmd.Parameters.Add("@CrtRPublicProsecutor", SqlDbType.VarChar, 50).Value = sProsecutor;
            cmd.Parameters.Add("@CrtRClerk", SqlDbType.VarChar, 50).Value = sClerk;
            cmd.Parameters.Add("@CrtRInterpreter", SqlDbType.VarChar, 50).Value = sInterpreter;
            cmd.Parameters.Add("@CrtRActive", SqlDbType.VarChar, 1).Value = sActive;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = sLastUser;
            // Heidi 2013-09-23 added @CrtRStatusFlag parameter for the courtroom is active, inactive or no longer used(5102)
            cmd.Parameters.Add("@CrtRStatusFlag", SqlDbType.Char, 1).Value = crtRStatusFlag;

            SqlParameter parameterCrtRIntNo = new SqlParameter("@CrtRIntNo", SqlDbType.Int, 4);
            parameterCrtRIntNo.Value = 0;
            parameterCrtRIntNo.Direction = ParameterDirection.InputOutput;
            cmd.Parameters.Add(parameterCrtRIntNo);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Dispose();
                int CrtRIntNo = (int)cmd.Parameters["@CrtRIntNo"].Value;
                return CrtRIntNo;
            }
            catch (Exception e)
            {

                con.Dispose();
                string msg = e.Message;
                return 0;
            }
        }


        /// <summary>
        /// Gets the court list.
        /// </summary>
        /// <returns></returns>
        public SqlDataReader GetCourtRoomList(int nCourt)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CourtRoomList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = nCourt;
            myCommand.Parameters.Add(parameterCrtIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }


        /// <summary>
        /// Gets the court list.Heidi 2013-09-23 added for lookup court room by CrtRStatusFlag='C' OR 'P'
        /// </summary>
        /// <returns></returns>
        public SqlDataReader GetCourtRoomListByCrtRStatusFlag(int nCourt)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CourtRoomListByCrtRStatusFlag", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = nCourt;
            myCommand.Parameters.Add(parameterCrtIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        /// <summary>
        /// Updates the court room.
        /// </summary>
        /// <returns></returns>
        public int UpdateCourtRoom(int crtRIntNo, string sCourtRoom, string sPrefix, int nCaseNo, int nWoaNo, string sPresidingOfficer, string sProsecutor, string sClerk, string sInterpreter,
            string sActive, string sLastUser, string crtRStatusFlag="R")
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("CourtRoomUpdate", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtRIntNo = new SqlParameter("@CrtRIntNo", SqlDbType.Int, 4);
            parameterCrtRIntNo.Value = crtRIntNo;
            parameterCrtRIntNo.Direction = ParameterDirection.InputOutput;
            cmd.Parameters.Add(parameterCrtRIntNo);

            cmd.Parameters.Add("@CrtRoomName", SqlDbType.VarChar, 50).Value = sCourtRoom;
            cmd.Parameters.Add("@CrtRPrefix", SqlDbType.NVarChar, 5).Value = sPrefix;
            cmd.Parameters.Add("@CrtRNextCaseNo", SqlDbType.Int).Value = nCaseNo;
            cmd.Parameters.Add("@CrtRNextWOANo", SqlDbType.Int).Value = nWoaNo;
            cmd.Parameters.Add("@CrtRPresidingOfficer", SqlDbType.VarChar, 50).Value = sPresidingOfficer;
            cmd.Parameters.Add("@CrtRPublicProsecutor", SqlDbType.VarChar, 50).Value = sProsecutor;
            cmd.Parameters.Add("@CrtRClerk", SqlDbType.VarChar, 50).Value = sClerk;
            cmd.Parameters.Add("@CrtRInterpreter", SqlDbType.VarChar, 50).Value = sInterpreter;
            cmd.Parameters.Add("@CrtRActive", SqlDbType.VarChar, 1).Value = sActive;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = sLastUser;
            // Heidi 2013-09-23 added @CrtRStatusFlag parameter for the courtroom is active, inactive or no longer used(5102)
            cmd.Parameters.Add("@CrtRStatusFlag", SqlDbType.Char, 1).Value = crtRStatusFlag;

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Dispose();
                int CrtRIntNo = (int)cmd.Parameters["@CrtRIntNo"].Value;
                return CrtRIntNo;
            }
            catch (Exception e)
            {

                con.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// Deletes the court.
        /// </summary>
        /// <param name="crtIntNo">The CRT int no.</param>
        /// <returns></returns>
        public int DeleteCourtRoom(int crtRIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CourtRoomDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtRIntNo = new SqlParameter("@CrtRIntNo", SqlDbType.Int, 4);
            parameterCrtRIntNo.Value = crtRIntNo;
            parameterCrtRIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterCrtRIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int CrtRIntNo = (int)parameterCrtRIntNo.Value;

                return CrtRIntNo;
            }
            catch
            {
                myConnection.Dispose();
                return 0;
            }
        }

        public DataSet GetSummonsForJudgement(int crIntNo, DateTime dt, string caseNo,
            int pageSize, int pageIndex, out int totalCount, bool isPaidAtCourt = false)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CourtRoomSummonsListForJudgement", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@CRIntNo", SqlDbType.Int, 4).Value = crIntNo;
            com.Parameters.Add("@Date", SqlDbType.DateTime, 8).Value = dt;
            com.Parameters.Add("@CaseNo", SqlDbType.NVarChar, 50).Value = caseNo;
            com.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            com.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;
            com.Parameters.Add("@IsPaidAtCourt", SqlDbType.Bit).Value = isPaidAtCourt;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            com.Parameters.Add(paraTotalCount);


            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            return ds;
        }

        //2013-04-08 add by Henry
        public DataSet GetSummonsForJudgementSumCharge(int crIntNo, DateTime dt, string caseNo, int sumIntNo, bool isPaidAtCourt = false)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CourtRoomSumChargeListForJudgement", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@CRIntNo", SqlDbType.Int, 4).Value = crIntNo;
            com.Parameters.Add("@Date", SqlDbType.DateTime, 8).Value = dt;
            com.Parameters.Add("@CaseNo", SqlDbType.NVarChar, 50).Value = caseNo;
            com.Parameters.Add("@SumIntNo", SqlDbType.Int).Value = sumIntNo;
            com.Parameters.Add("@IsPaidAtCourt", SqlDbType.Bit).Value = isPaidAtCourt;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }



        public DataSet GetSummonsForJudgementReport(int crIntNo, DateTime dt)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CourtRoomSummonsListForJudgementReport", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@CRIntNo", SqlDbType.Int, 4).Value = crIntNo;
            com.Parameters.Add("@Date", SqlDbType.DateTime, 8).Value = dt;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        /// <summary>
        /// Gets the list of summons for recording a WOA where the expiry date has been exceeded!
        /// </summary>
        /// <param name="crIntNo">The court room int no.</param>
        /// <param name="dt">The date.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetSummonsExpiredListForWOA(int crIntNo, DateTime dt)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CourtRoomSummonsExpiryListForWOA", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@CRIntNo", SqlDbType.Int, 4).Value = crIntNo;
            com.Parameters.Add("@Date", SqlDbType.DateTime, 8).Value = dt;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        /// <summary>
        /// Gets the charges for summons.
        /// </summary>
        /// <param name="sumIntNo">The summons int no.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetChargesForSummons(int sumIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CourtRoomChargeForSummons", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        public DataTable CheckWOAType(int sumIntNo, out string woaType)
        {
            DataTable returnTable = new DataTable();
            returnTable.Columns.AddRange(new DataColumn[] { 
                new DataColumn("WOANumber"),
                new DataColumn("WOAIssueDate"),
                new DataColumn("WOAFineAmount"),
                new DataColumn("WOAAddAmount"),
                new DataColumn("WOAGeneratedDate") ,
                new DataColumn("WOAType")
            });
            //WOANumber,WOAIssueDate,WOAFineAmount,WOAAddAmount,WOAGeneratedDate
            woaType = string.Empty;
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("CheckWOATypeForCourtJudgement", con);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@SumIntNo", SqlDbType.Int).Value = sumIntNo;

            try
            {
                con.Open();
                using (IDataReader result = cmd.ExecuteReader())
                {
                    while (result.Read())
                    {
                        woaType = result.GetString(0);
                    }
                    if (result.NextResult())
                    {
                        DataRow dr = returnTable.NewRow();
                        while (result.Read())
                        {
                            dr["WOANumber"] = result["WOANumber"];
                            dr["WOAIssueDate"] = result["WOAIssueDate"] is DBNull ? "" : ((DateTime)result["WOAIssueDate"]).ToString("yyyy/MM/dd");

                            //update by Rachel 20140811 for 5337
                            //dr["WOAFineAmount"] = result["WOAFineAmount"] is DBNull ? "0.00" : String.Format("{0:N2}", Convert.ToDecimal(result["WOAFineAmount"]));
                            //dr["WOAAddAmount"] = result["WOAAddAmount"] is DBNull ? "0.00" : String.Format("{0:N2}", Convert.ToDecimal(result["WOAAddAmount"]));

                            dr["WOAFineAmount"] = result["WOAFineAmount"] is DBNull ? "0.00" : String.Format(CultureInfo.InvariantCulture, "{0:N2}", result["WOAFineAmount"]);
                            dr["WOAAddAmount"] = result["WOAAddAmount"] is DBNull ? "0.00" : String.Format(CultureInfo.InvariantCulture, "{0:N2}", result["WOAAddAmount"]);
                            //end update by Rachel 20140811 for 5337
                            dr["WOAGeneratedDate"] = result["WOAGeneratedDate"] is DBNull ? "" : ((DateTime)result["WOAGeneratedDate"]).ToString("yyyy/MM/dd");
                            dr["WOAType"] = result["WOAType"];
                            returnTable.Rows.Add(dr);
                        }
                    }
                }
                woaType = Convert.ToString(cmd.ExecuteScalar());
                //return woaType;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
            return returnTable;
        }

        public int GetIfIdReferencedByOtherTable(int CrtRIntNo)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("CourtRoomIfIdReferencedByOtherTable", con);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@CrtRIntNo", SqlDbType.Int).Value = CrtRIntNo;

            SqlParameter parameterCrtRIntNo = new SqlParameter("@ReferencedCount", SqlDbType.Int, 4);
            parameterCrtRIntNo.Value = 0;
            parameterCrtRIntNo.Direction = ParameterDirection.InputOutput;
            cmd.Parameters.Add(parameterCrtRIntNo);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Dispose();
                return (int)cmd.Parameters["@ReferencedCount"].Value;
            }
            catch (Exception e)
            {

                con.Dispose();
                string msg = e.Message;
                return 0;
            }
        }
    }
}