﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.DAL.Services;

namespace Stalberg.TMS.Data
{
    public class NoAOGDB
    {
        readonly string connString;
        DateRuleService drService;
        Dictionary<int, int> generateSummonsDays;

        public NoAOGDB(string connString)
        {
            this.connString = connString;
        }

        public bool IsNoAOG(int pk, NoAOGQueryType type, bool checkAll = true)
        {
            var result = new SqlParameter("@Result", SqlDbType.Bit) { Direction = ParameterDirection.ReturnValue };
            var paras = new[]
            {
                new SqlParameter("@PK", pk),
                new SqlParameter("@Type", type.Value),
                new SqlParameter("@CheckAll", checkAll),
                result
            };
            using (var conn = new SqlConnection(this.connString))
            {
                using (var cmd = new SqlCommand("fnIsNoAOG", conn) { CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.AddRange(paras);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    return (bool)result.Value;
                }
            }
        }

        public DateTime GetNoAOGGenerateSummonsActionDate(int autIntNo, DateTime? postDate = null)
        {
            //2013-11-06 Heidi changed for used new date rule DR_NoAOGPostedDate_SumGenerateDate default days=21(5115)
            var days = 21;//5; //default value 

            if (!postDate.HasValue || DateTime.MinValue == postDate.Value)
                postDate = DateTime.Now;

            if (autIntNo > 0)
            {
                int tmpDays;
                if (this.generateSummonsDays != null
                    && this.generateSummonsDays.TryGetValue(autIntNo, out tmpDays))
                    days = tmpDays;
                else
                {
                    if (this.generateSummonsDays == null)
                        this.generateSummonsDays = new Dictionary<int, int>();

                    if (this.drService == null)
                        this.drService = new DateRuleService();
                    //2013-11-06 Heidi changed for used new date rule DR_NoAOGPostedDate_SumGenerateDate default days=21(5115)
                   // using (var dr = this.drService.GetByDRNameAutIntNo("DR_NotPosted1stNoticeDate_SumIssueDate", autIntNo))
                    using (var dr = this.drService.GetByDRNameAutIntNo("DR_NoAOGPostedDate_SumGenerateDate", autIntNo))
                    {
                        if (dr.Read())
                        {
                            days = Convert.ToInt32(dr["ADRNoOfDays"]);
                            this.generateSummonsDays.Add(autIntNo, days);
                        }
                    }
                }
            }

            return postDate.Value.AddDays(days);
        }
    }

    public class NoAOGQueryType
    {
        readonly string value;

        NoAOGQueryType(string value)
        {
            this.value = value;
        }

        #region Values

        public static NoAOGQueryType Notice
        {
            get { return new NoAOGQueryType("N"); }
        }
        public static NoAOGQueryType Charge
        {
            get { return new NoAOGQueryType("C"); }
        }
        public static NoAOGQueryType Summons
        {
            get { return new NoAOGQueryType("S"); }
        }
        public static NoAOGQueryType SumCharge
        {
            get { return new NoAOGQueryType("SC"); }
        }

        #endregion

        public string Value
        {
            get { return this.value; }
        }

        public override string ToString()
        {
            return this.value;
        }
    }
}
