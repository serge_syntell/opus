using System;

namespace Stalberg.TMS.Data
{
   
    /// <summary>
    /// Represents the 
    /// </summary>
    [Serializable]
    public class ArchiveInfo
    {
        // Fields
        private int nFrameIntNo;
        private int nFilmIntNo;
        private string sFilmNo;
        private string sFrameNo;
        private DateTime dtOffenceDate;
        //private string sChargeStatus;
        // Oscar changed for 4416
        private string nNoticeStatus;
        private string sFrameStatus;

        //public ArchiveInfo(int nFrameIntNo,int nFilmIntNo,string sFilmNo, string sFrameNo , DateTime dtOffenceDate , string sChargeStatus , string sFrameStatus)
        // Oscar changed for 4416
        public ArchiveInfo(int nFrameIntNo, int nFilmIntNo, string sFilmNo, string sFrameNo, DateTime dtOffenceDate, string nNoticeStatus, string sFrameStatus)
        {
            this.nFrameIntNo = nFrameIntNo;
            this.nFilmIntNo = nFilmIntNo;
            this.sFilmNo = sFilmNo;
            this.sFrameNo = sFrameNo;
            this.dtOffenceDate = dtOffenceDate;
            this.nNoticeStatus = nNoticeStatus;
            this.sFrameStatus = sFrameStatus;
        }

        /// <summary>
        /// Gets the frame int no.
        /// </summary>
        /// <value>The frame int no.</value>
        public int FrameIntNo
        {
            get { return this.nFrameIntNo; }
        }

        public int FilmIntNo
        {
            get { return this.nFilmIntNo; }
        }

        public string FilmNo
        {
            get { return this.sFilmNo; }
        }

        public string FrameNo
        {
            get { return this.sFrameNo; }
        }

        public DateTime  OffenceDate
        {
            get { return this.dtOffenceDate; }
        }

        public string NoticeStatus
        {
            get { return this.nNoticeStatus; }
        }

        public string FrameStatus
        {
            get { return this.sFrameStatus; }
        }
    }
}
