using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // VehicleMakeDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class VehicleMakeDetails 
	{
        public int VMIntNo;
        public string VMCode;
		public string VMDescr;
        public string Natis;
        public string TCS;
        public string Civitas;
    }

    public partial class VehicleMakeLookupDetails
    {
        public Int32 VMLIntNo;
        public Int32 VMIntNo1;
        public Int32 VMIntNo2;
    }

    //*******************************************************
    //
    // VehicleMakeDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query VehicleMakes within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class VehicleMakeDB {

		string mConstr = "";

		public VehicleMakeDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // VehicleMakeDB.GetVehicleMakeDetails() Method <a name="GetVehicleMakeDetails"></a>
        //
        // The GetVehicleMakeDetails method returns a VehicleMakeDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public VehicleMakeDetails GetVehicleMakeDetails(int vmIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("VehicleMakeDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterVMIntNo = new SqlParameter("@VMIntNo", SqlDbType.Int, 4);
            parameterVMIntNo.Value = vmIntNo;
            myCommand.Parameters.Add(parameterVMIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			VehicleMakeDetails myVehicleMakeDetails = new VehicleMakeDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myVehicleMakeDetails.VMDescr = result["VMDescr"].ToString();
				myVehicleMakeDetails.VMCode = result["VMCode"].ToString();
                myVehicleMakeDetails.Natis = result["Natis"].ToString();
                myVehicleMakeDetails.TCS = result["TCS"].ToString();
                myVehicleMakeDetails.Civitas = result["Civitas"].ToString();
			}
			result.Close();
            return myVehicleMakeDetails;
        }

        public VehicleMakeDetails GetVehicleMakeDetailsByCode(string vmCode, string system)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("VehicleMakeDetailByCode", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterVMCode = new SqlParameter("@VMCode", SqlDbType.VarChar, 3);
            parameterVMCode.Value = vmCode;
            myCommand.Parameters.Add(parameterVMCode);

            SqlParameter parameterSystem = new SqlParameter("@System", SqlDbType.Char, 1);
            parameterSystem.Value = system;
            myCommand.Parameters.Add(parameterSystem);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            VehicleMakeDetails myVehicleMakeDetails = new VehicleMakeDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myVehicleMakeDetails.VMDescr = result["VMDescr"].ToString();
                myVehicleMakeDetails.VMCode = result["VMCode"].ToString();
                myVehicleMakeDetails.VMIntNo = Convert.ToInt32(result["VMIntNo"]);
                myVehicleMakeDetails.Natis = result["Natis"].ToString();
                myVehicleMakeDetails.TCS = result["TCS"].ToString();
                myVehicleMakeDetails.Civitas = result["Civitas"].ToString();
            }
            result.Close();
            return myVehicleMakeDetails;
        }

        //*******************************************************
        //
        // VehicleMakeDB.AddVehicleMake() Method <a name="AddVehicleMake"></a>
        //
        // The AddVehicleMake method inserts a new menu record
        // into the menus database.  A unique "VehicleMakeId"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddVehicleMake (string vmCode, string vmDescr, string natis, string tcs, string civitas, string lastUser) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("VehicleMakeAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC

			SqlParameter parameterVMDescr = new SqlParameter("@VMDescr", SqlDbType.VarChar, 30);
			parameterVMDescr.Value = vmDescr;
			myCommand.Parameters.Add(parameterVMDescr);

			SqlParameter parameterVMCode = new SqlParameter("@VMCode", SqlDbType.VarChar, 3);
			parameterVMCode.Value = vmCode;
			myCommand.Parameters.Add(parameterVMCode);

            SqlParameter parameterNatis = new SqlParameter("@Natis", SqlDbType.VarChar, 3);
            parameterNatis.Value = natis;
            myCommand.Parameters.Add(parameterNatis);

            SqlParameter parameterTCS = new SqlParameter("@TCS", SqlDbType.VarChar, 3);
            parameterTCS.Value = tcs;
            myCommand.Parameters.Add(parameterTCS);

            SqlParameter parameterCivitas= new SqlParameter("@Civitas", SqlDbType.VarChar, 3);
            parameterCivitas.Value = civitas;
            myCommand.Parameters.Add(parameterCivitas);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterVMIntNo = new SqlParameter("@VMIntNo", SqlDbType.Int, 4);
            parameterVMIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterVMIntNo);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int vmIntNo = Convert.ToInt32(parameterVMIntNo.Value);

                return vmIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

		public SqlDataReader GetVehicleMakeList(string search)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("VehicleMakeList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;


			SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
			parameterSearch.Value = search;
			myCommand.Parameters.Add(parameterSearch);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

        public DataSet GetVehicleMakeListDS(string search)
        { 
            int totalCount = 0;
            return GetVehicleMakeListDS(search, out totalCount);
        }
		
		public DataSet GetVehicleMakeListDS(string search, out int totalCount, int pageSize = 0, int pageIndex = 0)
		{
			SqlDataAdapter sqlDAVehicleMakes = new SqlDataAdapter();
			DataSet dsVehicleMakes = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAVehicleMakes.SelectCommand = new SqlCommand();
			sqlDAVehicleMakes.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAVehicleMakes.SelectCommand.CommandText = "VehicleMakeList";

			// Mark the Command as a SPROC
			sqlDAVehicleMakes.SelectCommand.CommandType = CommandType.StoredProcedure;

			SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
			parameterSearch.Value = search;
			sqlDAVehicleMakes.SelectCommand.Parameters.Add(parameterSearch);

            sqlDAVehicleMakes.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            sqlDAVehicleMakes.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter parameterTotal = new SqlParameter("@TotalCount", SqlDbType.Int);
            parameterTotal.Direction = ParameterDirection.Output;
            sqlDAVehicleMakes.SelectCommand.Parameters.Add(parameterTotal);

			// Execute the command and close the connection
			sqlDAVehicleMakes.Fill(dsVehicleMakes);
			sqlDAVehicleMakes.SelectCommand.Connection.Dispose();

            totalCount = (int)(parameterTotal.Value == DBNull.Value ? 0 : parameterTotal.Value);

			// Return the dataset result
			return dsVehicleMakes;		
		}


        public int UpdateVehicleMake(int vmIntNo, string vmCode, string vmDescr, string natis, string tcs, string civitas, string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("VehicleMakeUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterVMDescr = new SqlParameter("@VMDescr", SqlDbType.VarChar, 30);
			parameterVMDescr.Value = vmDescr;
			myCommand.Parameters.Add(parameterVMDescr);

			SqlParameter parameterVMCode = new SqlParameter("@VMCode", SqlDbType.VarChar, 3);
			parameterVMCode.Value = vmCode;
			myCommand.Parameters.Add(parameterVMCode);

            SqlParameter parameterNatis = new SqlParameter("@Natis", SqlDbType.VarChar, 3);
            parameterNatis.Value = natis;
            myCommand.Parameters.Add(parameterNatis);

            SqlParameter parameterTCS = new SqlParameter("@TCS", SqlDbType.VarChar, 3);
            parameterTCS.Value = tcs;
            myCommand.Parameters.Add(parameterTCS);

            SqlParameter parameterCivitas = new SqlParameter("@Civitas", SqlDbType.VarChar, 3);
            parameterCivitas.Value = civitas;
            myCommand.Parameters.Add(parameterCivitas);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterVMIntNo = new SqlParameter("@VMIntNo", SqlDbType.Int);
			parameterVMIntNo.Direction = ParameterDirection.InputOutput;
			parameterVMIntNo.Value = vmIntNo;
			myCommand.Parameters.Add(parameterVMIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int VMIntNo = (int)myCommand.Parameters["@VMIntNo"].Value;
				//int menuId = (int)parameterVMIntNo.Value;

				return VMIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
                string msg = e.Message;
				return 0;
			}
		}

		
		public String DeleteVehicleMake (int vmIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("VehicleMakeDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterVMIntNo = new SqlParameter("@VMIntNo", SqlDbType.Int, 4);
			parameterVMIntNo.Value = vmIntNo;
			parameterVMIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterVMIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int VMIntNo = (int)parameterVMIntNo.Value;

				return VMIntNo.ToString();
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return String.Empty;
			}
		}

        public DataSet GetVehicleMakeLookupListDS(int vmIntNo)
        {
            SqlDataAdapter sqlDAVehicleMakes = new SqlDataAdapter();
            DataSet ds = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAVehicleMakes.SelectCommand = new SqlCommand();
            sqlDAVehicleMakes.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAVehicleMakes.SelectCommand.CommandText = "VehicleMakeLookupList";

            // Mark the Command as a SPROC
            sqlDAVehicleMakes.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterVMIntNo = new SqlParameter("@VMIntNo", SqlDbType.Int, 4);
            parameterVMIntNo.Value = vmIntNo;
            sqlDAVehicleMakes.SelectCommand.Parameters.Add(parameterVMIntNo);

            // Execute the command and close the connection
            sqlDAVehicleMakes.Fill(ds);
            sqlDAVehicleMakes.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return ds;
        }
        // 2013-07-29 add parameter lastUser by Henry
        public int AddVehicleMakeLookups(int vmIntNo, ArrayList vtl, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("VehicleMakeLookupAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterVMIntNo = new SqlParameter("@VMIntNo", SqlDbType.Int, 4);
            parameterVMIntNo.Value = vmIntNo;
            parameterVMIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterVMIntNo);
            myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            string lookupValues = "";

            for (int i = 0; i < vtl.Count; i++)
            {
                lookupValues += vtl[i] + ";";
            }

            SqlParameter parameterLookupValues = new SqlParameter("@LookupValues", SqlDbType.VarChar, 100);
            parameterLookupValues.Value = lookupValues;
            myCommand.Parameters.Add(parameterLookupValues);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int success = (int)myCommand.Parameters["@VMIntNo"].Value;

                return success;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                myConnection.Dispose();
                return -1;
            }
        }
	}
}

