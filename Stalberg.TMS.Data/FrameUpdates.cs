using System;
using System.Data.SqlClient;

/// <summary>
/// Summary description for FrameUpdates
/// </summary>
namespace Stalberg.TMS
{
    public class FrameUpdates
    {
        private string connectionString = "";
        private FrameDB frame = null;
        private FrameList frames = null;
        private int _statusProsecute = 0;
        private int _statusCancel = 0;
        private int _statusVerified = 0;
        private int _statusRejected = 0;
        private int _statusNatis = 0;
        private string _PreUpdatedFrameRegNo = "";
        private string _PreUpdatedRejReason = "";
        private string _PreUpdatedAllowContinue = "Y";
        private int _FrameIntNo = 0;
        private string _AllowContinue = "Y";
        private string Session_AdjOfficerNo = "";
        private int Session_USIntNo = 0;
        private bool Session_IsNatisLast = false;

        private const string ASD_INVALID_SETUP = "ASD Camera Setup Invalid";

        public FrameUpdates(string conStr, ref FrameList frameList)
        {
            connectionString = conStr;
            this.frames = frameList;
            frame = new FrameDB(connectionString);
        }

        //public string Adjudicate(string rejReason, int rejIntNo, string registration, bool changedMake, string vehType,
        //    string vehMake, string userName, string processType, int statusProsecute, int statusCancel, int statusNatis,
        //    string frameNo, string sessionFrameRegNo, string sessionRejReason, string sessionAdjOfficerNo, int sessionUSIntNo)
        
        public string Adjudicate(string rejReason, int rejIntNo, string registration, bool changedMake, string vehType,
            string vehMake, string userName, string processType, int statusProsecute, int statusCancel, int statusNatis,
            string frameNo, string preUpdatedRegNo, string preUpdatedRejReason, string sessionAdjOfficerNo, int sessionUSIntNo,
            int frameIntNo, bool allowContinue, string preUpdatedAllowContinue, Int64 nRowVersion, bool isNaTISLast, int autIntNo, bool bNatisData,
            bool adjAt100, bool reject, bool sendToNatis, bool saveImageSettings, decimal contrast, decimal brightness)
        {
            string returnMessage = "";

            _statusProsecute = statusProsecute;
            _statusCancel = statusCancel;
            _statusNatis = statusNatis;

            _PreUpdatedFrameRegNo = preUpdatedRegNo;
            _PreUpdatedRejReason = preUpdatedRejReason;
            _PreUpdatedAllowContinue = preUpdatedAllowContinue;

            _FrameIntNo = frameIntNo;

            if (!allowContinue)
                _AllowContinue = "N";

            Session_AdjOfficerNo = sessionAdjOfficerNo;
            Session_USIntNo = sessionUSIntNo;
            Session_IsNatisLast = isNaTISLast;

            // Update Vehicle type, make, and Reg No
            if (!Convert.ToBoolean(changedMake))
            {
                //dls 070830 - there is no longer a check for blank registration numbers.
                if (registration.Trim().Equals(""))
                {
                    return "Registration number may not be blank";
                }

                //frame.UpdateFrameDetailsAtAdjudication(this.frames.Current, Convert.ToInt32(vehType), Convert.ToInt32(vehMake), registration, userName);
                SqlDataReader reader = frame.UpdateFrameDetailsAtAdjudication(_FrameIntNo, Convert.ToInt32(vehType), Convert.ToInt32(vehMake), 
                    registration, userName, rejIntNo, ref returnMessage);

                reader.Read();
                if (Convert.ToInt32( reader[0].ToString()) < 1)
                {
                    return "Unable to update this frame - " + returnMessage;
                }
                nRowVersion = Convert.ToInt64(reader[1]);
                reader.Dispose();
            }   

            int frameStatus = 0;

            if (sendToNatis)
                frameStatus = statusNatis;
            else if (processType.Equals("Prosecute"))
                frameStatus = statusProsecute;
            else
                frameStatus = statusCancel;

            returnMessage = this.AdjudicateFrame(frameStatus, rejReason, rejIntNo, registration, frameNo, userName, nRowVersion, autIntNo, isNaTISLast, bNatisData,
                adjAt100, reject, saveImageSettings, contrast, brightness);

            return returnMessage;
        }

        public string AdjudicateFrame(int status, string rejReason, int rejIntNo, string registration, string frameNo, string userName,
            Int64 nRowVersion, int autIntNo, bool isNaTISLast, bool bNatisData, bool adjAt100, bool reject, bool saveImageSettings, 
            decimal contrast, decimal brightness)
        {
            string returnMessage = "";

            string confirmViolation = "N";
            if (status == _statusProsecute)
                confirmViolation = "Y";

            if (reject)
            {
                status = _statusCancel;
            }

            if (status == _statusProsecute)
            {
                if (_PreUpdatedFrameRegNo != null)
                {
                    //dls 060606 - don't resend zeroes to natis
                    if (!_PreUpdatedFrameRegNo.Equals(registration) && !registration.Equals("0000000000"))
                        status = _statusNatis;
                }

                if (_PreUpdatedRejReason != null)
                {
                    //if the reason for rejection has changed to None, we need to resend the frame to NAtis
                    if (!_PreUpdatedRejReason.Equals(rejReason) && rejReason.Equals("None"))
                        status = _statusNatis;

                    if (_PreUpdatedRejReason.Equals(ASD_INVALID_SETUP))
                    {
                        returnMessage = "Violation may not be prosecuted - the ASD Camera Setup Details are invalid";
                        return returnMessage;
                    }
                }
            }

            string strResetRejReason = "N";
            if (rejReason.Equals("None") || status == _statusNatis)
            {
                int regNo = 0;

                try
                {
                    regNo = Convert.ToInt32(registration);
                    if (regNo == 0 || registration.Trim().Length < 3)
                    {
                        returnMessage = "Registration may not be zeroes or blank for a violation";
                        return returnMessage;
                    }
                }
                catch { }
            }
            //else if (GetRuleFullAdjudicated(autIntNo, userName) == "Y")
            else if (adjAt100 && status == _statusProsecute)
            {
                if (status == _statusProsecute)
                {
                    if (!bNatisData)
                    {
                        if (!isNaTISLast)
                        {
                            status = 50;
                        }
                        else
                        {
                            status = 710;
                        }
                    }

                    if (!rejReason.Equals("None"))
                    {
                        confirmViolation = "Y";
                        strResetRejReason = "Y";
                    }
                }
            }

            string errMessage = "";

            //int updFrameIntNo = frame.AdjudicateFrame(this.frames.Current, rejIntNo, registration, confirmViolation, userName, status, Session_AdjOfficerNo, ref errMessage);
            int updFrameIntNo = frame.AdjudicateFrame(_FrameIntNo, confirmViolation, userName, status, Session_AdjOfficerNo, ref errMessage,
                _AllowContinue, _PreUpdatedAllowContinue, nRowVersion, strResetRejReason);

            switch (updFrameIntNo)
            {
                case 0:
                    returnMessage = "Unable to update this frame - " + errMessage;
                    break;
                case -1:
                    returnMessage = "Frame does not exist, rowversion does not match, or the frame has already been updated - unable to update frame";
                    break;
                case -2:
                    returnMessage = "Transaction rollback - unable to update frame";
                    break;
                case -3:
                    returnMessage = "Frame cannot be updated - the frame status is incorrect for adjudication";
                    break;
                case -4:
                    returnMessage = "Frame cannot be adjudicated - there is no corresponding offence code for the values on the frame";
                    break;
                case -5:
                    returnMessage = "Frame cannot be adjudicated - Registration may not be blank or zeroes";
                    break;
                case -12:
                    returnMessage = "Proxy details are missing or incorrect - unable to prosecute. Please change the rejection reason";
                    break;
                case -13:
                    returnMessage = "Driver details are missing or incorrect - unable to prosecute. Please change the rejection reason";
                    break;
                case -14:
                    returnMessage = "Owner details are missing or incorrect - unable to prosecute. Please change the rejection reason";
                    break;
                default:                   

                    // dls 070517 - this will get done as a scheduled job at end of day
                    //LMZ 15-03-2007 - check to see if this frame has already been adjudicated
                    /*if (!this.frames.CurrentProcessed)
                    {
                        try
                        {
                            UserDB db = new UserDB(connectionString);
                            int nUSIntNo = db.UserShiftEdit(Session_USIntNo, "", "Adjudicate");
                        }
                        catch { }
                    }*/
                    //returnMessage = "Frame " + frameNo + " has been updated";
                    // FBJ Added (2007-03-12): Moved this to a page reload to avoid accidental reposting of data
                    //this.frames.GetNextFrame(false);
                    //this.GetNextFrameToAdjudicate();
                    //this.frames.MarkProcessed();
                    //this.SetIFrame(0, false);
                    this.frames.MarkProcessed(_FrameIntNo);

                    //dls 070604 - remove from the framelist so that they don't try to update it again
                    if (!Session_IsNatisLast && status == _statusNatis)
                        this.frames.HideFrame(_FrameIntNo);

                    //dls 100119 - need to update the ScImPrintVal = 1 ScanImage for this frame if brightness and contrast have been set
                    if (saveImageSettings && contrast != 99 && brightness != 99)
                    {
                        ImageProcesses imageProcess = new ImageProcesses(this.connectionString);

                        int scImIntNo = imageProcess.UpdateImageSetting(0, contrast, brightness * 0.1m, userName, _FrameIntNo);

                        if (scImIntNo < 1)
                        {
                            if (returnMessage.Length > 0)
                                returnMessage += "; ";

                            returnMessage += "Unable to update the contrast and brightness settings on this image";
                            break;
                        }
                    }
                    break;
            }

            return returnMessage;
        }


        public string Verify (string process, string rejReason, int rejIntNo, string registration, string vehType,
            string vehMake, string userName, string processType, int statusVerified, int statusRejected, int statusNatis, int statusInvestigate,
            int speed1, int speed2, DateTime offenceDate, string frameNo, string preUpdatedRegNo, string preUpdatedRejReason,
            bool isNaTISLast, int frameIntNo, bool allowContinue, string preUpdatedAllowContinue, Int64 nRowVersion, int autIntNo, bool adjAt100, int inReIntNo)
        {
            string returnMessage = "";

            int vmIntNo = Convert.ToInt32(vehMake);
            int vtIntNo = Convert.ToInt32(vehType);

            _statusVerified = statusVerified;
            _statusRejected = statusRejected;

            string violation = "Y";

            //dls 090618 - moved this to the frame verification page so that it doesn't ahve to call the database on on every frame
            //if (GetRuleFullAdjudicated(autIntNo, userName) == "Y")
            if (adjAt100)
            {
                if (!isNaTISLast)
                {
                    _statusRejected = 500;
                    //violation = "N";
                }
                else
                    return "Cannot be Natis Last when authurity rule 0600 is enabled.";
            }
            //dls 090624 - added this so that when the stuff that has no natis data is rejected, it will move on to Adjudication (will get resent to Natis there if they decide to prosecute)
            // otherwise the rejected stuff with no natis data keeps getting sent back to natis (_StatusVerified = 50!)
            //dls 2010-10-12 - need to stop everything from ending up in Adjudication
            else if (processType.Equals("Reject") && !isNaTISLast)
            {
                _statusVerified = 500;
                _statusRejected = 999;
            }
                
            _statusNatis = statusNatis;

            _PreUpdatedFrameRegNo = preUpdatedRegNo;
            _PreUpdatedRejReason = preUpdatedRejReason;
            _PreUpdatedAllowContinue = preUpdatedAllowContinue;

            _FrameIntNo = frameIntNo;

            if (!allowContinue)
                _AllowContinue = "N";

            int frameStatus = 0;

            //the frame status must be set to 450 so that the frame data can be moved forward to the adjudication stage
            //if (process.Equals("ReturnToNatis"))
            //    frameStatus = _statusNatis;
            //else if (isNaTISLast && processType.Equals("Reject"))
            //    frameStatus = _statusRejected;
            //else
            //    frameStatus = _statusVerified;

            if (process.Equals("ReturnToNatis"))
                frameStatus = _statusNatis;
            else if (processType.Equals("Reject"))
                frameStatus = _statusRejected;
            else if (processType.Equals("Investigate"))
            {
                frameStatus = statusInvestigate;
            }
            else
                frameStatus = _statusVerified;

            returnMessage = VerifyFrame(frameStatus, processType, rejReason, rejIntNo, vmIntNo, vtIntNo, registration, speed1, speed2, offenceDate, frameNo, userName, nRowVersion, violation, inReIntNo);

            return returnMessage;
        }

        //private string GetRuleFullAdjudicated(int autIntNo, string userName)
        //{
        //    //tf 20090611 - get AuthorityRule for 100% adjuducated
        //    AuthorityRulesDB arDB = new AuthorityRulesDB(connectionString);
        //    AuthorityRulesDetails arDetails = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "0600", "Rule to ensure that 100% of frames are Adjudicated",
        //        0, "N", "Y - Yes; N - No(Default)", userName);

        //    return arDetails.ARString;
        //}

        public string VerifyFrame(int status, string processType, string rejReason, int rejIntNo, int vmIntNo, int vtIntNo,
            string registration, int speed1, int speed2, DateTime offenceDate, string frameNo, string userName, Int64 nRowVersion, string violation, int inReIntNo)
        {
            
            string returnMessage = "";

            if (processType.Equals("Reject")) 
                violation = "N";

            if (status == _statusVerified)
            {
                //dls 060606 - don't resend zeroes to natis
                if (!_PreUpdatedFrameRegNo.Equals(registration) && !registration.Equals("0000000000"))
                {
                    status = _statusNatis;
                }
            }

            if (rejReason.Equals("None"))
            {
                int regNo = 0;

                try
                {
                    regNo = Convert.ToInt32(registration);
                    if (regNo == 0 || registration.Trim().Length < 3)
                    {
                        returnMessage = "Registration may not be zeroes for a violation";
                        return returnMessage;
                    }
                }
                catch { }
            }

            string errMessage = "";

            int updFrameIntNo = frame.VerifyFrame(_FrameIntNo, vmIntNo, vtIntNo, speed1, speed2, offenceDate,
                rejIntNo, registration, violation, _AllowContinue, _PreUpdatedAllowContinue, status, userName,
                ref errMessage, nRowVersion);

            switch (updFrameIntNo)
            {
                case 0:
                    returnMessage = "Unable to update this frame - " + errMessage;
                    break;
                case -1:
                    returnMessage = "Speed may not be 0 for speed violations";
                    break;
                case -2:
                    returnMessage = "Registration no may not be blank - enter '0000000000' for a non-violation";
                    break;
                case -3:
                    returnMessage = "Registration no may not be '0000000000' if reason for rejection is 'None'";
                    break;
                case -4:
                    returnMessage = "Reason for rejection must be 'None' for a violation";
                    break;
                case -5:
                    returnMessage = "Reason for rejection may not be 'None' for a non-violation";
                    break;
                case -6:
                    returnMessage = "The difference between the first and second speeds may not be more than 3";
                    break;
                case -7:
                    returnMessage = "The database has no offence code for this violation type for speed (" + speed1 + ") and vehicle type - unable to update frame";
                    break;
                case -8:
                    returnMessage = "Transaction rollback - unable to update frame";
                    break;
                case -9:
                    returnMessage = "Frame not found, rowversion does not match, or the frame has already been updated - unable to update frame";
                    break;
                case -10:
                    returnMessage = "Frame cannot be updated - the frame status is incorrect for verification";
                    break;
                case -11:
                    returnMessage = "Transaction rollback - unable to update frame offence";
                    break;
                case -12:
                    returnMessage = "Proxy details are missing or incorrect - please update before proceeding (or change the rejection reason)";
                    break;
                case -13:
                    returnMessage = "Driver details are missing or incorrect - please update before proceeding (or change the rejection reason)";
                    break;
                case -14:
                    returnMessage = "Owner details are missing or incorrect - please update before proceeding (or change the rejection reason)";
                    break;
                default:
                    //returnMessage = "Frame " + frameNo + " has been updated";
                    this.frames.MarkProcessed(_FrameIntNo);

                    //dls 070604 - remove from the framelist so that they don't try to update it again
                    if (status == _statusNatis)
                        this.frames.HideFrame(_FrameIntNo);

                    break;
            }

            return returnMessage;
        }
    }
}