using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


namespace Stalberg.TMS
{

    //*******************************************************
    //
    // Frame_OffenceDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class Frame_OffenceDetails
    {
        public int FrameIntNo; 
        public int OffIntNo; 
        public string Violation; 
        public string ConfirmViolation; 
        public string StatusComment; 
        public double FineAmount; 
        public string FineAllocation;
		public int OffenderType; 
        public int ReferenceNo; 
        public string OffenceLetter; 
        public string TicketNr; 
        public string TicketStatus;
        public DateTime DateUploaded_TMS;
    }

    //*******************************************************
    //
    // Frame_OffenceDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query Frame_Offences within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class Frame_OffenceDB
    {

        string mConstr = "";

        public Frame_OffenceDB(string vConstr)
        {
            mConstr = vConstr;
        }

        //*******************************************************
        //
        // Frame_OffenceDB.GetFrame_OffenceDetails() Method <a name="GetFrame_OffenceDetails"></a>
        //
        // The GetFrame_OffenceDetails method returns a Frame_OffenceDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public Frame_OffenceDetails GetFrame_OffenceDetails(int frOfIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("Frame_OffenceDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFrOfIntNo = new SqlParameter("@FrOfIntNo", SqlDbType.Int, 4);
            parameterFrOfIntNo.Value = frOfIntNo;
            myCommand.Parameters.Add(parameterFrOfIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            Frame_OffenceDetails myFrame_OffenceDetails = new Frame_OffenceDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myFrame_OffenceDetails.FrameIntNo = Convert.ToInt32(result["FrameIntNo"]);
                myFrame_OffenceDetails.OffIntNo = Convert.ToInt32(result["OffIntNo"]);
                myFrame_OffenceDetails.Violation = result["Violation"].ToString();
                myFrame_OffenceDetails.ConfirmViolation = result["ConfirmViolation"].ToString();
                myFrame_OffenceDetails.StatusComment = result["StatusComment"].ToString();
                myFrame_OffenceDetails.FineAmount = Convert.ToDouble(result["FineAmount"]);
                myFrame_OffenceDetails.FineAllocation = result["FineAllocation"].ToString();
                myFrame_OffenceDetails.OffenderType  = Convert.ToInt32(result["OffenderType"]);
                myFrame_OffenceDetails.ReferenceNo = Convert.ToInt32(result["ReferenceNo"]); 
                myFrame_OffenceDetails.OffenceLetter = result["OffenceLetter"].ToString();
                myFrame_OffenceDetails.TicketNr = result["TicketNr"].ToString();
                myFrame_OffenceDetails.TicketStatus = result["TicketStatus"].ToString();
                myFrame_OffenceDetails.DateUploaded_TMS = Convert.ToDateTime(result["DateUploaded_TMS"]);
               
            }
            result.Close();
            return myFrame_OffenceDetails;
        }

        //*******************************************************
        //
        // Frame_OffenceDB.AddFrame_Offence() Method <a name="AddFrame_Offence"></a>
        //
        // The AddFrame_Offence method inserts a new menu record
        // into the menus database.  A unique "Frame_OffenceId"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddFrame_Offence(int frameIntNo, int OffIntNo, string violation, 
        string confirmViolation, string statusComment, string fineAmount, 
        string fineAllocation, int offenderType, int referenceNo, string offenceLetter, 
        string ticketNr,  string ticketStatus, string dateUploaded_TMS, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("Frame_OffenceAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Value = frameIntNo;
            myCommand.Parameters.Add(parameterFrameIntNo);

            SqlParameter parameterOffIntNo = new SqlParameter("@OffIntNo", SqlDbType.Int, 4);
            parameterOffIntNo.Value = OffIntNo;
            myCommand.Parameters.Add(parameterOffIntNo);

            SqlParameter parameterViolation = new SqlParameter("@Violation", SqlDbType.Char, 1);
            parameterViolation.Value = violation;
            myCommand.Parameters.Add(parameterViolation);

            SqlParameter parameterConfirmViolation = new SqlParameter("@ConfirmViolation", SqlDbType.Char, 1);
            parameterConfirmViolation.Value = confirmViolation;
            myCommand.Parameters.Add(parameterConfirmViolation);

            SqlParameter parameterStatusComment = new SqlParameter("@StatusComment", SqlDbType.VarChar, 30);
            parameterStatusComment.Value = statusComment;
            myCommand.Parameters.Add(parameterStatusComment);

            SqlParameter parameterFineAmount = new SqlParameter("@FineAmount", SqlDbType.Real);
            parameterFineAmount.Value = fineAmount;
            myCommand.Parameters.Add(parameterFineAmount);

            SqlParameter parameterFineAllocation = new SqlParameter("@FineAllocation", SqlDbType.Char, 1);
            parameterFineAllocation.Value = fineAllocation;
            myCommand.Parameters.Add(parameterFineAllocation);

            SqlParameter parameterOffenderType = new SqlParameter("@OffenderType", SqlDbType.Int, 4);
            parameterOffenderType.Value = offenderType;
            myCommand.Parameters.Add(parameterOffenderType);

            SqlParameter parameterReferenceNo = new SqlParameter("@ReferenceNo", SqlDbType.Int, 4);
            parameterReferenceNo.Value = referenceNo;
            myCommand.Parameters.Add(parameterReferenceNo);

            SqlParameter parameterOffenceLetter = new SqlParameter("@OffenceLetter", SqlDbType.Char, 2);
            parameterOffenceLetter.Value = offenceLetter;
            myCommand.Parameters.Add(parameterOffenceLetter);

            SqlParameter parameterTicketNr = new SqlParameter("@TicketNr", SqlDbType.VarChar, 20);
            parameterTicketNr.Value = ticketNr;
            myCommand.Parameters.Add(parameterTicketNr);

            SqlParameter parameterTicketStatus = new SqlParameter("@TicketStatus", SqlDbType.VarChar, 255);
            parameterTicketStatus.Value = ticketStatus;
            myCommand.Parameters.Add(parameterTicketStatus);

            SqlParameter parameterDateUploaded_TMS = new SqlParameter("@DateUploaded_TMS", SqlDbType.SmallDateTime);
            parameterDateUploaded_TMS.Value = dateUploaded_TMS;
            myCommand.Parameters.Add(parameterDateUploaded_TMS);
   
            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterFrOfIntNo = new SqlParameter("@FrOfIntNo", SqlDbType.Int, 4);
            parameterFrOfIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterFrOfIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int frOfIntNo = Convert.ToInt32(parameterFrOfIntNo.Value);

                return frOfIntNo;
            }
            catch
            {
                myConnection.Dispose();
                return 0;
            }
        }

        public SqlDataReader GetFrame_OffenceList(int frameIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("Frame_OffenceList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Value = frameIntNo;
            myCommand.Parameters.Add(parameterFrameIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }


        public DataSet GetFrame_OffenceListDS(int frameIntNo)
        {
            SqlDataAdapter sqlDAFrame_Offences = new SqlDataAdapter();
            DataSet dsFrame_Offences = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAFrame_Offences.SelectCommand = new SqlCommand();
            sqlDAFrame_Offences.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAFrame_Offences.SelectCommand.CommandText = "Frame_OffenceList";

            // Mark the Command as a SPROC
            sqlDAFrame_Offences.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Value = frameIntNo;
            sqlDAFrame_Offences.SelectCommand.Parameters.Add(parameterFrameIntNo);

            // Execute the command and close the connection
            sqlDAFrame_Offences.Fill(dsFrame_Offences);
            sqlDAFrame_Offences.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsFrame_Offences;
        }


        public int UpdateFrame_Offence(int frOfIntNo, int frameIntNo, int OffIntNo, string violation,
        string confirmViolation, string statusComment, string fineAmount,
        string fineAllocation, int offenderType, int referenceNo, string offenceLetter,
        string ticketNr, string ticketStatus, string dateUploaded_TMS, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("Frame_OffenceUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Value = frameIntNo;
            myCommand.Parameters.Add(parameterFrameIntNo);

            SqlParameter parameterOffIntNo = new SqlParameter("@OffIntNo", SqlDbType.Int, 4);
            parameterOffIntNo.Value = OffIntNo;
            myCommand.Parameters.Add(parameterOffIntNo);

            SqlParameter parameterViolation = new SqlParameter("@Violation", SqlDbType.Char, 1);
            parameterViolation.Value = violation;
            myCommand.Parameters.Add(parameterViolation);

            SqlParameter parameterConfirmViolation = new SqlParameter("@ConfirmViolation", SqlDbType.Char, 1);
            parameterConfirmViolation.Value = confirmViolation;
            myCommand.Parameters.Add(parameterConfirmViolation);

            SqlParameter parameterStatusComment = new SqlParameter("@StatusComment", SqlDbType.VarChar, 30);
            parameterStatusComment.Value = statusComment;
            myCommand.Parameters.Add(parameterStatusComment);

            SqlParameter parameterFineAmount = new SqlParameter("@FineAmount", SqlDbType.Real);
            parameterFineAmount.Value = fineAmount;
            myCommand.Parameters.Add(parameterFineAmount);

            SqlParameter parameterFineAllocation = new SqlParameter("@FineAllocation", SqlDbType.Char, 1);
            parameterFineAllocation.Value = fineAllocation;
            myCommand.Parameters.Add(parameterFineAllocation);

            SqlParameter parameterOffenderType = new SqlParameter("@OffenderType", SqlDbType.Int, 4);
            parameterOffenderType.Value = offenderType;
            myCommand.Parameters.Add(parameterOffenderType);

            SqlParameter parameterReferenceNo = new SqlParameter("@ReferenceNo", SqlDbType.Int, 4);
            parameterReferenceNo.Value = referenceNo;
            myCommand.Parameters.Add(parameterReferenceNo);

            SqlParameter parameterOffenceLetter = new SqlParameter("@OffenceLetter", SqlDbType.Char, 2);
            parameterOffenceLetter.Value = offenceLetter;
            myCommand.Parameters.Add(parameterOffenceLetter);

            SqlParameter parameterTicketNr = new SqlParameter("@TicketNr", SqlDbType.VarChar, 20);
            parameterTicketNr.Value = ticketNr;
            myCommand.Parameters.Add(parameterTicketNr);

            SqlParameter parameterTicketStatus = new SqlParameter("@TicketStatus", SqlDbType.VarChar, 255);
            parameterTicketStatus.Value = ticketStatus;
            myCommand.Parameters.Add(parameterTicketStatus);

            SqlParameter parameterDateUploaded_TMS = new SqlParameter("@DateUploaded_TMS", SqlDbType.SmallDateTime);
            parameterDateUploaded_TMS.Value = dateUploaded_TMS;
            myCommand.Parameters.Add(parameterDateUploaded_TMS);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterFrOfIntNo = new SqlParameter("@FrOfIntNo", SqlDbType.Int);
            parameterFrOfIntNo.Direction = ParameterDirection.InputOutput;
            parameterFrOfIntNo.Value = frOfIntNo;
            myCommand.Parameters.Add(parameterFrOfIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int FrOfIntNo = (int)myCommand.Parameters["@FrOfIntNo"].Value;
                //int menuId = (int)parameterFrOfIntNo.Value;

                return FrOfIntNo;
            }
            catch
            {
                myConnection.Dispose();
                return 0;
            }
        }


        public String DeleteFrame_Offence(int frOfIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("Frame_OffenceDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFrOfIntNo = new SqlParameter("@FrOfIntNo", SqlDbType.Int, 4);
            parameterFrOfIntNo.Value = frOfIntNo;
            parameterFrOfIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterFrOfIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int FrOfIntNo = (int)parameterFrOfIntNo.Value;

                return FrOfIntNo.ToString();
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return String.Empty;
            }
        }
    }
}


