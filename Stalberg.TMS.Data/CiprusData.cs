using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using Stalberg.TMS;

namespace Stalberg.TMS.Data
{
    public class Ciprus21Record         //Notice numbers record - up fil
    {
        public string recType;          // N2  = 52
        public string interfaceType;    // A1  = C
        public string versionNo;        // N2 = 01
        public string noOfUsedPanels;   //N2 1-10
        public string panel1Start;      //A7 = doc type (2) + Serial No (5)
        public string panel1End;        //A7 = doc type (2) + Serial No (5)
        public string panel2Start;      //A7 = doc type (2) + Serial No (5)
        public string panel2End;        //A7 = doc type (2) + Serial No (5)
        public string panel3Start;      //A7 = doc type (2) + Serial No (5)
        public string panel3End;        //A7 = doc type (2) + Serial No (5)
        public string panel4Start;      //A7 = doc type (2) + Serial No (5)
        public string panel4End;        //A7 = doc type (2) + Serial No (5)
        public string panel5Start;      //A7 = doc type (2) + Serial No (5)
        public string panel5End;        //A7 = doc type (2) + Serial No (5)
        public string panel6Start;      //A7 = doc type (2) + Serial No (5)
        public string panel6End;        //A7 = doc type (2) + Serial No (5)
        public string panel7Start;      //A7 = doc type (2) + Serial No (5)
        public string panel7End;        //A7 = doc type (2) + Serial No (5)
        public string panel8Start;      //A7 = doc type (2) + Serial No (5)
        public string panel8End;        //A7 = doc type (2) + Serial No (5)
        public string panel9Start;      //A7 = doc type (2) + Serial No (5)
        public string panel9End;        //A7 = doc type (2) + Serial No (5)
        public string panel10Start;     //A7 = doc type (2) + Serial No (5)
        public string panel10End;       //A7 = doc type (2) + Serial No (5)
    }

    public class Ciprus66Record         //Contravention record - Down file
    {
        //added elements to struct
        public string recType;          // N2 = 66
        public string interfaceType;    // A1 = C
        public string versionNo;        // N2 = 01
        public string supplierCode;     // A1 = Q 
        public string cameraID;         // N3
        public string autNo;            // N3 = autNo
        public string noticeNo;         // N7 = DocType(2) + Serial No (7) - padeleft with zeroes
        public string filmNo;           // N8
        public string refNo;            // N9 = frame intno
        public string firstSpeed;       // N4 = 0789 = 78.9 km/h
        public string secondSpeed;      // N4 = 1200 = 120.0 km/h
        public string officerNo;        // A9
        public string cameraLoc;        // N6
        public string courtNo;          // N6
        public string offenceDate;      // D8 = CCYYMMDD
        public string offenceTime;      // T4 = HHMM
        public string vehMCode;         // N2
        public string vehTCode;         // N2
        public string regNo;            // A10
        public string travelDirection;  // A1
        public string offenceCode;      // N6
        public string typeVehOwner;     // N1 = (1) private, (2) representative of a business, (3) Business, no rep
        public string idNo;             // N13
        public string surname;          // A30
        public string initials;         // A5
        public string businessName;     // A30
        public string streetAddrLine1;  // A26;
        public string streetAddrLine2;  // A26;
        public string streetAddrLine3;  // A26;
        public string streetAddrCode;   // N4;
        public string postAddrLine1;    // A26;
        public string postAddrLine2;    // A26;
        public string postAddrLine3;    // A26;
        public string postAddrCode;     // N4;
        public string dateCaptured;     // D8 = CCYYMMDD
        public string dateVerified;     // D8 = CCYYMMDD
        public string dateNatisReceived;// D8 = CCYYMMDD
        public string dateAdjudicated;  // D8 = CCYYMMDD
        public string adjOfficerNo;     // A9
    }

    public class Ciprus52Record         //Down file
    {
        //added elements to struct
        public string recType;   // N2  = 52
        public string interfaceType; // A1  = C
        public string versionNo; // N2 = 04
        public string supplierCode;    // A1 = T (Tellumat)
        public string cameraID;  // N3
        public string systemCode; // N3
        public string filmNoOld; // N4
        public string refNo; // N9  - frame intno
        public string firstSpeed;    // N4
        public string secondSpeed;   // N4
        public string officerNo; // A9
        public string cameraLocOld;  // N4
        public string offenceLoc;  // A60
        public string courtNo; // N6
        public string offenceDate; // N8  - CCYYMMDD
        public string offenceTime; // N4  - HHMM
        public string vehMCode; // N2
        public string vehTCode; // N2
        public string regNo;    // A10
        public string travelDirection; // A1
        public string offenceCodeOld;  // N5
        public string dataInsertionCode; // N3
        public string insertionData; // N3
        public string cameraLoc; // N6
        public string filmNo; // N8
        public string branch; // N2
        public string offenceCode; // N6
        //public string metroCode;
        //public string autPCGateway;
    }

    /// <summary>
    /// Represents a Ciprus Request Payment File Record (Type = 55)
    /// </summary>
    public class Ciprus55Record
    {
        // Fields
        public int AutIntNo;
        public readonly string Type = "55";
        public readonly string Interface = "C";
        public readonly string VersionControlNumber = "09"; // As indicated by Willie Blignault
        public string SystemCode; // LA code
        public readonly string SupplierCode = "Q"; // As indicated by Willie Blignault
        public DateTime StartDate;
        public DateTime EndDate;
        public string FTPGateway;

        // Constants
        private const string DATE_FORMAT = "yyyyMMdd";

        /// <summary>
        /// Writes the file output of this record.
        /// </summary>
        /// <returns>A <see cref="String"/> containing the output of the record</returns>
        public string Write()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(this.Type);
            sb.Append(this.Interface);
            sb.Append(this.VersionControlNumber);
            sb.Append(this.SystemCode.Trim().PadLeft(3, '0'));
            sb.Append(this.SupplierCode);
            sb.Append(this.StartDate.ToString(DATE_FORMAT));
            sb.Append(this.EndDate.ToString(DATE_FORMAT));
            sb.Append(this.FTPGateway.PadRight(20, ' '));

            return sb.ToString();
        }
    }

    public class Ciprus59UpControlRecord
    {
        public string recType; // As String * 2               'N  = 59
        public string interfaceType; // As String * 1         'A  = C
        public string versionNo; // As String * 2             'N
        public string recCount; // As String * 6              'N  - excludes control record
        public string noticesCancelled; // As String * 3      'N
    }

    public class CiprusPrintRequestAuthRecord
    {
        //added elements to struct
        public string recType;   // N2  = 02
        public string versionNo; // N3 = 001
        public string systemCode; // N3
        public string authName; // A30
        public string issuedBy; // A30 - Print as "Issued by" on the Sec 341.
    }

    public class CiprusPrintRequestCourtRecord
    {
        //added elements to struct
        public string recType;   // N2  = 03
        public string versionNo; // N3 = 001
        public string systemCode; // N3
        public string courtNo; // N6
        public string branchCode; // N2         -- dls 060214 - added from latest Ciprus spec
        public string courtName; // A30         -- dls 060214 - added from latest Ciprus spec
        public string paymentAddr1; // A25
        public string paymentAddr2; // A25
        public string paymentAddr3; // A25
        public string paymentAddr4; // A25
    }

    public class CiprusPrintRequestDataRecord
    {
        //added elements to struct
        public string recType;   // N2  = 10
        public string versionNo; // N3 = 001
        public string systemCode; // N3
        public string noticeNo; // A16          -- need to add / in the following way: 12/34567/890/123456
        public string branchCode; // N2
        public string docType;      //N1        -- dls 060214 - added from latest Ciprus spec: 1=Speed, 2=RLV
        public string supplierCode; // A1
        public string refNo; // N9 - frameintno
        public string filmNo; // N8
        public string offenderType; // N1 - 1 = private person, 2 = responsible person/proxy
        public string companyName; // N30
        public string offenderIDNo;    // N13
        public string offenderSName;    // A30
        public string offenderFName;    // A40
        public string offenderInit;    // A5
        public string postAddr1;    // A30
        public string postAddr2;    // A30
        public string postAddr3;    // A30
        public string postCode;    // N4
        public string vehRegNo;     // A10
        public string vehMake;      // A30
        public string vehType;      // A30
        public string speed;      // N5 - KKK.D
        public string parkMeterNo;  // A10
        public string offenceDate;  // D8
        public string offenceTime;  // T6
        public string offenceCode;  // N6
        public string locLine1; // A20
        public string locLine2; // A40
        public string officerNo;      // A9
        public string courtNo;    // N6
        public string fineAmount;   // N7   - RRRRRCC
        public string easyPayNo;    // N17  - print with the EasyPay logo if there is data in this field
    }

    public class CiprusPrintRequestChargeSheetRecord
    {
        //added elements to struct
        public string recType;   // N2  = 20
        public string versionNo; // N3 = 001
        public string systemCode; // N3
        public string offenceCode;  // N6
        public string statRefShort; // A46
        public string statRefLine1; // A46
        public string statRefLine2; // A70
        public string statRefLine3; // A70
        public string statRefLine4; // A70
        public string chargeDescrLine1; // A46
        public string chargeDescrLine2; // A46
        public string chargeDescrLine3; // A46
    }

    public class CiprusPrintRequestDeleteImageRecord
    {
        //added elements to struct
        public string recType;   // N2  = 95
        public string versionNo; // N3 = 001
        public string systemCode; // N3
        public string branchCode;  // N2
        public string supplierCode; // A1
        public string noticeNo; // A16
        public string refNo; // N9 - frameintno
        public string filmNo; // N8
    }

    public class CiprusPrintRequestFileTrailerRecord
    {
        //added elements to struct
        public string recType;   // N2  = 99
        public string versionNo; // N3 = 001
        public string noOfRecords; // N8
    }

    /// <summary>
    /// Represents the notice and receipt identities of a spot fine
    /// </summary>
    public struct SpotFineReceipt
    {
        //BD 3858 update to handle the storing of which items we want to print in the print file
        
        // Fields
        private int notIntNo;
        private int rctIntNo;
        private int autIntNo;
        private bool isRLV;
        private bool printNotice;
        private bool printReceipt;

		/// <summary>
		/// Initializes a new instance of the <see cref="SportFineReceipt"/> struct.
		/// </summary>
		/// <param name="notIntNo">The not int no.</param>
		/// <param name="rctIntNo">The Receipt int no.</param>
		/// <param name="autIntNo">The Authority int no.</param>
		/// <param name="isRLV">if set to <c>true</c> this notice is an RLV.</param>
        public SpotFineReceipt(int notIntNo, int rctIntNo, int autIntNo, bool isRLV, bool printNotice, bool printReceipt)
        {
            this.notIntNo = notIntNo;
            this.rctIntNo = rctIntNo;
            this.autIntNo = autIntNo;
            this.isRLV = isRLV;
            this.printNotice = printNotice;
            this.printReceipt = printReceipt;
        }

        /// <summary>
        /// Gets a value indicating whether this receipt was for a red light violation.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is red light violation; otherwise, <c>false</c>.
        /// </value>
        public bool IsRedLightViolation
        {
            get { return this.isRLV; }
        }

        public bool PrintReceipt
        {
            get { return this.printReceipt; }
        }

        public bool PrintNotice
        {
            get { return this.printNotice; }
        }

        /// <summary>
        /// Gets the Authority int no.
        /// </summary>
        /// <value>The authority int no.</value>
        public int AutIntNo
        {
            get { return this.autIntNo; }
        }

        /// <summary>
        /// Gets the Receipt int no.
        /// </summary>
        /// <value>The Receipt int no.</value>
        public int RctIntNo
        {
            get { return this.rctIntNo; }
        }

        /// <summary>
        /// Gets the Notice int no.
        /// </summary>
        /// <value>The notice int no.</value>
        public int NotIntNo
        {
            get { return this.notIntNo; }
        }

    }

    /// <summary>
    /// Contains the database logic for interacting with Ciprus data
    /// </summary>
    public class CiprusData
    {
        // Fields
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="CiprusData"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public CiprusData(string connectionString)
        {
            this.connectionString = connectionString;
        }

        //public SqlDataReader GetCiprusContraventionData(int autIntNo, string camUnitID, int statusLoaded, int statusFixed)
        // 2013-07-19 comment by Henry for useless
        //public DataSet GetCiprusContraventionData(int autIntNo, string camUnitID, int statusLoaded, int statusFixed)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("NoticeContraventionData_CPI", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;
        //    //dls 070720 - allow for timeouts
        //    myCommand.CommandTimeout = 0;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
        //    parameterCamUnitID.Value = camUnitID;
        //    myCommand.Parameters.Add(parameterCamUnitID);

        //    SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
        //    parameterStatusLoaded.Value = statusLoaded;
        //    myCommand.Parameters.Add(parameterStatusLoaded);

        //    SqlParameter parameterStatusFixed = new SqlParameter("@StatusFixed", SqlDbType.Int, 4);
        //    parameterStatusFixed.Value = statusFixed;
        //    myCommand.Parameters.Add(parameterStatusFixed);

        //    SqlDataAdapter da = new SqlDataAdapter(myCommand);
        //    DataSet ds = new DataSet();
        //    da.Fill(ds);

        //    return ds;
        //}

        public DataSet GetCiprusAartoInfringementData(int autIntNo, string camUnitID, int statusLoaded, int statusFixed)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("NoticeInfringementData_CA", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            //dls 070720 - allow for timeouts
            myCommand.CommandTimeout = 0;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
            parameterCamUnitID.Value = camUnitID;
            myCommand.Parameters.Add(parameterCamUnitID);

            SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
            parameterStatusLoaded.Value = statusLoaded;
            myCommand.Parameters.Add(parameterStatusLoaded);

            SqlParameter parameterStatusFixed = new SqlParameter("@StatusFixed", SqlDbType.Int, 4);
            parameterStatusFixed.Value = statusFixed;
            myCommand.Parameters.Add(parameterStatusFixed);

            SqlDataAdapter da = new SqlDataAdapter(myCommand);
            DataSet ds = new DataSet();
            da.Fill(ds);

            return ds;
        }

        // 2013-07-19 comment by Henry for useless
        //public SqlDataReader GetCiprus52Data(int filmIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("Ciprus52Data", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.VarChar, 20);
        //    parameterFilmIntNo.Value = filmIntNo;
        //    myCommand.Parameters.Add(parameterFilmIntNo);
        //    try
        //    {
        //        myConnection.Open();
        //        SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //        return result;
        //    }
        //    catch (Exception e)
        //    {
        //        string error = e.Message;
        //        return null;
        //    }
        //}

        public SqlDataReader GetCiprus50Data(string fileName)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("Ciprus50Data", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFileName = new SqlParameter("@FileName", SqlDbType.VarChar, 50);
            parameterFileName.Value = fileName;
            myCommand.Parameters.Add(parameterFileName);
            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception e)
            {
                string error = e.Message;
                return null;
            }
        }

        public Ciprus59UpControlRecord GetCiprus59Data()
        {
            // Create Struct
            Ciprus59UpControlRecord my59 = new Ciprus59UpControlRecord();
            // Populate Struct
            my59.recType = "59";
            my59.interfaceType = "C";
            my59.versionNo = "04";
            my59.recCount = "0"; // insert the record count here
            my59.noticesCancelled = "   "; // (3)
            return my59;
        }

        /// <summary>
        /// Gets a list of the payment file requests for each LA where a request is eligible.
        /// </summary>
        /// <returns>A list of <see cref="Ciprus55Record"/>s</returns>
        public Ciprus55Record GetPaymentFileRequest(int autIntNo)
        {
            Ciprus55Record record = null;

            SqlConnection con = new SqlConnection(this.connectionString);
            try
            {
                // Check each LA for whether a request is due
                SqlCommand com = new SqlCommand("CiprusGetPaymentFileRequests", con);
                com.CommandType = CommandType.StoredProcedure;

                com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

                // Populate the list of request objects
                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    record = new Ciprus55Record();
                    record.AutIntNo = (int)reader["AutIntNo"];
                    // dls 080111 - have added a latency of 7 days so that Ciprus payments can catch up any batch backlogs
                    //record.EndDate = DateTime.Today.AddDays(1.0);
                    record.EndDate = (DateTime)reader["EndDate"];
                    record.FTPGateway = reader["AutPCGateway"].ToString().Trim();
                    //record.StartDate = (DateTime)reader["LastSpotFineBatchRequestDate"];
                    record.StartDate = (DateTime)reader["StartDate"];
                    record.SystemCode = reader["AutNo"].ToString();
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Assert(false, ex.Message);
            }
            finally
            {
                con.Close();
            }

            return record;
        }

        /// <summary>
        /// Updates the spot fine request date for each authority in the list.
        /// </summary>
        /// <param name="requests">The list of requests.</param>
        // 2013-07-24 comment out by Henry for useless
        //public void UpdateSpotFineRequestDate(Ciprus55Record request)
        //{
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand cmd = new SqlCommand("CiprusSetPaymentFileRequestDate", con);
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    cmd.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = request.AutIntNo;
        //    cmd.Parameters.Add("@EndDate", SqlDbType.SmallDateTime).Value = request.EndDate;

        //    try
        //    {
        //        con.Open();
        //        cmd.ExecuteNonQuery();
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        /// <summary>
        /// Imports the spot fine data into the database.
        /// </summary>
        /// <param name="record">The spot fine data record.</param>
        /// <param name="lastUser">The last user.</param>
        // 2013-07-24 comment out by Henry for useless
        //public bool ImportSpotFineData(CiprusPaymentResponseFile.Record record, string lastUser)
        //{
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand cmd = new SqlCommand("CiprusCreateSpotFineBatchNotice", con);
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    cmd.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = record.AutIntNo;
        //    cmd.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = record.NotIntNo;
        //    cmd.Parameters.Add("@PaymentDate", SqlDbType.SmallDateTime, 4).Value = record.PaymentDate;
        //    cmd.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = record.Amount == 0 ? -record.Refund : record.Amount;
        //    cmd.Parameters.Add("@CashType", SqlDbType.Char, 1).Value = Helper.GetCashTypeChar(record.PaymentType);

        //    //dls 090112 - added fields
        //    cmd.Parameters.Add("@RctNo", SqlDbType.VarChar, 10).Value = record.RctNo;
        //    cmd.Parameters.Add("@BatchSize", SqlDbType.Int, 4).Value = record.BatchSize;
        //    cmd.Parameters.Add("@PaymentCentreName", SqlDbType.VarChar, 30).Value = record.PaymentCentreName;
        //    cmd.Parameters.Add("@PaymentCentreCode", SqlDbType.Char, 3).Value = record.PaymentCentreCode;
        //    cmd.Parameters.Add("@AGNumber", SqlDbType.VarChar, 25).Value = record.AGNumber;//Jerry 2013-02-04 change it from 15 to 25
        //    cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

        //    try
        //    {
        //        con.Open();
        //        int i = Convert.ToInt32(cmd.ExecuteScalar());

        //        return i == 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex);
        //        return false;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

		/// <summary>
		/// Gets the spot fine batch print data.
		/// </summary>
		/// <param name="printFile">The print file.</param>
		/// <param name="receipts">The receipts.</param>
		/// <returns>
		/// 	<c>true</c> if the batch is valid for printing
		/// </returns>
        // 2013-07-24 add parameter lastUser by Henry
        public bool SpotFineBatchPrintData(string printFile, DateTime userCompleteDate, List<SpotFineReceipt> receipts, string lastUser) 
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand cmd = new SqlCommand("SpotFineBatchPrintCheck", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@PrintName", SqlDbType.VarChar, 50).Value = printFile;
            cmd.Parameters.Add("@UserCompleteDate", SqlDbType.VarChar, 50).Value = userCompleteDate;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            SpotFineReceipt receipt;
            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                // Get the valid indicator
                if (!reader.Read())
                    return false;
                bool response = reader.GetBoolean(0);
                if (!response)
                    return false;

                reader.NextResult();

                // Get the list of notice receipts
                while (reader.Read())
                {
                    receipt = new SpotFineReceipt(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2), reader.GetBoolean(3), reader.GetBoolean(4), reader.GetBoolean(5));
                    receipts.Add(receipt);
                }
                reader.Close();

                return response;
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Insert error line into ciprs errors table for failed file
        /// </summary>
        /// <param name="requests">The list of requests.</param>
        public void InsertCivitasLoadError(string fileName, DateTime fileDate, string fileError)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand cmd = new SqlCommand("CivitasLoadErrorAdd", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = fileName;
            cmd.Parameters.Add("@FileDate", SqlDbType.SmallDateTime).Value = fileDate;
            cmd.Parameters.Add("@FileError", SqlDbType.VarChar, 250).Value = fileError;

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }

        // 2013-07-19 comment by Henry for useless
        /// <summary>
        /// Imports the spot fine data into the database.
        /// </summary>
        /// <param name="record">The spot fine data record.</param>
        /// <param name="lastUser">The last user.</param>
        //public int ImportSummonsData(CiprusSummonsFile.Record record, string lastUser)
        //{
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand cmd = new SqlCommand("CiprusCreateSummons", con);
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    cmd.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = record.AutIntNo;
        //    cmd.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = record.NotIntNo;
        //    cmd.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = record.TicketNo.Trim();
        //    cmd.Parameters.Add("@Type", SqlDbType.Int, 4).Value = record.Type;
        //    cmd.Parameters.Add("@LocalAuthCode", SqlDbType.Int, 4).Value = record.LocalAuthCode;
        //    cmd.Parameters.Add("@BranchCode", SqlDbType.Int, 4).Value = record.BranchCode;
        //    cmd.Parameters.Add("@SummonsType", SqlDbType.VarChar, 4).Value = record.SummonsType.ToString();
        //    cmd.Parameters.Add("@DocumentSequence", SqlDbType.Int, 4).Value = record.DocumentSequence;
        //    cmd.Parameters.Add("@NoticeNo", SqlDbType.VarChar, 50).Value = record.NoticeNo.Trim();
        //    cmd.Parameters.Add("@PostalCode", SqlDbType.Int, 4).Value = record.PostalCode;
        //    cmd.Parameters.Add("@MagistratesCourtCode", SqlDbType.VarChar, 50).Value = record.MagistratesCourtCode.ToString();
        //    cmd.Parameters.Add("@CourtDate", SqlDbType.SmallDateTime).Value = record.CourtDate;
        //    cmd.Parameters.Add("@SummonsNo", SqlDbType.VarChar, 50).Value = record.SummonsNo.Trim();
        //    cmd.Parameters.Add("@VersionNo", SqlDbType.Int, 4).Value = record.VersionNo;
        //    cmd.Parameters.Add("@AccusedTelNo", SqlDbType.VarChar, 15).Value = record.AccusedTelNo.Trim();
        //    cmd.Parameters.Add("@NotUsed", SqlDbType.VarChar, 50).Value = record.NotUsed;
        //    cmd.Parameters.Add("@AccusedName", SqlDbType.VarChar, 100).Value = record.AccusedName.Trim();
        //    cmd.Parameters.Add("@AccusedInitials", SqlDbType.VarChar, 10).Value = record.AccusedInitials.Trim();
        //    cmd.Parameters.Add("@CompanyName", SqlDbType.VarChar, 50).Value = record.CompanyName.Trim();
        //    cmd.Parameters.Add("@OffenderAddress1", SqlDbType.VarChar, 100).Value = record.OffenderAddress1.Trim();
        //    cmd.Parameters.Add("@OffenderAddress2", SqlDbType.VarChar, 100).Value = record.OffenderAddress2.Trim();
        //    cmd.Parameters.Add("@OffenderAddress3", SqlDbType.VarChar, 100).Value = record.OffenderAddress3.Trim();
        //    cmd.Parameters.Add("@CameraSupplierCode", SqlDbType.VarChar, 50).Value = record.CameraSupplierCode.Trim();
        //    cmd.Parameters.Add("@AccusedIDNo", SqlDbType.VarChar, 25).Value = record.AccusedIDNo.Trim();
        //    cmd.Parameters.Add("@SummonsDate", SqlDbType.SmallDateTime).Value = record.SummonsDate;
        //    cmd.Parameters.Add("@VehicleRegNo", SqlDbType.VarChar, 15).Value = record.VehicleRegNo.Trim();
        //    cmd.Parameters.Add("@FineAmount", SqlDbType.Money).Value = record.FineAmount;
        //    cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser.Trim();

        //    //cmd.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = record.AutIntNo;
        //    //cmd.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = record.NotIntNo;
        //    //cmd.Parameters.Add("@PaymentDate", SqlDbType.SmallDateTime, 4).Value = record.PaymentDate;
        //    //cmd.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = record.Amount == 0 ? -record.Refund : record.Amount;
        //    //cmd.Parameters.Add("@CashType", SqlDbType.Char, 1).Value = Helper.GetCashTypeChar(record.PaymentType);
        //    //cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

        //    try
        //    {
        //        con.Open();
        //        int i = Convert.ToInt32(cmd.ExecuteScalar());

        //        //return i == 0;
        //        return i;
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex);
        //        return -10;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}
    }
}
