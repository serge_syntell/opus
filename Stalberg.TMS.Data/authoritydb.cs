using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;


namespace Stalberg.TMS
{
    /// <summary>
    /// A simple data class that encapsulates details about a particular user 
    /// </summary>
    public class AuthorityDetails
    {
        public Int32 AutIntNo;
        public Int32 MtrIntNo;
        public string AutCode;
        public string AutNo;
        public string AutName;
        public string AutPostAddr1;
        public string AutPostAddr2;
        public string AutPostAddr3;
        public string AutPostCode;
        public string AutTel;
        public string AutFax;
        public string AutEmail;
        public string LastUser;
        public string BranchCode;
        public string BranchName;
        public string AutTicketProcessor;
        public string AutPCGateway;
        public string Aut3PCodingSystem;
        public string AutPhysAddr1;
        public string AutPhysAddr2;
        public string AutPhysAddr3;
        public string AutPhysAddr4;
        public string AutPhysCode;
        public string AutNoticePaymentInfo;
        public string AutNoticeIssuedByInfo;
        public int EasyPayReceiptID;                //EasyPayReceiptIdentifier;
        public int EasyPayNumberLength;             //EasyPayAccountNumberLength;
        public int JudgementPeriod;
        public int GracePeriod;
        public string GracePeriodRptEmail;              //GracePeriodEmail;
        public string AutAGListEmail;               //AGEmail;
        public string AutSpotFineEmail;             //SFEmail;
        public int NPIntNo;
        public DateTime LastSpotFineBatchRequestDate;
        public DateTime SummonsLastCreateDate;
        public DateTime AutAGListLastRunDate;
        public string AutEmailWarrant;
        // jerry 2011-1-12 add
        public int ENatisAuthorityNumber;
        //add by richard
        public string AutDocumentFromEnglish;
        public string AutDocumentFromAfri;
        public string AutOfficialName;
        public string AutFooterText2;
        public string AutFooterText1;
        public string AutDepartName;
        public string AutDocumentFrom;
        public byte[] AutLogo;
        public string AutNameAfri;
        //Oscar 20120202 add
        public string AutNatisCode;
        public string AutSendNatisEmail;
        //2013-10-23 added by Nancy start
        public string BankAccountName;
        public string BankName;
        public string BankAccountNo;
        public string BankBranchCode;
        //2013-10-23 added by Nancy end
    }

    /// <summary>
    /// Business/Data Logic Class that encapsulates all data
    /// logic necessary to add/login/query Authorities within
    /// the Commerce Starter Kit Customer database.
    /// </summary>
    public class AuthorityDB
    {
        // Fields
        string mConstr = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorityDB"/> class.
        /// </summary>
        /// <param name="vConstr">The v constr.</param>
        public AuthorityDB(string vConstr)
        {
            mConstr = vConstr;
        }

        /// <summary>
        /// The GetAuthorityDetails method returns a AuthorityDetails
        /// struct that contains information about a specific
        /// customer (name, password, etc).
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        /// 
        public AuthorityDetails GetAuthorityDetails(int autIntNo)
        {

            return GetAuthorityDetails(autIntNo, string.Empty);
        }

        public AuthorityDetails GetAuthorityDetails(int autIntNo, string systemEmailAddress)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthorityDetail", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            #region Jerry 2014-10-27 add try catch finally
            //myConnection.Open();
            //SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            //// Create CustomerDetails Struct
            //AuthorityDetails myAuthorityDetails = new AuthorityDetails();

            //if (result.Read())
            //{
            //    // Populate Struct using Output Params from SPROC
            //    myAuthorityDetails.MtrIntNo = Convert.ToInt32(result["MtrIntNo"]);
            //    myAuthorityDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
            //    myAuthorityDetails.AutCode = result["AutCode"].ToString();
            //    myAuthorityDetails.AutNo = result["AutNo"].ToString();
            //    myAuthorityDetails.AutName = result["AutName"].ToString();
            //    myAuthorityDetails.AutPostAddr1 = result["AutPostAddr1"].ToString();
            //    myAuthorityDetails.AutPostAddr2 = result["AutPostAddr2"].ToString();
            //    myAuthorityDetails.AutPostAddr3 = result["AutPostAddr3"].ToString();
            //    myAuthorityDetails.AutPostCode = result["AutPostCode"].ToString();
            //    myAuthorityDetails.AutTel = result["AutTel"].ToString();
            //    myAuthorityDetails.AutFax = result["AutFax"].ToString();
            //    myAuthorityDetails.AutEmail = result["AutEmail"].ToString();
            //    myAuthorityDetails.AutNatisCode = result["AutNaTISCode"].ToString();
            //    myAuthorityDetails.BranchCode = result["BranchCode"].ToString();
            //    myAuthorityDetails.BranchName = result["BranchName"].ToString();
            //    myAuthorityDetails.AutTicketProcessor = result["AutTicketProcessor"].ToString();
            //    myAuthorityDetails.AutSendNatisEmail = result["AutSendNatisEmail"].ToString();
            //    myAuthorityDetails.AutPCGateway = result["AutPCGateway"].ToString();
            //    //myAuthorityDetails.AutNatisFolder = result["AutNatisFolder"].ToString();
            //    myAuthorityDetails.Aut3PCodingSystem = result["Aut3PCodingSystem"].ToString();
            //    myAuthorityDetails.AutPhysAddr1 = result["AutPhysAddr1"].ToString();
            //    myAuthorityDetails.AutPhysAddr2 = result["AutPhysAddr2"].ToString();
            //    myAuthorityDetails.AutPhysAddr3 = result["AutPhysAddr3"].ToString();
            //    myAuthorityDetails.AutPhysAddr4 = result["AutPhysAddr4"].ToString();
            //    myAuthorityDetails.AutPhysCode = result["AutPhysCode"].ToString();
            //    myAuthorityDetails.AutNoticePaymentInfo = result["AutNoticePaymentInfo"].ToString();
            //    myAuthorityDetails.AutNoticeIssuedByInfo = result["AutNoticeIssuedByInfo"].ToString();
            //    // jerry 2011-1-12 add
            //    myAuthorityDetails.ENatisAuthorityNumber = Convert.ToInt32(result["eNatisAuthorityNumber"]);

            //    if (result["EasyPayReceiptID"] != DBNull.Value)
            //        myAuthorityDetails.EasyPayReceiptID = (int)result["EasyPayReceiptID"];

            //    if (result["EasyPayNumberLength"] != DBNull.Value)
            //        myAuthorityDetails.EasyPayNumberLength = (int)result["EasyPayNumberLength"];

            //    myAuthorityDetails.JudgementPeriod = Convert.ToInt32(result["JudgementPeriod"]);
            //    myAuthorityDetails.GracePeriod = Convert.ToInt32(result["GracePeriod"]);

            //    if (result["GracePeriodRptEmail"].ToString().Equals(string.Empty))
            //        myAuthorityDetails.GracePeriodRptEmail = systemEmailAddress;
            //    else
            //        myAuthorityDetails.GracePeriodRptEmail = result["GracePeriodRptEmail"].ToString();

            //    if (result["AutEmailWarrant"].ToString().Equals(string.Empty))
            //        myAuthorityDetails.AutEmailWarrant = systemEmailAddress;
            //    else
            //        myAuthorityDetails.AutEmailWarrant = result["AutEmailWarrant"].ToString();

            //    if (result["AutAGListEmail"].ToString().Equals(string.Empty))
            //        myAuthorityDetails.AutAGListEmail  = systemEmailAddress;
            //    else 
            //        myAuthorityDetails.AutAGListEmail = result["AutAGListEmail"].ToString();

            //    if (result["AutSpotFineEmail"].ToString().Equals(string.Empty))
            //        myAuthorityDetails.AutSpotFineEmail  = systemEmailAddress;
            //    else
            //        myAuthorityDetails.AutSpotFineEmail = result["AutSpotFineEmail"].ToString();

            //    if (result["AutAGListLastRunDate"] != DBNull.Value)
            //        myAuthorityDetails.AutAGListLastRunDate = Convert.ToDateTime(result["AutAGListLastRunDate"].ToString());
                
            //    if (result["SummonsLastCreateDate"] != DBNull.Value)
            //        myAuthorityDetails.SummonsLastCreateDate = Convert.ToDateTime(result["SummonsLastCreateDate"].ToString());

            //    if (result["LastSpotFineBatchRequestDate"] != DBNull.Value)
            //        myAuthorityDetails.LastSpotFineBatchRequestDate = Convert.ToDateTime(result["LastSpotFineBatchRequestDate"].ToString());
            //   // myAuthorityDetails.AutDocumentFrom = result["AutDocumentFrom"].ToString();
            //    myAuthorityDetails.AutDepartName = result["AutDepartName"].ToString();
            //    myAuthorityDetails.AutFooterText1 = result["AutFooterText1"].ToString();
            //    myAuthorityDetails.AutFooterText2 = result["AutFooterText2"].ToString();
            //    myAuthorityDetails.AutOfficialName = result["AutOfficialName"].ToString();
            //    myAuthorityDetails.AutDocumentFromAfri = result["AutDocumentFromAfri"].ToString();
            //    myAuthorityDetails.AutDocumentFromEnglish = result["AutDocumentFromEnglish"].ToString();
            //    //myAuthorityDetails.AutLogo = string.IsNullOrEmpty(result["AutLogo"].ToString()) ? null : (byte[])result["AutLogo"];
            //    //2011-9-7 jerry add
            //    myAuthorityDetails.AutNameAfri = result["AutNameAfrikaans"].ToString();

            //    //2013-10-30 added by Nancy start
            //    if (result["BankAccountName"] != DBNull.Value)
            //    {
            //        myAuthorityDetails.BankAccountName = result["BankAccountName"].ToString();
            //    }
            //    if (result["BankName"] != DBNull.Value)
            //    {
            //        myAuthorityDetails.BankName = result["BankName"].ToString();
            //    }
            //    if (result["BankAccountNo"] != DBNull.Value)
            //    {
            //        myAuthorityDetails.BankAccountNo = result["BankAccountNo"].ToString();
            //    }
            //    if (result["BankBranchCode"] != DBNull.Value)
            //    {
            //        myAuthorityDetails.BankBranchCode = result["BankBranchCode"].ToString();
            //    }
            //    //2013-10-30 added by Nancy end
            //}

            //result.Close();
            #endregion

            // Create CustomerDetails Struct
            AuthorityDetails myAuthorityDetails = new AuthorityDetails();

            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                if (result.Read())
                {
                    // Populate Struct using Output Params from SPROC
                    myAuthorityDetails.MtrIntNo = Convert.ToInt32(result["MtrIntNo"]);
                    myAuthorityDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
                    myAuthorityDetails.AutCode = result["AutCode"].ToString();
                    myAuthorityDetails.AutNo = result["AutNo"].ToString();
                    myAuthorityDetails.AutName = result["AutName"].ToString();
                    myAuthorityDetails.AutPostAddr1 = result["AutPostAddr1"].ToString();
                    myAuthorityDetails.AutPostAddr2 = result["AutPostAddr2"].ToString();
                    myAuthorityDetails.AutPostAddr3 = result["AutPostAddr3"].ToString();
                    myAuthorityDetails.AutPostCode = result["AutPostCode"].ToString();
                    myAuthorityDetails.AutTel = result["AutTel"].ToString();
                    myAuthorityDetails.AutFax = result["AutFax"].ToString();
                    myAuthorityDetails.AutEmail = result["AutEmail"].ToString();
                    myAuthorityDetails.AutNatisCode = result["AutNaTISCode"].ToString();
                    myAuthorityDetails.BranchCode = result["BranchCode"].ToString();
                    myAuthorityDetails.BranchName = result["BranchName"].ToString();
                    myAuthorityDetails.AutTicketProcessor = result["AutTicketProcessor"].ToString();
                    myAuthorityDetails.AutSendNatisEmail = result["AutSendNatisEmail"].ToString();
                    myAuthorityDetails.AutPCGateway = result["AutPCGateway"].ToString();
                    //myAuthorityDetails.AutNatisFolder = result["AutNatisFolder"].ToString();
                    myAuthorityDetails.Aut3PCodingSystem = result["Aut3PCodingSystem"].ToString();
                    myAuthorityDetails.AutPhysAddr1 = result["AutPhysAddr1"].ToString();
                    myAuthorityDetails.AutPhysAddr2 = result["AutPhysAddr2"].ToString();
                    myAuthorityDetails.AutPhysAddr3 = result["AutPhysAddr3"].ToString();
                    myAuthorityDetails.AutPhysAddr4 = result["AutPhysAddr4"].ToString();
                    myAuthorityDetails.AutPhysCode = result["AutPhysCode"].ToString();
                    myAuthorityDetails.AutNoticePaymentInfo = result["AutNoticePaymentInfo"].ToString();
                    myAuthorityDetails.AutNoticeIssuedByInfo = result["AutNoticeIssuedByInfo"].ToString();
                    // jerry 2011-1-12 add
                    myAuthorityDetails.ENatisAuthorityNumber = Convert.ToInt32(result["eNatisAuthorityNumber"]);

                    if (result["EasyPayReceiptID"] != DBNull.Value)
                        myAuthorityDetails.EasyPayReceiptID = (int)result["EasyPayReceiptID"];

                    if (result["EasyPayNumberLength"] != DBNull.Value)
                        myAuthorityDetails.EasyPayNumberLength = (int)result["EasyPayNumberLength"];

                    myAuthorityDetails.JudgementPeriod = Convert.ToInt32(result["JudgementPeriod"]);
                    myAuthorityDetails.GracePeriod = Convert.ToInt32(result["GracePeriod"]);

                    if (result["GracePeriodRptEmail"].ToString().Equals(string.Empty))
                        myAuthorityDetails.GracePeriodRptEmail = systemEmailAddress;
                    else
                        myAuthorityDetails.GracePeriodRptEmail = result["GracePeriodRptEmail"].ToString();

                    if (result["AutEmailWarrant"].ToString().Equals(string.Empty))
                        myAuthorityDetails.AutEmailWarrant = systemEmailAddress;
                    else
                        myAuthorityDetails.AutEmailWarrant = result["AutEmailWarrant"].ToString();

                    if (result["AutAGListEmail"].ToString().Equals(string.Empty))
                        myAuthorityDetails.AutAGListEmail = systemEmailAddress;
                    else
                        myAuthorityDetails.AutAGListEmail = result["AutAGListEmail"].ToString();

                    if (result["AutSpotFineEmail"].ToString().Equals(string.Empty))
                        myAuthorityDetails.AutSpotFineEmail = systemEmailAddress;
                    else
                        myAuthorityDetails.AutSpotFineEmail = result["AutSpotFineEmail"].ToString();

                    if (result["AutAGListLastRunDate"] != DBNull.Value)
                        myAuthorityDetails.AutAGListLastRunDate = Convert.ToDateTime(result["AutAGListLastRunDate"].ToString());

                    if (result["SummonsLastCreateDate"] != DBNull.Value)
                        myAuthorityDetails.SummonsLastCreateDate = Convert.ToDateTime(result["SummonsLastCreateDate"].ToString());

                    if (result["LastSpotFineBatchRequestDate"] != DBNull.Value)
                        myAuthorityDetails.LastSpotFineBatchRequestDate = Convert.ToDateTime(result["LastSpotFineBatchRequestDate"].ToString());
                    // myAuthorityDetails.AutDocumentFrom = result["AutDocumentFrom"].ToString();
                    myAuthorityDetails.AutDepartName = result["AutDepartName"].ToString();
                    myAuthorityDetails.AutFooterText1 = result["AutFooterText1"].ToString();
                    myAuthorityDetails.AutFooterText2 = result["AutFooterText2"].ToString();
                    myAuthorityDetails.AutOfficialName = result["AutOfficialName"].ToString();
                    myAuthorityDetails.AutDocumentFromAfri = result["AutDocumentFromAfri"].ToString();
                    myAuthorityDetails.AutDocumentFromEnglish = result["AutDocumentFromEnglish"].ToString();
                    //myAuthorityDetails.AutLogo = string.IsNullOrEmpty(result["AutLogo"].ToString()) ? null : (byte[])result["AutLogo"];
                    //2011-9-7 jerry add
                    myAuthorityDetails.AutNameAfri = result["AutNameAfrikaans"].ToString();

                    //2013-10-30 added by Nancy start
                    if (result["BankAccountName"] != DBNull.Value)
                    {
                        myAuthorityDetails.BankAccountName = result["BankAccountName"].ToString();
                    }
                    if (result["BankName"] != DBNull.Value)
                    {
                        myAuthorityDetails.BankName = result["BankName"].ToString();
                    }
                    if (result["BankAccountNo"] != DBNull.Value)
                    {
                        myAuthorityDetails.BankAccountNo = result["BankAccountNo"].ToString();
                    }
                    if (result["BankBranchCode"] != DBNull.Value)
                    {
                        myAuthorityDetails.BankBranchCode = result["BankBranchCode"].ToString();
                    }
                    //2013-10-30 added by Nancy end
                }

                result.Close();
                result.Dispose();
            }
            finally
            {
                myConnection.Close();
                myConnection.Dispose();
            }

            return myAuthorityDetails;
        }

        public AuthorityDetails GetLogo(int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthorityDetail", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            AuthorityDetails myAuthorityDetails = new AuthorityDetails();

            if (result.Read())
            {
                myAuthorityDetails.AutLogo = string.IsNullOrEmpty(result["AutLogo"].ToString()) ? new byte[4] : (byte[])result["AutLogo"];
            }

            result.Close();
            return myAuthorityDetails;
        }
        /// <summary>
        /// Updates the authority details.
        /// The AddAuthority method inserts a new user record
        /// into the Authority database.  A unique "AutIntNo"
        /// key is then returned from the method.  
        /// </summary>
        /// <param name="authority">The authority.</param>
        /// <returns>The <c>AutIntNo</c></returns>
        public int UpdateAuthority(AuthorityDetails authority)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("AuthoritySave", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = authority.AutIntNo;
            com.Parameters.Add("@MtrIntNo", SqlDbType.Int, 4).Value = authority.MtrIntNo;
            com.Parameters.Add("@AutCode", SqlDbType.Char, 3).Value = authority.AutCode;
            com.Parameters.Add("@AutNo", SqlDbType.Char, 6).Value = authority.AutNo;
            com.Parameters.Add("@AutName", SqlDbType.VarChar, 100).Value = authority.AutName;
            com.Parameters.Add("@AutPostAddr1", SqlDbType.VarChar, 100).Value = authority.AutPostAddr1;
            com.Parameters.Add("@AutPostAddr2", SqlDbType.VarChar, 100).Value = authority.AutPostAddr2;
            com.Parameters.Add("@AutPostAddr3", SqlDbType.VarChar, 100).Value = authority.AutPostAddr3;
            com.Parameters.Add("@AutPostCode", SqlDbType.VarChar, 5).Value = authority.AutPostCode;
            com.Parameters.Add("@AutTel", SqlDbType.VarChar, 30).Value = authority.AutTel;
            com.Parameters.Add("@AutFax", SqlDbType.VarChar, 30).Value = authority.AutFax;
            com.Parameters.Add("@AutEmail", SqlDbType.VarChar, 50).Value = authority.AutEmail;
            com.Parameters.Add(new SqlParameter("@AutNatisCode", authority.AutNatisCode));  //Oscar 20120202 add
            com.Parameters.Add("@BranchCode", SqlDbType.Char, 2).Value = authority.BranchCode;
            com.Parameters.Add("@BranchName", SqlDbType.VarChar, 30).Value = authority.BranchName;
            com.Parameters.Add("@AutTicketProcessor", SqlDbType.VarChar, 10).Value = authority.AutTicketProcessor;
            com.Parameters.Add("@AutPCGateway", SqlDbType.VarChar, 20).Value = authority.AutPCGateway;
            com.Parameters.Add(new SqlParameter("@AutSendNatisEmail", authority.AutSendNatisEmail));    //Oscar 20120202 add
            com.Parameters.Add("@Aut3PCodingSystem", SqlDbType.Char, 1).Value = authority.Aut3PCodingSystem;
            com.Parameters.Add("@AutPhysAddr1", SqlDbType.VarChar, 100).Value = authority.AutPhysAddr1;
            com.Parameters.Add("@AutPhysAddr2", SqlDbType.VarChar, 100).Value = authority.AutPhysAddr2;
            com.Parameters.Add("@AutPhysAddr3", SqlDbType.VarChar, 100).Value = authority.AutPhysAddr3;
            com.Parameters.Add("@AutPhysAddr4", SqlDbType.VarChar, 100).Value = authority.AutPhysAddr4;
            com.Parameters.Add("@AutPhysCode", SqlDbType.VarChar, 5).Value = authority.AutPhysCode;
            com.Parameters.Add("@AutNoticePaymentInfo", SqlDbType.Text).Value = authority.AutNoticePaymentInfo;
            com.Parameters.Add("@AutNoticeIssuedByInfo", SqlDbType.Text).Value = authority.AutNoticeIssuedByInfo;
            if (authority.EasyPayReceiptID > 0)
                com.Parameters.Add("@EPReceiptID", SqlDbType.Int, 4).Value = authority.EasyPayReceiptID;
            else
                com.Parameters.Add("@EPReceiptID", SqlDbType.Int, 4).Value = DBNull.Value;
            com.Parameters.Add("@EPAccountNoLength", SqlDbType.Int, 4).Value = authority.EasyPayNumberLength;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = authority.LastUser;
            com.Parameters.Add("@JudgementPeriod", SqlDbType.Int).Value = authority.JudgementPeriod;
            com.Parameters.Add("@GracePeriod", SqlDbType.Int).Value = authority.GracePeriod;
            com.Parameters.Add("@GracePeriodEmail", SqlDbType.VarChar, 50).Value = authority.GracePeriodRptEmail;
            com.Parameters.Add("@AGEmail", SqlDbType.VarChar, 50).Value = authority.AutAGListEmail;
            com.Parameters.Add("@SFEmail", SqlDbType.VarChar, 50).Value = authority.AutSpotFineEmail;
            //add by richard 2011-06-07
            //com.Parameters.Add("@AutDocumentFrom", SqlDbType.VarChar, 50).Value = authority.AutDocumentFrom;
            // jerry 2012-02-16 change parameter's length
            com.Parameters.Add("@AutDepartName", SqlDbType.VarChar, 100).Value = authority.AutDepartName;
            com.Parameters.Add("@AutFooterText1", SqlDbType.VarChar, 255).Value = authority.AutFooterText1;
            com.Parameters.Add("@AutFooterText2", SqlDbType.VarChar, 255).Value = authority.AutFooterText2;
            com.Parameters.Add("@AutOfficialName", SqlDbType.VarChar, 100).Value = authority.AutOfficialName;
            com.Parameters.Add("@AutDocumentFromAfri", SqlDbType.VarChar, 255).Value = authority.AutDocumentFromAfri;
            com.Parameters.Add("@AutDocumentFromEnglish", SqlDbType.VarChar, 255).Value = authority.AutDocumentFromEnglish;
            com.Parameters.Add("@AutLogo", SqlDbType.Image).Value = authority.AutLogo;
            //2011-9-6 jerry add
            com.Parameters.Add("@AutNameAfri", SqlDbType.VarChar, 100).Value = authority.AutNameAfri;
            //2013-10-30 added by Nancy start
            com.Parameters.Add("@BankAccountName", SqlDbType.VarChar, 100).Value = authority.BankAccountName;
            com.Parameters.Add("@BankName", SqlDbType.VarChar, 100).Value = authority.BankName;
            com.Parameters.Add("@BankAccountNo", SqlDbType.VarChar, 100).Value = authority.BankAccountNo;
            com.Parameters.Add("@BankBranchCode", SqlDbType.VarChar, 100).Value = authority.BankBranchCode;
            //2013-10-30 added by Nancy end
            try
            {
                con.Open();
                return Convert.ToInt32(com.ExecuteScalar());
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        [Obsolete("Use the Overload that takes and AuthorityDetails object instead", true)]
        public int AddAuthority
         (int mtrIntNo, string autCode, string autNo, string autName,
         string autPostAddr1, string autPostAddr2, string autPostAddr3,
         string autPostCode, string autTel, string autFax, string autEmail,
         string branchCode, string branchName, string autTicketProcessor, string autPCGateway,
         string aut3PCodingSystem,
         string autPhysAddr1, string autPhysAddr2, string autPhysAddr3, string autPhysAddr4,
         string autPhysCode, string autNoticePaymentInfo, string autNoticeIssuedByInfo, string lastUser,
         string epReceiptID)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("AuthorityAdd", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@MtrIntNo", SqlDbType.Int, 4).Value = mtrIntNo;
            com.Parameters.Add("@AutCode", SqlDbType.Char, 3).Value = autCode;
            com.Parameters.Add("@AutNo", SqlDbType.Char, 6).Value = autNo;
            com.Parameters.Add("@AutName", SqlDbType.VarChar, 100).Value = autName;
            com.Parameters.Add("@AutPostAddr1", SqlDbType.VarChar, 100).Value = autPostAddr1;
            com.Parameters.Add("@AutPostAddr2", SqlDbType.VarChar, 100).Value = autPostAddr2;
            com.Parameters.Add("@AutPostAddr3", SqlDbType.VarChar, 100).Value = autPostAddr3;
            com.Parameters.Add("@AutPostCode", SqlDbType.VarChar, 5).Value = autPostCode;
            com.Parameters.Add("@AutTel", SqlDbType.VarChar, 30).Value = autTel;
            com.Parameters.Add("@AutFax", SqlDbType.VarChar, 30).Value = autFax;
            com.Parameters.Add("@AutEmail", SqlDbType.VarChar, 50).Value = autEmail;
            com.Parameters.Add("@BranchCode", SqlDbType.Char, 2).Value = branchCode;
            com.Parameters.Add("BranchName", SqlDbType.VarChar, 30).Value = branchName;
            com.Parameters.Add("@AutTicketProcessor", SqlDbType.VarChar, 10).Value = autTicketProcessor;
            com.Parameters.Add("@AutPCGateway", SqlDbType.VarChar, 20).Value = autPCGateway;
            //com.Parameters.Add("@AutNaTISCode", SqlDbType.VarChar, 4).Value = autNaTISCode;
            //com.Parameters.Add("@AutNatisFolder", SqlDbType.VarChar, 255).Value = autNatisFolder;
            //com.Parameters.Add("@AutSendNatisEmail", SqlDbType.VarChar, 50).Value = autSendNatisEmail;
            com.Parameters.Add("@Aut3PCodingSystem", SqlDbType.Char, 1).Value = aut3PCodingSystem;
            com.Parameters.Add("@AutPhysAddr1", SqlDbType.VarChar, 100).Value = autPhysAddr1;
            com.Parameters.Add("@AutPhysAddr2", SqlDbType.VarChar, 100).Value = autPhysAddr2;
            com.Parameters.Add("@AutPhysAddr3", SqlDbType.VarChar, 100).Value = autPhysAddr3;
            com.Parameters.Add("@AutPhysAddr4", SqlDbType.VarChar, 100).Value = autPhysAddr4;
            com.Parameters.Add("@AutPhysCode", SqlDbType.VarChar, 5).Value = autPhysCode;
            com.Parameters.Add("@AutNoticePaymentInfo", SqlDbType.Text).Value = autNoticePaymentInfo;
            com.Parameters.Add("@AutNoticeIssuedByInfo", SqlDbType.Text).Value = autNoticeIssuedByInfo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            if (epReceiptID.Trim().Length > 0)
            {
                int id;
                if (int.TryParse(epReceiptID.Trim(), out id))
                    com.Parameters.Add("@EPReceiptID", SqlDbType.Int, 4).Value = id;
            }

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Direction = ParameterDirection.Output;
            com.Parameters.Add(parameterAutIntNo);

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                // Calculate the CustomerID using Output Param from SPROC
                int addAutIntNo = Convert.ToInt32(parameterAutIntNo.Value);

                return addAutIntNo;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        public SqlDataReader GetAuthorityList(int mtrIntNo, string orderBy)
        {
            return GetAuthorityList(string.Empty, mtrIntNo, orderBy);
        }

        public SqlDataReader GetAuthorityList(string processor, int mtrIntNo, string orderBy)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthorityList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterProcessor = new SqlParameter("@Processor", SqlDbType.VarChar, 10);
            parameterProcessor.Value = processor;
            myCommand.Parameters.Add(parameterProcessor);


            SqlParameter parameterMTRIntNo = new SqlParameter("@MTRIntNo", SqlDbType.Int, 4);
            parameterMTRIntNo.Value = mtrIntNo;
            myCommand.Parameters.Add(parameterMTRIntNo);

            //			SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
            //			parameterSearch.Value = search;
            //			myCommand.Parameters.Add(parameterSearch);

            SqlParameter parameterOrderBy = new SqlParameter("@OrderBy", SqlDbType.VarChar, 10);
            parameterOrderBy.Value = orderBy;
            myCommand.Parameters.Add(parameterOrderBy);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        public SqlDataReader GetAuthorityDetailsByAutCode(string autCode, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthorityDetailByAutCode", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutCode = new SqlParameter("@AutCode", SqlDbType.Char, 3);
            parameterAutCode.Value = autCode;
            myCommand.Parameters.Add(parameterAutCode);

            try
            {
                // Execute the command
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                // Return the datareader result
                return result;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return null;
            }
        }

        public SqlDataReader GetAuthorityDetailsByAutNo(string autNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthorityDetailByAutNo", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutAutNo = new SqlParameter("@AutNo", SqlDbType.Char, 6);
            parameterAutAutNo.Value = autNo;
            myCommand.Parameters.Add(parameterAutAutNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        public DataSet GetAuthorityTicketProcessorList()
        {
            SqlDataAdapter sqlDAAuthority = new SqlDataAdapter();
            DataSet dsAuthority = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAAuthority.SelectCommand = new SqlCommand();
            sqlDAAuthority.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAAuthority.SelectCommand.CommandText = "AuthorityTicketProcessorGetList";

            // Mark the Command as a SPROC
            sqlDAAuthority.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command and close the connection
            sqlDAAuthority.Fill(dsAuthority);
            sqlDAAuthority.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsAuthority;
        }

        public SqlDataReader GetAuthorityDetailsByAutIntNo(int autNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthorityDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutAutNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutAutNo.Value = autNo;
            myCommand.Parameters.Add(parameterAutAutNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        // 2013-07-19 comment by Henry for useless
        /// <summary>
        /// LF: (27/06/2008) Gets a list of Authority Int (pk) of valid Authorities from the Authority's table
        /// This list of AutIntNo's is then use to filter the report by authority
        /// The primary purpose of this list is to make sure that there are no
        /// blank reports generated downstream.  This is accomplished within the while
        /// loop that looks at records from the AGList table with it's relative authorities.
        /// The while loop then runs against this list.
        /// </summary>
        /// <param name="pAGorSpot">1 - Valid AutIntNo's for Acknowledgement of Guilt
        /// 2 - Valid AutIntNo's for Spot fines.</param>
        /// <returns></returns>
        //public SqlDataReader GetAllAutIntNosFromAuthorityTable(int pAGorSpot)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("GetAllAutIntNosFromAuthority", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    SqlParameter parameterAGorSpot = new SqlParameter("@AGorSpot", SqlDbType.Int);
        //    parameterAGorSpot.Value = pAGorSpot;
        //    myCommand.Parameters.Add(parameterAGorSpot);

        //    // Execute the command
        //    myConnection.Open( );
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the datareader result
        //    return result;
        //}

        public DataSet GetAuthorityListDS( int mtrIntNo, string orderBy)
        {
            return GetAuthorityListDS( string.Empty, mtrIntNo, orderBy);
        }

        public DataSet GetAuthorityListDS(string processor,int mtrIntNo, string orderBy)
        {
            SqlDataAdapter sqlDAAuthority = new SqlDataAdapter();
            DataSet dsAuthority = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAAuthority.SelectCommand = new SqlCommand();
            sqlDAAuthority.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAAuthority.SelectCommand.CommandText = "AuthorityList";

            // Mark the Command as a SPROC
            sqlDAAuthority.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC

            SqlParameter parameterProcessor = new SqlParameter("@Processor", SqlDbType.VarChar, 10);
            parameterProcessor.Value = processor;
            sqlDAAuthority.SelectCommand.Parameters.Add(parameterProcessor);


            SqlParameter parameterMTRIntNo = new SqlParameter("@MTRIntNo", SqlDbType.Int, 4);
            parameterMTRIntNo.Value = mtrIntNo;
            sqlDAAuthority.SelectCommand.Parameters.Add(parameterMTRIntNo);

            //			SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
            //			parameterSearch.Value = search;
            //			sqlDAAuthority.SelectCommand.Parameters.Add(parameterSearch);

            SqlParameter parameterOrderBy = new SqlParameter("@OrderBy", SqlDbType.VarChar, 10);
            parameterOrderBy.Value = orderBy;
            sqlDAAuthority.SelectCommand.Parameters.Add(parameterOrderBy);

            // Execute the command and close the connection
            sqlDAAuthority.Fill(dsAuthority);
            sqlDAAuthority.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsAuthority;
        }

        //public int UpdateAuthority
        //    //password only updated via change password
        //    (int autIntNo, int mtrIntNo, string autCode, string autNo, string autName,
        //    string autPostAddr1, string autPostAddr2, string autPostAddr3,
        //    string autPostCode, string autTel, string autFax, string autEmail,
        //    string branchCode, string branchName, string autTicketProcessor, string autPCGateway,
        //    string autNaTISCode, string autSendNatisEmail, string autNatisFolder, string aut3PCodingSystem,
        //    string autPhysAddr1, string autPhysAddr2, string autPhysAddr3, string autPhysAddr4,
        //    string autPhysCode, string autNoticePaymentInfo, string autNoticeIssuedByInfo, string lastUser,
        //    string easyPayReceiptID)
        public int UpdateAuthority
            //password only updated via change password
         (int autIntNo, int mtrIntNo, string autCode, string autNo, string autName,
         string autPostAddr1, string autPostAddr2, string autPostAddr3,
         string autPostCode, string autTel, string autFax, string autEmail,
         string branchCode, string branchName, string autTicketProcessor, string autPCGateway,
         string aut3PCodingSystem,
         string autPhysAddr1, string autPhysAddr2, string autPhysAddr3, string autPhysAddr4,
         string autPhysCode, string autNoticePaymentInfo, string autNoticeIssuedByInfo, string lastUser,
         string easyPayReceiptID)
        {

            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("AuthorityUpdate", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@MtrIntNo", SqlDbType.Int, 4).Value = mtrIntNo;
            com.Parameters.Add("@AutCode", SqlDbType.Char, 3).Value = autCode;
            com.Parameters.Add("@AutNo", SqlDbType.Char, 6).Value = autNo;
            com.Parameters.Add("@AutName", SqlDbType.VarChar, 100).Value = autName;
            com.Parameters.Add("@AutPostAddr1", SqlDbType.VarChar, 100).Value = autPostAddr1;
            com.Parameters.Add("@AutPostAddr2", SqlDbType.VarChar, 100).Value = autPostAddr2;
            com.Parameters.Add("@AutPostAddr3", SqlDbType.VarChar, 100).Value = autPostAddr3;
            com.Parameters.Add("@AutPostCode", SqlDbType.VarChar, 5).Value = autPostCode;
            com.Parameters.Add("@AutTel", SqlDbType.VarChar, 30).Value = autTel;
            com.Parameters.Add("@Autfax", SqlDbType.VarChar, 30).Value = autFax;
            com.Parameters.Add("@AutEmail", SqlDbType.VarChar, 50).Value = autEmail;
            com.Parameters.Add("@BranchCode", SqlDbType.Char, 2).Value = branchCode;
            com.Parameters.Add("BranchName", SqlDbType.VarChar, 30).Value = branchName;
            com.Parameters.Add("@AutTicketProcessor", SqlDbType.VarChar, 10).Value = autTicketProcessor;
            com.Parameters.Add("@AutPCGateway", SqlDbType.VarChar, 20).Value = autPCGateway;
            //com.Parameters.Add("@AutNaTISCode", SqlDbType.VarChar, 4).Value = autNaTISCode;
            //com.Parameters.Add("@AutNatisFolder", SqlDbType.VarChar, 255).Value = autNatisFolder;
            //com.Parameters.Add("@AutSendNatisEmail", SqlDbType.VarChar, 50).Value = autSendNatisEmail;
            com.Parameters.Add("@Aut3PCodingSystem", SqlDbType.Char, 1).Value = aut3PCodingSystem;
            com.Parameters.Add("@AutPhysAddr1", SqlDbType.VarChar, 100).Value = autPhysAddr1;
            com.Parameters.Add("@AutPhysAddr2", SqlDbType.VarChar, 100).Value = autPhysAddr2;
            com.Parameters.Add("@AutPhysAddr3", SqlDbType.VarChar, 100).Value = autPhysAddr3;
            com.Parameters.Add("@AutPhysAddr4", SqlDbType.VarChar, 100).Value = autPhysAddr4;
            com.Parameters.Add("@AutPhysCode", SqlDbType.VarChar, 5).Value = autPhysCode;
            com.Parameters.Add("@AutNoticePaymentInfo", SqlDbType.Text).Value = autNoticePaymentInfo;
            com.Parameters.Add("@AutNoticeIssuedByInfo", SqlDbType.Text).Value = autNoticeIssuedByInfo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            if (easyPayReceiptID.Trim().Length > 0)
            {
                int id;
                if (int.TryParse(easyPayReceiptID, out id))
                    com.Parameters.Add("@EPReceiptID", SqlDbType.Int, 4).Value = id;
            }
            else
                com.Parameters.Add("@EPReceiptID", SqlDbType.Int, 4).Value = null;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Direction = ParameterDirection.InputOutput;
            parameterAutIntNo.Value = autIntNo;
            com.Parameters.Add(parameterAutIntNo);

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                // Calculate the CustomerID using Output Param from SPROC
                int updAutIntNo = (int)com.Parameters["@AutIntNo"].Value;

                return updAutIntNo;
            }
            catch (Exception e)
            {
                com.Dispose();
                string msg = e.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        public int DeleteAuthority(int autIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthorityDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            parameterAutIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterAutIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int delAutIntNo = (int)parameterAutIntNo.Value;

                return delAutIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        //BD 20080527 need to handle the new stored proc [SummonsLastCreateUpdate] for updating summons
        //last paid date in authorities
        // 2013-07-23 comment by Henry for useless
        //public int SummonsLastCreateUpdate(int autIntNo, DateTime SummonsLastCreateDate)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("SummonsLastCreateUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    parameterAutIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterAutIntNo);
        //    myCommand.Parameters.Add("@SummonsLastCreateDate", SqlDbType.SmallDateTime).Value = SummonsLastCreateDate;

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int updAutIntNo = (int)parameterAutIntNo.Value;

        //        return updAutIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        public int CheckMaximumStatusRule(int autIntNo, string lastUser)
        {
            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = autIntNo;
            rule.ARCode = "4570";
            rule.LastUser = lastUser;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.mConstr);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            return value.Key; //rule.ARNumeric;
        }

        /// <summary>
        /// Verifies whether the authority is registered for EasyPay submissions.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <returns>An <see cref="EasyPayAuthority"/></returns>
        public EasyPayAuthority VerifyAuthorityForEasyPay(int autIntNo, int noOfDays)
        {
            //EasyPayAuthority authority = null;     // = new EasyPayAuthority();
            EasyPayAuthority authority = new EasyPayAuthority();

            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("EasyPayVerifyAuthority", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();

                if (reader.Read())
                {
                    authority = new EasyPayAuthority(
                        (int)reader["AutIntNo"],
                        (int)reader["EasyPayReceiptID"],
                        (string)reader["AutName"],
                        noOfDays);
                }
                reader.Close();
            }
            finally
            {
                con.Close();
            }

            return authority;
        }

        /// <summary>
        /// Gets the list of autorities that have data ready for easy pay export.
        /// </summary>
        /// <returns></returns>
        public List<EasyPayAuthority> GetAutoritiesForEasyPayExport()
        {
            List<EasyPayAuthority> authorities = new List<EasyPayAuthority>();

            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("EasyPayAutoritiesNeedingExport", con);
            com.CommandType = CommandType.StoredProcedure;

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    DateRulesDetails dr = new DateRulesDetails();
                    dr.AutIntNo = (int)reader["AutIntNo"];
                    dr.DtRStartDate = "EasyPayDate";
                    dr.DtREndDate = "NotPaymentDate";

                    DefaultDateRules rule = new DefaultDateRules(dr, this.mConstr);
                    int noDaysForExpiry = rule.SetDefaultDateRule();

                    EasyPayAuthority authority = this.VerifyAuthorityForEasyPay((int)reader["AutIntNo"], noDaysForExpiry);
                    if (authority.IsValid)
                        authorities.Add(authority);
                }
                reader.Close();

            }
            finally
            {
                con.Close();
            }


            return authorities;
        }

    }
}

