using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


namespace Stalberg.TMS
{

	//*******************************************************
	//
	// UserGroup_AuthDetails Class
	//
	// A simple data class that encapsulates details about a particular user 
	//
	//*******************************************************

	public partial class UserGroup_AuthDetails
	{
		public Int32 UGIntNo;
		public Int32 AutIntNo;
		public string UGAMetro;
	}

	//*******************************************************
	//
	// UserGroup_AuthDB Class
	//
	// Business/Data Logic Class that encapsulates all data
	// logic necessary to add/login/query UserGroup_Auths within
	// the Commerce Starter Kit Customer database.
	//
	//*******************************************************

	public partial class UserGroup_AuthDB
	{

		string mConstr = "";

		public UserGroup_AuthDB(string vConstr)
		{
			mConstr = vConstr;
		}

		//*******************************************************
		//
		// UserGroup_AuthDB.GetUserGroup_AuthDetails() Method <a name="GetUserGroup_AuthDetails"></a>
		//
		// The GetUserGroup_AuthDetails method returns a UserGroup_AuthDetails
		// struct that contains information about a specific
		// customer (name, password, etc).
		//
		//*******************************************************

		public UserGroup_AuthDetails GetUserGroup_AuthDetails(int ugaIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("UserGroup_AuthDetail", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterUGAIntNo = new SqlParameter("@UGAIntNo", SqlDbType.Int, 4);
			parameterUGAIntNo.Value = ugaIntNo;
			myCommand.Parameters.Add(parameterUGAIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Create CustomerDetails Struct
			UserGroup_AuthDetails myUserGroup_AuthDetails = new UserGroup_AuthDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myUserGroup_AuthDetails.UGIntNo = Convert.ToInt32(result["UGIntNo"]);
				myUserGroup_AuthDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
				myUserGroup_AuthDetails.UGAMetro = result["UGAMetro"].ToString();
			}

			result.Close();
			return myUserGroup_AuthDetails;
		}

		//*******************************************************
		//
		// UserGroup_AuthsDB.AddUser() Method <a name="AddUser"></a>
		//
		// The AddUser method inserts a new user record
		// into the userGroups database.  A unique "UserId"
		// key is then returned from the method.  
		//
		//*******************************************************

		public int AddUserGroup_Auth
			(int ugIntNo, int autIntNo, string ugaMetro)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("UserGroup_AuthAdd", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterUGIntNo = new SqlParameter("@UGIntNo", SqlDbType.Int, 4);
			parameterUGIntNo.Value = ugIntNo;
			myCommand.Parameters.Add(parameterUGIntNo);

			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			myCommand.Parameters.Add(parameterAutIntNo);

			SqlParameter parameterUGAMetro = new SqlParameter("@UGAMetro", SqlDbType.Char, 1);
			parameterUGAMetro.Value = ugaMetro;
			myCommand.Parameters.Add(parameterUGAMetro);

			SqlParameter parameterUGAIntNo = new SqlParameter("@UGAIntNo", SqlDbType.Int, 4);
			parameterUGAIntNo.Direction = ParameterDirection.Output;
			myCommand.Parameters.Add(parameterUGAIntNo);

			try
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int addUGAIntNo = Convert.ToInt32(parameterUGAIntNo.Value);

				return addUGAIntNo;
			}
			catch
			{
				myConnection.Dispose();
				return 0;
			}
		}

		public SqlDataReader GetUserGroup_AuthListByUserGroup(int ugIntNo, int autIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection con = new SqlConnection(mConstr);
			SqlCommand com = new SqlCommand("UserGroup_AuthList", con);
			com.CommandType = CommandType.StoredProcedure;

			// Add Parameters to the SP
			com.Parameters.Add("@UGIntNo", SqlDbType.Int, 4).Value = ugIntNo;
			com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

			// Execute the command
			con.Open();
			return com.ExecuteReader(CommandBehavior.CloseConnection);
		}

		public SqlDataReader GetUserGroup_MetroListByUserGroup(int ugIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("UserGroup_MetroList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterUGIntNo = new SqlParameter("@UGIntNo", SqlDbType.Int, 4);
			parameterUGIntNo.Value = ugIntNo;
			myCommand.Parameters.Add(parameterUGIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		public DataSet GetUserGroup_AuthListDS(int ugIntNo, int autIntNo)
		{
			SqlDataAdapter sqlDAUserGroup_Auths = new SqlDataAdapter();
			DataSet dsUserGroup_Auths = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAUserGroup_Auths.SelectCommand = new SqlCommand();
			sqlDAUserGroup_Auths.SelectCommand.Connection = new SqlConnection(mConstr);
			sqlDAUserGroup_Auths.SelectCommand.CommandText = "UserGroup_AuthList";

			// Mark the Command as a SPROC
			sqlDAUserGroup_Auths.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterUGIntNo = new SqlParameter("@UGIntNo", SqlDbType.Int, 4);
			parameterUGIntNo.Value = ugIntNo;
			sqlDAUserGroup_Auths.SelectCommand.Parameters.Add(parameterUGIntNo);

			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			sqlDAUserGroup_Auths.SelectCommand.Parameters.Add(parameterAutIntNo);

			// Execute the command and close the connection
			sqlDAUserGroup_Auths.Fill(dsUserGroup_Auths);
			sqlDAUserGroup_Auths.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsUserGroup_Auths;
		}

		public int UpdateUserGroup_Auth

			//password only updated via change password
			(int ugaIntNo, int ugIntNo, string autIntNo, string ugaMetro)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("UserGroup_AuthUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterUGIntNo = new SqlParameter("@UGIntNo", SqlDbType.Int, 4);
			parameterUGIntNo.Value = ugIntNo;
			myCommand.Parameters.Add(parameterUGIntNo);

			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			myCommand.Parameters.Add(parameterAutIntNo);

			SqlParameter parameterUGAMetro = new SqlParameter("@UGAMetro", SqlDbType.Char, 1);
			parameterUGAMetro.Value = ugaMetro;
			myCommand.Parameters.Add(parameterUGAMetro);

			SqlParameter parameterUGAIntNo = new SqlParameter("@UGAIntNo", SqlDbType.Int, 4);
			parameterUGAIntNo.Direction = ParameterDirection.InputOutput;
			parameterUGAIntNo.Value = ugaIntNo;
			myCommand.Parameters.Add(parameterUGAIntNo);

			try
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int updUGAIntNo = (int)myCommand.Parameters["@UGAIntNo"].Value;

				return updUGAIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		public String DeleteUserGroup_Auth(int ugaIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("UserGroup_AuthDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterUGAIntNo = new SqlParameter("@UGAIntNo", SqlDbType.Int, 4);
			parameterUGAIntNo.Value = ugaIntNo;
			parameterUGAIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterUGAIntNo);

			try
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int delUGAIntNo = (int)parameterUGAIntNo.Value;

				return delUGAIntNo.ToString();
			}
			catch
			{
				myConnection.Dispose();
				return String.Empty;
			}
		}
	}
}

