using System;
using System.Collections.Generic;
using System.Text;
using Stalberg.TMS_TPExInt.Objects;
using System.IO;
using Stalberg.TMS;
using System.Collections;

namespace Stalberg.ThaboAggregator.Objects
{
    /// <summary>
    /// Represents an EasyPay Traffic Data File
    /// </summary>
    public class TrafficDataFile
    {
        // Fields
        private EasyPayAuthority authority;
        private string fileName = string.Empty;
        private decimal totalInserted;
        private int countInserted;
        private decimal totalUpdated;
        private int countUpdated;
        private int countDeleted;
        private bool hasErrors;
        private List<HoldbackFileError> errors;

        // Constants
        private const string CURRENCY_FORMAT = "0";

        /// <summary>
        /// The format for dates in the Traffic Data Files name
        /// </summary>
        public static readonly string DATE_FORMAT = "yyyyMMdd";
        /// <summary>
        /// The file extension to use for Traffic Data Files
        /// </summary>
        public static readonly string EXTENSION = ".txt";

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TrafficDataFile"/> class.
        /// </summary>
        /// <param name="authority">The authority.</param>
        public TrafficDataFile(EasyPayAuthority authority)
        {
            this.authority = authority;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TrafficDataFile"/> class.
        /// </summary>
        /// <param name="holdBackFileName">The name of the hold back file.</param>
        public TrafficDataFile(string holdBackFileName)
        {
            this.fileName = holdBackFileName;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the EasyPay authority.
        /// </summary>
        /// <value>The authority.</value>
        public EasyPayAuthority Authority
        {
            get { return this.authority; }
        }

        /// <summary>
        /// Gets the name of the file.
        /// </summary>
        /// <value>The name of the file.</value>
        public string FileName
        {
            get { return this.fileName; }
        }

        /// <summary>
        /// Gets the list of HoldBack errors.
        /// </summary>
        /// <value>The errors.</value>
        public List<HoldbackFileError> Errors
        {
            get { return this.errors; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has errors.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has errors; otherwise, <c>false</c>.
        /// </value>
        public bool HasErrors
        {
            get { return this.hasErrors; }
        }

        #endregion

        #region Create Methods

        /// <summary>
        /// Cleans the data, removing duplicates and redundant entries.
        /// </summary>
        public void CleanExportData()
        {
            int sortStartPos = 0;
            int sortEndPos = 0;
            List<EasyPayTransaction> list = this.authority.EasyPayTransactions;
            int count = list.Count;

            // Bubble sort the section of the list
            //for (int x = 0; x < count; x++)
            //{
            //    for (int i = 0; i < count - (x + 1); i++)
            //    {
            //        if (list[i].CompareTo(list[i + 1]) > 0)
            //        {
            //            EasyPayTransaction temp = list[i];
            //            list[i] = list[i + 1];
            //            list[i + 1] = temp;
            //        }
            //    }
            //}

            for (int i = 0; i < count; i++)
            {
                EasyPayTransaction tran = list[sortEndPos];

                if (sortEndPos < list.Count - 1 && !tran.Equals(list[sortEndPos + 1]))
                {
                    // Sort the list
                    sortEndPos = this.Sort(sortStartPos, sortEndPos, list);
                    sortStartPos = sortEndPos + 1;
                }

                sortEndPos++;
            }

            this.Sort(sortStartPos, list.Count - 1, list);
        }

        private int Sort(int startPos, int endPos, List<EasyPayTransaction> list)
        {
            if (endPos < startPos)
                return 0;

            // Check for a Delete when there's an insert and discard all for this EasyPay number.
            if (list[startPos].EasyPayAction == 'I' && list[endPos].EasyPayAction == 'D')
            {
                for (int x = startPos; x < endPos; x++)
                    list.Remove(list[startPos]);

                return startPos;
            }
            
            if (list[startPos].EasyPayAction == 'U' && list[endPos].EasyPayAction == 'D')
            {
                // Check for an update when there's a delete and discard all but the delete.
                for (int x = startPos; x < endPos - 1; x++)
                    list.Remove(list[startPos]);

                return startPos + 1;
            }

            return endPos;
        }

        /// <summary>
        /// Creates the Traffic Data File for the authority.
        /// </summary>
        /// <param name="transferFileName">Name of the file.</param>
        public void CreateFile(string transferFileName)
        {
            FileStream fs = null;
            StreamWriter writer = null;
            this.fileName = transferFileName;

            // FBJ (2009-05-26): Added this check to prevent overwriting an existing file that has content
            FileInfo fi = new FileInfo(transferFileName);
            //Jerry 20120309: We have found a problem in FreeState that 3 different authorties are using the same EasyPay receiver ID which resulted in the same file name.The current code hereby will throw out an exception which will be captuered in the parent method which will invoke rollback and delete the file.
            //if (fi.Exists && fi.Length > 0)
                //throw new ApplicationException("The file: " + transferFileName + " already exists and has data in it, it cannot be overwritten.");

            try
            {
                fs = fi.Open(FileMode.OpenOrCreate);
                writer = new StreamWriter(fs, Encoding.UTF8);

                this.WriteHeader(writer);

                foreach (EasyPayTransaction tran in this.authority.EasyPayTransactions)
                    this.WriteTransaction(writer, tran);

                this.WriteFooter(writer);
            }
            finally
            {
                writer.Close();
                fs.Close();
                fs.Dispose();
            }
        }

        /// <summary>
        /// Jerry 2012-04-09 add
        /// append data to file
        /// </summary>
        /// <param name="transferFileName"></param>
        public void AppendDataToFile(string transferFileName)
        {
            FileStream fs = null;
            StreamWriter writer = null;
            this.fileName = transferFileName;

            FileInfo fi = new FileInfo(transferFileName);

            try
            {
                fs = fi.Open(FileMode.Append, FileAccess.Write);
                writer = new StreamWriter(fs, Encoding.UTF8);

                writer.WriteLine();

                foreach (EasyPayTransaction tran in this.authority.EasyPayTransactions)
                    this.WriteTransaction(writer, tran);

                this.WriteFooter(writer);
            }
            finally
            {
                writer.Close();
                fs.Close();
                fs.Dispose();
            }
        }

        /// <summary>
        /// Jerry 2012-04-10 add
        /// handle generated file to set 1 header and 1 end
        /// </summary>
        /// <param name="transferFileName"></param>
        public void HandleTrafficDataFile(string transferFileName)
        {
            StreamReader objReader = new StreamReader(transferFileName,Encoding.UTF8);
            string strLine = "";
            ArrayList strLineList = new ArrayList();
            string strFinalData = string.Empty;
            try
            {
                while (strLine != null)
                {
                    strLine = objReader.ReadLine();
                    if (strLine != null && !strLine.Equals(""))
                    {
                        //get all line data into ArrayList
                        strLineList.Add(strLine);
                        //get all line data except 'END'
                        if (!strLine.Contains("END"))
                        {
                            strFinalData += strLine + "\r\n";
                        }
                    }
                }
            }
            catch (Exception)
            {
                return;
            }
            finally
            {
                objReader.Dispose();
                objReader.Close();
            }

            string strFirstEndLine = string.Empty;
            string strLastEndLine = string.Empty;
            
            if (strLineList.Count > 0)
            {
                //get first 'END' line data
                if (strLineList[strLineList.Count - 3].ToString().Contains("END"))
                {
                    strFirstEndLine = strLineList[strLineList.Count - 3].ToString();
                }

                //get last 'END' line data
                if (strLineList[strLineList.Count - 1].ToString().Contains("END"))
                {
                    strLastEndLine = strLineList[strLineList.Count - 1].ToString();
                }

                //split 'END' line data of first and last by ','
                //get final last line
                if (!string.IsNullOrEmpty(strFirstEndLine) && !string.IsNullOrEmpty(strLastEndLine))
                {
                    strLineList[strLineList.Count - 3] = strLineList[strLineList.Count - 2];
                    string[] strFirstEndLineList = strFirstEndLine.Split(',');
                    string[] strLastEndLineList = strLastEndLine.Split(',');
                    string strLastLine = "END,";

                    for (int i = 1; i < strFirstEndLineList.Length; i++)
                    {
                        strLastLine = strLastLine + (Convert.ToInt32(strFirstEndLineList[i]) + Convert.ToInt32(strLastEndLineList[i]));
                        if(i< strFirstEndLineList.Length -1)
                            strLastLine = strLastLine + ",";
                    }
                    strFinalData += strLastLine;

                    StreamWriter sw = new StreamWriter(transferFileName, false, Encoding.UTF8);
                    sw.WriteLine(strFinalData);
                    sw.Flush();
                    sw.Dispose();
                    sw.Close();
                }
            }
        }

        private void WriteFooter(StreamWriter writer)
        {
            writer.Write("END,");
            writer.Write(countDeleted + countInserted + countUpdated);
            writer.Write(',');
            writer.Write(countInserted);
            writer.Write(',');
            writer.Write(countDeleted);
            writer.Write(',');
            writer.Write(countUpdated);
            writer.Write(',');
            writer.Write(totalInserted.ToString(CURRENCY_FORMAT));
            writer.Write(',');
            writer.Write(totalUpdated.ToString(CURRENCY_FORMAT));
        }

        private void WriteTransaction(StreamWriter writer, EasyPayTransaction tran)
        {
            writer.Write(tran.EasyPayAction);
            writer.Write(',');
            writer.Write(tran.EasyPayNumber);

            switch (tran.EasyPayAction)
            {
                case 'I':
                    this.countInserted++;
                    this.totalInserted += tran.Amount;
                    break;
                case 'U':
                    this.countUpdated++;
                    this.totalUpdated += tran.Amount;
                    break;
                case 'D':
                    this.countDeleted++;
                    writer.WriteLine();
                    return;
                    // FBJ (2010-04-28): This line is complete at this stage as the record is a delete and doesn't need any details
                default:
                    // should we also just continue without causing a bomb out - ie comment the throw exception 
                    // and add an entry 'E,easy pay number, etc ....????
                    throw new ApplicationException(string.Format("Unrecognised EasyPay Action '{0}'.", tran.EasyPayAction));
            }

            writer.Write(',');
            //writer.Write(tran.Amount.ToString(CURRENCY_FORMAT)); they want cents
            writer.Write(tran.Cents.ToString());
            writer.Write(',');
            writer.Write(tran.ExpiryDate.ToString(DATE_FORMAT));
            writer.Write(",,,");
            writer.WriteLine(tran.RegNo);
        }

        private void WriteHeader(StreamWriter writer)
        {
            //writer.Write("TF,Ver01.80,");
            writer.Write("TF,Ver01.81,");
            writer.WriteLine(DateTime.Today.ToString(DATE_FORMAT));
        }

        #endregion

        #region Read Methods

        /// <summary>
        /// Reads the Traffic Data File and processes
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if there were no problems and there is data to process.
        /// </returns>
        public void Read()
        {
            this.hasErrors = false;
            this.errors = new List<HoldbackFileError>();

            FileInfo file = new FileInfo(this.fileName);
            if (!file.Exists)
                throw new ApplicationException(string.Format("The Holdback file ({0}) does not exist!", this.fileName));

            StreamReader reader = new StreamReader(file.Open(FileMode.Open));
            string fileContents = reader.ReadToEnd();
            reader.Close();

            EasyPayTransaction lastTran = null;
            LineType lineType = LineType.None;

            string[] lines = fileContents.Split(new char[] { '\n' });
            int i = 0;
            foreach (string line in lines)
            {
                i++;
                lineType = this.ReadLineType(line.Trim());
                switch (lineType)
                {
                    case LineType.Update:
                        lastTran = this.ReadInsertUpdate(line.Trim());
                        lastTran.EasyPayAction = 'U';
                        break;

                    case LineType.Insert:
                        lastTran = this.ReadInsertUpdate(line.Trim());
                        lastTran.EasyPayAction = 'I';
                        break;

                    case LineType.Delete:
                        lastTran = this.ReadDelete(line.Trim());
                        break;

                    case LineType.FileError:
                    case LineType.RecordError:
                        this.hasErrors = true;
                        this.errors.Add(new HoldbackFileError(line.Trim(), lastTran, i));
                        break;
                }
            }
        }

        private EasyPayTransaction ReadDelete(string line)
        {
            EasyPayTransaction tran = new EasyPayTransaction(this.authority);

            string[] values = line.Split(new char[] { ',' });
            tran.EasyPayAction = 'D';
            tran.EasyPayNumber = values[1];

            return tran;
        }

        private EasyPayTransaction ReadInsertUpdate(string line)
        {
            EasyPayTransaction tran = new EasyPayTransaction(this.authority);

            string[] values = line.Split(new char[] { ',' });
            tran.EasyPayNumber = values[1].Trim();
            tran.Amount = decimal.Parse(values[2].Trim());
            tran.ExpiryDate = this.ParseEasyPayDateTime(values[3].Trim());

            return tran;
        }

        private DateTime ParseEasyPayDateTime(string value)
        {
            int year = int.Parse(value.Substring(0, 4));
            int month = int.Parse(value.Substring(4, 2));
            int day = int.Parse(value.Substring(6, 2));

            return new DateTime(year, month, day);
        }

        private LineType ReadLineType(string line)
        {
            int pos = line.IndexOf(',');
            if (pos > 0)
            {
                string lineTypeRaw = line.Substring(0, pos);
                switch (lineTypeRaw.ToUpper())
                {
                    case "TF":
                        return LineType.Header;

                    case "I":
                        return LineType.Insert;

                    case "U":
                        return LineType.Update;

                    case "D":
                        return LineType.Delete;

                    case "END":
                        return LineType.Trailer;
                }
            }

            if (line.Length > 0 && line[0] == '#')
                return LineType.Comment;

            if (line.Substring(0, 3) == "***")
                return LineType.FileError;

            return LineType.RecordError;
        }

        #endregion
    }

    /// <summary>
    /// Lists the different types of line in a HoldBack file
    /// </summary>
    internal enum LineType
    {
        None = 0,
        Header = 1,
        Insert = 2,
        Update = 3,
        Delete = 4,
        Trailer = 5,
        Comment = 6,
        FileError = 7,
        RecordError = 8
    }

}
