using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the details of a representation
    /// </summary>
    public partial class RepresentationDetails
    {
        //RCIntNo, RepDetails, RepDate, RepOfficial, RepRecommend, RepRecommendDate
        public int RepIntNo;
        public int ChgIntNo;
        public int RCIntNo;
        public string RepDetails;
        public string RepOfficial;
        public string RepRecommend;
        public DateTime RepRecommendDate;
        public DateTime RepDate;
        public string RepOffenderName;
        public string RepOffenderAddress;
        public string RepOffenderTelNo;
        public string LastUser;
    }

    /// <summary>
    /// Contains all the database interaction for Representations
    /// </summary>
    public partial class RepresentationDB
    {
        // Fields
        private string connectionString = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepresentationDB"/> class.
        /// </summary>
        /// <param name="vConstr">The v constr.</param>
        public RepresentationDB(string vConstr)
        {
            this.connectionString = vConstr;
        }

        public Int32 LoggedRepsRunOutOfGrace(int autIntNo, int noOfDaysGracePeriod, bool cancel, string lastUser, ref string errMessage)
        {
            int noOfNotices = 0;

            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("RepresentationsLoggedOutOfGrace", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@NoOfDaysGracePeriod", SqlDbType.Int, 4).Value = noOfDaysGracePeriod;
            com.Parameters.Add("@Cancel", SqlDbType.Bit).Value = cancel;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                con.Open();
                noOfNotices = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (Exception e)
            {
                con.Dispose();
                errMessage = e.Message;
                return -1;
            }
            finally
            {
                con.Dispose();
            }

            return noOfNotices;
        }

        public Int32 ReverseSummonsLoggedRep(Int32 srIntNo, string lastUser, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("SummonsReverseLoggedRep", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            com.Parameters.Add("@SRIntNo", SqlDbType.Int, 4).Value = srIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                con.Open();
                srIntNo = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (Exception e)
            {
                con.Dispose();
                errMessage = e.Message;
                return -6;
            }
            finally
            {
                con.Dispose();
            }

            return srIntNo;
        }

        /// <summary>
        /// Makes the changes to representation when a decision has been made.
        /// </summary>
        /// <param name="repIntNo">The rep int no.</param>
        /// <param name="repCode">The rep code.</param>
        /// <param name="repOfficial">The rep official.</param>
        /// <param name="repRecommend">The rep recommend.</param>
        /// <param name="decisionDate">The decision date.</param>
        /// <param name="lastUser">The last user.</param>
        /// <param name="chgIntNo">The charge int no.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="csCode">The cs code.</param>
        /// <param name="chargeVersion">An integer representing the RowVersion of the charge when it was selected.</param>
        public Int32 DecideRepresentation(int repIntNo, int repCode,int? rwrCode, string repOfficial, string repRecommend, DateTime decisionDate,
            string lastUser, int chgIntNo, decimal amount, int csCode, Int64 chargeVersion, decimal fCurrentAmount,
            string sChangeOfOffender, string repDetails, string letterTo, bool isSummons, string changeRegNo, ref string errMessage, int noOfDaysForPayment)
        {
            bool ignore = false;

            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("RepresentationDecide2", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            com.Parameters.Add("@ChgIntNo", SqlDbType.Int, 4).Value = chgIntNo;
            com.Parameters.Add("@RepCode", SqlDbType.Int, 4).Value = repCode;
            com.Parameters.Add("@WithdrawReasonCode", SqlDbType.Int, 4).Value = rwrCode;
            com.Parameters.Add("@RepIntNo", SqlDbType.Int, 4).Value = repIntNo;
            com.Parameters.Add("@Official", SqlDbType.VarChar, 100).Value = repOfficial;
            com.Parameters.Add("@Recommendation", SqlDbType.Text).Value = repRecommend;
            com.Parameters.Add("@Date", SqlDbType.SmallDateTime).Value = decisionDate;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@FineAmount", SqlDbType.Float).Value = amount;
            com.Parameters.Add("@CSCode", SqlDbType.Int, 4).Value = csCode;
            com.Parameters.Add("@ChargeVersion", SqlDbType.BigInt, 8).Value = chargeVersion;
            com.Parameters.Add("@CurrentFineAmount", SqlDbType.Decimal).Value = fCurrentAmount;
            //com.Parameters.Add("@ChangeOfOffender", SqlDbType.VarChar, 1).Value = sChangeOfOffender;
            com.Parameters.Add("@RepDetails", SqlDbType.Text).Value = repDetails;
            com.Parameters.Add("@LetterTo", SqlDbType.Char, 1).Value = letterTo;
            com.Parameters.Add("@IsSummons", SqlDbType.Bit, 1).Value = isSummons;
            //com.Parameters.Add("@ChangeRegNo", SqlDbType.Char, 1).Value = changeRegNo;
            com.Parameters.Add("@NoOfDaysForPayment", SqlDbType.Int, 4).Value = noOfDaysForPayment;
            //com.Parameters.Add("@DataWashingActived", SqlDbType.Char, 1).Value = "N";
            //com.Parameters.Add("@UseRepAddress", SqlDbType.Char, 1).Value = "N";
            //com.Parameters.Add("@RepAddrExpiryPeriod", SqlDbType.Int, 4).Value = 12;

            try
            {
                con.Open();
                //return Convert.ToInt32(com.ExecuteScalar());

                //dls 080412 - need to bypass the resultsets returned from nested stored procs
                SqlDataReader reader = com.ExecuteReader();
                while (!ignore)
                {
                    while (reader.Read() && !ignore)
                    {
                        Int32 nValue = 0;

                        if (Int32.TryParse(reader[0].ToString(), out nValue))
                        {
                            //ignore this result - it is returned from one of the nested stored procs
                            ignore = true;
                        }
                        else if (reader[0] == System.DBNull.Value)
                        {
                            //ignore this result - it is returned from one of the nested stored procs
                            ignore = true;
                        }
                        else
                        {
                            string value = reader.GetString(0);

                            if (value.Equals("Representation"))
                            {
                                return reader.GetInt32(1);
                            }
                            else if (value.Equals("Error"))
                            {
                                errMessage = "Error (" + reader.GetInt32(1) + ") - " + reader.GetString(2);
                                return -3;
                            }
                        }
                    }

                    // if there is another resultset go through the outer loop again 
                    if (ignore && reader.NextResult())
                        ignore = false;
                    else
                        ignore = true;

                }
                reader.Close();

            }
            catch (Exception e)
            {
                string msg = e.Message;
                return -5;
            }
            finally
            {
                con.Close();
            }
            return -1;
        }

        //Barry Dickson - added regno details 
        //dls 080320 - pass thru all variables, so that everything can get completed in one pass

        public List<Int32> DecideRepresentation(int repIntNo, int repCode,int? rwrCode, string repOfficial, string repRecommend, DateTime decisionDate,
            string lastUser, int chgIntNo, decimal amount, int csCode, Int64 chargeVersion, decimal fCurrentAmount,
            string sChangeOfOffender, string repDetails, string letterTo, bool isSummons, string changeRegNo,
            int notIntNo, string newRegNoType, string newRegNo, int newVMIntNo, int newVTIntNo, string newVehicleColourDescr,
            DriverDetails driver, bool easyPayDelete, int noOfDaysForExpiry,
            bool updatePersona, string personaSource, bool withdrawSummons, ref string errMessage, int noOfDaysForPayment, string dataWashingActived = "N", string useRepAddress = "N", int repAddrExpiryPeriod = 12, bool isPOD = false)
        {

            List<Int32> representation = new List<Int32>();
            bool ignore = false;

            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("RepresentationDecide2", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            com.Parameters.Add("@ChgIntNo", SqlDbType.Int, 4).Value = chgIntNo;
            com.Parameters.Add("@WithdrawReasonCode", SqlDbType.Int, 4).Value = rwrCode;
            com.Parameters.Add("@RepCode", SqlDbType.Int, 4).Value = repCode;
            com.Parameters.Add("@RepIntNo", SqlDbType.Int, 4).Value = repIntNo;
            com.Parameters.Add("@Official", SqlDbType.VarChar, 100).Value = repOfficial;
            com.Parameters.Add("@Recommendation", SqlDbType.Text).Value = repRecommend;
            com.Parameters.Add("@Date", SqlDbType.SmallDateTime).Value = decisionDate;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@FineAmount", SqlDbType.Float).Value = amount;
            com.Parameters.Add("@CSCode", SqlDbType.Int, 4).Value = csCode;
            com.Parameters.Add("@ChargeVersion", SqlDbType.BigInt, 8).Value = chargeVersion;
            com.Parameters.Add("@CurrentFineAmount", SqlDbType.Decimal).Value = fCurrentAmount;
            //com.Parameters.Add("@ChangeOfOffender", SqlDbType.VarChar, 1).Value = sChangeOfOffender;
            com.Parameters.Add("@RepDetails", SqlDbType.Text).Value = repDetails;
            com.Parameters.Add("@LetterTo", SqlDbType.Char, 1).Value = letterTo;
            com.Parameters.Add("@IsSummons", SqlDbType.Bit, 1).Value = isSummons;
            //com.Parameters.Add("@ChangeRegNo", SqlDbType.Char, 1).Value = changeRegNo;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters.Add("@NoOfDaysForPayment", SqlDbType.Int, 4).Value = noOfDaysForPayment;

            //new driver parameters
            //if (driver != null)
            //{
            //    com.Parameters.Add("@DrvIntNo", SqlDbType.Int, 4).Value = driver.DrvIntNo;
            //    com.Parameters.Add("@Surname", SqlDbType.VarChar, 100).Value = driver.DrvSurname;
            //    com.Parameters.Add("@Forenames", SqlDbType.VarChar, 100).Value = driver.DrvForeNames;
            //    com.Parameters.Add("@Initials", SqlDbType.VarChar, 10).Value = driver.DrvInitials;
            //    com.Parameters.Add("@IDType", SqlDbType.VarChar, 3).Value = driver.DrvIDType;
            //    com.Parameters.Add("@IDNumber", SqlDbType.VarChar, 25).Value = driver.DrvIDNumber;
            //    com.Parameters.Add("@Nationality", SqlDbType.VarChar, 3).Value = driver.DrvNationality;
            //    com.Parameters.Add("@Age", SqlDbType.VarChar, 3).Value = driver.DrvAge;
            //    com.Parameters.Add("@PO1", SqlDbType.VarChar, 100).Value = driver.DrvPOAdd1;
            //    com.Parameters.Add("@PO2", SqlDbType.VarChar, 100).Value = driver.DrvPOAdd2;
            //    com.Parameters.Add("@PO3", SqlDbType.VarChar, 100).Value = driver.DrvPOAdd3;
            //    com.Parameters.Add("@PO4", SqlDbType.VarChar, 100).Value = driver.DrvPOAdd4;
            //    com.Parameters.Add("@PO5", SqlDbType.VarChar, 100).Value = driver.DrvPOAdd5;
            //    com.Parameters.Add("@POCode", SqlDbType.VarChar, 10).Value = driver.DrvPOCode;
            //    com.Parameters.Add("@Street1", SqlDbType.VarChar, 100).Value = driver.DrvStAdd1;
            //    com.Parameters.Add("@Street2", SqlDbType.VarChar, 100).Value = driver.DrvStAdd2;
            //    com.Parameters.Add("@Street3", SqlDbType.VarChar, 100).Value = driver.DrvStAdd3;
            //    com.Parameters.Add("@Street4", SqlDbType.VarChar, 100).Value = driver.DrvStAdd4;
            //    com.Parameters.Add("@StreetCode", SqlDbType.VarChar, 100).Value = driver.DrvStCode;
            //    com.Parameters.Add("@DrvWorkNo", SqlDbType.VarChar, 20).Value = driver.DrvWorkNo;
            //    com.Parameters.Add("@DrvCellNo", SqlDbType.VarChar, 20).Value = driver.DrvCellNo;
            //    com.Parameters.Add("@DrvHomeNo", SqlDbType.VarChar, 20).Value = driver.DrvHomeNo;
            //}

            //regno details
            //com.Parameters.Add("@NotNewRegNo", SqlDbType.VarChar, 10).Value = newRegNo;
            //com.Parameters.Add("@NotNewRegNoType", SqlDbType.Char, 1).Value = newRegNoType;
            //com.Parameters.Add("@NewVMIntNo", SqlDbType.Int, 4).Value = newVMIntNo;
            //com.Parameters.Add("@NewVTIntNo", SqlDbType.Int, 4).Value = newVTIntNo;
            //com.Parameters.Add("@NewVehicleColourDescr", SqlDbType.VarChar, 20).Value = newVehicleColourDescr;

            //Delete easyPay transaction?
            //com.Parameters.Add("@EasyPayDelete", SqlDbType.Char, 1).Value = easyPayDelete ? "Y" : "N";
            //com.Parameters.Add("@NoOfDaysForExpiry", SqlDbType.TinyInt).Value = noOfDaysForExpiry;

            //update Persona in MI5
            //com.Parameters.Add("@UpdatePersona", SqlDbType.Char, 1).Value = updatePersona ? "Y" : "N";
            //com.Parameters.Add("@PersonaSource", SqlDbType.VarChar, 10).Value = personaSource;

            //withdraw summons
            //com.Parameters.Add("@WithdrawSummons", SqlDbType.Char, 1).Value = withdrawSummons ? "Y" : "N";

            //com.Parameters.Add("@DataWashingActived", SqlDbType.Char, 1).Value = dataWashingActived;
            //com.Parameters.Add("@UseRepAddress", SqlDbType.Char, 1).Value = useRepAddress;
            //com.Parameters.Add("@RepAddrExpiryPeriod", SqlDbType.Int, 4).Value = repAddrExpiryPeriod;

            com.Parameters.AddWithValue("@IsPOD", isPOD);

            try
            {
                con.Open();

                SqlDataReader reader = com.ExecuteReader();
                while (!ignore)
                {
                    while (reader.Read() && !ignore)
                    {
                        Int32 nValue = 0;

                        if (Int32.TryParse(reader[0].ToString(), out nValue))
                        {
                            //ignore this result - it is returned from one of the nested stored procs
                            ignore = true;
                        }
                        else if (reader[0] == System.DBNull.Value)
                        {
                            //ignore this result - it is returned from one of the nested stored procs
                            ignore = true;
                        }
                        else
                        {
                            string value = reader.GetString(0);

                            if (value.Equals("Representation"))
                            {
                                representation.Add(reader.GetInt32(1));
                            }
                            else if (value.Equals("Error"))
                            {
                                errMessage = "Error (" + reader.GetInt32(1) + ") - " + reader.GetString(2);
                                representation.Add(reader.GetInt32(1));
                            }
                        }
                    }

                    // if there is another resultset go through the outer loop again 
                    if (ignore && reader.NextResult())
                        ignore = false;
                    else
                        ignore = true;

                }
                reader.Close();
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                representation.Add(-1);
            }
            finally
            {
                con.Dispose();
            }

            return representation;
        }
        // 2013-07-19 comment by Henry for useless
        //public SqlDataReader GetRepresentationList(int chgIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("RepresentationList", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterChgIntNo = new SqlParameter("@ChgIntNo", SqlDbType.Int, 4);
        //    parameterChgIntNo.Value = chgIntNo;
        //    myCommand.Parameters.Add(parameterChgIntNo);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the data reader result
        //    return result;
        //}

        /// <summary>
        /// Only for Change of Offender
        /// </summary>
        // 2014-02-14, Oscar added
        public List<int> DecideRepresentationForCoo(out string error,
            int repCode, string repOfficial, string repRecommend, DateTime decisionDate, string repOffenderName, string repOffenderAddress, string repOffenderTelNo, string repType, string lastUser, int chgIntNo, int csCode, long chargeVersion, string changeOfOffender, string repDetails, string letterTo, bool isSummons, string changeRegNo, int notIntNo, string newRegNo, string newRegNoType, int newVMIntNo, int newVTIntNo, string newVehicleColourDescr, string personaSource, int noOfDaysForPayment, string dataWashingActived = "N", string useRepAddress = "N", int repAddrExpiryPeriod = 12, bool isPOD = false,
            DriverDetails driver = null)
        {
            error = null;
            var result = new List<int>();

            #region Parameters

            var paras = new[]
            {
                new SqlParameter("@RepCode", repCode),
                new SqlParameter("@Official", repOfficial),
                new SqlParameter("@Recommendation", repRecommend),
                new SqlParameter("@Date", decisionDate),
                new SqlParameter("@RepOffenderName", repOffenderName),
                new SqlParameter("@RepOffenderAddress", repOffenderAddress),
                new SqlParameter("@RepOffenderTelNo", repOffenderTelNo),
                new SqlParameter("@RepType", repType),
                new SqlParameter("@LastUser", lastUser),
                new SqlParameter("@ChgIntNo", chgIntNo),
                new SqlParameter("@CSCode", csCode),
                new SqlParameter("@ChargeVersion", chargeVersion),
                new SqlParameter("@ChangeOfOffender", changeOfOffender),
                new SqlParameter("@RepDetails", repDetails),
                new SqlParameter("@LetterTo", letterTo),
                new SqlParameter("@IsSummons", isSummons),
                new SqlParameter("@ChangeRegNo", changeRegNo),
                new SqlParameter("@NotIntNo", notIntNo),
                new SqlParameter("@NotNewRegNo", newRegNo),
                new SqlParameter("@NotNewRegNoType", newRegNoType),
                new SqlParameter("@NewVMIntNo", newVMIntNo),
                new SqlParameter("@NewVTIntNo", newVTIntNo),
                new SqlParameter("@NewVehicleColourDescr", newVehicleColourDescr),
                new SqlParameter("@PersonaSource", personaSource),
                new SqlParameter("@NoOfDaysForPayment", noOfDaysForPayment),
                new SqlParameter("@DataWashingActived", dataWashingActived),
                new SqlParameter("@UseRepAddress", useRepAddress),
                new SqlParameter("@RepAddrExpiryPeriod", repAddrExpiryPeriod),
                new SqlParameter("@IsPOD", isPOD)
            };

            if (driver != null)
            {
                paras = paras.Concat(new[]
                {
                    new SqlParameter("@DrvIntNo", driver.DrvIntNo),
                    new SqlParameter("@Surname", driver.DrvSurname),
                    new SqlParameter("@ForeNames", driver.DrvForeNames),
                    new SqlParameter("@Initials", driver.DrvInitials),
                    new SqlParameter("@IDType", driver.DrvIDType),
                    new SqlParameter("@IDNumber", driver.DrvIDNumber),
                    new SqlParameter("@Nationality", driver.DrvNationality),
                    new SqlParameter("@Age", driver.DrvAge),
                    new SqlParameter("@PO1", driver.DrvPOAdd1),
                    new SqlParameter("@PO2", driver.DrvPOAdd2),
                    new SqlParameter("@PO3", driver.DrvPOAdd3),
                    new SqlParameter("@PO4", driver.DrvPOAdd4),
                    new SqlParameter("@PO5", driver.DrvPOAdd5),
                    new SqlParameter("@POCode", driver.DrvPOCode),
                    new SqlParameter("@Street1", driver.DrvStAdd1),
                    new SqlParameter("@Street2", driver.DrvStAdd2),
                    new SqlParameter("@Street3", driver.DrvStAdd3),
                    new SqlParameter("@Street4", driver.DrvStAdd4),
                    new SqlParameter("@StreetCode", driver.DrvStCode),
                    new SqlParameter("@DrvWorkNo", driver.DrvWorkNo),
                    new SqlParameter("@DrvCellNo", driver.DrvCellNo),
                    new SqlParameter("@DrvHomeNo", driver.DrvHomeNo)
                }).ToArray();
            }

            #endregion

            #region Operation

            try
            {
                using (var conn = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand("RepresentationDecideForCoo", conn) { CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.AddRange(paras);
                        conn.Open();
                        using (var dr = cmd.ExecuteReader())
                        {
                            do
                            {
                                while (dr.Read())
                                {
                                    var obj = dr[0];

                                    //ignore this result - it is returned from one of the nested stored procs
                                    int num;
                                    if (int.TryParse(obj.ToString(), out num))
                                        break;

                                    //ignore this result - it is returned from one of the nested stored procs
                                    if (obj == DBNull.Value)
                                        break;

                                    var key = obj.ToString();
                                    var code = dr.FieldCount > 1 ? dr.GetInt32(1) : 0;
                                    var value = dr.FieldCount > 2 ? dr.GetString(2) : "";

                                    if (key.Equals("Representation", StringComparison.OrdinalIgnoreCase))
                                        result.Add(code);
                                    else if (key.Equals("Error", StringComparison.OrdinalIgnoreCase))
                                    {
                                        error = "Error (" + code + ") - " + value;
                                        result.Add(code);
                                        goto  BreakPoint;
                                    }
                                }
                            } while (dr.NextResult());

                            BreakPoint:
                            ;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            #endregion

            return result;
        }

        /// <summary>
        /// Only for Presentation Of Document
        /// </summary>
        // 2014-02-14, Oscar added
        public List<int> DecideRepresentationForPod(out string error,
            int repCode, string repOfficial, string repRecommend, DateTime decisionDate, string repOffenderName, string repOffenderAddress, string repOffenderTelNo, string repType, string lastUser, int chgIntNo, int csCode, long chargeVersion, string changeOfOffender, string repDetails, string letterTo, bool isSummons, string changeRegNo, int notIntNo, int noOfDaysForPayment, bool isPOD = false)
        {
            error = null;
            var result = new List<int>();

            #region Parameters

            var paras = new[]
            {
                new SqlParameter("@RepCode", repCode),
                new SqlParameter("@Official", repOfficial),
                new SqlParameter("@Recommendation", repRecommend),
                new SqlParameter("@Date", decisionDate),
                new SqlParameter("@RepOffenderName", repOffenderName),
                new SqlParameter("@RepOffenderAddress", repOffenderAddress),
                new SqlParameter("@RepOffenderTelNo", repOffenderTelNo),
                new SqlParameter("@RepType", repType),
                new SqlParameter("@LastUser", lastUser),
                new SqlParameter("@ChgIntNo", chgIntNo),
                new SqlParameter("@CSCode", csCode),
                new SqlParameter("@ChargeVersion", chargeVersion),
                new SqlParameter("@ChangeOfOffender", changeOfOffender),
                new SqlParameter("@RepDetails", repDetails),
                new SqlParameter("@LetterTo", letterTo),
                new SqlParameter("@IsSummons", isSummons),
                new SqlParameter("@ChangeRegNo", changeRegNo),
                new SqlParameter("@NotIntNo", notIntNo),
                new SqlParameter("@NoOfDaysForPayment", noOfDaysForPayment),
                new SqlParameter("@IsPOD", isPOD)
            };

            #endregion

            #region Operation

            try
            {
                using (var conn = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand("RepresentationDecideForPod", conn) { CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.AddRange(paras);
                        conn.Open();
                        using (var dr = cmd.ExecuteReader())
                        {
                            do
                            {
                                while (dr.Read())
                                {
                                    var obj = dr[0];

                                    //ignore this result - it is returned from one of the nested stored procs
                                    int num;
                                    if (int.TryParse(obj.ToString(), out num))
                                        break;

                                    //ignore this result - it is returned from one of the nested stored procs
                                    if (obj == DBNull.Value)
                                        break;

                                    var key = obj.ToString();
                                    var code = dr.FieldCount > 1 ? dr.GetInt32(1) : 0;
                                    var value = dr.FieldCount > 2 ? dr.GetString(2) : "";

                                    if (key.Equals("Representation", StringComparison.OrdinalIgnoreCase))
                                        result.Add(code);
                                    else if (key.Equals("Error", StringComparison.OrdinalIgnoreCase))
                                    {
                                        error = "Error (" + code + ") - " + value;
                                        result.Add(code);
                                        goto BreakPoint;
                                    }
                                }
                            } while (dr.NextResult());

                            BreakPoint:
                            ;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            #endregion

            return result;
        }

        /// <summary>
        /// Retrieves for charge representations within a specific Authority.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="ticketNumber">The ticket number.</param>
        /// <param name="nRepIntNo">The rep int no.</param>
        /// <param name="repAction">The representation action.</param>
        /// <param name="columnName">Name of the column to check whether payment date is valid.</param>
        /// <returns>A SQL Data Reader</returns>
        public SqlDataReader NewSearchRepresentations(int autIntNo, string ticketNumber, int nRepIntNo, string repAction, string columnName)
        {
            return this.NewSearchRepresentations(autIntNo, ticketNumber, nRepIntNo, repAction, columnName, 255);
        }

        /// <summary>
        /// News the search representations.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="ticketNumber">The ticket number.</param>
        /// <param name="nRepIntNo">The n rep int no.</param>
        /// <param name="repAction">The rep action.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="minStatus">The min status.</param>
        /// <returns></returns>
        public SqlDataReader NewSearchRepresentations(int autIntNo, string ticketNumber, int nRepIntNo, string repAction, string columnName, int minStatus)
        {
            // Setup the SQL objects
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("RepresentationSearch", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SP
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = ticketNumber;
            com.Parameters.Add("@RepIntNo", SqlDbType.Int, 4).Value = nRepIntNo;
            com.Parameters.Add("@RepAction", SqlDbType.VarChar, 1).Value = repAction;
            com.Parameters.Add("@ColName", SqlDbType.VarChar, 50).Value = columnName;
            com.Parameters.Add("@MinStatus", SqlDbType.Int, 4).Value = minStatus;

            // Return the dataset result
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        /// <summary>
        /// Retrieves the tickets with representations.
        /// </summary>
        /// <param name="authIntNo">The auth int no.</param>
        /// <param name="ticketNo">The ticket no.</param>
        /// <returns>A <see cref="SqlDataReader"/>.</returns>
        //public SqlDataReader RetrieveRepresentations(int authIntNo, string ticketNo)
        //{
        //    // Setup the SQL objects
        //    SqlConnection con = new SqlConnection(connectionString);
        //    SqlCommand com = new SqlCommand("RepresentationRetrieval", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SP
        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = authIntNo;
        //    com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = ticketNo;

        //    // Return the dataset result
        //    con.Open();
        //    return com.ExecuteReader(CommandBehavior.CloseConnection);
        //}

        /// <summary>
        /// Retrieves the representation.
        /// </summary>
        /// <param name="repIntNo">The representation int no.</param>
        /// <returns></returns>
        public SqlDataReader RetrieveRepresentation(int repIntNo, bool isSummons)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("RepresentationDetails", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("RepIntNo", SqlDbType.Int, 4).Value = repIntNo;
            com.Parameters.Add("@IsSummons", SqlDbType.Bit, 1).Value = isSummons;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        /// <summary>
        /// Updates a representation.
        /// </summary>
        /// <param name="repIntNo">The rep int no.</param>
        /// <param name="chgIntNo">The Charge int no.</param>
        /// <param name="repDetails">The representation details.</param>
        /// <param name="repDate">The representation date.</param>
        /// <param name="repOffenderName">Name of the offender.</param>
        /// <param name="repOffenderAddress">The offender address.</param>
        /// <param name="repOffenderTelNo">The offender tel no.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns>The Representation Int No.</returns>
        public int NewUpdateRepresentation(int repIntNo, int chgIntNo, string repDetails, DateTime repDate,
            string repOffenderName, string repOffenderAddress, string repOffenderTelNo, string repOfficial, string lastUser,
            int autIntNo, int csCode, decimal fCurrentFineAmount, string sChangeOfOffender, string repType,
            ref string errMsg, bool isSummons)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("RepresentationUpdate", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@ChgIntNo", SqlDbType.Int, 4).Value = chgIntNo;
            com.Parameters.Add("@RepDetails", SqlDbType.Text).Value = repDetails;
            com.Parameters.Add("@RepDate", SqlDbType.SmallDateTime).Value = repDate;
            com.Parameters.Add("@RepOffenderName", SqlDbType.VarChar, 50).Value = repOffenderName;
            com.Parameters.Add("@RepOffenderAddress", SqlDbType.Text).Value = repOffenderAddress;
            com.Parameters.Add("@RepOffenderTelNo", SqlDbType.VarChar, 100).Value = repOffenderTelNo;
            com.Parameters.Add("@RepOfficial", SqlDbType.VarChar, 100).Value = repOfficial;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@RepIntNo", SqlDbType.Int, 4).Value = repIntNo;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@CSCode", SqlDbType.Int, 4).Value = csCode;
            com.Parameters.Add("@CurrentFineAmount", SqlDbType.Decimal).Value = fCurrentFineAmount;
            com.Parameters.Add("@ChangeOfOffender", SqlDbType.VarChar, 1).Value = sChangeOfOffender;
            com.Parameters.Add("@RepType", SqlDbType.Char, 1).Value = repType;
            com.Parameters.Add("@IsSummons", SqlDbType.Bit, 1).Value = isSummons;

            try
            {
                con.Open();
                repIntNo = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (Exception e)
            {
                con.Dispose();
                errMsg = e.Message;
                return -2;
            }
            finally
            {
                con.Dispose();
            }

            return repIntNo;
        }

        /// <summary>
        /// Update representation print status for report engine
        /// </summary>
        public int UpdateRepresentationForPrint(int autIntNo, int repIntNo, string lastUser, string repType,
    bool isSummons, out string error)
        {
            // 2014-02-12, Oscar added returned value handling
            var result = 0;
            error = null;

            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("RepresentationPrintUpdate", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@RepIntNo", SqlDbType.Int, 4).Value = repIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@RepType", SqlDbType.Char, 1).Value = repType;
            com.Parameters.Add("@IsSummons", SqlDbType.Bit, 1).Value = isSummons;

            try
            {
                con.Open();
                //com.ExecuteNonQuery();
                // 2014-02-12, Oscar added returned value handling
                int.TryParse(Convert.ToString(com.ExecuteScalar()), out result);
            }
            catch (Exception e)
            {
                //System.Diagnostics.Debug.WriteLine(e.Message);
                // 2014-02-12, Oscar added returned value handling
                error = e.Message;
                result = -99;
            }
            finally
            {
                con.Dispose();
            }
            return result;
        }

        /// <summary>
        /// Reverses a representation.
        /// </summary>
        /// <param name="repIntNo">The rep int no.</param>
        /// <param name="chgIntNo">The Charge int no.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns>int </returns>
        //public int ReverseRepresentation(int repIntNo, int chgIntNo, string repReverseReason, string lastUser, 
        //    String repAction, bool isSummons)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(connectionString);
        //    SqlCommand com = new SqlCommand("RepresentationReversal", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@ChgIntNo", SqlDbType.Int, 4).Value = chgIntNo;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
        //    com.Parameters.Add("@RepIntNo", SqlDbType.Int, 4).Value = repIntNo;
        //    com.Parameters.Add("@RepAction", SqlDbType.Char, 1).Value = repAction;
        //    com.Parameters.Add("@RepReverseReason", SqlDbType.VarChar, 100).Value = repReverseReason;
        //    com.Parameters.Add("IsSummons", SqlDbType.Bit, 1).Value = isSummons;

        //    try
        //    {
        //        con.Open();
        //        repIntNo = Convert.ToInt32(com.ExecuteScalar().ToString());
        //        con.Dispose();
        //    }
        //    catch (Exception e)
        //    {
        //        con.Dispose();
        //        string msg = e.Message;
        //        return -1;
        //    }
        //    return repIntNo;
        //}

        public List<Int32> ReverseRepresentation(int repIntNo, int chgIntNo, string repReverseReason, string lastUser,
            String repAction, bool isSummons, OwnerDetails owner, int drvIntNo, int notIntNo, string drvIDNumber,
            bool changeOfOffender, ref string errMessage, string dataWashingActived, string useRepAddress, int repAddrExpiryPeriod)
        {
            List<Int32> representation = new List<Int32>();
            bool ignore = false;

            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("RepresentationReversal", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@ChgIntNo", SqlDbType.Int, 4).Value = chgIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@RepIntNo", SqlDbType.Int, 4).Value = repIntNo;
            com.Parameters.Add("@RepAction", SqlDbType.Char, 1).Value = repAction;
            com.Parameters.Add("@RepReverseReason", SqlDbType.VarChar, 100).Value = repReverseReason;
            com.Parameters.Add("@IsSummons", SqlDbType.Bit, 1).Value = isSummons;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters.Add("@ChangeOfOffender", SqlDbType.Char, 1).Value = changeOfOffender ? 'Y' : 'N';

            //Owner details to change the driver details

            if (owner != null)
            {
                com.Parameters.Add("@DrvIntNo", SqlDbType.Int, 4).Value = drvIntNo;
                com.Parameters.Add("@Surname", SqlDbType.VarChar, 100).Value = owner.OwnSurname;
                com.Parameters.Add("@Forenames", SqlDbType.VarChar, 100).Value = owner.OwnForeNames;
                com.Parameters.Add("@Initials", SqlDbType.VarChar, 10).Value = owner.OwnInitials;
                com.Parameters.Add("@IDType", SqlDbType.VarChar, 3).Value = owner.OwnIDType;
                com.Parameters.Add("@IDNumber", SqlDbType.VarChar, 25).Value = owner.OwnIDNumber;
                com.Parameters.Add("@Nationality", SqlDbType.VarChar, 3).Value = owner.OwnNationality;
                com.Parameters.Add("@Age", SqlDbType.VarChar, 3).Value = owner.OwnAge;
                com.Parameters.Add("@PO1", SqlDbType.VarChar, 100).Value = owner.OwnPOAdd1;
                com.Parameters.Add("@PO2", SqlDbType.VarChar, 100).Value = owner.OwnPOAdd2;
                com.Parameters.Add("@PO3", SqlDbType.VarChar, 100).Value = owner.OwnPOAdd3;
                com.Parameters.Add("@PO4", SqlDbType.VarChar, 100).Value = owner.OwnPOAdd4;
                com.Parameters.Add("@PO5", SqlDbType.VarChar, 100).Value = owner.OwnPOAdd5;
                com.Parameters.Add("@POCode", SqlDbType.VarChar, 10).Value = owner.OwnPOCode;
                com.Parameters.Add("@Street1", SqlDbType.VarChar, 100).Value = owner.OwnStAdd1;
                com.Parameters.Add("@Street2", SqlDbType.VarChar, 100).Value = owner.OwnStAdd2;
                com.Parameters.Add("@Street3", SqlDbType.VarChar, 100).Value = owner.OwnStAdd3;
                com.Parameters.Add("@Street4", SqlDbType.VarChar, 100).Value = owner.OwnStAdd4;
                com.Parameters.Add("@StreetCode", SqlDbType.VarChar, 100).Value = owner.OwnStCode;

                //Persona ID Number
                com.Parameters.Add("@PersonaDrvIDNumber", SqlDbType.VarChar, 20).Value = drvIDNumber;
            }

            com.Parameters.Add("@DataWashingActived", SqlDbType.Char, 1).Value = dataWashingActived;
            com.Parameters.Add("@UseRepAddress", SqlDbType.Char, 1).Value = useRepAddress;
            com.Parameters.Add("@RepAddrExpiryPeriod", SqlDbType.Int, 4).Value = repAddrExpiryPeriod;
            //try
            //{
            //    con.Open();
            //    repIntNo = Convert.ToInt32(com.ExecuteScalar().ToString());
            //    con.Dispose();
            //}
            //catch (Exception e)
            //{
            //    con.Dispose();
            //    string msg = e.Message;
            //    return -1;
            //}
            //return repIntNo;

            try
            {
                con.Open();

                SqlDataReader reader = com.ExecuteReader();
                while (!ignore)
                {
                    while (reader.Read() && !ignore)
                    {
                        Int32 nValue = 0;

                        if (Int32.TryParse(reader[0].ToString(), out nValue))
                        {
                            //ignore this result - it is returned from one of the nested stored procs
                            ignore = true;
                        }
                        else if (reader[0] == System.DBNull.Value)
                        {
                            //ignore this result - it is returned from one of the nested stored procs
                            ignore = true;
                        }
                        else
                        {
                            string value = reader.GetString(0);

                            if (value.Equals("Representation"))
                            {
                                representation.Add(reader.GetInt32(1));
                            }
                            else if (value.Equals("Error"))
                            {
                                errMessage = "Error (" + reader.GetInt32(1) + ") - " + reader.GetString(2);
                                representation.Add(reader.GetInt32(1));
                            }
                        }
                    }

                    // if there is another resultset go through the outer loop again 
                    if (ignore && reader.NextResult())
                        ignore = false;
                    else
                        ignore = true;

                }
                reader.Close();
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                representation.Add(-1);
            }
            finally
            {
                con.Dispose();
            }

            return representation;
        }

        /// <summary>
        /// Reverses a representation.
        /// </summary>
        /// <param name="repIntNo">The rep int no.</param>
        /// <param name="chgIntNo">The Charge int no.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns>int </returns>
        //public int ReverseChargeAmount(int chgIntNo, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(connectionString);
        //    SqlCommand com = new SqlCommand("ReverseChargeAmount", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@ChgIntNo", SqlDbType.Int, 4).Value = chgIntNo;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();
        //        con.Dispose();
        //    }
        //    catch (Exception e)
        //    {
        //        con.Dispose();
        //        string msg = e.Message;
        //        return -1;
        //    }
        //    return 1;
        //}

        /// <summary>
        /// Deletes the representation.
        /// </summary>
        /// <param name="repIntNo">The representation int no.</param>
        /// <returns>The number of rows affected</returns>
        public int DeleteRepresentation(int repIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("RepresentationDelete", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add the Parameter to the SP
            com.Parameters.Add("@RepIntNo", SqlDbType.Int, 4).Value = repIntNo;

            try
            {
                con.Open();
                return com.ExecuteNonQuery();
            }
            catch
            {
                return 0;
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Gets a data set of representations the with no results.
        /// </summary>
        /// <param name="autIntNo">The authority int no. 
        /// If it is less than one then all Authorities are returned.</param>
        /// <returns>A <see cref="dsRepresentationsWithNoResults"/>.</returns>
        /// Jake 2014-09-16 added parameter year
        /// default value=2000, will get all data if year=2000
        /// 2014-09-23 Heidi added @BeginDate and @EndDate Filter conditions(5360)
        public dsRepresentationsWithNoResults RepresentationsWithNoResults(int autIntNo, int year = 2000, string beginDate ="", string endDate ="")
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("RepresentationsWithNoResults", con) { CommandTimeout = 0 }; 
            com.CommandType = CommandType.StoredProcedure;

            if (autIntNo > 0)
                com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@Year", SqlDbType.Int, 4).Value = year;

            DateTime BeginDate = Convert.ToDateTime("1900-01-01");
            DateTime EndDate = Convert.ToDateTime("2079-06-06");

            if (!string.IsNullOrEmpty(beginDate))
            {
                BeginDate = Convert.ToDateTime(beginDate);
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                EndDate = Convert.ToDateTime(endDate);
            } 
            SqlParameter parameterBeginDate = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            parameterBeginDate.Value = BeginDate;
            com.Parameters.Add(parameterBeginDate);

            SqlParameter parameterEndDate = new SqlParameter("@EndDate", SqlDbType.DateTime);
            parameterEndDate.Value = EndDate;
            com.Parameters.Add(parameterEndDate);
           

            dsRepresentationsWithNoResults ds = new dsRepresentationsWithNoResults();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            // Oscar 20121011 added dispose
            com.Dispose();
            con.Dispose();

            return ds;
        }

        /// <summary>
        /// Withdraws a summons.
        /// </summary>
        /// <param name="SChIntNo">The SumCharge int no.</param>
        public void WithdrawSummons(int SChIntNo, string lastUser)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SummonsRepresentationWithdrawSummons", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@SChIntNo", SqlDbType.Int, 4).Value = SChIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// 2011-09-30 to update Representation or SummonsRepresentation table columns
        /// </summary>
        /// <param name="isPrintBatch"></param>
        /// <param name="isSummons"></param>
        /// <param name="repIntNo"></param>
        /// <param name="autIntNo"></param>
        /// <param name="repPrintedFlag"></param>
        /// <param name="repPrintedDate"></param>
        /// <param name="repPrintFileName"></param>
        public void RepresentationUpdateForPrintBatch(bool isPrintBatch, bool isSummons, int repIntNo, int autIntNo, bool repPrintedFlag, bool isSetRepPrintedDate, string repPrintUser, string repPrintFileName = null)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("RepresentationUpdateForPrintBatch", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@IsPrintBatch", SqlDbType.Bit, 1).Value = isPrintBatch;
            com.Parameters.Add("@IsSummons", SqlDbType.Bit, 1).Value = isSummons;
            com.Parameters.Add("@RepIntNo", SqlDbType.Int, 4).Value = repIntNo;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@RepPrintedFlag", SqlDbType.Bit, 1).Value = repPrintedFlag;
            if (isSetRepPrintedDate)
            {
                com.Parameters.Add("@RepPrintedDate", SqlDbType.SmallDateTime).Value = DateTime.Now;
            }
            else
            {
                com.Parameters.Add("@RepPrintedDate", SqlDbType.SmallDateTime).Value = null;
            }

            com.Parameters.Add("@RepPrintFileName", SqlDbType.NVarChar, 100).Value = repPrintFileName;
            com.Parameters.Add("@RepPrintUser", SqlDbType.NVarChar, 50).Value = repPrintUser;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Get data set of Representation batch file
        /// </summary>
        /// <param name="autIntNo"></param>
        /// <param name="showAll"></param>
        /// <returns></returns>
        public DataSet RepresentationGetPrintBatchFile(int autIntNo, string showAll, int pageSize, int pageIndex, out int totalCount)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlDataAdapter sqlPrintrun = new SqlDataAdapter();
            DataSet dsPrintrun = new DataSet();

            // Create Instance of Connection and Command Object
            sqlPrintrun.SelectCommand = new SqlCommand("RepresentationGetPrintBatchFile", con);

            // Mark the Command as a SPROC
            sqlPrintrun.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            sqlPrintrun.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            sqlPrintrun.SelectCommand.Parameters.Add("@ShowAll", SqlDbType.Char, 1).Value = showAll;
            sqlPrintrun.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            sqlPrintrun.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter sqlParaTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            sqlParaTotalCount.Direction = ParameterDirection.Output;
            sqlPrintrun.SelectCommand.Parameters.Add(sqlParaTotalCount);

            // Execute the command and close the connection
            sqlPrintrun.Fill(dsPrintrun);
            sqlPrintrun.SelectCommand.Connection.Dispose();

            totalCount = (int)(sqlParaTotalCount.Value == DBNull.Value ? 0 : sqlParaTotalCount.Value);

            // Return the dataset result
            return dsPrintrun;
        }

        /// <summary>
        /// update RepPrintedDate and flag
        /// </summary>
        /// <param name="autIntNo"></param>
        /// <param name="printFileName"></param>
        /// <returns></returns>
        public int UpdateRepresentationLetterPrinted(int autIntNo, string printFileName)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("UpdateRepresentationLetterPrinted", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@PrintFileName", SqlDbType.VarChar, 50).Value = printFileName;
            
            SqlParameter parameterNoOfRows = com.Parameters.Add("@NoOfRows", SqlDbType.Int, 4);
            parameterNoOfRows.Direction = ParameterDirection.InputOutput;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                int noOfRows = (int)com.Parameters["@NoOfRows"].Value;

                return noOfRows;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return -1;
            }
            finally
            {
                con.Dispose();
            }
        }

        /// <summary>
        /// Get RepIntNo By PrintFile
        /// </summary>
        /// <param name="printFileName"></param>
        /// <returns></returns>
        public SqlDataReader GetRepInfoByPrintFile(string printFileName, bool printedFlag)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("RepresentationGetRepInfoByPrintFile", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@PrintFileName", SqlDbType.NVarChar, 100).Value = printFileName;
            com.Parameters.Add("@PrintedFlag", SqlDbType.Bit).Value = printedFlag;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        /// <summary>
        /// Get rep info by repIntNo
        /// </summary>
        /// <param name="repIntNo"></param>
        /// <param name="isSummons"></param>
        /// <returns></returns>
        public SqlDataReader GetRepPrintFileNameByRepIntNo(int repIntNo, bool isSummons)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("RepresentationGetRepInfoByRepIntNo", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@RepIntNo", SqlDbType.Int).Value = repIntNo;
            com.Parameters.Add("@isSummons", SqlDbType.Bit).Value = isSummons;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public int CheckIsHandwrittenCorrrection(int chgIntNo, bool isSummons)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("CheckIsHandwrittenCorrrection", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@ChgIntNo", SqlDbType.Int, 4).Value = chgIntNo;
            com.Parameters.Add("@IsSummons", SqlDbType.Bit, 1).Value = isSummons;
            com.Parameters.Add("@OutCode", SqlDbType.Int).Direction = ParameterDirection.Output;

            try
            {
                con.Open();
                return Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (Exception e)
            {
                con.Dispose();
                return -2;
            }
            finally
            {
                con.Dispose();
            }
        }

        /// <summary>
        /// Jerry 2013-04-03 check notice or summons is paid
        /// </summary>
        /// <param name="chgIntNo"></param>
        /// <param name="isSummons"></param>
        /// <returns></returns>
        public int CheckNoticeOrSummonsIsPaid(int chgIntNo, bool isSummons)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("CheckNoticeOrSummonsIsPaid", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@ChgIntNo", SqlDbType.Int, 4).Value = chgIntNo;
            com.Parameters.Add("@IsSummons", SqlDbType.Bit, 1).Value = isSummons;

            try
            {
                con.Open();
                return Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                con.Dispose();
                return -2;
            }
            finally
            {
                con.Dispose();
            }
        }

    }
}
