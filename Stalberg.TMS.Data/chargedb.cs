using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{

    public partial class ChargeDetails
    {
        public int ChgIntNo;
        public int NotIntNo;
        public int CSIntNo;
        public int CTIntNo;
        public string ChgOffenceType;
        public string ChgOffenceDescr;
        public string ChgOffenceDescr2;
        public string ChgOffenceDescr3;
        public string ChgStatutoryRef;
        public string ChgStatRefLine1;
        public string ChgStatRefLine2;
        public string ChgStatRefLine3;
        public string ChgStatRefLine4;
        public string ChgOffenceCode;
        public double ChgFineAmount;
        public string ChgFineAlloc;
        public decimal RevFineAmount;
        public Int64 RowVersion;
        public int ChargeStatus;
    }

    /// <summary>
    /// Contains all the database code for interacting with Charges.
    /// </summary>
    public partial class ChargeDB
    {
        string mConstr = string.Empty;

        public ChargeDB(string vConstr)
        {
            mConstr = vConstr;
        }

        /// <summary>
        /// The GetChargeDetails method returns a ChargeDetails
        /// struct that contains information about a specific transaction number
        /// </summary>
        /// <param name="ChgIntNo">The CHG int no.</param>
        /// <returns></returns>
        public ChargeDetails GetChargeDetails(int ChgIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ChargeDetail", con);
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@ChgIntNo", SqlDbType.Int, 4).Value = ChgIntNo;

            con.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            ChargeDetails myChargeDetails = new ChargeDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myChargeDetails.NotIntNo = Convert.ToInt32(result["NotIntNo"]);
                //myChargeDetails.CSIntNo = Convert.ToInt32(result["CSIntNo"]);
                myChargeDetails.CTIntNo = Convert.ToInt32(result["CTIntNo"]);
                myChargeDetails.ChgFineAmount = Convert.ToDouble(result["ChgFineAmount"]);
                myChargeDetails.ChgOffenceDescr = result["ChgOffenceDescr"].ToString();
                myChargeDetails.ChgOffenceDescr2 = result["ChgOffenceDescr2"].ToString();
                myChargeDetails.ChgOffenceDescr3 = result["ChgOffenceDescr3"].ToString();
                myChargeDetails.ChgOffenceType = result["ChgOffenceType"].ToString();
                myChargeDetails.ChgFineAlloc = result["ChgFineAlloc"].ToString();
                myChargeDetails.ChgStatutoryRef = result["ChgStatutoryRef"].ToString();
                myChargeDetails.ChgStatRefLine1 = result["ChgStatRefLine1"].ToString();
                myChargeDetails.ChgStatRefLine2 = result["ChgStatRefLine2"].ToString();
                myChargeDetails.ChgStatRefLine3 = result["ChgStatRefLine3"].ToString();
                myChargeDetails.ChgStatRefLine4 = result["ChgStatRefLine4"].ToString();
                myChargeDetails.ChgOffenceCode = result["ChgOffenceCode"].ToString();
                myChargeDetails.RevFineAmount = Convert.ToDecimal(result["ChgRevFineAmount"]);
                myChargeDetails.RowVersion = Convert.ToInt64(result["ChargeRowVersion"]);
                myChargeDetails.ChargeStatus = Convert.ToInt32(result["ChargeStatus"]);
                myChargeDetails.ChgIntNo = ChgIntNo;
            }
            result.Close();
            con.Close();

            return myChargeDetails;
        }

        //*******************************************************
        //
        // The AddCharge method inserts a new transaction number record
        // into the Charge database.  A unique "ChgIntNo"
        // key is then returned from the method.  
        //
        //*******************************************************

        // 2013-07-19 comment by Henry for useless
        //public int AddCharge(int notIntNo, int csIntNo, int ctIntNo,
        //    string chgOffenceDescr, string chgOffenceCode, string chgOffenceType,
        //    string chgStatutoryRef, string chgFineAlloc,
        //    double chgFineAmount, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChargeAdd", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
        //    parameterNotIntNo.Value = notIntNo;
        //    myCommand.Parameters.Add(parameterNotIntNo);

        //    SqlParameter parameterCSIntNo = new SqlParameter("@CSIntNo", SqlDbType.Int, 4);
        //    parameterCSIntNo.Value = csIntNo;
        //    myCommand.Parameters.Add(parameterCSIntNo);

        //    SqlParameter parameterCTIntNo = new SqlParameter("@CTIntNo", SqlDbType.Int, 4);
        //    parameterCTIntNo.Value = ctIntNo;
        //    myCommand.Parameters.Add(parameterCTIntNo);

        //    SqlParameter parameterChgOffenceType = new SqlParameter("@ChgOffenceType", SqlDbType.Char, 2);
        //    parameterChgOffenceType.Value = chgOffenceType;
        //    myCommand.Parameters.Add(parameterChgOffenceType);

        //    SqlParameter parameterChgOffenceDescr = new SqlParameter("@ChgOffenceDescr", SqlDbType.VarChar, 50);
        //    parameterChgOffenceDescr.Value = chgOffenceDescr;
        //    myCommand.Parameters.Add(parameterChgOffenceDescr);

        //    SqlParameter parameterChgOffenceCode = new SqlParameter("@ChgOffenceCode", SqlDbType.VarChar, 15);
        //    parameterChgOffenceCode.Value = chgOffenceCode;
        //    myCommand.Parameters.Add(parameterChgOffenceCode);

        //    SqlParameter parameterChgStatutoryRef = new SqlParameter("@ChgStatutoryRef", SqlDbType.Text);
        //    parameterChgStatutoryRef.Value = chgStatutoryRef;
        //    myCommand.Parameters.Add(parameterChgStatutoryRef);

        //    SqlParameter parameterChgFineAlloc = new SqlParameter("@ChgFineAlloc", SqlDbType.VarChar, 15);
        //    parameterChgFineAlloc.Value = chgFineAlloc;
        //    myCommand.Parameters.Add(parameterChgFineAlloc);

        //    SqlParameter parameterChgFineAmount = new SqlParameter("@ChgFineAmount", SqlDbType.Real, 4);
        //    parameterChgFineAmount.Value = chgFineAmount;
        //    myCommand.Parameters.Add(parameterChgFineAmount);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterChgIntNo = new SqlParameter("@ChgIntNo", SqlDbType.Int, 4);
        //    parameterChgIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterChgIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int ChgIntNo = Convert.ToInt32(parameterChgIntNo.Value);

        //        return ChgIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        //public SqlDataReader GetChargeListByBatch(int batIntNo, string csCode)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChargeListByBatch", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterBatIntNo = new SqlParameter("@BatIntNo", SqlDbType.Int, 4);
        //    parameterBatIntNo.Value = batIntNo;
        //    myCommand.Parameters.Add(parameterBatIntNo);

        //    SqlParameter parameterCSCode = new SqlParameter("@CSCode", SqlDbType.Char, 3);
        //    parameterCSCode.Value = csCode;
        //    myCommand.Parameters.Add(parameterCSCode);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the datareader result
        //    return result;
        //}

        public SqlDataReader GetChargeListByNotice(int notIntNo, int status)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ChargeListByNotice", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = notIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
            parameterStatus.Value = status;
            myCommand.Parameters.Add(parameterStatus);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        //public SqlDataReader GetNaTISSendData(int notIntNo)
        //{
        //    // Returns a recordset
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChargeSendNaTIS", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
        //    parameterNotIntNo.Value = notIntNo;
        //    myCommand.Parameters.Add(parameterNotIntNo);
        //    try
        //    {
        //        myConnection.Open();
        //        SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //        return result;
        //    }
        //    catch (Exception e)
        //    {
        //        string error = e.Message;
        //        return null;
        //    }
        //}

        // 2006/0601 not used
        //public DataSet GetChargeListDS(int autIntNo)
        //{
        //    SqlDataAdapter sqlDACharges = new SqlDataAdapter();
        //    DataSet dsCharges = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlDACharges.SelectCommand = new SqlCommand();
        //    sqlDACharges.SelectCommand.Connection = new SqlConnection(mConstr);			
        //    sqlDACharges.SelectCommand.CommandText = "ChargeList";

        //    // Mark the Command as a SPROC
        //    sqlDACharges.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    sqlDACharges.SelectCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command and close the connection
        //    sqlDACharges.Fill(dsCharges);
        //    sqlDACharges.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsCharges;		
        //}

        public DataSet GetChargeListCashDS(int notIntNo)
        {
            SqlDataAdapter sqlDAChargeCash = new SqlDataAdapter();
            DataSet dsChargeCash = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAChargeCash.SelectCommand = new SqlCommand();
            sqlDAChargeCash.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAChargeCash.SelectCommand.CommandText = "ChargeListCash";

            // Mark the Command as a SPROC
            sqlDAChargeCash.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = notIntNo;
            sqlDAChargeCash.SelectCommand.Parameters.Add(parameterNotIntNo);

            // Execute the command and close the connection
            sqlDAChargeCash.Fill(dsChargeCash);
            sqlDAChargeCash.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsChargeCash;
        }

        public DataSet GetChargeTotalsDS(int autIntNo, string startPeriod, string endPeriod)
        {
            SqlDataAdapter sqlDAChargeTotals = new SqlDataAdapter();
            DataSet dsChargeTotals = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAChargeTotals.SelectCommand = new SqlCommand();
            sqlDAChargeTotals.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAChargeTotals.SelectCommand.CommandText = "ChargeTotalsGet";

            // Mark the Command as a SPROC
            sqlDAChargeTotals.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter paramAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            paramAutIntNo.Value = autIntNo;
            sqlDAChargeTotals.SelectCommand.Parameters.Add(paramAutIntNo);

            SqlParameter paramStartPeriod = new SqlParameter("@StartPeriod", SqlDbType.VarChar, 7);
            paramStartPeriod.Value = startPeriod;
            sqlDAChargeTotals.SelectCommand.Parameters.Add(paramStartPeriod);

            SqlParameter paramEndPeriod = new SqlParameter("@EndPeriod", SqlDbType.VarChar, 7);
            paramEndPeriod.Value = endPeriod;
            sqlDAChargeTotals.SelectCommand.Parameters.Add(paramEndPeriod);

            // Execute the command and close the connection
            sqlDAChargeTotals.Fill(dsChargeTotals);
            sqlDAChargeTotals.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsChargeTotals;
        }

        public DataSet GetChargeListByNoticeDS(int notIntNo, int status)
        {
            SqlDataAdapter sqlDACharges = new SqlDataAdapter();
            DataSet dsCharges = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDACharges.SelectCommand = new SqlCommand();
            sqlDACharges.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDACharges.SelectCommand.CommandText = "ChargeListByNotice";

            // Mark the Command as a SPROC
            sqlDACharges.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = notIntNo;
            sqlDACharges.SelectCommand.Parameters.Add(parameterNotIntNo);

            SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
            parameterStatus.Value = status;
            sqlDACharges.SelectCommand.Parameters.Add(parameterStatus);

            // Execute the command and close the connection
            sqlDACharges.Fill(dsCharges);
            sqlDACharges.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsCharges;
        }

        // 2013-07-19 comment by Henry for useless
        //public int UpdateCharge(int chgIntNo, int notIntNo, int csIntNo, int ctIntNo,
        //    string chgOffenceDescr, string chgOffenceCode, string chgOffenceType,
        //    string chgStatutoryRef, string chgFineAlloc,
        //    double chgFineAmount, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChargeUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
        //    parameterNotIntNo.Value = notIntNo;
        //    myCommand.Parameters.Add(parameterNotIntNo);

        //    SqlParameter parameterCSIntNo = new SqlParameter("@CSIntNo", SqlDbType.Int, 4);
        //    parameterCSIntNo.Value = csIntNo;
        //    myCommand.Parameters.Add(parameterCSIntNo);

        //    SqlParameter parameterCTIntNo = new SqlParameter("@CTIntNo", SqlDbType.Int, 4);
        //    parameterCTIntNo.Value = ctIntNo;
        //    myCommand.Parameters.Add(parameterCTIntNo);

        //    SqlParameter parameterChgOffenceType = new SqlParameter("@ChgOffenceType", SqlDbType.Char, 2);
        //    parameterChgOffenceType.Value = chgOffenceType;
        //    myCommand.Parameters.Add(parameterChgOffenceType);

        //    SqlParameter parameterChgOffenceDescr = new SqlParameter("@ChgOffenceDescr", SqlDbType.VarChar, 50);
        //    parameterChgOffenceDescr.Value = chgOffenceDescr;
        //    myCommand.Parameters.Add(parameterChgOffenceDescr);

        //    SqlParameter parameterChgOffenceCode = new SqlParameter("@ChgOffenceCode", SqlDbType.VarChar, 15);
        //    parameterChgOffenceCode.Value = chgOffenceCode;
        //    myCommand.Parameters.Add(parameterChgOffenceCode);

        //    SqlParameter parameterChgStatutoryRef = new SqlParameter("@ChgStatutoryRef", SqlDbType.Text);
        //    parameterChgStatutoryRef.Value = chgStatutoryRef;
        //    myCommand.Parameters.Add(parameterChgStatutoryRef);

        //    SqlParameter parameterChgFineAlloc = new SqlParameter("@ChgFineAlloc", SqlDbType.VarChar, 15);
        //    parameterChgFineAlloc.Value = chgFineAlloc;
        //    myCommand.Parameters.Add(parameterChgFineAlloc);

        //    SqlParameter parameterChgFineAmount = new SqlParameter("@ChgFineAmount", SqlDbType.Real, 4);
        //    parameterChgFineAmount.Value = chgFineAmount;
        //    myCommand.Parameters.Add(parameterChgFineAmount);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterChgIntNo = new SqlParameter("@ChgIntNo", SqlDbType.Int, 4);
        //    parameterChgIntNo.Value = chgIntNo;
        //    parameterChgIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterChgIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int ChgIntNo = (int)myCommand.Parameters["@ChgIntNo"].Value;

        //        return ChgIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        // 2013-07-19 comment by Henry for useless
        //public bool UpdateChargeOnNotice(int autIntNo, string printFile)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("UpdateChargeFromCiprusOffence", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterPrintFile = new SqlParameter("@PrintFile", SqlDbType.VarChar, 50);
        //    parameterPrintFile.Value = printFile;
        //    myCommand.Parameters.Add(parameterPrintFile);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        return true;

        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return false;
        //    }
        //}

        // 2013-07-19 comment by Henry for useless
        //public int UpdateChargeBatchOK_CPI(string autNo, string camUnitID, string fileName, string lastUser,
        //    int statusNoErrors, int statusBatchOK, ref string errMessage)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand cmd = new SqlCommand("ChargeUpdateBatchOK_CPI", myConnection);

        //    // Mark the Command as a SPROC
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    cmd.Parameters.Add("@AutNo", SqlDbType.VarChar, 6).Value = autNo;
        //    cmd.Parameters.Add("@CamUnitID", SqlDbType.VarChar, 5).Value = camUnitID;
        //    cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 20).Value = fileName;
        //    cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
        //    cmd.Parameters.Add("@StatusNoErrors", SqlDbType.Int, 4).Value = statusNoErrors;
        //    cmd.Parameters.Add("@StatusBatchOK", SqlDbType.Int, 4).Value = statusBatchOK;
        //    cmd.Parameters.Add("@NoOfRows", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;

        //    try
        //    {
        //        myConnection.Open();
        //        cmd.ExecuteNonQuery();

        //        int noOfRows = (int)cmd.Parameters["@NoOfRows"].Value;
        //        return noOfRows;
        //    }
        //    catch (Exception e)
        //    {
        //        errMessage = e.Message;
        //        return -1;
        //    }
        //    finally
        //    {
        //        myConnection.Dispose();
        //    }
        //}

        public int UpdateChargeOnNotice(int chgIntNo, int offIntNo,
            string chgStatutoryRef, double chgFineAmount, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ChargeOnNoticeUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterOffIntNo = new SqlParameter("@OffIntNo", SqlDbType.Int, 4);
            parameterOffIntNo.Value = offIntNo;
            myCommand.Parameters.Add(parameterOffIntNo);

            SqlParameter parameterChgStatutoryRef = new SqlParameter("@ChgStatutoryRef", SqlDbType.Text);
            parameterChgStatutoryRef.Value = chgStatutoryRef;
            myCommand.Parameters.Add(parameterChgStatutoryRef);

            SqlParameter parameterChgFineAmount = new SqlParameter("@ChgFineAmount", SqlDbType.Real, 4);
            parameterChgFineAmount.Value = chgFineAmount;
            myCommand.Parameters.Add(parameterChgFineAmount);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterChgIntNo = new SqlParameter("@ChgIntNo", SqlDbType.Int, 4);
            parameterChgIntNo.Value = chgIntNo;
            parameterChgIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterChgIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int ChgIntNo = (int)myCommand.Parameters["@ChgIntNo"].Value;

                return ChgIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        // 2013-07-19 comment by Henry for useless
        //public String DeleteCharge(int ChgIntNo)
        //{

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChargeDelete", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterChgIntNo = new SqlParameter("@ChgIntNo", SqlDbType.Int, 4);
        //    parameterChgIntNo.Value = ChgIntNo;
        //    parameterChgIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterChgIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int userId = (int)parameterChgIntNo.Value;

        //        return userId.ToString();
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return String.Empty;
        //    }
        //}

        // 20060601 no longer used
        //public int ChargeStatusChange(int autIntNo, string oldStatus, string newStatus, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChargeStatusChange", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterOldStatus = new SqlParameter("@OldStatus", SqlDbType.Char, 3);
        //    parameterOldStatus.Value = oldStatus;
        //    myCommand.Parameters.Add(parameterOldStatus);

        //    SqlParameter parameterNewStatus = new SqlParameter("@NewStatus", SqlDbType.Char, 3);
        //    parameterNewStatus.Value = newStatus;
        //    myCommand.Parameters.Add(parameterNewStatus);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    parameterAutIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int AutIntNo = (int)myCommand.Parameters["@AutIntNo"].Value;

        //        return AutIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        // 20060601 no longer in use
        // 20061207 - back in use!
        public int ChargeStatusChangeForNotice(int notIntNo, int oldStatus, int newStatus, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ChargeStatusChangeForNotice", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterOldStatus = new SqlParameter("@OldStatus", SqlDbType.Int, 4);
            parameterOldStatus.Value = oldStatus;
            myCommand.Parameters.Add(parameterOldStatus);

            SqlParameter parameterNewStatus = new SqlParameter("@NewStatus", SqlDbType.Int, 4);
            parameterNewStatus.Value = newStatus;
            myCommand.Parameters.Add(parameterNewStatus);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = notIntNo;
            parameterNotIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNotIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                notIntNo = (int)myCommand.Parameters["@NotIntNo"].Value;

                return notIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        // LMZ 2007-04-18 - added for new Data washing functionality
        public SqlDataReader DataWashingDetails()
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("DataWashing", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            // Return the data reader result
            return result;
        }

        /// <summary>
        /// Updates the charge status.
        /// </summary>
        /// <param name="chgIntNo">The Charge int no.</param>
        /// <param name="status">The status.</param>
        public void UpdateStatus(int chgIntNo, int status, string lastUser) // 2013-07-24 add parameter lastUser by Henry
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("ChargeUpdateStatus", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@ChgIntNo", SqlDbType.Int, 4).Value = chgIntNo;
            com.Parameters.Add("@Status", SqlDbType.Int, 4).Value = status;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }


        public int UpdateStatusMissingImage(int notIntNo, int chargeStatus, ref string errMessage)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("ChargeUpdateStatusForMissingNotice", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters.Add("@ChargeStatus", SqlDbType.Int, 4).Value = chargeStatus;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                notIntNo = (int)com.Parameters["@NotIntNo"].Value;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                notIntNo = 0;
            }
            finally
            {
                con.Close();
            }

            return notIntNo;
        }

        public int SelectOffenceByCode(string code)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("SelectOffenceByCode", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@OffenceCode", SqlDbType.VarChar, 15).Value = code;
            int count = -1;
            try
            {
                con.Open();
                count = (int)com.ExecuteScalar();
            }
            catch (Exception e)
            {
                count = -1;
            }
            finally
            {
                con.Close();
            }
            return count;
        }
    }
}
