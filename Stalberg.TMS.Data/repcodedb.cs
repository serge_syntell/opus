using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // RepCodeDetails Class
    //
    // A simple data class that encapsulates details about a particular loc 
    //
    //*******************************************************

    public partial class RepCodeDetails 
	{
		public Int32 RCIntNo;
		public Int32 AutIntNo;
		public string RepCode; 
		public string RCDescr;
        public DateTime RCValidFrom; 
    }

    //*******************************************************
    //
    // RepCodeDB Class
    //
    //*******************************************************

    public partial class RepCodeDB {

		string mConstr = "";

		public RepCodeDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        //*******************************************************
        // 2013-07-19 comment by Henry for useless
        //public RepCodeDetails GetRepCodeDetails(int rcIntNo) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("RepCodeDetail", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterRCIntNo = new SqlParameter("@RCIntNo", SqlDbType.Int, 4);
        //    parameterRCIntNo.Value = rcIntNo;
        //    myCommand.Parameters.Add(parameterRCIntNo);

        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
        //    RepCodeDetails myRepCodeDetails = new RepCodeDetails();

        //    while (result.Read())
        //    {
        //        // Populate Struct using Output Params from SPROC
        //        myRepCodeDetails.RCIntNo = Convert.ToInt32(result["RCIntNo"]);
        //        myRepCodeDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
        //        myRepCodeDetails.RepCode = result["RepCode"].ToString();
        //        myRepCodeDetails.RCDescr = result["RCDescr"].ToString();
        //        if (result["RCValidFrom"] != System.DBNull.Value)
        //            myRepCodeDetails.RCValidFrom = Convert.ToDateTime(result["RCValidFrom"]);
        //    }
        //    result.Close();
        //    return myRepCodeDetails;
        //}

		/// <summary>
		/// Gets the representation code details based on the supplied authority and code.
		/// </summary>
		/// <param name="autIntNo">The aut int no.</param>
		/// <param name="repCode">The rep code.</param>
		/// <param name="rcDescr">The rc descr.</param>
		/// <param name="lastUser">The last user.</param>
		/// <returns></returns>
        // 2013-07-19 comment by Henry for useless
        //public RepCodeDetails GetRepCodeDetailsByCode(int autIntNo, string repCode, string rcDescr, string lastUser)
        //{
        //    RepCodeDetails myRepCodeDetails = new RepCodeDetails();

        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(mConstr);
        //    SqlCommand com = new SqlCommand("RepCodeDetailByCode", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to the SP
        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    com.Parameters.Add("@RepCode", SqlDbType.Char, 3).Value = repCode;
        //    com.Parameters.Add("@RCDescr", SqlDbType.VarChar, 50).Value = rcDescr;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            
        //    con.Open();
        //    SqlDataReader result = com.ExecuteReader(CommandBehavior.CloseConnection);
        //    while (result.Read())
        //    {
        //        // Populate Struct using Output Params from SPROC
        //        myRepCodeDetails.RCIntNo = Convert.ToInt32(result["RCIntNo"]);
        //        myRepCodeDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
        //        myRepCodeDetails.RepCode = result["RepCode"].ToString();
        //        myRepCodeDetails.RCDescr = result["RCDescr"].ToString();
        //        if (result["RCValidFrom"] != System.DBNull.Value)
        //            myRepCodeDetails.RCValidFrom = Convert.ToDateTime(result["RCValidFrom"]);
        //    }
        //    result.Close();

        //    return myRepCodeDetails;
        //}

		//20050715 converted to account
        // 2013-07-19 comment by Henry for useless
        //public int AddRepCode (int autIntNo, string repCode, string rcDescr, DateTime rcValidFrom, string lastUser) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("RepCodeAdd", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterRepCode = new SqlParameter("@RepCode", SqlDbType.Char, 3);
        //    parameterRepCode.Value = repCode;
        //    myCommand.Parameters.Add(parameterRepCode);

        //    SqlParameter parameterRCDescr = new SqlParameter("@RCDescr", SqlDbType.VarChar, 50);
        //    parameterRCDescr.Value = rcDescr;
        //    myCommand.Parameters.Add(parameterRCDescr);

        //    SqlParameter parameterRCValidFrom = new SqlParameter("@RCValidFrom", SqlDbType.SmallDateTime);
        //    parameterRCValidFrom.Value = rcValidFrom;
        //    myCommand.Parameters.Add(parameterRCValidFrom);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);
			
        //    SqlParameter parameterRCIntNo = new SqlParameter("@RCIntNo", SqlDbType.Int, 4);
        //    parameterRCIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterRCIntNo);

        //    try {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int rcIntNo = Convert.ToInt32(parameterRCIntNo.Value);

        //        return rcIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        // FBJ Added (2006-11-16): Removed RepCode to be able to have a consistent schema across all LAs
		// 20050718 converted to account
		public SqlDataReader GetRepCodeList(int autIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection con = new SqlConnection(this.mConstr);
			SqlCommand com = new SqlCommand("RepCodeList", con);
			com.CommandType = CommandType.StoredProcedure;

			// Execute the command
			con.Open();
			return com.ExecuteReader(CommandBehavior.CloseConnection);
		}

		// 20050715 converted to account
		public DataSet GetRepCodeListDS(int autIntNo)
		{
			SqlDataAdapter sqlDARepCodes = new SqlDataAdapter();
			DataSet dsRepCodes = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDARepCodes.SelectCommand = new SqlCommand();
			sqlDARepCodes.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDARepCodes.SelectCommand.CommandText = "RepCodeList";

			// Mark the Command as a SPROC
			sqlDARepCodes.SelectCommand.CommandType = CommandType.StoredProcedure;

			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			sqlDARepCodes.SelectCommand.Parameters.Add(parameterAutIntNo);

			// Execute the command and close the connection
			sqlDARepCodes.Fill(dsRepCodes);
			sqlDARepCodes.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsRepCodes;		
		}
		// 20050715 converted to account
        // 2013-07-19 comment by Henry for useless
        //public int UpdateRepCode(int rcIntNo, int autIntNo, string repCode, string rcDescr, 
        //    DateTime rcValidFrom, string lastUser) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("RepCodeUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterRepCode = new SqlParameter("@RepCode", SqlDbType.VarChar, 20);
        //    parameterRepCode.Value = repCode;
        //    myCommand.Parameters.Add(parameterRepCode);

        //    SqlParameter parameterRCDescr = new SqlParameter("@RCDescr", SqlDbType.VarChar, 50);
        //    parameterRCDescr.Value = rcDescr;
        //    myCommand.Parameters.Add(parameterRCDescr);

        //    SqlParameter parameterRCValidFrom = new SqlParameter("@RCValidFrom", SqlDbType.SmallDateTime);
        //    parameterRCValidFrom.Value = rcValidFrom;
        //    myCommand.Parameters.Add(parameterRCValidFrom);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterRCIntNo = new SqlParameter("@RCIntNo", SqlDbType.Int);
        //    parameterRCIntNo.Direction = ParameterDirection.InputOutput;
        //    parameterRCIntNo.Value = rcIntNo;
        //    myCommand.Parameters.Add(parameterRCIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        rcIntNo = (int)myCommand.Parameters["@RCIntNo"].Value;

        //        return rcIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

		
		public String DeleteRepCode (int rcIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("RepCodeDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterRCIntNo = new SqlParameter("@RCIntNo", SqlDbType.Int, 4);
			parameterRCIntNo.Value = rcIntNo;
			parameterRCIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterRCIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				rcIntNo = (int)parameterRCIntNo.Value;

				return rcIntNo.ToString();
			}
			catch
			{
				myConnection.Dispose();
				return String.Empty;
			}
		}
        // 2013-07-19 comment by Henry for useless
        //public int CopyRepCode(int fromAutIntNo, int autIntNo, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("RepCodeCopy", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterFromAutIntNo = new SqlParameter("@FromAutIntNo", SqlDbType.Int, 4);
        //    parameterFromAutIntNo.Value = fromAutIntNo;
        //    myCommand.Parameters.Add(parameterFromAutIntNo);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    parameterAutIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        autIntNo = Convert.ToInt32(parameterAutIntNo.Value);

        //        return autIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}
	}
}

