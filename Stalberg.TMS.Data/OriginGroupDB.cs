﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Stalberg.TMS.Data
{
    public class OriginGroupDB
    {
        string connectionString = string.Empty;

        public OriginGroupDB(string vConstr)
        {
            connectionString = vConstr;
        }

        public SqlDataReader GetOriginGroupList()
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("OgiginGroupList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        public SqlDataReader OriginGroupListByArrayStrOGIntNo(string oGIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("OriginGroupListByArrayStrOGIntNo", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            myCommand.Parameters.Add("@TempOGIntNoStr", SqlDbType.VarChar, 25).Value = oGIntNo;

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }
    }
}
