using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{
	
	public partial class NoticePrefixDetails 
	{
		public Int32 NPIntNo;
		public Int32 NTIntNo;
		public Int32 AutIntNo;
		public string NPrefix;
		public string LastUser;
	}

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public partial class NoticePrefixDB
	{
		string mConstr = "";

			public NoticePrefixDB (string vConstr)
			{
				mConstr = vConstr;
			}

		//*******************************************************
		//
		// The GetNoticePrefixDetails method returns a NoticePrefixDetails
		// struct that contains information about a specific transaction number
		//
		//*******************************************************

		public NoticePrefixDetails GetNoticePrefixDetailsByType(int ntIntNo, int autIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("NoticePrefixDetailByCode", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterNTIntNo = new SqlParameter("@NTIntNo", SqlDbType.Int, 4);
			parameterNTIntNo.Value = ntIntNo;
			myCommand.Parameters.Add(parameterNTIntNo);

			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			myCommand.Parameters.Add(parameterAutIntNo);

            #region Jerry 2014-10-27 change
            //myConnection.Open();
            //SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            //NoticePrefixDetails myNoticePrefixDetails = new NoticePrefixDetails();

            //while (result.Read())
            //{
            //    // Populate Struct using Output Params from SPROC
            //    myNoticePrefixDetails.NTIntNo = Convert.ToInt32(result["NTIntNo"]);
            //    myNoticePrefixDetails.NPIntNo = Convert.ToInt32(result["NPIntNo"]);
            //    myNoticePrefixDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
            //    myNoticePrefixDetails.NPrefix = result["NPrefix"].ToString();
            //    myNoticePrefixDetails.LastUser = result["LastUser"].ToString();
            //}
            //result.Close();
            #endregion

            NoticePrefixDetails myNoticePrefixDetails = new NoticePrefixDetails();
            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                while (result.Read())
                {
                    // Populate Struct using Output Params from SPROC
                    myNoticePrefixDetails.NTIntNo = Convert.ToInt32(result["NTIntNo"]);
                    myNoticePrefixDetails.NPIntNo = Convert.ToInt32(result["NPIntNo"]);
                    myNoticePrefixDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
                    myNoticePrefixDetails.NPrefix = result["NPrefix"].ToString();
                    myNoticePrefixDetails.LastUser = result["LastUser"].ToString();
                }
                result.Close();
                result.Dispose();
            }
            finally
            {
                myConnection.Close();
                myConnection.Dispose();
            }

			return myNoticePrefixDetails;
		}

		//*******************************************************
		//
		// The AddNoticePrefix method inserts a new transaction number record
		// into the NoticePrefix database.  A unique "TNIntNo"
		// key is then returned from the method.  
		//
		//*******************************************************

		public int AddNoticePrefix(int ntIntNo, int autIntNo, string nPrefix, string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("NoticePrefixAdd", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterNTIntNo = new SqlParameter("@NTIntNo", SqlDbType.Int, 4);
			parameterNTIntNo.Value = ntIntNo;
			myCommand.Parameters.Add(parameterNTIntNo);

			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			myCommand.Parameters.Add(parameterAutIntNo);

            // dls 070508 - Stellenbosch is using 1001 - NoticePrefix has been extended from a char(2) to a varchar(4)
			SqlParameter parameterNPrefix = new SqlParameter("@NPrefix", SqlDbType.VarChar, 4);
			parameterNPrefix.Value = nPrefix;
			myCommand.Parameters.Add(parameterNPrefix);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterNPIntNo = new SqlParameter("@NPIntNo", SqlDbType.Int, 4);
			parameterNPIntNo.Direction = ParameterDirection.Output;
			myCommand.Parameters.Add(parameterNPIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int npIntNo = Convert.ToInt32(parameterNPIntNo.Value);

				return npIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}
        // 2013-07-19 comment by Henry for useless
        //public SqlDataReader GetNoticePrefixList(int autIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("NoticePrefixsList", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the datareader result
        //    return result;
        //}

        // 2013-07-19 comment by Henry for useless
        //public DataSet GetNoticePrefixListDS(int autIntNo)
        //{
        //    SqlDataAdapter sqlDANoticePrefixs = new SqlDataAdapter();
        //    DataSet dsNoticePrefixs = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlDANoticePrefixs.SelectCommand = new SqlCommand();
        //    sqlDANoticePrefixs.SelectCommand.Connection = new SqlConnection(mConstr);			
        //    sqlDANoticePrefixs.SelectCommand.CommandText = "NoticePrefixList";

        //    // Mark the Command as a SPROC
        //    sqlDANoticePrefixs.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    sqlDANoticePrefixs.SelectCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command and close the connection
        //    sqlDANoticePrefixs.Fill(dsNoticePrefixs);
        //    sqlDANoticePrefixs.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsNoticePrefixs;		
        //}

		public int UpdateNoticePrefix(int ntIntNo, int autIntNo, string nPrefix, string lastUser, int npIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("NoticePrefixUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterNTIntNo = new SqlParameter("@NTIntNo", SqlDbType.Int, 4);
			parameterNTIntNo.Value = ntIntNo;
			myCommand.Parameters.Add(parameterNTIntNo);

			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			myCommand.Parameters.Add(parameterAutIntNo);

            // dls 070508 - Stellenbosch is using 1001 - NoticePrefix has been extended from a char(2) to a varchar(4)
			SqlParameter parameterNPrefix = new SqlParameter("@NPrefix", SqlDbType.VarChar, 4);
			parameterNPrefix.Value = nPrefix;
			myCommand.Parameters.Add(parameterNPrefix);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterNPIntNo = new SqlParameter("@NPIntNo", SqlDbType.Int, 4);
			parameterNPIntNo.Value = npIntNo;
			parameterNPIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterNPIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				npIntNo = (int)myCommand.Parameters["@NPIntNo"].Value;

				return npIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		public String DeleteNoticePrefix (int tnIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("NoticePrefixDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterTNIntNo = new SqlParameter("@TNIntNo", SqlDbType.Int, 4);
			parameterTNIntNo.Value = tnIntNo;
			parameterTNIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterTNIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int userId = (int)parameterTNIntNo.Value;

				return userId.ToString();
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return String.Empty;
			}
		}
	}
}
