using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a DateRule from the database
    /// </summary>
    public partial class DateRulesDetails
    {
        public Int32 DtRIntNo;
        public Int32 AutIntNo;
        public string DtRStartDate;
        public string DtREndDate;
        public string DtRDescr;
        public Int32 DtRNoOfDays;
        public string LastUser;
    }
    class DateRuleInfo
    {
        public int DRID;
        public string DRName;
        public string DtRDescr;
        public int AutIntNo;
        public int ADRNoOfDays;

    }
    /// <summary>
    /// Contains all the logic for interacting with DateRules from the database
    /// </summary>
    public class DateRulesDB
    {
        // Fields
        private string mConstr;

        /// <summary>
        /// Initializes a new instance of the <see cref="DateRulesDB"/> class.
        /// </summary>
        /// <param name="vConstr">The database connection string.</param>
        public DateRulesDB(string vConstr)
        {
            this.mConstr = vConstr;
        }

        //*******************************************************
        //
        // The GetDateRulesDetails method returns a DateRulesDetails
        // struct that contains information about a specific transaction number
        //
        //*******************************************************
        public DateRulesDetails GetDateRulesDetails(int dtrIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("DateRulesDetail", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            com.Parameters.Add("@DtRIntNo", SqlDbType.Int, 4).Value = dtrIntNo;

            con.Open();
            SqlDataReader result = com.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            DateRulesDetails myDateRulesDetails = new DateRulesDetails();

            if (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myDateRulesDetails.DtRIntNo = Convert.ToInt32(result["DtRIntNo"]);
                myDateRulesDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
                myDateRulesDetails.DtRStartDate = result["DtRStartDate"].ToString();
                myDateRulesDetails.DtRNoOfDays = Convert.ToInt32(result["DtRNoOfDays"]);
                myDateRulesDetails.DtREndDate = result["DtREndDate"].ToString();
                myDateRulesDetails.DtRDescr = result["DtRDescr"].ToString();
                myDateRulesDetails.LastUser = result["LastUser"].ToString();
            }
            result.Close();

            return myDateRulesDetails;
        }

        //*******************************************************
        //
        // The AddDateRules method inserts a new transaction number record
        // into the DateRules database.  A unique "DtRIntNo"
        // key is then returned from the method.  
        //
        //*******************************************************
        public int AddDateRules(int autIntNo, string dtrStartDate, string dtrEndDate, int dtrNoOfDays, string dtrDescr, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("DateRulesAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterDtRStartDate = new SqlParameter("@DtRStartDate", SqlDbType.VarChar, 50);
            parameterDtRStartDate.Value = dtrStartDate;
            myCommand.Parameters.Add(parameterDtRStartDate);

            SqlParameter parameterDtREndDate = new SqlParameter("@DtREndDate", SqlDbType.VarChar, 50);
            parameterDtREndDate.Value = dtrEndDate;
            myCommand.Parameters.Add(parameterDtREndDate);

            SqlParameter parameterDtRNoOfDays = new SqlParameter("@DtRNoOfDays", SqlDbType.Int);
            parameterDtRNoOfDays.Value = dtrNoOfDays;
            myCommand.Parameters.Add(parameterDtRNoOfDays);

            SqlParameter parameterDtRDescr = new SqlParameter("@DtRDescr", SqlDbType.VarChar, 255);
            parameterDtRDescr.Value = dtrDescr;
            myCommand.Parameters.Add(parameterDtRDescr);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterDtRIntNo = new SqlParameter("@DtRIntNo", SqlDbType.Int, 4);
            parameterDtRIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterDtRIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();

                return Convert.ToInt32(parameterDtRIntNo.Value);
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        /// <summary>
        /// Copies the date rules from one Authority to another.
        /// </summary>
        /// <param name="fromAutIntNo">From aut int no.</param>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        public int CopyDateRules(int fromAutIntNo, int autIntNo, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("DateRulesCopy", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFromAutIntNo = new SqlParameter("@FromAutIntNo", SqlDbType.Int, 4);
            parameterFromAutIntNo.Value = fromAutIntNo;
            myCommand.Parameters.Add(parameterFromAutIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            parameterAutIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterAutIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();

                autIntNo = Convert.ToInt32(parameterAutIntNo.Value);

                return autIntNo;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        /// <summary>
        /// Gets the list of date rules for an Authority.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        //public SqlDataReader GetDateRulesList(int autIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(mConstr);
        //    SqlCommand com = new SqlCommand("DateRulesList", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to the SP
        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            
        //    // Execute the command
        //    con.Open();
        //    return com.ExecuteReader(CommandBehavior.CloseConnection);
        //}

        //public List<DateRulesDetails> GetDateRulesListByAutCode(string autCode)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("DateRulesList_AutCode", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutCode = new SqlParameter("@AutCode", SqlDbType.Char, 3);
        //    parameterAutCode.Value = autCode;
        //    myCommand.Parameters.Add(parameterAutCode);

        //    try
        //    {
        //        // Execute the command
        //        if (myConnection.State != ConnectionState.Open)
        //            myConnection.Open();
        //        SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
        //        List<DateRulesDetails> ht = new List<DateRulesDetails>();
        //        if (result.HasRows)
        //        {
        //            while (result.Read())
        //            {
        //                DateRulesDetails rule = new DateRulesDetails();
        //                rule.DtRIntNo = Convert.ToInt32(result["DtRIntNo"]);
        //                rule.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
        //                rule.DtRStartDate = result["DtRStartDate"].ToString();
        //                rule.DtRNoOfDays = Convert.ToInt32(result["DtRNoOfDays"]);
        //                rule.DtREndDate = result["DtREndDate"].ToString();
        //                rule.DtRDescr = result["DtRDescr"].ToString();
        //                rule.LastUser = result["LastUser"].ToString();
        //                ht.Add(rule);
        //            }
        //        }
        //        return ht;
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    finally
        //    {
        //        myConnection.Dispose();
        //    }
        //}

        /// <summary>
        /// Gets the list of date rules for an authority as a DataSet.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        public DataSet GetDateRulesListDS(int autIntNo, int pageSize, int pageIndex,string searchString, out int totalCount)
        {
            SqlDataAdapter sqlDADateRuless = new SqlDataAdapter();
            DataSet dsDateRuless = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDADateRuless.SelectCommand = new SqlCommand();
            sqlDADateRuless.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDADateRuless.SelectCommand.CommandText = "DateRule_GetByAutIntNo";

            // Mark the Command as a SPROC
            sqlDADateRuless.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDADateRuless.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterSearchString = new SqlParameter("@SearchText", SqlDbType.NVarChar, 250);
            parameterSearchString.Value = searchString;
            sqlDADateRuless.SelectCommand.Parameters.Add(parameterSearchString);

            SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int, 4);
            parameterPageSize.Value = pageSize;
            sqlDADateRuless.SelectCommand.Parameters.Add(parameterPageSize);

            SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int, 4);
            parameterPageIndex.Value = pageIndex;
            sqlDADateRuless.SelectCommand.Parameters.Add(parameterPageIndex);

            SqlParameter parameterTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int, 4);
            parameterTotalCount.Direction = ParameterDirection.Output;
            sqlDADateRuless.SelectCommand.Parameters.Add(parameterTotalCount);

            // Execute the command and close the connection
            sqlDADateRuless.Fill(dsDateRuless);
            sqlDADateRuless.SelectCommand.Connection.Dispose();

            totalCount = (int)(parameterTotalCount.Value == DBNull.Value ? 0 : parameterTotalCount.Value);

            // Return the dataset result
            return dsDateRuless;
        }

        public int UpdateDateRules(int autIntNo, string dtrStartDate, string dtrEndDate, int dtrNoOFDays, string dtrDescr, string lastUser, int dtrIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("DateRulesUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterDtRStartDate = new SqlParameter("@DtRStartDate", SqlDbType.VarChar, 50);
            parameterDtRStartDate.Value = dtrStartDate;
            myCommand.Parameters.Add(parameterDtRStartDate);

            SqlParameter parameterDtREndDate = new SqlParameter("@DtREndDate", SqlDbType.VarChar, 50);
            parameterDtREndDate.Value = dtrEndDate;
            myCommand.Parameters.Add(parameterDtREndDate);

            SqlParameter parameterDtRNoOfDays = new SqlParameter("@DtRNoOfDays", SqlDbType.Int);
            parameterDtRNoOfDays.Value = dtrNoOFDays;
            myCommand.Parameters.Add(parameterDtRNoOfDays);

            SqlParameter parameterDtRDescr = new SqlParameter("@DtRDescr", SqlDbType.VarChar, 255);
            parameterDtRDescr.Value = dtrDescr;
            myCommand.Parameters.Add(parameterDtRDescr);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterDtRIntNo = new SqlParameter("@DtRIntNo", SqlDbType.Int, 4);
            parameterDtRIntNo.Value = dtrIntNo;
            parameterDtRIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterDtRIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int DtRIntNo = (int)myCommand.Parameters["@DtRIntNo"].Value;

                return DtRIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        //public String DeleteDateRules(int dtrIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("DateRulesDelete", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterDtRIntNo = new SqlParameter("@DtRIntNo", SqlDbType.Int, 4);
        //    parameterDtRIntNo.Value = dtrIntNo;
        //    parameterDtRIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterDtRIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int userId = (int)parameterDtRIntNo.Value;

        //        return userId.ToString();
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return String.Empty;
        //    }
        //}

        /// <summary>
        /// Gets a Date Rule for an Authority, creating a default if it does not exist
        /// </summary>
        /// <param name="rule">The rule.</param>
        /// <returns>A <see cref="DateRulesDetails"/></returns>
        //public DateRulesDetails GetDefaultDateRule(DateRulesDetails rule)
        //{
        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand cmd = new SqlCommand("CreateDefaultDateRule", con);
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    cmd.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = rule.AutIntNo;
        //    cmd.Parameters.Add("@DtRStartDate", SqlDbType.VarChar, 30).Value = rule.DtRStartDate;
        //    cmd.Parameters.Add("@DtREndDate", SqlDbType.VarChar, 30).Value = rule.DtREndDate;
        //    cmd.Parameters.Add("@DtRNoOfDays", SqlDbType.Int, 4).Value = rule.DtRNoOfDays;
        //    cmd.Parameters.Add("@DtRDescr", SqlDbType.VarChar, 255).Value = rule.DtRDescr;
        //    cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = rule.LastUser;

        //    con.Open();
        //    SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        //    if (reader != null)
        //    {
        //        if (reader.Read())
        //        {
        //            rule.DtRIntNo = (int)reader["DtRIntNo"];
        //            rule.DtRNoOfDays = (int)reader["DtRNoOfDays"];
        //        }
        //        reader.Close();
        //        reader.Dispose();
        //    }
        //    cmd.Dispose();

        //    return rule;
        //}
        public DateRulesDetails GetDefaultDateRule(DateRulesDetails rule)
        {
            DateRuleInfo drInfo = null;
            drInfo = GetDateRuleInfoByDRNameAutIntNo(rule.AutIntNo, rule.DtRStartDate, rule.DtREndDate);

            if (drInfo != null)
            {
                rule.DtRNoOfDays = drInfo.ADRNoOfDays;
            }
            else
            {
                #region Jerry 2012-05-28 change
                //DateRule drEntity = new DateRule();
                //drEntity = new DateRuleService().GetByDrName("DR_" + rule.DtRStartDate + "_" + rule.DtREndDate);

                //#region Jerry 2012-05-28 change
                ////if (drEntity != null)
                ////{
                ////    drEntity.DrName = "DR_" + rule.DtRStartDate + "_" + rule.DtREndDate;
                ////    drEntity.DtRdescr = rule.DtRDescr;
                ////    drEntity = new DateRuleService().Save(drEntity);

                ////    DateRuleLookup drLookup = new DateRuleLookup();
                ////    drLookup.Drid = drEntity.Drid;
                ////    drLookup.DtRdescr = drEntity.DtRdescr;
                ////    drLookup = new DateRuleLookupService().Save(drLookup);
                ////}
                //#endregion

                //if (drEntity != null)
                //{
                //    AuthorityDateRule adrEntity = new AuthorityDateRule();
                //    adrEntity.Drid = drEntity.Drid;
                //    adrEntity.AutIntNo = rule.AutIntNo;
                //    adrEntity.AdrNoOfDays = rule.DtRNoOfDays;
                //    adrEntity.LastUser = rule.LastUser;
                //    adrEntity = new AuthorityDateRuleService().Save(adrEntity);
                //}
                //else
                //{
                //    return null;
                //}

                //drInfo = GetDateRuleInfoByDRNameAutIntNo(rule.AutIntNo, rule.DtRStartDate, rule.DtREndDate);
                //rule.DtRNoOfDays = drInfo.ADRNoOfDays;
                #endregion
                return null;
            }
            return rule;
        }


        private DateRuleInfo GetDateRuleInfoByDRNameAutIntNo(int autIntNo, string startDate, string endDate)
        {
            // 2014-10-15, Oscar added "using"
            using (IDataReader rd = new DateRuleService().GetByDRNameAutIntNo("DR_" + startDate + "_" + endDate, autIntNo))
            {
                DateRuleInfo dateRule = null;
                if (rd.Read())
                {
                    dateRule = new DateRuleInfo();
                    dateRule.DRName = rd["DRName"].ToString();
                    dateRule.DtRDescr = rd["DtRDescr"].ToString();
                    dateRule.ADRNoOfDays = int.Parse(rd["ADRNoOfDays"].ToString());
                }
                //rd.Close();
                return dateRule;
            }
        }
        //// added lmz to get the specific date rule
        //public int GetSpecificDateRule(int AutIntNo)
        //{
        //    int nRetVal = 30;
        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("GetDateRule", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int).Value = AutIntNo;
        //    con.Open();
        //    SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection);
        //    if (reader.Read())
        //    {
        //        nRetVal = (int)reader["DtrNoOfDays"];
        //    }
        //    reader.Close();

        //    return nRetVal;
        //}

    }
}
