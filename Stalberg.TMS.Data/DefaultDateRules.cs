﻿
using System;
using Stalberg.TMS.Data;
using Stalberg.TMS.Data.Resources;
namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the full list of date rules
    /// </summary>
    public class DefaultDateRules
    {
        // Fields
        private DateRulesDetails rule = new DateRulesDetails();
        private readonly string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultDateRules"/> class.
        /// </summary>
        /// <param name="dateRule">The date rule.</param>
        /// <param name="connectionString">The connection string.</param>
        public DefaultDateRules(DateRulesDetails dateRule, string connectionString)
        {
            this.rule = dateRule;
            this.connectionString = connectionString;
        }

        #region Jerry 2012-05-28 disable
        //public int SetDefaultDateRule()
        //{
        //    if (this.rule.DtRStartDate == "CDate" && this.rule.DtREndDate == "FinalCourtRoll")
        //    {
        //        this.rule.DtRDescr = "Amount in days that the final court roll must be printed";
        //        this.rule.DtRNoOfDays = -3;
        //    }
        //    else if (this.rule.DtRStartDate == "CDate" && this.rule.DtREndDate == "LastAOGDate")
        //    {
        //        this.rule.DtRDescr = "Amount in days that an AOG fine can be paid from court date";
        //        this.rule.DtRNoOfDays = -7;
        //    }
        //    else if (this.rule.DtRStartDate == "CDate" && this.rule.DtREndDate == "SumServeByDate")
        //    {
        //        this.rule.DtRDescr = "Amount in days that a summons must be served by from the court date";
        //        this.rule.DtRNoOfDays = -30;
        //    }
        //    else if (this.rule.DtRStartDate == "EasyPayDate" && this.rule.DtREndDate == "NotPaymentDate")
        //    {
        //        this.rule.DtRDescr = "No. Days Before NOTICE PAYMENT DATE that the EasyPay transaction expires";
        //        this.rule.DtRNoOfDays = 2;
        //    }
        //    else if (this.rule.DtRStartDate == "NotIssue2ndNoticeDate" && this.rule.DtREndDate == "Not2ndPaymentDate")
        //    {
        //        this.rule.DtRDescr = "Min no. of days from 2nd Notice Issued to 2nd Payment Date";
        //        this.rule.DtRNoOfDays = 30;
        //    }
        //    else if (this.rule.DtRStartDate == "NotIssueSummonsDate" && this.rule.DtREndDate == "CDate")
        //    {
        //        this.rule.DtRDescr = "Min no. of days from SUMMONS ISSUE DATE to COURT DATE";
        //        this.rule.DtRNoOfDays = 30;
        //    }
        //    else if (this.rule.DtRStartDate == "NotOffenceDate" && this.rule.DtREndDate == "CRCourtDate")
        //    {
        //        this.rule.DtRDescr = "Max. no days before COURT DATE that payment can be received.";
        //        this.rule.DtRNoOfDays = 7;
        //    }
        //    else if (this.rule.DtRStartDate == "NotOffenceDate" && this.rule.DtREndDate == "NotExpireDate")
        //    {
        //        this.rule.DtRDescr = "Amount in days before a notice expires (Default = 548 days = 18 months)";
        //        this.rule.DtRNoOfDays = 548;
        //    }
        //    else if (this.rule.DtRStartDate == "NotOffenceDate" && this.rule.DtREndDate == "NotIssue1stNoticeDate")
        //    {
        //        this.rule.DtRDescr = "Max no. of days from OFFENCE DATE to FIRST NOTICE ISSUE DATE (incl. posted date)";
        //        this.rule.DtRNoOfDays = 35;
        //    }
        //    else if (this.rule.DtRStartDate == "NotOffenceDate" && this.rule.DtREndDate == "NotPaymentDate")
        //    {
        //        this.rule.DtRDescr = "Max no. of days from OFFENCE DATE to PAYMENT DATE";
        //        this.rule.DtRNoOfDays = 60;
        //    }
        //    //dls 2010-07-08 - this is needed for the system overview report
        //    else if (this.rule.DtRStartDate == "NotOffenceDate" && this.rule.DtREndDate == "NotPosted1stNoticeDate")
        //    {
        //        this.rule.DtRDescr = "Max no. of days from OFFENCE DATE to  FIRST NOTICE POST DATE";
        //        this.rule.DtRNoOfDays = 30;
        //    }
        //    else if (this.rule.DtRStartDate == "NotOffenceDate" && this.rule.DtREndDate == "NotSummonsBeforeDate")
        //    {
        //        this.rule.DtRDescr = "Amount in days before a notice cannot be summonsed before its expiry date (Default = 548 days = 18 months)";
        //        this.rule.DtRNoOfDays = 548;
        //    }
        //    else if (this.rule.DtRStartDate == "NotPosted1stNoticeDate" && this.rule.DtREndDate == "NotIssue2ndNoticeDate")
        //    {
        //        this.rule.DtRDescr = "Max no. of days from POSTED 1ST NOTICE DATE to ISSUE 2ND NOTICE DATE";
        //        this.rule.DtRNoOfDays = 30;
        //    }
        //    else if (this.rule.DtRStartDate == "NotPosted1stNoticeDate" && this.rule.DtREndDate == "NotIssueSummonsDate")
        //    {
        //        this.rule.DtRDescr = "Min no. of days from FIRST NOTICE POST DATE to SUMMONS ISSUE DATE";
        //        this.rule.DtRNoOfDays = 90;
        //    }
        //    //dls duplicated
        //    //else if (this.rule.DtRStartDate == "NotPosted1stNoticeDate" && this.rule.DtREndDate == "NotIssueSummonsDate")
        //    //{
        //    //    this.rule.DtRDescr = "Min no. of days from FIRST NOTICE POST DATE to SUMMONS ISSUE DATE	";
        //    //    this.rule.DtRNoOfDays = 90;
        //    //}

        //    //Oscar 20120413 added 
        //    else if (CheckStartAndEndDate("NotOffenceDate", "NotIssueSummonsDate"))
        //    {
        //        this.rule.DtRDescr = "Min no. of days from FIRST NOTICE POST DATE to SUMMONS ISSUE DATE";
        //        this.rule.DtRNoOfDays = 90;
        //    }

        //    else if (this.rule.DtRStartDate == "SumCourtDate" && this.rule.DtREndDate == "WOALoadDate")
        //    {
        //        this.rule.DtRDescr = "Min no. of days from COURT DATE to WOA LOAD DATE";
        //        this.rule.DtRNoOfDays = 7;
        //    }
        //    else if (this.rule.DtRStartDate == "SumIssueDate" && this.rule.DtREndDate == "SumExpiredDate")
        //    {
        //        this.rule.DtRDescr = "Amount in days to expire a summons from issue date";
        //        this.rule.DtRNoOfDays = 548;
        //    }
        //    else if (this.rule.DtRStartDate == "RoadBlockDate" && this.rule.DtREndDate == "SumCourtDate")
        //    {
        //        this.rule.DtRDescr = "Number of days between a roadblock summons and COURT DATE";
        //        this.rule.DtRNoOfDays = 10;
        //    }
        //    else if (this.rule.DtRStartDate == "WOAIssueDate" && this.rule.DtREndDate == "WOAExpireDate")
        //    {
        //        this.rule.DtRDescr = "Amount in days before a WOA expires (Default = 1825 days = 5 years)";
        //        this.rule.DtRNoOfDays = 1825;
        //    }
        //    else if (this.rule.DtRStartDate == "WOALoadDate" && this.rule.DtREndDate == "WOAPrintDate")
        //    {
        //        rule.DtRDescr = "Amount in days before a WOA print (Default = 14 days)";
        //        rule.DtRNoOfDays = 14;
        //    }
        //    else if (this.rule.DtRStartDate == "DataWashDate" && this.rule.DtREndDate == "NotIssue2ndNoticeDate")
        //    {
        //        rule.DtRDescr = "Amount in days before 2nd notice issued (Default = 14 days)";
        //        rule.DtRNoOfDays = 14;
        //    }
        //    else if (this.rule.DtRStartDate == "DataWashDate" && this.rule.DtREndDate == "NotIssueSummonDate")
        //    {
        //        rule.DtRDescr = "Amount in days before summon issued (Default = 14 days)";
        //        rule.DtRNoOfDays = 14;
        //    }
        //    else if (this.rule.DtRStartDate == "" && this.rule.DtREndDate == "")
        //    {
        //        rule.DtRDescr = "";
        //        rule.DtRNoOfDays = 0;
        //    }

        //    DateRulesDB dr = new DateRulesDB(connectionString);

        //    rule = dr.GetDefaultDateRule(rule);

        //    return rule.DtRNoOfDays;
        //}
        #endregion

        public int SetDefaultDateRule()
        {
            DateRulesDB dr = new DateRulesDB(connectionString);

            string ruleCode = "DR_" + this.rule.DtRStartDate + "_" + this.rule.DtREndDate;
            rule = dr.GetDefaultDateRule(rule);

            if (this.rule == null)
            {
                throw new Exception(string.Format(RuleErrorRe.MissingDateRule, ruleCode));
            }

            return rule.DtRNoOfDays;
        }

        private bool CheckStartAndEndDate(string startDate, string endDate)
        {
            if (!this.rule.DtRStartDate.Equals(startDate, System.StringComparison.OrdinalIgnoreCase))
                return false;
            if (!this.rule.DtREndDate.Equals(endDate, System.StringComparison.OrdinalIgnoreCase))
                return false;
            return true;
        }
    }
}
