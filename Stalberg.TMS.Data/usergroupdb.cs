using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


namespace Stalberg.TMS
{

    //*******************************************************
    //
    // UserGroupDetails Class
    //
    // A simple data class that encapsulates details about a particular user 
    // // Updated Jake 20090428 added field RoadblockUser
    //*******************************************************

    public partial class UserGroupDetails 
	{
		public Int32 MenuIntNo;
        public string UGName;
		public string UGComment;
        public string RoadblockUser;

    }

    //*******************************************************
    //
    // UserGroupDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query UserGroups within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class UserGroupDB {

		string mConstr = "";

		public UserGroupDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // UserGroupDB.GetUserGroupDetails() Method <a name="GetUserGroupDetails"></a>
        //
        // The GetUserGroupDetails method returns a UserGroupDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        // Updated Jake 20090428 new field RoadBlockUser
        //*******************************************************

        public UserGroupDetails GetUserGroupDetails(int ugIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("UserGroupDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterUGIntNo = new SqlParameter("@UGIntNo", SqlDbType.Int, 4);
            parameterUGIntNo.Value = ugIntNo;
            myCommand.Parameters.Add(parameterUGIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			UserGroupDetails myUserGroupDetails = new UserGroupDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myUserGroupDetails.MenuIntNo = Convert.ToInt32(result["MenuIntNo"]);
				myUserGroupDetails.UGName = result["UGName"].ToString();
				myUserGroupDetails.UGComment = result["UGComment"].ToString();
                myUserGroupDetails.RoadblockUser = result["RoadblockUser"].ToString();
			}

			result.Close();
            return myUserGroupDetails;
        }

        //*******************************************************
        //
        // UserGroupsDB.AddUser() Method <a name="AddUser"></a>
        //
        // The AddUser method inserts a new user record
        // into the userGroups database.  A unique "UserId"
        // key is then returned from the method.  
        //// Updated Jake 20090428 added field roadBlockUser for roadblock
        //*******************************************************

        public int AddUserGroup
            (int menuIntNo, string ugName, string ugComment, string roadBlockUser) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("UserGroupAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
			SqlParameter parameterMenuIntNo = new SqlParameter("@MenuIntNo", SqlDbType.Int,4);
			parameterMenuIntNo.Value = menuIntNo;
			myCommand.Parameters.Add(parameterMenuIntNo);

			SqlParameter parameterUGName = new SqlParameter("@UGName", SqlDbType.VarChar, 50);
			parameterUGName.Value = ugName;
			myCommand.Parameters.Add(parameterUGName);

			SqlParameter parameterUGComment = new SqlParameter("@UGComment", SqlDbType.Text);
			parameterUGComment.Value = ugComment;
			myCommand.Parameters.Add(parameterUGComment);

            SqlParameter parameterRoadblockUser = new SqlParameter("@RoadblockUser", SqlDbType.VarChar, 50);
            parameterRoadblockUser.Value = roadBlockUser;
            myCommand.Parameters.Add(parameterRoadblockUser);

			SqlParameter parameterUGIntNo = new SqlParameter("@UGIntNo", SqlDbType.Int, 4);
            parameterUGIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterUGIntNo);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int addUGIntNo = Convert.ToInt32(parameterUGIntNo.Value);

                return addUGIntNo;
            }
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
            
        }

 
		public SqlDataReader GetUserGroupList()
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("UserGroupList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		public DataSet GetUserGroupListDS()
		{
			SqlDataAdapter sqlDAUserGroups = new SqlDataAdapter();
			DataSet dsUserGroups = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAUserGroups.SelectCommand = new SqlCommand();
			sqlDAUserGroups.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAUserGroups.SelectCommand.CommandText = "UserGroupList";

			// Mark the Command as a SPROC
			sqlDAUserGroups.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Execute the command and close the connection
			sqlDAUserGroups.Fill(dsUserGroups);
			sqlDAUserGroups.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsUserGroups;		
		}

        /// Updated Jake 20090428 added field roadBlockUser
		public int UpdateUserGroup

			//password only updated via change password
            (int ugIntNo, int menuIntNo, string ugName, string ugComment, string roadBlockUser) 
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("UserGroupUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterMenuIntNo = new SqlParameter("@MenuIntNo", SqlDbType.Int,4);
			parameterMenuIntNo.Value = menuIntNo;
			myCommand.Parameters.Add(parameterMenuIntNo);

			SqlParameter parameterUGName = new SqlParameter("@UGName", SqlDbType.VarChar, 50);
			parameterUGName.Value = ugName;
			myCommand.Parameters.Add(parameterUGName);

			SqlParameter parameterUGComment = new SqlParameter("@UGComment", SqlDbType.Text);
			parameterUGComment.Value = ugComment;
			myCommand.Parameters.Add(parameterUGComment);

            SqlParameter parameterRoadblockUser = new SqlParameter("@RoadblockUser", SqlDbType.VarChar, 50);
            parameterRoadblockUser.Value = roadBlockUser;
            myCommand.Parameters.Add(parameterRoadblockUser);

			SqlParameter parameterUGIntNo = new SqlParameter("@UGIntNo", SqlDbType.Int, 4);
			parameterUGIntNo.Direction = ParameterDirection.InputOutput;
			parameterUGIntNo.Value = ugIntNo;
			myCommand.Parameters.Add(parameterUGIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int updUGIntNo = (int)myCommand.Parameters["@UGIntNo"].Value;

				return updUGIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		public int DeleteUserGroup (int ugIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("UserGroupDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterUGIntNo = new SqlParameter("@UGIntNo", SqlDbType.Int, 4);
			parameterUGIntNo.Value = ugIntNo;
			parameterUGIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterUGIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int delUGIntNo = (int)parameterUGIntNo.Value;

				return delUGIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}
	}
}

