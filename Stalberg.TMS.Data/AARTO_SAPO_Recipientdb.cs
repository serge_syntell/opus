﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Stalberg.TMS.Data
{
    public class AARTO_SAPO_Recipient
    {
        // Fields
        private int id;
        private int reportTypeID;
        private string function;
        private string name;
        private string email;
        private string reportTypeCode;

        /// <summary>
        /// Gets or sets the object's database ID.
        /// </summary>
        /// <value>The ID.</value>
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Gets or sets the ReportType.
        /// </summary>
        /// <value>The ReportType.</value>
        public int ReportTypeID
        {
            get { return reportTypeID; }
            set { reportTypeID = value; }
        }

        /// <summary>
        /// Gets or sets the Function.
        /// </summary>
        /// <value>The Function.</value>
        public string Function
        {
            get { return function; }
            set { function = value; }
        }

        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        /// <value>The Name.</value>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Gets or sets the Email.
        /// </summary>
        /// <value>The Email.</value>
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        /// <summary>
        /// Gets or sets the reportTypeCode.
        /// </summary>
        /// <value>The reportTypeCode.</value>
        public string ReportTypeCode
        {
            get { return reportTypeCode; }
            set { reportTypeCode = value; }
        }
    }

    public class AARTO_SAPO_RecipientDB
    {
        // Fields
        private SqlConnection con;

        public AARTO_SAPO_RecipientDB(string connectionString)
        {
            this.con = new SqlConnection(connectionString);
        }

        public List<AARTO_SAPO_Recipient> ListAARTO_SAPO_Recipients()
        {
            List<AARTO_SAPO_Recipient> list = new List<AARTO_SAPO_Recipient>();
            AARTO_SAPO_Recipient temp;

            SqlCommand cmd = new SqlCommand("AARTO_SAPO_RecipientList", this.con);
            try
            {
                this.con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    temp = new AARTO_SAPO_Recipient();
                    temp.ReportTypeID = Convert.ToInt32(reader["ARTIntNo"]);
                    temp.ID = Convert.ToInt32(reader["ASRIntNo"]);
                    temp.Function = Convert.ToString(reader["ASRFunction"]);
                    temp.Name = Convert.ToString(reader["ASRName"]);
                    temp.Email = Convert.ToString(reader["ASREmail"]);
                    temp.ReportTypeCode = Convert.ToString(reader["ARTReportCode"]);
                    list.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                list = null;
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.con.Close();
            }

            return list;
        }

        public List<AARTO_SAPO_Recipient> ListAARTO_SAPO_Recipients(string strReportTypeCode)
        {
            List<AARTO_SAPO_Recipient> list = new List<AARTO_SAPO_Recipient>();
            AARTO_SAPO_Recipient temp;

            SqlCommand cmd = new SqlCommand("AARTO_SAPO_RecipientList_byReportType", this.con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@RptCode", SqlDbType.VarChar, 10).Value = strReportTypeCode;
            try
            {
                this.con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    temp = new AARTO_SAPO_Recipient();
                    temp.ReportTypeID = Convert.ToInt32(reader["ARTIntNo"]);
                    temp.ID = Convert.ToInt32(reader["ASRIntNo"]);
                    temp.Function = Convert.ToString(reader["ASRFunction"]);
                    temp.Name = Convert.ToString(reader["ASRName"]);
                    temp.Email = Convert.ToString(reader["ASREmail"]);
                    temp.ReportTypeCode = Convert.ToString(reader["ARTReportCode"]);
                    list.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                list = null;
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.con.Close();
            }

            return list;
        }

        public void UpdateAARTO_SAPO_Recipient(AARTO_SAPO_Recipient ASR, string lastUser)
        {
            SqlCommand cmd = new SqlCommand("AARTO_SAPO_RecipientUpdate", this.con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ASRIntNo", SqlDbType.Int).Value = ASR.ID;
            cmd.Parameters.Add("@ARTIntNo", SqlDbType.Int).Value = ASR.ReportTypeID;
            cmd.Parameters.Add("@ASRFunction", SqlDbType.VarChar, 50).Value = ASR.Function;
            cmd.Parameters.Add("@ASRName", SqlDbType.VarChar, 100).Value = ASR.Name;
            cmd.Parameters.Add("@ASREmail", SqlDbType.VarChar, 100).Value = ASR.Email;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                this.con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.con.Close();
            }
        }

        public int InsertAARTO_SAPO_Recipient(AARTO_SAPO_Recipient ASR, string lastUser)
        {
            SqlCommand cmd = new SqlCommand("AARTO_SAPO_RecipientAdd", this.con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ARTIntNo", SqlDbType.Int).Value = ASR.ReportTypeID;
            cmd.Parameters.Add("@ASRFunction", SqlDbType.VarChar, 50).Value = ASR.Function;
            cmd.Parameters.Add("@ASRName", SqlDbType.VarChar, 100).Value = ASR.Name;
            cmd.Parameters.Add("@ASREmail", SqlDbType.VarChar, 100).Value = ASR.Email;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                this.con.Open();
                int id = (int)cmd.ExecuteScalar();
                ASR.ID = id;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.con.Close();
            }
            return ASR.ID;
        }

        public void DeleteAARTO_SAPO_Recipient(int id)
        {
            SqlCommand cmd = new SqlCommand("AARTO_SAPO_RecipientDelete", this.con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ASRIntNo", SqlDbType.Int).Value = id;
            
            try
            {
                this.con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.con.Close();
            }
        }
    }
}
