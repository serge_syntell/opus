﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Stalberg.TMS.Data
{

    public class AlphabetNumericMappingDB
    {
        string mConstr = "";
        public AlphabetNumericMappingDB(string connStr)
        {
            this.mConstr = connStr;
        }

        public Dictionary<string, int> GetAllAlphabetNumericMapping()
        {
            Dictionary<string, int> dict = new Dictionary<string, int>();

            //AlphabetNumericMappingList

            SqlConnection conn = new SqlConnection(this.mConstr);
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "AlphabetNumericMappingList";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            try
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                using (IDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        dict.Add(reader["Alphabet"].ToString(), Convert.ToInt32(reader["Numeric"]));
                    }
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                //conn.Close();
                // 2014-10-15, Oscar changed
                cmd.Dispose();
                conn.Dispose();
            }

            return dict;
        }
        public bool CheckIsMobileType(int ntIntNo)
        {
            bool flag = false;

            SqlConnection conn = new SqlConnection(this.mConstr);
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "CheckIsMobileType";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@ntIntNo", ntIntNo));

            try
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();

              int reValue=(int)cmd.ExecuteScalar();
              if (reValue == 1)
                  flag = true;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return flag;
        }
    }
}
