using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // PostReturnTypeDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class PostReturnTypeDetails
    {
        public int PRTypeCode;
        public string PRTypeDescr;
    }

    //*******************************************************
    //
    // PostReturnTypeDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query PostReturnTypes within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class PostReturnTypeDB
    {

        string mConstr = "";

        public PostReturnTypeDB(string vConstr)
        {
            mConstr = vConstr;
        }

        //*******************************************************
        //
        // PostReturnTypeDB.GetPostReturnTypeDetails() Method <a name="GetPostReturnTypeDetails"></a>
        //
        // The GetPostReturnTypeDetails method returns a PostReturnTypeDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public PostReturnTypeDetails GetPostReturnTypeDetails(int prTIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("PostReturnTypeDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterPRTIntNo = new SqlParameter("@PRTIntNo", SqlDbType.Int, 4);
            parameterPRTIntNo.Value = prTIntNo;
            myCommand.Parameters.Add(parameterPRTIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            PostReturnTypeDetails myPostReturnTypeDetails = new PostReturnTypeDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myPostReturnTypeDetails.PRTypeCode = Convert.ToInt32(result["PRTypeCode"]);
                myPostReturnTypeDetails.PRTypeDescr = result["PRTDescr"].ToString();
            }
            result.Close();
            return myPostReturnTypeDetails;
        }

        //*******************************************************
        //
        // PostReturnTypeDB.AddPostReturnType() Method <a name="AddPostReturnType"></a>
        //
        // The AddPostReturnType method inserts a new menu record
        // into the menus database.  A unique "AddPostReturnTypeID"
        // key is then returned from the method.  
        //
        //*******************************************************
        // 2013-07-25 add parameter lastUser by Henry
        public int AddPostReturnType(int prTypeCode, string prTypeDescr, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("PostReturnTypeAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterPRTypeCode = new SqlParameter("@PRTypeCode", SqlDbType.Int, 4);
            parameterPRTypeCode.Value = prTypeCode;
            myCommand.Parameters.Add(parameterPRTypeCode);

            SqlParameter parameterPRTypeDescr = new SqlParameter("@PRTypeDescr", SqlDbType.VarChar, 30);
            parameterPRTypeDescr.Value = prTypeDescr;
            myCommand.Parameters.Add(parameterPRTypeDescr);

            SqlParameter parameterPRTIntNo = new SqlParameter("@PRTIntNo", SqlDbType.Int, 4);
            parameterPRTIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterPRTIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int prTIntNo = Convert.ToInt32(parameterPRTIntNo.Value);

                return prTIntNo;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e.Message);
                myConnection.Dispose();
                return 0;
            }
        }

        public SqlDataReader GetPostReturnTypeList()
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("PostReturnTypeList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }


        public DataSet GetPostReturnTypeListDS(bool bIn)
        {
            SqlDataAdapter sqlDARoadTypes = new SqlDataAdapter();
            DataSet dsPostReturnTypes = new DataSet();
            string sIn = (bIn == true ? "Y" : "N");

            // Create Instance of Connection and Command Object
            sqlDARoadTypes.SelectCommand = new SqlCommand();
            sqlDARoadTypes.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDARoadTypes.SelectCommand.CommandText = "PostReturnTypeList";

            // Mark the Command as a SPROC
            sqlDARoadTypes.SelectCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter prmIn = new SqlParameter("@SortOrder", SqlDbType.VarChar, 1);
            prmIn.Direction = ParameterDirection.Input;
            prmIn.Value = sIn;
            sqlDARoadTypes.SelectCommand.Parameters.Add(prmIn);

            // Execute the command and close the connection
            sqlDARoadTypes.Fill(dsPostReturnTypes);
            sqlDARoadTypes.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsPostReturnTypes;
        }

        // 2013-07-25 add parameter lastUser by Henry
        public int UpdatePostReturnType(int prTIntNo, int prTypeCode, string prTypeDescr, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("PostReturnTypeUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterPRTypeCode = new SqlParameter("@PRTypeCode", SqlDbType.Int, 4);
            parameterPRTypeCode.Value = prTypeCode;
            myCommand.Parameters.Add(parameterPRTypeCode);

            SqlParameter parameterPRTypeDescr = new SqlParameter("@PRTypeDescr", SqlDbType.VarChar, 30);
            parameterPRTypeDescr.Value = prTypeDescr;
            myCommand.Parameters.Add(parameterPRTypeDescr);

            SqlParameter parameterPRTIntNo = new SqlParameter("@PRTIntNo", SqlDbType.Int);
            parameterPRTIntNo.Direction = ParameterDirection.InputOutput;
            parameterPRTIntNo.Value = prTIntNo;
            myCommand.Parameters.Add(parameterPRTIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int PRTIntNo = (int)myCommand.Parameters["@PRTIntNo"].Value;
                
                return PRTIntNo;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error " + e.Message);
                myConnection.Dispose();
                return 0;
            }
        }


        public String DeletePostReturnType(int prTIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("PostReturnTypeDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterPRTIntNo = new SqlParameter("@PRTIntNo", SqlDbType.Int, 4);
            parameterPRTIntNo.Value = prTIntNo;
            parameterPRTIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterPRTIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int PRTIntNo = (int)parameterPRTIntNo.Value;

                return PRTIntNo.ToString();
            }
            catch
            {
                myConnection.Dispose();
                return String.Empty;
            }
        }
    }
}


