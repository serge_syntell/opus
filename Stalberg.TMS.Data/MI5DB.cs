using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the details of an entry in the MI5 tables
    /// </summary>
    public class Persona
    {
        public string Surname = string.Empty;
        public string Initials = string.Empty;
        public string FirstName = string.Empty;
        public string IDType = string.Empty;
        public string IDNumber = string.Empty;
        public string PostalAddress1 = string.Empty;
        public string PostalAddress2 = string.Empty;
        public string PostalAddress3 = string.Empty;
        public string PostalAddress4 = string.Empty;
        public string PostalAddress5 = string.Empty;
        public string PostalCode = string.Empty;
        public string StreetAddress1 = string.Empty;
        public string StreetAddress2 = string.Empty;
        public string StreetAddress3 = string.Empty;
        public string StreetAddress4 = string.Empty;
        public string StreetCode = string.Empty;
        public string Source=string.Empty;
    }

    /// <summary>
    /// Contains all the code for interacting with the MI5 tables in the database
    /// </summary>
    public class MI5DB
    {
        // Fields
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="MI5DB"/> class.
        /// </summary>
        public MI5DB(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Updates the MI5 Persona details.
        /// </summary>
        /// <param name="persona">The persona.</param>
        /// <param name="lastUser">The last user.</param>
        public void UpdatePersona(Persona persona, string lastUser)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("MI5_PersonaModify", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@MIPSurname", SqlDbType.VarChar, 100).Value = persona.Surname;
            com.Parameters.Add("@MIPInitials", SqlDbType.VarChar, 10).Value = persona.Initials;
            com.Parameters.Add("@MIPForeNames", SqlDbType.VarChar, 30).Value = persona.FirstName;
            com.Parameters.Add("@MIPIDType", SqlDbType.VarChar, 3).Value = persona.IDType;
            com.Parameters.Add("@MIPIDNumber", SqlDbType.VarChar, 25).Value = persona.IDNumber;
            com.Parameters.Add("@MIPProxyIDNumber", SqlDbType.VarChar, 25).Value = string.Empty;
            com.Parameters.Add("@MIPProxySurname", SqlDbType.VarChar, 100).Value = string.Empty;
            com.Parameters.Add("@MIPProxyInitials", SqlDbType.VarChar, 10).Value = string.Empty;
            com.Parameters.Add("@MIPProxyForeNames", SqlDbType.VarChar, 30).Value = string.Empty;
            com.Parameters.Add("@MIPASource", SqlDbType.VarChar, 10).Value = persona.Source;
            com.Parameters.Add("@MIPAPoAdd1", SqlDbType.VarChar, 100).Value = persona.PostalAddress1;
            com.Parameters.Add("@MIPAPoAdd2", SqlDbType.VarChar, 100).Value = persona.PostalAddress2;
            com.Parameters.Add("@MIPAPoAdd3", SqlDbType.VarChar, 100).Value = persona.PostalAddress3;
            com.Parameters.Add("@MIPAPoAdd4", SqlDbType.VarChar, 100).Value = persona.PostalAddress4;
            com.Parameters.Add("@MIPAPoAdd5", SqlDbType.VarChar, 100).Value = persona.PostalAddress5;
            com.Parameters.Add("@MIPAPoCode", SqlDbType.VarChar, 10).Value = persona.PostalCode;
            com.Parameters.Add("@MIPAStAdd1", SqlDbType.VarChar, 100).Value = persona.StreetAddress1;
            com.Parameters.Add("@MIPAStAdd2", SqlDbType.VarChar, 100).Value = persona.StreetAddress2;
            com.Parameters.Add("@MIPAStAdd3", SqlDbType.VarChar, 100).Value = persona.StreetAddress3;
            com.Parameters.Add("@MIPAStAdd4", SqlDbType.VarChar, 100).Value = persona.StreetAddress4;
            com.Parameters.Add("@MIPAStCode", SqlDbType.VarChar, 10).Value = persona.StreetCode;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            catch (Exception ex)//Jerry 2014-05-12 add catch
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Update the MI5 Persona last washed date to null.
        /// </summary>
        public void UpdatePersonaLastWashedDate(string sIDNumber, string lastUser)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("MI5_PersonaModifyLastWashedDate", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@MIPIDNumber", SqlDbType.VarChar, 25).Value = sIDNumber;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }

        public void DeletePersona(string sIDNumber, string lastUser)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("MI5_PersonaDelete", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@IDNumber", SqlDbType.VarChar, 25).Value = sIDNumber;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Gets the blocked address list.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public DataSet GetBlockedAddressList(int autIntNo, DateTime dt)
        {
            int totalCount = 0;
            return GetBlockedAddressList(autIntNo, dt, 
             10, 1, out totalCount);
        }
        public DataSet GetBlockedAddressList(int autIntNo, DateTime dt, 
            int pageSize, int pageIndex, out int totalCount)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("NoticeBlockedAddressesByAuthority", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@Date", SqlDbType.DateTime, 8).Value = dt;

            com.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            com.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            com.Parameters.Add(paraTotalCount);

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            return ds;
        }

        /// <summary>
        /// Get persona details by mipIDNumber
        /// </summary>
        /// <param name="idNumber">mipIDNumber</param>
        /// <returns></returns>
        public DataSet GetPersonaDetailsByID(string idNumber, string lastUser)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("GetPersonaDetailsByID_WS", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@MIPIDNumber", SqlDbType.VarChar, 25).Value = idNumber;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        /// <summary>
        /// Creates a new MI5 Persona from a driver's details.
        /// </summary>
        /// <param name="driver">The driver.</param>
        /// <returns>A <see cref="Persona"/>.</returns>
        public Persona PersonaFromDriver(DriverDetails driver)
        {
            Persona persona = new Persona();
            persona.FirstName = driver.DrvForeNames;
            persona.IDType = driver.DrvIDType;
            persona.IDNumber = driver.DrvIDNumber;
            persona.Initials = driver.DrvInitials;
            persona.PostalAddress1 = driver.DrvPOAdd1;
            persona.PostalAddress2 = driver.DrvPOAdd2;
            persona.PostalAddress3 = driver.DrvPOAdd3;
            persona.PostalAddress4 = driver.DrvPOAdd4;
            persona.PostalAddress5 = driver.DrvPOAdd5;
            persona.PostalCode = driver.DrvPOCode;
            persona.StreetAddress1 = driver.DrvStAdd1;
            persona.StreetAddress2 = driver.DrvStAdd2;
            persona.StreetAddress3 = driver.DrvStAdd3;
            persona.StreetAddress4 = driver.DrvStAdd4;
            persona.StreetCode = driver.DrvStCode;
            persona.Surname = driver.DrvSurname;
            return persona;
        }
    }
}