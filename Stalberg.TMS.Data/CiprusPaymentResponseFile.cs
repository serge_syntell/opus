using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Stalberg.TMS.Data
{
    /// <summary>
    /// Represents a response file from Ciprus to a payment request
    /// </summary>
    public class CiprusPaymentResponseFile
    {
        // Fields
        private bool isValid;
        private DateTime startDate;
        private DateTime endDate;
        private List<Record> records;
        private string errorMessage = string.Empty;

        private static Regex regex = new Regex(@"^[\s\d]{5}\.\d{2}$|^\d{7}$", RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);

        /// <summary>
        /// Initializes a new instance of the <see cref="CiprusPaymentResponseFile"/> class.
        /// </summary>
        /// <param name="contents">The contents.</param>
        public CiprusPaymentResponseFile(string contents)
        {
            this.isValid = true;
            this.records = new List<Record>();

            try
            {
                string[] lines = contents.Split(new char[] { '\n' });
                int recordType = 0;
                int pos = 0;

                string line;
                for (int i = 0; i < lines.Length; i++)
                {
                    line = lines[i].Trim();

                    if (line.Length == 0)
                        continue;

                    pos = line.IndexOf(',');
                    recordType = int.Parse(line.Substring(0, pos).Trim());

                    if (recordType == 81)
                    {
                        this.ReadHeader(line);
                        continue;
                    }

                    if (recordType == 89)
                    {
                        this.ReadFooter(line);
                        continue;
                    }

                    this.ReadRecord(line);
                }
            }
            catch (Exception ex)
            {
                this.isValid = false;
                this.errorMessage = ex.Message;
            }
        }

        private void ReadRecord(string line)
        {
            string[] split = line.Split(new char[] { ',' });

            Record record = new Record();
            record.Type = int.Parse(split[0].Trim());
            record.Version = int.Parse(split[1].Trim());
            record.NoticeNo = split[2].Trim();
            record.PaymentDate = this.ParseDate(split[3].Trim());
            record.Amount = this.ParseAmount(split[4].Trim(), false);
            record.Amount2 = this.ParseAmount(split[5].Trim(), true);

            //dls 090108 - need to convert the Civitas Payment Type into Opus CashType
            //record.PaymentType = (CashType)(int)int.Parse(split[6].Trim()).ToString()[0];
            int cashType = int.Parse(split[6].Trim());

            switch (cashType)
            {
                case 1:
                    record.PaymentType = CashType.CiprusCash;
                    break;
                case 2:
                    record.PaymentType = CashType.CiprusCheque;
                    break;
                case 3:
                    record.PaymentType = CashType.CiprusPostalOrder;
                    break;
                case 4:
                    record.PaymentType = CashType.CiprusCreditCard;
                    break;
                case 5:
                    record.PaymentType = CashType.CiprusOther;
                    break;
                case 6:
                    record.PaymentType = CashType.CiprusInternetPayment;
                    break;
                case 7:
                    record.PaymentType = CashType.CiprusMoneyOrder;
                    break;
                case 8:
                    record.PaymentType = CashType.CiprusDebitCard;
                    break;
                case 9:
                    record.PaymentType = CashType.CiprusForeignCurrency;
                    break;
                case 10:
                    record.PaymentType = CashType.CiprusDishonouredCheque;
                    break;
                case 11:
                    record.PaymentType = CashType.CiprusDirectDeposit;
                    break;
                case 20:
                    record.PaymentType = CashType.CiprusRefundToOffender;
                    break;
                default:
                    record.PaymentType = CashType.CiprusOther;
                    break;
            }

            record.Refund = this.ParseAmount(split[7].Trim(), false);
            record.Refund2 = this.ParseAmount(split[8].Trim(), true);
            //record.RctNo = split[9].Trim();           -- dls 090108 - this is the OffenceDate 

            //dls 090112 - additional fields added
            record.RctNo = split[10].Trim();
            record.PaymentCentreCode = split[11].Trim();
            record.PaymentCentreName = split[12].Trim();
            if (split[13].Trim().Length > 0)
                record.AGNumber = split[13].Trim();

            this.records.Add(record);
        }

        private void ReadFooter(string line)
        {
            string[] split = line.Split(new char[] { ',' });

            //int recordType = int.Parse(split[0].Trim());
            //string version = split[1].Trim();
            int count = int.Parse(split[2].Trim());
            if (this.records.Count + 1 != count)
            {
                this.isValid = false;
                throw new ApplicationException(string.Format("{0} records have been read from the Spot Fine payments file, but there are {1} recorded in the file's control record.", this.records.Count, count));
            }
        }

        private void ReadHeader(string line)
        {
            string[] split = line.Split(new char[] { ',', });

            string recordType = split[0].Trim();
            string versionNo = split[1].Trim();
            this.startDate = this.ParseDate(split[2].Trim());
            this.endDate = this.ParseDate(split[3].Trim());
            string supplierCode = split[4].Trim();
        }

        private decimal ParseAmount(string stringAmount, bool divide)
        {
            Match match = regex.Match(stringAmount);
            if (!match.Success)
            {
                this.isValid = false;
                throw new ApplicationException("The amount in the file is not in a valid format: " + stringAmount);
            }

            decimal value = decimal.Parse(match.Value.Trim());
            if (divide && value != 0)
                value /= 100;
            return value;
        }

        private DateTime ParseDate(string dateString)
        {
            int year = int.Parse(dateString.Substring(0, 4));
            int month = int.Parse(dateString.Substring(4, 2));
            int day = int.Parse(dateString.Substring(6, 2));

            return new DateTime(year, month, day);
        }

        /// <summary>
        /// Gets a value indicating whether this instance is valid.
        /// </summary>
        /// <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
        public bool IsValid
        {
            get { return this.isValid; }
        }

        /// <summary>
        /// Sets the is invalid.
        /// </summary>
        public void SetIsInvalid()
        {
            this.isValid = false;
        }

        /// <summary>
        /// Gets any error message generated by the response file.
        /// </summary>
        /// <value>The error message.</value>
        public string ErrorMessage
        {
            get { return this.errorMessage; }
        }

        /// <summary>
        /// Gets the records that were read from this file.
        /// </summary>
        /// <value>The records.</value>
        public List<Record> Records
        {
            get { return this.records; }
        }


        /// <summary>
        /// Represents a record in a payment response file
        /// </summary>
        public class Record
        {
            /// <summary>
            /// The Ciprus record type number
            /// </summary>
            internal int Type;
            /// <summary>
            /// The specification version that the record confirms to
            /// </summary>
            internal int Version;
            /// <summary>
            /// This is a 7 digit representation of the notice number containing document type and the sequence number
            /// </summary>
            public string NoticeNo;
            /// <summary>
            /// The date that payment was actually made
            /// </summary>
            public DateTime PaymentDate;
            /// <summary>
            /// The amount that was paid
            /// </summary>
            public decimal Amount;
            /// <summary>
            /// A second format for the amount that was paid
            /// </summary>
            internal decimal Amount2;
            /// <summary>
            /// The payment method, e.g Cash
            /// </summary>
            public CashType PaymentType;
            /// <summary>
            /// Any refund amount
            /// </summary>
            public decimal Refund;
            /// <summary>
            /// A second version of the refund amount
            /// </summary>
            public decimal Refund2;
            /// <summary>
            /// The full ticket number
            /// </summary>
            public string TicketNo;
            /// <summary>
            /// The database identity of the notice
            /// </summary>
            public int NotIntNo;
            /// <summary>
            /// The database authority identity value
            /// </summary>
            public int AutIntNo;
            /// <summary>
            /// The Receipt Number
            /// </summary>
            public string RctNo;
            //dls 090109 - added so that we don't have to keep doing this in the proc
            public int BatchSize;
            //dls 090112 - added other fields required
            public string AGNumber;
            public string PaymentCentreName;
            public string PaymentCentreCode;
        }

    }
}
