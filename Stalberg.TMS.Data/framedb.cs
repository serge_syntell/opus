using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using SIL.AARTO.DAL.Services;


namespace Stalberg.TMS
{

    //*******************************************************
    //
    // FrameDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public class NatisDetails
    {
        public int FrameIntNo;
        public int AutIntNo;
        public int FrameStatus;
        public string RegNo;
        public string VMCode;
        public string VTCode;
        public string FrameNatisRegNo;
        public string FrameNatisVMCode;
        public string FrameNatisVTCode;
        public string FrameNatisIndicator;
    }

    //*******************************
    // Update by Jake 04/07/2009 
    // Description : Add new field (NRCPriority)
    //*******************************
    public class NatisReturnCodeDetails
    {
        public int NRCIntNo;
        public string NRCode;
        public string NRCDescr;
        public int NRCPriority;
    }


    /// <summary>
    /// Represents the details of an individual Frame from the database
    /// </summary>
    public class FrameDetails
    {
        public int FrameIntNo;
        public int FilmIntNo;
        public int LocIntNo;
        public int VMIntNo;
        public int VTIntNo;
        public int CamIntNo;
        public int InReIntNo;
        public int TOIntNo;
        public int RdTIntNo;
        public int SzIntNo;
        public int CrtIntNo;
        public int RejIntNo;
        public string FrameNo;
        public string Sequence;
        public string RegNo;
        public DateTime OffenceDate;
        public int FirstSpeed;
        public int SecondSpeed;
        public string ElapsedTime;
        public string TravelDirection;
        public string ManualView;
        public string MultFrames;
        //public string TicketNr; 
        //public string TicketStatus;
        //public DateTime DateUploaded_TMS;
        public DateTime FrameVerifyDateTime;
        public string FrameVerifyUser;
        public string AllowContinue;
        public string ConfirmError;
        public DateTime FrameLoadResponseDate;
        public int FrameLoadResponseStatusCode;
        public string FrameLoadResponseStatusDescr;
        public string FrameNatisRegNo;
        public string FrameNatisVMCode;
        public string FrameNatisVTCode;
        public string FrameNatisIndicator;
        public string FrameNaTISErrorFieldList;
        public string FrameStatus;
        public string FrameProxyFlag;
        public string LocDescr;
        public int SpeedLimit;
        public string OfficerDetails;
        public Int64 RowVersion;
        //public string NRCDescr;     // added by Jake 090412 
        public string NBReason;     // added by Jake 090412

        //FT 100517 For Average speed over distance 
        public int ASD2ndCameraIntNo;
        public DateTime ASDGPSDateTime1;
        public DateTime ASDGPSDateTime2;

        public int ASDTimeDifference;
        public int ASDSectionStartLane;
        public int ASDSectionEndLane;
        public int ASDSectionDistance;

        public int ASD1stCamUnitID;
        public int ASD2ndCamUnitID;
        public int LCSIntNo;
        public string ASDCameraSerialNo1;
        public string ASDCameraSerialNo2;

        public string SectionName;
        public string SectionCode;

        public int LCSCameraIntNo1;
        public int LCSCameraIntNo2;

        public bool LCSActive;

        public string FilmType;

        //Jerry 2013-04-10 add
        public int ? ParentFrameIntNo;
        public DateTime ? FrameVehicleLicenceExpiry;
    }

    //*******************************************************
    //
    // FrameDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query Frames within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class FrameDB
    {

        string mConstr = "";

        public FrameDB(string vConstr)
        {
            mConstr = vConstr;
        }

        /// <summary>
        /// FrameDB.GetFrameDetails() Method <see cref="GetFrameDetails"/>
        ///
        /// The GetFrameDetails method returns a FrameDetails
        /// struct that contains information about a specific
        /// customer (name, password, etc).
        /// Gets the frame details.
        /// Update by Jake 090412 
        /// Description : Added NRCDescr 
        /// </summary>
        /// <param name="frameIntNo">The frame int no.</param>
        /// <returns>A <see cref="FrameDetails object"/></returns>
        public FrameDetails GetFrameDetails(int frameIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("FrameDetail", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@FrameIntNo", SqlDbType.Int, 4).Value = frameIntNo;

            con.Open();
            SqlDataReader result = com.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            FrameDetails myFrameDetails = new FrameDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myFrameDetails.FilmIntNo = Convert.ToInt32(result["FilmIntNo"]);
                myFrameDetails.LocIntNo = Convert.ToInt32(result["LocIntNo"]);
                myFrameDetails.VMIntNo = Convert.ToInt32(result["VMIntNo"]);
                myFrameDetails.VTIntNo = Convert.ToInt32(result["VTIntNo"]);
                myFrameDetails.CamIntNo = Convert.ToInt32(result["CamIntNo"]);
                myFrameDetails.CamIntNo = Convert.ToInt32(result["InReIntNo"] == System.DBNull.Value ? 0 : result["InReIntNo"]);
                myFrameDetails.TOIntNo = Convert.ToInt32(result["TOIntNo"]);
                myFrameDetails.RdTIntNo = Convert.ToInt32(result["RdTIntNo"]);
                myFrameDetails.SzIntNo = Convert.ToInt32(result["SzIntNo"]);
                myFrameDetails.CrtIntNo = Convert.ToInt32(result["CrtIntNo"]);
                myFrameDetails.RejIntNo = Convert.ToInt32(result["RejIntNo"]);
                myFrameDetails.FrameNo = result["FrameNo"].ToString();
                myFrameDetails.RegNo = result["RegNo"].ToString();
                myFrameDetails.Sequence = result["Sequence"].ToString();
                myFrameDetails.OffenceDate = Convert.ToDateTime(result["OffenceDate"]);
                myFrameDetails.FirstSpeed = Convert.ToInt32(result["FirstSpeed"]);
                myFrameDetails.SecondSpeed = Convert.ToInt32(result["SecondSpeed"]);
                myFrameDetails.ElapsedTime = result["ElapsedTime"].ToString();
                myFrameDetails.TravelDirection = result["TravelDirection"].ToString();
                myFrameDetails.ManualView = result["ManualView"].ToString();
                myFrameDetails.MultFrames = result["MultFrames"].ToString();
                myFrameDetails.FrameLoadResponseStatusCode = Convert.ToInt32(result["FrameLoadResponseStatusCode"] == DBNull.Value ? 0 : result["FrameLoadResponseStatusCode"]);
                myFrameDetails.FrameLoadResponseStatusDescr = result["FrameLoadResponseStatusDescr"].ToString();
                if (result["FrameLoadResponseDate"] != System.DBNull.Value)
                    myFrameDetails.FrameLoadResponseDate = Convert.ToDateTime(result["FrameLoadResponseDate"]);
                if (result["FrameVerifyDateTime"] != System.DBNull.Value)
                    myFrameDetails.FrameVerifyDateTime = Convert.ToDateTime(result["FrameVerifyDateTime"]);
                myFrameDetails.FrameVerifyUser = result["FrameVerifyUser"].ToString();
                myFrameDetails.AllowContinue = result["AllowContinue"].ToString();
                myFrameDetails.ConfirmError = result["ConfirmError"].ToString();
                myFrameDetails.FrameNatisRegNo = result["FrameNatisRegNo"].ToString();
                myFrameDetails.FrameNatisVMCode = result["FrameNatisVMCode"].ToString();
                myFrameDetails.FrameNatisVTCode = result["FrameNatisVTCode"].ToString();
                myFrameDetails.FrameNatisIndicator = result["FrameNatisIndicator"].ToString();
                myFrameDetails.FrameNaTISErrorFieldList = result["FrameNaTISErrorFieldList"].ToString();
                myFrameDetails.FrameStatus = result["FrameStatus"].ToString();
                myFrameDetails.FrameProxyFlag = result["FrameProxyFlag"].ToString();
                myFrameDetails.LocDescr = (string)result["LocDescr"];
                myFrameDetails.SpeedLimit = (int)result["SZSpeed"];
                myFrameDetails.OfficerDetails = ((string)result["TOInit"] + ". " + (string)result["TOSName"]).Trim(new char[] { '.', ' ' });
                myFrameDetails.RowVersion = Convert.ToInt64(result["rowversion"]);
                myFrameDetails.InReIntNo = (int)(result["InReIntNo"] == DBNull.Value ? 0 : result["InReIntNo"]); //20130104 added by Nancy for investigate reason

                if (result["ASD2ndCameraIntNo"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(result["ASD2ndCameraIntNo"]) > 0)
                    {
                        myFrameDetails.ASD2ndCameraIntNo = Convert.ToInt32(result["ASD2ndCameraIntNo"]);
                        myFrameDetails.ASDGPSDateTime1 = Convert.ToDateTime(result["ASDGPSDateTime1"]);
                        myFrameDetails.ASDGPSDateTime2 = Convert.ToDateTime(result["ASDGPSDateTime2"]);

                        myFrameDetails.ASDTimeDifference = Convert.ToInt32(result["ASDTimeDifference"]);
                        myFrameDetails.ASDSectionStartLane = Convert.ToInt32(result["ASDSectionStartLane"]);
                        myFrameDetails.ASDSectionEndLane = Convert.ToInt32(result["ASDSectionEndLane"]);
                        myFrameDetails.ASDSectionDistance = Convert.ToInt32(result["ASDSectionDistance"]);

                        myFrameDetails.ASD1stCamUnitID = Convert.ToInt32(result["ASD1stCamUnitID"]);
                        myFrameDetails.ASD2ndCamUnitID = Convert.ToInt32(result["ASD2ndCamUnitID"]);
                        //myFrameDetails.LCSIntNo = Convert.ToInt32(result["LCSIntNo"] == DBNull.Value ? 0 : result["LCSIntNo"]);
                        myFrameDetails.LCSIntNo = Convert.ToInt32(result["LCSIntNo"] == System.DBNull.Value ? 0 : result["LCSIntNo"]);
                        myFrameDetails.ASDCameraSerialNo1 = Convert.ToString(result["ASDCameraSerialNo1"]);
                        myFrameDetails.ASDCameraSerialNo2 = Convert.ToString(result["ASDCameraSerialNo2"]);
                    }
                }

                //myFrameDetails.NRCDescr = result["NRCDescr"].ToString();  //added Jake
                myFrameDetails.NBReason = result["NBReason"].ToString();  //added Jake

                //Jerry 2013-04-10 add
                if (result["ParentFrameIntNo"] != System.DBNull.Value)
                    myFrameDetails.ParentFrameIntNo = Convert.ToInt32(result["ParentFrameIntNo"]);
                else
                    myFrameDetails.ParentFrameIntNo = null;

                if (result["FrameVehicleLicenceExpiry"] != System.DBNull.Value)
                    myFrameDetails.FrameVehicleLicenceExpiry = Convert.ToDateTime(result["FrameVehicleLicenceExpiry"]);
                else
                    myFrameDetails.FrameVehicleLicenceExpiry = null;
            }
            result.Close();
            return myFrameDetails;
        }

        // 2013-07-24 comment out by Henry for useless
        //dls 090702- need to pass in rule re Adj@ 100% so that frames are allowed to go to Adjudication
        //public int UpdateExpiredFrames(int autIntNo, string lastUser, ref string errMsg, ref int success, string adjAt100, int noOfDays, int rejIntNoExpired)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("FrameUpdateExpired", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;
        //    myCommand.CommandTimeout = 0;

        //    // Add Parameters to SPROC

        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterNoOfFrames = new SqlParameter("@NoOfFrames", SqlDbType.Int, 4);
        //    parameterNoOfFrames.Value = 0;
        //    parameterNoOfFrames.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterNoOfFrames);

        //    SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int, 4);
        //    parameterSuccess.Value = 0;
        //    parameterSuccess.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterSuccess);

        //    SqlParameter parameterAdjAt100 = new SqlParameter("@AdjAt100", SqlDbType.Char, 1);
        //    parameterAdjAt100.Value = adjAt100;
        //    myCommand.Parameters.Add(parameterAdjAt100);

        //    SqlParameter parameterNoOfDays = new SqlParameter("@NoOfDays", SqlDbType.Int, 4);
        //    parameterNoOfDays.Value = noOfDays;
        //    myCommand.Parameters.Add(parameterNoOfDays);

        //    SqlParameter parameterRejIntNoExpired = new SqlParameter("@RejIntNoExpired", SqlDbType.Int, 4);
        //    parameterRejIntNoExpired.Value = rejIntNoExpired;
        //    myCommand.Parameters.Add(parameterRejIntNoExpired);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int noOfFrames = (int)myCommand.Parameters["@NoOfFrames"].Value;
        //        success = (int)myCommand.Parameters["@Success"].Value;

        //        return noOfFrames;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        errMsg = e.Message;
        //        return 0;
        //    }
        //}


        /// <summary>
        /// Updated by Jake 04/07/2009
        /// Update NatisReturnCode By NRCIntNo
        /// </summary>
        public NatisReturnCodeDetails GetNRCDetails(string nrCode)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NatisErrorByCode", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNRCode = new SqlParameter("@NRCode", SqlDbType.VarChar, 3);
            parameterNRCode.Value = nrCode;
            myCommand.Parameters.Add(parameterNRCode);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            NatisReturnCodeDetails nrcDetails = new NatisReturnCodeDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                nrcDetails.NRCIntNo = Convert.ToInt32(result["NRCIntNo"]);
                nrcDetails.NRCode = result["NRCode"].ToString();
                nrcDetails.NRCDescr = result["NRCDescr"].ToString();
                nrcDetails.NRCPriority = Convert.ToInt32(result["NRCPrority"]);
            }
            result.Close();
            return nrcDetails;
        }

        //Rejection details in RejectionDB.cs performs the same function - redundant code
        ///// <summary>
        ///// added Jake 04/12/09
        ///// description : get rejection reason 
        ///// </summary>
        ///// <param name="rejIntNo"></param>
        ///// <returns></returns>
        //public string GetRejReasion(int rejIntNo)
        //{
        //    string rejReason = string.Empty;
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("RejseaonGetByIntNo", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC

        //    SqlParameter parameterRejIntNo = new SqlParameter("@RejIntNo", SqlDbType.Int);
        //    parameterRejIntNo.Value = rejIntNo;
        //    myCommand.Parameters.Add(parameterRejIntNo);

        //    try {
        //        using (SqlDataReader result = 
        //            myCommand.ExecuteReader(CommandBehavior.CloseConnection))
        //        {
        //            while (result.Read())
        //            {
        //                rejReason = result["RejReason"].ToString();
        //            }
        //        }
        //        return rejReason;
        //    }
        //    catch (SqlException ex) { throw ex; }

        //}

        /// <summary>
        /// Add by Jake 04/07/2009
        /// Update NatisReturnCode By NRCIntNo
        /// </summary>
        public NatisReturnCodeDetails GetNRCDetailsByIntNo(int nrcIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NatisReturnCodeByRCIntNo", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNRIntNo = new SqlParameter("@NRCIntNo", SqlDbType.Int);
            parameterNRIntNo.Value = nrcIntNo;
            myCommand.Parameters.Add(parameterNRIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            NatisReturnCodeDetails nrcDetails = new NatisReturnCodeDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                nrcDetails.NRCIntNo = Convert.ToInt32(result["NRCIntNo"]);
                nrcDetails.NRCode = result["NRCode"].ToString();
                nrcDetails.NRCDescr = result["NRCDescr"].ToString();
                nrcDetails.NRCPriority = Convert.ToInt32(result["NRCPriority"]);
            }
            result.Close();
            return nrcDetails;
        }

        /// <summary>
        /// Add by Jake 04/07/2009
        /// Update NatisReturnCode By NRCIntNo
        /// </summary>
        /// <param name="nrcIntNo"></param>
        /// <param name="nrcDescr"></param>
        /// <param name="nrcCode"></param>
        /// <param name="lastUser"></param>
        /// <param name="nrcPriority"></param>
        /// <returns></returns>
        public int UpdateNatisReturnCode(int nrcIntNo, string nrcDescr, string nrcCode,
            string lastUser, int nrcPriority)
        {

            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NatisReturnCodeUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC

            SqlParameter parameterNRCode = new SqlParameter("@NRCode", SqlDbType.VarChar, 3);
            parameterNRCode.Value = nrcCode;
            myCommand.Parameters.Add(parameterNRCode);

            SqlParameter parameterNRCDescr = new SqlParameter("@NRCDescr", SqlDbType.VarChar, 255);
            parameterNRCDescr.Value = nrcDescr;
            myCommand.Parameters.Add(parameterNRCDescr);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterNRCPriority = new SqlParameter("@NRCPriority", SqlDbType.TinyInt);
            parameterNRCPriority.Value = nrcPriority;
            myCommand.Parameters.Add(parameterNRCPriority);

            SqlParameter parameterNRCIntNo = new SqlParameter("@NRCIntNo", SqlDbType.Int, 4);
            parameterNRCIntNo.Value = nrcIntNo;
            parameterNRCIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNRCIntNo);

            try
            {

                myConnection.Open();
                myCommand.ExecuteNonQuery();
                nrcIntNo = Convert.ToInt32(parameterNRCIntNo.Value);

                return nrcIntNo;

            }

            catch (SqlException ex) { throw ex; }
            finally { myConnection.Close(); }
        }

        /// <summary>
        /// Add by Jake 04/07/2009
        /// </summary>
        /// <param name="nrcIntNo"></param>
        /// <param name="nrcDescr"></param>
        /// <param name="nrcCode"></param>
        /// <param name="lastUser"></param>
        /// <param name="nrcPriority"></param>
        /// <returns></returns>
        public int AddNatisReturnCode(string nrcDescr, string nrcCode,
            string lastUser, int nrcPriority)
        {
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NatisReturnCodeAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC

            SqlParameter parameterNRCode = new SqlParameter("@NRCode", SqlDbType.VarChar, 3);
            parameterNRCode.Value = nrcCode;
            myCommand.Parameters.Add(parameterNRCode);

            SqlParameter parameterNRCDescr = new SqlParameter("@NRCDescr", SqlDbType.VarChar, 255);
            parameterNRCDescr.Value = nrcDescr;
            myCommand.Parameters.Add(parameterNRCDescr);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterNRCPriority = new SqlParameter("@NRCPriority", SqlDbType.TinyInt);
            parameterNRCPriority.Value = nrcPriority;
            myCommand.Parameters.Add(parameterNRCPriority);

            SqlParameter parameterNRCIntNo = new SqlParameter("@NRCIntNo", SqlDbType.Int, 4);
            parameterNRCIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterNRCIntNo);

            try
            {

                myConnection.Open();
                myCommand.ExecuteNonQuery();
                int nrcIntNo = Convert.ToInt32(parameterNRCIntNo.Value);

                return nrcIntNo;

            }

            catch (SqlException ex) { throw ex; }
            finally { myConnection.Close(); }

        }

        /// <summary>
        /// Add by Jake 04/07/2009
        /// Delete NatisReturnCode By NRCIntNo
        /// </summary>
        /// <param name="nrcIntNo"></param>
        /// <returns></returns>
        public int DeleteNatisReturnCode(int nrcIntNo)
        {
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NatisReturnCodeDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC

            SqlParameter parameterNRCIntNo = new SqlParameter("@NRCIntNo", SqlDbType.Int);
            parameterNRCIntNo.Value = nrcIntNo;
            parameterNRCIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNRCIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();

                nrcIntNo = Convert.ToInt32(parameterNRCIntNo.Value);

                return nrcIntNo;
            }
            catch (SqlException ex)
            { throw ex; }
            finally { myConnection.Close(); }
        }


        /// <summary>
        /// Add by Jake 04/07/2009
        /// Description : Get NatisReturnCodeList
        /// </summary>
        /// <returns></returns>
        public DataSet GetNatisReturnCodeList()
        {
            SqlConnection myConnection = new SqlConnection(mConstr);

            SqlDataAdapter myAdapter = new SqlDataAdapter();
            myAdapter.SelectCommand = new SqlCommand("NatisReturnCodeList", myConnection);
            myAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            DataSet dsNatisRC = new DataSet();
            // Mark the Command as a SPROC
            try
            {
                myConnection.Open();
                myAdapter.Fill(dsNatisRC);

                return dsNatisRC;
            }
            catch (SqlException ex)
            { throw ex; }
            finally { myConnection.Close(); }
        }


        public NatisDetails GetNatisDetails(int FrameIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FrameNatisRowDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Value = FrameIntNo;
            myCommand.Parameters.Add(parameterFrameIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            NatisDetails myNatisDetails = new NatisDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myNatisDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
                myNatisDetails.FrameStatus = Convert.ToInt32(result["FrameStatus"]);
                myNatisDetails.RegNo = result["RegNo"].ToString();
                myNatisDetails.VMCode = result["VMCode"].ToString();
                myNatisDetails.VTCode = result["VTCode"].ToString();
                myNatisDetails.FrameNatisRegNo = result["FrameNatisRegNo"].ToString();
                myNatisDetails.FrameNatisVMCode = result["FrameNatisVMCode"].ToString();
                myNatisDetails.FrameNatisVTCode = result["FrameNatisVTCode"].ToString();
                myNatisDetails.FrameNatisIndicator = result["FrameNatisIndicator"].ToString();
            }
            result.Close();
            return myNatisDetails;
        }

        public FrameDetails GetFrameDetailsForFilm(int filmIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FrameDetailForFilm", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Value = filmIntNo;
            myCommand.Parameters.Add(parameterFilmIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            FrameDetails myFrameDetails = new FrameDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myFrameDetails.FilmIntNo = Convert.ToInt32(result["FilmIntNo"]);
                myFrameDetails.LocIntNo = Convert.ToInt32(result["LocIntNo"]);
                myFrameDetails.VMIntNo = Convert.ToInt32(result["VMIntNo"]);
                myFrameDetails.VTIntNo = Convert.ToInt32(result["VTIntNo"]);
                myFrameDetails.CamIntNo = Convert.ToInt32(result["CamIntNo"]);
                myFrameDetails.TOIntNo = Convert.ToInt32(result["TOIntNo"]);
                myFrameDetails.RdTIntNo = Convert.ToInt32(result["RdTIntNo"]);
                myFrameDetails.SzIntNo = Convert.ToInt32(result["SzIntNo"]);
                myFrameDetails.CrtIntNo = Convert.ToInt32(result["CrtIntNo"]);
                myFrameDetails.RejIntNo = Convert.ToInt32(result["RejIntNo"]);
                myFrameDetails.FrameNo = result["FrameNo"].ToString();
                myFrameDetails.RegNo = result["RegNo"].ToString();
                myFrameDetails.Sequence = result["Sequence"].ToString();
                myFrameDetails.OffenceDate = Convert.ToDateTime(result["OffenceDate"]);
                myFrameDetails.FirstSpeed = Convert.ToInt32(result["FirstSpeed"]);
                myFrameDetails.SecondSpeed = Convert.ToInt32(result["SecondSpeed"]);
                myFrameDetails.ElapsedTime = result["ElapsedTime"].ToString();
                myFrameDetails.TravelDirection = result["TravelDirection"].ToString();
                myFrameDetails.ManualView = result["ManualView"].ToString();
                myFrameDetails.MultFrames = result["MultFrames"].ToString();
                myFrameDetails.FrameLoadResponseStatusCode = Convert.ToInt32(result["FrameLoadResponseStatusCode"]);
                myFrameDetails.FrameLoadResponseStatusDescr = result["FrameLoadResponseStatusDescr"].ToString();
                if (result["FrameLoadResponseDate"] != System.DBNull.Value)
                    myFrameDetails.FrameLoadResponseDate = Convert.ToDateTime(result["FrameLoadResponseDate"]);
                if (result["FrameVerifyDateTime"] != System.DBNull.Value)
                    myFrameDetails.FrameVerifyDateTime = Convert.ToDateTime(result["FrameVerifyDateTime"]);
                myFrameDetails.FrameVerifyUser = result["FrameVerifyUser"].ToString();
                myFrameDetails.AllowContinue = result["AllowContinue"].ToString();
                myFrameDetails.FrameNatisRegNo = result["FrameNatisRegNo"].ToString();
                myFrameDetails.FrameNatisVMCode = result["FrameNatisVMCode"].ToString();
                myFrameDetails.FrameNatisVTCode = result["FrameNatisVTCode"].ToString();
                myFrameDetails.FrameNatisIndicator = result["FrameNatisIndicator"].ToString();
                myFrameDetails.FrameNaTISErrorFieldList = result["FrameNaTISErrorFieldList"].ToString();
                myFrameDetails.FrameStatus = result["FrameStatus"].ToString();
                myFrameDetails.FilmType = (result["FilmType"] == System.DBNull.Value ? "" : result["FilmType"].ToString());

                //FT 100517 For Average speed over distance
                if (result["ASD2ndCameraIntNo"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(result["ASD2ndCameraIntNo"]) > 0)
                    {
                        myFrameDetails.ASD2ndCameraIntNo = Convert.ToInt32(result["ASD2ndCameraIntNo"]);
                        myFrameDetails.ASDGPSDateTime1 = Convert.ToDateTime(result["ASDGPSDateTime1"]);
                        myFrameDetails.ASDGPSDateTime2 = Convert.ToDateTime(result["ASDGPSDateTime2"]);

                        myFrameDetails.ASDTimeDifference = Convert.ToInt32(result["ASDTimeDifference"]);
                        myFrameDetails.ASDSectionStartLane = Convert.ToInt32(result["ASDSectionStartLane"]);
                        myFrameDetails.ASDSectionEndLane = Convert.ToInt32(result["ASDSectionEndLane"]);
                        myFrameDetails.ASDSectionDistance = Convert.ToInt32(result["ASDSectionDistance"]);

                        myFrameDetails.ASD1stCamUnitID = Convert.ToInt32(result["ASD1stCamUnitID"]);
                        myFrameDetails.ASD2ndCamUnitID = Convert.ToInt32(result["ASD2ndCamUnitID"]);
                        //by seawen 2013-04-25
                        myFrameDetails.LCSIntNo = (result["LCSIntNo"] == System.DBNull.Value ? 0 : Convert.ToInt32(result["LCSIntNo"]));
                        myFrameDetails.ASDCameraSerialNo1 = Convert.ToString(result["ASDCameraSerialNo1"]);
                        myFrameDetails.ASDCameraSerialNo2 = Convert.ToString(result["ASDCameraSerialNo2"]);

                        myFrameDetails.SectionName = Convert.ToString(result["LCSSectionName"]);
                        myFrameDetails.SectionCode = Convert.ToString(result["LCSSectionCode"]);

                        myFrameDetails.LCSCameraIntNo1 = (result["CameraIntNo1"] == System.DBNull.Value ? 0 : Convert.ToInt32(result["CameraIntNo1"]));
                        myFrameDetails.LCSCameraIntNo2 = (result["CameraIntNo2"] == System.DBNull.Value ? 0 : Convert.ToInt32(result["CameraIntNo2"]));

                        myFrameDetails.LCSActive = Convert.ToBoolean(result["LCSActive"]);
                    }
                }
            }
            result.Close();
            return myFrameDetails;
        }

        public string NatisFieldDescr(string fieldCode)
        {
            string descr = "";
            switch (fieldCode)
            {
                case "001":
                    descr = "returned as information acknowledgement";
                    break;
                case "002":
                    descr = "The field number(s) (if applicable) that is(are) causing trouble";
                    break;
                case "003":
                    descr = "Vehicle licence number";
                    break;
                case "004":
                    descr = "Vehicle usage";
                    break;
                case "005":
                    descr = "Date of change of motor vehicle registration qualification";
                    break;
                case "006":
                    descr = "Motor vehicle registration qualification";
                    break;
                case "007":
                    descr = "Motor vehicle state change date";
                    break;
                case "008":
                    descr = "Motor vehicle state";
                    break;
                case "009":
                    descr = "Date of change of ownership";
                    break;
                case "010":
                    descr = "Vehicle description";
                    break;
                case "011":
                    descr = "Vehicle category";
                    break;
                case "012":
                    descr = "Clearance certificate number";
                    break;
                case "013":
                    descr = "Registering authority where vehicle is licensed";
                    break;
                case "014":
                    descr = "Person's identification type";
                    break;
                case "015":
                    descr = "Person's identification number";
                    break;
                case "016":
                    descr = "Name of person";
                    break;
                case "017":
                    descr = "Person's initials";
                    break;
                case "018":
                    descr = "Nature of statutory ownership";
                    break;
                case "019":
                    descr = "Person's postal address line 1";
                    break;
                case "020":
                    descr = "Person's postal address line 2";
                    break;
                case "021":
                    descr = "Person's postal address line 3";
                    break;
                case "022":
                    descr = "Person's postal address line 4";
                    break;
                case "023":
                    descr = "Person's postal address line 5";
                    break;
                case "024":
                    descr = "Person's postal code";
                    break;
                case "025":
                    descr = "Person's street address line 1";
                    break;
                case "026":
                    descr = "Person's street address line 2";
                    break;
                case "027":
                    descr = "Person's street address line 3";
                    break;
                case "028":
                    descr = "Person's street address line 4";
                    break;
                case "029":
                    descr = "Nature of person";
                    break;
                case "030":
                    descr = "Person's street postal code";
                    break;
                case "031":
                    descr = "Identity type of proxy";
                    break;
                case "032":
                    descr = "Name of proxy";
                    break;
                case "033":
                    descr = "Initials of proxy";
                    break;
                case "034":
                    descr = "Vehicle make";
                    break;
                case "035":
                    descr = "Vehicle model name";
                    break;
                case "036":
                    descr = "Previous clearance certificate number";
                    break;
                case "037":
                    descr = "Date of change of address";
                    break;
                case "040":
                    descr = "Model number";
                    break;
                case "041":
                    descr = "Vehicle type";
                    break;
                case "042":
                    descr = "Vehicle usage";
                    break;
                case "043":
                    descr = "Main colour";
                    break;
                case "053":
                    descr = "Identification number of proxy";
                    break;
                case "056":
                    descr = "Nationality/population group";
                    break;
                case "061":
                    descr = "Vehicle register number";
                    break;
                case "062":
                    descr = "Driving licence code";
                    break;
                case "063":
                    descr = "Place where licence was issued";
                    break;
                case "064":
                    descr = "Age";
                    break;
                case "066":
                    descr = "VIN/chassis number";
                    break;
                case "067":
                    descr = "Engine number";
                    break;
                case "068":
                    descr = "Motor vehicle trailer type";
                    break;
                case "069":
                    descr = "Proxy/representative indicator";
                    break;
                case "153":
                    descr = "Vehicle licence expiry date";
                    break;
            }

            return descr;
        }
        //dls 060522 - moved from Notice to Frame level
        //dls 060805 - add filename to stored proc
        public int UpdateFrameFromNATIS(string nfFileName, string f001, string f002, string f003, string f004, DateTime f005, string f006, DateTime f007, string f008, DateTime f009, string f010,
           string f011, string f012, string f013, string f014, string f015, string f016, string f017, string f018, string f019, string f020,
           string f021, string f022, string f023, string f024, string f025, string f026, string f027, string f028, string f029, string f030,
           string f031, string f032, string f033, string f034, string f035, string f036, DateTime f037, string f038, string f040, string f041,
           string f042, string f043, string f053, string f056, string f057, string f058, string f059, string f060, string f061, string f062,
           string f063, string f064, string f066, string f067, string f068, string f069, DateTime f153, int autIntNo,
           int frameIntNo, string outFile, string lastUser, string showDifferencesOnly, ref string errMessage, string natisLast, string allowLookups, out int natisErrorCount, out int returnValue)
        {
            //Heidi 2013-07-09 added for task 4997
            natisErrorCount = 0;

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FrameUpdateFromNATIS", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC

            SqlParameter parameterNatisLast = new SqlParameter("@NatisLast", SqlDbType.Char, 1);
            parameterNatisLast.Value = natisLast;
            myCommand.Parameters.Add(parameterNatisLast);

            SqlParameter parameterNFFileName = new SqlParameter("@NFFileName", SqlDbType.VarChar, 10);
            parameterNFFileName.Value = nfFileName;
            myCommand.Parameters.Add(parameterNFFileName);

            SqlParameter parameterF001 = new SqlParameter("@F001", SqlDbType.Char, 1);
            parameterF001.Value = f001;
            myCommand.Parameters.Add(parameterF001);

            SqlParameter parameterF002 = new SqlParameter("@F002", SqlDbType.VarChar, 255);
            parameterF002.Value = f002;
            myCommand.Parameters.Add(parameterF002);

            SqlParameter parameterF003 = new SqlParameter("@F003", SqlDbType.VarChar, 10);
            parameterF003.Value = f003;
            myCommand.Parameters.Add(parameterF003);

            SqlParameter parameterF004 = new SqlParameter("@F004", SqlDbType.VarChar, 3);
            parameterF004.Value = f004;
            myCommand.Parameters.Add(parameterF004);

            SqlParameter parameterF005 = new SqlParameter("@F005", SqlDbType.SmallDateTime);
            parameterF005.Value = f005;
            myCommand.Parameters.Add(parameterF005);

            SqlParameter parameterF006 = new SqlParameter("@F006", SqlDbType.VarChar, 3);
            parameterF006.Value = f006;
            myCommand.Parameters.Add(parameterF006);

            SqlParameter parameterF007 = new SqlParameter("@F007", SqlDbType.SmallDateTime);
            parameterF007.Value = f007;
            myCommand.Parameters.Add(parameterF007);

            SqlParameter parameterF008 = new SqlParameter("@F008", SqlDbType.VarChar, 3);
            parameterF008.Value = f008;
            myCommand.Parameters.Add(parameterF008);

            SqlParameter parameterF009 = new SqlParameter("@F009", SqlDbType.SmallDateTime);
            parameterF009.Value = f009;
            myCommand.Parameters.Add(parameterF009);

            SqlParameter parameterF010 = new SqlParameter("@F010", SqlDbType.VarChar, 3);
            parameterF010.Value = f010;
            myCommand.Parameters.Add(parameterF010);

            SqlParameter parameterF011 = new SqlParameter("@F011", SqlDbType.VarChar, 3);
            parameterF011.Value = f011;
            myCommand.Parameters.Add(parameterF011);

            SqlParameter parameterF012 = new SqlParameter("@F012", SqlDbType.VarChar, 50);
            parameterF012.Value = f012;
            myCommand.Parameters.Add(parameterF012);

            SqlParameter parameterF013 = new SqlParameter("@F013", SqlDbType.VarChar, 10);
            parameterF013.Value = f013;
            myCommand.Parameters.Add(parameterF013);

            SqlParameter parameterF014 = new SqlParameter("@F014", SqlDbType.VarChar, 3);
            parameterF014.Value = f014;
            myCommand.Parameters.Add(parameterF014);

            SqlParameter parameterF015 = new SqlParameter("@F015", SqlDbType.VarChar, 25);
            parameterF015.Value = f015;
            myCommand.Parameters.Add(parameterF015);

            SqlParameter parameterF016 = new SqlParameter("@F016", SqlDbType.VarChar, 100);
            parameterF016.Value = f016;
            myCommand.Parameters.Add(parameterF016);

            SqlParameter parameterF017 = new SqlParameter("@F017", SqlDbType.VarChar, 10);
            parameterF017.Value = f017;
            myCommand.Parameters.Add(parameterF017);

            SqlParameter parameterF018 = new SqlParameter("@F018", SqlDbType.VarChar, 3);
            parameterF018.Value = f018;
            myCommand.Parameters.Add(parameterF018);

            SqlParameter parameterF019 = new SqlParameter("@F019", SqlDbType.VarChar, 100);
            parameterF019.Value = f019;
            myCommand.Parameters.Add(parameterF019);

            SqlParameter parameterF020 = new SqlParameter("@F020", SqlDbType.VarChar, 100);
            parameterF020.Value = f020;
            myCommand.Parameters.Add(parameterF020);

            SqlParameter parameterF021 = new SqlParameter("@F021", SqlDbType.VarChar, 100);
            parameterF021.Value = f021;
            myCommand.Parameters.Add(parameterF021);

            SqlParameter parameterF022 = new SqlParameter("@F022", SqlDbType.VarChar, 100);
            parameterF022.Value = f022;
            myCommand.Parameters.Add(parameterF022);

            SqlParameter parameterF023 = new SqlParameter("@F023", SqlDbType.VarChar, 100);
            parameterF023.Value = f023;
            myCommand.Parameters.Add(parameterF023);

            SqlParameter parameterF024 = new SqlParameter("@F024", SqlDbType.VarChar, 10);
            parameterF024.Value = f024;
            myCommand.Parameters.Add(parameterF024);

            SqlParameter parameterF025 = new SqlParameter("@F025", SqlDbType.VarChar, 100);
            parameterF025.Value = f025;
            myCommand.Parameters.Add(parameterF025);

            SqlParameter parameterF026 = new SqlParameter("@F026", SqlDbType.VarChar, 100);
            parameterF026.Value = f026;
            myCommand.Parameters.Add(parameterF026);

            SqlParameter parameterF027 = new SqlParameter("@F027", SqlDbType.VarChar, 100);
            parameterF027.Value = f027;
            myCommand.Parameters.Add(parameterF027);

            SqlParameter parameterF028 = new SqlParameter("@F028", SqlDbType.VarChar, 100);
            parameterF028.Value = f028;
            myCommand.Parameters.Add(parameterF028);

            SqlParameter parameterF029 = new SqlParameter("@F029", SqlDbType.VarChar, 3);
            parameterF029.Value = f029;
            myCommand.Parameters.Add(parameterF029);

            SqlParameter parameterF030 = new SqlParameter("@F030", SqlDbType.VarChar, 10);
            parameterF030.Value = f030;
            myCommand.Parameters.Add(parameterF030);

            SqlParameter parameterF031 = new SqlParameter("@F031", SqlDbType.VarChar, 3);
            parameterF031.Value = f031;
            myCommand.Parameters.Add(parameterF031);

            SqlParameter parameterF032 = new SqlParameter("@F032", SqlDbType.VarChar, 100);
            parameterF032.Value = f032;
            myCommand.Parameters.Add(parameterF032);

            SqlParameter parameterF033 = new SqlParameter("@F033", SqlDbType.VarChar, 10);
            parameterF033.Value = f033;
            myCommand.Parameters.Add(parameterF033);

            SqlParameter parameterF034 = new SqlParameter("@F034", SqlDbType.VarChar, 3);
            parameterF034.Value = f034;
            myCommand.Parameters.Add(parameterF034);

            SqlParameter parameterF035 = new SqlParameter("@F035", SqlDbType.VarChar, 10);
            parameterF035.Value = f035;
            myCommand.Parameters.Add(parameterF035);

            SqlParameter parameterF036 = new SqlParameter("@F036", SqlDbType.VarChar, 10);
            parameterF036.Value = f036;
            myCommand.Parameters.Add(parameterF036);

            SqlParameter parameterF037 = new SqlParameter("@F037", SqlDbType.SmallDateTime);
            parameterF037.Value = f037;
            myCommand.Parameters.Add(parameterF037);

            SqlParameter parameterF040 = new SqlParameter("@F040", SqlDbType.VarChar, 10);
            parameterF040.Value = f040;
            myCommand.Parameters.Add(parameterF040);

            SqlParameter parameterF041 = new SqlParameter("@F041", SqlDbType.VarChar, 3);
            parameterF041.Value = f041;
            myCommand.Parameters.Add(parameterF041);

            SqlParameter parameterF042 = new SqlParameter("@F042", SqlDbType.VarChar, 3);
            parameterF042.Value = f042;
            myCommand.Parameters.Add(parameterF042);

            SqlParameter parameterF043 = new SqlParameter("@F043", SqlDbType.VarChar, 3);
            parameterF043.Value = f043;
            myCommand.Parameters.Add(parameterF043);

            SqlParameter parameterF053 = new SqlParameter("@F053", SqlDbType.VarChar, 25);
            parameterF053.Value = f053;
            myCommand.Parameters.Add(parameterF053);

            SqlParameter parameterF056 = new SqlParameter("@F056", SqlDbType.VarChar, 3);
            parameterF056.Value = f056;
            myCommand.Parameters.Add(parameterF056);

            SqlParameter parameterF061 = new SqlParameter("@F061", SqlDbType.VarChar, 10);
            parameterF061.Value = f061;
            myCommand.Parameters.Add(parameterF061);

            SqlParameter parameterF062 = new SqlParameter("@F062", SqlDbType.VarChar, 3);
            parameterF062.Value = f062;
            myCommand.Parameters.Add(parameterF062);

            SqlParameter parameterF063 = new SqlParameter("@F063", SqlDbType.VarChar, 50);
            parameterF063.Value = f063;
            myCommand.Parameters.Add(parameterF063);

            SqlParameter parameterF064 = new SqlParameter("@F064", SqlDbType.VarChar, 3);
            parameterF064.Value = f064;
            myCommand.Parameters.Add(parameterF064);

            SqlParameter parameterF066 = new SqlParameter("@F066", SqlDbType.VarChar, 50);
            parameterF066.Value = f066;
            myCommand.Parameters.Add(parameterF066);

            SqlParameter parameterF067 = new SqlParameter("@F067", SqlDbType.VarChar, 50);
            parameterF067.Value = f067;
            myCommand.Parameters.Add(parameterF067);

            SqlParameter parameterF068 = new SqlParameter("@F068", SqlDbType.VarChar, 3);
            parameterF068.Value = f068;
            myCommand.Parameters.Add(parameterF068);

            SqlParameter parameterF069 = new SqlParameter("@F069", SqlDbType.VarChar, 3);
            parameterF069.Value = f069;
            myCommand.Parameters.Add(parameterF069);

            SqlParameter parameterF153 = new SqlParameter("@F153", SqlDbType.SmallDateTime);
            parameterF153.Value = f153;
            myCommand.Parameters.Add(parameterF153);

            //SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            //parameterAutIntNo.Value = autIntNo;
            //myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterOutFile = new SqlParameter("@OutFile", SqlDbType.VarChar, 25);
            parameterOutFile.Value = outFile;
            myCommand.Parameters.Add(parameterOutFile);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterShowDifferencesOnly = new SqlParameter("@ShowDifferencesOnly", SqlDbType.Char, 1);
            parameterShowDifferencesOnly.Value = showDifferencesOnly;
            myCommand.Parameters.Add(parameterShowDifferencesOnly);

            SqlParameter parameterAllowLookups = new SqlParameter("@AllowLookups", SqlDbType.Char, 1);
            parameterAllowLookups.Value = allowLookups;
            myCommand.Parameters.Add(parameterAllowLookups);

            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Value = frameIntNo;
            parameterFrameIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterFrameIntNo);

            //Heidi 2013-07-09 added for task 4997
            SqlParameter parameterNatisErrorCount = new SqlParameter("@NatisErrorCount", SqlDbType.Int, 4);
            parameterNatisErrorCount.Value = natisErrorCount;
            parameterNatisErrorCount.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNatisErrorCount);

            try
            {
                myConnection.Open();
                //myCommand.ExecuteNonQuery();
                // 2013-12-24, Oscar changed for error handling
                returnValue = Convert.ToInt32(myCommand.ExecuteScalar());

                myConnection.Dispose();

                frameIntNo = (int)myCommand.Parameters["@FrameIntNo"].Value;

                //Heidi 2013-07-09 added for task 4997
                natisErrorCount = (int)myCommand.Parameters["@NatisErrorCount"].Value;

                return frameIntNo;
            }
            catch (Exception e)
            {
                // 2013-12-24, Oscar added for error handling
                returnValue = -999;

                myConnection.Dispose();
                errMessage = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// Update Frame From ENATIS by GUID
        /// </summary>
        /// <param name="strRequestRecord"></param>
        /// <param name="lastUser"></param>
        /// <param name="notIntNo"></param>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        public int UpdateFrameFromENATIS(string ResponseFile, string lastUser, Guid Guid, string ShowDifferencesOnly, string NatisLast, string AllowLookups, ref string errMessage, string F001, string F002, out int natisErrorCount, out int returnValue)
        {
            //Heidi 2013-12-30 added for send back to Dms
            natisErrorCount = 0;
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("FrameUpdateFromENATIS", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ResponseFile", SqlDbType.Xml, ResponseFile.Length).Value = ResponseFile;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@Guid", SqlDbType.UniqueIdentifier).Value = Guid;
            com.Parameters.Add("@ShowDifferencesOnly", SqlDbType.Char, 1).Value = ShowDifferencesOnly;
            com.Parameters.Add("@NatisLast", SqlDbType.Char, 1).Value = NatisLast;
            com.Parameters.Add("@AllowLookups", SqlDbType.Char, 1).Value = AllowLookups;
            com.Parameters.Add("@F001", SqlDbType.VarChar, 1).Value = F001;
            com.Parameters.Add("@F002", SqlDbType.VarChar, 255).Value = F002;

            com.Parameters.Add("@FrameIntNo", SqlDbType.Int, 4).Value = 0;
            com.Parameters["@FrameIntNo"].Direction = ParameterDirection.Output;

            //Heidi 2013-12-30 added for send back to Dms
            com.Parameters.Add("@NatisErrorCount", SqlDbType.Int, 4).Value = 0;
            com.Parameters["@NatisErrorCount"].Direction = ParameterDirection.Output;

            try
            {
                con.Open();
                //com.ExecuteNonQuery();
                // 2013-12-30, Heidi changed for error handling(the same as Frame_Natis_Out)
                returnValue = Convert.ToInt32(com.ExecuteScalar());
                con.Dispose();

                int frameIntNo = (int)com.Parameters["@FrameIntNo"].Value;

                //Heidi 2013-12-30 added for send back to Dms
                natisErrorCount = (int)com.Parameters["@NatisErrorCount"].Value;

                return frameIntNo;
            }
            catch (Exception e)
            {
                // 2013-12-30, Heidi added for error handling
                returnValue = -999;

                System.Diagnostics.Debug.WriteLine(e.Message);
                errMessage = e.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }
        /// <summary>
        /// Update Frame with Person data
        /// </summary>
        /// <param name="strRequestRecord"></param>
        /// <param name="lastUser"></param>
        /// <param name="notIntNo"></param>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        public int UpdateFrameFromENATISPerson(string ResponseFile, string lastUser, Guid Guid, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("FrameUpdateFromENATISPerson", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ResponseFile", SqlDbType.Xml, ResponseFile.Length).Value = ResponseFile;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@Guid", SqlDbType.UniqueIdentifier).Value = Guid;

            com.Parameters.Add("@FrameIntNo", SqlDbType.Int, 4).Value = 0;
            com.Parameters["@FrameIntNo"].Direction = ParameterDirection.Output;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                int frameIntNo = (int)com.Parameters["@FrameIntNo"].Value;
                return frameIntNo;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                errMessage = e.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        // 2013-07-24 comment out by Henry for uselsess
        //public SqlDataReader GetNaTISSendData(int autIntNo, string lastUser, string status)
        //public SqlDataReader GetNaTISSendData(int autIntNo, string lastUser, int status, string showDifferencesOnly,
        //    int noOfRecords, DateTime dtLastRun)
        //{
        //    // Returns a recordset
        //    // Create Instance of Connection and Command Object
        //    // 2006/01/03 no more batch
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("FrameSendNaTIS", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterShowDifferencesOnly = new SqlParameter("@ShowDifferencesOnly", SqlDbType.Char, 1);
        //    parameterShowDifferencesOnly.Value = showDifferencesOnly;
        //    myCommand.Parameters.Add(parameterShowDifferencesOnly);

        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
        //    parameterStatus.Value = status;
        //    myCommand.Parameters.Add(parameterStatus);

        //    SqlParameter parameterNoOFRecords = new SqlParameter("@NoOFRecords", SqlDbType.Int, 4);
        //    parameterNoOFRecords.Value = noOfRecords;
        //    myCommand.Parameters.Add(parameterNoOFRecords);

        //    SqlParameter parameterDtLastRun = new SqlParameter("@DtLastRun", SqlDbType.SmallDateTime);
        //    parameterDtLastRun.Value = dtLastRun;
        //    myCommand.Parameters.Add(parameterDtLastRun);

        //    try
        //    {
        //        myConnection.Open();
        //        SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //        return result;
        //    }
        //    catch (Exception e)
        //    {
        //        string error = e.Message;
        //        return null;
        //    }
        //}


        public SqlDataReader GetNaTISSendDataByFilm(int filmIntNo, int autIntNo, string lastUser, int status, string showDifferencesOnly, int noOfRecords, string sForce)
        {
            // Returns a recordset
            // Create Instance of Connection and Command Object
            // 2006/01/03 no more batch
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FrameSendNaTISByFilmNo", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int);
            parameterFilmIntNo.Value = filmIntNo;
            myCommand.Parameters.Add(parameterFilmIntNo);

            SqlParameter parameterShowDifferencesOnly = new SqlParameter("@ShowDifferencesOnly", SqlDbType.Char, 1);
            parameterShowDifferencesOnly.Value = showDifferencesOnly;
            myCommand.Parameters.Add(parameterShowDifferencesOnly);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
            parameterStatus.Value = status;
            myCommand.Parameters.Add(parameterStatus);

            SqlParameter parameterNoOFRecords = new SqlParameter("@NoOFRecords", SqlDbType.Int, 4);
            parameterNoOFRecords.Value = noOfRecords;
            myCommand.Parameters.Add(parameterNoOFRecords);

            SqlParameter parameterForce = new SqlParameter("@ForceFlag", SqlDbType.Char, 1);
            parameterForce.Value = sForce;
            myCommand.Parameters.Add(parameterForce);

            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception e)
            {
                string error = e.Message;
                return null;
            }
        }

        public int UpdateFrameINP(int status, string FrameSendNatisFileName, string lastUser, int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FrameUpdateNatisINP", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
            parameterStatus.Value = status;
            myCommand.Parameters.Add(parameterStatus);

            SqlParameter parameterFrameSendNatisFileName = new SqlParameter("@FrameSendNatisFileName", SqlDbType.VarChar, 25);
            parameterFrameSendNatisFileName.Value = FrameSendNatisFileName;
            myCommand.Parameters.Add(parameterFrameSendNatisFileName);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            parameterAutIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterAutIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                autIntNo = (int)myCommand.Parameters["@AutIntNo"].Value;

                return autIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int UpdateFrameGuid(int FrameIntNo, int status, string lastUser, Guid eNaTISGuid)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FrameUpdateNatisGuid", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
            parameterStatus.Value = status;
            myCommand.Parameters.Add(parameterStatus);

            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Value = FrameIntNo;
            myCommand.Parameters.Add(parameterFrameIntNo);


            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterGuid = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier);
            parameterGuid.Value = eNaTISGuid;
            myCommand.Parameters.Add(parameterGuid);

            try
            {
                myConnection.Open();
                return myCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        public int FrameStatusChange(int autIntNo, int oldStatus, int newStatus, string lastUser, ref string error)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FrameStatusChange", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterOldStatus = new SqlParameter("@OldStatus", SqlDbType.Int, 4);
            parameterOldStatus.Value = oldStatus;
            myCommand.Parameters.Add(parameterOldStatus);

            SqlParameter parameterNewStatus = new SqlParameter("@NewStatus", SqlDbType.Int, 4);
            parameterNewStatus.Value = newStatus;
            myCommand.Parameters.Add(parameterNewStatus);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            parameterAutIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterAutIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int AutIntNo = (int)myCommand.Parameters["@AutIntNo"].Value;

                return AutIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                error = e.Message;
                return 0;
            }
        }

        public SqlDataReader GetFrameList(int filmIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FrameList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Value = filmIntNo;
            myCommand.Parameters.Add(parameterFilmIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        public SqlDataReader GetFramesDulicateRegNo(int filmIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FrameListDuplicateRegNo", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Value = filmIntNo;
            myCommand.Parameters.Add(parameterFilmIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }
        // 2013-07-19 comment by Henry for useless
        //public int FrameCheckStatusByFilmNo(string filmNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("FrameCheckStatusByFilmNo ", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    // jerry 2012-02-06 change FilmNo length from 10 to 25
        //    SqlParameter parameterFilmNo = new SqlParameter("@FilmNo", SqlDbType.VarChar, 25);
        //    parameterFilmNo.Value = filmNo;
        //    myCommand.Parameters.Add(parameterFilmNo);

        //    SqlParameter parameterCount = new SqlParameter("@Count", SqlDbType.Int, 4);
        //    parameterCount.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterCount);
        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int count = Convert.ToInt32(parameterCount.Value);

        //        return count;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return -1;
        //    }
        //}

        public int CheckFrameExists(int filmIntNo, string frameNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CheckFrameExists", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Value = filmIntNo;
            myCommand.Parameters.Add(parameterFilmIntNo);

            SqlParameter parameterFrameNo = new SqlParameter("@FrameNo", SqlDbType.VarChar, 4);
            parameterFrameNo.Value = frameNo;
            myCommand.Parameters.Add(parameterFrameNo);

            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterFrameIntNo);
            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int FrameIntNo = Convert.ToInt32(parameterFrameIntNo.Value);

                return FrameIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// Frames to adjudicate.
        /// </summary>
        /// <param name="filmIntNo">The film int no.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        public SqlDataReader FrameToAdjudicate(int filmIntNo, Stalberg.TMS.ValidationStatus status)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("FrameToAdjudicate", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@FilmIntNo", SqlDbType.Int, 4).Value = filmIntNo;
            com.Parameters.Add("@Status", SqlDbType.Int, 4).Value = status;

            // Execute the command
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public SqlDataReader FrameToInvestigate(int filmIntNo, Stalberg.TMS.ValidationStatus status)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("FrameToInvestigate", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@FilmIntNo", SqlDbType.Int, 4).Value = filmIntNo;
            com.Parameters.Add("@Status", SqlDbType.Int, 4).Value = (int)status;

            // Execute the command
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        /// <summary>
        /// Gets frames to verify for an LA.
        /// </summary>
        /// <param name="filmIntNo">The film int no.</param>
        /// <param name="authIntNo">The auth int no.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="status">The validation status to use.</param>
        /// <returns>A <see cref="SqlDataReader"/>.</returns>
        public SqlDataReader FrameToVerify(int filmIntNo, int authIntNo, string userName, ValidationStatus status, bool showDiscrepanciesOnly)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = CommandType.StoredProcedure;

            //// FBJ Added (2006-08-03): Check if the DiscrepanciesOnly parameter is set
            //bool discrepanciesOnly = true;
            //com.CommandText = "AuthorityRulesDetailByCode";
            //com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = authIntNo;
            //com.Parameters.Add("@ARCode", SqlDbType.VarChar, 10).Value = "0550";
            //com.Parameters.Add("@ARString", SqlDbType.VarChar, 25).Value = "Y";
            //com.Parameters.Add("@ARNumeric", SqlDbType.Int, 4).Value = 1;
            //com.Parameters.Add("@ARDescr", SqlDbType.VarChar, 100).Value = "Rule to indicate whether to just show NaTIS discrepancy frames or all frames for verification.";
            //com.Parameters.Add("@ARComment", SqlDbType.VarChar, 255).Value = "Y = Yes, only show NaTIS discrepant frames; N = No, shows all frames of the film.";
            //com.Parameters.Add("@LastUser", SqlDbType.VarChar, 10).Value = userName;

            //con.Open();
            //SqlDataReader reader = com.ExecuteReader();
            //if (reader.Read())
            //    discrepanciesOnly = reader.GetString(3).Equals("Y") ? true : false;
            //con.Close();

            // Add Parameters to SPROC
            //com.Parameters.Clear();
            com.CommandText = "FrameToVerify";
            com.Parameters.Add("@FilmIntNo", SqlDbType.Int, 4).Value = filmIntNo;
            com.Parameters.Add("@DiscrepanciesOnly", SqlDbType.Bit, 1).Value = showDiscrepanciesOnly;
            com.Parameters.Add("@Status", SqlDbType.Int, 4).Value = status;

            // Execute the command
            con.Open();
            SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return reader;
        }

        public DataSet GetFrameListDS(int filmIntNo)
        {
            SqlDataAdapter sqlDAFrames = new SqlDataAdapter();
            DataSet dsFrames = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAFrames.SelectCommand = new SqlCommand();
            sqlDAFrames.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAFrames.SelectCommand.CommandText = "FrameList";

            // Mark the Command as a SPROC
            sqlDAFrames.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Value = filmIntNo;
            sqlDAFrames.SelectCommand.Parameters.Add(parameterFilmIntNo);

            // Execute the command and close the connection
            sqlDAFrames.Fill(dsFrames);
            sqlDAFrames.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsFrames;
        }


        public int UpdateAllFramesForFilm(int filmIntNo, int locIntNo, int camIntNo, string proceed, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FrameUpdateAllForFilm", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Value = filmIntNo;
            myCommand.Parameters.Add(parameterFilmIntNo);

            SqlParameter parameterLocIntNo = new SqlParameter("@LocIntNo", SqlDbType.Int, 4);
            parameterLocIntNo.Value = locIntNo;
            myCommand.Parameters.Add(parameterLocIntNo);

            SqlParameter parameterCamIntNo = new SqlParameter("@CamIntNo", SqlDbType.Int, 4);
            parameterCamIntNo.Value = camIntNo;
            myCommand.Parameters.Add(parameterCamIntNo);

            //SqlParameter parameterTOIntNo = new SqlParameter("@TOIntNo", SqlDbType.Int, 4);
            //parameterTOIntNo.Value = toIntNo;
            //myCommand.Parameters.Add(parameterTOIntNo);

            //SqlParameter parameterRdTIntNo = new SqlParameter("@RdTIntNo", SqlDbType.Int, 4);
            //parameterRdTIntNo.Value = rdtIntNo;
            //myCommand.Parameters.Add(parameterRdTIntNo);

            //SqlParameter parameterSZIntNo = new SqlParameter("@SZIntNo", SqlDbType.Int, 4);
            //parameterSZIntNo.Value = szIntNo;
            //myCommand.Parameters.Add(parameterSZIntNo);

            //SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            //parameterCrtIntNo.Value = crtIntNo;
            //myCommand.Parameters.Add(parameterCrtIntNo);

            SqlParameter parameterProceed = new SqlParameter("@Proceed", SqlDbType.Char, 1);
            parameterProceed.Value = proceed;
            myCommand.Parameters.Add(parameterProceed);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int, 4);
            parameterSuccess.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterSuccess);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int success = (int)myCommand.Parameters["@Success"].Value;
                //int menuId = (int)parameterFrameIntNo.Value;

                return success;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                myConnection.Dispose();
                return -2;
            }
        }

        //dls 070517 - no longer updating registration and rejintno in this proc - was completed in previous proc
        //public int AdjudicateFrame(int frameIntNo, int rejIntNo,
        //   string regNo, string confirnViolation, string lastUser, int status, string adjOfficerNo, ref string errMessage)
        public int AdjudicateFrame(int frameIntNo, string confirnViolation, string lastUser, int status,
            string adjOfficerNo, ref string errMessage, string allowContinue, string preUpdatedAllowContinue, Int64 nRowVersion, string ResetRejReason,
            string regNumber)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FrameAdjudicate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            //// Add Parameters to SPROC
            //SqlParameter parameterRejIntNo = new SqlParameter("@RejIntNo", SqlDbType.Int, 4);
            //parameterRejIntNo.Value = rejIntNo;
            //myCommand.Parameters.Add(parameterRejIntNo);

            //SqlParameter parameterRegNo = new SqlParameter("@RegNo", SqlDbType.VarChar, 10);
            //parameterRegNo.Value = regNo;
            //myCommand.Parameters.Add(parameterRegNo);

            SqlParameter parameterConfirmViolation = new SqlParameter("@ConfirmViolation", SqlDbType.Char, 1);
            parameterConfirmViolation.Value = confirnViolation;
            myCommand.Parameters.Add(parameterConfirmViolation);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
            parameterStatus.Value = status;
            myCommand.Parameters.Add(parameterStatus);

            myCommand.Parameters.Add("@AdjOfficerNo", SqlDbType.VarChar, 10).Value = adjOfficerNo;
            myCommand.Parameters.Add("@AllowContinue", SqlDbType.Char, 1).Value = allowContinue;
            myCommand.Parameters.Add("@PreUpdatedAllowContinue", SqlDbType.Char, 1).Value = preUpdatedAllowContinue;
            myCommand.Parameters.Add("@RowVersion", SqlDbType.BigInt, 8).Value = nRowVersion;
            myCommand.Parameters.Add("@ResetRejReason", SqlDbType.Char, 1).Value = ResetRejReason;

            if (!string.IsNullOrEmpty(regNumber))
            {
                myCommand.Parameters.Add("@RegNumber", SqlDbType.NVarChar).Value = regNumber;
            }

            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int);
            parameterFrameIntNo.Direction = ParameterDirection.InputOutput;
            parameterFrameIntNo.Value = frameIntNo;
            myCommand.Parameters.Add(parameterFrameIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int FrameIntNo = (int)myCommand.Parameters["@FrameIntNo"].Value;
                //int menuId = (int)parameterFrameIntNo.Value;

                return FrameIntNo;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                myConnection.Dispose();
                return 0;
            }
        }

        //Henry 120531 for tms old
        public int AdjudicateFrame(int frameIntNo, string confirnViolation, string lastUser, int status,
            string adjOfficerNo, ref string errMessage, string allowContinue, string preUpdatedAllowContinue, Int64 nRowVersion, string ResetRejReason)
        {
            return AdjudicateFrame(frameIntNo,confirnViolation,lastUser,status,
            adjOfficerNo, ref errMessage, allowContinue, preUpdatedAllowContinue,nRowVersion, ResetRejReason, null);
        }


        // dls 070509 - return error for display
        //public int VerifyFrame(int frameIntNo, int vmIntNo, int vtIntNo, int rejIntNo,
        // string regNo, DateTime offenceDate, int firstSpeed, int secondSpeed, string elapsedTime,
        //  string violation, string allowContinue, int status, string lastUser, ref string errMessage,int nRowVersion)

        public int VerifyFrame(int frameIntNo, int vmIntNo, int vtIntNo, int firstSpeed, int secondSpeed, DateTime offenceDate,
            int rejIntNo, string regNo, string violation, string allowContinue, string preUpdatedAllowContinue,
            int status, string lastUser, ref string errMessage, Int64 nRowVersion)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FrameVerify", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterVMIntNo = new SqlParameter("@VMIntNo", SqlDbType.Int, 4);
            parameterVMIntNo.Value = vmIntNo;
            myCommand.Parameters.Add(parameterVMIntNo);

            SqlParameter parameterVTIntNo = new SqlParameter("@VTIntNo", SqlDbType.Int, 4);
            parameterVTIntNo.Value = vtIntNo;
            myCommand.Parameters.Add(parameterVTIntNo);

            SqlParameter parameterRejIntNo = new SqlParameter("@RejIntNo", SqlDbType.Int, 4);
            parameterRejIntNo.Value = rejIntNo;
            myCommand.Parameters.Add(parameterRejIntNo);

            SqlParameter parameterRegNo = new SqlParameter("@RegNo", SqlDbType.VarChar, 10);
            parameterRegNo.Value = regNo;
            myCommand.Parameters.Add(parameterRegNo);

            SqlParameter parameterOffenceDate = new SqlParameter("@OffenceDate", SqlDbType.SmallDateTime);
            parameterOffenceDate.Value = offenceDate;
            myCommand.Parameters.Add(parameterOffenceDate);

            SqlParameter parameterFirstSpeed = new SqlParameter("@FirstSpeed", SqlDbType.Int, 4);
            parameterFirstSpeed.Value = firstSpeed;
            myCommand.Parameters.Add(parameterFirstSpeed);

            SqlParameter parameterSecondSpeed = new SqlParameter("@SecondSpeed", SqlDbType.Int, 4);
            parameterSecondSpeed.Value = secondSpeed;
            myCommand.Parameters.Add(parameterSecondSpeed);

            //SqlParameter parameterElapsedTime = new SqlParameter("@ElapsedTime", SqlDbType.VarChar, 5);
            //parameterElapsedTime.Value = elapsedTime;
            //myCommand.Parameters.Add(parameterElapsedTime);

            SqlParameter parameterConfirmViolation = new SqlParameter("@Violation", SqlDbType.Char, 1);
            parameterConfirmViolation.Value = violation;
            myCommand.Parameters.Add(parameterConfirmViolation);

            SqlParameter parameterAllowContinue = new SqlParameter("@AllowContinue", SqlDbType.Char, 1);
            parameterAllowContinue.Value = allowContinue;
            myCommand.Parameters.Add(parameterAllowContinue);

            SqlParameter parameterPreUpdatedAllowContinue = new SqlParameter("@PreUpdatedAllowContinue", SqlDbType.Char, 1);
            parameterPreUpdatedAllowContinue.Value = preUpdatedAllowContinue;
            myCommand.Parameters.Add(parameterPreUpdatedAllowContinue);

            SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
            parameterStatus.Value = status;
            myCommand.Parameters.Add(parameterStatus);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterRowVersion = new SqlParameter("@RowVersion", SqlDbType.BigInt, 8);
            parameterRowVersion.Value = nRowVersion;
            myCommand.Parameters.Add(parameterRowVersion);

            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int);
            parameterFrameIntNo.Direction = ParameterDirection.InputOutput;
            parameterFrameIntNo.Value = frameIntNo;
            myCommand.Parameters.Add(parameterFrameIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int FrameIntNo = (int)myCommand.Parameters["@FrameIntNo"].Value;

                return FrameIntNo;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                myConnection.Dispose();
                return 0;
            }
        }

        public String DeleteFrame(int frameIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FrameDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Value = frameIntNo;
            parameterFrameIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterFrameIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int FrameIntNo = (int)parameterFrameIntNo.Value;

                return FrameIntNo.ToString();
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return String.Empty;
            }
        }

        /// <summary>
        /// Updates the frame details at adjudication.
        /// </summary>
        /// <param name="frameIntNo">The frame int no.</param>
        /// <param name="vehicleType">Type of the vehicle.</param>
        /// <param name="vehicleMake">The vehicle make.</param>
        /// <param name="registrationNumber">The registration number.</param>
        /// <param name="lastUser">The last user.</param>
        public SqlDataReader UpdateFrameDetailsAtAdjudication(int frameIntNo, int vehicleType, int vehicleMake, string registrationNumber, string lastUser,
            int rejIntNo, ref string errMessage)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("FrameUpdateAtAdjudication", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@FrameIntNo", SqlDbType.Int, 4).Value = frameIntNo;
            com.Parameters["@FrameIntNo"].Direction = ParameterDirection.Input;
            com.Parameters.Add("@RejIntNo", SqlDbType.Int, 4).Value = rejIntNo;
            com.Parameters.Add("@VehTypeIntNo", SqlDbType.Int, 4).Value = vehicleType;
            com.Parameters.Add("@VehMakeIntNo", SqlDbType.Int, 4).Value = vehicleMake;
            com.Parameters.Add("@RegNo", SqlDbType.VarChar, 10).Value = registrationNumber;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection);

                // Return the data reader result
                return reader;

            }
            catch (Exception e)
            {
                errMessage = e.Message;
                con.Dispose();
                return null;
            }
        }

        #region [Image Remove]

        /// <summary>
        /// update FrameImageRemoved value after remove all images by frame
        /// </summary>
        /// <param name="frameIntNo"></param>
        /// <param name="removed"></param>
        /// <returns></returns>
        #endregion

        /// <summary>
        /// 091015 FT GetFramesToRollbackToAdjudicate
        /// </summary>
        /// <param name="frameIntNo"></param>
        /// <param name="vehicleType"></param>
        /// <param name="vehicleMake"></param>
        /// <param name="registrationNumber"></param>
        /// <param name="lastUser"></param>
        /// <param name="rejIntNo"></param>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        public SqlDataReader GetFramesForRollbackToAdjudicate(int frameNo, string filmNo, int minFrameStatus, ref string errMessage)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("FrameForRollbacktoAdjudication", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@FrameNo", SqlDbType.Int, 4).Value = frameNo;
            // jerry 2012-02-06 change FilmNo length from 10 to 25
            com.Parameters.Add("@FilmNo", SqlDbType.VarChar, 25).Value = filmNo;
            com.Parameters.Add("@minFrameStatus", SqlDbType.Int, 4).Value = minFrameStatus;

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection);

                // Return the data reader result
                return reader;

            }
            catch (Exception e)
            {
                errMessage = e.Message;
                con.Dispose();
                return null;
            }
        }

        /// <summary>
        /// RollbackFramesToAdjudicate
        /// Jerry 2014-12-10 add errMsg parameter
        /// </summary>
        /// <param name="FrameIntNo"></param>
        /// <param name="NotIntNo"></param>
        /// <param name="ROWVERSION"></param>
        /// <param name="FrameStatus"></param>
        /// <param name="LastUser"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public int RollbackFramesToAdjudicate(int FrameIntNo, int NotIntNo, Int64 ROWVERSION, int FrameStatus, string LastUser, ref string errMsg)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("FrameStatusRollbacktoAdjudication", con);
            com.CommandType = CommandType.StoredProcedure;
            //Jerry 2015-01-28 add CommandTimeout = 0
            com.CommandTimeout = 0;

            int Success = 0;
            com.Parameters.Add("@FrameIntNo", SqlDbType.Int, 4).Value = FrameIntNo;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = NotIntNo;
            com.Parameters.Add("@ROWVERSION", SqlDbType.BigInt, 8).Value = ROWVERSION;
            com.Parameters.Add("@FrameStatus", SqlDbType.Int, 4).Value = FrameStatus;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = LastUser;
            com.Parameters.Add("@Success", SqlDbType.Int, 4).Value = Success;
            com.Parameters["@Success"].Direction = ParameterDirection.Output;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                // Return the data reader result
                Success = (int)com.Parameters["@Success"].Value;
                return Success;

            }
            catch (Exception e)
            {
                errMsg = e.Message;
                return 0;
            }
            finally
            {
                con.Close();
                con.Dispose();
                com.Dispose();
            }
        }

        #region [Image Remove]
        /// <summary>
        /// Get frame details list for remove image by film
        /// </summary>
        /// <param name="filmIntNo">filme integer number</param>
        /// <param name="fileServerIntNo">Image file server int no</param>
        /// <param name="filmRootPath">film root path end with directory separator char</param>
        /// <returns>List of FrameDetails</returns>
        // 2013-07-19 comment by Henry for useless
        //public List<FrameDetails> GetFrameDetailsListForRomveImage(int filmIntNo, int fileServerIntNo, string filmRootPath)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("FrameListGetDataForRemoveImages", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    myCommand.Parameters.Add("@FilmIntNo", SqlDbType.Int).Value = filmIntNo;
        //    myCommand.Parameters.Add("@RootPath", SqlDbType.NVarChar, 255).Value = filmRootPath;
        //    myCommand.Parameters.Add("@IFSIntNo", SqlDbType.Int).Value = fileServerIntNo;

        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Create CustomerDetails Struct
        //    List<FrameDetails> list = new List<FrameDetails>();

        //    while (result.Read())
        //    {
        //        FrameDetails myFrameDetails = new FrameDetails();

        //        // Populate Struct using Output Params from SPROC
        //        myFrameDetails.FrameIntNo = Convert.ToInt32(result["FrameIntNo"]);
        //        //myFrameDetails.FilmIntNo = Convert.ToInt32(result["FilmIntNo"]);
        //        //myFrameDetails.LocIntNo = Convert.ToInt32(result["LocIntNo"]);
        //        //myFrameDetails.VMIntNo = Convert.ToInt32(result["VMIntNo"]);
        //        //myFrameDetails.VTIntNo = Convert.ToInt32(result["VTIntNo"]);
        //        //myFrameDetails.CamIntNo = Convert.ToInt32(result["CamIntNo"]);
        //        //myFrameDetails.TOIntNo = Convert.ToInt32(result["TOIntNo"]);
        //        //myFrameDetails.RdTIntNo = Convert.ToInt32(result["RdTIntNo"]);
        //        //myFrameDetails.SzIntNo = Convert.ToInt32(result["SzIntNo"]);
        //        //myFrameDetails.CrtIntNo = Convert.ToInt32(result["CrtIntNo"]);
        //        //myFrameDetails.RejIntNo = Convert.ToInt32(result["RejIntNo"]);
        //        myFrameDetails.FrameNo = result["FrameNo"].ToString();
        //        //myFrameDetails.RegNo = result["RegNo"].ToString();
        //        //myFrameDetails.Sequence = result["Sequence"].ToString();
        //        //myFrameDetails.OffenceDate = Convert.ToDateTime(result["OffenceDate"]);
        //        //myFrameDetails.FirstSpeed = Convert.ToInt32(result["FirstSpeed"]);
        //        //myFrameDetails.SecondSpeed = Convert.ToInt32(result["SecondSpeed"]);
        //        //myFrameDetails.ElapsedTime = result["ElapsedTime"].ToString();
        //        //myFrameDetails.TravelDirection = result["TravelDirection"].ToString();
        //        //myFrameDetails.ManualView = result["ManualView"].ToString();
        //        //myFrameDetails.MultFrames = result["MultFrames"].ToString();
        //        //myFrameDetails.FrameLoadResponseStatusCode = Convert.ToInt32(result["FrameLoadResponseStatusCode"]);
        //        //myFrameDetails.FrameLoadResponseStatusDescr = result["FrameLoadResponseStatusDescr"].ToString();
        //        //if (result["FrameLoadResponseDate"] != System.DBNull.Value)
        //        //    myFrameDetails.FrameLoadResponseDate = Convert.ToDateTime(result["FrameLoadResponseDate"]);
        //        //if (result["FrameVerifyDateTime"] != System.DBNull.Value)
        //        //    myFrameDetails.FrameVerifyDateTime = Convert.ToDateTime(result["FrameVerifyDateTime"]);
        //        //myFrameDetails.FrameVerifyUser = result["FrameVerifyUser"].ToString();
        //        //myFrameDetails.AllowContinue = result["AllowContinue"].ToString();
        //        //myFrameDetails.FrameNatisRegNo = result["FrameNatisRegNo"].ToString();
        //        //myFrameDetails.FrameNatisVMCode = result["FrameNatisVMCode"].ToString();
        //        //myFrameDetails.FrameNatisVTCode = result["FrameNatisVTCode"].ToString();
        //        //myFrameDetails.FrameNatisIndicator = result["FrameNatisIndicator"].ToString();
        //        //myFrameDetails.FrameNaTISErrorFieldList = result["FrameNaTISErrorFieldList"].ToString();
        //        //myFrameDetails.FrameStatus = result["FrameStatus"].ToString();

        //        list.Add(myFrameDetails);
        //    }
        //    result.Close();
        //    return list;
        //}

        /// <summary>
        /// update FrameImageRemoved value after remove all images by frame
        /// </summary>
        /// <param name="frameIntNo"></param>
        /// <param name="removed"></param>
        /// <returns></returns>
        // 2013-07-19 comment by Henry for useless
        //public int UpdateFrameRemoved(int frameIntNo, bool removed, out string exceptionMessage)
        //{
        //    exceptionMessage = string.Empty;
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("FrameUpdateRemoved", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    myCommand.Parameters.Add("@FrameIntNo", SqlDbType.Int).Value = frameIntNo;
        //    myCommand.Parameters.Add("@IsFinished", SqlDbType.Bit).Value = removed;

        //    try
        //    {
        //        myConnection.Open();
        //        int result = myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        return result;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        exceptionMessage = e.Message;
        //        return -1;
        //    }
        //}
        #endregion

        public string GetFrameDmsDocumentNoByNoticeNotIntNo(int notIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand cmd = new SqlCommand("Frame_GetDmsDocumentNoByNoticeNotIntNo", myConnection);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter paraNotIntNo = new SqlParameter("@notIntNo", SqlDbType.Int, 4);
            paraNotIntNo.Value = notIntNo;
            cmd.Parameters.Add(paraNotIntNo);

            try
            {

                myConnection.Open();

                string frameDmsDocumentNo = cmd.ExecuteScalar().ToString();
                myConnection.Close();
                return frameDmsDocumentNo;
            }
            catch (Exception ex)
            {
                myConnection.Close();
                myConnection.Dispose();
                return "";
            }

        }

        public string GetViolationType(int frameIntNo)
        {
            string violationType = string.Empty;
            using (SqlConnection conn = new SqlConnection(this.mConstr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand("GetViolationTypeByFrameIntNo", conn) { CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new SqlParameter("@FrameIntNo", frameIntNo) { DbType = DbType.Int32 });
                        if (conn.State != ConnectionState.Open)
                            conn.Open();
                        object value = cmd.ExecuteScalar();
                        if (value != null)
                            violationType = value.ToString().Trim();
                        return violationType;
                    }
                }
                catch (Exception ex)
                {
                    conn.Close();
                    throw ex;
                }
            }
        }

        #region 2014-03-13, Oscar added RegNoHasChanged

        public static void CheckRegNoHasChanged(ref FrameService service, int frameIntNo, string regNo, string lastUser)
        {
            SIL.AARTO.DAL.Entities.Frame frame;

            if (frameIntNo <= 0
                || (frame = (service ?? (service = new FrameService())).GetByFrameIntNo(frameIntNo)) == null
                || string.Equals("0000000000", regNo, StringComparison.OrdinalIgnoreCase)   // 2014-12-01, Oscar added (bontq 1741, ref 1439)
                || string.Equals(frame.RegNo, regNo, StringComparison.OrdinalIgnoreCase)
                || frame.RegNoHasChanged.GetValueOrDefault()
                || (frame.FrameStatus != 50 && frame.FrameStatus != 710 && frame.FrameStatus != 740))
                return;

            frame.RegNoHasChanged = true;
            frame.LastUser = lastUser;
            service.Save(frame);
        }

        /// <summary>
        /// If returns false, skip current frame and process next one.
        /// </summary>
        public static bool CheckChangedRegNoMatched(ref FrameService service, int frameIntNo, string regNo, string fileName, string lastUser)
        {
            SIL.AARTO.DAL.Entities.Frame frame;

            if (frameIntNo <= 0
                || (frame = (service ?? (service = new FrameService())).GetByFrameIntNo(frameIntNo)) == null)
                return false;

            if (frame.RegNoHasChanged.GetValueOrDefault())
            {
                int index;

                var sendFile = frame.FrameSendNatisFileName;
                if (!string.IsNullOrEmpty(sendFile)
                    && (index = sendFile.IndexOf(".INP", StringComparison.OrdinalIgnoreCase)) >= 0)
                    sendFile = sendFile.Substring(0, index);

                if (!string.IsNullOrEmpty(fileName)
                    && (index = fileName.IndexOf(".OUT", StringComparison.OrdinalIgnoreCase)) >= 0)
                    fileName = fileName.Substring(0, index);

                if (!string.Equals(frame.RegNo, regNo, StringComparison.OrdinalIgnoreCase)
                    && !string.Equals(sendFile, fileName, StringComparison.OrdinalIgnoreCase))
                    return false;

                frame.RegNoHasChanged = false;
                frame.LastUser = lastUser;
                service.Save(frame);
            }
            return true;
        }

        #endregion

    }
}


