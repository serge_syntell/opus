using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
	
	public partial class FrameOwnerDetails 
	{
		public int FrOwnIntNo;
		public int FrameIntNo;
		public string FrOwnSurname; 
		public string FrOwnInitials; 
		public string FrOwnIDType; 
		public string FrOwnIDNumber; 
		public string FrOwnNationality;
		public string FrOwnAge;
		public string FrOwnPOAdd1;
		public string FrOwnPOAdd2;
		public string FrOwnPOAdd3;
		public string FrOwnPOAdd4;
		public string FrOwnPOAdd5;
		public string FrOwnPOCode;
		public string FrOwnStAdd1;
		public string FrOwnStAdd2;
		public string FrOwnStAdd3;
		public string FrOwnStAdd4;
		public string FrOwnStCode;
		public DateTime FrOwnDateCOA;
		public string FrOwnLicenceCode;
		public string FrOwnLicencePlace;
		public string LastUser;
	}

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public partial class FrameOwnerDB
	{
		string mConstr = "";

			public FrameOwnerDB (string vConstr)
			{
				mConstr = vConstr;
			}

		//*******************************************************
		//
		// The GetFrameOwnerDetails method returns a FrameOwnerDetails
		// struct that contains information about a specific transaction number
		//
		//*******************************************************

		public FrameOwnerDetails GetFrameOwnerDetails(int FrOwnIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("FrameOwnerDetail", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFrOwnIntNo = new SqlParameter("@FrOwnIntNo", SqlDbType.Int, 4);
			parameterFrOwnIntNo.Value = FrOwnIntNo;
			myCommand.Parameters.Add(parameterFrOwnIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			// Create CustomerDetails Struct
			FrameOwnerDetails myFrameOwnerDetails = new FrameOwnerDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myFrameOwnerDetails.FrameIntNo = Convert.ToInt32(result["FrameIntNo"]);
				myFrameOwnerDetails.FrOwnIntNo = Convert.ToInt32(result["FrOwnIntNo"]);
				myFrameOwnerDetails.FrOwnSurname = result["FrOwnSurname"].ToString(); 
				myFrameOwnerDetails.FrOwnInitials = result["FrOwnInitials"].ToString(); 
				myFrameOwnerDetails.FrOwnIDType = result["FrOwnIDType"].ToString(); 
				myFrameOwnerDetails.FrOwnIDNumber = result["FrOwnIDNumber"].ToString(); 
				myFrameOwnerDetails.FrOwnNationality = result["FrOwnNationality"].ToString();
				myFrameOwnerDetails.FrOwnAge = result["FrOwnAge"].ToString();
				myFrameOwnerDetails.FrOwnPOAdd1 = result["FrOwnPoAdd1"].ToString();
				myFrameOwnerDetails.FrOwnPOAdd2 = result["FrOwnPoAdd2"].ToString();
				myFrameOwnerDetails.FrOwnPOAdd3 = result["FrOwnPoAdd3"].ToString();
				myFrameOwnerDetails.FrOwnPOAdd4 = result["FrOwnPoAdd4"].ToString();
				myFrameOwnerDetails.FrOwnPOAdd5 = result["FrOwnPoAdd5"].ToString();
				myFrameOwnerDetails.FrOwnPOCode = result["FrOwnPoCode"].ToString();
				myFrameOwnerDetails.FrOwnStAdd1 = result["FrOwnStAdd1"].ToString();
				myFrameOwnerDetails.FrOwnStAdd2 = result["FrOwnStAdd2"].ToString();
				myFrameOwnerDetails.FrOwnStAdd3 = result["FrOwnStAdd3"].ToString();
				myFrameOwnerDetails.FrOwnStAdd4 = result["FrOwnStAdd4"].ToString();
				myFrameOwnerDetails.FrOwnStCode = result["FrOwnStCode"].ToString();
				if (result["FrOwnDateCOA"] != System.DBNull.Value)
					myFrameOwnerDetails.FrOwnDateCOA = Convert.ToDateTime(result["FrOwnDateCOA"]);
				myFrameOwnerDetails.FrOwnLicenceCode = result["FrOwnLicenceCode"].ToString();
				myFrameOwnerDetails.FrOwnLicencePlace = result["FrOwnLicencePlace"].ToString();
				myFrameOwnerDetails.LastUser = result["LastUser"].ToString();
			}
			result.Close();
			return myFrameOwnerDetails;
			
		}
		
		public FrameOwnerDetails GetFrameOwnerDetailsByFrame(int FrameIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("FrameOwnerDetailByNotice", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
			parameterFrameIntNo.Value = FrameIntNo;
			myCommand.Parameters.Add(parameterFrameIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			// Create CustomerDetails Struct
			FrameOwnerDetails myFrameOwnerDetails = new FrameOwnerDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myFrameOwnerDetails.FrameIntNo = Convert.ToInt32(result["FrameIntNo"]);
				myFrameOwnerDetails.FrOwnIntNo = Convert.ToInt32(result["FrOwnIntNo"]);
				myFrameOwnerDetails.FrOwnSurname = result["FrOwnSurname"].ToString(); 
				myFrameOwnerDetails.FrOwnInitials = result["FrOwnInitials"].ToString(); 
				myFrameOwnerDetails.FrOwnIDType = result["FrOwnIDType"].ToString(); 
				myFrameOwnerDetails.FrOwnIDNumber = result["FrOwnIDNumber"].ToString(); 
				myFrameOwnerDetails.FrOwnNationality = result["FrOwnNationality"].ToString();
				myFrameOwnerDetails.FrOwnAge = result["FrOwnAge"].ToString();
				myFrameOwnerDetails.FrOwnPOAdd1 = result["FrOwnPoAdd1"].ToString();
				myFrameOwnerDetails.FrOwnPOAdd2 = result["FrOwnPoAdd2"].ToString();
				myFrameOwnerDetails.FrOwnPOAdd3 = result["FrOwnPoAdd3"].ToString();
				myFrameOwnerDetails.FrOwnPOAdd4 = result["FrOwnPoAdd4"].ToString();
				myFrameOwnerDetails.FrOwnPOAdd5 = result["FrOwnPoAdd5"].ToString();
				myFrameOwnerDetails.FrOwnPOCode = result["FrOwnPoCode"].ToString();
				myFrameOwnerDetails.FrOwnStAdd1 = result["FrOwnStAdd1"].ToString();
				myFrameOwnerDetails.FrOwnStAdd2 = result["FrOwnStAdd2"].ToString();
				myFrameOwnerDetails.FrOwnStAdd3 = result["FrOwnStAdd3"].ToString();
				myFrameOwnerDetails.FrOwnStAdd4 = result["FrOwnStAdd4"].ToString();
				myFrameOwnerDetails.FrOwnStCode = result["FrOwnStCode"].ToString();
				if (result["FrOwnDateCOA"] != System.DBNull.Value)
					myFrameOwnerDetails.FrOwnDateCOA = Convert.ToDateTime(result["FrOwnDateCOA"]);
				myFrameOwnerDetails.FrOwnLicenceCode = result["FrOwnLicenceCode"].ToString();
				myFrameOwnerDetails.FrOwnLicencePlace = result["FrOwnLicencePlace"].ToString();
				myFrameOwnerDetails.LastUser = result["LastUser"].ToString();
			}

			result.Close();
			return myFrameOwnerDetails;
			
		}

		//*******************************************************
		//
		// The AddFrameOwner method inserts a new transaction number record
		// into the FrameOwner database.  A unique "FrOwnIntNo"
		// key is then returned from the method.  
		//
		//*******************************************************

		public int AddFrameOwner(int frameIntNo, string frOwnSurname, string frOwnInitials, 
			string frOwnIDType, string frOwnIDNumber, string frOwnNationality,
			string frOwnAge, string frOwnPoAdd1, string frOwnPoAdd2, string frOwnPoAdd3,
			string frOwnPoAdd4, string frOwnPoAdd5,	string frOwnPoCode, string frOwnStAdd1,
			string frOwnStAdd2, string frOwnStAdd3,	string frOwnStAdd4, string frOwnStCode,
			DateTime frOwnDateCOA, string frOwnLicenceCode, string frOwnLicencePlace, string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("FrameOwnerAdd", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
			parameterFrameIntNo.Value = frameIntNo;
			myCommand.Parameters.Add(parameterFrameIntNo);

			SqlParameter parameterOwnSurname = new SqlParameter("@FrOwnSurname", SqlDbType.VarChar, 100);
			parameterOwnSurname.Value = frOwnSurname;
			myCommand.Parameters.Add(parameterOwnSurname);

			SqlParameter parameterOwnInitials = new SqlParameter("@FrOwnInitials", SqlDbType.VarChar, 10);
			parameterOwnInitials.Value = frOwnInitials;
			myCommand.Parameters.Add(parameterOwnInitials);
		
			SqlParameter parameterOwnIDType = new SqlParameter("@FrOwnIDType", SqlDbType.VarChar, 3);
			parameterOwnIDType.Value = frOwnIDType;
			myCommand.Parameters.Add(parameterOwnIDType);
		
			SqlParameter parameterOwnIDNumber = new SqlParameter("@FrOwnIDNumber", SqlDbType.VarChar, 25);
			parameterOwnIDNumber.Value = frOwnIDNumber;
			myCommand.Parameters.Add(parameterOwnIDNumber);
		
			SqlParameter parameterOwnNationality = new SqlParameter("@FrOwnNationality", SqlDbType.VarChar, 3);
			parameterOwnNationality.Value = frOwnNationality;
			myCommand.Parameters.Add(parameterOwnNationality);
		
			SqlParameter parameterOwnAge = new SqlParameter("@FrOwnAge", SqlDbType.VarChar, 3);
			parameterOwnAge.Value = frOwnAge;
			myCommand.Parameters.Add(parameterOwnAge);
		
			SqlParameter parameterOwnPoAdd1 = new SqlParameter("@FrOwnPoAdd1", SqlDbType.VarChar, 100);
			parameterOwnPoAdd1.Value = frOwnPoAdd1;
			myCommand.Parameters.Add(parameterOwnPoAdd1);
		
			SqlParameter parameterOwnPoAdd2 = new SqlParameter("@FrOwnPoAdd2", SqlDbType.VarChar, 100);
			parameterOwnPoAdd2.Value = frOwnPoAdd2;
			myCommand.Parameters.Add(parameterOwnPoAdd2);
		
			SqlParameter parameterOwnPoAdd3 = new SqlParameter("@FrOwnPoAdd3", SqlDbType.VarChar, 100);
			parameterOwnPoAdd3.Value = frOwnPoAdd3;
			myCommand.Parameters.Add(parameterOwnPoAdd3);
		
			SqlParameter parameterOwnPoAdd4 = new SqlParameter("@FrOwnPoAdd4", SqlDbType.VarChar, 100);
			parameterOwnPoAdd4.Value = frOwnPoAdd4;
			myCommand.Parameters.Add(parameterOwnPoAdd4);
		
			SqlParameter parameterOwnPoAdd5 = new SqlParameter("@FrOwnPoAdd5", SqlDbType.VarChar, 100);
			parameterOwnPoAdd5.Value = frOwnPoAdd5;
			myCommand.Parameters.Add(parameterOwnPoAdd5);
		
			SqlParameter parameterOwnPoCode = new SqlParameter("@FrOwnPoCode", SqlDbType.VarChar, 10);
			parameterOwnPoCode.Value = frOwnPoCode;
			myCommand.Parameters.Add(parameterOwnPoCode);
		
			SqlParameter parameterOwnStAdd1 = new SqlParameter("@FrOwnStAdd1", SqlDbType.VarChar, 100);
			parameterOwnStAdd1.Value = frOwnStAdd1;
			myCommand.Parameters.Add(parameterOwnStAdd1);
		
			SqlParameter parameterOwnStAdd2 = new SqlParameter("@FrOwnStAdd2", SqlDbType.VarChar, 100);
			parameterOwnStAdd2.Value = frOwnStAdd2;
			myCommand.Parameters.Add(parameterOwnStAdd2);
			
			SqlParameter parameterOwnStAdd3 = new SqlParameter("@FrOwnStAdd3", SqlDbType.VarChar, 100);
			parameterOwnStAdd3.Value = frOwnStAdd3;
			myCommand.Parameters.Add(parameterOwnStAdd3);
			
			SqlParameter parameterOwnStAdd4 = new SqlParameter("@FrOwnStAdd4", SqlDbType.VarChar, 100);
			parameterOwnStAdd4.Value = frOwnStAdd4;
			myCommand.Parameters.Add(parameterOwnStAdd4);
			
			SqlParameter parameterOwn = new SqlParameter("@FrOwnStCode", SqlDbType.VarChar, 10);
			parameterOwn.Value = frOwnStCode;
			myCommand.Parameters.Add(parameterOwn);
			
			SqlParameter parameterOwnDateCOA = new SqlParameter("@FrOwnDateCOA", SqlDbType.SmallDateTime);
			parameterOwnDateCOA.Value = frOwnDateCOA;
			myCommand.Parameters.Add(parameterOwnDateCOA);
			
			SqlParameter parameterOwnLicenceCode = new SqlParameter("@FrOwnLicenceCode", SqlDbType.VarChar, 3);
			parameterOwnLicenceCode.Value = frOwnLicenceCode;
			myCommand.Parameters.Add(parameterOwnLicenceCode);
			
			SqlParameter parameterOwnLicencePlace = new SqlParameter("@FrOwnLicencePlace", SqlDbType.VarChar, 50);
			parameterOwnLicencePlace.Value = frOwnLicencePlace;
			myCommand.Parameters.Add(parameterOwnLicencePlace);
			
			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterFrOwnIntNo = new SqlParameter("@FrOwnIntNo", SqlDbType.Int, 4);
			parameterFrOwnIntNo.Direction = ParameterDirection.Output;
			myCommand.Parameters.Add(parameterFrOwnIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int FrOwnIntNo = Convert.ToInt32(parameterFrOwnIntNo.Value);

				return FrOwnIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		public SqlDataReader GetFrameOwnerList(int frameIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("FrameOwnerList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
			parameterFrameIntNo.Value = frameIntNo;
			myCommand.Parameters.Add(parameterFrameIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}


		public DataSet GetFrameOwnerListDS(int frameIntNo)
		{
			SqlDataAdapter sqlDAFrameOwners = new SqlDataAdapter();
			DataSet dsFrameOwners = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAFrameOwners.SelectCommand = new SqlCommand();
			sqlDAFrameOwners.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAFrameOwners.SelectCommand.CommandText = "FrameOwnerList";

			// Mark the Command as a SPROC
			sqlDAFrameOwners.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
			parameterFrameIntNo.Value = frameIntNo;
			sqlDAFrameOwners.SelectCommand.Parameters.Add(parameterFrameIntNo);

			// Execute the command and close the connection
			sqlDAFrameOwners.Fill(dsFrameOwners);
			sqlDAFrameOwners.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsFrameOwners;		
		}

		public int UpdateFrameOwner(int frOwnIntNo, int frameIntNo, string frOwnSurname, string frOwnInitials, 
			string frOwnIDType, string frOwnIDNumber, string frOwnNationality,
			string frOwnAge, string frOwnPoAdd1, string frOwnPoAdd2, string frOwnPoAdd3,
			string frOwnPoAdd4, string frOwnPoAdd5,	string frOwnPoCode, string frOwnStAdd1,
			string frOwnStAdd2, string frOwnStAdd3,	string frOwnStAdd4, string frOwnStCode,
			string frOwnDateCOA, string frOwnLicenceCode, string frOwnLicencePlace, string lastUser)  
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("FrameOwnerUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
			parameterFrameIntNo.Value = frameIntNo;
			myCommand.Parameters.Add(parameterFrameIntNo);

			SqlParameter parameterOwnSurname = new SqlParameter("@FrOwnSurname", SqlDbType.VarChar, 100);
			parameterOwnSurname.Value = frOwnSurname;
			myCommand.Parameters.Add(parameterOwnSurname);

			SqlParameter parameterOwnInitials = new SqlParameter("@FrOwnInitials", SqlDbType.VarChar, 10);
			parameterOwnInitials.Value = frOwnInitials;
			myCommand.Parameters.Add(parameterOwnInitials);
		
			SqlParameter parameterOwnIDType = new SqlParameter("@FrOwnIDType", SqlDbType.VarChar, 3);
			parameterOwnIDType.Value = frOwnIDType;
			myCommand.Parameters.Add(parameterOwnIDType);
		
			SqlParameter parameterOwnIDNumber = new SqlParameter("@FrOwnIDNumber", SqlDbType.VarChar, 25);
			parameterOwnIDNumber.Value = frOwnIDNumber;
			myCommand.Parameters.Add(parameterOwnIDNumber);
		
			SqlParameter parameterOwnNationality = new SqlParameter("@FrOwnNationality", SqlDbType.VarChar, 3);
			parameterOwnNationality.Value = frOwnNationality;
			myCommand.Parameters.Add(parameterOwnNationality);
		
			SqlParameter parameterOwnAge = new SqlParameter("@FrOwnAge", SqlDbType.VarChar, 3);
			parameterOwnAge.Value = frOwnAge;
			myCommand.Parameters.Add(parameterOwnAge);
		
			SqlParameter parameterOwnPoAdd1 = new SqlParameter("@FrOwnPoAdd1", SqlDbType.VarChar, 100);
			parameterOwnPoAdd1.Value = frOwnPoAdd1;
			myCommand.Parameters.Add(parameterOwnPoAdd1);
		
			SqlParameter parameterOwnPoAdd2 = new SqlParameter("@FrOwnPoAdd2", SqlDbType.VarChar, 100);
			parameterOwnPoAdd2.Value = frOwnPoAdd2;
			myCommand.Parameters.Add(parameterOwnPoAdd2);
		
			SqlParameter parameterOwnPoAdd3 = new SqlParameter("@FrOwnPoAdd3", SqlDbType.VarChar, 100);
			parameterOwnPoAdd3.Value = frOwnPoAdd3;
			myCommand.Parameters.Add(parameterOwnPoAdd3);
		
			SqlParameter parameterOwnPoAdd4 = new SqlParameter("@FrOwnPoAdd4", SqlDbType.VarChar, 100);
			parameterOwnPoAdd4.Value = frOwnPoAdd4;
			myCommand.Parameters.Add(parameterOwnPoAdd4);
		
			SqlParameter parameterOwnPoAdd5 = new SqlParameter("@FrOwnPoAdd5", SqlDbType.VarChar, 100);
			parameterOwnPoAdd5.Value = frOwnPoAdd5;
			myCommand.Parameters.Add(parameterOwnPoAdd5);
		
			SqlParameter parameterOwnPoCode = new SqlParameter("@FrOwnPoCode", SqlDbType.VarChar, 10);
			parameterOwnPoCode.Value = frOwnPoCode;
			myCommand.Parameters.Add(parameterOwnPoCode);
		
			SqlParameter parameterOwnStAdd1 = new SqlParameter("@FrOwnStAdd1", SqlDbType.VarChar, 100);
			parameterOwnStAdd1.Value = frOwnStAdd1;
			myCommand.Parameters.Add(parameterOwnStAdd1);
		
			SqlParameter parameterOwnStAdd2 = new SqlParameter("@FrOwnStAdd2", SqlDbType.VarChar, 100);
			parameterOwnStAdd2.Value = frOwnStAdd2;
			myCommand.Parameters.Add(parameterOwnStAdd2);
			
			SqlParameter parameterOwnStAdd3 = new SqlParameter("@FrOwnStAdd3", SqlDbType.VarChar, 100);
			parameterOwnStAdd3.Value = frOwnStAdd3;
			myCommand.Parameters.Add(parameterOwnStAdd3);
			
			SqlParameter parameterOwnStAdd4 = new SqlParameter("@FrOwnStAdd4", SqlDbType.VarChar, 100);
			parameterOwnStAdd4.Value = frOwnStAdd4;
			myCommand.Parameters.Add(parameterOwnStAdd4);
				
			SqlParameter parameterOwn = new SqlParameter("@FrOwnStCode", SqlDbType.VarChar, 10);
			parameterOwn.Value = frOwnStCode;
			myCommand.Parameters.Add(parameterOwn);
			
			SqlParameter parameterOwnDateCOA = new SqlParameter("@FrOwnDateCOA", SqlDbType.VarChar, 10);
			parameterOwnDateCOA.Value = frOwnDateCOA;
			myCommand.Parameters.Add(parameterOwnDateCOA);
			
			SqlParameter parameterOwnLicenceCode = new SqlParameter("@FrOwnLicenceCode", SqlDbType.VarChar, 3);
			parameterOwnLicenceCode.Value = frOwnLicenceCode;
			myCommand.Parameters.Add(parameterOwnLicenceCode);
			
			SqlParameter parameterOwnLicencePlace = new SqlParameter("@FrOwnLicencePlace", SqlDbType.VarChar, 50);
			parameterOwnLicencePlace.Value = frOwnLicencePlace;
			myCommand.Parameters.Add(parameterOwnLicencePlace);
			
			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterFrOwnIntNo = new SqlParameter("@FrOwnIntNo", SqlDbType.Int, 4);
			parameterFrOwnIntNo.Value = frOwnIntNo;
			parameterFrOwnIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterFrOwnIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int FrOwnIntNo = (int)myCommand.Parameters["@FrOwnIntNo"].Value;

				return FrOwnIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}


        /// <summary>
        /// added Jake 090413
        /// description : Update Owner Base Information
        /// </summary>
        /// <param name="frOwnIntNo"></param>
        /// <param name="frOwnSurname"></param>
        /// <param name="frOwnInitials"></param>
        /// <param name="frOwnIDNumber"></param>
        /// <param name="frOwnPoAdd1"></param>
        /// <param name="frOwnPoAdd2"></param>
        /// <param name="frOwnPoAdd3"></param>
        /// <param name="frOwnPoAdd4"></param>
        /// <param name="frOwnPoAdd5"></param>
        /// <param name="frOwnPoCode"></param>
        /// <param name="frOwnStAdd1"></param>
        /// <param name="frOwnStAdd2"></param>
        /// <param name="frOwnStAdd3"></param>
        /// <param name="frOwnStAdd4"></param>
        /// <param name="frOwnStAdd5"></param>
        /// <param name="lastUser"></param>
        /// <returns></returns>
        public int UpdateFrameOwner(int frOwnIntNo, int frameIntNo, string frOwnSurname, string frOwnInitials,
                        string frOwnIDNumber, string frOwnPoAdd1, string frOwnPoAdd2, string frOwnPoAdd3,
                        string frOwnPoAdd4, string frOwnPoAdd5, string frOwnPoCode, string frOwnStAdd1,
                        string frOwnStAdd2, string frOwnStAdd3, string frOwnStAdd4,
                        string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OwnUpdateBaseInformation", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterOwnSurname = new SqlParameter("@FrOwnSurnname", SqlDbType.VarChar, 100);
            parameterOwnSurname.Value = frOwnSurname;
            myCommand.Parameters.Add(parameterOwnSurname);

            SqlParameter parameterOwnIDNumber = new SqlParameter("@FrOwnIDNumber", SqlDbType.VarChar, 25);
            parameterOwnIDNumber.Value = frOwnIDNumber;
            myCommand.Parameters.Add(parameterOwnIDNumber);

            SqlParameter parameterOwnInitials = new SqlParameter("@FrOwnInitials", SqlDbType.VarChar, 10);
            parameterOwnInitials.Value = frOwnInitials;
            myCommand.Parameters.Add(parameterOwnInitials);

            SqlParameter parameterOwnPoAdd1 = new SqlParameter("@FrOwnPoAdd1", SqlDbType.VarChar, 100);
            parameterOwnPoAdd1.Value = frOwnPoAdd1;
            myCommand.Parameters.Add(parameterOwnPoAdd1);

            SqlParameter parameterOwnPoAdd2 = new SqlParameter("@FrOwnPoAdd2", SqlDbType.VarChar, 100);
            parameterOwnPoAdd2.Value = frOwnPoAdd2;
            myCommand.Parameters.Add(parameterOwnPoAdd2);

            SqlParameter parameterOwnPoAdd3 = new SqlParameter("@FrOwnPoAdd3", SqlDbType.VarChar, 100);
            parameterOwnPoAdd3.Value = frOwnPoAdd3;
            myCommand.Parameters.Add(parameterOwnPoAdd3);

            SqlParameter parameterOwnPoAdd4 = new SqlParameter("@FrOwnPoAdd4", SqlDbType.VarChar, 100);
            parameterOwnPoAdd4.Value = frOwnPoAdd4;
            myCommand.Parameters.Add(parameterOwnPoAdd4);

            SqlParameter parameterOwnPoAdd5 = new SqlParameter("@FrOwnPoAdd5", SqlDbType.VarChar, 100);
            parameterOwnPoAdd5.Value = frOwnPoAdd5;
            myCommand.Parameters.Add(parameterOwnPoAdd5);

            SqlParameter parameterOwnPoCode = new SqlParameter("@FrOwnPoCode", SqlDbType.VarChar, 10);
            parameterOwnPoCode.Value = frOwnPoCode;
            myCommand.Parameters.Add(parameterOwnPoCode);

            SqlParameter parameterOwnStAdd1 = new SqlParameter("@FrOwnStAdd1", SqlDbType.VarChar, 100);
            parameterOwnStAdd1.Value = frOwnStAdd1;
            myCommand.Parameters.Add(parameterOwnStAdd1);

            SqlParameter parameterOwnStAdd2 = new SqlParameter("@FrOwnStAdd2", SqlDbType.VarChar, 100);
            parameterOwnStAdd2.Value = frOwnStAdd2;
            myCommand.Parameters.Add(parameterOwnStAdd2);

            SqlParameter parameterOwnStAdd3 = new SqlParameter("@FrOwnStAdd3", SqlDbType.VarChar, 100);
            parameterOwnStAdd3.Value = frOwnStAdd3;
            myCommand.Parameters.Add(parameterOwnStAdd3);

            SqlParameter parameterOwnStAdd4 = new SqlParameter("@FrOwnStAdd4", SqlDbType.VarChar, 100);
            parameterOwnStAdd4.Value = frOwnStAdd4;
            myCommand.Parameters.Add(parameterOwnStAdd4);

            //SqlParameter parameterOwnStAdd5 = new SqlParameter("@OwnStAdd5", SqlDbType.VarChar, 100);
            //parameterOwnStAdd5.Value = frOwnStAdd5;
            //myCommand.Parameters.Add(parameterOwnStAdd5);

            //SqlParameter parameterOwn = new SqlParameter("@OwnStCode", SqlDbType.VarChar, 10);
            //parameterOwn.Value = frOwnStCode;
            //myCommand.Parameters.Add(parameterOwn);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo",SqlDbType.Int,4);
            parameterFrameIntNo.Value = frameIntNo;
            myCommand.Parameters.Add(parameterFrameIntNo);

            SqlParameter parameterOwnIntNo = new SqlParameter("@FrOwnIntNo", SqlDbType.Int, 4);
           
            parameterOwnIntNo.Direction = ParameterDirection.InputOutput;
            parameterOwnIntNo.Value = frOwnIntNo;

            myCommand.Parameters.Add(parameterOwnIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                int OwnIntNo =(int) parameterOwnIntNo.Value;
                myConnection.Dispose();

                return OwnIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }
	
	}
}
