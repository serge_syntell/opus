using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    //*******************************************************
    //
    // SysParamDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************
    public class SysParamDetails
    {
        /// <summary>
        /// The column name constant for the Java based Excel Reporting server's host address
        /// </summary>
        public const string EXCEL_REPORTING_HOST_NAME = "ExcelReportingHostName";
        public const string VIOLATION_CUT_OFF = "ViolationCutOff";

        public int SPIntNo;
        public string SPColumnName;
        public int SPIntegerValue;
        public string SPStringValue;
        public string SPDescr;
        public string LastUser;
    }

    public class DefaultSysParam
    {
        protected SysParamDetails details = new SysParamDetails();
        private string mConstr;

        public DefaultSysParam(SysParamDetails sysParam, string vConstr)
        {
            this.details = sysParam;
            this.mConstr = vConstr;
        }

        public KeyValuePair<int, string> SetDefaultSysParam()
        {
            switch (this.details.SPColumnName)
            {
                case SysParamDetails.VIOLATION_CUT_OFF:
                    details.SPDescr = "Rule to generate compressed images for notices";
                    details.SPStringValue = "N";
                    details.SPIntegerValue = 0;
                    break;

                case SysParamDetails.EXCEL_REPORTING_HOST_NAME:
                    details.SPDescr = "The column name constant for the Java based Excel Reporting server's host address";
                    details.SPStringValue = "http://localhost:8080/ExcelReporting";
                    details.SPIntegerValue = 0;
                    break;

                case "":
                    details.SPDescr = "";
                    details.SPStringValue = "N";
                    details.SPIntegerValue = 0;
                    break;
            }

            SysParamDB sp = new SysParamDB(mConstr);

            this.details = sp.GetSysParamWithDefaults(this.details);

            KeyValuePair<int, string> value;

            if (this.details != null)
            {
                value = new KeyValuePair<int, string>(this.details.SPIntegerValue, this.details.SPStringValue);
            }
            else
            {
                value = new KeyValuePair<int, string>(0, "-");
            }

            return value;

        }
    }

    //public class DefaultSysParam
    //{
    //    public  static SysParamDetails GetSysParamDetails(string columnName)
    //    {
    //        SysParamDetails details = new SysParamDetails();

    //        switch(columnName)
    //        {
    //            case SysParamDetails.EXCEL_REPORTING_HOST_NAME:
    //                details.SPColumnName = SysParamDetails.EXCEL_REPORTING_HOST_NAME;
    //                details.SPDescr = "The column name constant for the Java based Excel Reporting server's host address";
    //                details.SPStringValue = "http://localhost:8080/ExcelReporting";
    //                break;
    //        }

    //        return details;
    //    }
    //}

    //*******************************************************
    //
    // SysParamDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query SysParams within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************
    public class SysParamDB
    {
        private readonly string connectionString = string.Empty;

        public SysParamDB(string vConstr)
        {
            this.connectionString = vConstr;
        }

        //*******************************************************
        //
        // SysParamDB.GetSysParamDetails() Method <a name="GetSysParamDetails"></a>
        //
        // The GetSysParamDetails method returns a SysParamDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************
        public SysParamDetails GetSysParamDetails(int spIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("SysParamDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterSPIntNo = new SqlParameter("@SPIntNo", SqlDbType.Int, 4);
            parameterSPIntNo.Value = spIntNo;
            myCommand.Parameters.Add(parameterSPIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            SysParamDetails mySysParamDetails = new SysParamDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                mySysParamDetails.SPColumnName = result["SPColumnName"].ToString();
                mySysParamDetails.SPIntegerValue = Convert.ToInt32(result["SPIntegerValue"]);
                mySysParamDetails.SPStringValue = result["SPStringValue"].ToString();
                mySysParamDetails.SPDescr = result["SPDescr"].ToString();
                mySysParamDetails.LastUser = result["LastUser"].ToString();
            }
            result.Close();
            return mySysParamDetails;
        }

        ////*******************************************************
        ////
        //// SysParamDB.AddSysParam() Method <a name="AddSysParam"></a>
        ////
        //// The AddSysParam method inserts a new menu record
        //// into the menus database.  A unique "SysParamId"
        //// key is then returned from the method.  
        ////
        ////*******************************************************
        //public int AddSysParam(string spColumnName, int spIntegerValue, string spStringValue, string spDescr, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("SysParamAdd", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterSPColumnName = new SqlParameter("@SPColumnName", SqlDbType.VarChar, 50);
        //    parameterSPColumnName.Value = spColumnName;
        //    myCommand.Parameters.Add(parameterSPColumnName);

        //    SqlParameter parameterSPIntegerValue = new SqlParameter("@SPIntegerValue", SqlDbType.Int, 4);
        //    parameterSPIntegerValue.Value = spIntegerValue;
        //    myCommand.Parameters.Add(parameterSPIntegerValue);

        //    SqlParameter parameterSPStringValue = new SqlParameter("@SPStringValue", SqlDbType.VarChar, 100);
        //    parameterSPStringValue.Value = spStringValue;
        //    myCommand.Parameters.Add(parameterSPStringValue);

        //    SqlParameter parameterSPDescr = new SqlParameter("@SPDescr", SqlDbType.VarChar, 100);
        //    parameterSPDescr.Value = spDescr;
        //    myCommand.Parameters.Add(parameterSPDescr);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterSPIntNo = new SqlParameter("@SPIntNo", SqlDbType.Int, 4);
        //    parameterSPIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterSPIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int spIntNo = Convert.ToInt32(parameterSPIntNo.Value);

        //        return spIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        /// <summary>
        /// Gets the name of the system parameter by coulmn name.
        /// </summary>
        /// <param name="spColumnName">Name of the sytem parameter column.</param>
        /// <param name="spIntegerValue">The sytem parameter integer value.</param>
        /// <param name="spStringValue">The sytem parameter string value.</param>
        /// <param name="spDescr">The sytem parameter description.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns>
        /// The integer value of the paramter, or zero if not found.
        /// </returns>
        //public int GetSysParamByColumnName(string spColumnName, ref int spIntegerValue, ref string spStringValue, string spDescr, string lastUser)
        //{
        //    SqlConnection con = new SqlConnection(connectionString);
        //    SqlCommand com = new SqlCommand("SysParamAdd", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@SPColumnName", SqlDbType.VarChar, 50).Value = spColumnName;
        //    com.Parameters.Add("@SPDescr", SqlDbType.VarChar, 50).Value = spDescr;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

        //    SqlParameter parameterSPIntegerValue = com.Parameters.Add("@SPIntegerValue", SqlDbType.Int, 4);
        //    parameterSPIntegerValue.Value = spIntegerValue;
        //    parameterSPIntegerValue.Direction = ParameterDirection.Output;

        //    SqlParameter parameterSPStringValue = com.Parameters.Add("@SPStringValue", SqlDbType.VarChar, 100);
        //    parameterSPStringValue.Value = spStringValue;
        //    parameterSPStringValue.Direction = ParameterDirection.Output;

        //    SqlParameter parameterSPIntNo = com.Parameters.Add("@SPIntNo", SqlDbType.Int, 4);
        //    parameterSPIntNo.Direction = ParameterDirection.Output;


        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();
        //        con.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int spIntNo = Convert.ToInt32(parameterSPIntNo.Value);

        //        spStringValue = parameterSPStringValue.Value.ToString();
        //        spIntegerValue = Convert.ToInt32(parameterSPIntegerValue.Value);

        //        return spIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        con.Dispose();
        //        System.Diagnostics.Debug.WriteLine(e.Message);
        //        return 0;
        //    }
        //}

        /// <summary>
        /// Gets the system parameter, supplying default values.
        /// </summary>
        /// <param name="parameter">The system parameter details.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns>
        /// 	<c>true</c> if the string parameter returned is the string 'Yes'.
        /// </returns>
        //public bool GetSysParamWithDefaults(ref SysParamDetails parameter)
        public SysParamDetails GetSysParamWithDefaults(SysParamDetails parameter)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("GetSysParamWithDefaults", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@SPColumnName", SqlDbType.VarChar, 50).Value = parameter.SPColumnName;
            com.Parameters.Add("@SPIntegerValue", SqlDbType.Int, 4).Value = parameter.SPIntegerValue;
            com.Parameters.Add("@SPStringValue", SqlDbType.VarChar, 100).Value = parameter.SPStringValue;
            com.Parameters.Add("@SPDescr", SqlDbType.VarChar, 100).Value = parameter.SPDescr;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = parameter.LastUser;

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                if (reader.Read())
                {
                    parameter.SPIntNo = Convert.ToInt32(reader["SPIntNo"]);
                    parameter.SPColumnName = (string)reader["SPColumnName"];
                    parameter.SPDescr = (string)reader["SPDescr"];
                    parameter.SPIntegerValue = (int)reader["SPIntegerValue"];
                    parameter.SPStringValue = (string)reader["SPStringValue"];
                }
                reader.Close();

                //return (parameter.SPStringValue.Equals("Yes"));
                return parameter;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                //return false;
                return null;
            }
            finally
            {
                con.Dispose();
            }
        }

        public SqlDataReader GetSysParamList()
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("SysParamList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        public DataSet GetSysParamListDS()
        {
            SqlDataAdapter sqlDASysParams = new SqlDataAdapter();
            DataSet dsSysParams = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDASysParams.SelectCommand = new SqlCommand();
            sqlDASysParams.SelectCommand.Connection = new SqlConnection(connectionString);
            sqlDASysParams.SelectCommand.CommandText = "SysParamList";

            // Mark the Command as a SPROC
            sqlDASysParams.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command and close the connection
            sqlDASysParams.Fill(dsSysParams);
            sqlDASysParams.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsSysParams;
        }


        public int UpdateSysParam(int spIntNo, string spColumnName, int spIntegerValue, string spStringValue, string spDescr, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("SysParamUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterSPColumnName = new SqlParameter("@SPColumnName", SqlDbType.VarChar, 50);
            parameterSPColumnName.Value = spColumnName;
            myCommand.Parameters.Add(parameterSPColumnName);

            SqlParameter parameterSPIntegerValue = new SqlParameter("@SPIntegerValue", SqlDbType.Int, 4);
            parameterSPIntegerValue.Value = spIntegerValue;
            myCommand.Parameters.Add(parameterSPIntegerValue);

            SqlParameter parameterSPStringValue = new SqlParameter("@SPStringValue", SqlDbType.VarChar, 100);
            parameterSPStringValue.Value = spStringValue;
            myCommand.Parameters.Add(parameterSPStringValue);

            SqlParameter parameterSPDescr = new SqlParameter("@SPDescr", SqlDbType.VarChar, 100);
            parameterSPDescr.Value = spDescr;
            myCommand.Parameters.Add(parameterSPDescr);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterSPIntNo = new SqlParameter("@SPIntNo", SqlDbType.Int);
            parameterSPIntNo.Direction = ParameterDirection.InputOutput;
            parameterSPIntNo.Value = spIntNo;
            myCommand.Parameters.Add(parameterSPIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int SPIntNo = (int)myCommand.Parameters["@SPIntNo"].Value;
                //int menuId = (int)parameterSPIntNo.Value;

                return SPIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        // 2013-07-19 comment by Henry for useless
        //public String DeleteSysParam(int spIntNo)
        //{

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("SysParamDelete", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterSPIntNo = new SqlParameter("@SPIntNo", SqlDbType.Int, 4);
        //    parameterSPIntNo.Value = spIntNo;
        //    parameterSPIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterSPIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int SPIntNo = (int)parameterSPIntNo.Value;

        //        return SPIntNo.ToString();
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return String.Empty;
        //    }
        //}

        /// <summary>
        /// Checks to see if the system parameter has been set.
        /// </summary>
        /// <param name="columnName">Name of the column to check for.</param>
        /// <param name="intValue">The integer value of the system parameter.</param>
        /// <param name="stringValue">The string value of the system parameter.</param>
        /// <returns>
        /// 	<c>true</c> if the system parameter has been set in the database.
        /// </returns>
        public bool CheckSysParam(string columnName, ref int intValue, ref string stringValue)
        {
            bool response = false;

            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SysParamCheck", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@ColumnName", SqlDbType.VarChar, 50).Value = columnName;

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                if (reader.Read())
                {
                    intValue = (int)reader["SPIntegerValue"];
                    stringValue = (string)reader["SPStringValue"];
                    response = true;
                }
                reader.Close();
            }
            finally
            {
                con.Close();
            }

            return response;
        }

    }
}

