namespace Stalberg.TMS
{
    /// <summary>
    /// Lists the different types of 'Cash' payment methods.
    /// (the integer values of the enumeration refer to the characters that represent them i n the database)
    /// </summary>
    public enum CourtJudgementType
    {
        //dls 080122 - add a new payment type BankPayment (K) - may not be used in conjunction with other types when doing multiple payments
        NoJudgement_0 = 0,
        Payment_5 = 5,
        DeferredPayment_6 = 6,                // When @YesDeferred = 1
        StruckOffRoll_10 = 10,
        //Withdrawn_15 = 15,
        WithdrawnCharge_15 = 15,    //Oscar 20101206 - changed
        WithdrawnToReissue_16 = 16,
        WithdrawnSummons_17 = 17, //Oscar 20101206 - added
        Remand_19 = 19,
        WOA_20 = 20,
        ReduceFineAmountPayment_21 = 21,//Edge 2013-10-29
        CaseFinalised_LicenceSuspended=22, // Jake 2013-12-18 added
        CommunityService_25 = 25,
        PaidAtCourt_49 = 49,
        MoreChargeJudgement_59 = 59, //Oscar 20101206 - added
        Guilty_26=26,  //Rachel 20141112 - added
        NotGuilty_27=27, //Rachel 20141112 - added
        Other_99 = 99
    }

}