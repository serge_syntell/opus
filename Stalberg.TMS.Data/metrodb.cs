using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // MetroDetails Class
    //
    // A simple data class that encapsulates details about a particular user 
    //
    //*******************************************************

    public partial class MetroDetails
    {
        public Int32 RegIntNo;
        public string MtrCode;
        public string MtrNo;
        public string MtrName;
        public string MtrPostAddr1;
        public string MtrPostAddr2;
        public string MtrPostAddr3;
        public string MtrPostCode;
        public string MtrTel;
        public string MtrFax;
        public string MtrEmail;
        public Int32 MtrIntNo;
        //add by richard 2011-06-07
        public string MtrNoticePaymentInfo;
        public string MtrNoticeIssuedByInfo;
        public string MtrPhysAdd1;
        public string MtrPhysAdd2;
        public string MtrPhysAdd3;
        public string MtrPhysAdd4;
        public string MtrPhysCode;
        public string MtrPayPoint1Add1;
        public string MtrPayPoint1Add2;
        public string MtrPayPoint1Add3;
        public string MtrPayPoint1Add4;
        public string MtrPayPoint1Code;
        public string MtrPayPoint1Tel;
        public string MtrPayPoint1Fax;
        public string MtrPayPoint1Email;
        public string MtrPayPoint2Add1;
        public string MtrPayPoint2Add2;
        public string MtrPayPoint2Add3;
        public string MtrPayPoint2Add4;
        public string MtrPayPoint2Code;
        public string MtrPayPoint2Tel;
        public string MtrPayPoint2Fax;
        public string MtrPayPoint2Email;
        public string MtrDocumentFrom;
        public string MtrReceiptFrom;
        public string MtrReceiptEnglishAct;
        public string MtrReceiptAfrikaansAct;
        public string MtrDepartName;
        public string MtrDocumentFromEnglish;
        public string MtrDocumentFromAfrikaans;
        public string MtrFooter;
        public byte[] MtrLogo;
        public string MtrNameAfri;
    }

    public partial class MetroDB
    {

        string mConstr = "";

        public MetroDB(string vConstr)
        {
            mConstr = vConstr;
        }

        public MetroDetails GetMetroDetails(int mtrIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("MetroDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterMtrIntNo = new SqlParameter("@MtrIntNo", SqlDbType.Int, 4);
            parameterMtrIntNo.Value = mtrIntNo;
            myCommand.Parameters.Add(parameterMtrIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            MetroDetails myMetroDetails = new MetroDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myMetroDetails.RegIntNo = Convert.ToInt32(result["RegIntNo"].ToString());
                myMetroDetails.MtrCode = result["MtrCode"].ToString();
                myMetroDetails.MtrNo = result["MtrNo"].ToString();
                myMetroDetails.MtrName = result["MtrName"].ToString();
                myMetroDetails.MtrPostAddr1 = result["MtrPostAddr1"].ToString();
                myMetroDetails.MtrPostAddr2 = result["MtrPostAddr2"].ToString();
                myMetroDetails.MtrPostAddr3 = result["MtrPostAddr3"].ToString();
                myMetroDetails.MtrPostCode = result["MtrPostCode"].ToString();
                myMetroDetails.MtrTel = result["MtrTel"].ToString();
                myMetroDetails.MtrFax = result["MtrFax"].ToString();
                myMetroDetails.MtrEmail = result["MtrEmail"].ToString();
                //add by richard 2011-06-07
                myMetroDetails.MtrNoticePaymentInfo = result["MtrNoticePaymentInfo"].ToString();
                myMetroDetails.MtrNoticeIssuedByInfo = result["MtrNoticeIssuedByInfo"].ToString();
                myMetroDetails.MtrPhysAdd1 = result["MtrPhysAdd1"].ToString();
                myMetroDetails.MtrPhysAdd2 = result["MtrPhysAdd2"].ToString();
                myMetroDetails.MtrPhysAdd3 = result["MtrPhysAdd3"].ToString();
                myMetroDetails.MtrPhysAdd4 = result["MtrPhysAdd4"].ToString();
                myMetroDetails.MtrPhysCode = result["MtrPhysCode"].ToString();
                myMetroDetails.MtrPayPoint1Add1 = result["MtrPayPoint1Add1"].ToString();
                myMetroDetails.MtrPayPoint1Add2 = result["MtrPayPoint1Add2"].ToString();
                myMetroDetails.MtrPayPoint1Add3 = result["MtrPayPoint1Add3"].ToString();
                myMetroDetails.MtrPayPoint1Add4 = result["MtrPayPoint1Add4"].ToString();
                myMetroDetails.MtrPayPoint1Code = result["MtrPayPoint1Code"].ToString();
                myMetroDetails.MtrPayPoint1Tel = result["MtrPayPoint1Tel"].ToString();
                myMetroDetails.MtrPayPoint1Fax = result["MtrPayPoint1Fax"].ToString();
                myMetroDetails.MtrPayPoint1Email = result["MtrPayPoint1Email"].ToString();
                myMetroDetails.MtrPayPoint2Add1 = result["MtrPayPoint2Add1"].ToString();
                myMetroDetails.MtrPayPoint2Add2 = result["MtrPayPoint2Add2"].ToString();
                myMetroDetails.MtrPayPoint2Add3 = result["MtrPayPoint2Add3"].ToString();
                myMetroDetails.MtrPayPoint2Add4 = result["MtrPayPoint2Add4"].ToString();
                myMetroDetails.MtrPayPoint2Code = result["MtrPayPoint2Code"].ToString();
                myMetroDetails.MtrPayPoint2Tel = result["MtrPayPoint2Tel"].ToString();
                myMetroDetails.MtrPayPoint2Fax = result["MtrPayPoint2Fax"].ToString();
                myMetroDetails.MtrPayPoint2Email = result["MtrPayPoint2Email"].ToString();
                //myMetroDetails.MtrDocumentFrom = result["MtrDocumentFrom"].ToString();
                //myMetroDetails.MtrReceiptFrom = result["MtrReceiptFrom"].ToString();
                myMetroDetails.MtrReceiptEnglishAct = result["MtrReceiptEnglishAct"].ToString();
                myMetroDetails.MtrReceiptAfrikaansAct = result["MtrReceiptAfrikaansAct"].ToString();
                myMetroDetails.MtrDepartName = result["MtrDepartName"].ToString();
                myMetroDetails.MtrDocumentFromEnglish = result["MtrDocumentFromEnglish"].ToString();
                myMetroDetails.MtrDocumentFromAfrikaans = result["MtrDocumentFromAfrikaans"].ToString();
                myMetroDetails.MtrFooter = result["MtrFooter"].ToString();
                myMetroDetails.MtrNameAfri = result["MtrNameAfrikaans"].ToString();
            }
            result.Close();
            return myMetroDetails;
        }

        public MetroDetails GetLogo(int mtrIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("MetroDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterMtrIntNo = new SqlParameter("@MtrIntNo", SqlDbType.Int, 4);
            parameterMtrIntNo.Value = mtrIntNo;
            myCommand.Parameters.Add(parameterMtrIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            MetroDetails myMetroDetails = new MetroDetails();

            while (result.Read())
            {
                myMetroDetails.MtrLogo = string.IsNullOrEmpty(result["MtrLogo"].ToString()) ? new byte[4] : (byte[])result["MtrLogo"];
            }
            result.Close();
            return myMetroDetails;
        }
        public int AddMetro(int regIntNo, string mtrCode, string mtrNo, string mtrName, string mtrPostAddr1, string mtrPostAddr2, string mtrPostAddr3, string mtrPostCode, string mtrTel, string mtrFax, string mtrEmail, string user,
             string MtrNoticePaymentInfo, string MtrNoticeIssuedByInfo,
 string MtrPhysAdd1,
 string MtrPhysAdd2,
 string MtrPhysAdd3,
 string MtrPhysAdd4,
 string MtrPhysCode,
 string MtrPayPoint1Add1,
 string MtrPayPoint1Add2,
 string MtrPayPoint1Add3,
 string MtrPayPoint1Add4,
 string MtrPayPoint1Code,
 string MtrPayPoint1Tel,
 string MtrPayPoint1Fax,
 string MtrPayPoint1Email,
 string MtrPayPoint2Add1,
 string MtrPayPoint2Add2,
 string MtrPayPoint2Add3,
 string MtrPayPoint2Add4,
 string MtrPayPoint2Code,
 string MtrPayPoint2Tel,
 string MtrPayPoint2Fax,
 string MtrPayPoint2Email,
 //string MtrDocumentFrom,
 //string MtrReceiptFrom,
 string MtrReceiptEnglishAct,
 string MtrReceiptAfrikaansAct,
 string MtrDepartName,
 string MtrDocumentFromEnglish,
 string MtrDocumentFromAfrikaans,
 string MtrFooter, byte[] MtrLogo, string MtrNameAfri)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("MetroAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterRegIntNo = new SqlParameter("@RegIntNo", SqlDbType.Int);
            parameterRegIntNo.Value = regIntNo;
            myCommand.Parameters.Add(parameterRegIntNo);

            SqlParameter parameterMtrCode = new SqlParameter("@MtrCode", SqlDbType.Char, 3);
            parameterMtrCode.Value = mtrCode;
            myCommand.Parameters.Add(parameterMtrCode);

            SqlParameter parameterMtrNo = new SqlParameter("@MtrNo", SqlDbType.VarChar, 6);
            parameterMtrNo.Value = mtrNo;
            myCommand.Parameters.Add(parameterMtrNo);

            SqlParameter parameterMtrName = new SqlParameter("@MtrName", SqlDbType.VarChar, 100);
            parameterMtrName.Value = mtrName;
            myCommand.Parameters.Add(parameterMtrName);

            SqlParameter parameterMtrPostAddr1 = new SqlParameter("@MtrPostAddr1", SqlDbType.VarChar, 100);
            parameterMtrPostAddr1.Value = mtrPostAddr1;
            myCommand.Parameters.Add(parameterMtrPostAddr1);

            SqlParameter parameterMtrPostAddr2 = new SqlParameter("@MtrPostAddr2", SqlDbType.VarChar, 100);
            parameterMtrPostAddr2.Value = mtrPostAddr2;
            myCommand.Parameters.Add(parameterMtrPostAddr2);

            SqlParameter parameterMtrPostAddr3 = new SqlParameter("@MtrPostAddr3", SqlDbType.VarChar, 100);
            parameterMtrPostAddr3.Value = mtrPostAddr3;
            myCommand.Parameters.Add(parameterMtrPostAddr3);

            SqlParameter parameterMtrPostCode = new SqlParameter("@MtrPostCode", SqlDbType.VarChar, 4);
            parameterMtrPostCode.Value = mtrPostCode;
            myCommand.Parameters.Add(parameterMtrPostCode);

            SqlParameter parameterMtrTel = new SqlParameter("@MtrTel", SqlDbType.VarChar, 30);
            parameterMtrTel.Value = mtrTel;
            myCommand.Parameters.Add(parameterMtrTel);

            SqlParameter parameterMtrFax = new SqlParameter("@MtrFax", SqlDbType.VarChar, 30);
            parameterMtrFax.Value = mtrFax;
            myCommand.Parameters.Add(parameterMtrFax);

            SqlParameter parameterMtrEmail = new SqlParameter("@MtrEmail", SqlDbType.VarChar, 50);
            parameterMtrEmail.Value = mtrEmail;
            myCommand.Parameters.Add(parameterMtrEmail);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = user;
            myCommand.Parameters.Add(parameterLastUser);


            SqlParameter parameterMtrNoticePaymentInfo = new SqlParameter("@MtrNoticePaymentInfo", SqlDbType.VarChar);
            parameterMtrNoticePaymentInfo.Value = MtrNoticePaymentInfo;
            myCommand.Parameters.Add(parameterMtrNoticePaymentInfo);

            SqlParameter parameterMtrNoticeIssuedByInfo = new SqlParameter("@MtrNoticeIssuedByInfo", SqlDbType.VarChar);
            parameterMtrNoticeIssuedByInfo.Value = MtrNoticeIssuedByInfo;
            myCommand.Parameters.Add(parameterMtrNoticeIssuedByInfo);

            SqlParameter parameterMtrPhysAdd1 = new SqlParameter("@MtrPhysAdd1", SqlDbType.VarChar, 100);
            parameterMtrPhysAdd1.Value = MtrPhysAdd1;
            myCommand.Parameters.Add(parameterMtrPhysAdd1);

            SqlParameter parameterMtrPhysAdd2 = new SqlParameter("@MtrPhysAdd2", SqlDbType.VarChar, 100);
            parameterMtrPhysAdd2.Value = MtrPhysAdd2;
            myCommand.Parameters.Add(parameterMtrPhysAdd2);

            SqlParameter parameterMtrPhysAdd3 = new SqlParameter("@MtrPhysAdd3", SqlDbType.VarChar, 100);
            parameterMtrPhysAdd3.Value = MtrPhysAdd3;
            myCommand.Parameters.Add(parameterMtrPhysAdd3);

            SqlParameter parameterMtrPhysAdd4 = new SqlParameter("@MtrPhysAdd4", SqlDbType.VarChar, 100);
            parameterMtrPhysAdd4.Value = MtrPhysAdd4;
            myCommand.Parameters.Add(parameterMtrPhysAdd4);

            SqlParameter parameterMtrPhysCode = new SqlParameter("@MtrPhysCode", SqlDbType.VarChar, 5);
            parameterMtrPhysCode.Value = MtrPhysCode;
            myCommand.Parameters.Add(parameterMtrPhysCode);

            SqlParameter parameterMtrPayPoint1Add1 = new SqlParameter("@MtrPayPoint1Add1", SqlDbType.VarChar, 100);
            parameterMtrPayPoint1Add1.Value = MtrPayPoint1Add1;
            myCommand.Parameters.Add(parameterMtrPayPoint1Add1);

            SqlParameter parameterMtrPayPoint1Add2 = new SqlParameter("@MtrPayPoint1Add2", SqlDbType.VarChar, 100);
            parameterMtrPayPoint1Add2.Value = MtrPayPoint1Add2;
            myCommand.Parameters.Add(parameterMtrPayPoint1Add2);

            SqlParameter parameterMtrPayPoint1Add3 = new SqlParameter("@MtrPayPoint1Add3", SqlDbType.VarChar, 100);
            parameterMtrPayPoint1Add3.Value = MtrPayPoint1Add3;
            myCommand.Parameters.Add(parameterMtrPayPoint1Add3);

            SqlParameter parameterMtrPayPoint1Add4 = new SqlParameter("@MtrPayPoint1Add4", SqlDbType.VarChar, 100);
            parameterMtrPayPoint1Add4.Value = MtrPayPoint1Add4;
            myCommand.Parameters.Add(parameterMtrPayPoint1Add4);

            SqlParameter parameterMtrPayPoint1Code = new SqlParameter("@MtrPayPoint1Code", SqlDbType.VarChar, 5);
            parameterMtrPayPoint1Code.Value = MtrPayPoint1Code;
            myCommand.Parameters.Add(parameterMtrPayPoint1Code);

            SqlParameter parameterMtrPayPoint1Tel = new SqlParameter("@MtrPayPoint1Tel", SqlDbType.VarChar, 30);
            parameterMtrPayPoint1Tel.Value = MtrPayPoint1Tel;
            myCommand.Parameters.Add(parameterMtrPayPoint1Tel);

            SqlParameter parameterMtrPayPoint1Fax = new SqlParameter("@MtrPayPoint1Fax", SqlDbType.VarChar, 30);
            parameterMtrPayPoint1Fax.Value = MtrPayPoint1Fax;
            myCommand.Parameters.Add(parameterMtrPayPoint1Fax);

            SqlParameter parameterMtrPayPoint1Email = new SqlParameter("@MtrPayPoint1Email", SqlDbType.VarChar, 50);
            parameterMtrPayPoint1Email.Value = MtrPayPoint1Email;
            myCommand.Parameters.Add(parameterMtrPayPoint1Email);

            SqlParameter parameterMtrPayPoint2Add1 = new SqlParameter("@MtrPayPoint2Add1", SqlDbType.VarChar, 100);
            parameterMtrPayPoint2Add1.Value = MtrPayPoint2Add1;
            myCommand.Parameters.Add(parameterMtrPayPoint2Add1);

            SqlParameter parameterMtrPayPoint2Add2 = new SqlParameter("@MtrPayPoint2Add2", SqlDbType.VarChar, 100);
            parameterMtrPayPoint2Add2.Value = MtrPayPoint2Add2;
            myCommand.Parameters.Add(parameterMtrPayPoint2Add2);

            SqlParameter parameterMtrPayPoint2Add3 = new SqlParameter("@MtrPayPoint2Add3", SqlDbType.VarChar, 100);
            parameterMtrPayPoint2Add3.Value = MtrPayPoint2Add3;
            myCommand.Parameters.Add(parameterMtrPayPoint2Add3);

            SqlParameter parameterMtrPayPoint2Add4 = new SqlParameter("@MtrPayPoint2Add4", SqlDbType.VarChar, 100);
            parameterMtrPayPoint2Add4.Value = MtrPayPoint2Add4;
            myCommand.Parameters.Add(parameterMtrPayPoint2Add4);

            SqlParameter parameterMtrPayPoint2Code = new SqlParameter("@MtrPayPoint2Code", SqlDbType.VarChar, 5);
            parameterMtrPayPoint2Code.Value = MtrPayPoint2Code;
            myCommand.Parameters.Add(parameterMtrPayPoint2Code);

            SqlParameter parameterMtrPayPoint2Tel = new SqlParameter("@MtrPayPoint2Tel", SqlDbType.VarChar, 30);
            parameterMtrPayPoint2Tel.Value = MtrPayPoint2Tel;
            myCommand.Parameters.Add(parameterMtrPayPoint2Tel);

            SqlParameter parameterMtrPayPoint2Fax = new SqlParameter("@MtrPayPoint2Fax", SqlDbType.VarChar, 30);
            parameterMtrPayPoint2Fax.Value = MtrPayPoint2Fax;
            myCommand.Parameters.Add(parameterMtrPayPoint2Fax);

            SqlParameter parameterMtrPayPoint2Email = new SqlParameter("@MtrPayPoint2Email", SqlDbType.VarChar, 50);
            parameterMtrPayPoint2Email.Value = MtrPayPoint2Email;
            myCommand.Parameters.Add(parameterMtrPayPoint2Email);

            //SqlParameter parameterMtrDocumentFrom = new SqlParameter("@MtrDocumentFrom", SqlDbType.VarChar, 255);
            //parameterMtrDocumentFrom.Value = MtrDocumentFrom;
            //myCommand.Parameters.Add(parameterMtrDocumentFrom);

            //SqlParameter parameterMtrReceiptFrom = new SqlParameter("@MtrReceiptFrom", SqlDbType.VarChar, 255);
            //parameterMtrReceiptFrom.Value = MtrReceiptFrom;
            //myCommand.Parameters.Add(parameterMtrReceiptFrom);

            SqlParameter parameterMtrReceiptEnglishAct = new SqlParameter("@MtrReceiptEnglishAct", SqlDbType.VarChar, 255);
            parameterMtrReceiptEnglishAct.Value = MtrReceiptEnglishAct;
            myCommand.Parameters.Add(parameterMtrReceiptEnglishAct);

            SqlParameter parameterMtrReceiptAfrikaansAct = new SqlParameter("@MtrReceiptAfrikaansAct", SqlDbType.VarChar, 255);
            parameterMtrReceiptAfrikaansAct.Value = MtrReceiptAfrikaansAct;
            myCommand.Parameters.Add(parameterMtrReceiptAfrikaansAct);

            SqlParameter parameterMtrDepartName = new SqlParameter("@MtrDepartName", SqlDbType.VarChar, 100);
            parameterMtrDepartName.Value = MtrDepartName;
            myCommand.Parameters.Add(parameterMtrDepartName);

            SqlParameter parameterMtrDocumentFromEnglish = new SqlParameter("@MtrDocumentFromEnglish", SqlDbType.VarChar, 255);
            parameterMtrDocumentFromEnglish.Value = MtrDocumentFromEnglish;
            myCommand.Parameters.Add(parameterMtrDocumentFromEnglish);

            SqlParameter parameterMtrDocumentFromAfrikaans = new SqlParameter("@MtrDocumentFromAfrikaans", SqlDbType.VarChar, 255);
            parameterMtrDocumentFromAfrikaans.Value = MtrDocumentFromAfrikaans;
            myCommand.Parameters.Add(parameterMtrDocumentFromAfrikaans);

            SqlParameter parameterMtrFooter = new SqlParameter("@MtrFooter", SqlDbType.VarChar, 100);
            parameterMtrFooter.Value = MtrFooter;
            myCommand.Parameters.Add(parameterMtrFooter);

            SqlParameter parameterMtrLogo = new SqlParameter("@MtrLogo", SqlDbType.Image);
            parameterMtrLogo.Value = MtrLogo;
            myCommand.Parameters.Add(parameterMtrLogo);

            SqlParameter parameterMtrNameAfri = new SqlParameter("@MtrNameAfri", SqlDbType.VarChar, 100);
            parameterMtrNameAfri.Value = MtrNameAfri;
            myCommand.Parameters.Add(parameterMtrNameAfri);

            SqlParameter parameterMtrIntNo = new SqlParameter("@MtrIntNo", SqlDbType.VarChar, 100);
            parameterMtrIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterMtrIntNo);
            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                // Calculate the CustomerID using Output Param from SPROC
                int addMtrIntNo = Convert.ToInt32(parameterMtrIntNo.Value);
                return addMtrIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public SqlDataReader GetMetroList(string orderBy)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("MetroList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterOrderBy = new SqlParameter("@OrderBy", SqlDbType.VarChar, 10);
            parameterOrderBy.Value = orderBy;
            myCommand.Parameters.Add(parameterOrderBy);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        public DataSet GetMetroListDS(string orderBy)
        {
            SqlDataAdapter sqlDAMetro = new SqlDataAdapter();
            DataSet dsMetro = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAMetro.SelectCommand = new SqlCommand();
            sqlDAMetro.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAMetro.SelectCommand.CommandText = "MetroList";

            // Mark the Command as a SPROC
            sqlDAMetro.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterOrderBy = new SqlParameter("@OrderBy", SqlDbType.VarChar, 10);
            parameterOrderBy.Value = orderBy;
            sqlDAMetro.SelectCommand.Parameters.Add(parameterOrderBy);

            // Execute the command and close the connection
            sqlDAMetro.Fill(dsMetro);
            sqlDAMetro.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsMetro;
        }

        public int UpdateMetro(int regIntNo, string mtrCode, string mtrNo, string mtrName, string mtrPostAddr1, string mtrPostAddr2, string mtrPostAddr3, string mtrPostCode, string mtrTel, string mtrFax, string mtrEmail, string user, int mtrIntNo,
                         string MtrNoticePaymentInfo, string MtrNoticeIssuedByInfo,
 string MtrPhysAdd1,
 string MtrPhysAdd2,
 string MtrPhysAdd3,
 string MtrPhysAdd4,
 string MtrPhysCode,
 string MtrPayPoint1Add1,
 string MtrPayPoint1Add2,
 string MtrPayPoint1Add3,
 string MtrPayPoint1Add4,
 string MtrPayPoint1Code,
 string MtrPayPoint1Tel,
 string MtrPayPoint1Fax,
 string MtrPayPoint1Email,
 string MtrPayPoint2Add1,
 string MtrPayPoint2Add2,
 string MtrPayPoint2Add3,
 string MtrPayPoint2Add4,
 string MtrPayPoint2Code,
 string MtrPayPoint2Tel,
 string MtrPayPoint2Fax,
 string MtrPayPoint2Email,
 //string MtrDocumentFrom,
 //string MtrReceiptFrom,
 string MtrReceiptEnglishAct,
 string MtrReceiptAfrikaansAct,
 string MtrDepartName,
 string MtrDocumentFromEnglish,
 string MtrDocumentFromAfrikaans,
 string MtrFooter, byte[] MtrLogo, string MtrNameAfri)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("MetroUpdate", myConnection);
            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            // Add Parameters to SPROC
            SqlParameter parameterRegIntNo = new SqlParameter("@RegIntNo", SqlDbType.Int);
            parameterRegIntNo.Value = regIntNo;
            myCommand.Parameters.Add(parameterRegIntNo);

            SqlParameter parameterMtrCode = new SqlParameter("@MtrCode", SqlDbType.Char, 3);
            parameterMtrCode.Value = mtrCode;
            myCommand.Parameters.Add(parameterMtrCode);

            SqlParameter parameterMtrNo = new SqlParameter("@MtrNo", SqlDbType.VarChar, 6);
            parameterMtrNo.Value = mtrNo;
            myCommand.Parameters.Add(parameterMtrNo);

            SqlParameter parameterMtrName = new SqlParameter("@MtrName", SqlDbType.VarChar, 100);
            parameterMtrName.Value = mtrName;
            myCommand.Parameters.Add(parameterMtrName);

            SqlParameter parameterMtrPostAddr1 = new SqlParameter("@MtrPostAddr1", SqlDbType.VarChar, 100);
            parameterMtrPostAddr1.Value = mtrPostAddr1;
            myCommand.Parameters.Add(parameterMtrPostAddr1);

            SqlParameter parameterMtrPostAddr2 = new SqlParameter("@MtrPostAddr2", SqlDbType.VarChar, 100);
            parameterMtrPostAddr2.Value = mtrPostAddr2;
            myCommand.Parameters.Add(parameterMtrPostAddr2);

            SqlParameter parameterMtrPostAddr3 = new SqlParameter("@MtrPostAddr3", SqlDbType.VarChar, 100);
            parameterMtrPostAddr3.Value = mtrPostAddr3;
            myCommand.Parameters.Add(parameterMtrPostAddr3);

            SqlParameter parameterMtrPostCode = new SqlParameter("@MtrPostCode", SqlDbType.VarChar, 4);
            parameterMtrPostCode.Value = mtrPostCode;
            myCommand.Parameters.Add(parameterMtrPostCode);

            SqlParameter parameterMtrTel = new SqlParameter("@MtrTel", SqlDbType.VarChar, 30);
            parameterMtrTel.Value = mtrTel;
            myCommand.Parameters.Add(parameterMtrTel);

            SqlParameter parameterMtrFax = new SqlParameter("@MtrFax", SqlDbType.VarChar, 30);
            parameterMtrFax.Value = mtrFax;
            myCommand.Parameters.Add(parameterMtrFax);

            SqlParameter parameterMtrEmail = new SqlParameter("@MtrEmail", SqlDbType.VarChar, 50);
            parameterMtrEmail.Value = mtrEmail;
            myCommand.Parameters.Add(parameterMtrEmail);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = user;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterMtrNoticePaymentInfo = new SqlParameter("@MtrNoticePaymentInfo", SqlDbType.VarChar);
            parameterMtrNoticePaymentInfo.Value = MtrNoticePaymentInfo;
            myCommand.Parameters.Add(parameterMtrNoticePaymentInfo);

            SqlParameter parameterMtrNoticeIssuedByInfo = new SqlParameter("@MtrNoticeIssuedByInfo", SqlDbType.VarChar);
            parameterMtrNoticeIssuedByInfo.Value = MtrNoticeIssuedByInfo;
            myCommand.Parameters.Add(parameterMtrNoticeIssuedByInfo);

            SqlParameter parameterMtrPhysAdd1 = new SqlParameter("@MtrPhysAdd1", SqlDbType.VarChar, 100);
            parameterMtrPhysAdd1.Value = MtrPhysAdd1;
            myCommand.Parameters.Add(parameterMtrPhysAdd1);

            SqlParameter parameterMtrPhysAdd2 = new SqlParameter("@MtrPhysAdd2", SqlDbType.VarChar, 100);
            parameterMtrPhysAdd2.Value = MtrPhysAdd2;
            myCommand.Parameters.Add(parameterMtrPhysAdd2);

            SqlParameter parameterMtrPhysAdd3 = new SqlParameter("@MtrPhysAdd3", SqlDbType.VarChar, 100);
            parameterMtrPhysAdd3.Value = MtrPhysAdd3;
            myCommand.Parameters.Add(parameterMtrPhysAdd3);

            SqlParameter parameterMtrPhysAdd4 = new SqlParameter("@MtrPhysAdd4", SqlDbType.VarChar, 100);
            parameterMtrPhysAdd4.Value = MtrPhysAdd4;
            myCommand.Parameters.Add(parameterMtrPhysAdd4);

            SqlParameter parameterMtrPhysCode = new SqlParameter("@MtrPhysCode", SqlDbType.VarChar, 5);
            parameterMtrPhysCode.Value = MtrPhysCode;
            myCommand.Parameters.Add(parameterMtrPhysCode);

            SqlParameter parameterMtrPayPoint1Add1 = new SqlParameter("@MtrPayPoint1Add1", SqlDbType.VarChar, 100);
            parameterMtrPayPoint1Add1.Value = MtrPayPoint1Add1;
            myCommand.Parameters.Add(parameterMtrPayPoint1Add1);

            SqlParameter parameterMtrPayPoint1Add2 = new SqlParameter("@MtrPayPoint1Add2", SqlDbType.VarChar, 100);
            parameterMtrPayPoint1Add2.Value = MtrPayPoint1Add2;
            myCommand.Parameters.Add(parameterMtrPayPoint1Add2);

            SqlParameter parameterMtrPayPoint1Add3 = new SqlParameter("@MtrPayPoint1Add3", SqlDbType.VarChar, 100);
            parameterMtrPayPoint1Add3.Value = MtrPayPoint1Add3;
            myCommand.Parameters.Add(parameterMtrPayPoint1Add3);

            SqlParameter parameterMtrPayPoint1Add4 = new SqlParameter("@MtrPayPoint1Add4", SqlDbType.VarChar, 100);
            parameterMtrPayPoint1Add4.Value = MtrPayPoint1Add4;
            myCommand.Parameters.Add(parameterMtrPayPoint1Add4);

            SqlParameter parameterMtrPayPoint1Code = new SqlParameter("@MtrPayPoint1Code", SqlDbType.VarChar, 5);
            parameterMtrPayPoint1Code.Value = MtrPayPoint1Code;
            myCommand.Parameters.Add(parameterMtrPayPoint1Code);

            SqlParameter parameterMtrPayPoint1Tel = new SqlParameter("@MtrPayPoint1Tel", SqlDbType.VarChar, 30);
            parameterMtrPayPoint1Tel.Value = MtrPayPoint1Tel;
            myCommand.Parameters.Add(parameterMtrPayPoint1Tel);

            SqlParameter parameterMtrPayPoint1Fax = new SqlParameter("@MtrPayPoint1Fax", SqlDbType.VarChar, 30);
            parameterMtrPayPoint1Fax.Value = MtrPayPoint1Fax;
            myCommand.Parameters.Add(parameterMtrPayPoint1Fax);

            SqlParameter parameterMtrPayPoint1Email = new SqlParameter("@MtrPayPoint1Email", SqlDbType.VarChar, 50);
            parameterMtrPayPoint1Email.Value = MtrPayPoint1Email;
            myCommand.Parameters.Add(parameterMtrPayPoint1Email);

            SqlParameter parameterMtrPayPoint2Add1 = new SqlParameter("@MtrPayPoint2Add1", SqlDbType.VarChar, 100);
            parameterMtrPayPoint2Add1.Value = MtrPayPoint2Add1;
            myCommand.Parameters.Add(parameterMtrPayPoint2Add1);

            SqlParameter parameterMtrPayPoint2Add2 = new SqlParameter("@MtrPayPoint2Add2", SqlDbType.VarChar, 100);
            parameterMtrPayPoint2Add2.Value = MtrPayPoint2Add2;
            myCommand.Parameters.Add(parameterMtrPayPoint2Add2);

            SqlParameter parameterMtrPayPoint2Add3 = new SqlParameter("@MtrPayPoint2Add3", SqlDbType.VarChar, 100);
            parameterMtrPayPoint2Add3.Value = MtrPayPoint2Add3;
            myCommand.Parameters.Add(parameterMtrPayPoint2Add3);

            SqlParameter parameterMtrPayPoint2Add4 = new SqlParameter("@MtrPayPoint2Add4", SqlDbType.VarChar, 100);
            parameterMtrPayPoint2Add4.Value = MtrPayPoint2Add4;
            myCommand.Parameters.Add(parameterMtrPayPoint2Add4);

            SqlParameter parameterMtrPayPoint2Code = new SqlParameter("@MtrPayPoint2Code", SqlDbType.VarChar, 5);
            parameterMtrPayPoint2Code.Value = MtrPayPoint2Code;
            myCommand.Parameters.Add(parameterMtrPayPoint2Code);

            SqlParameter parameterMtrPayPoint2Tel = new SqlParameter("@MtrPayPoint2Tel", SqlDbType.VarChar, 30);
            parameterMtrPayPoint2Tel.Value = MtrPayPoint2Tel;
            myCommand.Parameters.Add(parameterMtrPayPoint2Tel);

            SqlParameter parameterMtrPayPoint2Fax = new SqlParameter("@MtrPayPoint2Fax", SqlDbType.VarChar, 30);
            parameterMtrPayPoint2Fax.Value = MtrPayPoint2Fax;
            myCommand.Parameters.Add(parameterMtrPayPoint2Fax);

            SqlParameter parameterMtrPayPoint2Email = new SqlParameter("@MtrPayPoint2Email", SqlDbType.VarChar, 50);
            parameterMtrPayPoint2Email.Value = MtrPayPoint2Email;
            myCommand.Parameters.Add(parameterMtrPayPoint2Email);

            //SqlParameter parameterMtrDocumentFrom = new SqlParameter("@MtrDocumentFrom", SqlDbType.VarChar, 255);
            //parameterMtrDocumentFrom.Value = MtrDocumentFrom;
            //myCommand.Parameters.Add(parameterMtrDocumentFrom);

            //SqlParameter parameterMtrReceiptFrom = new SqlParameter("@MtrReceiptFrom", SqlDbType.VarChar, 255);
            //parameterMtrReceiptFrom.Value = MtrReceiptFrom;
            //myCommand.Parameters.Add(parameterMtrReceiptFrom);

            SqlParameter parameterMtrReceiptEnglishAct = new SqlParameter("@MtrReceiptEnglishAct", SqlDbType.VarChar, 255);
            parameterMtrReceiptEnglishAct.Value = MtrReceiptEnglishAct;
            myCommand.Parameters.Add(parameterMtrReceiptEnglishAct);

            SqlParameter parameterMtrReceiptAfrikaansAct = new SqlParameter("@MtrReceiptAfrikaansAct", SqlDbType.VarChar, 255);
            parameterMtrReceiptAfrikaansAct.Value = MtrReceiptAfrikaansAct;
            myCommand.Parameters.Add(parameterMtrReceiptAfrikaansAct);

            SqlParameter parameterMtrDepartName = new SqlParameter("@MtrDepartName", SqlDbType.VarChar, 100);
            parameterMtrDepartName.Value = MtrDepartName;
            myCommand.Parameters.Add(parameterMtrDepartName);

            SqlParameter parameterMtrDocumentFromEnglish = new SqlParameter("@MtrDocumentFromEnglish", SqlDbType.VarChar, 255);
            parameterMtrDocumentFromEnglish.Value = MtrDocumentFromEnglish;
            myCommand.Parameters.Add(parameterMtrDocumentFromEnglish);

            SqlParameter parameterMtrDocumentFromAfrikaans = new SqlParameter("@MtrDocumentFromAfrikaans", SqlDbType.VarChar, 255);
            parameterMtrDocumentFromAfrikaans.Value = MtrDocumentFromAfrikaans;
            myCommand.Parameters.Add(parameterMtrDocumentFromAfrikaans);

            SqlParameter parameterMtrFooter = new SqlParameter("@MtrFooter", SqlDbType.VarChar, 100);
            parameterMtrFooter.Value = MtrFooter;
            myCommand.Parameters.Add(parameterMtrFooter);

            SqlParameter parameterMtrLogo = new SqlParameter("@MtrLogo", SqlDbType.Image);
            parameterMtrLogo.Value = MtrLogo;
            myCommand.Parameters.Add(parameterMtrLogo);

            SqlParameter parameterMtrNameAfri = new SqlParameter("@MtrNameAfri", SqlDbType.VarChar, 100);
            parameterMtrNameAfri.Value = MtrNameAfri;
            myCommand.Parameters.Add(parameterMtrNameAfri);

            SqlParameter parameterMtrIntNo = new SqlParameter("@MtrIntNo", SqlDbType.Int, 4);
            parameterMtrIntNo.Direction = ParameterDirection.InputOutput;
            parameterMtrIntNo.Value = mtrIntNo;
            myCommand.Parameters.Add(parameterMtrIntNo);
            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                int updMtrIntNo = (int)myCommand.Parameters["@MtrIntNo"].Value;
                return updMtrIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int DeleteMetro(int mtrIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("MetroDelete", myConnection);
            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            // Add Parameters to SPROC
            SqlParameter parameterMtrIntNo = new SqlParameter("@MtrIntNo", SqlDbType.Int, 4);
            parameterMtrIntNo.Value = mtrIntNo;
            parameterMtrIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterMtrIntNo);
            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                int delMtrIntNo = (int)parameterMtrIntNo.Value;
                return delMtrIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }
    }
}

