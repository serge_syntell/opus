using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using Stalberg.TMS;

namespace Stalberg.TMS.Data
{
    public class CiprusAarto61Record    //Response record - up file
    {
        public string recType;          // N2 = 61
        public string interfaceType;    // A1 = C
        public string versionNo;        // N2 = 05          //03/04
        public string processCode;      // N2
        public string severityCode;     // N1
        public string recordNumber;     // N6
        public string legislation;      // N1 - 1 = Aarto; 2 = Civitas
        public string noticeNoType;     // N1 - 1 = Aarto Notice No; 2 = Ciprus Notice No
        public string noticeNumber;     // N16
        public string issuingAuthority; // A40
        public string officeNatisNo;    // A9
        public string officerName;      // A40
        public string officerInit;      // A5
        public string penaltyAmount;    // N8 = RRRRRRcc
        public string discountAmount;   // N8 = RRRRRRcc
        public string discountedAmount; // N8 = RRRRRRcc
        public string chargeType;       // A10 = 'Major' or 'Minor'
        public string demeritPoints;    // N4
        public string tranData;         // A50 first 9 characters are the referenceno/notintno
        public string uniqueDocID;      // A12
        public string infringementRecord;    //A973 = contents of infringement record
        //dls 090325 - added in version 04
        public string filler1;          // A146     changed 2009-07-03    A200 - reserved for future use
        //Changed by Tod Zhang on the 090701
        public string adjOfficerSurname;    //A40
        public string adjOfficerInitials;   //A5
        public string adjOfficerInfrastructureNo;       //A9

        public string ciprusLoadDate;   // N8 - YYYYMMDD
    }

    public class CiprusAarto66Record         //Infringement record - Down file
    {
        //added elements to struct
        public string recType;          // N2 = 66
        public string interfaceType;    // A1 = C
        public string versionNo;        // N2 = 05          //03
        public string supplierCode;     // A1 = Q 
        public string cameraID;         // N3
        public string autNo;            // N3 = autNo
        public string Filler1;          // A7 - NOT IN USE - used to be NoticeNo: N7 = DocType(2) + Serial No (7) - padeleft with zeroes
        public string filmNo;           // N8
        public string refNo;            // N9 = frame intno
        public string firstSpeed;       // N4 = 0789 = 78.9 km/h
        public string secondSpeed;      // N4 = 1200 = 120.0 km/h
        public string officerNo;        // A9
        public string cameraLoc;        // N6
        public string courtNo;          // N6
        public string offenceDate;      // D8 = CCYYMMDD
        public string Filler2;          // A4 - NOT IN USE  - used to be OffenceTime 
        public string Filler3;          // A2 - NOT IN USE - used to be vehMCode
        public string Filler4;          // A2 - NOT IN USE - used to be vehTCode
        public string regNo;            // A10
        public string travelDirection;  // A1
        public string offenceCode;      // N6
        public string typeVehOwner;     // N1 = (1) private, (2) representative of a business, (3) Business, no rep
        public string idNo;             // N13
        public string surname;          // A30
        public string forenames;        // A40  - optional (default = sapces)
        public string initials;         // A5
        public string businessName;     // A30
        public string natureOfPerson;   // N2  DP 20110317
        public string Filler5;          // A28
        public string gender;           // N2 - optional (default = 00)
        public string organisationType; // A30 - optiona (default = spaces)
        public string dateOfBirth;      // D8 - CCYYMMDD optional (default = 00000000)
        public string streetAddrLine1;  // A40
        public string streetAddrLine2;  // A40
        public string streetAddrLine3;  // A40
        public string streetAddrLine4;  // A40
        public string streetAddrLine5;  // A40
        public string streetAddrCode;   // N4
        public string postAddrLine1;    // A40
        public string postAddrLine2;    // A40
        public string postAddrLine3;    // A40
        public string postAddrLine4;    // A40
        public string postAddrLine5;    // A40
        public string postAddrCode;     // N4
        public string telHome;          // A12 - optional (default = spaces)
        public string telWork;          // A12 - optional (default = spaces)
        public string fax;              // A12 - optional (default = spaces)
        public string cell;             // A12 - optional (default = spaces)
        public string email;            // A30 - optional (default = spaces)
        public string natisVehMCode;    // A3
        public string natisVehDescrCode; //A2
        public string idType;         // N2
        public string countryOfIssue;   // N3 - optional (default = 000)
        public string licenceDiscNo;    // A20
        public string prDPCode;         // A20 - optional (default = spaces)
        public string driverLicenceCode;// A3 - optional (default = spaces)
        public string operatorCardNo;   // A20 - optional (default = spaces)
        public string vehicleGVM;       // N8 - optional (default = 00000000)
        public string offenceTime;      // N6 = HHMMSS
        public string amberTime;        // N6 = SSSSss (Seconds & milli-seconds)
        public string redTime;          // N6 = SSSSss (Seconds & milli-seconds)
        public string dateCaptured;     // D8 = CCYYMMDD
        public string dateVerified;     // D8 = CCYYMMDD
        public string dateNatisReceived;// D8 = CCYYMMDD
        public string dateAdjudicated;  // D8 = CCYYMMDD
        public string adjOfficerNo;     // A9
        public string tranData;         //A50

        //dls 090128 - no images at this stage!
        //public string sceneImage;       //A?? = string version of Base64Binary encoding of main image
        //public string regNoImage;       //A?? = string version of Base64Binary encoding of regNo image
    }

    public class CiprusAartoData
    {
        // Fields
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="CiprusData"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public CiprusAartoData(string connectionString)
        {
            this.connectionString = connectionString;
        }

        // 2013-07-19 comment by Henry for useless
        //public DataSet GetCiprusInfringementData(int autIntNo, string camUnitID, int statusLoaded, int statusFixed)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("NoticeContraventionData_CA", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;
        //    //dls 070720 - allow for timeouts
        //    myCommand.CommandTimeout = 0;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
        //    parameterCamUnitID.Value = camUnitID;
        //    myCommand.Parameters.Add(parameterCamUnitID);

        //    SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
        //    parameterStatusLoaded.Value = statusLoaded;
        //    myCommand.Parameters.Add(parameterStatusLoaded);

        //    SqlParameter parameterStatusFixed = new SqlParameter("@StatusFixed", SqlDbType.Int, 4);
        //    parameterStatusFixed.Value = statusFixed;
        //    myCommand.Parameters.Add(parameterStatusFixed);

        //    SqlDataAdapter da = new SqlDataAdapter(myCommand);
        //    DataSet ds = new DataSet();
        //    da.Fill(ds);

        //    return ds;
        //}

    }
}
