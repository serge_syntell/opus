using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    //*******************************************************
    //
    // AccountDetails Class
    //
    // A simple data class that encapsulates details about a particular loc 
    //
    //*******************************************************
    [Serializable]
    public class WOACancellationDetails
    {
        //public Int32 AccIntNo;
        //public Int32 AutIntNo;
        //public string AccountNo;
        public string WOANumber;
        public string SummonsNo;
    }

    //*******************************************************
    //
    // AccountDB Class
    //
    //*******************************************************
    public class WoaCancellationDB
    {
        private readonly string connectionString = string.Empty;
        private string error = string.Empty;

        public WoaCancellationDB(string vConstr)
        {
            this.connectionString = vConstr;
        }

        public string Error
        {
            get { return error; }
            set { error = value; }
        }

        //*******************************************************
        //// 2013-07-19 comment by Henry for useless
        //*******************************************************
        //public WOACancellationDetails GetSummonsNumber(string woa)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("GetSummonsNoByWOANumber", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterWoa = new SqlParameter("@WOANumber", SqlDbType.VarChar, 50);
        //    parameterWoa.Value = woa;
        //    myCommand.Parameters.Add(parameterWoa);

        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    WOACancellationDetails woaDetails = new WOACancellationDetails();

        //    while (result.Read())
        //    {
        //        // Populate Struct using Output Params from SPROC
        //        //woaDetails.WOANumber = result["WOANumber"].ToString();
        //        woaDetails.SummonsNo = result["SummonsNo"].ToString();
        //        //myAccountDetails.AccountNo = result["AccountNo"].ToString();
        //        //myAccountDetails.AccName = result["AccName"].ToString();
        //        //myAccountDetails.AccVATStatus = result["AccVATStatus"].ToString();
        //    }
        //    result.Close();
        //    return woaDetails;
        //}

        //public int AddAccount(int autIntNo, string accountNo, string accName, string accVATStatus, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("AccountAdd", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterAccountNo = new SqlParameter("@AccountNo", SqlDbType.VarChar, 20);
        //    parameterAccountNo.Value = accountNo;
        //    myCommand.Parameters.Add(parameterAccountNo);

        //    SqlParameter parameterAccName = new SqlParameter("@AccName", SqlDbType.VarChar, 50);
        //    parameterAccName.Value = accName;
        //    myCommand.Parameters.Add(parameterAccName);

        //    SqlParameter parameterAccVATStatus = new SqlParameter("@AccVATStatus", SqlDbType.Char, 1);
        //    parameterAccVATStatus.Value = accVATStatus;
        //    myCommand.Parameters.Add(parameterAccVATStatus);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterAccIntNo = new SqlParameter("@AccIntNo", SqlDbType.Int, 4);
        //    parameterAccIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterAccIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int accIntNo = Convert.ToInt32(parameterAccIntNo.Value);

        //        return accIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        //public int GetAccount(int autIntNo, int ctIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("Acc_CTGetAccIntNo", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    myCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    myCommand.Parameters.Add("@CTIntNo", SqlDbType.Int, 4).Value = ctIntNo;

        //    SqlParameter parameterAccIntNo = new SqlParameter("@AccIntNo", SqlDbType.Int, 4);
        //    parameterAccIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterAccIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int accIntNo = Convert.ToInt32(parameterAccIntNo.Value);

        //        return accIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        ///// <summary>
        ///// Gets the list of ledger accounts for the supplied authority.
        ///// </summary>
        ///// <param name="autIntNo">The authority int no.</param>
        ///// <returns></returns>
        //public SqlDataReader GetAccountList(int autIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("AccountList", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the data reader result
        //    return result;
        //}

        // 20090122  
        public DataSet GetWOAList(int autIntNo, string woaNumb)
        {
            SqlDataAdapter sqlDAWoa = new SqlDataAdapter();
            DataSet dsWoa = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAWoa.SelectCommand = new SqlCommand();
            sqlDAWoa.SelectCommand.Connection = new SqlConnection(connectionString);
            sqlDAWoa.SelectCommand.CommandText = "GetWOADetailsByWOANumber";

            // Mark the Command as a SPROC
            sqlDAWoa.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDAWoa.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterWOA = new SqlParameter("@WOANumber", SqlDbType.VarChar, 50);
            parameterWOA.Value = woaNumb;
            sqlDAWoa.SelectCommand.Parameters.Add(parameterWOA);

            // Execute the command and close the connection
            sqlDAWoa.Fill(dsWoa);
            sqlDAWoa.SelectCommand.Connection.Dispose();
            // Return the dataset result
            return dsWoa;
        }
        // 2013-07-19 comment by Henry for useless
        // 20050715 converted to account
        //public void updateSummons(string sumReasonCanceled, DateTime sumDateCancel, DateTime sumWithDrawnDate, string sumPProsecutor, string sumSummonsNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("UpdateSummonsByWOARowDelete", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterReasonCanceld = new SqlParameter("@SumReasonCancelled", SqlDbType.VarChar, 100);
        //    parameterReasonCanceld.Value = sumReasonCanceled;
        //    myCommand.Parameters.Add(parameterReasonCanceld);

        //    SqlParameter parameterDateCanceld = new SqlParameter("@SumDateCancelled", SqlDbType.DateTime);
        //    parameterDateCanceld.Value = sumDateCancel;
        //    myCommand.Parameters.Add(parameterDateCanceld);

        //    SqlParameter parameterWithDrawDate = new SqlParameter("@SumWithdrawnDate", SqlDbType.DateTime);
        //    parameterWithDrawDate.Value = sumWithDrawnDate;
        //    myCommand.Parameters.Add(parameterWithDrawDate);

        //    SqlParameter parameterPProsecutor = new SqlParameter("@SumPublicProsecutor", SqlDbType.VarChar, 100);
        //    parameterPProsecutor.Value = sumPProsecutor;
        //    myCommand.Parameters.Add(parameterPProsecutor);

        //    SqlParameter parameterSummonsNo = new SqlParameter("@SummonsNo", SqlDbType.VarChar, 50);
        //    parameterSummonsNo.Value = sumSummonsNo;
        //    myCommand.Parameters.Add(parameterSummonsNo);

        //    //SqlParameter parameterAccIntNo = new SqlParameter("@AccIntNo", SqlDbType.Int);
        //    //parameterAccIntNo.Direction = ParameterDirection.InputOutput;
        //    //parameterAccIntNo.Value = accIntNo;
        //    //myCommand.Parameters.Add(parameterAccIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        //accIntNo = (int)myCommand.Parameters["@AccIntNo"].Value;

        //        //return accIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        //return 0;
        //    }
        //}
        // 2013-07-19 comment by Henry for useless
        //public void updateSumCharge(string woaNumber, float sumChContemptAmt)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("WithDrawContemptAmountFromSumCharge", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterWoaNumb = new SqlParameter("@WOANumber", SqlDbType.VarChar, 50);
        //    parameterWoaNumb.Value = woaNumber;
        //    myCommand.Parameters.Add(parameterWoaNumb);

        //    SqlParameter parameterSumConAmount = new SqlParameter("@SChContemptAmount", SqlDbType.Money);
        //    parameterSumConAmount.Value = sumChContemptAmt;
        //    myCommand.Parameters.Add(parameterSumConAmount);


        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        //accIntNo = (int)myCommand.Parameters["@AccIntNo"].Value;

        //        //return accIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        //return 0;
        //    }
        //}

        // 2013-07-19 comment by Henry for useless
        //public void updateCharge(string woaNumber, int chgStatus, float ChgContemptAmt)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("UpdateChargeSetContemptAmount", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterWoaNumb = new SqlParameter("@WOANumber", SqlDbType.VarChar, 50);
        //    parameterWoaNumb.Value = woaNumber;
        //    myCommand.Parameters.Add(parameterWoaNumb);

        //    SqlParameter parameterChgConAmount = new SqlParameter("@ChgContemptCourt", SqlDbType.Money);
        //    parameterChgConAmount.Value = ChgContemptAmt;
        //    myCommand.Parameters.Add(parameterChgConAmount);

        //    SqlParameter paramChgStatus = new SqlParameter("@ChargeStatus", SqlDbType.Int, 4);
        //    paramChgStatus.Value = chgStatus;
        //    myCommand.Parameters.Add(paramChgStatus);



        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        //accIntNo = (int)myCommand.Parameters["@AccIntNo"].Value;

        //        //return accIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        //return 0;
        //    }
        //}
        // 2013-07-19 comment by Henry for useless
        //public void updateSumChargeStatus(string woaNumber, int sumChgStatus)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(connectionString);
        //    SqlCommand cmd = new SqlCommand("updateSumChargeStatus", con);

        //    // Mark the Command as a SPROC
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterWoaNumb = new SqlParameter("@WOANumber", SqlDbType.VarChar, 50);
        //    parameterWoaNumb.Value = woaNumber;
        //    cmd.Parameters.Add(parameterWoaNumb);

        //    SqlParameter paramChgStatus = new SqlParameter("@SumChargeStatus", SqlDbType.Int, 4);
        //    paramChgStatus.Value = sumChgStatus;
        //    cmd.Parameters.Add(paramChgStatus);

        //    try
        //    {
        //        con.Open();
        //        cmd.ExecuteNonQuery();
        //        con.Dispose();
        //    }
        //    catch (Exception e)
        //    {
        //        con.Dispose();
        //        string msg = e.Message;
        //        //return 0;
        //    }
        //}

        // 2013-07-19 comment by Henry for useless
        //public void DeleteWOARow(string woa)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("DeleteRowByWOANumber", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterWOA = new SqlParameter("@WOANumber", SqlDbType.VarChar, 50);
        //    parameterWOA.Value = woa;
        //    //parameterAccIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterWOA);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        //// Calculate the CustomerID using Output Param from SPROC
        //        //accIntNo = (int)parameterAccIntNo.Value;

        //        //return accIntNo.ToString();
        //    }
        //    catch
        //    {
        //        myConnection.Dispose();
        //        //return String.Empty;
        //    }
        //}

        //public int AddAcc_CT(int accIntNo, int ctIntNo, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("Acc_CTAdd", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAccIntNo = new SqlParameter("@AccIntNo", SqlDbType.Int, 4);
        //    parameterAccIntNo.Value = accIntNo;
        //    myCommand.Parameters.Add(parameterAccIntNo);

        //    SqlParameter parameterCTIntNo = new SqlParameter("@CTIntNo", SqlDbType.Int, 4);
        //    parameterCTIntNo.Value = ctIntNo;
        //    myCommand.Parameters.Add(parameterCTIntNo);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterAcCtIntNo = new SqlParameter("@AcCtIntNo", SqlDbType.Int, 4);
        //    parameterAcCtIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterAcCtIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int acCtIntNo = Convert.ToInt32(parameterAcCtIntNo.Value);

        //        return acCtIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        //Jake 2014-03-04 modified this function
        //use WOAIntNo instead WoaNumber (parameter)  form package 5223
        public int CancelWarant(int WoaIntNo, int sumChargeStatus, string reasonCancelled, DateTime dateCancelled, string publicProsecutor, bool withdrawContempt, string lastUser)
        {
            int returnValue = 0;
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand cmd = new SqlCommand("WOACancel", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@WOAIntNo", SqlDbType.VarChar, 50).Value = WoaIntNo;
            cmd.Parameters.Add("@sumChargeStatus", SqlDbType.Int, 4).Value = sumChargeStatus;
            cmd.Parameters.Add("@SumReasonCancelled", SqlDbType.VarChar, 100).Value = reasonCancelled;
            cmd.Parameters.Add("@SumDateCancelled", SqlDbType.DateTime, 8).Value = dateCancelled;
            cmd.Parameters.Add("@SumPublicProsecutor", SqlDbType.VarChar, 100).Value = publicProsecutor;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            cmd.Parameters.Add("@WithdrawContempt", SqlDbType.Bit, 1).Value = withdrawContempt;
            cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;


            try
            {
                con.Open();
                int effort = cmd.ExecuteNonQuery();

                returnValue = Convert.ToInt32(cmd.Parameters["@ReturnValue"].Value);

                return returnValue;

            }
            catch (Exception ex)
            {
                this.error = ex.Message;
            }
            finally
            {
                con.Close();
            }

            return 0;
        }

        //Jake 2014-03-04 modified the parameter for this function
        //removed WOaNumber and use WOAIntNo instead
        public int WOAExecutionofCancelWOA(int WOAIntNo, string reasonCancelled, DateTime dateCancelled, string magistrate, bool withdrawContempt, string lastUser)
        {

            int returnValue = 0;
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand cmd = new SqlCommand("WOAExecution_Cancel", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@WOAIntNo", SqlDbType.Int).Value = WOAIntNo;
            cmd.Parameters.Add("@SumReasonCancelled", SqlDbType.VarChar, 100).Value = reasonCancelled;
            cmd.Parameters.Add("@SumDateCancelled", SqlDbType.DateTime, 8).Value = dateCancelled;
            cmd.Parameters.Add("@SumMagistrate", SqlDbType.VarChar, 100).Value = magistrate;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            cmd.Parameters.Add("@WithdrawContempt", SqlDbType.Bit, 1).Value = withdrawContempt;
            cmd.Parameters.Add("@ReturnValue", SqlDbType.Int).Direction = ParameterDirection.Output;


            try
            {
                con.Open();
                int effort = cmd.ExecuteNonQuery();

                returnValue = Convert.ToInt32(cmd.Parameters["@ReturnValue"].Value);

                return returnValue;

            }
            catch (Exception ex)
            {
                this.error = ex.Message;
            }
            finally
            {
                con.Close();
            }

            return 0;
        }

    }
}

