﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Stalberg.TMS
{
    public class CourtRoomTranDetails
    {
        //private int crtRIntNo;
        //private string cRTranType;
        //private int cRTranNumber;
        //private string cRTranDescr;
        //private string lastUser;
        //private int cRTranYear;

        public CourtRoomTranDetails()
        {
        }

        public int CrtRIntNo { get; set; }
        public string CRTranType { get; set; }
        public int CRTranNumber { get; set; }
        public string CRTranDescr { get; set; }
        public string LastUser { get; set; }
        public int CRTranYear { get; set; }
        public int CRTranIntNo { get; set; }
    }


    public class CourtRoomTranDB
    {
        private string connectionString;

        public CourtRoomTranDB(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public int GetCaseNoAndWOANextNum(int crtRIntNo, string cRTranType, string cRTranDescr, int  cRTranYear, string lastUser)
        {
            int cRTranNumber = 0;
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CaseNoAndWOATranNext", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@CrtRIntNo", SqlDbType.Int, 4).Value = crtRIntNo;
            com.Parameters.Add("@CRTranType", SqlDbType.VarChar, 3).Value = cRTranType;
            com.Parameters.Add("@CRTranDescr", SqlDbType.VarChar, 50).Value = cRTranDescr;
            com.Parameters.Add("@CRTranYear", SqlDbType.Int, 4).Value = cRTranYear;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@CRTranNumber", SqlDbType.Int, 4).Value = cRTranNumber;
            com.Parameters["@CRTranNumber"].Direction = ParameterDirection.InputOutput;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                cRTranNumber = (int)com.Parameters["@CRTranNumber"].Value;
                return cRTranNumber;
            }
            finally
            {
                con.Close();
            }
        }

        public DataSet GetCourtRoomTranListDS(int crtRIntNo, 
            int pageSize, int pageIndex, out int totalCount) // 2013-04-07 add by Henry for pagination
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CourtRoomTranList", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@CrtRIntNo", SqlDbType.Int, 4).Value = crtRIntNo;
            com.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            com.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            com.Parameters.Add(paraTotalCount);

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            return ds;
        }

        public CourtRoomTranDetails GetDetails(int cRTranIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CourtRoomTranDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@CRTranIntNo", SqlDbType.Int, 4).Value = cRTranIntNo;

            try
            {
                CourtRoomTranDetails detail = new CourtRoomTranDetails();

                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                if (reader.Read())
                {
                    detail.CrtRIntNo = (int)reader["CrtRIntNo"];
                    detail.CRTranNumber = (int)reader["CRTranNumber"];
                    detail.CRTranDescr = (string)reader["CRTranDescr"];
                    detail.CRTranType = (string)reader["CRTranType"];
                    detail.CRTranYear = Convert.ToInt32(reader["CRTranYear"]);
                    detail.CRTranIntNo = Convert.ToInt32(reader["CRTranIntNo"]);
                }
                reader.Close();

                return detail;
            }
            finally
            {
                con.Close();
            }
        }

        public bool UpdateCourtRoomTranDetails(CourtRoomTranDetails details, string lastUser)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CourtRoomTranDetailsUpdate", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@CRTranIntNo", SqlDbType.Int, 4).Value = details.CRTranIntNo;
            com.Parameters.Add("@CrtRIntNo", SqlDbType.Int, 4).Value = details.CrtRIntNo;
            com.Parameters.Add("@CRTranType", SqlDbType.VarChar, 3).Value = details.CRTranType;
            com.Parameters.Add("@CRTranNumber", SqlDbType.Int, 4).Value = details.CRTranNumber;
            com.Parameters.Add("@CRTranDescr", SqlDbType.VarChar, 50).Value = details.CRTranDescr;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@CRTranYear", SqlDbType.Int, 4).Value = details.CRTranYear;

            try
            {
                con.Open();
                int rowsAffected = com.ExecuteNonQuery();
                return (rowsAffected == 1);
            }
            catch
            {
                return false;
            }
            finally
            {
                con.Close();
            }
        }

    }
}
