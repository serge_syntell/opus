using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Stalberg.TMS
{
    // dls 070330 - Persona functionality (PersonaFromDriver) of this class has been moved to MI5DB - it caused a problem with the console apps to have all this stuff here!
    // dls 070330 - AddPageBlocker moved to Helper_Web
    /// <summary>
    /// Contains an array of Helper methods.
    /// </summary>
    public static partial class Helper
    {
        /// <summary>
        ///  Converts and Integer to an alphabetical Excel column name.
        /// </summary>
        /// <param name="column">The physical (1 based) column number.</param>
        /// <returns>A <see cref="String"/> representing the Excel column format</returns>
        public static string ConvertToExcelColumn(int column)
        {
            if (column < 0)
                column = 0;

            int prefix = 0;
            if ((column % 26) == 0)
                prefix = ((column - 1) / 26);
            else
                prefix = (column / 26);
            int letter = (column - (26 * prefix)) + 64;
            if (letter < 65)
                letter = 65;
            return string.Format("{0}{1}", ((prefix > 0) ? ((char)(prefix + 64)).ToString() : string.Empty), (char)letter);
        }


        /// <summary>
        /// Gets the reader value.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns>The value read from the database.</returns>
        public static T GetReaderValue<T>(SqlDataReader reader, string columnName, Action<T> onHasValue = null)
        {
            var value = reader[columnName];
            if (value == DBNull.Value)
                return default(T);
            else
            {
                var type = typeof(T);
                if (type.IsGenericType && typeof(Nullable<>) == type.GetGenericTypeDefinition())
                    type = new NullableConverter(type).UnderlyingType;
                //return (T)Convert.ChangeType(value, type);
                var valueT = (T)Convert.ChangeType(value, type);
                if (onHasValue != null)
                    onHasValue(valueT);
                return valueT;
            }
        }

        public static T GetDataRowValue<T>(DataRow dr, string columnName)
        {
            if (dr[columnName] == DBNull.Value)
                return default(T);
            else
                return (T)Convert.ChangeType(dr[columnName], typeof(T));
        }

         //<summary>
         //Returns the character that represents the <see cref="CashType"/> in the database.
         //</summary>
         //<param name="cashType">Type of cash received.</param>
         //<returns>The character that represents the CashType</returns>
        public static char GetCashTypeChar(CashType cashType)
        {
            return (char)(int)cashType;
        }

        public static CashType GetCashTypeValue(char ct)
        {
            return (CashType)(int)(ct);
        }

        /// <summary>
        /// Parses a currency string, stripping out any non-decimal characters, and returns it as a decimal.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>A <see cref="decimal"/></returns>
        public static decimal ParseMoney(string value)
        {
            if (string.IsNullOrEmpty(value))
                return 0;

            StringBuilder sb = new StringBuilder();
            foreach (char c in value)
            {
                if (char.IsNumber(c))
                    sb.Append(c);
                if (c == '.')
                    sb.Append(c);
            }

            decimal response = 0;
            decimal.TryParse(sb.ToString(), out response);

            return response;
        }

        /// <summary>
        /// 2014-01-16 Heidi added for use "/" not create file(5101)
        /// Replace("/", "~")
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ReplaceSpecialCharacters(string str)
        { 
            return str.Replace("/", "~");
        }

    }
}