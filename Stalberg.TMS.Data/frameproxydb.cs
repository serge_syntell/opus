using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
	
	public partial class FrameProxyDetails 
	{
		public int FrPrxIntNo;
		public int FrameIntNo;
		public string FrPrxSurname; 
		public string FrPrxInitials; 
		public string FrPrxIDType; 
		public string FrPrxIDNumber;
        public string FrPrxNationality;
        public string FrPrxAge;
        public string FrPrxPOAdd1;
        public string FrPrxPOAdd2;
        public string FrPrxPOAdd3;
        public string FrPrxPOAdd4;
        public string FrPrxPOAdd5;
        public string FrPrxPOCode;
        public string FrPrxStAdd1;
        public string FrPrxStAdd2;
        public string FrPrxStAdd3;
        public string FrPrxStAdd4;
        public string FrPrxStAdd5;
        public string FrPrxStCode;
        public DateTime FrPrxDateCOA;
        public string FrPrxLicenceCode;
        public string FrPrxLicencePlace;
		public string LastUser;
	}

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public partial class FrameProxyDB
	{
		string mConstr = "";

			public FrameProxyDB (string vConstr)
			{
				mConstr = vConstr;
			}

		//*******************************************************
		//
		// The GetFrameProxyDetails method returns a FrameProxyDetails
		// struct that contains information about a specific transaction number
		//
		//*******************************************************

		public FrameProxyDetails GetFrameProxyDetails(int FrPrxIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("FrameProxyDetail", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterPrxIntNo = new SqlParameter("@FrPrxIntNo", SqlDbType.Int, 4);
			parameterPrxIntNo.Value = FrPrxIntNo;
			myCommand.Parameters.Add(parameterPrxIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			// Create CustomerDetails Struct
			FrameProxyDetails myFrameProxyDetails = new FrameProxyDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myFrameProxyDetails.FrameIntNo = Convert.ToInt32(result["FrameIntNo"]);
				myFrameProxyDetails.FrPrxIntNo = Convert.ToInt32(result["FrPrxIntNo"]);
				myFrameProxyDetails.FrPrxSurname = result["FrPrxSurname"].ToString(); 
				myFrameProxyDetails.FrPrxInitials = result["FrPrxInitials"].ToString(); 
				myFrameProxyDetails.FrPrxIDType = result["FrPrxIDType"].ToString(); 
				myFrameProxyDetails.FrPrxIDNumber = result["FrPrxIDNumber"].ToString();
                myFrameProxyDetails.FrPrxNationality = result["FrPrxNationality"].ToString();
                myFrameProxyDetails.FrPrxAge = result["FrPrxAge"].ToString();
                myFrameProxyDetails.FrPrxPOAdd1 = result["FrPrxPoAdd1"].ToString();
                myFrameProxyDetails.FrPrxPOAdd2 = result["FrPrxPoAdd2"].ToString();
                myFrameProxyDetails.FrPrxPOAdd3 = result["FrPrxPoAdd3"].ToString();
                myFrameProxyDetails.FrPrxPOAdd4 = result["FrPrxPoAdd4"].ToString();
                myFrameProxyDetails.FrPrxPOAdd5 = result["FrPrxPoAdd5"].ToString();
                myFrameProxyDetails.FrPrxPOCode = result["FrPrxPoCode"].ToString();
                myFrameProxyDetails.FrPrxStAdd1 = result["FrPrxStAdd1"].ToString();
                myFrameProxyDetails.FrPrxStAdd2 = result["FrPrxStAdd2"].ToString();
                myFrameProxyDetails.FrPrxStAdd3 = result["FrPrxStAdd3"].ToString();
                myFrameProxyDetails.FrPrxStAdd4 = result["FrPrxStAdd4"].ToString();
                myFrameProxyDetails.FrPrxStCode = result["FrPrxStCode"].ToString();
                if (result["FrPrxDateCOA"] != System.DBNull.Value)
                    myFrameProxyDetails.FrPrxDateCOA = Convert.ToDateTime(result["FrPrxDateCOA"]);
                myFrameProxyDetails.FrPrxLicenceCode = result["FrPrxLicenceCode"].ToString();
                myFrameProxyDetails.FrPrxLicencePlace = result["FrPrxLicencePlace"].ToString();
				myFrameProxyDetails.LastUser = result["LastUser"].ToString();
			}
			result.Close();
			return myFrameProxyDetails;
			
		}
		
		public FrameProxyDetails GetFrameProxyDetailsByFrame(int FrameIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("FrameProxyDetailByNotice", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
			parameterFrameIntNo.Value = FrameIntNo;
			myCommand.Parameters.Add(parameterFrameIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			// Create CustomerDetails Struct
			FrameProxyDetails myFrameProxyDetails = new FrameProxyDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myFrameProxyDetails.FrameIntNo = Convert.ToInt32(result["FrameIntNo"]);
				myFrameProxyDetails.FrPrxIntNo = Convert.ToInt32(result["FrPrxIntNo"]);
				myFrameProxyDetails.FrPrxSurname = result["FrPrxSurname"].ToString(); 
				myFrameProxyDetails.FrPrxInitials = result["FrPrxInitials"].ToString(); 
				myFrameProxyDetails.FrPrxIDType = result["FrPrxIDType"].ToString(); 
				myFrameProxyDetails.FrPrxIDNumber = result["FrPrxIDNumber"].ToString();
                myFrameProxyDetails.FrPrxNationality = result["FrPrxNationality"].ToString();
                myFrameProxyDetails.FrPrxAge = result["FrPrxAge"].ToString();
                myFrameProxyDetails.FrPrxPOAdd1 = result["FrPrxPoAdd1"].ToString();
                myFrameProxyDetails.FrPrxPOAdd2 = result["FrPrxPoAdd2"].ToString();
                myFrameProxyDetails.FrPrxPOAdd3 = result["FrPrxPoAdd3"].ToString();
                myFrameProxyDetails.FrPrxPOAdd4 = result["FrPrxPoAdd4"].ToString();
                myFrameProxyDetails.FrPrxPOAdd5 = result["FrPrxPoAdd5"].ToString();
                myFrameProxyDetails.FrPrxPOCode = result["FrPrxPoCode"].ToString();
                myFrameProxyDetails.FrPrxStAdd1 = result["FrPrxStAdd1"].ToString();
                myFrameProxyDetails.FrPrxStAdd2 = result["FrPrxStAdd2"].ToString();
                myFrameProxyDetails.FrPrxStAdd3 = result["FrPrxStAdd3"].ToString();
                myFrameProxyDetails.FrPrxStAdd4 = result["FrPrxStAdd4"].ToString();
                myFrameProxyDetails.FrPrxStCode = result["FrPrxStCode"].ToString();
                if (result["FrPrxDateCOA"] != System.DBNull.Value)
                    myFrameProxyDetails.FrPrxDateCOA = Convert.ToDateTime(result["FrPrxDateCOA"]);
                myFrameProxyDetails.FrPrxLicenceCode = result["FrPrxLicenceCode"].ToString();
                myFrameProxyDetails.FrPrxLicencePlace = result["FrPrxLicencePlace"].ToString();
				myFrameProxyDetails.LastUser = result["LastUser"].ToString();
			}
			result.Close();
			return myFrameProxyDetails;
			
		}

		//*******************************************************
		//
		// The AddFrameProxy method inserts a new transaction number record
		// into the FrameProxy database.  A unique "FrPrxIntNo"
		// key is then returned from the method.  
		//
		//*******************************************************

		public int AddFrameProxy(int FrameIntNo, string frPrxSurname, string frPrxInitials, 
			string frPrxIDType, string frPrxIDNumber, 
//			string frPrxNationality,
//			string frPrxAge, string frPrxPoAdd1, string frPrxPoAdd2, string frPrxPoAdd3,
//			string frPrxPoAdd4, string frPrxPoAdd5,	string frPrxPoCode, string frPrxStAdd1,
//			string frPrxStAdd2, string frPrxStAdd3,	string frPrxStAdd4, string frPrxStAdd5, string frPrxStCode,
//			DateTime frPrxDateCOA, string frPrxLicenceCode, string frPrxLicencePlace, 
			string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("FrameProxyAdd", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
			parameterFrameIntNo.Value = FrameIntNo;
			myCommand.Parameters.Add(parameterFrameIntNo);

			SqlParameter parameterPrxSurname = new SqlParameter("@FrPrxSurname", SqlDbType.VarChar, 100);
			parameterPrxSurname.Value = frPrxSurname;
			myCommand.Parameters.Add(parameterPrxSurname);

			SqlParameter parameterPrxInitials = new SqlParameter("@FrPrxInitials", SqlDbType.VarChar, 10);
			parameterPrxInitials.Value = frPrxInitials;
			myCommand.Parameters.Add(parameterPrxInitials);
		
			SqlParameter parameterPrxIDType = new SqlParameter("@FrPrxIDType", SqlDbType.VarChar, 3);
			parameterPrxIDType.Value = frPrxIDType;
			myCommand.Parameters.Add(parameterPrxIDType);
		
			SqlParameter parameterPrxIDNumber = new SqlParameter("@FrPrxIDNumber", SqlDbType.VarChar, 25);
			parameterPrxIDNumber.Value = frPrxIDNumber;
			myCommand.Parameters.Add(parameterPrxIDNumber);
		
//			SqlParameter parameterPrxNationality = new SqlParameter("@FrPrxNationality", SqlDbType.VarChar, 3);
//			parameterPrxNationality.Value = frPrxNationality;
//			myCommand.Parameters.Add(parameterPrxNationality);
//		
//			SqlParameter parameterPrxAge = new SqlParameter("@FrPrxAge", SqlDbType.VarChar, 3);
//			parameterPrxAge.Value = frPrxAge;
//			myCommand.Parameters.Add(parameterPrxAge);
//		
//			SqlParameter parameterPrxPoAdd1 = new SqlParameter("@FrPrxPoAdd1", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd1.Value = frPrxPoAdd1;
//			myCommand.Parameters.Add(parameterPrxPoAdd1);
//		
//			SqlParameter parameterPrxPoAdd2 = new SqlParameter("@FrPrxPoAdd2", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd2.Value = frPrxPoAdd2;
//			myCommand.Parameters.Add(parameterPrxPoAdd2);
//		
//			SqlParameter parameterPrxPoAdd3 = new SqlParameter("@FrPrxPoAdd3", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd3.Value = frPrxPoAdd3;
//			myCommand.Parameters.Add(parameterPrxPoAdd3);
//		
//			SqlParameter parameterPrxPoAdd4 = new SqlParameter("@FrPrxPoAdd4", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd4.Value = frPrxPoAdd4;
//			myCommand.Parameters.Add(parameterPrxPoAdd4);
//		
//			SqlParameter parameterPrxPoAdd5 = new SqlParameter("@FrPrxPoAdd5", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd5.Value = frPrxPoAdd5;
//			myCommand.Parameters.Add(parameterPrxPoAdd5);
//		
//			SqlParameter parameterPrxPoCode = new SqlParameter("@FrPrxPoCode", SqlDbType.VarChar, 10);
//			parameterPrxPoCode.Value = frPrxPoCode;
//			myCommand.Parameters.Add(parameterPrxPoCode);
//		
//			SqlParameter parameterPrxStAdd1 = new SqlParameter("@FrPrxStAdd1", SqlDbType.VarChar, 100);
//			parameterPrxStAdd1.Value = frPrxStAdd1;
//			myCommand.Parameters.Add(parameterPrxStAdd1);
//		
//			SqlParameter parameterPrxStAdd2 = new SqlParameter("@FrPrxStAdd2", SqlDbType.VarChar, 100);
//			parameterPrxStAdd2.Value = frPrxStAdd2;
//			myCommand.Parameters.Add(parameterPrxStAdd2);
//			
//			SqlParameter parameterPrxStAdd3 = new SqlParameter("@FrPrxStAdd3", SqlDbType.VarChar, 100);
//			parameterPrxStAdd3.Value = frPrxStAdd3;
//			myCommand.Parameters.Add(parameterPrxStAdd3);
//			
//			SqlParameter parameterPrxStAdd4 = new SqlParameter("@FrPrxStAdd4", SqlDbType.VarChar, 100);
//			parameterPrxStAdd4.Value = frPrxStAdd4;
//			myCommand.Parameters.Add(parameterPrxStAdd4);
//			
//			SqlParameter parameterPrxStAdd5 = new SqlParameter("@FrPrxStAdd5", SqlDbType.VarChar, 100);
//			parameterPrxStAdd5.Value = frPrxStAdd5;
//			myCommand.Parameters.Add(parameterPrxStAdd5);
//			
//			SqlParameter parameterPrx = new SqlParameter("@FrPrxStCode", SqlDbType.VarChar, 10);
//			parameterPrx.Value = frPrxStCode;
//			myCommand.Parameters.Add(parameterPrx);
//			
//			SqlParameter parameterPrxDateCOA = new SqlParameter("@FrPrxDateCOA", SqlDbType.SmallDateTime);
//			parameterPrxDateCOA.Value = frPrxDateCOA;
//			myCommand.Parameters.Add(parameterPrxDateCOA);
//			
//			SqlParameter parameterPrxLicenceCode = new SqlParameter("@FrPrxLicenceCode", SqlDbType.VarChar, 3);
//			parameterPrxLicenceCode.Value = frPrxLicenceCode;
//			myCommand.Parameters.Add(parameterPrxLicenceCode);
//			
//			SqlParameter parameterPrxLicencePlace = new SqlParameter("@FrPrxLicencePlace", SqlDbType.VarChar, 50);
//			parameterPrxLicencePlace.Value = frPrxLicencePlace;
//			myCommand.Parameters.Add(parameterPrxLicencePlace);
//			
			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterPrxIntNo = new SqlParameter("@FrPrxIntNo", SqlDbType.Int, 4);
			parameterPrxIntNo.Direction = ParameterDirection.Output;
			myCommand.Parameters.Add(parameterPrxIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int FrPrxIntNo = Convert.ToInt32(parameterPrxIntNo.Value);

				return FrPrxIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		public SqlDataReader GetFrameProxyList(int FrameIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("FrameProxyList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
			parameterFrameIntNo.Value = FrameIntNo;
			myCommand.Parameters.Add(parameterFrameIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}


		public DataSet GetFrameProxyListDS(int FrameIntNo)
		{
			SqlDataAdapter sqlDAFrameProxys = new SqlDataAdapter();
			DataSet dsFrameProxys = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAFrameProxys.SelectCommand = new SqlCommand();
			sqlDAFrameProxys.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAFrameProxys.SelectCommand.CommandText = "FrameProxyList";

			// Mark the Command as a SPROC
			sqlDAFrameProxys.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
			parameterFrameIntNo.Value = FrameIntNo;
			sqlDAFrameProxys.SelectCommand.Parameters.Add(parameterFrameIntNo);

			// Execute the command and close the connection
			sqlDAFrameProxys.Fill(dsFrameProxys);
			sqlDAFrameProxys.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsFrameProxys;		
		}

		public int UpdateFrameProxy(int frPrxIntNo, int FrameIntNo, string frPrxSurname, string frPrxInitials, 
			string frPrxIDType, string frPrxIDNumber, 
//			string frPrxNationality,
//			string frPrxAge, string frPrxPoAdd1, string frPrxPoAdd2, string frPrxPoAdd3,
//			string frPrxPoAdd4, string frPrxPoAdd5,	string frPrxPoCode, string frPrxStAdd1,
//			string frPrxStAdd2, string frPrxStAdd3,	string frPrxStAdd4, string frPrxStAdd5, string frPrxStCode,
//			DateTime frPrxDateCOA, string frPrxLicenceCode, string frPrxLicencePlace, 
			string lastUser)  
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("FrameProxyUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
			parameterFrameIntNo.Value = FrameIntNo;
			myCommand.Parameters.Add(parameterFrameIntNo);

			SqlParameter parameterPrxSurname = new SqlParameter("@FrPrxSurname", SqlDbType.VarChar, 100);
			parameterPrxSurname.Value = frPrxSurname;
			myCommand.Parameters.Add(parameterPrxSurname);

			SqlParameter parameterPrxInitials = new SqlParameter("@FrPrxInitials", SqlDbType.VarChar, 10);
			parameterPrxInitials.Value = frPrxInitials;
			myCommand.Parameters.Add(parameterPrxInitials);
		
			SqlParameter parameterPrxIDType = new SqlParameter("@FrPrxIDType", SqlDbType.VarChar, 3);
			parameterPrxIDType.Value = frPrxIDType;
			myCommand.Parameters.Add(parameterPrxIDType);
		
			SqlParameter parameterPrxIDNumber = new SqlParameter("@FrPrxIDNumber", SqlDbType.VarChar, 25);
			parameterPrxIDNumber.Value = frPrxIDNumber;
			myCommand.Parameters.Add(parameterPrxIDNumber);
		
//			SqlParameter parameterPrxNationality = new SqlParameter("@FrPrxNationality", SqlDbType.VarChar, 3);
//			parameterPrxNationality.Value = frPrxNationality;
//			myCommand.Parameters.Add(parameterPrxNationality);
		
//			SqlParameter parameterPrxAge = new SqlParameter("@FrPrxAge", SqlDbType.VarChar, 3);
//			parameterPrxAge.Value = frPrxAge;
//			myCommand.Parameters.Add(parameterPrxAge);
//		
//			SqlParameter parameterPrxPoAdd1 = new SqlParameter("@FrPrxPoAdd1", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd1.Value = frPrxPoAdd1;
//			myCommand.Parameters.Add(parameterPrxPoAdd1);
//		
//			SqlParameter parameterPrxPoAdd2 = new SqlParameter("@FrPrxPoAdd2", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd2.Value = frPrxPoAdd2;
//			myCommand.Parameters.Add(parameterPrxPoAdd2);
//		
//			SqlParameter parameterPrxPoAdd3 = new SqlParameter("@FrPrxPoAdd3", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd3.Value = frPrxPoAdd3;
//			myCommand.Parameters.Add(parameterPrxPoAdd3);
//		
//			SqlParameter parameterPrxPoAdd4 = new SqlParameter("@FrPrxPoAdd4", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd4.Value = frPrxPoAdd4;
//			myCommand.Parameters.Add(parameterPrxPoAdd4);
//		
//			SqlParameter parameterPrxPoAdd5 = new SqlParameter("@FrPrxPoAdd5", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd5.Value = frPrxPoAdd5;
//			myCommand.Parameters.Add(parameterPrxPoAdd5);
//		
//			SqlParameter parameterPrxPoCode = new SqlParameter("@FrPrxPoCode", SqlDbType.VarChar, 10);
//			parameterPrxPoCode.Value = frPrxPoCode;
//			myCommand.Parameters.Add(parameterPrxPoCode);
//		
//			SqlParameter parameterPrxStAdd1 = new SqlParameter("@FrPrxStAdd1", SqlDbType.VarChar, 100);
//			parameterPrxStAdd1.Value = frPrxStAdd1;
//			myCommand.Parameters.Add(parameterPrxStAdd1);
//		
//			SqlParameter parameterPrxStAdd2 = new SqlParameter("@FrPrxStAdd2", SqlDbType.VarChar, 100);
//			parameterPrxStAdd2.Value = frPrxStAdd2;
//			myCommand.Parameters.Add(parameterPrxStAdd2);
//			
//			SqlParameter parameterPrxStAdd3 = new SqlParameter("@FrPrxStAdd3", SqlDbType.VarChar, 100);
//			parameterPrxStAdd3.Value = frPrxStAdd3;
//			myCommand.Parameters.Add(parameterPrxStAdd3);
//			
//			SqlParameter parameterPrxStAdd4 = new SqlParameter("@FrPrxStAdd4", SqlDbType.VarChar, 100);
//			parameterPrxStAdd4.Value = frPrxStAdd4;
//			myCommand.Parameters.Add(parameterPrxStAdd4);
//			
//			SqlParameter parameterPrxStAdd5 = new SqlParameter("@FrPrxStAdd5", SqlDbType.VarChar, 100);
//			parameterPrxStAdd5.Value = frPrxStAdd5;
//			myCommand.Parameters.Add(parameterPrxStAdd5);
//			
//			SqlParameter parameterPrx = new SqlParameter("@FrPrxStCode", SqlDbType.VarChar, 10);
//			parameterPrx.Value = frPrxStCode;
//			myCommand.Parameters.Add(parameterPrx);
//			
//			SqlParameter parameterPrxDateCOA = new SqlParameter("@FrPrxDateCOA", SqlDbType.SmallDateTime);
//			parameterPrxDateCOA.Value = frPrxDateCOA;
//			myCommand.Parameters.Add(parameterPrxDateCOA);
//			
//			SqlParameter parameterPrxLicenceCode = new SqlParameter("@FrPrxLicenceCode", SqlDbType.VarChar, 3);
//			parameterPrxLicenceCode.Value = frPrxLicenceCode;
//			myCommand.Parameters.Add(parameterPrxLicenceCode);
//			
//			SqlParameter parameterPrxLicencePlace = new SqlParameter("@FrPrxLicencePlace", SqlDbType.VarChar, 50);
//			parameterPrxLicencePlace.Value = frPrxLicencePlace;
//			myCommand.Parameters.Add(parameterPrxLicencePlace);
//			
			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterPrxIntNo = new SqlParameter("@FrPrxIntNo", SqlDbType.Int, 4);
			parameterPrxIntNo.Value = frPrxIntNo;
			parameterPrxIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterPrxIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int FrPrxIntNo = (int)myCommand.Parameters["@FrPrxIntNo"].Value;

				return FrPrxIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}



        /// <summary>
        /// added Jake 090413
        /// description : Update Proxy Base Information
        /// </summary>
        /// <param name="frPrxIntNo"></param>
        /// <param name="frPrxSurname"></param>
        /// <param name="frPrxInitials"></param>
        /// <param name="frPrxIDNumber"></param>
        /// <param name="frPrxPoAdd1"></param>
        /// <param name="frPrxPoAdd2"></param>
        /// <param name="frPrxPoAdd3"></param>
        /// <param name="frPrxPoAdd4"></param>
        /// <param name="frPrxPoAdd5"></param>
        /// <param name="frPrxPoCode"></param>
        /// <param name="frPrxStAdd1"></param>
        /// <param name="frPrxStAdd2"></param>
        /// <param name="frPrxStAdd3"></param>
        /// <param name="frPrxStAdd4"></param>
        /// <param name="frPrxStAdd5"></param>
        /// <param name="lastUser"></param>
        /// <returns></returns>
        public int UpdateFrameProxy(int frPrxIntNo, int frameIntNo,string frPrxSurname, string frPrxInitials,
                        string frPrxIDNumber, string frPrxPoAdd1, string frPrxPoAdd2, string frPrxPoAdd3,
            			string frPrxPoAdd4, string frPrxPoAdd5,	string frPrxPoCode, string frPrxStAdd1,
            			string frPrxStAdd2, string frPrxStAdd3,	string frPrxStAdd4,
                        string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ProxyUpdateBaseInformation", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterPrxSurname = new SqlParameter("@PrxSurnname", SqlDbType.VarChar, 100);
            parameterPrxSurname.Value = frPrxSurname;
            myCommand.Parameters.Add(parameterPrxSurname);

            SqlParameter parameterPrxIDNumber = new SqlParameter("@PrxIDNumber", SqlDbType.VarChar, 25);
            parameterPrxIDNumber.Value = frPrxIDNumber;
            myCommand.Parameters.Add(parameterPrxIDNumber);

            SqlParameter parameterPrxInitials = new SqlParameter("@PrxInitials", SqlDbType.VarChar, 10);
            parameterPrxInitials.Value = frPrxInitials;
            myCommand.Parameters.Add(parameterPrxInitials);

            SqlParameter parameterPrxPoAdd1 = new SqlParameter("@PrxPoAdd1", SqlDbType.VarChar, 100);
            parameterPrxPoAdd1.Value = frPrxPoAdd1;
            myCommand.Parameters.Add(parameterPrxPoAdd1);

            SqlParameter parameterPrxPoAdd2 = new SqlParameter("@PrxPoAdd2", SqlDbType.VarChar, 100);
            parameterPrxPoAdd2.Value = frPrxPoAdd2;
            myCommand.Parameters.Add(parameterPrxPoAdd2);

            SqlParameter parameterPrxPoAdd3 = new SqlParameter("@PrxPoAdd3", SqlDbType.VarChar, 100);
            parameterPrxPoAdd3.Value = frPrxPoAdd3;
            myCommand.Parameters.Add(parameterPrxPoAdd3);

            SqlParameter parameterPrxPoAdd4 = new SqlParameter("@PrxPoAdd4", SqlDbType.VarChar, 100);
            parameterPrxPoAdd4.Value = frPrxPoAdd4;
            myCommand.Parameters.Add(parameterPrxPoAdd4);

            SqlParameter parameterPrxPoAdd5 = new SqlParameter("@PrxPoAdd5", SqlDbType.VarChar, 100);
            parameterPrxPoAdd5.Value = frPrxPoAdd5;
            myCommand.Parameters.Add(parameterPrxPoAdd5);

            SqlParameter parameterPrxPoCode = new SqlParameter("@PrxPoCode", SqlDbType.VarChar, 10);
            parameterPrxPoCode.Value = frPrxPoCode;
            myCommand.Parameters.Add(parameterPrxPoCode);

            SqlParameter parameterPrxStAdd1 = new SqlParameter("@PrxStAdd1", SqlDbType.VarChar, 100);
            parameterPrxStAdd1.Value = frPrxStAdd1;
            myCommand.Parameters.Add(parameterPrxStAdd1);

            SqlParameter parameterPrxStAdd2 = new SqlParameter("@PrxStAdd2", SqlDbType.VarChar, 100);
            parameterPrxStAdd2.Value = frPrxStAdd2;
            myCommand.Parameters.Add(parameterPrxStAdd2);

            SqlParameter parameterPrxStAdd3 = new SqlParameter("@PrxStAdd3", SqlDbType.VarChar, 100);
            parameterPrxStAdd3.Value = frPrxStAdd3;
            myCommand.Parameters.Add(parameterPrxStAdd3);

            SqlParameter parameterPrxStAdd4 = new SqlParameter("@PrxStAdd4", SqlDbType.VarChar, 100);
            parameterPrxStAdd4.Value = frPrxStAdd4;
            myCommand.Parameters.Add(parameterPrxStAdd4);

            //SqlParameter parameterPrxStAdd5 = new SqlParameter("@PrxStAdd5", SqlDbType.VarChar, 100);
            //parameterPrxStAdd5.Value = frPrxStAdd5;
            //myCommand.Parameters.Add(parameterPrxStAdd5);

            //SqlParameter parameterPrx = new SqlParameter("@PrxStCode", SqlDbType.VarChar, 10);
            //parameterPrx.Value = frPrxStCode;
            //myCommand.Parameters.Add(parameterPrx);
            			
            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Value = frameIntNo;
            myCommand.Parameters.Add(parameterFrameIntNo);

            SqlParameter parameterPrxIntNo = new SqlParameter("@PrxIntNo", SqlDbType.Int, 4);
            
            parameterPrxIntNo.Direction = ParameterDirection.InputOutput;
            parameterPrxIntNo.Value = frPrxIntNo;
            myCommand.Parameters.Add(parameterPrxIntNo);

            try
            {
                myConnection.Open();
               myCommand.ExecuteNonQuery();
               int prxIntNo = (int)parameterPrxIntNo.Value;
               myConnection.Dispose();

                return prxIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

	
	}
}
