using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS
{

    /// <summary>
    /// The notice Reprint DB class contains all the database access logic for retrieving notice reprints
    /// </summary>
    public class NoticeReprintDB
    {
        // Fields
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="NoticeReportDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public NoticeReprintDB(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Gets the notice details.
        /// </summary>
        /// <param name="NotIntNo">The not int no.</param>
        /// <returns>a Notice data structure</returns>
        public DataSet GetNoticeReprintDetails(int NotIntNo, int pageSize, int pageIndex, out int totalCount)
        {

            // Create SQL data access objects
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = new SqlCommand("NoticeReprintSearch", new SqlConnection(this.connectionString));
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            da.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            da.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            da.SelectCommand.Parameters.Add(paraTotalCount);

            // Add Parameters to the SP
            da.SelectCommand.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = NotIntNo;

            // Execute the command and fill the data set
            dsNoticeReprints ds = new dsNoticeReprints();
            da.Fill(ds);
            da.SelectCommand.Connection.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            // Return the data set result
            return ds;
            
            
        }

        
    }
}