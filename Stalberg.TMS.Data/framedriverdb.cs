using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
	
	public partial class FrameDriverDetails 
	{
		public int FrDrvIntNo;
		public int FrameIntNo;
		public string FrDrvSurname; 
		public string FrDrvInitials; 
		public string FrDrvIDType; 
		public string FrDrvIDNumber; 
		public string FrDrvNationality;
		public string FrDrvAge;
		public string FrDrvPOAdd1;
		public string FrDrvPOAdd2;
		public string FrDrvPOAdd3;
		public string FrDrvPOAdd4;
		public string FrDrvPOAdd5;
		public string FrDrvPOCode;
		public string FrDrvStAdd1;
		public string FrDrvStAdd2;
		public string FrDrvStAdd3;
		public string FrDrvStAdd4;
		public string FrDrvStCode;
		public DateTime FrDrvDateCOA;
		public string FrDrvLicenceCode;
		public string FrDrvLicencePlace;
		public string LastUser;
	}

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public partial class FrameDriverDB
	{
		string mConstr = "";

			public FrameDriverDB (string vConstr)
			{
				mConstr = vConstr;
			}

		//*******************************************************
		//
		// The GetFrameDriverDetails method returns a FrameDriverDetails
		// struct that contains information about a specific transaction number
		//
		//*******************************************************

		public FrameDriverDetails GetFrameDriverDetails(int FrDrvIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("FrameDriverDetail", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterDrvIntNo = new SqlParameter("@FrDrvIntNo", SqlDbType.Int, 4);
			parameterDrvIntNo.Value = FrDrvIntNo;
			myCommand.Parameters.Add(parameterDrvIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			// Create CustomerDetails Struct
			FrameDriverDetails myFrameDriverDetails = new FrameDriverDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myFrameDriverDetails.FrameIntNo = Convert.ToInt32(result["FrameIntNo"]);
				myFrameDriverDetails.FrDrvIntNo = Convert.ToInt32(result["FrDrvIntNo"]);
				myFrameDriverDetails.FrDrvSurname = result["FrDrvSurname"].ToString(); 
				myFrameDriverDetails.FrDrvInitials = result["FrDrvInitials"].ToString(); 
				myFrameDriverDetails.FrDrvIDType = result["FrDrvIDType"].ToString(); 
				myFrameDriverDetails.FrDrvIDNumber = result["FrDrvIDNumber"].ToString(); 
				myFrameDriverDetails.FrDrvNationality = result["FrDrvNationality"].ToString();
				myFrameDriverDetails.FrDrvAge = result["FrDrvAge"].ToString();
				myFrameDriverDetails.FrDrvPOAdd1 = result["FrDrvPoAdd1"].ToString();
				myFrameDriverDetails.FrDrvPOAdd2 = result["FrDrvPoAdd2"].ToString();
				myFrameDriverDetails.FrDrvPOAdd3 = result["FrDrvPoAdd3"].ToString();
				myFrameDriverDetails.FrDrvPOAdd4 = result["FrDrvPoAdd4"].ToString();
				myFrameDriverDetails.FrDrvPOAdd5 = result["FrDrvPoAdd5"].ToString();
				myFrameDriverDetails.FrDrvPOCode = result["FrDrvPoCode"].ToString();
				myFrameDriverDetails.FrDrvStAdd1 = result["FrDrvStAdd1"].ToString();
				myFrameDriverDetails.FrDrvStAdd2 = result["FrDrvStAdd2"].ToString();
				myFrameDriverDetails.FrDrvStAdd3 = result["FrDrvStAdd3"].ToString();
				myFrameDriverDetails.FrDrvStAdd4 = result["FrDrvStAdd4"].ToString();
				myFrameDriverDetails.FrDrvStCode = result["FrDrvStCode"].ToString();
				if (result["FrDrvDateCOA"] != System.DBNull.Value)
					myFrameDriverDetails.FrDrvDateCOA = Convert.ToDateTime(result["FrDrvDateCOA"]);
				myFrameDriverDetails.FrDrvLicenceCode = result["FrDrvLicenceCode"].ToString();
				myFrameDriverDetails.FrDrvLicencePlace = result["FrDrvLicencePlace"].ToString();
				myFrameDriverDetails.LastUser = result["LastUser"].ToString();
			}

			result.Close();
			return myFrameDriverDetails;
			
		}
		
		public FrameDriverDetails GetFrameDriverDetailsByFrame(int FrameIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("FrameDriverDetailByNotice", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
			parameterFrameIntNo.Value = FrameIntNo;
			myCommand.Parameters.Add(parameterFrameIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			// Create CustomerDetails Struct
			FrameDriverDetails myFrameDriverDetails = new FrameDriverDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myFrameDriverDetails.FrameIntNo = Convert.ToInt32(result["FrameIntNo"]);
				myFrameDriverDetails.FrDrvIntNo = Convert.ToInt32(result["FrDrvIntNo"]);
				myFrameDriverDetails.FrDrvSurname = result["FrDrvSurname"].ToString(); 
				myFrameDriverDetails.FrDrvInitials = result["FrDrvInitials"].ToString(); 
				myFrameDriverDetails.FrDrvIDType = result["FrDrvIDType"].ToString(); 
				myFrameDriverDetails.FrDrvIDNumber = result["FrDrvIDNumber"].ToString(); 
				myFrameDriverDetails.FrDrvNationality = result["FrDrvNationality"].ToString();
				myFrameDriverDetails.FrDrvAge = result["FrDrvAge"].ToString();
				myFrameDriverDetails.FrDrvPOAdd1 = result["FrDrvPoAdd1"].ToString();
				myFrameDriverDetails.FrDrvPOAdd2 = result["FrDrvPoAdd2"].ToString();
				myFrameDriverDetails.FrDrvPOAdd3 = result["FrDrvPoAdd3"].ToString();
				myFrameDriverDetails.FrDrvPOAdd4 = result["FrDrvPoAdd4"].ToString();
				myFrameDriverDetails.FrDrvPOAdd5 = result["FrDrvPoAdd5"].ToString();
				myFrameDriverDetails.FrDrvPOCode = result["FrDrvPoCode"].ToString();
				myFrameDriverDetails.FrDrvStAdd1 = result["FrDrvStAdd1"].ToString();
				myFrameDriverDetails.FrDrvStAdd2 = result["FrDrvStAdd2"].ToString();
				myFrameDriverDetails.FrDrvStAdd3 = result["FrDrvStAdd3"].ToString();
				myFrameDriverDetails.FrDrvStAdd4 = result["FrDrvStAdd4"].ToString();
				myFrameDriverDetails.FrDrvStCode = result["FrDrvStCode"].ToString();
				if (result["FrDrvDateCOA"] != System.DBNull.Value)
					myFrameDriverDetails.FrDrvDateCOA = Convert.ToDateTime(result["FrDrvDateCOA"]);
				myFrameDriverDetails.FrDrvLicenceCode = result["FrDrvLicenceCode"].ToString();
				myFrameDriverDetails.FrDrvLicencePlace = result["FrDrvLicencePlace"].ToString();
				myFrameDriverDetails.LastUser = result["LastUser"].ToString();
			}

			result.Close();
			return myFrameDriverDetails;
			
		}

		//*******************************************************
		//
		// The AddFrameDriver method inserts a new transaction number record
		// into the FrameDriver database.  A unique "FrDrvIntNo"
		// key is then returned from the method.  
		//
		//*******************************************************

		public int AddFrameDriver(int frameIntNo, string frDrvSurname, string frDrvInitials, 
			string frDrvIDType, string frDrvIDNumber, string frDrvNationality,
			string frDrvAge, string frDrvPoAdd1, string frDrvPoAdd2, string frDrvPoAdd3,
			string frDrvPoAdd4, string frDrvPoAdd5,	string frDrvPoCode, string frDrvStAdd1,
			string frDrvStAdd2, string frDrvStAdd3,	string frDrvStAdd4, string frDrvStCode,
			DateTime frDrvDateCOA, string frDrvLicenceCode, string frDrvLicencePlace, string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("FrameDriverAdd", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
			parameterFrameIntNo.Value = frameIntNo;
			myCommand.Parameters.Add(parameterFrameIntNo);

			SqlParameter parameterDrvSurname = new SqlParameter("@FrDrvSurname", SqlDbType.VarChar, 100);
			parameterDrvSurname.Value = frDrvSurname;
			myCommand.Parameters.Add(parameterDrvSurname);

			SqlParameter parameterDrvInitials = new SqlParameter("@FrDrvInitials", SqlDbType.VarChar, 10);
			parameterDrvInitials.Value = frDrvInitials;
			myCommand.Parameters.Add(parameterDrvInitials);
		
			SqlParameter parameterDrvIDType = new SqlParameter("@FrDrvIDType", SqlDbType.VarChar, 3);
			parameterDrvIDType.Value = frDrvIDType;
			myCommand.Parameters.Add(parameterDrvIDType);
		
			SqlParameter parameterDrvIDNumber = new SqlParameter("@FrDrvIDNumber", SqlDbType.VarChar, 25);
			parameterDrvIDNumber.Value = frDrvIDNumber;
			myCommand.Parameters.Add(parameterDrvIDNumber);
		
			SqlParameter parameterDrvNationality = new SqlParameter("@FrDrvNationality", SqlDbType.VarChar, 3);
			parameterDrvNationality.Value = frDrvNationality;
			myCommand.Parameters.Add(parameterDrvNationality);
		
			SqlParameter parameterDrvAge = new SqlParameter("@FrDrvAge", SqlDbType.VarChar, 3);
			parameterDrvAge.Value = frDrvAge;
			myCommand.Parameters.Add(parameterDrvAge);
		
			SqlParameter parameterDrvPoAdd1 = new SqlParameter("@FrDrvPoAdd1", SqlDbType.VarChar, 100);
			parameterDrvPoAdd1.Value = frDrvPoAdd1;
			myCommand.Parameters.Add(parameterDrvPoAdd1);
		
			SqlParameter parameterDrvPoAdd2 = new SqlParameter("@FrDrvPoAdd2", SqlDbType.VarChar, 100);
			parameterDrvPoAdd2.Value = frDrvPoAdd2;
			myCommand.Parameters.Add(parameterDrvPoAdd2);
		
			SqlParameter parameterDrvPoAdd3 = new SqlParameter("@FrDrvPoAdd3", SqlDbType.VarChar, 100);
			parameterDrvPoAdd3.Value = frDrvPoAdd3;
			myCommand.Parameters.Add(parameterDrvPoAdd3);
		
			SqlParameter parameterDrvPoAdd4 = new SqlParameter("@FrDrvPoAdd4", SqlDbType.VarChar, 100);
			parameterDrvPoAdd4.Value = frDrvPoAdd4;
			myCommand.Parameters.Add(parameterDrvPoAdd4);
		
			SqlParameter parameterDrvPoAdd5 = new SqlParameter("@FrDrvPoAdd5", SqlDbType.VarChar, 100);
			parameterDrvPoAdd5.Value = frDrvPoAdd5;
			myCommand.Parameters.Add(parameterDrvPoAdd5);
		
			SqlParameter parameterDrvPoCode = new SqlParameter("@FrDrvPoCode", SqlDbType.VarChar, 10);
			parameterDrvPoCode.Value = frDrvPoCode;
			myCommand.Parameters.Add(parameterDrvPoCode);
		
			SqlParameter parameterDrvStAdd1 = new SqlParameter("@FrDrvStAdd1", SqlDbType.VarChar, 100);
			parameterDrvStAdd1.Value = frDrvStAdd1;
			myCommand.Parameters.Add(parameterDrvStAdd1);
		
			SqlParameter parameterDrvStAdd2 = new SqlParameter("@FrDrvStAdd2", SqlDbType.VarChar, 100);
			parameterDrvStAdd2.Value = frDrvStAdd2;
			myCommand.Parameters.Add(parameterDrvStAdd2);
			
			SqlParameter parameterDrvStAdd3 = new SqlParameter("@FrDrvStAdd3", SqlDbType.VarChar, 100);
			parameterDrvStAdd3.Value = frDrvStAdd3;
			myCommand.Parameters.Add(parameterDrvStAdd3);
			
			SqlParameter parameterDrvStAdd4 = new SqlParameter("@FrDrvStAdd4", SqlDbType.VarChar, 100);
			parameterDrvStAdd4.Value = frDrvStAdd4;
			myCommand.Parameters.Add(parameterDrvStAdd4);
			
			SqlParameter parameterDrv = new SqlParameter("@FrDrvStCode", SqlDbType.VarChar, 10);
			parameterDrv.Value = frDrvStCode;
			myCommand.Parameters.Add(parameterDrv);
			
			SqlParameter parameterDrvDateCOA = new SqlParameter("@FrDrvDateCOA", SqlDbType.SmallDateTime);
			parameterDrvDateCOA.Value = frDrvDateCOA;
			myCommand.Parameters.Add(parameterDrvDateCOA);
			
			SqlParameter parameterDrvLicenceCode = new SqlParameter("@FrDrvLicenceCode", SqlDbType.VarChar, 3);
			parameterDrvLicenceCode.Value = frDrvLicenceCode;
			myCommand.Parameters.Add(parameterDrvLicenceCode);
			
			SqlParameter parameterDrvLicencePlace = new SqlParameter("@FrDrvLicencePlace", SqlDbType.VarChar, 50);
			parameterDrvLicencePlace.Value = frDrvLicencePlace;
			myCommand.Parameters.Add(parameterDrvLicencePlace);
			
			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterDrvIntNo = new SqlParameter("@FrDrvIntNo", SqlDbType.Int, 4);
			parameterDrvIntNo.Direction = ParameterDirection.Output;
			myCommand.Parameters.Add(parameterDrvIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int FrDrvIntNo = Convert.ToInt32(parameterDrvIntNo.Value);

				return FrDrvIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		public SqlDataReader GetFrameDriverList(int frameIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("FrameDriverList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
			parameterFrameIntNo.Value = frameIntNo;
			myCommand.Parameters.Add(parameterFrameIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}


		public DataSet GetFrameDriverListDS(int frameIntNo)
		{
			SqlDataAdapter sqlDAFrameDrivers = new SqlDataAdapter();
			DataSet dsFrameDrivers = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAFrameDrivers.SelectCommand = new SqlCommand();
			sqlDAFrameDrivers.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAFrameDrivers.SelectCommand.CommandText = "FrameDriverList";

			// Mark the Command as a SPROC
			sqlDAFrameDrivers.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
			parameterFrameIntNo.Value = frameIntNo;
			sqlDAFrameDrivers.SelectCommand.Parameters.Add(parameterFrameIntNo);

			// Execute the command and close the connection
			sqlDAFrameDrivers.Fill(dsFrameDrivers);
			sqlDAFrameDrivers.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsFrameDrivers;		
		}

        // 2013-07-19 comment by Henry for useless
        //public int UpdateFrameDriver(int frDrvIntNo, int frameIntNo, string frDrvSurname, string frDrvInitials, 
        //    string frDrvIDType, string frDrvIDNumber, string frDrvNationality,
        //    string frDrvAge, string frDrvPoAdd1, string frDrvPoAdd2, string frDrvPoAdd3,
        //    string frDrvPoAdd4, string frDrvPoAdd5,	string frDrvPoCode, string frDrvStAdd1,
        //    string frDrvStAdd2, string frDrvStAdd3,	string frDrvStAdd4, string frDrvStCode,
        //    string frDrvDateCOA, string frDrvLicenceCode, string frDrvLicencePlace, string lastUser)  
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("FrameDriverUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
        //    parameterFrameIntNo.Value = frameIntNo;
        //    myCommand.Parameters.Add(parameterFrameIntNo);

        //    SqlParameter parameterDrvSurname = new SqlParameter("@FrDrvSurname", SqlDbType.VarChar, 100);
        //    parameterDrvSurname.Value = frDrvSurname;
        //    myCommand.Parameters.Add(parameterDrvSurname);

        //    SqlParameter parameterDrvInitials = new SqlParameter("@FrDrvInitials", SqlDbType.VarChar, 10);
        //    parameterDrvInitials.Value = frDrvInitials;
        //    myCommand.Parameters.Add(parameterDrvInitials);
		
        //    SqlParameter parameterDrvIDType = new SqlParameter("@FrDrvIDType", SqlDbType.VarChar, 3);
        //    parameterDrvIDType.Value = frDrvIDType;
        //    myCommand.Parameters.Add(parameterDrvIDType);
		
        //    SqlParameter parameterDrvIDNumber = new SqlParameter("@FrDrvIDNumber", SqlDbType.VarChar, 25);
        //    parameterDrvIDNumber.Value = frDrvIDNumber;
        //    myCommand.Parameters.Add(parameterDrvIDNumber);
		
        //    SqlParameter parameterDrvNationality = new SqlParameter("@FrDrvNationality", SqlDbType.VarChar, 3);
        //    parameterDrvNationality.Value = frDrvNationality;
        //    myCommand.Parameters.Add(parameterDrvNationality);
		
        //    SqlParameter parameterDrvAge = new SqlParameter("@FrDrvAge", SqlDbType.VarChar, 3);
        //    parameterDrvAge.Value = frDrvAge;
        //    myCommand.Parameters.Add(parameterDrvAge);
		
        //    SqlParameter parameterDrvPoAdd1 = new SqlParameter("@FrDrvPoAdd1", SqlDbType.VarChar, 100);
        //    parameterDrvPoAdd1.Value = frDrvPoAdd1;
        //    myCommand.Parameters.Add(parameterDrvPoAdd1);
		
        //    SqlParameter parameterDrvPoAdd2 = new SqlParameter("@FrDrvPoAdd2", SqlDbType.VarChar, 100);
        //    parameterDrvPoAdd2.Value = frDrvPoAdd2;
        //    myCommand.Parameters.Add(parameterDrvPoAdd2);
		
        //    SqlParameter parameterDrvPoAdd3 = new SqlParameter("@FrDrvPoAdd3", SqlDbType.VarChar, 100);
        //    parameterDrvPoAdd3.Value = frDrvPoAdd3;
        //    myCommand.Parameters.Add(parameterDrvPoAdd3);
		
        //    SqlParameter parameterDrvPoAdd4 = new SqlParameter("@FrDrvPoAdd4", SqlDbType.VarChar, 100);
        //    parameterDrvPoAdd4.Value = frDrvPoAdd4;
        //    myCommand.Parameters.Add(parameterDrvPoAdd4);
		
        //    SqlParameter parameterDrvPoAdd5 = new SqlParameter("@FrDrvPoAdd5", SqlDbType.VarChar, 100);
        //    parameterDrvPoAdd5.Value = frDrvPoAdd5;
        //    myCommand.Parameters.Add(parameterDrvPoAdd5);
		
        //    SqlParameter parameterDrvPoCode = new SqlParameter("@FrDrvPoCode", SqlDbType.VarChar, 10);
        //    parameterDrvPoCode.Value = frDrvPoCode;
        //    myCommand.Parameters.Add(parameterDrvPoCode);
		
        //    SqlParameter parameterDrvStAdd1 = new SqlParameter("@FrDrvStAdd1", SqlDbType.VarChar, 100);
        //    parameterDrvStAdd1.Value = frDrvStAdd1;
        //    myCommand.Parameters.Add(parameterDrvStAdd1);
		
        //    SqlParameter parameterDrvStAdd2 = new SqlParameter("@FrDrvStAdd2", SqlDbType.VarChar, 100);
        //    parameterDrvStAdd2.Value = frDrvStAdd2;
        //    myCommand.Parameters.Add(parameterDrvStAdd2);
			
        //    SqlParameter parameterDrvStAdd3 = new SqlParameter("@FrDrvStAdd3", SqlDbType.VarChar, 100);
        //    parameterDrvStAdd3.Value = frDrvStAdd3;
        //    myCommand.Parameters.Add(parameterDrvStAdd3);
			
        //    SqlParameter parameterDrvStAdd4 = new SqlParameter("@FrDrvStAdd4", SqlDbType.VarChar, 100);
        //    parameterDrvStAdd4.Value = frDrvStAdd4;
        //    myCommand.Parameters.Add(parameterDrvStAdd4);
						
        //    SqlParameter parameterDrv = new SqlParameter("@FrDrvStCode", SqlDbType.VarChar, 10);
        //    parameterDrv.Value = frDrvStCode;
        //    myCommand.Parameters.Add(parameterDrv);
			
        //    SqlParameter parameterDrvDateCOA = new SqlParameter("@FrDrvDateCOA", SqlDbType.VarChar, 10);
        //    parameterDrvDateCOA.Value = frDrvDateCOA;
        //    myCommand.Parameters.Add(parameterDrvDateCOA);
			
        //    SqlParameter parameterDrvLicenceCode = new SqlParameter("@FrDrvLicenceCode", SqlDbType.VarChar, 3);
        //    parameterDrvLicenceCode.Value = frDrvLicenceCode;
        //    myCommand.Parameters.Add(parameterDrvLicenceCode);
			
        //    SqlParameter parameterDrvLicencePlace = new SqlParameter("@FrDrvLicencePlace", SqlDbType.VarChar, 50);
        //    parameterDrvLicencePlace.Value = frDrvLicencePlace;
        //    myCommand.Parameters.Add(parameterDrvLicencePlace);
			
        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterDrvIntNo = new SqlParameter("@FrDrvIntNo", SqlDbType.Int, 4);
        //    parameterDrvIntNo.Value = frDrvIntNo;
        //    parameterDrvIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterDrvIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int FrDrvIntNo = (int)myCommand.Parameters["@FrDrvIntNo"].Value;

        //        return FrDrvIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        /// <summary>
        /// added Jake 090413
        /// description : Update Driver Base Information
        /// </summary>
        /// <param name="frDrvIntNo"></param>
        /// <param name="frDrvSurname"></param>
        /// <param name="frDrvInitials"></param>
        /// <param name="frDrvIDNumber"></param>
        /// <param name="frDrvPoAdd1"></param>
        /// <param name="frDrvPoAdd2"></param>
        /// <param name="frDrvPoAdd3"></param>
        /// <param name="frDrvPoAdd4"></param>
        /// <param name="frDrvPoAdd5"></param>
        /// <param name="frDrvPoCode"></param>
        /// <param name="frDrvStAdd1"></param>
        /// <param name="frDrvStAdd2"></param>
        /// <param name="frDrvStAdd3"></param>
        /// <param name="frDrvStAdd4"></param>
        /// <param name="frDrvStAdd5"></param>
        /// <param name="lastUser"></param>
        /// <returns></returns>
        public int UpdateFrameDriver(int frDrvIntNo, int frameIntNo, string frDrvSurname, string frDrvInitials,
                        string frDrvIDNumber, string frDrvPoAdd1, string frDrvPoAdd2, string frDrvPoAdd3,
                        string frDrvPoAdd4, string frDrvPoAdd5, string frDrvPoCode, string frDrvStAdd1,
                        string frDrvStAdd2, string frDrvStAdd3, string frDrvStAdd4, 
                        string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("DrvUpdateBaseInformation", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterDrvSurname = new SqlParameter("@DrvSurnname", SqlDbType.VarChar, 100);
            parameterDrvSurname.Value = frDrvSurname;
            myCommand.Parameters.Add(parameterDrvSurname);

            SqlParameter parameterDrvIDNumber = new SqlParameter("@DrvIDNumber", SqlDbType.VarChar, 25);
            parameterDrvIDNumber.Value = frDrvIDNumber;
            myCommand.Parameters.Add(parameterDrvIDNumber);

            SqlParameter parameterDrvInitials = new SqlParameter("@DrvInitials", SqlDbType.VarChar, 10);
            parameterDrvInitials.Value = frDrvInitials;
            myCommand.Parameters.Add(parameterDrvInitials);

            SqlParameter parameterDrvPoAdd1 = new SqlParameter("@DrvPoAdd1", SqlDbType.VarChar, 100);
            parameterDrvPoAdd1.Value = frDrvPoAdd1;
            myCommand.Parameters.Add(parameterDrvPoAdd1);

            SqlParameter parameterDrvPoAdd2 = new SqlParameter("@DrvPoAdd2", SqlDbType.VarChar, 100);
            parameterDrvPoAdd2.Value = frDrvPoAdd2;
            myCommand.Parameters.Add(parameterDrvPoAdd2);

            SqlParameter parameterDrvPoAdd3 = new SqlParameter("@DrvPoAdd3", SqlDbType.VarChar, 100);
            parameterDrvPoAdd3.Value = frDrvPoAdd3;
            myCommand.Parameters.Add(parameterDrvPoAdd3);

            SqlParameter parameterDrvPoAdd4 = new SqlParameter("@DrvPoAdd4", SqlDbType.VarChar, 100);
            parameterDrvPoAdd4.Value = frDrvPoAdd4;
            myCommand.Parameters.Add(parameterDrvPoAdd4);

            SqlParameter parameterDrvPoAdd5 = new SqlParameter("@DrvPoAdd5", SqlDbType.VarChar, 100);
            parameterDrvPoAdd5.Value = frDrvPoAdd5;
            myCommand.Parameters.Add(parameterDrvPoAdd5);

            SqlParameter parameterDrvPoCode = new SqlParameter("@DrvPoCode", SqlDbType.VarChar, 10);
            parameterDrvPoCode.Value = frDrvPoCode;
            myCommand.Parameters.Add(parameterDrvPoCode);

            SqlParameter parameterDrvStAdd1 = new SqlParameter("@DrvStAdd1", SqlDbType.VarChar, 100);
            parameterDrvStAdd1.Value = frDrvStAdd1;
            myCommand.Parameters.Add(parameterDrvStAdd1);

            SqlParameter parameterDrvStAdd2 = new SqlParameter("@DrvStAdd2", SqlDbType.VarChar, 100);
            parameterDrvStAdd2.Value = frDrvStAdd2;
            myCommand.Parameters.Add(parameterDrvStAdd2);

            SqlParameter parameterDrvStAdd3 = new SqlParameter("@DrvStAdd3", SqlDbType.VarChar, 100);
            parameterDrvStAdd3.Value = frDrvStAdd3;
            myCommand.Parameters.Add(parameterDrvStAdd3);

            SqlParameter parameterDrvStAdd4 = new SqlParameter("@DrvStAdd4", SqlDbType.VarChar, 100);
            parameterDrvStAdd4.Value = frDrvStAdd4;
            myCommand.Parameters.Add(parameterDrvStAdd4);

            //SqlParameter parameterDrvStAdd5 = new SqlParameter("@DrvStAdd5", SqlDbType.VarChar, 100);
            //parameterDrvStAdd5.Value = frDrvStAdd5;
            //myCommand.Parameters.Add(parameterDrvStAdd5);

            //SqlParameter parameterDrv = new SqlParameter("@DrvStCode", SqlDbType.VarChar, 10);
            //parameterDrv.Value = frDrvStCode;
            //myCommand.Parameters.Add(parameterDrv);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterFrameIntNo = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            parameterFrameIntNo.Value = frameIntNo;
            myCommand.Parameters.Add(parameterFrameIntNo);

            SqlParameter parameterDrvIntNo = new SqlParameter("@DrvIntNo", SqlDbType.Int, 4);
            
            parameterDrvIntNo.Direction = ParameterDirection.InputOutput;
            parameterDrvIntNo.Value = frDrvIntNo;
            myCommand.Parameters.Add(parameterDrvIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                int drvIntNo = (int)parameterDrvIntNo.Value;
                myConnection.Dispose();

                return drvIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

	
	}
}
