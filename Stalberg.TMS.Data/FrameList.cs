using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using Stalberg.TMS.Data.Resources;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a list of frames from a film for either Adjudication or verification, and stores the current position in the list
    /// </summary>
    [Serializable]
    public class FrameList : List<FrameInfo>
    {
        // Fields
        private ValidationStatus status = ValidationStatus.None;
        private FrameInfo current = null;
        private string filmNo = string.Empty;
        private bool noFrames = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="FrameList"/> class.
        /// </summary>
        public FrameList()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrameList"/> class.
        /// </summary>
        /// <param name="status">The verification status.</param>
        /// <param name="filmNo">The film number that the frames belong to.</param>
        /// <param name="reader">A <see cref="SqlDataReader"/> that contains the list of frame information for processing.</param>
        public FrameList(ValidationStatus status, string filmNo, SqlDataReader reader)
        {
            this.filmNo = filmNo;
            this.status = status;

            bool isSet = false;

            while (reader.Read())
            {
                base.Add(new FrameInfo((int)reader["FrameIntNo"]));

                if (!isSet)
                {
                    try
                    {
                        if (reader[1] != null)
                            if (Convert.ToInt32(reader[1]) == -1)
                                this.noFrames = true;
                    }
                    catch
                    {
                        this.noFrames = false;
                    }
                    isSet = true;
                }
            }
            reader.Close();
        }

        /// <summary>
        /// Gets the next frame for the film from the list.
        /// (if it gets to the end of the list, it starts again from the beginning)
        /// </summary>
        /// <param name="cycle">if set to <c>true</c> cycle through the records in the list infinitely.</param>
        /// <returns>
        /// The Frame Int No of the Next frame, or -1
        /// </returns>
        public int GetNextFrame(bool cycle)
        {
            if (this.current == null)
                this.current = FrameInfo.Empty;

            // Check that there's something in the list
            if (this.Count == 0)
            {
                this.current = FrameInfo.Empty;
                return this.current.FrameIntNo;
            }

            // Check that there's a current
            if (this.current.Equals(FrameInfo.Empty))
            {
                this.current = base[0];
                return this.current.FrameIntNo;
            }

            // Check if there's only one frame in the film
            if (base.Count == 1)
            {
                if (this.current.Processed)
                    this.current = FrameInfo.Empty;
                else
                    this.current = base[0];
                return this.current.FrameIntNo;
            }

            // Otherwise get the next one
            int processed = 0;
            for (int x = 0; x < base.Count; x++)
            {
                // Count the number of processed frames in the list
                if (base[x].Processed)
                    processed++;

                // If the number of processed frames is the same as the list then we've finished
                if (processed == base.Count)
                    break;

                // Is the current frame the same as the one we're on in the list
                if (this.current.Equals(base[x]))
                {
                    // Are we at the end of the list
                    if (x + 1 < base.Count)
                    {
                        // Assign the next one to Current
                        this.current = base[x + 1];

                        // If its not been processed then return it
                        if (!this.current.Processed)
                        {
                            // Make sure its not hidden
                            if (!this.current.IsHidden)
                                return this.current.FrameIntNo;
                        }
                    }
                    else
                    {
                        // Restart the loop from the beginning
                        x = -1;
                        this.current = base[0];
                        if (!this.current.Processed)
                        {
                            // Make sure its not hidden
                            if (!this.current.IsHidden)
                                return this.current.FrameIntNo;
                        }
                    }
                }
            }

            // We've fallen off the end of the list
            this.current = FrameInfo.Empty;
            return this.current.FrameIntNo;
        }

        /// <summary>
        /// Gets the previous frame from the list.
        /// (If it gets to the beginning of the list, it starts again from the end)
        /// </summary>
        /// <returns>The previous Frame Int No, or -1</returns>
        public int GetPreviousFrame()
        {
            // Check that there's something in the list
            if (this.Count == 0)
            {
                this.current = FrameInfo.Empty;
                return this.current.FrameIntNo;
            }

            // Check that there's a current
            if (this.current.Equals(FrameInfo.Empty))
            {
                this.current = base[base.Count - 1];
                return this.current.FrameIntNo;
            }

            // Otherwise, find the previous one
            for (int x = base.Count - 1; x > -1; x--)
            {
                if (this.current.Equals(base[x]))
                {
                    if (x == 0)
                        this.current = base[base.Count - 1];
                    else
                        this.current = base[x - 1];

                    if (!this.current.IsHidden)
                        return this.current.FrameIntNo;
                }
            }

            this.current = FrameInfo.Empty;
            return this.current.FrameIntNo;
        }

        /// <summary>
        /// Marks the current frame as processed.
        /// </summary>
        public void MarkProcessed()
        {
            if (!this.current.Equals(FrameInfo.Empty))
                this.current.MarkProcessed();
        }

        /// <summary>
        /// Marks the frame identified by the <c>frameIntNo</c> as processed.
        /// </summary>
        /// <param name="frameIntNo">The frame int no.</param>
        public void MarkProcessed(int frameIntNo)
        {
            foreach (FrameInfo info in this)
            {
                if (info.FrameIntNo == frameIntNo)
                {
                    info.MarkProcessed();
                    return;
                }
            }

            throw new ApplicationException(string.Format("The Frame Int No: {0}, could not be found in the frame list to mark as processed!", frameIntNo));
        }

        /// <summary>
        /// Gets the current Frame Int No.
        /// </summary>
        /// <value>The current.</value>
        public int Current
        {
            get
            {
                if (this.current == null)
                    this.GetNextFrame(true);
                return this.current.FrameIntNo;
            }
        }

        /// <summary>
        /// Gets the current bool Processed.
        /// </summary>
        /// <value>The current.</value>
        public bool CurrentProcessed
        {
            get
            {
                if (this.current == null)
                    this.GetNextFrame(true);
                return this.current.Processed;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [no frames].
        /// </summary>
        /// <value><c>true</c> if [no frames]; otherwise, <c>false</c>.</value>
        public bool NoFrames
        {
            get
            {
                return this.noFrames;
            }
        }

        /// <summary>
        /// Gets the film number that the frames belong to.
        /// </summary>
        /// <value>The film number.</value>
        public string FilmNumber
        {
            get { return this.filmNo; }
        }

        /// <summary>
        /// hides the specified frame int no.
        /// </summary>
        /// <param name="frameIntNo">The frame int no.</param>
        /// <returns>
        /// 	<c>true</c> if the frame int no was found and removed from the list.
        /// </returns>
        public void HideFrame(int frameIntNo)
        {
            foreach (FrameInfo info in this)
            {
                if (info.FrameIntNo == frameIntNo)
                {
                    info.MarkHidden();
                    return;
                }
            }

            throw new ApplicationException(string.Format("The Frame Int No: {0}, could not be found in the frame list to hide!", frameIntNo));
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            string statusStr = string.Empty;
            switch (status)
            {
                case ValidationStatus.Verification:
                    statusStr = FrameListRe.Verification;
                    break;
                case ValidationStatus.Adjudication:
                    statusStr = FrameListRe.Adjudication;
                    break;
                case ValidationStatus.Investigation:
                    statusStr = FrameListRe.Investigation;
                    break;
                default:
                    break;
            }
            return string.Format("{0} ({1} {3} {2})", this.filmNo, base.Count, statusStr, FrameListRe.FramesFor);
        }

    }

    /// <summary>
    /// Represents frame information needed to view and manipulate frame information
    /// </summary>
    [Serializable]
    public class FrameInfo
    {
        // Fields
        private int frameIntNo;
        private bool processed;
        private bool hide;

        private static FrameInfo empty = new FrameInfo(-1);

        /// <summary>
        /// Gets the empty.
        /// </summary>
        /// <value>The empty.</value>
        public static FrameInfo Empty
        {
            get { return FrameInfo.empty; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrameInfo"/> class.
        /// </summary>
        /// <param name="frameIntNo">The frame int no.</param>
        public FrameInfo(int frameIntNo)
        {
            this.processed = false;
            this.hide = false;
            this.frameIntNo = frameIntNo;
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="FrameInfo"/> is processed.
        /// </summary>
        /// <value><c>true</c> if processed; otherwise, <c>false</c>.</value>
        public bool Processed
        {
            get { return this.processed; }
        }

        /// <summary>
        /// Gets the frame int no.
        /// </summary>
        /// <value>The frame int no.</value>
        public int FrameIntNo
        {
            get { return this.frameIntNo; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is hidden.
        /// </summary>
        /// <value><c>true</c> if this instance is hidden; otherwise, <c>false</c>.</value>
        public bool IsHidden
        {
            get { return this.hide; }
        }

        /// <summary>
        /// Marks frame info as having been processed.
        /// </summary>
        public void MarkProcessed()
        {
            this.processed = true;
        }

        /// <summary>
        /// Marks the frame info as hidden.
        /// </summary>
        public void MarkHidden()
        {
            this.hide = true;
        }

        /// <summary>
        /// Checks to see if the supplied frame int no equals this one.
        /// </summary>
        /// <param name="frameIntNo">The frame int no.</param>
        /// <returns>true if obj and this instance are the same type and represent the same value; otherwise, false.</returns>
        public bool Equals(FrameInfo frame, bool checkForProcessed)
        {
            return this.Equals(frame, checkForProcessed, false);
        }

        /// <summary>
        /// Checks to see if the supplied frame int no equals this one.
        /// </summary>
        /// <param name="frame">The frame.</param>
        /// <param name="checkForProcessed">if set to <c>true</c> [check for processed].</param>
        /// <param name="showHidden">if set to <c>true</c> show hidden frames.</param>
        /// <returns>
        /// true if obj and this instance are the same type and represent the same value; otherwise, false.
        /// </returns>
        public bool Equals(FrameInfo frame, bool checkForProcessed, bool showHidden)
        {
            // If the Frame Int no is different they are never equal
            if (this.frameIntNo != frame.FrameIntNo)
                return false;

            // If we should check for processed then make sure that this hasn't been processed
            if (checkForProcessed && this.processed)
                return false;

            // If we shouldn't show and this is hidden then they aren't equal
            if (!showHidden && this.hide)
                return false;

            // If it doesn't fail any of the tests then its must be the same
            return true;
        }

        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        /// <param name="obj">Another object to compare to.</param>
        /// <returns>
        /// true if obj and this instance are the same type and represent the same value; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (!(obj is FrameInfo))
                return false;

            return this.Equals((FrameInfo)obj, false, true);
        }

        /// <summary>
        /// Serves as a hash function for a particular type. <see cref="M:System.Object.GetHashCode"></see> is suitable for use in hashing algorithms and data structures like a hash table.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}