using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Stalberg.TMS
{
    /// <summary>
    /// Lists the different types of People responsible for a notice
    /// dls 080122 - add a persona type to indicate Driver, Owner/Proxy   
    /// </summary>
    public enum PersonaType
    {
        
        Driver = 68,                        // D Driver = Default
        Owner = 79,                         // O Owner
        Proxy = 80,                         // P Proxy
        Accused = 65,                       // A Accused
        Other = 88                          // X Other
    }

}