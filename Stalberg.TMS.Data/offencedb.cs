using System;
using System.Data;
using System.Data.SqlClient;
using Stalberg.TMS.Data;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // OffenceDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class OffenceDetails
    {
        public Int32 OGIntNo;
        public string OffCode;
        public string OffDescr;
        public int OffRangeStart;
        public int OffRangeEnd;
        public string OffNoAOG;
        public string LastUser;
        //2010/9/29 jerry add new column into Offence table: OffCodeAlternate, OffHasMandatoryAlternate
        public string OffCodeAlternate;
        public bool OffHasMandatoryAlternate;
        public bool IsPresentationOfDocument;

        // Oscar 20130109 added
        public string LongDescriptionEng { get; set; }
        public string LongDescriptionAfr { get; set; }

        // 2013-05-06 add by Henry for by-law
        public bool IsByLaw { get; set; }

        // 2013-11-27 Jake added
        public bool IsS35NoAOG { get; set; }
    }

    //*******************************************************
    //
    // OffenceDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query Offences within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class OffenceDB
    {

        string mConstr = "";

        public OffenceDB(string vConstr)
        {
            mConstr = vConstr;
        }

        //*******************************************************
        //
        // OffenceDB.GetOffenceDetails() Method <a name="GetOffenceDetails"></a>
        //
        // The GetOffenceDetails method returns a OffenceDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public OffenceDetails GetOffenceDetails(int offIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenceDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterOffIntNo = new SqlParameter("@OffIntNo", SqlDbType.Int, 4);
            parameterOffIntNo.Value = offIntNo;
            myCommand.Parameters.Add(parameterOffIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            OffenceDetails myOffenceDetails = new OffenceDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myOffenceDetails.OGIntNo = Convert.ToInt32(result["OGIntNo"]);
                myOffenceDetails.OffDescr = result["OffDescr"].ToString();
                myOffenceDetails.OffCode = result["OffCode"].ToString();
                myOffenceDetails.OffNoAOG = result["OffNoAOG"].ToString();
                myOffenceDetails.LastUser = result["LastUser"].ToString();
                myOffenceDetails.OffRangeStart = Convert.ToInt32(result["OffRangeStart"]);
                myOffenceDetails.OffRangeEnd = Convert.ToInt32(result["OffRangeEnd"]);
                myOffenceDetails.OffCodeAlternate = result["OffCodeAlternate"].ToString();
                myOffenceDetails.OffHasMandatoryAlternate = Convert.ToBoolean(result["OffHasMandatoryAlternate"]);
                myOffenceDetails.IsPresentationOfDocument = Convert.ToBoolean(result["IsPresentationOfDocument"]);

                // Oscar 20130109 added
                myOffenceDetails.LongDescriptionEng = Helper.GetReaderValue<string>(result, "OffLongDescriptionEng");
                myOffenceDetails.LongDescriptionAfr = Helper.GetReaderValue<string>(result, "OffLongDescriptionAfr");

                // 2013-05-06 add by Henry for by-law
                myOffenceDetails.IsByLaw = (bool)result["OffIsByLaw"];

                myOffenceDetails.IsS35NoAOG = (bool)result["IsS35NoAOG"];
            }
            result.Close();
            return myOffenceDetails;
        }

        //*******************************************************
        //
        // OffenceDB.AddOffence() Method <a name="AddOffence"></a>
        //
        // The AddOffence method inserts a new menu record
        // into the menus database.  A unique "OffenceId"
        // key is then returned from the method.  
        //
        //*******************************************************
        // 2013-05-06 add isByLaw by Henry for by-law
        public int AddOffence(int OGIntNo, string offCode, string offDescr,
            int offRangeStart, int offRangeEnd, string offNoAOG, string lastUser,
            string offCodeAlternate, bool offHasMandatoryAlternate, bool isByLaw,
            bool isPOD = false, string longDescEng = null, string longDescAfr = null, bool isS35NoAOG = false)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenceAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int, 4);
            parameterOGIntNo.Value = OGIntNo;
            myCommand.Parameters.Add(parameterOGIntNo);

            SqlParameter parameterOffDescr = new SqlParameter("@OffDescr", SqlDbType.VarChar);
            parameterOffDescr.Value = offDescr;
            myCommand.Parameters.Add(parameterOffDescr);

            SqlParameter parameterOffCode = new SqlParameter("@OffCode", SqlDbType.VarChar, 6);
            parameterOffCode.Value = offCode;
            myCommand.Parameters.Add(parameterOffCode);

            SqlParameter parameterOffNoAOG = new SqlParameter("@OffNoAOG", SqlDbType.Char, 1);
            parameterOffNoAOG.Value = offNoAOG;
            myCommand.Parameters.Add(parameterOffNoAOG);

            SqlParameter parameterOffRangeStart = new SqlParameter("@OffRangeStart", SqlDbType.Int, 4);
            parameterOffRangeStart.Value = offRangeStart;
            myCommand.Parameters.Add(parameterOffRangeStart);

            SqlParameter parameterOffRangeEnd = new SqlParameter("@OffRangeEnd", SqlDbType.Int, 4);
            parameterOffRangeEnd.Value = offRangeEnd;
            myCommand.Parameters.Add(parameterOffRangeEnd);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterOffCodeAlternate = new SqlParameter("@OffCodeAlternate", SqlDbType.VarChar, 50);
            parameterOffCodeAlternate.Value = offCodeAlternate;
            myCommand.Parameters.Add(parameterOffCodeAlternate);

            SqlParameter parameterOffHasMandatoryAlternate = new SqlParameter("@OffHasMandatoryAlternate", SqlDbType.Bit);
            parameterOffHasMandatoryAlternate.Value = offHasMandatoryAlternate;
            myCommand.Parameters.Add(parameterOffHasMandatoryAlternate);

            SqlParameter parameterOffIntNo = new SqlParameter("@OffIntNo", SqlDbType.Int, 4);
            parameterOffIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterOffIntNo);

            // Oscar 20120813 added for PresentationOfDocument
            var paraIsPOD = new SqlParameter("@IsPOD", isPOD);
            myCommand.Parameters.Add(paraIsPOD);

            // Oscar 20130109 added
            myCommand.Parameters.AddWithValue("@LongDescEng", longDescEng ?? "");
            myCommand.Parameters.AddWithValue("@LongDescAfr", longDescAfr ?? "");

            // 2013-05-06 add isByLaw by Henry for by-law
            myCommand.Parameters.Add("@OffIsByLaw", SqlDbType.Bit).Value = isByLaw;

            // 2013-11-27 Jake added new parameter
            myCommand.Parameters.Add("@IsS35NoAOG", SqlDbType.Bit).Value = isS35NoAOG;

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int offIntNo = Convert.ToInt32(parameterOffIntNo.Value);

                return offIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public SqlDataReader GetOffenceList(int ogIntNo, string search)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenceList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int, 4);
            parameterOGIntNo.Value = ogIntNo;
            myCommand.Parameters.Add(parameterOGIntNo);

            SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
            parameterSearch.Value = search;
            myCommand.Parameters.Add(parameterSearch);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        public SqlDataReader GetOffenceListByNoticeDetails(int szIntNo, int vtIntNo,
            int rdtIntNo, int speed, string offenceType)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenceListByNoticeDetails", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterSZIntNo = new SqlParameter("@SZIntNo", SqlDbType.Int, 4);
            parameterSZIntNo.Value = szIntNo;
            myCommand.Parameters.Add(parameterSZIntNo);

            SqlParameter parameterVTIntNo = new SqlParameter("@VTIntNo", SqlDbType.Int, 4);
            parameterVTIntNo.Value = vtIntNo;
            myCommand.Parameters.Add(parameterVTIntNo);

            SqlParameter parameterRdTIntNo = new SqlParameter("@RdTIntNo", SqlDbType.Int, 4);
            parameterRdTIntNo.Value = rdtIntNo;
            myCommand.Parameters.Add(parameterRdTIntNo);

            SqlParameter parameterSpeed = new SqlParameter("@Speed", SqlDbType.Int, 4);
            parameterSpeed.Value = speed;
            myCommand.Parameters.Add(parameterSpeed);

            SqlParameter parameterOffenceType = new SqlParameter("@OffenceType", SqlDbType.Char, 2);
            parameterOffenceType.Value = offenceType;
            myCommand.Parameters.Add(parameterOffenceType);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }


        public DataSet GetOffenceListDS(int ogIntNo, string search, int pageSize, int pageIndex, out int totalCount)
        {
            SqlDataAdapter sqlDAOffences = new SqlDataAdapter();
            DataSet dsOffences = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAOffences.SelectCommand = new SqlCommand();
            sqlDAOffences.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAOffences.SelectCommand.CommandText = "OffenceList";

            // Mark the Command as a SPROC
            sqlDAOffences.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int, 4);
            parameterOGIntNo.Value = ogIntNo;
            sqlDAOffences.SelectCommand.Parameters.Add(parameterOGIntNo);

            SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
            parameterSearch.Value = search;
            sqlDAOffences.SelectCommand.Parameters.Add(parameterSearch);

            SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int);
            parameterPageSize.Value = pageSize;
            sqlDAOffences.SelectCommand.Parameters.Add(parameterPageSize);

            SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int);
            parameterPageIndex.Value = pageIndex;
            sqlDAOffences.SelectCommand.Parameters.Add(parameterPageIndex);

            SqlParameter parameterTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            parameterTotalCount.Direction = ParameterDirection.Output;
            sqlDAOffences.SelectCommand.Parameters.Add(parameterTotalCount);

            // Execute the command and close the connection
            sqlDAOffences.Fill(dsOffences);
            sqlDAOffences.SelectCommand.Connection.Dispose();

            totalCount = (int)(parameterTotalCount.Value == DBNull.Value ? 0 : parameterTotalCount.Value);

            // Return the dataset result
            return dsOffences;
        }

        //2013-05-06 add isByLaw by Henry for by-law
        public int UpdateOffence(int offIntNo, int OGIntNo, string offCode, string offDescr,
            int offRangeStart, int offRangeEnd, string offNoAOG, string lastUser, string offCodeAlternate,
            bool offHasMandatoryAlternate, bool isByLaw, bool isPOD = false, string longDescEng = null,
            string longDescAfr = null, bool isS35NoAOG = false)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenceUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int, 4);
            parameterOGIntNo.Value = OGIntNo;
            myCommand.Parameters.Add(parameterOGIntNo);

            SqlParameter parameterOffDescr = new SqlParameter("@OffDescr", SqlDbType.VarChar);
            parameterOffDescr.Value = offDescr;
            myCommand.Parameters.Add(parameterOffDescr);

            SqlParameter parameterOffCode = new SqlParameter("@OffCode", SqlDbType.VarChar, 6);
            parameterOffCode.Value = offCode;
            myCommand.Parameters.Add(parameterOffCode);

            SqlParameter parameterOffNoAOG = new SqlParameter("@OffNoAOG", SqlDbType.Char, 1);
            parameterOffNoAOG.Value = offNoAOG;
            myCommand.Parameters.Add(parameterOffNoAOG);

            SqlParameter parameterOffRangeStart = new SqlParameter("@OffRangeStart", SqlDbType.Int, 4);
            parameterOffRangeStart.Value = offRangeStart;
            myCommand.Parameters.Add(parameterOffRangeStart);

            SqlParameter parameterOffRangeEnd = new SqlParameter("@OffRangeEnd", SqlDbType.Int, 4);
            parameterOffRangeEnd.Value = offRangeEnd;
            myCommand.Parameters.Add(parameterOffRangeEnd);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterOffCodeAlternate = new SqlParameter("@OffCodeAlternate", SqlDbType.VarChar, 50);
            parameterOffCodeAlternate.Value = offCodeAlternate;
            myCommand.Parameters.Add(parameterOffCodeAlternate);

            SqlParameter parameterOffHasMandatoryAlternate = new SqlParameter("@OffHasMandatoryAlternate", SqlDbType.Bit);
            parameterOffHasMandatoryAlternate.Value = offHasMandatoryAlternate;
            myCommand.Parameters.Add(parameterOffHasMandatoryAlternate);

            SqlParameter parameterOffIntNo = new SqlParameter("@OffIntNo", SqlDbType.Int);
            parameterOffIntNo.Direction = ParameterDirection.InputOutput;
            parameterOffIntNo.Value = offIntNo;
            myCommand.Parameters.Add(parameterOffIntNo);

            // Oscar 20120813 added for PresentationOfDocument
            var paraIsPOD = new SqlParameter("@IsPOD", isPOD);
            myCommand.Parameters.Add(paraIsPOD);

            // Oscar 20130109 added
            myCommand.Parameters.AddWithValue("@LongDescEng", longDescEng ?? "");
            myCommand.Parameters.AddWithValue("@LongDescAfr", longDescAfr ?? "");

            //2013-05-06 add isByLaw by Henry for by-law
            myCommand.Parameters.Add("@OffIsByLaw", SqlDbType.Bit).Value = isByLaw;

            // 2013-11-27 Jake added new parameter
            myCommand.Parameters.Add("@IsS35NoAOG", SqlDbType.Bit).Value = isS35NoAOG;

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int OffIntNo = (int)myCommand.Parameters["@OffIntNo"].Value;
                //int menuId = (int)parameterOffIntNo.Value;

                return OffIntNo;
            }
            catch
            {
                myConnection.Dispose();
                return 0;
            }
        }

        public int GetNumberofLines(int notIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("HistoryPrintAll", myConnection);
            int count = 0;
            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = notIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct


            while (result.Read())
            {
                //// Populate Struct using Output Params from SPROC
                //myOffenceDetails.OGIntNo = Convert.ToInt32(result["OGIntNo"]);
                //myOffenceDetails.OffDescr = result["OffDescr"].ToString();
                //myOffenceDetails.OffCode = result["OffCode"].ToString();
                //myOffenceDetails.OffNoAOG = result["OffNoAOG"].ToString();
                //myOffenceDetails.LastUser = result["LastUser"].ToString();
                //myOffenceDetails.OffRangeStart = Convert.ToInt32(result["OffRangeStart"]);
                //myOffenceDetails.OffRangeEnd = Convert.ToInt32(result["OffRangeEnd"]);

                count = count + 1;
            }
            result.Close();
            return count;
        }


        public String DeleteOffence(int offIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenceDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterOffIntNo = new SqlParameter("@OffIntNo", SqlDbType.Int, 4);
            parameterOffIntNo.Value = offIntNo;
            parameterOffIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterOffIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int OffIntNo = (int)parameterOffIntNo.Value;

                return OffIntNo.ToString();
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return String.Empty;
            }
        }
    }
}

