using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // VehicleTypeDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class VehicleTypeDetails
    {
        public Int32 VTIntNo;
        public Int32 VTGIntNo;
        public string VTCode;
        public string VTDescr;
        public string Natis;
        public string TCS;
        public string Civitas;
        public string VTArchiveExclude;
        public string VTNatisDescr;
    }

    public partial class VehicleTypeLookupDetails
    {
        public Int32 VTLIntNo;
        public Int32 VTIntNo1;
        public Int32 VTIntNo2;
    }

    //*******************************************************
    //
    // VehicleTypeDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query VehicleTypes within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public class VehicleTypeDB
    {
        private string conString = "";

        public VehicleTypeDB(string vConstr)
        {
            conString = vConstr;
        }

        //*******************************************************
        //
        // VehicleTypeDB.GetVehicleTypeDetails() Method <a name="GetVehicleTypeDetails"></a>
        //
        // The GetVehicleTypeDetails method returns a VehicleTypeDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************
        public VehicleTypeDetails GetVehicleTypeDetails(int vtIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("VehicleTypeDetail", con);
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterVTIntNo = new SqlParameter("@VTIntNo", SqlDbType.Int, 4);
            parameterVTIntNo.Value = vtIntNo;
            cmd.Parameters.Add(parameterVTIntNo);

            con.Open();
            SqlDataReader result = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            VehicleTypeDetails myVehicleTypeDetails = new VehicleTypeDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myVehicleTypeDetails.VTGIntNo = Convert.ToInt32(result["VTGIntNo"]);
                myVehicleTypeDetails.VTDescr = result["VTDescr"].ToString();
                myVehicleTypeDetails.VTCode = result["VTCode"].ToString();
                myVehicleTypeDetails.Natis = result["Natis"].ToString();
                myVehicleTypeDetails.TCS = result["TCS"].ToString();
                myVehicleTypeDetails.Civitas = result["Civitas"].ToString();
                myVehicleTypeDetails.VTArchiveExclude = result["VTArchiveExclude"].ToString();
                myVehicleTypeDetails.VTNatisDescr = result["VTNatisDescr"].ToString();
            }
            result.Close();
            return myVehicleTypeDetails;
        }

        public VehicleTypeDetails GetVehicleTypeDetailsByCode(string vtCode, string system)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(conString);
            SqlCommand myCommand = new SqlCommand("VehicleTypeDetailByCode", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterVTCode = new SqlParameter("@VTCode", SqlDbType.VarChar, 3);
            parameterVTCode.Value = vtCode;
            myCommand.Parameters.Add(parameterVTCode);

            SqlParameter parameterSystem = new SqlParameter("@System", SqlDbType.Char, 1);
            parameterSystem.Value = system;
            myCommand.Parameters.Add(parameterSystem);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            VehicleTypeDetails myVehicleTypeDetails = new VehicleTypeDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myVehicleTypeDetails.VTGIntNo = Convert.ToInt32(result["VTGIntNo"]);
                myVehicleTypeDetails.VTIntNo = Convert.ToInt32(result["VTIntNo"]);
                myVehicleTypeDetails.VTDescr = result["VTDescr"].ToString();
                myVehicleTypeDetails.VTCode = result["VTCode"].ToString();
                myVehicleTypeDetails.Natis = result["Natis"].ToString();
                myVehicleTypeDetails.TCS = result["TCS"].ToString();
                myVehicleTypeDetails.Civitas = result["Civitas"].ToString();
                myVehicleTypeDetails.VTArchiveExclude = result["VTArchiveExclude"].ToString();
            }
            result.Close();
            return myVehicleTypeDetails;
        }

        //*******************************************************
        //
        // VehicleTypeDB.AddVehicleType() Method <a name="AddVehicleType"></a>
        //
        // The AddVehicleType method inserts a new menu record
        // into the menus database.  A unique "VehicleTypeId"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddVehicleType(int vtgIntNo, string vtCode, string vtDescr, string natis,
            string tcs, string civitas, string lastUser, string vtArchiveExclude, string natisDescr)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("VehicleTypeAdd", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterVTGIntNo = new SqlParameter("@VTGIntNo", SqlDbType.Int, 4);
            parameterVTGIntNo.Value = vtgIntNo;
            cmd.Parameters.Add(parameterVTGIntNo);

            SqlParameter parameterVTDescr = new SqlParameter("@VTDescr", SqlDbType.VarChar, 30);
            parameterVTDescr.Value = vtDescr;
            cmd.Parameters.Add(parameterVTDescr);

            SqlParameter parameterVTCode = new SqlParameter("@VTCode", SqlDbType.VarChar, 3);
            parameterVTCode.Value = vtCode;
            cmd.Parameters.Add(parameterVTCode);

            SqlParameter parameterNatis = new SqlParameter("@Natis", SqlDbType.VarChar, 3);
            parameterNatis.Value = natis;
            cmd.Parameters.Add(parameterNatis);

            SqlParameter parameterTCS = new SqlParameter("@TCS", SqlDbType.VarChar, 3);
            parameterTCS.Value = tcs;
            cmd.Parameters.Add(parameterTCS);

            SqlParameter parameterCivitas = new SqlParameter("@Civitas", SqlDbType.VarChar, 3);
            parameterCivitas.Value = civitas;
            cmd.Parameters.Add(parameterCivitas);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            cmd.Parameters.Add(parameterLastUser);

            SqlParameter parameterVTArchiveExclude = new SqlParameter("@VTArchiveExclude", SqlDbType.NChar, 1);
            parameterVTArchiveExclude.Value = vtArchiveExclude;
            cmd.Parameters.Add(parameterVTArchiveExclude);

            SqlParameter parameterVTIntNo = new SqlParameter("@VTIntNo", SqlDbType.Int, 4);
            parameterVTIntNo.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(parameterVTIntNo);

            cmd.Parameters.Add("VTNatisDescr", SqlDbType.VarChar, 254).Value = natisDescr;

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int vtIntNo = Convert.ToInt32(parameterVTIntNo.Value);

                return vtIntNo;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                con.Dispose();
                return 0;
            }
        }

        #region Jerry 2013-09-13 comment it, this SP only webservice use it, other code can't use it directly
        /// <summary>
        /// Gets the auth_ court list by auth DS.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        //public DataSet GetValueTextListByTableNameDS(string tableName,string autCode)
        //{
        //    SqlDataAdapter sqlDALocationSuburbs = new SqlDataAdapter();
        //    DataSet dsLocationSuburbs = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlDALocationSuburbs.SelectCommand = new SqlCommand();
        //    sqlDALocationSuburbs.SelectCommand.Connection = new SqlConnection(conString);
        //    sqlDALocationSuburbs.SelectCommand.CommandText = "SILCustom_VehicleType_GetValueTextListByTableName";

        //    // Mark the Command as a SPROC
        //    sqlDALocationSuburbs.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    SqlParameter parameterTableName = new SqlParameter("@tableName", SqlDbType.NVarChar, 50);
        //    parameterTableName.Value = tableName;
        //    sqlDALocationSuburbs.SelectCommand.Parameters.Add(parameterTableName);

        //    SqlParameter parameterAutCode = new SqlParameter("@autCode", SqlDbType.VarChar,3);
        //    parameterAutCode.Value = autCode;
        //    sqlDALocationSuburbs.SelectCommand.Parameters.Add(parameterAutCode);

        //    // Execute the command and close the connection
        //    sqlDALocationSuburbs.Fill(dsLocationSuburbs);
        //    sqlDALocationSuburbs.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsLocationSuburbs;
        //}
        #endregion

        public SqlDataReader GetVehicleTypeList(int vtgIntNo, string search)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(conString);
            SqlCommand myCommand = new SqlCommand("VehicleTypeList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterVTGIntNo = new SqlParameter("@VTGIntNo", SqlDbType.Int, 4);
            parameterVTGIntNo.Value = vtgIntNo;
            myCommand.Parameters.Add(parameterVTGIntNo);

            SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
            parameterSearch.Value = search;
            myCommand.Parameters.Add(parameterSearch);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        // 2013-03-15 add by Henry for pagination
        public DataSet GetVehicleTypeListDS(int vtgIntNo, string search)
        {
            int totalCount = 0;
            return GetVehicleTypeListDS(vtgIntNo, search, out totalCount);
        }

        public DataSet GetVehicleTypeListDS(int vtgIntNo, string search,
            out int totalCount, int pageSize = 0, int pageIndex = 0)// 2013-03-15 add by Henry for pagination
        {
            SqlDataAdapter sqlDAVehicleTypes = new SqlDataAdapter();
            DataSet dsVehicleTypes = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAVehicleTypes.SelectCommand = new SqlCommand();
            sqlDAVehicleTypes.SelectCommand.Connection = new SqlConnection(conString);
            sqlDAVehicleTypes.SelectCommand.CommandText = "VehicleTypeList";

            // Mark the Command as a SPROC
            sqlDAVehicleTypes.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterVTGIntNo = new SqlParameter("@VTGIntNo", SqlDbType.Int, 4);
            parameterVTGIntNo.Value = vtgIntNo;
            sqlDAVehicleTypes.SelectCommand.Parameters.Add(parameterVTGIntNo);

            SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
            parameterSearch.Value = search;
            sqlDAVehicleTypes.SelectCommand.Parameters.Add(parameterSearch);

            sqlDAVehicleTypes.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            sqlDAVehicleTypes.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter parameterTotal = new SqlParameter("@TotalCount", SqlDbType.Int);
            parameterTotal.Direction = ParameterDirection.Output;
            sqlDAVehicleTypes.SelectCommand.Parameters.Add(parameterTotal);
            
            // Execute the command and close the connection
            sqlDAVehicleTypes.Fill(dsVehicleTypes);
            sqlDAVehicleTypes.SelectCommand.Connection.Dispose();

            totalCount = (int)(parameterTotal.Value == DBNull.Value ? 0 : parameterTotal.Value);

            // Return the dataset result
            return dsVehicleTypes;
        }


        public int UpdateVehicleType(int vtIntNo, int vtgIntNo, string vtCode, string vtDescr, string natis,
            string tcs, string civitas, string lastUser, string vtArchiveExclude, string natisDescr)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("VehicleTypeUpdate", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterVTGIntNo = new SqlParameter("@VTGIntNo", SqlDbType.Int, 4);
            parameterVTGIntNo.Value = vtgIntNo;
            cmd.Parameters.Add(parameterVTGIntNo);

            SqlParameter parameterVTDescr = new SqlParameter("@VTDescr", SqlDbType.VarChar, 30);
            parameterVTDescr.Value = vtDescr;
            cmd.Parameters.Add(parameterVTDescr);

            SqlParameter parameterVTCode = new SqlParameter("@VTCode", SqlDbType.VarChar, 3);
            parameterVTCode.Value = vtCode;
            cmd.Parameters.Add(parameterVTCode);

            SqlParameter parameterNatis = new SqlParameter("@Natis", SqlDbType.VarChar, 3);
            parameterNatis.Value = natis;
            cmd.Parameters.Add(parameterNatis);

            SqlParameter parameterTCS = new SqlParameter("@TCS", SqlDbType.VarChar, 3);
            parameterTCS.Value = tcs;
            cmd.Parameters.Add(parameterTCS);

            SqlParameter parameterCivitas = new SqlParameter("@Civitas", SqlDbType.VarChar, 3);
            parameterCivitas.Value = civitas;
            cmd.Parameters.Add(parameterCivitas);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            cmd.Parameters.Add(parameterLastUser);

            SqlParameter parameterVTArchiveExclude = new SqlParameter("@VTArchiveExclude", SqlDbType.NChar, 1);
            parameterVTArchiveExclude.Value = vtArchiveExclude;
            cmd.Parameters.Add(parameterVTArchiveExclude);

            cmd.Parameters.Add("VTNatisDescr", SqlDbType.VarChar, 254).Value = natisDescr;

            SqlParameter parameterVTIntNo = new SqlParameter("@VTIntNo", SqlDbType.Int);
            parameterVTIntNo.Direction = ParameterDirection.InputOutput;
            parameterVTIntNo.Value = vtIntNo;
            cmd.Parameters.Add(parameterVTIntNo);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                // Calculate the CustomerID using Output Param from SPROC
                int VTIntNo = (int)cmd.Parameters["@VTIntNo"].Value;
                //int menuId = (int)parameterVTIntNo.Value;

                return VTIntNo;
            }
            catch
            {
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }


        public String DeleteVehicleType(int vtIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(conString);
            SqlCommand myCommand = new SqlCommand("VehicleTypeDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterVTIntNo = new SqlParameter("@VTIntNo", SqlDbType.Int, 4);
            parameterVTIntNo.Value = vtIntNo;
            parameterVTIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterVTIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int VTIntNo = (int)parameterVTIntNo.Value;

                return VTIntNo.ToString();
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return String.Empty;
            }
        }

        public DataSet GetVehicleTypeLookupListDS(int vtIntNo)
        {
            SqlDataAdapter sqlDAVehicleTypes = new SqlDataAdapter();
            DataSet dsVehicleTypes = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAVehicleTypes.SelectCommand = new SqlCommand();
            sqlDAVehicleTypes.SelectCommand.Connection = new SqlConnection(conString);
            sqlDAVehicleTypes.SelectCommand.CommandText = "VehicleTypeLookupList";

            // Mark the Command as a SPROC
            sqlDAVehicleTypes.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterVTIntNo = new SqlParameter("@VTIntNo", SqlDbType.Int, 4);
            parameterVTIntNo.Value = vtIntNo;
            sqlDAVehicleTypes.SelectCommand.Parameters.Add(parameterVTIntNo);

            // Execute the command and close the connection
            sqlDAVehicleTypes.Fill(dsVehicleTypes);
            sqlDAVehicleTypes.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsVehicleTypes;
        }
        // 2013-07-29 add parameter lastUser by Henry
        public int AddVehicleTypeLookups(int vtIntNo, ArrayList vtl, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(conString);
            SqlCommand myCommand = new SqlCommand("VehicleTypeLookupAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterVTIntNo = new SqlParameter("@VTIntNo", SqlDbType.Int, 4);
            parameterVTIntNo.Value = vtIntNo;
            parameterVTIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterVTIntNo);
            myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            string lookupValues = "";

            for (int i = 0; i < vtl.Count; i++)
            {
                lookupValues += vtl[i] + ";";
            }

            SqlParameter parameterLookupValues = new SqlParameter("@LookupValues", SqlDbType.VarChar, 100);
            parameterLookupValues.Value = lookupValues;
            myCommand.Parameters.Add(parameterLookupValues);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int success = (int)myCommand.Parameters["@VTIntNo"].Value;

                return success;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                myConnection.Dispose();
                return -1;
            }

        }
    }
}

