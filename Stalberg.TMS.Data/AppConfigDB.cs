﻿
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


namespace Stalberg.TMS.Data
{

    public class AppConfigDetail
    {
        public int AppConfigNO { get; set; }
        public string AppName { get; set; }
        public string FtpProcess { get; set; }
        public string FtpExportServer { get; set; }
        public string FtpExportPath { get; set; }
        public string FtpReceivedPath { get; set; }
        public string FtpHostServer { get; set; }
        public string FtpHostIP { get; set; }
        public string FtpHostUser { get; set; }
        public string FtpHostPass { get; set; }
        public string FtpHostPath { get; set; }
        public string Email { get; set; }
        public string Smtp { get; set; }
        public string Mode { get; set; }        
        public string ImageFolder { get; set; } 
        public int Port { get; set; }
        public string EmailFrom { get; set; }
        public bool Info { get; set; }
        public bool Error { get; set; }
        public bool Debug { get; set; }
        public string FileOrigin { get; set; }
        public string CopyDestinationPath { get; set; }
        public bool MakeBackup { get; set; }
        public string BackupDestinationPath { get; set; }
        public string LogFileDestination { get; set; }
        public string ConvertedDestination { get; set; }
        public bool SendEmailNotifications { get; set; }
        public string NotificationRecipient { get; set; }
        public bool LogFileEvents { get; set; }
        public string SmtpUser { get; set; }
        public string SmtpPassword { get; set; }

    }
    public class AppConfigDB
    {

        string mConstr = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppConfigDB"/> class.
        /// </summary>
        /// <param name="vConstr">The v constr.</param>
        public AppConfigDB ( string vConstr )
        {
            mConstr = vConstr;
        }


        public int UpdateAppConfig ( string copyDestinationPath, bool makeBackup, string backupDestinationPath, string logFileDestination, string convertedDestination
           , bool sendEmailNotifications, string notificationRecipient, bool infor, bool error, bool debug, int port, string emailFrom, string ftpProcess, string ftpExportServer,
           string ftpExportPath, string ftpReceivedPath, string ftpHostServer, string ftpHostIP, string ftpHostUser,
           string ftpHostPass, string ftpHostPath, string email, string smtp, string mode, string imageFolder, int appConfig, string smtpUser, string smtpPassword )
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection ( mConstr );
            SqlCommand myCommand = new SqlCommand("[ViolationLoader].[Sp_AppConfigUpdate]", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCopyDestinationPath = new SqlParameter ( "@CopyDestinationPath", SqlDbType.NVarChar, 1000 );
            parameterCopyDestinationPath.Value = copyDestinationPath;
            myCommand.Parameters.Add ( parameterCopyDestinationPath );


            // Add Parameters to SPROC
            SqlParameter parameterMakeBackup = new SqlParameter ( "@MakeBackup", SqlDbType.Bit );
            parameterMakeBackup.Value = makeBackup;
            myCommand.Parameters.Add ( parameterMakeBackup );


            // Add Parameters to SPROC
            SqlParameter parameterBackupDestinationPath = new SqlParameter ( "@BackupDestinationPath", SqlDbType.NVarChar, 1000 );
            parameterBackupDestinationPath.Value = backupDestinationPath;
            myCommand.Parameters.Add ( parameterBackupDestinationPath );


            // Add Parameters to SPROC
            SqlParameter parameterLogFileDestination = new SqlParameter ( "@LogFileDestination", SqlDbType.NVarChar, 1000 );
            parameterLogFileDestination.Value = logFileDestination;
            myCommand.Parameters.Add ( parameterLogFileDestination );


            // Add Parameters to SPROC
            SqlParameter parameterEmailFrom = new SqlParameter ( "@EmailFrom", SqlDbType.NVarChar, 1000 );
            parameterEmailFrom.Value = emailFrom;
            myCommand.Parameters.Add ( parameterEmailFrom );


            // Add Parameters to SPROC
            SqlParameter parameterPort = new SqlParameter ( "@Port", SqlDbType.Int );
            parameterPort.Value = port;
            myCommand.Parameters.Add ( parameterPort );


            // Add Parameters to SPROC
            SqlParameter parameterConvertedDestination = new SqlParameter ( "@ConvertedDestination", SqlDbType.NVarChar, 1000 );
            parameterConvertedDestination.Value = convertedDestination;
            myCommand.Parameters.Add ( parameterConvertedDestination );


            // Add Parameters to SPROC
            SqlParameter parameterSendEmailNotifications = new SqlParameter ( "@SendEmailNotifications", SqlDbType.Bit );
            parameterSendEmailNotifications.Value = sendEmailNotifications;
            myCommand.Parameters.Add ( parameterSendEmailNotifications );


            // Add Parameters to SPROC
            SqlParameter parameterNotificationRecipient = new SqlParameter ( "@NotificationRecipient", SqlDbType.VarChar, 50 );
            parameterNotificationRecipient.Value = notificationRecipient;
            myCommand.Parameters.Add ( parameterNotificationRecipient );


            // Add Parameters to SPROC
            SqlParameter parameterLogInfor = new SqlParameter ( "@Infor", SqlDbType.Bit );
            parameterLogInfor.Value = infor;
            myCommand.Parameters.Add ( parameterLogInfor );


            // Add Parameters to SPROC
            SqlParameter parameterLogError = new SqlParameter ( "@Error", SqlDbType.Bit );
            parameterLogError.Value = error;
            myCommand.Parameters.Add ( parameterLogError );

            // Add Parameters to SPROC
            SqlParameter parameterLogDebug = new SqlParameter ( "@Debug", SqlDbType.Bit );
            parameterLogDebug.Value = debug;
            myCommand.Parameters.Add ( parameterLogDebug );


            // Add Parameters to SPROC
            SqlParameter parameterFtpProcess = new SqlParameter ( "@ftpProcess", SqlDbType.VarChar, 50 );
            parameterFtpProcess.Value = ftpProcess;
            myCommand.Parameters.Add ( parameterFtpProcess );

            // Add Parameters to SPROC
            SqlParameter parameterFtpExportServer = new SqlParameter ( "@ftpExportServer", SqlDbType.VarChar, 50 );
            parameterFtpExportServer.Value = ftpExportServer;
            myCommand.Parameters.Add ( parameterFtpExportServer );


            // Add Parameters to SPROC
            SqlParameter parameterFtpExportPath = new SqlParameter ( "@ftpExportPath", SqlDbType.VarChar, 50 );
            parameterFtpExportPath.Value = ftpExportPath;
            myCommand.Parameters.Add ( parameterFtpExportPath );

            // Add Parameters to SPROC
            SqlParameter parameterFtpReceivedPath = new SqlParameter ( "@ftpReceivedPath", SqlDbType.VarChar, 50 );
            parameterFtpReceivedPath.Value = ftpReceivedPath;
            myCommand.Parameters.Add ( parameterFtpReceivedPath );

            // Add Parameters to SPROC
            SqlParameter parameterFtpHostIP = new SqlParameter ( "@ftpHostIP", SqlDbType.VarChar, 50 );
            parameterFtpHostIP.Value = ftpHostIP;
            myCommand.Parameters.Add ( parameterFtpHostIP );

            // Add Parameters to SPROC
            SqlParameter parameterFtpHostUser = new SqlParameter ( "@ftpHostUser", SqlDbType.VarChar, 50 );
            parameterFtpHostUser.Value = ftpHostUser;
            myCommand.Parameters.Add ( parameterFtpHostUser );

            // Add Parameters to SPROC
            SqlParameter parameterFtpHostPass = new SqlParameter ( "@ftpHostPass", SqlDbType.VarChar, 50 );
            parameterFtpHostPass.Value = ftpHostPass;
            myCommand.Parameters.Add ( parameterFtpHostPass );

            // Add Parameters to SPROC
            SqlParameter parameterFtpHostServer = new SqlParameter ( "@ftpHostServer", SqlDbType.VarChar, 50 );
            parameterFtpHostServer.Value = ftpHostServer;
            myCommand.Parameters.Add ( parameterFtpHostServer );

            // Add Parameters to SPROC
            SqlParameter parameterFtpHostPath = new SqlParameter ( "@ftpHostPath", SqlDbType.VarChar, 50 );
            parameterFtpHostPath.Value = ftpHostPath;
            myCommand.Parameters.Add ( parameterFtpHostPath );

            // Add Parameters to SPROC
            SqlParameter parameterEmail = new SqlParameter ( "@email", SqlDbType.VarChar, 50 );
            parameterEmail.Value = email;
            myCommand.Parameters.Add ( parameterEmail );

            // Add Parameters to SPROC
            SqlParameter parameterSmtp = new SqlParameter ( "@smtp", SqlDbType.VarChar, 50 );
            parameterSmtp.Value = smtp;
            myCommand.Parameters.Add ( parameterSmtp );

            // Add Parameters to SPROC
            SqlParameter parameterMode = new SqlParameter ( "@mode", SqlDbType.VarChar, 50 );
            parameterMode.Value = mode;
            myCommand.Parameters.Add ( parameterMode );

            // Add Parameters to SPROC
            SqlParameter parameterSmtpUser = new SqlParameter("@smtpUser", SqlDbType.VarChar, 256);
            parameterSmtpUser.Value = smtpUser;
            myCommand.Parameters.Add(parameterSmtpUser);

            // Add Parameters to SPROC
            SqlParameter parametersmtpPassword = new SqlParameter("@smtpPassword", SqlDbType.VarChar, 256);
            parametersmtpPassword.Value = smtpPassword;
            myCommand.Parameters.Add(parametersmtpPassword);

            // Add Parameters to SPROC
            SqlParameter parameterImageFolder = new SqlParameter ( "@imageFolder", SqlDbType.VarChar, 50 );
            parameterImageFolder.Value = imageFolder;
            myCommand.Parameters.Add ( parameterImageFolder );

            SqlParameter parameterAppConfigIntNo = new SqlParameter ( "@AppConfigNo", SqlDbType.Int, 4 );
            parameterAppConfigIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add ( parameterAppConfigIntNo );

            try
            {
                myConnection.Open ( );
                myCommand.ExecuteNonQuery ( );
                myConnection.Dispose ( );

                int AppConfigIntNo = Convert.ToInt32 ( parameterAppConfigIntNo.Value );

                return AppConfigIntNo;
            }
            catch (Exception e)
            {

                myConnection.Dispose ( );
                string msg = e.Message;
                return 0;
            }

        }


        /// <summary>
        ///  Get Application Configuration Details
        /// </summary>
        /// <returns></returns>

        public AppConfigDetail GetAppConfigDetails ( )
        {
                // Create Instance of Connection and Command Object
                 SqlConnection myConnection = new SqlConnection ( mConstr );
                 SqlCommand myCommand = new SqlCommand("[ViolationLoader].[AppConfigDetails]", myConnection);
                 myCommand.CommandType = CommandType.StoredProcedure;

                  try
                     {

                    myConnection.Open ( );
                    SqlDataReader result = myCommand.ExecuteReader ( CommandBehavior.CloseConnection );
                    AppConfigDetail appConfigtDetails = new AppConfigDetail ( );

                    while (result.Read ( ))
                    {
                        // Populate Struct using Output Params from SPROC
                        appConfigtDetails.CopyDestinationPath = result["CopyDestinationPath"].ToString ( );
                        appConfigtDetails.MakeBackup = bool.Parse ( result["MakeBackup"].ToString ( ) );
                        appConfigtDetails.BackupDestinationPath = result["BackupDestinationPath"].ToString ( );
                        appConfigtDetails.LogFileDestination = result["LogFileDestination"].ToString ( );
                        appConfigtDetails.ConvertedDestination = result["ConvertedDestination"].ToString ( );
                        appConfigtDetails.SendEmailNotifications = bool.Parse ( result["SendEmailNotifications"].ToString ( ) );
                        appConfigtDetails.NotificationRecipient = result["NotificationRecipient"].ToString ( );
                        appConfigtDetails.Info = bool.Parse ( result["Infor"].ToString ( ) );
                        appConfigtDetails.Error = bool.Parse ( result["Error"].ToString ( ) );
                        appConfigtDetails.Debug = bool.Parse ( result["Debug"].ToString ( ) );
                        appConfigtDetails.FtpProcess = result["FtpProcess"].ToString ( );
                        appConfigtDetails.FtpExportServer = result["FtpExportServer"].ToString ( );
                        appConfigtDetails.FtpExportPath = result["FtpExportPath"].ToString ( );
                        appConfigtDetails.FtpReceivedPath = result["FtpReceivedPath"].ToString ( );
                        appConfigtDetails.FtpHostServer = result["FtpHostServer"].ToString ( );
                        appConfigtDetails.FtpHostIP = result["FtpHostIP"].ToString ( );
                        appConfigtDetails.FtpHostUser = result["FtpHostUser"].ToString ( );
                        appConfigtDetails.FtpHostPass = result["FtpHostPass"].ToString ( );
                        appConfigtDetails.FtpHostPath = result["FtpHostPath"].ToString ( );
                        appConfigtDetails.Email = result["Email"].ToString ( );
                        appConfigtDetails.Smtp = result["SMTP"].ToString ( );
                        appConfigtDetails.Mode = result["Mode"].ToString ( );
                        appConfigtDetails.ImageFolder = result["ImageFolder"].ToString ( );
                        appConfigtDetails.Port = Convert.ToInt32 ( result["Port"].ToString ( ) );
                        appConfigtDetails.EmailFrom = result["EmailFrom"].ToString ( );
                        appConfigtDetails.SmtpUser = result["SmtpUserName"].ToString();
                        appConfigtDetails.SmtpPassword = result["SmtpPassword"].ToString();

                    }

                    result.Close ( );
                    return appConfigtDetails;
                }

                catch (Exception e)
                {

                    myConnection.Dispose ( );
                    string msg = e.Message;
                    return null;

                }
           
        }
    }
}
