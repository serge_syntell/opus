using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    /// <summary>
    /// Summary description for framehistorydb
    /// </summary>
    public class framehistorydb
    {
        // Fields
        private readonly string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public framehistorydb(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Gets the notice audit results.
        /// </summary>
        /// <param name="sAutIntNo">The string aut int no.</param>
        /// <param name="sFilmNo">The s film no.</param>
        /// <param name="frameNo">The string frame no.</param>
        /// <param name="startDate">The start date string (empty for all).</param>
        /// <param name="endDate">The end date string (empty for all).</param>
        /// <returns>A <see cref="SqlDataReader"/></returns>
        public SqlDataReader GetFrameHistory(string sAutIntNo, string sFilmNo, string frameNo, string startDate, string endDate)
        {
            DateTime startDt = DateTime.Now;
            if (!DateTime.TryParse(startDate, out startDt))
                //startDt = DateTime.Now.Subtract(TimeSpan.FromDays(30));
                startDt = Convert.ToDateTime("1900/01/01");
            DateTime endDt;
            if (!DateTime.TryParse(endDate, out endDt))
                //endDt = DateTime.Now;
                endDt = Convert.ToDateTime("1900/01/01");

            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("GetFrameHistoryByDate", con);
            com.CommandType = CommandType.StoredProcedure;
            // jerry 2012-02-06 change FilmNo length from 50 to 25
            com.Parameters.Add("@FilmNo", SqlDbType.VarChar, 25).Value = sFilmNo;
            com.Parameters.Add("@FrameNo", SqlDbType.VarChar, 10).Value = frameNo;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int).Value = int.Parse(sAutIntNo); ;
            com.Parameters.Add("@DateFrom", SqlDbType.SmallDateTime, 4).Value = startDt;
            com.Parameters.Add("@DateTo", SqlDbType.SmallDateTime, 4).Value = endDt;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }
    }
}



