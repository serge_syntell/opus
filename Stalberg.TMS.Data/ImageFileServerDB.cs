﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;

namespace Stalberg.TMS.Data
{
    public class ImageFileServerDB
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageFileServerDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public ImageFileServerDB(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>The connection string.</value>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Get all ImageFileServer in database
        /// </summary>
        /// <returns></returns>
        public List<ImageFileServerDetails> GetAllImageFileServers()
        {
            // Create Instance of Connection and Command Object
            List<ImageFileServerDetails> list = new List<ImageFileServerDetails>();
            SqlConnection con = new SqlConnection(this.ConnectionString);

            try
            {
                SqlCommand cmd = new SqlCommand("ImageFileServersList", con);

                // Mark the Command as a SPROC
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataReader result = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                // Create ImageFileServerDetails Struct

                if (result != null)
                {
                    while (result.Read())
                    {
                        ImageFileServerDetails imageFileServerDetails = this.FillDetails(result);
                        list.Add(imageFileServerDetails);
                    }
                    result.Close();
                    result.Dispose();
                }

                cmd.Dispose();
            }
            finally
            {
                con.Dispose();
            }

            return list;
        }

        /// <summary>
        /// Get ImageFileServer by appointed IfsIntNO
        /// </summary>
        /// <param name="ifsIntNo">appointed IfsIntNO</param>
        /// <returns></returns>
        public ImageFileServerDetails GetImageFileServer(int ifsIntNo)
        {
            ImageFileServerDetails imageFileServerDetails = null;
            SqlConnection con = new SqlConnection(this.ConnectionString);

            try
            {
                SqlCommand cmd = new SqlCommand("ImageFileServersListGetByID", con);

                // Mark the Command as a SPROC
                cmd.CommandType = CommandType.StoredProcedure;

                // Add Parameters to SPROC
                cmd.Parameters.Add("@IFSIntNo", SqlDbType.Int).Value = ifsIntNo;

                con.Open();
                SqlDataReader result = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                // Populate Struct using Output Params from SPROC
                if (result != null)
                {
                    while (result.Read())
                    {
                        imageFileServerDetails = this.FillDetails(result);
                    }
                    result.Close();
                    result.Dispose();
                }

                cmd.Dispose();
            }
            finally
            {
                con.Dispose();
            }

            return imageFileServerDetails;
        }

        /// <summary>
        /// Get default ImageFileServer instance for application (such as: 3P_Loader)
        /// </summary>
        /// <returns></returns>
        public ImageFileServerDetails GetDefaultImageFileServer()
        {
            SqlConnection con = new SqlConnection(this.ConnectionString);
            ImageFileServerDetails imageFileServerDetails = null;

            try
            {
                SqlCommand cmd = new SqlCommand("ImageFileServersGetDefault", con);

                // Mark the Command as a SPROC
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataReader result = cmd.ExecuteReader(CommandBehavior.CloseConnection);


                // Populate Struct using Output Params from SPROC
                if (result != null)
                {
                    while (result.Read())
                    {
                        imageFileServerDetails = this.FillDetails(result);
                    }
                    result.Close();
                    result.Dispose();
                }

                cmd.Dispose();
            }
            finally
            {
                con.Dispose();
            }

            return imageFileServerDetails;
        }

        /// <summary>
        /// Fill custom structure ImageFileServerDetails from SqlDataReader
        /// </summary>
        /// <param name="reader">SqlDataReader with datas</param>
        /// <returns></returns>
        private ImageFileServerDetails FillDetails(SqlDataReader reader)
        {
            ImageFileServerDetails imageFileServerDetails = new ImageFileServerDetails();
            imageFileServerDetails.IFSIntNo = Convert.ToInt32(reader["IFSIntNo"]);
            imageFileServerDetails.ImageDrive = reader["ImageDrive"].ToString();
            imageFileServerDetails.ImageMachineName = reader["ImageMachineName"].ToString();
            imageFileServerDetails.ImageRootFolder = reader["ImageRootFolder"].ToString();
            imageFileServerDetails.ImageShareName = reader["ImageShareName"].ToString();
            //imageFileServerDetails.RemotePassword = reader["RemotePassword"].ToString();
            //imageFileServerDetails.RemoteUserName = reader["RemoteUserName"].ToString();
            imageFileServerDetails.IsDefault = (bool)reader["IsDefault"];
            return imageFileServerDetails;
        }
    }

    public class ImageFileServerDetails
    {
        public int IFSIntNo { get; set; }
        public string ImageMachineName { get; set; }
        public string ImageDrive { get; set; }
        public string ImageRootFolder { get; set; }
        public string ImageShareName { get; set; }
        //public string RemoteUserName { get; set; }
        //public string RemotePassword { get; set; }
        public bool IsDefault { get; set; }
    }
}
