using System;
using System.Collections.Generic;
using System.Text;
using Stalberg.TMS_TPExInt.Objects;

namespace Stalberg.ThaboAggregator.Objects
{
    /// <summary>
    /// Represents a Holdback File Error containing the error message and a reference to the erroring transaction line
    /// </summary>
    public class HoldbackFileError
    {
        // Fields
        private string error;
        private EasyPayTransaction tran;
        private int lineNo;

        /// <summary>
        /// Initializes a new instance of the <see cref="HoldbackFileError"/> class.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <param name="tran">The tran.</param>
        public HoldbackFileError(string error, EasyPayTransaction tran, int lineNo)
        {
            this.tran = tran;
            if (tran == null)
                this.lineNo = lineNo;
            else
                this.lineNo = lineNo - 1;
            if (error.Substring(0, 3) == "***")
                this.error = error.Substring(3);
            else
                this.error = error;
        }

        /// <summary>
        /// Gets the line number in the source file where the error occurred.
        /// </summary>
        /// <value>The line number.</value>
        public int LineNumber
        {
            get { return this.lineNo; }
        }

        /// <summary>
        /// Gets the error message for the transaction.
        /// </summary>
        /// <value>The error.</value>
        public string Error
        {
            get { return this.error; }
        }

        /// <summary>
        /// Gets the EasyPay transaction that is in error.
        /// </summary>
        /// <value>The transaction.</value>
        public EasyPayTransaction Transaction
        {
            get { return this.tran; }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            return string.Format("{0}; '{1}'", (this.tran == null) ? "File" : this.tran.EasyPayAction.ToString(), this.error);
        }

    }
}
