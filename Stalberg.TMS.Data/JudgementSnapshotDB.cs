﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml;
using System.IO;
using System.Data.SqlClient;
using System.Reflection;
using System.Xml.Serialization;

namespace Stalberg.TMS.Data
{
    public class JudgementSnapshotDB
    {
        // Fields
        private SqlConnection con;

        public JudgementSnapshotDB(string connectionString)
        {
            this.con = new SqlConnection(connectionString);
        }

        public bool SetSnapshot(int sumIntNo, bool lockSnapshot, string lastUser)
        {
            DataSet dsJudgementSnapshot = GetSnapshotDataFromDB(sumIntNo);

            if (dsJudgementSnapshot != null)
            {
                XmlDocument xmlDoc = ReadSnapshotToXML(dsJudgementSnapshot);

                if (xmlDoc != null && !String.IsNullOrEmpty(xmlDoc.InnerXml))
                {
                    //Save xml to DB
                    return AddSummonsBeforeJudgementSnapshot(sumIntNo, lockSnapshot, xmlDoc.InnerXml, lastUser);

                }
            }
            return false;
        }

        public bool RollBackFromSnapshot(int sumIntNo)
        {
            DataSet dsJudgementSnapshot = GetSnapshotDataFromDB(sumIntNo);

            if (dsJudgementSnapshot != null)
            {
                XmlDocument xmlDoc = ReadSnapshotToXML(dsJudgementSnapshot);

                if (xmlDoc != null && !String.IsNullOrEmpty(xmlDoc.InnerXml))
                {
                    JudgementSnapshot snapShot = ReadSnapshotToEntity(xmlDoc);

                    if (snapShot != null)
                    {
                        //roll back operation here

                        return true;
                    }
                }

            }
            return false;
        }


        #region DB Access

        bool AddSummonsBeforeJudgementSnapshot(int sumIntNo, bool locked, string snapShotXML, string lastUser)
        {

            SqlCommand command = con.CreateCommand();
            command.CommandText = "SummonsBeforeJudgementSnapshotAdd";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@SumIntNo", SqlDbType.Int)).Value = sumIntNo;
            command.Parameters.Add(new SqlParameter("@SBJSSummonsDataXML", SqlDbType.Xml)).Value = snapShotXML;
            command.Parameters.Add(new SqlParameter("@SBJSLocked", SqlDbType.Bit)).Value = locked;
            command.Parameters.Add(new SqlParameter("@LastUser", SqlDbType.NVarChar, 50)).Value = lastUser;

            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();

                int SBJSIntNo = Convert.ToInt32(command.ExecuteScalar());

                return SBJSIntNo > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        // 2013-07-19 comment by Henry for function useless
        //bool UpdateSummonsBeforeJudgementSnapshot(int sumIntNo, bool lockSnapShot, string snapShotXML, string lastUser)
        //{

        //    SqlCommand command = con.CreateCommand();
        //    command.CommandText = "SummonsBeforeJudgementSnapshotUpdate";
        //    command.CommandType = CommandType.StoredProcedure;

        //    command.Parameters.Add(new SqlParameter("@SumIntNo", SqlDbType.Int)).Value = sumIntNo;
        //    command.Parameters.Add(new SqlParameter("@SBJSSummonsDataXML", SqlDbType.Xml)).Value = snapShotXML;
        //    command.Parameters.Add(new SqlParameter("@SBJSLocked", SqlDbType.Bit)).Value = lockSnapShot;
        //    command.Parameters.Add(new SqlParameter("@LastUser", SqlDbType.NVarChar, 50)).Value = lastUser;

        //    try
        //    {
        //        if (con.State != ConnectionState.Open)
        //            con.Open();

        //        return Convert.ToInt32(command.ExecuteNonQuery()) > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}
        
        DataSet GetSnapshotDataFromDB(int sumIntNo)
        {
            DataSet dsReturn = new DataSet();

            SqlCommand command = con.CreateCommand();
            command.CommandText = "CreateJudgementSnapshot";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@SumIntNo", SqlDbType.Int)).Value = sumIntNo;

            SqlDataAdapter da = new SqlDataAdapter(command);

            try
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                da.Fill(dsReturn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }

            return dsReturn;
        }

        void RollBackFromSnapShot(JudgementSnapshot snapShot)
        {
            try {
                SqlTransaction transac = con.BeginTransaction();

                //Roall back Summons

                //roll back notice

                //roll back notice_summons

                //roll back sumcharge

                //roll back charge_sumcharge

                //roll back charge
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion







        void SortSnapshotDataSet(DataSet ds)
        {
            try
            {
                ds.DataSetName = "JudgementSnapshot";

                for (int index = 0; index < ds.Tables.Count; index++)
                {
                    switch (index)
                    {
                        case 0:

                            ds.Tables[index].TableName = "Summons"; break;
                        case 1:

                            ds.Tables[index].TableName = "Notice"; break;
                        case 2:

                            ds.Tables[index].TableName = "Notice_Summons"; break;
                        case 3:

                            ds.Tables[index].TableName = "SumCharge"; break;
                        case 4:

                            ds.Tables[index].TableName = "Charge_SumCharge"; break;
                        case 5:

                            ds.Tables[index].TableName = "Charge"; break;

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        XmlDocument ReadSnapshotToXML(DataSet ds)
        {
            XmlDocument document = new XmlDocument();
            MemoryStream memoryStream = new MemoryStream();
            try
            {
                ds.WriteXml(memoryStream, XmlWriteMode.IgnoreSchema);
                memoryStream.Position = 0;

                document.Load(memoryStream);

                memoryStream.Close();
                memoryStream.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return document;
        }

        JudgementSnapshot ReadSnapshotToEntity(XmlDocument document)
        {
            JudgementSnapshot snapShot = new JudgementSnapshot();

            XmlNode root = document.FirstChild;

            foreach (XmlNode node in root.ChildNodes)
            {
                object obj = null;
                switch (node.Name)
                {
                    case "Summons": obj = new Summons(); break;
                    case "Notice": obj = new Notice(); break;
                    case "Notice_Summons": obj = new Notice_Summons(); break;
                    case "Charge": obj = new Charge(); break;
                    case "Charge_SumCharge": obj = new Charge_SumCharge(); break;
                    case "SumCharge": obj = new SumCharge(); break;
                }

                if (obj != null)
                {
                    PropertyInfo proInfo = null;
                    foreach (XmlNode valueNode in node.ChildNodes)
                    {
                        proInfo = obj.GetType().GetProperty(valueNode.Name);
                        if (proInfo != null)
                        {
                            if (!String.IsNullOrEmpty(valueNode.InnerText) && valueNode.InnerText.Trim() != "0")
                                proInfo.SetValue(obj, System.Convert.ChangeType(valueNode.InnerText.Trim(), proInfo.PropertyType), null);
                        }
                    }


                    switch (obj.GetType().Name)
                    {
                        case "Summons": snapShot.Summons = obj as Summons; break;
                        case "Notice": snapShot.Notices = obj as Notice; break;
                        case "Notice_Summons": snapShot.AddNoticeSummons(obj as Notice_Summons); break;
                        case "Charge": snapShot.AddCharge(obj as Charge); break;
                        case "Charge_SumCharge": snapShot.AddChargeSumCharge(obj as Charge_SumCharge); break;
                        case "SumCharge": snapShot.AddSumCharge(obj as SumCharge); break;
                    }
                }

            }

            return snapShot;
        }

        XmlDocument SerialJudgementSnapshot(JudgementSnapshot snapShot)
        {
            XmlDocument doc = new XmlDocument();
            XmlElement element = doc.CreateElement("JudgementSnapshot");

            XmlNode root = doc.AppendChild(element);

            //XmlSerializer xs = new XmlSerializer(snapShot.GetType());

            foreach (PropertyInfo pInfo in snapShot.GetType().GetProperties())
            {
                switch (pInfo.Name)
                {
                    case "Summons":
                        Serialize(doc, snapShot.Summons);
                        break;
                    case "Notices":
                        Serialize(doc, snapShot.Notices);
                        break;
                    case "Charges":
                        Serialize<Charge>(doc, snapShot.Charges);
                        break;
                    case "ChargeSumCharges":
                        Serialize<Charge_SumCharge>(doc, snapShot.ChargeSumCharges);
                        break;
                    case "NoticeSummonsList":
                        Serialize<Notice_Summons>(doc, snapShot.NoticeSummonsList);
                        break;
                    case "SumCharges":
                        Serialize<SumCharge>(doc, snapShot.SumCharges);
                        break;
                    case "CourtDates":
                        Serialize(doc, snapShot.CourtDates);
                        break;
                }
            }

            return doc;
        }

        public void Serialize(XmlDocument doc, object obj)
        {
            if (obj == null)
                return;

            XmlNode pNode = doc.CreateNode(XmlNodeType.Element, obj.GetType().Name, string.Empty);

            foreach (PropertyInfo pInfo in obj.GetType().GetProperties())
            {
                XmlNode node = doc.CreateNode(XmlNodeType.Element, pInfo.Name, string.Empty);
                if (pInfo.GetValue(obj, null) != null)
                {
                    node.InnerText = pInfo.GetValue(obj, null) == null ? "" : pInfo.GetValue(obj, null).ToString();
                    pNode.AppendChild(node);
                }
            }

            doc.FirstChild.AppendChild(pNode);

        }

        public void Serialize<T>(XmlDocument doc, List<T> list)
        {
            if (list != null)
            {
                foreach (T obj in list)
                {
                    XmlNode pNode = doc.CreateNode(XmlNodeType.Element, obj.GetType().Name, string.Empty);

                    foreach (PropertyInfo pInfo in obj.GetType().GetProperties())
                    {
                        XmlNode node = doc.CreateNode(XmlNodeType.Element, pInfo.Name, string.Empty);
                        if (pInfo.GetValue(obj, null) != null)
                        {
                            node.InnerText = pInfo.GetValue(obj, null) == null ? "" : pInfo.GetValue(obj, null).ToString();
                            pNode.AppendChild(node);
                        }
                    }

                    doc.FirstChild.AppendChild(pNode);
                }
            }
        }
    }
}
