// 20060523 replaced autIntNo with MtrIntNo
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the details of a traffic officer
    /// </summary>
    public class TrafficOfficerDetails
    {
        public Int32 MtrIntNo;
        public string TONo;
        //public string TOGroup;
        public string TOSName;
        public string TOInit;
        public string TOIdNumber;
        public bool IsSenior;
        public string TOInfrastructureNumber;
    }

    public enum OfficerSeniority
    {
        Neither = 0,
        Senior = 1,
        Junior = 2,
        Both = 4
    }

    /// <summary>
    /// Contains all the database logic for interacting with the traffic officer table
    /// </summary>
    public class TrafficOfficerDB
    {
        string mConstr = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrafficOfficerDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public TrafficOfficerDB(string connectionString)
        {
            mConstr = connectionString;
        }

        public int ValidateTrafficOfficer(int autIntNo, string toNo, ref string toName)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand cmd = new SqlCommand("TrafficOfficerValidate", myConnection);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            cmd.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            cmd.Parameters.Add("@ToNO", SqlDbType.VarChar, 10).Value = toNo;
            cmd.Parameters.Add("@ToName", SqlDbType.VarChar, 50).Direction = ParameterDirection.InputOutput;
            cmd.Parameters.Add("@TOIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;

            try
            {
                myConnection.Open();
                cmd.ExecuteNonQuery();

                // Calculate the CustomerID using Output Param from SPROC
                int toIntNo = Convert.ToInt32(cmd.Parameters["@TOIntNo"].Value);
                toName = cmd.Parameters["@TOName"].Value.ToString();

                return toIntNo;
            }
            catch
            {
                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        public SqlDataReader GetTrafficOfficerList(int mtrIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("TrafficOfficerList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterMtrIntNo = new SqlParameter("@MtrIntNo", SqlDbType.Int, 4);
            parameterMtrIntNo.Value = mtrIntNo;
            myCommand.Parameters.Add(parameterMtrIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        /// <summary>
        /// Gets the traffic officer list by authority.
        /// </summary>
        /// <param name="nAutIntNo">The n aut int no.</param>
        /// <returns></returns>
        public SqlDataReader GetTrafficOfficerListByAuthority(int nAutIntNo)
        {
            return this.GetTrafficOfficerListByAuthority(nAutIntNo, OfficerSeniority.Both);
        }

        /// <summary>
        /// Gets the traffic officer list by authority.
        /// </summary>
        /// <param name="nAutIntNo">The n aut int no.</param>
        /// <param name="option">Whether to return senior officers, junior officers, or both.</param>
        /// <returns>A <see cref="SqlDataReader"/></returns>
        public SqlDataReader GetTrafficOfficerListByAuthority(int nAutIntNo, OfficerSeniority seniority)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("TrafficOfficerListByAuthority", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            myCommand.Parameters.Add("@Senior", SqlDbType.Int, 4).Value = (int)seniority;
            myCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = nAutIntNo;

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        public DataSet GetTrafficOfficerListByAuthorityDS(int nAutIntNo, OfficerSeniority seniority)
        {
            SqlDataAdapter sqlDATrafficOfficers = new SqlDataAdapter();
            DataSet dsTrafficOfficers = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDATrafficOfficers.SelectCommand = new SqlCommand();
            sqlDATrafficOfficers.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDATrafficOfficers.SelectCommand.CommandText = "TrafficOfficerListByAuthority";

            // Mark the Command as a SPROC
            sqlDATrafficOfficers.SelectCommand.CommandType = CommandType.StoredProcedure;

            sqlDATrafficOfficers.SelectCommand.Parameters.Add("@Senior", SqlDbType.Int, 4).Value = (int)seniority;
            sqlDATrafficOfficers.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = nAutIntNo;

            // Execute the command and close the connection
            sqlDATrafficOfficers.Fill(dsTrafficOfficers);
            sqlDATrafficOfficers.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsTrafficOfficers;
        }

        public DataSet GetTrafficOfficerListDS(int mtrIntNo, string partToNo = "")
        {
            SqlDataAdapter sqlDATrafficOfficers = new SqlDataAdapter();
            DataSet dsTrafficOfficers = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDATrafficOfficers.SelectCommand = new SqlCommand();
            sqlDATrafficOfficers.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDATrafficOfficers.SelectCommand.CommandText = "TrafficOfficerList";

            // Mark the Command as a SPROC
            sqlDATrafficOfficers.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterMtrIntNo = new SqlParameter("@MtrIntNo", SqlDbType.Int, 4);
            parameterMtrIntNo.Value = mtrIntNo;
            sqlDATrafficOfficers.SelectCommand.Parameters.Add(parameterMtrIntNo);

            SqlParameter parameterPartToNo = new SqlParameter("@PartToNo", SqlDbType.VarChar, 10);
            parameterPartToNo.Value = partToNo;
            sqlDATrafficOfficers.SelectCommand.Parameters.Add(parameterPartToNo);

            // Execute the command and close the connection
            sqlDATrafficOfficers.Fill(dsTrafficOfficers);
            sqlDATrafficOfficers.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsTrafficOfficers;
        }
    }
}

