using System;
using System.IO;
using System.Text;
using Stalberg.TMS;
using System.Globalization;

namespace Stalberg.TMS_TPExInt.Objects
{
	/// <summary>
	/// Represents the data that EasyPay and PayFine data exchange file.
	/// </summary>
	public class FineExchangeDataFile
	{
		// Fields
		private EasyPayAuthority authority;
		private readonly string fileName = string.Empty;
		private readonly string exportPath = string.Empty;
		private string machineName = string.Empty;
		private string batchNumber = string.Empty;
		private int version;
		private int id;
		private Stream stream;
		private readonly Encoding _encoding = Encoding.GetEncoding("Latin1");

		// Constants
		private const char DELIMITER = (char)255;
		/// <summary>
		/// Version 1 - Original
		/// Version 2 - Added a IsNoAOG (Y/N) to the charge reader and writer
		/// </summary>
		private const int VERSION = 2;

		public const string DATE_FORMAT = "yyyy-MM-dd_HH-mm-ss-ff";
		public const string FILE_PREFIX = "ThaboData";
		public const string EXTENSION = ".fedf";

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="FineExchangeDataFile"/> class.
		/// </summary>
		/// <param name="authority">The authority.</param>
		/// <param name="machineName">Name of the machine that is sending the file.</param>
		/// <param name="exportPath">The export path.</param>
		/// <param name="fileName">Name of the file.</param>
		public FineExchangeDataFile(EasyPayAuthority authority, string machineName, string exportPath, string fileName)
		{
			this.authority = authority;
			this.exportPath = exportPath;
			this.machineName = machineName;

			FileInfo file = new FileInfo(Path.Combine(this.exportPath, fileName));
			this.fileName = file.FullName;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FineExchangeDataFile"/> class.
		/// </summary>
		/// <param name="stream">The stream.</param>
		public FineExchangeDataFile(Stream stream)
		{
			if (stream.Position != 0)
				stream.Seek(0, SeekOrigin.Begin);
			this.stream = stream;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets the name of the file.
		/// </summary>
		/// <value>The name of the file.</value>
		public string FileName
		{
			get { return this.fileName; }
		}

		/// <summary>
		/// Gets the authority.
		/// </summary>
		/// <value>The authority.</value>
		public EasyPayAuthority Authority
		{
			get { return this.authority; }
		}

		/// <summary>
		/// Gets or sets the name of the machine that the data file came from.
		/// </summary>
		/// <value>The name of the machine.</value>
		public string MachineName
		{
			get { return this.machineName; }
			set { this.machineName = value; }
		}

		/// <summary>
		/// Gets the notice count.
		/// </summary>
		/// <value>The notice count.</value>
		public int NoticeCount
		{
			get { return this.id; }
		}

		/// <summary>
		/// Gets or sets the Batch Number.
		/// </summary>
		/// <value>The reg no.</value>
		public string BatchNumber
		{
			get { return this.batchNumber; }
			set { this.batchNumber = value; }
		}

		#endregion

		#region Write Methods

		/// <summary>
		/// Gets the data for the FineExchangeDataFile.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if there is valid data to be sent to the hub.
		/// </returns>
		public bool Write()
		{
			bool response = false;

			if (!this.authority.HasData)
				return response;



			// Create the writer stream
			FileInfo file = new FileInfo(this.fileName);
			if (!file.Directory.Exists)
				file.Directory.Create();

			var sw = new StreamWriter(file.Open(FileMode.Create), this._encoding);

			try
			{
				// Write authority data
				this.WriteHeader(sw);

				int notIntNo = 0;
				foreach (EasyPayFineData fine in authority.Fines)
				{
					if (notIntNo != fine.NoticeID)
						this.WriteNoticeData(fine, sw);

					// Write charge data by notice
					this.WriteCharge(fine, sw);

					// Write EasyPay data by notice and authority
					if (authority.IsEasyPay && notIntNo != fine.NoticeID)
						this.WriteEasyPayTransactions(fine, sw);

					notIntNo = fine.NoticeID;
				}

				// Log the file
				//this.writer.WriteLine("\tCompleted writing the export file: {0}", file.Name);
			}
			catch (Exception ex)
			{
				throw new ApplicationException("There was an error writing the export file.", ex);
			}
			finally
			{
				sw.Close();
				sw.Dispose();
			}

			return response;
		}

		private void WriteCharge(EasyPayFineData fine, StreamWriter writer)
		{
			writer.Write('C');
			writer.Write(DELIMITER);
            //update by Rachel 20140812 for 5337
			//writer.Write(fine.FineAmount);
            writer.Write(fine.FineAmount.ToString(CultureInfo.InvariantCulture));
            //end update by Rachel 20140812 for 5337
			writer.Write(DELIMITER);
			writer.Write(fine.Status);
			writer.Write(DELIMITER);
			writer.Write(fine.StatusDescription);
			writer.Write(DELIMITER);
			writer.Write(fine.OffenceCode);
			writer.Write(DELIMITER);
			writer.Write(fine.OffenceDescription);
			writer.Write(DELIMITER);
			writer.Write(fine.IsNoAOG);
			writer.Write("\n");
		}

		private void WriteNoticeData(EasyPayFineData fine, StreamWriter writer)
		{
			writer.Write('N');
			writer.Write(DELIMITER);
			writer.Write(fine.TicketNumber);
			writer.Write(DELIMITER);
			writer.Write(fine.CameraSerialNumber);
			writer.Write(DELIMITER);
			writer.Write(fine.FilmNumber);
			writer.Write(DELIMITER);
			writer.Write(fine.FrameNumber);
			writer.Write(DELIMITER);
			writer.Write(fine.Location);
			writer.Write(DELIMITER);
			writer.Write(fine.OffenceDate.ToString(DATE_FORMAT));
			writer.Write(DELIMITER);
			writer.Write(fine.OwnerID);
			writer.Write(DELIMITER);
			writer.Write(fine.OwnerName);
			writer.Write(DELIMITER);
			writer.Write(fine.PaymentDate.ToString(DATE_FORMAT));
			writer.Write(DELIMITER);
			writer.Write(fine.RegNo);
			writer.Write(DELIMITER);
			writer.Write(fine.Speed1);
			writer.Write(DELIMITER);
			writer.Write(fine.Speed2);
			writer.Write(DELIMITER);
			writer.Write(fine.SpeedZone);
			writer.Write("\n");
		}

		private void WriteEasyPayTransactions(EasyPayFineData fine, StreamWriter writer)
		{
			// Loop the EasyPay transactions to find a match on the NotIntNo
			EasyPayTransaction tran;
			for (int i = 0; i < this.authority.EasyPayTransactions.Count; i++)
			{
				tran = this.authority.EasyPayTransactions[i];

				if (tran.EasyPayNumber.Length > 0 && tran.NoticeID == fine.NoticeID)
				{
					writer.Write('E');
					writer.Write(DELIMITER);
					writer.Write(tran.EasyPayAction);
					writer.Write(DELIMITER);
					writer.Write(tran.EasyPayNumber);
					writer.Write(DELIMITER);
                    //update by Rachel 20140812 for 5337
					//writer.Write(tran.Amount.ToString());
                    writer.Write(tran.Amount.ToString(CultureInfo.InvariantCulture));
                    //end update by Rachel 20140812 for 5337
					writer.Write(DELIMITER);
					writer.Write(tran.ExpiryDate.ToString(DATE_FORMAT));
					writer.Write("\n");

					// Now its written remove it from the collection
					this.authority.EasyPayTransactions.Remove(tran);
					i--;
				}
			}
		}

		private void WriteHeader(StreamWriter writer)
		{
			writer.Write('H');
			// Authority details
			writer.Write(DELIMITER);
			writer.Write(this.machineName);
			writer.Write(DELIMITER);
			writer.Write(VERSION);
			writer.Write(DELIMITER);
			writer.Write(this.authority.Name);
			writer.Write(DELIMITER);
			writer.Write(this.authority.AuthorityCode);
			writer.Write(DELIMITER);
			writer.Write(this.authority.ReceiverIdentifier);
			writer.Write(DELIMITER);
			writer.Write(this.authority.AuthorityNumber);
			// Metro
			writer.Write(DELIMITER);
			writer.Write(this.authority.MetroCode);
			writer.Write(DELIMITER);
			writer.Write(this.authority.MetroName);
			writer.Write(DELIMITER);
			writer.Write(this.authority.MetroNumber);
			// Region
			writer.Write(DELIMITER);
			writer.Write(this.authority.RegionCode);
			writer.Write(DELIMITER);
			writer.Write(this.authority.RegionName);
			writer.Write(DELIMITER);
			writer.Write(this.authority.RegionPhone);
			writer.Write("\n");
		}

		#endregion

		#region Read Methods

		/// <summary>
		/// Reads the contents of this Fine Exchange Data File.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if there were no problems reading the file.
		/// </returns>
		public bool Read()
		{
			bool response = true;

			var reader = new StreamReader(this.stream, this._encoding);
			string fileContents = reader.ReadToEnd();
			reader.Close();
			this.stream.Close();
			this.id = 0;

			try
			{
				EasyPayFineData fine = null;
				string[] lines = fileContents.Split(new char[] { '\n' });
				foreach (string line in lines)
				{
					if (line.Trim().Length == 0)
						continue;
					try
					{
						switch (line[0])
						{

							case 'H':
								this.ReadHeader(line);
								break;

							case 'N':
								fine = this.ReadNotice(line);
								break;

							case 'C':
								this.ReadCharge(line, fine);
								break;

							case 'E':
								this.ReadEasyPay(line, fine);
								break;

							default:
								throw new ApplicationException(string.Format("There was a problem processing a line in {0}, it began with {1} which is not a valid Line Identifier.", this.FileName, line[0]));
						}

					}
					catch (Exception ex1)
					{
						throw new ApplicationException(string.Format("There was a problem reading the Fine Exchange Data File at line\n{1}\n{0}", line, ex1.Message), ex1);
					}
				}
			}
			catch (Exception ex)
			{
				response = false;
				throw new ApplicationException(string.Format("There was a problem reading the Fine Exchange Data File \n{0}", ex.Message), ex);
			}
			finally
			{
				reader.Close();
				reader.Dispose();
			}

			return response;
		}

		private void ReadEasyPay(string line, EasyPayFineData fine)
		{
			string[] values = line.Split(new char[] { DELIMITER });

			EasyPayTransaction tran = new EasyPayTransaction(this.authority);
			tran.EasyPayAction = values[1][0];
			tran.EasyPayNumber = values[2];
			tran.Amount = decimal.Parse(values[3]);
			tran.ExpiryDate = this.ParseDate(values[4]);
			tran.NoticeID = fine.NoticeID;

			this.authority.EasyPayTransactions.Add(tran);
		}

		private void ReadCharge(string line, EasyPayFineData fine)
		{
			if (fine.Status > 0)
			{
				fine = fine.Copy();
				this.authority.Fines.Add(fine);
			}

			string[] values = line.Split(new char[] { DELIMITER });
			fine.FineAmount = decimal.Parse(values[1]);
			fine.Status = int.Parse(values[2]);
			fine.StatusDescription = values[3];
			fine.OffenceCode = values[4];
			fine.OffenceDescription = values[5];
			if (this.version > 1)
				fine.IsNoAOG = values[6].Trim()[0];
		}

		private EasyPayFineData ReadNotice(string line)
		{
			string[] values = line.Split(new char[] { DELIMITER });

			EasyPayFineData fine = new EasyPayFineData(this.authority);

			fine.TicketNumber = values[1];
			fine.CameraSerialNumber = values[2];
			fine.FilmNumber = values[3];
			fine.FrameNumber = values[4];
			fine.Location = values[5];
			fine.OffenceDate = this.ParseDate(values[6]);
			fine.OwnerID = values[7];
			fine.OwnerName = values[8];
			fine.PaymentDate = this.ParseDate(values[9]);
			fine.RegNo = values[10];
			fine.Speed1 = int.Parse(values[11]);
			fine.Speed2 = int.Parse(values[12]);
			fine.SpeedZone = int.Parse(values[13]);
			fine.NoticeID = ++id;

			this.authority.Fines.Add(fine);

			return fine;
		}

		private DateTime ParseDate(string dateValue)
		{
			int year = int.Parse(dateValue.Substring(0, 4));
			int month = int.Parse(dateValue.Substring(5, 2));
			int day = int.Parse(dateValue.Substring(8, 2));
			int hour = int.Parse(dateValue.Substring(11, 2));
			int minute = int.Parse(dateValue.Substring(14, 2));
			int second = int.Parse(dateValue.Substring(17, 2));

			return new DateTime(year, month, day, hour, minute, second);
		}

		private void ReadHeader(string line)
		{
			string[] values = line.Split(new char[] { DELIMITER });

			this.machineName = values[1];
			this.version = int.Parse(values[2]);

			this.authority = new EasyPayAuthority();
			this.authority.Name = values[3];
			this.authority.AuthorityCode = values[4];
			this.authority.ReceiverIdentifier = int.Parse(values[5]);
			this.authority.AuthorityNumber = values[6];
			this.Authority.MetroCode = values[7];
			this.authority.MetroName = values[8];
			this.authority.MetroNumber = values[9];
			this.authority.RegionCode = values[10];
			this.authority.RegionName = values[11];
			this.authority.RegionPhone = values[12];
		}

		/// <summary>
		/// Deletes the underlying file that was red to create the data in this object.
		/// </summary>
		public void Delete()
		{
			if (this.fileName.Length == 0)
			{
				this.stream = null;
				return;
			}

			FileInfo file = new FileInfo(this.fileName);
			if (file.Exists)
			{
				try
				{
					file.Delete();
				}
				catch (IOException ie)
				{
					throw new ApplicationException(string.Format("There was a problem deleting the file {0}.\n{1}", file.Name, ie), ie);
				}
			}
			file = null;
		}

		#endregion

	}
}