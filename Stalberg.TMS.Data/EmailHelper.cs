using System;
using System.Configuration;
using System.Net.Mail;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Collections.Generic;

namespace Stalberg
{
    /// <summary>
    /// Represents a class that wraps the email functionality required to send emails
    /// </summary>
    public class EmailHelper
    {
        // Fields
        private string smtp;
        private string email;
        private string from = "support@in8.co.za";
        private string message = string.Empty;
        private string subject = string.Empty;
        private List<string> attachments;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailHelper"/> class.
        /// </summary>
        /// <param name="subject">The subject line of the email.</param>
        public EmailHelper(string subject)
            : this()
        {
            this.subject = subject;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailHelper"/> class.
        /// </summary>
        public EmailHelper()
        {
            this.attachments = new List<string>();

            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(Path.Combine(Environment.CurrentDirectory, "SysParam.xml"));

            XmlNode node = xDoc.SelectSingleNode("/codes/smtp");
            if (node != null)
                this.smtp = node.InnerText;
            node = xDoc.SelectSingleNode("/codes/systemEmail");
            if (node != null)
                this.email = node.InnerText;
            node = xDoc.SelectSingleNode("/codes/emailFrom");
            if (node != null)
                this.from = node.InnerText;
        }

        /// <summary>
        /// Gets the list of attachments.
        /// </summary>
        /// <value>The attachments.</value>
        public List<string> Attachments
        {
            get { return this.attachments; }
        }

        /// <summary>
        /// Gets or sets the subject line of the email.
        /// </summary>
        /// <value>The subject.</value>
        public string Subject
        {
            get { return this.subject; }
            set { this.subject = value; }
        }

        /// <summary>
        /// Gets or sets the message body of the email.
        /// </summary>
        /// <value>The message.</value>
        public string Message
        {
            get { return this.message; }
            set { this.message = value; }
        }

        /// <summary>
        /// Gets or sets the message receiver of the email.
        /// </summary>
        /// <value>The email.</value>
        public string Eamil
        {
            get { return this.email; }
            set { this.email = value; }
        }

        /// <summary>
        /// Tries to send the email.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if the Email was sent successfully.
        /// </returns>
        public bool Send()
        {
            bool response = false;

            MailMessage msg = new MailMessage(this.from, this.email, this.subject, this.message);
            msg.IsBodyHtml = false;

            if (this.attachments.Count > 0)
            {
                foreach (string attachment in this.attachments)
                {
                    if (File.Exists(attachment))
                        msg.Attachments.Add(new Attachment(attachment));
                }
            }

            try
            {
                SmtpClient server = new SmtpClient(this.smtp);
                server.Send(msg);
                response = true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Assert(false, ex.Message);
            }

            return response;
        }

    }
}
