using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Collections;

namespace Stalberg.TMS
{
    public class SpotFineBatchDetails
    {
        public Int32 SFBIntNo;
        public Int32 AutIntNo;
        public DateTime SFBRegisterDate;
        public DateTime SFBCreateDate;
        public string SFBCreateUser;
        public DateTime SFBCompleteDate;
        public string SFBCompleteUser;
        public DateTime SFBPrintDate;
        public string SFBPrintUser;
        public string SFBComments;
        public string LastUser;
    }

    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    public class SpotFineBatchDB
    {
        // Fields
        private string connectionString = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="SpotFineBatchDB"/> class.
        /// </summary>
        /// <param name="vConstr">The v constr.</param>
        public SpotFineBatchDB(string connectionString)
        {
            this.connectionString = connectionString;
        }

        //*******************************************************
        //
        // The GetSpotFineBatchDetails method returns a SpotFineBatchDetails
        // struct that contains information about a specific transaction number
        //
        //*******************************************************
        // 2013-07-19 comment by Henry for useless
        //public SpotFineBatchDetails GetSpotFineBatchDetails(int sfbIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(this.connectionString);
        //    SqlCommand myCommand = new SqlCommand("SpotFineBatchDetail", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterSFBIntNo = new SqlParameter("@SFBIntNo", SqlDbType.Int, 4);
        //    parameterSFBIntNo.Value = sfbIntNo;
        //    myCommand.Parameters.Add(parameterSFBIntNo);

        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Create CustomerDetails Struct
        //    SpotFineBatchDetails mySpotFineBatchDetails = new SpotFineBatchDetails();

        //    while (result.Read())
        //    {
        //        // Populate Struct using Output Params from SPROC
        //        mySpotFineBatchDetails.SFBIntNo = Convert.ToInt32(result["SFBIntNo"]);
        //        mySpotFineBatchDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
        //        mySpotFineBatchDetails.SFBRegisterDate = Convert.ToDateTime(result["SFBRegisterDate"]);
        //        mySpotFineBatchDetails.SFBCreateDate = Convert.ToDateTime(result["SFBCreateDate"]);
        //        mySpotFineBatchDetails.SFBCreateUser = result["SFBCreateUser"].ToString();
        //        mySpotFineBatchDetails.SFBCompleteDate = Convert.ToDateTime(result["SFBCompleteDate"]);
        //        mySpotFineBatchDetails.SFBCompleteUser = result["SFBCompleteUser"].ToString();
        //        mySpotFineBatchDetails.SFBPrintDate = Convert.ToDateTime(result["SFBPrintDate"]);
        //        mySpotFineBatchDetails.SFBPrintUser = result["SFBPrintUser"].ToString();
        //        mySpotFineBatchDetails.SFBComments = result["SFBComments"].ToString();
        //        mySpotFineBatchDetails.LastUser = result["LastUser"].ToString();
        //    }
        //    result.Close();
        //    return mySpotFineBatchDetails;
        //}

        
        public SqlDataReader GetSpotFineBatchList(int autIntNo, char CompletedBatches, char PrintedBatches)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionString);
            SqlCommand myCommand = new SqlCommand("SpotFineBatchList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCompletedBatches = new SqlParameter("@CompletedBatches", SqlDbType.VarChar, 1);
            parameterCompletedBatches.Value = CompletedBatches;
            myCommand.Parameters.Add(parameterCompletedBatches);


            SqlParameter parameterPrintedBatches = new SqlParameter("@PrintedBatches", SqlDbType.VarChar, 1);
            parameterPrintedBatches.Value = PrintedBatches;
            myCommand.Parameters.Add(parameterPrintedBatches);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        /// <summary>
        /// Gets the spot fine batch list DataSet.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="completedBatches">The completed batches.</param>
        /// <param name="printedBatches">The printed batches.</param>
        /// <returns></returns>
        public DataSet GetSpotFineBatchListDS(int autIntNo, string completedBatches, string printedBatches, string lastUser)
        {
            int totalCount = 0;
            return GetSpotFineBatchListDS(autIntNo, completedBatches, printedBatches, lastUser, 0, 0, out totalCount);
        }

        public DataSet GetSpotFineBatchListDS(int autIntNo, string completedBatches, string printedBatches, string lastUser, int pageSize, int pageIndex, out int totalCount)
        {
            //BD need to also pass through user as must only show batches for that user.
            
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand cmd = new SqlCommand("SpotFineBatchList", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value=autIntNo;
            cmd.Parameters.Add("@CompletedBatches", SqlDbType.VarChar, 1).Value=completedBatches;
            cmd.Parameters.Add("@PrintedBatches", SqlDbType.VarChar, 1).Value=printedBatches;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            cmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(paraTotalCount);

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            da.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            return ds;
        }

        /// <summary>
        /// Gets the spot fine batch list DataSet.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="completedBatches">The completed batches.</param>
        /// <param name="printedBatches">The printed batches.</param>
        /// <returns></returns>
        // 2013-07-26 add parameter userIntNo by Henry 
        public int UpdateSpotFineBatch(int sfbIntNo, int userIntNo, string lastUser, string updateReason)
        {
            //BD need to also pass through user as must only show batches for that user.

            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand cmd = new SqlCommand("UpdateSpotFineBatch", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@SFBIntNo", SqlDbType.Int, 4).Value = sfbIntNo;
            cmd.Parameters.Add("@UserIntNo", SqlDbType.Int, 4).Value = userIntNo;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            cmd.Parameters.Add("@UpdateReason", SqlDbType.VarChar, 250).Value = updateReason;

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();

                return 1;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Assert(false, ex.Message);
                return -1;
            }
            finally
            {
                con.Close();
            }
        }

        /// <summary>
        /// Gets the receipt data for the sport fine.
        /// </summary>
        /// <param name="rctIntNo">The Receipt int no.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetReceiptData(int rctIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand cmd = new SqlCommand("SpotFineBatchReceiptPrint", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@RctIntNo", SqlDbType.Int, 4).Value = rctIntNo;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        /// <summary>
        /// Gets the file request sequence number.
        /// </summary>
        /// <returns>An integer</returns>
        // 2013-07-26 comment out by Henry for useless
        //public int GetNextTranNo()
        //{            
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand cmd = new SqlCommand("SpotFineBatchNextTranNo", con);
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    try
        //    {
        //        con.Open();
        //        return Convert.ToInt32(cmd.ExecuteScalar());
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.Assert(false, ex.Message);
        //        return 0;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        /// <summary>
        /// Sets the next batch number for the requested user
        /// </summary>
        /// <returns>An integer</returns>
        public int AllocateNextBatch(string userName, int autIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand cmd = new SqlCommand("SpotFineBatch_AllocatedNewBatch", con);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterUserName = new SqlParameter("@UserName", SqlDbType.VarChar, 50);
            parameterUserName.Value = userName;
            cmd.Parameters.Add(parameterUserName);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            cmd.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterSFBIntNo = new SqlParameter("@SFBIntNo", SqlDbType.Int, 4);
            parameterSFBIntNo.Value = 0;
            parameterSFBIntNo.Direction = ParameterDirection.InputOutput;
            cmd.Parameters.Add(parameterSFBIntNo);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();

                int SFBIntNo = (int)parameterSFBIntNo.Value;
                return SFBIntNo;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Assert(false, ex.Message);
                return -1;
            }
            finally
            {
                con.Close();
            }
        }

    }
}
