using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{

    public partial class AccusedDetails
    {
        public int AccIntNo;
        public int NotIntNo;
        public string AccSurname;
        public string AccInitials;
        public string AccIDType;
        public string AccIDNumber;
        public string AccNationality;
        public string AccAge;
        public string AccPOAdd1;
        public string AccPOAdd2;
        public string AccPOAdd3;
        public string AccPOAdd4;
        public string AccPOAdd5;
        public string AccPOCode;
        public string AccStAdd1;
        public string AccStAdd2;
        public string AccStAdd3;
        public string AccStAdd4;
        public string AccStCode;
        public DateTime AccDateCOA;
        public string AccLicenceCode;
        public string AccLicencePlace;
        public string LastUser;
    }

    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    public partial class AccusedDB
    {
        string mConstr = "";

        public AccusedDB(string vConstr)
        {
            mConstr = vConstr;
        }

        //*******************************************************
        //
        // The GetAccusedDetails method returns a AccusedDetails
        // struct that contains information about a specific transaction number
        //
        //*******************************************************

        public AccusedDetails GetAccusedDetails(int AccIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AccusedDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAccIntNo = new SqlParameter("@AccIntNo", SqlDbType.Int, 4);
            parameterAccIntNo.Value = AccIntNo;
            myCommand.Parameters.Add(parameterAccIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            AccusedDetails myAccusedDetails = new AccusedDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myAccusedDetails.NotIntNo = Convert.ToInt32(result["NotIntNo"]);
                myAccusedDetails.AccIntNo = Convert.ToInt32(result["AccIntNo"]);
                myAccusedDetails.AccSurname = result["AccSurname"].ToString();
                myAccusedDetails.AccInitials = result["AccInitials"].ToString();
                myAccusedDetails.AccIDType = result["AccIDType"].ToString();
                myAccusedDetails.AccIDNumber = result["AccIDNumber"].ToString();
                myAccusedDetails.AccNationality = result["AccNationality"].ToString();
                myAccusedDetails.AccAge = result["AccAge"].ToString();
                myAccusedDetails.AccPOAdd1 = result["AccPoAdd1"].ToString();
                myAccusedDetails.AccPOAdd2 = result["AccPoAdd2"].ToString();
                myAccusedDetails.AccPOAdd3 = result["AccPoAdd3"].ToString();
                myAccusedDetails.AccPOAdd4 = result["AccPoAdd4"].ToString();
                myAccusedDetails.AccPOAdd5 = result["AccPoAdd5"].ToString();
                myAccusedDetails.AccPOCode = result["AccPoCode"].ToString();
                myAccusedDetails.AccStAdd1 = result["AccStAdd1"].ToString();
                myAccusedDetails.AccStAdd2 = result["AccStAdd2"].ToString();
                myAccusedDetails.AccStAdd3 = result["AccStAdd3"].ToString();
                myAccusedDetails.AccStAdd4 = result["AccStAdd4"].ToString();
                myAccusedDetails.AccStCode = result["AccStCode"].ToString();
                if (result["AccDateCOA"] != System.DBNull.Value)
                    myAccusedDetails.AccDateCOA = Convert.ToDateTime(result["AccDateCOA"]);
                myAccusedDetails.AccLicenceCode = result["AccLicenceCode"].ToString();
                myAccusedDetails.AccLicencePlace = result["AccLicencePlace"].ToString();
                myAccusedDetails.LastUser = result["LastUser"].ToString();
            }
            result.Close();
            return myAccusedDetails;

        }

        //public AccusedDetails GetAccusedDetailsByNotice(int NotIntNo) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("AccusedDetailByNotice", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
        //    parameterNotIntNo.Value = NotIntNo;
        //    myCommand.Parameters.Add(parameterNotIntNo);

        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Create CustomerDetails Struct
        //    AccusedDetails myAccusedDetails = new AccusedDetails();

        //    while (result.Read())
        //    {
        //        // Populate Struct using Output Params from SPROC
        //        myAccusedDetails.NotIntNo = Convert.ToInt32(result["NotIntNo"]);
        //        myAccusedDetails.AccIntNo = Convert.ToInt32(result["AccIntNo"]);
        //        myAccusedDetails.AccSurname = result["AccSurname"].ToString(); 
        //        myAccusedDetails.AccInitials = result["AccInitials"].ToString(); 
        //        myAccusedDetails.AccIDType = result["AccIDType"].ToString(); 
        //        myAccusedDetails.AccIDNumber = result["AccIDNumber"].ToString(); 
        //        myAccusedDetails.AccNationality = result["AccNationality"].ToString();
        //        myAccusedDetails.AccAge = result["AccAge"].ToString();
        //        myAccusedDetails.AccPOAdd1 = result["AccPoAdd1"].ToString();
        //        myAccusedDetails.AccPOAdd2 = result["AccPoAdd2"].ToString();
        //        myAccusedDetails.AccPOAdd3 = result["AccPoAdd3"].ToString();
        //        myAccusedDetails.AccPOAdd4 = result["AccPoAdd4"].ToString();
        //        myAccusedDetails.AccPOAdd5 = result["AccPoAdd5"].ToString();
        //        myAccusedDetails.AccPOCode = result["AccPoCode"].ToString();
        //        myAccusedDetails.AccStAdd1 = result["AccStAdd1"].ToString();
        //        myAccusedDetails.AccStAdd2 = result["AccStAdd2"].ToString();
        //        myAccusedDetails.AccStAdd3 = result["AccStAdd3"].ToString();
        //        myAccusedDetails.AccStAdd4 = result["AccStAdd4"].ToString();
        //        myAccusedDetails.AccStCode = result["AccStCode"].ToString();
        //        if (result["AccDateCOA"] != System.DBNull.Value)
        //            myAccusedDetails.AccDateCOA = Convert.ToDateTime(result["AccDateCOA"]);
        //        myAccusedDetails.AccLicenceCode = result["AccLicenceCode"].ToString();
        //        myAccusedDetails.AccLicencePlace = result["AccLicencePlace"].ToString();
        //        myAccusedDetails.LastUser = result["LastUser"].ToString();
        //    }

        //    result.Close();
        //    return myAccusedDetails;

        //}

        //*******************************************************
        //
        // The AddAccused method inserts a new transaction number record
        // into the Accused database.  A unique "AccIntNo"
        // key is then returned from the method.  
        //
        //*******************************************************
        public int AddAccused(int sumIntNo, DriverDetails driver)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("AccusedAdd", con);

            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;
            com.Parameters.Add("@AccSurname", SqlDbType.VarChar, 100).Value = driver.DrvSurname;
            com.Parameters.Add("@AccInitials", SqlDbType.VarChar, 10).Value = driver.DrvInitials;
            com.Parameters.Add("@AccForenames", SqlDbType.VarChar, 100).Value = driver.DrvForeNames;
            com.Parameters.Add("@AccIDType", SqlDbType.VarChar, 3).Value = driver.DrvIDType;
            com.Parameters.Add("@AccIDNumber", SqlDbType.VarChar, 25).Value = driver.DrvIDNumber;
            com.Parameters.Add("@AccNationality", SqlDbType.VarChar, 3).Value = driver.DrvNationality;
            com.Parameters.Add("@AccAge", SqlDbType.VarChar, 3).Value = driver.DrvAge;
            com.Parameters.Add("@AccPoAdd1", SqlDbType.VarChar, 100).Value = driver.DrvPOAdd1;
            com.Parameters.Add("@AccPoAdd2", SqlDbType.VarChar, 100).Value = driver.DrvPOAdd2;
            com.Parameters.Add("@AccPoAdd3", SqlDbType.VarChar, 100).Value = driver.DrvPOAdd3;
            com.Parameters.Add("@AccPoAdd4", SqlDbType.VarChar, 100).Value = driver.DrvPOAdd4;
            com.Parameters.Add("@AccPoAdd5", SqlDbType.VarChar, 100).Value = driver.DrvPOAdd5;
            com.Parameters.Add("@AccPoCode", SqlDbType.VarChar, 10).Value = driver.DrvPOCode;
            com.Parameters.Add("@AccStAdd1", SqlDbType.VarChar, 100).Value = driver.DrvStAdd1;
            com.Parameters.Add("@AccStAdd2", SqlDbType.VarChar, 100).Value = driver.DrvStAdd2;
            com.Parameters.Add("@AccStAdd3", SqlDbType.VarChar, 100).Value = driver.DrvStAdd3;
            com.Parameters.Add("@AccStAdd4", SqlDbType.VarChar, 100).Value = driver.DrvStAdd4;
            com.Parameters.Add("@AccStCode", SqlDbType.VarChar, 10).Value = driver.DrvStCode;
            com.Parameters.Add("@AccDateCOA", SqlDbType.SmallDateTime).Value = driver.DrvDateCOA;
            com.Parameters.Add("@AccLicenceCode", SqlDbType.VarChar, 3).Value = driver.DrvLicenceCode;
            com.Parameters.Add("@AccLicencePlace", SqlDbType.VarChar, 50).Value = driver.DrvLicencePlace;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = driver.LastUser;

            SqlParameter parameterAccIntNo = new SqlParameter("@AccIntNo", SqlDbType.Int, 4);
            parameterAccIntNo.Direction = ParameterDirection.Output;
            com.Parameters.Add(parameterAccIntNo);

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                object o = parameterAccIntNo.Value;
                return Convert.ToInt32(o);
            }
            catch
            {
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        // 2013-07-19 comment by Henry for useless
        //public SqlDataReader GetAccusedList(int notIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("AccusedList", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
        //    parameterNotIntNo.Value = notIntNo;
        //    myCommand.Parameters.Add(parameterNotIntNo);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the datareader result
        //    return result;
        //}

        // 2013-07-19 comment by Henry for useless
        //public DataSet GetAccusedListDS(int notIntNo)
        //{
        //    SqlDataAdapter sqlDAAccuseds = new SqlDataAdapter();
        //    DataSet dsAccuseds = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlDAAccuseds.SelectCommand = new SqlCommand();
        //    sqlDAAccuseds.SelectCommand.Connection = new SqlConnection(mConstr);
        //    sqlDAAccuseds.SelectCommand.CommandText = "AccusedList";

        //    // Mark the Command as a SPROC
        //    sqlDAAccuseds.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
        //    parameterNotIntNo.Value = notIntNo;
        //    sqlDAAccuseds.SelectCommand.Parameters.Add(parameterNotIntNo);

        //    // Execute the command and close the connection
        //    sqlDAAccuseds.Fill(dsAccuseds);
        //    sqlDAAccuseds.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsAccuseds;
        //}

        public int UpdateAccused(int accIntNo, int notIntNo, string accSurname, string accInitials,
            string accIDType, string accIDNumber, string accNationality,
            string accAge, string accPoAdd1, string accPoAdd2, string accPoAdd3,
            string accPoAdd4, string accPoAdd5, string accPoCode, string accStAdd1,
            string accStAdd2, string accStAdd3, string accStAdd4, string accStCode,
            string accDateCOA, string accLicenceCode, string accLicencePlace, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AccusedUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = notIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            SqlParameter parameterAccSurname = new SqlParameter("@AccSurname", SqlDbType.VarChar, 100);
            parameterAccSurname.Value = accSurname;
            myCommand.Parameters.Add(parameterAccSurname);

            SqlParameter parameterAccInitials = new SqlParameter("@AccInitials", SqlDbType.VarChar, 10);
            parameterAccInitials.Value = accInitials;
            myCommand.Parameters.Add(parameterAccInitials);

            SqlParameter parameterAccIDType = new SqlParameter("@AccIDType", SqlDbType.VarChar, 3);
            parameterAccIDType.Value = accIDType;
            myCommand.Parameters.Add(parameterAccIDType);

            SqlParameter parameterAccIDNumber = new SqlParameter("@AccIDNumber", SqlDbType.VarChar, 25);
            parameterAccIDNumber.Value = accIDNumber;
            myCommand.Parameters.Add(parameterAccIDNumber);

            SqlParameter parameterAccNationality = new SqlParameter("@AccNationality", SqlDbType.VarChar, 3);
            parameterAccNationality.Value = accNationality;
            myCommand.Parameters.Add(parameterAccNationality);

            SqlParameter parameterAccAge = new SqlParameter("@AccAge", SqlDbType.VarChar, 3);
            parameterAccAge.Value = accAge;
            myCommand.Parameters.Add(parameterAccAge);

            SqlParameter parameterAccPoAdd1 = new SqlParameter("@AccPoAdd1", SqlDbType.VarChar, 100);
            parameterAccPoAdd1.Value = accPoAdd1;
            myCommand.Parameters.Add(parameterAccPoAdd1);

            SqlParameter parameterAccPoAdd2 = new SqlParameter("@AccPoAdd2", SqlDbType.VarChar, 100);
            parameterAccPoAdd2.Value = accPoAdd2;
            myCommand.Parameters.Add(parameterAccPoAdd2);

            SqlParameter parameterAccPoAdd3 = new SqlParameter("@AccPoAdd3", SqlDbType.VarChar, 100);
            parameterAccPoAdd3.Value = accPoAdd3;
            myCommand.Parameters.Add(parameterAccPoAdd3);

            SqlParameter parameterAccPoAdd4 = new SqlParameter("@AccPoAdd4", SqlDbType.VarChar, 100);
            parameterAccPoAdd4.Value = accPoAdd4;
            myCommand.Parameters.Add(parameterAccPoAdd4);

            SqlParameter parameterAccPoAdd5 = new SqlParameter("@AccPoAdd5", SqlDbType.VarChar, 100);
            parameterAccPoAdd5.Value = accPoAdd5;
            myCommand.Parameters.Add(parameterAccPoAdd5);

            SqlParameter parameterAccPoCode = new SqlParameter("@AccPoCode", SqlDbType.VarChar, 10);
            parameterAccPoCode.Value = accPoCode;
            myCommand.Parameters.Add(parameterAccPoCode);

            SqlParameter parameterAccStAdd1 = new SqlParameter("@AccStAdd1", SqlDbType.VarChar, 100);
            parameterAccStAdd1.Value = accStAdd1;
            myCommand.Parameters.Add(parameterAccStAdd1);

            SqlParameter parameterAccStAdd2 = new SqlParameter("@AccStAdd2", SqlDbType.VarChar, 100);
            parameterAccStAdd2.Value = accStAdd2;
            myCommand.Parameters.Add(parameterAccStAdd2);

            SqlParameter parameterAccStAdd3 = new SqlParameter("@AccStAdd3", SqlDbType.VarChar, 100);
            parameterAccStAdd3.Value = accStAdd3;
            myCommand.Parameters.Add(parameterAccStAdd3);

            SqlParameter parameterAccStAdd4 = new SqlParameter("@AccStAdd4", SqlDbType.VarChar, 100);
            parameterAccStAdd4.Value = accStAdd4;
            myCommand.Parameters.Add(parameterAccStAdd4);

            SqlParameter parameterAcc = new SqlParameter("@AccStCode", SqlDbType.VarChar, 10);
            parameterAcc.Value = accStCode;
            myCommand.Parameters.Add(parameterAcc);

            SqlParameter parameterAccDateCOA = new SqlParameter("@AccDateCOA", SqlDbType.VarChar, 10);
            parameterAccDateCOA.Value = accDateCOA;
            myCommand.Parameters.Add(parameterAccDateCOA);

            SqlParameter parameterAccLicenceCode = new SqlParameter("@AccLicenceCode", SqlDbType.VarChar, 3);
            parameterAccLicenceCode.Value = accLicenceCode;
            myCommand.Parameters.Add(parameterAccLicenceCode);

            SqlParameter parameterAccLicencePlace = new SqlParameter("@AccLicencePlace", SqlDbType.VarChar, 50);
            parameterAccLicencePlace.Value = accLicencePlace;
            myCommand.Parameters.Add(parameterAccLicencePlace);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterAccIntNo = new SqlParameter("@AccIntNo", SqlDbType.Int, 4);
            parameterAccIntNo.Value = accIntNo;
            parameterAccIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterAccIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int AccIntNo = (int)myCommand.Parameters["@AccIntNo"].Value;

                return AccIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// Gets the accused by summons int no.
        /// </summary>
        /// <param name="sumIntNo">The summons int no.</param>
        /// <returns>A <see cref="DriverDetails"/></returns>
        public DriverDetails GetAccusedBySumIntNo(int sumIntNo)
        {
            DriverDetails driver = new DriverDetails();

            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("AccusedDetailBySumIntNo", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;

            try
            {
                con.Open();
                SqlDataReader result = com.ExecuteReader();

                if (result.Read())
                {
                    // Populate Struct using Output Params from SPROC
                    driver.DrvIntNo = Convert.ToInt32(result["AccIntNo"]);
                    driver.DrvSurname = result["AccSurname"].ToString();
                    driver.DrvInitials = result["AccInitials"].ToString();
                    driver.DrvForeNames = Helper.GetReaderValue<string>(result, "AccForenames");
                    driver.DrvFullName = Helper.GetReaderValue<string>(result, "AccFullname");
                    driver.DrvIDType = result["AccIDType"].ToString();
                    driver.DrvIDNumber = result["AccIDNumber"].ToString();
                    driver.DrvNationality = result["AccNationality"].ToString();
                    driver.DrvAge = result["AccAge"].ToString();
                    driver.DrvPOAdd1 = result["AccPoAdd1"].ToString();
                    driver.DrvPOAdd2 = result["AccPoAdd2"].ToString();
                    driver.DrvPOAdd3 = result["AccPoAdd3"].ToString();
                    driver.DrvPOAdd4 = result["AccPoAdd4"].ToString();
                    driver.DrvPOAdd5 = result["AccPoAdd5"].ToString();
                    driver.DrvPOCode = result["AccPoCode"].ToString();
                    driver.DrvStAdd1 = result["AccStAdd1"].ToString();
                    driver.DrvStAdd2 = result["AccStAdd2"].ToString();
                    driver.DrvStAdd3 = result["AccStAdd3"].ToString();
                    driver.DrvStAdd4 = result["AccStAdd4"].ToString();
                    driver.DrvStCode = result["AccStCode"].ToString();
                    driver.DrvDateCOA = Helper.GetReaderValue<DateTime>(result, "AccDateCOA");
                    driver.DrvLicenceCode = result["AccLicenceCode"].ToString();
                    driver.DrvLicencePlace = result["AccLicencePlace"].ToString();
                    driver.LastUser = result["LastUser"].ToString();
                    driver.DrvHomeNo = Helper.GetReaderValue<string>(result, "AccTelHome");
                    driver.DrvWorkNo = Helper.GetReaderValue<string>(result, "AccTelWork");
                    driver.DrvCellNo = Helper.GetReaderValue<string>(result, "AccMobile");
                }
                result.Close();
            }
            finally
            {
                con.Close();
            }

            return driver;
        }

        /// <summary>
        /// Update Accused
        /// Jerry 2014-01-22 add ref string message parameter
        /// </summary>
        /// <param name="offerDetail"></param>
        /// <param name="sumIntNo"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public int UpdateAccused(OffenderDetails offerDetail, int sumIntNo, ref string message)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("AccusedUpdate", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@SumIntNo", SqlDbType.Int,4).Value = sumIntNo;
            com.Parameters.Add("@ID", SqlDbType.Int, 4).Value = offerDetail.ID;
            com.Parameters.Add("@AccSurname", SqlDbType.VarChar, 100).Value = offerDetail.Surname ;
            com.Parameters.Add("@AccForeName", SqlDbType.VarChar, 100).Value = offerDetail.ForeNames ;
            com.Parameters.Add("@AccIDNumber", SqlDbType.VarChar, 25).Value = offerDetail.IDNumber;
            com.Parameters.Add("@AccPoAdd1", SqlDbType.VarChar, 100).Value = offerDetail.PoAddress1 ;
            com.Parameters.Add("@AccPoAdd2", SqlDbType.VarChar, 100).Value = offerDetail.PoAddress2;
            com.Parameters.Add("@AccPoAdd3", SqlDbType.VarChar, 100).Value = offerDetail.PoAddress3;
            com.Parameters.Add("@AccPoAdd4", SqlDbType.VarChar, 100).Value = offerDetail.PoAddress4;
            com.Parameters.Add("@AccPoCode", SqlDbType.VarChar, 10).Value = offerDetail.PoAreaCode ;
            com.Parameters.Add("@AccStrAdd1", SqlDbType.VarChar, 100).Value = offerDetail.Address1;
            com.Parameters.Add("@AccStrAdd2", SqlDbType.VarChar, 100).Value = offerDetail.Address2;
            com.Parameters.Add("@AccStrAdd3", SqlDbType.VarChar, 100).Value = offerDetail.Address3;
            com.Parameters.Add("@AccStrAdd4", SqlDbType.VarChar, 100).Value = offerDetail.Address4;
            com.Parameters.Add("@AccStrCode", SqlDbType.VarChar, 10).Value = offerDetail.AreaCode;
            com.Parameters.Add("@CellNo", SqlDbType.VarChar, 15).Value = offerDetail.CellNo;
            com.Parameters.Add("@HomeNo", SqlDbType.VarChar, 15).Value = offerDetail.HomeNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = offerDetail.LastUser;

            try
            {
                con.Open();
                return com.ExecuteNonQuery();
            }
            catch (Exception ex)//Jerry 2014-01-22 add
            {
                message = ex.Message;
                return 0;
            }
            finally
            {
                con.Close();
            }
     
        }
      
    }
}
