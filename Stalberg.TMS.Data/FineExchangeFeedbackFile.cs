using System;
using System.Configuration;
using System.IO;
using System.Text;
using Stalberg.TMS_TPExInt.Objects;
using System.Globalization;

namespace Stalberg.ThaboAggregator.Objects
{
    /// <summary>
    /// Represents a file that contains feedback information on Fine data that has been sent to the Thabo service
    /// </summary>
    public class FineExchangeFeedbackFile
    {
        // Constants
        private const string FILE_PREFIX = "FineExchangeFeedbackFine";
        private const string FILE_DATE_FORMAT = "yyyy-MM-dd_HH-MM-ss-ffff";
        private const string DATA_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
        private const char DELIMITER = (char)255;
        private const decimal VERSION = 2.0M;

        /// <summary>
        /// The file extension for Fine Exchange Feedback Files (*.feff)
        /// </summary>
        public static readonly string EXTENSION = ".feff";

        // Fields
        private RemoteAuthority _authority;
        private string _fileName = string.Empty;
        private StreamWriter _writer;
        private readonly Encoding _encoding = Encoding.GetEncoding("Latin1");

        /// <summary>
        /// Initializes a new instance of the <see cref="FineExchangeFeedbackFile"/> class.
        /// </summary>
        /// <param name="authority">The details of the authority to send the feedback file to.</param>
        public FineExchangeFeedbackFile(RemoteAuthority authority)
        {
            this._authority = authority;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FineExchangeFeedbackFile"/> class.
        /// </summary>
        /// <param name="stream">The stream.</param>
        public FineExchangeFeedbackFile(Stream stream)
        {
            StreamReader sr = new StreamReader(stream, this._encoding, true);
            string contents = sr.ReadToEnd();
            sr.Close();

            this.Read(contents);
        }

        /// <summary>
        /// Gets the authority to send this feedback file to.
        /// </summary>
        /// <value>The authority.</value>
        public RemoteAuthority Authority
        {
            get { return this._authority; }
        }

        /// <summary>
        /// Gets the name of the file.
        /// </summary>
        /// <value>The name of the file.</value>
        public string FileName
        {
            get { return this._fileName; }
        }

        /// <summary>
        /// Deletes this instance.
        /// </summary>
        public void Dispose()
        {
            this._writer.Close();
            if (this._fileName.Length > 0)
                File.Delete(this._fileName);
        }

        /// <summary>
        /// Writes the header row into the file, having created the file.
        /// </summary>
        public void WriteHeader()
        {
            this._fileName = Path.Combine(ConfigurationManager.AppSettings["FeedbackPath"],
                string.Format("{0}_{1}_{2}{3}", FILE_PREFIX, this._authority.AuthorityCode, DateTime.Now.ToString(FILE_DATE_FORMAT), EXTENSION));

            FileInfo file = new FileInfo(this._fileName);
            this._writer = new StreamWriter(file.Open(FileMode.Create), this._encoding);

            this._writer.Write('H');
            this._writer.Write(DELIMITER);
            this._writer.Write(this._authority.AuthorityCode);
            this._writer.Write(DELIMITER);
            this._writer.Write(this._authority.Name);
            this._writer.Write(DELIMITER);
            this._writer.Write(this._authority.ReceiptID.ToString());
            this._writer.Write(DELIMITER);
            this._writer.Write(DateTime.Now.ToString(DATA_DATE_FORMAT));
            this._writer.Write(DELIMITER);
            this._writer.Write(VERSION);
            this._writer.Write('\n');
        }

        /// <summary>
        /// Jerry 2013-09-24 add for win service
        /// Writes the header row into the file, having created the file.
        /// </summary>
        /// <param name="feedbackPath"></param>
        public void WriteHeader(string feedbackPath)
        {
            this._fileName = Path.Combine(feedbackPath,
                string.Format("{0}_{1}_{2}{3}", FILE_PREFIX, this._authority.AuthorityCode, DateTime.Now.ToString(FILE_DATE_FORMAT), EXTENSION));

            FileInfo file = new FileInfo(this._fileName);
            this._writer = new StreamWriter(file.Open(FileMode.Create), this._encoding);

            this._writer.Write('H');
            this._writer.Write(DELIMITER);
            this._writer.Write(this._authority.AuthorityCode);
            this._writer.Write(DELIMITER);
            this._writer.Write(this._authority.Name);
            this._writer.Write(DELIMITER);
            this._writer.Write(this._authority.ReceiptID.ToString());
            this._writer.Write(DELIMITER);
            this._writer.Write(DateTime.Now.ToString(DATA_DATE_FORMAT));
            this._writer.Write(DELIMITER);
            this._writer.Write(VERSION);
            this._writer.Write('\n');
        }

        /// <summary>
        /// Closes the feedback file's stream writer
        /// </summary>
        public void Close()
        {
            if (this._writer != null)
            {
                this._writer.Flush();
                this._writer.Close();
            }
        }

        /// <summary>
        /// Writes the EasyPay error information.
        /// </summary>
        /// <param name="epNo">The ep no.</param>
        /// <param name="error">The error.</param>
        public void WriteError(string epNo, string error)
        {
            this._writer.Write('E');
            this._writer.Write(DELIMITER);
            this._writer.Write(epNo);
            this._writer.Write(DELIMITER);
            this._writer.Write(error);
            this._writer.Write('\n');
        }

        /// <summary>
        /// Writers the payment feedback.
        /// </summary>
        /// <param name="ticketNo">The ticket no.</param>
        /// <param name="partnerId">The partner id.</param>
        /// <param name="source">The source.</param>
        /// <param name="paymentDate">The payment date.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="msgDateTime">The MSG date time.</param>
        /// <param name="paymentAction">The payment action.</param>
        /// <param name="transType">Type of the trans.</param>
        public void WriterPaymentFeedback(string ticketNo, string partnerId, string source, DateTime paymentDate, decimal amount, DateTime msgDateTime, byte paymentAction, string transType)
        {
            this._writer.Write('P');
            this._writer.Write(DELIMITER);
            this._writer.Write(ticketNo);
            this._writer.Write(DELIMITER);
            this._writer.Write(partnerId);
            this._writer.Write(DELIMITER);
            this._writer.Write(source);
            this._writer.Write(DELIMITER);
            this._writer.Write(paymentDate.ToString(DATA_DATE_FORMAT));
            this._writer.Write(DELIMITER);

            //update by Rachel 20140819 for 5337
            //this._writer.Write(amount.ToString("0.00"));
            this._writer.Write(amount.ToString("0.00",CultureInfo.InvariantCulture));
            //end update by Rachel 20140819 for 5337

            this._writer.Write(DELIMITER);
            this._writer.Write(msgDateTime.ToString(DATA_DATE_FORMAT));
            this._writer.Write(DELIMITER);
            this._writer.Write(paymentAction);
            this._writer.Write(DELIMITER);
            this._writer.Write(transType);
            this._writer.Write("\n");
        }

        private void Read(string contents)
        {
            String[] lines = contents.Split(new char[] { '\n' });
            foreach (string line in lines)
            {
                if (line.Trim().Length == 0)
                    continue;

                switch (line[0])
                {
                    case 'H': // Header
                        this.ReadHeader(line);
                        break;

                    case 'E': // EasyPay
                        this.ReadEasyPayInfo(line, (FeedbackAuthority)this._authority);
                        break;

                    case 'P': // Payment
                        this.ReadPayment(line, (FeedbackAuthority)this._authority);
                        break;

                    default:
                        Console.WriteLine("{0} is not a know line type for a feedback file.", line[0]);
                        break;
                }
            }
        }

        private void ReadEasyPayInfo(string line, FeedbackAuthority feedbackAuthority)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        private void ReadPayment(string line, FeedbackAuthority authority)
        {
            string[] values = line.Split(new char[] { DELIMITER });

            FeedbackPayment payment = new FeedbackPayment();
            payment.TicketNo = values[1];
            payment.PartnerID = values[2];
            payment.SetSource(values[3]);
            payment.PaymentDate = DateTime.Parse(values[4]);
            payment.Amount = decimal.Parse(values[5]);
            if (values.Length > 6)
                payment.MessageDate = DateTime.Parse(values[6]);
            if (values.Length > 7)
                payment.PaymentAction = byte.Parse(values[7]);
            if (values.Length > 8)
                payment.Type = values[8];

            authority.Payments.Add(payment);
        }

        private void ReadHeader(string line)
        {
            string[] values = line.Split(new char[] { DELIMITER });

            this._authority = new FeedbackAuthority();
            this._authority.AuthorityCode = values[1];
            this._authority.Name = values[2];
            this._authority.ReceiptID = int.Parse(values[3]);
            ((FeedbackAuthority)this._authority).DateSent = DateTime.Parse(values[4]);
        }

    }
}
