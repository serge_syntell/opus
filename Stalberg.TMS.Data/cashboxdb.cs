using System;
using System.Data;
using System.Data.SqlClient;


namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the details of the contains a cash box float
    /// </summary>
    public class CashboxDetails
    {
        public int CBIntNo;
        public int AutIntNo;
        public string CBName;
        public double CBStartAmount;
        public int CBRand500s;
        public int CBRand200s;
        public int CBRand100s;
        public int CBRand50s;
        public int CBRand20s;
        public int CBRand10s;
        public int CBRand5s;
        public int CBRand2s;
        public int CBRand1s;
        public double CBReceived;
        public double CBBanked;
        public char CashBoxType;
    }

    /// <summary>
    /// Represents the contents of of a recon
    /// </summary>
    public class CashBoxRecon
    {
        public int CBIntNo;
        public int R500;
        public int R200;
        public int R100;
        public int R50;
        public int R20;
        public int R10;
        public int R5;
        public int R2;
        public int R1;
        public int ChequeCount;
        public decimal ChequeAmount;
        public int CardCount;
        public int DebitCardCount; // SD: 20081210
        public decimal CardAmount;
        public decimal DebitCardAmount; // SD: 20081210
        public int POCount;
        public decimal POAmount;
        public decimal Float;
        public string LastUser; // 2013-07-23 add by Henry

        /// <summary>
        /// Gets the cash part of the reconciliation.
        /// </summary>
        /// <value>The cash.</value>
        public decimal Cash
        {
            get
            {
                return (
                    (this.R500 * 500) +
                    (this.R50 * 50) +
                    (this.R5 * 5) +
                    (this.R200 * 200) +
                    (this.R20 * 20) +
                    (this.R2 * 2) +
                    (this.R100 * 100) +
                    (this.R10 * 10) +
                    (decimal)this.R1
                    );
            }
        }
    }

    /// <summary>
    /// Represents the data in the deposit slips for today
    /// </summary>
    public class DBReconData
    {
        public int CBIntNo;
        public decimal Cash;
        public int ChequeCount;
        public decimal ChequeAmount;
        public int CardCount;
        public int DebitCardCount;// SD: 20081210
        public decimal CardAmount;
        public decimal DebitCardAmount; // SD: 20081210
        public int PostalOrderCount;
        public decimal PostalOrderAmount;
        public decimal Float;
    }

    /// <summary>
    /// The type or usage of the cash box.
    /// </summary>
    [Serializable]
    public enum CashboxType
    {
        /// <summary>
        /// The cash box will be used for the over-the-counter receipts at the LA
        /// </summary>
        OverTheCounter = 67, // C (Counter)
        /// <summary>
        /// The cashbox is 
        /// </summary>
        PostalReceipts = 80, // P - (Postal)
        /// <summary>
        /// The cashbox is being used at a road-block
        /// </summary>
        RoadblockReceipts = 82 // R - (Roadblock)
    }

    public class Cashbox_UserDB
    {
        private int CBUIntNo;
        private int UserIntNo;
        private int CurrentCBIntNo;
        // Fields
        private string connectionString = string.Empty;

        public Cashbox_UserDB(string vConstr)
        {
            connectionString = vConstr;
        }

        public DataSet GetCashbox_UserDBList()
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "Cashbox_UserDBList";


            SqlDataAdapter adapter = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            adapter.Fill(ds);

            return ds;
        }

        public SqlDataReader GetCashierList()
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand cmd = new SqlCommand("CashierList", con);
            cmd.CommandType = CommandType.StoredProcedure;

            con.Open();
            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            return reader;
        }

        /// <summary>
        /// Adds the cash-box user DB.
        /// </summary>
        /// <param name="userIntNo">The user int no.</param>
        /// <returns></returns>
        public int AddCashbox_UserDB(int userIntNo, ref string errMessage)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Cashbox_UserDBAdd";

            SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int, 4);
            parameterUserIntNo.Value = userIntNo;
            cmd.Parameters.Add(parameterUserIntNo);

            //SqlParameter parameterCashBoxInt = new SqlParameter("@CashBoxInt", SqlDbType.Int, 4);
            //parameterCashBoxInt.Value = cashBoxInt;
            //com.Parameters.Add(parameterCashBoxInt);

            SqlParameter parameterCashbox_UserIntNo = new SqlParameter("@Cashbox_UserIntNo", SqlDbType.Int, 4);
            parameterCashbox_UserIntNo.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(parameterCashbox_UserIntNo);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();

                int cashbox_UserIntNo = Convert.ToInt32(parameterCashbox_UserIntNo.Value);

                return cashbox_UserIntNo;
            }
            catch (Exception e)
            {
                con.Dispose();
                errMessage = e.Message;
                return 0;
            }
        }

        public int DeleteCashBox_User(int cashBox_UserIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand cmd = new SqlCommand("CashBox_UserDelete", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCashBox_UserIntNo = new SqlParameter("@CashBox_UserIntNo", SqlDbType.Int, 4);
            parameterCashBox_UserIntNo.Value = cashBox_UserIntNo;
            parameterCashBox_UserIntNo.Direction = ParameterDirection.InputOutput;
            cmd.Parameters.Add(parameterCashBox_UserIntNo);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                cashBox_UserIntNo = (int)parameterCashBox_UserIntNo.Value;

                return cashBox_UserIntNo;
            }
            catch
            {
                con.Dispose();
                return 0;
            }
        }
    }

    /// <summary>
    /// Represents a class that contains all the logic for interacting with the database cashbox table
    /// </summary>
    public class CashboxDB
    {
        // Fields
        private string connectionString = string.Empty;

        public CashboxDB(string vConstr)
        {
            connectionString = vConstr;
        }

        public CashboxDetails GetCashboxDetails(int cbIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CashboxDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCBIntNo = new SqlParameter("@CBIntNo", SqlDbType.Int, 4);
            parameterCBIntNo.Value = cbIntNo;
            myCommand.Parameters.Add(parameterCBIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            CashboxDetails myCashboxDetails = new CashboxDetails();

            while (result.Read())
            {
                myCashboxDetails.CBIntNo = Convert.ToInt32(result["CBIntNo"]);
                //myCashboxDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
                myCashboxDetails.CBName = result["CBName"].ToString();
                myCashboxDetails.CBStartAmount = Convert.ToDouble(result["CBStartAmount"]);
                myCashboxDetails.CBRand500s = Convert.ToInt32(result["CBRand500s"]);
                myCashboxDetails.CBRand200s = Convert.ToInt32(result["CBRand200s"]);
                myCashboxDetails.CBRand100s = Convert.ToInt32(result["CBRand100s"]);
                myCashboxDetails.CBRand50s = Convert.ToInt32(result["CBRand50s"]);
                myCashboxDetails.CBRand20s = Convert.ToInt32(result["CBRand20s"]);
                myCashboxDetails.CBRand10s = Convert.ToInt32(result["CBRand10s"]);
                myCashboxDetails.CBRand5s = Convert.ToInt32(result["CBRand5s"]);
                myCashboxDetails.CBRand2s = Convert.ToInt32(result["CBRand2s"]);
                myCashboxDetails.CBRand1s = Convert.ToInt32(result["CBRand1s"]);
                myCashboxDetails.CBReceived = Convert.ToDouble(result["CBReceived"]);
                myCashboxDetails.CBBanked = Convert.ToDouble(result["CBBanked"]);
            }
            result.Close();
            return myCashboxDetails;
        }

        public int AddCashbox(string cbName, double cbStartAmount, int cbRand500s, int cbRand200s, int cbRand100s, int cbRand50s,
            int cbRand20s, int cbRand10s, int cbRand5s, int cbRand2s, int cbRand1s, double cbReceived, double cbBanked, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CashboxAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            //SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            //parameterAutIntNo.Value = autIntNo;
            //myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCBName = new SqlParameter("@CBName", SqlDbType.VarChar, 30);
            parameterCBName.Value = cbName;
            myCommand.Parameters.Add(parameterCBName);

            SqlParameter parameterCBStartAmount = new SqlParameter("@CBStartAmount", SqlDbType.Real);
            parameterCBStartAmount.Value = cbStartAmount;
            myCommand.Parameters.Add(parameterCBStartAmount);

            SqlParameter parameterCBRand500s = new SqlParameter("@CBRand500s", SqlDbType.Int);
            parameterCBRand500s.Value = cbRand500s;
            myCommand.Parameters.Add(parameterCBRand500s);

            SqlParameter parameterCBRand200s = new SqlParameter("@CBRand200s", SqlDbType.Int);
            parameterCBRand200s.Value = cbRand200s;
            myCommand.Parameters.Add(parameterCBRand200s);

            SqlParameter parameterCBRand100s = new SqlParameter("@CBRand100s", SqlDbType.Int);
            parameterCBRand100s.Value = cbRand100s;
            myCommand.Parameters.Add(parameterCBRand100s);

            SqlParameter parameterCBRand50s = new SqlParameter("@CBRand50s", SqlDbType.Int);
            parameterCBRand50s.Value = cbRand50s;
            myCommand.Parameters.Add(parameterCBRand50s);

            SqlParameter parameterCBRand20s = new SqlParameter("@CBRand20s", SqlDbType.Int);
            parameterCBRand20s.Value = cbRand20s;
            myCommand.Parameters.Add(parameterCBRand20s);

            SqlParameter parameterCBRand10s = new SqlParameter("@CBRand10s", SqlDbType.Int);
            parameterCBRand10s.Value = cbRand10s;
            myCommand.Parameters.Add(parameterCBRand10s);

            SqlParameter parameterCBRand5s = new SqlParameter("@CBRand5s", SqlDbType.Int);
            parameterCBRand5s.Value = cbRand5s;
            myCommand.Parameters.Add(parameterCBRand5s);

            SqlParameter parameterCBRand2s = new SqlParameter("@CBRand2s", SqlDbType.Int);
            parameterCBRand2s.Value = cbRand2s;
            myCommand.Parameters.Add(parameterCBRand2s);

            SqlParameter parameterCBRand1s = new SqlParameter("@CBRand1s", SqlDbType.Int);
            parameterCBRand1s.Value = cbRand1s;
            myCommand.Parameters.Add(parameterCBRand1s);

            SqlParameter parameterCBReceived = new SqlParameter("@CBReceived", SqlDbType.Real);
            parameterCBReceived.Value = cbReceived;
            myCommand.Parameters.Add(parameterCBReceived);

            SqlParameter parameterCBBanked = new SqlParameter("@CBBanked", SqlDbType.Real);
            parameterCBBanked.Value = cbBanked;
            myCommand.Parameters.Add(parameterCBBanked);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterCBIntNo = new SqlParameter("@CBIntNo", SqlDbType.Int, 4);
            parameterCBIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterCBIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                int cbIntNo = Convert.ToInt32(parameterCBIntNo.Value);
                return cbIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        // 2013-07-19 comment by Henry for useless
        //public int GetCashbox(int autIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    SqlCommand myCommand = new SqlCommand("CashboxesByAuth", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    myCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

        //    SqlParameter parameterCBIntNo = new SqlParameter("@CBIntNo", SqlDbType.Int, 4);
        //    parameterCBIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterCBIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int cbIntNo = Convert.ToInt32(parameterCBIntNo.Value);

        //        return cbIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        public SqlDataReader GetCashboxList(int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CashboxList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            //Jerry 2013-10-12 remove the parameter
            //SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            //parameterAutIntNo.Value = autIntNo;
            //myCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        /// <summary>
        /// Gets the cashbox list DataSet.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <returns></returns>
        public DataSet GetCashboxListDS()
        {
            SqlDataAdapter sqlDACashboxes = new SqlDataAdapter();
            DataSet dsCashboxes = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDACashboxes.SelectCommand = new SqlCommand();
            sqlDACashboxes.SelectCommand.Connection = new SqlConnection(connectionString);
            sqlDACashboxes.SelectCommand.CommandText = "CashboxList";
            sqlDACashboxes.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Parameters
            //sqlDACashboxes.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            //sqlDACashboxes.SelectCommand.Parameters.Add("@ShowAll", SqlDbType.Bit, 1).Value = showAll;

            // Execute the command and close the connection
            sqlDACashboxes.Fill(dsCashboxes);
            sqlDACashboxes.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsCashboxes;
        }


                /// <summary>
        /// Gets the cashbox list DataSet.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <returns></returns>
        public DataSet GetCashboxListDS(int autIntNo,
            int pageSize, int pageIndex, out int totalCount) //2013-04-09 add by Henry for pagination
        {
            return GetCashboxListDS(autIntNo, true, pageSize, pageIndex, out totalCount);
        }

        /// <summary>
        /// Gets the cashbox list DataSet.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="showAll">if set to <c>true</c> show all cash boxes not just unused ones.</param>
        /// <returns></returns>
        public DataSet GetCashboxListDS(int autIntNo, bool showAll, 
            int pageSize, int pageIndex, out int totalCount) //2013-04-09 add by Henry for pagination
        {
            SqlDataAdapter sqlDACashboxes = new SqlDataAdapter();
            DataSet dsCashboxes = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDACashboxes.SelectCommand = new SqlCommand();
            sqlDACashboxes.SelectCommand.Connection = new SqlConnection(connectionString);
            sqlDACashboxes.SelectCommand.CommandText = "CashboxList";
            sqlDACashboxes.SelectCommand.CommandType = CommandType.StoredProcedure;

            sqlDACashboxes.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            sqlDACashboxes.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;
            
            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            sqlDACashboxes.SelectCommand.Parameters.Add(paraTotalCount);

            // Parameters
            //sqlDACashboxes.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            //sqlDACashboxes.SelectCommand.Parameters.Add("@ShowAll", SqlDbType.Bit, 1).Value = showAll;

            // Execute the command and close the connection
            sqlDACashboxes.Fill(dsCashboxes);
            sqlDACashboxes.SelectCommand.Connection.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            // Return the dataset result
            return dsCashboxes;
        }

        public int UpdateCashbox(int cbIntNo, string cbName, double cbStartAmount, int cbRand500s, int cbRand200s, int cbRand100s, int cbRand50s,
            int cbRand20s, int cbRand10s, int cbRand5s, int cbRand2s, int cbRand1s, double cbReceived, double cbBanked, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CashboxUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            //SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            //parameterAutIntNo.Value = autIntNo;
            //myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCBName = new SqlParameter("@CBName", SqlDbType.VarChar, 30);
            parameterCBName.Value = cbName;
            myCommand.Parameters.Add(parameterCBName);

            SqlParameter parameterCBStartAmount = new SqlParameter("@CBStartAmount", SqlDbType.Real);
            parameterCBStartAmount.Value = cbStartAmount;
            myCommand.Parameters.Add(parameterCBStartAmount);

            SqlParameter parameterCBRand500s = new SqlParameter("@CBRand500s", SqlDbType.Int);
            parameterCBRand500s.Value = cbRand500s;
            myCommand.Parameters.Add(parameterCBRand500s);

            SqlParameter parameterCBRand200s = new SqlParameter("@CBRand200s", SqlDbType.Int);
            parameterCBRand200s.Value = cbRand200s;
            myCommand.Parameters.Add(parameterCBRand200s);

            SqlParameter parameterCBRand100s = new SqlParameter("@CBRand100s", SqlDbType.Int);
            parameterCBRand100s.Value = cbRand100s;
            myCommand.Parameters.Add(parameterCBRand100s);

            SqlParameter parameterCBRand50s = new SqlParameter("@CBRand50s", SqlDbType.Int);
            parameterCBRand50s.Value = cbRand50s;
            myCommand.Parameters.Add(parameterCBRand50s);

            SqlParameter parameterCBRand20s = new SqlParameter("@CBRand20s", SqlDbType.Int);
            parameterCBRand20s.Value = cbRand20s;
            myCommand.Parameters.Add(parameterCBRand20s);

            SqlParameter parameterCBRand10s = new SqlParameter("@CBRand10s", SqlDbType.Int);
            parameterCBRand10s.Value = cbRand10s;
            myCommand.Parameters.Add(parameterCBRand10s);

            SqlParameter parameterCBRand5s = new SqlParameter("@CBRand5s", SqlDbType.Int);
            parameterCBRand5s.Value = cbRand5s;
            myCommand.Parameters.Add(parameterCBRand5s);

            SqlParameter parameterCBRand2s = new SqlParameter("@CBRand2s", SqlDbType.Int);
            parameterCBRand2s.Value = cbRand2s;
            myCommand.Parameters.Add(parameterCBRand2s);

            SqlParameter parameterCBRand1s = new SqlParameter("@CBRand1s", SqlDbType.Int);
            parameterCBRand1s.Value = cbRand1s;
            myCommand.Parameters.Add(parameterCBRand1s);

            SqlParameter parameterCBReceived = new SqlParameter("@CBReceived", SqlDbType.Real);
            parameterCBReceived.Value = cbReceived;
            myCommand.Parameters.Add(parameterCBReceived);

            SqlParameter parameterCBBanked = new SqlParameter("@CBBanked", SqlDbType.Real);
            parameterCBBanked.Value = cbBanked;
            myCommand.Parameters.Add(parameterCBBanked);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterCBIntNo = new SqlParameter("@CBIntNo", SqlDbType.Int);
            parameterCBIntNo.Direction = ParameterDirection.InputOutput;
            parameterCBIntNo.Value = cbIntNo;
            myCommand.Parameters.Add(parameterCBIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                cbIntNo = (int)myCommand.Parameters["@CBIntNo"].Value;

                return cbIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int DeleteCashbox(int cbIntNo, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("CashboxDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCBIntNo = new SqlParameter("@CBIntNo", SqlDbType.Int, 4);
            parameterCBIntNo.Value = cbIntNo;
            parameterCBIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterCBIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                cbIntNo = (int)parameterCBIntNo.Value;
                return cbIntNo;
            }
            catch(Exception ex)
            {
                myConnection.Dispose();
                errMessage = ex.Message;
                return -2;
            }
        }

        /// <summary>
        /// Confirms the cash box float.
        /// </summary>
        /// <param name="userIntNo">The user int no.</param>
        /// <param name="cbIntNo">The cash box int no.</param>
        /// <param name="lastUser">The last user.</param>
        /// <param name="baIntNo">The bank account int no.</param>
        /// <param name="cashboxType">Type of the cashbox.</param>
        //public void ConfirmCashboxFloat(int userIntNo, int cbIntNo, string lastUser, int baIntNo, CashboxType cashboxType)
        public void ConfirmCashboxFloat(int userIntNo, int cbIntNo, string lastUser, int cbuIntNo, CashboxType cashboxType)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CashboxConfirm", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@UserIntNo", SqlDbType.Int, 4).Value = userIntNo;
            com.Parameters.Add("@CBIntNo", SqlDbType.Int, 4).Value = cbIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            //com.Parameters.Add("@BAIntNo", SqlDbType.Int, 4).Value = baIntNo;
            com.Parameters.Add("@CBUIntNo", SqlDbType.Int, 4).Value = cbuIntNo;
            com.Parameters.Add("@CashboxType", SqlDbType.Char, 1).Value = (char)((int)cashboxType);

            con.Open();
            com.ExecuteNonQuery();
            con.Close();
        }

        /// <summary>
        /// Records the cashbox recon data.
        /// </summary>
        /// <param name="reconData">The recon data.</param>
        public void RecordCashboxReconData(CashBoxRecon reconData)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CashboxRecordReconData", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@CBIntNo", SqlDbType.Int, 4).Value = reconData.CBIntNo;
            com.Parameters.Add("@500", SqlDbType.Int, 4).Value = reconData.R500;
            com.Parameters.Add("@200", SqlDbType.Int, 4).Value = reconData.R200;
            com.Parameters.Add("@100", SqlDbType.Int, 4).Value = reconData.R100;
            com.Parameters.Add("@50", SqlDbType.Int, 4).Value = reconData.R50;
            com.Parameters.Add("@20", SqlDbType.Int, 4).Value = reconData.R20;
            com.Parameters.Add("@10", SqlDbType.Int, 4).Value = reconData.R10;
            com.Parameters.Add("@5", SqlDbType.Int, 4).Value = reconData.R5;
            com.Parameters.Add("@2", SqlDbType.Int, 4).Value = reconData.R2;
            com.Parameters.Add("@1", SqlDbType.Int, 4).Value = reconData.R1;
            com.Parameters.Add("@ChequeCount", SqlDbType.Int, 4).Value = reconData.ChequeCount;
            com.Parameters.Add("@ChequeAmount", SqlDbType.Money, 8).Value = reconData.ChequeAmount;

            com.Parameters.Add("@CardCount", SqlDbType.Int, 4).Value = reconData.CardCount;
            com.Parameters.Add("@CardAmount", SqlDbType.Money, 8).Value = reconData.CardAmount;
            
            com.Parameters.Add("@DebitCardCount", SqlDbType.Int, 4).Value = reconData.DebitCardCount; 
            com.Parameters.Add("@DebitCardAmount", SqlDbType.Money, 8).Value = reconData.DebitCardAmount; 

            com.Parameters.Add("@POCount", SqlDbType.Int, 4).Value = reconData.POCount;
            com.Parameters.Add("@POAmount", SqlDbType.Money, 8).Value = reconData.POAmount;
            com.Parameters.Add("@LastUser", SqlDbType.NVarChar, 50).Value = reconData.LastUser; // 2013-07-23 add by Henry

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
                con.Dispose();
                com.Dispose();
            }
        }

        /// <summary>
        /// Gets the cash box recon data for today.
        /// </summary>
        /// <param name="cbIntNo">The cb int no.</param>
        /// <param name="showBalancingHints">if set to <c>true</c> balancing hints should be returned.</param>
        /// <returns>A <see cref="CashBoxRecon"/> object.</returns>
        public CashBoxRecon GetCashBoxRecon(int cbIntNo, bool showBalancingHints)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CashboxGetReconData", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@CBIntNo", SqlDbType.Int, 4).Value = cbIntNo;
            com.Parameters.Add("@ShowBalancingHints", SqlDbType.Bit, 1).Value = showBalancingHints;

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                CashBoxRecon recon = new CashBoxRecon();
                recon.CBIntNo = 0;
                if (reader.Read())
                {
                    recon.CBIntNo = (int)reader["CBIntNo"];
                    recon.CardAmount = (decimal)reader["ReconCardAmount"];
                    recon.CardCount = (int)reader["ReconCardCount"];

                    recon.DebitCardAmount = (decimal)reader["ReconDebitCardAmount"];
                    recon.DebitCardCount = (int)reader["ReconDebitCardCount"];

                    recon.ChequeAmount = (decimal)reader["ReconChequeAmount"];
                    recon.ChequeCount = (int)reader["ReconChequeCount"];

                    recon.POAmount = (decimal)reader["ReconPOAmount"];
                    recon.POCount = (int)reader["ReconPOCount"];

                    recon.R1 = (int)reader["Recon1s"];
                    recon.R10 = (int)reader["Recon10s"];

                    recon.R100 = (int)reader["Recon100s"];
                    recon.R2 = (int)reader["Recon2s"];

                    recon.R20 = (int)reader["Recon20s"];
                    recon.R200 = (int)reader["Recon200s"];

                    recon.R5 = (int)reader["Recon5s"];
                    recon.R50 = (int)reader["Recon50s"];

                    recon.R500 = (int)reader["Recon500s"];
                    recon.Float = decimal.Parse(reader["CBStartAmount"].ToString());
                }
                reader.Close();
                return recon;
            }
            finally
            {
                con.Close();
                con.Dispose();
                com.Dispose();
            }
        }

        /// <summary>
        /// Gets the database recon data.
        /// </summary>
        /// <param name="cbIntNo">The cashbox int no.</param>
        /// <returns>A DBReconData object</returns>
        public DBReconData GetDBReconData(int cbIntNo, int autIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CashboxGetDBReconData", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@CBIntNo", SqlDbType.Int, 4).Value = cbIntNo;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            con.Open();
            SqlDataReader reader = com.ExecuteReader();
            DBReconData recon = new DBReconData();
            recon.CBIntNo = cbIntNo;
            if (reader.Read())
            {
                recon.Cash = (decimal)reader["Cash"];
                recon.CardAmount = (decimal)reader["CardAmount"];
                recon.CardCount = (int)reader["CardCount"];

                recon.DebitCardAmount = (decimal)reader["DebitCardAmount"];
                recon.DebitCardCount = (int)reader["DebitCardCount"];

                recon.ChequeAmount = (decimal)reader["ChequeAmount"];
                recon.ChequeCount = (int)reader["ChequeCount"];

                recon.PostalOrderAmount = (decimal)reader["PostalOrderAmount"];
                recon.PostalOrderCount = (int)reader["PostalOrderCount"];

                recon.Float = decimal.Parse(reader["CBStartAmount"].ToString());
            }
            reader.Close();
            con.Close();

            return recon;
        }

        /// <summary>
        /// Gets the report recon data.
        /// </summary>
        /// <param name="cbIntNo">The cb int no.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetReportReconData(int cbIntNo, int autIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CashboxGetDBReconData", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@CBIntNo", SqlDbType.Int, 4).Value = cbIntNo;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        /// <summary>
        /// Gets the supervisor cash box list.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetSupervisorCashBoxList()
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CashBoxSuperviserList", con);
            com.CommandType = CommandType.StoredProcedure;

            //com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        /// <summary>
        /// Receives the cashbox back from a cashier, resetting the user's current cash box and the cashbox status.
        /// </summary>
        /// <param name="cbIntNo">The cashbox int no.</param>
        public int ReceiveCashbox(int cbIntNo, ref string errMessage)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("CashboxReceive", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@CBIntNo", SqlDbType.Int, 4).Value = cbIntNo;
            com.Parameters["@CBIntNo"].Direction = ParameterDirection.InputOutput;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Dispose();
                cbIntNo = (int)com.Parameters["@CBIntNo"].Value;
                return cbIntNo;
            }
            catch (Exception ex)
            {
                con.Dispose();
                errMessage = ex.Message;
                return -4;
            }
        }


        //public string GetCashBoxType(string strRctIntNo)
        //{
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("CashboxReceive", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@CBIntNo", SqlDbType.Int, 4).Value = cbIntNo;

        //    con.Open();
        //    com.ExecuteNonQuery();
        //    con.Close();
        //}

        // 2013-07-19 comment by Henry for useless
        //public string GetCashBoxType(string strRctIntNo, string strNoticeTicketNo)
        //{
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("CashBoxTypeValue", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@RCTIntNo", SqlDbType.Int).Value = int.Parse(strRctIntNo);
        //    com.Parameters.Add("@NoticeTicketNo", SqlDbType.VarChar, 50).Value = strNoticeTicketNo;
        //    com.Parameters.Add("@CashBoxType", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
        //    com.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;

        //    string mCashBoxType = "";
        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();
        //        con.Close();

        //        // (LF:13/11/2008) CHECK Output params.                
        //        int mError = (int)com.Parameters["@Error"].Value;

        //        if (mError == 0)
        //        {
        //            mCashBoxType = com.Parameters["@CashBoxType"].Value.ToString();
        //        }
        //    }
        //    catch(Exception)
        //    {}
        //    finally
        //    {
        //        con.Close();
        //        con.Dispose();
        //        com.Dispose();
        //    }

        //    return mCashBoxType;
        //}
    }
}

