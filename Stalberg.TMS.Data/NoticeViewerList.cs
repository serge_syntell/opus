using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a list of frames from a film for either Adjudication or verification, and stores the current position in the list
    /// </summary>
    [Serializable]
    public class NoticeViewerList : List<NoticeInfo>
    {
        // Fields
        private NoticeInfo current;

        /// <summary>
        /// Initializes a new instance of the <see cref="NoticeViewerList"/> class.
        /// </summary>
        public NoticeViewerList()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NoticeViewerList"/> class.
        /// </summary>
        /// <param name="reader">A <see cref="SqlDataReader"/> that contains the list of notice information for processing.</param>
        public NoticeViewerList(SqlDataReader reader)
        {
            int x = 1;

            while (reader.Read())
            {
                base.Add(new NoticeInfo((int)reader["NotIntNo"], x, (int)reader["AutIntNo"]));
                x++;
            }
            reader.Close();
        }

        /// <summary>
        /// Gets the next notice for the film from the list.
        /// (if it gets to the end of the list, it starts again from the beginning)
        /// </summary>
        /// <returns>
        /// The Notice Int No of the Next notice, or -1
        /// </returns>
        //public int GetNextNotice(bool cycle)
        public NoticeInfo GetNextNotice()
        {
            if (this.current == null)
                this.current = NoticeInfo.Empty;

            // Check that there's something in the list
            if (this.Count == 0)
            {
                this.current = NoticeInfo.Empty;
                return this.current;        //.NotIntNo;
            }

            // Check that there's a current
            if (this.current.Equals(NoticeInfo.Empty))
            {
                this.current = base[0];
                return this.current;        //.NotIntNo;
            }

            // Check if there's only one notice in the list
            if (base.Count == 1)
            {
                this.current = base[0];
                return this.current;        //.NotIntNo;
            }

            // Otherwise get the next one
            for (int x = 0; x < base.Count; x++)
            {
                // Is the current notice the same as the one we're on in the list
                if (this.current.Equals(base[x]))
                {
                    // Are we at the end of the list
                    if (x + 1 < base.Count)
                    {
                        // Assign the next one to Current
                        this.current = base[x + 1];
                        return this.current;        //.NotIntNo;
                    }
                    else
                    {
                        // Restart the loop from the beginning
                        x = -1;
                        this.current = base[0];
                        return this.current;        //.NotIntNo;
                    }
                }
            }

            // We've fallen off the end of the list
            this.current = NoticeInfo.Empty;
            return this.current;            //.NotIntNo;
        }

        /// <summary>
        /// Gets the previous notice from the list.
        /// (If it gets to the beginning of the list, it starts again from the end)
        /// </summary>
        /// <returns>The previous Notice Int No, or -1</returns>
        public NoticeInfo GetPreviousNotice()
        {
            // Check that there's something in the list
            if (this.Count == 0)
            {
                this.current = NoticeInfo.Empty;
                return this.current;        //.NotIntNo;
            }

            // Check that there's a current
            if (this.current.Equals(NoticeInfo.Empty))
            {
                this.current = base[base.Count - 1];
                return this.current;            //.NotIntNo;
            }

            // Otherwise, find the previous one
            for (int x = base.Count - 1; x > -1; x--)
            {
                if (this.current.Equals(base[x]))
                {
                    if (x == 0)
                        this.current = base[base.Count - 1];
                    else
                        this.current = base[x - 1];

                    return this.current;            //.NotIntNo;
                }
            }

            this.current = NoticeInfo.Empty;
            return this.current;            //.NotIntNo;
        }

        /// <summary>
        /// Gets the current Notice Int No.
        /// </summary>
        /// <value>The current.</value>
        public int Current
        {
            get
            {
                if (this.current == null)
                    this.GetNextNotice();
                return this.current.NotIntNo;       
            }
        }

        public int CurrentIndex
        {
            get
            {
                return this.current.Index;        //.NotIntNo;
            }
        }

        //this sets the current notice
        public void SetCurrent (int notIntNo)
        {
            // Otherwise get the next one
            for (int x = 0; x < base.Count; x++)
            {
                // Is the current notice the same as the one we're on in the list
                if (notIntNo == base[x].NotIntNo)
                {
                    this.current = base[x];
                    return;
                }
            }
        }

    }

    /// <summary>
    /// Represents notice information needed to view and manipulate notice information
    /// </summary>
    [Serializable]
    public class NoticeInfo
    {
        // Fields
        private int notIntNo;
        private int index;
        private int autIntNo;

        private static NoticeInfo empty = new NoticeInfo(-1, 0, -1);

        /// <summary>
        /// Gets the empty.
        /// </summary>
        /// <value>The empty.</value>
        public static NoticeInfo Empty
        {
            get { return NoticeInfo.empty; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NoticeInfo"/> class.
        /// </summary>
        /// <param name="NotIntNo">The notice int no.</param>
        public NoticeInfo(int notIntNo, int index, int autIntNo)
        {
            this.notIntNo = notIntNo;
            this.index = index;
            this.autIntNo = autIntNo;
        }

        /// <summary>
        /// Gets the notice int no.
        /// </summary>
        /// <value>The notice int no.</value>
        public int NotIntNo
        {
            get { return this.notIntNo; }
        }

        public int AutIntNo
        {
            get { return this.autIntNo; }
        }

        public int Index
        {
            get { return this.index; }
        }

        /// <summary>
        /// Serves as a hash function for a particular type. <see cref="M:System.Object.GetHashCode"></see> is suitable for use in hashing algorithms and data structures like a hash table.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}