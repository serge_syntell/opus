using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


/* SD : 23.07.2008
 * Page : S56Bookissuedb.cs
 * Functionality : Datastructure to maintain the S56 Book Issue information
 */

namespace Stalberg.TMS
{
    // SD: 23.07.2008
	//*******************************************************
	//
	// S56 Book Issue Class
	//
	// A simple data class that encapsulates details about a particular loc 
	//
	//*******************************************************

    public partial class S56BookUsageDetails
	{
        public Int32 S56BIntNo;
        public Int32 AutIntNo;
        public Int32 TOIntNo;
        public string S56BNo;
        public string S56BIStartNo;
        public string S56BIEndNo;
        public Int32 S56BNoOfForms;
        public string S56BLastNoIssued;
        public string S56BILastNoUsed;
        public DateTime S56BIIssueDate;
        public DateTime S56BILastUsedDate;
        public Int32 S56BIIntNo;
        public string TOSName;

	}


    //*******************************************************
	//
    // S56 BookIssueDB Class
	//
	//*******************************************************

	public partial class S56BookUsageDB
    {
		string mConstr = "";

        public S56BookUsageDB(string vConstr)
		{
			mConstr = vConstr;
		}

		//*******************************************************
        // SD :  23.07.2008 
		// Functionality : to fetch the  S56 Book details to database
        //                 the reader is used to bind books list 
        //*******************************************************


        public SqlDataReader PopulateBookList(int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("S56_GetBooksByAutId", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader 
            return result;
        }


        //*******************************************************
        // SD :  23.07.2008 
        // Functionality : to fetch the  Traffic Officers  details 
        //                 the reader is used to bind Traffic Officers list
        //*******************************************************

        // 2013-07-19 comment by Henry for useless
        //public SqlDataReader PopulateOfficers(int autIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("S56_GetOfficerbyAuthorityID", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the data reader 
        //    return result;
        //}




        // SD :25.07.2008


        public int UpdateS56BookUsage(int s56BIIntNo, DateTime s56BIIssueDate, int s56BIStartNo, int s56BIEndNo,
                                    int s56BILastNoUsed, DateTime s56BILastUsedDate, ref string errMessage, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("S56_UpdateBookIssue", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

           //  Add Parameters to SPROC
            SqlParameter parameterS56BIIntNo = new SqlParameter("@S56BIIntNo", SqlDbType.Int, 4);
            parameterS56BIIntNo.Value = s56BIIntNo;
            parameterS56BIIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterS56BIIntNo);

            if (s56BIIssueDate != DateTime.MinValue)
            {
                SqlParameter parameterS56BIIssueDate = new SqlParameter("@S56BIIssueDate", SqlDbType.DateTime);
                parameterS56BIIssueDate.Value = s56BIIssueDate;
                myCommand.Parameters.Add(parameterS56BIIssueDate);
            }

            SqlParameter parameterS56BStartNo = new SqlParameter("@S56BIStartNo", SqlDbType.Int, 2);
            parameterS56BStartNo.Value = s56BIStartNo;
            myCommand.Parameters.Add(parameterS56BStartNo);

            SqlParameter parameters56BendNo = new SqlParameter("@S56BIEndNo", SqlDbType.Int, 2);
            parameters56BendNo.Value = s56BIEndNo;
            myCommand.Parameters.Add(parameters56BendNo);

            SqlParameter parameterS56BILastNoUsed = new SqlParameter("@S56BILastNoUsed", SqlDbType.Int, 2);
            parameterS56BILastNoUsed.Value = s56BILastNoUsed;
            myCommand.Parameters.Add(parameterS56BILastNoUsed);

            if (s56BILastUsedDate != DateTime.MinValue)
            {
                SqlParameter parameters56BILastUsed = new SqlParameter("@S56BILastUsedDate", SqlDbType.DateTime);
                parameters56BILastUsed.Value = s56BILastUsedDate;
                myCommand.Parameters.Add(parameters56BILastUsed);
            }

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the BookID using Output Param from SPROC
                s56BIIntNo = Convert.ToInt32(parameterS56BIIntNo.Value);

                return s56BIIntNo;
          }
            catch (Exception e)
            {
                myConnection.Dispose();
                errMessage = e.Message;
                return 0;
            }
        }



        // SD : 21.07.2008 & 22.07.2008 
        // Functionality : to fetch the S56BookIssue information from database
        //                 based on the input parameter supplied 

        public S56BookUsageDetails GetS56BookUsageDetails(int s56BIIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("S56_GetBookDetailsToEditUsage", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
            SqlParameter parameterss56BIIntNo = new SqlParameter("@S56BIIntNo", SqlDbType.Int, 4);
            parameterss56BIIntNo.Value = s56BIIntNo;
            myCommand.Parameters.Add(parameterss56BIIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            S56BookUsageDetails myBookDetails = new S56BookUsageDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
                myBookDetails.S56BIntNo = Convert.ToInt32(result["S56BIntNo"]);
                myBookDetails.S56BNo = result["S56BNo"].ToString();
                myBookDetails.S56BIStartNo = result["S56BIStartNo"].ToString();
                myBookDetails.S56BIEndNo = result["S56BIEndNo"].ToString();
                myBookDetails.S56BNoOfForms = Convert.ToInt32(result["S56BNoOfForms"]);
                myBookDetails.S56BILastNoUsed = result["S56BILastNoUsed"].ToString();

                if (result["S56BIIssueDate"] != System.DBNull.Value)
                    myBookDetails.S56BIIssueDate = Convert.ToDateTime(result["S56BIIssueDate"]);
                if (result["S56BILastUsedDate"] != System.DBNull.Value)
                    myBookDetails.S56BILastUsedDate = Convert.ToDateTime(result["S56BILastUsedDate"]);
              //  myBookDetails.TOSName = result["TOSName"].ToString(); 
                

			}
			result.Close();
            return myBookDetails;
		}

        //delete
        public int DeleteS56Book(int s56BIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("S56BookDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterS56IntNo = new SqlParameter("@S56BIntNo", SqlDbType.Int, 4);
            parameterS56IntNo.Value = s56BIntNo;
            parameterS56IntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterS56IntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                s56BIntNo = (int)parameterS56IntNo.Value;

                return s56BIntNo;
            }
            catch
            {
                myConnection.Dispose();
                return 0;
            }
        }

        public int DeleteBookUsageDetails(int s56BIIntNo)
        {
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("S56_DelinkOfficerByBookIssueId", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameters56BIIntNo = new SqlParameter("@S56BIIntNo", SqlDbType.Int, 4);
            parameters56BIIntNo.Value = s56BIIntNo;
            parameters56BIIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameters56BIIntNo);

            try
            {

                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                return s56BIIntNo;
            }

            catch
            {
                myConnection.Dispose();
                return 0;
            }
        
        }

        // 2013-07-19 comment by Henry for useless
        //public int ReturnBookUsageDetails(int s56BIIntNo)
        //{
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("S56_ReturnOfficerByBookIssueId", myConnection);
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    SqlParameter parameters56BIIntNo = new SqlParameter("@S56BIIntNo", SqlDbType.Int, 4);
        //    parameters56BIIntNo.Value = s56BIIntNo;
        //    parameters56BIIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameters56BIIntNo);

        //    try
        //    {

        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();
        //        return s56BIIntNo;
        //    }

        //    catch
        //    {
        //        myConnection.Dispose();
        //        return 0;
        //    }

        //}





        /// <summary>
        ///SD: BookIssue Dataset to bind the data to datagrid
        // 2013-07-19 comment by Henry for useless
        //public DataSet GetS56BookUsageListDS (int autIntNo)
        //{
        //    SqlDataAdapter sqlDAS56BookIssue = new SqlDataAdapter();
        //    DataSet dsS56BookIssue = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlDAS56BookIssue.SelectCommand = new SqlCommand();
        //    sqlDAS56BookIssue.SelectCommand.Connection = new SqlConnection(mConstr);
        //    sqlDAS56BookIssue.SelectCommand.CommandText = "S56_GetBookIssueDetailsByAutId";

        //    // Mark the Command as a SPROC
        //    sqlDAS56BookIssue.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    sqlDAS56BookIssue.SelectCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command and close the connection
        //    sqlDAS56BookIssue.Fill(dsS56BookIssue);
        //    sqlDAS56BookIssue.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsS56BookIssue;
        //}
		
     

	

	}
}

