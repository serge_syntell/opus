using System;

namespace Stalberg.TMS
{
	/// <summary>
	/// Run general procedures and functions
	/// </summary>
	public partial class General
	{
		public string SetMainImage (object image)
		{
			if (image == null)
                return "graphics/mainimage.JPG";
			else
                return "graphics/" + image.ToString();
		}

		public string SetBackground (object backGround)
		{
			if (backGround == null)
                return "backgrounds/background.JPG";
			else
                return "backgrounds/" + backGround.ToString();
		}

		public string SetStyleSheet(object styleSheet)
		{
			if (styleSheet == null)
                return "stylesheets/Generic.css";
			else
                return "stylesheets/" + styleSheet.ToString();
		}
		public string SetTitle(object title)
		{
			if (title == null)
				return "";
			else
				return title.ToString();
		}

		public string SetDescription(object descr)
		{
			if (descr == null)
				return "";
			else
				return descr.ToString();
		}

		public string SetKeywords(object keywords)
		{
			if (keywords == null)
				return "";
			else
				return keywords.ToString();
		}
	}
}
