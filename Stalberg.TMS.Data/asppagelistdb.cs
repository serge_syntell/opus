using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // ASPPageListDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class ASPPageListDetails 
	{
        public string APLPageName;
		public string APLPageURL;
		public string APLPageDescr;
		public string APLSysAdminOnly;
		public string LastUser;
    }

    //*******************************************************
    //
    // ASPPageListDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query ASPPageLists within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class ASPPageListDB {

		string mConstr = "";

		public ASPPageListDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // ASPPageListDB.GetASPPageListDetails() Method <a name="GetASPPageListDetails"></a>
        //
        // The GetASPPageListDetails method returns a ASPPageListDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public ASPPageListDetails GetASPPageListDetails(int aplIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ASPPageListDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAPLIntNo = new SqlParameter("@APLIntNo", SqlDbType.Int, 4);
            parameterAPLIntNo.Value = aplIntNo;
            myCommand.Parameters.Add(parameterAPLIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			ASPPageListDetails myASPPageListDetails = new ASPPageListDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myASPPageListDetails.APLPageName = result["APLPageName"].ToString();
				myASPPageListDetails.APLPageURL = result["APLPageURL"].ToString();
				myASPPageListDetails.APLPageDescr = result["APLPageDescr"].ToString();
				myASPPageListDetails.APLSysAdminOnly = result["APLSysAdminOnly"].ToString();
				myASPPageListDetails.LastUser = result["LastUser"].ToString();
			}
			result.Close();
            return myASPPageListDetails;
        }

        //*******************************************************
        //
        // ASPPageListDB.AddASPPageList() Method <a name="AddASPPageList"></a>
        //
        // The AddASPPageList method inserts a new menu record
        // into the menus database.  A unique "ASPPageListId"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddASPPageList (string aplPageName, string aplPageURL,
			string aplPageDescr, string aplSysAdminOnly, string lastUser) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ASPPageListAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
			SqlParameter parameterAPLPageName = new SqlParameter("@APLPageName", SqlDbType.VarChar, 50);
			parameterAPLPageName.Value = aplPageName;
			myCommand.Parameters.Add(parameterAPLPageName);

			SqlParameter parameterAPLPageURL = new SqlParameter("@APLPageURL", SqlDbType.VarChar, 50);
			parameterAPLPageURL.Value = aplPageURL;
			myCommand.Parameters.Add(parameterAPLPageURL);
			
			SqlParameter parameterAPLPageDescr = new SqlParameter("@APLPageDescr", SqlDbType.VarChar, 100);
			parameterAPLPageDescr.Value = aplPageDescr;
			myCommand.Parameters.Add(parameterAPLPageDescr);

			SqlParameter parameterAPLSysAdminOnly = new SqlParameter("@APLSysAdminOnly", SqlDbType.Char, 1);
			parameterAPLSysAdminOnly.Value = aplSysAdminOnly;
			myCommand.Parameters.Add(parameterAPLSysAdminOnly);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterAPLIntNo = new SqlParameter("@APLIntNo", SqlDbType.Int, 4);
            parameterAPLIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterAPLIntNo);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int aplIntNo = Convert.ToInt32(parameterAPLIntNo.Value);

                return aplIntNo;
            }
            catch {
				 myConnection.Dispose();
                return 0;
            }
        }

		public SqlDataReader GetASPPageListList(int type)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("ASPPageListList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterType = new SqlParameter("@Type", SqlDbType.Int);
			parameterType.Value = type;
			myCommand.Parameters.Add(parameterType);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		public DataSet GetASPPageListListDS(int type)
		{
			SqlDataAdapter sqlDAASPPageLists = new SqlDataAdapter();
			DataSet dsASPPageLists = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAASPPageLists.SelectCommand = new SqlCommand();
			sqlDAASPPageLists.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAASPPageLists.SelectCommand.CommandText = "ASPPageListList";

			// Mark the Command as a SPROC
			sqlDAASPPageLists.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterType = new SqlParameter("@Type", SqlDbType.Int);
			parameterType.Value = type;
			sqlDAASPPageLists.SelectCommand.Parameters.Add(parameterType);

			// Execute the command and close the connection
			sqlDAASPPageLists.Fill(dsASPPageLists);
			sqlDAASPPageLists.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsASPPageLists;		
		}

		public int UpdateASPPageList (int aplIntNo, string aplPageName, string aplPageURL, 
			string aplPageDescr, string aplSysAdminOnly, string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("ASPPageListUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterAPLPageName = new SqlParameter("@APLPageName", SqlDbType.VarChar, 50);
			parameterAPLPageName.Value = aplPageName;
			myCommand.Parameters.Add(parameterAPLPageName);

			SqlParameter parameterAPLPageURL = new SqlParameter("@APLPageURL", SqlDbType.VarChar, 50);
			parameterAPLPageURL.Value = aplPageURL;
			myCommand.Parameters.Add(parameterAPLPageURL);
			
			SqlParameter parameterAPLPageDescr = new SqlParameter("@APLPageDescr", SqlDbType.VarChar, 100);
			parameterAPLPageDescr.Value = aplPageDescr;
			myCommand.Parameters.Add(parameterAPLPageDescr);

			SqlParameter parameterAPLSysAdminOnly = new SqlParameter("@APLSysAdminOnly", SqlDbType.Char, 1);
			parameterAPLSysAdminOnly.Value = aplSysAdminOnly;
			myCommand.Parameters.Add(parameterAPLSysAdminOnly);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterAPLIntNo = new SqlParameter("@APLIntNo", SqlDbType.Int);
			parameterAPLIntNo.Direction = ParameterDirection.InputOutput;
			parameterAPLIntNo.Value = aplIntNo;
			myCommand.Parameters.Add(parameterAPLIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int APLIntNo = (int)myCommand.Parameters["@APLIntNo"].Value;
				//int menuId = (int)parameterAPLIntNo.Value;

				return APLIntNo;
			}
			catch (Exception e)
			{
				string msg = e.Message;
				myConnection.Dispose();
				return 0;
			}
		}

		public String DeleteASPPageList (int aplIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("ASPPageListDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterAPLIntNo = new SqlParameter("@APLIntNo", SqlDbType.Int, 4);
			parameterAPLIntNo.Value = aplIntNo;
			myCommand.Parameters.Add(parameterAPLIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int APLIntNo = (int)parameterAPLIntNo.Value;

				return APLIntNo.ToString();
			}
			catch 
			{
				 myConnection.Dispose();
				return String.Empty;
			}
		}

		public int GetASPPageByURL(string aplPageURL)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("ASPPageByURL", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterAPLPageURL = new SqlParameter("@APLPageURL", SqlDbType.VarChar, 50);
			parameterAPLPageURL.Value = aplPageURL;
			myCommand.Parameters.Add(parameterAPLPageURL);

			SqlParameter parameterAPLIntNo = new SqlParameter("@APLIntNo", SqlDbType.Int);
			parameterAPLIntNo.Direction = ParameterDirection.Output;
			myCommand.Parameters.Add(parameterAPLIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int APLIntNo = (int)myCommand.Parameters["@APLIntNo"].Value;

				return APLIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		public int GetASPPageForUser(int aplIntNo, int userIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("ASPPageForUser", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterAPLIntNo = new SqlParameter("@APLIntNo", SqlDbType.Int);
			parameterAPLIntNo.Value = aplIntNo;
			myCommand.Parameters.Add(parameterAPLIntNo);

			SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int);
			parameterUserIntNo.Value = userIntNo;
			myCommand.Parameters.Add(parameterUserIntNo);

			SqlParameter parameterUsAPIntNo = new SqlParameter("@UsAPIntNo", SqlDbType.Int);
			parameterUsAPIntNo.Direction = ParameterDirection.Output;
			myCommand.Parameters.Add(parameterUsAPIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int usAPIntNo = (int)myCommand.Parameters["@UsAPIntNo"].Value;

				return usAPIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		public int UpdateASPPageForUser(int aplIntNo, int userIntNo, string allow, string lastUser)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("ASPPageForUserUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterAPLIntNo = new SqlParameter("@APLIntNo", SqlDbType.Int);
			parameterAPLIntNo.Value = aplIntNo;
			myCommand.Parameters.Add(parameterAPLIntNo);

			SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int);
			parameterUserIntNo.Value = userIntNo;
			myCommand.Parameters.Add(parameterUserIntNo);

			SqlParameter parameterAllow = new SqlParameter("@Allow", SqlDbType.Char);
			parameterAllow.Value = allow;
			myCommand.Parameters.Add(parameterAllow);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterUsAPIntNo = new SqlParameter("@UsAPIntNo", SqlDbType.Int);
			parameterUsAPIntNo.Direction = ParameterDirection.Output;
			myCommand.Parameters.Add(parameterUsAPIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int usAPIntNo = (int)myCommand.Parameters["@UsAPIntNo"].Value;

				return usAPIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return -1;
			}
		}
	}
}

