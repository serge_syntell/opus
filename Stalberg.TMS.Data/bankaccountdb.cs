using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{
    /// <summary>
    /// BankAccountDetails Class
    /// A simple data class that encapsulates details about a particular loc 
    /// </summary>
    public class BankAccountDetails
    {
        public Int32 BAIntNo;
        //public Int32 AutIntNo;
        public string BAAccountNo;
        public string BAAccountName;
        public string BAName;
        public string BABranchName;
        public string BABranchNo;
    }

    /// <summary>
    /// Represents a User who is a valid cashier
    /// </summary>
    [Serializable]
    public class Cashier
    {
        public int CBIntNo = -1;
        public int UserIntNo = -1;
        public int CashBoxUserIntNo = -1;
        public decimal Float = -1;
        public bool MustCountCashBox;
        public CashboxType CashBoxType = CashboxType.OverTheCounter;
        public string UserName = string.Empty;
        public int BAIntNo = -1;
    }

    /// <summary>
    /// BankAccountDB Class
    /// </summary>
    public class BankAccountDB
    {
        string connectionString = String.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="BankAccountDB"/> class.
        /// </summary>
        /// <param name="vConstr">The database connection string.</param>
        public BankAccountDB(string vConstr)
        {
            this.connectionString = vConstr;
        }

        /// <summary>
        /// Gets the bank account details.
        /// </summary>
        /// <param name="baIntNo">The bank account int no.</param>
        /// <returns></returns>
        public BankAccountDetails GetBankAccountDetails(int baIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("BankAccountDetail", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            com.Parameters.Add("@BAIntNo", SqlDbType.Int, 4).Value = baIntNo;

            con.Open();
            SqlDataReader result = com.ExecuteReader(CommandBehavior.CloseConnection);

            BankAccountDetails account = new BankAccountDetails();
            while (result.Read())
            {
                // Populate object using Output Params from SPROC
                account.BAIntNo = Convert.ToInt32(result["BAIntNo"]);
                //account.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
                account.BAAccountNo = result["BAAccountNo"].ToString();
                account.BAAccountName = result["BAAccountName"].ToString();
                account.BAName = result["BAName"].ToString();
                account.BABranchName = result["BABranchName"].ToString();
                account.BABranchNo = result["BABranchNo"].ToString();
            }
            result.Close();

            return account;
        }

        public int AddBankAccount( string baAccountNo, string baAccountName, string baName, string baBranchName, string baBranchNo, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("BankAccountAdd", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            //SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            //parameterAutIntNo.Value = autIntNo;
            //com.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterBAAccountNo = new SqlParameter("@BAAccountNo", SqlDbType.VarChar, 20);
            parameterBAAccountNo.Value = baAccountNo;
            com.Parameters.Add(parameterBAAccountNo);

            SqlParameter parameterBAAccountName = new SqlParameter("@BAAccountName", SqlDbType.VarChar, 50);
            parameterBAAccountName.Value = baAccountName;
            com.Parameters.Add(parameterBAAccountName);

            SqlParameter parameterBAName = new SqlParameter("@BAName", SqlDbType.VarChar, 50);
            parameterBAName.Value = baName;
            com.Parameters.Add(parameterBAName);

            SqlParameter parameterBABranchName = new SqlParameter("@BABranchName", SqlDbType.VarChar, 50);
            parameterBABranchName.Value = baBranchName;
            com.Parameters.Add(parameterBABranchName);

            SqlParameter parameterBABranchNo = new SqlParameter("@BABranchNo", SqlDbType.VarChar, 20);
            parameterBABranchNo.Value = baBranchNo;
            com.Parameters.Add(parameterBABranchNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            com.Parameters.Add(parameterLastUser);

            SqlParameter parameterBAIntNo = new SqlParameter("@BAIntNo", SqlDbType.Int, 4);
            parameterBAIntNo.Direction = ParameterDirection.Output;
            com.Parameters.Add(parameterBAIntNo);

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int baIntNo = Convert.ToInt32(parameterBAIntNo.Value);

                return baIntNo;
            }
            catch (Exception e)
            {
                con.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        //public int GetBankAccount(int autIntNo, int ctIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("BA_CTGetBAIntNo", con);

        //    // Mark the Command as a SPROC
        //    com.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    //SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    //parameterAutIntNo.Value = autIntNo;
        //    //com.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterCTIntNo = new SqlParameter("@CTIntNo", SqlDbType.Int, 4);
        //    parameterCTIntNo.Value = ctIntNo;
        //    com.Parameters.Add(parameterCTIntNo);

        //    SqlParameter parameterBAIntNo = new SqlParameter("@BAIntNo", SqlDbType.Int, 4);
        //    parameterBAIntNo.Direction = ParameterDirection.Output;
        //    com.Parameters.Add(parameterBAIntNo);

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();
        //        con.Dispose();

        //        int baIntNo = Convert.ToInt32(parameterBAIntNo.Value);

        //        return baIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        con.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        public SqlDataReader GetBankAccountList(int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("BankAccountList", con);

            // Mark the Command as a SPROC
            com.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            com.Parameters.Add(parameterAutIntNo);

            // Execute the command
            con.Open();
            SqlDataReader result = com.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        // 20050715 converted to BankAccount
        public DataSet GetBankAccountListDS()
        {
            SqlDataAdapter sqlDABankAccounts = new SqlDataAdapter();
            DataSet dsBankAccounts = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDABankAccounts.SelectCommand = new SqlCommand();
            sqlDABankAccounts.SelectCommand.Connection = new SqlConnection(this.connectionString);
            sqlDABankAccounts.SelectCommand.CommandText = "BankAccountList";

            // Mark the Command as a SPROC
            sqlDABankAccounts.SelectCommand.CommandType = CommandType.StoredProcedure;

            //SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            //parameterAutIntNo.Value = autIntNo;
            //sqlDABankAccounts.SelectCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command and close the connection
            sqlDABankAccounts.Fill(dsBankAccounts);
            sqlDABankAccounts.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsBankAccounts;
        }
        // 20050715 converted to BankAccount
        public int UpdateBankAccount(int baIntNo, string baAccountNo, string baAccountName, string baName, string baBranchName, string baBranchNo, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("BankAccountUpdate", con);

            // Mark the Command as a SPROC
            com.CommandType = CommandType.StoredProcedure;

            //// Add Parameters to SPROC
            //SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            //parameterAutIntNo.Value = autIntNo;
            //com.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterBAAccountNo = new SqlParameter("@BAAccountNo", SqlDbType.VarChar, 20);
            parameterBAAccountNo.Value = baAccountNo;
            com.Parameters.Add(parameterBAAccountNo);

            SqlParameter parameterBAAccountName = new SqlParameter("@BAAccountName", SqlDbType.VarChar, 50);
            parameterBAAccountName.Value = baAccountName;
            com.Parameters.Add(parameterBAAccountName);

            SqlParameter parameterBAName = new SqlParameter("@BAName", SqlDbType.VarChar, 50);
            parameterBAName.Value = baName;
            com.Parameters.Add(parameterBAName);

            SqlParameter parameterBABranchName = new SqlParameter("@BABranchName", SqlDbType.VarChar, 50);
            parameterBABranchName.Value = baBranchName;
            com.Parameters.Add(parameterBABranchName);

            SqlParameter parameterBABranchNo = new SqlParameter("@BABranchNo", SqlDbType.VarChar, 20);
            parameterBABranchNo.Value = baBranchNo;
            com.Parameters.Add(parameterBABranchNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            com.Parameters.Add(parameterLastUser);

            SqlParameter parameterBAIntNo = new SqlParameter("@BAIntNo", SqlDbType.Int);
            parameterBAIntNo.Direction = ParameterDirection.InputOutput;
            parameterBAIntNo.Value = baIntNo;
            com.Parameters.Add(parameterBAIntNo);

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Dispose();

                baIntNo = (int)com.Parameters["@BAIntNo"].Value;

                return baIntNo;
            }
            catch (Exception e)
            {
                con.Dispose();
                string msg = e.Message;
                return 0;
            }
        }


        public int DeleteBankAccount(int BAIntNo, ref string errMessage)
        {

            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("BankAccountDelete", con);

            // Mark the Command as a SPROC
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterBAIntNo = new SqlParameter("@BAIntNo", SqlDbType.Int, 4);
            parameterBAIntNo.Value = BAIntNo;
            parameterBAIntNo.Direction = ParameterDirection.InputOutput;
            com.Parameters.Add(parameterBAIntNo);

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                // Calculate the CustomerID using Output Param from SPROC
                BAIntNo = (int)parameterBAIntNo.Value;

                return BAIntNo;
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        //public int AddBA_CT(int baIntNo, int ctIntNo, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("BA_CTAdd", con);

        //    // Mark the Command as a SPROC
        //    com.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterBAIntNo = new SqlParameter("@BAIntNo", SqlDbType.Int, 4);
        //    parameterBAIntNo.Value = baIntNo;
        //    com.Parameters.Add(parameterBAIntNo);

        //    SqlParameter parameterCTIntNo = new SqlParameter("@CTIntNo", SqlDbType.Int, 4);
        //    parameterCTIntNo.Value = ctIntNo;
        //    com.Parameters.Add(parameterCTIntNo);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    com.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterBaCtIntNo = new SqlParameter("@BaCtIntNo", SqlDbType.Int, 4);
        //    parameterBaCtIntNo.Direction = ParameterDirection.Output;
        //    com.Parameters.Add(parameterBaCtIntNo);

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();
        //        con.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int baCtIntNo = Convert.ToInt32(parameterBaCtIntNo.Value);

        //        return baCtIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        con.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        /// <summary>
        /// Gets the authority bank accounts.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <returns>A Data Set</returns>
        //public DataSet GetAuthorityBankAccounts()
        //{
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand();
        //    com.Connection = con;
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.CommandText = "GetAuthorityCashierList";

        //    //com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

        //    SqlDataAdapter adapter = new SqlDataAdapter(com);
        //    DataSet ds = new DataSet();
        //    adapter.Fill(ds);

        //    return ds;
        //}


        /// <summary>
        /// Gets the user bank account.
        /// </summary>
        /// <param name="userIntNo">The user int no.</param>
        /// <returns>A <see cref="Cashier"/></returns>
        public Cashier GetUserBankAccount(int userIntNo)
        {
            Cashier cashier = new Cashier();

            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("GetUserBankAccount", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@UserIntNo", SqlDbType.Int, 4).Value = userIntNo;
            //com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                if (reader.Read())
                {
                    //dls 100715 - please can we do things properly and call a spade a spade - trying to reuse columns when we've renamed them is not very helpful
                    //cashier.CashBoxUserIntNo = (int)reader["BAIntNo"]; //N.B. BAIntNo value is [CashBox_User].CBUIntNo
                    cashier.BAIntNo = (int)reader["BAIntNo"];
                    cashier.CashBoxUserIntNo = (int)reader["CBUIntNo"];

                    if (reader["UserCountCashBox"] == DBNull.Value)
                        cashier.MustCountCashBox = true;
                    else if (reader["UserCountCashBox"].ToString().ToUpper()[0].Equals('Y'))
                        cashier.MustCountCashBox = true;
                    else
                        cashier.MustCountCashBox = false;

                    cashier.UserIntNo = (int)reader["UserIntNo"];
                    if (reader["CurrentCBIntNo"] != DBNull.Value)
                        cashier.CBIntNo = (int)reader["CurrentCBIntNo"];
                    if (reader["CBStartAmount"] != DBNull.Value)
                        cashier.Float = Convert.ToDecimal(reader["CBStartAmount"]);
                    if (reader["CashboxType"] != DBNull.Value)
                        cashier.CashBoxType = (CashboxType)((int)((string)reader["CashboxType"])[0]);
                    if (reader["UserLoginName"] != DBNull.Value)
                        cashier.UserName = (string)reader["UserLoginName"];
                }
                else
                    cashier = null;

                reader.Close();

            }
            finally
            {
                con.Close();
            }

            return cashier;
        }

        /// <summary>
        /// Sets an authority cashier bank account.
        /// </summary>
        /// <param name="userIntNo">The user int no.</param>
        /// <param name="baIntNo">The bank account int no.</param>

        ////dls 080507 - add Authority to call for setting bank account
        //public void SetAuthorityCashierBankAccount(int userIntNo, int baIntNo)
        //{
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("SetAuthorityCashierBankAccount", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@UserIntNo", SqlDbType.Int, 4).Value = userIntNo;
        //    com.Parameters.Add("@BAIntNo", SqlDbType.Int, 4).Value = baIntNo;
        //    //com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

        //    con.Open();
        //    com.ExecuteNonQuery();
        //    con.Close();
        //}

        //public void SetAuthorityCashierBankAccount(int userIntNo, int cashBoxIntNo)
        //{
        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlCommand com = new SqlCommand("SetCashier", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@UserIntNo", SqlDbType.Int, 4).Value = userIntNo;
        //    com.Parameters.Add("@CashBoxIntNo", SqlDbType.Int, 4).Value = cashBoxIntNo;
        //    //com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

        //    con.Open();
        //    com.ExecuteNonQuery();
        //    con.Close();
        //}

    }
}

