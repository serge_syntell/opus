using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // OffenceGroupDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class OffenceGroupDetails
    {
        public Int32 SZIntNo;
        public Int32 RdTIntNo;
        public Int32 VTGIntNo;
        public Int32 OcTIntNo;
        public string OGCode;
        public string OGDescr;
        public string OGStatutoryRef;
        public string OGStatutoryRefShort;
        public string OffGroupS35StatRef;
        public string OffGroupS35StatRefShot;

    }

    //*******************************************************
    //
    // OffenceGroupDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query OffenceGroups within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class OffenceGroupDB
    {

        string mConstr = "";

        public OffenceGroupDB(string vConstr)
        {
            mConstr = vConstr;
        }

        //*******************************************************
        //
        // OffenceGroupDB.GetOffenceGroupDetails() Method <a name="GetOffenceGroupDetails"></a>
        //
        // The GetOffenceGroupDetails method returns a OffenceGroupDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public OffenceGroupDetails GetOffenceGroupDetails(int OGIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenceGroupDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int, 4);
            parameterOGIntNo.Value = OGIntNo;
            myCommand.Parameters.Add(parameterOGIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            OffenceGroupDetails myOffenceGroupDetails = new OffenceGroupDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myOffenceGroupDetails.SZIntNo = Convert.ToInt32(result["SZIntNo"]);
                myOffenceGroupDetails.RdTIntNo = Convert.ToInt32(result["RdTIntNo"]);
                myOffenceGroupDetails.VTGIntNo = Convert.ToInt32(result["VTGIntNo"]);
                myOffenceGroupDetails.OcTIntNo = Convert.ToInt32(result["OcTIntNo"]);
                myOffenceGroupDetails.OGDescr = result["OGDescr"].ToString();
                myOffenceGroupDetails.OGCode = result["OGCode"].ToString();
                myOffenceGroupDetails.OGStatutoryRef = result["OGStatutoryRef"].ToString();
                myOffenceGroupDetails.OGStatutoryRefShort = result["OGStatutoryRefShort"].ToString();
                myOffenceGroupDetails.OffGroupS35StatRef = result["OffGroupS35StatRef"] is DBNull ? "" : result["OffGroupS35StatRef"].ToString();
                myOffenceGroupDetails.OffGroupS35StatRefShot = result["OffGroupS35ShortStatRef"] is DBNull ? "" : result["OffGroupS35ShortStatRef"].ToString();
            }
            result.Close();
            return myOffenceGroupDetails;
        }

        //*******************************************************
        //
        // OffenceGroupDB.AddOffenceGroup() Method <a name="AddOffenceGroup"></a>
        //
        // The AddOffenceGroup method inserts a new menu record
        // into the menus database.  A unique "OffenceGroupId"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddOffenceGroup(int szIntNo, int rdtIntNo, int vtgIntNo,
            int ocTIntNo, string OGCode, string OGDescr, string OGStatutoryRef, string lastUser,
            string ogStatutoryRefShort, string s35StatuoryRef, string s35StatuoryRefShort)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenceGroupAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterSZIntNo = new SqlParameter("@SZIntNo", SqlDbType.Int, 4);
            parameterSZIntNo.Value = szIntNo;
            myCommand.Parameters.Add(parameterSZIntNo);

            SqlParameter parameterRdTIntNo = new SqlParameter("@RdTIntNo", SqlDbType.Int, 4);
            parameterRdTIntNo.Value = rdtIntNo;
            myCommand.Parameters.Add(parameterRdTIntNo);

            SqlParameter parameterVTGIntNo = new SqlParameter("@VTGIntNo", SqlDbType.Int, 4);
            parameterVTGIntNo.Value = vtgIntNo;
            myCommand.Parameters.Add(parameterVTGIntNo);

            SqlParameter parameterOcTIntNo = new SqlParameter("@OcTIntNo", SqlDbType.Int, 4);
            parameterOcTIntNo.Value = ocTIntNo;
            myCommand.Parameters.Add(parameterOcTIntNo);

            SqlParameter parameterOGDescr = new SqlParameter("@OGDescr", SqlDbType.VarChar, 100);
            parameterOGDescr.Value = OGDescr;
            myCommand.Parameters.Add(parameterOGDescr);

            SqlParameter parameterOGCode = new SqlParameter("@OGCode", SqlDbType.VarChar, 5);
            parameterOGCode.Value = OGCode;
            myCommand.Parameters.Add(parameterOGCode);

            SqlParameter parameterOGStatutoryRef = new SqlParameter("@OGStatutoryRef", SqlDbType.VarChar);
            parameterOGStatutoryRef.Value = OGStatutoryRef;
            myCommand.Parameters.Add(parameterOGStatutoryRef);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterOGtatutoryRefShort = new SqlParameter("@OGStatutoryRefShort", SqlDbType.VarChar, 255);
            parameterOGtatutoryRefShort.Value = ogStatutoryRefShort;
            myCommand.Parameters.Add(parameterOGtatutoryRefShort);

            // Jake 2013-11-27 added two parameters
            SqlParameter parameterS35StatRef = new SqlParameter("@OffGroupS35StatRef", SqlDbType.NVarChar, 2000);
            parameterS35StatRef.Value = s35StatuoryRef;
            myCommand.Parameters.Add(parameterS35StatRef);

            SqlParameter parameterS35StatRefShort = new SqlParameter("@OffGroupS35ShortStatRef", SqlDbType.NVarChar, 255);
            parameterS35StatRefShort.Value = s35StatuoryRefShort;
            myCommand.Parameters.Add(parameterS35StatRefShort);

            SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int, 4);
            parameterOGIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterOGIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int OGIntNo = Convert.ToInt32(parameterOGIntNo.Value);

                return OGIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public SqlDataReader GetOffenceGroupList(string search)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenceGroupList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
            parameterSearch.Value = search;
            myCommand.Parameters.Add(parameterSearch);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }


        public DataSet GetOffenceGroupListDS(string search)
        {
            SqlDataAdapter sqlDAOffenceGroups = new SqlDataAdapter();
            DataSet dsOffenceGroups = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAOffenceGroups.SelectCommand = new SqlCommand();
            sqlDAOffenceGroups.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAOffenceGroups.SelectCommand.CommandText = "OffenceGroupList";

            // Mark the Command as a SPROC
            sqlDAOffenceGroups.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
            parameterSearch.Value = search;
            sqlDAOffenceGroups.SelectCommand.Parameters.Add(parameterSearch);

            // Execute the command and close the connection
            sqlDAOffenceGroups.Fill(dsOffenceGroups);
            sqlDAOffenceGroups.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsOffenceGroups;
        }


        public int UpdateOffenceGroup(int OGIntNo, int szIntNo, int rdtIntNo,
            int vtgIntNo, int ocTIntNo, string OGCode, string OGDescr,
            string OGStatutoryRef, string lastUser, string ogStatutoryRefShort,
            string s35StatuoryRef, string s35StatuoryRefShort)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenceGroupUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterSZIntNo = new SqlParameter("@SZIntNo", SqlDbType.Int, 4);
            parameterSZIntNo.Value = szIntNo;
            myCommand.Parameters.Add(parameterSZIntNo);

            SqlParameter parameterRdTIntNo = new SqlParameter("@RdTIntNo", SqlDbType.Int, 4);
            parameterRdTIntNo.Value = rdtIntNo;
            myCommand.Parameters.Add(parameterRdTIntNo);

            SqlParameter parameterVTGIntNo = new SqlParameter("@VTGIntNo", SqlDbType.Int, 4);
            parameterVTGIntNo.Value = vtgIntNo;
            myCommand.Parameters.Add(parameterVTGIntNo);

            SqlParameter parameterOcTIntNo = new SqlParameter("@OcTIntNo", SqlDbType.Int, 4);
            parameterOcTIntNo.Value = ocTIntNo;
            myCommand.Parameters.Add(parameterOcTIntNo);

            SqlParameter parameterOGDescr = new SqlParameter("@OGDescr", SqlDbType.VarChar, 100);
            parameterOGDescr.Value = OGDescr;
            myCommand.Parameters.Add(parameterOGDescr);

            SqlParameter parameterOGCode = new SqlParameter("@OGCode", SqlDbType.VarChar, 5);
            parameterOGCode.Value = OGCode;
            myCommand.Parameters.Add(parameterOGCode);

            SqlParameter parameterOGStatutoryRef = new SqlParameter("@OGStatutoryRef", SqlDbType.VarChar);
            parameterOGStatutoryRef.Value = OGStatutoryRef;
            myCommand.Parameters.Add(parameterOGStatutoryRef);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterOGtatutoryRefShort = new SqlParameter("@OGStatutoryRefShort", SqlDbType.VarChar, 255);
            parameterOGtatutoryRefShort.Value = ogStatutoryRefShort;
            myCommand.Parameters.Add(parameterOGtatutoryRefShort);

            // Jake 2013-11-27 added two parameters
            SqlParameter parameterS35StatRef = new SqlParameter("@OffGroupS35StatRef", SqlDbType.NVarChar, 2000);
            parameterS35StatRef.Value = s35StatuoryRef;
            myCommand.Parameters.Add(parameterS35StatRef);

            SqlParameter parameterS35StatRefShort = new SqlParameter("@OffGroupS35ShortStatRef", SqlDbType.NVarChar, 255);
            parameterS35StatRefShort.Value = s35StatuoryRefShort;
            myCommand.Parameters.Add(parameterS35StatRefShort);

            SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int);
            parameterOGIntNo.Direction = ParameterDirection.InputOutput;
            parameterOGIntNo.Value = OGIntNo;
            myCommand.Parameters.Add(parameterOGIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int ogIntNo = (int)myCommand.Parameters["@OGIntNo"].Value;
                //int menuId = (int)parameterOGIntNo.Value;

                return ogIntNo;
            }
            catch
            {
                myConnection.Dispose();
                return 0;
            }
        }


        public String DeleteOffenceGroup(int OGIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenceGroupDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int, 4);
            parameterOGIntNo.Value = OGIntNo;
            parameterOGIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterOGIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int ogIntNo = (int)parameterOGIntNo.Value;

                return ogIntNo.ToString();
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return String.Empty;
            }
        }
    }
}

