using System;
using System.Data;
using System.Data.SqlClient;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a WOA
    /// </summary>
    public class WOADetails
    {
        public int SumIntNo;
        public int WOAIntNo;
        public int ACIntNo;
        public string LastUser;
    }

    /// <summary>
    /// Represents the criteria of a query for Notice
    /// </summary>
    public class WOAQueryCriteria
    {
        public int AutIntNo;
        public string ColumnName;
        public string Value;
        public DateTime DateSince;
        public int NoticeIntNo;

        /// <summary>
        /// Initializes a new instance of the <see cref="WOAQueryCriteria"/> class.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="value">The value of the column.</param>
        /// <param name="dateSince">The date after which to search for notices.</param>
        public WOAQueryCriteria(int autIntNo, string columnName, string value, string dateSince)
        {
            this.AutIntNo = autIntNo;
            this.ColumnName = columnName;
            this.Value = value;
            DateTime dt;
            if (DateTime.TryParse(dateSince, out dt))
                this.DateSince = dt;
            else
                this.DateSince = new DateTime(2000, 1, 1);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WOAQueryCriteria"/> class.
        /// </summary>
        /// <param name="notIntNo">The not int no.</param>
        public WOAQueryCriteria(int notIntNo)
        {
            this.NoticeIntNo = notIntNo;
        }
    }

    /// <summary>
    /// The notice DB class contains all the database access logic for retrieving notices
    /// </summary>
    public class WOADB
    {
        private string mConstr = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:WOADB"/> class.
        /// </summary>
        /// <param name="vConstr">The database connection string.</param>
        public WOADB(string vConstr)
        {
            mConstr = vConstr;
        }

        /// <summary>
        /// Gets the notice details.
        /// </summary>
        /// <param name="NotIntNo">The not int no.</param>
        /// <returns>a Notice data structure</returns>
        public WOADetails GetWoaDetails(int NotIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NoticeDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = NotIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            WOADetails myWoaDetails = new WOADetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myWoaDetails.ACIntNo = Convert.ToInt32(result["ACIntNo"]);
                myWoaDetails.LastUser = result["LastUser"].ToString();
            }
            return myWoaDetails;
        }

        /// <summary>
        ///  Heidi 2013-08-27 added for Gets the WOA search DS.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="colName">Name of the col.</param>
        /// <param name="colValue">The col value.</param>
        /// <param name="dateSince">The date since.</param>
        /// <param name="minStatus">The min status.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet WOASearch_GetPaged(int autIntNo, string colName, string colValue, DateTime dateSince, int minStatus, int pageIndex, int pageSize, out int totalCount)
        {
            totalCount = 0;
            // Create SQL data access objects
            SqlDataAdapter da = new SqlDataAdapter();
            //2014-08-13 Heidi changed for fixing time out issue.(bontq1298)
            da.SelectCommand = new SqlCommand("WOASearch_GetPaged", new SqlConnection(this.mConstr)) { CommandTimeout = 0 };
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            da.SelectCommand.Parameters.Add("@ColumnName", SqlDbType.VarChar, 10).Value = colName;
            da.SelectCommand.Parameters.Add("@ColumnValue", SqlDbType.VarChar, 30).Value = colValue;
            da.SelectCommand.Parameters.Add("@DateSince", SqlDbType.DateTime, 8).Value = dateSince;
            da.SelectCommand.Parameters.Add("@MinStatus", SqlDbType.Int, 4).Value = minStatus;

            da.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int, 4).Value = pageIndex;
            da.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int, 4).Value = pageSize;
            SqlParameter spTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int, 4);
            spTotalCount.Direction = ParameterDirection.Output;
            da.SelectCommand.Parameters.Add(spTotalCount);

            // Execute the command and fill the data set
            dsOfenceSummary ds = new dsOfenceSummary();
            da.Fill(ds);
            da.SelectCommand.Connection.Dispose();

            totalCount = (int)spTotalCount.Value;

            // Return the data set result
            return ds;
        }

        // Jake 2011-06-09 added to get woa for separate print
        public DataSet GetWOAPrintFileDetailSelect(string woaPrintFileName, int pageSize, int pageIndex, out int totalCount)
        {
            SqlDataAdapter sqlPrintrun = new SqlDataAdapter();
            DataSet dsPrintrun = new DataSet();

            // Create Instance of Connection and Command Object
            sqlPrintrun.SelectCommand = new SqlCommand();
            sqlPrintrun.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlPrintrun.SelectCommand.CommandText = "WOAPrintFileDetailSelect";

            // Mark the Command as a SPROC
            sqlPrintrun.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterPrintFileName = new SqlParameter("@WOAPrintFileName", SqlDbType.VarChar, 100);
            parameterPrintFileName.Value = woaPrintFileName;
            sqlPrintrun.SelectCommand.Parameters.Add(parameterPrintFileName);

            //2013-03-27 add by Henry for pagination
            sqlPrintrun.SelectCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int)).Value = pageSize;
            sqlPrintrun.SelectCommand.Parameters.Add(new SqlParameter("@PageIndex", SqlDbType.Int)).Value = pageIndex;

            SqlParameter parameterTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            parameterTotalCount.Direction = ParameterDirection.Output;
            sqlPrintrun.SelectCommand.Parameters.Add(parameterTotalCount);

            // Execute the command and close the connection
            try
            {
                sqlPrintrun.Fill(dsPrintrun);
                totalCount = (int)(parameterTotalCount.Value ==DBNull.Value? 0 : parameterTotalCount.Value);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                sqlPrintrun.SelectCommand.Connection.Close();
                sqlPrintrun.SelectCommand.Connection.Dispose();
            }
            // Return the dataset result
            return dsPrintrun;
        }

        // Jake 2011-06-09 added to update woa print file name
        // 2013-07-26 add parameter lastUser by Henry
        public bool UpdateWOAPrintFileName(string woaPrintFileName, string woaIntNoStr, string lastUser)
        {
            bool success = false;
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand cmd = new SqlCommand("WOASeparatePrintFile", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterWoaPrintFilename = new SqlParameter("@WOAPrintFileName", SqlDbType.VarChar, 100);
            parameterWoaPrintFilename.Value = woaPrintFileName;
            cmd.Parameters.Add(parameterWoaPrintFilename);

            SqlParameter paramWoaIntNoStr = new SqlParameter("@WOAIntNo", SqlDbType.VarChar, 4000);
            paramWoaIntNoStr.Value = woaIntNoStr;
            cmd.Parameters.Add(paramWoaIntNoStr);

            SqlParameter paramReturnValue = new SqlParameter("@ReturnValue", SqlDbType.Int);
            cmd.Parameters.Add(paramReturnValue);
            paramReturnValue.Direction = ParameterDirection.Output;

            SqlParameter paramLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            paramLastUser.Value = lastUser;
            cmd.Parameters.Add(paramLastUser);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                success = Convert.ToInt32(paramReturnValue.Value) > 0;
                con.Dispose();
            }
            catch (Exception e)
            {
                
                con.Dispose();
                //string msg = e.Message;
                //return 0;
                throw e;
            }

            return success;
        }

        public DataSet GetWOAPrintrunDS(int autIntNo, string showAll, int days, int pageSize, int pageIndex, out int totalCount,string woaType="S")
        {
            SqlDataAdapter sqlPrintrun = new SqlDataAdapter();
            DataSet dsPrintrun = new DataSet();

            
            // Create Instance of Connection and Command Object
            sqlPrintrun.SelectCommand = new SqlCommand();
            sqlPrintrun.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlPrintrun.SelectCommand.CommandText = "WOAPrintrun";

            // Mark the Command as a SPROC
            sqlPrintrun.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlPrintrun.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterShowAll = new SqlParameter("@ShowAll", SqlDbType.Char, 1);
            parameterShowAll.Value = showAll;
            sqlPrintrun.SelectCommand.Parameters.Add(parameterShowAll);

            SqlParameter parameterWOAType = new SqlParameter("@WOAType", SqlDbType.Char, 1);
            parameterWOAType.Value = woaType;
            sqlPrintrun.SelectCommand.Parameters.Add(parameterWOAType);

            //Insert the 14 days rule
            SqlParameter parameterNoOfDaysBeforeWOA = new SqlParameter("@NoOfDaysBeforeWOA", SqlDbType.Int, 4);
            parameterNoOfDaysBeforeWOA.Value = days;
            sqlPrintrun.SelectCommand.Parameters.Add(parameterNoOfDaysBeforeWOA);

            SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int);
            parameterPageSize.Value = pageSize;
            sqlPrintrun.SelectCommand.Parameters.Add(parameterPageSize);

            SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int);
            parameterPageIndex.Value = pageIndex;
            sqlPrintrun.SelectCommand.Parameters.Add(parameterPageIndex);

            SqlParameter parameterTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            parameterTotalCount.Direction = ParameterDirection.Output;
            sqlPrintrun.SelectCommand.Parameters.Add(parameterTotalCount);

            // Execute the command and close the connection
            sqlPrintrun.Fill(dsPrintrun);
            sqlPrintrun.SelectCommand.Connection.Dispose();

            totalCount = (int)(parameterTotalCount.Value == DBNull.Value ? 0 : parameterTotalCount.Value);

            // Return the dataset result
            return dsPrintrun;
        }

        //2013-04-10 remove by Henry no use
        //not implemented
        //public DataSet GetWOAErrors(int autIntNo)
        //{
        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("SummonsErrorStats", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

        //    SqlDataAdapter da = new SqlDataAdapter(com);
        //    DataSet ds = new DataSet();
        //    da.Fill(ds);

        //    return ds;
        //}
        // 2013-07-26 add parameter lastUser by Henry
        // Jake 2013-10-31 change the print file name from 50 to 100
        public decimal UpdateWOAPrintStatus(string printFile,string lastUser, string AuthRule = "B")
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("UpdateWOAPrintStatus", con) { CommandTimeout = 0 };    // 2013-09-26, Oscar added timeout
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@WOAPrintFileName", SqlDbType.VarChar, 100).Value = printFile;
            com.Parameters.Add(new SqlParameter("@AuthRule", SqlDbType.VarChar, 3) { Value = AuthRule }); //2013-06-17 added by Heidi for 4973
            com.Parameters.Add("@WOAIntNo", SqlDbType.Decimal, 4).Direction = ParameterDirection.InputOutput;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                return (decimal)com.Parameters["@WOAIntNo"].Value;
            }
            catch (Exception)
            {
                com.Dispose();
                throw;  // 2013-09-26, Oscar added throw
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        // Jake 2013-10-31 change the print file name from 50 to 100
        //Authorize the selected WOA
        public decimal UpdateWOAAuthorizeStatus(string printFile, string lastUser)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("WOAAuthorizeStatusUpdate", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@WOAPrintFileName", SqlDbType.VarChar, 100).Value = printFile;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@WOAIntNo", SqlDbType.Decimal, 4).Direction = ParameterDirection.InputOutput;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                return (decimal)com.Parameters["@WOAIntNo"].Value;
            }
            catch (Exception)
            {
                com.Dispose();
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        //public int AddWOA(int sumIntNo, int autIntNo, string woaNumber, string woaPrintFileName,
        //                  decimal woaFineAmount, decimal woaAddAmount, string lastUser, string issueWOANotice,
        //                  ref string errMessage)
        public int AddWOA(int sumIntNo, int autIntNo, string woaNumberPrefix, string woaNumberSuffix, string woaPrintFileName,
                  decimal woaFineAmount, decimal woaAddAmount, string lastUser, string issueWOANotice,
                  ref string errMessage, string DisplayRepresentative)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("WOAAdd", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            //com.Parameters.Add("@WOANumber", SqlDbType.VarChar, 50).Value = woaNumber;
            com.Parameters.Add("@WOANumberPrefix", SqlDbType.VarChar, 10).Value = woaNumberPrefix;
            com.Parameters.Add("@WOANumberSuffix", SqlDbType.VarChar, 10).Value = woaNumberSuffix;
            // Jake 2013-10-31 change the print file name from 50 to 100
            com.Parameters.Add("@WOAPrintFileName", SqlDbType.VarChar, 100).Value = woaPrintFileName;
            com.Parameters.Add("@WOAFineAmount", SqlDbType.Money).Value = woaFineAmount;
            com.Parameters.Add("@WOAAddAmount", SqlDbType.Money).Value = woaAddAmount;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@IssueWOANotice", SqlDbType.Char, 1).Value = issueWOANotice;
            com.Parameters.Add("@WOAIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;
            com.Parameters.Add("@DisplayRepresentative", SqlDbType.Char, 1).Value = DisplayRepresentative;            

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                return (int)com.Parameters["@WOAIntNo"].Value;
            }
            catch (Exception e)
            {
                com.Dispose();
                errMessage = e.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        //dls 081103 - moved to NoticeOfWOADB.cs
        ////mrs 20081019 added to return notice of woa print files
        //public DataSet GetNoticeOfWOAPrintrunDS(int autIntNo)
        //{
        //    SqlDataAdapter sqlPrintrun = new SqlDataAdapter();
        //    DataSet dsPrintrun = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlPrintrun.SelectCommand = new SqlCommand();
        //    sqlPrintrun.SelectCommand.Connection = new SqlConnection(mConstr);
        //    sqlPrintrun.SelectCommand.CommandText = "GetNoticeOfWOAPrintFile";

        //    // Mark the Command as a SPROC
        //    sqlPrintrun.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    sqlPrintrun.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    //sqlPrintrun.SelectCommand.Parameters.Add("@Type", SqlDbType.VarChar, 6).Value = cType;
        //    //sqlPrintrun.SelectCommand.Parameters.Add("@StatusLoad", SqlDbType.Int, 4).Value = status;
        //    //sqlPrintrun.SelectCommand.Parameters.Add("@ShowAll", SqlDbType.Char, 1).Value = showAll;
        //    //sqlPrintrun.SelectCommand.Parameters.Add("@ShowControlRegister", SqlDbType.Char, 1).Value = showControlRegister;

        //    // Execute the command and close the connection
        //    sqlPrintrun.Fill(dsPrintrun);
        //    sqlPrintrun.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsPrintrun;
        //}




        //// SD: Update expired WOA with ChargeStatus 970

        //public void UpdateWOAWith1825()
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("UpdateCharge1825", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;
        //    myCommand.CommandTimeout = 0;


        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        throw new Exception(e.Message);

        //    }
        //}

        public SqlDataReader GetWOAExpiredListByAuth(int autIntNo, int noDaysForWOAExpiry)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("WOAExpiredListByAuth", con);
            com.CommandType = CommandType.StoredProcedure;
            com.CommandTimeout = 0;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@NoDaysForWOAExpiry", SqlDbType.Int, 4).Value = noDaysForWOAExpiry;

            try
            {
                con.Open();
                return com.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return null;
            }
            finally
            {
                con.Open();
            }
        }
        // 2013-07-26 comment out by Henry for useless
        //public int UpdateExpiredWOA(ref string errMessage, int noDaysForWOAExpiry, int woaIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("WOAExpired", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;
        //    myCommand.CommandTimeout = 0;

        //    myCommand.Parameters.Add("@WOAIntNo", SqlDbType.Int, 4).Value = woaIntNo;
        //    myCommand.Parameters.Add("@NoDaysForWOAExpiry", SqlDbType.Int, 4).Value = noDaysForWOAExpiry;
        //    myCommand.Parameters.Add("@NoOfWOA", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        int noOfExpiredWOA = int.Parse(myCommand.Parameters["@NoOfWOA"].Value.ToString());
        //        return noOfExpiredWOA;
        //    }
        //    catch (Exception e)
        //    {
        //        errMessage = e.Message;
        //        return -3;
        //    }
        //    finally
        //    {
        //        myConnection.Dispose();
        //    }
        //}

        public DataSet GetGetWOAList(int autIntNo, int noOfDays, string processor, int crtIntNo, int crtRIntNo, 
            int pageSize, int pageIndex, out int totalCount, // 2013-04-11 add by Henry for pagination
            int cdIntNo = 0)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("WOAListForAuthority", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@Processor", SqlDbType.VarChar, 10).Value = processor;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@NoOfDays", SqlDbType.Int, 4).Value = noOfDays;
            //jerry 2011-11-16 add CrtIntNo parameter
            com.Parameters.Add("@CrtIntNo", SqlDbType.Int, 4).Value = crtIntNo;
            com.Parameters.Add("@CrtRIntNo", SqlDbType.Int, 4).Value = crtRIntNo;

            com.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            com.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            com.Parameters.Add(paraTotalCount);

            //jerry 2012-03-16 add CDIntNo parameter
            if (cdIntNo > 0)
            {
                com.Parameters.Add("@CDIntNo", SqlDbType.Int, 4).Value = cdIntNo;
            }

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(com);
                DataSet ds = new DataSet();
                da.Fill(ds);
                da.SelectCommand.Connection.Dispose();
                totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);
                return ds;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Assert(false, e.Message);
                totalCount = 0;
                return null;
            }
        }

        /// <summary>
        /// jerry 2011-11-21 add woa according to the existed woaNumber
        /// </summary>
        /// <param name="sumIntNo"></param>
        /// <param name="autIntNo"></param>
        /// <param name="woaNumber"></param>
        /// <param name="NextWOANumber"></param>
        /// <param name="woaPrintFileName"></param>
        /// <param name="woaFineAmount"></param>
        /// <param name="woaAddAmount"></param>
        /// <param name="lastUser"></param>
        /// <param name="issueWOANotice"></param>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        public int AddWOAByWOANumber(int sumIntNo, int autIntNo, string woaNumber, int nextWOANumber, string woaPrintFileName, decimal woaFineAmount, decimal woaAddAmount, string lastUser, string issueWOANotice, ref string errMessage,string DisplayRepresentative)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("WOAAddByWOANumber", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@WOANumber", SqlDbType.VarChar, 50).Value = woaNumber;
            com.Parameters.Add("@NextWOANumber", SqlDbType.Int).Value = nextWOANumber;
            // Jake 2013-10-31 change the print file name length from 50 to 100
            com.Parameters.Add("@WOAPrintFileName", SqlDbType.VarChar, 100).Value = woaPrintFileName;
            com.Parameters.Add("@WOAFineAmount", SqlDbType.Money).Value = woaFineAmount;
            com.Parameters.Add("@WOAAddAmount", SqlDbType.Money).Value = woaAddAmount;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@IssueWOANotice", SqlDbType.Char, 1).Value = issueWOANotice;
            com.Parameters.Add("@WOAIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;
            com.Parameters.Add("@DisplayRepresentative", SqlDbType.Char, 1).Value = DisplayRepresentative;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                return (int)com.Parameters["@WOAIntNo"].Value;
            }
            catch (Exception e)
            {
                com.Dispose();
                errMessage = e.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        public SqlDataReader GetWoaIntNoByWoaPrintFileName(string woaPrintFileName)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("GetWoaIntNoByWoaPrintFileName", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@WOAPrintFileName", SqlDbType.VarChar).Value = woaPrintFileName;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }
    }
}
