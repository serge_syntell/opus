using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // VehicleTypeGroupDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class VehicleTypeGroupDetails 
	{
        public string VTGCode;
		public string VTGDescr;
        public Int32 OcTIntNo;
        public int VTGMaxSpeed;
    }

    //*******************************************************
    //
    // VehicleTypeGroupDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query VehicleTypeGroups within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class VehicleTypeGroupDB {

		string mConstr = "";

		public VehicleTypeGroupDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // VehicleTypeGroupDB.GetVehicleTypeGroupDetails() Method <a name="GetVehicleTypeGroupDetails"></a>
        //
        // The GetVehicleTypeGroupDetails method returns a VehicleTypeGroupDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public VehicleTypeGroupDetails GetVehicleTypeGroupDetails(int vtgIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("VehicleTypeGroupDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterVTGIntNo = new SqlParameter("@VTGIntNo", SqlDbType.Int, 4);
            parameterVTGIntNo.Value = vtgIntNo;
            myCommand.Parameters.Add(parameterVTGIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			VehicleTypeGroupDetails myVehicleTypeGroupDetails = new VehicleTypeGroupDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myVehicleTypeGroupDetails.VTGDescr = result["VTGDescr"].ToString();
				myVehicleTypeGroupDetails.VTGCode = result["VTGCode"].ToString();
                if (!(result["OcTIntNo"] is DBNull))
                myVehicleTypeGroupDetails.OcTIntNo = Convert.ToInt32(result["OcTIntNo"]);
                myVehicleTypeGroupDetails.VTGMaxSpeed = Convert.ToInt32(result["VTGMaxSpeed"]);

			}

			result.Close();
            return myVehicleTypeGroupDetails;
        }

        //*******************************************************
        //
        // VehicleTypeGroupDB.AddVehicleTypeGroup() Method <a name="AddVehicleTypeGroup"></a>
        //
        // The AddVehicleTypeGroup method inserts a new menu record
        // into the menus database.  A unique "VehicleTypeGroupId"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddVehicleTypeGroup(string vtgCode, string vtgDescr, string lastUser, int ocTIntNo, int vtgMaxSpeed) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("VehicleTypeGroupAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterOcTIntNo = new SqlParameter("@OcTIntNo", SqlDbType.Int, 4);
            parameterOcTIntNo.Value = ocTIntNo;
            myCommand.Parameters.Add(parameterOcTIntNo);

            SqlParameter parameterVTGMaxSpeed = new SqlParameter("@VTGMaxSpeed", SqlDbType.Int, 2);
            parameterVTGMaxSpeed.Value = vtgMaxSpeed;
            myCommand.Parameters.Add(parameterVTGMaxSpeed);

			SqlParameter parameterVTGDescr = new SqlParameter("@VTGDescr", SqlDbType.VarChar, 30);
			parameterVTGDescr.Value = vtgDescr;
			myCommand.Parameters.Add(parameterVTGDescr);

			SqlParameter parameterVTGCode = new SqlParameter("@VTGCode", SqlDbType.VarChar, 3);
			parameterVTGCode.Value = vtgCode;
			myCommand.Parameters.Add(parameterVTGCode);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterVTGIntNo = new SqlParameter("@VTGIntNo", SqlDbType.Int, 4);
            parameterVTGIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterVTGIntNo);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int vtgIntNo = Convert.ToInt32(parameterVTGIntNo.Value);

                return vtgIntNo;
            }
            catch {
				myConnection.Dispose();
                return 0;
            }
        }

		public SqlDataReader GetVehicleTypeGroupList(string search)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("VehicleTypeGroupList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
			parameterSearch.Value = search;
			myCommand.Parameters.Add(parameterSearch);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		
		public DataSet GetVehicleTypeGroupListDS(string search, int pageSize, int pageIndex, out int totalCount)
			//(int autIntNo)
		{
			SqlDataAdapter sqlDAVehicleTypeGroups = new SqlDataAdapter();
			DataSet dsVehicleTypeGroups = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAVehicleTypeGroups.SelectCommand = new SqlCommand();
			sqlDAVehicleTypeGroups.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAVehicleTypeGroups.SelectCommand.CommandText = "VehicleTypeGroupList";

			// Mark the Command as a SPROC
			sqlDAVehicleTypeGroups.SelectCommand.CommandType = CommandType.StoredProcedure;

			SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
			parameterSearch.Value = search;
			sqlDAVehicleTypeGroups.SelectCommand.Parameters.Add(parameterSearch);

            sqlDAVehicleTypeGroups.SelectCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int)).Value = pageSize;
            sqlDAVehicleTypeGroups.SelectCommand.Parameters.Add(new SqlParameter("@PageIndex", SqlDbType.Int)).Value = pageIndex;

            SqlParameter parameterTotal = new SqlParameter("@TotalCount", SqlDbType.Int);
            parameterTotal.Direction = ParameterDirection.Output;
            sqlDAVehicleTypeGroups.SelectCommand.Parameters.Add(parameterTotal);

			// Execute the command and close the connection
			sqlDAVehicleTypeGroups.Fill(dsVehicleTypeGroups);
			sqlDAVehicleTypeGroups.SelectCommand.Connection.Dispose();

            totalCount = (int)(parameterTotal.Value == DBNull.Value ? 0 : parameterTotal.Value);

			// Return the dataset result
			return dsVehicleTypeGroups;		
		}


        public int UpdateVehicleTypeGroup(int vtgIntNo, string vtgCode, string vtgDescr, string lastUser, int ocTIntNo, int vtgMaxSpeed) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("VehicleTypeGroupUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
            SqlParameter parameterOcTIntNo = new SqlParameter("@OcTIntNo", SqlDbType.Int, 4);
            parameterOcTIntNo.Value = ocTIntNo;
            myCommand.Parameters.Add(parameterOcTIntNo);

            SqlParameter parameterVTGMaxSpeed = new SqlParameter("@VTGMaxSpeed", SqlDbType.Int, 2);
            parameterVTGMaxSpeed.Value = vtgMaxSpeed;
            myCommand.Parameters.Add(parameterVTGMaxSpeed);

			SqlParameter parameterVTGDescr = new SqlParameter("@VTGDescr", SqlDbType.VarChar, 30);
			parameterVTGDescr.Value = vtgDescr;
			myCommand.Parameters.Add(parameterVTGDescr);

			SqlParameter parameterVTGCode = new SqlParameter("@VTGCode", SqlDbType.VarChar, 3);
			parameterVTGCode.Value = vtgCode;
			myCommand.Parameters.Add(parameterVTGCode);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterVTGIntNo = new SqlParameter("@VTGIntNo", SqlDbType.Int);
			parameterVTGIntNo.Direction = ParameterDirection.InputOutput;
			parameterVTGIntNo.Value = vtgIntNo;
			myCommand.Parameters.Add(parameterVTGIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int VTGIntNo = (int)myCommand.Parameters["@VTGIntNo"].Value;
				//int menuId = (int)parameterVTGIntNo.Value;

				return VTGIntNo;
			}
			catch 
			{
				myConnection.Dispose();
				return 0;
			}
		}

		
		public String DeleteVehicleTypeGroup (int vtgIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("VehicleTypeGroupDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterVTGIntNo = new SqlParameter("@VTGIntNo", SqlDbType.Int, 4);
			parameterVTGIntNo.Value = vtgIntNo;
			parameterVTGIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterVTGIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int VTGIntNo = (int)parameterVTGIntNo.Value;

				return VTGIntNo.ToString();
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return String.Empty;
			}
		}
	}
}

