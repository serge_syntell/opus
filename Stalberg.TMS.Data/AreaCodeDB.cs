using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
	/// <summary>
	/// COntains all the database code for interacting with Area Codes
	/// </summary>
    public class AreaDetails
    {
        public string AreaCode;
        public string AreaDescr;
    }

    public class AreaCodeDB
	{
		// Fields
		private string connectionString;

		/// <summary>
		/// Initializes a new instance of the <see cref="AreaCodeDB"/> class.
		/// </summary>
		/// <param name="connectionString">The connection string.</param>
		public AreaCodeDB(string connectionString)
		{
			this.connectionString = connectionString;
		}
        /// <summary>
        /// The GetAuthorityDetails method returns a AuthorityDetails
        /// struct that contains information about a specific
        /// customer (name, password, etc).
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        public AreaDetails GetAreaDetails(string areaCode)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("AreaDetail", con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAreaCode = new SqlParameter("@AreaCode", SqlDbType.VarChar, 8);
            parameterAreaCode.Value = areaCode;
            com.Parameters.Add(parameterAreaCode);

            con.Open();
            SqlDataReader result = com.ExecuteReader(CommandBehavior.CloseConnection);

            // Create Struct
            AreaDetails myAreaDetails = new AreaDetails();

            if (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myAreaDetails.AreaCode = Convert.ToString(result["AreaCode"]);
                myAreaDetails.AreaDescr = Convert.ToString(result["AreaDescr"]);
            }

            result.Close();
            return myAreaDetails;
        }

        public DataSet GetAllAreaCodesDS()
        {
            return GetAllAreaCodesDS(string.Empty);
        }

		/// <summary>
		/// Gets all area codes.
		/// </summary>
		/// <returns>A SQL Data Reader</returns>
		public DataSet GetAllAreaCodesDS(string search)
		{
			SqlConnection con = new SqlConnection(this.connectionString);
			SqlCommand com = new SqlCommand("GetAllAreaCodes", con);
			com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@Search", SqlDbType.VarChar, 20).Value = search;

			SqlDataAdapter da = new SqlDataAdapter(com);

			try
			{
				DataSet ds = new DataSet();
				da.Fill(ds);
				return ds;
			}
			finally
			{
				da.Dispose();
			}
		}

		/// <summary>
		/// Gets a filtered list of area codes.
		/// </summary>
		/// <param name="areaCode">The area code.</param>
		/// <param name="areaDescription">The area description.</param>
		/// <returns>A Data set</returns>
		public DataSet GetFilteredAreaCodes(string areaCode, string areaDescription)
		{
			SqlConnection con = new SqlConnection(this.connectionString);
			SqlCommand com = new SqlCommand("GetFilteredAreaCodes", con);
			com.CommandType = CommandType.StoredProcedure;
			SqlDataAdapter da = new SqlDataAdapter(com);
			if (areaCode.Length > 0)
				com.Parameters.Add("@AreaCode", SqlDbType.VarChar, 8).Value = areaCode;
			if (areaDescription.Length > 0)
				com.Parameters.Add("@AreaDescr", SqlDbType.VarChar, 100).Value = areaDescription;
			try
			{
				DataSet ds = new DataSet();
				da.Fill(ds);
				return ds;
			}
			finally
			{
				da.Dispose();
			}
		}

		/// <summary>
		/// Saves the area code's details.
		/// </summary>
		/// <param name="areaCode">The area code.</param>
		/// <param name="description">The description.</param>
		/// <returns>
		/// 	<c>true</c> if the insert/update is successful, otherwise <c>false</c>.
		/// </returns>
		public bool SaveArea(string areaCode, string description)
		{
			SqlConnection con = new SqlConnection(connectionString);
			SqlCommand com = new SqlCommand("AreaUpdate", con);
			com.CommandType = CommandType.StoredProcedure;

			com.Parameters.Add("@AreaCode", SqlDbType.VarChar, 8).Value = areaCode;
			com.Parameters.Add("@AreaDescr", SqlDbType.VarChar, 100).Value = description;

			try
			{
				con.Open();
				com.ExecuteNonQuery();

				return true;
			}
			catch (SqlException se)
			{
				System.Diagnostics.Debug.WriteLine(se.Message);
			}
			finally
			{
				con.Close();
			}

			return false;
		}

        public string DeleteArea(string areaCode)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("AreaDelete", con);
            com.CommandType = CommandType.StoredProcedure;
            // Add output parameter
            SqlParameter parameterAreaCode = new SqlParameter("@AreaCode", SqlDbType.VarChar, 8);
            parameterAreaCode.Value = areaCode;
            parameterAreaCode.Direction = ParameterDirection.InputOutput;
            com.Parameters.Add(parameterAreaCode);
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Dispose();
                string ac = (string)com.Parameters["@AreaCode"].Value;
                return ac;
            }
            catch (Exception e)
            {
                con.Dispose();
                string msg = e.Message;
                return "Bad";
            }
        }

    }
}
