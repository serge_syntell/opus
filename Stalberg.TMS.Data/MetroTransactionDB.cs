﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{
    public partial class MetroTransactionDetails
    {
        public Int32 MTIntNo;
        public Int32 MetroIntNo;
        public Int32 TTIntNo;
        public Int32 MTNumber;
        public string LastUser;
    }
    public partial class MetroTransactionDB
    {
        string mConstr = "";

        public MetroTransactionDB(string vConstr)
        {
            mConstr = vConstr;
        }
        public DataSet GetMetroTransactionListDS(int metroIntNo)
        {
            SqlDataAdapter sqlDAMetroTransactions = new SqlDataAdapter();
            DataSet dsMetroTransactions = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAMetroTransactions.SelectCommand = new SqlCommand();
            sqlDAMetroTransactions.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAMetroTransactions.SelectCommand.CommandText = "MetroTransactionList";

            // Mark the Command as a SPROC
            sqlDAMetroTransactions.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterMetroIntNo = new SqlParameter("@MetroIntNo", SqlDbType.Int, 4);
            parameterMetroIntNo.Value = metroIntNo;
            sqlDAMetroTransactions.SelectCommand.Parameters.Add(parameterMetroIntNo);

            // Execute the command and close the connection
            sqlDAMetroTransactions.Fill(dsMetroTransactions);
            sqlDAMetroTransactions.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsMetroTransactions;
        }

        public MetroTransactionDetails GetMetroTransactionDetails(int mtIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("MetroTransactionDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterMTIntNo = new SqlParameter("@MTIntNo", SqlDbType.Int, 4);
            parameterMTIntNo.Value = mtIntNo;
            myCommand.Parameters.Add(parameterMTIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            MetroTransactionDetails myMetroTransactionDetails = new MetroTransactionDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myMetroTransactionDetails.MTIntNo = Convert.ToInt32(result["MTIntNo"]);
                myMetroTransactionDetails.MetroIntNo = Convert.ToInt32(result["MetroIntNo"]);
                myMetroTransactionDetails.TTIntNo = Convert.ToInt32(result["TTIntNo"]);
                myMetroTransactionDetails.MTNumber = Convert.ToInt32(result["MTNumber"]);
                myMetroTransactionDetails.LastUser = result["LastUser"].ToString();
            }
            result.Close();
            return myMetroTransactionDetails;
        }

        public int AddMetroTransaction(int metroIntNo, string ttIntNo, int mtNumber, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("MetroTransactionAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterMetroIntNo = new SqlParameter("@MetroIntNo", SqlDbType.Int, 4);
            parameterMetroIntNo.Value = metroIntNo;
            myCommand.Parameters.Add(parameterMetroIntNo);

            SqlParameter parameterTTIntNo = new SqlParameter("@TTIntNo", SqlDbType.VarChar, 3);
            parameterTTIntNo.Value = ttIntNo;
            myCommand.Parameters.Add(parameterTTIntNo);

            SqlParameter parameterMTNumber = new SqlParameter("@MTNumber", SqlDbType.Int);
            parameterMTNumber.Value = mtNumber;
            myCommand.Parameters.Add(parameterMTNumber);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterMTIntNo = new SqlParameter("@MTIntNo", SqlDbType.Int, 4);
            parameterMTIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterMTIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int mtIntNo = Convert.ToInt32(parameterMTIntNo.Value);

                return mtIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int UpdateMetroTransaction(int mtIntNo, string metroIntNo, string ttIntNo, string mtNumber, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("MetroTransactionUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            // Add Parameters to SPROC
            SqlParameter parameterMetroIntNo = new SqlParameter("@MetroIntNo", SqlDbType.Int, 4);
            parameterMetroIntNo.Value = metroIntNo;
            myCommand.Parameters.Add(parameterMetroIntNo);

            SqlParameter parameterTTIntNo = new SqlParameter("@TTIntNo", SqlDbType.VarChar, 3);
            parameterTTIntNo.Value = ttIntNo;
            myCommand.Parameters.Add(parameterTTIntNo);

            SqlParameter parameterMTNumber = new SqlParameter("@MTNumber", SqlDbType.Int);
            parameterMTNumber.Value = mtNumber;
            myCommand.Parameters.Add(parameterMTNumber);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterMTIntNo = new SqlParameter("@MTIntNo", SqlDbType.Int, 4);
            parameterMTIntNo.Value = mtIntNo;
            parameterMTIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterMTIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                mtIntNo = Convert.ToInt32(myCommand.Parameters["@MTIntNo"].Value);

                return mtIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }
        public String DeleteMetroTransaction(int mtIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("MetroTransactionDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterMTIntNo = new SqlParameter("@MTIntNo", SqlDbType.Int, 4);
            parameterMTIntNo.Value = mtIntNo;
            parameterMTIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterMTIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                mtIntNo = (int)parameterMTIntNo.Value;

                return mtIntNo.ToString();
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return String.Empty;
            }
        }
    }
}
