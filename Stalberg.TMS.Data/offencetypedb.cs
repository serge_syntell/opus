using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // OffenceTypeDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class OffenceTypeDetails 
	{
        public string OcTCode;
		public string OcType;
		public string OcTDescr;
		public string OcTDescr1;
        public string OcTEngShortDescr;
        public string OcTAfrShortDescr;
        public string OcTDescriptionEng;
        public string OcTDescriptionAfr;
    }

    //*******************************************************
    //
    // OffenceTypeDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query OffenceTypes within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class OffenceTypeDB {

		string mConstr = "";

		public OffenceTypeDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // OffenceTypeDB.GetOffenceTypeDetails() Method <a name="GetOffenceTypeDetails"></a>
        //
        // The GetOffenceTypeDetails method returns a OffenceTypeDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public OffenceTypeDetails GetOffenceTypeDetails(int ocTIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenceTypeDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterOcTIntNo = new SqlParameter("@OcTIntNo", SqlDbType.Int, 4);
            parameterOcTIntNo.Value = ocTIntNo;
            myCommand.Parameters.Add(parameterOcTIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			OffenceTypeDetails myOffenceTypeDetails = new OffenceTypeDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myOffenceTypeDetails.OcTCode = result["OcTCode"].ToString();
				myOffenceTypeDetails.OcType = result["OcType"].ToString();
				myOffenceTypeDetails.OcTDescr = result["OcTDescr"].ToString();
				myOffenceTypeDetails.OcTDescr1 = result["OcTDescr1"].ToString();
                myOffenceTypeDetails.OcTEngShortDescr = result["OcTEngShortDescr"].ToString();
                myOffenceTypeDetails.OcTAfrShortDescr = result["OcTAfrShortDescr"].ToString();
                myOffenceTypeDetails.OcTDescriptionEng = result["OcTDescriptionEng"].ToString();
                myOffenceTypeDetails.OcTDescriptionAfr = result["OcTDescriptionAfr"].ToString();
			}
			result.Close();
            return myOffenceTypeDetails;
        }

		public OffenceTypeDetails GetOffenceTypeDetailsByCode(string ocTCode) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OffenceTypeDetailByCode", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterOcTCode = new SqlParameter("@OcTCode", SqlDbType.Char,2 );
			parameterOcTCode.Value = ocTCode;
			myCommand.Parameters.Add(parameterOcTCode);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			// Create CustomerDetails Struct
			OffenceTypeDetails myOffenceTypeDetails = new OffenceTypeDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myOffenceTypeDetails.OcTCode = result["OcTCode"].ToString();
				myOffenceTypeDetails.OcType = result["OcType"].ToString();
				myOffenceTypeDetails.OcTDescr = result["OcTDescr"].ToString();
				myOffenceTypeDetails.OcTDescr1 = result["OcTDescr1"].ToString();
                myOffenceTypeDetails.OcTEngShortDescr = result["OcTEngShortDescr"].ToString();
                myOffenceTypeDetails.OcTAfrShortDescr = result["OcTAfrShortDescr"].ToString();
			}
			result.Close();
			return myOffenceTypeDetails;
		}

        //*******************************************************
        //
        // OffenceTypeDB.AddOffenceType() Method <a name="AddOffenceType"></a>
        //
        // The AddOffenceType method inserts a new menu record
        // into the menus database.  A unique "OffenceTypeId"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddOffenceType(string ocTCode, string ocType, string ocTDescr,
                 string ocTDescr1, string ocTDescriptionEng, string ocTDescriptionAfr, string lastUser)
        {
            return AddOffenceType(ocTCode, ocType, ocTDescr, ocTDescr1, ocTDescriptionEng, ocTDescriptionAfr, lastUser, string.Empty, string.Empty);
        }

        public int AddOffenceType (string ocTCode, string ocType, string ocTDescr,
                 string ocTDescr1, string ocTDescriptionEng, string ocTDescriptionAfr, string lastUser, string ocTEngShortDescr, string ocTAfrShortDescr) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("OffenceTypeAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
			SqlParameter parameterOcTCode = new SqlParameter("@OcTCode", SqlDbType.Char,2 );
			parameterOcTCode.Value = ocTCode;
			myCommand.Parameters.Add(parameterOcTCode);

			SqlParameter parameterOcType = new SqlParameter("@OcType", SqlDbType.VarChar, 50);
			parameterOcType.Value = ocType;
			myCommand.Parameters.Add(parameterOcType);

			SqlParameter parameterOcTDescr = new SqlParameter("@OcTDescr", SqlDbType.Text);
			parameterOcTDescr.Value = ocTDescr;
			myCommand.Parameters.Add(parameterOcTDescr);
			
			SqlParameter parameterOcTDescr1 = new SqlParameter("@OcTDescr1", SqlDbType.Text);
			parameterOcTDescr1.Value = ocTDescr1;
			myCommand.Parameters.Add(parameterOcTDescr1);

            //2013-03-06 added by Nancy for 4890 --start
            SqlParameter parameterDescriptionEng = new SqlParameter("@OcTDescriptionEng", SqlDbType.VarChar);
            parameterDescriptionEng.Value = ocTDescriptionEng;
            myCommand.Parameters.Add(parameterDescriptionEng);

            SqlParameter parameterDescriptionAfr = new SqlParameter("@OcTDescriptionAfr", SqlDbType.VarChar);
            parameterDescriptionAfr.Value = ocTDescriptionAfr;
            myCommand.Parameters.Add(parameterDescriptionAfr);
            //2013-03-06 added by Nancy for 4890 --end
			
			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterOcTIntNo = new SqlParameter("@OcTIntNo", SqlDbType.Int, 4);
            parameterOcTIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterOcTIntNo);

            SqlParameter parameterOcTEngShortDescr = new SqlParameter("@OcTEngShortDescr", SqlDbType.VarChar, 255);
            parameterOcTEngShortDescr.Value = ocTEngShortDescr;
            myCommand.Parameters.Add(parameterOcTEngShortDescr);

            SqlParameter parameterOcTAfrShortDescr = new SqlParameter("@OcTAfrShortDescr", SqlDbType.VarChar, 255);
            parameterOcTAfrShortDescr.Value = ocTAfrShortDescr;
            myCommand.Parameters.Add(parameterOcTAfrShortDescr);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int ocTIntNo = Convert.ToInt32(parameterOcTIntNo.Value);

                return ocTIntNo;
            }
            catch (Exception e)
			{
				string msg = e.Message;
				myConnection.Dispose();
                return 0;
            }
        }

		public SqlDataReader GetOffenceTypeList()
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OffenceTypeList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		
		public DataSet GetOffenceTypeListDS()
		{
			SqlDataAdapter sqlDAOffenceTypes = new SqlDataAdapter();
			DataSet dsOffenceTypes = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAOffenceTypes.SelectCommand = new SqlCommand();
			sqlDAOffenceTypes.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAOffenceTypes.SelectCommand.CommandText = "OffenceTypeList";

			// Mark the Command as a SPROC
			sqlDAOffenceTypes.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Execute the command and close the connection
			sqlDAOffenceTypes.Fill(dsOffenceTypes);
			sqlDAOffenceTypes.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsOffenceTypes;		
		}

        public DataSet GetOffenceTypeListForSpeedOffencesDS()
        {
            SqlDataAdapter sqlDAOffenceTypes = new SqlDataAdapter();
            DataSet dsOffenceTypes = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAOffenceTypes.SelectCommand = new SqlCommand();
            sqlDAOffenceTypes.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAOffenceTypes.SelectCommand.CommandText = "OffenceTypeListForSpeedOffences";

            // Mark the Command as a SPROC
            sqlDAOffenceTypes.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command and close the connection
            sqlDAOffenceTypes.Fill(dsOffenceTypes);
            sqlDAOffenceTypes.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsOffenceTypes;
        }

		
		public int UpdateOffenceType (int ocTIntNo, string ocTCode, string ocType, string ocTDescr,
			string ocTDescr1, string DescriptionEng, string DescriptionAfr, string lastUser, string ocTEngShortDescr, string ocTAfrShortDescr) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OffenceTypeUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterOcTCode = new SqlParameter("@OcTCode", SqlDbType.Char,2 );
			parameterOcTCode.Value = ocTCode;
			myCommand.Parameters.Add(parameterOcTCode);

			SqlParameter parameterOcType = new SqlParameter("@OcType", SqlDbType.VarChar, 50);
			parameterOcType.Value = ocType;
			myCommand.Parameters.Add(parameterOcType);

			SqlParameter parameterOcTDescr = new SqlParameter("@OcTDescr", SqlDbType.Text);
			parameterOcTDescr.Value = ocTDescr;
			myCommand.Parameters.Add(parameterOcTDescr);
			
			SqlParameter parameterOcTDescr1 = new SqlParameter("@OcTDescr1", SqlDbType.Text);
			parameterOcTDescr1.Value = ocTDescr1;
			myCommand.Parameters.Add(parameterOcTDescr1);

            //2013-03-06 added by Nancy for 4890 --start
            SqlParameter parameterDescriptionEng = new SqlParameter("@OcTDescriptionEng", SqlDbType.Text);
            parameterDescriptionEng.Value = DescriptionEng;
            myCommand.Parameters.Add(parameterDescriptionEng);

            SqlParameter parameterDescriptionAfr = new SqlParameter("@OcTDescriptionAfr", SqlDbType.Text);
            parameterDescriptionAfr.Value = DescriptionAfr;
            myCommand.Parameters.Add(parameterDescriptionAfr);
            //2013-03-06 added by Nancy for 4890 --end

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);
			
			SqlParameter parameterOcTIntNo = new SqlParameter("@OcTIntNo", SqlDbType.Int);
			parameterOcTIntNo.Direction = ParameterDirection.InputOutput;
			parameterOcTIntNo.Value = ocTIntNo;
			myCommand.Parameters.Add(parameterOcTIntNo);

            SqlParameter parameterOcTEngShortDescr = new SqlParameter("@OcTEngShortDescr", SqlDbType.VarChar, 255);
            parameterOcTEngShortDescr.Value = ocTEngShortDescr;
            myCommand.Parameters.Add(parameterOcTEngShortDescr);

            SqlParameter parameterOcTAfrShortDescr = new SqlParameter("@OcTAfrShortDescr", SqlDbType.VarChar, 255);
            parameterOcTAfrShortDescr.Value = ocTAfrShortDescr;
            myCommand.Parameters.Add(parameterOcTAfrShortDescr);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int OcTIntNo = (int)myCommand.Parameters["@OcTIntNo"].Value;
				//int menuId = (int)parameterOcTIntNo.Value;

				return OcTIntNo;
			}
			catch (Exception e)
			{
				string msg = e.Message;
				myConnection.Dispose();
				return 0;
			}
		}

		
		public String DeleteOffenceType (int ocTIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OffenceTypeDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterOcTIntNo = new SqlParameter("@OcTIntNo", SqlDbType.Int, 4);
			parameterOcTIntNo.Value = ocTIntNo;
			parameterOcTIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterOcTIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int OcTIntNo = (int)parameterOcTIntNo.Value;

				return OcTIntNo.ToString();
			}
			catch 
			{
				myConnection.Dispose();
				return String.Empty;
			}
		}
	}
}

