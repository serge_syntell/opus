﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS.Data
{
    public class ChangeOfOwnerDB
    {
        string mConstr = "";

        public ChangeOfOwnerDB(string vConstr)
		{
			mConstr = vConstr;
		}

        public DataSet GetCOOIDNotice(int CCIIntNo, int CCPIntNo, int PageIndex, int PageSize)
        {
            // Create SQL data access objects
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = new SqlCommand("COOGetIDNotice", new SqlConnection(this.mConstr));
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            da.SelectCommand.Parameters.Add("@CCIIntNo", SqlDbType.Int, 4).Value = CCIIntNo;
            da.SelectCommand.Parameters.Add("@CCPIntNo", SqlDbType.Int, 4).Value = CCPIntNo;
            da.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int, 4).Value = PageIndex;
            da.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int, 4).Value = PageSize;

            // Execute the command and fill the data set
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.SelectCommand.Connection.Dispose();

            // Return the data set result
            return ds;
        }

        public int UpdateCOOIDNotice(int autIntNo, int CCIIntNo, int CCPIntNo, int ActionCode, string LastUser)
        {
            int result = -1;
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand cmd = new SqlCommand("COOUpdateIDNotice", con);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            cmd.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCCIIntNo = new SqlParameter("@CCIIntNo", SqlDbType.Int, 4);
            parameterCCIIntNo.Value = CCIIntNo;
            cmd.Parameters.Add(parameterCCIIntNo);

            SqlParameter parameterCCPIntNo = new SqlParameter("@CCPIntNo", SqlDbType.Int, 4);
            parameterCCPIntNo.Value = CCPIntNo;
            cmd.Parameters.Add(parameterCCPIntNo);

            SqlParameter parameterActionCode = new SqlParameter("@ActionCode", SqlDbType.Int, 4);
            parameterActionCode.Value = ActionCode;
            cmd.Parameters.Add(parameterActionCode);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = LastUser;
            cmd.Parameters.Add(parameterLastUser);

            con.Open();
            result = cmd.ExecuteNonQuery();
            con.Dispose();

            return result;
        }
    }
}
