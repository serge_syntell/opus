﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using Stalberg.TMS;

namespace Stalberg.TMS.Data
{
    public class RoadBlockFalseNumberPlateData      //Road block record - Down file
    {
        //added elements to struct
        public string filmNo;
        public string frameNo;
        public string imageNoA;
        public string imageNoB;
        public string registrationNo;
        public string locationDesc;
        public string offenceDateTime;
        public string offenceVehicleType;
        public string offenceVehicleDesc;
        public string natisVehicleType;
        public string natisVehicleDesc;
        public string natisVehicleLicence;
        public string natisVehicleLicenceExpire;
        public string natisVINNo;
        public string natisEngineNo;
        public string natisVehicleColour;
        public string recType;
        public string offenceDesc;
        public string AuthName; //2013-11-06 added byNancy

        /// <summary>
        /// Writes the file output of this record.
        /// </summary>
        /// <returns>A <see cref="String"/> containing the output of the record</returns>
        public string Write()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(this.filmNo);
            sb.Append(",");
            sb.Append(this.frameNo);
            sb.Append(",");
            sb.Append(this.registrationNo);
            sb.Append(",");
            sb.Append(this.locationDesc);
            sb.Append(",");
            sb.Append(this.offenceDateTime);
            sb.Append(",");
            sb.Append(this.offenceVehicleType);
            sb.Append(",");
            sb.Append(this.offenceVehicleDesc);
            sb.Append(",");
            sb.Append(this.natisVehicleType);
            sb.Append(",");
            sb.Append(this.natisVehicleDesc);
            sb.Append(",");
            sb.Append(this.natisVehicleLicence);
            sb.Append(",");
            sb.Append(this.natisVehicleLicenceExpire);
            sb.Append(",");
            sb.Append(this.natisVINNo);
            sb.Append(",");
            sb.Append(this.natisEngineNo);
            sb.Append(",");
            sb.Append(this.natisVehicleColour);
            sb.Append(",");
            sb.Append(this.recType);
            sb.Append(",");
            sb.Append(this.offenceDesc);
            //2013-11-06 added by Nancy 'Authority name'
            sb.Append(",");
            sb.Append(this.AuthName);
            sb.Append((char)Convert.ToInt32("0D", 16));
            sb.Append((char)Convert.ToInt32("0A", 16));
            return sb.ToString();
        }
    }


    /// <summary>
    /// Contains the database logic for interacting with road block data
    /// </summary>
    public class RoadBlockFalseNumberPlateRecord
    {
        // Fields
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="CiprusData"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public RoadBlockFalseNumberPlateRecord(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public SqlDataReader GetRoadBlockFalseNumberPlateData(string mtrCode, out SqlConnection connection)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("RoadblockData_FalseNumberPlate", myConnection);

            //Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            //Allow for timeouts
            myCommand.CommandTimeout = 0;

            //Add Parameters to SPROC
            //SqlParameter parameterARString = new SqlParameter("@ARString", SqlDbType.Char, 3);
            //parameterARString.Value = ARString;
            //myCommand.Parameters.Add(parameterARString);

            SqlParameter parameterMTRCode = new SqlParameter("@MTRCode", SqlDbType.Char, 3);
            parameterMTRCode.Value = mtrCode;
            myCommand.Parameters.Add(parameterMTRCode);

            try
            {
                connection = myConnection;
                myConnection.Open();
                SqlDataReader reader = myCommand.ExecuteReader();
                //myConnection.Close();
                return reader;
            }
            catch (Exception e)
            {
                connection = null;
                myConnection.Dispose();
                string errorMsg = e.Message;
                return null;
            }
        }


    }
}
