﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Stalberg.TMS.Data
{
    public partial class LocationSuburbDetails
    {
        public Int32 LoSuIntNo;
        public string LoSuDescr;
        public Int32 AutIntNo;
    }

   public partial  class LocationSuburbDB
    {
        // Fields
		string mConstr = string.Empty;
     
        public LocationSuburbDB(string vConstr)
		{
			mConstr = vConstr;
		}

        /// <summary>
        /// Gets the auth_ court list by auth.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        public SqlDataReader Auth_LocationSuburbListByAuth(int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("Auth_LocationSuburbListByAuth", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader 
            return result;
        }

        /// <summary>
        /// Gets the auth_ court list by auth DS.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        public DataSet Auth_LocationSuburbListByAuthDS(int autIntNo)
        {
            SqlDataAdapter sqlDALocationSuburbs = new SqlDataAdapter();
            DataSet dsLocationSuburbs = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDALocationSuburbs.SelectCommand = new SqlCommand();
            sqlDALocationSuburbs.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDALocationSuburbs.SelectCommand.CommandText = "Auth_LocationSuburbListByAuth";

            // Mark the Command as a SPROC
            sqlDALocationSuburbs.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDALocationSuburbs.SelectCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command and close the connection
            sqlDALocationSuburbs.Fill(dsLocationSuburbs);
            sqlDALocationSuburbs.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsLocationSuburbs;
        }

       /// <summary>
        /// Jerry 2013-09-12 Get Loction Suburb DDL by AutIntNo
       /// </summary>
       /// <param name="autIntNo"></param>
       /// <returns></returns>
        public DataSet GetLocationSuburbListByAutIntNo(int autIntNo)
        {
            SqlDataAdapter sqlDALocationSuburbs = new SqlDataAdapter();
            DataSet dsLocationSuburbs = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDALocationSuburbs.SelectCommand = new SqlCommand();
            sqlDALocationSuburbs.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDALocationSuburbs.SelectCommand.CommandText = "SILCustom_LocationSuburb_GetByAutIntNo";

            // Mark the Command as a SPROC
            sqlDALocationSuburbs.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Value = autIntNo;
            sqlDALocationSuburbs.SelectCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command and close the connection
            sqlDALocationSuburbs.Fill(dsLocationSuburbs);
            sqlDALocationSuburbs.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsLocationSuburbs;
        }

    }
}
