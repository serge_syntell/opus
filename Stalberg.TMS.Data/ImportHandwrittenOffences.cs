﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;


namespace Stalberg.TMS.Data
{
    public class ImportHandwrittenOffences
    {
        //Jerry 2012-10-24 change
        static string mConstr; //= ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString();

        public ImportHandwrittenOffences(string vConstr)
        {
            mConstr = vConstr;
        }

        public ImportHandwrittenOffences()
        {

        }

        public int GetAuthIntNoByAutCode(string autCode)
        {
            int autIntNo = 0;
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthorityGetIDFromCode", myConnection);

            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutCode = new SqlParameter("@AutCode", SqlDbType.VarChar, 3);
            parameterAutCode.Value = autCode;
            myCommand.Parameters.Add(parameterAutCode);

            try
            {

                myConnection.Open();
                SqlDataReader reader = myCommand.ExecuteReader();
                while (reader.Read())
                {
                    autIntNo = Convert.ToInt32(reader["AutIntNo"].ToString());
                    break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                myConnection.Close();
            }
            return autIntNo;
        }

        public int CheckImageFileServer(string sysFileName, string ifServerName, string imageMachineName, string imageDrive, string imageRoorFolder, bool isDefault, string imageSharename, string lastUser)
        {

            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CheckImageFileServer", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterSysFileName = new SqlParameter("@SysFileName", SqlDbType.VarChar, 50);
            parameterSysFileName.Value = sysFileName;
            myCommand.Parameters.Add(parameterSysFileName);

            SqlParameter parameterIFServerName = new SqlParameter("@IFServerName", SqlDbType.VarChar, 50);
            parameterIFServerName.Value = ifServerName;
            myCommand.Parameters.Add(parameterIFServerName);

            SqlParameter parameterImageMachineName = new SqlParameter("@ImageMachineName", SqlDbType.VarChar, 50);
            parameterImageMachineName.Value = imageMachineName;
            myCommand.Parameters.Add(parameterImageMachineName);

            SqlParameter parameterImageDrive = new SqlParameter("@ImageDrive", SqlDbType.Char, 2);
            parameterImageDrive.Value = imageDrive;
            myCommand.Parameters.Add(parameterImageDrive);

            SqlParameter parameterImageRootFolder = new SqlParameter("@ImageRootFolder", SqlDbType.VarChar, 100);
            parameterImageRootFolder.Value = imageRoorFolder;
            myCommand.Parameters.Add(parameterImageRootFolder);

            SqlParameter parameterIsDefault = new SqlParameter("@IsDefault", SqlDbType.Bit);
            parameterIsDefault.Value = isDefault;
            myCommand.Parameters.Add(parameterIsDefault);

            SqlParameter parameterImageShareName = new SqlParameter("@ImageShareName", SqlDbType.VarChar, 100);
            parameterImageShareName.Value = imageSharename;
            myCommand.Parameters.Add(parameterImageShareName);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterIFSIntNo = new SqlParameter("@IfSIntNo", SqlDbType.Int);
            //parameterIFSIntNo.Value = ifSIntNo;
            parameterIFSIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterIFSIntNo);

            try
            {
                int ifSIntNo = 0;

                myConnection.Open();
                myCommand.ExecuteNonQuery();

                ifSIntNo = Convert.ToInt32(parameterIFSIntNo.Value);
                //int notIntNo = Convert.ToInt16(myCommand.Parameters["@NotIntNo"].Value.ToString().Trim());
                return ifSIntNo;

                //return notIntNo;
            }
            catch (Exception ex)
            {
                //ImportHandwrittenOffences.AddLog(ex.Message);
                throw ex;

            }
            finally
            {
                myConnection.Dispose();
            }

        }

        public bool ImportAartoNoticeDocument(int notIntNo, string docNo,
            int docImageOrder, string imageFilePath, int ifsIntNo, string flag, DateTime imageLoadedDate, string lastUser)
        {

            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ImportNoticeDocumentForHandWritten", myConnection);

            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int);
            parameterNotIntNo.Value = notIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            //SqlParameter parameterBatchID = new SqlParameter("@BatchID", SqlDbType.Int);
            //parameterBatchID.Value = batchID;
            //myCommand.Parameters.Add(parameterBatchID);

            SqlParameter parameterDocNo = new SqlParameter("@DocNo", SqlDbType.VarChar, 20);
            parameterDocNo.Value = docNo;
            myCommand.Parameters.Add(parameterDocNo);

            SqlParameter parameterAaDocImgDateLoad = new SqlParameter("@AaDocImgDateLoad", SqlDbType.DateTime);
            parameterAaDocImgDateLoad.Value = imageLoadedDate;
            myCommand.Parameters.Add(parameterAaDocImgDateLoad);

            SqlParameter parameterAaDocImgOrder = new SqlParameter("@AaDocImgOrder", SqlDbType.Int);
            parameterAaDocImgOrder.Value = docImageOrder;
            myCommand.Parameters.Add(parameterAaDocImgOrder);

            SqlParameter parameterImageFilePath = new SqlParameter("@ImageFilePath", SqlDbType.VarChar, 200);
            parameterImageFilePath.Value = imageFilePath;
            myCommand.Parameters.Add(parameterImageFilePath);

            SqlParameter parameterIFSIntNo = new SqlParameter("@IFSIntNo", SqlDbType.Int);
            parameterIFSIntNo.Value = ifsIntNo;
            myCommand.Parameters.Add(parameterIFSIntNo);

            SqlParameter parameterFlag = new SqlParameter("@Flag", SqlDbType.VarChar, 20);
            parameterFlag.Value = flag;
            myCommand.Parameters.Add(parameterFlag);

            //SqlParameter parameterDoTeTypeID = new SqlParameter("@DoTeTypeID", SqlDbType.Int);
            //parameterDoTeTypeID.Value = docTypeID;
            //myCommand.Parameters.Add(parameterDoTeTypeID);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 200);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            try
            {
                myConnection.Open();

                return myCommand.ExecuteNonQuery() > 0;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                myConnection.Close();
            }

        }


        //public int AddOfficerError(SqlConnection conn, SqlTransaction tran, int notIntNo, string officerError, string lastUser)
        //{
        //    int returnValue = 0;
        //    //SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("SILCustom_Notice_NoticeOfficerErrorAdd", conn, tran);
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int);
        //    parameterNotIntNo.Value = notIntNo;
        //    myCommand.Parameters.Add(parameterNotIntNo);

        //    SqlParameter parameterOfficerError = new SqlParameter("@OfficerError", SqlDbType.VarChar, 500);
        //    parameterOfficerError.Value = officerError;
        //    myCommand.Parameters.Add(parameterOfficerError);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterReturn = new SqlParameter("@ReturnValue", SqlDbType.Int);
        //    parameterReturn.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterReturn);


        //    try
        //    {

        //        //myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        returnValue = Convert.ToInt32(parameterReturn.Value);
        //        //int notIntNo = Convert.ToInt16(myCommand.Parameters["@NotIntNo"].Value.ToString().Trim());
        //        return returnValue;

        //        //return notIntNo;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        // myConnection.Dispose();
        //    }
        //}

        //Jake 10-08-26 Added sqltransaction to commit
        // Jake 2010-12-30 added twe fields for notice
        //Heidi 2013-05-20 added loSuIntNo
        //Jerry 2014-07-31 added accTelCell, policeStation, district,  licenceNumber, pdpCodeName, licenceCodeName parameters
        #region ImportHandwrittenOffencesSection56

        public int ImportHandwrittenOffencesSection56(int autIntNo, string notFilmNo, string notFrameNo, string notTicketNo,
            int speedLimit, int speedReading, DateTime offenceDate, string notLocDescr, string notRegNo,
            string notVehicleMakeCode, string notVehicleMake, string notVehicleTypeCode, string notVehicleType,
            string notOfficerNo, string notOfficerSname, string notOfficerGroup,//17
            string notDMSCaptureClerk, DateTime? notDMSCaptureDate,
            string chgOffenceCode1, string chgOffenceDescr1,
            string chgOffenceCode2, string chgOffenceDescr2,
            string chgOffenceCode3, string chgOffenceDescr3,
            float chgFineAmount1, float chgFineAmount2, float chgFineAmount3, DateTime? notPaymentDate,
            string chgOfficerNatisNo, string chgOfficerName, string conCode,

            DateTime? sumCourtDate, string sumOfficerNo, string sumCaseNo,
            string crtNo, string crtRoomNo, int ifsIntNo,//7

            string accSurname, string accForenames, string accIdType, string accIdNumber, string accOccupation, string accAge, string accSex,
            string accPoAdd1, string accPoCode, string accTelHome, string accStAdd1, string accStAdd2, string accStCode,
            string accTelWork, bool isOfficerError,
            string docNo, int aaDocImgOrder, string imageFilePath, string lastUser, string officerErrorString,
            string nationality, string drvLicence, string accPoAdd2, string agNo, string accPoSuburb, string accPoTownOrCity,
            string accStSuburb, string accStTownOrCity, bool overAge,
            string chgAlternateCode1, string chgAlternateCode2, string chgAlternateCode3,
            string chgNoAog1, string chgNoAog2, string chgNoAog3, string nstCode, int loSuIntNo, string scanDate, string psttName = "HandwrittenS56Capture", string regNo2 = "",
            string accTelCell = "", string policeStation = "", string district = "", string licenceNumber = "", string licenceTypeName = "")//12
        {
            SqlConnection myConnection = new SqlConnection(mConstr);

            SqlCommand myCommand = new SqlCommand("ImportHandwrittenOffencesSection56", myConnection);

            myCommand.CommandType = CommandType.StoredProcedure;

            #region Notice
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterNotFilmNo = new SqlParameter("@NotFilmNo", SqlDbType.VarChar, 20);
            parameterNotFilmNo.Value = notFilmNo;
            myCommand.Parameters.Add(parameterNotFilmNo);

            SqlParameter parameterNotFrameNo = new SqlParameter("@NotFrameNo", SqlDbType.VarChar, 20);
            parameterNotFrameNo.Value = notFrameNo;
            myCommand.Parameters.Add(parameterNotFrameNo);

            SqlParameter parameterNotTicketNo = new SqlParameter("@NotTicketNo", SqlDbType.VarChar, 50);
            parameterNotTicketNo.Value = notTicketNo;
            myCommand.Parameters.Add(parameterNotTicketNo);

            SqlParameter parameterSpeedLimit = new SqlParameter("@SpeedLimit", SqlDbType.Int);
            parameterSpeedLimit.Value = speedLimit;
            myCommand.Parameters.Add(parameterSpeedLimit);

            SqlParameter parameterSpeedReading = new SqlParameter("@SpeedReading", SqlDbType.Int);
            parameterSpeedReading.Value = speedReading;
            myCommand.Parameters.Add(parameterSpeedReading);

            SqlParameter parameterOffenceDate = new SqlParameter("@OffenceDate", SqlDbType.DateTime);
            parameterOffenceDate.Value = offenceDate;
            myCommand.Parameters.Add(parameterOffenceDate);

            //SqlParameter parameterNotLocCode = new SqlParameter("@NotLocCode", SqlDbType.VarChar, 50);
            //parameterNotLocCode.Value = notLocCode;
            //myCommand.Parameters.Add(parameterNotLocCode);

            SqlParameter parameterNotLocDescr = new SqlParameter("@NotLocDescr", SqlDbType.VarChar, 150);
            if (String.IsNullOrEmpty(notLocDescr)) notLocDescr = "";
            else
            {
                if (notLocDescr.Length > 150) notLocDescr = notLocDescr.Substring(0, 150);
            }
            parameterNotLocDescr.Value = notLocDescr;
            myCommand.Parameters.Add(parameterNotLocDescr);

            SqlParameter parameterNotRegNo = new SqlParameter("@NotRegNo", SqlDbType.VarChar, 10);
            parameterNotRegNo.Value = notRegNo;
            myCommand.Parameters.Add(parameterNotRegNo);

            //Jerry 2013-07-09 add for Swartland
            SqlParameter parameterRegNo2 = new SqlParameter("@RegNo2", SqlDbType.VarChar, 10);
            parameterRegNo2.Value = regNo2;
            myCommand.Parameters.Add(parameterRegNo2);

            SqlParameter parameterNotVehicleMakeCode = new SqlParameter("@NotVehicleMakeCode", SqlDbType.VarChar, 3);
            parameterNotVehicleMakeCode.Value = notVehicleMakeCode;
            myCommand.Parameters.Add(parameterNotVehicleMakeCode);

            SqlParameter parameterNotVehicleMake = new SqlParameter("@NotVehicleMake", SqlDbType.VarChar, 30);
            parameterNotVehicleMake.Value = notVehicleMake;
            myCommand.Parameters.Add(parameterNotVehicleMake);

            SqlParameter parameterNotVehicleTypeCode = new SqlParameter("@NotVehicleTypeCode", SqlDbType.VarChar, 3);
            parameterNotVehicleTypeCode.Value = notVehicleTypeCode;
            myCommand.Parameters.Add(parameterNotVehicleTypeCode);

            SqlParameter parameterNotVehicleType = new SqlParameter("@NotVehicleType", SqlDbType.VarChar, 30);
            parameterNotVehicleType.Value = notVehicleType;
            myCommand.Parameters.Add(parameterNotVehicleType);

            SqlParameter parameterNotOfficerNo = new SqlParameter("@NotOfficerNo", SqlDbType.VarChar, 10);
            parameterNotOfficerNo.Value = notOfficerNo;
            myCommand.Parameters.Add(parameterNotOfficerNo);


            SqlParameter parameterNotOfficerSname = new SqlParameter("@NotOfficerSname", SqlDbType.VarChar, 35);
            parameterNotOfficerSname.Value = notOfficerSname;
            myCommand.Parameters.Add(parameterNotOfficerSname);

            SqlParameter parameterNotOfficerGroup = new SqlParameter("@NotOfficerGroup", SqlDbType.VarChar, 50);
            parameterNotOfficerGroup.Value = notOfficerGroup;
            myCommand.Parameters.Add(parameterNotOfficerGroup);

            SqlParameter parameterIsOfficerError = new SqlParameter("@IsOfficerError", SqlDbType.Bit);
            parameterIsOfficerError.Value = isOfficerError;
            myCommand.Parameters.Add(parameterIsOfficerError);

            SqlParameter parameterOfficerErrorStr = new SqlParameter("@OfficerErrorStr", SqlDbType.VarChar, 500);
            parameterOfficerErrorStr.Value = officerErrorString;
            myCommand.Parameters.Add(parameterOfficerErrorStr);

            SqlParameter parameterNotDMSCaptureClerk = new SqlParameter("@NotDMSCaptureCLerk", SqlDbType.VarChar, 25);
            parameterNotDMSCaptureClerk.Value = notDMSCaptureClerk;
            myCommand.Parameters.Add(parameterNotDMSCaptureClerk);

            SqlParameter parameterNotDMSCaptureDate = new SqlParameter("@NotDMSCaptureDate", SqlDbType.SmallDateTime);
            parameterNotDMSCaptureDate.Value = notDMSCaptureDate;
            myCommand.Parameters.Add(parameterNotDMSCaptureDate);

            SqlParameter parameterNSTypeCode = new SqlParameter("@NSTCode", SqlDbType.NVarChar, 5);
            parameterNSTypeCode.Value = nstCode;
            myCommand.Parameters.Add(parameterNSTypeCode);

            //Heidi 2013-05-20 add
            SqlParameter parameterLoSuIntNo = new SqlParameter("@LoSuIntNo", SqlDbType.Int);
            parameterLoSuIntNo.Value = loSuIntNo;
            myCommand.Parameters.Add(parameterLoSuIntNo);

            //SqlParameter parameterPayDate = new SqlParameter("@NotPaymentDate", SqlDbType.Bit);
            //parameterPayDate.Value = payDate;
            //myCommand.Parameters.Add(parameterPayDate);

            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int);
            parameterNotIntNo.Value = 0;
            parameterNotIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNotIntNo);

            SqlParameter parameterNotScanDate = new SqlParameter("@NotScanDate", SqlDbType.DateTime);
            parameterNotScanDate.Value = (string.IsNullOrEmpty(scanDate) ? null : scanDate);
            myCommand.Parameters.Add(parameterNotScanDate);

            #endregion

            #region Charge
            SqlParameter parameterChgOffenceCode1 = new SqlParameter("@ChgOffenceCode1", SqlDbType.VarChar, 15);
            parameterChgOffenceCode1.Value = chgOffenceCode1;
            myCommand.Parameters.Add(parameterChgOffenceCode1);

            SqlParameter parameterChgOffenceDescr1 = new SqlParameter("@ChgOffenceDescr1", SqlDbType.VarChar);
            parameterChgOffenceDescr1.Value = chgOffenceDescr1;
            myCommand.Parameters.Add(parameterChgOffenceDescr1);

            SqlParameter parameterChgOffenceCode2 = new SqlParameter("@ChgOffenceCode2", SqlDbType.VarChar, 15);
            parameterChgOffenceCode2.Value = chgOffenceCode2;
            myCommand.Parameters.Add(parameterChgOffenceCode2);

            SqlParameter parameterChgOffenceDescr2 = new SqlParameter("@ChgOffenceDescr2", SqlDbType.VarChar);
            parameterChgOffenceDescr2.Value = chgOffenceDescr2;
            myCommand.Parameters.Add(parameterChgOffenceDescr2);

            SqlParameter parameterChgOffenceCode3 = new SqlParameter("@ChgOffenceCode3", SqlDbType.VarChar, 15);
            parameterChgOffenceCode3.Value = chgOffenceCode3;
            myCommand.Parameters.Add(parameterChgOffenceCode3);

            SqlParameter parameterChgOffenceDescr3 = new SqlParameter("@ChgOffenceDescr3", SqlDbType.VarChar);
            parameterChgOffenceDescr3.Value = chgOffenceDescr3;
            myCommand.Parameters.Add(parameterChgOffenceDescr3);

            SqlParameter parameterChgFineAmount1 = new SqlParameter("@ChgFineAmount1", SqlDbType.Real);
            parameterChgFineAmount1.Value = chgFineAmount1;
            myCommand.Parameters.Add(parameterChgFineAmount1);

            SqlParameter parameterChgFineAmount2 = new SqlParameter("@ChgFineAmount2", SqlDbType.Real);
            parameterChgFineAmount2.Value = chgFineAmount2;
            myCommand.Parameters.Add(parameterChgFineAmount2);

            SqlParameter parameterChgFineAmount3 = new SqlParameter("@ChgFineAmount3", SqlDbType.Real);
            parameterChgFineAmount3.Value = chgFineAmount3;
            myCommand.Parameters.Add(parameterChgFineAmount3);

            //Jake Added 20101108 AlternateCharge code 
            SqlParameter parameterChgAlternateCode1 = new SqlParameter("@ChgAlternateCode1", SqlDbType.VarChar, 20);
            parameterChgAlternateCode1.Value = chgAlternateCode1;
            myCommand.Parameters.Add(parameterChgAlternateCode1);

            SqlParameter parameterChgAlternateCode2 = new SqlParameter("@ChgAlternateCode2", SqlDbType.VarChar, 20);
            parameterChgAlternateCode2.Value = chgAlternateCode2;
            myCommand.Parameters.Add(parameterChgAlternateCode2);

            SqlParameter parameterChgAlternateCode3 = new SqlParameter("@ChgAlternateCode3", SqlDbType.VarChar, 20);
            parameterChgAlternateCode3.Value = chgAlternateCode3;
            myCommand.Parameters.Add(parameterChgAlternateCode3);

            SqlParameter parameterOffNoAOG1 = new SqlParameter("@OffNoAOG1", SqlDbType.Char, 1);
            parameterOffNoAOG1.Value = chgNoAog1;
            myCommand.Parameters.Add(parameterOffNoAOG1);

            SqlParameter parameterOffNoAOG2 = new SqlParameter("@OffNoAOG2", SqlDbType.Char, 1);
            parameterOffNoAOG2.Value = chgNoAog2;
            myCommand.Parameters.Add(parameterOffNoAOG2);

            SqlParameter parameterOffNoAOG3 = new SqlParameter("@OffNoAOG3", SqlDbType.Char, 1);
            parameterOffNoAOG3.Value = chgNoAog3;
            myCommand.Parameters.Add(parameterOffNoAOG3);

            //SqlParameter parameterChgPaidDate = new SqlParameter("@ChgPaidDate", SqlDbType.DateTime);
            //if (chgPaidDate == null)
            //{
            //    parameterChgPaidDate.Value = DBNull.Value;
            //}
            //else
            //{
            //    parameterChgPaidDate.Value = chgPaidDate;
            //}
            //myCommand.Parameters.Add(parameterChgPaidDate);

            SqlParameter parameterNotPaymentDate = new SqlParameter("@NotPaymentDate", SqlDbType.DateTime);
            if (notPaymentDate == null)
            {
                parameterNotPaymentDate.Value = DBNull.Value;
            }
            else
            {
                parameterNotPaymentDate.Value = notPaymentDate;
            }
            myCommand.Parameters.Add(parameterNotPaymentDate);

            SqlParameter parameterChgOfficerNatisNo = new SqlParameter("@ChgOfficerNatisNo", SqlDbType.VarChar, 10);
            parameterChgOfficerNatisNo.Value = chgOfficerNatisNo;
            myCommand.Parameters.Add(parameterChgOfficerNatisNo);

            SqlParameter parameterChgOfficerName = new SqlParameter("@ChgOfficerName", SqlDbType.VarChar, 40);
            parameterChgOfficerName.Value = chgOfficerName;
            myCommand.Parameters.Add(parameterChgOfficerName);

            SqlParameter parameterConCode = new SqlParameter("@ConCode", SqlDbType.VarChar, 10);
            parameterConCode.Value = conCode;
            myCommand.Parameters.Add(parameterConCode);



            #endregion

            #region Summons
            SqlParameter parameterSumCourtDate = new SqlParameter("@SumCourtDate", SqlDbType.DateTime);
            if (sumCourtDate == null)
            {
                parameterSumCourtDate.Value = DBNull.Value;
            }
            else
            {
                parameterSumCourtDate.Value = sumCourtDate;
            }
            myCommand.Parameters.Add(parameterSumCourtDate);

            SqlParameter parameterSumOfficerNo = new SqlParameter("@SumOfficerNo", SqlDbType.VarChar, 10);
            parameterSumOfficerNo.Value = sumOfficerNo;
            myCommand.Parameters.Add(parameterSumOfficerNo);

            SqlParameter parameterSumCaseNo = new SqlParameter("@SumCaseNo", SqlDbType.VarChar, 50);
            parameterSumCaseNo.Value = sumCaseNo;
            myCommand.Parameters.Add(parameterSumCaseNo);

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtNo", SqlDbType.VarChar, 20);
            parameterCrtIntNo.Value = crtNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            SqlParameter parameterCrtRIntNo = new SqlParameter("@CrtRoomNo", SqlDbType.VarChar);
            parameterCrtRIntNo.Value = crtRoomNo;
            myCommand.Parameters.Add(parameterCrtRIntNo);

            //SqlParameter parameterSummonsFilePath = new SqlParameter("@SummonsFilePath", SqlDbType.VarChar, 255);
            //parameterSummonsFilePath.Value = summonsFilePath;
            //myCommand.Parameters.Add(parameterSummonsFilePath);

            SqlParameter parameterIfsIntNo = new SqlParameter("@IfsIntNo", SqlDbType.Int);
            parameterIfsIntNo.Value = ifsIntNo;
            myCommand.Parameters.Add(parameterIfsIntNo);
            #endregion

            #region Accused
            SqlParameter parameterAccSurname = new SqlParameter("@AccSurname", SqlDbType.VarChar, 100);
            parameterAccSurname.Value = accSurname;
            myCommand.Parameters.Add(parameterAccSurname);

            SqlParameter parameterAccForenames = new SqlParameter("@AccForenames", SqlDbType.VarChar, 100);
            parameterAccForenames.Value = accForenames;
            myCommand.Parameters.Add(parameterAccForenames);

            SqlParameter parameterAccIdType = new SqlParameter("@AccIdType", SqlDbType.VarChar, 3);
            parameterAccIdType.Value = accIdType;
            myCommand.Parameters.Add(parameterAccIdType);

            SqlParameter parameterAccIdNumber = new SqlParameter("@AccIdNumber", SqlDbType.VarChar, 25);
            parameterAccIdNumber.Value = accIdNumber;
            myCommand.Parameters.Add(parameterAccIdNumber);

            SqlParameter parameterAccOccupation = new SqlParameter("@AccOccupation", SqlDbType.VarChar, 25);
            parameterAccOccupation.Value = accOccupation;
            myCommand.Parameters.Add(parameterAccOccupation);


            SqlParameter parameterAccAge = new SqlParameter("@AccAge", SqlDbType.VarChar, 25);
            parameterAccAge.Value = accAge;
            myCommand.Parameters.Add(parameterAccAge);

            SqlParameter parameterAccSex = new SqlParameter("@AccSex", SqlDbType.VarChar, 25);
            parameterAccSex.Value = accSex;
            myCommand.Parameters.Add(parameterAccSex);

            SqlParameter parameterAccStAdd1 = new SqlParameter("@AccStAdd1", SqlDbType.VarChar, 25);
            parameterAccStAdd1.Value = accStAdd1;
            myCommand.Parameters.Add(parameterAccStAdd1);

            SqlParameter parameterAccStAdd2 = new SqlParameter("@AccStAdd2", SqlDbType.VarChar, 25);
            parameterAccStAdd2.Value = accStAdd2;
            myCommand.Parameters.Add(parameterAccStAdd2);

            //SqlParameter parameterAccStAdd3 = new SqlParameter("@AccStAdd3", SqlDbType.VarChar, 25);
            //parameterAccStAdd3.Value = accStAdd3;
            //myCommand.Parameters.Add(parameterAccStAdd3);

            SqlParameter parameterAccPoCode = new SqlParameter("@AccPoCode", SqlDbType.VarChar, 25);
            parameterAccPoCode.Value = accPoCode;
            myCommand.Parameters.Add(parameterAccPoCode);

            SqlParameter parameterAccTelHome = new SqlParameter("@AccTelHome", SqlDbType.VarChar, 15);
            parameterAccTelHome.Value = accTelHome;
            myCommand.Parameters.Add(parameterAccTelHome);

            SqlParameter parameterAccPoAdd1 = new SqlParameter("@AccPoAdd1", SqlDbType.VarChar, 25);
            parameterAccPoAdd1.Value = accPoAdd1;
            myCommand.Parameters.Add(parameterAccPoAdd1);

            SqlParameter parameterAccStCode = new SqlParameter("@AccStCode", SqlDbType.VarChar, 25);
            parameterAccStCode.Value = accStCode;
            myCommand.Parameters.Add(parameterAccStCode);

            SqlParameter parameterAccTelWork = new SqlParameter("@AccTelWork", SqlDbType.VarChar, 15);
            parameterAccTelWork.Value = accTelWork;
            myCommand.Parameters.Add(parameterAccTelWork);


            #endregion

            #region AartoDocument and AartoDocumentImage
            SqlParameter parameterDocNo = new SqlParameter("@DocNo", SqlDbType.VarChar, 20);
            parameterDocNo.Value = docNo;
            myCommand.Parameters.Add(parameterDocNo);

            SqlParameter parameterAaDocImgOrder = new SqlParameter("@AaDocImgOrder", SqlDbType.Int);
            parameterAaDocImgOrder.Value = aaDocImgOrder;
            myCommand.Parameters.Add(parameterAaDocImgOrder);

            SqlParameter parameterImageFilePath = new SqlParameter("@ImageFilePath", SqlDbType.VarChar, 100);
            parameterImageFilePath.Value = imageFilePath;
            myCommand.Parameters.Add(parameterImageFilePath);

            #endregion



            //------ 2010-09-26 Jake added some new fields

            SqlParameter parameterNationality = new SqlParameter("@Nationality", SqlDbType.VarChar, 3);
            parameterNationality.Value = nationality;
            myCommand.Parameters.Add(parameterNationality);

            //Jerry 2014-08-06 change it from 10 length to 50, it save the licence code
            SqlParameter parameterDrvLicence = new SqlParameter("@DrvLicence", SqlDbType.NVarChar, 50);
            parameterDrvLicence.Value = drvLicence;
            myCommand.Parameters.Add(parameterDrvLicence);

            SqlParameter parameterAccPoAdd2 = new SqlParameter("@AccPoAdd2", SqlDbType.VarChar, 100);
            parameterAccPoAdd2.Value = accPoAdd2;
            myCommand.Parameters.Add(parameterAccPoAdd2);

            SqlParameter parameterAGNo = new SqlParameter("@AGNo", SqlDbType.VarChar, 20);
            parameterAGNo.Value = agNo;
            myCommand.Parameters.Add(parameterAGNo);

            SqlParameter parameterOverAge = new SqlParameter("@OverAge", SqlDbType.Bit);
            parameterOverAge.Value = overAge;
            myCommand.Parameters.Add(parameterOverAge);


            SqlParameter parameterAccPoSuburb = new SqlParameter("@AccPoSuburb", SqlDbType.VarChar, 100);
            parameterAccPoSuburb.Value = accPoSuburb;
            myCommand.Parameters.Add(parameterAccPoSuburb);

            SqlParameter parameterACCPoTownOrCity = new SqlParameter("@AccPoTownOrCity", SqlDbType.VarChar, 100);
            parameterACCPoTownOrCity.Value = accPoTownOrCity;
            myCommand.Parameters.Add(parameterACCPoTownOrCity);

            SqlParameter parameterAccStSuburb = new SqlParameter("@AccStSuburb", SqlDbType.VarChar, 100);
            parameterAccStSuburb.Value = accStSuburb;
            myCommand.Parameters.Add(parameterAccStSuburb);

            SqlParameter parameterACCStTownOrCity = new SqlParameter("@AccStTownOrCity", SqlDbType.VarChar, 100);
            parameterACCStTownOrCity.Value = accStTownOrCity;
            myCommand.Parameters.Add(parameterACCStTownOrCity);

            //---------------------------

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterPsttName = new SqlParameter("@PSTTName", SqlDbType.VarChar, 50);
            parameterPsttName.Value = psttName;
            myCommand.Parameters.Add(parameterPsttName);

            //jerry 2011-11-03 add code to get OGIntNo
            CourtService courtService = new CourtService();
            Court court = null;
            court = courtService.GetByCrtNo(crtNo);
            if (court == null)
            {
                court = courtService.GetAll().FirstOrDefault();
            }

            int ogIntNo;
            CourtRulesDetails courtRulesDetails = new CourtRulesDetails();
            courtRulesDetails.CrtIntNo = court.CrtIntNo;
            courtRulesDetails.CRCode = "3010";
            courtRulesDetails.LastUser = lastUser;
            DefaultCourtRules courtRule = new DefaultCourtRules(courtRulesDetails, mConstr);
            KeyValuePair<int, string> rule3010 = courtRule.SetDefaultCourtRule();
            string defaultRule3010 = rule3010.Value;
            if (defaultRule3010 == "N")
            {
                ogIntNo = (int)OriginGroupList.ALL;
            }
            else
            {
                courtRulesDetails = new CourtRulesDetails();
                courtRulesDetails.CrtIntNo = court.CrtIntNo;
                courtRulesDetails.CRCode = "3020";
                courtRulesDetails.LastUser = lastUser;
                courtRule = new DefaultCourtRules(courtRulesDetails, mConstr);
                KeyValuePair<int, string> rule3020 = courtRule.SetDefaultCourtRule();
                string defaultRule3020 = rule3020.Value;
                if (defaultRule3020 == "N")// is HWO or CAM
                {
                    ogIntNo = (int)OriginGroupList.HWO;

                }
                else
                {
                    ogIntNo = (int)OriginGroupList.S56;

                }
            }
            SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int);
            parameterOGIntNo.Value = ogIntNo;
            myCommand.Parameters.Add(parameterOGIntNo);

            // Oscar 2013-05-27 added
            var allowNoAOGOffence = new AuthorityRulesDB(mConstr).GetAuthorityRule(autIntNo, "4537").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase);
            myCommand.Parameters.AddWithValue("@AllowNoAOGOffence", allowNoAOGOffence);

            //Jerry 2014-07-31 add
            SqlParameter parameterAccTelCell = new SqlParameter("@AccTelCell", SqlDbType.VarChar, 15);
            parameterAccTelCell.Value = accTelCell;
            myCommand.Parameters.Add(parameterAccTelCell);

            SqlParameter parameterPoliceStation = new SqlParameter("@PoliceStation", SqlDbType.VarChar, 50);
            parameterPoliceStation.Value = policeStation;
            myCommand.Parameters.Add(parameterPoliceStation);

            SqlParameter parameterDistrict = new SqlParameter("@District", SqlDbType.VarChar, 50);
            parameterDistrict.Value = district;
            myCommand.Parameters.Add(parameterDistrict);

            SqlParameter parameterLicenceNumber = new SqlParameter("@LicenceNumber", SqlDbType.NVarChar, 25);
            parameterLicenceNumber.Value = licenceNumber;
            myCommand.Parameters.Add(parameterLicenceNumber);

            SqlParameter parameterLicenceTypeName = new SqlParameter("@LicenceTypeName", SqlDbType.NVarChar, 50);
            parameterLicenceTypeName.Value = licenceTypeName;
            myCommand.Parameters.Add(parameterLicenceTypeName);


            //SqlTransaction tran = null;
            try
            {
                myConnection.Open();

                //jake 2010-11-12 moved sqltransaction
                //tran = myConnection.BeginTransaction();
                //myCommand.Transaction = tran;
                object returnValue = myCommand.ExecuteScalar();
                if (returnValue == null) returnValue = 0;

                //int notIntNo = Convert.ToInt32(myCommand.Parameters["@NotIntNo"].Value.ToString().Trim());
                int notIntNo = Convert.ToInt32(returnValue);
                //Jake 2010-11-12 Add officer error has been moved into storedprocedure

                return notIntNo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                myConnection.Dispose();
            }
        }

        #endregion


        //Heidi 2013-05-20 added loSuIntNo
        #region ImportHandwrittenOffencesProvincialSection56

        public int ImportHandwrittenOffencesProvincialSection56(int autIntNo, string notFilmNo, string notFrameNo, string notTicketNo,
            int speedLimit, int speedReading, DateTime offenceDate, DateTime offenceDate2, DateTime offenceDate3, string notLocDescr, 
            string notRegNo, string regNo2, string regNo3, 
            string notVehicleMakeCode, string notVehicleMake, string notVehicleTypeCode, string notVehicleType,
            string notOfficerNo, string officerNo2, string officerNo3, string notOfficerSname, string notOfficerGroup,//17
            string notDMSCaptureClerk, DateTime? notDMSCaptureDate,
            string chgOffenceCode1, string chgOffenceDescr1,
            string chgOffenceCode2, string chgOffenceDescr2,
            string chgOffenceCode3, string chgOffenceDescr3,
            float chgFineAmount1, float chgFineAmount2, float chgFineAmount3, DateTime? notPaymentDate,
            string chgOfficerNatisNo, string chgOfficerName, string conCode,

            DateTime? sumCourtDate, string sumOfficerNo, string sumCaseNo,
            string crtNo, string crtRoomNo, int ifsIntNo,//7

            string accSurname, string accForenames, string accIdNumber, string accOccupation, string accAge, string accSex,
            string accPoAdd1, string accPoCode, string accTelHome, string accStAdd1, string accStAdd2, string accStAdd3, string accStCode,
            string accTelWork, bool isOfficerError,
            string docNo, int aaDocImgOrder, string imageFilePath, string lastUser, string officerErrorString,
            string nationality, string drvLicence, string accPoAdd2, string accPoAdd3, string agNo, string accPoSuburb, string accPoTownOrCity,
            string accStSuburb, string accStTownOrCity, bool overAge,
            string chgAlternateCode1, string chgAlternateCode2, string chgAlternateCode3,
            string chgNoAog1, string chgNoAog2, string chgNoAog3, string pTCCode, string offenceStatutoryRef1, string offenceStatutoryRef2, string offenceStatutoryRef3, string nstCode, int loSuIntNo,string scanDate)//12
        {
            SqlConnection myConnection = new SqlConnection(mConstr);

            SqlCommand myCommand = new SqlCommand("ImportHandwrittenOffencesProvincialSection56", myConnection);

            myCommand.CommandType = CommandType.StoredProcedure;

            #region Notice
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterNotFilmNo = new SqlParameter("@NotFilmNo", SqlDbType.VarChar, 20);
            parameterNotFilmNo.Value = notFilmNo;
            myCommand.Parameters.Add(parameterNotFilmNo);

            SqlParameter parameterNotFrameNo = new SqlParameter("@NotFrameNo", SqlDbType.VarChar, 20);
            parameterNotFrameNo.Value = notFrameNo;
            myCommand.Parameters.Add(parameterNotFrameNo);

            SqlParameter parameterNotTicketNo = new SqlParameter("@NotTicketNo", SqlDbType.VarChar, 50);
            parameterNotTicketNo.Value = notTicketNo;
            myCommand.Parameters.Add(parameterNotTicketNo);

            SqlParameter parameterSpeedLimit = new SqlParameter("@SpeedLimit", SqlDbType.Int);
            parameterSpeedLimit.Value = speedLimit;
            myCommand.Parameters.Add(parameterSpeedLimit);

            SqlParameter parameterSpeedReading = new SqlParameter("@SpeedReading", SqlDbType.Int);
            parameterSpeedReading.Value = speedReading;
            myCommand.Parameters.Add(parameterSpeedReading);

            SqlParameter parameterOffenceDate = new SqlParameter("@OffenceDate", SqlDbType.DateTime);
            parameterOffenceDate.Value = offenceDate;
            myCommand.Parameters.Add(parameterOffenceDate);

            SqlParameter parameterOffenceDate2 = new SqlParameter("@OffenceDate2", SqlDbType.DateTime);
            parameterOffenceDate2.Value = offenceDate2;
            myCommand.Parameters.Add(parameterOffenceDate2);

            SqlParameter parameterOffenceDate3 = new SqlParameter("@OffenceDate3", SqlDbType.DateTime);
            parameterOffenceDate3.Value = offenceDate3;
            myCommand.Parameters.Add(parameterOffenceDate3);

            //SqlParameter parameterNotLocCode = new SqlParameter("@NotLocCode", SqlDbType.VarChar, 50);
            //parameterNotLocCode.Value = notLocCode;
            //myCommand.Parameters.Add(parameterNotLocCode);

            SqlParameter parameterNotLocDescr = new SqlParameter("@NotLocDescr", SqlDbType.VarChar, 150);
            if (String.IsNullOrEmpty(notLocDescr)) notLocDescr = "";
            else
            {
                if (notLocDescr.Length > 150) notLocDescr = notLocDescr.Substring(0, 150);
            }
            parameterNotLocDescr.Value = notLocDescr;
            myCommand.Parameters.Add(parameterNotLocDescr);

            SqlParameter parameterNotRegNo = new SqlParameter("@NotRegNo", SqlDbType.VarChar, 10);
            parameterNotRegNo.Value = notRegNo;
            myCommand.Parameters.Add(parameterNotRegNo);

            SqlParameter parameterRegNo2 = new SqlParameter("@RegNo2", SqlDbType.VarChar, 10);
            parameterRegNo2.Value = regNo2;
            myCommand.Parameters.Add(parameterRegNo2);

            SqlParameter parameterRegNo3 = new SqlParameter("@RegNo3", SqlDbType.VarChar, 10);
            parameterRegNo3.Value = regNo3;
            myCommand.Parameters.Add(parameterRegNo3);

            SqlParameter parameterNotVehicleMakeCode = new SqlParameter("@NotVehicleMakeCode", SqlDbType.VarChar, 3);
            parameterNotVehicleMakeCode.Value = notVehicleMakeCode;
            myCommand.Parameters.Add(parameterNotVehicleMakeCode);

            SqlParameter parameterNotVehicleMake = new SqlParameter("@NotVehicleMake", SqlDbType.VarChar, 30);
            parameterNotVehicleMake.Value = notVehicleMake;
            myCommand.Parameters.Add(parameterNotVehicleMake);

            SqlParameter parameterNotVehicleTypeCode = new SqlParameter("@NotVehicleTypeCode", SqlDbType.VarChar, 3);
            parameterNotVehicleTypeCode.Value = notVehicleTypeCode;
            myCommand.Parameters.Add(parameterNotVehicleTypeCode);

            SqlParameter parameterNotVehicleType = new SqlParameter("@NotVehicleType", SqlDbType.VarChar, 30);
            parameterNotVehicleType.Value = notVehicleType;
            myCommand.Parameters.Add(parameterNotVehicleType);

            SqlParameter parameterNotOfficerNo = new SqlParameter("@NotOfficerNo", SqlDbType.VarChar, 10);
            parameterNotOfficerNo.Value = notOfficerNo;
            myCommand.Parameters.Add(parameterNotOfficerNo);

            SqlParameter parameterNotOfficerNo2 = new SqlParameter("@OfficerNo2", SqlDbType.VarChar, 10);
            parameterNotOfficerNo2.Value = officerNo2;
            myCommand.Parameters.Add(parameterNotOfficerNo2);

            SqlParameter parameterNotOfficerNo3 = new SqlParameter("@OfficerNo3", SqlDbType.VarChar, 10);
            parameterNotOfficerNo3.Value = officerNo3;
            myCommand.Parameters.Add(parameterNotOfficerNo3);

            SqlParameter parameterNotOfficerSname = new SqlParameter("@NotOfficerSname", SqlDbType.VarChar, 35);
            parameterNotOfficerSname.Value = notOfficerSname;
            myCommand.Parameters.Add(parameterNotOfficerSname);

            SqlParameter parameterNotOfficerGroup = new SqlParameter("@NotOfficerGroup", SqlDbType.VarChar, 50);
            parameterNotOfficerGroup.Value = notOfficerGroup;
            myCommand.Parameters.Add(parameterNotOfficerGroup);

            SqlParameter parameterIsOfficerError = new SqlParameter("@IsOfficerError", SqlDbType.Bit);
            parameterIsOfficerError.Value = isOfficerError;
            myCommand.Parameters.Add(parameterIsOfficerError);

            SqlParameter parameterOfficerErrorStr = new SqlParameter("@OfficerErrorStr", SqlDbType.VarChar, 500);
            parameterOfficerErrorStr.Value = officerErrorString;
            myCommand.Parameters.Add(parameterOfficerErrorStr);

            SqlParameter parameterNotDMSCaptureClerk = new SqlParameter("@NotDMSCaptureCLerk", SqlDbType.VarChar, 25);
            parameterNotDMSCaptureClerk.Value = notDMSCaptureClerk;
            myCommand.Parameters.Add(parameterNotDMSCaptureClerk);

            SqlParameter parameterNotDMSCaptureDate = new SqlParameter("@NotDMSCaptureDate", SqlDbType.SmallDateTime);
            parameterNotDMSCaptureDate.Value = notDMSCaptureDate;
            myCommand.Parameters.Add(parameterNotDMSCaptureDate);

            SqlParameter parameterNSTypeCode = new SqlParameter("@NSTCode", SqlDbType.NVarChar, 5);
            parameterNSTypeCode.Value = nstCode;
            myCommand.Parameters.Add(parameterNSTypeCode);

            //Heidi 2013-05-20 add
            SqlParameter parameterLoSuIntNo = new SqlParameter("@LoSuIntNo", SqlDbType.Int);
            parameterLoSuIntNo.Value = loSuIntNo;
            myCommand.Parameters.Add(parameterLoSuIntNo);

            //SqlParameter parameterPayDate = new SqlParameter("@NotPaymentDate", SqlDbType.Bit);
            //parameterPayDate.Value = payDate;
            //myCommand.Parameters.Add(parameterPayDate);

            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int);
            parameterNotIntNo.Value = 0;
            parameterNotIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNotIntNo);

            SqlParameter parameterNotScanDate = new SqlParameter("@NotScanDate", SqlDbType.DateTime);
            parameterNotScanDate.Value = (string.IsNullOrEmpty(scanDate) ? null : scanDate);
            myCommand.Parameters.Add(parameterNotScanDate);

            #endregion

            #region Charge
            SqlParameter parameterChgOffenceCode1 = new SqlParameter("@ChgOffenceCode1", SqlDbType.VarChar, 15);
            parameterChgOffenceCode1.Value = chgOffenceCode1;
            myCommand.Parameters.Add(parameterChgOffenceCode1);

            SqlParameter parameterChgOffenceDescr1 = new SqlParameter("@ChgOffenceDescr1", SqlDbType.VarChar);
            parameterChgOffenceDescr1.Value = chgOffenceDescr1;
            myCommand.Parameters.Add(parameterChgOffenceDescr1);

            SqlParameter parameterChgOffenceCode2 = new SqlParameter("@ChgOffenceCode2", SqlDbType.VarChar, 15);
            parameterChgOffenceCode2.Value = chgOffenceCode2;
            myCommand.Parameters.Add(parameterChgOffenceCode2);

            SqlParameter parameterChgOffenceDescr2 = new SqlParameter("@ChgOffenceDescr2", SqlDbType.VarChar);
            parameterChgOffenceDescr2.Value = chgOffenceDescr2;
            myCommand.Parameters.Add(parameterChgOffenceDescr2);

            SqlParameter parameterChgOffenceCode3 = new SqlParameter("@ChgOffenceCode3", SqlDbType.VarChar, 15);
            parameterChgOffenceCode3.Value = chgOffenceCode3;
            myCommand.Parameters.Add(parameterChgOffenceCode3);

            SqlParameter parameterChgOffenceDescr3 = new SqlParameter("@ChgOffenceDescr3", SqlDbType.VarChar);
            parameterChgOffenceDescr3.Value = chgOffenceDescr3;
            myCommand.Parameters.Add(parameterChgOffenceDescr3);

            SqlParameter parameterChgFineAmount1 = new SqlParameter("@ChgFineAmount1", SqlDbType.Real);
            parameterChgFineAmount1.Value = chgFineAmount1;
            myCommand.Parameters.Add(parameterChgFineAmount1);

            SqlParameter parameterChgFineAmount2 = new SqlParameter("@ChgFineAmount2", SqlDbType.Real);
            parameterChgFineAmount2.Value = chgFineAmount2;
            myCommand.Parameters.Add(parameterChgFineAmount2);

            SqlParameter parameterChgFineAmount3 = new SqlParameter("@ChgFineAmount3", SqlDbType.Real);
            parameterChgFineAmount3.Value = chgFineAmount3;
            myCommand.Parameters.Add(parameterChgFineAmount3);

            //Jake Added 20101108 AlternateCharge code 
            SqlParameter parameterChgAlternateCode1 = new SqlParameter("@ChgAlternateCode1", SqlDbType.VarChar, 20);
            parameterChgAlternateCode1.Value = chgAlternateCode1;
            myCommand.Parameters.Add(parameterChgAlternateCode1);

            SqlParameter parameterChgAlternateCode2 = new SqlParameter("@ChgAlternateCode2", SqlDbType.VarChar, 20);
            parameterChgAlternateCode2.Value = chgAlternateCode2;
            myCommand.Parameters.Add(parameterChgAlternateCode2);

            SqlParameter parameterChgAlternateCode3 = new SqlParameter("@ChgAlternateCode3", SqlDbType.VarChar, 20);
            parameterChgAlternateCode3.Value = chgAlternateCode3;
            myCommand.Parameters.Add(parameterChgAlternateCode3);

            SqlParameter parameterOffNoAOG1 = new SqlParameter("@OffNoAOG1", SqlDbType.Char, 1);
            parameterOffNoAOG1.Value = chgNoAog1;
            myCommand.Parameters.Add(parameterOffNoAOG1);

            SqlParameter parameterOffNoAOG2 = new SqlParameter("@OffNoAOG2", SqlDbType.Char, 1);
            parameterOffNoAOG2.Value = chgNoAog2;
            myCommand.Parameters.Add(parameterOffNoAOG2);

            SqlParameter parameterOffNoAOG3 = new SqlParameter("@OffNoAOG3", SqlDbType.Char, 1);
            parameterOffNoAOG3.Value = chgNoAog3;
            myCommand.Parameters.Add(parameterOffNoAOG3);

            //SqlParameter parameterChgPaidDate = new SqlParameter("@ChgPaidDate", SqlDbType.DateTime);
            //if (chgPaidDate == null)
            //{
            //    parameterChgPaidDate.Value = DBNull.Value;
            //}
            //else
            //{
            //    parameterChgPaidDate.Value = chgPaidDate;
            //}
            //myCommand.Parameters.Add(parameterChgPaidDate);

            SqlParameter parameterNotPaymentDate = new SqlParameter("@NotPaymentDate", SqlDbType.DateTime);
            if (notPaymentDate == null)
            {
                parameterNotPaymentDate.Value = DBNull.Value;
            }
            else
            {
                parameterNotPaymentDate.Value = notPaymentDate;
            }
            myCommand.Parameters.Add(parameterNotPaymentDate);

            SqlParameter parameterChgOfficerNatisNo = new SqlParameter("@ChgOfficerNatisNo", SqlDbType.VarChar, 10);
            parameterChgOfficerNatisNo.Value = chgOfficerNatisNo;
            myCommand.Parameters.Add(parameterChgOfficerNatisNo);

            SqlParameter parameterChgOfficerName = new SqlParameter("@ChgOfficerName", SqlDbType.VarChar, 40);
            parameterChgOfficerName.Value = chgOfficerName;
            myCommand.Parameters.Add(parameterChgOfficerName);

            SqlParameter parameterConCode = new SqlParameter("@ConCode", SqlDbType.VarChar, 10);
            parameterConCode.Value = conCode;
            myCommand.Parameters.Add(parameterConCode);



            #endregion

            #region Summons
            SqlParameter parameterSumCourtDate = new SqlParameter("@SumCourtDate", SqlDbType.DateTime);
            if (sumCourtDate == null)
            {
                parameterSumCourtDate.Value = DBNull.Value;
            }
            else
            {
                parameterSumCourtDate.Value = sumCourtDate;
            }
            myCommand.Parameters.Add(parameterSumCourtDate);

            SqlParameter parameterSumOfficerNo = new SqlParameter("@SumOfficerNo", SqlDbType.VarChar, 10);
            parameterSumOfficerNo.Value = sumOfficerNo;
            myCommand.Parameters.Add(parameterSumOfficerNo);

            SqlParameter parameterSumCaseNo = new SqlParameter("@SumCaseNo", SqlDbType.VarChar, 50);
            parameterSumCaseNo.Value = sumCaseNo;
            myCommand.Parameters.Add(parameterSumCaseNo);

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtNo", SqlDbType.VarChar, 20);
            parameterCrtIntNo.Value = crtNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            SqlParameter parameterCrtRIntNo = new SqlParameter("@CrtRoomNo", SqlDbType.VarChar);
            parameterCrtRIntNo.Value = crtRoomNo;
            myCommand.Parameters.Add(parameterCrtRIntNo);

            //SqlParameter parameterSummonsFilePath = new SqlParameter("@SummonsFilePath", SqlDbType.VarChar, 255);
            //parameterSummonsFilePath.Value = summonsFilePath;
            //myCommand.Parameters.Add(parameterSummonsFilePath);

            SqlParameter parameterIfsIntNo = new SqlParameter("@IfsIntNo", SqlDbType.Int);
            parameterIfsIntNo.Value = ifsIntNo;
            myCommand.Parameters.Add(parameterIfsIntNo);
            #endregion

            #region Accused
            SqlParameter parameterAccSurname = new SqlParameter("@AccSurname", SqlDbType.VarChar, 100);
            parameterAccSurname.Value = accSurname;
            myCommand.Parameters.Add(parameterAccSurname);

            SqlParameter parameterAccForenames = new SqlParameter("@AccForenames", SqlDbType.VarChar, 100);
            parameterAccForenames.Value = accForenames;
            myCommand.Parameters.Add(parameterAccForenames);

            SqlParameter parameterAccIdNumber = new SqlParameter("@AccIdNumber", SqlDbType.VarChar, 25);
            parameterAccIdNumber.Value = accIdNumber;
            myCommand.Parameters.Add(parameterAccIdNumber);

            SqlParameter parameterAccOccupation = new SqlParameter("@AccOccupation", SqlDbType.VarChar, 25);
            parameterAccOccupation.Value = accOccupation;
            myCommand.Parameters.Add(parameterAccOccupation);


            SqlParameter parameterAccAge = new SqlParameter("@AccAge", SqlDbType.VarChar, 25);
            parameterAccAge.Value = accAge;
            myCommand.Parameters.Add(parameterAccAge);

            SqlParameter parameterAccSex = new SqlParameter("@AccSex", SqlDbType.VarChar, 25);
            parameterAccSex.Value = accSex;
            myCommand.Parameters.Add(parameterAccSex);

            SqlParameter parameterAccStAdd1 = new SqlParameter("@AccStAdd1", SqlDbType.VarChar, 25);
            parameterAccStAdd1.Value = accStAdd1;
            myCommand.Parameters.Add(parameterAccStAdd1);

            SqlParameter parameterAccStAdd2 = new SqlParameter("@AccStAdd2", SqlDbType.VarChar, 25);
            parameterAccStAdd2.Value = accStAdd2;
            myCommand.Parameters.Add(parameterAccStAdd2);

            SqlParameter parameterAccStAdd3 = new SqlParameter("@AccStAdd3", SqlDbType.VarChar, 25);
            parameterAccStAdd3.Value = accStAdd3;
            myCommand.Parameters.Add(parameterAccStAdd3);

            SqlParameter parameterAccPoCode = new SqlParameter("@AccPoCode", SqlDbType.VarChar, 25);
            parameterAccPoCode.Value = accPoCode;
            myCommand.Parameters.Add(parameterAccPoCode);

            SqlParameter parameterAccTelHome = new SqlParameter("@AccTelHome", SqlDbType.VarChar, 15);
            parameterAccTelHome.Value = accTelHome;
            myCommand.Parameters.Add(parameterAccTelHome);

            SqlParameter parameterAccPoAdd1 = new SqlParameter("@AccPoAdd1", SqlDbType.VarChar, 25);
            parameterAccPoAdd1.Value = accPoAdd1;
            myCommand.Parameters.Add(parameterAccPoAdd1);

            SqlParameter parameterAccStCode = new SqlParameter("@AccStCode", SqlDbType.VarChar, 25);
            parameterAccStCode.Value = accStCode;
            myCommand.Parameters.Add(parameterAccStCode);

            SqlParameter parameterAccTelWork = new SqlParameter("@AccTelWork", SqlDbType.VarChar, 15);
            parameterAccTelWork.Value = accTelWork;
            myCommand.Parameters.Add(parameterAccTelWork);


            #endregion

            #region AartoDocument and AartoDocumentImage
            SqlParameter parameterDocNo = new SqlParameter("@DocNo", SqlDbType.VarChar, 20);
            parameterDocNo.Value = docNo;
            myCommand.Parameters.Add(parameterDocNo);

            SqlParameter parameterAaDocImgOrder = new SqlParameter("@AaDocImgOrder", SqlDbType.Int);
            parameterAaDocImgOrder.Value = aaDocImgOrder;
            myCommand.Parameters.Add(parameterAaDocImgOrder);

            SqlParameter parameterImageFilePath = new SqlParameter("@ImageFilePath", SqlDbType.VarChar, 100);
            parameterImageFilePath.Value = imageFilePath;
            myCommand.Parameters.Add(parameterImageFilePath);

            #endregion



            //------ 2010-09-26 Jake added some new fields

            SqlParameter parameterNationality = new SqlParameter("@Nationality", SqlDbType.VarChar, 3);
            parameterNationality.Value = nationality;
            myCommand.Parameters.Add(parameterNationality);

            SqlParameter parameterDrvLicence = new SqlParameter("@DrvLicence", SqlDbType.VarChar, 10);
            parameterDrvLicence.Value = drvLicence;
            myCommand.Parameters.Add(parameterDrvLicence);

            SqlParameter parameterAccPoAdd2 = new SqlParameter("@AccPoAdd2", SqlDbType.VarChar, 100);
            parameterAccPoAdd2.Value = accPoAdd2;
            myCommand.Parameters.Add(parameterAccPoAdd2);

            SqlParameter parameterAccPoAdd3 = new SqlParameter("@AccPoAdd3", SqlDbType.VarChar, 100);
            parameterAccPoAdd3.Value = accPoAdd3;
            myCommand.Parameters.Add(parameterAccPoAdd3);

            SqlParameter parameterAGNo = new SqlParameter("@AGNo", SqlDbType.VarChar, 20);
            parameterAGNo.Value = agNo;
            myCommand.Parameters.Add(parameterAGNo);

            SqlParameter parameterOverAge = new SqlParameter("@OverAge", SqlDbType.Bit);
            parameterOverAge.Value = overAge;
            myCommand.Parameters.Add(parameterOverAge);


            SqlParameter parameterAccPoSuburb = new SqlParameter("@AccPoSuburb", SqlDbType.VarChar, 100);
            parameterAccPoSuburb.Value = accPoSuburb;
            myCommand.Parameters.Add(parameterAccPoSuburb);

            SqlParameter parameterACCPoTownOrCity = new SqlParameter("@AccPoTownOrCity", SqlDbType.VarChar, 100);
            parameterACCPoTownOrCity.Value = accPoTownOrCity;
            myCommand.Parameters.Add(parameterACCPoTownOrCity);

            SqlParameter parameterAccStSuburb = new SqlParameter("@AccStSuburb", SqlDbType.VarChar, 100);
            parameterAccStSuburb.Value = accStSuburb;
            myCommand.Parameters.Add(parameterAccStSuburb);

            SqlParameter parameterACCStTownOrCity = new SqlParameter("@AccStTownOrCity", SqlDbType.VarChar, 100);
            parameterACCStTownOrCity.Value = accStTownOrCity;
            myCommand.Parameters.Add(parameterACCStTownOrCity);

            //---------------------------

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            //jerry 2011-11-03 add code to get OGIntNo
            CourtService courtService = new CourtService();
            Court court = null;
            court = courtService.GetByCrtNo(crtNo);
            if (court == null)
            {
                court = courtService.GetAll().FirstOrDefault();
            }

            int ogIntNo;
            CourtRulesDetails courtRulesDetails = new CourtRulesDetails();
            courtRulesDetails.CrtIntNo = court.CrtIntNo;
            courtRulesDetails.CRCode = "3010";
            courtRulesDetails.LastUser = lastUser;
            DefaultCourtRules courtRule = new DefaultCourtRules(courtRulesDetails, mConstr);
            KeyValuePair<int, string> rule3010 = courtRule.SetDefaultCourtRule();
            string defaultRule3010 = rule3010.Value;
            if (defaultRule3010 == "N")
            {
                ogIntNo = (int)OriginGroupList.ALL;
            }
            else
            {
                courtRulesDetails = new CourtRulesDetails();
                courtRulesDetails.CrtIntNo = court.CrtIntNo;
                courtRulesDetails.CRCode = "3020";
                courtRulesDetails.LastUser = lastUser;
                courtRule = new DefaultCourtRules(courtRulesDetails, mConstr);
                KeyValuePair<int, string> rule3020 = courtRule.SetDefaultCourtRule();
                string defaultRule3020 = rule3020.Value;
                if (defaultRule3020 == "N")// is HWO or CAM
                {
                    ogIntNo = (int)OriginGroupList.HWO;

                }
                else
                {
                    ogIntNo = (int)OriginGroupList.S56;

                }
            }
            SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int);
            parameterOGIntNo.Value = ogIntNo;
            myCommand.Parameters.Add(parameterOGIntNo);

            //20120906 Adam: Add the new column for provincial traffic center and other new columns
            SqlParameter paramPTCCode = new SqlParameter("@PTCCode", SqlDbType.NVarChar, 10);
            paramPTCCode.Value = pTCCode;
            myCommand.Parameters.Add(paramPTCCode);

            SqlParameter paramOffenceStatutoryRef1 = new SqlParameter("@OffenceStatutoryRef1", SqlDbType.VarChar);
            paramOffenceStatutoryRef1.Value = offenceStatutoryRef1;
            myCommand.Parameters.Add(paramOffenceStatutoryRef1);

            SqlParameter paramOffenceStatutoryRef2 = new SqlParameter("@OffenceStatutoryRef2", SqlDbType.VarChar);
            paramOffenceStatutoryRef2.Value = offenceStatutoryRef2;
            myCommand.Parameters.Add(paramOffenceStatutoryRef2);

            SqlParameter paramOffenceStatutoryRef3 = new SqlParameter("@OffenceStatutoryRef3", SqlDbType.VarChar);
            paramOffenceStatutoryRef3.Value = offenceStatutoryRef3;
            myCommand.Parameters.Add(paramOffenceStatutoryRef3);

            // Oscar 2013-05-27 added
            var allowNoAOGOffence = new AuthorityRulesDB(mConstr).GetAuthorityRule(autIntNo, "4537").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase);
            myCommand.Parameters.AddWithValue("@AllowNoAOGOffence", allowNoAOGOffence);

            //SqlTransaction tran = null;
            try
            {
                myConnection.Open();

                //jake 2010-11-12 moved sqltransaction
                //tran = myConnection.BeginTransaction();
                //myCommand.Transaction = tran;
                object returnValue = myCommand.ExecuteScalar();
                if (returnValue == null) returnValue = 0;

                //int notIntNo = Convert.ToInt32(myCommand.Parameters["@NotIntNo"].Value.ToString().Trim());
                int notIntNo = Convert.ToInt32(returnValue);
                //Jake 2010-11-12 Add officer error has been moved into storedprocedure

                return notIntNo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                myConnection.Dispose();
            }
        }

        #endregion

        //Jake 10-08-26 Added sqltransaction to commit
        // Jake 10 -12-28 added paramters for charge code 3
        //Jake 2010-12-30 added two fields for notice
        //Henry 2013-02-04 added notPaymentDate
        //Heidi 2013-05-20 added loSuIntNo
        //Jerry 2014-08-04 add otherNationality, licenceCodeName, licenceClassName, licenceTypeName, mvaNo, ccNumber
        #region ImportHandwrittenOffencesSection341

        public int ImportHandwrittenOffencesSection341(int autIntNo, string notTicketNo,
            string notCourtNo, DateTime offenceDate, string notLocDescr, string notRegNo, string notVehicleRegisterNo,
            string notVehicleMakeCode, string notVehicleMake, string notVehicleTypeCode, string notVehicleType, string notVehicleColour,
            DateTime? notVehicleLicenceExpiry, string notOfficerNo, string notOfficerSname, string notOfficerGroup,//17
            string notDMSCaptureClerk, DateTime? notDMSCaptureDate,
            string chgOffenceCode, string chgOffenceCode2, string chgOffenceCode3, string chgOffenceDescr, string chgOffenceDescr2, string chgOffenceDescr3, float chgFineAmount, float chgFineAmount2, float chgFineAmount3,

            string filmNo,
            string frameNo, int? ifsIntNo, bool isOfficerError,
             string docNo, int aaDocImgOrder, string imageFilePath, string lastUser, string conCode, string officerError,
            string chgAlternateCode1, string chgAlternateCode2, string chgAlternateCode3, string chgNoAog1, string chgNoAog2, string chgNoAog3,
            string sName, string fNamem, string idNum, string poAddr1, string poAddr2, string stAddr1, string stAddr2,
            string businessSuburb, string businessTownOrCity, string telNo1, string telNo2, string postalCode1, string postalCode2,
            string suburb, string townOrCity, string nstCode, string pstType, int loSuIntNo, string scanDate, DateTime notPaymentDate = default(DateTime), string aR_2500 = "N", string otherNationality="",
            string licenceCodeName = "", string licenceClassName = "", string licenceTypeName = "", string mvaNo = "", string ccNumber = "")
        {
            SqlConnection myConnection = new SqlConnection(mConstr);
            //SqlTransaction tran = myConnection.BeginTransaction();
            SqlCommand myCommand = new SqlCommand("ImportHandwrittenOffencesSection341", myConnection);

            myCommand.CommandType = CommandType.StoredProcedure;

            #region Notice
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterNotTicketNo = new SqlParameter("@NotTicketNo", SqlDbType.VarChar, 50);
            parameterNotTicketNo.Value = notTicketNo;
            myCommand.Parameters.Add(parameterNotTicketNo);

            SqlParameter parameterNotCourtNo = new SqlParameter("@NotCourtNo", SqlDbType.VarChar, 6);
            parameterNotCourtNo.Value = notCourtNo;
            myCommand.Parameters.Add(parameterNotCourtNo);

            //SqlParameter parameterNotCourtName = new SqlParameter("@NotCourtName", SqlDbType.VarChar, 30);
            //parameterNotCourtName.Value = notCourtName;
            //myCommand.Parameters.Add(parameterNotCourtName);

            SqlParameter parameterOffenceDate = new SqlParameter("@OffenceDate", SqlDbType.DateTime);
            parameterOffenceDate.Value = offenceDate;
            myCommand.Parameters.Add(parameterOffenceDate);

            //SqlParameter parameterNotLocCode = new SqlParameter("@NotLocCode", SqlDbType.VarChar, 50);
            //parameterNotLocCode.Value = notLocCode;
            //myCommand.Parameters.Add(parameterNotLocCode);

            SqlParameter parameterNotLocDescr = new SqlParameter("@NotLocDescr", SqlDbType.VarChar, 150);
            if (String.IsNullOrEmpty(notLocDescr)) notLocDescr = "";
            else
            {
                if (notLocDescr.Length > 150) notLocDescr = notLocDescr.Substring(0, 150);
            }
            parameterNotLocDescr.Value = notLocDescr;
            myCommand.Parameters.Add(parameterNotLocDescr);

            SqlParameter parameterNotRegNo = new SqlParameter("@NotRegNo", SqlDbType.VarChar, 10);
            parameterNotRegNo.Value = notRegNo;
            myCommand.Parameters.Add(parameterNotRegNo);

            SqlParameter parameterNotVehicleRegisterNo = new SqlParameter("@NotVehicleRegisterNo", SqlDbType.VarChar, 10);
            parameterNotVehicleRegisterNo.Value = notVehicleRegisterNo;
            myCommand.Parameters.Add(parameterNotVehicleRegisterNo);

            SqlParameter parameterNotVehicleMakeCode = new SqlParameter("@NotVehicleMakeCode", SqlDbType.VarChar, 3);
            parameterNotVehicleMakeCode.Value = notVehicleMakeCode;
            myCommand.Parameters.Add(parameterNotVehicleMakeCode);

            SqlParameter parameterNotVehicleMake = new SqlParameter("@NotVehicleMake", SqlDbType.VarChar, 30);
            parameterNotVehicleMake.Value = notVehicleMake;
            myCommand.Parameters.Add(parameterNotVehicleMake);

            SqlParameter parameterNotVehicleTypeCode = new SqlParameter("@NotVehicleTypeCode", SqlDbType.VarChar, 3);
            parameterNotVehicleTypeCode.Value = notVehicleTypeCode;
            myCommand.Parameters.Add(parameterNotVehicleTypeCode);

            SqlParameter parameterNotVehicleType = new SqlParameter("@NotVehicleType", SqlDbType.VarChar, 30);
            parameterNotVehicleType.Value = notVehicleType;
            myCommand.Parameters.Add(parameterNotVehicleType);

            SqlParameter parameterNotVehicleColour = new SqlParameter("@NotVehicleColour", SqlDbType.VarChar, 30);
            parameterNotVehicleColour.Value = notVehicleColour;
            myCommand.Parameters.Add(parameterNotVehicleColour);

            SqlParameter parameterNotVehicleLicenceExpiry = new SqlParameter("@NotVehicleLicenceExpiry", SqlDbType.DateTime);
            if (notVehicleLicenceExpiry == null)
                parameterNotVehicleLicenceExpiry.Value = DBNull.Value;
            else
                parameterNotVehicleLicenceExpiry.Value = notVehicleLicenceExpiry;
            myCommand.Parameters.Add(parameterNotVehicleLicenceExpiry);

            SqlParameter parameterNotOfficerNo = new SqlParameter("@NotOfficerNo", SqlDbType.VarChar, 10);
            parameterNotOfficerNo.Value = notOfficerNo;
            myCommand.Parameters.Add(parameterNotOfficerNo);

            SqlParameter parameterNotOfficerSname = new SqlParameter("@NotOfficerSname", SqlDbType.VarChar, 35);
            parameterNotOfficerSname.Value = notOfficerSname;
            myCommand.Parameters.Add(parameterNotOfficerSname);

            SqlParameter parameterNotOfficerGroup = new SqlParameter("@NotOfficerGroup", SqlDbType.VarChar, 50);
            parameterNotOfficerGroup.Value = notOfficerGroup;
            myCommand.Parameters.Add(parameterNotOfficerGroup);

            SqlParameter parameterIsOfficerError = new SqlParameter("@IsOfficerError", SqlDbType.Bit);
            parameterIsOfficerError.Value = isOfficerError;
            myCommand.Parameters.Add(parameterIsOfficerError);

            SqlParameter parameterOfficerErrorStr = new SqlParameter("@OfficerErrorStr", SqlDbType.VarChar, 500);
            parameterOfficerErrorStr.Value = officerError;
            myCommand.Parameters.Add(parameterOfficerErrorStr);

            SqlParameter parameterNotDMSCaptureClerk = new SqlParameter("@NotDMSCaptureCLerk", SqlDbType.VarChar, 25);
            parameterNotDMSCaptureClerk.Value = notDMSCaptureClerk;
            myCommand.Parameters.Add(parameterNotDMSCaptureClerk);

            SqlParameter parameterNotDMSCaptureDate = new SqlParameter("@NotDMSCaptureDate", SqlDbType.SmallDateTime);
            parameterNotDMSCaptureDate.Value = notDMSCaptureDate;
            myCommand.Parameters.Add(parameterNotDMSCaptureDate);

            SqlParameter parameterNSTypeCode = new SqlParameter("@NSTCode", SqlDbType.NVarChar, 5);
            parameterNSTypeCode.Value = nstCode;
            myCommand.Parameters.Add(parameterNSTypeCode);

            //Heidi 2013-05-20 add
            SqlParameter parameterLoSuIntNo = new SqlParameter("@LoSuIntNo", SqlDbType.Int);
            parameterLoSuIntNo.Value = loSuIntNo;
            myCommand.Parameters.Add(parameterLoSuIntNo);

            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.VarChar, 50);
            parameterNotIntNo.Value = 0;
            parameterNotIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNotIntNo);

            //dls 2013-03-25 - notPaymentDate based on date rules must ALWAYS be used
            //if (isVideo || aR_2500.Equals("Y", StringComparison.OrdinalIgnoreCase))
            //{
                SqlParameter parameterNotPaymentDate = new SqlParameter("@NotPaymentDate", SqlDbType.DateTime);
                parameterNotPaymentDate.Value = notPaymentDate;
                myCommand.Parameters.Add(parameterNotPaymentDate);
            //}

            SqlParameter parameterNotScanDate = new SqlParameter("@NotScanDate", SqlDbType.DateTime);
            parameterNotScanDate.Value = (string.IsNullOrEmpty(scanDate) ? null : scanDate);
            myCommand.Parameters.Add(parameterNotScanDate);

            #endregion

            #region Accuse

            SqlParameter parameterDrvIdNumber = new SqlParameter("@DrvIdNumber", SqlDbType.VarChar);
            parameterDrvIdNumber.Value = idNum;
            myCommand.Parameters.Add(parameterDrvIdNumber);

            SqlParameter parameterDrvSName = new SqlParameter("@DrvSName", SqlDbType.VarChar);
            parameterDrvSName.Value = sName;
            myCommand.Parameters.Add(parameterDrvSName);

            SqlParameter parameterDrvFName = new SqlParameter("@DrvFName", SqlDbType.VarChar);
            parameterDrvFName.Value = fNamem;
            myCommand.Parameters.Add(parameterDrvFName);

            SqlParameter parameterDrvPoAddr1 = new SqlParameter("@DrvPoAddr1", SqlDbType.VarChar);
            parameterDrvPoAddr1.Value = poAddr1;
            myCommand.Parameters.Add(parameterDrvPoAddr1);

            SqlParameter parameterDrvPoAddr2 = new SqlParameter("@DrvPoAddr2", SqlDbType.VarChar);
            parameterDrvPoAddr2.Value = poAddr2;
            myCommand.Parameters.Add(parameterDrvPoAddr2);

            SqlParameter parameterDrvPoAddr3 = new SqlParameter("@DrvPoAddr3", SqlDbType.VarChar);
            parameterDrvPoAddr3.Value = suburb;
            myCommand.Parameters.Add(parameterDrvPoAddr3);

            SqlParameter parameterDrvPoAddr4 = new SqlParameter("@DrvPoAddr4", SqlDbType.VarChar);
            parameterDrvPoAddr4.Value = townOrCity;
            myCommand.Parameters.Add(parameterDrvPoAddr4);

            SqlParameter parameterDrvStAddr1 = new SqlParameter("@DrvStAddr1", SqlDbType.VarChar);
            parameterDrvStAddr1.Value = stAddr1;
            myCommand.Parameters.Add(parameterDrvStAddr1);

            SqlParameter parameterDrvStAddr2 = new SqlParameter("@DrvStAddr2", SqlDbType.VarChar);
            parameterDrvStAddr2.Value = stAddr2;
            myCommand.Parameters.Add(parameterDrvStAddr2);

            SqlParameter parameterDrvStAddr3 = new SqlParameter("@DrvStAddr3", SqlDbType.VarChar);
            parameterDrvStAddr3.Value = suburb;
            myCommand.Parameters.Add(parameterDrvStAddr3);

            SqlParameter parameterDrvStAddr4 = new SqlParameter("@DrvStAddr4", SqlDbType.VarChar);
            parameterDrvStAddr4.Value = townOrCity;
            myCommand.Parameters.Add(parameterDrvStAddr4);

            SqlParameter parameterBusinessSuburb = new SqlParameter("@BusinessSuburb", SqlDbType.VarChar);
            parameterBusinessSuburb.Value = businessSuburb;
            myCommand.Parameters.Add(parameterBusinessSuburb);

            SqlParameter parameterBusinessTownOrCity = new SqlParameter("@BusinessTownOrCity", SqlDbType.VarChar);
            parameterBusinessTownOrCity.Value = businessTownOrCity;
            myCommand.Parameters.Add(parameterBusinessTownOrCity);

            SqlParameter parameterTelNo1 = new SqlParameter("@TelNo1", SqlDbType.VarChar);
            parameterTelNo1.Value = telNo1;
            myCommand.Parameters.Add(parameterTelNo1);

            SqlParameter parameterTelNo2 = new SqlParameter("@TelNo2", SqlDbType.VarChar);
            parameterTelNo2.Value = telNo2;
            myCommand.Parameters.Add(parameterTelNo2);

            SqlParameter parameterPostalCode1 = new SqlParameter("@StCode", SqlDbType.VarChar);
            parameterPostalCode1.Value = postalCode1;
            myCommand.Parameters.Add(parameterPostalCode1);

            SqlParameter parameterPostalCode2 = new SqlParameter("@PoCode", SqlDbType.VarChar);
            parameterPostalCode2.Value = postalCode2;
            myCommand.Parameters.Add(parameterPostalCode2);

            #endregion

            #region Charge
            SqlParameter parameterChgOffenceCode = new SqlParameter("@ChgOffenceCode", SqlDbType.VarChar, 15);
            parameterChgOffenceCode.Value = chgOffenceCode;
            myCommand.Parameters.Add(parameterChgOffenceCode);

            SqlParameter parameterChgOffenceCode2 = new SqlParameter("@ChgOffenceCode2", SqlDbType.VarChar, 15);
            parameterChgOffenceCode2.Value = chgOffenceCode2;
            myCommand.Parameters.Add(parameterChgOffenceCode2);

            SqlParameter parameterChgOffenceCode3 = new SqlParameter("@ChgOffenceCode3", SqlDbType.VarChar, 15);
            parameterChgOffenceCode3.Value = chgOffenceCode3;
            myCommand.Parameters.Add(parameterChgOffenceCode3);

            SqlParameter parameterChgOffenceDescr = new SqlParameter("@ChgOffenceDescr", SqlDbType.VarChar);
            parameterChgOffenceDescr.Value = chgOffenceDescr;
            myCommand.Parameters.Add(parameterChgOffenceDescr);

            SqlParameter parameterChgOffenceDescr2 = new SqlParameter("@ChgOffenceDescr2", SqlDbType.VarChar, 200);
            parameterChgOffenceDescr2.Value = chgOffenceDescr2;
            myCommand.Parameters.Add(parameterChgOffenceDescr2);

            SqlParameter parameterChgOffenceDescr3 = new SqlParameter("@ChgOffenceDescr3", SqlDbType.VarChar, 200);
            parameterChgOffenceDescr3.Value = chgOffenceDescr3;
            myCommand.Parameters.Add(parameterChgOffenceDescr3);


            SqlParameter parameterChgFineAmount = new SqlParameter("@ChgFineAmount", SqlDbType.Real);
            parameterChgFineAmount.Value = chgFineAmount;
            myCommand.Parameters.Add(parameterChgFineAmount);

            SqlParameter parameterChgFineAmount2 = new SqlParameter("@ChgFineAmount2", SqlDbType.Real);
            parameterChgFineAmount2.Value = chgFineAmount2;
            myCommand.Parameters.Add(parameterChgFineAmount2);

            SqlParameter parameterChgFineAmount3 = new SqlParameter("@ChgFineAmount3", SqlDbType.Real);
            parameterChgFineAmount3.Value = chgFineAmount3;
            myCommand.Parameters.Add(parameterChgFineAmount3);

            //Jake Added 20101108 AlternateCharge code 
            SqlParameter parameterChgAlternateCode1 = new SqlParameter("@ChgAlternateCode1", SqlDbType.VarChar, 20);
            parameterChgAlternateCode1.Value = chgAlternateCode1;
            myCommand.Parameters.Add(parameterChgAlternateCode1);

            SqlParameter parameterChgAlternateCode2 = new SqlParameter("@ChgAlternateCode2", SqlDbType.VarChar, 20);
            parameterChgAlternateCode2.Value = chgAlternateCode2;
            myCommand.Parameters.Add(parameterChgAlternateCode2);

            SqlParameter parameterChgAlternateCode3 = new SqlParameter("@ChgAlternateCode3", SqlDbType.VarChar, 20);
            parameterChgAlternateCode3.Value = chgAlternateCode3;
            myCommand.Parameters.Add(parameterChgAlternateCode3);

            SqlParameter parameterOffNoAOG1 = new SqlParameter("@OffNoAOG1", SqlDbType.Char, 1);
            parameterOffNoAOG1.Value = chgNoAog1;
            myCommand.Parameters.Add(parameterOffNoAOG1);

            SqlParameter parameterOffNoAOG2 = new SqlParameter("@OffNoAOG2", SqlDbType.Char, 1);
            parameterOffNoAOG2.Value = chgNoAog2;
            myCommand.Parameters.Add(parameterOffNoAOG2);

            SqlParameter parameterOffNoAOG3 = new SqlParameter("@OffNoAOG3", SqlDbType.Char, 1);
            parameterOffNoAOG3.Value = chgNoAog3;
            myCommand.Parameters.Add(parameterOffNoAOG3);

            #endregion

            #region Film & Frame
            SqlParameter parameterFilmNo = new SqlParameter("@FilmNo", SqlDbType.VarChar, 25);
            parameterFilmNo.Value = filmNo;
            myCommand.Parameters.Add(parameterFilmNo);

            SqlParameter parameterFrameNo = new SqlParameter("@FrameNo", SqlDbType.VarChar, 50);
            parameterFrameNo.Value = frameNo;
            myCommand.Parameters.Add(parameterFrameNo);

            //SqlParameter parameterFrameFilePath = new SqlParameter("@FrameFilePath", SqlDbType.VarChar, 255);
            //parameterFrameFilePath.Value = frameFilePath;
            //myCommand.Parameters.Add(parameterFrameFilePath);

            SqlParameter parameterIfsIntNo = new SqlParameter("@IfsIntNo", SqlDbType.Int);
            parameterIfsIntNo.Value = ifsIntNo;
            myCommand.Parameters.Add(parameterIfsIntNo);
            #endregion

            #region AartoDocument and AartoDocumentImage
            SqlParameter parameterDocNo = new SqlParameter("@DocNo", SqlDbType.VarChar, 20);
            parameterDocNo.Value = docNo;
            myCommand.Parameters.Add(parameterDocNo);

            SqlParameter parameterAaDocImgOrder = new SqlParameter("@AaDocImgOrder", SqlDbType.Int);
            parameterAaDocImgOrder.Value = aaDocImgOrder;
            myCommand.Parameters.Add(parameterAaDocImgOrder);

            SqlParameter parameterImageFilePath = new SqlParameter("@ImageFilePath", SqlDbType.VarChar, 100);
            parameterImageFilePath.Value = imageFilePath;
            myCommand.Parameters.Add(parameterImageFilePath);

            #endregion

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterConCode = new SqlParameter("@ConCode", SqlDbType.VarChar, 10);
            parameterConCode.Value = conCode;
            myCommand.Parameters.Add(parameterConCode);
            SqlTransaction tran = null;

            SqlParameter parameterPSTType = new SqlParameter("@PSTTName", SqlDbType.NVarChar, 50);
            parameterPSTType.Value = pstType;
            myCommand.Parameters.Add(parameterPSTType);

            //Jerry 2013-02-06 add
            SqlParameter parameterAR2500 = new SqlParameter("@AR_2500", SqlDbType.Char, 1);
            parameterAR2500.Value = aR_2500;
            myCommand.Parameters.Add(parameterAR2500);

            // Oscar 2013-05-27 added
            var allowNoAOGOffence = new AuthorityRulesDB(mConstr).GetAuthorityRule(autIntNo, "4537").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase);
            myCommand.Parameters.AddWithValue("@AllowNoAOGOffence", allowNoAOGOffence);

            //Jerry 2014-08-04 add for EMPD
            SqlParameter parameterOtherNationality = new SqlParameter("@OtherNationality", SqlDbType.VarChar, 3);
            parameterOtherNationality.Value = otherNationality;
            myCommand.Parameters.Add(parameterOtherNationality);

            SqlParameter parameterLicenceCodeName = new SqlParameter("@LicenceCodeName", SqlDbType.NVarChar, 50);
            parameterLicenceCodeName.Value = licenceCodeName;
            myCommand.Parameters.Add(parameterLicenceCodeName);

            SqlParameter parameterLicenceClassName = new SqlParameter("@LicenceClassName", SqlDbType.NVarChar, 50);
            parameterLicenceClassName.Value = licenceClassName;
            myCommand.Parameters.Add(parameterLicenceClassName);

            SqlParameter parameterLicenceTypeName = new SqlParameter("@LicenceTypeName", SqlDbType.NVarChar, 50);
            parameterLicenceTypeName.Value = licenceTypeName;
            myCommand.Parameters.Add(parameterLicenceTypeName);

            SqlParameter parameterMVANo = new SqlParameter("@MVANo", SqlDbType.NVarChar, 25);
            parameterMVANo.Value = mvaNo;
            myCommand.Parameters.Add(parameterMVANo);

            SqlParameter parameterCCNumber = new SqlParameter("@CCNumber", SqlDbType.NVarChar, 25);
            parameterCCNumber.Value = ccNumber;
            myCommand.Parameters.Add(parameterCCNumber);

            try
            {
                myConnection.Open();
                //tran = myConnection.BeginTransaction();
                //myCommand.Transaction = tran;
                object returnValue = myCommand.ExecuteScalar();
                if (returnValue == null) returnValue = 0;
                //int notIntNo = Convert.ToInt32(myCommand.Parameters["@NotIntNo"].Value.ToString().Trim());
                int notIntNo = Convert.ToInt32(returnValue);
                return notIntNo;
                //Jake 2010-11-12 Add officer has been moved to storedprocedure
            }
            catch (Exception ex)
            {
                if (tran != null)
                    tran.Rollback();

                throw ex;
            }
            finally
            {
                myConnection.Dispose();
            }
        }
        #endregion

        #region ImportHandwrittenOffencesSection341

        public int ImportVideoOffencesSection341(int autIntNo, string notTicketNo,
            string notCourtNo, DateTime offenceDate, string notLocDescr, string notRegNo, string notVehicleRegisterNo,
            string notVehicleMakeCode, string notVehicleMake, string notVehicleTypeCode, string notVehicleType, string notVehicleColour,
            DateTime? notVehicleLicenceExpiry, string notOfficerNo, string notOfficerSname, string notOfficerGroup,//17
            string notDMSCaptureClerk, DateTime? notDMSCaptureDate,
            string chgOffenceCode, string chgOffenceDescr, float chgFineAmount, 
            string filmNo, string frameNo, bool isOfficerError,
            string docNo, string lastUser, string conCode, string chgNoAog1,
            string sName, string fNamem, string idNum, string poAddr1, string poAddr2, string stAddr1, string stAddr2,
            string nstCode, string pstType, int loSuIntNo, DateTime notPaymentDate = default(DateTime), string aR_2500 = "N")
        {
            SqlConnection myConnection = new SqlConnection(mConstr);
            //SqlTransaction tran = myConnection.BeginTransaction();
            SqlCommand myCommand = new SqlCommand("ImportVideoOffencesSection341", myConnection);

            myCommand.CommandType = CommandType.StoredProcedure;

            #region Notice
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterNotTicketNo = new SqlParameter("@NotTicketNo", SqlDbType.VarChar, 50);
            parameterNotTicketNo.Value = notTicketNo;
            myCommand.Parameters.Add(parameterNotTicketNo);

            SqlParameter parameterNotCourtNo = new SqlParameter("@NotCourtNo", SqlDbType.VarChar, 6);
            parameterNotCourtNo.Value = notCourtNo;
            myCommand.Parameters.Add(parameterNotCourtNo);

            //SqlParameter parameterNotCourtName = new SqlParameter("@NotCourtName", SqlDbType.VarChar, 30);
            //parameterNotCourtName.Value = notCourtName;
            //myCommand.Parameters.Add(parameterNotCourtName);

            SqlParameter parameterOffenceDate = new SqlParameter("@OffenceDate", SqlDbType.DateTime);
            parameterOffenceDate.Value = offenceDate;
            myCommand.Parameters.Add(parameterOffenceDate);

            //SqlParameter parameterNotLocCode = new SqlParameter("@NotLocCode", SqlDbType.VarChar, 50);
            //parameterNotLocCode.Value = notLocCode;
            //myCommand.Parameters.Add(parameterNotLocCode);

            SqlParameter parameterNotLocDescr = new SqlParameter("@NotLocDescr", SqlDbType.VarChar, 150);
            if (String.IsNullOrEmpty(notLocDescr)) notLocDescr = "";
            else
            {
                if (notLocDescr.Length > 150) notLocDescr = notLocDescr.Substring(0, 150);
            }
            parameterNotLocDescr.Value = notLocDescr;
            myCommand.Parameters.Add(parameterNotLocDescr);

            SqlParameter parameterNotRegNo = new SqlParameter("@NotRegNo", SqlDbType.VarChar, 10);
            parameterNotRegNo.Value = notRegNo;
            myCommand.Parameters.Add(parameterNotRegNo);

            SqlParameter parameterNotVehicleRegisterNo = new SqlParameter("@NotVehicleRegisterNo", SqlDbType.VarChar, 10);
            parameterNotVehicleRegisterNo.Value = notVehicleRegisterNo;
            myCommand.Parameters.Add(parameterNotVehicleRegisterNo);

            SqlParameter parameterNotVehicleMakeCode = new SqlParameter("@NotVehicleMakeCode", SqlDbType.VarChar, 3);
            parameterNotVehicleMakeCode.Value = notVehicleMakeCode;
            myCommand.Parameters.Add(parameterNotVehicleMakeCode);

            SqlParameter parameterNotVehicleMake = new SqlParameter("@NotVehicleMake", SqlDbType.VarChar, 30);
            parameterNotVehicleMake.Value = notVehicleMake;
            myCommand.Parameters.Add(parameterNotVehicleMake);

            SqlParameter parameterNotVehicleTypeCode = new SqlParameter("@NotVehicleTypeCode", SqlDbType.VarChar, 3);
            parameterNotVehicleTypeCode.Value = notVehicleTypeCode;
            myCommand.Parameters.Add(parameterNotVehicleTypeCode);

            SqlParameter parameterNotVehicleType = new SqlParameter("@NotVehicleType", SqlDbType.VarChar, 30);
            parameterNotVehicleType.Value = notVehicleType;
            myCommand.Parameters.Add(parameterNotVehicleType);

            SqlParameter parameterNotVehicleColour = new SqlParameter("@NotVehicleColour", SqlDbType.VarChar, 30);
            parameterNotVehicleColour.Value = notVehicleColour;
            myCommand.Parameters.Add(parameterNotVehicleColour);

            SqlParameter parameterNotVehicleLicenceExpiry = new SqlParameter("@NotVehicleLicenceExpiry", SqlDbType.DateTime);
            if (notVehicleLicenceExpiry == null)
                parameterNotVehicleLicenceExpiry.Value = DBNull.Value;
            else
                parameterNotVehicleLicenceExpiry.Value = notVehicleLicenceExpiry;
            myCommand.Parameters.Add(parameterNotVehicleLicenceExpiry);

            SqlParameter parameterNotOfficerNo = new SqlParameter("@NotOfficerNo", SqlDbType.VarChar, 10);
            parameterNotOfficerNo.Value = notOfficerNo;
            myCommand.Parameters.Add(parameterNotOfficerNo);

            SqlParameter parameterNotOfficerSname = new SqlParameter("@NotOfficerSname", SqlDbType.VarChar, 35);
            parameterNotOfficerSname.Value = notOfficerSname;
            myCommand.Parameters.Add(parameterNotOfficerSname);

            SqlParameter parameterNotOfficerGroup = new SqlParameter("@NotOfficerGroup", SqlDbType.VarChar, 50);
            parameterNotOfficerGroup.Value = notOfficerGroup;
            myCommand.Parameters.Add(parameterNotOfficerGroup);

            SqlParameter parameterIsOfficerError = new SqlParameter("@IsOfficerError", SqlDbType.Bit);
            parameterIsOfficerError.Value = isOfficerError;
            myCommand.Parameters.Add(parameterIsOfficerError);

            SqlParameter parameterNotDMSCaptureClerk = new SqlParameter("@NotDMSCaptureCLerk", SqlDbType.VarChar, 25);
            parameterNotDMSCaptureClerk.Value = notDMSCaptureClerk;
            myCommand.Parameters.Add(parameterNotDMSCaptureClerk);

            SqlParameter parameterNotDMSCaptureDate = new SqlParameter("@NotDMSCaptureDate", SqlDbType.SmallDateTime);
            parameterNotDMSCaptureDate.Value = notDMSCaptureDate;
            myCommand.Parameters.Add(parameterNotDMSCaptureDate);

            SqlParameter parameterNSTypeCode = new SqlParameter("@NSTCode", SqlDbType.NVarChar, 5);
            parameterNSTypeCode.Value = nstCode;
            myCommand.Parameters.Add(parameterNSTypeCode);

            //Heidi 2013-05-20 add
            SqlParameter parameterLoSuIntNo = new SqlParameter("@LoSuIntNo", SqlDbType.Int);
            parameterLoSuIntNo.Value = loSuIntNo;
            myCommand.Parameters.Add(parameterLoSuIntNo);

            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.VarChar, 50);
            parameterNotIntNo.Value = 0;
            parameterNotIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNotIntNo);

            //dls 2013-03-25 - notPaymentDate based on date rules must ALWAYS be used
            //if (isVideo || aR_2500.Equals("Y", StringComparison.OrdinalIgnoreCase))
            //{
            SqlParameter parameterNotPaymentDate = new SqlParameter("@NotPaymentDate", SqlDbType.DateTime);
            parameterNotPaymentDate.Value = notPaymentDate;
            myCommand.Parameters.Add(parameterNotPaymentDate);
            //}

            #endregion

            #region Accuse

            SqlParameter parameterDrvIdNumber = new SqlParameter("@DrvIdNumber", SqlDbType.VarChar);
            parameterDrvIdNumber.Value = idNum;
            myCommand.Parameters.Add(parameterDrvIdNumber);

            SqlParameter parameterDrvSName = new SqlParameter("@DrvSName", SqlDbType.VarChar);
            parameterDrvSName.Value = sName;
            myCommand.Parameters.Add(parameterDrvSName);

            SqlParameter parameterDrvFName = new SqlParameter("@DrvFName", SqlDbType.VarChar);
            parameterDrvFName.Value = fNamem;
            myCommand.Parameters.Add(parameterDrvFName);

            SqlParameter parameterDrvPoAddr1 = new SqlParameter("@DrvPoAddr1", SqlDbType.VarChar);
            parameterDrvPoAddr1.Value = poAddr1;
            myCommand.Parameters.Add(parameterDrvPoAddr1);

            SqlParameter parameterDrvPoAddr2 = new SqlParameter("@DrvPoAddr2", SqlDbType.VarChar);
            parameterDrvPoAddr2.Value = poAddr2;
            myCommand.Parameters.Add(parameterDrvPoAddr2);

            SqlParameter parameterDrvStAddr1 = new SqlParameter("@DrvStAddr1", SqlDbType.VarChar);
            parameterDrvStAddr1.Value = stAddr1;
            myCommand.Parameters.Add(parameterDrvStAddr1);

            SqlParameter parameterDrvStAddr2 = new SqlParameter("@DrvStAddr2", SqlDbType.VarChar);
            parameterDrvStAddr2.Value = stAddr2;
            myCommand.Parameters.Add(parameterDrvStAddr2);

            #endregion

            #region Charge
            SqlParameter parameterChgOffenceCode = new SqlParameter("@ChgOffenceCode", SqlDbType.VarChar, 15);
            parameterChgOffenceCode.Value = chgOffenceCode;
            myCommand.Parameters.Add(parameterChgOffenceCode);

            SqlParameter parameterChgOffenceDescr = new SqlParameter("@ChgOffenceDescr", SqlDbType.VarChar);
            parameterChgOffenceDescr.Value = chgOffenceDescr;
            myCommand.Parameters.Add(parameterChgOffenceDescr);


            SqlParameter parameterChgFineAmount = new SqlParameter("@ChgFineAmount", SqlDbType.Real);
            parameterChgFineAmount.Value = chgFineAmount;
            myCommand.Parameters.Add(parameterChgFineAmount);

            SqlParameter parameterOffNoAOG1 = new SqlParameter("@OffNoAOG1", SqlDbType.Char, 1);
            parameterOffNoAOG1.Value = chgNoAog1;
            myCommand.Parameters.Add(parameterOffNoAOG1);

            #endregion

            #region Film & Frame
            SqlParameter parameterFilmNo = new SqlParameter("@FilmNo", SqlDbType.VarChar, 25);
            parameterFilmNo.Value = filmNo;
            myCommand.Parameters.Add(parameterFilmNo);

            SqlParameter parameterFrameNo = new SqlParameter("@FrameNo", SqlDbType.VarChar, 50);
            parameterFrameNo.Value = frameNo;
            myCommand.Parameters.Add(parameterFrameNo);

            //SqlParameter parameterFrameFilePath = new SqlParameter("@FrameFilePath", SqlDbType.VarChar, 255);
            //parameterFrameFilePath.Value = frameFilePath;
            //myCommand.Parameters.Add(parameterFrameFilePath);
            #endregion

            #region AartoDocument and AartoDocumentImage
            SqlParameter parameterDocNo = new SqlParameter("@DocNo", SqlDbType.VarChar, 20);
            parameterDocNo.Value = docNo;
            myCommand.Parameters.Add(parameterDocNo);

            #endregion

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterConCode = new SqlParameter("@ConCode", SqlDbType.VarChar, 10);
            parameterConCode.Value = conCode;
            myCommand.Parameters.Add(parameterConCode);
            SqlTransaction tran = null;

            SqlParameter parameterPSTType = new SqlParameter("@PSTTName", SqlDbType.NVarChar, 50);
            parameterPSTType.Value = pstType;
            myCommand.Parameters.Add(parameterPSTType);

            //Jerry 2013-02-06 add
            SqlParameter parameterAR2500 = new SqlParameter("@AR_2500", SqlDbType.Char, 1);
            parameterAR2500.Value = aR_2500;
            myCommand.Parameters.Add(parameterAR2500);

            // Oscar 2013-05-27 added
            var allowNoAOGOffence = new AuthorityRulesDB(mConstr).GetAuthorityRule(autIntNo, "4537").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase);
            myCommand.Parameters.AddWithValue("@AllowNoAOGOffence", allowNoAOGOffence);

            try
            {
                myConnection.Open();
                //tran = myConnection.BeginTransaction();
                //myCommand.Transaction = tran;
                object returnValue = myCommand.ExecuteScalar();
                if (returnValue == null) returnValue = 0;
                //int notIntNo = Convert.ToInt32(myCommand.Parameters["@NotIntNo"].Value.ToString().Trim());
                int notIntNo = Convert.ToInt32(returnValue);
                return notIntNo;
                //Jake 2010-11-12 Add officer has been moved to storedprocedure
            }
            catch (Exception ex)
            {
                if (tran != null)
                    tran.Rollback();

                throw ex;
            }
            finally
            {
                myConnection.Dispose();
            }
        }
        #endregion

        public int ImportHandwrittenOffenceForCancelledDocument(string documentType, int autIntNo, string notTicketNo,
            string filmNo, string frameNo, int ifsIntNo, string officerNo, string officerName, string officerGroup,
            bool isOfficerError, string officerError, string docNo, int aaDocImgOrder,
            string imageFilePath, string conCode, string lastUser, string nstCode,
            string notDMSCaptureClerk, DateTime notDMSCaptureDate, string psttName,string scanDate) // Henry 2012-9-14 added by henry for Punch Statistics
        {
            SqlConnection myConnection = new SqlConnection(mConstr);
            //SqlTransaction tran = myConnection.BeginTransaction();
            SqlCommand myCommand = new SqlCommand("ImportHandwrittenOffencesForCancelledDocument", myConnection);

            myCommand.CommandType = CommandType.StoredProcedure;

            #region Notice
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterNotTicketNo = new SqlParameter("@NotTicketNo", SqlDbType.VarChar, 50);
            parameterNotTicketNo.Value = notTicketNo;
            myCommand.Parameters.Add(parameterNotTicketNo);

            SqlParameter parameterIsOfficerError = new SqlParameter("@IsOfficerError", SqlDbType.Bit);
            parameterIsOfficerError.Value = isOfficerError;
            myCommand.Parameters.Add(parameterIsOfficerError);

            SqlParameter parameterOfficerErrorStr = new SqlParameter("@OfficerErrorStr", SqlDbType.VarChar, 500);
            parameterOfficerErrorStr.Value = officerError;
            myCommand.Parameters.Add(parameterOfficerErrorStr);

            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.VarChar, 50);
            parameterNotIntNo.Value = 0;
            parameterNotIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNotIntNo);

            SqlParameter parameterNSTypeCode = new SqlParameter("@NSTCode", SqlDbType.NVarChar, 5);
            parameterNSTypeCode.Value = nstCode;
            myCommand.Parameters.Add(parameterNSTypeCode);

            SqlParameter parameterNotDMSCaptureClerk = new SqlParameter("@NotDMSCaptureClerk", SqlDbType.NVarChar, 25);
            parameterNotDMSCaptureClerk.Value = notDMSCaptureClerk;
            myCommand.Parameters.Add(parameterNotDMSCaptureClerk);

            SqlParameter parameterNotDMSCaptureDate = new SqlParameter("@NotDMSCaptureDate", SqlDbType.SmallDateTime, 25);
            parameterNotDMSCaptureDate.Value = notDMSCaptureDate;
            myCommand.Parameters.Add(parameterNotDMSCaptureDate);

            SqlParameter parameterPSTTName = new SqlParameter("@PSTTName", SqlDbType.NVarChar, 50);
            parameterPSTTName.Value = psttName;
            myCommand.Parameters.Add(parameterPSTTName);
            
            #endregion Notice


            #region Film & Frame
            SqlParameter parameterFilmNo = new SqlParameter("@NotFilmNo", SqlDbType.VarChar, 10);
            parameterFilmNo.Value = filmNo;
            myCommand.Parameters.Add(parameterFilmNo);

            SqlParameter parameterFrameNo = new SqlParameter("@NotFrameNo", SqlDbType.VarChar, 50);
            parameterFrameNo.Value = frameNo;
            myCommand.Parameters.Add(parameterFrameNo);

            //SqlParameter parameterFrameFilePath = new SqlParameter("@FrameFilePath", SqlDbType.VarChar, 255);
            //parameterFrameFilePath.Value = frameFilePath;
            //myCommand.Parameters.Add(parameterFrameFilePath);

            SqlParameter parameterIfsIntNo = new SqlParameter("@IfsIntNo", SqlDbType.Int);
            parameterIfsIntNo.Value = ifsIntNo;
            myCommand.Parameters.Add(parameterIfsIntNo);
            #endregion

            SqlParameter parameterOfficerNo = new SqlParameter("@NotOfficerNo", SqlDbType.VarChar, 10);
            parameterOfficerNo.Value = officerNo;
            myCommand.Parameters.Add(parameterOfficerNo);

            SqlParameter parameterOfficerName = new SqlParameter("@NotOfficerName", SqlDbType.VarChar, 10);
            parameterOfficerName.Value = officerName;
            myCommand.Parameters.Add(parameterOfficerName);

            SqlParameter parameterOfficerGroup = new SqlParameter("@NotOfficerGroup", SqlDbType.VarChar, 50);
            parameterOfficerGroup.Value = officerGroup;
            myCommand.Parameters.Add(parameterOfficerGroup);


            #region AartoDocument and AartoDocumentImage
            SqlParameter parameterDocNo = new SqlParameter("@DocNo", SqlDbType.VarChar, 20);
            parameterDocNo.Value = docNo;
            myCommand.Parameters.Add(parameterDocNo);

            SqlParameter parameterAaDocImgOrder = new SqlParameter("@AaDocImgOrder", SqlDbType.Int);
            parameterAaDocImgOrder.Value = aaDocImgOrder;
            myCommand.Parameters.Add(parameterAaDocImgOrder);

            SqlParameter parameterImageFilePath = new SqlParameter("@ImageFilePath", SqlDbType.VarChar, 100);
            parameterImageFilePath.Value = imageFilePath;
            myCommand.Parameters.Add(parameterImageFilePath);

            #endregion

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterConCode = new SqlParameter("@ConCode", SqlDbType.VarChar, 10);
            parameterConCode.Value = conCode;
            myCommand.Parameters.Add(parameterConCode);

            SqlParameter parameterDocumentType = new SqlParameter("@DocumentType", SqlDbType.VarChar, 20);
            parameterDocumentType.Value = documentType;
            myCommand.Parameters.Add(parameterDocumentType);

            SqlParameter parameterNotScanDate = new SqlParameter("@NotScanDate", SqlDbType.DateTime);
            parameterNotScanDate.Value = (string.IsNullOrEmpty(scanDate) ? null : scanDate);
            myCommand.Parameters.Add(parameterNotScanDate);


            //SqlTransaction tran = null;

            try
            {
                myConnection.Open();
                //tran = myConnection.BeginTransaction();
                //myCommand.Transaction = tran;
                object returnValue = myCommand.ExecuteScalar();
                if (returnValue == null) returnValue = 0;
                //int notIntNo = Convert.ToInt32(myCommand.Parameters["@NotIntNo"].Value.ToString().Trim());
                int notIntNo = Convert.ToInt32(returnValue);


                // Jake 2010-11-12 add officer error has been moved to storedprocedure

                return notIntNo;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                myConnection.Dispose();
            }


        }

        /// <summary>
        /// Update AARTOBMDOcument status when Import cancelled document successfully
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="tran"></param>
        /// <param name="documentNo"></param>
        /// <returns></returns>
        //public static int UpdateBmDocumentStatus(SqlConnection conn, SqlTransaction tran, string documentNo)
        //{

        //    //SqlTransaction tran = myConnection.BeginTransaction();
        //    SqlCommand myCommand = new SqlCommand("SILCustom_AARTOBMDocument_UpdateBMDocumentStatusForCancelledDocument", conn, tran);

        //    SqlParameter parameterDocumentNo = new SqlParameter("@DocumentNo", SqlDbType.VarChar, 50);
        //    parameterDocumentNo.Value = documentNo;
        //    myCommand.Parameters.Add(parameterDocumentNo);

        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    try
        //    {

        //        return myCommand.ExecuteNonQuery();

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;

        //    }

        //}

        //public static void AddLog(string message)
        //{
        //    Logger.Write(message, "Information");
        //}

        //public static void AddErrorLog(Exception ex, string lastUser)
        //{
        //    string logName = ex.Source.Substring(0, ex.Source.Length > 50 ? 50 : ex.Source.Length);
        //    string description = ex.Message + ex.StackTrace;
        //    int prority = 5;

        //    Logger.Write(ex, "Error", prority, 100000, System.Diagnostics.TraceEventType.Critical, "HandwrittenImporter Consloe");

        //    WriterErrorLogToDB(prority, logName, description, lastUser);
        //}

        //public static void AddErrorLog(string logName, string description, string lastUser)
        //{
        //    Logger.Write(description, "Error", 10, 100000, System.Diagnostics.TraceEventType.Error, "HandwrittenImporter Consloe");

        //    WriterErrorLogToDB(10, logName, description, lastUser);
        //}

        private static bool WriterErrorLogToDB(int prority, string logName, string description, string lastUser)
        {

            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AARTOErrorLogAdd", myConnection);

            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterPrority = new SqlParameter("@Prority", SqlDbType.Int);
            parameterPrority.Value = prority;
            myCommand.Parameters.Add(parameterPrority);

            SqlParameter parameterLogName = new SqlParameter("@LogName", SqlDbType.VarChar, 100);
            parameterLogName.Value = logName;
            myCommand.Parameters.Add(parameterLogName);

            SqlParameter parameterLogDescription = new SqlParameter("@LogDescription", SqlDbType.NVarChar);
            parameterLogDescription.Value = description;
            myCommand.Parameters.Add(parameterLogDescription);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);
            try
            {
                myConnection.Open();
                return (myCommand.ExecuteNonQuery() > 0);
            }
            catch (SqlException ex)
            {
                throw ex;
                //return false;
            }
            finally
            {

                myConnection.Close();
            }
        }

    }
}
