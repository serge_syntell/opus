using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
	
	public partial class ProxyDetails 
	{
		public int PrxIntNo;
		public int NotIntNo;
		public string PrxSurname; 
		public string PrxInitials; 
		public string PrxIDType; 
		public string PrxIDNumber;
        public string PrxNationality;
        public string PrxAge;
        public string PrxPOAdd1;
        public string PrxPOAdd2;
        public string PrxPOAdd3;
        public string PrxPOAdd4;
        public string PrxPOAdd5;
        public string PrxPOCode;
        public string PrxStAdd1;
        public string PrxStAdd2;
        public string PrxStAdd3;
        public string PrxStAdd4;
        public string PrxStAdd5;
        public string PrxStCode;
        public DateTime PrxDateCOA;
        public string PrxLicenceCode;
        public string PrxLicencePlace;
		public string LastUser;
        public string PrxForeNames;
        public string PrxFullName;
        public string PrxCellNo;
        public string PrxWorkNo;
        public string PrxHomeNo;
	}

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public partial class ProxyDB
	{
		string mConstr = "";

			public ProxyDB (string vConstr)
			{
				mConstr = vConstr;
			}

		//*******************************************************
		//
		// The GetProxyDetails method returns a ProxyDetails
		// struct that contains information about a specific transaction number
		//
		//*******************************************************

		public ProxyDetails GetProxyDetails(int PrxIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("ProxyDetail", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterPrxIntNo = new SqlParameter("@PrxIntNo", SqlDbType.Int, 4);
			parameterPrxIntNo.Value = PrxIntNo;
			myCommand.Parameters.Add(parameterPrxIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			// Create CustomerDetails Struct
			ProxyDetails myProxyDetails = new ProxyDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myProxyDetails.NotIntNo = Convert.ToInt32(result["NotIntNo"]);
				myProxyDetails.PrxIntNo = Convert.ToInt32(result["PrxIntNo"]);
				myProxyDetails.PrxSurname = result["PrxSurname"].ToString(); 
				myProxyDetails.PrxInitials = result["PrxInitials"].ToString();
                myProxyDetails.PrxForeNames = result["PrxForeNames"].ToString();
                myProxyDetails.PrxFullName = result["PrxFullName"].ToString(); 
				myProxyDetails.PrxIDType = result["PrxIDType"].ToString(); 
				myProxyDetails.PrxIDNumber = result["PrxIDNumber"].ToString();
                myProxyDetails.PrxNationality = result["PrxNationality"].ToString();
                myProxyDetails.PrxAge = result["PrxAge"].ToString();
                myProxyDetails.PrxPOAdd1 = result["PrxPoAdd1"].ToString();
                myProxyDetails.PrxPOAdd2 = result["PrxPoAdd2"].ToString();
                myProxyDetails.PrxPOAdd3 = result["PrxPoAdd3"].ToString();
                myProxyDetails.PrxPOAdd4 = result["PrxPoAdd4"].ToString();
                myProxyDetails.PrxPOAdd5 = result["PrxPoAdd5"].ToString();
                myProxyDetails.PrxPOCode = result["PrxPoCode"].ToString();
                myProxyDetails.PrxStAdd1 = result["PrxStAdd1"].ToString();
                myProxyDetails.PrxStAdd2 = result["PrxStAdd2"].ToString();
                myProxyDetails.PrxStAdd3 = result["PrxStAdd3"].ToString();
                myProxyDetails.PrxStAdd4 = result["PrxStAdd4"].ToString();
                myProxyDetails.PrxStCode = result["PrxStCode"].ToString();
                if (result["PrxDateCOA"] != System.DBNull.Value)
                    myProxyDetails.PrxDateCOA = Convert.ToDateTime(result["PrxDateCOA"]);
                myProxyDetails.PrxLicenceCode = result["PrxLicenceCode"].ToString();
                myProxyDetails.PrxLicencePlace = result["PrxLicencePlace"].ToString();
				myProxyDetails.LastUser = result["LastUser"].ToString();
                myProxyDetails.PrxFullName = result["PrxFullName"].ToString();
                myProxyDetails.PrxCellNo = result["PrxCellNo"].ToString();
                myProxyDetails.PrxWorkNo = result["PrxWorkNo"].ToString();
                myProxyDetails.PrxHomeNo = result["PrxHomeNo"].ToString();  
			}
			result.Close();
			return myProxyDetails;
			
		}
		
		public ProxyDetails GetProxyDetailsByNotice(int NotIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("ProxyDetailByNotice", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
			parameterNotIntNo.Value = NotIntNo;
			myCommand.Parameters.Add(parameterNotIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			// Create CustomerDetails Struct
			ProxyDetails myProxyDetails = new ProxyDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myProxyDetails.NotIntNo = Convert.ToInt32(result["NotIntNo"]);
				myProxyDetails.PrxIntNo = Convert.ToInt32(result["PrxIntNo"]);
				myProxyDetails.PrxSurname = result["PrxSurname"].ToString(); 
				myProxyDetails.PrxInitials = result["PrxInitials"].ToString();
                myProxyDetails.PrxForeNames = result["PrxForeNames"].ToString();
                myProxyDetails.PrxFullName = result["PrxFullName"].ToString(); 
				myProxyDetails.PrxIDType = result["PrxIDType"].ToString(); 
				myProxyDetails.PrxIDNumber = result["PrxIDNumber"].ToString();
                myProxyDetails.PrxNationality = result["PrxNationality"].ToString();
                myProxyDetails.PrxAge = result["PrxAge"].ToString();
                myProxyDetails.PrxPOAdd1 = result["PrxPoAdd1"].ToString();
                myProxyDetails.PrxPOAdd2 = result["PrxPoAdd2"].ToString();
                myProxyDetails.PrxPOAdd3 = result["PrxPoAdd3"].ToString();
                myProxyDetails.PrxPOAdd4 = result["PrxPoAdd4"].ToString();
                myProxyDetails.PrxPOAdd5 = result["PrxPoAdd5"].ToString();
                myProxyDetails.PrxPOCode = result["PrxPoCode"].ToString();
                myProxyDetails.PrxStAdd1 = result["PrxStAdd1"].ToString();
                myProxyDetails.PrxStAdd2 = result["PrxStAdd2"].ToString();
                myProxyDetails.PrxStAdd3 = result["PrxStAdd3"].ToString();
                myProxyDetails.PrxStAdd4 = result["PrxStAdd4"].ToString();
                myProxyDetails.PrxStCode = result["PrxStCode"].ToString();
                if (result["PrxDateCOA"] != System.DBNull.Value)
                    myProxyDetails.PrxDateCOA = Convert.ToDateTime(result["PrxDateCOA"]);
                myProxyDetails.PrxLicenceCode = result["PrxLicenceCode"].ToString();
                myProxyDetails.PrxLicencePlace = result["PrxLicencePlace"].ToString();
				myProxyDetails.LastUser = result["LastUser"].ToString();
                myProxyDetails.PrxFullName = result["PrxFullName"].ToString();
                myProxyDetails.PrxCellNo = result["PrxCellNo"].ToString();
                myProxyDetails.PrxWorkNo = result["PrxWorkNo"].ToString();
                myProxyDetails.PrxHomeNo = result["PrxHomeNo"].ToString();  
			}
			result.Close();
			return myProxyDetails;
			
		}

		//*******************************************************
		//
		// The AddProxy method inserts a new transaction number record
		// into the Proxy database.  A unique "PrxIntNo"
		// key is then returned from the method.  
		//
		//*******************************************************

		public int AddProxy(int notIntNo, string prxSurname, string prxInitials, 
			string prxIDType, string prxIDNumber, 
//			string prxNationality,
//			string prxAge, string prxPoAdd1, string prxPoAdd2, string prxPoAdd3,
//			string prxPoAdd4, string prxPoAdd5,	string prxPoCode, string prxStAdd1,
//			string prxStAdd2, string prxStAdd3,	string prxStAdd4, string prxStAdd5, string prxStCode,
//			DateTime prxDateCOA, string prxLicenceCode, string prxLicencePlace, 
			string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("ProxyAdd", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
			parameterNotIntNo.Value = notIntNo;
			myCommand.Parameters.Add(parameterNotIntNo);

			SqlParameter parameterPrxSurname = new SqlParameter("@PrxSurname", SqlDbType.VarChar, 100);
			parameterPrxSurname.Value = prxSurname;
			myCommand.Parameters.Add(parameterPrxSurname);

			SqlParameter parameterPrxInitials = new SqlParameter("@PrxInitials", SqlDbType.VarChar, 10);
			parameterPrxInitials.Value = prxInitials;
			myCommand.Parameters.Add(parameterPrxInitials);
		
			SqlParameter parameterPrxIDType = new SqlParameter("@PrxIDType", SqlDbType.VarChar, 3);
			parameterPrxIDType.Value = prxIDType;
			myCommand.Parameters.Add(parameterPrxIDType);
		
			SqlParameter parameterPrxIDNumber = new SqlParameter("@PrxIDNumber", SqlDbType.VarChar, 25);
			parameterPrxIDNumber.Value = prxIDNumber;
			myCommand.Parameters.Add(parameterPrxIDNumber);
		
//			SqlParameter parameterPrxNationality = new SqlParameter("@PrxNationality", SqlDbType.VarChar, 3);
//			parameterPrxNationality.Value = prxNationality;
//			myCommand.Parameters.Add(parameterPrxNationality);
//		
//			SqlParameter parameterPrxAge = new SqlParameter("@PrxAge", SqlDbType.VarChar, 3);
//			parameterPrxAge.Value = prxAge;
//			myCommand.Parameters.Add(parameterPrxAge);
//		
//			SqlParameter parameterPrxPoAdd1 = new SqlParameter("@PrxPoAdd1", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd1.Value = prxPoAdd1;
//			myCommand.Parameters.Add(parameterPrxPoAdd1);
//		
//			SqlParameter parameterPrxPoAdd2 = new SqlParameter("@PrxPoAdd2", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd2.Value = prxPoAdd2;
//			myCommand.Parameters.Add(parameterPrxPoAdd2);
//		
//			SqlParameter parameterPrxPoAdd3 = new SqlParameter("@PrxPoAdd3", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd3.Value = prxPoAdd3;
//			myCommand.Parameters.Add(parameterPrxPoAdd3);
//		
//			SqlParameter parameterPrxPoAdd4 = new SqlParameter("@PrxPoAdd4", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd4.Value = prxPoAdd4;
//			myCommand.Parameters.Add(parameterPrxPoAdd4);
//		
//			SqlParameter parameterPrxPoAdd5 = new SqlParameter("@PrxPoAdd5", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd5.Value = prxPoAdd5;
//			myCommand.Parameters.Add(parameterPrxPoAdd5);
//		
//			SqlParameter parameterPrxPoCode = new SqlParameter("@PrxPoCode", SqlDbType.VarChar, 10);
//			parameterPrxPoCode.Value = prxPoCode;
//			myCommand.Parameters.Add(parameterPrxPoCode);
//		
//			SqlParameter parameterPrxStAdd1 = new SqlParameter("@PrxStAdd1", SqlDbType.VarChar, 100);
//			parameterPrxStAdd1.Value = prxStAdd1;
//			myCommand.Parameters.Add(parameterPrxStAdd1);
//		
//			SqlParameter parameterPrxStAdd2 = new SqlParameter("@PrxStAdd2", SqlDbType.VarChar, 100);
//			parameterPrxStAdd2.Value = prxStAdd2;
//			myCommand.Parameters.Add(parameterPrxStAdd2);
//			
//			SqlParameter parameterPrxStAdd3 = new SqlParameter("@PrxStAdd3", SqlDbType.VarChar, 100);
//			parameterPrxStAdd3.Value = prxStAdd3;
//			myCommand.Parameters.Add(parameterPrxStAdd3);
//			
//			SqlParameter parameterPrxStAdd4 = new SqlParameter("@PrxStAdd4", SqlDbType.VarChar, 100);
//			parameterPrxStAdd4.Value = prxStAdd4;
//			myCommand.Parameters.Add(parameterPrxStAdd4);
//			
//			SqlParameter parameterPrxStAdd5 = new SqlParameter("@PrxStAdd5", SqlDbType.VarChar, 100);
//			parameterPrxStAdd5.Value = prxStAdd5;
//			myCommand.Parameters.Add(parameterPrxStAdd5);
//			
//			SqlParameter parameterPrx = new SqlParameter("@PrxStCode", SqlDbType.VarChar, 10);
//			parameterPrx.Value = prxStCode;
//			myCommand.Parameters.Add(parameterPrx);
//			
//			SqlParameter parameterPrxDateCOA = new SqlParameter("@PrxDateCOA", SqlDbType.SmallDateTime);
//			parameterPrxDateCOA.Value = prxDateCOA;
//			myCommand.Parameters.Add(parameterPrxDateCOA);
//			
//			SqlParameter parameterPrxLicenceCode = new SqlParameter("@PrxLicenceCode", SqlDbType.VarChar, 3);
//			parameterPrxLicenceCode.Value = prxLicenceCode;
//			myCommand.Parameters.Add(parameterPrxLicenceCode);
//			
//			SqlParameter parameterPrxLicencePlace = new SqlParameter("@PrxLicencePlace", SqlDbType.VarChar, 50);
//			parameterPrxLicencePlace.Value = prxLicencePlace;
//			myCommand.Parameters.Add(parameterPrxLicencePlace);
//			
			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterPrxIntNo = new SqlParameter("@PrxIntNo", SqlDbType.Int, 4);
			parameterPrxIntNo.Direction = ParameterDirection.Output;
			myCommand.Parameters.Add(parameterPrxIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int PrxIntNo = Convert.ToInt32(parameterPrxIntNo.Value);

				return PrxIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		public SqlDataReader GetProxyList(int notIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("ProxyList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
			parameterNotIntNo.Value = notIntNo;
			myCommand.Parameters.Add(parameterNotIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}


		public DataSet GetProxyListDS(int notIntNo)
		{
			SqlDataAdapter sqlDAProxys = new SqlDataAdapter();
			DataSet dsProxys = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAProxys.SelectCommand = new SqlCommand();
			sqlDAProxys.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAProxys.SelectCommand.CommandText = "ProxyList";

			// Mark the Command as a SPROC
			sqlDAProxys.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
			parameterNotIntNo.Value = notIntNo;
			sqlDAProxys.SelectCommand.Parameters.Add(parameterNotIntNo);

			// Execute the command and close the connection
			sqlDAProxys.Fill(dsProxys);
			sqlDAProxys.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsProxys;		
		}

		public int UpdateProxy(int prxIntNo, int notIntNo, string prxSurname, string prxInitials, 
			string prxIDType, string prxIDNumber, 
//			string prxNationality,
//			string prxAge, string prxPoAdd1, string prxPoAdd2, string prxPoAdd3,
//			string prxPoAdd4, string prxPoAdd5,	string prxPoCode, string prxStAdd1,
//			string prxStAdd2, string prxStAdd3,	string prxStAdd4, string prxStAdd5, string prxStCode,
//			DateTime prxDateCOA, string prxLicenceCode, string prxLicencePlace, 
			string lastUser)  
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("ProxyUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
			parameterNotIntNo.Value = notIntNo;
			myCommand.Parameters.Add(parameterNotIntNo);

			SqlParameter parameterPrxSurname = new SqlParameter("@PrxSurname", SqlDbType.VarChar, 100);
			parameterPrxSurname.Value = prxSurname;
			myCommand.Parameters.Add(parameterPrxSurname);

			SqlParameter parameterPrxInitials = new SqlParameter("@PrxInitials", SqlDbType.VarChar, 10);
			parameterPrxInitials.Value = prxInitials;
			myCommand.Parameters.Add(parameterPrxInitials);
		
			SqlParameter parameterPrxIDType = new SqlParameter("@PrxIDType", SqlDbType.VarChar, 3);
			parameterPrxIDType.Value = prxIDType;
			myCommand.Parameters.Add(parameterPrxIDType);
		
			SqlParameter parameterPrxIDNumber = new SqlParameter("@PrxIDNumber", SqlDbType.VarChar, 25);
			parameterPrxIDNumber.Value = prxIDNumber;
			myCommand.Parameters.Add(parameterPrxIDNumber);
		
//			SqlParameter parameterPrxNationality = new SqlParameter("@PrxNationality", SqlDbType.VarChar, 3);
//			parameterPrxNationality.Value = prxNationality;
//			myCommand.Parameters.Add(parameterPrxNationality);
		
//			SqlParameter parameterPrxAge = new SqlParameter("@PrxAge", SqlDbType.VarChar, 3);
//			parameterPrxAge.Value = prxAge;
//			myCommand.Parameters.Add(parameterPrxAge);
//		
//			SqlParameter parameterPrxPoAdd1 = new SqlParameter("@PrxPoAdd1", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd1.Value = prxPoAdd1;
//			myCommand.Parameters.Add(parameterPrxPoAdd1);
//		
//			SqlParameter parameterPrxPoAdd2 = new SqlParameter("@PrxPoAdd2", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd2.Value = prxPoAdd2;
//			myCommand.Parameters.Add(parameterPrxPoAdd2);
//		
//			SqlParameter parameterPrxPoAdd3 = new SqlParameter("@PrxPoAdd3", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd3.Value = prxPoAdd3;
//			myCommand.Parameters.Add(parameterPrxPoAdd3);
//		
//			SqlParameter parameterPrxPoAdd4 = new SqlParameter("@PrxPoAdd4", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd4.Value = prxPoAdd4;
//			myCommand.Parameters.Add(parameterPrxPoAdd4);
//		
//			SqlParameter parameterPrxPoAdd5 = new SqlParameter("@PrxPoAdd5", SqlDbType.VarChar, 100);
//			parameterPrxPoAdd5.Value = prxPoAdd5;
//			myCommand.Parameters.Add(parameterPrxPoAdd5);
//		
//			SqlParameter parameterPrxPoCode = new SqlParameter("@PrxPoCode", SqlDbType.VarChar, 10);
//			parameterPrxPoCode.Value = prxPoCode;
//			myCommand.Parameters.Add(parameterPrxPoCode);
//		
//			SqlParameter parameterPrxStAdd1 = new SqlParameter("@PrxStAdd1", SqlDbType.VarChar, 100);
//			parameterPrxStAdd1.Value = prxStAdd1;
//			myCommand.Parameters.Add(parameterPrxStAdd1);
//		
//			SqlParameter parameterPrxStAdd2 = new SqlParameter("@PrxStAdd2", SqlDbType.VarChar, 100);
//			parameterPrxStAdd2.Value = prxStAdd2;
//			myCommand.Parameters.Add(parameterPrxStAdd2);
//			
//			SqlParameter parameterPrxStAdd3 = new SqlParameter("@PrxStAdd3", SqlDbType.VarChar, 100);
//			parameterPrxStAdd3.Value = prxStAdd3;
//			myCommand.Parameters.Add(parameterPrxStAdd3);
//			
//			SqlParameter parameterPrxStAdd4 = new SqlParameter("@PrxStAdd4", SqlDbType.VarChar, 100);
//			parameterPrxStAdd4.Value = prxStAdd4;
//			myCommand.Parameters.Add(parameterPrxStAdd4);
//			
//			SqlParameter parameterPrxStAdd5 = new SqlParameter("@PrxStAdd5", SqlDbType.VarChar, 100);
//			parameterPrxStAdd5.Value = prxStAdd5;
//			myCommand.Parameters.Add(parameterPrxStAdd5);
//			
//			SqlParameter parameterPrx = new SqlParameter("@PrxStCode", SqlDbType.VarChar, 10);
//			parameterPrx.Value = prxStCode;
//			myCommand.Parameters.Add(parameterPrx);
//			
//			SqlParameter parameterPrxDateCOA = new SqlParameter("@PrxDateCOA", SqlDbType.SmallDateTime);
//			parameterPrxDateCOA.Value = prxDateCOA;
//			myCommand.Parameters.Add(parameterPrxDateCOA);
//			
//			SqlParameter parameterPrxLicenceCode = new SqlParameter("@PrxLicenceCode", SqlDbType.VarChar, 3);
//			parameterPrxLicenceCode.Value = prxLicenceCode;
//			myCommand.Parameters.Add(parameterPrxLicenceCode);
//			
//			SqlParameter parameterPrxLicencePlace = new SqlParameter("@PrxLicencePlace", SqlDbType.VarChar, 50);
//			parameterPrxLicencePlace.Value = prxLicencePlace;
//			myCommand.Parameters.Add(parameterPrxLicencePlace);
//			
			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterPrxIntNo = new SqlParameter("@PrxIntNo", SqlDbType.Int, 4);
			parameterPrxIntNo.Value = prxIntNo;
			parameterPrxIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterPrxIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int PrxIntNo = (int)myCommand.Parameters["@PrxIntNo"].Value;

				return PrxIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}
	
	}
}
