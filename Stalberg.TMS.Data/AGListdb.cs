using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Stalberg.TMS_TPExInt;

namespace Stalberg.TMS
{
    //dls 090108 - we don't like StalbergXCeption - it needs to be removed at some point, or reworked

    public class AGListdb
    {
        // Fields
		string mConstr = string.Empty;
        //StalbergXception sEx;

        /// <summary>
        /// Initializes a new instance of the <see cref="AGListdb"/> class.
        /// </summary>
        /// <param name="vConstr">The v constr.</param>
        public AGListdb (string vConstr)
		{
			mConstr = vConstr;
		}

        // 2013-07-23 comment by Henry for useless
        //public int PopulateAGList(int autIntNo, ref string errMessage, DateTime currentDateTime)
        //{
        //    SqlConnection myConnection = new SqlConnection(mConstr);

        //    // Create Instance of Connection and Command Object
        //    myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("PopulateAGListTable", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = System.Data.CommandType.StoredProcedure;
        //    myCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    myCommand.Parameters.Add("@AGLDateLoaded", SqlDbType.SmallDateTime).Value = currentDateTime;
        //    myCommand.Parameters.Add("@NoOfRecords", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();

        //        int recsAffected = Convert.ToInt32(myCommand.Parameters["@NoOfRecords"].Value);
        //        return recsAffected;
        //    }
        //    catch (Exception X)
        //    {
        //        errMessage = X.Message;
        //        return -1;
        //    }
        //    finally
        //    {
        //        myConnection.Dispose();
        //    }
        //}

        #region 2013-11-19, Oscar removed
        ///// <summary>
        ///// jerry add 2011-12-12
        ///// </summary>
        ///// <param name="mtrIntNo"></param>
        ///// <param name="errMessage"></param>
        ///// <param name="currentDateTime"></param>
        ///// <returns></returns>
        //public int PopulateAGList_WS(int mtrIntNo, ref string errMessage, DateTime currentDateTime, string lastUser, int rowCount)
        //{
        //    SqlConnection myConnection = new SqlConnection(mConstr);

        //    // Create Instance of Connection and Command Object
        //    myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("PopulateAGListTable_WS", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = System.Data.CommandType.StoredProcedure;
        //    myCommand.Parameters.Add("@MtrIntNo", SqlDbType.Int, 4).Value = mtrIntNo;
        //    myCommand.Parameters.Add("@AGLDateLoaded", SqlDbType.SmallDateTime).Value = currentDateTime;
        //    myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar).Value = lastUser;
        //    myCommand.Parameters.Add("@NoOfRecords", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;

        //    // 2013-08-06, Oscar added batch
        //    myCommand.Parameters.AddWithValue("@ProcessRowCount", rowCount);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();

        //        int recsAffected = Convert.ToInt32(myCommand.Parameters["@NoOfRecords"].Value);
        //        return recsAffected;
        //    }
        //    catch (Exception X)
        //    {
        //        errMessage = X.Message;
        //        return -1;
        //    }
        //    finally
        //    {
        //        myConnection.Dispose();
        //    }
        //}
        #endregion

        // 2013-11-19, Oscar added
        public int PopulateAGList_WS2(int mtrIntNo, int rowCount, string lastUser, out string errorMsg)
        {
            errorMsg = null;
            var paras = new[]
            {
                new SqlParameter("@MtrIntNo", mtrIntNo),
                new SqlParameter("@ProcessRowCount", rowCount),
                new SqlParameter("@LastUser", lastUser)
            };
            try
            {
                using (var conn = new SqlConnection(mConstr))
                {
                    using (var cmd = new SqlCommand("PopulateAGListTable_WS2", conn)
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandTimeout = 0
                    })
                    {
                        cmd.Parameters.AddRange(paras);
                        conn.Open();
                        var obj = cmd.ExecuteScalar();
                        var result = int.Parse(Convert.ToString(obj));
                        cmd.Parameters.Clear();
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                errorMsg = ex.ToString();
                return -99;
            }
        }

        public DateTime GetAGListLastRunDate(int autIntNo, ref string errMessage)
        {         
            DateTime lastRunDate = DateTime.MinValue;

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AGListLastRunDate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = System.Data.CommandType.StoredProcedure;

            myCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            myCommand.Parameters.Add("@AutAGListLastRunDate", SqlDbType.DateTime).Direction = ParameterDirection.InputOutput;

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();

                // Calculate the CustomerID using Output Param from SPROC
                if (myCommand.Parameters["@AutAGListLastRunDate"].Value != System.DBNull.Value)
                    lastRunDate = Convert.ToDateTime(myCommand.Parameters["@AutAGListLastRunDate"].Value);
            }
            catch (Exception X)
            {
                errMessage = X.Message;
            }
            finally
            {
                myConnection.Dispose();
            }

            return lastRunDate;
        }

        // 2013-07-23 comment by Henry for useless
        //public bool SetAGListLastRunDate(int autIntNo, DateTime pDateTime, ref string errMessage)
        //{
        //    bool failed = false;
        //    //bool dateSet = false;
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("AGListLastRunUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = System.Data.CommandType.StoredProcedure;

        //    myCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    myCommand.Parameters.Add("@LastRunDate", SqlDbType.DateTime).Value = pDateTime;

        //    try
        //    {
        //        myConnection.Open();
        //        int success = myCommand.ExecuteNonQuery();

        //        if (success != 1)
        //            failed = true;

        //    }
        //    catch (Exception X)
        //    {
        //        errMessage = X.Message;
        //        failed = true;
        //    }
        //    finally
        //    {
        //        myConnection.Dispose();
        //    }

        //    return failed;
        //}

        //private object XecuteSqlScalar(string pSQLstring)
        //{
        //    SqlConnection myConnection = null;
        //    object scalarObject = null;

        //    try
        //    {                
        //        string sqlSelect = pSQLstring;
        //        myConnection = new SqlConnection(mConstr);
        //        SqlCommand myCommand = new SqlCommand(sqlSelect, myConnection);
        //        myConnection.Open( );
        //        scalarObject = myCommand.ExecuteScalar( );                    
        //    }
        //    catch(Exception eX)
        //    {
        //        throw eX;
        //    }
        //    finally
        //    {
        //        myConnection.Close( );
        //    }

        //    return scalarObject;
        //}

        //dls 090108 - we never use inline SQL!!!!!
        //public DateTime GetAGListLastRunDate( )
        //{
        //    TimeSpan ts = new TimeSpan(1,0,0,0);
        //    DateTime lastRunDate = (DateTime.Today.Subtract(ts));
        //    try
        //    {
        //        sEx = new StalbergXception("AGListdb");
        //        //dls 090108 - we never use inline SQL!!!!!

        //        // Create SQL Select String.
        //        string sqlSelect = "SELECT TOP 1 AutAGListLastRunDate FROM Authority";

        //        object objLastRun = XecuteSqlScalar(sqlSelect);

        //        if(objLastRun != null)
        //        {
        //            //lastRunDate = (DateTime)objLastRun;
        //            lastRunDate = Convert.ToDateTime(objLastRun);
        //        }
        //    }
        //    catch(SqlException SqlX)
        //    {
        //        sEx.AddException(SqlX);
        //    }
        //    catch(Exception X)
        //    {
        //        sEx.AddException(X);
        //    }

        //    return lastRunDate;
        //}


        //public StalbergXception PopulateAGList()
        //{
        //    SqlConnection myConnection = new SqlConnection(mConstr);

        //        // Create Instance of Connection and Command Object
        //        myConnection = new SqlConnection(mConstr);
        //        SqlCommand myCommand = new SqlCommand("PopulateAGListTable", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = System.Data.CommandType.StoredProcedure;

        //        try
        //        {
        //        myConnection.Open();
        //        int recsAffected = myCommand.ExecuteNonQuery();
        //        return sEx;
        //    }
        //    catch (SqlException SqlX)
        //    {
        //        sEx.AddException(SqlX);
        //    }
        //    catch (Exception X)
        //    {
        //        sEx.AddException(X);
        //    }
        //    finally
        //    {
        //        if (myConnection != null & myConnection.State == System.Data.ConnectionState.Open)
        //            myConnection.Close();
        //    }
        //    return sEx;
        //}

        //public StalbergXception PopulateAGList( ) 
        //{
        //    SqlConnection myConnection = null;
        //    try
        //    {
        //        sEx = new StalbergXception("AGListdb");
        //        // Create Instance of Connection and Command Object
        //        myConnection = new SqlConnection(mConstr);
        //        SqlCommand myCommand = new SqlCommand("PopulateAGListTable", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = System.Data.CommandType.StoredProcedure;

        //        myConnection.Open( );
        //        int recsAffected = myCommand.ExecuteNonQuery( );
        //        return sEx;
        //    }
        //    catch(SqlException SqlX)
        //    {
        //        sEx.AddException(SqlX);
        //    }
        //    catch (Exception X)
        //    {
        //        sEx.AddException(X);
        //    }
        //    finally
        //    {
        //        if (myConnection != null & myConnection.State == System.Data.ConnectionState.Open)
        //            myConnection.Close( );
        //    }
        //    return sEx;
        //}
    }
}
