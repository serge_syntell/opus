using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


namespace Stalberg.TMS
{
    //*******************************************************
    //
    // CiprusOffenceDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************
    public partial class CiprusOffenceDetails 
	{
        public int COfIntNo;
        public int AutIntNo; 
        public string COfCode = string.Empty;
        public string COfShortStatRef = string.Empty;
        public string COfAbbrOffenceDescr = string.Empty;
        public string COfAbbrAfrChgSheet1 = string.Empty;
	    public string COfAbbrAfrChgSheet2 = string.Empty;
	    public string COfAbbrAfrChgSheet3 = string.Empty;
	    public string COfAbbrEngChgSheet1 = string.Empty;
	    public string COfAbbrEngChgSheet2 = string.Empty;
	    public string COfAbbrEngChgSheet3 = string.Empty;
	    public string COfAfrChgSheet1 = string.Empty;
	    public string COfAfrChgSheet2 = string.Empty;
	    public string COfAfrChgSheet3 = string.Empty;
	    public string COfAfrChgSheet4 = string.Empty;
	    public string COfAfrChgSheet5 = string.Empty;
	    public string COfAfrChgSheet6 = string.Empty;
	    public string COfAfrChgSheet7 = string.Empty;
	    public string COfAfrChgSheet8 = string.Empty;
	    public string COfAfrChgSheet9 = string.Empty;
	    public string COfAfrChgSheet10 = string.Empty;
	    public string COfAfrChgSheet11 = string.Empty;
	    public string COfAfrChgSheet12 = string.Empty;
	    public string COfEngChgSheet1 = string.Empty;
	    public string COfEngChgSheet2 = string.Empty;
	    public string COfEngChgSheet3 = string.Empty;
	    public string COfEngChgSheet4 = string.Empty;
	    public string COfEngChgSheet5 = string.Empty;
	    public string COfEngChgSheet6 = string.Empty;
	    public string COfEngChgSheet7 = string.Empty;
	    public string COfEngChgSheet8 = string.Empty;
	    public string COfEngChgSheet9 = string.Empty;
	    public string COfEngChgSheet10 = string.Empty;
	    public string COfEngChgSheet11 = string.Empty;
	    public string COfEngChgSheet12 = string.Empty;
	    public string LastUser = string.Empty;
    }

    //*******************************************************
    //
    // CiprusOffenceDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query CiprusOffences within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************
    public partial class CiprusOffenceDB {

		string mConstr = "";

		public CiprusOffenceDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // CiprusOffenceDB.GetCiprusOffenceDetails() Method <a name="GetCiprusOffenceDetails"></a>
        //
        // The GetCiprusOffenceDetails method returns a CiprusOffenceDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************
        // 2013-07-19 comment by Henry for useless
        //public CiprusOffenceDetails GetCiprusOffenceDetails(int crtIntNo) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("CiprusOffenceDetail", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
        //    parameterCrtIntNo.Value = crtIntNo;
        //    myCommand.Parameters.Add(parameterCrtIntNo);

        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
        //    // Create CustomerDetails Struct
        //    CiprusOffenceDetails myCiprusOffenceDetails = new CiprusOffenceDetails();

        //    while (result.Read())
        //    {
        //        // Populate Struct using Output Params from SPROC
        //        myCiprusOffenceDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
        //        myCiprusOffenceDetails.COfCode = result["COfCode"].ToString();
        //        myCiprusOffenceDetails.COfShortStatRef = result["COfShortStatRef"].ToString();
        //        myCiprusOffenceDetails.COfAbbrOffenceDescr = result["COfAbbrOffenceDescr"].ToString();
        //        myCiprusOffenceDetails.COfAbbrAfrChgSheet1 = result["COfAbbrAfrChgSheet1"].ToString();
        //        myCiprusOffenceDetails.COfAbbrAfrChgSheet2 = result["COfAbbrAfrChgSheet2"].ToString();
        //        myCiprusOffenceDetails.COfAbbrAfrChgSheet3 = result["COfAbbrAfrChgSheet3"].ToString();
        //        myCiprusOffenceDetails.COfAbbrEngChgSheet1 = result["COfAbbrEngChgSheet1"].ToString();
        //        myCiprusOffenceDetails.COfAbbrEngChgSheet2 = result["COfAbbrEngChgSheet2"].ToString();
        //        myCiprusOffenceDetails.COfAbbrEngChgSheet3 = result["COfAbbrEngChgSheet3"].ToString();
        //        myCiprusOffenceDetails.COfAfrChgSheet1 = result["COfAfrChgSheet1"].ToString();
        //        myCiprusOffenceDetails.COfAfrChgSheet2 = result["COfAfrChgSheet2"].ToString();
        //        myCiprusOffenceDetails.COfAfrChgSheet3 = result["COfAfrChgSheet3"].ToString();
        //        myCiprusOffenceDetails.COfAfrChgSheet4 = result["COfAfrChgSheet4"].ToString();
        //        myCiprusOffenceDetails.COfAfrChgSheet5 = result["COfAfrChgSheet5"].ToString();
        //        myCiprusOffenceDetails.COfAfrChgSheet6 = result["COfAfrChgSheet6"].ToString();
        //        myCiprusOffenceDetails.COfAfrChgSheet7 = result["COfAfrChgSheet7"].ToString();
        //        myCiprusOffenceDetails.COfAfrChgSheet8 = result["COfAfrChgSheet8"].ToString();
        //        myCiprusOffenceDetails.COfAfrChgSheet9 = result["COfAfrChgSheet9"].ToString();
        //        myCiprusOffenceDetails.COfAfrChgSheet10 = result["COfAfrChgSheet10"].ToString();
        //        myCiprusOffenceDetails.COfAfrChgSheet11 = result["COfAfrChgSheet11"].ToString();
        //        myCiprusOffenceDetails.COfAfrChgSheet12 = result["COfAfrChgSheet12"].ToString();
        //        myCiprusOffenceDetails.COfEngChgSheet1 = result["COfEngChgSheet1"].ToString();
        //        myCiprusOffenceDetails.COfEngChgSheet2 = result["COfEngChgSheet2"].ToString();
        //        myCiprusOffenceDetails.COfEngChgSheet3 = result["COfEngChgSheet3"].ToString();
        //        myCiprusOffenceDetails.COfEngChgSheet4 = result["COfEngChgSheet4"].ToString();
        //        myCiprusOffenceDetails.COfEngChgSheet5 = result["COfEngChgSheet5"].ToString();
        //        myCiprusOffenceDetails.COfEngChgSheet6 = result["COfEngChgSheet6"].ToString();
        //        myCiprusOffenceDetails.COfEngChgSheet7 = result["COfEngChgSheet7"].ToString();
        //        myCiprusOffenceDetails.COfEngChgSheet8 = result["COfEngChgSheet8"].ToString();
        //        myCiprusOffenceDetails.COfEngChgSheet9 = result["COfEngChgSheet9"].ToString();
        //        myCiprusOffenceDetails.COfEngChgSheet10 = result["COfEngChgSheet10"].ToString();
        //        myCiprusOffenceDetails.COfEngChgSheet11 = result["COfEngChgSheet11"].ToString();
        //        myCiprusOffenceDetails.COfEngChgSheet12 = result["COfEngChgSheet12"].ToString();
        //        myCiprusOffenceDetails.LastUser = result["LastUser"].ToString();
        //    }
        //    result.Close();
        //    return myCiprusOffenceDetails;
        //}

        //*******************************************************
        //
        // CiprusOffenceDB.AddCiprusOffence() Method <a name="AddCiprusOffence"></a>
        //
        // The AddCiprusOffence method inserts a new menu record
        // into the menus database.  A unique "CiprusOffenceId"
        // key is then returned from the method.  
        //
        //*******************************************************
        public int ModifyCiprusOffence (CiprusOffenceDetails cofDetails, ref string errorMsg) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("CiprusOffenceModify", myConnection);

            // Mark the Command as a SPROC
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = cofDetails.AutIntNo;
            com.Parameters.Add("@COfCode", SqlDbType.VarChar, 6).Value = cofDetails.COfCode;
            com.Parameters.Add("@COfShortStatRef", SqlDbType.VarChar, 46).Value = cofDetails.COfShortStatRef;
            com.Parameters.Add("@COfAbbrOffenceDescr", SqlDbType.VarChar, 30).Value = cofDetails.COfAbbrOffenceDescr;
            com.Parameters.Add("@COfAbbrAfrChgSheet1", SqlDbType.VarChar, 46).Value = cofDetails.COfAbbrAfrChgSheet1;
            com.Parameters.Add("@COfAbbrAfrChgSheet2", SqlDbType.VarChar, 46).Value = cofDetails.COfAbbrAfrChgSheet2;
            com.Parameters.Add("@COfAbbrAfrChgSheet3", SqlDbType.VarChar, 46).Value = cofDetails.COfAbbrAfrChgSheet3;
            com.Parameters.Add("@COfAbbrEngChgSheet1", SqlDbType.VarChar, 46).Value = cofDetails.COfAbbrEngChgSheet1;
            com.Parameters.Add("@COfAbbrEngChgSheet2", SqlDbType.VarChar, 46).Value = cofDetails.COfAbbrEngChgSheet2;
            com.Parameters.Add("@COfAbbrEngChgSheet3", SqlDbType.VarChar, 46).Value = cofDetails.COfAbbrEngChgSheet3;
            com.Parameters.Add("@COfAfrChgSheet1", SqlDbType.VarChar, 70).Value = cofDetails.COfAfrChgSheet1;
            com.Parameters.Add("@COfAfrChgSheet2", SqlDbType.VarChar, 70).Value = cofDetails.COfAfrChgSheet2;
            com.Parameters.Add("@COfAfrChgSheet3", SqlDbType.VarChar, 70).Value = cofDetails.COfAfrChgSheet3;
            com.Parameters.Add("@COfAfrChgSheet4", SqlDbType.VarChar, 70).Value = cofDetails.COfAfrChgSheet4;
            com.Parameters.Add("@COfAfrChgSheet5", SqlDbType.VarChar, 70).Value = cofDetails.COfAfrChgSheet5;
            com.Parameters.Add("@COfAfrChgSheet6", SqlDbType.VarChar, 70).Value = cofDetails.COfAfrChgSheet6;
            com.Parameters.Add("@COfAfrChgSheet7", SqlDbType.VarChar, 70).Value = cofDetails.COfAfrChgSheet7;
            com.Parameters.Add("@COfAfrChgSheet8", SqlDbType.VarChar, 70).Value = cofDetails.COfAfrChgSheet8;
            com.Parameters.Add("@COfAfrChgSheet9", SqlDbType.VarChar, 70).Value = cofDetails.COfAfrChgSheet9;
            com.Parameters.Add("@COfAfrChgSheet10", SqlDbType.VarChar, 70).Value = cofDetails.COfAfrChgSheet10;
            com.Parameters.Add("@COfAfrChgSheet11", SqlDbType.VarChar, 70).Value = cofDetails.COfAfrChgSheet11;
            com.Parameters.Add("@COfAfrChgSheet12", SqlDbType.VarChar, 70).Value = cofDetails.COfAfrChgSheet12;
            com.Parameters.Add("@COfEngChgSheet1", SqlDbType.VarChar, 70).Value = cofDetails.COfEngChgSheet1;
            com.Parameters.Add("@COfEngChgSheet2", SqlDbType.VarChar, 70).Value = cofDetails.COfEngChgSheet2;
            com.Parameters.Add("@COfEngChgSheet3", SqlDbType.VarChar, 70).Value = cofDetails.COfEngChgSheet3;
            com.Parameters.Add("@COfEngChgSheet4", SqlDbType.VarChar, 70).Value = cofDetails.COfEngChgSheet4;
            com.Parameters.Add("@COfEngChgSheet5", SqlDbType.VarChar, 70).Value = cofDetails.COfEngChgSheet5;
            com.Parameters.Add("@COfEngChgSheet6", SqlDbType.VarChar, 70).Value = cofDetails.COfEngChgSheet6;
            com.Parameters.Add("@COfEngChgSheet7", SqlDbType.VarChar, 70).Value = cofDetails.COfEngChgSheet7;
            com.Parameters.Add("@COfEngChgSheet8", SqlDbType.VarChar, 70).Value = cofDetails.COfEngChgSheet8;
            com.Parameters.Add("@COfEngChgSheet9", SqlDbType.VarChar, 70).Value = cofDetails.COfEngChgSheet9;
            com.Parameters.Add("@COfEngChgSheet10", SqlDbType.VarChar, 70).Value = cofDetails.COfEngChgSheet10;
            com.Parameters.Add("@COfEngChgSheet11", SqlDbType.VarChar, 70).Value = cofDetails.COfEngChgSheet11;
            com.Parameters.Add("@COfEngChgSheet12", SqlDbType.VarChar, 70).Value = cofDetails.COfEngChgSheet12;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = cofDetails.LastUser;

            com.Parameters.Add("@COfIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;

            try
            {
                myConnection.Open();
                com.ExecuteNonQuery();
                
                // Calculate the CustomerID using Output Param from SPROC
                int cofIntNo = Convert.ToInt32(com.Parameters["@COfIntNo"].Value);

                return cofIntNo;
            }
            catch (Exception e)
            {
                errorMsg = e.Message;
                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

	}
}

