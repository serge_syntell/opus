using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{
	
	public partial class NoticeTypeDetails 
	{
		public Int32 NTIntNo;
		public string NTCode;
		public string NTDescr;
		public string NTStartPrefix;
		public string LastUser;
	}

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public partial class NoticeTypeDB
	{
		string mConstr = "";

			public NoticeTypeDB (string vConstr)
			{
				mConstr = vConstr;
			}

		//*******************************************************
		//
		// The GetNoticeTypeDetails method returns a NoticeTypeDetails
		// struct that contains information about a specific transaction number
		//
		//*******************************************************

		public NoticeTypeDetails GetNoticeTypeDetailsByCode(string ntCode) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("NoticeTypeDetailByCode", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
            //NTCode type is nvarchar(10) by seawen
			SqlParameter parameterNTCode = new SqlParameter("@NTCode", SqlDbType.NVarChar, 10);
			parameterNTCode.Value = ntCode;
			myCommand.Parameters.Add(parameterNTCode);

            #region Jerry 2014-10-27 change
            //myConnection.Open();
            //SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            //NoticeTypeDetails myNoticeTypeDetails = new NoticeTypeDetails();

            //while (result.Read())
            //{
            //    // Populate Struct using Output Params from SPROC
            //    myNoticeTypeDetails.NTIntNo = Convert.ToInt32(result["NTIntNo"]);
            //    myNoticeTypeDetails.NTCode = result["NTCode"].ToString();
            //    myNoticeTypeDetails.NTDescr = result["NTDescr"].ToString();
            //    myNoticeTypeDetails.NTStartPrefix = result["NTStartPrefix"].ToString();
            //    myNoticeTypeDetails.LastUser = result["LastUser"].ToString();
            //}
            //result.Close();
            #endregion

            NoticeTypeDetails myNoticeTypeDetails = new NoticeTypeDetails();
            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                while (result.Read())
                {
                    // Populate Struct using Output Params from SPROC
                    myNoticeTypeDetails.NTIntNo = Convert.ToInt32(result["NTIntNo"]);
                    myNoticeTypeDetails.NTCode = result["NTCode"].ToString();
                    myNoticeTypeDetails.NTDescr = result["NTDescr"].ToString();
                    myNoticeTypeDetails.NTStartPrefix = result["NTStartPrefix"].ToString();
                    myNoticeTypeDetails.LastUser = result["LastUser"].ToString();
                }
                result.Close();
                result.Dispose();
            }
            finally
            {
                myConnection.Close();
                myConnection.Dispose();
            }

            return myNoticeTypeDetails;
		}

		//*******************************************************
		//
		// The AddNoticeType method inserts a new transaction number record
		// into the NoticeType database.  A unique "TNIntNo"
		// key is then returned from the method.  
		//
		//*******************************************************
        // 2013-07-19 comment by Henry for useless
        //public int AddNoticeType(int autIntNo, string tnType, int tNumber, string tnDescr, string lastUser) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("NoticeTypeAdd", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterTNType = new SqlParameter("@TNType", SqlDbType.VarChar, 3);
        //    parameterTNType.Value = tnType;
        //    myCommand.Parameters.Add(parameterTNType);

        //    SqlParameter parameterTNumber = new SqlParameter("@TNumber", SqlDbType.Int);
        //    parameterTNumber.Value = tNumber;
        //    myCommand.Parameters.Add(parameterTNumber);

        //    SqlParameter parameterTNDescr = new SqlParameter("@TNDescr", SqlDbType.VarChar, 50);
        //    parameterTNDescr.Value = tnDescr;
        //    myCommand.Parameters.Add(parameterTNDescr);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterTNIntNo = new SqlParameter("@TNIntNo", SqlDbType.Int, 4);
        //    parameterTNIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterTNIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int tnIntNo = Convert.ToInt32(parameterTNIntNo.Value);

        //        return tnIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

		public SqlDataReader GetNoticeTypeList()
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("NoticeTypesList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

            //// Add Parameters to SPROC
            //SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            //parameterAutIntNo.Value = autIntNo;
            //myCommand.Parameters.Add(parameterAutIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

        // 2013-07-19 comment by Henry for useless
        //public DataSet GetNoticeTypeListDS()
        //{
        //    SqlDataAdapter sqlDANoticeTypes = new SqlDataAdapter();
        //    DataSet dsNoticeTypes = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlDANoticeTypes.SelectCommand = new SqlCommand();
        //    sqlDANoticeTypes.SelectCommand.Connection = new SqlConnection(mConstr);			
        //    sqlDANoticeTypes.SelectCommand.CommandText = "NoticeTypeList";

        //    // Mark the Command as a SPROC
        //    sqlDANoticeTypes.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    //// Add Parameters to SPROC
        //    //SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    //parameterAutIntNo.Value = autIntNo;
        //    //sqlDANoticeTypes.SelectCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command and close the connection
        //    sqlDANoticeTypes.Fill(dsNoticeTypes);
        //    sqlDANoticeTypes.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsNoticeTypes;		
        //}
        // 2013-07-19 comment by Henry for useless
        //public int UpdateNoticeType(int autIntNo, string tnType, int tNumber, string tnDescr, string lastUser, int tnIntNo) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("NoticeTypeUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterTNType = new SqlParameter("@TNType", SqlDbType.VarChar, 3);
        //    parameterTNType.Value = tnType;
        //    myCommand.Parameters.Add(parameterTNType);

        //    SqlParameter parameterTNumber = new SqlParameter("@TNumber", SqlDbType.Int);
        //    parameterTNumber.Value = tNumber;
        //    myCommand.Parameters.Add(parameterTNumber);

        //    SqlParameter parameterTNDescr = new SqlParameter("@TNDescr", SqlDbType.VarChar, 50);
        //    parameterTNDescr.Value = tnDescr;
        //    myCommand.Parameters.Add(parameterTNDescr);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterTNIntNo = new SqlParameter("@TNIntNo", SqlDbType.Int, 4);
        //    parameterTNIntNo.Value = tnIntNo;
        //    parameterTNIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterTNIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int TNIntNo = (int)myCommand.Parameters["@TNIntNo"].Value;

        //        return TNIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

		public String DeleteNoticeType (int tnIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("NoticeTypeDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterTNIntNo = new SqlParameter("@TNIntNo", SqlDbType.Int, 4);
			parameterTNIntNo.Value = tnIntNo;
			parameterTNIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterTNIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int userId = (int)parameterTNIntNo.Value;

				return userId.ToString();
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return String.Empty;
			}
		}
	}
}
