using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // ChequeListDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class ChequeListDetails 
	{
		public Int32 DSIntNo;
		public Int32 CLIntNo;
		public string CLChequePayee;
		public double CLChequeAmount;
		public DateTime CLChequeDate;
		public string CLChequeBank;
		public string CLChequeBankBranch;
		public string LastUser;
    }

    //*******************************************************
    //
    // ChequeListDB Class
    //
    //*******************************************************

    public partial class ChequeListDB {

		string mConstr = "";

		public ChequeListDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // ChequeListDB.GetChequeListDetails() Method <a name="GetChequeListDetails"></a>
        //
        //*******************************************************

		// 20050723 converted to chequeList
        public ChequeListDetails GetChequeListDetails(int clIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ChequeListDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCLIntNo = new SqlParameter("@CLIntNo", SqlDbType.Int, 4);
            parameterCLIntNo.Value = clIntNo;
            myCommand.Parameters.Add(parameterCLIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			ChequeListDetails myChequeListDetails = new ChequeListDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myChequeListDetails.CLIntNo = Convert.ToInt32(result["CLIntNo"]);
				myChequeListDetails.DSIntNo = Convert.ToInt32(result["DSIntNo"]);
				myChequeListDetails.CLChequePayee = result["CLChequePayee"].ToString();
				myChequeListDetails.CLChequeAmount = Convert.ToDouble(result["CLChequeAmount"]);
				myChequeListDetails.CLChequeDate = Convert.ToDateTime(result["CLChequeDate"]);
				myChequeListDetails.CLChequeBank = result["CLChequeBank"].ToString();
				myChequeListDetails.CLChequeBankBranch = result["CLChequeBankBranch"].ToString();
			}
			result.Close();
            return myChequeListDetails;
        }

        //*******************************************************
        //
        // ChequeListDB.AddChequeList() Method <a name="AddChequeList"></a>
        //
        //*******************************************************

        // 2013-07-19 comment by Henry for useless
        //public int AddChequeList (int DSIntNo, string CLChequeAmount, string CLChequePayee, 
        //    string lastUser) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChequeListAdd", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterDSIntNo = new SqlParameter("@DSIntNo", SqlDbType.Int, 4);
        //    parameterDSIntNo.Value = DSIntNo;
        //    myCommand.Parameters.Add(parameterDSIntNo);

        //    SqlParameter parameterCLChequePayee = new SqlParameter("@CLChequePayee", SqlDbType.VarChar, 18);
        //    parameterCLChequePayee.Value = CLChequePayee;
        //    myCommand.Parameters.Add(parameterCLChequePayee);

        //    SqlParameter parameterCLChequeAmount = new SqlParameter("@CLChequeAmount", SqlDbType.VarChar, 5);
        //    parameterCLChequeAmount.Value = CLChequeAmount;
        //    myCommand.Parameters.Add(parameterCLChequeAmount);
			
        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterCLIntNo = new SqlParameter("@CLIntNo", SqlDbType.Int, 4);
        //    parameterCLIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterCLIntNo);

        //    try {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int clIntNo = Convert.ToInt32(parameterCLIntNo.Value);

        //        return clIntNo;
        //    }
        //    catch 
        //    {
        //        myConnection.Dispose();
        //        return 0;
        //    }
        //}

		public SqlDataReader GetChequeListList(int DSIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("ChequeListList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			SqlParameter parameterDSIntNo = new SqlParameter("@DSIntNo", SqlDbType.Int, 4);
			parameterDSIntNo.Value = DSIntNo;
			myCommand.Parameters.Add(parameterDSIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		// 20050723 converted to chequeList		
		public DataSet GetChequeListListDS(int dsIntNo)
		{
			SqlDataAdapter sqlDAChequeLists = new SqlDataAdapter();
			DataSet dsChequeLists = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAChequeLists.SelectCommand = new SqlCommand();
			sqlDAChequeLists.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAChequeLists.SelectCommand.CommandText = "ChequeListList";

			// Mark the Command as a SPROC
			sqlDAChequeLists.SelectCommand.CommandType = CommandType.StoredProcedure;

			SqlParameter parameterDSIntNo = new SqlParameter("@DSIntNo", SqlDbType.Int, 4);
			parameterDSIntNo.Value = dsIntNo;
			sqlDAChequeLists.SelectCommand.Parameters.Add(parameterDSIntNo);

			// Execute the command and close the connection
			sqlDAChequeLists.Fill(dsChequeLists);
			sqlDAChequeLists.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsChequeLists;		
		}

		
		public int UpdateChequeList (int clIntNo, int dsIntNo, string clChequePayee, double clChequeAmount, DateTime clChequeDate, 
			string clChequeBank, string clChequeBankBranch , string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("ChequeListUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterDSIntNo = new SqlParameter("@DSIntNo", SqlDbType.Int, 4);
			parameterDSIntNo.Value = dsIntNo;
			myCommand.Parameters.Add(parameterDSIntNo);

			SqlParameter parameterCLChequePayee = new SqlParameter("@CLChequePayee", SqlDbType.VarChar, 50);
			parameterCLChequePayee.Value = clChequePayee;
			myCommand.Parameters.Add(parameterCLChequePayee);

			SqlParameter parameterCLChequeAmount = new SqlParameter("@CLChequeAmount", SqlDbType.Real);
			parameterCLChequeAmount.Value = clChequeAmount;
			myCommand.Parameters.Add(parameterCLChequeAmount);

			SqlParameter parameterCLChequeDate = new SqlParameter("@CLChequeDate", SqlDbType.SmallDateTime);
			parameterCLChequeDate.Value = clChequeDate;
			myCommand.Parameters.Add(parameterCLChequeDate);

			SqlParameter parameterCLChequeBank = new SqlParameter("@CLChequeBank", SqlDbType.VarChar, 50);
			parameterCLChequeBank.Value = clChequeBank;
			myCommand.Parameters.Add(parameterCLChequeBank);

			SqlParameter parameterCLChequeBankBranch = new SqlParameter("@CLChequeBankBranch", SqlDbType.VarChar, 50);
			parameterCLChequeBankBranch.Value = clChequeBankBranch;
			myCommand.Parameters.Add(parameterCLChequeBankBranch);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterCLIntNo = new SqlParameter("@CLIntNo", SqlDbType.Int);
			parameterCLIntNo.Direction = ParameterDirection.InputOutput;
			parameterCLIntNo.Value = clIntNo;
			myCommand.Parameters.Add(parameterCLIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				clIntNo = (int)myCommand.Parameters["@CLIntNo"].Value;

				return clIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

        // 2013-07-19 comment by Henry for useless
        //public String DeleteChequeList (int clIntNo)
        //{

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChequeListDelete", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterCLIntNo = new SqlParameter("@CLIntNo", SqlDbType.Int, 4);
        //    parameterCLIntNo.Value = clIntNo;
        //    parameterCLIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterCLIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int CLIntNo = (int)parameterCLIntNo.Value;

        //        return CLIntNo.ToString();
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return String.Empty;
        //    }
        //}
	}
}

