using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
	
	public partial class RepDocumentDetails 
	{
		public int RepIntNo;
        public int SRIntNo;
		public int RDIntNo;
        public string RDName;
        public string RDType;
        public string RDComment;
        public DateTime RDDateLoaded;
		public string LastUser;
	}

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public partial class RepDocumentDB
	{
		string mConstr = "";

			public RepDocumentDB (string vConstr)
			{
				mConstr = vConstr;
			}

		//*******************************************************
		//
		// The GetRepDocumentDetails method returns a RepDocumentDetails
		// struct that contains information about a specific transaction number
		//
		//*******************************************************

		public RepDocumentDetails GetRepDocumentDetails(int rdIntNo, int repIntNo, int srIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("RepDocumentDetail", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterRepIntNo = new SqlParameter("@RepIntNo", SqlDbType.Int, 4);
			parameterRepIntNo.Value = repIntNo;
			myCommand.Parameters.Add(parameterRepIntNo);

			// Add Parameters to SPROC
			SqlParameter parameterRDIntNo = new SqlParameter("@RDIntNo", SqlDbType.Int, 4);
            parameterRDIntNo.Value = rdIntNo;
            myCommand.Parameters.Add(parameterRDIntNo);

            SqlParameter parameterSRIntNo = new SqlParameter("@SRIntNo", SqlDbType.Int, 4);
            parameterSRIntNo.Value = srIntNo;
            myCommand.Parameters.Add(parameterSRIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			// Create CustomerDetails Struct
			RepDocumentDetails myRepDocumentDetails = new RepDocumentDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myRepDocumentDetails.RepIntNo = Convert.ToInt32(result["RepIntNo"]);
				myRepDocumentDetails.RDIntNo = Convert.ToInt32(result["RDIntNo"]);
                myRepDocumentDetails.RDName = result["RDName"].ToString();
                myRepDocumentDetails.RDType = result["RDType"].ToString();
                myRepDocumentDetails.RDComment = result["RDComment"].ToString();
                myRepDocumentDetails.LastUser = result["LastUser"].ToString(); 
                if (result["RDDateLoaded"] != System.DBNull.Value)
                    myRepDocumentDetails.RDDateLoaded = Convert.ToDateTime(result["RDDateLoaded"]); 
			}
			result.Close();
			return myRepDocumentDetails;
			
		}
		
		//*******************************************************
		//
		// The AddRepDocument method inserts a new transaction number record
		// into the RepDocument database.  A unique "RepIntNo"
		// key is then returned from the method.  
		//
		//*******************************************************

        public int AddRepDocument(int repIntNo, string rdName, string rdType, string rdComment, string lastUser, int srIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("RepDocumentAdd", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterRepIntNo = new SqlParameter("@RepIntNo", SqlDbType.Int, 4);
			parameterRepIntNo.Value = repIntNo;
			myCommand.Parameters.Add(parameterRepIntNo);

            SqlParameter parameterSRIntNo = new SqlParameter("@SRIntNo", SqlDbType.Int, 4);
            parameterSRIntNo.Value = srIntNo;
            myCommand.Parameters.Add(parameterSRIntNo);

            SqlParameter parameterRDName = new SqlParameter("@RDName", SqlDbType.VarChar, 50);
            parameterRDName.Value = rdName;
            myCommand.Parameters.Add(parameterRDName);

            SqlParameter parameterRDType = new SqlParameter("@RDType", SqlDbType.VarChar, 50);
            parameterRDType.Value = rdType;
            myCommand.Parameters.Add(parameterRDType);

            SqlParameter parameterRDComment = new SqlParameter("@RDComment", SqlDbType.VarChar, 255);
            parameterRDComment.Value = rdComment;
            myCommand.Parameters.Add(parameterRDComment);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterRDIntNo = new SqlParameter("@RDIntNo", SqlDbType.Int, 4);
            parameterRDIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterRDIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

                int RDIntNo = Convert.ToInt32(parameterRDIntNo.Value);

                return RDIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		public SqlDataReader GetRepDocumentList(int repIntNo, int srIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("RepDocumentList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterRepIntNo = new SqlParameter("@RepIntNo", SqlDbType.Int, 4);
            parameterRepIntNo.Value = repIntNo;
			myCommand.Parameters.Add(parameterRepIntNo);

            SqlParameter parameterSRIntNo = new SqlParameter("@SRIntNo", SqlDbType.Int, 4);
            parameterSRIntNo.Value = srIntNo;
            myCommand.Parameters.Add(parameterSRIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}


        public int UpdateRepDocument(int rdIntNo, int repIntNo, string rdName, string rdType, string rdComment, string lastUser, int srIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("RepDocumentUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
            SqlParameter parameterRepIntNo = new SqlParameter("@RepIntNo", SqlDbType.Int, 4);
            parameterRepIntNo.Value = repIntNo;
            myCommand.Parameters.Add(parameterRepIntNo);

            SqlParameter parameterSRIntNo = new SqlParameter("@SRIntNo", SqlDbType.Int, 4);
            parameterSRIntNo.Value = srIntNo;
            myCommand.Parameters.Add(parameterSRIntNo);

            SqlParameter parameterRDName = new SqlParameter("@RDName", SqlDbType.Char, 3);
            parameterRDName.Value = rdType;
            myCommand.Parameters.Add(parameterRDName);

            SqlParameter parameterRDType = new SqlParameter("@RDType", SqlDbType.VarChar, 50);
            parameterRDType.Value = rdName;
            myCommand.Parameters.Add(parameterRDType);

            SqlParameter parameterRDComment = new SqlParameter("@RDComment", SqlDbType.VarChar, 255);
            parameterRDComment.Value = rdComment;
            myCommand.Parameters.Add(parameterRDComment);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterRDIntNo = new SqlParameter("@RDIntNo", SqlDbType.Int, 4);
            parameterRDIntNo.Value = rdIntNo;
            parameterRDIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterRDIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

                int RDIntNo = (int)myCommand.Parameters["@RDIntNo"].Value;

                return RDIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}


        public int DeleteRepDocument(int rdIntNo, ref string errMessage, int repIntNo, int srIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RepDocumentDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterRepIntNo = new SqlParameter("@RepIntNo", SqlDbType.Int, 4);
            parameterRepIntNo.Value = repIntNo;
            myCommand.Parameters.Add(parameterRepIntNo);

            SqlParameter parameterSRIntNo = new SqlParameter("@SRIntNo", SqlDbType.Int, 4);
            parameterSRIntNo.Value = srIntNo;
            myCommand.Parameters.Add(parameterSRIntNo);

            SqlParameter parameterRDIntNo = new SqlParameter("@RDIntNo", SqlDbType.Int, 4);
            parameterRDIntNo.Value = rdIntNo;
            parameterRDIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterRDIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                rdIntNo = (int)parameterRDIntNo.Value;

                return rdIntNo;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                myConnection.Dispose();
                return -1;
            }
        }
	}
}
