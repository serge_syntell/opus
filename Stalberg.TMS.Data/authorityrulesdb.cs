using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Collections;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using Stalberg.TMS.Data;
using Stalberg.TMS.Data.Resources;

namespace Stalberg.TMS
{
    public class AuthorityRulesDetails
    {
        public Int32 ARIntNo;
        public Int32 AutIntNo;
        public string ARCode;
        public string ARDescr;
        public int ARNumeric;
        public string ARString;
        public string ARComment;
        public string LastUser;
    }

    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    public partial class AuthorityRulesDB
    {
        string mConstr = "";

        public AuthorityRulesDB(string vConstr)
        {
            mConstr = vConstr;
        }

        //*******************************************************
        //
        // The GetAuthorityRulesDetails method returns a AuthorityRulesDetails
        // struct that contains information about a specific transaction number
        //
        //*******************************************************
        public AuthorityRulesDetails GetAuthorityRulesDetails(int arIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthorityRulesDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterARIntNo = new SqlParameter("@ARIntNo", SqlDbType.Int, 4);
            parameterARIntNo.Value = arIntNo;
            myCommand.Parameters.Add(parameterARIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            AuthorityRulesDetails myAuthorityRulesDetails = new AuthorityRulesDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myAuthorityRulesDetails.ARIntNo = Convert.ToInt32(result["ARIntNo"]);
                myAuthorityRulesDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
                myAuthorityRulesDetails.ARCode = result["ARCode"].ToString();
                myAuthorityRulesDetails.ARString = result["ARString"].ToString();
                myAuthorityRulesDetails.ARNumeric = Convert.ToInt32(result["ARNumeric"]);
                myAuthorityRulesDetails.ARDescr = result["ARDescr"].ToString();
                myAuthorityRulesDetails.ARComment = result["ARComment"].ToString();
                myAuthorityRulesDetails.LastUser = result["LastUser"].ToString();
            }
            result.Close();
            return myAuthorityRulesDetails;
        }


        //public int GetAuthorityRulesByCode(int autIntNo, string arCode,
        //   string arDescr, ref int arNumeric, ref string arString, string arComment, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("AuthorityRulesByCode", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNoo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNoo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNoo);

        //    SqlParameter parameterARCode = new SqlParameter("@ARCode", SqlDbType.VarChar, 10);
        //    parameterARCode.Value = arCode;
        //    myCommand.Parameters.Add(parameterARCode);

        //    SqlParameter parameterARComment = new SqlParameter("@ARComment", SqlDbType.VarChar, 255);
        //    parameterARComment.Value = arComment;
        //    myCommand.Parameters.Add(parameterARComment);

        //    SqlParameter parameterARNumeric = new SqlParameter("@ARNumeric", SqlDbType.Int);
        //    parameterARNumeric.Value = arNumeric;
        //    parameterARNumeric.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterARNumeric);

        //    SqlParameter parameterARDescr = new SqlParameter("@ARDescr", SqlDbType.VarChar, 100);
        //    parameterARDescr.Value = arDescr;
        //    myCommand.Parameters.Add(parameterARDescr);

        //    SqlParameter parameterARString = new SqlParameter("@ARString", SqlDbType.VarChar, 25);
        //    parameterARString.Value = arString;
        //    parameterARString.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterARString);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterARIntNo = new SqlParameter("@ARIntNo", SqlDbType.Int, 4);
        //    parameterARIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterARIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int arIntNo = Convert.ToInt32(parameterARIntNo.Value);
        //        arNumeric = Convert.ToInt32(parameterARNumeric.Value);
        //        arString = parameterARString.Value.ToString();
        //        return arIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        ///// <summary>
        ///// The AddAuthorityRules method inserts a new transaction number record
        ///// into the AuthorityRules database.  A unique "ARIntNo"
        ///// key is then returned from the method.  
        ///// </summary>
        ///// <param name="autIntNo">The aut int no.</param>
        ///// <param name="arCode">The ar code.</param>
        ///// <param name="arDescr">The ar descr.</param>
        ///// <param name="arNumeric">The ar numeric.</param>
        ///// <param name="arString">The ar string.</param>
        ///// <param name="arComment">The ar comment.</param>
        ///// <param name="lastUser">The last user.</param>
        ///// <returns></returns>
        //public AuthorityRulesDetails GetAuthorityRulesDetailsByCode(int autIntNo, string arCode,
        // string arDescr, int arNumeric, string arString, string arComment, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("AuthorityRulesDetailByCode", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    com.Parameters.Add("@ARCode", SqlDbType.VarChar, 10).Value = arCode;
        //    com.Parameters.Add("@ARComment", SqlDbType.VarChar, 255).Value = arComment;
        //    com.Parameters.Add("@ARNumeric", SqlDbType.Int).Value = arNumeric;
        //    com.Parameters.Add("@ARDescr", SqlDbType.VarChar, 100).Value = arDescr;
        //    com.Parameters.Add("@ARString", SqlDbType.VarChar, 25).Value = arString;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

        //    // Create CustomerDetails Struct
        //    AuthorityRulesDetails myAuthorityRulesDetails = new AuthorityRulesDetails();

        //    con.Open();
        //    SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection);
        //    while (reader.Read())
        //    {
        //        // Populate Struct using Output Params from SPROC
        //        myAuthorityRulesDetails.ARIntNo = Convert.ToInt32(reader["ARIntNo"]);
        //        myAuthorityRulesDetails.AutIntNo = Convert.ToInt32(reader["AutIntNo"]);
        //        myAuthorityRulesDetails.ARCode = reader["ARCode"].ToString();
        //        myAuthorityRulesDetails.ARString = reader["ARString"].ToString();
        //        myAuthorityRulesDetails.ARNumeric = Convert.ToInt32(reader["ARNumeric"]);
        //        myAuthorityRulesDetails.ARDescr = reader["ARDescr"].ToString();
        //        myAuthorityRulesDetails.ARComment = reader["ARComment"].ToString();
        //        myAuthorityRulesDetails.LastUser = reader["LastUser"].ToString();
        //    }
        //    reader.Close();
        //    return myAuthorityRulesDetails;
        //}

        /// <summary>
        /// The AddAuthorityRules method inserts a new transaction number record
        /// into the AuthorityRules database.  A unique "ARIntNo"
        /// key is then returned from the method.  
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="arCode">The ar code.</param>
        /// <param name="arDescr">The ar descr.</param>
        /// <param name="arNumeric">The ar numeric.</param>
        /// <param name="arString">The ar string.</param>
        /// <param name="arComment">The ar comment.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        public int AddAuthorityRules(int autIntNo, string arCode, string arDescr,
            int arNumeric, string arString, string arComment, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthorityRulesAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterARCode = new SqlParameter("@ARCode", SqlDbType.VarChar, 10);
            parameterARCode.Value = arCode;
            myCommand.Parameters.Add(parameterARCode);

            SqlParameter parameterARComment = new SqlParameter("@ARComment", SqlDbType.VarChar, 255);
            parameterARComment.Value = arComment;
            myCommand.Parameters.Add(parameterARComment);

            SqlParameter parameterARNumeric = new SqlParameter("@ARNumeric", SqlDbType.Int);
            parameterARNumeric.Value = arNumeric;
            myCommand.Parameters.Add(parameterARNumeric);

            SqlParameter parameterARDescr = new SqlParameter("@ARDescr", SqlDbType.VarChar, 100);
            parameterARDescr.Value = arDescr;
            myCommand.Parameters.Add(parameterARDescr);

            SqlParameter parameterARString = new SqlParameter("@ARString", SqlDbType.VarChar, 25);
            parameterARString.Value = arString;
            myCommand.Parameters.Add(parameterARString);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterARIntNo = new SqlParameter("@ARIntNo", SqlDbType.Int, 4);
            parameterARIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterARIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int arIntNo = Convert.ToInt32(parameterARIntNo.Value);

                return arIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int CopyAuthorityRules(int fromAutIntNo, int autIntNo, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthorityRulesCopy", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFromAutIntNo = new SqlParameter("@FromAutIntNo", SqlDbType.Int, 4);
            parameterFromAutIntNo.Value = fromAutIntNo;
            myCommand.Parameters.Add(parameterFromAutIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            parameterAutIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterAutIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                autIntNo = Convert.ToInt32(parameterAutIntNo.Value);

                return autIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        //public bool DoAuthoritiesHaveWOASummons()
        //{
        //    bool hasWOA = false;

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("GetNoticeOfWOA", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add parameter to look for Authorities - 'autlst' return only AutIntNo.
        //    myCommand.Parameters.AddWithValue("@Return", "autlst");
            
        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    hasWOA = result.HasRows;

        //    if (myConnection.State == ConnectionState.Open)
        //        myConnection.Close();

        //    if (!result.IsClosed)
        //    {
        //        result.Close();
        //        result.Dispose();
        //    }

        //    return hasWOA;
        //}

        //public SqlDataReader GetAuthorityRulesList(int autIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("AuthorityRulesList", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the datareader result
        //    return result;
        //}

        //public List<AuthorityRulesDetails> GetAuthorityRulesListByAutCode(string autCode)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("AuthorityRulesList_AutCode", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutCode = new SqlParameter("@AutCode", SqlDbType.Char, 3);
        //    parameterAutCode.Value = autCode;
        //    myCommand.Parameters.Add(parameterAutCode);

        //    try
        //    {
        //        // Execute the command
        //        if (myConnection.State != ConnectionState.Open)
        //            myConnection.Open();
        //        SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
        //        List<AuthorityRulesDetails> ht = new List<AuthorityRulesDetails>();
        //        if (result.HasRows)
        //        {
        //            while (result.Read())
        //            {
        //                AuthorityRulesDetails rule = new AuthorityRulesDetails();
        //                rule.ARIntNo = Helper.GetReaderValue<int>(result, "ARIntNo");
        //                rule.ARCode = Helper.GetReaderValue<string>(result, "ARCode");
        //                rule.ARComment = Helper.GetReaderValue<string>(result, "ARComment");
        //                rule.ARDescr = Helper.GetReaderValue<string>(result, "ARDescr");
        //                rule.ARNumeric = Helper.GetReaderValue<int>(result, "ARNumeric");
        //                rule.ARString = Helper.GetReaderValue<string>(result, "ARString");
        //                ht.Add(rule);
        //            }
        //        }
        //        result.Close();
        //        result.Dispose();
        //        return ht;
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    finally
        //    {
        //        myConnection.Dispose();
        //    }
        //}

        public DataSet GetAuthorityRulesListDS(int autIntNo, int pageSize, int pageIndex, string searchText, out int totalCount)
        {
            SqlDataAdapter sqlDAAuthorityRuless = new SqlDataAdapter();
            DataSet dsAuthorityRuless = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAAuthorityRuless.SelectCommand = new SqlCommand();
            sqlDAAuthorityRuless.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAAuthorityRuless.SelectCommand.CommandText = "WorkFlowRuleForAuthority_GetByAutIntNo";

            // Mark the Command as a SPROC
            sqlDAAuthorityRuless.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDAAuthorityRuless.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterSearchText = new SqlParameter("@SearchText", SqlDbType.NVarChar, 250);
            parameterSearchText.Value = searchText;
            sqlDAAuthorityRuless.SelectCommand.Parameters.Add(parameterSearchText);

            SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int, 4);
            parameterPageSize.Value = pageSize;
            sqlDAAuthorityRuless.SelectCommand.Parameters.Add(parameterPageSize);

            SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int, 4);
            parameterPageIndex.Value = pageIndex;
            sqlDAAuthorityRuless.SelectCommand.Parameters.Add(parameterPageIndex);

            SqlParameter parameterTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int, 4);
            parameterTotalCount.Direction = ParameterDirection.Output;
            sqlDAAuthorityRuless.SelectCommand.Parameters.Add(parameterTotalCount);

            // Execute the command and close the connection
            sqlDAAuthorityRuless.Fill(dsAuthorityRuless);
            sqlDAAuthorityRuless.SelectCommand.Connection.Dispose();

            totalCount = (int)(parameterTotalCount.Value == DBNull.Value ? 0 : parameterTotalCount.Value);

            // Return the dataset result
            return dsAuthorityRuless;
        }

        public int UpdateAuthorityRules(int autIntNo, string arCode, string arDescr,
            int arNumeric, string arString, string arComment, string lastUser, int arIntNo)
        {
             //Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthorityRulesUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterARCode = new SqlParameter("@ARCode", SqlDbType.VarChar, 10);
            parameterARCode.Value = arCode;
            myCommand.Parameters.Add(parameterARCode);

            SqlParameter parameterARComment = new SqlParameter("@ARComment", SqlDbType.VarChar, 255);
            parameterARComment.Value = arComment;
            myCommand.Parameters.Add(parameterARComment);

            SqlParameter parameterARNumeric = new SqlParameter("@ARNumeric", SqlDbType.Int);
            parameterARNumeric.Value = arNumeric;
            myCommand.Parameters.Add(parameterARNumeric);

            SqlParameter parameterARDescr = new SqlParameter("@ARDescr", SqlDbType.VarChar, 100);
            parameterARDescr.Value = arDescr;
            myCommand.Parameters.Add(parameterARDescr);

            SqlParameter parameterARString = new SqlParameter("@ARString", SqlDbType.VarChar, 25);
            parameterARString.Value = arString;
            myCommand.Parameters.Add(parameterARString);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterARIntNo = new SqlParameter("@ARIntNo", SqlDbType.Int, 4);
            parameterARIntNo.Value = arIntNo;
            parameterARIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterARIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int ARIntNo = (int)myCommand.Parameters["@ARIntNo"].Value;

                return ARIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public String DeleteAuthorityRules(int arIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("AuthorityRulesDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterARIntNo = new SqlParameter("@ARIntNo", SqlDbType.Int, 4);
            parameterARIntNo.Value = arIntNo;
            parameterARIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterARIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int userId = (int)parameterARIntNo.Value;

                return userId.ToString();
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return String.Empty;
            }
        }

        /// <summary>
        /// Gets an AuthorityRule, specifying a default if one does not exist.
        /// </summary>
        /// <param name="rule">The rule that is used as the bases for the default values.</param>
        public void GetDefaultRule(AuthorityRulesDetails rule)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("AuthorityRuleGetDefaultRule", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = rule.AutIntNo;
            com.Parameters.Add("@Code", SqlDbType.VarChar, 10).Value = rule.ARCode;
            com.Parameters.Add("@Description", SqlDbType.VarChar, 100).Value = rule.ARDescr;
            com.Parameters.Add("@Number", SqlDbType.Int, 4).Value = rule.ARNumeric;
            com.Parameters.Add("@String", SqlDbType.VarChar, 35).Value = rule.ARString;
            com.Parameters.Add("@Comment", SqlDbType.VarChar, 255).Value = rule.ARComment;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = rule.LastUser;

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                if (reader.Read())
                {
                    rule.ARIntNo = Helper.GetReaderValue<int>(reader, "ARIntNo");
                    rule.ARCode = Helper.GetReaderValue<string>(reader, "ARCode");
                    rule.ARComment = Helper.GetReaderValue<string>(reader, "ARComment");
                    rule.ARDescr = Helper.GetReaderValue<string>(reader, "ARDescr");
                    rule.ARNumeric = Helper.GetReaderValue<int>(reader, "ARNumeric");
                    rule.ARString = Helper.GetReaderValue<string>(reader, "ARString");
                }
                reader.Close();
            }
            finally
            {
                con.Close();
            }
        }

        //public bool HasDaysElapsedAsPerAuthRule()
        //{
        //    bool hasDaysElapsed = false;

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("GetNoticeOfWOA", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    if (result.HasRows)
        //    {
        //        hasDaysElapsed = true;
        //    }

        //    if (myConnection.State == ConnectionState.Open)
        //        myConnection.Close();

        //    if (!result.IsClosed)
        //    {
        //        result.Close();
        //        result.Dispose();
        //    }

        //    return hasDaysElapsed;
        //}

        /// <summary>
        /// Check if an Authority Rule exists and if not, creates a default one.
        /// </summary>
        /// <param name="pConStr"></param>
        //public void CheckOrCreateAuthorityRuleForNoticeOfWOA(string pConStr)
        //{
        //    CheckOrCreateAuthorityRule("5800", 2, "N",
        //        "Indicates how many days must elapse before generating the Notice of a WOA (NoWa).",
        //        "2 days (Default); ARString: Y = Yes (Default), process Notice of a WOA, N = No, dont process Notice Pending WOA.",
        //        "Lester", pConStr);
        //}

        //public void CheckOrCreateAuthorityRule(
        //    string pAutRuleCode, int pAutRuleNumericValue,
        //    string pAutRuleStringFlag, string pAutRuleDescription,
        //    string pAutRuleComment, string pLastUser, string pConStr)
        //{
        //    List<SqlParameter> paramList = new List<SqlParameter>(2);
        //    paramList = AddSqlParam(paramList, "@ARCode", SqlDbType.VarChar, pAutRuleCode);
        //    paramList = AddSqlParam(paramList, "@ARNumeric", SqlDbType.Int, pAutRuleNumericValue);
        //    paramList = AddSqlParam(paramList, "@ARString", SqlDbType.VarChar, pAutRuleStringFlag);
        //    paramList = AddSqlParam(paramList, "@ARDescr", SqlDbType.VarChar, pAutRuleDescription);
        //    paramList = AddSqlParam(paramList, "@ARComment", SqlDbType.VarChar, pAutRuleComment);
        //    paramList = AddSqlParam(paramList, "@LastUser", SqlDbType.VarChar, pLastUser);

        //    CommonSql.XecuteProc("CheckCreateAuthRule", pConStr, paramList);
        //}

        //public static List<SqlParameter> AddSqlParam(List<SqlParameter> pParameterList,
        //    string pNameOfParam, SqlDbType sqlDbType, object pValue)
        //{
        //    List<SqlParameter> paramList = pParameterList;

        //    SqlParameter parameter = new SqlParameter(pNameOfParam, sqlDbType);
        //    parameter.Value = pValue;

        //    paramList.Add(parameter);

        //    return paramList;
        //}

        //public AuthorityRulesDetails GetDefaultAuthRule(AuthorityRulesDetails rule)
        //{
        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("AuthorityRuleGetDefaultRule", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = rule.AutIntNo;
        //    com.Parameters.Add("@Code", SqlDbType.VarChar, 10).Value = rule.ARCode;
        //    com.Parameters.Add("@Description", SqlDbType.VarChar, 100).Value = rule.ARDescr;
        //    com.Parameters.Add("@Number", SqlDbType.Int, 4).Value = rule.ARNumeric;
        //    com.Parameters.Add("@String", SqlDbType.VarChar, 35).Value = rule.ARString;
        //    com.Parameters.Add("@Comment", SqlDbType.VarChar, 255).Value = rule.ARComment;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = rule.LastUser;

        //    try
        //    {
        //        con.Open();
        //        SqlDataReader reader = com.ExecuteReader();
        //        if (reader.Read())
        //        {
        //            rule.ARIntNo = Helper.GetReaderValue<int>(reader, "ARIntNo");
        //            rule.ARCode = Helper.GetReaderValue<string>(reader, "ARCode");
        //            rule.ARComment = Helper.GetReaderValue<string>(reader, "ARComment");
        //            rule.ARDescr = Helper.GetReaderValue<string>(reader, "ARDescr");
        //            rule.ARNumeric = Helper.GetReaderValue<int>(reader, "ARNumeric");
        //            rule.ARString = Helper.GetReaderValue<string>(reader, "ARString");
        //        }
        //        reader.Close();
        //    }
        //    finally
        //    {
        //        con.Close();  
        //    }

        //    return rule;

        //}

        public AuthorityRulesDetails GetDefaultAuthRule(AuthorityRulesDetails rule)
        {
            // 2014-10-15, Oscar added "using".
            using (IDataReader rd = new WorkFlowRuleForAuthorityService().GetByWFRFANameAutIntNo("AR_" + rule.ARCode, rule.AutIntNo))
            {

                if (rd.Read())
                {
                    rule.ARCode = rd["WFRFAName"].ToString();
                    rule.ARComment = rd["WFRFAComment"].ToString();
                    rule.ARDescr = rd["WFRFADescr"].ToString();
                    rule.ARNumeric = int.Parse(rd["ARNumeric"].ToString());
                    rule.ARString = rd["ARString"].ToString();
                    rd.Close();
                }
                else
                {
                    #region Jerry 2012-05-28 change
                    //WorkFlowRuleForAuthority wfEntity = new WorkFlowRuleForAuthority();
                    //wfEntity = new WorkFlowRuleForAuthorityService().GetByWfrfaName("AR_" + rule.ARCode);
                    //#region Jerry 2012-05-28 change
                    ////if (wfEntity == null)
                    ////{
                    ////    wfEntity.WfrfaName = "AR_" + rule.ARCode;
                    ////    wfEntity.WfrfaComment = rule.ARComment;
                    ////    wfEntity.WfrfaDescr = rule.ARDescr;
                    ////    wfEntity = new WorkFlowRuleForAuthorityService().Save(wfEntity);

                    ////    WorkFlowRuleForAuthorityDescrLookup wfaLookup = new WorkFlowRuleForAuthorityDescrLookup();
                    ////    wfaLookup.LsCode = "en-US";
                    ////    wfaLookup.Wfrfaid = wfEntity.Wfrfaid;
                    ////    wfaLookup.WfrfaDescr = wfEntity.WfrfaDescr;
                    ////    wfaLookup = new WorkFlowRuleForAuthorityDescrLookupService().Save(wfaLookup);

                    ////    WorkFlowRuleForAuthorityCommentLookup wfaCommentLookup = new WorkFlowRuleForAuthorityCommentLookup();
                    ////    wfaCommentLookup.LsCode = "en-US";
                    ////    wfaCommentLookup.Wfrfaid = wfEntity.Wfrfaid;
                    ////    wfaCommentLookup.WfrfaComment = wfEntity.WfrfaComment;
                    ////    wfaCommentLookup = new WorkFlowRuleForAuthorityCommentLookupService().Save(wfaCommentLookup);

                    ////}
                    //#endregion

                    //if (wfEntity != null)
                    //{
                    //    AuthorityRule arEntity = new AuthorityRule();
                    //    arEntity.ArNumeric = rule.ARNumeric;
                    //    arEntity.ArString = rule.ARString;
                    //    arEntity.AutIntNo = rule.AutIntNo;
                    //    arEntity.Wfrfaid = wfEntity.Wfrfaid;
                    //    arEntity.LastUser = rule.LastUser;
                    //    arEntity = new AuthorityRuleService().Save(arEntity);
                    //}
                    //else
                    //{
                    //    return null;
                    //}

                    //IDataReader reader = new WorkFlowRuleForAuthorityService().GetByWFRFANameAutIntNo("AR_" + rule.ARCode, rule.AutIntNo);
                    //while (reader.Read())
                    //{
                    //    rule.ARCode = reader["WFRFAName"].ToString();
                    //    rule.ARComment = reader["WFRFAComment"].ToString();
                    //    rule.ARDescr = reader["WFRFADescr"].ToString();
                    //    rule.ARNumeric = int.Parse(reader["ARNumeric"].ToString());
                    //    rule.ARString = reader["ARString"].ToString();
                    //}
                    //reader.Close();
                    #endregion
                    rule = null;
                }

                return rule;
            }

        }

        public AuthorityRulesDetails GetAuthorityRule(int autIntNo, string arCode)
        {
            if (autIntNo <= 0 || arCode == null || string.IsNullOrEmpty(arCode.Trim())) return null;
            arCode = arCode.Trim();
            if (arCode.StartsWith("AR_", StringComparison.OrdinalIgnoreCase))
                arCode = arCode.Remove(0, 3);

            var ar = new AuthorityRulesDetails
            {
                AutIntNo = autIntNo,
                ARCode = arCode
            };
            ar = GetDefaultAuthRule(ar);

            if (ar == null)
            {
                if (System.Web.HttpContext.Current != null)
                    throw new Exception(string.Format(RuleErrorRe.MissingAuthorityRule, arCode));
                return null;
            }

            return ar;
        }

    }
}
