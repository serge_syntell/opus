using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    //*******************************************************
    //
    // DRDetails Class
    //
    // A simple data class that encapsulates details about a particular domain 
    //
    //*******************************************************
    public partial class DomainRuleDetails 
	{
		public string DomainName;
		public string DomainURL;
		public string DRComment;
		public string DRLoginReq;
		public string DREmailReq;
		public string DRAuthorityReq;
		public string DRPasswdReq;
		public string DRBackground;
		public string DRStylesheet;
		public string DRTitle;
		public string DRDescription;
		public string DRKeywords;
		public string DRMainImage;

    }
    //*******************************************************
    //
    // DomainRuleDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query Domains within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class DomainRuleDB {

		string mConstr = "";

		public DomainRuleDB (string vConstr)
		{
			mConstr = vConstr;
		}
        //*******************************************************
        //
        // DomainRuleDB.GetDRDetails() Method <a name="GetDRDetails"></a>
        //
        // The DomainRuleDB method returns a DRDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************
        public DomainRuleDetails GetDRDetails(int drIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("DomainRuleDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterDRIntNo = new SqlParameter("@DRIntNo", SqlDbType.Int, 4);
            parameterDRIntNo.Value = drIntNo;
            myCommand.Parameters.Add(parameterDRIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			DomainRuleDetails myDRDetails = new DomainRuleDetails();

			while (result.Read())
				{
					// Populate Struct using Output Params from SPROC
					myDRDetails.DomainName = result["DomainName"].ToString();
					myDRDetails.DomainURL = result["DomainURL"].ToString();
					myDRDetails.DRComment = result["DRComment"].ToString();
					myDRDetails.DRLoginReq = result["DRLoginReq"].ToString();
					myDRDetails.DREmailReq = result["DREmailReq"].ToString();
					myDRDetails.DRPasswdReq = result["DRPasswdReq"].ToString();
					myDRDetails.DRStylesheet = result["DRStylesheet"].ToString();
					myDRDetails.DRBackground = result["DRBackground"].ToString();
					myDRDetails.DRTitle = result["DRTitle"].ToString();
					myDRDetails.DRDescription = result["DRDescription"].ToString();
					myDRDetails.DRKeywords = result["DRKeywords"].ToString();
					myDRDetails.DRMainImage = result["DRMainImage"].ToString();
					myDRDetails.DRAuthorityReq = result["DRAuthorityReq"].ToString();
				}
			result.Close();
            return myDRDetails;
        }

		public SqlDataReader GetDRDetailsByURL(string domainURL) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("DomainRuleDetailByURL", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterDomainURL = new SqlParameter("@DomainURL", SqlDbType.VarChar, 100);
			parameterDomainURL.Value = domainURL;
			myCommand.Parameters.Add(parameterDomainURL);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			return result;
		}

        //*******************************************************
        //
        // DomainRuleDB.AddDomain() Method <a name="AddDomain"></a>
        //
        // The AddDomain method inserts a new domain record
        // into the domains database.  A unique "DRIntNo"
        // key is then returned from the method.  
        //
        //*******************************************************
        // 2013-07-19 comment by Henry for useless
        //public int AddDomain
        //    (string domainName, string domainURL, string drComment, string drLoginReq,
        //    string drEmailReq, string drPasswdReq, string drAuthorityReq, 
        //    string drBackground, string drStylesheet, string drTitle, string drDescription,
        //    string drKeywords, string drMainImage, string lastUser) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("DomainRuleAdd", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterDomainName = new SqlParameter("@DomainName", SqlDbType.VarChar, 50);
        //    parameterDomainName.Value = domainName;
        //    myCommand.Parameters.Add(parameterDomainName);

        //    SqlParameter parameterDomainURL = new SqlParameter("@DomainURL", SqlDbType.VarChar, 100);
        //    parameterDomainURL.Value = domainURL;
        //    myCommand.Parameters.Add(parameterDomainURL);

        //    SqlParameter parameterDRComment = new SqlParameter("@DRComment", SqlDbType.Text);
        //    parameterDRComment.Value = drComment;
        //    myCommand.Parameters.Add(parameterDRComment);

        //    SqlParameter parameterDRLoginReq = new SqlParameter("@DRLoginReq", SqlDbType.Char, 1);
        //    parameterDRLoginReq.Value = drLoginReq;
        //    myCommand.Parameters.Add(parameterDRLoginReq);

        //    SqlParameter parameterDREmailReq = new SqlParameter("@DREmailReq", SqlDbType.Char, 1);
        //    parameterDREmailReq.Value = drEmailReq;
        //    myCommand.Parameters.Add(parameterDREmailReq);

        //    SqlParameter parameterDRPasswdReq = new SqlParameter("@DRPasswdReq", SqlDbType.Char, 1);
        //    parameterDRPasswdReq.Value = drPasswdReq;
        //    myCommand.Parameters.Add(parameterDRPasswdReq);

        //    SqlParameter parameterDRAuthorityReq = new SqlParameter("@DRAuthorityReq", SqlDbType.Char, 1);
        //    parameterDRAuthorityReq.Value = drAuthorityReq;
        //    myCommand.Parameters.Add(parameterDRAuthorityReq);

        //    SqlParameter parameterDRBackground = new SqlParameter("@DRBackground", SqlDbType.VarChar, 30);
        //    parameterDRBackground.Value = drBackground;
        //    myCommand.Parameters.Add(parameterDRBackground);

        //    SqlParameter parameterDRStylesheet = new SqlParameter("@DRStylesheet", SqlDbType.VarChar, 30);
        //    parameterDRStylesheet.Value = drStylesheet;
        //    myCommand.Parameters.Add(parameterDRStylesheet);

        //    SqlParameter parameterDRTitle = new SqlParameter("@DRTitle", SqlDbType.VarChar, 100);
        //    parameterDRTitle.Value = drTitle;
        //    myCommand.Parameters.Add(parameterDRTitle);

        //    SqlParameter parameterDRDescription = new SqlParameter("@DRDescription", SqlDbType.VarChar, 255);
        //    parameterDRDescription.Value = drDescription;
        //    myCommand.Parameters.Add(parameterDRDescription);

        //    SqlParameter parameterDRKeywords = new SqlParameter("@DRKeywords", SqlDbType.VarChar, 255);
        //    parameterDRKeywords.Value = drKeywords;
        //    myCommand.Parameters.Add(parameterDRKeywords);

        //    SqlParameter parameterDRMainImage = new SqlParameter("@DRMainImage", SqlDbType.VarChar, 30);
        //    parameterDRMainImage.Value = drMainImage;
        //    myCommand.Parameters.Add(parameterDRMainImage);
			
        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterDRIntNo = new SqlParameter("@DRIntNo", SqlDbType.Int, 4);
        //    parameterDRIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterDRIntNo);

        //    try {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int drIntNo = Convert.ToInt32(parameterDRIntNo.Value);

        //        return drIntNo;
        //    }
        //    catch {
        //        myConnection.Dispose();
        //        return 0;
        //    }
        //}

  		public SqlDataReader GetDomainsList()
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("DomainRuleList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		public DataSet GetDomainsListDS()
		{
			SqlDataAdapter sqlDADomains = new SqlDataAdapter();
			DataSet dsDomains = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDADomains.SelectCommand = new SqlCommand();
			sqlDADomains.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDADomains.SelectCommand.CommandText = "DomainRuleList";

			// Mark the Command as a SPROC
			sqlDADomains.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Execute the command and close the connection
			sqlDADomains.Fill(dsDomains);
			sqlDADomains.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsDomains;		
		}

		public int UpdateDomain

			//Domain name and URL only updated by system user - not in this update
			(int drIntNo, string domainName, string domainURL, string drComment, 
			string drLoginReq, string drEmailReq, string drPasswdReq, string drAuthorityReq, 
			string drBackground, string drStylesheet, string drTitle, string drDescription,
			string drKeywords, string drMainImage, string lastUser)   
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("DomainRuleUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterDomainName = new SqlParameter("@DomainName", SqlDbType.VarChar, 50);
			parameterDomainName.Value = domainName;
			myCommand.Parameters.Add(parameterDomainName);

			SqlParameter parameterDomainURL = new SqlParameter("@DomainURL", SqlDbType.VarChar, 100);
			parameterDomainURL.Value = domainURL;
			myCommand.Parameters.Add(parameterDomainURL);

			SqlParameter parameterDRComment = new SqlParameter("@DRComment", SqlDbType.Text);
			parameterDRComment.Value = drComment;
			myCommand.Parameters.Add(parameterDRComment);

			SqlParameter parameterDRLoginReq = new SqlParameter("@DRLoginReq", SqlDbType.Char, 1);
			parameterDRLoginReq.Value = drLoginReq;
			myCommand.Parameters.Add(parameterDRLoginReq);

			SqlParameter parameterDREmailReq = new SqlParameter("@DREmailReq", SqlDbType.Char, 1);
			parameterDREmailReq.Value = drEmailReq;
			myCommand.Parameters.Add(parameterDREmailReq);

			SqlParameter parameterDRPasswdReq = new SqlParameter("@DRPasswdReq", SqlDbType.Char, 1);
			parameterDRPasswdReq.Value = drPasswdReq;
			myCommand.Parameters.Add(parameterDRPasswdReq);

			SqlParameter parameterDRAuthorityReq = new SqlParameter("@DRAuthorityReq", SqlDbType.Char, 1);
			parameterDRAuthorityReq.Value = drAuthorityReq;
			myCommand.Parameters.Add(parameterDRAuthorityReq);

			SqlParameter parameterDRStylesheet = new SqlParameter("@DRStylesheet", SqlDbType.VarChar, 30);
			parameterDRStylesheet.Value = drStylesheet;
			myCommand.Parameters.Add(parameterDRStylesheet);

			SqlParameter parameterDRBackground = new SqlParameter("@DRBackground", SqlDbType.VarChar, 30);
			parameterDRBackground.Value = drBackground;
			myCommand.Parameters.Add(parameterDRBackground);

			SqlParameter parameterDRTitle = new SqlParameter("@DRTitle", SqlDbType.VarChar, 100);
			parameterDRTitle.Value = drTitle;
			myCommand.Parameters.Add(parameterDRTitle);

			SqlParameter parameterDRDescription = new SqlParameter("@DRDescription", SqlDbType.VarChar, 255);
			parameterDRDescription.Value = drDescription;
			myCommand.Parameters.Add(parameterDRDescription);

			SqlParameter parameterDRKeywords = new SqlParameter("@DRKeywords", SqlDbType.VarChar, 255);
			parameterDRKeywords.Value = drKeywords;
			myCommand.Parameters.Add(parameterDRKeywords);

			SqlParameter parameterDRMainImage = new SqlParameter("@DRMainImage", SqlDbType.VarChar, 30);
			parameterDRMainImage.Value = drMainImage;
			myCommand.Parameters.Add(parameterDRMainImage);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterDRIntNo = new SqlParameter("@DRIntNo", SqlDbType.Int, 4);
			parameterDRIntNo.Direction = ParameterDirection.InputOutput;
			parameterDRIntNo.Value = drIntNo;
			myCommand.Parameters.Add(parameterDRIntNo);
		
			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int DRIntNo = (int)myCommand.Parameters["@DRIntNo"].Value;

				return DRIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		public String DeleteDomain (int drIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("DomainRuleDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterDRIntNo = new SqlParameter("@DRIntNo", SqlDbType.Int, 4);
			parameterDRIntNo.Value = drIntNo;
			myCommand.Parameters.Add(parameterDRIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int DRIntNo = (int)parameterDRIntNo.Value;

				return DRIntNo.ToString();
			}
			catch 
			{
				myConnection.Dispose();
				return String.Empty;
			}
		}

	}
}

