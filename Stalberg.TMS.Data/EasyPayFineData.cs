using System;
using System.Collections.Generic;
using System.Text;
using Stalberg.TMS;

namespace Stalberg.TMS_TPExInt.Objects
{
    /// <summary>
    /// Represents the data that will be exported for EasyPay and PayFine as well as statistical information
    /// </summary>
    public class EasyPayFineData
    {
        // Fields
        private int notIntNo;
        private string ticketNo = string.Empty;
        private DateTime offenceDate;
        private DateTime paymentDate;
        private decimal amount;
        private string filmNo = string.Empty;
        private string frameNo = string.Empty;
        private string location = string.Empty;
        private string ownerName = string.Empty;
        private string ownerID = string.Empty;
        private int status;
        private string statusDescr = string.Empty;
        private string regNo = string.Empty;
        private EasyPayAuthority authority;
        private string cameraSerialNo = string.Empty;
        private int speed1;
        private int speed2;
        private string offenceCode = string.Empty;
        private string offenceDescr = string.Empty;
        private int speedZone;
        private char isNoAOG = 'N';
        private DateTime updateDate;
        private bool updateSuccess = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="EasyPayFineData"/> class.
        /// </summary>
        public EasyPayFineData(EasyPayAuthority authority)
        {
            this.authority = authority;
        }

        /// <summary>
        /// Gets or sets a value that indicates if the fine is flagged as a no Admission of Guilt.
        /// </summary>
        /// <value>The is no AOG.</value>
        public char IsNoAOG
        {
            get { return this.isNoAOG; }
            set { this.isNoAOG = value; }
        }

        /// <summary>
        /// Gets or sets the speed zone.
        /// </summary>
        /// <value>The speed zone.</value>
        public int SpeedZone
        {
            get { return this.speedZone; }
            set { this.speedZone = value; }
        }

        /// <summary>
        /// Gets or sets the database notice ID.
        /// </summary>
        /// <value>The notice ID.</value>
        public int NoticeID
        {
            get { return this.notIntNo; }
            set { this.notIntNo = value; }
        }

        /// <summary>
        /// Gets the authority that this fine data belongs to.
        /// </summary>
        /// <value>The authority.</value>
        public Authority Authority
        {
            get { return this.authority; }
        }

        /// <summary>
        /// Gets or sets the offence code description.
        /// </summary>
        /// <value>The offence description.</value>
        public string OffenceDescription
        {
            get { return this.offenceDescr; }
            set { this.offenceDescr = value; }
        }

        /// <summary>
        /// Gets or sets the offence code.
        /// </summary>
        /// <value>The offence code.</value>
        public string OffenceCode
        {
            get { return this.offenceCode; }
            set { this.offenceCode = value; }
        }

        /// <summary>
        /// Gets or sets the second speed.
        /// </summary>
        /// <value>The speed2.</value>
        public int Speed2
        {
            get { return this.speed2; }
            set { this.speed2 = value; }
        }

        /// <summary>
        /// Gets or sets the first speed.
        /// </summary>
        /// <value>The speed1.</value>
        public int Speed1
        {
            get { return this.speed1; }
            set { this.speed1 = value; }
        }

        /// <summary>
        /// Gets or sets the camera serial number.
        /// </summary>
        /// <value>The camera serial number.</value>
        public string CameraSerialNumber
        {
            get { return this.cameraSerialNo; }
            set { this.cameraSerialNo = value; }
        }

        /// <summary>
        /// Gets or sets the ticket number.
        /// </summary>
        /// <value>The ticket number.</value>
        public string TicketNumber
        {
            get { return this.ticketNo; }
            set { this.ticketNo = value; }
        }

        /// <summary>
        /// Gets or sets the offence date.
        /// </summary>
        /// <value>The offence date.</value>
        public DateTime OffenceDate
        {
            get { return this.offenceDate; }
            set { this.offenceDate = value; }
        }

        /// <summary>
        /// Gets or sets the payment date.
        /// </summary>
        /// <value>The payment date.</value>
        public DateTime PaymentDate
        {
            get { return this.paymentDate; }
            set { this.paymentDate = value; }
        }

        /// <summary>
        /// Gets or sets the fine amount.
        /// </summary>
        /// <value>The fine amount.</value>
        public decimal FineAmount
        {
            get { return this.amount; }
            set { this.amount = value; }
        }

        /// <summary>
        /// Gets or sets the film number.
        /// </summary>
        /// <value>The film number.</value>
        public string FilmNumber
        {
            get { return this.filmNo; }
            set { this.filmNo = value; }
        }

        /// <summary>
        /// Gets or sets the frame number.
        /// </summary>
        /// <value>The frame number.</value>
        public string FrameNumber
        {
            get { return this.frameNo; }
            set { this.frameNo = value; }
        }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>The location.</value>
        public string Location
        {
            get { return this.location; }
            set { this.location = value; }
        }

        /// <summary>
        /// Gets or sets the name of the owner.
        /// </summary>
        /// <value>The name of the owner.</value>
        public string OwnerName
        {
            get { return this.ownerName; }
            set { this.ownerName = value; }
        }

        /// <summary>
        /// Gets or sets the owner's ID number.
        /// </summary>
        /// <value>The owner ID.</value>
        public string OwnerID
        {
            get { return this.ownerID; }
            set { this.ownerID = value; }
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public int Status
        {
            get { return this.status; }
            set { this.status = value; }
        }

        /// <summary>
        /// Gets or sets the status description.
        /// </summary>
        /// <value>The status description.</value>
        public string StatusDescription
        {
            get { return this.statusDescr; }
            set { this.statusDescr = value; }
        }

        /// <summary>
        /// Gets or sets the reg no.
        /// </summary>
        /// <value>The reg no.</value>
        public string RegNo
        {
            get { return this.regNo; }
            set { this.regNo = value; }
        }

        /// <summary>
        /// Gets or sets the Update Date.
        /// </summary>
        /// <value>The reg no.</value>
        public DateTime UpdateDate
        {
            get { return this.updateDate; }
            set { this.updateDate = value; }
        }

        /// <summary>
        /// Gets or sets the Update Success.
        /// </summary>
        /// <value>The reg no.</value>
        public bool UpdateSuccess
        {
            get { return this.updateSuccess; }
            set { this.updateSuccess = value; }
        }

        /// <summary>
        /// Copies the Notice data from this instance to another.
        /// </summary>
        /// <returns></returns>
        public EasyPayFineData Copy()
        {
            EasyPayFineData copy = new EasyPayFineData(this.authority);
            copy.CameraSerialNumber = this.cameraSerialNo;
            copy.FilmNumber = this.filmNo;
            copy.FrameNumber = this.frameNo;
            copy.Location = this.location;
            copy.OffenceDate = this.offenceDate;
            copy.OwnerID = this.ownerID;
            copy.OwnerName = this.ownerName;
            copy.PaymentDate = this.paymentDate;
            copy.RegNo = this.regNo;
            copy.Speed1 = this.speed1;
            copy.Speed2 = this.speed2;
            copy.SpeedZone = this.speedZone;
            copy.IsNoAOG = this.IsNoAOG;
            copy.OffenceDescription = this.offenceDescr;
            copy.OffenceCode = this.offenceCode;

            return copy;
        }

    }
}
