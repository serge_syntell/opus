using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // ChargeTypeDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class ChargeTypeDetails 
	{
        public string CType;
		public string CTDescr;
    }

    //*******************************************************
    //
    // ChargeTypeDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query ChargeTypes within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class ChargeTypeDB {

		string mConstr = "";

		public ChargeTypeDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // ChargeTypeDB.GetChargeTypeDetails() Method <a name="GetChargeTypeDetails"></a>
        //
        // The GetChargeTypeDetails method returns a ChargeTypeDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public ChargeTypeDetails GetChargeTypeDetails(int ctIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ChargeTypeDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCTIntNo = new SqlParameter("@CTIntNo", SqlDbType.Int, 4);
            parameterCTIntNo.Value = ctIntNo;
            myCommand.Parameters.Add(parameterCTIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			ChargeTypeDetails myChargeTypeDetails = new ChargeTypeDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myChargeTypeDetails.CTDescr = result["CTDescr"].ToString();
				myChargeTypeDetails.CType = result["CType"].ToString();
			}
			result.Close();
            return myChargeTypeDetails;
        }

		public int GetChargeTypeByType(string cType) 
		{
			// Create Instance of Connection and Command Objecs
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("ChargeTypeByType", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterCType = new SqlParameter("@CType", SqlDbType.VarChar, 15);
			parameterCType.Value = cType;
			myCommand.Parameters.Add(parameterCType);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			int ctIntNo = 0;
			while (result.Read())
			{
				// Populate Strucs using Output Params from SPROC
				ctIntNo = Convert.ToInt32(result["CTIntNo"]);
			}
			result.Close();
			return ctIntNo;
		}

        //*******************************************************
        //
        // ChargeTypeDB.AddChargeType() Method <a name="AddChargeType"></a>
        //
        // The AddChargeType method inserts a new menu record
        // into the menus database.  A unique "ChargeTypeId"
        // key is then returned from the method.  
        //
        //*******************************************************
        // 2013-07-19 comment by Henry for useless
        //public int AddChargeType (string ctCode, string ctDescr) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChargeTypeAdd", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC

        //    SqlParameter parameterCTDescr = new SqlParameter("@CTDescr", SqlDbType.VarChar, 50);
        //    parameterCTDescr.Value = ctDescr;
        //    myCommand.Parameters.Add(parameterCTDescr);

        //    SqlParameter parameterCType = new SqlParameter("@CType", SqlDbType.VarChar, 15);
        //    parameterCType.Value = ctCode;
        //    myCommand.Parameters.Add(parameterCType);

        //    SqlParameter parameterCTIntNo = new SqlParameter("@CTIntNo", SqlDbType.Int, 4);
        //    parameterCTIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterCTIntNo);

        //    try {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int ctIntNo = Convert.ToInt32(parameterCTIntNo.Value);

        //        return ctIntNo;
        //    }
        //    catch {
        //        myConnection.Dispose();
        //        return 0;
        //    }
        //}

		public SqlDataReader GetChargeTypeList(string search)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("ChargeTypeList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;


			SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
			parameterSearch.Value = search;
			myCommand.Parameters.Add(parameterSearch);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		
		public DataSet GetChargeTypeListDS(string search)
		{
			SqlDataAdapter sqlDAChargeTypes = new SqlDataAdapter();
			DataSet dsChargeTypes = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAChargeTypes.SelectCommand = new SqlCommand();
			sqlDAChargeTypes.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAChargeTypes.SelectCommand.CommandText = "ChargeTypeList";

			// Mark the Command as a SPROC
			sqlDAChargeTypes.SelectCommand.CommandType = CommandType.StoredProcedure;

			SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
			parameterSearch.Value = search;
			sqlDAChargeTypes.SelectCommand.Parameters.Add(parameterSearch);

			// Execute the command and close the connection
			sqlDAChargeTypes.Fill(dsChargeTypes);
			sqlDAChargeTypes.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsChargeTypes;		
		}

        // 2013-07-19 comment by Henry for useless
        //public int UpdateChargeType (int ctIntNo, string ctCode, string ctDescr) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChargeTypeUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterCTDescr = new SqlParameter("@CTDescr", SqlDbType.VarChar, 15);
        //    parameterCTDescr.Value = ctDescr;
        //    myCommand.Parameters.Add(parameterCTDescr);

        //    SqlParameter parameterCType = new SqlParameter("@CType", SqlDbType.VarChar, 15);
        //    parameterCType.Value = ctCode;
        //    myCommand.Parameters.Add(parameterCType);

        //    SqlParameter parameterCTIntNo = new SqlParameter("@CTIntNo", SqlDbType.Int);
        //    parameterCTIntNo.Direction = ParameterDirection.InputOutput;
        //    parameterCTIntNo.Value = ctIntNo;
        //    myCommand.Parameters.Add(parameterCTIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int CTIntNo = (int)myCommand.Parameters["@CTIntNo"].Value;
        //        //int menuId = (int)parameterCTIntNo.Value;

        //        return CTIntNo;
        //    }
        //    catch 
        //    {
        //        myConnection.Dispose();
        //        return 0;
        //    }
        //}

        // 2013-07-19 comment by Henry for useless
        //public String DeleteChargeType (int ctIntNo)
        //{

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChargeTypeDelete", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterCTIntNo = new SqlParameter("@CTIntNo", SqlDbType.Int, 4);
        //    parameterCTIntNo.Value = ctIntNo;
        //    parameterCTIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterCTIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int CTIntNo = (int)parameterCTIntNo.Value;

        //        return CTIntNo.ToString();
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return String.Empty;
        //    }
        //}
	}
}

