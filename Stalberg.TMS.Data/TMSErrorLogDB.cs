using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.IO;

namespace Stalberg.TMS
{
    /// <summary>
    /// A simple data class that encapsulates details about a particular menu 
    /// </summary>
    public class TMSErrorLogDetails 
	{
        public Int32 TELIntNo;
        public string TELAppName;
        public string TELModuleName;
        public string TELProcName;
		public DateTime TELDateTime;
		public string TELErrMessage;
        public DateTime TELDateFixed;
        public string TELFixedByUser;
    }

    /// <summary>
    /// Business/Data Logic Class that encapsulates all data
    /// logic necessary to add/login/query TMSErrorLogs within
    /// the Commerce Starter Kit Customer database.
    /// </summary>
    public class TMSErrorLogDB 
    {
        // Fields
		string mConstr = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="TMSErrorLogDB"/> class.
        /// </summary>
        /// <param name="vConstr">The v constr.</param>
        public TMSErrorLogDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // TMSErrorLogDB.GetTMSErrorLogDetails() Method <a name="GetTMSErrorLogDetails"></a>
        //
        // The GetTMSErrorLogDetails method returns a TMSErrorLogDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************
        public TMSErrorLogDetails GetTMSErrorLogDetails(Int32 telIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("TMSErrorLogDetails", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterTELIntNo = new SqlParameter("@TELIntNo", SqlDbType.Int, 4);
            parameterTELIntNo.Value = telIntNo;
            myCommand.Parameters.Add(parameterTELIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			TMSErrorLogDetails myTMSErrorLogDetails = new TMSErrorLogDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myTMSErrorLogDetails.TELAppName = result["TELAppName"].ToString();
                myTMSErrorLogDetails.TELModuleName = result["TELModuleName"].ToString();
                myTMSErrorLogDetails.TELProcName = result["TELProcName"].ToString();
                if (result["TELDateTime"] != System.DBNull.Value)
				    myTMSErrorLogDetails.TELDateTime = DateTime.Parse( result["TELDateTime"].ToString());
				myTMSErrorLogDetails.TELErrMessage = result["TELErrMessage"].ToString();
                if (result["TELDateFixed"] != System.DBNull.Value)
                    myTMSErrorLogDetails.TELDateFixed = DateTime.Parse(result["TELDateFixed"].ToString());
                myTMSErrorLogDetails.TELFixedByUser = result["TELFixedByUser"].ToString();
			}
			result.Close();
            return myTMSErrorLogDetails;
        }
        
        //*******************************************************
        //
        // TMSErrorLogDB.AddTMSErrorLog() Method <a name="TMSErrorLogAddUpdate"></a>
        //
        // The AddTMSErrorLog method inserts a new menu record
        // into the menus database.  A unique "TMSErrorLogId"
        // key is then returned from the method.  
        //
        //*******************************************************
        public Int32 AddUpdateTMSErrorLog(TMSErrorLogDetails tmsErrorLogDetails, ref string errMessage) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("TMSErrorLogAddUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@TELAppName", SqlDbType.VarChar, 50).Value = tmsErrorLogDetails.TELAppName;
            myCommand.Parameters.Add("@TELModuleName", SqlDbType.VarChar, 50).Value = tmsErrorLogDetails.TELModuleName;
            myCommand.Parameters.Add("@TELProcName", SqlDbType.VarChar, 50).Value = tmsErrorLogDetails.TELProcName;

            if (tmsErrorLogDetails.TELDateTime != DateTime.MinValue)
                myCommand.Parameters.Add("@TELDateTime", SqlDbType.SmallDateTime).Value = tmsErrorLogDetails.TELDateTime;

            myCommand.Parameters.Add("@TELErrMessage", SqlDbType.VarChar, tmsErrorLogDetails.TELErrMessage.Length).Value = tmsErrorLogDetails.TELErrMessage;

            if (tmsErrorLogDetails.TELDateTime != DateTime.MinValue)
                myCommand.Parameters.Add("@TELDateFixed", SqlDbType.SmallDateTime).Value = tmsErrorLogDetails.TELDateFixed;

            myCommand.Parameters.Add("@TELFixedByUser", SqlDbType.VarChar, 50).Value = tmsErrorLogDetails.TELFixedByUser;

            myCommand.Parameters.Add("@TELIntNo", SqlDbType.Int, 4).Value = tmsErrorLogDetails.TELIntNo;
            myCommand.Parameters["@TELIntNo"].Direction = ParameterDirection.Output;

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();

                // Calculate the CustomerID using Output Param from SPROC
                Int32 TELIntNo = Convert.ToInt32(myCommand.Parameters["@TELIntNo"].Value);

                return TELIntNo;
            }
            catch (Exception e)
            {

                myConnection.Dispose();
                errMessage = e.Message;
                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

         /// <summary>
        /// Gets the TMSErrorLog list.
        /// </summary>
        /// <returns></returns>
		public SqlDataReader GetTMSErrorLogList(string telAppName, string startDate, string endDate, bool showAll)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("TMSErrorLogList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterTELAppName = new SqlParameter("@TELAppName", SqlDbType.VarChar, 50);
            parameterTELAppName.Value = telAppName;
            myCommand.Parameters.Add(parameterTELAppName);

            SqlParameter parameterStartDate = new SqlParameter("@StartDate", SqlDbType.SmallDateTime);
            parameterStartDate.Value = startDate;
            myCommand.Parameters.Add(parameterStartDate);

            SqlParameter parameterEndDate = new SqlParameter("@EndDate", SqlDbType.SmallDateTime);
            parameterEndDate.Value = endDate;
            myCommand.Parameters.Add(parameterEndDate);

            SqlParameter parameterShowAll= new SqlParameter("@ShowAll", SqlDbType.Bit, 1);
            parameterShowAll.Value = showAll;
            myCommand.Parameters.Add(parameterShowAll);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the data reader result
			return result;
		}

        /// <summary>
        /// Gets the TMSErrorLog list DS.
        /// </summary>
        /// <returns></returns>
		public DataSet GetTMSErrorLogListDS(string telAppName, DateTime startDate, DateTime endDate, bool showAll)
		{
			SqlDataAdapter sqlDA = new SqlDataAdapter();
			DataSet ds = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDA.SelectCommand = new SqlCommand();
			sqlDA.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDA.SelectCommand.CommandText = "TMSErrorLogList";

			// Mark the Command as a SPROC
			sqlDA.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterTELAppName = new SqlParameter("@TELAppName", SqlDbType.VarChar, 50);
            parameterTELAppName.Value = telAppName;
            sqlDA.SelectCommand.Parameters.Add(parameterTELAppName);

            SqlParameter parameterStartDate = new SqlParameter("@StartDate", SqlDbType.SmallDateTime);
            parameterStartDate.Value = startDate;
            sqlDA.SelectCommand.Parameters.Add(parameterStartDate);

            SqlParameter parameterEndDate = new SqlParameter("@EndDate", SqlDbType.SmallDateTime);
            parameterEndDate.Value = endDate;
            sqlDA.SelectCommand.Parameters.Add(parameterEndDate);

            SqlParameter parameterShowAll = new SqlParameter("@ShowAll", SqlDbType.Bit, 1);
            parameterShowAll.Value = showAll;
            sqlDA.SelectCommand.Parameters.Add(parameterShowAll);

			// Execute the command and close the connection
			sqlDA.Fill(ds);
			sqlDA.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return ds;		
		}

        public void WriteToErrorLog(TMSErrorLogDetails tmsErrorLogDetails, ref StreamWriter writer)
        {
            string errMessage = string.Empty;

            try
            {
                writer.WriteLine(string.Format("{0}.{1}: {2}", tmsErrorLogDetails.TELModuleName, tmsErrorLogDetails.TELProcName, tmsErrorLogDetails.TELErrMessage));
                writer.WriteLine();
                writer.Flush();

            }
            catch { }

            Int32 telIntNo = this.AddUpdateTMSErrorLog(tmsErrorLogDetails, ref errMessage);

            if (telIntNo < 1)
            {
                writer.WriteLine("Unable to write error to database error log: " + errMessage);
                writer.WriteLine();
                writer.Flush();
            }
        }
	}
}

