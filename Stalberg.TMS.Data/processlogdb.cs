using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{
	
	public partial class ProcessLogDB 
	{
		public Int32 PLIntNo;
		public string Process;
		public DateTime startDate;
		public DateTime endDate;
		public string LastUser;
	}

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
    public partial class ProcessLogDB
    {
        string mConstr = "";

        public ProcessLogDB(string vConstr)
        {
            mConstr = vConstr;
        }

        //*******************************************************
        //
        // The GetProcessLogData method gets data from ProcessLogDB
        // 
        //
        //*******************************************************

        public DataSet  GetProcessLogData(string sProcess, DateTime start, DateTime end)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlDataAdapter sqlA = new SqlDataAdapter();
            DataSet ds = new DataSet();

            sqlA.SelectCommand = new SqlCommand();
            sqlA.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlA.SelectCommand.CommandText = "ProcessLogDetail";
            sqlA.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterProcess = new SqlParameter("@Process", SqlDbType.VarChar, 50);
            parameterProcess.Value = sProcess;
            sqlA.SelectCommand .Parameters.Add(parameterProcess);

            SqlParameter parameterStartDate = new SqlParameter("@StartDate", SqlDbType.DateTime);
            parameterStartDate.Value = start;
            sqlA.SelectCommand .Parameters.Add(parameterStartDate);

            SqlParameter parameterEndDate = new SqlParameter("@EndDate", SqlDbType.DateTime);
            parameterEndDate.Value = end;
            sqlA.SelectCommand.Parameters .Add(parameterEndDate);
                      
            // Execute the command and close the connection
            sqlA.Fill(ds);
            sqlA.SelectCommand.Connection.Dispose();
            // Return the dataset result
            return ds;	
          
        }



        //*******************************************************
        //
        // The AddProcessLogDB method inserts a new transaction number record
        // into the ProcessLog database.  A unique "PLIntNo"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddProcessLog(int nPLIntNo, string sProcess, DateTime start, DateTime end, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ProcessLogAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterProcess = new SqlParameter("@Process", SqlDbType.VarChar, 50);
            parameterProcess.Value = sProcess;
            myCommand.Parameters.Add(parameterProcess);

            SqlParameter parameterStartDate = new SqlParameter("@StartDate", SqlDbType.DateTime);
            parameterStartDate.Value = start;
            myCommand.Parameters.Add(parameterStartDate);

            SqlParameter parameterEndDate = new SqlParameter("@EndDate", SqlDbType.DateTime);
            parameterEndDate.Value = end;
            myCommand.Parameters.Add(parameterEndDate);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterPLIntNo = new SqlParameter("@PLIntNo", SqlDbType.Int, 4);
            parameterPLIntNo.Value = nPLIntNo;
            parameterPLIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterPLIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                // Calculate the ID using Output Param from SPROC
                int nReturn = (int)parameterPLIntNo.Value;
                return nReturn;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        //*******************************************************
        //
        // The UpdateProcessLogDB method updates an exiting transaction number record
        // into the ProcessLog database.  
        //
        //*******************************************************

        public int UpdateProcessLog(int nPLIntNo, string sProcess, DateTime end, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ProcessLogUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterProcess = new SqlParameter("@Process", SqlDbType.VarChar, 50);
            parameterProcess.Value = sProcess;
            myCommand.Parameters.Add(parameterProcess);

            SqlParameter parameterEndDate = new SqlParameter("@EndDate", SqlDbType.DateTime);
            parameterEndDate.Value = end;
            myCommand.Parameters.Add(parameterEndDate);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterPLIntNo = new SqlParameter("@PLIntNo", SqlDbType.Int, 4);
            parameterPLIntNo.Value = nPLIntNo;
            parameterPLIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterPLIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the ID using Output Param from SPROC
                int nReturn = (int)parameterPLIntNo.Value;
                return nReturn;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }



        public DateTime  GetProcessTime(string sProcess)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ProcessLogMaxDateForProcess", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterProcess = new SqlParameter("@ProcessName", SqlDbType.VarChar, 50);
            parameterProcess.Value = sProcess;
            myCommand.Parameters.Add(parameterProcess);

            SqlParameter parameterEndTime = new SqlParameter("@MaxDate", SqlDbType.SmallDateTime);            
            parameterEndTime.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterEndTime);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                //end = Convert.ToDateTime(parameterEndTime.Value);
                return Convert.ToDateTime(parameterEndTime.Value);
            }

            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return DateTime.MinValue;
            }
        }
    }
		
}
