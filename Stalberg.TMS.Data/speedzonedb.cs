using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // SpeedZoneDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class SpeedZoneDetails 
	{
        public int SZSpeed;
		public string SZDescr;
    }

    //*******************************************************
    //
    // SpeedZoneDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query SpeedZones within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class SpeedZoneDB {

		string mConstr = "";

		public SpeedZoneDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // SpeedZoneDB.GetSpeedZoneDetails() Method <a name="GetSpeedZoneDetails"></a>
        //
        // The GetSpeedZoneDetails method returns a SpeedZoneDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public SpeedZoneDetails GetSpeedZoneDetails(int szIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("SpeedZoneDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterSZIntNo = new SqlParameter("@SZIntNo", SqlDbType.Int, 4);
            parameterSZIntNo.Value = szIntNo;
            myCommand.Parameters.Add(parameterSZIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			SpeedZoneDetails mySpeedZoneDetails = new SpeedZoneDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				mySpeedZoneDetails.SZSpeed = Convert.ToInt32(result["SZSpeed"]);
				mySpeedZoneDetails.SZDescr = result["SZDescr"].ToString();
			}
			result.Close();
            return mySpeedZoneDetails;
        }

        //*******************************************************
        //
        // SpeedZoneDB.AddSpeedZone() Method <a name="AddSpeedZone"></a>
        //
        // The AddSpeedZone method inserts a new menu record
        // into the menus database.  A unique "SpeedZoneId"
        // key is then returned from the method.  
        //
        //*******************************************************
        // 2013-07-26 add parameter lastUser by Henry
        public int AddSpeedZone (int szSpeed, string szDescr, string lastUser) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("SpeedZoneAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
			SqlParameter parameterSZSpeed = new SqlParameter("@SZSpeed", SqlDbType.Int, 4);
			parameterSZSpeed.Value = szSpeed;
			myCommand.Parameters.Add(parameterSZSpeed);

			SqlParameter parameterSZDescr = new SqlParameter("@SZDescr", SqlDbType.VarChar, 30);
			parameterSZDescr.Value = szDescr;
			myCommand.Parameters.Add(parameterSZDescr);

            SqlParameter parameterSZIntNo = new SqlParameter("@SZIntNo", SqlDbType.Int, 4);
            parameterSZIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterSZIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int szIntNo = Convert.ToInt32(parameterSZIntNo.Value);

                return szIntNo;
            }
            catch {
				myConnection.Dispose();
                return 0;
            }
        }

		public SqlDataReader GetSpeedZoneList()
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("SpeedZoneList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		
		public DataSet GetSpeedZoneListDS()
		{
			SqlDataAdapter sqlDASpeedZones = new SqlDataAdapter();
			DataSet dsSpeedZones = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDASpeedZones.SelectCommand = new SqlCommand();
			sqlDASpeedZones.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDASpeedZones.SelectCommand.CommandText = "SpeedZoneList";

			// Mark the Command as a SPROC
			sqlDASpeedZones.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Execute the command and close the connection
			sqlDASpeedZones.Fill(dsSpeedZones);
			sqlDASpeedZones.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsSpeedZones;		
		}

		// 2013-07-26 add parameter lastUser by Henry
		public int UpdateSpeedZone (int szIntNo, int szSpeed, string szDescr, string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("SpeedZoneUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterSZSpeed = new SqlParameter("@SZSpeed", SqlDbType.Int, 4);
			parameterSZSpeed.Value = szSpeed;
			myCommand.Parameters.Add(parameterSZSpeed);

			SqlParameter parameterSZDescr = new SqlParameter("@SZDescr", SqlDbType.VarChar, 30);
			parameterSZDescr.Value = szDescr;
			myCommand.Parameters.Add(parameterSZDescr);

			SqlParameter parameterSZIntNo = new SqlParameter("@SZIntNo", SqlDbType.Int);
			parameterSZIntNo.Direction = ParameterDirection.InputOutput;
			parameterSZIntNo.Value = szIntNo;
			myCommand.Parameters.Add(parameterSZIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int SZIntNo = (int)myCommand.Parameters["@SZIntNo"].Value;
				//int menuId = (int)parameterSZIntNo.Value;

				return SZIntNo;
			}
			catch 
			{
				myConnection.Dispose();
				return 0;
			}
		}

		
		public String DeleteSpeedZone (int szIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("SpeedZoneDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterSZIntNo = new SqlParameter("@SZIntNo", SqlDbType.Int, 4);
			parameterSZIntNo.Value = szIntNo;
			parameterSZIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterSZIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int SZIntNo = (int)parameterSZIntNo.Value;

				return SZIntNo.ToString();
			}
			catch 
			{
				myConnection.Dispose();
				return String.Empty;
			}
		}
	}
}

