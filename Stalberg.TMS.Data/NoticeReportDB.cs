using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Stalberg.TMS_TPExInt.Components;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS
{
    /// <summary>
    /// Contains the methods for interacting with the Notice table in the database Report data
    /// </summary>
    public class NoticeReportDB
    {
        // Fields
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="NoticeReportDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public NoticeReportDB(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Gets a set of results from searching notice.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="colName">Name of the column.</param>
        /// <param name="colValue">The col value.</param>
        /// <param name="dateSince">The date since.</param>
        /// <returns>A <see cref="DataSet"/>.</returns>
        public DataSet GetNoticeSearchDS(int autIntNo, string colName, string colValue, DateTime dateSince)
        {
            return this.GetNoticeSearchDS(autIntNo, colName, colValue, dateSince, 255);
        }

        /// <summary>
        /// Gets the notice search DS.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="colName">Name of the col.</param>
        /// <param name="colValue">The col value.</param>
        /// <param name="dateSince">The date since.</param>
        /// <param name="minStatus">The min status.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        //2013-04-09 add by Henry for pagination
        public DataSet GetNoticeSearchDS(int autIntNo, string colName, string colValue, DateTime dateSince, int minStatus)
        {
            int totalCount = 0;
            return GetNoticeSearchDS(autIntNo, colName, colValue, dateSince, minStatus, 0, 0, out totalCount);
        }

        public DataSet GetNoticeSearchDS(int autIntNo, string colName, string colValue, DateTime dateSince, int minStatus,
            int pageSize, int pageIndex, out int totalCount) //2013-04-09 add by Henry for pagination
        {
            // Create SQL data access objects
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = new SqlCommand("NoticeSearch", new SqlConnection(this.connectionString));
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            da.SelectCommand.Parameters.Add("@ColumnName", SqlDbType.VarChar, 10).Value = colName;
            da.SelectCommand.Parameters.Add("@ColumnValue", SqlDbType.VarChar, 30).Value = colValue;
            da.SelectCommand.Parameters.Add("@DateSince", SqlDbType.DateTime, 8).Value = dateSince;
            da.SelectCommand.Parameters.Add("@MinStatus", SqlDbType.Int, 4).Value = minStatus;

            da.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            da.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            da.SelectCommand.Parameters.Add(paraTotalCount);

            // Execute the command and fill the data set
            dsOfenceSummary ds = new dsOfenceSummary();
            da.Fill(ds);
            da.SelectCommand.Connection.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            // Return the data set result
            return ds;
        }

        /// <summary>
        /// Gets the notice search DS.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="colName">Name of the col.</param>
        /// <param name="colValue">The col value.</param>
        /// <param name="dateSince">The date since.</param>
        /// <param name="minStatus">The min status.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetNoticeSearchDS_GetPaged(int autIntNo, string colName, string colValue, DateTime dateSince, int minStatus, int pageIndex, int pageSize, out int totalCount)
        {
             totalCount = 0;
            // Create SQL data access objects
            SqlDataAdapter da = new SqlDataAdapter();
            //2014-08-13 Heidi changed for fixing time out issue.(bontq1298)
            da.SelectCommand = new SqlCommand("NoticeSearch_GetPaged", new SqlConnection(this.connectionString)) { CommandTimeout = 0};
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            da.SelectCommand.Parameters.Add("@ColumnName", SqlDbType.VarChar, 10).Value = colName;
            da.SelectCommand.Parameters.Add("@ColumnValue", SqlDbType.VarChar, 30).Value = colValue;
            da.SelectCommand.Parameters.Add("@DateSince", SqlDbType.DateTime, 8).Value = dateSince;
            da.SelectCommand.Parameters.Add("@MinStatus", SqlDbType.Int, 4).Value = minStatus;

            da.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int, 4).Value = pageIndex;
            da.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int, 4).Value = pageSize;
            SqlParameter spTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int, 4);
            spTotalCount.Direction = ParameterDirection.Output;
            da.SelectCommand.Parameters.Add(spTotalCount); 

            // Execute the command and fill the data set
            dsOfenceSummary ds = new dsOfenceSummary();
            da.Fill(ds);
            da.SelectCommand.Connection.Dispose();

            totalCount = (int)spTotalCount.Value;

            // Return the data set result
            return ds;
        }

        /// <summary>
        /// Gets a set of results from searching notice.
        /// </summary>
        /// <param name="nNotIntNo">NotIntNo.</param>
        /// <returns>A <see cref="DataSet"/>.</returns>
        public DataSet HistoryPrint(int nNotIntNo)
        {
            // Create SQL data access objects
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = new SqlCommand("HistoryPrint", new SqlConnection(this.connectionString));
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            da.SelectCommand.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = nNotIntNo ;
            // Execute the command and fill the data set
            dsEvidenceHistory ds = new dsEvidenceHistory();

            da.Fill(ds);
            da.SelectCommand.Connection.Dispose();

            // Return the data set result
            return ds;
        }

        /// <summary>
        /// Gets the details of an individual notice.
        /// </summary>
        /// <param name="notIntNo">The notice int no.</param>
        /// <returns>A strongly typed data set</returns>
        public DataSet GetNoticeReportDetails(int notIntNo)
        {
            // Setup he SQL data objects
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("ViewNoticeDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;

            // Fill a data set
            SqlDataAdapter da = new SqlDataAdapter(com);
            dsOffenceDetails ds = new dsOffenceDetails();
            da.Fill(ds);

            // Return the data set
            return ds;
        }

        /// <summary>
        /// Gets the summons server error list.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetSummonsServerErrorList(int autIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SummonsServerErrors", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            SqlDataAdapter da = new SqlDataAdapter(com);
            dsSummonsServerErrors ds = new dsSummonsServerErrors();
            da.Fill(ds);

            return ds;
        }

        /// <summary>
        /// Gets the list of summons with no court dates.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetSummonsNoCourtDates(int autIntNo, int noDays)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SummonsCourtDateErrors", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@NoDays", SqlDbType.Int, 4).Value = noDays;

            SqlDataAdapter da = new SqlDataAdapter(com);
            dsSummonsCourtDateErrors ds = new dsSummonsCourtDateErrors();
            da.Fill(ds);

            return ds;
        }

        /// <summary>
        /// Gets a <see cref="DataSet"/> containing a ciprus notice re-print.
        /// </summary>
        /// <param name="notIntNo">The notice int no.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetCiprusNoticeRePrint(int notIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand cmd = new SqlCommand("NoticeRePrintCiprus", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

    }
}