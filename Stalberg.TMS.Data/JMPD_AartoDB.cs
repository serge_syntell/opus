﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace Stalberg.TMS.Data
{
    public class JMPD_AartoDB
    {

        // Fields
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="CiprusData"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public JMPD_AartoDB(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DataSet GetAartoInfringementData(int notIntNo, int statusLoaded, int statusFixed)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);

            SqlCommand myCommand = new SqlCommand("NoticeInfringementData_JMPD", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            //dls 070720 - allow for timeouts
            myCommand.CommandTimeout = 0;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = notIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
            parameterStatusLoaded.Value = statusLoaded;
            myCommand.Parameters.Add(parameterStatusLoaded);

            SqlParameter parameterStatusFixed = new SqlParameter("@StatusFixed", SqlDbType.Int, 4);
            parameterStatusFixed.Value = statusFixed;
            myCommand.Parameters.Add(parameterStatusFixed);

            SqlDataAdapter da = new SqlDataAdapter(myCommand);
            DataSet ds = new DataSet();

            try
            {
                if (myConnection.State == ConnectionState.Closed)
                    myConnection.Open();
                da.Fill(ds);
            }
            finally
            {
                myConnection.Close();
                myConnection.Dispose();
                myCommand.Dispose();
            }

            return ds;
        }

        public DataSet GetAartoInfringementDataList(int autIntNo, int statusLoaded, int statusFixed)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);

            SqlCommand myCommand = new SqlCommand("NoticeInfringementDataList_JMPD", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            //dls 070720 - allow for timeouts
            myCommand.CommandTimeout = 0;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
            parameterStatusLoaded.Value = statusLoaded;
            myCommand.Parameters.Add(parameterStatusLoaded);

            SqlParameter parameterStatusFixed = new SqlParameter("@StatusFixed", SqlDbType.Int, 4);
            parameterStatusFixed.Value = statusFixed;
            myCommand.Parameters.Add(parameterStatusFixed);

            SqlDataAdapter da = new SqlDataAdapter(myCommand);
            DataSet ds = new DataSet();
            try
            {
                if (myConnection.State == ConnectionState.Closed)
                    myConnection.Open();
                da.Fill(ds);
            }
            finally
            {
                myConnection.Close();
                myConnection.Dispose();
                myCommand.Dispose();
            }
            

            return ds;
        }
        // 2013-07-25 comment out by Henry for useless
        //public int UpdateInfringementDataChargeStatus(int NotIntNo, int status, Guid guid, string lastUser)
        //{
        //    int result = 0;

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    //if (myConnection.State == ConnectionState.Closed)
        //    //    myConnection.Open();

        //    SqlCommand myCommand = new SqlCommand("NoticeChargeUpdateStatus_JMPD", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;
        //    myCommand.CommandTimeout = 0;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
        //    parameterNotIntNo.Value = NotIntNo;
        //    myCommand.Parameters.Add(parameterNotIntNo);

        //    SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
        //    parameterStatus.Value = status;
        //    myCommand.Parameters.Add(parameterStatus);

        //    SqlParameter parameterGuid = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier);
        //    parameterGuid.Value = guid;
        //    myCommand.Parameters.Add(parameterGuid);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    try
        //    {
        //        myConnection.Open();

        //        result = myCommand.ExecuteNonQuery();
        //    }
        //    catch (Exception e)
        //    {
        //        string msg = e.Message;
        //        result = -1;
        //    }
        //    finally
        //    {
        //        myConnection.Close();
        //    }
        //    return result;
        //}
        ///  2013-07-25 comment out by Henry for useless
        //public int UpdateMissingFieldInfringementDataChargeStatus(int NotIntNo, int status, int Code, string strDesc)
        //{
        //    int result = 0;

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(connectionString);
        //    //if (myConnection.State == ConnectionState.Closed)
        //    //    myConnection.Open();

        //    SqlCommand myCommand = new SqlCommand("NoticeChargeMissingFieldUpdateStatus_JMPD", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;
        //    myCommand.CommandTimeout = 0;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
        //    parameterNotIntNo.Value = NotIntNo;
        //    myCommand.Parameters.Add(parameterNotIntNo);

        //    SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
        //    parameterStatus.Value = status;
        //    myCommand.Parameters.Add(parameterStatus);

        //    SqlParameter parameterCode = new SqlParameter("@Code", SqlDbType.Int, 4);
        //    parameterCode.Value = Code;
        //    myCommand.Parameters.Add(parameterCode);

        //    SqlParameter parameterDesc = new SqlParameter("@Description", SqlDbType.VarChar, 100);
        //    parameterDesc.Value = strDesc;
        //    myCommand.Parameters.Add(parameterDesc);

        //    try
        //    {
        //        myConnection.Open();

        //        result = myCommand.ExecuteNonQuery();
        //    }
        //    catch (Exception e)
        //    {
        //        string msg = e.Message;
        //        result = -1;
        //    }
        //    finally
        //    {
        //        myConnection.Close();
        //    }
        //    return result;
        //}


        public int UpdateAartoResponse(int status, Guid guid, string NotTicketNo, string strLastUser, int Code, string strDesc)
        {
            int result = 0;

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            //if (myConnection.State == ConnectionState.Closed)
            //    myConnection.Open();

            SqlCommand myCommand = new SqlCommand("NoticeUpdateResponse_JMPD", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            myCommand.CommandTimeout = 0;

            int NotIntNo =0;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = NotIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
            parameterStatus.Value = status;
            myCommand.Parameters.Add(parameterStatus);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = strLastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterGuid = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier);
            parameterGuid.Value = guid;
            myCommand.Parameters.Add(parameterGuid);

            SqlParameter parameterNotTicketNo = new SqlParameter("@NotTicketNo", SqlDbType.VarChar, 50);
            parameterNotTicketNo.Value = NotTicketNo;
            myCommand.Parameters.Add(parameterNotTicketNo);

            SqlParameter parameterCode = new SqlParameter("@Code", SqlDbType.Int, 4);
            parameterCode.Value = Code;
            myCommand.Parameters.Add(parameterCode);

            SqlParameter parameterDesc = new SqlParameter("@Description", SqlDbType.VarChar, 100);
            parameterDesc.Value = strDesc;
            myCommand.Parameters.Add(parameterDesc);

            try
            {
                myConnection.Open();

                result = myCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                string msg = e.Message;
                result = -1;
            }
            finally
            {
                myConnection.Close();
            }
            return result;
        }

    }
}
