using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Stalberg.TMS
{

    /// <summary>
    /// The notice DB class contains all the database access logic for retrieving notices
    /// </summary>
    public class NoticeOfWoaDB
    {
        string mConstr = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:woaDB"/> class.
        /// </summary>
        /// <param name="vConstr">The database connection string.</param>
        public NoticeOfWoaDB(string vConstr)
        {
            mConstr = vConstr;
        }

        //dls 081103 - moved out of woaDB to here
        //mrs 20081019 added to return notice of woa print files
        public DataSet GetNoticeOfWOAPrintrunDS(int autIntNo, 
            int pageSize, int pageIndex, out int totalCount)    //2013-04-09 add by Henry for pagination
        {
            SqlDataAdapter sqlPrintrun = new SqlDataAdapter();
            DataSet dsPrintrun = new DataSet();

            // Create Instance of Connection and Command Object
            sqlPrintrun.SelectCommand = new SqlCommand();
            sqlPrintrun.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlPrintrun.SelectCommand.CommandText = "NoticeOfWoaPrintFile";

            // Mark the Command as a SPROC
            sqlPrintrun.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            sqlPrintrun.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            sqlPrintrun.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            sqlPrintrun.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            sqlPrintrun.SelectCommand.Parameters.Add(paraTotalCount);

            // Execute the command and close the connection
            sqlPrintrun.Fill(dsPrintrun);
            sqlPrintrun.SelectCommand.Connection.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            // Return the dataset result
            return dsPrintrun;
        }

        public int GetNoOfSummonForNoticeOfWOA(int autIntNo, int noOfDaysBeforeNoticeofWOA)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("SummonsForNoticeOfWOAForAuth", con);

            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@NoOfDaysBeforeNoticeofWOA", SqlDbType.Int, 4).Value = noOfDaysBeforeNoticeofWOA;
            com.Parameters.Add("@NoOfSummons", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;

            
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                int noOfSummons = (int)com.Parameters["@NoOfSummons"].Value;
                return noOfSummons;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return -1;
            }
            finally
            {
                con.Dispose();
            }

        }

        public int CheckSummonForNoticeOfWOA(int sumIntNo, int noOfDaysBeforeNoticeofWOA)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("SummonsForNoticeOfWOA_WS", con);

            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;
            com.Parameters.Add("@NoOfDaysBeforeNoticeofWOA", SqlDbType.Int, 4).Value = noOfDaysBeforeNoticeofWOA;
            com.Parameters.Add("@NoOfSummons", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;


            try
            {
                con.Open();
                com.ExecuteNonQuery();
                int noOfSummons = (int)com.Parameters["@NoOfSummons"].Value;
                return noOfSummons;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return -1;
            }
            finally
            {
                con.Dispose();
            }

        }


        public int SetNoticeOfWoaAsPrinted(string printFile, bool isPrinted, string lastUser, ref string errorMsg)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("SetPrintedStatusOfNoticeOfWoa", con);

            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@OnOf", SqlDbType.Bit, 1).Value = isPrinted;
            com.Parameters.Add("@NoticeOfWoaPrintFileName", SqlDbType.VarChar, 50).Value = printFile;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser; //mrs 20081107 added
            com.Parameters.Add("@Success", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                return (int)com.Parameters["@Success"].Value;
            }
            catch (Exception e)
            {
                errorMsg = e.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        public int SetNoticeOfWoaAsPosted(string printFile, bool isPosted, string lastUser, ref string errorMsg)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            //mrs 20081107 wrong proc called
            SqlCommand com = new SqlCommand("SetPostedStatusOfNoticeOfWoa", con);

            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@OnOf", SqlDbType.Bit, 1).Value = isPosted;
            com.Parameters.Add("@NoticeOfWoaPrintFileName", SqlDbType.VarChar, 50).Value = printFile;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser; //mrs 20081107 added
            com.Parameters.Add("@Success", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                return (int)com.Parameters["@Success"].Value;
            }
            catch (Exception e)
            {
                errorMsg = e.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }

        // 2013-07-25 comment out by Henry for useless
        //public int UpdateSummonsListForNoticeOfWOA(int autIntNo, int noOfDays, string printFileName, string lastUser)
        //{
        //    //mrs 20081102 sets the printfile name on all summons rows at status 810
        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("NoticeOfWOAPrintStatusUpdate", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    com.Parameters.Add("@NoOfDaysBeforeNoticeofWOA", SqlDbType.Int,4).Value = noOfDays;
        //    com.Parameters.Add("@PrintFileName", SqlDbType.VarChar, 50).Value = printFileName;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
        //    com.Parameters.Add("@Count", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();
        //        return (int)com.Parameters["@Count"].Value;
        //    }
        //    catch (Exception ex)
        //    {
        //        string message = ex.Message;
        //            com.Dispose();
        //        return -1;
        //    }
        //    finally
        //    {
        //        con.Dispose();
        //    }
        //}

        public int UpdateSummonsForNoticeOfWOA(int sumIntNo, int noOfDays, string printFileName, string lastUser)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NoticeOfWOAPrintStatusUpdate_WS", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@sumIntNo", SqlDbType.Int, 4).Value = sumIntNo;
            com.Parameters.Add("@NoOfDaysBeforeNoticeofWOA", SqlDbType.Int, 4).Value = noOfDays;
            com.Parameters.Add("@PrintFileName", SqlDbType.VarChar, 50).Value = printFileName;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@Count", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                return (int)com.Parameters["@Count"].Value;
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                com.Dispose();
                return -1;
            }
            finally
            {
                con.Dispose();
            }
        }
    }
}