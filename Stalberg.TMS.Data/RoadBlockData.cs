﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using Stalberg.TMS;

namespace Stalberg.TMS.Data
{
    public class RoadBlockData      //Road block record - Down file
    {
        //added elements to struct
        public string recType;
        public string filter1; 
        public string localauthCode;
        public string filter2;
        public string filter3; 
        public string noticeNo;        
        public string recIdentifier;                
        public string filmNo;
        public string frameNo;
        public string OffenderIdentityNo;
        public string surname;
        public string initials;      
        public string vehicleNo;
        public string offenceDate;      
        public string offenceTime;      
        public string firstSpeedReading;        
        public string locationOffended;        
        public string courtCode;
        public string courtDate;  
        public string caseNo;      
        public string summonsNo;
        public string summonsIssueDate;                   
        public string woaNo;
        public string woaPrintedDate;  
        public string representation;  
        public string offenceCode1;
        public string chargeType1;   
        public string fineAmount1;    
        public string chargeResult1;
        public string offenceCode2;
        public string chargeType2;
        public string fineAmount2;
        public string chargeResult2;
        public string offenceCode3;
        public string chargeType3;
        public string fineAmount3;
        public string chargeResult3;
        public string offenceCode4;
        public string chargeType4;
        public string fineAmount4;
        public string chargeResult4;
        public string offenceCode5;
        public string chargeType5;
        public string fineAmount5;
        public string chargeResult5;





        /// <summary>
        /// Writes the file output of this record.
        /// </summary>
        /// <returns>A <see cref="String"/> containing the output of the record</returns>
        public string Write()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(this.recType);
            sb.Append(",");
            sb.Append(this.filter1);
            sb.Append(",");
            sb.Append(this.localauthCode);
            sb.Append(",");
            sb.Append(this.filter2);
            sb.Append(",");
            sb.Append(this.filter3);
            sb.Append(",");
            sb.Append(this.noticeNo);
            sb.Append(",");
            sb.Append(this.recIdentifier);
            sb.Append(",");
            sb.Append(this.filmNo);
            sb.Append(",");
            sb.Append(this.frameNo);
            sb.Append(",");
            sb.Append(this.OffenderIdentityNo);
            sb.Append(",");
            sb.Append(this.surname);
            sb.Append(",");
            sb.Append(this.initials);
            sb.Append(",");
            sb.Append(this.vehicleNo);
            sb.Append(",");
            sb.Append(this.offenceDate.ToString());
            sb.Append(",");
            sb.Append(this.offenceTime.ToString());
            sb.Append(",");
            sb.Append(this.firstSpeedReading);
            sb.Append(",");
            sb.Append(this.locationOffended);
            sb.Append(",");
            sb.Append(this.courtCode);
            sb.Append(",");
            sb.Append(this.courtDate);
            sb.Append(",");
            sb.Append(this.caseNo);
            sb.Append(",");
            sb.Append(this.summonsNo);
            sb.Append(",");
            sb.Append(this.summonsIssueDate);
            sb.Append(",");
            sb.Append(this.woaNo);
            sb.Append(",");
            sb.Append(this.woaPrintedDate);
            sb.Append(",");
            sb.Append(this.representation);
            sb.Append(",");
            sb.Append(this.offenceCode1);
            sb.Append(",");
            sb.Append(this.chargeType1);
            sb.Append(",");
            sb.Append(this.fineAmount1);
            sb.Append(",");
            sb.Append(this.chargeResult1);
            sb.Append(",");
            sb.Append(this.offenceCode2);
            sb.Append(",");
            sb.Append(this.chargeType2);
            sb.Append(",");
            sb.Append(this.fineAmount2);
            sb.Append(",");
            sb.Append(this.chargeResult2);
            sb.Append(",");
            sb.Append(this.offenceCode3);
            sb.Append(",");
            sb.Append(this.chargeType3);
            sb.Append(",");
            sb.Append(this.fineAmount3);
            sb.Append(",");
            sb.Append(this.chargeResult3);
            sb.Append(",");
            sb.Append(this.offenceCode4);
            sb.Append(",");
            sb.Append(this.chargeType4);
            sb.Append(",");
            sb.Append(this.fineAmount4);
            sb.Append(",");
            sb.Append(this.chargeResult4);
            sb.Append(",");
            sb.Append(this.offenceCode5);
            sb.Append(",");
            sb.Append(this.chargeType5);
            sb.Append(",");
            sb.Append(this.fineAmount5);
            sb.Append(",");
            sb.Append(this.chargeResult5);
            sb.Append((char)Convert.ToInt32("0D", 16));
            sb.Append((char)Convert.ToInt32("0A", 16));
            return sb.ToString();
        }
    }

   
    /// <summary>
    /// Contains the database logic for interacting with road block data
    /// </summary>
    public class RoadBlockRecord
    {
        // Fields
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="CiprusData"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public RoadBlockRecord(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public SqlDataReader GetRoadBlockData(string arString, string mtrCode, out SqlConnection connection)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("RoadblockData", myConnection);

            //Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            //Allow for timeouts
            myCommand.CommandTimeout = 0;

            //Add Parameters to SPROC
            SqlParameter parameterARString = new SqlParameter("@ARString", SqlDbType.Char, 3);
            parameterARString.Value = arString;
            myCommand.Parameters.Add(parameterARString);

            SqlParameter parameterMTRCode = new SqlParameter("@MTRCode", SqlDbType.Char, 3);
            parameterMTRCode.Value = mtrCode;
            myCommand.Parameters.Add(parameterMTRCode);

            try
            {
                connection = myConnection;
                myConnection.Open();
                SqlDataReader reader = myCommand.ExecuteReader();
                return reader;
                
            
            }
            catch (Exception e)
            {
                connection=null;
                string errorMsg = e.Message;
                myConnection.Dispose();
                return null;
            }
            //finally
            //{
            //    myConnection.Dispose();
            //}
        }
        

    }
}
