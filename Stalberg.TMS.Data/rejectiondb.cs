using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


namespace Stalberg.TMS
{
     //*******************************************************
    // Update by Jake 04/07/2009 
    //Description : Add new Field (NatisBasketSkip)
    // RejectionDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class RejectionDetails
    {
        public Int32 RejIntNo;
        public string RejReason;
        public string RejSkip;
        public Int32 RejCIntNo; //mrs 20081126 added
        public string NatisBasketSkip;
    }

    //SD: 15.07.2008

    public partial class RejectionCategoryDetails
    {
        public Int32 RejCIntNo; //mrs 20081126 the PK is RejCIntNo not RejIntNo
        public string RejCategory;
        public string RejSkip;
    }
    //*******************************************************
    //
    // RejectionDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query Rejections within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class RejectionDB
    {

        string mConstr = "";

        public RejectionDB(string vConstr)
        {
            mConstr = vConstr;
        }

        //*******************************************************
        //
        // RejectionDB.GetRejectionDetails() Method <a name="GetRejectionDetails"></a>
        //
        // The GetRejectionDetails method returns a RejectionDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public RejectionDetails GetRejectionDetails(int rejIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RejectionDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterRejIntNo = new SqlParameter("@RejIntNo", SqlDbType.Int, 4);
            parameterRejIntNo.Value = rejIntNo;
            myCommand.Parameters.Add(parameterRejIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            RejectionDetails myRejectionDetails = new RejectionDetails();

            int regCIntNo;
            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myRejectionDetails.RejReason = result["RejReason"].ToString();
                myRejectionDetails.RejSkip = result["RejSkip"].ToString();
                regCIntNo = result["RejCIntNo"] == DBNull.Value || string.IsNullOrEmpty(result["RejCIntNo"].ToString()) ?
                    0 : Convert.ToInt32(result["RejCIntNo"].ToString());
                myRejectionDetails.RejCIntNo = regCIntNo;
                myRejectionDetails.NatisBasketSkip = result["NatisBasketSkip"] == DBNull.Value ? string.Empty : result["NatisBasketSkip"].ToString();
            }
            result.Close();
            return myRejectionDetails;
        }

        //*******************************************************
        //
        // RejectionDB.AddRejection() Method <a name="AddRejection"></a>
        //
        // The AddRejection method inserts a new menu record
        // into the menus database.  A unique "RejectionId"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddRejection(string rejReason, string rejSkip, string lastUser, int rejCIntNo, string natisBSkip) //mrs 20081126 added rejCIntNo

        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RejectionAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC

            SqlParameter parameterRejReason = new SqlParameter("@RejReason", SqlDbType.VarChar, 100);
            parameterRejReason.Value = rejReason;
            myCommand.Parameters.Add(parameterRejReason);

            SqlParameter parameterRejSkip = new SqlParameter("@RejSkip", SqlDbType.Char, 1);
            parameterRejSkip.Value = rejSkip;
            myCommand.Parameters.Add(parameterRejSkip);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);
            SqlParameter parameterRejCIntNo = new SqlParameter("@RejCIntNo", SqlDbType.Int, 4);
            parameterRejCIntNo.Value = rejCIntNo;
            myCommand.Parameters.Add(parameterRejCIntNo);

            SqlParameter parameterNatisBSkip = new SqlParameter("@NatisBasketSkip", SqlDbType.Char, 1);
            parameterNatisBSkip.Value = natisBSkip;
            myCommand.Parameters.Add(parameterNatisBSkip);

            SqlParameter parameterRejIntNo = new SqlParameter("@RejIntNo", SqlDbType.Int, 4);
            parameterRejIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterRejIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int rejIntNo = Convert.ToInt32(parameterRejIntNo.Value);

                return rejIntNo;
            }
            catch
            {
                myConnection.Dispose();
                return 0;
            }
        }

        public SqlDataReader GetRejectionList()
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RejectionList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }


        public DataSet GetRejectionListDS()
        {
            SqlDataAdapter sqlDARejReasons = new SqlDataAdapter();
            DataSet dsRejReasons = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDARejReasons.SelectCommand = new SqlCommand();
            sqlDARejReasons.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDARejReasons.SelectCommand.CommandText = "RejectionList";

            // Mark the Command as a SPROC
            sqlDARejReasons.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command and close the connection
            sqlDARejReasons.Fill(dsRejReasons);
            sqlDARejReasons.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsRejReasons;
        }

        //mrs 20081126 added FK to rejectCategory
        public int UpdateRejection(int rejIntNo,  string rejReason, string rejSkip, string lastUser, int rejCIntNo,  string natisBSkip)

        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RejectionUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterRejReason = new SqlParameter("@RejReason", SqlDbType.VarChar, 100);
            parameterRejReason.Value = rejReason;
            myCommand.Parameters.Add(parameterRejReason);

            SqlParameter parameterRejSkip = new SqlParameter("@RejSkip", SqlDbType.Char, 1);
            parameterRejSkip.Value = rejSkip;
            myCommand.Parameters.Add(parameterRejSkip);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterRejCIntNo = new SqlParameter("@RejCIntNo", SqlDbType.Int, 4);
            parameterRejCIntNo.Value = rejCIntNo;
            myCommand.Parameters.Add(parameterRejCIntNo);

            SqlParameter parameterNatisBskip = new SqlParameter("@NatisBasketSkip", SqlDbType.Char, 1);
            parameterNatisBskip.Value = natisBSkip;
            myCommand.Parameters.Add(parameterNatisBskip);

            SqlParameter parameterRejIntNo = new SqlParameter("@RejIntNo", SqlDbType.Int);
            parameterRejIntNo.Direction = ParameterDirection.InputOutput;
            parameterRejIntNo.Value = rejIntNo;
            myCommand.Parameters.Add(parameterRejIntNo);

            try
            {
                if (myConnection.State != ConnectionState.Open)
                    myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int RejIntNo = (int)myCommand.Parameters["@RejIntNo"].Value;
                //int menuId = (int)parameterRejIntNo.Value;

                return RejIntNo;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                myConnection.Dispose();
                return 0;
            }
        }


        public int DeleteRejectionCategory(int rejIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RejectionCategoryDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterRejIntNo = new SqlParameter("@RejCIntNo", SqlDbType.Int, 4);
            parameterRejIntNo.Value = rejIntNo;
            parameterRejIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterRejIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int RejIntNo = (int)parameterRejIntNo.Value;

                return RejIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }



        //SD : 14.07.2008
        //*******************************************************
        //
        // RejectionDB.GetRejectionListCategory() Method
        //
        // The GetRejectionListCategory fetches the Rejection Category
        // list from RejectCategory Table to bind the list to a Datagrid
        // on Rejection.aspx page
        //
        //*******************************************************


        public SqlDataReader GetRejectionListCategory()
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RejectionCategoryList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }


        // SD: 14.07.2008

        public DataSet GetRejectionCategoryDS()
        {

            SqlDataAdapter sqlDARejCategory = new SqlDataAdapter();
            DataSet dsRejCategory = new DataSet();

            sqlDARejCategory.SelectCommand = new SqlCommand();
            sqlDARejCategory.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDARejCategory.SelectCommand.CommandText = "RejectionCategoryList";

            sqlDARejCategory.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDARejCategory.Fill(dsRejCategory);
            sqlDARejCategory.SelectCommand.Connection.Dispose();
            return dsRejCategory;
                                 
        }

        public DataSet GetRejectionCategoryDetails(int RejCatIntNo)
        {

            SqlDataAdapter sqlDARejCategory = new SqlDataAdapter();
            DataSet dsRejCategory = new DataSet();

            sqlDARejCategory.SelectCommand = new SqlCommand();
            sqlDARejCategory.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDARejCategory.SelectCommand.CommandText = "GetRejectionsByRejectionCategoryId";

            sqlDARejCategory.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterRejCatIntNo = new SqlParameter("@RejCIntNo", SqlDbType.Int, 4);
            parameterRejCatIntNo.Value = RejCatIntNo;
            sqlDARejCategory.SelectCommand.Parameters.Add(parameterRejCatIntNo);

            sqlDARejCategory.Fill(dsRejCategory);
            sqlDARejCategory.SelectCommand.Connection.Dispose();
            return dsRejCategory;

        }
       
        //public RejectionCategoryDetails GetRejectionCategoryDetails(int RejCatIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("GetRejectionsByRejectionCategoryId", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterRejCatIntNo = new SqlParameter("@RejCIntNo", SqlDbType.Int, 4);
        //    parameterRejCatIntNo.Value = RejCatIntNo;
        //    myCommand.Parameters.Add(parameterRejCatIntNo);

        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Create CustomerDetails Struct
        //    RejectionCategoryDetails myRejectionCategoryDetails = new RejectionCategoryDetails();

        //    while (result.Read())
        //    {
        //        // Populate Struct using Output Params from SPROC
        //        myRejectionCategoryDetails.RejCategory = result["RejCCategory"].ToString();
        //        myRejectionCategoryDetails.RejSkip = result["RejCSkip"].ToString();
                
        //    }
        //    result.Close();
        //    return myRejectionCategoryDetails;
        //}

        public Int32 AddRejectionCategory(string rejReason, string rejSkip, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RejectionCategoryAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC

            SqlParameter parameterRejReason = new SqlParameter("@RejCCategory", SqlDbType.VarChar, 50);
            parameterRejReason.Value = rejReason;
            myCommand.Parameters.Add(parameterRejReason);

            SqlParameter parameterRejSkip = new SqlParameter("@RejCSkip", SqlDbType.Char, 1);
            parameterRejSkip.Value = rejSkip;
            myCommand.Parameters.Add(parameterRejSkip);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterRejIntNo = new SqlParameter("@RejCIntNo", SqlDbType.Int);
            parameterRejIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterRejIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int rejCIntNo = (int)myCommand.Parameters["@RejCIntNo"].Value;

                return rejCIntNo;
            }
            catch
            {
                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        //SD: 16.07.2008

        public SqlDataReader GetRejectionCategoryList()
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("RejectionCategoryList", con);
            com.CommandType = CommandType.StoredProcedure;
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public int UpdateRejectionCategories(int rejIntNo, string rejReason, string rejSkip, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RejectionCategoryUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterRejReason = new SqlParameter("@RejCCategory", SqlDbType.VarChar, 50);
            parameterRejReason.Value = rejReason;
            myCommand.Parameters.Add(parameterRejReason);

            SqlParameter parameterRejSkip = new SqlParameter("@RejCSkip", SqlDbType.Char, 1);
            parameterRejSkip.Value = rejSkip;
            myCommand.Parameters.Add(parameterRejSkip);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterRejIntNo = new SqlParameter("@RejCIntNo", SqlDbType.Int);
            parameterRejIntNo.Direction = ParameterDirection.InputOutput;
            parameterRejIntNo.Value = rejIntNo;
            myCommand.Parameters.Add(parameterRejIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // mrs 20081128 must use the correct variable names - potential confusion
                //int RejIntNo = (int)myCommand.Parameters["@RejCIntNo"].Value;
                int rejCIntNo = (int)myCommand.Parameters["@RejCIntNo"].Value;

                return rejCIntNo;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                myConnection.Dispose();
                return 0;
            }
        }



        //SD : 17.07.2008// rejection category

        public RejectionCategoryDetails GetRejectionCategory(int rejIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RejectionCategoryDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterRejIntNo = new SqlParameter("@RejCIntNo", SqlDbType.Int, 4);
            parameterRejIntNo.Value = rejIntNo;
            myCommand.Parameters.Add(parameterRejIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            RejectionCategoryDetails myRejectionDetails = new RejectionCategoryDetails();

           while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myRejectionDetails.RejCategory = result["RejCCategory"].ToString();
                myRejectionDetails.RejSkip = result["RejCSkip"].ToString();
            }
            result.Close();
            return myRejectionDetails;
        }

        // SD : 18.07.2008
        public int DeleteRejection(int rejIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RejectionDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterRejIntNo = new SqlParameter("@RejIntNo", SqlDbType.Int, 4);
            parameterRejIntNo.Value = rejIntNo;
            parameterRejIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterRejIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int RejIntNo = (int)parameterRejIntNo.Value;

                return RejIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        
      
    }
}


