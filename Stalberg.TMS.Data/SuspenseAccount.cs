using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a Dummy Notice for use with a Suspense Account payment
    /// </summary>
    public class DummyNotice
    {
        // Fields
        private int notIntNo = 0;
        private string noticeNo = string.Empty;
        private int chgIntNo = 0;
        private int filmIntNo = 0;
        private string filmNo = string.Empty;
        private int ctIntNo = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="DummyNotice"/> class.
        /// </summary>
        internal DummyNotice()
        {
        }

        /// <summary>
        /// Gets or sets the Charge Type Int no.
        /// </summary>
        /// <value>The CT Int no.</value>
        public int CTINtNo
        {
            get { return this.ctIntNo; }
            internal set { this.ctIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the film Number.
        /// </summary>
        /// <value>The film Number.</value>
        public string FilmNumber
        {
            get { return this.filmNo; }
            internal set { this.filmNo = value; }
        }

        /// <summary>
        /// Gets or sets the film int no.
        /// </summary>
        /// <value>The film int no.</value>
        public int FilmIntNo
        {
            get { return this.filmIntNo; }
            internal set { this.filmIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the CHG int no.
        /// </summary>
        /// <value>The CHG int no.</value>
        public int ChgIntNo
        {
            get { return this.chgIntNo; }
            internal set { this.chgIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the notice number.
        /// </summary>
        /// <value>The notice number.</value>
        public string NoticeNumber
        {
            get { return this.noticeNo; }
            internal set { this.noticeNo = value; }
        }

        /// <summary>
        /// Gets or sets the not int no.
        /// </summary>
        /// <value>The not int no.</value>
        public int NotIntNo
        {
            get { return this.notIntNo; }
            internal set { this.notIntNo = value; }
        }
    }

    /// <summary>
    /// Represents a Notice Assignment from a Suspense account dummy notice
    /// </summary>
    [Serializable]
    public class NoticeAssignment
    {
        // Fields
        private bool isValid;
        private int sourceNotIntNo;
        private string sourceTicketNo = string.Empty;
        private decimal sourceAmount = 0M;
        private int destinationNotIntNo;
        private string destinationTicketNo = string.Empty;
        private decimal destinationAmount = 0M;

        /// <summary>
        /// Initializes a new instance of the <see cref="NoticeAssignment"/> class.
        /// </summary>
        public NoticeAssignment()
        {
        }

        /// <summary>
        /// Sets the suspense notice details
        /// </summary>
        /// <param name="notIntNo">The not int no.</param>
        /// <param name="ticketNo">The ticket no.</param>
        /// <param name="amount">The amount.</param>
        public void SetSuspenseNotice(int notIntNo, string ticketNo, decimal amount)
        {
            this.sourceAmount = amount;
            this.sourceNotIntNo = notIntNo;
            this.sourceTicketNo = ticketNo;
            this.CheckValidity();
        }

        /// <summary>
        /// Sets the destination notice.
        /// </summary>
        /// <param name="notIntNo">The not int no.</param>
        /// <param name="ticketNo">The ticket no.</param>
        /// <param name="amount">The amount.</param>

        public void SetDestinationNotice(int notIntNo, string ticketNo, decimal amount)
        {
            this.destinationAmount = amount;
            this.destinationNotIntNo = notIntNo;
            this.destinationTicketNo = ticketNo;
            this.CheckValidity();
        }

        private void CheckValidity()
        {
            if (this.destinationTicketNo.Length == 0)
            {
                this.isValid = false;
                return;
            }

            if (this.sourceTicketNo.Length == 0)
            {
                this.isValid = false;
                return;
            }

            if (this.destinationAmount != this.sourceAmount)
            {
                this.isValid = false;
                return;
            }

            this.isValid = true;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is valid.
        /// </summary>
        /// <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
        public bool IsValid
        {
            get { return this.isValid; }
        }

        /// <summary>
        /// Gets the destination int no.
        /// </summary>
        /// <value>The destination int no.</value>
        internal int DestinationIntNo
        {
            get { return this.destinationNotIntNo; }
        }

        /// <summary>
        /// Gets the suspense int no.
        /// </summary>
        /// <value>The suspense int no.</value>
        internal int SuspenseIntNo
        {
            get { return this.sourceNotIntNo; }
        }

        /// <summary>
        /// Gets the notice amount.
        /// </summary>
        /// <value>The amount.</value>
        internal decimal Amount
        {
            get { return this.sourceAmount; }
        }

        /// <summary>
        /// Gets the destination ticket no.
        /// </summary>
        /// <value>The destination ticket no.</value>
        public string DestinationTicketNo
        {
            get { return this.destinationTicketNo; }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            if (this.IsValid)
                return string.Format("Are you sure you want to assign the suspense account payment from '{0}' to the unpaid notice '{1}'?", this.sourceTicketNo, this.destinationTicketNo);

            if (this.destinationTicketNo.Length == 0)
                return "The destination ticket has not yet been selected.";

            if (this.sourceTicketNo.Length == 0)
                return "The Suspense account ticket number has not yet been selected.";

            if (this.sourceAmount != this.destinationAmount)
            {
                //update by Rachel 20140819 for 5337
                //return string.Format("The selected Suspense notice payment (R {0}) does not match the selected actual ticket (R {1})", this.sourceAmount, this.destinationAmount);
                return string.Format(CultureInfo.InvariantCulture,"The selected Suspense notice payment (R {0}) does not match the selected actual ticket (R {1})", this.sourceAmount, this.destinationAmount);
                //end update by Rachel 20140815 for 5337
            }

            return base.ToString();
        }

    }

    /// <summary>
    /// Represents a Reversible suspense account Notice 
    /// </summary>
    [Serializable]
    public class ReversibleNotice
    {
        // Fields
        private int notIntNo;
        private string ticketNo = string.Empty;
        private DateTime offenceDate;
        private DateTime receiptDate;
        private string oldNotice = string.Empty;

        // Static
        private static Regex regex = null;

        /// <summary>
        /// Initializes the <see cref="ReversableNotice"/> class.
        /// </summary>
        static ReversibleNotice()
        {
            string pattern = @"[A-Z]{1,3}/\d{7}$";
            ReversibleNotice.regex = new Regex(
                pattern, RegexOptions.Compiled |
                RegexOptions.IgnoreCase |
                RegexOptions.Singleline | RegexOptions.CultureInvariant);
        }

        /// <summary>
        /// Determines whether the specified receipt detail is reversible.
        /// </summary>
        /// <param name="receiptDetail">The receipt detail.</param>
        /// <returns>
        /// 	<c>true</c> if the specified receipt detail is reversible; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsReversible(string receiptDetail)
        {
            return ReversibleNotice.regex.IsMatch(receiptDetail);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReversableNotice"/> class.
        /// </summary>
        public ReversibleNotice()
        {
        }

        /// <summary>
        /// Gets the old notice number.
        /// </summary>
        /// <value>The old notice.</value>
        public string OldNotice
        {
            get { return this.oldNotice; }
        }

        /// <summary>
        /// Sets the old notice.
        /// </summary>
        /// <param name="receiptDetails">The receipt details.</param>
        public void SetOldNotice(string receiptDetails)
        {
            Match match = ReversibleNotice.regex.Match(receiptDetails);
            this.oldNotice = match.Value;
        }

        /// <summary>
        /// Gets or sets the receipt date.
        /// </summary>
        /// <value>The receipt date.</value>
        public DateTime ReceiptDate
        {
            get { return this.receiptDate; }
            set { this.receiptDate = value; }
        }

        /// <summary>
        /// Gets or sets the offence date.
        /// </summary>
        /// <value>The offence date.</value>
        public DateTime OffenceDate
        {
            get { return this.offenceDate; }
            set { this.offenceDate = value; }
        }

        /// <summary>
        /// Gets or sets the ticket number.
        /// </summary>
        /// <value>The ticket number.</value>
        public string TicketNumber
        {
            get { return this.ticketNo; }
            set { this.ticketNo = value; }
        }

        /// <summary>
        /// Gets or sets the not int no.
        /// </summary>
        /// <value>The not int no.</value>
        public int NotIntNo
        {
            get { return this.notIntNo; }
            set { this.notIntNo = value; }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            return string.Format("Are you sure you wish to reverse the assignment of ticket number '{0}' back to the dummy notice '{1}'?", this.ticketNo, this.oldNotice);
        }

    }

    /// <summary>
    /// Represents all the database interaction logic for dealing with Suspense Account payments
    /// </summary>
    public class SuspenseAccount
    {
        // Fields
        private SqlConnection con;

        /// <summary>
        /// The message to display when a suspense account payment is being processed.
        /// </summary>
        public const string SUSPENSE_MESSAGE = "<font color=\"Red\"><b><i>N.B.</i></b> A dummy notice will be created for this payment as the original notice is not known. This payment can be transferred to the original notice at a later date.</color><br/>&nbsp;<br/>";

        /// <summary>
        /// Initializes a new instance of the <see cref="SuspenseAccount"/> class.
        /// </summary>
        public SuspenseAccount(string connectionString)
        {
            this.con = new SqlConnection(connectionString);
        }

        /// <summary>
        /// Gets the next dummy notice for the supplied Authority.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="amount">The amount of the fine.</param>
        /// <returns></returns>
        // 2013-07-26 add parameter lastUser by Henry
        public DummyNotice GetDummyNotice(int autIntNo, decimal amount, string lastUser, string notTicketNo = null)
        {
            DummyNotice notice = new DummyNotice();

            SqlCommand com = new SqlCommand("SuspenseAccountGetDummyNotice", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@ConCode", SqlDbType.Char, 2).Value = "SI";
            com.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = amount;
            com.Parameters.Add("@NotTicketNo", SqlDbType.NVarChar, 20).Value = notTicketNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                this.con.Open();
                SqlDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    notice.ChgIntNo = (int)reader["ChgIntNo"];
                    notice.FilmIntNo = (int)reader["FilmIntNo"];
                    notice.FilmNumber = (string)reader["FilmNo"];
                    notice.NoticeNumber = (string)reader["TicketNumber"];
                    notice.NotIntNo = (int)reader["NotIntNo"];
                    notice.CTINtNo = (int)reader["CTIntNo"];
                }
                reader.Close();
            }
            finally
            {
                this.con.Close();
            }

            return notice;
        }

        /// <summary>
        /// Gets a list of suspense notices matching the ticket number supplied
        /// </summary>
        /// <param name="ticketNo">The ticket no t search for.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetSuspenseNotices(string ticketNo)
        {
            DataSet ds = new DataSet();

            SqlCommand com = new SqlCommand("SuspenseAccountGetSuspenseNotices", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = ticketNo;

            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        /// <summary>
        /// Assigns a Suspense notice to an actual Notice, transferring the receipt and removing the dummy notice.
        /// </summary>
        /// <param name="assignment">The assignment.</param>
        /// <returns>The RctIntNo for reprinting the receipt.</returns>
        public int AssignNotice(NoticeAssignment assignment, string lastUser, int rctIntNo)
        {
            SqlCommand com = new SqlCommand("SuspenseAccountAssignNotice", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@SourceNotice", SqlDbType.Int, 4).Value = assignment.SuspenseIntNo;
            com.Parameters.Add("@DestinationNotice", SqlDbType.Int, 4).Value = assignment.DestinationIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@SourceRctIntNo", SqlDbType.Int, 4).Value = rctIntNo;

            try
            {
                this.con.Open();
                return Convert.ToInt32(com.ExecuteScalar());
            }
            finally
            {
                this.con.Close();
            }
        }

        /// <summary>
        /// Searches for all assigned notices that match the supplied ticket number.
        /// </summary>
        /// <param name="ticketNo">The ticket no.</param>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        public List<ReversibleNotice> FindAssignedNotice(string ticketNo, int autIntNo)
        {
            List<ReversibleNotice> list = new List<ReversibleNotice>();

            SqlCommand com = new SqlCommand("SuspenseAccountSearchAssigned", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = ticketNo;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            try
            {
                this.con.Open();
                com.Prepare();

                SqlDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    if (!ReversibleNotice.IsReversible(Helper.GetReaderValue<string>(reader, "RctDetails")))
                        continue;

                    ReversibleNotice notice = new ReversibleNotice();
                    notice.NotIntNo = (int)reader["NotIntNo"];
                    notice.TicketNumber = (string)reader["NotTicketNo"];
                    notice.OffenceDate = (DateTime)reader["NotOffenceDate"];
                    notice.ReceiptDate = (DateTime)reader["RctDate"];
                    notice.SetOldNotice((string)reader["RctDetails"]);

                    list.Add(notice);
                }
                reader.Close();
            }
            finally
            {
                this.con.Close();
            }

            return list;
        }

        /// <summary>
        /// Reverses a suspense account dummy notice assignment.
        /// </summary>
        /// <param name="notice">The reversible notice details.</param>
        public int ReverseAssignment(ReversibleNotice notice, string lastUser)
        {
            SqlCommand com = new SqlCommand("SuspenseAccountReverseAssignment", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notice.NotIntNo;
            com.Parameters.Add("@OldTicketNo", SqlDbType.VarChar, 50).Value = notice.OldNotice;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                this.con.Open();
                com.Prepare();

                return Convert.ToInt32(com.ExecuteScalar());
            }
            catch
            {
                return -1;
            }
            finally
            {
                this.con.Close();
            }
        }

        /// <summary>
        /// Gets the payment report data set.
        /// </summary>
        /// <param name="date">The date from which to begin the report.</param>
        /// <param name="autIntNo">The authority int no.</param>
        /// <returns>A <see cref="DataSet"/>.</returns>
        /// TLP 20081023 - added new parameter, easyPayOnly - set to 1 if reporting only on Easypay
        /// suspense account payments.
        public DataSet PaymentReport(DateTime date, DateTime endDate, int easyPayOnly, int autIntNo)
        {
            SqlCommand com = new SqlCommand("SuspenseAccountPaymentReport", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@Date", SqlDbType.DateTime, 8).Value = date;
            com.Parameters.Add("@EndDate", SqlDbType.DateTime, 8).Value = endDate;
            com.Parameters.Add("@EasyPayOnly", SqlDbType.Int, 4).Value = easyPayOnly;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

    }
}