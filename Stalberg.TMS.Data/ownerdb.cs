using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
	
	public partial class OwnerDetails 
	{
		public int OwnIntNo;
		public int NotIntNo;
		public string OwnSurname; 
		public string OwnInitials; 
		public string OwnIDType; 
		public string OwnIDNumber; 
		public string OwnNationality;
		public string OwnAge;
		public string OwnPOAdd1;
		public string OwnPOAdd2;
		public string OwnPOAdd3;
		public string OwnPOAdd4;
		public string OwnPOAdd5;
		public string OwnPOCode;
		public string OwnStAdd1;
		public string OwnStAdd2;
		public string OwnStAdd3;
		public string OwnStAdd4;
		public string OwnStCode;
		public DateTime OwnDateCOA;
		public string OwnLicenceCode;
		public string OwnLicencePlace;
		public string LastUser;
        public string OwnForeNames;
        public string OwnFullName;
        public string OwnCellNo;
        public string OwnWorkNo;
        public string OwnHomeNo;
	}

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public partial class OwnerDB
	{
		string mConstr = "";

			public OwnerDB (string vConstr)
			{
				mConstr = vConstr;
			}

		//*******************************************************
		//
		// The GetOwnerDetails method returns a OwnerDetails
		// struct that contains information about a specific transaction number
		//
		//*******************************************************

		public OwnerDetails GetOwnerDetails(int OwnIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OwnerDetail", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterOwnIntNo = new SqlParameter("@OwnIntNo", SqlDbType.Int, 4);
			parameterOwnIntNo.Value = OwnIntNo;
			myCommand.Parameters.Add(parameterOwnIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			// Create CustomerDetails Struct
			OwnerDetails myOwnerDetails = new OwnerDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myOwnerDetails.NotIntNo = Convert.ToInt32(result["NotIntNo"]);
				myOwnerDetails.OwnIntNo = Convert.ToInt32(result["OwnIntNo"]);
				myOwnerDetails.OwnSurname = result["OwnSurname"].ToString(); 
				myOwnerDetails.OwnInitials = result["OwnInitials"].ToString(); 
				myOwnerDetails.OwnIDType = result["OwnIDType"].ToString(); 
				myOwnerDetails.OwnIDNumber = result["OwnIDNumber"].ToString(); 
				myOwnerDetails.OwnNationality = result["OwnNationality"].ToString();
				myOwnerDetails.OwnAge = result["OwnAge"].ToString();
				myOwnerDetails.OwnPOAdd1 = result["OwnPoAdd1"].ToString();
				myOwnerDetails.OwnPOAdd2 = result["OwnPoAdd2"].ToString();
				myOwnerDetails.OwnPOAdd3 = result["OwnPoAdd3"].ToString();
				myOwnerDetails.OwnPOAdd4 = result["OwnPoAdd4"].ToString();
				myOwnerDetails.OwnPOAdd5 = result["OwnPoAdd5"].ToString();
				myOwnerDetails.OwnPOCode = result["OwnPoCode"].ToString();
				myOwnerDetails.OwnStAdd1 = result["OwnStAdd1"].ToString();
				myOwnerDetails.OwnStAdd2 = result["OwnStAdd2"].ToString();
				myOwnerDetails.OwnStAdd3 = result["OwnStAdd3"].ToString();
				myOwnerDetails.OwnStAdd4 = result["OwnStAdd4"].ToString();
				myOwnerDetails.OwnStCode = result["OwnStCode"].ToString();
				if (result["OwnDateCOA"] != System.DBNull.Value)
					myOwnerDetails.OwnDateCOA = Convert.ToDateTime(result["OwnDateCOA"]);
				myOwnerDetails.OwnLicenceCode = result["OwnLicenceCode"].ToString();
				myOwnerDetails.OwnLicencePlace = result["OwnLicencePlace"].ToString();
				myOwnerDetails.LastUser = result["LastUser"].ToString();
                myOwnerDetails.OwnForeNames = result["OwnForeNames"].ToString();
                myOwnerDetails.OwnFullName = result["OwnFullName"].ToString();
                myOwnerDetails.OwnCellNo = result["OwnCellNo"].ToString();
                myOwnerDetails.OwnWorkNo = result["OwnWorkNo"].ToString();
                myOwnerDetails.OwnHomeNo = result["OwnHomeNo"].ToString();  
			}
			result.Close();
			return myOwnerDetails;
			
		}
		
		public OwnerDetails GetOwnerDetailsByNotice(int NotIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OwnerDetailByNotice", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
			parameterNotIntNo.Value = NotIntNo;
			myCommand.Parameters.Add(parameterNotIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			// Create CustomerDetails Struct
			OwnerDetails myOwnerDetails = new OwnerDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myOwnerDetails.NotIntNo = Convert.ToInt32(result["NotIntNo"]);
				myOwnerDetails.OwnIntNo = Convert.ToInt32(result["OwnIntNo"]);
				myOwnerDetails.OwnSurname = result["OwnSurname"].ToString(); 
				myOwnerDetails.OwnInitials = result["OwnInitials"].ToString(); 
				myOwnerDetails.OwnIDType = result["OwnIDType"].ToString(); 
				myOwnerDetails.OwnIDNumber = result["OwnIDNumber"].ToString(); 
				myOwnerDetails.OwnNationality = result["OwnNationality"].ToString();
				myOwnerDetails.OwnAge = result["OwnAge"].ToString();
				myOwnerDetails.OwnPOAdd1 = result["OwnPoAdd1"].ToString();
				myOwnerDetails.OwnPOAdd2 = result["OwnPoAdd2"].ToString();
				myOwnerDetails.OwnPOAdd3 = result["OwnPoAdd3"].ToString();
				myOwnerDetails.OwnPOAdd4 = result["OwnPoAdd4"].ToString();
				myOwnerDetails.OwnPOAdd5 = result["OwnPoAdd5"].ToString();
				myOwnerDetails.OwnPOCode = result["OwnPoCode"].ToString();
				myOwnerDetails.OwnStAdd1 = result["OwnStAdd1"].ToString();
				myOwnerDetails.OwnStAdd2 = result["OwnStAdd2"].ToString();
				myOwnerDetails.OwnStAdd3 = result["OwnStAdd3"].ToString();
				myOwnerDetails.OwnStAdd4 = result["OwnStAdd4"].ToString();
				myOwnerDetails.OwnStCode = result["OwnStCode"].ToString();
				if (result["OwnDateCOA"] != System.DBNull.Value)
					myOwnerDetails.OwnDateCOA = Convert.ToDateTime(result["OwnDateCOA"]);
				myOwnerDetails.OwnLicenceCode = result["OwnLicenceCode"].ToString();
				myOwnerDetails.OwnLicencePlace = result["OwnLicencePlace"].ToString();
				myOwnerDetails.LastUser = result["LastUser"].ToString();
                myOwnerDetails.OwnForeNames = result["OwnForeNames"].ToString();
                myOwnerDetails.OwnFullName = result["OwnFullName"].ToString();
                myOwnerDetails.OwnCellNo = result["OwnCellNo"].ToString();
                myOwnerDetails.OwnWorkNo = result["OwnWorkNo"].ToString();
                myOwnerDetails.OwnHomeNo = result["OwnHomeNo"].ToString();  
			}

			result.Close();
			return myOwnerDetails;
			
		}

		//*******************************************************
		//
		// The AddOwner method inserts a new transaction number record
		// into the Owner database.  A unique "OwnIntNo"
		// key is then returned from the method.  
		//
		//*******************************************************

		public int AddOwner(int notIntNo, string ownSurname, string ownInitials, 
			string ownIDType, string ownIDNumber, string ownNationality,
			string ownAge, string ownPoAdd1, string ownPoAdd2, string ownPoAdd3,
			string ownPoAdd4, string ownPoAdd5,	string ownPoCode, string ownStAdd1,
			string ownStAdd2, string ownStAdd3,	string ownStAdd4, string ownStCode,
			DateTime ownDateCOA, string ownLicenceCode, string ownLicencePlace, string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OwnerAdd", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
			parameterNotIntNo.Value = notIntNo;
			myCommand.Parameters.Add(parameterNotIntNo);

			SqlParameter parameterOwnSurname = new SqlParameter("@OwnSurname", SqlDbType.VarChar, 100);
			parameterOwnSurname.Value = ownSurname;
			myCommand.Parameters.Add(parameterOwnSurname);

			SqlParameter parameterOwnInitials = new SqlParameter("@OwnInitials", SqlDbType.VarChar, 10);
			parameterOwnInitials.Value = ownInitials;
			myCommand.Parameters.Add(parameterOwnInitials);
		
			SqlParameter parameterOwnIDType = new SqlParameter("@OwnIDType", SqlDbType.VarChar, 3);
			parameterOwnIDType.Value = ownIDType;
			myCommand.Parameters.Add(parameterOwnIDType);
		
			SqlParameter parameterOwnIDNumber = new SqlParameter("@OwnIDNumber", SqlDbType.VarChar, 25);
			parameterOwnIDNumber.Value = ownIDNumber;
			myCommand.Parameters.Add(parameterOwnIDNumber);
		
			SqlParameter parameterOwnNationality = new SqlParameter("@OwnNationality", SqlDbType.VarChar, 3);
			parameterOwnNationality.Value = ownNationality;
			myCommand.Parameters.Add(parameterOwnNationality);
		
			SqlParameter parameterOwnAge = new SqlParameter("@OwnAge", SqlDbType.VarChar, 3);
			parameterOwnAge.Value = ownAge;
			myCommand.Parameters.Add(parameterOwnAge);
		
			SqlParameter parameterOwnPoAdd1 = new SqlParameter("@OwnPoAdd1", SqlDbType.VarChar, 100);
			parameterOwnPoAdd1.Value = ownPoAdd1;
			myCommand.Parameters.Add(parameterOwnPoAdd1);
		
			SqlParameter parameterOwnPoAdd2 = new SqlParameter("@OwnPoAdd2", SqlDbType.VarChar, 100);
			parameterOwnPoAdd2.Value = ownPoAdd2;
			myCommand.Parameters.Add(parameterOwnPoAdd2);
		
			SqlParameter parameterOwnPoAdd3 = new SqlParameter("@OwnPoAdd3", SqlDbType.VarChar, 100);
			parameterOwnPoAdd3.Value = ownPoAdd3;
			myCommand.Parameters.Add(parameterOwnPoAdd3);
		
			SqlParameter parameterOwnPoAdd4 = new SqlParameter("@OwnPoAdd4", SqlDbType.VarChar, 100);
			parameterOwnPoAdd4.Value = ownPoAdd4;
			myCommand.Parameters.Add(parameterOwnPoAdd4);
		
			SqlParameter parameterOwnPoAdd5 = new SqlParameter("@OwnPoAdd5", SqlDbType.VarChar, 100);
			parameterOwnPoAdd5.Value = ownPoAdd5;
			myCommand.Parameters.Add(parameterOwnPoAdd5);
		
			SqlParameter parameterOwnPoCode = new SqlParameter("@OwnPoCode", SqlDbType.VarChar, 10);
			parameterOwnPoCode.Value = ownPoCode;
			myCommand.Parameters.Add(parameterOwnPoCode);
		
			SqlParameter parameterOwnStAdd1 = new SqlParameter("@OwnStAdd1", SqlDbType.VarChar, 100);
			parameterOwnStAdd1.Value = ownStAdd1;
			myCommand.Parameters.Add(parameterOwnStAdd1);
		
			SqlParameter parameterOwnStAdd2 = new SqlParameter("@OwnStAdd2", SqlDbType.VarChar, 100);
			parameterOwnStAdd2.Value = ownStAdd2;
			myCommand.Parameters.Add(parameterOwnStAdd2);
			
			SqlParameter parameterOwnStAdd3 = new SqlParameter("@OwnStAdd3", SqlDbType.VarChar, 100);
			parameterOwnStAdd3.Value = ownStAdd3;
			myCommand.Parameters.Add(parameterOwnStAdd3);
			
			SqlParameter parameterOwnStAdd4 = new SqlParameter("@OwnStAdd4", SqlDbType.VarChar, 100);
			parameterOwnStAdd4.Value = ownStAdd4;
			myCommand.Parameters.Add(parameterOwnStAdd4);
			
			SqlParameter parameterOwn = new SqlParameter("@OwnStCode", SqlDbType.VarChar, 10);
			parameterOwn.Value = ownStCode;
			myCommand.Parameters.Add(parameterOwn);
			
			SqlParameter parameterOwnDateCOA = new SqlParameter("@OwnDateCOA", SqlDbType.SmallDateTime);
			parameterOwnDateCOA.Value = ownDateCOA;
			myCommand.Parameters.Add(parameterOwnDateCOA);
			
			SqlParameter parameterOwnLicenceCode = new SqlParameter("@OwnLicenceCode", SqlDbType.VarChar, 3);
			parameterOwnLicenceCode.Value = ownLicenceCode;
			myCommand.Parameters.Add(parameterOwnLicenceCode);
			
			SqlParameter parameterOwnLicencePlace = new SqlParameter("@OwnLicencePlace", SqlDbType.VarChar, 50);
			parameterOwnLicencePlace.Value = ownLicencePlace;
			myCommand.Parameters.Add(parameterOwnLicencePlace);
			
			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterOwnIntNo = new SqlParameter("@OwnIntNo", SqlDbType.Int, 4);
			parameterOwnIntNo.Direction = ParameterDirection.Output;
			myCommand.Parameters.Add(parameterOwnIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int OwnIntNo = Convert.ToInt32(parameterOwnIntNo.Value);

				return OwnIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		public SqlDataReader GetOwnerList(int notIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OwnerList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
			parameterNotIntNo.Value = notIntNo;
			myCommand.Parameters.Add(parameterNotIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}


		public DataSet GetOwnerListDS(int notIntNo)
		{
			SqlDataAdapter sqlDAOwners = new SqlDataAdapter();
			DataSet dsOwners = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAOwners.SelectCommand = new SqlCommand();
			sqlDAOwners.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAOwners.SelectCommand.CommandText = "OwnerList";

			// Mark the Command as a SPROC
			sqlDAOwners.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
			parameterNotIntNo.Value = notIntNo;
			sqlDAOwners.SelectCommand.Parameters.Add(parameterNotIntNo);

			// Execute the command and close the connection
			sqlDAOwners.Fill(dsOwners);
			sqlDAOwners.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsOwners;		
		}

		public int UpdateOwner(int ownIntNo, int notIntNo, string ownSurname, string ownInitials, 
			string ownIDType, string ownIDNumber, string ownNationality,
			string ownAge, string ownPoAdd1, string ownPoAdd2, string ownPoAdd3,
			string ownPoAdd4, string ownPoAdd5,	string ownPoCode, string ownStAdd1,
			string ownStAdd2, string ownStAdd3,	string ownStAdd4, string ownStCode,
			string ownDateCOA, string ownLicenceCode, string ownLicencePlace, string lastUser)  
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("OwnerUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
			parameterNotIntNo.Value = notIntNo;
			myCommand.Parameters.Add(parameterNotIntNo);

			SqlParameter parameterOwnSurname = new SqlParameter("@OwnSurname", SqlDbType.VarChar, 100);
			parameterOwnSurname.Value = ownSurname;
			myCommand.Parameters.Add(parameterOwnSurname);

			SqlParameter parameterOwnInitials = new SqlParameter("@OwnInitials", SqlDbType.VarChar, 10);
			parameterOwnInitials.Value = ownInitials;
			myCommand.Parameters.Add(parameterOwnInitials);
		
			SqlParameter parameterOwnIDType = new SqlParameter("@OwnIDType", SqlDbType.VarChar, 3);
			parameterOwnIDType.Value = ownIDType;
			myCommand.Parameters.Add(parameterOwnIDType);
		
			SqlParameter parameterOwnIDNumber = new SqlParameter("@OwnIDNumber", SqlDbType.VarChar, 25);
			parameterOwnIDNumber.Value = ownIDNumber;
			myCommand.Parameters.Add(parameterOwnIDNumber);
		
			SqlParameter parameterOwnNationality = new SqlParameter("@OwnNationality", SqlDbType.VarChar, 3);
			parameterOwnNationality.Value = ownNationality;
			myCommand.Parameters.Add(parameterOwnNationality);
		
			SqlParameter parameterOwnAge = new SqlParameter("@OwnAge", SqlDbType.VarChar, 3);
			parameterOwnAge.Value = ownAge;
			myCommand.Parameters.Add(parameterOwnAge);
		
			SqlParameter parameterOwnPoAdd1 = new SqlParameter("@OwnPoAdd1", SqlDbType.VarChar, 100);
			parameterOwnPoAdd1.Value = ownPoAdd1;
			myCommand.Parameters.Add(parameterOwnPoAdd1);
		
			SqlParameter parameterOwnPoAdd2 = new SqlParameter("@OwnPoAdd2", SqlDbType.VarChar, 100);
			parameterOwnPoAdd2.Value = ownPoAdd2;
			myCommand.Parameters.Add(parameterOwnPoAdd2);
		
			SqlParameter parameterOwnPoAdd3 = new SqlParameter("@OwnPoAdd3", SqlDbType.VarChar, 100);
			parameterOwnPoAdd3.Value = ownPoAdd3;
			myCommand.Parameters.Add(parameterOwnPoAdd3);
		
			SqlParameter parameterOwnPoAdd4 = new SqlParameter("@OwnPoAdd4", SqlDbType.VarChar, 100);
			parameterOwnPoAdd4.Value = ownPoAdd4;
			myCommand.Parameters.Add(parameterOwnPoAdd4);
		
			SqlParameter parameterOwnPoAdd5 = new SqlParameter("@OwnPoAdd5", SqlDbType.VarChar, 100);
			parameterOwnPoAdd5.Value = ownPoAdd5;
			myCommand.Parameters.Add(parameterOwnPoAdd5);
		
			SqlParameter parameterOwnPoCode = new SqlParameter("@OwnPoCode", SqlDbType.VarChar, 10);
			parameterOwnPoCode.Value = ownPoCode;
			myCommand.Parameters.Add(parameterOwnPoCode);
		
			SqlParameter parameterOwnStAdd1 = new SqlParameter("@OwnStAdd1", SqlDbType.VarChar, 100);
			parameterOwnStAdd1.Value = ownStAdd1;
			myCommand.Parameters.Add(parameterOwnStAdd1);
		
			SqlParameter parameterOwnStAdd2 = new SqlParameter("@OwnStAdd2", SqlDbType.VarChar, 100);
			parameterOwnStAdd2.Value = ownStAdd2;
			myCommand.Parameters.Add(parameterOwnStAdd2);
			
			SqlParameter parameterOwnStAdd3 = new SqlParameter("@OwnStAdd3", SqlDbType.VarChar, 100);
			parameterOwnStAdd3.Value = ownStAdd3;
			myCommand.Parameters.Add(parameterOwnStAdd3);
			
			SqlParameter parameterOwnStAdd4 = new SqlParameter("@OwnStAdd4", SqlDbType.VarChar, 100);
			parameterOwnStAdd4.Value = ownStAdd4;
			myCommand.Parameters.Add(parameterOwnStAdd4);
				
			SqlParameter parameterOwn = new SqlParameter("@OwnStCode", SqlDbType.VarChar, 10);
			parameterOwn.Value = ownStCode;
			myCommand.Parameters.Add(parameterOwn);
			
			SqlParameter parameterOwnDateCOA = new SqlParameter("@OwnDateCOA", SqlDbType.VarChar, 10);
			parameterOwnDateCOA.Value = ownDateCOA;
			myCommand.Parameters.Add(parameterOwnDateCOA);
			
			SqlParameter parameterOwnLicenceCode = new SqlParameter("@OwnLicenceCode", SqlDbType.VarChar, 3);
			parameterOwnLicenceCode.Value = ownLicenceCode;
			myCommand.Parameters.Add(parameterOwnLicenceCode);
			
			SqlParameter parameterOwnLicencePlace = new SqlParameter("@OwnLicencePlace", SqlDbType.VarChar, 50);
			parameterOwnLicencePlace.Value = ownLicencePlace;
			myCommand.Parameters.Add(parameterOwnLicencePlace);
			
			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterOwnIntNo = new SqlParameter("@OwnIntNo", SqlDbType.Int, 4);
			parameterOwnIntNo.Value = ownIntNo;
			parameterOwnIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterOwnIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int OwnIntNo = (int)myCommand.Parameters["@OwnIntNo"].Value;

				return OwnIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}
	
	}
}
