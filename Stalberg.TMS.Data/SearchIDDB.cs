﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Stalberg.TMS.Data
{
    public class SearchIDDB
    {
        // Fields
        private SqlConnection con;

        public SearchIDDB(string connectionString)
        {
            this.con = new SqlConnection(connectionString);
        }

        public int UpdateSearchID(int notIntNo, ref string errMessage)
        {
            SqlCommand com = new SqlCommand("InsertSearchID_WS", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@NotIntNo", SqlDbType.Int).Value = notIntNo;
            try
            {
                this.con.Open();
                return Convert.ToInt32(com.ExecuteScalar());
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return 0;
            }
            finally
            {
                this.con.Close();
            }
        }
    }
}
