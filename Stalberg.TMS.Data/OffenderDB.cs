using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    /// <summary>
    /// Contains ann the business logic for accessing Offender details from the database
    /// </summary>
    public class OffenderDB
    {
        // Fields
        private SqlConnection con;

        /// <summary>
        /// Initializes a new instance of the <see cref="OffenderDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public OffenderDB(string connectionString)
        {
            this.con = new SqlConnection(connectionString);
        }

        /// <summary>
        /// Gets the offender details.
        /// </summary>
        /// <param name="notINtNo">The not I nt no.</param>
        /// <returns>An <see cref="OffenderDetails"/> object</returns>
        public OffenderDetails GetOffenderDetails(int notINtNo)
        {
            OffenderDetails details = null;

            SqlCommand com = new SqlCommand("OffenderGetDetails", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notINtNo;

            try
            {
                this.con.Open();
                SqlDataReader reader = com.ExecuteReader();
                if (reader.Read())
                {
                    details = new OffenderDetails();

                    details.ForeNames = Helper.GetReaderValue<string>(reader, "ForeNames");
                    details.Surname = Helper.GetReaderValue<string>(reader, "Surname");
                    details.IDNumber = Helper.GetReaderValue<string>(reader, "IDNumber");
                    details.Address1 = Helper.GetReaderValue<string>(reader, "Address1");
                    details.Address2 = Helper.GetReaderValue<string>(reader, "Address2");
                    details.Address3 = Helper.GetReaderValue<string>(reader, "Address3");
                    details.Address4 = Helper.GetReaderValue<string>(reader, "Address4");
                    details.AreaCode = Helper.GetReaderValue<string>(reader, "AreaCode");
                    details.ID = Helper.GetReaderValue<int>(reader, "IntNo");
                    details.PoAddress1 = Helper.GetReaderValue<string>(reader, "PoAddress1");
                    details.PoAddress2 = Helper.GetReaderValue<string>(reader, "PoAddress2");
                    details.PoAddress3 = Helper.GetReaderValue<string>(reader, "PoAddress3");
                    details.PoAddress4 = Helper.GetReaderValue<string>(reader, "PoAddress4");
                    details.PoAreaCode = Helper.GetReaderValue<string>(reader, "PoAreaCode");
                    details.CellNo = Helper.GetReaderValue<string>(reader, "CellNo");
                    details.HomeNo = Helper.GetReaderValue<string>(reader, "HomeNo");
                    details.IsProxy = Helper.GetReaderValue<string>(reader, "NotSendTo")[0].Equals('P');
                    details.Initials = Helper.GetReaderValue<string>(reader, "Initials"); //Jerry 2014-04-15 add
                    details.IDType = Helper.GetReaderValue<string>(reader, "IDType"); //Jerry 2014-04-28 add
                    details.FullName = Helper.GetReaderValue<string>(reader, "FullName"); //Jerry 2014-06-05 add
                }
                reader.Close();
                reader.Dispose();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                this.con.Close();
            }

            return details;
        }

        /// <summary>
        /// Updates the offender's details.
        /// </summary>
        /// <param name="details">The details.</param>
        public bool UpdateOffenderDetails(OffenderDetails details, ref string errMessage)
        {
            SqlCommand com = new SqlCommand("OffenderUpdateDetails", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@IsProxy", SqlDbType.Bit, 1).Value = details.IsProxy;
            com.Parameters.Add("@ID", SqlDbType.Int, 4).Value = details.ID;
            com.Parameters.Add("@Forename", SqlDbType.VarChar, 100).Value = details.ForeNames;
            com.Parameters.Add("@Surname", SqlDbType.VarChar, 100).Value = details.Surname;
            com.Parameters.Add("@IDNumber", SqlDbType.VarChar, 25).Value = details.IDNumber;
            com.Parameters.Add("@Address1", SqlDbType.VarChar, 100).Value = details.Address1;
            com.Parameters.Add("@Address2", SqlDbType.VarChar, 100).Value = details.Address2;
            com.Parameters.Add("@Address3", SqlDbType.VarChar, 100).Value = details.Address3;
            com.Parameters.Add("@Address4", SqlDbType.VarChar, 100).Value = details.Address4;
            com.Parameters.Add("@AreaCode", SqlDbType.VarChar, 10).Value = details.AreaCode;
            com.Parameters.Add("@PoAddress1", SqlDbType.VarChar, 100).Value = details.PoAddress1;
            com.Parameters.Add("@PoAddress2", SqlDbType.VarChar, 100).Value = details.PoAddress2;
            com.Parameters.Add("@PoAddress3", SqlDbType.VarChar, 100).Value = details.PoAddress3;
            com.Parameters.Add("@PoAddress4", SqlDbType.VarChar, 100).Value = details.PoAddress4;
            com.Parameters.Add("@PoAreaCode", SqlDbType.VarChar, 10).Value = details.PoAreaCode;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = details.LastUser;
            com.Parameters.Add("@CellNo", SqlDbType.VarChar, 15).Value = details.CellNo ;
            com.Parameters.Add("@HomeNo", SqlDbType.VarChar, 15).Value = details.HomeNo;
            try
            {
                this.con.Open();
                return Convert.ToBoolean(com.ExecuteScalar());
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return false;
            }
            finally
            {
                this.con.Close();
            }
        }

        /// <summary>
        /// Updates the offender's details by notIntNo
        /// Jerry 2014-02-27 add
        /// </summary>
        /// <param name="details"></param>
        /// <param name="notIntNo"></param>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        public int UpdateOffenderDetails(OffenderDetails details, int notIntNo, ref string errMessage)
        {
            SqlCommand com = new SqlCommand("OffenderUpdateDetailsByNotIntNo", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@NotIntNo", SqlDbType.Int).Value = notIntNo;
            com.Parameters.Add("@Forename", SqlDbType.VarChar, 100).Value = details.ForeNames;
            com.Parameters.Add("@Surname", SqlDbType.VarChar, 100).Value = details.Surname;
            com.Parameters.Add("@Initials", SqlDbType.VarChar, 10).Value = details.Initials;
            com.Parameters.Add("@IDNumber", SqlDbType.VarChar, 25).Value = details.IDNumber;
            com.Parameters.Add("@Address1", SqlDbType.VarChar, 100).Value = details.Address1;
            com.Parameters.Add("@Address2", SqlDbType.VarChar, 100).Value = details.Address2;
            com.Parameters.Add("@Address3", SqlDbType.VarChar, 100).Value = details.Address3;
            com.Parameters.Add("@Address4", SqlDbType.VarChar, 100).Value = details.Address4;
            com.Parameters.Add("@AreaCode", SqlDbType.VarChar, 10).Value = details.AreaCode;
            com.Parameters.Add("@PoAddress1", SqlDbType.VarChar, 100).Value = details.PoAddress1;
            com.Parameters.Add("@PoAddress2", SqlDbType.VarChar, 100).Value = details.PoAddress2;
            com.Parameters.Add("@PoAddress3", SqlDbType.VarChar, 100).Value = details.PoAddress3;
            com.Parameters.Add("@PoAddress4", SqlDbType.VarChar, 100).Value = details.PoAddress4;
            com.Parameters.Add("@PoAreaCode", SqlDbType.VarChar, 10).Value = details.PoAreaCode;
            com.Parameters.Add("@CellNo", SqlDbType.VarChar, 15).Value = details.CellNo;
            com.Parameters.Add("@HomeNo", SqlDbType.VarChar, 15).Value = details.HomeNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = details.LastUser;
            
            try
            {
                this.con.Open();
                return Convert.ToInt32(com.ExecuteScalar());
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return 0;
            }
            finally
            {
                this.con.Close();
            }
        }

        /// <summary>
        /// update offender details for single summons
        /// Jerry 2015-02-13 add
        /// </summary>
        /// <param name="details"></param>
        /// <param name="notIntNo"></param>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        public int UpdateOffenderDetailsForSingleSummons(OffenderDetails details, int notIntNo, ref string errMessage)
        {
            SqlCommand com = new SqlCommand("OffenderUpdateDetailsForSingleSummons", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@NotIntNo", SqlDbType.Int).Value = notIntNo;
            com.Parameters.Add("@Forename", SqlDbType.VarChar, 100).Value = details.ForeNames;
            com.Parameters.Add("@Surname", SqlDbType.VarChar, 100).Value = details.Surname;
            com.Parameters.Add("@IDNumber", SqlDbType.VarChar, 25).Value = details.IDNumber;
            com.Parameters.Add("@Address1", SqlDbType.VarChar, 100).Value = details.Address1;
            com.Parameters.Add("@Address2", SqlDbType.VarChar, 100).Value = details.Address2;
            com.Parameters.Add("@Address3", SqlDbType.VarChar, 100).Value = details.Address3;
            com.Parameters.Add("@Address4", SqlDbType.VarChar, 100).Value = details.Address4;
            com.Parameters.Add("@AreaCode", SqlDbType.VarChar, 10).Value = details.AreaCode;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = details.LastUser;

            try
            {
                this.con.Open();
                return Convert.ToInt32(com.ExecuteScalar());
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return 0;
            }
            finally
            {
                this.con.Close();
            }
        }


    }

    /// <summary>
    /// Represents the personal details of an offender
    /// </summary>
    [Serializable]
    public class OffenderDetails
    {
        // Fields
        private int id;
        private bool isProxy;
        private string forename;
        private string surname;
        private string idNumber;
        private string address1;
        private string address2;
        private string address3;
        private string address4;
        private string areaCode;
        private string poAddress1;
        private string poAddress2;
        private string poAddress3;
        private string poAddress4;
        private string poAreaCode;
        private string cellNo;
        private string homeNo;
        private string lastUser = string.Empty;
        private string initials;//Jerry 2014-04-15 add
        private string idType; //Jerry 2014-04-28 add
        private string fullName; //Jerry 2014-06-04 add

        /// <summary>
        /// Initializes a new instance of the <see cref="OffenderDetails"/> class.
        /// </summary>
        public OffenderDetails()
        {
        }

        public string CellNo
        {
            get { return cellNo; }
            set { cellNo = value; }
        }

        public string HomeNo
        {
            get { return homeNo; }
            set { homeNo = value; }
        }

        public string PoAddress1
        {
            get 
            {
                return poAddress1;
            }
            set { poAddress1 = value; }
        }
        public string PoAddress2
        {
            get
            {
                return poAddress2;
            }
            set { poAddress2 = value; }
        }
        public string PoAddress3
        {
            get
            {
                return poAddress3;
            }
            set { poAddress3 = value; }
        }
        public string PoAddress4
        {
            get
            {
                return poAddress4;
            }
            set { poAddress4 = value; }
        }
        public string PoAreaCode
        {
            get { return poAreaCode; }
            set { poAreaCode = value; }
        }

        /// <summary>
        /// Gets or sets the ID number.
        /// </summary>
        /// <value>The ID number.</value>
        public string IDNumber
        {
            get { return this.idNumber; }
            set { this.idNumber = value; }
        }

        /// <summary>
        /// Gets or sets the last user.
        /// </summary>
        /// <value>The last user.</value>
        public string LastUser
        {
            get { return this.lastUser; }
            set { this.lastUser = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is proxy.
        /// </summary>
        /// <value><c>true</c> if this instance is proxy; otherwise, <c>false</c>.</value>
        public bool IsProxy
        {
            get { return this.isProxy; }
            set { this.isProxy = value; }
        }

        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        /// <value>The ID.</value>
        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        /// <summary>
        /// Gets or sets the area code.
        /// </summary>
        /// <value>The area code.</value>
        public string AreaCode
        {
            get { return this.areaCode; }
            set { this.areaCode = value; }
        }

        /// <summary>
        /// Gets or sets the address4.
        /// </summary>
        /// <value>The address4.</value>
        public string Address4
        {
            get { return this.address4; }
            set { this.address4 = value; }
        }

        /// <summary>
        /// Gets or sets the address3.
        /// </summary>
        /// <value>The address3.</value>
        public string Address3
        {
            get { return this.address3; }
            set { this.address3 = value; }
        }

        /// <summary>
        /// Gets or sets the address2.
        /// </summary>
        /// <value>The address2.</value>
        public string Address2
        {
            get { return this.address2; }
            set { this.address2 = value; }
        }

        /// <summary>
        /// Gets or sets the address1.
        /// </summary>
        /// <value>The address1.</value>
        public string Address1
        {
            get { return this.address1; }
            set { this.address1 = value; }
        }

        /// <summary>
        /// Gets or sets the surname.
        /// </summary>
        /// <value>The surname.</value>
        public string Surname
        {
            get { return this.surname; }
            set { this.surname = value; }
        }

        /// <summary>
        /// Gets or sets the fore names.
        /// </summary>
        /// <value>The fore names.</value>
        public string ForeNames
        {
            get { return this.forename; }
            set { this.forename = value; }
        }

        //Jerry 2014-04-15 add
        public string Initials
        {
            get { return this.initials; }
            set { this.initials = value; }
        }

        public string IDType
        {
            get { return this.idType; }
            set { this.idType = value; }
        }

        //Jerry 2014-06-04 add
        public string FullName
        {
            get { return this.fullName; }
            set { this.fullName = value; }
        }
    }

    /// <summary>
    /// Represents the details of an offender's judgement
    /// </summary>
    [Serializable]
    public class OffenderJudgement
    {
        // Fields
        private string name;
        private string summonsNo;
        private string caseNo;
        private DateTime dt;
        private string charge;
        private int sumIntNo;
        private bool isProcessed = false;
        //private DataSet charges = null;    //Removed by Oscar
        private int selectedRow;
        private string chargeType;
        private bool isSection72;
        private Int32 section72Count;

        /// <summary>
        /// Initializes a new instance of the <see cref="OffenderJudgement"/> class.
        /// </summary>
        public OffenderJudgement(int sumIntNo)
        {
            this.sumIntNo = sumIntNo;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this judgement has been processed.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is processed; otherwise, <c>false</c>.
        /// </value>
        public bool IsProcessed
        {
            get { return this.isProcessed; }
            set { this.isProcessed = value; }
        }

        /// <summary>
        /// Gets the sum int no.
        /// </summary>
        /// <value>The sum int no.</value>
        public int SumIntNo
        {
            get { return this.sumIntNo; }
        }

        /// <summary>
        /// Gets or sets the charge.
        /// </summary>
        /// <value>The charge.</value>
        public string Charge
        {
            get { return this.charge; }
            set { this.charge = value; }
        }

        /// <summary>
        /// Gets or sets the court date.
        /// </summary>
        /// <value>The court date.</value>
        public DateTime CourtDate
        {
            get { return this.dt; }
            set { this.dt = value; }
        }

        /// <summary>
        /// Gets or sets the case no.
        /// </summary>
        /// <value>The case no.</value>
        public string CaseNo
        {
            get { return this.caseNo; }
            set { this.caseNo = value; }
        }

        /// <summary>
        /// Gets or sets the summons no.
        /// </summary>
        /// <value>The summons no.</value>
        public string SummonsNo
        {
            get { return this.summonsNo; }
            set { this.summonsNo = value; }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        // Jake 2013-03-06 added 
        /// <summary>
        /// S72 document flag
        /// </summary>
        public bool IsSection72
        {
            get { return this.isSection72; }
            set { this.isSection72 = value; }
        }

        // Jake 2013-03-06 added 
        /// <summary>
        /// WOA of s72 issued count
        /// </summary>
        public Int32 Section72Count {
            get { return this.section72Count; }
            set { this.section72Count = value; }
        }

        // Oscar 20100915 - Removed 
        #region 
        ///// <summary>
        ///// Gets the SumCharge int no.
        ///// </summary>
        ///// <value>The SumCharge int no.</value>
        //public int SChIntNo
        //{
        //    get { return this.GetValue<int>("SChIntNo"); }
        //}

        ///// <summary>
        ///// Gets the fine amount.
        ///// </summary>
        ///// <value>The fine amount.</value>
        //public decimal RevisedFineAmount
        //{
        //    get { return this.GetValue<decimal>("SChRevAmount"); }
        //}

        //public decimal FineAmount
        //{
        //    get { return this.GetValue<decimal>("SChFineAmount"); }
        //}

        ///// <summary>
        ///// Gets the contempt amount.
        ///// </summary>
        ///// <value>The contempt amount.</value>
        //public decimal ContemptAmount
        //{
        //    get { return this.GetValue<decimal>("SChContemptAmount"); }
        //}

        ///// <summary>
        ///// Gets the sentence amount.
        ///// </summary>
        ///// <value>The sentence amount.</value>
        //public decimal SentenceAmount
        //{
        //    get { return this.GetValue<decimal>("SChSentenceAmount"); }
        //}

        ///// <summary>
        ///// Gets the other amount.
        ///// </summary>
        ///// <value>The other amount.</value>
        //public decimal OtherAmount
        //{
        //    get { return this.GetValue<decimal>("SChOtherAmount"); }
        //}

        ///// <summary>
        ///// Gets the verdict.
        ///// </summary>
        ///// <value>The verdict.</value>
        //public string Verdict
        //{
        //    get { return this.GetValue<string>("SChVerdict"); }
        //}

        ///// <summary>
        ///// Gets the sentence.
        ///// </summary>
        ///// <value>The sentence.</value>
        //public string Sentence
        //{
        //    get { return this.GetValue<string>("SChSentence"); }
        //}

        //private T GetValue<T>(string colName)
        //{
        //    DataRow row = this.charges.Tables[0].Rows[this.selectedRow];
        //    if (row[colName] == DBNull.Value)
        //        return default(T);
        //    else
        //        return (T)row[colName];
        //}

        ///// <summary>
        ///// Sets the charges.
        ///// </summary>
        ///// <param name="ds">The ds.</param>
        ///// <param name="selectedCharge">The selected charge.</param>
        //public void SetCharges(DataSet ds, int selectedCharge)
        //{
        //    this.charges = ds;
        //    this.selectedRow = selectedCharge;
        //}

        ///// <summary>
        ///// Gets the charge details.
        ///// </summary>
        ///// <returns>A <see cref="String"/> representation of the charge details</returns>
        //public string GetChargeDetails()
        //{
        //    DataRow row = this.charges.Tables[0].Rows[this.selectedRow];
        //    return string.Format("{0} - {1}", row["SChNumber"], row["SChDescr"]);
        //}
        #endregion

        // Oscar 20100915 - Rewrite and add some new fields
        public int ID { get; set; }
        public int SChIntNo { get; set; }
        public string Number { get; set; }
        public string Code { get; set; }
        public string Descr { get; set; }
        public decimal FineAmount { get; set; }
        public decimal RevisedFineAmount { get; set; }
        public decimal SentenceAmount { get; set; }
        public decimal ContemptAmount { get; set; }
        public decimal OtherAmount { get; set; }
        public string Verdict { get; set; }
        public string Sentence { get; set; }
        public string ChargeType { get; set; }

        public char NotFilmType { get; set; }

        public bool SChIsMain { get; set; }
        public int MainSumChargeID { get; set; }
        /// <summary>
        /// CourtJudgementType
        /// </summary>
        public int CJTIntNo { get; set; }
        public int CJTCode { get; set; }
        public bool SumRemandedSummonsFlag { get; set; }
        public DateTime? SumRemandedFromCourtDate { get; set; }
        public string SumRemandedFromCaseNumber { get; set; }

        // Oscar 20120606 add for push generate summons queue
        public int NotIntNo { get; set; }
        public string CrtNo { get; set; }
        public string CrtName { get; set; }
        //Jake 2013-12-18 added
        public bool IsSection35 { get; set; }
        public bool IsNoAOG { get; set; }

        public string GetChargeDetails
        {
            get { return string.Format("{0} ({1}) - {2}", Number, ChargeType, Descr); }
        }


    }

    /// <summary>
    /// Represents a list of judgements to be processed
    /// </summary>
    [Serializable]
    public class JudgementList : List<OffenderJudgement>
    {
        // Fields
        private int current = -1;

        /// <summary>
        /// Gets the next <see cref="OffenderJudgement"/> from the list, or <c>null</c>.
        /// </summary>
        /// <returns></returns>
        public OffenderJudgement GetNext()
        {
            current++;
            return this.CurrentValue;
        }

        /// <summary>
        /// Gets the current <see cref="OffenderJudgement"/> in the list.
        /// </summary>
        /// <value>The current value.</value>
        public OffenderJudgement CurrentValue
        {
            get
            {
                if (current >= base.Count || current < 0)
                    return null;

                return this[current];
            }
        }

        /// <summary>
        /// Gets the current position in the list.
        /// </summary>
        /// <value>The current.</value>
        public int CurrentIndex
        {
            get { return this.current + 1; }
            //Oscar 20100916 - append "Set Method"
            set { this.current = value - 1; }
        }

        // Oscar 20100916 - Get Charges by searching SumIntNo
        public JudgementList GetChargesBySumIntNo(int sumIntNo)
        {
            JudgementList result = new JudgementList();
            OffenderJudgement oj = null;
            for (int i = 0; i < base.Count; i++)
            {
                oj = this[i];
                if (oj.SumIntNo == sumIntNo)
                    result.Add(oj);
            }
            return result;
        }

        // Oscar 20101119 - added
        public void LocateCurrent(int sumIntNo, int? schIntNo)
        {
            OffenderJudgement oj;
            for (int i = 0; i < base.Count; i++)
            {
                oj = this[i];
                if (oj.SumIntNo == sumIntNo)
                {
                    current = i;
                    if (schIntNo != null && schIntNo > 0)
                    {
                        if (oj.SChIntNo == schIntNo)
                            return;
                    }
                    else
                        return;
                }
            }
        }
    }

}
