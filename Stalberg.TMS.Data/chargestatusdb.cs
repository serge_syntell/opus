using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // ChargeStatusDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class ChargeStatusDetails 
	{
        public string CSCode;
		public string CSDescr;
    }

    //*******************************************************
    //
    // ChargeStatusDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query ChargeStatuss within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class ChargeStatusDB {

		string mConstr = "";

		public ChargeStatusDB (string vConstr)
		{
			mConstr = vConstr;
		}

        // 20060601 no longer used
        //public ChargeStatusDetails GetChargeStatusDetails(int csIntNo) 
        //{
        //    // Create Instance of Connection and Command Objecs
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChargeStatusDetail", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterCSIntNo = new SqlParameter("@CSIntNo", SqlDbType.Int, 4);
        //    parameterCSIntNo.Value = csIntNo;
        //    myCommand.Parameters.Add(parameterCSIntNo);

        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
        //    // Create CustomerDetails Strucs
        //    ChargeStatusDetails myChargeStatusDetails = new ChargeStatusDetails();

        //    while (result.Read())
        //    {
        //        // Populate Strucs using Output Params from SPROC
        //        myChargeStatusDetails.CSDescr = result["CSDescr"].ToString();
        //        myChargeStatusDetails.CSCode = result["CSCode"].ToString();
        //    }
        //    result.Close();
        //    return myChargeStatusDetails;
        //}

        // 20060601 no longer in use
        //public int GetChargeStatusByCode(string csCode) 
        //{
        //    // Create Instance of Connection and Command Objecs
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChargeStatusByCode", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterCSCode = new SqlParameter("@CSCode", SqlDbType.Char, 3);
        //    parameterCSCode.Value = csCode;
        //    myCommand.Parameters.Add(parameterCSCode);

        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
        //    int csIntNo = 0;
        //    while (result.Read())
        //    {
        //        // Populate Strucs using Output Params from SPROC
        //        csIntNo = Convert.ToInt32(result["CSIntNo"]);
        //    }
        //    result.Close();
        //    return csIntNo;
        //}

        // 2013-07-19 comment by Henry for useless
        //public int AddChargeStatus (string csCode, string csDescr) 
        //{
        //    // Create Instance of Connection and Command Objecs
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChargeStatusAdd", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC

        //    SqlParameter parameterCSDescr = new SqlParameter("@CSDescr", SqlDbType.VarChar, 100);
        //    parameterCSDescr.Value = csDescr;
        //    myCommand.Parameters.Add(parameterCSDescr);

        //    SqlParameter parameterCSCode = new SqlParameter("@CSCode", SqlDbType.Char, 3);
        //    parameterCSCode.Value = csCode;
        //    myCommand.Parameters.Add(parameterCSCode);

        //    SqlParameter parameterCSIntNo = new SqlParameter("@CSIntNo", SqlDbType.Int, 4);
        //    parameterCSIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterCSIntNo);

        //    try {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int csIntNo = Convert.ToInt32(parameterCSIntNo.Value);

        //        return csIntNo;
        //    }
        //    catch {
        //        myConnection.Dispose();
        //        return 0;
        //    }
        //}

        //20060529 no longer using chargestatus ---- 20090706 reuse
        public DataSet GetChargeStatusList()
        {
            // Create Instance of Connection and Command Objecs
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ChargeStatusList", myConnection);
            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(myCommand);

            try
            {
                // Execute the command
                myConnection.Open();
                DataSet dsReturn = new DataSet();
                adapter.Fill(dsReturn);
                // Return the datareader result
                return dsReturn;
            }
            finally
            {
                myConnection.Close();
                myConnection.Dispose();
            }
        }

		// 20060601 no longer in use
        //public DataSet GetChargeStatusListDS(string search)
        //{
        //    SqlDataAdapter sqlDAChargeStatus = new SqlDataAdapter();
        //    DataSet dsChargeStatuss = new DataSet();

        //    // Create Instance of Connection and Command Objecs
        //    sqlDAChargeStatus.SelectCommand = new SqlCommand();
        //    sqlDAChargeStatus.SelectCommand.Connection = new SqlConnection(mConstr);			
        //    sqlDAChargeStatus.SelectCommand.CommandText = "ChargeStatusList";

        //    // Mark the Command as a SPROC
        //    sqlDAChargeStatus.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    SqlParameter parameterSearch = new SqlParameter("@Search", SqlDbType.VarChar, 10);
        //    parameterSearch.Value = search;
        //    sqlDAChargeStatus.SelectCommand.Parameters.Add(parameterSearch);

        //    // Execute the command and close the connecsion
        //    sqlDAChargeStatus.Fill(dsChargeStatuss);
        //    sqlDAChargeStatus.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsChargeStatuss;		
        //}

        // 2013-07-19 comment by Henry for useless
        //public int UpdateChargeStatus (int csIntNo, string csCode, string csDescr) 
        //{
        //    // Create Instance of Connection and Command Objecs
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChargeStatusUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterCSDescr = new SqlParameter("@CSDescr", SqlDbType.VarChar, 100);
        //    parameterCSDescr.Value = csDescr;
        //    myCommand.Parameters.Add(parameterCSDescr);

        //    SqlParameter parameterCSCode = new SqlParameter("@CSCode", SqlDbType.Char, 3);
        //    parameterCSCode.Value = csCode;
        //    myCommand.Parameters.Add(parameterCSCode);

        //    SqlParameter parameterCSIntNo = new SqlParameter("@CSIntNo", SqlDbType.Int);
        //    parameterCSIntNo.Direction = ParameterDirection.InputOutput;
        //    parameterCSIntNo.Value = csIntNo;
        //    myCommand.Parameters.Add(parameterCSIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int CSIntNo = (int)myCommand.Parameters["@CSIntNo"].Value;
        //        //int menuId = (int)parameterCSIntNo.Value;

        //        return CSIntNo;
        //    }
        //    catch 
        //    {
        //        myConnection.Dispose();
        //        return 0;
        //    }
        //}

        // 20090707 tf Add UpdateChargeStatusFlag for CSRoadblockOption and CSRoadblockIdentifier
        public int UpdateChargeStatusFlag(int csCode, string CSRoadblockOption, int CSRoadblockIdentifier, string strLastUser)
        {
            // Create Instance of Connection and Command Objecs
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ChargeStatusUpdateFlag", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@CSCode", SqlDbType.Int, 4).Value = csCode;
            myCommand.Parameters.Add("@CSRoadblockOption", SqlDbType.Char, 1).Value = CSRoadblockOption;
            myCommand.Parameters.Add("@CSRoadblockIdentifier", SqlDbType.SmallInt, 2).Value = CSRoadblockIdentifier;
            myCommand.Parameters.Add("@LastUser", SqlDbType.Char, 50).Value = strLastUser;

            try
            {
                myConnection.Open();
                return myCommand.ExecuteNonQuery();
            }
            catch
            {
                myConnection.Dispose();
                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }
		// 2005/06/01 no longer in use
        //public void ReturnNaTISCSIntNos (string csCode150, string csDescr150, ref int cs150, string csCode151, string csDescr151, ref int cs151, 
        //    string csCode152, string csDescr152, ref int cs152, string csCode153, string csDescr153, ref int cs153, 
        //    string csCode154, string csDescr154, ref int cs154, string csCode200, string csDescr200, ref int cs200, string lastUser)
        //{
        //    // Create Instance of Connection and Command Objecs
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChargeStatusReturnCSIntNos", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterCSCode150 = new SqlParameter("@CSCode150", SqlDbType.Char, 3);
        //    parameterCSCode150.Value = csCode150;
        //    myCommand.Parameters.Add(parameterCSCode150);

        //    SqlParameter parameterCSDescr150 = new SqlParameter("@CSDescr150", SqlDbType.VarChar, 100);
        //    parameterCSDescr150.Value = csDescr150;
        //    myCommand.Parameters.Add(parameterCSDescr150);

        //    SqlParameter parameterCS150 = new SqlParameter("@CS150", SqlDbType.Int);
        //    parameterCS150.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterCS150);

        //    SqlParameter parameterCSCode151 = new SqlParameter("@CSCode151", SqlDbType.Char, 3);
        //    parameterCSCode151.Value = csCode151;
        //    myCommand.Parameters.Add(parameterCSCode151);

        //    SqlParameter parameterCSDescr151 = new SqlParameter("@CSDescr151", SqlDbType.VarChar, 100);
        //    parameterCSDescr151.Value = csDescr151;
        //    myCommand.Parameters.Add(parameterCSDescr151);

        //    SqlParameter parameterCS151 = new SqlParameter("@CS151", SqlDbType.Int);
        //    parameterCS151.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterCS151);

        //    SqlParameter parameterCSCode152 = new SqlParameter("@CSCode152", SqlDbType.Char, 3);
        //    parameterCSCode152.Value = csCode152;
        //    myCommand.Parameters.Add(parameterCSCode152);

        //    SqlParameter parameterCSDescr152 = new SqlParameter("@CSDescr152", SqlDbType.VarChar, 100);
        //    parameterCSDescr152.Value = csDescr152;
        //    myCommand.Parameters.Add(parameterCSDescr152);

        //    SqlParameter parameterCS152 = new SqlParameter("@CS152", SqlDbType.Int);
        //    parameterCS152.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterCS152);

        //    SqlParameter parameterCSCode153 = new SqlParameter("@CSCode153", SqlDbType.Char, 3);
        //    parameterCSCode153.Value = csCode153;
        //    myCommand.Parameters.Add(parameterCSCode153);

        //    SqlParameter parameterCSDescr153 = new SqlParameter("@CSDescr153", SqlDbType.VarChar, 100);
        //    parameterCSDescr153.Value = csDescr153;
        //    myCommand.Parameters.Add(parameterCSDescr153);

        //    SqlParameter parameterCS153 = new SqlParameter("@CS153", SqlDbType.Int);
        //    parameterCS153.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterCS153);

        //    SqlParameter parameterCSCode154 = new SqlParameter("@CSCode154", SqlDbType.Char, 3);
        //    parameterCSCode154.Value = csCode154;
        //    myCommand.Parameters.Add(parameterCSCode154);

        //    SqlParameter parameterCSDescr154 = new SqlParameter("@CSDescr154", SqlDbType.VarChar, 100);
        //    parameterCSDescr154.Value = csDescr154;
        //    myCommand.Parameters.Add(parameterCSDescr154);

        //    SqlParameter parameterCS154 = new SqlParameter("@CS154", SqlDbType.Int);
        //    parameterCS154.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterCS154);

        //    SqlParameter parameterCSCode200 = new SqlParameter("@CSCode200", SqlDbType.Char, 3);
        //    parameterCSCode200.Value = csCode200;
        //    myCommand.Parameters.Add(parameterCSCode200);

        //    SqlParameter parameterCSDescr200 = new SqlParameter("@CSDescr200", SqlDbType.VarChar, 100);
        //    parameterCSDescr200.Value = csDescr200;
        //    myCommand.Parameters.Add(parameterCSDescr200);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterCS200 = new SqlParameter("@CS200", SqlDbType.Int);
        //    parameterCS200.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterCS200);
        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        cs150 = (int)myCommand.Parameters["@CS150"].Value;
        //        cs151 = (int)myCommand.Parameters["@CS151"].Value;
        //        cs152 = (int)myCommand.Parameters["@CS152"].Value;
        //        cs153 = (int)myCommand.Parameters["@CS153"].Value;
        //        cs154 = (int)myCommand.Parameters["@CS154"].Value;
        //        cs200 = (int)myCommand.Parameters["@CS200"].Value;

        //        return;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return;
        //    }
        //}

        // 2013-07-19 comment by Henry for useless
        //public String DeleteChargeStatus (int csIntNo)
        //{

        //    // Create Instance of Connection and Command Objecs
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ChargeStatusDelete", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterCSIntNo = new SqlParameter("@CSIntNo", SqlDbType.Int, 4);
        //    parameterCSIntNo.Value = csIntNo;
        //    parameterCSIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterCSIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int CSIntNo = (int)parameterCSIntNo.Value;

        //        return CSIntNo.ToString();
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return String.Empty;
        //    }
        //}

        public String GetChargeStatusById(string sTicket,string sId)
        {
            string sReturn = String.Empty;

            // Create Instance of Connection and Command Objects
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand cmd = new SqlCommand("ChargeStatusByIdNo", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            if (sTicket .Length > 0)
                cmd.Parameters.Add("@Ticket", SqlDbType.VarChar, 50).Value = sTicket;
            if (sId.Length > 0)
                cmd.Parameters.Add("@ID", SqlDbType.VarChar, 50).Value = sId;
            

            try
            {
                con.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                rd.Read();
                sReturn = rd["CSDescr"].ToString();
                return sReturn;
            }
            catch
            {
                return String.Empty;
            }
        }

        public String GetChargeStatusById(string sTicket, string sId, bool cashAfterCourtDate, bool cashAfterSummons, bool cashAfterWarrant, bool cashAfterGracePeriod, bool cashAfterCaseNumber, bool isHRK = false)
        {
            string sReturn = String.Empty;

            // Create Instance of Connection and Command Objects
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand cmd = new SqlCommand("ChargeStatusByIdNo", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;
            // 2014-10-15, Oscar added
            cmd.CommandTimeout = 0;

            // Add Parameters to SPROC
            if (sTicket.Length > 0)
                cmd.Parameters.Add("@Ticket", SqlDbType.VarChar, 50).Value = sTicket;
            if (sId.Length > 0)
                cmd.Parameters.Add("@ID", SqlDbType.VarChar, 50).Value = sId;

            cmd.Parameters.Add("@CashAfterCourtDate", SqlDbType.Bit, 1).Value = cashAfterCourtDate;
            cmd.Parameters.Add("@CashAfterSummons", SqlDbType.Bit, 1).Value = cashAfterSummons;
            cmd.Parameters.Add("@CashAfterWarrant", SqlDbType.Bit, 1).Value = cashAfterWarrant;
            //mrs 20080731 proc crashing - found parameter duplicated 
            //cmd.Parameters.Add("@CashAfterWarrant", SqlDbType.Bit, 1).Value = cashAfterGracePeriod;
            cmd.Parameters.Add("@CashAfterGracePeriod", SqlDbType.Bit, 1).Value = cashAfterGracePeriod;
            cmd.Parameters.Add("@CashAfterCaseNumber", SqlDbType.Bit, 1).Value = cashAfterCaseNumber;
            cmd.Parameters.Add("@IsHRK", SqlDbType.Bit, 1).Value = isHRK;

            try
            {
                con.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    rd.Read();
                    sReturn = rd["CSDescr"].ToString();
                }

                return sReturn;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return String.Empty;
            }
            finally
            {
                // 2014-10-15, Oscar added
                cmd.Dispose();
                con.Dispose();
            }
        }
	}
}

