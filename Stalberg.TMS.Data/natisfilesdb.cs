using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{
	
	public partial class NaTISFilesDetails 
	{
		public Int32 NFIntNo;
		public Int32 AutIntNo;
		public string NFFileName;
		public DateTime NFSendDate;
        public DateTime NFReturnDate;
		public Int32 NFNoOfFrames;
        public Int32 NFNoOfSuccess;
        public Int32 NFNoOfFailed;
        public Int32 NFNoOfNotFound;
        public Int32 NFNoOfPrevUpdated;
		public string LastUser;
	}

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public partial class NaTISFilesDB
	{
		string mConstr = "";

			public NaTISFilesDB (string vConstr)
			{
				mConstr = vConstr;
			}

		//*******************************************************
		//
		// The GetNaTISFilesDetails method returns a NaTISFilesDetails
		// struct that contains information about a specific transaction number
		//
		//*******************************************************
            // 2013-07-19 comment by Henry for useless
        //public NaTISFilesDetails GetNaTISFilesDetails(int nfIntNo) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("NaTISFilesDetail", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterNFIntNo = new SqlParameter("@NFIntNo", SqlDbType.Int, 4);
        //    parameterNFIntNo.Value = nfIntNo;
        //    myCommand.Parameters.Add(parameterNFIntNo);

        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
        //    // Create CustomerDetails Struct
        //    NaTISFilesDetails myNaTISFilesDetails = new NaTISFilesDetails();

        //    while (result.Read())
        //    {
        //        // Populate Struct using Output Params from SPROC
        //        myNaTISFilesDetails.NFIntNo = Convert.ToInt32(result["NFIntNo"]);
        //        myNaTISFilesDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
        //        myNaTISFilesDetails.NFFileName = result["NFFileName"].ToString();
        //        myNaTISFilesDetails.NFNoOfFrames = Convert.ToInt32(result["NFNoOfFrames"]);
        //        myNaTISFilesDetails.NFNoOfSuccess = Convert.ToInt32(result["NFNoOfSuccess"]);
        //        myNaTISFilesDetails.NFNoOfFailed = Convert.ToInt32(result["NFNoOfFailed"]);
        //        myNaTISFilesDetails.NFNoOfNotFound = Convert.ToInt32(result["NFNoOfNotFound"]);
        //        myNaTISFilesDetails.NFNoOfNotFound = Convert.ToInt32(result["NFNoOfNotFound"]);
        //        myNaTISFilesDetails.NFNoOfPrevUpdated = Convert.ToInt32(result["NFNoOfPrevUpdated"]);
        //        if (result["NFSendDate"] != System.DBNull.Value)
        //            myNaTISFilesDetails.NFSendDate = Convert.ToDateTime(result["NFSendDate"]);
        //        if (result["NFReturnDate"] != System.DBNull.Value)
        //            myNaTISFilesDetails.NFReturnDate = Convert.ToDateTime(result["NFReturnDate"]);
        //        myNaTISFilesDetails.LastUser = result["LastUser"].ToString();
        //    }
        //    result.Close();
        //    return myNaTISFilesDetails;
        //}
        // 2013-07-19 comment by Henry for useless
        //public SqlDataReader GetNaTISFilesList(int autIntNo, DateTime dateFrom, DateTime dateTo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("NaTISFilessList", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterDateFrom = new SqlParameter("@DateFrom", SqlDbType.SmallDateTime);
        //    parameterDateFrom.Value = dateFrom;
        //    myCommand.Parameters.Add(parameterDateFrom);

        //    SqlParameter parameterDateTo = new SqlParameter("@DateTo", SqlDbType.SmallDateTime);
        //    parameterDateTo.Value = dateTo;
        //    myCommand.Parameters.Add(parameterDateTo);


        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the datareader result
        //    return result;
        //}

        public DataSet GetNaTISFilesListDS(int autIntNo, DateTime dateFrom, DateTime dateTo)
		{
			SqlDataAdapter sqlDANaTISFiles = new SqlDataAdapter();
			DataSet dsNaTISFiless = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDANaTISFiles.SelectCommand = new SqlCommand();
			sqlDANaTISFiles.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDANaTISFiles.SelectCommand.CommandText = "NaTISFilesList";

			// Mark the Command as a SPROC
			sqlDANaTISFiles.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			sqlDANaTISFiles.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterDateFrom = new SqlParameter("@DateFrom", SqlDbType.SmallDateTime);
            parameterDateFrom.Value = dateFrom;
            sqlDANaTISFiles.SelectCommand.Parameters.Add(parameterDateFrom);

            SqlParameter parameterDateTo = new SqlParameter("@DateTo", SqlDbType.SmallDateTime);
            parameterDateTo.Value = dateTo;
            sqlDANaTISFiles.SelectCommand.Parameters.Add(parameterDateTo);

			// Execute the command and close the connection
			sqlDANaTISFiles.Fill(dsNaTISFiless);
			sqlDANaTISFiles.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsNaTISFiless;		
		}

        public int UpdateNatisFilesError(int autIntNo, string natisFile, string nrCode, string nrError, string lastUser, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NatisFilesUpdateError", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterNFFileName = new SqlParameter("@NFFileName", SqlDbType.VarChar, 10);
            parameterNFFileName.Value = natisFile;
            myCommand.Parameters.Add(parameterNFFileName);

            SqlParameter parameterNRCode = new SqlParameter("@NRCode", SqlDbType.Char, 1);
            parameterNRCode.Value = nrCode;
            myCommand.Parameters.Add(parameterNRCode);

            SqlParameter parameterNRError = new SqlParameter("@NRError", SqlDbType.VarChar, 100);
            parameterNRError.Value = nrError;
            myCommand.Parameters.Add(parameterNRError);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterNFIntNo = new SqlParameter("@NFIntNo", SqlDbType.Int, 4);
            parameterNFIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNFIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int nfIntNo = Convert.ToInt32(parameterNFIntNo.Value);

                return nfIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                errMessage = e.Message;
                return 0;
            }
        }

        public DateTime GetLastNatisDate(int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NatisFilesLastDate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterNFSendDate = new SqlParameter("@NFSendDate", SqlDbType.SmallDateTime);
            parameterNFSendDate.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNFSendDate);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                DateTime nfSendDate = Convert.ToDateTime(parameterNFSendDate.Value);

                return nfSendDate;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string errMessage = e.Message;
                return DateTime.MinValue;
            }
        }

        public int GetNatisDateCount(int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NatisFilesDateCount", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            
            SqlParameter parameterNFCount= new SqlParameter("@NFCount", SqlDbType.Int,4);
            parameterNFCount.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNFCount);


            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int nCount = Convert.ToInt32(parameterNFCount.Value);

                return nCount;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string errMessage = e.Message;
                return -1;
            }
        }

        public int UpdateNatisFilesReturnDate(int autIntNo, string natisFile, string lastUser, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("NatisFilesReturnUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterNFFileName = new SqlParameter("@NFFileName", SqlDbType.VarChar, 10);
            parameterNFFileName.Value = natisFile;
            myCommand.Parameters.Add(parameterNFFileName);

            SqlParameter parameterNFIntNo = new SqlParameter("@NFIntNo", SqlDbType.Int, 4);
            parameterNFIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNFIntNo);

            myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;    // 2013-07-29 add by Henry

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int nfIntNo = Convert.ToInt32(parameterNFIntNo.Value);

                return nfIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                errMessage = e.Message;
                return -2;
            }
        }

        public DataSet GetNaTISStatsDS(int autIntNo, ref int noOfNotFound, ref int noOfFailed)
        {
            SqlDataAdapter sqlDANaTISFiles = new SqlDataAdapter();
            DataSet dsNaTISFiless = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDANaTISFiles.SelectCommand = new SqlCommand();
            sqlDANaTISFiles.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDANaTISFiles.SelectCommand.CommandText = "NaTISFilesStatisticsGet";

            // Mark the Command as a SPROC
            sqlDANaTISFiles.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDANaTISFiles.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterNoOfNotFound = new SqlParameter("@NoOfNotFound", SqlDbType.Int, 4);
            parameterNoOfNotFound.Value = noOfNotFound;
            parameterNoOfNotFound.Direction = ParameterDirection.InputOutput;
            sqlDANaTISFiles.SelectCommand.Parameters.Add(parameterNoOfNotFound);

            SqlParameter parameterNoOfFailed = new SqlParameter("@NoOfFailed", SqlDbType.Int, 4);
            parameterNoOfFailed.Value = noOfFailed;
            parameterNoOfFailed.Direction = ParameterDirection.InputOutput;
            sqlDANaTISFiles.SelectCommand.Parameters.Add(parameterNoOfFailed);

            // Execute the command and close the connection
            sqlDANaTISFiles.Fill(dsNaTISFiless);

            // Calculate the CustomerID using Output Param from SPROC
            noOfNotFound = Convert.ToInt32(parameterNoOfNotFound.Value);
            noOfFailed = Convert.ToInt32(parameterNoOfFailed.Value);

            sqlDANaTISFiles.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsNaTISFiless;
        }

	}
}
