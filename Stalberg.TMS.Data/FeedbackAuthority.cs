using System;
using System.Collections.Generic;
using System.Text;
using Stalberg.ThaboAggregator.Objects;

namespace Stalberg.TMS_TPExInt.Objects
{
    /// <summary>
    /// Represents an authority that has feedback information to process
    /// </summary>
    public class FeedbackAuthority : RemoteAuthority
    {
        // Fields
        private DateTime dateSent;
        private List<FeedbackPayment> payments;
        private List<EasyPayTransaction> easyPayPayments;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="FeedbackAuthority"/> class.
        /// </summary>
        public FeedbackAuthority()
            : base()
        {
            this.easyPayPayments = new List<EasyPayTransaction>();
            this.payments = new List<FeedbackPayment>();
        }

        /// <summary>
        /// Gets the easy pay payments.
        /// </summary>
        /// <value>The easy pay payments.</value>
        public List<EasyPayTransaction> EasyPayPayments
        {
            get { return this.easyPayPayments; }
        }

        /// <summary>
        /// Gets the list of payments.
        /// </summary>
        /// <value>The payments.</value>
        public List<FeedbackPayment> Payments
        {
            get { return this.payments; }
        }

        /// <summary>
        /// Gets or sets the date that the feedback was sent.
        /// </summary>
        /// <value>The date sent.</value>
        public DateTime DateSent
        {
            get { return this.dateSent; }
            set { this.dateSent = value; }
        }

    }
}
