using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // DepositSlipDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class DepositSlipDetails 
	{
		public Int32 DSIntNo;
		public Int32 AutIntNo;
		public Int32 BAIntNo;
		public DateTime DSDate;
		public double DSAmount;
		public int DSNoOfCheques;
		public double DSCashAmount;
		public double DSChequeAmount;
		public string LastUser;
    }

    //*******************************************************
    //
    // DepositSlipDB Class
    //
    //*******************************************************

    public partial class DepositSlipDB {

		string mConstr = "";

		public DepositSlipDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // DepositSlipDB.GetDepositSlipDetails() Method <a name="GetDepositSlipDetails"></a>
        //
        //*******************************************************
        // 2013-07-19 comment by Henry for useless
        //public DepositSlipDetails GetDepositSlipDetails(int dsIntNo) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("DepositSlipDetail", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterDSIntNo = new SqlParameter("@DSIntNo", SqlDbType.Int, 4);
        //    parameterDSIntNo.Value = dsIntNo;
        //    myCommand.Parameters.Add(parameterDSIntNo);

        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
        //    // Create CustomerDetails Struct
        //    DepositSlipDetails myDepositSlipDetails = new DepositSlipDetails();

        //    while (result.Read())
        //    {
        //        // Populate Struct using Output Params from SPROC
        //        myDepositSlipDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
        //        myDepositSlipDetails.DSDate = Convert.ToDateTime(result["DSDate"]);
        //        myDepositSlipDetails.DSAmount = Convert.ToDouble(result["DSAmount"]);
        //    }
        //    result.Close();
        //    return myDepositSlipDetails;
        //}

        //*******************************************************
        //
        // DepositSlipDB.AddDepositSlip() Method <a name="AddDepositSlip"></a>
        //
        //*******************************************************

        public int AddDepositSlip (int autIntNo, string DSAmount, string DSDate, 
			string lastUser) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("DepositSlipAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			myCommand.Parameters.Add(parameterAutIntNo);

			SqlParameter parameterDSDate = new SqlParameter("@DSDate", SqlDbType.VarChar, 18);
			parameterDSDate.Value = DSDate;
			myCommand.Parameters.Add(parameterDSDate);

			SqlParameter parameterDSAmount = new SqlParameter("@DSAmount", SqlDbType.VarChar, 5);
			parameterDSAmount.Value = DSAmount;
			myCommand.Parameters.Add(parameterDSAmount);
			
			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterDSIntNo = new SqlParameter("@DSIntNo", SqlDbType.Int, 4);
            parameterDSIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterDSIntNo);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int dsIntNo = Convert.ToInt32(parameterDSIntNo.Value);

                return dsIntNo;
            }
            catch 
			{
				myConnection.Dispose();
                return 0;
            }
        }

        // FBJ Added (2006-11-21): Unused, SP removed
        //public SqlDataReader GetDepositSlipList(int autIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("DepositSlipList", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the datareader result
        //    return result;
        //}

		// 20050723 converted to depositSlip		
		public DataSet GetDepositSlipListDS(int autIntNo)
		{
			SqlDataAdapter sqlDADepositSlips = new SqlDataAdapter();
			DataSet dsDepositSlips = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDADepositSlips.SelectCommand = new SqlCommand();
			sqlDADepositSlips.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDADepositSlips.SelectCommand.CommandText = "DepositSlipList";

			// Mark the Command as a SPROC
			sqlDADepositSlips.SelectCommand.CommandType = CommandType.StoredProcedure;

			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			sqlDADepositSlips.SelectCommand.Parameters.Add(parameterAutIntNo);

			// Execute the command and close the connection
			sqlDADepositSlips.Fill(dsDepositSlips);
			sqlDADepositSlips.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsDepositSlips;		
		}

        // 2013-07-19 comment by Henry for useless
        //public int UpdateDepositSlip (int dsIntNo, int autIntNo,  string DSAmount, 
        //    string DSDate, string lastUser) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("DepositSlipUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterDSDate = new SqlParameter("@DSDate", SqlDbType.VarChar, 18);
        //    parameterDSDate.Value = DSDate;
        //    myCommand.Parameters.Add(parameterDSDate);

        //    SqlParameter parameterDSAmount = new SqlParameter("@DSAmount", SqlDbType.VarChar, 5);
        //    parameterDSAmount.Value = DSAmount;
        //    myCommand.Parameters.Add(parameterDSAmount);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterDSIntNo = new SqlParameter("@DSIntNo", SqlDbType.Int);
        //    parameterDSIntNo.Direction = ParameterDirection.InputOutput;
        //    parameterDSIntNo.Value = dsIntNo;
        //    myCommand.Parameters.Add(parameterDSIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int DSIntNo = (int)myCommand.Parameters["@DSIntNo"].Value;
        //        //int menuId = (int)parameterDSIntNo.Value;

        //        return DSIntNo;
        //    }
        //    catch 
        //    {
        //        myConnection.Dispose();
        //        return 0;
        //    }
        //}

        /// <summary>
        /// Deletes the deposit slip.
        /// </summary>
        /// <param name="dsIntNo">The ds int no.</param>
        /// <returns></returns>
		public String DeleteDepositSlip (int dsIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("DepositSlipDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterDSIntNo = new SqlParameter("@DSIntNo", SqlDbType.Int, 4);
			parameterDSIntNo.Value = dsIntNo;
			parameterDSIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterDSIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int DSIntNo = (int)parameterDSIntNo.Value;

				return DSIntNo.ToString();
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return String.Empty;
			}
		}

        /// <summary>
        /// Gets the supervisor deposit slip list.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetSupervisorDepositSlipList()
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("DepositSlipSupervisorList", con);
            com.CommandType = CommandType.StoredProcedure;

            //com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        /// <summary>
        /// Locks the deposit slips for an authority after the end of day cash up.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="lastUser">The last user.</param>
        public void LockDepositSlips(string lastUser)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("DepositSlipLock", con);
            com.CommandType = CommandType.StoredProcedure;

            //com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
                con.Dispose();
                com.Dispose();
            }
        }

    }
}

