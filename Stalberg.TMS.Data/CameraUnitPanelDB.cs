using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    //*******************************************************
    //
    // CameraUnitPanelDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class CameraUnitPanelDetails
    {
        public string CamUnitID;
        public string AutNo;
        public string CUPDocType;
        public int CUPPanelNo;
        public int CUPPanelStart;
        public int CUPPanelEnd;
        public int CUPNextNo;
        public string CUPPanelUsedUp;
        public string CUPConfigFile;
        public DateTime CUPPanelLoadDate;
        public DateTime CUPPanelUsedUpDate;
        public string CUPLoadFlag;
        public string LastUser;
    }

    //*******************************************************
    //
    // CameraUnitPanel Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query CiprusOffences within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class CameraUnitPanelDB
    {
        string mConstr = "";

        public CameraUnitPanelDB(string vConstr)
        {
            mConstr = vConstr;
        }

        //*******************************************************
        //
        // CiprusOffenceDB.GetCiprusOffenceDetails() Method <a name="GetCiprusOffenceDetails"></a>
        //
        // The GetCiprusOffenceDetails method returns a CiprusOffenceDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************
        // 2013-07-19 comment by Henry for useless
        //public int InsertCameraUnitPanel(CameraUnitPanelDetails panel, ref string errMessage)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand cmd = new SqlCommand("CameraUnitPanelInsert", myConnection);

        //    // Mark the Command as a SPROC
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    cmd.Parameters.Add("@CamUnitID", SqlDbType.VarChar, 5).Value = panel.CamUnitID.Trim();
        //    cmd.Parameters.Add("@AutNo", SqlDbType.VarChar, 6).Value = panel.AutNo.Trim();
        //    cmd.Parameters.Add("@CUPDocType", SqlDbType.Char, 2).Value = panel.CUPDocType;
        //    cmd.Parameters.Add("@CUPPanelStart", SqlDbType.Int, 4).Value = panel.CUPPanelStart;
        //    cmd.Parameters.Add("@CUPPanelEnd", SqlDbType.Int, 4).Value = panel.CUPPanelEnd;
        //    cmd.Parameters.Add("@CUPConfigFile", SqlDbType.VarChar, 30).Value = panel.CUPConfigFile;
        //    cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = panel.LastUser;
        //    cmd.Parameters.Add("@CUPIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

        //    try
        //    {
        //        myConnection.Open();
        //        cmd.ExecuteNonQuery();

        //        int cupIntNo = (int)cmd.Parameters["@CUPIntNo"].Value;

        //        return cupIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        string msg = e.Message;
        //        return 0;
        //    }
        //    finally
        //    {
        //        myConnection.Dispose();
        //    }
        //}

        public int ResetCameraUnitPanel(string camUnitID, string autNo, string lastUser, int startNo, int cupIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand cmd = new SqlCommand("CameraUnitPanelReset", myConnection);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            cmd.Parameters.Add("@CamUnitID", SqlDbType.VarChar, 5).Value = camUnitID;
            cmd.Parameters.Add("@AutNo", SqlDbType.VarChar, 6).Value = autNo;
            cmd.Parameters.Add("@StartNo", SqlDbType.Int, 4).Value = startNo;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            cmd.Parameters.Add("@CUPIntNo", SqlDbType.Int, 4).Value = cupIntNo;
            cmd.Parameters["@CUPIntNo"].Direction = ParameterDirection.InputOutput;

            try
            {
                myConnection.Open();
                cmd.ExecuteNonQuery();

                cupIntNo = (int)cmd.Parameters["@CUPIntNo"].Value;

                return cupIntNo;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        public SqlDataReader GetNextNoFromPanel(string camUnitID, string autNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CameraUnitPanelNextNo", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@CamUnitID", SqlDbType.VarChar, 5).Value = camUnitID;
            myCommand.Parameters.Add("@AutNo", SqlDbType.VarChar, 6).Value = autNo;

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        // 2013-07-19 comment by Henry for useless
        //public SqlDataReader CheckNoticeNoAvailability(string camUnitID, string autNo, int noToBeAvailable, ref string errMessage)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand cmd = new SqlCommand("CameraUnitPanelNumberAvailability", myConnection);

        //    // Mark the Command as a SPROC
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    cmd.Parameters.Add("@CamUnitID", SqlDbType.VarChar, 5).Value = camUnitID;
        //    cmd.Parameters.Add("@AutNo", SqlDbType.VarChar, 6).Value = autNo;
        //    cmd.Parameters.Add("@NoToBeAvailable", SqlDbType.Int, 4).Value = noToBeAvailable;

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = cmd.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the data reader result
        //    return result;
        //}


        public bool ResetLoadFlag(string autNo, string camUnitID, ref string errMessage, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand cmd = new SqlCommand("CameraUnitPanelResetLoadFlag", myConnection);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            cmd.Parameters.Add("@CamUnitID", SqlDbType.VarChar, 5).Value = camUnitID;
            cmd.Parameters.Add("@AutNo", SqlDbType.VarChar, 6).Value = autNo;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                myConnection.Open();
                cmd.ExecuteNonQuery();

                return false;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return true;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        public int CheckNextPanelStart(string autNo, string camUnitID, int cupIntNo, string lastUser, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand cmd = new SqlCommand("CameraUnitPanelCheckNextPanelStart", myConnection);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            cmd.Parameters.Add("@CamUnitID", SqlDbType.VarChar, 5).Value = camUnitID;
            cmd.Parameters.Add("@AutNo", SqlDbType.VarChar, 6).Value = autNo;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            cmd.Parameters.Add("@CUPIntNo", SqlDbType.Int, 4).Value = cupIntNo;
            cmd.Parameters["@CUPIntNo"].Direction = ParameterDirection.InputOutput;

            try
            {
                myConnection.Open();
                cmd.ExecuteNonQuery();

                cupIntNo = (int)cmd.Parameters["@CUPIntNo"].Value;

                return cupIntNo;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return -2;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        public DataSet GetCameraUnitPanelsDS(int autIntNo, int noToBeAvailable)
        {
            SqlDataAdapter sqlDA = new SqlDataAdapter();
            DataSet ds = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDA.SelectCommand = new SqlCommand();
            sqlDA.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDA.SelectCommand.CommandText = "CameraUnitCheckPanelsAvailable";

            // Mark the Command as a SPROC
            sqlDA.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDA.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            sqlDA.SelectCommand.Parameters.Add("@NoToBeAvailable", SqlDbType.Int, 4).Value = noToBeAvailable;

            // Execute the command and close the connection
            sqlDA.Fill(ds);
            sqlDA.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return ds;
        }

        // 2013-07-19 comment by Henry for useless
        //public DataSet ClearTicketsWithIncorrectStatus(Int32 autIntNo, string camUnitID)
        //{
        //    SqlDataAdapter sqlDA = new SqlDataAdapter();
        //    DataSet ds = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlDA.SelectCommand = new SqlCommand();
        //    sqlDA.SelectCommand.Connection = new SqlConnection(mConstr);
        //    sqlDA.SelectCommand.CommandText = "CameraUnitClearTickets";

        //    // Mark the Command as a SPROC
        //    sqlDA.SelectCommand.CommandType = CommandType.StoredProcedure;
        //    sqlDA.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    sqlDA.SelectCommand.Parameters.Add("@CamUnitID", SqlDbType.VarChar, 5).Value = camUnitID;

        //    // Execute the command and close the connection
        //    sqlDA.Fill(ds);
        //    sqlDA.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return ds;
        //}
    }
}

