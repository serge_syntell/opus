using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{

	//*******************************************************
	//
	// AccountDetails Class
	//
	// A simple data class that encapsulates details about a particular loc 
	//
	//*******************************************************

	public partial class AccountDetails
	{
		public Int32 AccIntNo;
		public string AccountNo;
		public string AccName;
		public string AccVATStatus;
	}

	//*******************************************************
	//
	// AccountDB Class
	//
	//*******************************************************

	public partial class AccountDB
	{
		string mConstr = "";

		public AccountDB (string vConstr)
		{
			mConstr = vConstr;
		}

		//*******************************************************
		//
		//*******************************************************

		public AccountDetails GetAccountDetails (int accIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("AccountDetail", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterAccIntNo = new SqlParameter("@AccIntNo", SqlDbType.Int, 4);
			parameterAccIntNo.Value = accIntNo;
			myCommand.Parameters.Add(parameterAccIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			AccountDetails myAccountDetails = new AccountDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myAccountDetails.AccIntNo = Convert.ToInt32(result["AccIntNo"]);
				myAccountDetails.AccountNo = result["AccountNo"].ToString();
				myAccountDetails.AccName = result["AccName"].ToString();
				myAccountDetails.AccVATStatus = result["AccVATStatus"].ToString();
			}
			result.Close();
			return myAccountDetails;
		}

		public int AddAccount (string accountNo, string accName, string accVATStatus, string lastUser)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("AccountAdd", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			SqlParameter parameterAccountNo = new SqlParameter("@AccountNo", SqlDbType.VarChar, 20);
			parameterAccountNo.Value = accountNo;
			myCommand.Parameters.Add(parameterAccountNo);

			SqlParameter parameterAccName = new SqlParameter("@AccName", SqlDbType.VarChar, 50);
			parameterAccName.Value = accName;
			myCommand.Parameters.Add(parameterAccName);

			SqlParameter parameterAccVATStatus = new SqlParameter("@AccVATStatus", SqlDbType.Char, 1);
			parameterAccVATStatus.Value = accVATStatus;
			myCommand.Parameters.Add(parameterAccVATStatus);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterAccIntNo = new SqlParameter("@AccIntNo", SqlDbType.Int, 4);
			parameterAccIntNo.Direction = ParameterDirection.Output;
			myCommand.Parameters.Add(parameterAccIntNo);

			try
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int accIntNo = Convert.ToInt32(parameterAccIntNo.Value);

				return accIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		public int GetAccount (int ctIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("Acc_CTGetAccIntNo", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			//myCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
			myCommand.Parameters.Add("@CTIntNo", SqlDbType.Int, 4).Value = ctIntNo;

			SqlParameter parameterAccIntNo = new SqlParameter("@AccIntNo", SqlDbType.Int, 4);
			parameterAccIntNo.Direction = ParameterDirection.Output;
			myCommand.Parameters.Add(parameterAccIntNo);

			try
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int accIntNo = Convert.ToInt32(parameterAccIntNo.Value);

				return accIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		/// <summary>
		/// Gets the list of ledger accounts for the supplied authority.
		/// </summary>
		/// <param name="autIntNo">The authority int no.</param>
		/// <returns></returns>
		public SqlDataReader GetAccountList ()
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("AccountList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the data reader result
			return result;
		}

		// 20050715 converted to account
		public DataSet GetAccountListDS ()
		{
			SqlDataAdapter sqlDAAccounts = new SqlDataAdapter();
			DataSet dsAccounts = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAAccounts.SelectCommand = new SqlCommand();
			sqlDAAccounts.SelectCommand.Connection = new SqlConnection(mConstr);
			sqlDAAccounts.SelectCommand.CommandText = "AccountList";

			// Mark the Command as a SPROC
			sqlDAAccounts.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Execute the command and close the connection
			sqlDAAccounts.Fill(dsAccounts);
			sqlDAAccounts.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsAccounts;
		}
		// 20050715 converted to account
		public int UpdateAccount (int accIntNo, string accountNo, string accName, string accVATStatus, string lastUser)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("AccountUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			SqlParameter parameterAccountNo = new SqlParameter("@AccountNo", SqlDbType.VarChar, 20);
			parameterAccountNo.Value = accountNo;
			myCommand.Parameters.Add(parameterAccountNo);

			SqlParameter parameterAccName = new SqlParameter("@AccName", SqlDbType.VarChar, 50);
			parameterAccName.Value = accName;
			myCommand.Parameters.Add(parameterAccName);

			SqlParameter parameterAccVATStatus = new SqlParameter("@AccVATStatus", SqlDbType.Char, 1);
			parameterAccVATStatus.Value = accVATStatus;
			myCommand.Parameters.Add(parameterAccVATStatus);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterAccIntNo = new SqlParameter("@AccIntNo", SqlDbType.Int);
			parameterAccIntNo.Direction = ParameterDirection.InputOutput;
			parameterAccIntNo.Value = accIntNo;
			myCommand.Parameters.Add(parameterAccIntNo);

			try
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				accIntNo = (int)myCommand.Parameters["@AccIntNo"].Value;

				return accIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}


		public int DeleteAccount (int accIntNo, ref string errMessage)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("AccountDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterAccIntNo = new SqlParameter("@AccIntNo", SqlDbType.Int, 4);
			parameterAccIntNo.Value = accIntNo;
			parameterAccIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterAccIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();

                // Calculate the CustomerID using Output Param from SPROC
                accIntNo = (int)parameterAccIntNo.Value;

                return accIntNo;
            }
            catch (Exception ex)
            {
                errMessage = ex.Message; 
                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
		}

		public int AddAcc_CT (int accIntNo, int ctIntNo, string lastUser)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("Acc_CTAdd", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterAccIntNo = new SqlParameter("@AccIntNo", SqlDbType.Int, 4);
			parameterAccIntNo.Value = accIntNo;
			myCommand.Parameters.Add(parameterAccIntNo);

			SqlParameter parameterCTIntNo = new SqlParameter("@CTIntNo", SqlDbType.Int, 4);
			parameterCTIntNo.Value = ctIntNo;
			myCommand.Parameters.Add(parameterCTIntNo);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterAcCtIntNo = new SqlParameter("@AcCtIntNo", SqlDbType.Int, 4);
			parameterAcCtIntNo.Direction = ParameterDirection.Output;
			myCommand.Parameters.Add(parameterAcCtIntNo);

			try
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int acCtIntNo = Convert.ToInt32(parameterAcCtIntNo.Value);

				return acCtIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

	}
}

