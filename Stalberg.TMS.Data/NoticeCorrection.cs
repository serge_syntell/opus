using System;
using System.Data;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a Notice with Natis data needing Correction
    /// </summary>
    public class NoticeCorrection
    {
        // Fields

        /// <summary>
        /// Initializes a new instance of the <see cref="NoticeCorrection"/> class.
        /// </summary>
        public NoticeCorrection()
        {
        }

    }
}