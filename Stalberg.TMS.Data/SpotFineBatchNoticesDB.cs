using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Collections;

namespace Stalberg.TMS
{
	/// <summary>
	/// Represents the details of a spot-fine batch
	/// </summary>
	public class SpotFineBatchNoticesDetails
	{
		public Int32 SFBNIntNo;
		public Int32 SFBIntNo;
		public Int32 AutIntNo;
		public Int32 NotIntNo;
		public string NotTicketNo;
		public DateTime NotOffenceDate;
		public string SFBNRctNumber;
		public decimal SFBNRTAmount;
		public Int32 ChgIntNo;
		public string ChgOffenceCode;
		public string LastUser;
	}

	/// <summary>
	/// Manages all the interaction between the Web site and the database concerning SpotFine batches
	/// </summary>
	public partial class SpotFineBatchNoticesDB
	{
		// String
		private string connectionString = "";

		/// <summary>
		/// Initializes a new instance of the <see cref="SpotFineBatchNoticesDB"/> class.
		/// </summary>
		/// <param name="connectionString">The connection string.</param>
		public SpotFineBatchNoticesDB(string connectionString)
		{
			this.connectionString = connectionString;
		}

		/// <summary>
		/// The GetSpotFineBatchNoticesDetails method returns a SpotFineBatchNoticesDetails
		/// struct that contains information about a specific transaction number
		/// </summary>
		/// <param name="sfbIntNo">The SFB int no.</param>
		/// <returns></returns>
        // 2013-07-19 comment by Henry for useless
        //public SpotFineBatchNoticesDetails GetSpotFineBatchNoticesDetails(int sfbIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(this.connectionString);
        //    SqlCommand myCommand = new SqlCommand("SpotFineBatchNoticesDetail", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterSFBIntNo = new SqlParameter("@SFBIntNo", SqlDbType.Int, 4);
        //    parameterSFBIntNo.Value = sfbIntNo;
        //    myCommand.Parameters.Add(parameterSFBIntNo);

        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Create CustomerDetails Struct
        //    SpotFineBatchNoticesDetails mySpotFineBatchNoticesDetails = new SpotFineBatchNoticesDetails();

        //    while (result.Read())
        //    {
        //        // Populate Struct using Output Params from SPROC
        //        mySpotFineBatchNoticesDetails.SFBNIntNo = Convert.ToInt32(result["SFBNIntNo"]);
        //        mySpotFineBatchNoticesDetails.SFBIntNo = Convert.ToInt32(result["SFBIntNo"]);
        //        mySpotFineBatchNoticesDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
        //        mySpotFineBatchNoticesDetails.NotIntNo = Convert.ToInt32(result["NotIntNo"]);
        //        mySpotFineBatchNoticesDetails.NotTicketNo = result["NotTicketNo"].ToString();
        //        mySpotFineBatchNoticesDetails.NotOffenceDate = Convert.ToDateTime(result["NotOffenceDate"]);
        //        mySpotFineBatchNoticesDetails.SFBNRctNumber = result["SFBNRctNumber"].ToString();
        //        mySpotFineBatchNoticesDetails.SFBNRTAmount = Convert.ToDecimal(result["SFBNRTAmount"]);
        //        mySpotFineBatchNoticesDetails.ChgIntNo = Convert.ToInt32(result["ChgIntNo"]);
        //        mySpotFineBatchNoticesDetails.ChgOffenceCode = result["ChgOffenceCode"].ToString();
        //        mySpotFineBatchNoticesDetails.LastUser = result["LastUser"].ToString();
        //    }
        //    result.Close();

        //    return mySpotFineBatchNoticesDetails;
        //}

		/// <summary>
		/// Spots the fine batch notice get next data set.
		/// </summary>
		/// <param name="autIntNo">The aut int no.</param>
		/// <param name="sfbIntNo">The SFB int no.</param>
		/// <returns>A <see cref="DataSet"/></returns>
		public DataSet SpotFineBatch_NoticeGetNextDS(int autIntNo, int sfbIntNo)
		{
			SqlConnection con = new SqlConnection(this.connectionString);
			SqlCommand cmd = new SqlCommand("SpotFineBatch_NoticeGetNext", con);
			cmd.CommandType = CommandType.StoredProcedure;

			cmd.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
			cmd.Parameters.Add("@SFBIntNo", SqlDbType.Int, 4).Value = sfbIntNo;

			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(cmd);
			da.Fill(ds);
			da.Dispose();

			return ds;
		}

		/// <summary>
		/// Updates the spot fine batch notice.
		/// </summary>
		/// <param name="sfbnIntNo">The SFBN int no.</param>
		/// <param name="sfbnRctNumber">The SFBN RCT number.</param>
		/// <param name="lastUser">The last user.</param>
		/// <returns></returns>
		public int UpdateSpotFineBatchNotice(int sfbnIntNo, string sfbnRctNumber, string lastUser)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(connectionString);
			SqlCommand myCommand = new SqlCommand("SpotFineBatch_NoticeUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterSFBNIntNo = new SqlParameter("@SFBNIntNo", SqlDbType.Int, 4);
			parameterSFBNIntNo.Value = sfbnIntNo;
			myCommand.Parameters.Add(parameterSFBNIntNo);

			SqlParameter parameterSFBNRctNumber = new SqlParameter("@SFBNRctNumber", SqlDbType.VarChar, 50);
			parameterSFBNRctNumber.Value = sfbnRctNumber;
			myCommand.Parameters.Add(parameterSFBNRctNumber);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			try
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();

				// Was the UPDATE successful or not -  using Output Param from SPROC
				int updSFBNIntNo = (int)myCommand.Parameters["@SFBNIntNo"].Value;

				return updSFBNIntNo;
			}
			catch (Exception e)
			{
				myCommand.Dispose();
				string msg = e.Message;
				return 0;
			}
			finally
			{
				myConnection.Dispose();
			}
		}

        public int UpdateSpotFineBatchNotice(int sfbnIntNo, string sfbnRctNumber, decimal sfbnAmount, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("SpotFineBatch_NoticeUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterSFBNIntNo = new SqlParameter("@SFBNIntNo", SqlDbType.Int, 4);
            parameterSFBNIntNo.Value = sfbnIntNo;
            myCommand.Parameters.Add(parameterSFBNIntNo);

            SqlParameter parameterSFBNRctNumber = new SqlParameter("@SFBNRctNumber", SqlDbType.VarChar, 50);
            parameterSFBNRctNumber.Value = sfbnRctNumber;
            myCommand.Parameters.Add(parameterSFBNRctNumber);

            SqlParameter parameterSFBNAmount = new SqlParameter("@SFBNAmount", SqlDbType.Money);
            parameterSFBNAmount.Value = sfbnAmount;
            myCommand.Parameters.Add(parameterSFBNAmount);

            SqlParameter parameterSFBNPNotice = new SqlParameter("@SFBNPrintNotice", SqlDbType.Bit);
            parameterSFBNPNotice.Value = -1;
            myCommand.Parameters.Add(parameterSFBNPNotice);

            SqlParameter parameterSFBNPReceipt = new SqlParameter("@SFBNPrintReceipt", SqlDbType.Bit);
            parameterSFBNPReceipt.Value = -1;
            myCommand.Parameters.Add(parameterSFBNPReceipt);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();

                // Was the UPDATE successful or not -  using Output Param from SPROC
                int updSFBNIntNo = (int)myCommand.Parameters["@SFBNIntNo"].Value;

                return updSFBNIntNo;
            }
            catch (Exception e)
            {
                myCommand.Dispose();
                string msg = e.Message;
                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        public int UpdateSpotFineBatchNotice(int sfbnIntNo, string sfbnRctNumber, decimal sfbnAmount, bool sfbnPrintNotice, bool sfbnPrintReceipt, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("SpotFineBatch_NoticeUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            int iPrintNotice = 0;
            int iPrintReceipt = 0;
            if (sfbnPrintNotice == true)
            {
                iPrintNotice = 1;
            }

            if (sfbnPrintReceipt == true)
            {
                iPrintReceipt = 1;
            }
            

            // Add Parameters to SPROC
            SqlParameter parameterSFBNIntNo = new SqlParameter("@SFBNIntNo", SqlDbType.Int, 4);
            parameterSFBNIntNo.Value = sfbnIntNo;
            myCommand.Parameters.Add(parameterSFBNIntNo);

            SqlParameter parameterSFBNRctNumber = new SqlParameter("@SFBNRctNumber", SqlDbType.VarChar, 50);
            parameterSFBNRctNumber.Value = sfbnRctNumber;
            myCommand.Parameters.Add(parameterSFBNRctNumber);

            SqlParameter parameterSFBNAmount = new SqlParameter("@SFBNAmount", SqlDbType.Money);
            parameterSFBNAmount.Value = sfbnAmount;
            myCommand.Parameters.Add(parameterSFBNAmount);

            SqlParameter parameterSFBNPNotice = new SqlParameter("@SFBNPrintNotice", SqlDbType.Bit);
            parameterSFBNPNotice.Value = iPrintNotice;
            myCommand.Parameters.Add(parameterSFBNPNotice);

            SqlParameter parameterSFBNPReceipt = new SqlParameter("@SFBNPrintReceipt", SqlDbType.Bit);
            parameterSFBNPReceipt.Value = iPrintReceipt;
            myCommand.Parameters.Add(parameterSFBNPReceipt);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();

                // Was the UPDATE successful or not -  using Output Param from SPROC
                int updSFBNIntNo = (int)myCommand.Parameters["@SFBNIntNo"].Value;

                return updSFBNIntNo;
            }
            catch (Exception e)
            {
                myCommand.Dispose();
                string msg = e.Message;
                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

		//public int AddSpotFineBatch(int sfbIntNo, int autIntNo, DateTime sfbRegisterDate, 
		//     DateTime sfbCreateDate, string sfbCreateUser, DateTime sfbCompleteDate, DateTime sfbCompleteUser,
		//     DateTime sfbPrintDate, DateTime sfbPrintUser, string sfbComments, string lastUser)
		// {
		//     // Create Instance of Connection and Command Object
		//     SqlConnection myConnection = new SqlConnection(mConstr);
		//     SqlCommand myCommand = new SqlCommand("SpotFineBatchAdd", myConnection);

		//     // Mark the Command as a SPROC
		//     myCommand.CommandType = CommandType.StoredProcedure;

		//     // Add Parameters to SPROC
		//     SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
		//     parameterAutIntNo.Value = autIntNo;
		//     myCommand.Parameters.Add(parameterAutIntNo);

		//     SqlParameter parameterARCode = new SqlParameter("@ARCode", SqlDbType.VarChar, 10);
		//     parameterARCode.Value = arCode;
		//     myCommand.Parameters.Add(parameterARCode);

		//     SqlParameter parameterARComment = new SqlParameter("@ARComment", SqlDbType.VarChar, 255);
		//     parameterARComment.Value = arComment;
		//     myCommand.Parameters.Add(parameterARComment);

		//     SqlParameter parameterARNumeric = new SqlParameter("@ARNumeric", SqlDbType.Int);
		//     parameterARNumeric.Value = arNumeric;
		//     myCommand.Parameters.Add(parameterARNumeric);

		//     SqlParameter parameterARDescr = new SqlParameter("@ARDescr", SqlDbType.VarChar, 100);
		//     parameterARDescr.Value = arDescr;
		//     myCommand.Parameters.Add(parameterARDescr);

		//     SqlParameter parameterARString = new SqlParameter("@ARString", SqlDbType.VarChar, 25);
		//     parameterARString.Value = arString;
		//     myCommand.Parameters.Add(parameterARString);

		//     SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
		//     parameterLastUser.Value = lastUser;
		//     myCommand.Parameters.Add(parameterLastUser);

		//     SqlParameter parameterARIntNo = new SqlParameter("@ARIntNo", SqlDbType.Int, 4);
		//     parameterARIntNo.Direction = ParameterDirection.Output;
		//     myCommand.Parameters.Add(parameterARIntNo);

		//     try
		//     {
		//         myConnection.Open();
		//         myCommand.ExecuteNonQuery();
		//         myConnection.Dispose();

		//         int arIntNo = Convert.ToInt32(parameterARIntNo.Value);

		//         return arIntNo;
		//     }
		//     catch (Exception e)
		//     {
		//         myConnection.Dispose();
		//         string msg = e.Message;
		//         return 0;
		//     }
		// }


		/// <summary>
		/// Gets the spot fine batch notices list.
		/// </summary>
		/// <param name="autIntNo">The aut int no.</param>
		/// <param name="sfbIntNo">The SFB int no.</param>
		/// <returns></returns>
		public SqlDataReader GetSpotFineBatchNoticesList(int autIntNo, int sfbIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(connectionString);
			SqlCommand myCommand = new SqlCommand("SpotFineBatchNoticeList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			myCommand.Parameters.Add(parameterAutIntNo);

			SqlParameter parameterSFBIntNo = new SqlParameter("@SFBIntNo", SqlDbType.Int, 4);
			parameterSFBIntNo.Value = sfbIntNo;
			myCommand.Parameters.Add(parameterSFBIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the data reader result
			return result;
		}

		/// <summary>
		/// Gets the spot fine batch notices list DS.
		/// </summary>
		/// <param name="autIntNo">The aut int no.</param>
		/// <param name="sfbIntNo">The SFB int no.</param>
		/// <returns></returns>
        /// 2013-04-1 Modify by Henry for pagination
        public DataSet GetSpotFineBatchNoticesListDS(int autIntNo, int sfbIntNo,
            //2013-04-1 add by Henry for pagination
            string sortColumn, string drection,
            int pageSize, int pageIndex, out int totalCount)
		{
			SqlDataAdapter sqlDASpotFineBatchNotices = new SqlDataAdapter();
			DataSet dsSpotFineBatchNotices = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDASpotFineBatchNotices.SelectCommand = new SqlCommand();
			sqlDASpotFineBatchNotices.SelectCommand.Connection = new SqlConnection(connectionString);
			sqlDASpotFineBatchNotices.SelectCommand.CommandText = "SpotFineBatchNoticeList";

			// Mark the Command as a SPROC
			sqlDASpotFineBatchNotices.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
			parameterAutIntNo.Value = autIntNo;
			sqlDASpotFineBatchNotices.SelectCommand.Parameters.Add(parameterAutIntNo);

			SqlParameter parameterSFBIntNo = new SqlParameter("@SFBIntNo", SqlDbType.Int, 4);
			parameterSFBIntNo.Value = sfbIntNo;
			sqlDASpotFineBatchNotices.SelectCommand.Parameters.Add(parameterSFBIntNo);

            sqlDASpotFineBatchNotices.SelectCommand.Parameters.Add("@SortColumn", SqlDbType.NVarChar, 20).Value = sortColumn;
            sqlDASpotFineBatchNotices.SelectCommand.Parameters.Add("@Direction", SqlDbType.NVarChar, 20).Value = drection;
            sqlDASpotFineBatchNotices.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            sqlDASpotFineBatchNotices.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            sqlDASpotFineBatchNotices.SelectCommand.Parameters.Add(paraTotalCount);

			// Execute the command and close the connection
			sqlDASpotFineBatchNotices.Fill(dsSpotFineBatchNotices);
			sqlDASpotFineBatchNotices.SelectCommand.Connection.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

			// Return the dataset result
			return dsSpotFineBatchNotices;
		}

        public DataSet GetSpotFineBatchNoticesListDS(int autIntNo, int sfbIntNo, bool forSpotFines, bool forSummons,
            //2013-04-1 add by Henry for pagination
            string sortColumn, string drection,
            int pageSize, int pageIndex, out int totalCount)
        {
            SqlDataAdapter sqlDASpotFineBatchNotices = new SqlDataAdapter();
            DataSet dsSpotFineBatchNotices = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDASpotFineBatchNotices.SelectCommand = new SqlCommand();
            sqlDASpotFineBatchNotices.SelectCommand.Connection = new SqlConnection(connectionString);
            sqlDASpotFineBatchNotices.SelectCommand.CommandText = "SpotFineBatchNoticeList";

            // Mark the Command as a SPROC
            sqlDASpotFineBatchNotices.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDASpotFineBatchNotices.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterSFBIntNo = new SqlParameter("@SFBIntNo", SqlDbType.Int, 4);
            parameterSFBIntNo.Value = sfbIntNo;
            sqlDASpotFineBatchNotices.SelectCommand.Parameters.Add(parameterSFBIntNo);

            SqlParameter parameterForSpotFines = new SqlParameter("@ForSpotFines", SqlDbType.Bit);
            parameterForSpotFines.Value = forSpotFines;
            sqlDASpotFineBatchNotices.SelectCommand.Parameters.Add(parameterForSpotFines);

            SqlParameter parameterForSummons = new SqlParameter("@ForSummons", SqlDbType.Bit);
            parameterForSummons.Value = forSummons;
            sqlDASpotFineBatchNotices.SelectCommand.Parameters.Add(parameterForSummons);

            sqlDASpotFineBatchNotices.SelectCommand.Parameters.Add("@SortColumn", SqlDbType.NVarChar, 20).Value = sortColumn;
            sqlDASpotFineBatchNotices.SelectCommand.Parameters.Add("@Direction", SqlDbType.NVarChar, 20).Value = drection;
            sqlDASpotFineBatchNotices.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            sqlDASpotFineBatchNotices.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            sqlDASpotFineBatchNotices.SelectCommand.Parameters.Add(paraTotalCount);

            // Execute the command and close the connection
            sqlDASpotFineBatchNotices.Fill(dsSpotFineBatchNotices);
            sqlDASpotFineBatchNotices.SelectCommand.Connection.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            // Return the dataset result
            return dsSpotFineBatchNotices;
        }

		//public int UpdateSpotFineBatch(int autIntNo, string arCode, string arDescr,
		//    int arNumeric, string arString, string arComment, string lastUser, int arIntNo)
		//{
		//    // Create Instance of Connection and Command Object
		//    SqlConnection myConnection = new SqlConnection(mConstr);
		//    SqlCommand myCommand = new SqlCommand("SpotFineBatchUpdate", myConnection);

		//    // Mark the Command as a SPROC
		//    myCommand.CommandType = CommandType.StoredProcedure;

		//    // Add Parameters to SPROC
		//    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
		//    parameterAutIntNo.Value = autIntNo;
		//    myCommand.Parameters.Add(parameterAutIntNo);

		//    SqlParameter parameterARCode = new SqlParameter("@ARCode", SqlDbType.VarChar, 10);
		//    parameterARCode.Value = arCode;
		//    myCommand.Parameters.Add(parameterARCode);

		//    SqlParameter parameterARComment = new SqlParameter("@ARComment", SqlDbType.VarChar, 255);
		//    parameterARComment.Value = arComment;
		//    myCommand.Parameters.Add(parameterARComment);

		//    SqlParameter parameterARNumeric = new SqlParameter("@ARNumeric", SqlDbType.Int);
		//    parameterARNumeric.Value = arNumeric;
		//    myCommand.Parameters.Add(parameterARNumeric);

		//    SqlParameter parameterARDescr = new SqlParameter("@ARDescr", SqlDbType.VarChar, 100);
		//    parameterARDescr.Value = arDescr;
		//    myCommand.Parameters.Add(parameterARDescr);

		//    SqlParameter parameterARString = new SqlParameter("@ARString", SqlDbType.VarChar, 25);
		//    parameterARString.Value = arString;
		//    myCommand.Parameters.Add(parameterARString);

		//    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
		//    parameterLastUser.Value = lastUser;
		//    myCommand.Parameters.Add(parameterLastUser);

		//    SqlParameter parameterARIntNo = new SqlParameter("@ARIntNo", SqlDbType.Int, 4);
		//    parameterARIntNo.Value = arIntNo;
		//    parameterARIntNo.Direction = ParameterDirection.InputOutput;
		//    myCommand.Parameters.Add(parameterARIntNo);

		//    try
		//    {
		//        myConnection.Open();
		//        myCommand.ExecuteNonQuery();
		//        myConnection.Dispose();

		//        int ARIntNo = (int)myCommand.Parameters["@ARIntNo"].Value;

		//        return ARIntNo;
		//    }
		//    catch (Exception e)
		//    {
		//        myConnection.Dispose();
		//        string msg = e.Message;
		//        return 0;
		//    }
		//}

		//public String DeleteSpotFineBatch(int arIntNo)
		//{

		//    // Create Instance of Connection and Command Object
		//    SqlConnection myConnection = new SqlConnection(mConstr);
		//    SqlCommand myCommand = new SqlCommand("SpotFineBatchDelete", myConnection);

		//    // Mark the Command as a SPROC
		//    myCommand.CommandType = CommandType.StoredProcedure;

		//    // Add Parameters to SPROC
		//    SqlParameter parameterARIntNo = new SqlParameter("@ARIntNo", SqlDbType.Int, 4);
		//    parameterARIntNo.Value = arIntNo;
		//    parameterARIntNo.Direction = ParameterDirection.InputOutput;
		//    myCommand.Parameters.Add(parameterARIntNo);

		//    try
		//    {
		//        myConnection.Open();
		//        myCommand.ExecuteNonQuery();
		//        myConnection.Dispose();

		//        // Calculate the CustomerID using Output Param from SPROC
		//        int userId = (int)parameterARIntNo.Value;

		//        return userId.ToString();
		//    }
		//    catch (Exception e)
		//    {
		//        myConnection.Dispose();
		//        string msg = e.Message;
		//        return String.Empty;
		//    }
		//}

		/// <summary>
		/// Marks the supplied batch as completed.
		/// </summary>
		/// <param name="sfbIntNo">The SpotFine Batch int no.</param>
		/// <param name="lastUser">The last user.</param>
		public void MarkBatchAsCompleted(int sfbIntNo, string lastUser)
		{
			SqlConnection con = new SqlConnection(this.connectionString);
			SqlCommand cmd = new SqlCommand("SpotFineBatchMarkCompete", con);
			cmd.CommandType = CommandType.StoredProcedure;

			cmd.Parameters.Add("@SFBIntNo", SqlDbType.Int, 4).Value = sfbIntNo;
			cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            // dls 080115 - the batch name will be calculated from the SpotFine batch register date, not the current date
			//cmd.Parameters.Add("@BatchName", SqlDbType.Char, 18).Value = DateTime.Now.ToString("yyyyMMddHHmmssffff");

			try
			{
				con.Open();
				cmd.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.Assert(false, ex.Message);
			}
			finally
			{
				con.Close();
			}
		}

        /// <summary>
        /// Check to see if there are any open batches for the logged in user
        /// </summary>
        /// <param name="userName">username</param>
        public int CheckOpenBatches(string userName)
        {   
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand cmd = new SqlCommand("SpotFineBatch_OpenUserCheck", con);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterUserName = new SqlParameter("@UserName", SqlDbType.VarChar, 50);
            parameterUserName.Value = userName;
            cmd.Parameters.Add(parameterUserName);

            SqlParameter parameterSFBIntNo = new SqlParameter("@SFBIntNo", SqlDbType.Int, 4);
            parameterSFBIntNo.Value = 0;
            parameterSFBIntNo.Direction = ParameterDirection.InputOutput;
            cmd.Parameters.Add(parameterSFBIntNo);
        
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();

                int SFBIntNo = (int)parameterSFBIntNo.Value;
                return SFBIntNo;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Assert(false, ex.Message);
                return -1;
            }
            finally
            {
                con.Close();
            }
        }

		/// <summary>
		/// Marks the supplied batch as having been printed.
		/// </summary>
		/// <param name="printName">Name of the print.</param>
		/// <param name="lastUser">The last user.</param>
		public void MarkBatchAsPrinted(string printName, string lastUser)
		{
			SqlConnection con = new SqlConnection(this.connectionString);
			SqlCommand cmd = new SqlCommand("SpotFineBatchMarkPrinted", con);
			cmd.CommandType = CommandType.StoredProcedure;

			cmd.Parameters.Add("@PrintName", SqlDbType.VarChar, 50).Value = printName;
			cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

			try
			{
				con.Open();
				cmd.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.Assert(false, ex.Message);
			}
			finally
			{
				con.Close();
			}
		}

		/// <summary>
		/// Gets the print batches for a SpotFine Batch.
		/// </summary>
		/// <param name="sfbIntNo">The SFB int no.</param>
		/// <returns>A <see cref="DataSet"/></returns>
        //2013-04-02 modify by Henry for pagination
		public DataSet GetPrintBatches(int sfbIntNo, int pageSize, int pageIndex, out int totalCount)
		{
			SqlConnection con = new SqlConnection(this.connectionString);
			SqlCommand cmd = new SqlCommand("SpotFineBatchGetPrintFiles", con);
			cmd.CommandType = CommandType.StoredProcedure;

			cmd.Parameters.Add("@SFBIntNo", SqlDbType.Int, 4).Value = sfbIntNo;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            cmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(paraTotalCount);

			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(cmd);
			da.Fill(ds);
			da.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

			return ds;
		}

        public void DeletePrintFile(string printFileName, string lastUser)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand cmd = new SqlCommand("SpotFineBatch_DeletePrintFile", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@PrintFileName", SqlDbType.VarChar, 50).Value = printFileName;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Assert(false, ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

	}
}
