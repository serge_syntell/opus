using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS
{
    /// <summary>
    /// Encapsulates all the business logic for interacting with Summonses
    /// </summary>
    public partial class SummonsDB
    {
        // Fields
        private SqlConnection con;

        /// <summary>
        /// Initializes a new instance of the <see cref="SummonsDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public SummonsDB(string connectionString)
        {
            this.con = new SqlConnection(connectionString);
        }

        //BD 20080521 need to handle a list fo summons locations for generation
        public DataSet GetSummonsLocationGenerated()
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("SummonsLocation");
            DataRow LocRow = dt.NewRow();
            DataRow LocRow1 = dt.NewRow();
            DataRow LocRow2 = dt.NewRow();

            LocRow[0] = "Admin";
            dt.Rows.Add(LocRow);

            //LocRow1[0] = "Road Block";
            //dt.Rows.Add(LocRow1);

            //dls 080602 - they can't choose this one so don't show it!
            //LocRow2[0] = "TPExInt";
            //dt.Rows.Add(LocRow2);

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);

            return ds;
        }

        /// <summary>
        /// Gets notice information for a single summons search.
        /// </summary>
        /// <param name="regNo">The vehicle registration no.</param>
        /// <param name="idNumber">The offender's id number.</param>
        /// <param name="ticketNumber">The ticket number.</param>
        /// <returns>A <see cref="DataSet"/>.</returns>
        //public DataSet GetSingleSummonsNotices(string regNo, string idNumber, string ticketNumber, bool isRoadBlock)
        public DataSet GetSingleSummonsNotices(string regNo, string idNumber, string ticketNumber, int autIntNo)
        {
            //dls 2010-07-28 - Roadblock follows a different path....

            string sp = "SummonsSingleSearchForNotice";

            ////BD 3910 use a different stored proc if we are handling a roadblock search. 
            //string sp;
            //if (isRoadBlock)
            //{
            //    sp = "SummonsRoadBlockSearchForNotice";
            //}
            //else
            //{
            //    sp = "SummonsSingleSearchForNotice";
            //}

            SqlCommand com = new SqlCommand(sp, this.con);
            com.CommandType = CommandType.StoredProcedure;

            if (!string.IsNullOrEmpty(regNo))
                com.Parameters.Add("@RegNo", SqlDbType.VarChar, 50).Value = regNo;
            if (!string.IsNullOrEmpty(idNumber))
                com.Parameters.Add("@IDNumber", SqlDbType.VarChar, 50).Value = idNumber;
            if (!string.IsNullOrEmpty(ticketNumber))
                com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = ticketNumber;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        public DataSet GetBookOutList(int autIntNo, int pageSize, int pageIndex, out int totalCount)
        {
            SqlCommand com = new SqlCommand("SummonsBookOutList", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@PageSize", SqlDbType.Int, 4).Value = pageSize;
            com.Parameters.Add("@PageIndex", SqlDbType.Int, 4).Value = pageIndex;

            SqlParameter pageParameter = new SqlParameter("@TotalCount", SqlDbType.Int);
            pageParameter.Direction = ParameterDirection.Output;
            com.Parameters.Add(pageParameter);

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            totalCount = (int)(pageParameter.Value == DBNull.Value ? 0 : pageParameter.Value);

            return ds;
        }

        //LF: (11/06/2008) - Added this 'ProcCaller' to retrieve all summons eligible to
        //      be Booked OUT/IN to/from the Clerk Of The Court (criteria:  ChargeStatus-620 and 635)
        /// <summary>
        /// Calls the stored proc that returns a recordset from the Summons table that
        /// has a ChargeStatus of 620 / 630 (printed/sent to Clerk of the Court) 
        /// - ready to be sent to the ClerkOftheCourt or Received from the Clerk of the Court.
        /// </summary>
        /// <returns>DataSource ie DataSet to be bound</returns>
        public DataSet GetSummonsToBeSentToCofC()
        {
            SqlCommand com = new SqlCommand("GetAllSummonsesToBeSentToClerkOfCourt", this.con);
            com.CommandType = CommandType.StoredProcedure;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        /// <summary>
        /// jerry 2011-11-09 add courtRule parameter
        /// </summary>
        /// <param name="autIntNo"></param>
        /// <param name="crtIntNo"></param>
        /// <param name="courtDate"></param>
        /// <returns></returns>
        public DataSet GetSummonsListForManualCaseNoEntry(int autIntNo, int crtIntNo, DateTime courtDate, string courtRule, int crtRIntNo,
            int pageSize1, int pageIndex1, out int totalCount1, int pageSize2, int pageIndex2, out int totalCount2)
        {
            SqlCommand com = new SqlCommand("SummonsForManualCaseNoEntry", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@CrtIntNo", SqlDbType.Int, 4).Value = crtIntNo;
            com.Parameters.Add("@CrtRIntNo", SqlDbType.Int, 4).Value = crtRIntNo;
            com.Parameters.Add("@SumCourtDate", SqlDbType.SmallDateTime).Value = courtDate;
            //jerry 2011-11-09 add
            com.Parameters.Add("@CourtRule", SqlDbType.Char, 1).Value = courtRule;

            SqlParameter parameterPageSize1 = new SqlParameter("@PageSize1", SqlDbType.Int);
            parameterPageSize1.Value = pageSize1;
            com.Parameters.Add(parameterPageSize1);

            SqlParameter parameterPageIndex1 = new SqlParameter("@PageIndex1", SqlDbType.Int);
            parameterPageIndex1.Value = pageIndex1;
            com.Parameters.Add(parameterPageIndex1);

            SqlParameter parameterTotalCount1 = new SqlParameter("@TotalCount1", SqlDbType.Int);
            parameterTotalCount1.Direction = ParameterDirection.Output;
            com.Parameters.Add(parameterTotalCount1);

            SqlParameter parameterPageSize2 = new SqlParameter("@PageSize2", SqlDbType.Int);
            parameterPageSize2.Value = pageSize2;
            com.Parameters.Add(parameterPageSize2);

            SqlParameter parameterPageIndex2 = new SqlParameter("@PageIndex2", SqlDbType.Int);
            parameterPageIndex2.Value = pageIndex2;
            com.Parameters.Add(parameterPageIndex2);

            SqlParameter parameterTotalCount2 = new SqlParameter("@TotalCount2", SqlDbType.Int);
            parameterTotalCount2.Direction = ParameterDirection.Output;
            com.Parameters.Add(parameterTotalCount2);

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            totalCount1 = (int)(parameterTotalCount1.Value == DBNull.Value ? 0 : parameterTotalCount1.Value);
            totalCount2 = (int)(parameterTotalCount2.Value == DBNull.Value ? 0 : parameterTotalCount2.Value);

            return ds;
        }

        //mrs 2008/01/17
        public int UpdateCaseNoFromManualEntry(int autIntNo, int crtIntNo, int crtRIntNo, string caseNo, int lastSeqNo, string lastUser, int sumIntNo, ref string errMessage,
            string caseNumberGenerateRule)
        {
            // Create Instance of Connection and Command Object
            SqlCommand com = new SqlCommand("SummonsUpdateCaseNo", this.con);
            com.CommandType = CommandType.StoredProcedure;
            // Add Parameters to SPROC
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@CrtIntNo", SqlDbType.Int, 4).Value = crtIntNo;
            com.Parameters.Add("@CrtRIntNo", SqlDbType.Int, 4).Value = crtRIntNo;
            com.Parameters.Add("@CaseNo", SqlDbType.VarChar, 50).Value = caseNo;
            com.Parameters.Add("@LastSeq", SqlDbType.Int, 4).Value = lastSeqNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;
            com.Parameters["@SumIntNo"].Direction = ParameterDirection.InputOutput;

            //dls 090623 - added checking of auth rule about which amount to use on the summons after the case no is generated
            com.Parameters.Add("@CaseNumberGenerateRule", SqlDbType.Char, 1).Value = caseNumberGenerateRule;
            try
            {
                this.con.Open();
                com.ExecuteNonQuery();
                sumIntNo = (int)com.Parameters["@SumIntNo"].Value;
                return sumIntNo;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return -3;
            }
            finally
            {
                this.con.Close();
            }
        }

        //// mrs 20081122 - search on SumIntNo not SummonsNo
        ////public int SummonsWithdrawnForReIssue(string summonsNo, int sumIntNo, ref string errMessage)
        //public int SummonsWithdrawnForReIssue(int sumIntNo, ref string errMessage, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlCommand com = new SqlCommand("SummonsWithdrawForReIssue", this.con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    // Add Parameters to SPROC
        //    //com.Parameters.Add("@SummonsNo", SqlDbType.VarChar, 50).Value = summonsNo;
        //    //mrs 20081122 SP only has one parameter
        //    com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;
        //    com.Parameters["@SumIntNo"].Direction = ParameterDirection.InputOutput;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
        //    try
        //    {
        //        this.con.Open();
        //        com.ExecuteNonQuery();
        //        int updSumIntNo = (int)com.Parameters["@SumIntNo"].Value;
        //        return updSumIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        errMessage = e.Message;
        //        return 0;
        //    }
        //    finally
        //    {
        //        this.con.Close();
        //    }
        //}

        /// <summary>
        /// Captures the summons served.
        /// </summary>
        /// <param name="sumIntNo">The sum int no.</param>
        /// <param name="sssIntNo">The SSS int no.</param>
        /// <param name="dateServed">The date served.</param>
        /// <param name="servedStatus">The served status.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        /// this is called from roadblock after printing summons = served immediately
        public Int32 CaptureSummonsServed(int sumIntNo, DateTime dateServed, int servedStatus, string lastUser, int sumChargeStatus, int toIntNo, ref string errMessage)
        {
            return CaptureSummonsServed(sumIntNo, 0, toIntNo, dateServed, servedStatus, lastUser, sumChargeStatus, ref errMessage);
        }

        public Int32 CaptureSummonsServed(int sumIntNo, int sssIntNo, DateTime dateServed, int servedStatus, string lastUser, int sumChargeStatus, ref string errMessage)
        {
            return CaptureSummonsServed(sumIntNo, sssIntNo, 0, dateServed, servedStatus, lastUser, sumChargeStatus, ref errMessage);
        }

        public Int32 CaptureSummonsServed(int sumIntNo, int sssIntNo, int toIntNo, DateTime dateServed, int servedStatus, string lastUser, int sumChargeStatus, ref string errMessage)
        {
            SqlCommand com = new SqlCommand("SummonsUpdateServedStatus", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;
            com.Parameters.Add("@SS_SIntNo", SqlDbType.Int, 4).Value = sssIntNo;
            com.Parameters.Add("@TOIntNo", SqlDbType.Int, 4).Value = toIntNo;
            com.Parameters.Add("@SumServedDate", SqlDbType.SmallDateTime, 4).Value = dateServed;
            com.Parameters.Add("@SumServedStatus", SqlDbType.Int, 2).Value = servedStatus;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@SumChargeStatus", SqlDbType.Int, 4).Value = sumChargeStatus;

            try
            {
                this.con.Open();
                com.ExecuteNonQuery();
                //return true;
                return Int32.Parse(com.Parameters["@SumIntNo"].Value.ToString()); ;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                //return false;
                return 0;
            }
            finally
            {
                this.con.Close();
            }
        }

        ///// <summary>
        ///// 20090531 tf Update the summons status.
        ///// </summary>
        ///// <param name="sumIntNo">The sum int no.</param>
        ///// <param name="summonsStatus">The summons status.</param>
        ///// <param name="lastUser">The last user.</param>
        ///// <returns></returns>
        //public bool UpdateSummonsStatus(int sumIntNo, string summonsStatus, int summonsServedStatus, string lastUser)
        //{
        //    SqlCommand com = new SqlCommand("SummonsUpdateStatus", this.con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;
        //    com.Parameters.Add("@SumStatus", SqlDbType.VarChar, 50).Value = summonsStatus;
        //    com.Parameters.Add("@SumServedStatus", SqlDbType.Int, 4).Value = summonsServedStatus;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

        //    try
        //    {
        //        this.con.Open();
        //        com.ExecuteNonQuery();
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //    finally
        //    {
        //        this.con.Close();
        //    }
        //}

        ///// <summary>
        ///// 20090531 tf Update the summons Comments.
        ///// </summary>
        ///// <param name="NotIntNo">The notice int no.</param>
        ///// <param name="summonsComments">The summons comments.</param>
        ///// <param name="lastUser">The last user.</param>
        ///// <returns></returns>
        //public bool UpdateSummonsComments(int NotIntNo, string summonsComments, string lastUser)
        //{
        //    SqlCommand com = new SqlCommand("SummonsUpdateComments", this.con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = NotIntNo;
        //    com.Parameters.Add("@SumComments", SqlDbType.Text, 16).Value = summonsComments;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

        //    try
        //    {
        //        this.con.Open();
        //        com.ExecuteNonQuery();
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //    finally
        //    {
        //        this.con.Close();
        //    }
        //}

        ///// <summary>
        ///// 20090601 tf Update the summons Comments.
        ///// </summary>
        ///// <param name="NotTicketNo">The notice int no.</param>
        ///// <param name="summonsComments">The summons comments.</param>
        ///// <param name="lastUser">The last user.</param>
        ///// <returns></returns>
        //public bool UpdateSummonsComments(string NotTicketNo, string summonsComments, string lastUser)
        //{
        //    SqlCommand com = new SqlCommand("SummonsUpdateCommentsWithTicketNo", this.con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@NotIntNo", SqlDbType.VarChar, 50).Value = NotTicketNo;
        //    com.Parameters.Add("@SumComments", SqlDbType.Text, 16).Value = summonsComments;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

        //    try
        //    {
        //        this.con.Open();
        //        com.ExecuteNonQuery();
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //    finally
        //    {
        //        this.con.Close();
        //    }
        //}

        public int BookOutSummons(int autIntNo, string sumPrintFileName, string lastUser, ref string errMessage)
        {
            SqlCommand com = new SqlCommand("SummonsBookOut", this.con) { CommandTimeout = 0 }; // 2013-09-25, Oscar added timeout = 0
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            // 2013-08-19, Oscar modify length of PrintFileName
            com.Parameters.Add("@SumPrintFileName", SqlDbType.VarChar, 100).Value = sumPrintFileName;
            com.Parameters.Add("@NoOfSummons", SqlDbType.Int, 2).Direction = ParameterDirection.InputOutput;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                this.con.Open();
                com.ExecuteNonQuery();

                int noOfSummons = (int)com.Parameters["@NoOfSummons"].Value;
                return noOfSummons;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return -1;
            }
            finally
            {
                this.con.Close();
            }
        }

        //2013-05-20 added by Nancy for geting number of not booked out
        public int GetNotBookedOutCount(string sumPrintFileName)
        {
            SqlCommand com = new SqlCommand("SummonsNotBookedOut", this.con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@SumPrintFileName", SqlDbType.VarChar, 50).Value = sumPrintFileName;
            com.Parameters.Add("@NoOfSummons", SqlDbType.Int, 2).Direction = ParameterDirection.InputOutput;
            com.Parameters.AddWithValue("@OnlyGetCount", true); // 2013-09-26, Oscar added

            try
            {
                this.con.Open();
                com.ExecuteNonQuery();

                int noOfSummons = (int)com.Parameters["@NoOfSummons"].Value;
                return noOfSummons;
            }
            catch (Exception e)
            {
                return -1;
            }
            finally
            {
                this.con.Close();
            }
        }

        //public object GetSummonsToBeSentOrReceivedFromCofC( ) //spelling
        // mrs 20081019 added back parameter autintno
        public DataSet GetSummonsToBeSentOrReceivedFromCofC(int autIntNo, int pageSize, int pageIndex, out int totalCount)
        {
            SqlCommand com = new SqlCommand("GetAllSummonsesToBeSentOrReceivedFromCofCourt", this.con);
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            com.Parameters.Add(parameterAutIntNo);

            com.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            com.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter totalParame = new SqlParameter("@TotalCount", SqlDbType.Int);
            totalParame.Direction = ParameterDirection.Output;
            com.Parameters.Add(totalParame);

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            totalCount = (int)(totalParame.Value == DBNull.Value ? 0 : totalParame.Value);

            return ds;
        }

        //[SummonsAndWarrantDetails]
        public SqlDataReader SummonsAndWarrantDetails(int notIntNo)
        {
            // Returns a recordset
            // Create Instance of Connection and Command Object
            //SqlConnection myConnection = new SqlConnection(mConstr);
            //mrs 20080905 removed acintno from the recordset - no apparent use
            SqlCommand myCommand = new SqlCommand("SummonsAndWarrantDetails", this.con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = notIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            try
            {
                this.con.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception e)
            {
                string error = e.Message;
                return null;
            }
        }

        //[SummonsAndWarrantDetails]
        public SqlDataReader SummonsAndWarrantDetailsByTicketNo(string ticketNo)
        {
            // Returns a recordset
            // Create Instance of Connection and Command Object
            //SqlConnection myConnection = new SqlConnection(mConstr);
            //mrs 20080905 removed acintno from the recordset - no apparent use
            SqlCommand myCommand = new SqlCommand("SummonsAndWarrantDetails", this.con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterTicketNumber = new SqlParameter("@TicketNumber", SqlDbType.NVarChar, 50);
            parameterTicketNumber.Value = ticketNo;
            myCommand.Parameters.Add(parameterTicketNumber);

            try
            {
                this.con.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                return result;
            }
            catch (Exception e)
            {
                string error = e.Message;
                return null;
            }
        }

        /// <summary>
        /// Gets the court prefix
        /// </summary>
        /// <returns></returns>
        public SqlDataReader SearchSummonsNo(string sSummonsNo)
        {
            // Create Instance of Connection and Command Object
            SqlCommand myCommand = new SqlCommand("SummonsNoSearch", this.con);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterA = new SqlParameter("@SummonsNo", SqlDbType.VarChar, 50);
            parameterA.Value = sSummonsNo;
            myCommand.Parameters.Add(parameterA);

            // Execute the command
            this.con.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        /// <summary>
        /// Creates the change of owner summons.
        /// </summary>
        /// <param name="srIntNo">The summons representation int no.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        // 2013-07-19 comment by Henry for useless
        //public int CreateChangeOfOwnerSummons(int srIntNo, string lastUser)
        //{
        //    SqlCommand com = new SqlCommand("SummonsNewOwner", this.con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@SRIntNo", SqlDbType.Int, 4).Value = srIntNo;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
        //    com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
        //    try
        //    {
        //        this.con.Open();

        //        com.ExecuteNonQuery();

        //        int SumIntNo = (int)com.Parameters["@SumIntNo"].Value;
        //        return SumIntNo;
        //    }
        //    catch
        //    {
        //        return -1;
        //    }
        //    finally
        //    {
        //        this.con.Close();
        //    }
        //}

        /// <summary>
        /// Reverses a withdrawn summons.
        /// </summary>
        /// <param name="srIntNo">The summons representation int no.</param>
        /// <param name="lastUser">The last user.</param>
        // 2013-07-19 comment by Henry for useless
        //public int ReverseSummonsWithdrawn(int srIntNo, string lastUser, string reverseReason)
        //{
        //    SqlCommand com = new SqlCommand("SummonsRepresentationReversal", this.con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@SRIntNo", SqlDbType.Int, 4).Value = srIntNo;
        //    com.Parameters["@SRIntNo"].Direction = ParameterDirection.InputOutput;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
        //    com.Parameters.Add("@ReverseReason", SqlDbType.VarChar, 100).Value = reverseReason;

        //    try
        //    {
        //        this.con.Open();
        //        com.ExecuteNonQuery();

        //        int SRIntNo = (int)com.Parameters["@SRIntNo"].Value;
        //        return SRIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        string msg = e.Message;
        //        return -1;
        //    }
        //    finally
        //    {
        //        this.con.Close();
        //    }
        //}

        //public int BookInSummonsFromClerkOfTheCourt(string sumPrintFileName, string login, ref string errMessage)
        //{
        //    SqlCommand com = new SqlCommand("SummonsUpdateAsRecievedFromClerkOfCourt", this.con);
        //    com.CommandType = CommandType.StoredProcedure;

        /// <summary>
        /// Set Locked indicator on summons
        /// </summary>
        /// <param name="judgementPeriod">The judgement period following the court date</param>
        // 2013-07-26 comment out by Henry for useless
        //public int SetSummonsLock(int AutIntNo, int judgementPeriod, ref string errMessage)
        //{
        //    SqlCommand com = new SqlCommand("SummonsSetLock", this.con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = AutIntNo;
        //    com.Parameters.Add("@JudgementPeriod", SqlDbType.Int, 4).Value = judgementPeriod;

        //    try
        //    {
        //        this.con.Open();
        //        com.ExecuteNonQuery();

        //        return 1;
        //    }
        //    catch (Exception e)
        //    {
        //        errMessage = e.Message;
        //        return -1;
        //    }
        //    finally
        //    {
        //        this.con.Close();
        //    }
        //}        

        /// <summary>
        /// Gets a data set of summons with grace expiry dates that have exceeded, including deferred payments
        /// </summary>
        /// <param name="autIntNo">The authority int no. 
        /// If it is less than one then all Authorities are returned.</param>
        /// <returns>A <see cref="dsRepresentationsWithNoResults"/>.</returns>
        public dsSummonsGraceExpiry SummonsWithExpiredGracePeriod(int autIntNo)
        {
            SqlCommand com = new SqlCommand("SummonsGraceExpiry", this.con);
            com.CommandType = CommandType.StoredProcedure;

            if (autIntNo > 0)
                com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            dsSummonsGraceExpiry ds = new dsSummonsGraceExpiry();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        /// <summary>
        /// jerry 2011-12-19 add for SummonsGracePeriodCheck windows service
        /// Gets a data set of summons with grace expiry dates that have exceeded, including deferred payments
        /// </summary>
        /// <param name="autIntNo"></param>
        /// <returns></returns>
        public dsSummonsGraceExpiry_WS SummonsWithExpiredGracePeriod_WS(int autIntNo)
        {
            SqlCommand com = new SqlCommand("SummonsGraceExpiry_WS", this.con);
            com.CommandType = CommandType.StoredProcedure;

            if (autIntNo > 0)
                com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            dsSummonsGraceExpiry_WS ds = new dsSummonsGraceExpiry_WS();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(com);
                da.Fill(ds);
                da.Dispose();
            }
            catch(Exception ex)
            {
                ds = null;
            }

            return ds;
        }

        //    com.Parameters.Add("@SumPrintFileName", SqlDbType.VarChar, 50).Value = sumPrintFileName;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = login;
        //    com.Parameters.Add("@NoOfSummons", SqlDbType.Int, 2).Direction = ParameterDirection.InputOutput;

        //    try
        //    {
        //        this.con.Open( );
        //        com.ExecuteNonQuery( );

        //        int noOfSummons = (int)com.Parameters["@NoOfSummons"].Value;
        //        return noOfSummons;
        //    }
        //    catch(Exception e)
        //    {
        //        errMessage = e.Message;
        //        return -1;
        //    }
        //    finally
        //    {
        //        this.con.Close( );
        //    }
        //}

        /// <summary>
        /// Books the summons in out to clerk of the court.
        /// </summary>
        /// <param name="sumPrintFileName">Name of the sum print file.</param>
        /// <param name="pCurrentChargeStatus">The p current charge status.</param>
        /// <param name="login">The login.</param>
        /// <param name="errMessage">The err message.</param>
        /// <returns></returns>
        public int BookSummonsInOutToClerkOfTheCourt(string sumPrintFileName, int pCurrentChargeStatus, string login, ref string errMessage)
        {
            SqlCommand com = new SqlCommand("SummonsBookInOutToClerkOfTheCourt", this.con) { CommandTimeout = 0 }; // 2013-09-27, Oscar added timeout = 0
            com.CommandType = CommandType.StoredProcedure;

            // 2013-08-19, Oscar modify length of PrintFileName
            com.Parameters.Add("@SumPrintFileName", SqlDbType.VarChar, 100).Value = sumPrintFileName;
            com.Parameters.Add("@CurrentChargeStatusCode", SqlDbType.Int).Value = pCurrentChargeStatus;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = login;
            com.Parameters.Add("@NoOfSummons", SqlDbType.Int, 2).Direction = ParameterDirection.InputOutput;

            try
            {
                this.con.Open();
                com.ExecuteNonQuery();

                int noOfSummons = (int)com.Parameters["@NoOfSummons"].Value;
                return noOfSummons;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return -1;
            }
            finally
            {
                this.con.Close();
            }
        }



        /// <summary>
        /// Gets a set of results from Search the Summons of Withdrawn Reissue.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="colName">Name of the column.</param>
        /// <param name="colValue">The col value.</param>
        /// <param name="dateSince">The date since.</param>
        /// <returns>A <see cref="DataSet"/>.</returns>
        public DataSet GetSumWithdrawnReissueSearchDS(int autIntNo, string colName, string colValue, DateTime dateSince)
        {
            return this.GetSumWithdrawnReissueSearchDS(autIntNo, colName, colValue, dateSince, 255);
        }

        /// <summary>
        /// Gets the Summons of Withdrawn Reissue search DS.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="colName">Name of the col.</param>
        /// <param name="colValue">The col value.</param>
        /// <param name="dateSince">The date since.</param>
        /// <param name="minStatus">The min status.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetSumWithdrawnReissueSearchDS(int autIntNo, string colName, string colValue, DateTime dateSince, int minStatus)
        {
            // Create SQL data access objects
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = new SqlCommand("SummonsWithdrawnReissueSearch", this.con);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            da.SelectCommand.Parameters.Add("@ColumnName", SqlDbType.VarChar, 10).Value = colName;
            da.SelectCommand.Parameters.Add("@ColumnValue", SqlDbType.VarChar, 30).Value = colValue;
            da.SelectCommand.Parameters.Add("@DateSince", SqlDbType.DateTime, 8).Value = dateSince;
            da.SelectCommand.Parameters.Add("@MinStatus", SqlDbType.Int, 4).Value = minStatus;

            // Execute the command and fill the data set
            dsOfenceSummary ds = new dsOfenceSummary();
            da.Fill(ds);
            da.SelectCommand.Connection.Dispose();

            // Return the data set result
            return ds;
        }

        /// <summary>
        /// Gets the Summons of Withdrawn Reissue search DS.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="colName">Name of the col.</param>
        /// <param name="colValue">The col value.</param>
        /// <param name="dateSince">The date since.</param>
        /// <param name="minStatus">The min status.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetSumWithdrawnReissueSearchDS_GetPaged(int autIntNo, string colName, string colValue, DateTime dateSince, int minStatus, int pageIndex, int pageSize, out int totalCount)
        {
           
            // Create SQL data access objects
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                // Heidi 2014-10-16 added "CommandTimeout=0" for fixing time out issue.(5367)
                da.SelectCommand = new SqlCommand("SummonsWithdrawnReissueSearch_GetPaged", this.con) { CommandTimeout = 0 };
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add Parameters to the SP
                da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
                da.SelectCommand.Parameters.Add("@ColumnName", SqlDbType.VarChar, 10).Value = colName;
                da.SelectCommand.Parameters.Add("@ColumnValue", SqlDbType.VarChar, 30).Value = colValue;
                da.SelectCommand.Parameters.Add("@DateSince", SqlDbType.DateTime, 8).Value = dateSince;
                da.SelectCommand.Parameters.Add("@MinStatus", SqlDbType.Int, 4).Value = minStatus;

                da.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int, 4).Value = pageIndex;
                da.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int, 4).Value = pageSize;
                SqlParameter spTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int, 4);
                spTotalCount.Direction = ParameterDirection.Output;
                da.SelectCommand.Parameters.Add(spTotalCount);

                // Execute the command and fill the data set
                dsOfenceSummary ds = new dsOfenceSummary();
                da.Fill(ds);
                da.SelectCommand.Connection.Dispose();
                totalCount = (int)spTotalCount.Value;
                // Return the data set result
                return ds;
           
            }
            finally
            {
                if (da.SelectCommand.Connection != null)
                {
                    da.SelectCommand.Connection.Dispose();
                }
            }
        }


        /// <summary>
        /// Gets a set of results from searching withdrown reissue summons.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="colName">Name of the column.</param>
        /// <param name="colValue">The col value.</param>
        /// <param name="dateSince">The date since.</param>
        /// <returns>A <see cref="DataSet"/>.</returns>
        public DataSet GetSummonsWithdrawnReissueSearchDS(int autIntNo, string colName, string colValue, DateTime dateSince)
        {
            return this.GetSummonsWithdrawnReissueSearchDS(autIntNo, colName, colValue, dateSince, 255);
        }

        /// <summary>
        /// Gets the withdrown reissue summons search DS.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="colName">Name of the col.</param>
        /// <param name="colValue">The col value.</param>
        /// <param name="dateSince">The date since.</param>
        /// <param name="minStatus">The min status.</param>
        /// <returns>A <see cref="DataSet"/></returns>
        public DataSet GetSummonsWithdrawnReissueSearchDS(int autIntNo, string colName, string colValue, DateTime dateSince, int minStatus)
        {
            // Create SQL data access objects
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = new SqlCommand("SummonsWithdrawnReissueSearch", this.con);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to the SP
            da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            da.SelectCommand.Parameters.Add("@ColumnName", SqlDbType.VarChar, 10).Value = colName;
            da.SelectCommand.Parameters.Add("@ColumnValue", SqlDbType.VarChar, 30).Value = colValue;
            da.SelectCommand.Parameters.Add("@DateSince", SqlDbType.DateTime, 8).Value = dateSince;
            da.SelectCommand.Parameters.Add("@MinStatus", SqlDbType.Int, 4).Value = minStatus;

            // Execute the command and fill the data set
            dsOfenceSummary ds = new dsOfenceSummary();
            da.Fill(ds);
            da.SelectCommand.Connection.Dispose();

            // Return the data set result
            return ds;
        }

        public string GetNotFilmTypeBySummonsNo(string summonsNo)
        {
            SqlCommand com = new SqlCommand("GetNotFilmTypeBySummonsNo", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@SummonsNo", SqlDbType.VarChar, 50).Value = summonsNo;
            com.Parameters.Add("@NotFilmType", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

            // Open the connection and execute the Command
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Dispose();

                string notFilmType = com.Parameters["@NotFilmType"].Value.ToString();
                return notFilmType;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return string.Empty;
            }
            finally
            {
                this.con.Close();
            }
        }

        public string GetAutCodeBySumIntNo(int sumIntNo)
        {
            SqlCommand com = new SqlCommand("GetAutCodeBySumIntNo_WS", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;
            com.Parameters.Add("@AutCode", SqlDbType.Char, 3).Direction = ParameterDirection.Output;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Dispose();
                string autCode = com.Parameters["@AutCode"].Value.ToString().Trim();
                return autCode;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return string.Empty;
            }
            finally
            {
                this.con.Close();
            }
        }

        public DateTime GetCourtDateBySumIntNo(int sumIntNo)
        {
            DateTime dt = new DateTime(2000, 1, 1);

            SqlCommand com = new SqlCommand("GetCourtDateBySumIntNo_WS", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;

            try
            {
                con.Open();
                SqlDataReader result = com.ExecuteReader();
                if (result.HasRows)
                {
                    while (result.Read())
                        dt = Helper.GetReaderValue<DateTime>(result, "SumCourtDate");
                }
                result.Close();
                con.Dispose();
                return dt;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return dt;
            }
            finally
            {
                this.con.Close();
            }
        }

        public int NoCaseNoFromManualEntry(int autIntNo, int crtIntNo, int crtRIntNo, string lastUser, int sumIntNo, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlCommand com = new SqlCommand("SummonsUpdateNoCaseNo", this.con);
            com.CommandType = CommandType.StoredProcedure;
            // Add Parameters to SPROC
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@CrtIntNo", SqlDbType.Int, 4).Value = crtIntNo;
            com.Parameters.Add("@CrtRIntNo", SqlDbType.Int, 4).Value = crtRIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = sumIntNo;
            com.Parameters["@SumIntNo"].Direction = ParameterDirection.InputOutput;
            
            try
            {
                this.con.Open();
                com.ExecuteNonQuery();
                sumIntNo = (int)com.Parameters["@SumIntNo"].Value;
                return sumIntNo;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return -3;
            }
            finally
            {
                this.con.Close();
            }
        }
    }
}
