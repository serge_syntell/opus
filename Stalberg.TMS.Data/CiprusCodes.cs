using System;
using System.Data;
using System.Configuration;
//using System.Web;
//using System.Web.Security;
//using System.Web.UI;
using System.Collections;
using System.Collections.Specialized;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a Ciprus response code
    /// </summary>
    public partial class ResponseProcessCode
    {
        // Fields
        private int processCode;
        private string processDescr;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResponseProcessCode"/> class.
        /// </summary>
        public ResponseProcessCode()
        {
        }

        public ResponseProcessCode AddProcessCode(int code, string descr)
        {
            ResponseProcessCode newProcessCode = new ResponseProcessCode();
            newProcessCode.SetCode(code);
            newProcessCode.SetDescr(descr);
            return newProcessCode;
        }

        public void SetCode(int code)
        {
            processCode = code;
        }

        public void SetDescr(string descr)
        {
            processDescr = descr;
        }

        public int GetCode()
        {
            return processCode;
        }

        public string GetDescr()
        {
            return processDescr;
        }
    }

    public partial class ResponseProcessCodeList
    {
        protected ArrayList codeList = new ArrayList();

        public ResponseProcessCodeList()
        {
            ResponseProcessCode processCode = new ResponseProcessCode();

            processCode = processCode.AddProcessCode(1, "Invalid Type of owner");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(2, "Invalid Business name");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(3, "Possible duplicate case (RegNo, Offence date & time");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(4, "File balances ok");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(5, "File out of balance");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(6, "More than 1 control record on file");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(7, "Trailer record is not the last record on file");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(8, "No trailer record on file");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(9, "Invalid ID number");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(10, "Offence code not registered");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(11, "Invalid Surname");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(12, "Notice number not in sequence");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(13, "Invalid initials");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(14, "Processed ok");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(15, "Invalid postal code in street address");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(16, "Invalid street address");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(17, "Invalid local authority code");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(18, "Invalid court code");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(19, "Duplicate notice number");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(20, "Invalid postal address");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(21, "Invalid record type");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(22, "Invalid interface type");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(23, "Invalid version control");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(24, "Invalid Supplier code");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(25, "Invalid unit id");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(26, "Invalid notice number");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(27, "Invalid officer-number");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(28, "Invalid location code");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(29, "Invalid date of offence");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(30, "Invalid time of offence");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(31, "Invalid registration number");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(32, "Invalid offence code");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(33, "Unable to calculate amount. Offence code / speed?");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(34, "Invalid postal code in postal address");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(35, "Invalid vehicle make");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(36, "Invalid vehicle type");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(37, "Invalid film number");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(38, "Invalid reference number");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(39, "Invalid speed reading");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(40, "File not processed - errors");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(41, "Invalid date captured");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(42, "Invalid date verified");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(43, "Invalid NaTIS date");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(44, "Invalid date adjudicated");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(45, "Invalid adjudicating officer");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(46, "Not used");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(47, "Not used");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(48, "Not used");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(49, "Not used");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(50, "Not used");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(51, "Number of cases invalid");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(52, "Direction invalid");
            AddCodeToList(processCode);
            //dls 071220 - new Ciprus code added
            processCode = processCode.AddProcessCode(53, "Offence date too old (30-day rule)");
            AddCodeToList(processCode);
            //dls 090324 - add Ciprus_Aarto error codes
            processCode = processCode.AddProcessCode(54, "AARTO charge code not mapped to CIPRUS offence code");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(55, "Invalid Forenames");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(56, "Invalid ID type");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(57, "Invalid country of issue of ID document");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(58, "Invalid vehicle GVM");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(59, "Invalid Amber time");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(60, "Invalid Red time");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(61, " Invalid Gender");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(62, " Officer has no infrastructure number in CIPRUS");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(63, " Invalid Business ID number");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(64, " Invalid branch code");
            AddCodeToList(processCode);
            //Jerry 2012-04-19 add 65,66,67,68,69
            processCode = processCode.AddProcessCode(65, "Invalid taxi indicator");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(66, "Accepted, but sent to Non-summons verification bucket");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(67, "Rejected in Non-summons verification - Clone");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(68, "Rejected in Non-summons verification - Expired (30-day rule)");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(69, "Rejected in Non-summons verification - Non-summons registration");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(71, " Processed ok,but details replaced by Tracing");
            AddCodeToList(processCode);
            //Heidi 2013-06-25 added 72 to 81
            processCode = processCode.AddProcessCode(72, "eNATIS = no IssAuthority; Rejection reason = IssAuthority is incorrect or missing");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(73, "eNATIS = no Province; Rejection reason = Province is incorrect or missing");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(74, "eNATIS = no RegAuthority; Rejection reason = RegAuthority is incorrect or missing");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(75, "eNATIS = no VehImage; Rejection reason = Vehicle Image is incorrect or missing");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(76, "eNATIS = no LicImage; Rejection reason = License Image is incorrect or missing");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(77, "eNATIS = no OtherLocInfo; Rejection reason = Other Location Info is incorrect or missing");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(78, "eNATIS = no StreetNameb; Rejection reason = Street Name B is incorrect or missing");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(79, "eNATIS = no Suburb; Rejection reason = Suburb is incorrect or missing");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(80, "eNATIS = no MagisterialCd; Rejection reason = MagisterialCd is incorrect or missing");
            AddCodeToList(processCode);
            processCode = processCode.AddProcessCode(81, "eNATIS = no OffenceCode; Rejection reason = Offence Code is incorrect or missing");
            AddCodeToList(processCode);
            

        }

        public void AddCodeToList(ResponseProcessCode processCode)
        {
            codeList.Add(processCode);
        }

        public string GetDescrByCode(int code)
        {
            string descr = "";
            for (int i = 0; i < codeList.Count; i++)
            {
                ResponseProcessCode processCode = (ResponseProcessCode)codeList[i];

                int listCode = processCode.GetCode();
                if (code == listCode)
                {
                    descr = processCode.GetDescr();
                    break;
                }
            }
            return descr;
        }
    }

    //if you are going to be using the codes more than once, store it in a session variable which can be unpacked whenever its needed
    public partial class demo
    {
        public demo()
        {
            ResponseProcessCodeList list = new ResponseProcessCodeList();

            string descr = list.GetDescrByCode(16);

            //if (Session["ResponseProcessCodeList"] == null)
            //  Session["ResponseProcessCodeList"] = list;
            //
            //ResponseProcessCodeList getlist = (ResponseProcessCodeList)Session["ResponseProcessCodeList"];
            //
            //descr = getlist.GetDescrByCode(16);
        }
    }

}