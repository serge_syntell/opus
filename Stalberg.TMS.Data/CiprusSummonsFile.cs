using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Stalberg.TMS.Data
{
    /// <summary>
    /// Represents a response file from Ciprus to a payment request
    /// </summary>
    public class CiprusSummonsFile
    {
        // Fields
        private bool isValid;
        //private DateTime startDate;
        //private DateTime endDate;
        private List<Record> records;
        private string errorMessage = string.Empty;

        private static Regex regex = new Regex(@"^[\s\d]{5}\.\d{2}$|^\d{7}$", RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);

        /// <summary>
        /// Initializes a new instance of the <see cref="CiprusPaymentResponseFile"/> class.
        /// </summary>
        /// <param name="contents">The contents.</param>
        public CiprusSummonsFile(string contents)
        {
            this.isValid = true;
            this.records = new List<Record>();

            try
            {
                string[] lines = contents.Split(new char[] { '\n' });
                int recordType = 0;
                //int pos = 0;

                string line;
                for (int i = 0; i < lines.Length; i++)
                {
                    line = lines[i].Trim();

                    if (line.Length == 0)
                        continue;

                    //pos = line.IndexOf(',');
                    recordType = int.Parse(line.Substring(0, 2).Trim());
                    //recordType = int.Parse(line.Substring(0, pos).Trim());

                    //if (recordType == 81)
                    //{
                    //    this.ReadHeader(line);
                    //    continue;
                    //}

                    if (recordType == 99)
                    {
                        this.ReadFooter(line);
                        continue;
                    }

                    this.ReadRecord(line);
                }
            }
            catch (Exception ex)
            {
                this.isValid = false;
                this.errorMessage = ex.Message;
            }
        }

        private void ReadRecord(string line)
        {
            //string[] split = line.Split(new char[] { ',' });

            //cofDetails.COfAfrChgSheet2 = srLine.Substring(441, 70);

            string srLine = line;

            Record record = new Record();
            record.Type = int.Parse(srLine.Substring(0, 2)); //int.Parse(split[0].Trim());
            record.LocalAuthCode = int.Parse(srLine.Substring(2, 3)); //int.Parse(split[1].Trim());
            record.BranchCode = int.Parse(srLine.Substring(5, 3)); //int.Parse(split[2].Trim());
            record.BatchNo = int.Parse(srLine.Substring(8, 4));
            record.SummonsType = int.Parse(srLine.Substring(12, 1)); //int.Parse(split[3].Trim());
            record.DocumentSequence = int.Parse(srLine.Substring(13, 5)); //int.Parse(split[4].Trim());
            record.NoticeNo = srLine.Substring(18, 16); //split[5].Trim();
            record.PostalCode = int.Parse(srLine.Substring(34, 4)); //int.Parse(split[7].Trim()); //(CashType)(int)int.Parse(split[6].Trim()).ToString()[0];
            record.MagistratesCourtCode = int.Parse(srLine.Substring(38, 6)); //int.Parse(split[7].Trim());
            record.CourtDate = this.ParseDate(srLine.Substring(44, 8)); //this.ParseDate(split[8].Trim()); //this.ParseAmount(split[8].Trim(), true);
            record.SummonsNo = srLine.Substring(52, 14).Trim(); //split[9].Trim();
            record.VersionNo = int.Parse(srLine.Substring(66, 2)); //int.Parse(split[10].Trim());
            record.AccusedTelNo = srLine.Substring(68, 12); //split[11].Trim();
            record.NotUsed = srLine.Substring(80, 3); //split[12].Trim(); //Not sure if we need to account for the unused space...
            record.AccusedName = srLine.Substring(83, 30).Trim(); //split[13].Trim();
            record.AccusedInitials = srLine.Substring(113, 5).Trim(); //split[14].Trim();
            record.CompanyName = srLine.Substring(118, 25).Trim(); //split[15].Trim();
            record.OffenderAddress1 = srLine.Substring(143, 25).Trim(); //split[16].Trim();
            record.OffenderAddress2 = srLine.Substring(168, 25).Trim(); //split[17].Trim();
            record.OffenderAddress3 = srLine.Substring(193, 25).Trim(); //split[18].Trim();
            record.CameraSupplierCode = srLine.Substring(218, 1); //split[19].Trim();
            record.AccusedIDNo = srLine.Substring(219, 13).Trim(); //split[20].Trim();
            record.SummonsDate = this.ParseDate(srLine.Substring(232, 8)); //this.ParseDate(split[21].Trim());
            record.VehicleRegNo = srLine.Substring(240, 10).Trim(); //split[22].Trim();
            record.FineAmount = this.ParseAmount(srLine.Substring(250, 8), true); //this.ParseAmount(split[23].Trim(), true);

            this.records.Add(record);
        }

        private void ReadFooter(string line)
        {
            //string[] split = line.Split(new char[] { ',' });

            //int recordType = int.Parse(split[0].Trim());
            //string version = split[1].Trim();
            //BD Have done this before, so should never fail here. Am leaving it in for the moment, to check the reading og the file and that no records get missed.
            int count = int.Parse(line.Substring(2, 8));
            if (this.records.Count != count)
            {
                this.isValid = false;
                throw new ApplicationException(string.Format("{0} records have been read from the Summons file, but there are {1} recorded in the file's control record.", this.records.Count, count));
            }
        }

        //private void ReadHeader(string line)
        //{
        //    string[] split = line.Split(new char[] { ',', });

        //    string recordType = split[0].Trim();
        //    string versionNo = split[1].Trim();
        //    this.startDate = this.ParseDate(split[2].Trim());
        //    this.endDate = this.ParseDate(split[3].Trim());
        //    string supplierCode = split[4].Trim();
        //}

        private decimal ParseAmount(string stringAmount, bool divide)
        {

            decimal value = 0;
            if (stringAmount != "00000000")
            {
                //Match match = regex.Match(stringAmount);
                //if (!match.Success)
                //{
                //    this.isValid = false;
                //    throw new ApplicationException("The amount in the file is not in a valid format: " + stringAmount);
                //}

                //value = decimal.Parse(match.Value.Trim());
                value = decimal.Parse(stringAmount);
                if (divide && value != 0)
                    value /= 100;
            }
            return value;
        }

        private DateTime ParseDate(string dateString)
        {
            int year = int.Parse(dateString.Substring(0, 4));
            int month = int.Parse(dateString.Substring(4, 2));
            int day = int.Parse(dateString.Substring(6, 2));

            return new DateTime(year, month, day);
        }

        /// <summary>
        /// Gets a value indicating whether this instance is valid.
        /// </summary>
        /// <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
        public bool IsValid
        {
            get { return this.isValid; }
        }

        /// <summary>
        /// Sets the is invalid.
        /// </summary>
        public void SetIsInvalid()
        {
            this.isValid = false;
        }

        /// <summary>
        /// Gets any error message generated by the response file.
        /// </summary>
        /// <value>The error message.</value>
        public string ErrorMessage
        {
            get { return this.errorMessage; }
        }

        /// <summary>
        /// Gets the records that were read from this file.
        /// </summary>
        /// <value>The records.</value>
        public List<Record> Records
        {
            get { return this.records; }
        }


        /// <summary>
        /// Represents a record in a summons file
        /// </summary>
        public class Record
        {
            /// <summary>
            /// The Ciprus record type number
            /// </summary>
            internal int Type;
            /// <summary>
            /// The document supplier code: Local authority code and branch code
            /// </summary>
            public int LocalAuthCode;
            public int BranchCode;
            /// <summary>
            /// This batch number
            /// </summary>
            public int BatchNo;
            /// <summary>
            /// The Summons Type
            /// </summary>
            public int SummonsType;
            /// <summary>
            /// The document sequence in the file, starting at 00001
            /// </summary>
            public int DocumentSequence;
            /// <summary>
            /// A second format for the amount that was paid
            /// </summary>
            internal decimal Amount2;
            /// <summary>
            /// The notice number
            /// </summary>
            public string NoticeNo;
            /// <summary>
            /// The postal code for the residential address
            /// </summary>
            public int PostalCode;
            /// <summary>
            /// The magistrates court code
            /// </summary>
            public int MagistratesCourtCode;
            /// <summary>
            /// The court date
            /// </summary>
            public DateTime CourtDate;
            /// <summary>
            /// The Summons number
            /// </summary>
            public string SummonsNo;
            /// <summary>
            /// The version control
            /// </summary>
            internal int VersionNo;
            /// <summary>
            /// The accused telephone number
            /// </summary>
            public string AccusedTelNo;
            /// <summary>
            /// Space in the file that is not used
            /// </summary>
            public string NotUsed;
            /// <summary>
            /// The accused name
            /// </summary>
            public string AccusedName;
            /// <summary>
            /// The accused initials
            /// </summary>
            public string AccusedInitials;
            /// <summary>
            /// The company name
            /// </summary>
            public string CompanyName;
            /// <summary>
            /// The offender address line 1
            /// </summary>
            public string OffenderAddress1;
            /// <summary>
            /// The offender address line 2
            /// </summary>
            public string OffenderAddress2;
            /// <summary>
            /// The offender address line 3
            /// </summary>
            public string OffenderAddress3;
            /// <summary>
            /// The camera supplier code
            /// </summary>
            public string CameraSupplierCode;
            /// <summary>
            /// The accused ID Number - in the spec it says it is a number format. You cant store an ID Number as a number, needs to be string.
            /// </summary>
            public string AccusedIDNo;
            /// <summary>
            /// The summons date
            /// </summary>
            public DateTime SummonsDate;
            /// <summary>
            /// The vehicle registration number
            /// </summary>
            public string VehicleRegNo;
            /// <summary>
            /// The fine amount (RRRRRRCC)
            /// </summary>
            public decimal FineAmount;
            /// <summary>
            /// The full ticket number
            /// </summary>
            public string TicketNo;
            /// <summary>
            /// The database identity of the notice
            /// </summary>
            public int NotIntNo;
            /// <summary>
            /// The database authority identity value
            /// </summary>
            public int AutIntNo;
        }

    }
}
