﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Stalberg.TMS.Data
{
    public class AARTO_ReportType
    {
        // Fields
        private int id;
        private string code;
        private string description;

        /// <summary>
        /// Gets or sets the object's database ID.
        /// </summary>
        /// <value>The ID.</value>
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
    }

    public class AARTO_ReportTypeDB
    {
        // Fields
        private SqlConnection con;

        public AARTO_ReportTypeDB(string connectionString)
        {
            this.con = new SqlConnection(connectionString);
        }

        public List<AARTO_ReportType> ListAARTO_ReportTypes()
        {
            List<AARTO_ReportType> list = new List<AARTO_ReportType>();
            AARTO_ReportType temp;

            SqlCommand cmd = new SqlCommand("AARTO_ReportTypeList", this.con);
            try
            {
                this.con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    temp = new AARTO_ReportType();
                    temp.Code = Convert.ToString(reader["ARTReportCode"]);
                    temp.ID = Convert.ToInt32(reader["ARTIntNo"]);
                    temp.Description = Convert.ToString(reader["ARTReportDescr"]);
                    list.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                list = null;
                throw ex;
            }
            finally
            {
                this.con.Close();
            }

            return list;
        }

        public void UpdateAARTO_ReportType(AARTO_ReportType Rpt, string lastUser)
        {
            SqlCommand cmd = new SqlCommand("AARTO_ReportTypeUpdate", this.con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@RptTypeID", SqlDbType.Int).Value = Rpt.ID;
            cmd.Parameters.Add("@RptCode", SqlDbType.VarChar, 10).Value = Rpt.Code;
            cmd.Parameters.Add("@RptDescr", SqlDbType.VarChar, 100).Value = Rpt.Description;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                this.con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.con.Close();
            }
        }

        public int InsertAARTO_ReportType(AARTO_ReportType Rpt, string lastUser)
        {
            SqlCommand cmd = new SqlCommand("AARTO_ReportTypeAdd", this.con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@RptCode", SqlDbType.VarChar, 10).Value = Rpt.Code;
            cmd.Parameters.Add("@RptDescr", SqlDbType.VarChar, 100).Value = Rpt.Description;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                this.con.Open();
                int id = Convert.ToInt32(cmd.ExecuteScalar());
                Rpt.ID = id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.con.Close();
            }
            return Rpt.ID;
        }

        public void DeleteAARTO_ReportType(int id)
        {
            SqlCommand cmd = new SqlCommand("AARTO_ReportTypeDelete", this.con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@RptTypeID", SqlDbType.Int).Value =id;
            
            try
            {
                this.con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.con.Close();
            }
        }

    }
}
