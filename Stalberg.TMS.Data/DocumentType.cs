﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;

namespace Stalberg.TMS.Data
{
    public class DocumentType
    {
        // Fields
        private int id;
        private string code;
        private string description;

        /// <summary>
        /// Gets or sets the object's database ID.
        /// </summary>
        /// <value>The ID.</value>
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
    }

    // Contains all the methods for interacting with the database concerning Document Type objects
    public class DocumentTypeDB
    {
        // Fields
        private SqlConnection con;

        public DocumentTypeDB(string connectionString)
        {
            this.con = new SqlConnection(connectionString);
        }

        public List<DocumentType> ListDocumentTypes()
        {
            List<DocumentType> list = new List<DocumentType>();
            DocumentType temp;

            SqlCommand cmd = new SqlCommand("ListDocumentType", this.con);
            try
            {
                this.con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    temp = new DocumentType();
                    temp.Code = GetSafeString(reader, "DocCode");
                    temp.ID = GetSafeInt(reader, "DocTypeID");
                    temp.Description = GetSafeString(reader, "DocDescr");
                    list.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                list = null;
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.con.Close();
            }

            return list;
        }

        private static int GetSafeInt(SqlDataReader reader, string columnName)
        {
            int ordinal = reader.GetOrdinal(columnName);

            if (reader.IsDBNull(ordinal))
                return 0;

            return reader.GetInt32(ordinal);
        }

        private static int GetSafeByte(SqlDataReader reader, string columnName)
        {
            int ordinal = reader.GetOrdinal(columnName);

            if (reader.IsDBNull(ordinal))
                return 0;

            return reader.GetByte(ordinal);
        }

        private static string GetSafeString(SqlDataReader reader, string columnName)
        {
            int ordinal = reader.GetOrdinal(columnName);

            if (reader.IsDBNull(ordinal))
                return string.Empty;

            return reader.GetString(ordinal);
        }


        public void UpdateDocumentType(DocumentType doc, string lastUser)
        {
            SqlCommand cmd = new SqlCommand("UpdateDocumentType", this.con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@DocTypeID", SqlDbType.Int).Value = doc.ID;
            cmd.Parameters.Add("@DocCode", SqlDbType.VarChar, 3).Value = doc.Code;
            cmd.Parameters.Add("@DocDescr", SqlDbType.VarChar, 254).Value = doc.Description;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                this.con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.con.Close();
            }
        }

        public void InsertDocumentType(DocumentType doc, string lastUser)
        {
            SqlCommand cmd = new SqlCommand("AddDocumentType", this.con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@DocCode", SqlDbType.VarChar, 3).Value = doc.Code;
            cmd.Parameters.Add("@DocDescr", SqlDbType.VarChar, 254).Value = doc.Description;
            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                this.con.Open();
                //Heidi 2014-03-25 fixed a problem that add document code repeatedly will throw an exception(5141)
                int id = Convert.ToInt32(cmd.ExecuteScalar());
                doc.ID = id;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.con.Close();
            }
        }

        public void DeleteDocumentType(int id)
        {
            SqlCommand cmd = new SqlCommand("DeleteDocumentType", this.con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@DocTypeID", SqlDbType.Int).Value =id;
            
            try
            {
                this.con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                this.con.Close();
            }
        }

    }

}
