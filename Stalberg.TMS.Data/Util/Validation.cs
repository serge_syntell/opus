﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;

namespace Stalberg.TMS.Data.Util
{
    public enum IdTypeEnum
    {
        DrivingLicence = 4,
        ForeignID = 3,
        Passport = 2,
        RSA = 1
    }

    public class Validation
    {
        static AlphabetNumericMappingDB anmDB = null;

        static Validation validationInstance = null;

        public static Validation GetInstance(string connString)
        {
            if (validationInstance == null)
            {
                validationInstance = new Validation();
            }
            if (anmDB == null)
            {
                anmDB = new AlphabetNumericMappingDB(connString);
            }
            return validationInstance;
        }
        /// <summary>
        /// Validate ID Number
        /// </summary>
        /// <param name="input"></param>
        /// <param name="idTypeFieldValue">DrivingLicence ,ForeignID,Passport,RSA</param>
        /// <returns></returns>
        public static bool ValidateIdNumber(string input, string idTypeFieldValue)
        {
            bool hasError = false;
            IdTypeEnum? e = null;
            try
            {
                e = (IdTypeEnum)Enum.Parse(typeof(IdTypeEnum), idTypeFieldValue);
            }
            catch
            {
                if (idTypeFieldValue == null && input.Length == 13)
                {
                    e = IdTypeEnum.RSA;
                }
            }
            switch (e)
            {
                case IdTypeEnum.RSA:
                    if (input.Length == 13)
                    {
                        int d = GetControlDigit(input.Substring(0, 12));
                        if (d == -1)
                        {
                            hasError = true;
                        }
                        else
                        {
                            if (input.Substring(12, 1) != d.ToString())
                            {
                                hasError = true;
                            }
                        }
                    }
                    else
                    {
                        hasError = true;
                    }
                    break;
                default:
                    break;
            }
            return hasError;
        }


        public bool ValidateTicketNumber(string input, string validateType)
        {
            if (String.IsNullOrEmpty(input))
            {
                return true;
            }
            //input = input.Replace("/", "").Replace("-", "").Trim();
            bool hasError = false;

            if (validateType.ToLower() == "cdv")
            { hasError = !Cdv(input); }
            else
            {
                input = input.Replace("-", "");
                hasError = !Verhoeff.Check(input);
            }

            return hasError;
        }

        private bool Cdv(string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;

            int s1 = 0, s2 = 0, s3 = 0, s4 = 0;

            if (value.IndexOf("/") > 0 || value.IndexOf("-") > 0)
            {
                value = value.Replace("/", "~").Replace("-", "~");

                string[] noticeArry = value.Split('~');

                if (noticeArry == null || noticeArry.Length != 4)
                    return false;

                s1 = GetIntValue(noticeArry[0]);
                Int32.TryParse(noticeArry[1], out s2);
                Int32.TryParse(noticeArry[2], out s3);
                Int32.TryParse(noticeArry[3], out s4);
            }
            //else
            //{
            //Regex ex = new Regex(@"\d{15,}");
            //if (!ex.IsMatch(value))
            //    return false;

            //if (value.Length <= 16)
            //{
            //    s1 = int.Parse(value.Substring(0, 2));
            //    s2 = int.Parse(value.Substring(2, 5));
            //    s3 = int.Parse(value.Substring(7, 3));
            //    s4 = int.Parse(value.Substring(10));
            //}
            //else
            //{
            //    s1 = int.Parse(value.Substring(0, 2));
            //    s2 = int.Parse(value.Substring(2, 6));
            //    s3 = int.Parse(value.Substring(8, 3));
            //    s4 = int.Parse(value.Substring(11));
            //}
            //}
            bool b = (s1 + 2 * s2 + s3 == s4);
            return b;
        }

        public bool CheckNoticeSequenceNumber(string value, int sequenceLength, string needPadding)
        {
            bool successed = false;
            if (value.IndexOf("/") > 0 || value.IndexOf("-") > 0)
            {
                value = value.Replace("/", "~").Replace("-", "~");

                string[] noticeArry = value.Split('~');

                if (noticeArry == null || noticeArry.Length != 4)
                    return false;

                successed = (noticeArry[1].Trim().Length == sequenceLength && (needPadding == "N" ? true : noticeArry[3].Trim().Length == 6));
            }

            return successed;
        }

        /// <summary>
        /// Get int number cross reference list
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int GetIntValue(string value)
        {
            int returnValue = 0;
            string pattern = @"^[A-Z]{1}";
            string prefix = value.Substring(0, 1);
            if (Regex.IsMatch(value, pattern))
            {
                int num = Convert.ToInt32(value.Substring(1));


                Dictionary<string, int> dict = anmDB.GetAllAlphabetNumericMapping();

                if (dict.ContainsKey(prefix))
                {
                    returnValue = dict[prefix] + num;
                }
                else
                {
                    return 0;
                    //switch (prefix)
                    //{
                    //    case "A": returnValue = 100 + num; break;
                    //    case "B": returnValue = 110 + num; break;
                    //    case "C": returnValue = 120 + num; break;
                    //    case "D": returnValue = 130 + num; break;
                    //    case "E": returnValue = 140 + num; break;
                    //    case "F": returnValue = 150 + num; break;
                    //    case "G": returnValue = 160 + num; break;
                    //    case "H": returnValue = 170 + num; break;
                    //    case "I": returnValue = 180 + num; break;
                    //    case "J": returnValue = 190 + num; break;
                    //    case "K": returnValue = 200 + num; break;
                    //    case "L": returnValue = 210 + num; break;
                    //    case "M": returnValue = 220 + num; break;
                    //    case "N": returnValue = 230 + num; break;
                    //    default: returnValue = 0; break;
                    //}
                }
            }
            else
            {
                returnValue = Convert.ToInt32(value);
            }
            return returnValue;
        }

        public string GetCharFromNumber(int number,int ntIntNo)
        {
            //'H' is only for mobile(H-170,i-180)
            bool isMobile = IsMobileNoticeType(ntIntNo);
            int temNum = (number / 10) * 10;
            if (!isMobile && temNum == 170 )
                temNum = 180;
            // Adam added on 20140514
            //S :280 is specifically reserved for S56 mobile
            if (!isMobile && temNum == 280)
                temNum = 290;
            string alphabet = string.Empty;
            Dictionary<string, int> dict = anmDB.GetAllAlphabetNumericMapping();
            if (dict.ContainsValue(temNum))
            {
                foreach (string key in dict.Keys)
                {
                    if (dict[key] == temNum)
                    {
                        alphabet = key; break;
                    }
                }
            }
            if (number < 100)
            {
                return number.ToString();
            }

            if (String.IsNullOrEmpty(alphabet)) return string.Empty;

            string returnValue = string.Empty;

            returnValue = alphabet + (number - (number / 10) * 10).ToString();

            return returnValue;
        }

        private bool IsMobileNoticeType(int ntIntNo)
        {
            return anmDB.CheckIsMobileType(ntIntNo);
        }

        private static int GetControlDigit(string parsedIdString)
        {
            int d = -1;
            try
            {
                int a = 0;
                for (int i = 0; i < 6; i++)
                {
                    a += int.Parse(parsedIdString[2 * i].ToString());
                }
                int b = 0;
                for (int i = 0; i < 6; i++)
                {
                    b = b * 10 + int.Parse(parsedIdString[2 * i + 1].ToString());
                }
                b *= 2;
                int c = 0;
                do
                {
                    c += b % 10;
                    b = b / 10;
                } while (b > 0);
                c += a;
                d = 10 - (c % 10);
                if (d == 10)
                    d = 0;
            }
            catch {/*ignore*/}
            return d;
        }
    }



}
