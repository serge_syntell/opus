﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Web;

namespace Stalberg.TMS.Data.Util
{
    public class RefHelper
    {
        RefHelper() { }
        static RefHelper instance;
        public static RefHelper GetSingleton() { return instance ?? (instance = new RefHelper()); }


        MethodInfo resourceMI;
        public string GetResource(string pageName, string key, string defaultValue)
        {
            string value = string.Empty;
            try
            {
                if (HttpContext.Current != null)
                {
                    if (this.resourceMI == null)
                    {
                        var method = Assembly.Load("SIL.AARTO.BLL")
                            .GetType("SIL.AARTO.BLL.Utility.GetResourcesInClassLibraries")
                            .GetMethod("GetResourcesValue", new[] { typeof(string), typeof(string) });
                        if (method != null)
                            this.resourceMI = method;
                    }
                    if (this.resourceMI != null)
                        value = Convert.ToString(this.resourceMI.Invoke(null, new object[] { pageName, key }));
                }
            }
            catch { }
            if ((string.IsNullOrEmpty(value) || value == key) && !string.IsNullOrEmpty(defaultValue))
                value = defaultValue;
            return value;
        }

    }
}
