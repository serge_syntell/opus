﻿using System;
using System.Collections.Generic;

namespace Stalberg.TMS.Data.Util
{
    /// <summary>
    /// Represents a utility class that contains methods to assist with the Java reporting engine
    /// </summary>
    public class ReportingUtil
    {
        // Fields
        private readonly string connectionString;
        private readonly string userName;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportingUtil"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="userName">the current user's user name</param>
        public ReportingUtil(string connectionString, string userName)
        {
            this.connectionString = connectionString;
            this.userName = userName;
        }

        /// <summary>
        /// Gets the report URL.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="reportName">Name of the report.</param>
        /// <returns></returns>
        public string GetReportUrl(Uri uri, string reportName)
        {
            //SysParamDetails param = DefaultSysParam.GetSysParamDetails(SysParamDetails.EXCEL_REPORTING_HOST_NAME);
            //param.SPStringValue = "http://" + uri.Host + ":8080/ExcelReporting";

            //SysParamDB db = new SysParamDB(this.connectionString);
            //db.GetSysParamWithDefaults(param, this.userName);

            SysParamDetails sysParam = new SysParamDetails();
            sysParam.SPColumnName = SysParamDetails.EXCEL_REPORTING_HOST_NAME;
            sysParam.SPStringValue = "http://" + uri.Host + ":8080/ExcelReporting";
            sysParam.LastUser = this.userName;

            DefaultSysParam param = new DefaultSysParam(sysParam, this.connectionString);

            KeyValuePair<int, string> spDetails = param.SetDefaultSysParam();
            string hostName = spDetails.Value;

            hostName = "http://" + uri.Host + hostName.Substring(hostName.LastIndexOf(":"), 20);

            if (hostName.EndsWith("/"))
                hostName += reportName;
            else
                hostName += "/" + reportName;

            
            return hostName;
        }




    }
}
