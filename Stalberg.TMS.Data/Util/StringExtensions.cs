﻿using System;
using System.Text.RegularExpressions;

namespace Stalberg.TMS.Data
{
    /// <summary>
    /// Contains additional extension methods for strings
    /// </summary>
    public static class StringExtensions
    {
        // Static
        private static readonly Regex regexHasNoValue = new Regex(@"(?:[\w\.\*])", RegexOptions.Compiled);

        /// <summary>
        /// Determines whether the string effectively has no value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        /// 	<c>true</c> if the string has no value; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasNoValue(this string value)
        {
            // Check if the string is null
            if (value == null)
                return true;

            // Check if the string has zero length
            if (value.Length == 0)
                return true;

            // Check if there is any content in the string
            if (!regexHasNoValue.IsMatch(value))
                return true;

            return false;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="me">The currently extended string.</param>
        /// <param name="other">The other string to compare it to.</param>
        /// <param name="trim">if set to <c>true</c> then both values are trimmed.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">
        /// The <paramref name="other"/> parameter is null.
        /// </exception>
        public static bool Equals(this string me, string other, bool trim)
        {
            if (me == null)
            {
                if (other == null)
                    return true;

                return false;
            }

            if (other == null)
                return false;

            if (trim)
                return me.Trim().Equals(other.Trim(), StringComparison.CurrentCultureIgnoreCase);

            return me.Equals(other, StringComparison.CurrentCultureIgnoreCase);
        }

    }
}
