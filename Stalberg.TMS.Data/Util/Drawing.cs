using System;
using System.Data;
using System.Configuration;
using System.Drawing;
using System.IO;

/// <summary>
/// Summary description for Drawing
/// </summary>
namespace Stalberg.TMS.Data.Util
{
    public class Drawing
    {
        private string style;
        private int X;
        private int Y;
        private Graphics g;

        public Drawing()
        {
        }

        private void DrawLines ()
        {
            Pen pen = new Pen(Brushes.Yellow);
            pen.Width = 5.0F;

            Point ptXStart = new Point(X - 100, Y);
            Point ptXEnd = new Point(X + 100, Y);
            Point ptYStart = new Point(X, Y - 100);
            Point ptYEnd = new Point(X, Y + 100);

            Point ptXStart1 = new Point(X - 100, Y);
            Point ptXEnd1 = new Point(X - 25, Y);
            Point ptXStart2 = new Point(X + 25, Y);
            Point ptXEnd2 = new Point(X + 100, Y);
            Point ptYStart1 = new Point(X, Y - 100);
            Point ptYEnd1 = new Point(X, Y - 25);
            Point ptYStart2 = new Point(X, Y + 25);
            Point ptYEnd2 = new Point(X, Y + 100);

            switch (style)
            {
                case "0":
                    g.DrawLine(pen, ptXStart, ptXEnd);
                    g.DrawLine(pen, ptYStart, ptYEnd);
                    break;
                case "1":
                    g.DrawLine(pen, ptXStart1, ptXEnd1);
                    g.DrawLine(pen, ptYStart1, ptYEnd1);
                    g.DrawLine(pen, ptXStart2, ptXEnd2);
                    g.DrawLine(pen, ptYStart2, ptYEnd2);
                    break;
                case "2":
                    g.DrawLine(pen, ptXStart, new Point(X - 2, Y));
                    g.DrawLine(pen, new Point(X + 2, Y), ptXEnd);
                    g.DrawLine(pen, ptYStart, new Point(X, Y - 2));
                    g.DrawLine(pen, new Point(X, Y + 2), ptYEnd);
                    break;
                case "3":
                    g.DrawLine(pen, ptXStart1, ptXEnd1);
                    g.DrawLine(pen, ptYStart1, ptYEnd1);
                    g.DrawLine(pen, ptXStart2, ptXEnd2);
                    g.DrawLine(pen, ptYStart2, ptYEnd2);
                    break;
                default:
                    break;
            }
            g.Dispose();
        }

        //called from web
        public void CrossHairs(Bitmap bitmap, string styleSetting, int XValue, int YValue)
        {
            this.X = XValue;
            this.Y = YValue;
            this.style = styleSetting;

            //dls 070310 - the Y position needs to be inverted. Atalasoft handles all image res, unlike LT
            Y = bitmap.Height - Y;

            g = Graphics.FromImage(bitmap);

            DrawLines();          
        }

        //called from TPExInt
        public Image CrossHairs(byte[] imageData, string styleSetting, int XValue, int YValue)
        {
            MemoryStream ms = new MemoryStream(imageData);
            Image image = Image.FromStream(ms);

            this.X = XValue;
            this.Y = YValue;
            this.style = styleSetting;

            //dls 070310 - the Y position needs to be inverted. Atalasoft handles all image res, unlike LT
            Y = image.Height - Y;

            g = Graphics.FromImage(image);

            DrawLines();

            return image;
        }

        //public void CrossHairs(ref ImageViewer viewer, int style, int X, int Y)
        //{
        //    //dls 070310 - the Y position needs to be inverted. Atalasoft handles all image res, unlike LT
        //    Y = viewer.Image.Height - Y;
            
        //    SolidFill fillYellow = new SolidFill(Color.Yellow);
        //    Canvas myCanvas = new Canvas(viewer.Image);

        //    Point ptXStart = new Point(X - 100, Y);
        //    Point ptXEnd = new Point(X + 100, Y);
        //    Point ptYStart = new Point(X, Y - 100);
        //    Point ptYEnd = new Point(X, Y + 100);

        //    Point ptXStart1 = new Point(X - 100, Y);
        //    Point ptXEnd1 = new Point(X - 25, Y);
        //    Point ptXStart2 = new Point(X + 25, Y);
        //    Point ptXEnd2 = new Point(X + 100, Y);
        //    Point ptYStart1 = new Point(X, Y - 100);
        //    Point ptYEnd1 = new Point(X, Y - 25);
        //    Point ptYStart2 = new Point(X, Y + 25);
        //    Point ptYEnd2 = new Point(X, Y + 100);

        //    switch (style)
        //    {
        //        case 0:
        //            myCanvas.DrawLine(ptXStart, ptXEnd, new AtalaPen(fillYellow, 5));
        //            myCanvas.DrawLine(ptYStart, ptYEnd, new AtalaPen(fillYellow, 5));
        //            viewer.Update();

        //            break;

        //        case 1:
        //            myCanvas.DrawLine(ptXStart1, ptXEnd1, new AtalaPen(fillYellow, 5));
        //            myCanvas.DrawLine(ptYStart1, ptYEnd1, new AtalaPen(fillYellow, 5));
        //            myCanvas.DrawLine(ptXStart2, ptXEnd2, new AtalaPen(fillYellow, 5));
        //            myCanvas.DrawLine(ptYStart2, ptYEnd2, new AtalaPen(fillYellow, 5));

        //            viewer.Update();
        //            break;

        //        case 2:
        //            DrawInvertedHorizontalLine(ref viewer, ptXStart.X, ptXStart.Y, ptXEnd.X, 5);
        //            DrawInvertedVerticalLine(ref viewer, ptYStart.X, ptYStart.Y, ptYEnd.Y, 5);
        //            break;

        //        case 3:
        //            DrawInvertedHorizontalLine(ref viewer, ptXStart1.X, ptXStart1.Y, ptXEnd1.X, 5);
        //            DrawInvertedHorizontalLine(ref viewer, ptXStart2.X, ptXStart2.Y, ptXEnd2.X, 5);
        //            DrawInvertedVerticalLine(ref viewer, ptYStart1.X, ptYStart1.Y, ptYEnd1.Y, 5);
        //            DrawInvertedVerticalLine(ref viewer, ptYStart2.X, ptYStart2.Y, ptYEnd2.Y, 5);
        //            break;
        //    }
        //}

        //public void DrawInvertedHorizontalLine(ref ImageViewer viewer, int x, int y, int x2, int penwidth)
        //{
        //    AtalaImage img = viewer.Image;
        //    AtalaImage bar = new AtalaImage(x2 - x, penwidth, img.PixelFormat, Color.White);
        //    OverlayMergedCommand cmd = new OverlayMergedCommand(bar, new Point(x, y), MergeOption.LogicalXOR);
        //    ImageResults res = cmd.Apply(img);
        //    bar.Dispose();
        //}

        //public void DrawInvertedVerticalLine(ref ImageViewer viewer, int x, int y, int y2, int penwidth)
        //{
        //    AtalaImage img = viewer.Image;
        //    AtalaImage bar = new AtalaImage(penwidth, y2 - y, img.PixelFormat, Color.White);
        //    OverlayMergedCommand cmd = new OverlayMergedCommand(bar, new Point(x, y), MergeOption.LogicalXOR);
        //    ImageResults res = cmd.Apply(img);
        //    bar.Dispose();
        //}

    }
}
