 using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Stalberg.TMS.Data
{
    public class ArchiveDB
    {
        private string sConn = "";

        public ArchiveDB(string sConn)
        {
            this.sConn = sConn;
        }

        /// <summary>
        /// The default rule to check what to archive will always be Vehicle type exclusions and any duplicate (clones) on the system
        /// </summary>
        /// <param name="nAutIntNo">The Authority number.</param>
        /// <param name="Dictionary<ArchiveInfo> objects">A <see cref="ArchiveInfo"/> a collection containing ArchiveInfo objects</param>
        public Dictionary<int , ArchiveInfo> GetDefaultRuleData(int nAutIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(sConn);
            SqlCommand cmd = new SqlCommand("ArchiveListByDefaultRule", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter prm = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            prm.Value = nAutIntNo;
            cmd.Parameters.Add(prm);

            con.Open();
            SqlDataReader result = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            Dictionary<int, ArchiveInfo> ai = new Dictionary<int , ArchiveInfo>();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                ai.Add(Convert.ToInt32(result["FrameIntNo"]),new ArchiveInfo(Convert.ToInt32(result["FrameIntNo"]),Convert.ToInt32(result["FilmIntNo"]),"","",DateTime .Now,"",""));
            }
            con.Dispose();
            return ai;
        }

        /// <summary>
        /// The default rule to check what to archive will always be Vehicle type exclusions and any duplicate (clones) on the system
        /// </summary>
        /// <param name="nAutIntNo">The Authority number.</param>
        /// <param name="Dictionary<ArchiveInfo> objects">A <see cref="ArchiveInfo"/> a collection containing ArchiveInfo objects</param>
        public Dictionary<int , ArchiveInfo> GetAllOldData(int nAutIntNo , DateTime dt )
        {
            DateTime dtHold;
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(sConn);
            SqlCommand cmd = new SqlCommand("ArchiveListAllOldOffences", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter prm = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            prm.Value = nAutIntNo;
            cmd.Parameters.Add(prm);

            SqlParameter prmA = new SqlParameter("@Date", SqlDbType.SmallDateTime );
            prmA.Value = dt;
            cmd.Parameters.Add(prmA);

            con.Open();
            SqlDataReader result = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            Dictionary<int, ArchiveInfo> ai = new Dictionary<int , ArchiveInfo>();

            while (result.Read())
            {
                DateTime.TryParse (result["NotOffenceDate"].ToString (), out dtHold);
                // Populate Struct using Output Params from SPROC
                ai.Add(Convert.ToInt32(result["FrameIntNo"]), new ArchiveInfo(Convert.ToInt32(result["FrameIntNo"]), Convert.ToInt32(result["FilmIntNo"]), result["NotFilmNo"].ToString(), result["NotFrameNo"].ToString(), dtHold
                    //, result["ChargeStatus"].ToString()
                    // Oscar changed for 4416
                    , result["NoticeStatus"].ToString()
                    , result["FrameStatus"].ToString()));
            }
            con.Dispose();
            return ai;
        }

        /// The default rule to check what to archive will always be Vehicle type exclusions and any duplicate (clones) on the system
        /// </summary>
        /// <param name="nAutIntNo">The Authority number.</param>
        /// <param name="Dictionary<ArchiveInfo> objects">A <see cref="ArchiveInfo"/> a collection containing ArchiveInfo objects</param>
        public Dictionary<int, ArchiveInfo> GetAllFullyPaidData(int nAutIntNo, DateTime dt)
        {
            DateTime dtHold;
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(sConn);
            SqlCommand cmd = new SqlCommand("ArchiveListAllFullyPaidOffences", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter prm = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            prm.Value = nAutIntNo;
            cmd.Parameters.Add(prm);

            SqlParameter prmA = new SqlParameter("@Date", SqlDbType.SmallDateTime);
            prmA.Value = dt;
            cmd.Parameters.Add(prmA);

            con.Open();
            SqlDataReader result = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            Dictionary<int, ArchiveInfo> ai = new Dictionary<int, ArchiveInfo>();

            while (result.Read())
            {
                DateTime.TryParse(result["NotOffenceDate"].ToString(), out dtHold);
                // Populate Struct using Output Params from SPROC
                ai.Add(Convert.ToInt32(result["FrameIntNo"]), new ArchiveInfo(Convert.ToInt32(result["FrameIntNo"]), Convert.ToInt32(result["FilmIntNo"]), result["NotFilmNo"].ToString(), result["NotFrameNo"].ToString(), dtHold
                    //, result["ChargeStatus"].ToString()
                    // Oscar changed for 4416
                    , result["NoticeStatus"].ToString()
                    , result["FrameStatus"].ToString()));
            }
            con.Dispose();
            return ai;
        }

        /// The default rule to check what to archive will always be Vehicle type exclusions and any duplicate (clones) on the system
        /// </summary>
        /// <param name="nAutIntNo">The Authority number.</param>
        /// <param name="Dictionary<ArchiveInfo> objects">A <see cref="ArchiveInfo"/> a collection containing ArchiveInfo objects</param>
        public Dictionary<int, ArchiveInfo> GetAllExpiredOtherReasonData(int nAutIntNo, DateTime dt)
        {
            DateTime dtHold;
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(sConn);
            SqlCommand cmd = new SqlCommand("ArchiveListAllExpiredOtherReasonOffences", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter prm = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            prm.Value = nAutIntNo;
            cmd.Parameters.Add(prm);

            SqlParameter prmA = new SqlParameter("@Date", SqlDbType.SmallDateTime);
            prmA.Value = dt;
            cmd.Parameters.Add(prmA);

            con.Open();
            SqlDataReader result = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            Dictionary<int, ArchiveInfo> ai = new Dictionary<int, ArchiveInfo>();

            while (result.Read())
            {
                DateTime.TryParse(result["NotOffenceDate"].ToString(), out dtHold);
                // Populate Struct using Output Params from SPROC
                ai.Add(Convert.ToInt32(result["FrameIntNo"]), new ArchiveInfo(Convert.ToInt32(result["FrameIntNo"]), Convert.ToInt32(result["FilmIntNo"]), result["NotFilmNo"].ToString(), result["NotFrameNo"].ToString(), dtHold
                    //, result["ChargeStatus"].ToString()
                    // Oscar changed for 4416
                    , result["NoticeStatus"].ToString()
                    , result["FrameStatus"].ToString()));
            }
            con.Dispose();
            return ai;
        }

        /// The default rule to check what to archive will always be Vehicle type exclusions and any duplicate (clones) on the system
        /// </summary>
        /// <param name="nAutIntNo">The Authority number.</param>
        /// <param name="Dictionary<ArchiveInfo> objects">A <see cref="ArchiveInfo"/> a collection containing ArchiveInfo objects</param>
        public Dictionary<int, ArchiveInfo> GetAllRejectedData(int nAutIntNo, DateTime dt)
        {
            DateTime dtHold;
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(sConn);
            SqlCommand cmd = new SqlCommand("ArchiveListAllRejectedOffences", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter prm = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            prm.Value = nAutIntNo;
            cmd.Parameters.Add(prm);

            SqlParameter prmA = new SqlParameter("@Date", SqlDbType.SmallDateTime);
            prmA.Value = dt;
            cmd.Parameters.Add(prmA);

            con.Open();
            SqlDataReader result = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            Dictionary<int, ArchiveInfo> ai = new Dictionary<int, ArchiveInfo>();

            while (result.Read())
            {
                DateTime.TryParse(result["NotOffenceDate"].ToString(), out dtHold);
                // Populate Struct using Output Params from SPROC
                ai.Add(Convert.ToInt32(result["FrameIntNo"]), new ArchiveInfo(Convert.ToInt32(result["FrameIntNo"]), Convert.ToInt32(result["FilmIntNo"]), result["NotFilmNo"].ToString(), result["NotFrameNo"].ToString(), dtHold
                    //, result["ChargeStatus"].ToString()
                    // Oscar changed for 4416
                    , result["NoticeStatus"].ToString()
                    , result["FrameStatus"].ToString()));
            }
            con.Dispose();
            return ai;
        }

        /// The default rule to check what to archive will always be Vehicle type exclusions and any duplicate (clones) on the system
        /// </summary>
        /// <param name="nFrameIntNo">The frame number.</param>
        public int ArchiveImage(int nFrameIntNo)
        {
            int firstScImIntNo = 0;

             // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(sConn);
            SqlCommand cmd = new SqlCommand("ArchiveImage", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter prm = new SqlParameter("@FrameIntNo", SqlDbType.Int, 4);
            prm.Direction = ParameterDirection.InputOutput;
            prm.Value = nFrameIntNo;
            cmd.Parameters.Add(prm);

            SqlParameter prmScImIntNo = new SqlParameter("@FirstScImIntNo", SqlDbType.Int);
            prmScImIntNo.Direction = ParameterDirection.InputOutput;
            //prmScImIntNo.Value = firstScImIntNo;
            cmd.Parameters.Add(prmScImIntNo);            

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Dispose();
                int nRes = (int)prm.Value;
                firstScImIntNo = (int)prmScImIntNo.Value;
 
                // Delete Frame's folder in remote file server
                if (nRes == nFrameIntNo)
                {
                    ScanImageDB imgDB = new ScanImageDB(sConn);
                    ScanImageDetails imgFirst = imgDB.GetImageFullPath(firstScImIntNo);
                    if (imgFirst.FileServer != null)
                    {
                        string strFrameFolder = Path.GetDirectoryName(imgFirst.ImageFullPath);
                        Directory.Delete(strFrameFolder, true);
                 }
                }

                return nRes;
            }
            catch (Exception e)
            {
                con.Dispose();
                string msg = e.Message;
                return -1;
            }


        }

        /// The default rule to check what to archive will always be Vehicle type exclusions and any duplicate (clones) on the system
        /// </summary>
        /// <param name="nFrameIntNo">The frame number.</param>
        //public int ReIndex()
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection con = new SqlConnection(sConn);
        //    SqlCommand cmd = new SqlCommand("ScanImageReIndex", con);

        //    // Mark the Command as a SPROC
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
            
        //    try
        //    {
        //        con.Open();
        //        cmd.ExecuteNonQuery();
        //        cmd.Dispose();
        //        con.Dispose();
        //        return 1;
        //    }
        //    catch (Exception e)
        //    {
        //        con.Dispose();
        //        string msg = e.Message;
        //        return -1;
        //    }
        //}
    }
}
