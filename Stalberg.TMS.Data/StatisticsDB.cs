using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    /// <summary>
    /// Contains the data access logic for creating statistics 
    /// </summary>
    public class StatisticsDB
    {
        // Fields
        private readonly string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public StatisticsDB(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Gets the film status statistics.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="startDate">The start date string (empty for all).</param>
        /// <param name="endDate">The end date string (empty for all).</param>
        /// <returns>A <see cref="SqlDataReader"/></returns>
        public SqlDataReader GetFilmStatusStatistics(int autIntNo, string startDate, string endDate)
        {
            DateTime startDt;
            if (!DateTime.TryParse(startDate, out startDt))
                startDt = new DateTime(2000, 1, 1);
            DateTime endDt;
            if (!DateTime.TryParse(endDate, out endDt))
                endDt = new DateTime(2050, 1, 1);

            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("FilmStatusStatisticsByDate", con);
            com.CommandType = CommandType.StoredProcedure;
            com.CommandTimeout = 0;//Jerry 2014-10-31 add

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@StartDate", SqlDbType.SmallDateTime, 4).Value = startDt;
            com.Parameters.Add("@EndDate", SqlDbType.SmallDateTime, 4).Value = endDt;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

    }
}