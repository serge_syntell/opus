using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Text;


namespace Stalberg.TMS
{
    #region Receipting Data Classes

    /// <summary>
    /// Contains a Field/Pair structure
    /// </summary>
    public struct FieldPair
    {
        public string fieldChgIntNo, fieldChgFineAmount;

        public FieldPair(string p1, string p2)
        {
            fieldChgIntNo = p1;
            fieldChgFineAmount = p2;
        }
    }

    /// <summary>
    /// Represents the data from an electronic payment
    /// </summary>
    public class ElectronicReceipt : ReceiptBase
    {
        public int NotIntNo = 0;
        public string Description = string.Empty;
    }

    /// <summary>
    /// Represents the base class for all receipts
    /// </summary>
    public abstract class ReceiptBase
    {
        public DateTime ReceivedDate;
        public string ReceivedFrom = string.Empty;
        public decimal Amount;
        public decimal ContemptAmount;
        public decimal FineAmount;
        public string Address1 = string.Empty;
        public string Address2 = string.Empty;
        public string Address3 = string.Empty;
        public string Address4 = string.Empty;
        public string Address5 = string.Empty;
        public string AddressAreaCode = string.Empty;
        public string ReceiptNo = string.Empty;
        public CashType CashType;
        public Cashier Cashier = null;
        public bool IsOneReceiptPerNotice = false;
    }
    //BD 20080624 new class to store the results from the the payment with multiple payment types
    public class ReceiptList
    {

        public int rctIntNo;
        public int woaIntNo;

        public ReceiptList(int rctIntNo, int woaIntNo)
        {
            this.rctIntNo = rctIntNo;
            this.woaIntNo = woaIntNo;
        }
    }

    /// <summary>
    /// Represents the transaction items details for a payment receipt
    /// </summary>
    public class ReceiptTransaction : ReceiptBase
    {
        public int AutIntNo;
        public int AccIntNo;
        public int ChgIntNo;
        public string Details;
        public string TicketNo;
        public int BAIntNo;
        public int CBIntNo;
        public string ContactNumber;
        public List<ReceiptCharge> Charges = new List<ReceiptCharge>();
        public bool AddressRequired = false;
        public int MaxStatus;
    }

    /// <summary>
    /// Represents an individual charge with the vote number and receipt number included
    /// </summary>
    public class ReceiptCharge
    {
        private int chgIntNo;
        private int accIntNo;
        private string receiptNo;

        public ReceiptCharge(int chgIntNo, int accIntNo, string receiptNo)
        {
            this.receiptNo = receiptNo;
            this.chgIntNo = chgIntNo;
            this.accIntNo = accIntNo;
        }

        /// <summary>
        /// Gets the receipt number for the charge.
        /// </summary>
        /// <value>The receipt number.</value>
        public string ReceiptNumber
        {
            get { return this.receiptNo; }
        }

        /// <summary>
        /// Gets the vote account int no.
        /// </summary>
        /// <value>The acc int no.</value>
        public int AccIntNo
        {
            get { return this.accIntNo; }
        }

        /// <summary>
        /// Gets the charge int no.
        /// </summary>
        /// <value>The CHG int no.</value>
        public int ChgIntNo
        {
            get { return this.chgIntNo; }
        }

        /// <summary>
        /// Returns the fully qualified type name of this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> containing a fully qualified type name.
        /// </returns>
        public override string ToString()
        {
            return string.Format("{0}:{1}:{2}", this.chgIntNo, this.accIntNo, receiptNo);
        }

    }

    /// <summary>
    /// Represents a cash or cheque receipt
    /// </summary>
    public class ChequeTransaction : ReceiptTransaction
    {
        public string ChequeBank;
        public string ChequeBranch;
        public DateTime ChequeDate;
        private string drawer = string.Empty;
        public string ChequeDrawer
        {
            get { return this.drawer; }
            set
            {
                this.drawer = value;
                if (base.ReceivedFrom.Length == 0)
                    base.ReceivedFrom = value;
            }
        }
        public int ChequeNo;
    }

    /// <summary>
    /// Represents a credit or debit card receipt
    /// </summary>
    public class CardReceipt : ReceiptTransaction
    {
        public string CardIssuer;
        private string name = string.Empty;
        public string NameOnCard
        {
            get { return this.name; }
            set
            {
                this.name = value;
                base.ReceivedFrom = value;
            }
        }
        public DateTime ExpiryDate;


    }

    /// <summary>
    /// Represents postal order receipt
    /// </summary>
    public class PostalOrderReceipt : ReceiptTransaction
    {
        public string PostalOrderNumber;
    }

    public class BankPaymentReceipt : ReceiptTransaction
    {
        public string Reference = string.Empty;
    }

    public class AdminReceipt : ReceiptTransaction
    {
        public string Reference = string.Empty;
    }

    #endregion

    /// <summary>
    /// Contains the database access logic for cash receipts
    /// </summary>
    public class CashReceiptDB
    {
        string mConstr = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:CashReceiptDB"/> class.
        /// </summary>
        /// <param name="vConstr">The v constr.</param>
        public CashReceiptDB(string vConstr)
        {
            mConstr = vConstr;
        }

        public SqlDataReader GetReceiptDetailForCharge(int chgIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ReceiptDetailForCharge", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterChgIntNo = new SqlParameter("@ChgIntNo", SqlDbType.Int, 4);
            parameterChgIntNo.Value = chgIntNo;
            myCommand.Parameters.Add(parameterChgIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        //public SqlDataReader GetReceiptDetailForReceipt(int rctIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("ReceiptDetail", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    SqlParameter parameterRctIntNo = new SqlParameter("@RctIntNo", SqlDbType.Int, 4);
        //    parameterRctIntNo.Value = rctIntNo;
        //    myCommand.Parameters.Add(parameterRctIntNo);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the datareader result
        //    return result;
        //}

        //dls 081212 - not used
        //public SqlDataReader GetCashReceiptList(int autIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("CashReceiptList", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the datareader result
        //    return result;
        //}

        //dls 081212 - not used
        //public DataSet GetCashReceiptListDS(int autIntNo)
        //{
        //    SqlDataAdapter sqlDACashReceipts = new SqlDataAdapter();
        //    DataSet dsCashReceipts = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlDACashReceipts.SelectCommand = new SqlCommand();
        //    sqlDACashReceipts.SelectCommand.Connection = new SqlConnection(mConstr);
        //    sqlDACashReceipts.SelectCommand.CommandText = "CashReceiptList";

        //    // Mark the Command as a SPROC
        //    sqlDACashReceipts.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    sqlDACashReceipts.SelectCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command and close the connection
        //    sqlDACashReceipts.Fill(dsCashReceipts);
        //    sqlDACashReceipts.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsCashReceipts;
        //}

        //dls 081212 - not used
        //public int UpdateCashReceipt(int camIntNo, int autIntNo, string camID,
        //    string camSerialNo, string lastUser)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("CashReceiptUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterCamSerialNo = new SqlParameter("@CamSerialNo", SqlDbType.VarChar, 18);
        //    parameterCamSerialNo.Value = camSerialNo;
        //    myCommand.Parameters.Add(parameterCamSerialNo);

        //    SqlParameter parameterCamID = new SqlParameter("@CamID", SqlDbType.VarChar, 5);
        //    parameterCamID.Value = camID;
        //    myCommand.Parameters.Add(parameterCamID);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterCamIntNo = new SqlParameter("@CamIntNo", SqlDbType.Int);
        //    parameterCamIntNo.Direction = ParameterDirection.InputOutput;
        //    parameterCamIntNo.Value = camIntNo;
        //    myCommand.Parameters.Add(parameterCamIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int CamIntNo = (int)myCommand.Parameters["@CamIntNo"].Value;
        //        //int menuId = (int)parameterCamIntNo.Value;

        //        return CamIntNo;
        //    }
        //    catch
        //    {
        //        myConnection.Dispose();
        //        return 0;
        //    }
        //}

        public String DeleteCashReceipt(int rctIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CashReceiptDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterRctIntNo = new SqlParameter("@RctIntNo", SqlDbType.Int, 4);
            parameterRctIntNo.Value = rctIntNo;
            parameterRctIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterRctIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int RctIntNo = (int)parameterRctIntNo.Value;

                return RctIntNo.ToString();
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return String.Empty;
            }
        }

        // 2013-07-19 comment by Henry for useless
        //<summary>
        //Processes manual capturing of cash from an external system into TMS.
        //</summary>
        //<param name="notIntNo">The notice int no.</param>
        //<param name="amount">The amount.</param>
        //<param name="captureDate">The capture date.</param>
        //<param name="receiptNo">The receipt number.</param>
        //<param name="lastUser">The last user.</param>
        //public int ManualCashCapture(int notIntNo, decimal amount, DateTime captureDate, string receiptNo, string lastUser)
        //{
        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("CashReceiptManual", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
        //    com.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = amount;
        //    com.Parameters.Add("@Date", SqlDbType.DateTime, 8).Value = captureDate;
        //    com.Parameters.Add("@ReceiptNo", SqlDbType.VarChar, 10).Value = receiptNo;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

        //    try
        //    {
        //        con.Open();
        //        return Convert.ToInt32(com.ExecuteScalar());
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        /// <summary>
        /// Processes manual capturing of cash from an external system into TMS.
        /// </summary>
        /// <param name="notIntNo">The notice int no.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="captureDate">The capture date.</param>
        /// <param name="receiptNo">The receipt number.</param>
        /// <param name="sPromunFlag">Promun flag.</param>
        /// <param name="lastUser">The last user.</param>
        //public int PromunCashCapture(int notIntNo, decimal amount, DateTime receiptDate, string receiptNo, string sTicketNo, string sPromunFlag, string sCashType, string lastUser)
        //{
        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("CashReceiptPromun", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
        //    com.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = amount;
        //    com.Parameters.Add("@Date", SqlDbType.DateTime, 8).Value = receiptDate;
        //    com.Parameters.Add("@ReceiptNo", SqlDbType.VarChar, 10).Value = receiptNo;
        //    com.Parameters.Add("@PrintFlag", SqlDbType.VarChar, 1).Value = 'N';
        //    if (sPromunFlag == "Y")
        //        com.Parameters.Add("@PromunFlag", SqlDbType.VarChar, 1).Value = sPromunFlag;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
        //    com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = sTicketNo;
        //    com.Parameters.Add("@CashType", SqlDbType.VarChar, 1).Value = sCashType;

        //    try
        //    {
        //        con.Open();
        //        return Convert.ToInt32(com.ExecuteScalar());
        //    }
        //    catch
        //    {
        //        return -1;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        /// <summary>
        /// Processes a cheque receipt.
        /// </summary>
        /// <param name="chequeTransaction">The cheque transaction.</param>
        /// <returns>The Receipt int no.</returns>
        //public List<int> ProcessChequeReceipt(ChequeTransaction receipt)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    foreach (ReceiptCharge charge in receipt.Charges)
        //    {
        //        sb.Append(charge.ToString());
        //        sb.Append(", ");
        //    }

        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("ProcessChequeReceipt", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    // com.Parameters.Add("@ReceiptNo", SqlDbType.VarChar, 10).Value = receipt.ReceiptNo; - Receipt is now in the ReceiptCharge object
        //    com.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = receipt.Amount;
        //    com.Parameters.Add("@Details", SqlDbType.Text).Value = receipt.Details;
        //    com.Parameters.Add("@ReceivedFrom", SqlDbType.VarChar, 100).Value = receipt.ReceivedFrom;
        //    com.Parameters.Add("@ContactNumber", SqlDbType.VarChar, 20).Value = receipt.ContactNumber;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = receipt.Cashier.UserName;
        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = receipt.AutIntNo;
        //    com.Parameters.Add("@BAIntNo", SqlDbType.Int, 4).Value = receipt.BAIntNo;
        //    com.Parameters.Add("@CBIntNo", SqlDbType.Int, 4).Value = receipt.CBIntNo;
        //    com.Parameters.Add("@CashType", SqlDbType.Char, 1).Value = Helper.GetCashTypeChar(receipt.CashType);
        //    com.Parameters.Add("@ChargeAccount", SqlDbType.VarChar, 100).Value = sb.ToString();
        //    com.Parameters.Add("@ChequeDrawer", SqlDbType.VarChar, 50).Value = receipt.ChequeDrawer;
        //    com.Parameters.Add("@ChequeNo", SqlDbType.Int, 4).Value = receipt.ChequeNo;
        //    com.Parameters.Add("@ChequeBank", SqlDbType.VarChar, 30).Value = receipt.ChequeBank;
        //    com.Parameters.Add("@ChequeBranch", SqlDbType.VarChar, 30).Value = receipt.ChequeBranch;
        //    com.Parameters.Add("@ChequeDate", SqlDbType.DateTime, 8).Value = receipt.ChequeDate;
        //    if (receipt.AddressRequired)
        //    {
        //        com.Parameters.Add("@Address1", SqlDbType.VarChar, 100).Value = receipt.Address1;
        //        com.Parameters.Add("@Address2", SqlDbType.VarChar, 100).Value = receipt.Address2;
        //        com.Parameters.Add("@Address3", SqlDbType.VarChar, 100).Value = receipt.Address3;
        //        com.Parameters.Add("@Address4", SqlDbType.VarChar, 100).Value = receipt.Address4;
        //        com.Parameters.Add("@Address5", SqlDbType.VarChar, 100).Value = receipt.Address5;
        //        com.Parameters.Add("@AddressCode", SqlDbType.VarChar, 100).Value = receipt.AddressAreaCode;
        //    }
        //    com.Parameters.Add("@IsOneReceiptPerNotice", SqlDbType.Bit, 1).Value = receipt.IsOneReceiptPerNotice;
        //    com.Parameters.Add("@UserIntNo", SqlDbType.Int, 4).Value = receipt.Cashier.UserIntNo;

        //    try
        //    {
        //        List<int> receipts = new List<int>();
        //        con.Open();
        //        SqlDataReader reader = com.ExecuteReader();

        //        //dls 070713 - had to add a nested proc to read the AuthRule regarding max charge status to allow for payments,
        //        //              so two result sets are returned and we need the 2nd one!
        //        reader.NextResult();
        //        while (reader.Read())
        //        {
        //            receipts.Add(reader.GetInt32(0));
        //        }
        //        reader.Close();

        //        return receipts;
        //    }
        //    catch (Exception e)
        //    {
        //        string error = e.Message;
        //        return null;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        /// <summary>
        /// Processes a credit or debit card receipt.
        /// </summary>
        /// <param name="receipt">The receipt.</param>
        /// <returns>The Receipt int no.</returns>
        //public List<int> ProcessCardReceipt(CardReceipt receipt)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    foreach (ReceiptCharge charge in receipt.Charges)
        //    {
        //        sb.Append(charge.ToString());
        //        sb.Append(", ");
        //    }

        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("ProcessCardReceipt", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    // com.Parameters.Add("@ReceiptNo", SqlDbType.VarChar, 10).Value = receipt.ReceiptNo;
        //    com.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = receipt.Amount;
        //    com.Parameters.Add("@Details", SqlDbType.Text).Value = receipt.Details;
        //    com.Parameters.Add("@ReceivedFrom", SqlDbType.VarChar, 100).Value = receipt.ReceivedFrom;
        //    com.Parameters.Add("@ContactNumber", SqlDbType.VarChar, 20).Value = receipt.ContactNumber;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = receipt.Cashier.UserName;
        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = receipt.AutIntNo;
        //    com.Parameters.Add("@BAIntNo", SqlDbType.Int, 4).Value = receipt.BAIntNo;
        //    com.Parameters.Add("@CBIntNo", SqlDbType.Int, 4).Value = receipt.CBIntNo;
        //    com.Parameters.Add("@ChargeAccount", SqlDbType.VarChar, 100).Value = sb.ToString();
        //    com.Parameters.Add("@CashType", SqlDbType.Char, 1).Value = Helper.GetCashTypeChar(receipt.CashType);
        //    com.Parameters.Add("@CardType", SqlDbType.VarChar, 20).Value = receipt.CardIssuer;
        //    com.Parameters.Add("@Payee", SqlDbType.VarChar, 50).Value = receipt.NameOnCard;
        //    com.Parameters.Add("@ExpiryDate", SqlDbType.DateTime, 8).Value = receipt.ExpiryDate;
        //    com.Parameters.Add("@ReceivedDate", SqlDbType.DateTime, 8).Value = receipt.ReceivedDate;

        //    if (receipt.AddressRequired)
        //    {
        //        com.Parameters.Add("@Address1", SqlDbType.VarChar, 100).Value = receipt.Address1;
        //        com.Parameters.Add("@Address2", SqlDbType.VarChar, 100).Value = receipt.Address2;
        //        com.Parameters.Add("@Address3", SqlDbType.VarChar, 100).Value = receipt.Address3;
        //        com.Parameters.Add("@Address4", SqlDbType.VarChar, 100).Value = receipt.Address4;
        //        com.Parameters.Add("@Address5", SqlDbType.VarChar, 100).Value = receipt.Address5;
        //        com.Parameters.Add("@AddressCode", SqlDbType.VarChar, 100).Value = receipt.AddressAreaCode;
        //    }

        //    com.Parameters.Add("@IsOneReceiptPerNotice", SqlDbType.Bit, 1).Value = receipt.IsOneReceiptPerNotice;
        //    com.Parameters.Add("@UserIntNo", SqlDbType.Int, 4).Value = receipt.Cashier.UserIntNo;

        //    try
        //    {
        //        List<int> receipts = new List<int>();
        //        con.Open();
        //        SqlDataReader reader = com.ExecuteReader();

        //        //dls 070713 - had to add a nested proc to read the AuthRule regarding max charge status to allow for payments,
        //        //              so two resultsets are returned and we need the 2nd one!
        //        reader.NextResult();

        //        while (reader.Read())
        //        {
        //            receipts.Add(reader.GetInt32(0));
        //        }
        //        reader.Close();

        //        return receipts;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        /// <summary>
        /// Processes a postal order receipt.
        /// </summary>
        /// <param name="receipt">The receipt.</param>
        /// <returns>The Receipt int no.</returns>
        //public List<int> ProcessPostalOrderReceipt(PostalOrderReceipt receipt)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    foreach (ReceiptCharge charge in receipt.Charges)
        //    {
        //        sb.Append(charge.ToString());
        //        sb.Append(", ");
        //    }

        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("ProcessPostalOrderReceipt", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = receipt.Amount;
        //    com.Parameters.Add("@Details", SqlDbType.Text).Value = receipt.Details;
        //    com.Parameters.Add("@ReceivedFrom", SqlDbType.VarChar, 100).Value = receipt.ReceivedFrom;
        //    com.Parameters.Add("@ContactNumber", SqlDbType.VarChar, 20).Value = receipt.ContactNumber;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = receipt.Cashier.UserName;
        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = receipt.AutIntNo;
        //    com.Parameters.Add("@BAIntNo", SqlDbType.Int, 4).Value = receipt.BAIntNo;
        //    com.Parameters.Add("@CBIntNo", SqlDbType.Int, 4).Value = receipt.CBIntNo;
        //    com.Parameters.Add("@ChargeAccount", SqlDbType.VarChar, 100).Value = sb.ToString();
        //    com.Parameters.Add("@CashType", SqlDbType.Char, 1).Value = Helper.GetCashTypeChar(receipt.CashType);
        //    com.Parameters.Add("@PostalOrderNo", SqlDbType.VarChar, 50).Value = receipt.PostalOrderNumber;
        //    com.Parameters.Add("@ReceivedDate", SqlDbType.DateTime, 8).Value = receipt.ReceivedDate;

        //    if (receipt.AddressRequired)
        //    {
        //        com.Parameters.Add("@Address1", SqlDbType.VarChar, 100).Value = receipt.Address1;
        //        com.Parameters.Add("@Address2", SqlDbType.VarChar, 100).Value = receipt.Address2;
        //        com.Parameters.Add("@Address3", SqlDbType.VarChar, 100).Value = receipt.Address3;
        //        com.Parameters.Add("@Address4", SqlDbType.VarChar, 100).Value = receipt.Address4;
        //        com.Parameters.Add("@Address5", SqlDbType.VarChar, 100).Value = receipt.Address5;
        //        com.Parameters.Add("@AddressCode", SqlDbType.VarChar, 100).Value = receipt.AddressAreaCode;
        //    }
        //    com.Parameters.Add("@IsOneReceiptPerNotice", SqlDbType.Bit, 1).Value = receipt.IsOneReceiptPerNotice;
        //    com.Parameters.Add("@UserIntNo", SqlDbType.Int, 4).Value = receipt.Cashier.UserIntNo;

        //    try
        //    {
        //        List<int> receipts = new List<int>();
        //        con.Open();

        //        SqlDataReader reader = com.ExecuteReader();

        //        //dls 070713 - had to add a nested proc to read the AuthRule regarding max charge status to allow for payments,
        //        //              so two resultsets are returned and we need the 2nd one!
        //        reader.NextResult();

        //        while (reader.Read())
        //        {
        //            receipts.Add(reader.GetInt32(0));
        //        }
        //        reader.Close();

        //        return receipts;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        /// <summary>
        /// Gets the details for a possible receipt reversal.
        /// </summary>
        /// <param name="rctIntNo">The receipt int no.</param>
        /// <returns>A <see cref="SqlDataReader"/>.</returns>
        public SqlDataReader GetReceiptDetailsForReversal(int rctIntNo)
        {
            SqlParameter totalCount;
            return GetReceiptDetailsForReversal(rctIntNo, 0, 0, out totalCount);
        }

        public SqlDataReader GetReceiptDetailsForReversal(int rctIntNo, int pageSize, int pageIndex, out SqlParameter totalCount)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("ReceiptDetailsForReversal", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@RctIntNo", SqlDbType.Int, 4).Value = rctIntNo;
            com.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            com.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            com.Parameters.Add(paraTotalCount);

            con.Open();
            SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection);

            //totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);
            totalCount = paraTotalCount;

            return reader;
        }

        /// <summary>
        /// Returns a list of receipts based on an offender's name.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="offenderName">Name of the offender.</param>
        /// <returns>A <see cref="DataSet"/>.</returns>
        //public DataSet ReceiptEnquiryOnOffender(int autIntNo, string offenderName)
        public DataSet ReceiptEnquiryOnOffender(string offenderName)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("ReceiptEnquiryOnOffender", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@Name", SqlDbType.VarChar, 50).Value = offenderName;
            //com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        /// <summary>
        /// Returns a list of receipts from a partial ticket number.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="ticketNo">The ticket no.</param>
        /// <returns>A <see cref="DataSet"/>.</returns>
        //public DataSet ReceiptEnquiryOnTicketNumber(int autIntNo, string ticketNo)
        public DataSet ReceiptEnquiryOnTicketNumber(string ticketNo)    // 2013-03-27 Henry add for pagination
        {
            int totalCount = 0;
            return ReceiptEnquiryOnTicketNumber(ticketNo, 0, 0, out totalCount);
        }

        public DataSet ReceiptEnquiryOnTicketNumber(string ticketNo, int pageSize, int pageIndex, out int totalCount)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("ReceiptEnquiryOnTicketNo", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = ticketNo;
            //com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            com.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            com.Parameters.Add(paraTotalCount);

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            return ds;
        }

        /// <summary>
        /// Processes a receipt reversal for the specified receipt into the the specified cash box.
        /// </summary>
        /// <param name="rctIntNo">The receipt int no.</param>
        /// <param name="cbIntNo">The cash box int no.</param>
        /// <param name="user">The user.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="number">The number.</param>
        /// <param name="person">The person.</param>
        /// <param name="institution">The institution.</param>
        /// <returns>The new reversed receipt int no.</returns>
        //public KeyValuePair<string, int> ProcessReceiptReversal(int rctIntNo, int cbIntNo, string user, decimal amount, int number, string person, string institution, DateTime? date)
        //{
        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("ReceiptReversal", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@RctIntNo", SqlDbType.Int, 4).Value = rctIntNo;
        //    com.Parameters.Add("@CBIntNo", SqlDbType.Int, 4).Value = cbIntNo;
        //    com.Parameters.Add("@User", SqlDbType.VarChar, 50).Value = user;
        //    com.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = amount;
        //    if (date.HasValue)
        //        com.Parameters.Add("@Date", SqlDbType.DateTime, 8).Value = date.Value;
        //    if (number > 0)
        //        com.Parameters.Add("@Number", SqlDbType.Int, 4).Value = number;
        //    if (person.Length > 0)
        //        com.Parameters.Add("@Person", SqlDbType.VarChar, 100).Value = person;
        //    if (institution.Length > 0)
        //        com.Parameters.Add("@Institution", SqlDbType.VarChar, 100).Value = institution;

        //    KeyValuePair<string, int> value = new KeyValuePair<string, int>();

        //    try
        //    {
        //        con.Open();
        //        SqlDataReader reader = com.ExecuteReader();
        //        if (reader.Read())
        //            value = new KeyValuePair<string, int>((string)reader[0], (int)reader[1]);
        //        reader.Close();

        //        return value;
        //    }
        //    catch (Exception e)
        //    {
        //        value = new KeyValuePair<string, int>(e.Message, 0);
        //        return value;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }
        //}

        public List<Int32> ProcessReceiptReversal(string receiptList, int cbIntNo, string user, int autIntNo, DateTime reversalDate, ref string errMessage, string cancellationReason = "")
        {
            List<Int32> receipts = new List<Int32>();
            bool ignore = false;

            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("ReceiptReversal", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@ReceiptListXML", SqlDbType.Xml, receiptList.Length).Value = receiptList;
            com.Parameters.Add("@CBIntNo", SqlDbType.Int, 4).Value = cbIntNo;
            com.Parameters.Add("@User", SqlDbType.VarChar, 50).Value = user;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@ReversalDate", SqlDbType.SmallDateTime).Value = reversalDate;
            com.Parameters.Add("@ReversalReason", SqlDbType.VarChar, 500).Value = cancellationReason;

            try
            {
                con.Open();

                SqlDataReader reader = com.ExecuteReader();
                while (!ignore)
                {
                    while (reader.Read() && !ignore)
                    {
                        Int32 nValue = 0;

                        if (Int32.TryParse(reader[0].ToString(), out nValue))
                        {
                            //ignore this result - it is returned from one of the nested stored procs
                            ignore = true;
                        }
                        else
                        {
                            string value = reader.GetString(0);

                            if (value.Equals("Receipt"))
                            {
                                receipts.Add(reader.GetInt32(2));
                            }
                            else if (value.Equals("Error"))
                            {
                                errMessage = "Error (" + reader.GetInt32(2) + ") - " + reader.GetString(1);
                                receipts.Add(reader.GetInt32(2));
                            }
                        }
                    }

                    // if there is another resultset go through the outer loop again 
                    if (ignore && reader.NextResult())
                        ignore = false;
                    else
                        ignore = true;

                }
                reader.Close();
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                receipts.Add(-1);
            }
            finally
            {
                con.Dispose();
            }

            return receipts;
        }

        public int GetReceiptByNotIntNo(int notIntNo, ref string Reversed)
        {
            int RctIntNo = 0;

            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("GetReceiptByNotIntNo", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                if (reader.Read())
                {
                    RctIntNo = (int)reader[0];
                    Reversed = reader[1].ToString();
                }
                reader.Close();

                return RctIntNo;
            }
            catch (Exception e)
            {
                throw new Exception();
            }
            finally
            {
                con.Close();
            }


        }

        public KeyValuePair<string, int> ProcessAdminReceiptReversal(int rctIntNo, string user, string reason, int autIntNo)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("ReceiptReversalAdmin", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@RctIntNo", SqlDbType.Int, 4).Value = rctIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = user;
            com.Parameters.Add("@Reason", SqlDbType.VarChar, 50).Value = reason;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;

            KeyValuePair<string, int> value = new KeyValuePair<string, int>();

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                if (reader.Read())
                    value = new KeyValuePair<string, int>((string)reader[0], (int)reader[1]);
                reader.Close();

                return value;
            }
            catch (Exception e)
            {
                value = new KeyValuePair<string, int>(e.Message, 0);
                return value;
            }
            finally
            {
                con.Close();
            }
        }

        // 2013-07-19 comment by Henry for useless
        /// <summary>
        /// Processes an electronic payment.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns>
        /// 	<c>true</c> if the payment is successfully processed.
        /// </returns>
        //public int ProcessElectronicPayment(ElectronicReceipt data)
        //{
        //    int nResult = -1;

        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("ProcessElectronicPayment", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = data.NotIntNo;
        //    com.Parameters.Add("@Date", SqlDbType.DateTime, 8).Value = data.ReceivedDate;
        //    com.Parameters.Add("@From", SqlDbType.VarChar, 100).Value = data.ReceivedFrom;
        //    com.Parameters.Add("@Amount", SqlDbType.Money, 8).Value = data.Amount;
        //    com.Parameters.Add("@ReceiptNo", SqlDbType.VarChar, 10).Value = data.ReceiptNo;
        //    com.Parameters.Add("@Address1", SqlDbType.VarChar, 100).Value = data.Address1;
        //    com.Parameters.Add("@Address2", SqlDbType.VarChar, 100).Value = data.Address2;
        //    com.Parameters.Add("@Address3", SqlDbType.VarChar, 100).Value = data.Address3;
        //    com.Parameters.Add("@Address4", SqlDbType.VarChar, 100).Value = data.Address4;
        //    com.Parameters.Add("@Address5", SqlDbType.VarChar, 100).Value = data.Address5;
        //    com.Parameters.Add("@Code", SqlDbType.VarChar, 10).Value = data.AddressAreaCode;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = data.Cashier.UserName;
        //    com.Parameters.Add("@CashType", SqlDbType.Char, 1).Value = Helper.GetCashTypeChar(data.CashType);
        //    if (data.Description.Length > 0)
        //        com.Parameters.Add("@Description", SqlDbType.Text).Value = data.Description;

        //    try
        //    {
        //        //con.Open();
        //        //nResult = Convert.ToInt32(com.ExecuteScalar());
        //        con.Open();
        //        SqlDataReader reader = com.ExecuteReader();
        //        reader.NextResult();
        //        if (reader.Read())
        //            nResult = Convert.ToInt32(reader[0]);
        //        reader.Close();
        //    }
        //    catch { }
        //    finally
        //    {
        //        con.Close();
        //    }

        //    return nResult;
        //}

        public DataSet GetReceiptPaymentListDS(int trhIntNo)
        {
            SqlDataAdapter sqlDAReceipts = new SqlDataAdapter();

            // 2014-10-15, Oscar added try-finally for Dispose.
            try
            {
                DataSet dsReceipts = new DataSet();

                // Create Instance of Connection and Command Object
                sqlDAReceipts.SelectCommand = new SqlCommand();
                sqlDAReceipts.SelectCommand.Connection = new SqlConnection(mConstr);
                sqlDAReceipts.SelectCommand.CommandText = "ReceiptPaymentList";
                // 2014-10-15, Oscar added
                sqlDAReceipts.SelectCommand.CommandTimeout = 0;

                // Mark the Command as a SPROC
                sqlDAReceipts.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDAReceipts.SelectCommand.Parameters.Add("@TRHIntNo", SqlDbType.Int, 4).Value = trhIntNo;

                // Execute the command and close the connection
                sqlDAReceipts.Fill(dsReceipts);
                //sqlDAReceipts.SelectCommand.Connection.Dispose();

                // Return the dataset result
                return dsReceipts;
            }
            finally
            {
                sqlDAReceipts.SelectCommand.Dispose();
                sqlDAReceipts.SelectCommand.Connection.Dispose();
            }
        }

        public int AddReceiptToPaymentList(ReceiptTransaction receipt, DateTime trhTranDate, int trhIntNo, ref string errMessage, bool header, string lastUser, int userIntNo)
        {
            StringBuilder sb = new StringBuilder();
            foreach (ReceiptCharge charge in receipt.Charges)
            {
                sb.Append(charge.ToString());
                sb.Append(", ");
            }

            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("ReceiptAddPayment", con);
            com.CommandType = CommandType.StoredProcedure;
            // 2014-10-15, Oscar added
            com.CommandTimeout = 0;

            //header parameters
            com.Parameters.Add("@TRHTranDate", SqlDbType.SmallDateTime).Value = trhTranDate;
            com.Parameters.Add("@UserIntNo", SqlDbType.Int, 4).Value = receipt.Cashier == null ? userIntNo : receipt.Cashier.UserIntNo;
            com.Parameters.Add("@TRHLastUser", SqlDbType.VarChar, 50).Value = receipt.Cashier == null ? lastUser : receipt.Cashier.UserName;

            com.Parameters.Add("@TRHIntNo", SqlDbType.Int, 4).Value = trhIntNo;
            com.Parameters["@TRHIntNo"].Direction = ParameterDirection.InputOutput;

            if (header)
            {
                com.Parameters.Add("@TRHDetails", SqlDbType.Text).Value = receipt.Details;
                com.Parameters.Add("@TRHReceivedFrom", SqlDbType.VarChar, 100).Value = receipt.ReceivedFrom;
                com.Parameters.Add("@TRHContactNumber", SqlDbType.VarChar, 20).Value = receipt.ContactNumber;

                com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = receipt.AutIntNo;
                com.Parameters.Add("@BAIntNo", SqlDbType.Int, 4).Value = receipt.BAIntNo;
                com.Parameters.Add("@CBIntNo", SqlDbType.Int, 4).Value = receipt.CBIntNo;
                com.Parameters.Add("@TRHAddress1", SqlDbType.VarChar, 100).Value = receipt.Address1;
                com.Parameters.Add("@TRHAddress2", SqlDbType.VarChar, 100).Value = receipt.Address2;
                com.Parameters.Add("@TRHAddress3", SqlDbType.VarChar, 100).Value = receipt.Address3;
                com.Parameters.Add("@TRHAddress4", SqlDbType.VarChar, 100).Value = receipt.Address4;
                com.Parameters.Add("@TRHAddress5", SqlDbType.VarChar, 100).Value = receipt.Address5;
                com.Parameters.Add("@TRHAddressCode", SqlDbType.VarChar, 100).Value = receipt.AddressAreaCode;
                com.Parameters.Add("@TRHIsOneReceiptPerNotice", SqlDbType.Bit, 1).Value = receipt.IsOneReceiptPerNotice;
                com.Parameters.Add("@TRHMaxStatus", SqlDbType.Int, 4).Value = receipt.MaxStatus;
                com.Parameters.Add("@TRDReceivedDate", SqlDbType.DateTime, 8).Value = receipt.ReceivedDate;

            }
            else
            {
                //common detail parameters
                com.Parameters.Add("@TRDAmount", SqlDbType.Decimal).Value = receipt.Amount;
                com.Parameters.Add("@TRDContemptAmount", SqlDbType.Decimal).Value = receipt.ContemptAmount;
                com.Parameters.Add("@TRDFineAmount", SqlDbType.Decimal).Value = receipt.FineAmount;
                com.Parameters.Add("@TRDCashType", SqlDbType.Char, 1).Value = Helper.GetCashTypeChar(receipt.CashType);
                com.Parameters.Add("@TRDIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;
                com.Parameters.Add("@TRDReceivedDate", SqlDbType.DateTime, 8).Value = receipt.ReceivedDate;

                switch (receipt.CashType)
                {
                    //cheque parameters
                    case CashType.Cash:
                    case CashType.Cheque:
                    case CashType.RoadBlockCash:
                    case CashType.RoadBlockCheque:
                        com.Parameters.Add("@TRDChequeDrawer", SqlDbType.VarChar, 50).Value = ((ChequeTransaction)receipt).ChequeDrawer;
                        com.Parameters.Add("@TRDChequeNo", SqlDbType.Int, 4).Value = ((ChequeTransaction)receipt).ChequeNo;
                        com.Parameters.Add("@TRDChequeBank", SqlDbType.VarChar, 30).Value = ((ChequeTransaction)receipt).ChequeBank;
                        com.Parameters.Add("@TRDChequeBranch", SqlDbType.VarChar, 30).Value = ((ChequeTransaction)receipt).ChequeBranch;
                        com.Parameters.Add("@TRDChequeDate", SqlDbType.DateTime, 8).Value = ((ChequeTransaction)receipt).ChequeDate;
                        break;

                    //credit card parameters
                    case CashType.CreditCard:
                    case CashType.DebitCard:
                    case CashType.RoadBlockCard:
                        com.Parameters.Add("@TRDCardType", SqlDbType.VarChar, 20).Value = ((CardReceipt)receipt).CardIssuer;
                        com.Parameters.Add("@TRDPayee", SqlDbType.VarChar, 50).Value = ((CardReceipt)receipt).NameOnCard;
                        com.Parameters.Add("@TRDExpiryDate", SqlDbType.SmallDateTime).Value = ((CardReceipt)receipt).ExpiryDate;
                        break;

                    //postal order parameters
                    case CashType.PostalOrder:
                        com.Parameters.Add("@TRDPostalOrderNo", SqlDbType.VarChar, 50).Value = ((PostalOrderReceipt)receipt).PostalOrderNumber;
                        break;

                    //bank payment parameters
                    case CashType.BankPayment:
                        com.Parameters.Add("@TRDReference", SqlDbType.Text).Value = ((BankPaymentReceipt)receipt).Reference;
                        break;

                    //admin receipt parameters
                    case CashType.AdminReceipt:
                        com.Parameters.Add("@TRDReference", SqlDbType.Text).Value = ((AdminReceipt)receipt).Reference;
                        break;
                }
            }
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                trhIntNo = Int32.Parse(com.Parameters["@TRHIntNo"].Value.ToString());

                return trhIntNo;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return -1;
            }
            finally
            {
                // 2014-10-15, Oscar added
                com.Dispose();
                con.Dispose();
            }
        }

        // 2013-07-19 comment by Henry for useless
        //public int RemovePaymentList(int trhIntNo)
        //{
        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("ReceiptDeletePaymentList", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@TRHIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;
        //    com.Parameters["@TRHIntNo"].Value = trhIntNo;

        //    try
        //    {
        //        con.Open();
        //        com.ExecuteNonQuery();

        //        trhIntNo = Int32.Parse(com.Parameters["@TRHIntNo"].Value.ToString());
        //        return trhIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        string error = e.Message;
        //        return -1;
        //    }
        //    finally
        //    {
        //        con.Dispose();
        //    }
        //}

        public DataTable CreateReceiptNotices(List<TempReceiptNotice> notices)
        {
            var table = new DataTable("ReceiptNoticesType");

            var pis = typeof(TempReceiptNotice).GetProperties();
            foreach (var pi in pis)
            {
                Type[] types;
                var type = (types = pi.PropertyType.GetGenericArguments()).Length > 0 ? types[0] : pi.PropertyType;
                table.Columns.Add(pi.Name, type);
            }

            if (notices != null && notices.Count > 0)
            {
                foreach (var notice in notices)
                {
                    var row = table.NewRow();
                    foreach (var pi in pis)
                    {
                        var value = pi.GetValue(notice, null);
                        if (value != null)
                            row[pi.Name] = value;
                    }
                    table.Rows.Add(row);
                }
            }

            return table;
        }

        public int AddNoticesToReceiptList(int trhIntNo, string noticeList, int isDeferredPayment, ref string error, List<TempReceiptNotice> notices = null)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("ReceiptNoticesForPayment", con);
            com.CommandType = CommandType.StoredProcedure;
            // 2014-10-15, Oscar added
            com.CommandTimeout = 0;

            com.Parameters.Add("@NoticeList", SqlDbType.Xml, noticeList.Length).Value = noticeList;
            com.Parameters.Add("@IsDeferredPayment", SqlDbType.Int, 4).Value = isDeferredPayment;
            com.Parameters.Add("@TRHIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;
            com.Parameters["@TRHIntNo"].Value = trhIntNo;

            // 2014-09-09, Oscar added
            com.Parameters.Add("@ReceiptNotices", SqlDbType.Structured).Value = CreateReceiptNotices(notices);

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                trhIntNo = Int32.Parse(com.Parameters["@TRHIntNo"].Value.ToString());
                return trhIntNo;
            }
            catch (Exception e)
            {
                error = e.Message;
                return -2;
            }
            finally
            {
                con.Dispose();
            }
        }

        public void RemovePaymentListForUser(int userIntNo)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("ReceiptDeletePaymentListForUser", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@UserIntNo", SqlDbType.Int, 4).Value = userIntNo;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            finally
            {
                con.Dispose();
            }
        }

        public int RemovePaymentFromList(int trdIntNo, ref string errMessage)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("ReceiptDeletePayment", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@TRDIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;
            com.Parameters["@TRDIntNo"].Value = trdIntNo;

            try
            {
                con.Open();
                com.ExecuteNonQuery();

                trdIntNo = Int32.Parse(com.Parameters["@TRDIntNo"].Value.ToString());
                return trdIntNo;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return -1;
            }
            finally
            {
                con.Dispose();
            }
        }
        //2014-02-25 Heidi added paymentOriginator parameter for fixed a problem about HRK payments - Payment Originator(5214)
        public List<ReceiptList> CreateReceiptFromMultiplePayments(int trhIntNo, ref string errMessage, int noOfDaysForPayment, string referenceNo = null, bool hrkPayment = false, string paymentOriginator = null)
        {
            //List<Int32> receipts = new List<Int32>();
            List<ReceiptList> receipts = new List<ReceiptList>();
            bool ignore = false;

            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("ProcessReceiptForMultiplePaymentTypes", con);

            com.CommandType = CommandType.StoredProcedure;
            //Adam 20140723 Add CommandTimeout=0 for the issue of timeout as the temporary solution.
            com.CommandTimeout = 0;
            com.Parameters.Add("@TRHIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;
            com.Parameters["@TRHIntNo"].Value = trhIntNo;

            com.Parameters.Add("@NoOfDaysForPayment", SqlDbType.Int, 4).Value = noOfDaysForPayment;
            com.Parameters.Add("@HRKPayment", SqlDbType.BigInt).Value = hrkPayment;

            if (!String.IsNullOrEmpty(referenceNo))
            {
                com.Parameters.Add("@ReferenceNo", SqlDbType.NVarChar, 15).Value = referenceNo;
            }

            //2014-02-25 Heidi added for fixed a problem about HRK payments - Payment Originator(5214)
            if (!String.IsNullOrEmpty(paymentOriginator))
            {
                //Jake 2014-12-19 comment out, i have added the enum value Opus on PaymentOriginator table 
                //and the default value has been set to Opus on cashier payment page for package 5383.
                //=================================================
                //if (paymentOriginator == "OPUS")
                //{
                //    //2014-05-15 Heidi changed for displaying 'OPUS' on the Cash Receipt page paid(5249)
                //    com.Parameters.Add("@PaymentOriginator", SqlDbType.NVarChar, 30).Value = paymentOriginator;
                //}
                //else
                //{
                //    int _paymentOriginator = 0;
                //    int.TryParse(paymentOriginator, out _paymentOriginator);
                //    if (_paymentOriginator > 0)
                //    {
                //        com.Parameters.Add("@PaymentOriginator", SqlDbType.NVarChar, 30).Value = _paymentOriginator;
                //    }
                //}
                //=================================================
                int _paymentOriginator = 0;
                int.TryParse(paymentOriginator, out _paymentOriginator);
                if (_paymentOriginator > 0)
                {
                    com.Parameters.Add("@PaymentOriginator", SqlDbType.NVarChar, 30).Value = _paymentOriginator;
                }
            }

            try
            {
                con.Open();

                SqlDataReader reader = com.ExecuteReader();
                while (!ignore)
                {
                    while (reader.Read() && !ignore)
                    {
                        Int32 nValue = 0;

                        //if (reader[0].ToString() != "")
                        //{
                        if (reader[0] == DBNull.Value || Int32.TryParse(reader[0].ToString(), out nValue))
                        {
                            //ignore this result - it is returned from one of the nested stored procs
                            ignore = true;
                        }
                        else
                        {

                            string value = reader.GetString(0);

                            if (value.Equals("Receipt"))
                            {
                                receipts.Add(new ReceiptList(reader.GetInt32(1), reader.GetInt32(2)));
                            }
                            else if (value.Equals("Error"))
                            {
                                errMessage = "Error (" + reader.GetInt32(1) + ") - " + reader.GetString(2);
                                receipts.Add(new ReceiptList(reader.GetInt32(1), 0));
                            }
                            else if (value.Equals("Representation") || value.Equals("Summons"))
                            {
                                ignore = true;
                            }

                        }
                        //}
                        //else
                        //{
                        //    //ignore this result - it is returned from one of the nested stored procs
                        //    ignore = true;
                        //}

                    }

                    // if there is another resultset go through the outer loop again 
                    if (ignore && reader.NextResult())
                        ignore = false;
                    else
                        ignore = true;

                }
                reader.Close();
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                receipts.Add(new ReceiptList(-1, -1));
            }
            finally
            {
                con.Dispose();
            }

            return receipts;
        }


        public List<int> CreateAdminReceipt(int trhIntNo, ref string errMessage, int adminRctIntNo, int noOfDaysForPayment)
        {
            List<int> receipts = new List<int>();
            bool ignore = false;

            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("ProcessAdminReceipt", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@TRHIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;
            com.Parameters["@TRHIntNo"].Value = trhIntNo;

            //dls 080227 - added possibility of reapplying payment reversed though admin receipt reversal
            com.Parameters.Add("@AdminRctIntNo", SqlDbType.Int, 4).Value = adminRctIntNo;
            com.Parameters.Add("@NoOfDaysForPayment", SqlDbType.Int, 4).Value = noOfDaysForPayment;

            try
            {
                con.Open();

                SqlDataReader reader = com.ExecuteReader();
                while (!ignore)
                {
                    while (reader.Read() && !ignore)
                    {
                        Int32 nValue = 0;

                        if (reader[0] == DBNull.Value || Int32.TryParse(reader[0].ToString(), out nValue))
                        {
                            //ignore this result - it is returned from one of the nested stored procs
                            ignore = true;
                        }
                        else
                        {
                            string value = reader.GetString(0);

                            if (value.Equals("Receipt"))
                            {
                                receipts.Add(reader.GetInt32(1));
                            }
                            else if (value.Equals("Error"))
                            {
                                errMessage = "Error (" + reader.GetInt32(1) + ") - " + reader.GetString(2);
                                receipts.Add(reader.GetInt32(1));
                            }
                            else if (value.Equals("Representation") || value.Equals("Summons"))
                            {
                                ignore = true;
                            }
                        }
                    }

                    // if there is another resultset go through the outer loop again 
                    if (ignore && reader.NextResult())
                        ignore = false;
                    else
                        ignore = true;

                }
                reader.Close();
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                receipts.Add(-1);
            }
            finally
            {
                con.Dispose();
            }

            return receipts;
        }

        public DataSet GetAdminReversalNotAppliedDS(int autIntNo, int pageSize, int pageIndex, out int totalCount)
        {
            SqlDataAdapter sqlDA = new SqlDataAdapter();
            DataSet ds = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDA.SelectCommand = new SqlCommand();
            sqlDA.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDA.SelectCommand.CommandText = "AdminReversalPaymentsNotApplied";

            // Mark the Command as a SPROC
            sqlDA.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDA.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            sqlDA.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            sqlDA.SelectCommand.Parameters.Add("@pageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            sqlDA.SelectCommand.Parameters.Add(paraTotalCount);

            // Execute the command and close the connection
            sqlDA.Fill(ds);
            sqlDA.SelectCommand.Connection.Dispose();

            totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            // Return the dataset result
            return ds;
        }

        public void MinimalCaptureForSection341(int authIntNo,
            string filmNo,
            string frameNo,
            string ticketNo,
            string idNumber,
            string surName,
            string foreName,
            string officerNo,
            string offenceCode1,
            string offenceCode2,
            string offenceCode3,
            string offenceDescr1,
            string offenceDescr2,
            string offenceDescr3,
            float fineAmount1,
            float finAmount2,
            float fineAmount3,
            string conCode,
            string lastUser, out int notIntNo, string nstCode,
            bool forHRK = false, DateTime notPaymentDate = default(DateTime), DateTime? receiptDate = null,
            int? crtIntNo = null, string notRegNo = null, DateTime? offenceDate = null)
        {
            notIntNo = 0;
            SqlConnection conn = new SqlConnection(this.mConstr);

            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HandwrittenOffenceSection341_MinimalCapture";
            // 2014-10-15, Oscar added
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@AutIntNo", SqlDbType.Int)).Value = authIntNo;
            cmd.Parameters.Add(new SqlParameter("@FilmNo", SqlDbType.NVarChar, 25)).Value = filmNo;

            cmd.Parameters.Add(new SqlParameter("@FrameNo", SqlDbType.NVarChar, 4)).Value = frameNo;
            cmd.Parameters.Add(new SqlParameter("@NotTicketNo", SqlDbType.NVarChar, 50)).Value = ticketNo;
            cmd.Parameters.Add(new SqlParameter("@DrvIdNumber", SqlDbType.NVarChar, 20)).Value = idNumber;
            cmd.Parameters.Add(new SqlParameter("@DrvSName", SqlDbType.NVarChar, 50)).Value = surName;
            cmd.Parameters.Add(new SqlParameter("@DrvFName", SqlDbType.NVarChar, 50)).Value = foreName;
            cmd.Parameters.Add(new SqlParameter("@NotOfficerNo", SqlDbType.NVarChar, 20)).Value = officerNo;
            cmd.Parameters.Add(new SqlParameter("@ChgOffenceCode", SqlDbType.NVarChar, 15)).Value = offenceCode1;

            cmd.Parameters.Add(new SqlParameter("@ChgOffenceCode2", SqlDbType.NVarChar, 15)).Value = offenceCode2;
            cmd.Parameters.Add(new SqlParameter("@ChgOffenceCode3", SqlDbType.NVarChar, 15)).Value = offenceCode3;
            cmd.Parameters.Add(new SqlParameter("@ChgOffenceDescr", SqlDbType.NVarChar, 200)).Value = offenceDescr1;
            cmd.Parameters.Add(new SqlParameter("@ChgOffenceDescr2", SqlDbType.NVarChar, 200)).Value = offenceDescr2;
            cmd.Parameters.Add(new SqlParameter("@ChgOffenceDescr3", SqlDbType.NVarChar, 200)).Value = offenceDescr3;
            cmd.Parameters.Add(new SqlParameter("@ChgFineAmount", SqlDbType.Real)).Value = fineAmount1;
            cmd.Parameters.Add(new SqlParameter("@ChgFineAmount2", SqlDbType.Real)).Value = finAmount2;
            cmd.Parameters.Add(new SqlParameter("@ChgFineAmount3", SqlDbType.Real)).Value = fineAmount3;
            cmd.Parameters.Add(new SqlParameter("@ConCode", SqlDbType.NVarChar, 20)).Value = conCode;
            cmd.Parameters.Add(new SqlParameter("@LastUser", SqlDbType.VarChar, 50)).Value = lastUser;
            cmd.Parameters.Add(new SqlParameter("@ForHRKInterface", SqlDbType.Bit)).Value = forHRK;
            cmd.Parameters.Add(new SqlParameter("@NotIntNo", SqlDbType.Int)).Direction = ParameterDirection.InputOutput;
            cmd.Parameters.Add(new SqlParameter("@NSTCode", SqlDbType.NVarChar, 5)).Value = nstCode;
            cmd.Parameters["@NotIntNo"].Value = 0;
            //Jerry 2013-02-07 add
            cmd.Parameters.Add(new SqlParameter("@NotPaymentDate", SqlDbType.DateTime)).Value = notPaymentDate;
            //Edge 3013-07-11
            cmd.Parameters.Add(new SqlParameter("@ReceiptDate", SqlDbType.DateTime)).Value = receiptDate;

            //Jake 2014-12-18 added for 5383
            if (crtIntNo.HasValue && crtIntNo.Value > 0)
                cmd.Parameters.Add(new SqlParameter("@CrtIntNo", SqlDbType.Int)).Value = crtIntNo;
            if (!String.IsNullOrEmpty(notRegNo))
                cmd.Parameters.Add(new SqlParameter("@NotRegNo", SqlDbType.NVarChar, 20)).Value = notRegNo;
            if (offenceDate.HasValue)
                cmd.Parameters.Add(new SqlParameter("@OffenceDate", SqlDbType.DateTime)).Value = offenceDate;
            //,@CrtIntNo int ,
            //@NotRegNo VARCHAR(20),
            //@OffenceDate SMALLDATETIME,
            //@PaymentOriginator INT 
            try
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                cmd.ExecuteNonQuery();

                notIntNo = Convert.ToInt32(cmd.Parameters["@NotIntNo"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //conn.Close();
                // 2014-10-15, Oscar changed
                cmd.Dispose();
                conn.Dispose();
            }
        }

        public void MinimalCaptureForSection56(int authIntNo,
            string filmNo,
            string frameNo,
            string ticketNo,
            string idNumber,
            string surName,
            string foreName,
            string officerNo,
            string offenceCode1,
            string offenceCode2,
            string offenceCode3,
            string offenceDescr1,
            string offenceDescr2,
            string offenceDescr3,
            float fineAmount1,
            float finAmount2,
            float fineAmount3,
            string conCode,
            string lastUser, out int notIntNo, string nstCode,
            bool forHRK = false, DateTime? receiptDate = null,
            int? crtIntNo = null, int? crtRIntNo = null, DateTime? sumCourtDate = null, string notRegNo = null, DateTime? offenceDate = null)
        {
            notIntNo = 0;
            SqlConnection conn = new SqlConnection(this.mConstr);

            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HandwrittenOffenceSection56_MinimalCapture";
            // 2014-10-15, Oscar added
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@AutIntNo", SqlDbType.Int)).Value = authIntNo;
            cmd.Parameters.Add(new SqlParameter("@NotFilmNo", SqlDbType.NVarChar, 25)).Value = filmNo;

            cmd.Parameters.Add(new SqlParameter("@NotFrameNo", SqlDbType.NVarChar, 4)).Value = frameNo;
            cmd.Parameters.Add(new SqlParameter("@NotTicketNo", SqlDbType.NVarChar, 50)).Value = ticketNo;
            cmd.Parameters.Add(new SqlParameter("@AccIdNumber", SqlDbType.NVarChar, 20)).Value = idNumber;
            cmd.Parameters.Add(new SqlParameter("@AccSurname", SqlDbType.NVarChar, 50)).Value = surName;
            cmd.Parameters.Add(new SqlParameter("@AccForenames", SqlDbType.NVarChar, 50)).Value = foreName;
            cmd.Parameters.Add(new SqlParameter("@NotOfficerNo", SqlDbType.NVarChar, 20)).Value = officerNo;
            cmd.Parameters.Add(new SqlParameter("@ChgOffenceCode1", SqlDbType.NVarChar, 15)).Value = offenceCode1;

            cmd.Parameters.Add(new SqlParameter("@ChgOffenceCode2", SqlDbType.NVarChar, 15)).Value = offenceCode2;
            cmd.Parameters.Add(new SqlParameter("@ChgOffenceCode3", SqlDbType.NVarChar, 15)).Value = offenceCode3;
            cmd.Parameters.Add(new SqlParameter("@ChgOffenceDescr1", SqlDbType.NVarChar, 200)).Value = offenceDescr1;
            cmd.Parameters.Add(new SqlParameter("@ChgOffenceDescr2", SqlDbType.NVarChar, 200)).Value = offenceDescr2;
            cmd.Parameters.Add(new SqlParameter("@ChgOffenceDescr3", SqlDbType.NVarChar, 200)).Value = offenceDescr3;
            cmd.Parameters.Add(new SqlParameter("@ChgFineAmount1", SqlDbType.Real)).Value = fineAmount1;
            cmd.Parameters.Add(new SqlParameter("@ChgFineAmount2", SqlDbType.Real)).Value = finAmount2;
            cmd.Parameters.Add(new SqlParameter("@ChgFineAmount3", SqlDbType.Real)).Value = fineAmount3;
            cmd.Parameters.Add(new SqlParameter("@ConCode", SqlDbType.NVarChar, 20)).Value = conCode;
            cmd.Parameters.Add(new SqlParameter("@LastUser", SqlDbType.VarChar, 50)).Value = lastUser;
            cmd.Parameters.Add(new SqlParameter("@ForHRKInterface", SqlDbType.Bit)).Value = forHRK;
            cmd.Parameters.Add(new SqlParameter("@NotIntNo", SqlDbType.Int)).Direction = ParameterDirection.InputOutput;
            cmd.Parameters.Add(new SqlParameter("@NSTCode", SqlDbType.NVarChar, 5)).Value = nstCode;

            cmd.Parameters["@NotIntNo"].Value = 0;
            //Edge 3013-07-11
            cmd.Parameters.Add(new SqlParameter("@ReceiptDate", SqlDbType.DateTime)).Value = receiptDate;

            //Jake 2014-12-18 added for 5383
            if (crtIntNo.HasValue && crtIntNo.Value > 0)
                cmd.Parameters.Add(new SqlParameter("@CrtIntNo", SqlDbType.Int)).Value = crtIntNo;
            if (crtRIntNo.HasValue && crtRIntNo.Value > 0)
                cmd.Parameters.Add(new SqlParameter("@CrtRIntNo", SqlDbType.Int)).Value = crtRIntNo;
            if (!String.IsNullOrEmpty(notRegNo))
                cmd.Parameters.Add(new SqlParameter("@NotRegNo", SqlDbType.NVarChar, 20)).Value = notRegNo;
            if (offenceDate.HasValue)
                cmd.Parameters.Add(new SqlParameter("@OffenceDate", SqlDbType.DateTime)).Value = offenceDate;
            if (sumCourtDate.HasValue)
                cmd.Parameters.Add(new SqlParameter("@SumCourtDate", SqlDbType.DateTime)).Value = sumCourtDate;
            //@CrtIntNo INT  ,
            //@CrtRIntNo int, 
            //@SumCourtDate SMALLDATETIME,
            //@NotRegNo VARCHAR(20),
            //@OffenceDate SMALLDATETIME

            try
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                cmd.ExecuteNonQuery();

                notIntNo = Convert.ToInt32(cmd.Parameters["@NotIntNo"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //conn.Close();
                // 2014-10-15, Oscar changed
                cmd.Dispose();
                conn.Dispose();
            }
        }
    }

    // Do not add / remove property in this entity.
    public sealed class TempReceiptNotice
    {
        public int? NotIntNo { get; set; }
        public int? ChgIntNo { get; set; }
        public int? CTIntNo { get; set; }
        public int? AccIntNo { get; set; }
        public decimal? ChgRevFineAmount { get; set; }
        public decimal? AmountToPay { get; set; }
        public decimal? ContemptAmount { get; set; }
        public decimal? DeferredAmount { get; set; }
        public int? SCDefPayIntNo { get; set; }
        public string SeniorOfficer { get; set; }
    }
}

