using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{
	
	public partial class CameraUnitTranNoDetails 
	{
		public Int32 CUTIntNo;
		public Int32 AutNo;
		public string CamUnitID;
        public Int32 CUTNextNo;
		public string LastUser;
	}

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public partial class CameraUnitTranNoDB
	{
		string mConstr = "";

			public CameraUnitTranNoDB (string vConstr)
			{
				mConstr = vConstr;
			}

		//*******************************************************
		//
		// The GetCameraUnitTranNoDetails method returns a CameraUnitTranNoDetails
		// struct that contains information about a specific transaction number
		//
		//*******************************************************

		public CameraUnitTranNoDetails GetCameraUnitTranNoDetails(int cutIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("CameraUnitTranNoDetail", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterCUTIntNo = new SqlParameter("@CUTIntNo", SqlDbType.Int, 4);
			parameterCUTIntNo.Value = cutIntNo;
			myCommand.Parameters.Add(parameterCUTIntNo);

			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
			// Create CustomerDetails Struct
			CameraUnitTranNoDetails myCameraUnitTranNoDetails = new CameraUnitTranNoDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
                myCameraUnitTranNoDetails.CUTIntNo = Convert.ToInt32(result["CUTIntNo"]);
				myCameraUnitTranNoDetails.AutNo = Convert.ToInt32(result["AutNo"]);
                myCameraUnitTranNoDetails.CamUnitID = result["CamUnitID"].ToString();
                myCameraUnitTranNoDetails.CUTNextNo = Convert.ToInt32(result["CUTNextNo"]);
				myCameraUnitTranNoDetails.LastUser = result["LastUser"].ToString();
			}
			result.Close();
			return myCameraUnitTranNoDetails;
		}

  		//*******************************************************
		//
		// The AddCameraUnitTranNo method inserts a new transaction number record
		// into the CameraUnitTranNo database.  A unique "CUTIntNo"
		// key is then returned from the method.  
		//
		//*******************************************************

		public int AddCameraUnitTranNo(string autNo, string camUnitID, int cutNextNo, string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("CameraUnitTranNoAdd", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
            SqlParameter parameterAutNo = new SqlParameter("@AutNo", SqlDbType.VarChar, 6);
            parameterAutNo.Value = autNo;
            myCommand.Parameters.Add(parameterAutNo);

            SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
            parameterCamUnitID.Value = camUnitID;
            myCommand.Parameters.Add(parameterCamUnitID);

            SqlParameter parameterCutNextNo = new SqlParameter("@CutNextNo", SqlDbType.Int, 4);
            parameterCutNextNo.Value = cutNextNo;
            myCommand.Parameters.Add(parameterCutNextNo);

    		SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterCUTIntNo = new SqlParameter("@CUTIntNo", SqlDbType.Int, 4);
			parameterCUTIntNo.Direction = ParameterDirection.Output;
			myCommand.Parameters.Add(parameterCUTIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int cutIntNo = Convert.ToInt32(parameterCUTIntNo.Value);

				return cutIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

        // 2013-07-19 comment by Henry for useless
        //public SqlDataReader GetCameraUnitTranNoList(string autNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("CameraUnitTranNosList", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutNo = new SqlParameter("@AutNo", SqlDbType.VarChar, 6);
        //    parameterAutNo.Value = autNo;
        //    myCommand.Parameters.Add(parameterAutNo);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the datareader result
        //    return result;
        //}

		public DataSet GetCameraUnitTranNoListDS(string autNo)
		{
			SqlDataAdapter sqlDACameraUnitTranNos = new SqlDataAdapter();
			DataSet dsCameraUnitTranNos = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDACameraUnitTranNos.SelectCommand = new SqlCommand();
			sqlDACameraUnitTranNos.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDACameraUnitTranNos.SelectCommand.CommandText = "CameraUnitTranNoList";

			// Mark the Command as a SPROC
			sqlDACameraUnitTranNos.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterAutNo = new SqlParameter("@AutNo", SqlDbType.VarChar, 6);
            parameterAutNo.Value = autNo;
            sqlDACameraUnitTranNos.SelectCommand.Parameters.Add(parameterAutNo);

			// Execute the command and close the connection
			sqlDACameraUnitTranNos.Fill(dsCameraUnitTranNos);
			sqlDACameraUnitTranNos.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsCameraUnitTranNos;		
		}

		public int UpdateCameraUnitTranNo(string autNo, string camUnitID, int cutNextNo, string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("CameraUnitTranNoUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
            SqlParameter parameterAutNo = new SqlParameter("@AutNo", SqlDbType.VarChar, 6);
            parameterAutNo.Value = autNo;
            myCommand.Parameters.Add(parameterAutNo);

            SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
            parameterCamUnitID.Value = camUnitID;
            myCommand.Parameters.Add(parameterCamUnitID);

            SqlParameter parameterCutNextNo = new SqlParameter("@CutNextNo", SqlDbType.Int, 4);
            parameterCutNextNo.Value = cutNextNo;
            myCommand.Parameters.Add(parameterCutNextNo);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterCUTIntNo = new SqlParameter("@CUTIntNo", SqlDbType.Int, 4);
			parameterCUTIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterCUTIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int CUTIntNo = (int)myCommand.Parameters["@CUTIntNo"].Value;

				return CUTIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

        // 2013-07-19 comment by Henry for useless
        //public int UpdateTranNumber(int cutNextNo, string lastUser, int cutIntNo) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("CameraUnitTranNoUpdateNumber", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterCutNextNo = new SqlParameter("@CutNextNo", SqlDbType.Int, 4);
        //    parameterCutNextNo.Value = cutNextNo;
        //    myCommand.Parameters.Add(parameterCutNextNo);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterCUTIntNo = new SqlParameter("@CUTIntNo", SqlDbType.Int, 4);
        //    parameterCUTIntNo.Value = cutIntNo;
        //    parameterCUTIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterCUTIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int CUTIntNo = (int)myCommand.Parameters["@CUTIntNo"].Value;

        //        return CUTIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        // 2013-07-19 comment by Henry for useless
        //public String DeleteCameraUnitTranNo (int cutIntNo)
        //{

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("CameraUnitTranNoDelete", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterCUTIntNo = new SqlParameter("@CUTIntNo", SqlDbType.Int, 4);
        //    parameterCUTIntNo.Value = cutIntNo;
        //    parameterCUTIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterCUTIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int userId = (int)parameterCUTIntNo.Value;

        //        return userId.ToString();
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return String.Empty;
        //    }
        //}

        public int GetNextCameraUnitTranNo(string autNo, string camUnitID, int maxNo, string lastUser)
        {
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("CameraUnitTranNoNextNumber", myConnection);

            // Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
            SqlParameter parameterCamUnitID = new SqlParameter("@CamUnitID", SqlDbType.VarChar, 5);
            parameterCamUnitID.Value = camUnitID;
            myCommand.Parameters.Add(parameterCamUnitID);

            SqlParameter parameterAutNo = new SqlParameter("@AutNo", SqlDbType.VarChar, 6);
            parameterAutNo.Value = autNo;
            myCommand.Parameters.Add(parameterAutNo);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterMaxNo = new SqlParameter("@MaxNo", SqlDbType.Int, 4);
            parameterMaxNo.Value = maxNo;
            myCommand.Parameters.Add(parameterMaxNo);

			SqlParameter parameterTNumber = new SqlParameter("@CUTNextNo", SqlDbType.Int);
			parameterTNumber.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterTNumber);
			
			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

                int cutNextNo = Convert.ToInt32(parameterTNumber.Value);

                return cutNextNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}
    }
}
