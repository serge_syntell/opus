﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS.Data;
using Stalberg.TMS.Data.Resources;

namespace Stalberg.TMS
{
    public class DefaultCourtRules
    {
        protected CourtRulesDetails rule = new CourtRulesDetails();
        private string mConstr;

        public DefaultCourtRules(CourtRulesDetails courtRule, string vConstr)
        {
            this.rule = courtRule;
            this.mConstr = vConstr;
        }

        #region Jerry 2012-05-28 disable
        //public KeyValuePair<int, string> SetDefaultCourtRule()
        //{
        //    switch (this.rule.CRCode)
        //    {
        //        case "1010":
        //            this.rule.CRComment = "Y = All origin groups combined (Default); N = Separate court rolls for camera and handwritten offences";
        //            this.rule.CRDescr = "Generate court roll for all origin groups";
        //            this.rule.CRString = "Y";
        //            this.rule.CRNumeric = 0;
        //            break;

        //        case "1020":
        //            this.rule.CRComment = "N = Personal and impersonal service (Default); Y = Personal service only";
        //            this.rule.CRDescr = "Generate case numbers for personal service only";
        //            this.rule.CRString = "N";
        //            this.rule.CRNumeric = 0;
        //            break;

        //        case "3010":
        //            this.rule.CRComment = "N = All origin groups use the same court date (Default); Y = Each origin group has a specific court date";
        //            this.rule.CRDescr = "Use separate court dates for each origin group";
        //            this.rule.CRString = "N";
        //            this.rule.CRNumeric = 0;
        //            break;

        //        case "3020":
        //            this.rule.CRComment = "N = Use same court dates for HWO-S341 and HWO-S56 (Default); Y = Use separate court dates for HWO-S341 and HWO-S56";
        //            this.rule.CRDescr = "Use separate court dates for HWO-S341 and HWO-S56";
        //            this.rule.CRString = "N";
        //            this.rule.CRNumeric = 0;
        //            break;

        //        //jerry 2012-03-22 add
        //        case "3030":
        //            this.rule.CRComment = "Does the court restrict the number of NoAOG cases the magistrate is prepared to handle each day? Default = N; Permitted values Y or N";
        //            this.rule.CRDescr = "Restrict the number of NoAOG cases per court date";
        //            this.rule.CRString = "N";
        //            this.rule.CRNumeric = 0;
        //            break;

        //        //jerry 2011-11-16 add
        //        case "5010":
        //            this.rule.CRComment = "Y = Automatic (Default); N = Manual";
        //            this.rule.CRDescr = "WOA numbers can be generated automatically (Y) or manually (N)";
        //            this.rule.CRString = "Y";
        //            this.rule.CRNumeric = 0;
        //            break;                
        //    }

        //    CourtRulesDB cr = new CourtRulesDB(mConstr);
        //    this.rule = cr.GetDefaultCourtRule(this.rule);

        //    KeyValuePair<int, string> value = new KeyValuePair<int, string>(this.rule.CRNumeric, this.rule.CRString);

        //    return value;
        //}
        #endregion

        public KeyValuePair<int, string> SetDefaultCourtRule()
        {
            CourtRulesDB cr = new CourtRulesDB(mConstr);

            string ruleCode = "CR_" + this.rule.CRCode;
            this.rule = cr.GetDefaultCourtRule(this.rule);

            if (this.rule == null)
            {
                throw new Exception(string.Format(RuleErrorRe.MissingCourtRule, ruleCode));
            }

            KeyValuePair<int, string> value = new KeyValuePair<int, string>(this.rule.CRNumeric, this.rule.CRString);
            return value;
        }
    }
}
