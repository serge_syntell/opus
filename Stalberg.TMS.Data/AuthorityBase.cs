using System;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a base class that represents an authority
    /// </summary>
    public class AuthorityBase
    {
        // Fields
        private int autIntNo = 0;
        private string autName = string.Empty;
        private string autNo = string.Empty;
        private int arIntNo = 0;
        private DateTime summonsLastCreateDate;
        private int noOfDaysGracePeriod = 30;
        private bool cancelLoggedReps = false;
        private string graceperiodRptEmail;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorityBase"/> class.
        /// </summary>
        public AuthorityBase()
        {
        }

        /// <summary>
        /// Gets or sets the Authority name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return autName; }
            set { autName = value; }
        }

        public string AutNo
        {
            get { return autNo; }
            set { autNo = value; }
        }

        public DateTime sLastCreateDate
        {
            get { return summonsLastCreateDate; }
            set { summonsLastCreateDate = value; }
        }

        public bool CancelLoggedReps
        {
            get { return cancelLoggedReps; }
            set { cancelLoggedReps = value; }
        }

        public int NoOfDaysGracePeriod
        {
            get { return noOfDaysGracePeriod; }
            set { noOfDaysGracePeriod = value; }
        }

        /// <summary>
        /// Gets or sets the database ID of the Authority.
        /// </summary>
        /// <value>The ID.</value>
        public int ID
        {
            get { return autIntNo; }
            set { autIntNo = value; }
        }

        public int ARIntNo
        {
            get { return arIntNo; }
            set { arIntNo = value; }
        }

        public string GracePeriodRptEmail
        {
            get { return graceperiodRptEmail; }
            set { graceperiodRptEmail = value; }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            return string.Format("{0} ({1})", this.autName, this.autIntNo);
        }

    }
}
