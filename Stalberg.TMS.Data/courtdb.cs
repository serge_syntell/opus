using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{
    /// <summary>
    /// A simple data class that encapsulates details about a particular menu 
    /// </summary>
    public class CourtDetails
    {
        public string CrtName;
        public string FullCrtName;//2013-03-05 added by Nancy for 4890
        public string CrtNo;
        public string CrtAddress;
        public string CrtPaymentAddr1;
        public string CrtPaymentAddr2;
        public string CrtPaymentAddr3;
        public string CrtPaymentAddr4;
        public string CrtPrefix;
        public decimal CrDefaultContempt;
        public string MagisterialCD;
    }

    /// <summary>
    /// Business/Data Logic Class that encapsulates all data
    /// logic necessary to add/login/query Courts within
    /// the Commerce Starter Kit Customer database.
    /// </summary>
    public class CourtDB
    {
        // Fields
        string mConstr = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="CourtDB"/> class.
        /// </summary>
        /// <param name="vConstr">The v constr.</param>
        public CourtDB(string vConstr)
        {
            mConstr = vConstr;
        }

        //*******************************************************
        //
        // CourtDB.GetCourtDetails() Method <a name="GetCourtDetails"></a>
        //
        // The GetCourtDetails method returns a CourtDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************
        public CourtDetails GetCourtDetails(int crtIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            CourtDetails myCourtDetails = new CourtDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myCourtDetails.CrtName = result["CrtName"].ToString();
                myCourtDetails.FullCrtName = result["CrtFullCourtName"].ToString();//2013-03-05 added by Nancy for 4890
                myCourtDetails.CrtNo = result["CrtNo"].ToString();
                myCourtDetails.CrtAddress = result["CrtAddress"].ToString();
                myCourtDetails.CrtPaymentAddr1 = result["CrtPaymentAddr1"].ToString();
                myCourtDetails.CrtPaymentAddr2 = result["CrtPaymentAddr2"].ToString();
                myCourtDetails.CrtPaymentAddr3 = result["CrtPaymentAddr3"].ToString();
                myCourtDetails.CrtPaymentAddr4 = result["CrtPaymentAddr4"].ToString();
                myCourtDetails.CrtPrefix = result["CrtPrefix"].ToString();
                myCourtDetails.CrDefaultContempt = decimal.Parse(result["CrDefaultContempt"].ToString());
                myCourtDetails.MagisterialCD = result["MagisterialCD"].ToString();
                myCourtDetails.MagisterialCD = result["MagisterialCD"].ToString();
            }
            result.Close();
            return myCourtDetails;
        }
        //Barry Dickson - need to get court details by SChIntNo
        public CourtDetails GetCourtDetailsbySumCharge(int SChIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("GetCourtDetailsbySumCharge", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterSChIntNo = new SqlParameter("@SChIntNo", SqlDbType.Int, 4);
            parameterSChIntNo.Value = SChIntNo;
            myCommand.Parameters.Add(parameterSChIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            CourtDetails myCourtDetailsbySumCharge = new CourtDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myCourtDetailsbySumCharge.CrtName = result["CrtName"].ToString();
                myCourtDetailsbySumCharge.CrtNo = result["CrtNo"].ToString();
                myCourtDetailsbySumCharge.CrtAddress = result["CrtAddress"].ToString();
                myCourtDetailsbySumCharge.CrtPaymentAddr1 = result["CrtPaymentAddr1"].ToString();
                myCourtDetailsbySumCharge.CrtPaymentAddr2 = result["CrtPaymentAddr2"].ToString();
                myCourtDetailsbySumCharge.CrtPaymentAddr3 = result["CrtPaymentAddr3"].ToString();
                myCourtDetailsbySumCharge.CrtPaymentAddr4 = result["CrtPaymentAddr4"].ToString();
                myCourtDetailsbySumCharge.CrtPrefix = result["CrtPrefix"].ToString();
                myCourtDetailsbySumCharge.CrDefaultContempt = decimal.Parse(result["CrDefaultContempt"].ToString());
            }
            result.Close();
            return myCourtDetailsbySumCharge;
        }

        //*******************************************************
        //
        // CourtDB.AddCourt() Method <a name="AddCourt"></a>
        //
        // The AddCourt method inserts a new menu record
        // into the menus database.  A unique "CourtId"
        // key is then returned from the method.  
        //
        //*******************************************************
        //2013-03-05 updated by Nancy for 4890(add 'string fullCrtName,')
        public int AddCourt(string crtName, string fullCrtName, string crtNo, string crtAddress, string crtPaymentAddr1,
            string crtPaymentAddr2, string crtPaymentAddr3, string crtPaymentAddr4, string lastUser, string sPrefix, decimal crDefaultContempt, string MagisterialCD)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtName = new SqlParameter("@CrtName", SqlDbType.VarChar, 30);
            parameterCrtName.Value = crtName;
            myCommand.Parameters.Add(parameterCrtName);

            //added by Nancy for 4890 --start
            SqlParameter parameterFullCrtName = new SqlParameter("@CrtFullCourtName", SqlDbType.VarChar, 100);
            parameterFullCrtName.Value = fullCrtName;
            myCommand.Parameters.Add(parameterFullCrtName);
            //added by Nancy for 4890 --end

            SqlParameter parameterCrtNo = new SqlParameter("@CrtNo", SqlDbType.VarChar, 6);
            parameterCrtNo.Value = crtNo;
            myCommand.Parameters.Add(parameterCrtNo);

            SqlParameter parameterCrtAddress = new SqlParameter("@CrtAddress", SqlDbType.VarChar, 255);
            parameterCrtAddress.Value = crtAddress;
            myCommand.Parameters.Add(parameterCrtAddress);

            SqlParameter parameterCrtPaymentAddr1 = new SqlParameter("@CrtPaymentAddr1", SqlDbType.VarChar, 25);
            parameterCrtPaymentAddr1.Value = crtPaymentAddr1;
            myCommand.Parameters.Add(parameterCrtPaymentAddr1);

            SqlParameter parameterCrtPaymentAddr2 = new SqlParameter("@CrtPaymentAddr2", SqlDbType.VarChar, 25);
            parameterCrtPaymentAddr2.Value = crtPaymentAddr2;
            myCommand.Parameters.Add(parameterCrtPaymentAddr2);

            SqlParameter parameterCrtPaymentAddr3 = new SqlParameter("@CrtPaymentAddr3", SqlDbType.VarChar, 25);
            parameterCrtPaymentAddr3.Value = crtPaymentAddr3;
            myCommand.Parameters.Add(parameterCrtPaymentAddr3);

            SqlParameter parameterCrtPaymentAddr4 = new SqlParameter("@CrtPaymentAddr4", SqlDbType.VarChar, 25);
            parameterCrtPaymentAddr4.Value = crtPaymentAddr4;
            myCommand.Parameters.Add(parameterCrtPaymentAddr4);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterPrefix = new SqlParameter("@CrtPrefix", SqlDbType.VarChar, 5);
            parameterPrefix.Value = sPrefix;
            myCommand.Parameters.Add(parameterPrefix);

            SqlParameter parameterDefaultContempt = new SqlParameter("@CrDefaultContempt", SqlDbType.Decimal);
            parameterDefaultContempt.Value = crDefaultContempt;
            myCommand.Parameters.Add(parameterDefaultContempt);

            SqlParameter parameterMagisterialCD = new SqlParameter("@MagisterialCD", SqlDbType.VarChar);
            parameterMagisterialCD.Value = MagisterialCD;
            myCommand.Parameters.Add(parameterMagisterialCD);

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterCrtIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int crtIntNo = Convert.ToInt32(parameterCrtIntNo.Value);

                return crtIntNo;
            }
            catch (Exception e)
            {

                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// Adds the auth_ court.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="crtIntNo">The CRT int no.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        public int AddAuth_Court(int autIntNo, int crtIntNo, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("Auth_CourtAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterACIntNo = new SqlParameter("@ACIntNo", SqlDbType.Int, 4);
            parameterACIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterACIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int acIntNo = Convert.ToInt32(parameterACIntNo.Value);

                return acIntNo;
            }
            catch (Exception e)
            {

                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// Gets the court list.
        /// </summary>
        /// <returns></returns>
        public SqlDataReader GetCourtList()
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        /// <summary>
        /// Gets the auth_ court list by auth.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        public SqlDataReader GetAuth_CourtListByAuth(int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("Auth_CourtListByAuth", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader 
            return result;
        }

        /// <summary>
        /// Gets the court date.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        public DateTime CourtDateByCourt(int crtIntNo, DateTime dtCourt)
        {
            DateTime dt = new DateTime(2000, 1, 1);
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtDateByCourt", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            SqlParameter parameterCrtDate = new SqlParameter("@CrtDate", SqlDbType.SmallDateTime);
            parameterCrtDate.Value = dtCourt;
            myCommand.Parameters.Add(parameterCrtDate);

            try
            {
                // Execute the command
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader();
                if (result.HasRows)
                {
                    while (result.Read())
                        dt = Helper.GetReaderValue<DateTime>(result, "CDate");
                }
                result.Close();
            }
            finally
            {
                myConnection.Dispose();
            }
            return dt;
        }

        public DateTime CourtDateByCourtForWarrant(int crtIntNo, DateTime courtDate)
        {
            DateTime dt = new DateTime(2000, 1, 1);
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtDateByCourtForWarrant", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            SqlParameter parameterCrtDate = new SqlParameter("@CrtDate", SqlDbType.SmallDateTime);
            parameterCrtDate.Value = courtDate;
            myCommand.Parameters.Add(parameterCrtDate);

            try
            {
                // Execute the command
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader();
                if (result.HasRows)
                {
                    while (result.Read())
                        dt = Helper.GetReaderValue<DateTime>(result, "CDateWar");
                }
                result.Close();
            }
            finally
            {
                myConnection.Dispose();
            }
            return dt;
        }

        /// <summary>
        /// Gets the auth_ court list by court.
        /// </summary>
        /// <param name="crtIntNo">The CRT int no.</param>
        /// <returns></returns>
        public SqlDataReader GetAuth_CourtListByCourt(int crtIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("Auth_CourtListByCourt", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        /// <summary>
        /// Gets the court list DS.
        /// </summary>
        /// <returns></returns>
        public DataSet GetCourtListDS()
        {
            SqlDataAdapter sqlDACourts = new SqlDataAdapter();
            DataSet dsCourts = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDACourts.SelectCommand = new SqlCommand();
            sqlDACourts.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDACourts.SelectCommand.CommandText = "CourtList";

            // Mark the Command as a SPROC
            sqlDACourts.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command and close the connection
            sqlDACourts.Fill(dsCourts);
            sqlDACourts.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsCourts;
        }

        /// <summary>
        /// Gets the auth_ court list by auth DS.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        public DataSet GetAuth_CourtListByAuthDS(int autIntNo)
        {
            SqlDataAdapter sqlDACourts = new SqlDataAdapter();
            DataSet dsCourts = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDACourts.SelectCommand = new SqlCommand();
            sqlDACourts.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDACourts.SelectCommand.CommandText = "Auth_CourtListByAuth";

            // Mark the Command as a SPROC
            sqlDACourts.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDACourts.SelectCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command and close the connection
            sqlDACourts.Fill(dsCourts);
            sqlDACourts.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsCourts;
        }

        /// <summary>
        /// Updates the court.
        /// </summary>
        /// <param name="crtIntNo">The CRT int no.</param>
        /// <param name="crtName">Name of the CRT.</param>
        /// <param name="crtNo">The CRT no.</param>
        /// <param name="crtAddress">The CRT address.</param>
        /// <param name="crtPaymentAddr1">The CRT payment addr1.</param>
        /// <param name="crtPaymentAddr2">The CRT payment addr2.</param>
        /// <param name="crtPaymentAddr3">The CRT payment addr3.</param>
        /// <param name="crtPaymentAddr4">The CRT payment addr4.</param>
        /// <param name="lastUser">The last user.</param>
        /// <param name="sPrefix">The s prefix.</param>
        /// <returns></returns>
        //2013-03-05 updated by Nancy for 4890(add 'string fullCrtName,')
        public int UpdateCourt(int crtIntNo, string crtName, string fullCrtName, string crtNo,
            string crtAddress, string crtPaymentAddr1,
            string crtPaymentAddr2, string crtPaymentAddr3, string crtPaymentAddr4, string lastUser, string sPrefix, decimal crDefaultContempt, string MagisterialCD)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtName = new SqlParameter("@CrtName", SqlDbType.VarChar, 30);
            parameterCrtName.Value = crtName;
            myCommand.Parameters.Add(parameterCrtName);

            //added by Nancy for 4890--start
            SqlParameter parameterFullCrtName = new SqlParameter("@CrtFullCourtName", SqlDbType.VarChar, 30);
            parameterFullCrtName.Value = fullCrtName;
            myCommand.Parameters.Add(parameterFullCrtName);
            //added by Nancy for 4890--start

            SqlParameter parameterCrtNo = new SqlParameter("@CrtNo", SqlDbType.VarChar, 6);
            parameterCrtNo.Value = crtNo;
            myCommand.Parameters.Add(parameterCrtNo);

            SqlParameter parameterCrtAddress = new SqlParameter("@CrtAddress", SqlDbType.VarChar, 255);
            parameterCrtAddress.Value = crtAddress;
            myCommand.Parameters.Add(parameterCrtAddress);

            SqlParameter parameterCrtPaymentAddr1 = new SqlParameter("@CrtPaymentAddr1", SqlDbType.VarChar, 25);
            parameterCrtPaymentAddr1.Value = crtPaymentAddr1;
            myCommand.Parameters.Add(parameterCrtPaymentAddr1);

            SqlParameter parameterCrtPaymentAddr2 = new SqlParameter("@CrtPaymentAddr2", SqlDbType.VarChar, 25);
            parameterCrtPaymentAddr2.Value = crtPaymentAddr2;
            myCommand.Parameters.Add(parameterCrtPaymentAddr2);

            SqlParameter parameterCrtPaymentAddr3 = new SqlParameter("@CrtPaymentAddr3", SqlDbType.VarChar, 25);
            parameterCrtPaymentAddr3.Value = crtPaymentAddr3;
            myCommand.Parameters.Add(parameterCrtPaymentAddr3);

            SqlParameter parameterCrtPaymentAddr4 = new SqlParameter("@CrtPaymentAddr4", SqlDbType.VarChar, 25);
            parameterCrtPaymentAddr4.Value = crtPaymentAddr4;
            myCommand.Parameters.Add(parameterCrtPaymentAddr4);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterPrefix = new SqlParameter("@CrtPrefix", SqlDbType.VarChar, 5);
            parameterPrefix.Value = sPrefix;
            myCommand.Parameters.Add(parameterPrefix);

            SqlParameter parameterDefaultContempt = new SqlParameter("@CrDefaultContempt", SqlDbType.Decimal);
            parameterDefaultContempt.Value = crDefaultContempt;
            myCommand.Parameters.Add(parameterDefaultContempt);

            SqlParameter parameterMagisterialCD = new SqlParameter("@MagisterialCD", SqlDbType.VarChar);
            parameterMagisterialCD.Value = MagisterialCD;
            myCommand.Parameters.Add(parameterMagisterialCD);

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int);
            parameterCrtIntNo.Direction = ParameterDirection.InputOutput;
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int CrtIntNo = (int)myCommand.Parameters["@CrtIntNo"].Value;
                //int menuId = (int)parameterCrtIntNo.Value;

                return CrtIntNo;
            }
            catch (Exception e)
            {

                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// Updates the court partial.
        /// </summary>
        /// <param name="crtIntNo">The CRT int no.</param>
        /// <param name="crtName">Name of the CRT.</param>
        /// <param name="crtPaymentAddr1">The CRT payment addr1.</param>
        /// <param name="crtPaymentAddr2">The CRT payment addr2.</param>
        /// <param name="crtPaymentAddr3">The CRT payment addr3.</param>
        /// <param name="crtPaymentAddr4">The CRT payment addr4.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        public int UpdateCourtPartial(int crtIntNo, string crtName, string crtPaymentAddr1,
        string crtPaymentAddr2, string crtPaymentAddr3, string crtPaymentAddr4, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtUpdatepartial", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtName = new SqlParameter("@CrtName", SqlDbType.VarChar, 30);
            parameterCrtName.Value = crtName;
            myCommand.Parameters.Add(parameterCrtName);

            SqlParameter parameterCrtPaymentAddr1 = new SqlParameter("@CrtPaymentAddr1", SqlDbType.VarChar, 25);
            parameterCrtPaymentAddr1.Value = crtPaymentAddr1;
            myCommand.Parameters.Add(parameterCrtPaymentAddr1);

            SqlParameter parameterCrtPaymentAddr2 = new SqlParameter("@CrtPaymentAddr2", SqlDbType.VarChar, 25);
            parameterCrtPaymentAddr2.Value = crtPaymentAddr2;
            myCommand.Parameters.Add(parameterCrtPaymentAddr2);

            SqlParameter parameterCrtPaymentAddr3 = new SqlParameter("@CrtPaymentAddr3", SqlDbType.VarChar, 25);
            parameterCrtPaymentAddr3.Value = crtPaymentAddr3;
            myCommand.Parameters.Add(parameterCrtPaymentAddr3);

            SqlParameter parameterCrtPaymentAddr4 = new SqlParameter("@CrtPaymentAddr4", SqlDbType.VarChar, 25);
            parameterCrtPaymentAddr4.Value = crtPaymentAddr4;
            myCommand.Parameters.Add(parameterCrtPaymentAddr4);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int);
            parameterCrtIntNo.Direction = ParameterDirection.InputOutput;
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int CrtIntNo = (int)myCommand.Parameters["@CrtIntNo"].Value;
                //int menuId = (int)parameterCrtIntNo.Value;

                return CrtIntNo;
            }
            catch (Exception e)
            {

                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// Updates the auth_ court.
        /// </summary>
        /// <param name="acIntNo">The ac int no.</param>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="crtIntNo">The CRT int no.</param>
        /// <param name="lastUser">The last user.</param>
        /// <returns></returns>
        public int UpdateAuth_Court(int acIntNo, int autIntNo, int crtIntNo, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterACIntNo = new SqlParameter("@ACIntNo", SqlDbType.Int);
            parameterACIntNo.Direction = ParameterDirection.InputOutput;
            parameterACIntNo.Value = acIntNo;
            myCommand.Parameters.Add(parameterACIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int ACIntNo = (int)myCommand.Parameters["@ACIntNo"].Value;
                //int menuId = (int)parameterCrtIntNo.Value;

                return ACIntNo;
            }
            catch (Exception e)
            {

                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// Gets the court int no by court no.
        /// </summary>
        /// <param name="courtNo">The court no.</param>
        /// <returns></returns>
        public int GetCourtIntNoByCourtNo(string courtNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtIntNoByCourtNo", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCourtNo = new SqlParameter("@CrtNo", SqlDbType.VarChar, 6);
            parameterCourtNo.Value = courtNo;
            myCommand.Parameters.Add(parameterCourtNo);

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterCrtIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int getCrtIntNo = Convert.ToInt32(parameterCrtIntNo.Value);

                return getCrtIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// Deletes the auth_ court.
        /// </summary>
        /// <param name="acIntNo">The ac int no.</param>
        /// <returns></returns>
        public String DeleteAuth_Court(int acIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("Auth_CourtDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterACIntNo = new SqlParameter("@ACIntNo", SqlDbType.Int, 4);
            parameterACIntNo.Value = acIntNo;
            parameterACIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterACIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int ACIntNo = (int)parameterACIntNo.Value;

                return ACIntNo.ToString();
            }
            catch
            {
                myConnection.Dispose();
                return String.Empty;
            }
        }

        /// <summary>
        /// Deletes the court.
        /// </summary>
        /// <param name="crtIntNo">The CRT int no.</param>
        /// <returns></returns>
        public String DeleteCourt(int crtIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtIntNo;
            parameterCrtIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterCrtIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int CrtIntNo = (int)parameterCrtIntNo.Value;

                return CrtIntNo.ToString();
            }
            catch
            {
                myConnection.Dispose();
                return String.Empty;
            }
        }

        /// <summary>
        /// Gets the auth_ court list by court.
        /// </summary>
        /// <param name="crtIntNo">The CRT int no.</param>
        /// <returns></returns>
        public SqlDataReader GetCourtRollLabels(int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtRollLabels", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterCrtIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        /// <summary>
        /// Gets the court prefix
        /// </summary>
        /// <returns></returns>
        public SqlDataReader GetCourtPrefixByAuthority(int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CourtPrefixByAuthority", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterCrtIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        //public int GetAvailabledNextSequenceNumberForWOANumber(int crtIntNo, int ttIntNo, string lastUser, out int ctnlaIntNo)
        // 2014-06-16, Oscar changed
        public int GetAvailabledNextSequenceNumberForWOANumber(int crtIntNo, int ttIntNo, string lastUser, out int ctnlaIntNo, string year = null)
        {
            //@CrtIntNo INT ,
            //@TTIntNo INT,
            //@LastUser VARCHAR(50),
            //@TNumber INT = 0 OUTPUT,
            //@CTNLAIntNo INT OUTPUT,
            //@year varchar(4) =null
            int number = 0;
            ctnlaIntNo = 0;
            SqlConnection conn = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("GetTNumberFromCourtTranNoListAvailableByType", conn);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@CrtIntNo", SqlDbType.Int).Value = crtIntNo;
            com.Parameters.Add("@TTIntNo", SqlDbType.Int).Value = ttIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.NVarChar, 50).Value = lastUser;
            //com.Parameters.Add("@year", SqlDbType.Int).Value = year;
            com.Parameters.Add("@TNumber", SqlDbType.Int);
            com.Parameters.Add("@CTNLAIntNo", SqlDbType.Int);

            // 2014-06-16, Oscar added
            if (year != null)
                com.Parameters.AddWithValue("@year", year);

            com.Parameters[3].Direction = ParameterDirection.Output;
            com.Parameters[4].Direction = ParameterDirection.Output;


            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                com.ExecuteNonQuery();

                if (com.Parameters[3].Value != null)
                {
                    number = Convert.ToInt32(com.Parameters[3].Value);
                }
                if (com.Parameters[4].Value != null)
                {
                    ctnlaIntNo = Convert.ToInt32(com.Parameters[4].Value);
                }

            }
            catch
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
            return number;
        }

        public bool SetForCourtTranNoListAvailable(int ctnLAIntNo, int status)
        {
            SqlConnection conn = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("SetIsAllocatedForCourtTranNoListAvailable", conn);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@CTNLAIntNO", SqlDbType.Int).Value = ctnLAIntNo;
            com.Parameters.Add("@AllocatedStatus", SqlDbType.Int).Value = status;

            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                int returnValue = Convert.ToInt32(com.ExecuteScalar());

                return returnValue == 1;
            }
            catch
            {
                throw;
            }
            finally
            {
                conn.Close();
            }

        }
    }
}

