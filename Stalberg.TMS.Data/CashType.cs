namespace Stalberg.TMS
{
    /// <summary>
    /// Lists the different types of 'Cash' payment methods.
    /// (the integer values of the enumeration refer to the characters that represent them i n the database)
    /// </summary>
    public enum CashType
    {
        //dls 080122 - add a new payment type BankPayment (K) - may not be used in conjunction with other types when doing multiple payments

        None = 0,                           // Invalid - must be assigned one of the other values
        Cash = 65,                          // A
        Cheque = 81,                        // Q
        CreditCard = 68,                    // D
        BankPayment = 75,                   // K - Bank Payment                      
        DebitCard = 66,                     // B - Debit Card
        PostalOrder = 80,                   // P
        DirectDeposit = 84,                 // T - Direct deposit into the LA's bank account
        ElectronicPayment = 69,             // E - Internet transfer (also used for Manual Cash Capture
        PayFine = 70,                       // F - PayFine
        EasyPay = 83,                       // S - EasyPay
        RoadBlockCash = 72,                 // H
        Judgement = 74,                     // J
        RoadBlockCheque = 85,               // U
        RoadBlockCard = 82,                 // R
        CiprusCash = 49,                    // 1
        CiprusCheque = 50,                  // 2
        CiprusPostalOrder = 51,             // 3
        CiprusCreditCard = 52,              // 4
        CiprusOther = 53,           // 5 - DLS swopped Other and InternetPayment around so as to tie up with Civitas spec
        CiprusInternetPayment = 54,         // 6
        CiprusMoneyOrder = 55,              // 7
        CiprusDebitCard = 56,               // 8      
        CiprusForeignCurrency = 57,         // 9
        CiprusDishonouredCheque = 87,       // W            (Code 10 from Civitas)
        CiprusDirectDeposit = 88,           // X            (Code 11 from Civitas)
        CiprusRefundToOffender = 89,        // Y            (Code 20 from Civitas)
        AdminReceipt = 90,                  // Z - Admin Receipt
        HRK_Cash = 67,                      // C -HRK Cash
        HRK_BankAccount = 71,               // G -HRK BankAccount
        HRK_Cheque = 73,                    // I -HRK Cheque
        HRK_CreditCard = 76,                // L -HRK CreditCard
        HRK_DebitCard = 77,                 // M -HRK DebitCard
        HRK_Other = 78,                      // N -HRK Other
        Other = 79,                            //O for overstrand cash type
        NTC_Cash = 91,                       //[ for NTC cash
        NTC_BankAccount = 40,                  //( for NTC bank account
        NTC_Cheque = 93,                      // ] for NTC cheque
        NTC_CreditCard = 41,                    // ) for NTC credit card  
        NTC_DebitCard = 123,                      //{
        NTC_PostalOrder = 125,                  // }
        NTC_Other = 42                          // *                           

    }

}