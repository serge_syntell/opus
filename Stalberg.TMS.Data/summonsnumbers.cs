using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
	/// <summary>
	/// Run general procedures and functions for managing Summons Numbers
	/// </summary>
	public class SummonsNumbers
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="SummonsNumbers"/> class.
        /// </summary>
		public SummonsNumbers()
		{
		}

        /// <summary>
        /// Generates the summons number.
        /// </summary>
        /// <param name="constr">The constr.</param>
        /// <param name="user">The user.</param>
        /// <param name="sTicketNo">The s ticket no.</param>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="errMessage">The err message.</param>
        /// <returns></returns>
		public string GenerateSummonsNumber(string constr, string user, string sTicketNo, int autIntNo, ref string errMessage, Int32 crtIntNo, DateTime SummonsCourtDate)
		{
            // lmz 2007-07-14
			// current method is as follows
			// A - Authority court code
			// B - sequence number
			// C - 2 digits for the last part of the year (i.e 2007 - would be 07)
            // e.g. ST/000009/07

			string summonsNumber = "0";
			int seqLength = 6; // currently sequence is six digits long
            char cTemp = '0';
                     
            //dls 081102 - this code is not referenced anywhere in this proc
            //Stalberg.TMS.AuthorityDB audb = new Stalberg.TMS.AuthorityDB (constr);
            //Stalberg.TMS.AuthorityDetails authDet = new Stalberg.TMS.AuthorityDetails();
            //authDet = audb.GetAuthorityDetails (autIntNo);

            Stalberg.TMS.CourtTranDB tranNo = new Stalberg.TMS.CourtTranDB(constr);
            string sNumber = tranNo.GetNextSummonsNumber(sTicketNo, user, ref errMessage, crtIntNo).Trim();
            
            if (sNumber == "-1" || sNumber == "-2")
                return sNumber;

            //BD 20081113 branch 4000 - need to change the last 2 numbers of the summons number to reflect the year of the court date and not of now
            //string sPostFix = DateTime.Now.Year.ToString ();
            string sPostFix = SummonsCourtDate.Year.ToString();
            try
            {
                int nPos = sNumber.IndexOf("*");
                summonsNumber = sNumber.Substring(0, nPos) + "/" + sNumber.Substring(nPos + 1, (sNumber.Length - (nPos + 1))).PadLeft(seqLength, cTemp) + "/" + sPostFix.Substring(2, 2);
            }
            catch
            {
                summonsNumber = "-1";
            }

            return summonsNumber;
        }

	}
}
