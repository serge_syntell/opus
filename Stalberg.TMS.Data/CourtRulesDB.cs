﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace Stalberg.TMS
{
    public class CourtRulesDetails
    {
        public Int32 CRIntNo;
        public Int32 CrtIntNo;
        public string CRCode;
        public string CRDescr;
        public int CRNumeric;
        public string CRString;
        public string CRComment;
        public string LastUser;
    }
    class CourtRuleInfo
    {
        public int WFRFCID;
        public string WFRFCName;
        public string WFRFCDescr;
        public string WFRFCComment;
        public int CrtIntNo;
        public int CRNumeric;
        public string CRString;

    }
    public partial class CourtRulesDB
    {
        string mConstr = "";

        public CourtRulesDB(string vConstr)
        {
            mConstr = vConstr;
        }

        //public List<CourtRulesDetails> GetCourtRulesListByCrtIntNo(int crtIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("CourtRulesList", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameter = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
        //    parameter.Value = crtIntNo;
        //    myCommand.Parameters.Add(parameter);

        //    try
        //    {
        //        // Execute the command
        //        if (myConnection.State != ConnectionState.Open)
        //            myConnection.Open();
        //        SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
        //        List<CourtRulesDetails> ht = new List<CourtRulesDetails>();
        //        if (result.HasRows)
        //        {
        //            while (result.Read())
        //            {
        //                CourtRulesDetails rule = new CourtRulesDetails();
        //                rule.CrtIntNo = Helper.GetReaderValue<int>(result, "CrtIntNo");
        //                rule.CRCode = Helper.GetReaderValue<string>(result, "CRCode");
        //                rule.CRComment = Helper.GetReaderValue<string>(result, "CRComment");
        //                rule.CRDescr = Helper.GetReaderValue<string>(result, "CRDescr");
        //                rule.CRNumeric = Helper.GetReaderValue<int>(result, "CRNumeric");
        //                rule.CRString = Helper.GetReaderValue<string>(result, "CRString");
        //                ht.Add(rule);
        //            }
        //        }
        //        return ht;
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    finally
        //    {
        //        myConnection.Close();
        //        myConnection.Dispose();
        //    }
        //}

        //public CourtRulesDetails GetDefaultCourtRule(CourtRulesDetails rule)
        //{
        //    SqlConnection con = new SqlConnection(this.mConstr);
        //    SqlCommand com = new SqlCommand("CourtRuleGetDefaultRule", con);
        //    com.CommandType = CommandType.StoredProcedure;

        //    com.Parameters.Add("@CrtIntNo", SqlDbType.Int, 4).Value = rule.CrtIntNo;
        //    com.Parameters.Add("@Code", SqlDbType.VarChar, 10).Value = rule.CRCode;
        //    com.Parameters.Add("@Description", SqlDbType.VarChar, 100).Value = rule.CRDescr;
        //    com.Parameters.Add("@Number", SqlDbType.Int, 4).Value = rule.CRNumeric;
        //    com.Parameters.Add("@String", SqlDbType.VarChar, 35).Value = rule.CRString;
        //    com.Parameters.Add("@Comment", SqlDbType.VarChar, 255).Value = rule.CRComment;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = rule.LastUser;

        //    try
        //    {
        //        con.Open();
        //        SqlDataReader reader = com.ExecuteReader();
        //        if (reader.Read())
        //        {
        //            rule.CrtIntNo = Helper.GetReaderValue<int>(reader, "CrtIntNo");
        //            rule.CRCode = Helper.GetReaderValue<string>(reader, "CRCode");
        //            rule.CRComment = Helper.GetReaderValue<string>(reader, "CRComment");
        //            rule.CRDescr = Helper.GetReaderValue<string>(reader, "CRDescr");
        //            rule.CRNumeric = Helper.GetReaderValue<int>(reader, "CRNumeric");
        //            rule.CRString = Helper.GetReaderValue<string>(reader, "CRString");
        //        }
        //        reader.Close();
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }

        //    return rule;

        //}

        public CourtRulesDetails GetDefaultCourtRule(CourtRulesDetails rule)
        {
            CourtRuleInfo courtRules = null;
            courtRules = GetCourtRuleInfoByWFRFCNameCrtIntNo(rule.CrtIntNo, rule.CRCode);

            if (courtRules != null)
            {
                rule.CRCode = courtRules.WFRFCName;
                rule.CRComment = courtRules.WFRFCComment;
                rule.CRDescr = courtRules.WFRFCDescr;
                rule.CRNumeric = (int)courtRules.CRNumeric;
                rule.CRString = courtRules.CRString;
            }
            else
            {
                #region Jerry 2012-05-28 change
                //WorkFlowRuleForCourt wfCourt = new WorkFlowRuleForCourt();
                //wfCourt = new WorkFlowRuleForCourtService().GetByWfrfcName("CR_" + rule.CRCode);

                //#region Jerry 2012-05-28 disable
                ////if (wfCourt == null)
                ////{
                ////    wfCourt.WfrfcName = "CR_" + rule.CRCode;
                ////    wfCourt.WfrfcDescr = rule.CRDescr;
                ////    wfCourt.WfrfcComment = rule.CRComment;
                ////    wfCourt = new WorkFlowRuleForCourtService().Save(wfCourt);

                ////    WorkFlowRuleForCourtDescrLookup wfDescLookup = new WorkFlowRuleForCourtDescrLookup();
                ////    wfDescLookup.LsCode = "en-US";
                ////    wfDescLookup.Wfrfcid = wfCourt.Wfrfcid;
                ////    wfDescLookup.WfrfcDescr = wfCourt.WfrfcDescr;
                ////    wfDescLookup = new WorkFlowRuleForCourtDescrLookupService().Save(wfDescLookup);

                ////    WorkFlowRuleForCourtCommentLookup wfCommentLookup = new WorkFlowRuleForCourtCommentLookup();
                ////    wfCommentLookup.LsCode = "en-US";
                ////    wfCommentLookup.Wfrfcid = wfCourt.Wfrfcid;
                ////    wfCommentLookup.WfrfcComment = wfCourt.WfrfcComment;
                ////    wfCommentLookup = new WorkFlowRuleForCourtCommentLookupService().Save(wfCommentLookup);
                ////}
                //#endregion

                //if (wfCourt != null)
                //{
                //    CourtRule crEntity = new CourtRule();
                //    crEntity.Wfrfcid = wfCourt.Wfrfcid;
                //    crEntity.CrtIntNo = rule.CrtIntNo;
                //    crEntity.CrNumeric = rule.CRNumeric;
                //    crEntity.CrString = rule.CRString;
                //    crEntity.LastUser = rule.LastUser;
                //    crEntity = new CourtRuleService().Save(crEntity);
                //}
                //else
                //{
                //    return null;
                //}

                //courtRules = GetCourtRuleInfoByWFRFCNameCrtIntNo(rule.CrtIntNo, rule.CRCode);
                //rule.CRCode = courtRules.WFRFCName;
                //rule.CRComment = courtRules.WFRFCComment;
                //rule.CRDescr = courtRules.WFRFCDescr;
                //rule.CRNumeric = (int)courtRules.CRNumeric;
                //rule.CRString = courtRules.CRString;
                #endregion
                return null;
            }

            return rule;

        }


        private CourtRuleInfo GetCourtRuleInfoByWFRFCNameCrtIntNo(int CrtIntNo, string code)
        {
            IDataReader rd = new WorkFlowRuleForCourtService().GetByWFRFCNameCrtIntNo("CR_" + code, CrtIntNo);
            CourtRuleInfo courtRule = null;
            if (rd.Read())
            {
                courtRule = new CourtRuleInfo();
                courtRule.WFRFCName = rd["WFRFCName"].ToString();
                courtRule.WFRFCComment = rd["WFRFCComment"].ToString();
                courtRule.WFRFCDescr = rd["WFRFCDescr"].ToString();
                courtRule.CRNumeric = int.Parse(rd["CRNumeric"].ToString());
                courtRule.CRString = rd["CRString"].ToString();
            }
            rd.Close();
            return courtRule;
        }
        public DataSet GetCourtRulesListDS(int crtIntNo)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dsCourtRules = new DataSet();

            // Create Instance of Connection and Command Object
            adapter.SelectCommand = new SqlCommand();
            adapter.SelectCommand.Connection = new SqlConnection(mConstr);
            adapter.SelectCommand.CommandText = "WorkFlowRuleForCourt_GetByCrtIntNo";

            // Mark the Command as a SPROC
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameter = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameter.Value = crtIntNo;
            adapter.SelectCommand.Parameters.Add(parameter);

            // Execute the command and close the connection
            adapter.Fill(dsCourtRules);
            adapter.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsCourtRules;
        }

        public CourtRulesDetails GetCourtRulesDetails(int crIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            //SP WILL BE DELETED CourtRulesDetail 
            //SqlCommand myCommand = new SqlCommand("CourtRulesDetail", myConnection);
            SqlCommand myCommand = new SqlCommand("WorkFlowRuleForCourtDetail", myConnection);


            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameter = new SqlParameter("@CRID", SqlDbType.Int, 4);
            parameter.Value = crIntNo;
            myCommand.Parameters.Add(parameter);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            CourtRulesDetails myCourtRulesDetails = new CourtRulesDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                myCourtRulesDetails.CRIntNo = Convert.ToInt32(result["CRIntNo"]);
                myCourtRulesDetails.CrtIntNo = Convert.ToInt32(result["CrtIntNo"]);
                myCourtRulesDetails.CRCode = result["CRCode"].ToString();
                myCourtRulesDetails.CRString = result["CRString"].ToString();
                myCourtRulesDetails.CRNumeric = Convert.ToInt32(result["CRNumeric"]);
                myCourtRulesDetails.CRDescr = result["CRDescr"].ToString();
                myCourtRulesDetails.CRComment = result["CRComment"].ToString();
                myCourtRulesDetails.LastUser = result["LastUser"].ToString();
            }
            result.Close();
            return myCourtRulesDetails;
        }

        public int CopyCourtRules(int fromCrtIntNo, int toCrtIntNo, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            //SqlCommand myCommand = new SqlCommand("CourtRulesCopy", myConnection);
            SqlCommand myCommand = new SqlCommand("CourtRuleCopy", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFromCrtIntNo = new SqlParameter("@FromCrtIntNo", SqlDbType.Int, 4);
            parameterFromCrtIntNo.Value = fromCrtIntNo;
            myCommand.Parameters.Add(parameterFromCrtIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterToCrtIntNo = new SqlParameter("@ToCrtIntNo", SqlDbType.Int, 4);
            parameterToCrtIntNo.Value = toCrtIntNo;
            parameterToCrtIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterToCrtIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                toCrtIntNo = Convert.ToInt32(parameterToCrtIntNo.Value);

                return toCrtIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int AddCourtRules(int crtIntNo, int wfrfcId, string crDescr,
            int crNumeric, string crString, string crComment, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            //SqlCommand myCommand = new SqlCommand("CourtRulesAdd", myConnection);
            SqlCommand myCommand = new SqlCommand("CourtRuleAdd", myConnection);


            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            SqlParameter parameterWFRFCID = new SqlParameter("@WFRFCID", SqlDbType.Int, 4);
            parameterWFRFCID.Value = wfrfcId;
            myCommand.Parameters.Add(parameterWFRFCID);

            //SqlParameter parameterCRComment = new SqlParameter("@CRComment", SqlDbType.VarChar, 255);
            //parameterCRComment.Value = crComment;
            //myCommand.Parameters.Add(parameterCRComment);

            SqlParameter parameterCRNumeric = new SqlParameter("@CRNumeric", SqlDbType.Int);
            parameterCRNumeric.Value = crNumeric;
            myCommand.Parameters.Add(parameterCRNumeric);

            //SqlParameter parameterCRDescr = new SqlParameter("@CRDescr", SqlDbType.VarChar, 100);
            //parameterCRDescr.Value = crDescr;
            //myCommand.Parameters.Add(parameterCRDescr);

            SqlParameter parameterCRString = new SqlParameter("@CRString", SqlDbType.VarChar, 25);
            parameterCRString.Value = crString;
            myCommand.Parameters.Add(parameterCRString);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterCRIntNo = new SqlParameter("@CRIntNo", SqlDbType.Int, 4);
            parameterCRIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterCRIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int arIntNo = Convert.ToInt32(parameterCRIntNo.Value);

                return arIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public int UpdateCourtRules(int crtIntNo, string crDescr,
            int crNumeric, string crString, string crComment, string lastUser, int crIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            //no longer use
            //SqlCommand myCommand = new SqlCommand("CourtRulesUpdate", myConnection);
            SqlCommand myCommand = new SqlCommand("CourRuleUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtIntNo", SqlDbType.Int, 4);
            parameterCrtIntNo.Value = crtIntNo;
            myCommand.Parameters.Add(parameterCrtIntNo);


            //SqlParameter parameterCRComment = new SqlParameter("@CRComment", SqlDbType.VarChar, 255);
            //parameterCRComment.Value = crComment;
            //myCommand.Parameters.Add(parameterCRComment);

            SqlParameter parameterCRNumeric = new SqlParameter("@CRNumeric", SqlDbType.Int);
            parameterCRNumeric.Value = crNumeric;
            myCommand.Parameters.Add(parameterCRNumeric);

            //SqlParameter parameterCRDescr = new SqlParameter("@CRDescr", SqlDbType.VarChar, 100);
            //parameterCRDescr.Value = crDescr;
            //myCommand.Parameters.Add(parameterCRDescr);

            SqlParameter parameterCRString = new SqlParameter("@CRString", SqlDbType.VarChar, 25);
            parameterCRString.Value = crString;
            myCommand.Parameters.Add(parameterCRString);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterCRIntNo = new SqlParameter("@CRIntNo", SqlDbType.Int, 4);
            parameterCRIntNo.Value = crIntNo;
            parameterCRIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterCRIntNo);

            SqlParameter parameterWFRFCID = new SqlParameter("@WFRFCID", SqlDbType.Int, 4);
            parameterWFRFCID.Value = crIntNo;
            parameterWFRFCID.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterWFRFCID);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                //int CRIntNo = (int)myCommand.Parameters["@CRIntNo"].Value;
                int WFRFCID = (int)myCommand.Parameters["@WFRFCID"].Value;

                return WFRFCID;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

    }
}
