using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // ExclusionDetails Class
    //
    // A simple data class that encapsulates details about a particular
    // official vehicle registration no. exclusion 
    //
    //*******************************************************

    public partial class ExclusionDetails 
	{
		public Int32 ExcIntNo;
        public Int32 AutIntNo;
		public string ExcRegNo; 
		public string ExcOfficialCapacity;
        public DateTime ExcFromDate;
        public DateTime ExcToDate;
        public string LastUser;
    }

    //*******************************************************
    //
    // ExclusionListDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/update/delete excluded vehicel
    // registration no's within the TMS database.
    //
    //*******************************************************

    public partial class ExclusionListDB {

		string mConstr = "";

		public ExclusionListDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // ExclusionListDB.GetExclusionDetails() Method <a name="GetExclusionDetails"></a>
        //
        // The GetExclusionDetails method returns an ExclusionDetails
        // struct that contains information about a specific
        // excluded vehicle (reg no, official capacity etc.)
        //
        //*******************************************************

        public ExclusionDetails GetExclusionDetails(int excIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ExclusionDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterExcIntNo = new SqlParameter("@ExcIntNo", SqlDbType.Int, 4);
            parameterExcIntNo.Value = excIntNo;
            myCommand.Parameters.Add(parameterExcIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create ExclusionDetails Struct
			ExclusionDetails myExclusionDetails = new ExclusionDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
                //myExclusionDetails.ExcIntNo = Convert.ToInt32(result["ExcIntNo"]);
                //myExclusionDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
                myExclusionDetails.ExcRegNo = result["ExcRegNo"].ToString();
                myExclusionDetails.ExcOfficialCapacity = result["ExcOfficialCapacity"].ToString();
                if (result["ExcFromDate"].ToString().ToUpper() != "")
                    myExclusionDetails.ExcFromDate = Convert.ToDateTime(result["ExcFromDate"].ToString());
                if (result["ExcToDate"].ToString().ToUpper() != "")
                    myExclusionDetails.ExcToDate = Convert.ToDateTime(result["ExcToDate"].ToString());                
                myExclusionDetails.LastUser = result["LastUser"].ToString();
			}
			result.Close();
            return myExclusionDetails;
        }

        //*******************************************************
        //
        // ExclusionListDB.AddExclusion() Method <a name="AddExclusion"></a>
        //
        // The AddExclusion method inserts a new exclusion record
        // into the ExclusionList table.  A unique "ExcIntNo"
        // key is then returned from the method.  
        //
        //*******************************************************

        public int AddExclusion(int autIntNo, string excRegNo, string excOfficialCapacity,
            DateTime excFromDate, DateTime excToDate, string lastUser)
			
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("ExclusionAdd", myConnection);

            // Mark the Command as a SPROC
            com.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC"

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@ExcRegNo", SqlDbType.VarChar, 20).Value = excRegNo;
            com.Parameters.Add("@ExcOfficialCapacity", SqlDbType.VarChar, 255).Value = excOfficialCapacity;
            if (excFromDate != DateTime.MinValue)
                com.Parameters.Add("@ExcFromDate", SqlDbType.SmallDateTime, 8).Value = excFromDate;
            if (excToDate != DateTime.MaxValue)
                com.Parameters.Add("@ExcToDate", SqlDbType.SmallDateTime, 8).Value = excToDate;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@ExcIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;

            try {
                myConnection.Open();
                com.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the ExclusionID using Output Param from SPROC
                int excIntNo = (int)com.Parameters["@ExcIntNo"].Value;

                return excIntNo;
            }
            catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
                return 0;
            }
        }

        //public SqlDataReader GetExclusionList(int autIntNo, string search, string orderBy)
        public SqlDataReader GetExclusionList(int autIntNo, string search)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand com = new SqlCommand("ExclusionList", myConnection);

			// Mark the Command as a SPROC
			com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@Search", SqlDbType.VarChar, 20).Value = search;

            //SqlParameter parameterOrderBy = new SqlParameter("@OrderBy", SqlDbType.VarChar, 20);
            //parameterOrderBy.Value = orderBy;
            //myCommand.Parameters.Add(parameterOrderBy);
			
			// Execute the command
			myConnection.Open();
			SqlDataReader result = com.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}


        //public DataSet GetLocationListDS(int autIntNo, string search, string orderBy)
        public DataSet GetExclusionListDS(int autIntNo, string search)
		{
			SqlDataAdapter sqlDAExclusions = new SqlDataAdapter();
			DataSet dsExclusions = new DataSet();

			// Create Instance of Connection and Command Object
            sqlDAExclusions.SelectCommand = new SqlCommand();
            sqlDAExclusions.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAExclusions.SelectCommand.CommandText = "ExclusionList";
            
			// Mark the Command as a SPROC
            sqlDAExclusions.SelectCommand.CommandType = CommandType.StoredProcedure;

            sqlDAExclusions.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            sqlDAExclusions.SelectCommand.Parameters.Add("@Search", SqlDbType.VarChar, 20).Value = search;

            //SqlParameter parameterOrderBy = new SqlParameter("@OrderBy", SqlDbType.VarChar, 20);
            //parameterOrderBy.Value = orderBy;
            //sqlDALocs.SelectCommand.Parameters.Add(parameterOrderBy);

			// Execute the command and close the connection
            sqlDAExclusions.Fill(dsExclusions);
            sqlDAExclusions.SelectCommand.Connection.Dispose();

			// Return the dataset result
            return dsExclusions;		
		}

        public int UpdateExclusion(int autIntNo, string excRegNo, 
            string excOfficialCapacity, DateTime excFromDate, DateTime excToDate, string lastUser, int excIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand com = new SqlCommand("ExclusionUpdate", myConnection);

			// Mark the Command as a SPROC
			com.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@ExcRegNo", SqlDbType.VarChar, 20).Value = excRegNo;
            com.Parameters.Add("@ExcOfficialCapacity", SqlDbType.VarChar, 255).Value = excOfficialCapacity;
            if (excFromDate != DateTime.MinValue)
                com.Parameters.Add("@ExcFromDate", SqlDbType.SmallDateTime, 8).Value = excFromDate;
            if (excToDate != DateTime.MaxValue)
                com.Parameters.Add("@ExcToDate", SqlDbType.SmallDateTime, 8).Value = excToDate;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            com.Parameters.Add("@ExcIntNo", SqlDbType.Int, 4).Value = excIntNo;
            com.Parameters["@ExcIntNo"].Direction = ParameterDirection.InputOutput;
            
			try 
			{
				myConnection.Open();
				com.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the ExclusionID using Output Param from SPROC
				int ExcIntNo = (int)com.Parameters["@ExcIntNo"].Value;

				return ExcIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return 0;
			}
		}

		
		public String DeleteExclusion (int excIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand com = new SqlCommand("ExclusionDelete", myConnection);

			// Mark the Command as a SPROC
			com.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
            com.Parameters.Add("@ExcIntNo", SqlDbType.Int, 4).Value = excIntNo;
            com.Parameters["@ExcIntNo"].Direction = ParameterDirection.InputOutput;

			try 
			{
				myConnection.Open();
				com.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
                int ExcIntNo = (int)com.Parameters["@ExcIntNo"].Value;

				return ExcIntNo.ToString();
			}
			catch (Exception e)
			{
				myConnection.Dispose();
                string msg = e.Message;
				return String.Empty;
			}
		}
	}
}

