using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.IO;
using System.Net;
using System.Configuration;

namespace Stalberg.TMS
{
	/// <summary>
	/// Run general procedures and functions
	/// </summary>
	public partial class AccessDBMethods
	{
		string mConstr = "";

		public AccessDBMethods(string vConstr)
		{
			mConstr = vConstr;
		}

		public bool OpenDatabase(OleDbConnection cn, string provider, string fileName, string pwd)
		{
			// TODO: Modify the connection string and include any
			// additional required properties for your database.

			// get the physical path and filename of the invoice document
			// doing this effectively creates a string literal which correctly handles the \\ problem
			string source = @"Data source="+fileName;
			string access = @";Mode=Read;Persist Security Info=False;Jet OLEDB:Database Password="+pwd;

			cn.ConnectionString = @"Provider="+provider+";"+source+access;
			try
			{
				cn.Open();
				// Insert code to process data.
				return true;
			}
			catch (Exception e)
			{
				string msg = e.Message;
				return false;
			}

		}

		public void CloseDatabase(OleDbConnection cn)
		{
			cn.Dispose();
		}

		public OleDbDataReader GetInputFromARS(OleDbConnection cn, string provider, 
			string fileName, string pwd, string tableName)
		{
			//open database
			bool valid = OpenDatabase(cn, provider, fileName, pwd);

			if (!valid) return null;

			//set up sql query
			string sql = "";

			switch (tableName)
			{
				case "InputFromARS":
					sql = "SELECT * FROM InputFromARS ORDER BY ReferenceNo";
					break;
				case "ThumbnailImagesARS":
					sql = "SELECT * FROM ThumbnailImagesARS ORDER BY ARSIntNo";
					break;
			}
			// Create Instance of Connection and Command Object
			OleDbCommand myCommand = new OleDbCommand(sql, cn);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.Text;

			// Add Parameters to SPROC
//			SqlParameter parameterType = new SqlParameter("@Type", SqlDbType.Int);
//			parameterType.Value = type;
//			myCommand.Parameters.Add(parameterType);

			// Execute the command
			try
			{
				OleDbDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
				// Return the datareader result
				return result;
			}
			catch (Exception e)
			{
				myCommand.Connection.Dispose();
				string msg = e.Message;
				return null;
			}
			
		}

		public void CopyImage (string fileName, OleDbDataReader reader, int colIndex)
		{
			FileStream fs;                          // Writes the BLOB to a file (*.bmp).
			BinaryWriter bw;                        // Streams the BLOB to the FileStream object.
			
			int bufferSize = 100;                   // Size of the BLOB buffer.
			byte[] outbyte = new byte[bufferSize];  // The BLOB byte[] buffer to be filled by GetBytes.
			long retval;                            // The bytes returned from GetBytes.
			long startIndex = 0;                    // The starting position in the BLOB output.

			fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write);

			bw = new BinaryWriter(fs);

			// Reset the starting byte for the new BLOB.
			startIndex = 0;

			// Read the bytes into outbyte[] and retain the number of bytes returned.
			retval = reader.GetBytes(colIndex, startIndex, outbyte, 0, bufferSize);

			// Continue reading and writing while there are bytes beyond the size of the buffer.
			while (retval == bufferSize)
			{
				bw.Write(outbyte);
				bw.Flush();

				// Reposition the start index to the end of the last buffer and fill the buffer.
				startIndex += bufferSize;
				retval = reader.GetBytes(colIndex, startIndex, outbyte, 0, bufferSize);
			}

			// Write the remaining buffer.
			if (retval != 0)		//if the buffer is empty, just flush it!
				bw.Write(outbyte, 0, (int)retval - 1);
			
			bw.Flush();

			// Close the output file.
			bw.Close();
			fs.Close();
		}

        //dls 081023 - need to return the length of the image so that we can verify that it was not corrupt when loaded into the database

        //public void UpdateBLOB(string tblName, string colName, string idColName, int idValue, string photoFilePath, string colType)
        // 2013-07-19 comment by Henry for useless
        //public bool UpdateBLOB(string tblName, string colName, string idColName, int idValue, string photoFilePath, 
        //    string colType, ref Int32 fileLength, ref string errMessage)
        //{
        //    bool failed = false;

        //    byte[] photo;
        //    string text = "";
        //    //int fileLength = 0;

        //    SqlConnection myConnection = new SqlConnection(mConstr);

        //    SqlCommand lsql = new SqlCommand("UPDATE "+tblName+" WITH (ROWLOCK) SET "+colName+" = @Photo WHERE "+idColName+" = @IdValue", myConnection); 

        //    lsql.Parameters.Add("@IdValue",  SqlDbType.Int).Value = idValue;

        //    SqlDbType lDBType = new SqlDbType();

        //    switch (colType)
        //    {
        //        case "image":
        //        {
        //            lDBType = SqlDbType.Image;
        //            photo  = GetPhoto(photoFilePath, ref fileLength);
        //            lsql.Parameters.Add("@Photo", lDBType, fileLength).Value = photo;
        //            break;
        //        }
        //        case "binary":
        //        {
        //            lDBType = SqlDbType.Binary;
        //            photo  = GetPhoto(photoFilePath, ref fileLength);
        //            lsql.Parameters.Add("@Photo", lDBType, fileLength).Value = photo;
        //            break;
        //        }
        //        case "text":
        //        {
        //            lDBType = SqlDbType.Text;
        //            text = GetText(photoFilePath, ref fileLength);
        //            lsql.Parameters.Add("@Photo", lDBType, fileLength).Value = text;
        //            break;
        //        }
        //        case "varchar":
        //        {
        //            lDBType = SqlDbType.VarChar;
        //            text = GetText(photoFilePath, ref fileLength);
        //            lsql.Parameters.Add("@Photo", lDBType, fileLength).Value = text;
        //            break;
        //        }
        //    }

        //    try
        //    {
        //        myConnection.Open();
        //        lsql.ExecuteNonQuery();
        //        failed = false;
        //    }
        //    catch (Exception e)
        //    {
        //        errMessage = e.Message;
        //        failed = true;
        //    }
        //    finally
        //    {
        //        myConnection.Dispose();
        //    }

        //    return failed;
        //}

		public byte[] GetPhoto(string filePath, ref int fileLength)
		{
            try
            {
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);

                byte[] photo = br.ReadBytes((int)fs.Length);

                fileLength = Convert.ToInt32(fs.Length);

                br.Close();
                fs.Close();
                return photo;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return null;
            }
			
		}

		public string GetText(string filePath, ref int fileLength)
		{
			FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
			StreamReader sr = new StreamReader(fs);

			string text = sr.ReadToEnd();

			fileLength = Convert.ToInt32(fs.Length);
			sr.Close();
			fs.Close();

			return text;
		}
        public byte[] DownLoadBLOB(int primaryIDVal, string primaryIDKey, string tableName, string columnName)
        {
            byte[] byteArray = null;
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand lsql = new SqlCommand("SELECT " + columnName + " FROM " + tableName + " WITH (NOLOCK) WHERE " + primaryIDKey + " = " + primaryIDVal, myConnection);
            myConnection.Open();
            SqlDataReader reader = lsql.ExecuteReader(CommandBehavior.CloseConnection);
            if (reader.Read())
            {
                byteArray = (byte[])reader[columnName];

            }
            return byteArray;
        }
        public void DownLoadBLOB(int primaryIDVal, string primaryIDKey, string tableName, string columnName, ref string FileName, string serverPath)
        {
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand lsql = new SqlCommand("SELECT " + columnName + " FROM " + tableName + " WITH (NOLOCK) WHERE " + primaryIDKey + " = " + primaryIDVal, myConnection);
            
            FileStream fs;                          // Writes the BLOB to a file (*.bmp).
            BinaryWriter bw;                        // Streams the BLOB to the FileStream object.

            int bufferSize = 100;                   // Size of the BLOB buffer.
            byte[] outbyte = new byte[bufferSize];  // The BLOB byte[] buffer to be filled by GetBytes.
            long retval = 0;                            // The bytes returned from GetBytes.
            long startIndex = 0;                    // The starting position in the BLOB output.


            // Create a file to hold the output.
            //				Uri baseUri = new Uri("http://localhost/BrokerWorld/Downloads/");
            //				Uri myUri = new Uri(baseUri, lFileName);

            fs = new FileStream(serverPath + "\\" + FileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);

            bw = new BinaryWriter(fs);

            // Reset the starting byte for the new BLOB.
            startIndex = 0;

            // Create a new WebClient instance.
            WebClient myWebClient = new WebClient();

            // Open the connection and read data into the DataReader.
            myConnection.Open();

            SqlDataReader myReader = lsql.ExecuteReader(CommandBehavior.SequentialAccess);

            while (myReader.Read())
            {
                 try
                    {
                        // Read the bytes into out-byte[] and retain the number of bytes returned.
                        retval = myReader.GetBytes(0, startIndex, outbyte, 0, bufferSize);

                        // Continue reading and writing while there are bytes beyond the size of the buffer.
                        while (retval == bufferSize)
                        {
                            bw.Write(outbyte);
                            bw.Flush();

                            // Reposition the start index to the end of the last buffer and fill the buffer.
                            startIndex += bufferSize;
                            retval = myReader.GetBytes(0, startIndex, outbyte, 0, bufferSize);
                        }
                        myConnection.Close();
                    }
                    catch (Exception e)
                    {
                        string msg = "No Image available " + e.Message;
                        // Close the output file.
                        bw.Close();
                        fs.Close();
                        fs.Dispose();
                        // Close the reader and the connection.
                        myReader.Close();
                        myConnection.Close();
                        return;
                    }
            }// while	

            // Write the remaining buffer.
            if (retval != 0)		//if the buffer is empty, just flush it!
            {
                bw.Write(outbyte, 0, (int)retval - 1);
            }

            // Close the output file.
            bw.Close();
            fs.Close();
            // Close the reader and the connection.
            myReader.Close();
            myConnection.Close();
        }

        public string DownLoadText (int primaryIDVal, string primaryIDField, string tableName, string columnName)
		{
			string text = "";

			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand lsql = new SqlCommand("SELECT "+columnName+" FROM "+tableName+" WITH (NOLOCK) WHERE "+primaryIDField+" = "+ primaryIDVal, myConnection);

            try
            {
                // Open the connection and read data into the DataReader.
                myConnection.Open();
                SqlDataReader myReader = lsql.ExecuteReader(CommandBehavior.SequentialAccess);

                while (myReader.Read())
                {
                    // Get the publisher id, which must occur before getting the logo.
                    text = myReader.GetString(0);
                }

                // Close the reader and the connection.
                myReader.Close();
            }
            finally
            {
                myConnection.Close();
            }

			return text;
		}


	}
}
