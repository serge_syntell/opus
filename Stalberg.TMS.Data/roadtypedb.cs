using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // RoadTypeDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class RoadTypeDetails 
	{
        public int RdTypeCode;
		public string RdTypeDescr;
    }

    //*******************************************************
    //
    // RoadTypeDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query RoadTypes within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class RoadTypeDB {

		string mConstr = "";

		public RoadTypeDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // RoadTypeDB.GetRoadTypeDetails() Method <a name="GetRoadTypeDetails"></a>
        //
        // The GetRoadTypeDetails method returns a RoadTypeDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public RoadTypeDetails GetRoadTypeDetails(int rdTIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RoadTypeDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterRdTIntNo = new SqlParameter("@RdTIntNo", SqlDbType.Int, 4);
            parameterRdTIntNo.Value = rdTIntNo;
            myCommand.Parameters.Add(parameterRdTIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			RoadTypeDetails myRoadTypeDetails = new RoadTypeDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myRoadTypeDetails.RdTypeCode = Convert.ToInt32(result["RdTypeCode"]);
				myRoadTypeDetails.RdTypeDescr = result["RdTypeDescr"].ToString();
			}
			result.Close();
            return myRoadTypeDetails;
        }

        //*******************************************************
        //
        // RoadTypeDB.AddRoadType() Method <a name="AddRoadType"></a>
        //
        // The AddRoadType method inserts a new menu record
        // into the menus database.  A unique "RoadTypeId"
        // key is then returned from the method.  
        //
        //*******************************************************
        // 2013-07-26 add parameter lastUser by Henry
        public int AddRoadType (int rdTypeCode, string rdTypeDescr, string lastUser) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RoadTypeAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
			SqlParameter parameterRdTypeCode = new SqlParameter("@RdTypeCode", SqlDbType.Int, 4);
			parameterRdTypeCode.Value = rdTypeCode;
			myCommand.Parameters.Add(parameterRdTypeCode);

			SqlParameter parameterRdTypeDescr = new SqlParameter("@RdTypeDescr", SqlDbType.VarChar, 30);
			parameterRdTypeDescr.Value = rdTypeDescr;
			myCommand.Parameters.Add(parameterRdTypeDescr);

            SqlParameter parameterRdTIntNo = new SqlParameter("@RdTIntNo", SqlDbType.Int, 4);
            parameterRdTIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterRdTIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int rdTIntNo = Convert.ToInt32(parameterRdTIntNo.Value);

                return rdTIntNo;
            }
            catch {
				myConnection.Dispose();
                return 0;
            }
        }

		public SqlDataReader GetRoadTypeList()
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("RoadTypeList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		
		public DataSet GetRoadTypeListDS()
		{
			SqlDataAdapter sqlDARoadTypes = new SqlDataAdapter();
			DataSet dsRoadTypes = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDARoadTypes.SelectCommand = new SqlCommand();
			sqlDARoadTypes.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDARoadTypes.SelectCommand.CommandText = "RoadTypeList";

			// Mark the Command as a SPROC
			sqlDARoadTypes.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Execute the command and close the connection
			sqlDARoadTypes.Fill(dsRoadTypes);
			sqlDARoadTypes.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsRoadTypes;		
		}

		// 2013-07-26 add parameter lastUser by Henry
		public int UpdateRoadType (int rdTIntNo, int rdTypeCode, string rdTypeDescr, string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("RoadTypeUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterRdTypeCode = new SqlParameter("@RdTypeCode", SqlDbType.Int, 4);
			parameterRdTypeCode.Value = rdTypeCode;
			myCommand.Parameters.Add(parameterRdTypeCode);

			SqlParameter parameterRdTypeDescr = new SqlParameter("@RdTypeDescr", SqlDbType.VarChar, 30);
			parameterRdTypeDescr.Value = rdTypeDescr;
			myCommand.Parameters.Add(parameterRdTypeDescr);

			SqlParameter parameterRdTIntNo = new SqlParameter("@RdTIntNo", SqlDbType.Int);
			parameterRdTIntNo.Direction = ParameterDirection.InputOutput;
			parameterRdTIntNo.Value = rdTIntNo;
			myCommand.Parameters.Add(parameterRdTIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int RdTIntNo = (int)myCommand.Parameters["@RdTIntNo"].Value;
				//int menuId = (int)parameterRdTIntNo.Value;

				return RdTIntNo;
			}
			catch 
			{
				myConnection.Dispose();
				return 0;
			}
		}

		
		public String DeleteRoadType (int rdTIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("RoadTypeDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterRdTIntNo = new SqlParameter("@RdTIntNo", SqlDbType.Int, 4);
			parameterRdTIntNo.Value = rdTIntNo;
			parameterRdTIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterRdTIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int RdTIntNo = (int)parameterRdTIntNo.Value;

				return RdTIntNo.ToString();
			}
			catch 
			{
				myConnection.Dispose();
				return String.Empty;
			}
		}
	}
}

