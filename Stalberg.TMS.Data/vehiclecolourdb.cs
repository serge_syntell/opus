using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{


    //*******************************************************
    //
    // VehicleColourDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query VehicleColours within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class VehicleColourDB {

		string mConstr = "";

		public VehicleColourDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // VehicleTypeDB.GetVehicleColours() Method <a name="GetVehicleColours"></a>
        //
        // The GetVehicleColours method returns a VehicleColours
        //
        //*******************************************************

        public SqlDataReader GetVehicleColours() 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("VehicleColoursGet", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            return result;
        }

       

        //*******************************************************
        //
        // VehicleColourDB.AddVehicleColour() Method <a name="AddVehicleColour"></a>
        //
        // The AddVehicleColour method inserts a new menu record
        //
        //*******************************************************

        public int AddVehicleColour(string sCode, string sDescr , string sLastUser) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("VehicleColourAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterVCIntNo = new SqlParameter("@VCIntNo", SqlDbType.Int, 4);
            parameterVCIntNo.Value = 0;
            parameterVCIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterVCIntNo);

            // Add Parameters to SPROC
            SqlParameter paramCode = new SqlParameter("@VCCode", SqlDbType.VarChar, 3);
            paramCode.Value = sCode;
            myCommand.Parameters.Add(paramCode);

			SqlParameter paramColour = new SqlParameter("@VCDescr", SqlDbType.VarChar, 30);
            paramColour.Value = sDescr;
            myCommand.Parameters.Add(paramColour);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = sLastUser;
			myCommand.Parameters.Add(parameterLastUser);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int vtIntNo = Convert.ToInt32(parameterVCIntNo.Value);

                return vtIntNo;
            }
            catch (Exception e)
			{
				string msg = e.Message;
				myConnection.Dispose();
                return 0;
            }
        }

        public int UpdateVehicleColour( string sCode, string sDescr, string sLastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("VehicleColourUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC

            SqlParameter parameterVCIntNo = new SqlParameter("@VCIntNo", SqlDbType.Int, 4);
            parameterVCIntNo.Value = 0;
            parameterVCIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterVCIntNo);

            SqlParameter paramCode = new SqlParameter("@VCCode", SqlDbType.VarChar, 3);
            paramCode.Value = sCode;
            myCommand.Parameters.Add(paramCode);

            SqlParameter paramColour = new SqlParameter("@VCDescr", SqlDbType.VarChar, 30);
            paramColour.Value = sDescr;
            myCommand.Parameters.Add(paramColour);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = sLastUser;
            myCommand.Parameters.Add(parameterLastUser);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int VCIntNo = (int)myCommand.Parameters["@VCIntNo"].Value;
				//int menuId = (int)parameterVTIntNo.Value;

				return VCIntNo;
			}
			catch 
			{
				myConnection.Dispose();
				return -1;
			}
		}
		
		public int DeleteVehicleColour (string sVCCode)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("VehicleColourDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterVCIntNo = new SqlParameter("@VCIntNo", SqlDbType.Int, 4);
            parameterVCIntNo.Value = 0;
			parameterVCIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterVCIntNo);

            SqlParameter parameterVCCode = new SqlParameter("@VCCode", SqlDbType.VarChar,3 );
            parameterVCCode.Value = sVCCode;
            myCommand.Parameters.Add(parameterVCCode);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int VCIntNo = (int)parameterVCIntNo.Value;

                return VCIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return -1;
			}
		}
    
    }
}

