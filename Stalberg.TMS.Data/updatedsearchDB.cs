using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Stalberg.TMS
{
   
    /// <summary>
    /// Business/Data Logic Class that encapsulates all data logic 
    /// </summary>
    public class UpdateSearchDB
    {
        // Fields
        string mConstr = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="CourtDB"/> class.
        /// </summary>
        /// <param name="vConstr">The v constr.</param>
        public UpdateSearchDB(string vConstr)
        {
            mConstr = vConstr;
        }


        //******************************************************************************************

        public int UpdateSearchID(ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("InsertSearchID", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            myCommand.CommandTimeout = 0;

            myCommand.Parameters.Add("@ErrorCode", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();

                int success = int.Parse(myCommand.Parameters["@ErrorCode"].Value.ToString());

                return success;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return -10;

            }
            finally
            {
                myConnection.Dispose();
            }

        }

        public int UpdateSearchSurName(ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("InsertSearchSurName", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            myCommand.Parameters.Add("@ErrorCode", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;
            myCommand.CommandTimeout = 0;

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();

                int success = int.Parse(myCommand.Parameters["@ErrorCode"].Value.ToString());

                return success;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                return -10;

            }
            finally
            {
                myConnection.Dispose();
            }

        }
    }
  }