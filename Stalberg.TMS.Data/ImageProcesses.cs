using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Collections.Generic;
using System.Drawing.Imaging;

namespace Stalberg.TMS
{
    public class ImageProcesses
    {
        private readonly string connectionString = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageProcesses"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public ImageProcesses(string connectionString)
        {
            this.connectionString = connectionString;
        }

        //dls 090130 - this will be used to get the auth rule relating to compressed images
        /// <summary>
        /// Gets the database connection string.
        /// </summary>
        /// <value>The connection string.</value>
        public string ConnectionString
        {
            get { return this.connectionString; }
        }

        public string GetBase64StringFromDB(int primaryIDVal, string primaryIDKey, string tableName, string columnName)
        {
            SqlConnection myConnection = new SqlConnection(connectionString);
            string lsql = "SELECT " + columnName + ", HasData = CASE WHEN DATALENGTH(" + columnName + ") = 0 THEN 0 ELSE 1 END FROM "
                + tableName + " WITH (NOLOCK) WHERE " + primaryIDKey + " = " + primaryIDVal;

            SqlCommand myCommand = new SqlCommand(lsql, myConnection);

            myConnection.Open();

            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            string data = string.Empty;

            while (result.Read())
            {
                data = result[columnName].ToString();
            }
            result.Close();

            return data;
        }

        public byte[] GetImageFromDB(int primaryIDVal, string primaryIDKey, string tableName, string columnName)
        {
            SqlConnection myConnection = new SqlConnection(connectionString);
            string sql = "SELECT " + columnName + ", HasData = CASE WHEN DATALENGTH(" + columnName + ") = 0 THEN 0 ELSE 1 END FROM "
                + tableName + " WITH (NOLOCK) WHERE " + primaryIDKey + " = " + primaryIDVal;

            SqlCommand myCommand = new SqlCommand(sql, myConnection);
            myCommand.CommandTimeout = 300;

            myConnection.Open();

            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            byte[] buffer = null;
            if (result != null)
            {
                while (result.Read())
                {
                    //if (!result[columnName].Equals(System.DBNull.Value))
                    if (result.GetInt32(1) == 1 && !result.IsDBNull(0))
                    {
                        buffer = (byte[])result[columnName];
                    }
                }
                result.Close();
            }

            return buffer;
        }

        public bool SaveImageToDB(int primaryIDVal, string primaryIDKey, string tableName, string columnName, byte[] imageData, ref string errMsg)
        {
            SqlConnection myConnection = new SqlConnection(connectionString);
            string lsql = "UPDATE " + tableName + " WITH (ROWLOCK) SET " + columnName + " = @Image WHERE " + primaryIDKey + " = " + primaryIDVal;

            SqlCommand myCommand = new SqlCommand(lsql, myConnection);

            SqlParameter myParameter = new SqlParameter("@Image", SqlDbType.Image, imageData.Length);
            myParameter.Value = imageData;
            myCommand.Parameters.Add(myParameter);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                return true;
            }
            catch (Exception err)
            {
                myConnection.Dispose();
                errMsg = err.Message;
                return false;
            }
        }

        public bool SaveImageAsBase64ToDB(int primaryIDVal, string primaryIDKey, string tableName, string columnName, string imageData, ref string errMsg)
        {
            SqlConnection myConnection = new SqlConnection(connectionString);
            string lsql = "UPDATE " + tableName + " WITH (ROWLOCK) SET " + columnName + " = @Image WHERE " + primaryIDKey + " = " + primaryIDVal;

            SqlCommand myCommand = new SqlCommand(lsql, myConnection);

            SqlParameter myParameter = new SqlParameter("@Image", SqlDbType.VarChar, imageData.Length);
            myParameter.Value = imageData;
            myCommand.Parameters.Add(myParameter);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                return true;
            }
            catch (Exception err)
            {
                myConnection.Dispose();
                errMsg = err.Message;
                return false;
            }
        }

        public byte[] CompressAndSaveImageToDB(int autIntNo, byte[] imgToConvert, string imgType, int percCompression, string LastUser, int primaryIDVal, string primaryIDKey, string tableName, string columnName, ref string errMsg)
        {
            //BD new class to process an image
            //1. Need to compress the image to a size that is small enough based on required auth size

            //need to pass in width and height of required image - ok, just need to pass in autintno as
            //width and height will be in a auth rule.
            // Auth rule will be dependant on img type
            int sourceWidth = 0;
            int sourceHeight = 0;

            float sourceWResolution = 0;
            float sourceHResolution = 0;

            int authWidth = 48;
            int authHeight = 35;

            int requiredWidth = 0;
            int requiredHeight = 0;

                //string imgBase64 = "";

            float nPercent = 0;

            int requiredSizeInKB = 0;

            byte[] compressedBytes = null;

            AuthorityRulesDetails ard = new AuthorityRulesDetails();

            //int requiredSizeInKB - needs to also be an auth rule
            ard.AutIntNo = autIntNo;
            ard.ARCode = "2440";
            ard.LastUser = LastUser;
            DefaultAuthRules authRule2440 = new DefaultAuthRules(ard, connectionString);
            KeyValuePair<int, string> value2440 = authRule2440.SetDefaultAuthRule();
            requiredSizeInKB = ard.ARNumeric;

            if (percCompression == 0)
            {

                //check if we are dealing with the vehicle image or the license plate image
                if (imgType == "A" || imgType == "B")
                {
                    ard.AutIntNo = autIntNo;
                    ard.ARCode = "2400";
                    ard.LastUser = LastUser;
                    DefaultAuthRules authRule2400 = new DefaultAuthRules(ard, connectionString);
                    KeyValuePair<int, string> value2400 = authRule2400.SetDefaultAuthRule();
                    authWidth = ard.ARNumeric;

                    ard.AutIntNo = autIntNo;
                    ard.ARCode = "2410";
                    ard.LastUser = LastUser;
                    DefaultAuthRules authRule2410 = new DefaultAuthRules(ard, connectionString);
                    KeyValuePair<int, string> value2410 = authRule2410.SetDefaultAuthRule();
                    authHeight = ard.ARNumeric;
                }
                else if (imgType == "R")
                {
                    ard.AutIntNo = autIntNo;
                    ard.ARCode = "2420";
                    ard.LastUser = LastUser;
                    DefaultAuthRules authRule2420 = new DefaultAuthRules(ard, connectionString);
                    KeyValuePair<int, string> value2420 = authRule2420.SetDefaultAuthRule();
                    authWidth = ard.ARNumeric;

                    ard.AutIntNo = autIntNo;
                    ard.ARCode = "2430";
                    ard.LastUser = LastUser;
                    DefaultAuthRules authRule2430 = new DefaultAuthRules(ard, connectionString);
                    KeyValuePair<int, string> value2430 = authRule2430.SetDefaultAuthRule();
                    authHeight = ard.ARNumeric;
                }
                requiredWidth = 0;
                requiredHeight = 0;

                MemoryStream ms_source = new MemoryStream(imgToConvert);
                Image sourceImg = new Bitmap(ms_source);

                sourceWidth = sourceImg.Width;
                sourceHeight = sourceImg.Height;

                //pixels per inch
                sourceHResolution = sourceImg.VerticalResolution;
                sourceWResolution = sourceImg.HorizontalResolution;

                authHeight = Convert.ToInt32((authHeight / 25.4) * sourceHResolution);
                authWidth = Convert.ToInt32((authWidth / 25.4) * sourceWResolution);

                requiredWidth = authWidth;
                requiredHeight = authHeight;
                //requiredHeight = Convert.ToInt32((requiredWidth * sourceHeight) / sourceWidth);
                //if (authHeight < requiredHeight)
                //{
                //    //need to adjust heights/widths so that both fit.
                //    requiredHeight = authHeight;
                //    requiredWidth = Convert.ToInt32((requiredHeight * sourceWidth) / sourceHeight);
                //}



                Bitmap compressedImg = new Bitmap(sourceImg, new Size(requiredWidth, requiredHeight));
                MemoryStream ms = new MemoryStream();
                compressedImg.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

                compressedBytes = ms.ToArray();
            }
            else
            {
                MemoryStream ms_source = new MemoryStream(imgToConvert);
                Image sourceImg = new Bitmap(ms_source);

                sourceWidth = sourceImg.Width;
                sourceHeight = sourceImg.Height;

                nPercent = ((float)(100 - percCompression) / 100);
                requiredWidth = (int)(sourceWidth * nPercent);
                requiredHeight = (int)(sourceHeight * nPercent);

                Bitmap compressedImg = new Bitmap(sourceImg, new Size(requiredWidth, requiredHeight));

                MemoryStream ms = new MemoryStream();
                compressedImg.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

                compressedBytes = ms.ToArray();
            }

            //Must save byte[] to DB
            SqlConnection myConnection = new SqlConnection(connectionString);
            string lsql = "UPDATE " + tableName + " WITH (ROWLOCK) SET " + columnName + " = @Image WHERE " + primaryIDKey + " = " + primaryIDVal;

            SqlCommand myCommand = new SqlCommand(lsql, myConnection);

            SqlParameter myParameter = new SqlParameter("@Image", SqlDbType.Image, compressedBytes.Length);
            myParameter.Value = compressedBytes;
            myCommand.Parameters.Add(myParameter);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                return compressedBytes;
            }
            catch (Exception err)
            {
                myConnection.Dispose();
                errMsg = err.Message;
                return null;
            }

        }

        public string ImageToBase64(int autIntNo, byte[] ConvertedImage, string LastUser)
        {
            int sourceWidth = 0;
            int sourceHeight = 0;

            int requiredWidth = 0;
            int requiredHeight = 0;

            int percCompression;

            string imgBase64 = "";

            float nPercent = 0;
            int requiredSizeInKB = 0;

            AuthorityRulesDetails ard = new AuthorityRulesDetails();

            //int requiredSizeInKB - needs to also be an auth rule
            ard.AutIntNo = autIntNo;
            ard.ARCode = "2440";
            ard.LastUser = LastUser;
            DefaultAuthRules authRule2440 = new DefaultAuthRules(ard, connectionString);
            KeyValuePair<int, string> value2440 = authRule2440.SetDefaultAuthRule();
            requiredSizeInKB = ard.ARNumeric;

            imgBase64 = Convert.ToBase64String(ConvertedImage, Base64FormattingOptions.None);


            //need to check that my out put string is less than the required size
            for (int i = 0; i <= 100; i++)
            {
                if ((imgBase64.Length / 1000) > requiredSizeInKB)
                {
                    percCompression = i + 10;

                    MemoryStream ms_source = new MemoryStream(ConvertedImage);
                    Image sourceImg = new Bitmap(ms_source);

                    sourceWidth = sourceImg.Width;
                    sourceHeight = sourceImg.Height;

                    nPercent = ((float)(100 - percCompression) / 100);
                    requiredWidth = (int)(sourceWidth * nPercent);
                    requiredHeight = (int)(sourceHeight * nPercent);

                    Bitmap compressedImg = new Bitmap(sourceImg, new Size(requiredWidth, requiredHeight));

                    MemoryStream ms = new MemoryStream();
                    compressedImg.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

                    byte[] compressedBytes = ms.ToArray();
                    imgBase64 = Convert.ToBase64String(compressedBytes, Base64FormattingOptions.None);
                }
                else
                {
                    return imgBase64;
                }
            }

            return imgBase64;


        }

        public byte[] CompressImage(byte[] imgToConvert, int requiredSizeInKB)
        {
            //BD new class to process an image
            //1. Need to compress the image to a size that is small enough based on required auth size

            //need to pass in width and height of required image - ok, just need to pass in autintno as
            //width and height will be in a auth rule.
            // Auth rule will be dependant on img type

            float sourceWResolution = 0;
            float sourceHResolution = 0;

            int authWidth = 48;
            int authHeight = 35;

            int requiredWidth = 0;
            int requiredHeight = 0;

            byte[] compressedBytes = null;

            MemoryStream ms_source = new MemoryStream(imgToConvert);
            Image sourceImg = new Bitmap(ms_source);

            //pixels per inch
            sourceHResolution = sourceImg.VerticalResolution;
            sourceWResolution = sourceImg.HorizontalResolution;

            authHeight = Convert.ToInt32((authHeight / 25.4) * sourceHResolution);
            authWidth = Convert.ToInt32((authWidth / 25.4) * sourceWResolution);

            requiredWidth = authWidth;
            requiredHeight = authHeight;
            //requiredHeight = Convert.ToInt32((requiredWidth * sourceHeight) / sourceWidth);

            Bitmap compressedImg = new Bitmap(sourceImg, new Size(requiredWidth, requiredHeight));
            MemoryStream ms = new MemoryStream();
            compressedImg.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

            compressedBytes = ms.ToArray();

            return compressedBytes;
        }

        private bool CheckImage(byte[] data)
        {
            bool bRe = true;
            System.Drawing.Image Bitimage = null;
            try
            {
                using (MemoryStream ms = new MemoryStream(data))
                {
                    Bitimage = System.Drawing.Image.FromStream(ms);
                }
            }
            catch
            {
                bRe = false;
            }
            finally
            {
                if (Bitimage != null)
                    Bitimage.Dispose();
            }
            return bRe;
        }

        public int UpdateImageSetting(Int64 ScImIntNo, Decimal Contrast, Decimal Brightness, string lastUser, int frameIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("ImageSettingUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@ScImIntNo", SqlDbType.Int, 4).Value = ScImIntNo;
            myCommand.Parameters.Add("@FrameIntNo", SqlDbType.Int, 4).Value = frameIntNo;
            myCommand.Parameters.Add("@Contrast", SqlDbType.Real).Value = Contrast;
            myCommand.Parameters.Add("@Brightness", SqlDbType.Real).Value = Brightness;
            myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                myConnection.Open();
                return myCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        //Move from image.apsx.cs,  become common function
        static public void Contrast(Bitmap bitmap, float contrastFactor)
        {
            Graphics g = Graphics.FromImage(bitmap);
            ImageAttributes imageAttributes = new ImageAttributes();

            ColorMatrix cm = new ColorMatrix(new float[][]{  new float[]{contrastFactor,0f,0f,0f,0f},
                              new float[]{0f,contrastFactor,0f,0f,0f},
                              new float[]{0f,0f,contrastFactor,0f,0f},
                              new float[]{0f,0f,0f,1f,0f},
                              //including the BLATANT FUDGE
                              new float[]{0.001f,0.001f,0.001f,0f,1f}});

            imageAttributes.SetColorMatrix(cm);
            g.DrawImage(bitmap, new Rectangle(0, 0, bitmap.Width, bitmap.Height), 0, 0, bitmap.Width, bitmap.Height, GraphicsUnit.Pixel, imageAttributes);

            g.Dispose();
            imageAttributes.Dispose();
        }

        static public void BrightNess(Bitmap bitmap, float brightnessLevel)
        {
            Graphics g = Graphics.FromImage(bitmap);
            ImageAttributes imageAttributes = new ImageAttributes();

            ColorMatrix cm = new ColorMatrix(new float[][]{  
                              new float[]{1f,0f,0f,0f,0f},
                              new float[]{0f,1f,0f,0f,0f},
                              new float[]{0f,0f,1f,0f,0f},
                              new float[]{0f,0f,0f,1f,0f},
                              new float[]{brightnessLevel,
                                  brightnessLevel,brightnessLevel,1f,1f}});

            imageAttributes.SetColorMatrix(cm);
            g.DrawImage(bitmap, new Rectangle(0, 0, bitmap.Width, bitmap.Height), 0, 0, bitmap.Width, bitmap.Height, GraphicsUnit.Pixel, imageAttributes);

            g.Dispose();
            imageAttributes.Dispose();
        }

        public byte[] ProcessImage(string imagePath)
        {
            byte[] imageData = null;

            using (FileStream fs = File.OpenRead(imagePath))
            {
                imageData = new byte[fs.Length];
                fs.Read(imageData, 0, imageData.Length);
            }            

            return imageData;
        }
    }
}
