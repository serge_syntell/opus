﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Stalberg.TMS.Data
{
    public class LocationCameraSection
    {
        // Fields
        private int _LCSIntNo;
        private int _LocIntNo;
        private int _CameraIntNo1;
        private int _CameraIntNo2;
        private string _LCSSectionCode;
        private string _LCSSectionName;
        private int _LCSSectionDistance;
        private bool _LCSActive;
        private DateTime? _LCSStartDate;
        private DateTime? _LCSEndDate;

        private string _CamSerialNo1;
        private string _CamSerialNo2;

        public int LCSIntNo
        {
            get { return _LCSIntNo; }
            set { _LCSIntNo = value; }
        }
        
        public int LocIntNo
        {
            get { return _LocIntNo; }
            set { _LocIntNo = value; }
        }
        
        public int CameraIntNo1
        {
            get { return _CameraIntNo1; }
            set { _CameraIntNo1 = value; }
        }
       
        public int CameraIntNo2
        {
            get { return _CameraIntNo2; }
            set { _CameraIntNo2 = value; }
        }  

        public string LCSSectionCode
        {
            get { return _LCSSectionCode; }
            set { _LCSSectionCode = value; }
        }

        public string LCSSectionName
        {
            get { return _LCSSectionName; }
            set { _LCSSectionName = value; }
        }
        
        public int LCSSectionDistance
        {
            get { return _LCSSectionDistance; }
            set { _LCSSectionDistance = value; }
        }
        
        public bool LCSActive
        {
            get { return _LCSActive; }
            set { _LCSActive = value; }
        }

        public DateTime? LCSStartDate
        {
            get { return _LCSStartDate; }
            set { _LCSStartDate = value; }
        }
        
        public DateTime? LCSEndDate
        {
            get { return _LCSEndDate; }
            set { _LCSEndDate = value; }
        }

        public string CamSerialNo1
        {
            get { return _CamSerialNo1; }
            set { _CamSerialNo1 = value; }
        }

        public string CamSerialNo2
        {
            get { return _CamSerialNo2; }
            set { _CamSerialNo2 = value; }
        }  
    }

    public class LocationCameraSectionDB
    {
        // Fields
        private SqlConnection con;

        public LocationCameraSectionDB(string connectionString)
        {
            this.con = new SqlConnection(connectionString);
        }

        public List<LocationCameraSection> ListLocationCameraSections(int LocIntNo)
        {
            List<LocationCameraSection> list = new List<LocationCameraSection>();
            LocationCameraSection temp;

            SqlCommand cmd = new SqlCommand("LocationCameraSectionList", this.con);
            cmd.Parameters.Add("@LocIntNo", SqlDbType.Int).Value = LocIntNo;
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                this.con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    temp = new LocationCameraSection();
                    temp.LCSIntNo = Convert.ToInt32(reader["LCSIntNo"]);
                    temp.LocIntNo = Convert.ToInt32(reader["LocIntNo"]);
                    temp.CameraIntNo1 = Convert.ToInt32(reader["CameraIntNo1"]);
                    temp.CameraIntNo2 = Convert.ToInt32(reader["CameraIntNo2"]);
                    temp.LCSSectionCode = Convert.ToString(reader["LCSSectionCode"]);
                    temp.LCSSectionName = Convert.ToString(reader["LCSSectionName"]);
                    temp.LCSSectionDistance = Convert.ToInt32(reader["LCSSectionDistance"]);
                    temp.LCSActive = Convert.ToBoolean(reader["LCSActive"]);
                    if (reader["LCSStartDate"] != System.DBNull.Value)
                        temp.LCSStartDate = Convert.ToDateTime(reader["LCSStartDate"]);
                    if (reader["LCSEndDate"] != System.DBNull.Value)
                        temp.LCSEndDate = Convert.ToDateTime(reader["LCSEndDate"]);

                    temp.CamSerialNo1 = reader["CamSerialNo1"].ToString();
                    temp.CamSerialNo2 = reader["CamSerialNo2"].ToString();

                    list.Add(temp);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                list = null;
                throw ex;
            }
            finally
            {
                this.con.Close();
            }

            return list;
        }

        public int UpdateLocationCameraSection(LocationCameraSection lcs, string lastUser, ref string errMessage)
        {
            SqlCommand cmd = new SqlCommand("LocationCameraSectionUpdate", this.con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@LCSIntNo", SqlDbType.Int).Value = lcs.LCSIntNo;
            cmd.Parameters.Add("@LocIntNo", SqlDbType.Int).Value = lcs.LocIntNo;
            cmd.Parameters.Add("@CameraIntNo1", SqlDbType.Int).Value = lcs.CameraIntNo1;
            cmd.Parameters.Add("@CameraIntNo2", SqlDbType.Int).Value = lcs.CameraIntNo2;

            cmd.Parameters.Add("@LCSSectionCode", SqlDbType.VarChar, 5).Value = lcs.LCSSectionCode;
            cmd.Parameters.Add("@LCSSectionName", SqlDbType.VarChar, 30).Value = lcs.LCSSectionName;
            cmd.Parameters.Add("@LCSSectionDistance", SqlDbType.Int).Value = lcs.LCSSectionDistance;

            cmd.Parameters.Add("@LCSActive", SqlDbType.Bit).Value = lcs.LCSActive;
            cmd.Parameters.Add("@LCSStartDate", SqlDbType.SmallDateTime).Value = lcs.LCSStartDate;
            cmd.Parameters.Add("@LCSEndDate", SqlDbType.SmallDateTime).Value = lcs.LCSEndDate;

            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;    // 2013-07-29 add by Henry

            try
            {
                this.con.Open();
                //Jerry 2015-04-10 changed
                //return cmd.ExecuteNonQuery();
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return -1;
            }
            finally
            {
                this.con.Close();
            }
        }

        public int InsertLocationCameraSection(LocationCameraSection lcs, string lastUser, ref string errMessage)
        {
            SqlCommand cmd = new SqlCommand("LocationCameraSectionAdd", this.con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@LocIntNo", SqlDbType.Int).Value = lcs.LocIntNo;

            cmd.Parameters.Add("@CameraIntNo1", SqlDbType.Int).Value = lcs.CameraIntNo1;
            cmd.Parameters.Add("@CameraIntNo2", SqlDbType.Int).Value = lcs.CameraIntNo2;

            cmd.Parameters.Add("@LCSSectionCode", SqlDbType.VarChar, 5).Value = lcs.LCSSectionCode;
            cmd.Parameters.Add("@LCSSectionName", SqlDbType.VarChar, 30).Value = lcs.LCSSectionName;
            cmd.Parameters.Add("@LCSSectionDistance", SqlDbType.Int).Value = lcs.LCSSectionDistance;

            cmd.Parameters.Add("@LCSActive", SqlDbType.Bit).Value = lcs.LCSActive;
            cmd.Parameters.Add("@LCSStartDate", SqlDbType.SmallDateTime).Value = lcs.LCSStartDate;
            cmd.Parameters.Add("@LCSEndDate", SqlDbType.SmallDateTime).Value = lcs.LCSEndDate;

            cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;    // 2013-07-29 add by Henry

            try
            {
                this.con.Open();
                int id = Convert.ToInt32(cmd.ExecuteScalar());
                lcs.LCSIntNo = id;
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                lcs.LCSIntNo = 0; //Jerry 2015-04-10 add
            }
            finally
            {
                this.con.Close();
            }
            return lcs.LCSIntNo;
        }

        public int DeleteLocationCameraSection(int id)
        {
            SqlCommand cmd = new SqlCommand("LocationCameraSectionDelete", this.con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@LCSIntNo", SqlDbType.Int).Value = id;
            
            try
            {
                this.con.Open();
                return cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.con.Close();
            }
        }
    }
}
