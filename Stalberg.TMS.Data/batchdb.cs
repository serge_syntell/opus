using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
	
	public partial class BatchDetails 
	{
		public Int32 AutIntNo;
		public string BatNo;
		public string BatDetails;
		public Int32 CTIntNo;
		public Int32 CSIntNo;
		public DateTime BatCreateDate;
		public DateTime BatStatusChangeDate;
		public string LastUser;
	}

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public partial class BatchDB
	{
		string mConstr = "";

			public BatchDB (string vConstr)
			{
				mConstr = vConstr;
			}

		//*******************************************************
		//
		// The GetBatchDetails method returns a BatchDetails
		// struct that contains information about a specific transaction number
		//
		//*******************************************************
        // 2013-07-19 comment by Henry for useless
        //public BatchDetails GetBatchDetails(int BatIntNo) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("BatchDetail", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterBatIntNo = new SqlParameter("@BatIntNo", SqlDbType.Int, 4);
        //    parameterBatIntNo.Value = BatIntNo;
        //    myCommand.Parameters.Add(parameterBatIntNo);

        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
        //    // Create CustomerDetails Struct
        //    BatchDetails myBatchDetails = new BatchDetails();

        //    while (result.Read())
        //    {
        //        // Populate Struct using Output Params from SPROC
        //        myBatchDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
        //        myBatchDetails.CSIntNo = Convert.ToInt32(result["CSIntNo"]);
        //        myBatchDetails.CTIntNo = Convert.ToInt32(result["CTIntNo"]);
        //        myBatchDetails.BatCreateDate = Convert.ToDateTime(result["BatCreateDate"]);
        //        myBatchDetails.BatStatusChangeDate = Convert.ToDateTime(result["BatStatusChangeDate"]);
        //        myBatchDetails.BatNo = result["BatNo"].ToString();
        //        myBatchDetails.BatDetails = result["BatDetails"].ToString();
        //        myBatchDetails.LastUser = result["LastUser"].ToString();
        //    }
        //    result.Close();
        //    return myBatchDetails;
        //}

        // 2013-07-19 comment by Henry for useless
        //public BatchDetails GetNextBatchByType(string BatNo, int autIntNo) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("BatchNextNumberByType", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterBatNo = new SqlParameter("@BatNo", SqlDbType.VarChar, 2);
        //    parameterBatNo.Value = BatNo;
        //    myCommand.Parameters.Add(parameterBatNo);

        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
        //    // Create CustomerDetails Struct
        //    BatchDetails myBatchDetails = new BatchDetails();

        //    while (result.Read())
        //    {
        //        // Populate Struct using Output Params from SPROC
        //        myBatchDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
        //        myBatchDetails.BatNo = result["BatNo"].ToString();
        //        myBatchDetails.BatDetails = result["BatDetails"].ToString();
        //        myBatchDetails.LastUser = result["LastUser"].ToString();
        //    }
        //    result.Close();
        //    return myBatchDetails;
        //}

        // 2013-07-19 comment by Henry for useless
        //public string GetBatchStatus(int batIntNo) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("BatchStatus", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterBatIntNo = new SqlParameter("@BatIntNo", SqlDbType.Int);
        //    parameterBatIntNo.Value = batIntNo;
        //    myCommand.Parameters.Add(parameterBatIntNo);

        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
        //    string csCode = "";

        //    while (result.Read())
        //    {
        //        // Populate Struct using Output Params from SPROC
        //        csCode = result["CSCode"].ToString();
        //    }
        //    result.Close();
        //    return csCode;
        //}

		//*******************************************************
		//
		// The AddBatch method inserts a new transaction number record
		// into the Batch database.  A unique "BatIntNo"
		// key is then returned from the method.  
		//
		//*******************************************************

        // 2013-07-19 comment by Henry for useless
        //public int AddBatch(int autIntNo, string BatNo, string BatDetails, 
        //    string csCode, string cType, string lastUser) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("BatchAdd", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterBatNo = new SqlParameter("@BatNo", SqlDbType.VarChar, 10);
        //    parameterBatNo.Value = BatNo;
        //    myCommand.Parameters.Add(parameterBatNo);

        //    SqlParameter parameterBatDetails = new SqlParameter("@BatDetails", SqlDbType.VarChar, 255);
        //    parameterBatDetails.Value = BatDetails;
        //    myCommand.Parameters.Add(parameterBatDetails);

        //    SqlParameter parameterCSCode = new SqlParameter("@CSCode", SqlDbType.Char, 3);
        //    parameterCSCode.Value = csCode;
        //    myCommand.Parameters.Add(parameterCSCode);
			
        //    SqlParameter parameterCType = new SqlParameter("@CType", SqlDbType.VarChar, 15);
        //    parameterCType.Value = cType;
        //    myCommand.Parameters.Add(parameterCType);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterBatIntNo = new SqlParameter("@BatIntNo", SqlDbType.Int, 4);
        //    parameterBatIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterBatIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int BatIntNo = Convert.ToInt32(parameterBatIntNo.Value);

        //        return BatIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        // 2013-07-19 comment by Henry for useless
        //public int SplitBatch(int autIntNo, string tnType, int oldBatIntNo,
        //    string csCode, string lastUser) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("BatchSplit", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterOldBatIntNo = new SqlParameter("@OldBatIntNo", SqlDbType.Int, 4);
        //    parameterOldBatIntNo.Value = oldBatIntNo;
        //    myCommand.Parameters.Add(parameterOldBatIntNo);

        //    SqlParameter parameterTNType = new SqlParameter("@TNType", SqlDbType.VarChar, 3);
        //    parameterTNType.Value = tnType;
        //    myCommand.Parameters.Add(parameterTNType);

        //    SqlParameter parameterCSCode = new SqlParameter("@CSCode", SqlDbType.Char, 3);
        //    parameterCSCode.Value = csCode;
        //    myCommand.Parameters.Add(parameterCSCode);
		
        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterBatIntNo = new SqlParameter("@BatIntNo", SqlDbType.Int, 4);
        //    parameterBatIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterBatIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int BatIntNo = Convert.ToInt32(parameterBatIntNo.Value);

        //        return BatIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        // 2013-07-19 comment by Henry for useless
        //public int CheckBatch(int autIntNo, string filmNo, string csCode, string lastUser, ref string batNo) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("BatchCheckForFilm", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    // jerry 2012-02-06 change FilmNo length from 20 to 25
        //    SqlParameter parameterFilmNo = new SqlParameter("@FilmNo", SqlDbType.VarChar, 25);
        //    parameterFilmNo.Value = filmNo;
        //    myCommand.Parameters.Add(parameterFilmNo);

        //    SqlParameter parameterCSCode = new SqlParameter("@CSCode", SqlDbType.Char, 3);
        //    parameterCSCode.Value = csCode;
        //    myCommand.Parameters.Add(parameterCSCode);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterBatIntNo = new SqlParameter("@BatIntNo", SqlDbType.Int, 4);
        //    parameterBatIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterBatIntNo);

        //    SqlParameter parameterBatNo = new SqlParameter("@BatNo", SqlDbType.VarChar, 10);
        //    parameterBatNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterBatNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int BatIntNo = 0;

        //        if (parameterBatIntNo.Value == System.DBNull.Value)
        //            BatIntNo = 0;
        //        else
        //            BatIntNo = Convert.ToInt32(parameterBatIntNo.Value);

        //        batNo = parameterBatNo.Value.ToString();;
 
        //        return BatIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        batNo = "";
        //        return -2;
        //    }
        //}

        // 2013-07-19 comment by Henry for useless
        //public SqlDataReader GetBatchList(int autIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("BatchList", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the datareader result
        //    return result;
        //}

        // 2013-07-19 comment by Henry for useless
        //public SqlDataReader GetBatchListForUpdateNotices(int autIntNo, int csIntNo, int ctIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("BatchListForUpdateNotices", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterCSIntNo = new SqlParameter("@CSIntNo", SqlDbType.Int, 4);
        //    parameterCSIntNo.Value = csIntNo;
        //    myCommand.Parameters.Add(parameterCSIntNo);

        //    SqlParameter parameterCTIntNo = new SqlParameter("@CTIntNo", SqlDbType.Int, 4);
        //    parameterCTIntNo.Value = ctIntNo;
        //    myCommand.Parameters.Add(parameterCTIntNo);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the datareader result
        //    return result;
        //}

        // 2013-07-19 comment by Henry for useless
        //public DataSet GetBatchListDS(int autIntNo)
        //{
        //    SqlDataAdapter sqlDABatches = new SqlDataAdapter();
        //    DataSet dsBatches = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlDABatches.SelectCommand = new SqlCommand();
        //    sqlDABatches.SelectCommand.Connection = new SqlConnection(mConstr);			
        //    sqlDABatches.SelectCommand.CommandText = "BatchList";

        //    // Mark the Command as a SPROC
        //    sqlDABatches.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    sqlDABatches.SelectCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command and close the connection
        //    sqlDABatches.Fill(dsBatches);
        //    sqlDABatches.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsBatches;		
        //}

        // 2013-07-19 comment by Henry for useless
        //public DataSet GetBatchesForNatisDS(int autIntNo, string cType, string csCode)
        //{
        //    SqlDataAdapter sqlDABatches = new SqlDataAdapter();
        //    DataSet dsBatches = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlDABatches.SelectCommand = new SqlCommand();
        //    sqlDABatches.SelectCommand.Connection = new SqlConnection(mConstr);			
        //    sqlDABatches.SelectCommand.CommandText = "BatchListForNatis";

        //    // Mark the Command as a SPROC
        //    sqlDABatches.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    sqlDABatches.SelectCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterCType = new SqlParameter("@CType", SqlDbType.Char, 3);
        //    parameterCType.Value = cType;
        //    sqlDABatches.SelectCommand.Parameters.Add(parameterCType);

        //    SqlParameter parameterCSCode = new SqlParameter("@CSCode", SqlDbType.Char, 3);
        //    parameterCSCode.Value = csCode;
        //    sqlDABatches.SelectCommand.Parameters.Add(parameterCSCode);

        //    // Execute the command and close the connection
        //    sqlDABatches.Fill(dsBatches);
        //    sqlDABatches.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsBatches;		
        //}

        // 2013-07-19 comment by Henry for useless
        //public DataSet GetBatchesFixNatisDS(int autIntNo, string cType, string csCode)
        //{
        //    SqlDataAdapter sqlDABatches = new SqlDataAdapter();
        //    DataSet dsBatches = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlDABatches.SelectCommand = new SqlCommand();
        //    sqlDABatches.SelectCommand.Connection = new SqlConnection(mConstr);			
        //    sqlDABatches.SelectCommand.CommandText = "BatchListFixNatis";

        //    // Mark the Command as a SPROC
        //    sqlDABatches.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    sqlDABatches.SelectCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterCType = new SqlParameter("@CType", SqlDbType.Char, 3);
        //    parameterCType.Value = cType;
        //    sqlDABatches.SelectCommand.Parameters.Add(parameterCType);

        //    SqlParameter parameterCSCode = new SqlParameter("@CSCode", SqlDbType.Char, 3);
        //    parameterCSCode.Value = csCode;
        //    sqlDABatches.SelectCommand.Parameters.Add(parameterCSCode);

        //    // Execute the command and close the connection
        //    sqlDABatches.Fill(dsBatches);
        //    sqlDABatches.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsBatches;		
        //}

        // 2013-07-19 comment by Henry for useless
        //public int UpdateBatch(int batIntNo, int autIntNo, string BatNo, string BatDetails, 
        //    int csIntNo, int ctIntNo, string lastUser) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("BatchUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterBatNo = new SqlParameter("@BatNo", SqlDbType.VarChar, 10);
        //    parameterBatNo.Value = BatNo;
        //    myCommand.Parameters.Add(parameterBatNo);

        //    SqlParameter parameterBatDetails = new SqlParameter("@BatDetails", SqlDbType.VarChar, 255);
        //    parameterBatDetails.Value = BatDetails;
        //    myCommand.Parameters.Add(parameterBatDetails);

        //    SqlParameter parameterCSIntNo = new SqlParameter("@CSIntNo", SqlDbType.Int, 4);
        //    parameterCSIntNo.Value = csIntNo;
        //    myCommand.Parameters.Add(parameterCSIntNo);
			
        //    SqlParameter parameterCTIntNo = new SqlParameter("@CTIntNo", SqlDbType.Int, 4);
        //    parameterCTIntNo.Value = ctIntNo;
        //    myCommand.Parameters.Add(parameterCTIntNo);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterBatIntNo = new SqlParameter("@BatIntNo", SqlDbType.Int, 4);
        //    parameterBatIntNo.Value = batIntNo;
        //    parameterBatIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterBatIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int BatIntNo = (int)myCommand.Parameters["@BatIntNo"].Value;

        //        return BatIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        // 2013-07-19 comment by Henry for useless
        //public int StatusChangeAllLevels(int batIntNo, string oldStatus, string newStatus, string lastUser) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("BatchStatusChangeAllLevels", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterOldStatus = new SqlParameter("@OldStatus", SqlDbType.Char, 3);
        //    parameterOldStatus.Value = oldStatus;
        //    myCommand.Parameters.Add(parameterOldStatus);
			
        //    SqlParameter parameterNewStatus = new SqlParameter("@NewStatus", SqlDbType.Char, 3);
        //    parameterNewStatus.Value = newStatus;
        //    myCommand.Parameters.Add(parameterNewStatus);
			
        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterBatIntNo = new SqlParameter("@BatIntNo", SqlDbType.Int, 4);
        //    parameterBatIntNo.Value = batIntNo;
        //    parameterBatIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterBatIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int BatIntNo = (int)myCommand.Parameters["@BatIntNo"].Value;

        //        return BatIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        // 2013-07-19 comment by Henry for useless
        //public int UpdateBatchStatus(int batIntNo, string oldStatus, string newStatus, string lastUser) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("BatchUpdateStatus", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterOldStatus = new SqlParameter("@OldStatus", SqlDbType.Char, 3);
        //    parameterOldStatus.Value = oldStatus;
        //    myCommand.Parameters.Add(parameterOldStatus);
			
        //    SqlParameter parameterNewStatus = new SqlParameter("@NewStatus", SqlDbType.Char, 3);
        //    parameterNewStatus.Value = newStatus;
        //    myCommand.Parameters.Add(parameterNewStatus);
			
        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterBatIntNo = new SqlParameter("@BatIntNo", SqlDbType.Int, 4);
        //    parameterBatIntNo.Value = batIntNo;
        //    parameterBatIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterBatIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int BatIntNo = (int)myCommand.Parameters["@BatIntNo"].Value;

        //        return BatIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        // 2013-07-19 comment by Henry for useless
		//should not allow this!!!
        //public String DeleteBatch (int BatIntNo)
        //{

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("BatchDelete", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterBatIntNo = new SqlParameter("@BatIntNo", SqlDbType.Int, 4);
        //    parameterBatIntNo.Value = BatIntNo;
        //    parameterBatIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterBatIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int userId = (int)parameterBatIntNo.Value;

        //        return userId.ToString();
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return String.Empty;
        //    }
        //}
	}
}
