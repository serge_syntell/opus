using System;
using Stalberg.TMS_TPExInt.Objects;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents an Authority that has been queried for EasyPay identity
    /// </summary>
    public class EasyPayAuthority : Authority
    {
        // Fields
        private readonly int _autIntNo;
        private readonly int _receiptId;
        private readonly bool _isValid;
        private readonly int _noDaysForExpiry;
        private int _receiverIdentifier;
        private string _autCode = string.Empty;
        private string _authorityNo = string.Empty;
        private readonly List<EasyPayFineData> _fines;
        private readonly List<EasyPayTransaction> _epTrans;
        private string _metroCode = string.Empty;
        private string _metroName = string.Empty;
        private string _metroNo = string.Empty;
        private string _regionCode = string.Empty;
        private string _regionName = string.Empty;
        private string _regionTel = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="EasyPayAuthority"/> class.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="receiptId">The receipt identification.</param>
        /// <param name="autName">Name of the authority.</param>
        /// <param name="noDaysForExpiry">The no days for expiry.</param>
        public EasyPayAuthority(int autIntNo, int receiptId, string autName, int noDaysForExpiry)
        {
            this._autIntNo = autIntNo;
            this._receiptId = receiptId;
            base.Name = autName;
            this._noDaysForExpiry = noDaysForExpiry;
            this._isValid = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EasyPayAuthority"/> class.
        /// </summary>
        public EasyPayAuthority()
        {
            this._fines = new List<EasyPayFineData>();
            this._epTrans = new List<EasyPayTransaction>();
        }

        /// <summary>
        /// Gets or sets the region phone.
        /// </summary>
        /// <value>The region phone.</value>
        public string RegionPhone
        {
            get { return this._regionTel; }
            set { this._regionTel = value; }
        }

        /// <summary>
        /// Gets or sets the name of the region.
        /// </summary>
        /// <value>The name of the region.</value>
        public string RegionName
        {
            get { return this._regionName; }
            set { this._regionName = value; }
        }

        /// <summary>
        /// Gets or sets the region code.
        /// </summary>
        /// <value>The region code.</value>
        public string RegionCode
        {
            get { return this._regionCode; }
            set { this._regionCode = value; }
        }

        /// <summary>
        /// Gets or sets the metro number.
        /// </summary>
        /// <value>The metro number.</value>
        public string MetroNumber
        {
            get { return this._metroNo; }
            set { this._metroNo = value; }
        }

        /// <summary>
        /// Gets or sets the name of the metro.
        /// </summary>
        /// <value>The name of the metro.</value>
        public string MetroName
        {
            get { return this._metroName; }
            set { this._metroName = value; }
        }

        /// <summary>
        /// Gets or sets the metro code.
        /// </summary>
        /// <value>The metro code.</value>
        public string MetroCode
        {
            get { return this._metroCode; }
            set { this._metroCode = value; }
        }

        /// <summary>
        /// Gets or sets the authority code.
        /// </summary>
        /// <value>The authority code.</value>
        public string AuthorityCode
        {
            get { return this._autCode; }
            set { this._autCode = value; }
        }

        /// <summary>
        /// Gets or sets the authority number.
        /// </summary>
        /// <value>The authority number.</value>
        public string AuthorityNumber
        {
            get { return this._authorityNo; }
            set { this._authorityNo = value; }
        }

        /// <summary>
        /// Gets or sets the list of EasyPay transaction.
        /// </summary>
        /// <value>The easy pay transactions.</value>
        public List<EasyPayTransaction> EasyPayTransactions
        {
            get { return this._epTrans; }
        }

        /// <summary>
        /// Gets or sets the list of fines for export.
        /// </summary>
        /// <value>The fines.</value>
        public List<EasyPayFineData> Fines
        {
            get { return this._fines; }
        }

        /// <summary>
        /// Gets or sets the EasyPay receiver identifier.
        /// </summary>
        /// <value>The receiver identifier.</value>
        public int ReceiverIdentifier
        {
            get { return this._receiverIdentifier; }
            set { this._receiverIdentifier = value; }
        }

        /// <summary>
        /// Gets a value indicating whether this Authority has an EasyPay receiver identifier.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is easy pay; otherwise, <c>false</c>.
        /// </value>
        public bool IsEasyPay
        {
            get { return (this._receiverIdentifier > 0); }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has data or not.
        /// </summary>
        /// <value><c>true</c> if this instance has data; otherwise, <c>false</c>.</value>
        public bool HasData
        {
            get { return (this._fines.Count > 0 || this._epTrans.Count > 0); }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            if (this.IsEasyPay)
                return string.Format("{0}: {1}", base.Name, this._receiverIdentifier);

            if (!String.IsNullOrEmpty(base.Name))
                return base.Name;

            if (!String.IsNullOrEmpty(base.AutNo))
                return base.AutNo;

            return base.ID.ToString();
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
        /// <returns>
        /// <see langword="true"/> if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, <see langword="false"/>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj is EasyPayAuthority)
                return this.Equals(obj as EasyPayAuthority);

            return base.Equals(obj);
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <param name="authority">The authority.</param>
        /// <returns>
        /// <see langword="true"/> if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, <see langword="false"/>.
        /// </returns>
        public bool Equals(EasyPayAuthority authority)
        {
            return this.ID.Equals(authority.ID) && this._receiverIdentifier.Equals(authority.ReceiverIdentifier);
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Gets the number of days after the Notice payment date when the EasyPay transaction will expire.
        /// </summary>
        /// <value>The number of days for expiry.</value>
        public int NumberOfDaysForExpiry
        {
            get { return this._noDaysForExpiry; }
        }

        /// <summary>
        /// Gets the database authority int no.
        /// </summary>
        /// <value>The authority int no.</value>
        public int AutIntNo
        {
            get { return this._autIntNo; }
        }

        /// <summary>
        /// Gets the Authority's receipt identification number.
        /// </summary>
        /// <value>The receipt identification.</value>
        public int ReceiptIdentification
        {
            get { return this._receiptId; }
        }

        /// <summary>
        /// Gets a value indicating whether this Authority is a valid EasyPay client.
        /// </summary>
        /// <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
        public bool IsValid
        {
            get { return this._isValid; }
        }

    }
}