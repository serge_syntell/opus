using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a notice that has been selected for potential payment
    /// </summary>
    [Serializable]
    public class NoticeForPayment
    {
        private int notIntNo = 0;
        private bool isSelected = false;
        private decimal amountToPay = 0;
        private string seniorOfficer = string.Empty;
        private bool hasJudgement = false;
        private int dpChargeStatus = 0;
        private int dpIntNo = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="NoticeForPayment"/> class.
        /// </summary>
        /// <param name="notIntNo">The not int no.</param>
        public NoticeForPayment(int notIntNo, bool isChecked)
        {
            this.notIntNo = notIntNo;
            this.isSelected = isChecked;
        }

        public decimal AmountToPay
        {
            get { return this.amountToPay; }
            set { this.amountToPay = value; }
        }

        public int NotIntNo
        {
            get { return this.notIntNo; }
            set { this.notIntNo = value; }
        }

        public bool IsSelected
        {
            get { return this.isSelected; }
            set { this.isSelected = value; }
        }

        public bool HasJudgement
        {
            get { return this.hasJudgement; }
            set { this.hasJudgement = value; }
        }

        public string SeniorOfficer
        {
            get { return this.seniorOfficer; }
            set { this.seniorOfficer = value; }
        }

        public int DPChargeStatus
        {
            get { return this.dpChargeStatus; }
            set { this.dpChargeStatus = value; }
        }

        public int DPIntNo
        {
            get { return this.dpIntNo; }
            set { this.dpIntNo = value; }
        }
        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"></see> is equal to the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <param name="obj">The <see cref="T:System.Object"></see> to compare with the current <see cref="T:System.Object"></see>.</param>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"></see> is equal to the current <see cref="T:System.Object"></see>; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            Type t = obj.GetType();

            if (t == typeof(int))
                return ((int)obj).Equals(this.NotIntNo);

            if (t == typeof(NoticeForPayment))
                return ((NoticeForPayment)obj).NotIntNo.Equals(this.NotIntNo);

            return base.Equals(obj);
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }

    /// <summary>
    /// Contains the database interaction code for manipulating for notices for payment
    /// </summary>
    public class NoticeForPaymentDB
    {
        // Fields
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="NoticeForPaymentDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public NoticeForPaymentDB(string connectionString)
        {
            this.connectionString = connectionString;
        }
        // dls 070712 - needed to add AutIntNo to proc so that we can check the new AuthRule for max status at which to allow payment
        //              and also only return the single LA's notices
        /// <summary>
        /// Gets the notices for an ID number.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="tickedNo">The ticked no.</param>
        /// <param name="notices">The notices.</param>
        /// <param name="displayAllForID">if set to <c>true</c> all notices for the ID should be displayed.</param>
        /// <returns>A DataSet</returns>
        public DataSet GetNoticesById(string id, string tickedNo, List<int> notices, bool displayAllForID, int noDaysBeforeSummons, int maxStatus, string notFilmType)
        {
            StringBuilder sb = new StringBuilder();
            if (notices != null)
            {
                foreach (int value in notices)
                {
                    sb.Append(value);
                    sb.Append("~1,");
                }
            }

            return this.GetNoticesById(id, tickedNo, sb.ToString(), displayAllForID, noDaysBeforeSummons, 255, false, false, false, false, false, maxStatus, notFilmType);
        }

        /// <summary>
        /// Gets the notices for an ID number.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="tickedNo">The ticked no.</param>
        /// <param name="notices">The notices.</param>
        /// <param name="displayAllForID">if set to <c>true</c> all notices for the ID should be displayed.</param>
        /// <returns>A DataSet</returns>
        public DataSet GetNoticesById(string id, string tickedNo, List<NoticeForPayment> notices, bool displayAllForID, int noDaysBeforeSummons, int maxStatus, string notFilmType)
        {
            return this.GetNoticesById(id, tickedNo, notices, displayAllForID, noDaysBeforeSummons, 255, false, false, false, false, false, maxStatus, notFilmType);
        }

        /// <summary>
        /// Gets the notices by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="tickedNo">The ticked no.</param>
        /// <param name="notices">The notices.</param>
        /// <param name="displayAllForID">if set to <c>true</c> [display all for ID].</param>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="noDaysBeforeSummons">The no days before summons.</param>
        /// <param name="minStatus">The min status.</param>
        /// <returns></returns>
        public DataSet GetNoticesById(string id, string tickedNo, List<NoticeForPayment> notices, bool displayAllForID, int noDaysBeforeSummons, int minStatus, int maxStatus, string notFilmType)
        {
            StringBuilder sb = new StringBuilder();
            if (notices != null)
            {
                foreach (NoticeForPayment value in notices)
                {
                    sb.Append(value.NotIntNo);
                    sb.Append('~');
                    sb.Append(value.IsSelected ? 1 : 0);
                    sb.Append(',');
                }
            }

            return this.GetNoticesById(id, tickedNo, sb.ToString(), displayAllForID, noDaysBeforeSummons, minStatus, false, false, false, false, false, maxStatus, notFilmType);
        }

        //Barry Dickson 20080408 now also accepts 3 more parameters for rules
        //BD 20080624 new parameter for cash after grace period
        /// <summary>
        /// Gets the notices by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="tickedNo">The ticked no.</param>
        /// <param name="notices">The notices.</param>
        /// <param name="displayAllForID">if set to <c>true</c> [display all for ID].</param>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="noDaysBeforeSummons">The no days before summons.</param>
        /// <param name="minStatus">The min status.</param>
        /// <param name="cashAfterCourtDate">Can accept cash after court date.</param>
        /// <param name="cashAfterSummons">Can accept cash after summons.</param>
        /// <param name="cashAfterWarrant">Can accept cash after warrant.</param>
        /// /// <param name="cashAfterGracePeriod">Can accept cash after grace period expired.</param>
        /// <returns></returns>
        /// 
        //dls 081223 - add rule for allowing cash after case no has been generated

        public DataSet GetNoticesById(string id, string tickedNo, List<NoticeForPayment> notices, bool displayAllForID, int noDaysBeforeSummons, int minStatus, bool cashAfterCourtDate, bool cashAfterSummons, bool cashAfterWarrant, bool cashAfterGracePeriod, bool cashAfterCaseNumber, int maxStatus, string notFilmType)
        {
            StringBuilder sb = new StringBuilder();
            if (notices != null)
            {
                foreach (NoticeForPayment value in notices)
                {
                    sb.Append(value.NotIntNo);
                    sb.Append('~');
                    sb.Append(value.IsSelected ? 1 : 0);
                    sb.Append(',');
                }
            }

            return this.GetNoticesById(id, tickedNo, sb.ToString(), displayAllForID, noDaysBeforeSummons, minStatus, cashAfterCourtDate, cashAfterSummons, cashAfterWarrant, cashAfterGracePeriod, cashAfterCaseNumber, maxStatus, notFilmType);
        }

        private DataSet GetNoticesById(string id, string tickedNo, string noticeList, bool displayAllForID, int noDaysBeforeSummons, int minStatus, bool cashAfterCourtDate, bool cashAfterSummons, bool cashAfterWarrant, bool cashAfterGracePeriod, bool cashAfterCaseNumber, int maxStatus, string notFilmType)
        {
            // Create the data access objects
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com;

            if (notFilmType.Equals("H"))
                com = new SqlCommand("NoticeByIdNoForS341", con);
            else
                com = new SqlCommand("NoticeByIdNo", con);

            com.CommandType = CommandType.StoredProcedure;

            // Set the parameter values
            com.Parameters.Add("@ID", SqlDbType.VarChar, 50).Value = (!String.IsNullOrEmpty(id)) ? id : null;
            com.Parameters.Add("@Ticket", SqlDbType.VarChar, 50).Value = (!String.IsNullOrEmpty(tickedNo)) ? tickedNo : null;
            com.Parameters.Add("@NoticeList", SqlDbType.VarChar).Value = noticeList; //Jake 2012-06-19 removed the length:150
            com.Parameters.Add("@DisplayAllForID", SqlDbType.Bit, 1).Value = displayAllForID;
            //com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@NoDaysBeforeSummons", SqlDbType.Int, 4).Value = noDaysBeforeSummons;
            com.Parameters.Add("@MinStatus", SqlDbType.Int, 4).Value = minStatus;
            com.Parameters.Add("@MaxChargeStatus", SqlDbType.Int, 4).Value = maxStatus;
            com.Parameters.Add("@CashAfterCourtDate", SqlDbType.Bit, 1).Value = cashAfterCourtDate;
            com.Parameters.Add("@CashAfterSummons", SqlDbType.Bit, 1).Value = cashAfterSummons;
            com.Parameters.Add("@CashAfterWarrant", SqlDbType.Bit, 1).Value = cashAfterWarrant;
            com.Parameters.Add("@CashAfterGracePeriod", SqlDbType.Bit, 1).Value = cashAfterGracePeriod;
            com.Parameters.Add("@CashAfterCaseNumber", SqlDbType.Bit, 1).Value = cashAfterCaseNumber;

            // Fill a data set
            SqlDataAdapter adapter = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            adapter.Dispose();

            // Return the data set
            return ds;
        }

        public DataSet GetNoticesByIdForHRKGeneral(string id, string tickedNo, List<NoticeForPayment> notices, bool displayAllForID, int noDaysBeforeSummons, int minStatus, bool cashAfterCourtDate, bool cashAfterSummons, bool cashAfterWarrant, bool cashAfterGracePeriod, bool cashAfterCaseNumber, int maxStatus, string notFilmType)
        {
            StringBuilder sb = new StringBuilder();
            if (notices != null)
            {
                foreach (NoticeForPayment value in notices)
                {
                    sb.Append(value.NotIntNo);
                    sb.Append('~');
                    sb.Append(value.IsSelected ? 1 : 0);
                    sb.Append(',');
                }
            }

            return this.GetNoticesByIdForHRKGeneral(id, tickedNo, sb.ToString(), displayAllForID, noDaysBeforeSummons, minStatus, cashAfterCourtDate, cashAfterSummons, cashAfterWarrant, cashAfterGracePeriod, cashAfterCaseNumber, maxStatus, notFilmType);
        }

        private DataSet GetNoticesByIdForHRKGeneral(string id, string tickedNo, string noticeList, bool displayAllForID, int noDaysBeforeSummons, int minStatus, bool cashAfterCourtDate, bool cashAfterSummons, bool cashAfterWarrant, bool cashAfterGracePeriod, bool cashAfterCaseNumber, int maxStatus, string notFilmType)
        {
            // Create the data access objects
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com;

            if (notFilmType.Equals("H"))
                com = new SqlCommand("NoticeByIdNoForS341_HRK_General", con);
            else
                com = new SqlCommand("NoticeByIdNo_HRK_General", con);

            com.CommandType = CommandType.StoredProcedure;
            com.CommandTimeout = 0; //Adam added on 2014-10-15
            // Set the parameter values
            com.Parameters.Add("@ID", SqlDbType.VarChar, 50).Value = (!String.IsNullOrEmpty(id)) ? id : null;
            com.Parameters.Add("@Ticket", SqlDbType.VarChar, 50).Value = (!String.IsNullOrEmpty(tickedNo)) ? tickedNo : null;
            com.Parameters.Add("@NoticeList", SqlDbType.VarChar).Value = noticeList; //Jake 2012-06-19 removed the length:150
            com.Parameters.Add("@DisplayAllForID", SqlDbType.Bit, 1).Value = displayAllForID;
            //com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@NoDaysBeforeSummons", SqlDbType.Int, 4).Value = noDaysBeforeSummons;
            com.Parameters.Add("@MinStatus", SqlDbType.Int, 4).Value = minStatus;
            com.Parameters.Add("@MaxChargeStatus", SqlDbType.Int, 4).Value = maxStatus;
            com.Parameters.Add("@CashAfterCourtDate", SqlDbType.Bit, 1).Value = cashAfterCourtDate;
            com.Parameters.Add("@CashAfterSummons", SqlDbType.Bit, 1).Value = cashAfterSummons;
            com.Parameters.Add("@CashAfterWarrant", SqlDbType.Bit, 1).Value = cashAfterWarrant;
            com.Parameters.Add("@CashAfterGracePeriod", SqlDbType.Bit, 1).Value = cashAfterGracePeriod;
            com.Parameters.Add("@CashAfterCaseNumber", SqlDbType.Bit, 1).Value = cashAfterCaseNumber;

            // Fill a data set
            SqlDataAdapter adapter = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            adapter.Dispose();

            // Return the data set
            return ds;
        }

        public DataSet GetNoticesByIdForHRK(string id, string tickedNo, List<NoticeForPayment> notices, bool displayAllForID, int noDaysBeforeSummons, int minStatus, bool cashAfterCourtDate, bool cashAfterSummons, bool cashAfterWarrant, bool cashAfterGracePeriod, bool cashAfterCaseNumber, int maxStatus, string notFilmType)
        {
            StringBuilder sb = new StringBuilder();
            if (notices != null)
            {
                foreach (NoticeForPayment value in notices)
                {
                    sb.Append(value.NotIntNo);
                    sb.Append('~');
                    sb.Append(value.IsSelected ? 1 : 0);
                    sb.Append(',');
                }
            }

            return this.GetNoticesByIdForHRK(id, tickedNo, sb.ToString(), displayAllForID, noDaysBeforeSummons, minStatus, cashAfterCourtDate, cashAfterSummons, cashAfterWarrant, cashAfterGracePeriod, cashAfterCaseNumber, maxStatus, notFilmType);
        }

        private DataSet GetNoticesByIdForHRK(string id, string tickedNo, string noticeList, bool displayAllForID, int noDaysBeforeSummons, int minStatus, bool cashAfterCourtDate, bool cashAfterSummons, bool cashAfterWarrant, bool cashAfterGracePeriod, bool cashAfterCaseNumber, int maxStatus, string notFilmType)
        {
            // Create the data access objects
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com;

            if (notFilmType.Equals("H"))
                com = new SqlCommand("NoticeByIdNoForS341_HRK", con);
            else
                com = new SqlCommand("NoticeByIdNo_HRK", con);

            com.CommandType = CommandType.StoredProcedure;
            com.CommandTimeout = 0; // 2014-10-14, Oscar added

            // Set the parameter values
            com.Parameters.Add("@ID", SqlDbType.VarChar, 50).Value = (!String.IsNullOrEmpty(id)) ? id : null;
            com.Parameters.Add("@Ticket", SqlDbType.VarChar, 50).Value = (!String.IsNullOrEmpty(tickedNo)) ? tickedNo : null;
            com.Parameters.Add("@NoticeList", SqlDbType.VarChar).Value = noticeList; //Jake 2012-06-19 removed the length:150
            com.Parameters.Add("@DisplayAllForID", SqlDbType.Bit, 1).Value = displayAllForID;
            //com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@NoDaysBeforeSummons", SqlDbType.Int, 4).Value = noDaysBeforeSummons;
            com.Parameters.Add("@MinStatus", SqlDbType.Int, 4).Value = minStatus;
            com.Parameters.Add("@MaxChargeStatus", SqlDbType.Int, 4).Value = maxStatus;
            com.Parameters.Add("@CashAfterCourtDate", SqlDbType.Bit, 1).Value = cashAfterCourtDate;
            com.Parameters.Add("@CashAfterSummons", SqlDbType.Bit, 1).Value = cashAfterSummons;
            com.Parameters.Add("@CashAfterWarrant", SqlDbType.Bit, 1).Value = cashAfterWarrant;
            com.Parameters.Add("@CashAfterGracePeriod", SqlDbType.Bit, 1).Value = cashAfterGracePeriod;
            com.Parameters.Add("@CashAfterCaseNumber", SqlDbType.Bit, 1).Value = cashAfterCaseNumber;

            // Fill a data set
            SqlDataAdapter adapter = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            adapter.Dispose();

            // Return the data set
            return ds;
        }

        public DataSet GetNoticeAddressDetails(int notIntNo)
        {
            // Create the data access objects
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("NoticeAddressDetails", con);
            com.CommandType = CommandType.StoredProcedure;

            // Set the parameter values
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;

            // Fill a data set
            SqlDataAdapter adapter = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            adapter.Dispose();

            // Return the data set
            return ds;
        }

        public DataSet GetSummonsAddressDetails(int notIntNo, PersonaType person)
        {
            // Create the data access objects
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("SummonsAddressDetails", con);
            com.CommandType = CommandType.StoredProcedure;

            // Set the parameter values
            com.Parameters.Add("@SumIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters.Add("@Person", SqlDbType.VarChar, 20).Value = person;

            // Fill a data set
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(com);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                adapter.Dispose();

                // Return the data set
                return ds;
            }
            catch
            {
                return null;
            }
        }

        public int NoticeHasValidReceipt(int notIntNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("NoticeHasValidReceipt", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
            com.Parameters.Add("@RctIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

            // Open the connection and execute the Command
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Dispose();

                int rctIntNo = Convert.ToInt32(com.Parameters["@RctIntNo"].Value.ToString());
                return rctIntNo;
            }
            catch (Exception e)
            {
                con.Dispose();
                string msg = e.Message;
                return 0;
            }

        }

        public string GetFilmType(string ticketNo)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("NoticeGetFilmType", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@NotTicketNo", SqlDbType.VarChar, 50).Value = ticketNo;
            com.Parameters.Add("@NotFilmType", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

            // Open the connection and execute the Command
            try
            {
                con.Open();
                com.ExecuteNonQuery();
                con.Dispose();

                string notFilmType = com.Parameters["@NotFilmType"].Value.ToString();
                return notFilmType;
            }
            catch (Exception e)
            {
                con.Dispose();
                string msg = e.Message;
                return string.Empty;
            }
        }
    }
}