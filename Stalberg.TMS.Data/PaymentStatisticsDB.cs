using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for PaymentStatisticsDB
/// </summary>
namespace Stalberg.TMS
{

    public partial class PaymentStatisticsDB
    {
        string mConstr = "";

        public PaymentStatisticsDB(string vConstr)
        {
	        mConstr = vConstr;
        }

        public DataSet GetPaymentStatsDataDS(String autIntNo, string sYear)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            // Create Instance of Connection and Command Object
            da.SelectCommand = new SqlCommand();
            da.SelectCommand.Connection = new SqlConnection(mConstr);
            da.SelectCommand.CommandText = "GetPaymentStats";
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int).Value = int.Parse(autIntNo);
            da.SelectCommand.Parameters.Add("@InYear", SqlDbType.Int).Value = int.Parse(sYear);

            // Execute the command and close the connection
            da.Fill(ds);
            da.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return ds;		
        }

        public DataSet GetPaymentStatsTotalDataDS(String autIntNo, string sYear, string type)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            // Create Instance of Connection and Command Object
            da.SelectCommand = new SqlCommand();
            da.SelectCommand.Connection = new SqlConnection(mConstr);
            da.SelectCommand.CommandText = "GetPaymentStatsTotal";
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int).Value = int.Parse(autIntNo);
            da.SelectCommand.Parameters.Add("@InYear", SqlDbType.Int).Value = int.Parse(sYear);
            da.SelectCommand.Parameters.Add("@Type", SqlDbType.VarChar, 10).Value = type;

            // Execute the command and close the connection
            da.Fill(ds);
            da.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return ds;
        }

    }
}