using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    //*******************************************************
    //
    // UserDetails Class
    //
    // A simple data class that encapsulates details about a particular user 
    //
    //*******************************************************

    [Serializable]
    public class UserDetails
    {
        public Int32 USGIntNo;
        public string UserSName;
        public string UserInit;
        public string UserFName;
        public string UserLoginName;
        public string UserPassword;
        public int UserAccessLevel;
        public string UserEmail;
        public string UserName;
        public string SpotFinePriorityDate;
    }

    //*******************************************************
    //
    // UsersDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query Users within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public class UserDB
    {
        // Fields
        readonly string connectionString = string.Empty;

        public UserDB(string vConstr)
        {
            this.connectionString = vConstr;
        }

        //    //*******************************************************
        //    //
        //    // UsersDB.GetUserDetails() Method <a name="GetUserDetails"></a>
        //    //
        //    // The GetUserDetails method returns a UserDetails
        //    // struct that contains information about a specific
        //    // customer (name, password, etc).
        //    //
        //    //*******************************************************

        public UserDetails GetUserDetails(int userIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("UserDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int, 4);
            parameterUserIntNo.Value = userIntNo;
            myCommand.Parameters.Add(parameterUserIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            UserDetails myUserDetails = new UserDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                //myUserDetails.USGIntNo = Convert.ToInt32(result["USGIntNo"]);
                myUserDetails.UserSName = result["UserSName"].ToString();
                myUserDetails.UserInit = result["UserInit"].ToString();
                myUserDetails.UserFName = result["UserFName"].ToString();
                myUserDetails.UserName = result["UserName"].ToString();
                myUserDetails.UserAccessLevel = Convert.ToInt32(result["UserAccessLevel"]);
                myUserDetails.UserLoginName = result["UserLoginName"].ToString();
                myUserDetails.UserEmail = result["UserEmail"].ToString();
                myUserDetails.SpotFinePriorityDate = result["SpotFinePriorityDate"].ToString();
                myUserDetails.UserPassword = result["UserPassword"].ToString();
            }
            result.Close();
            return myUserDetails;
        }

        //    //*******************************************************
        //    //
        //    // UsersDB.AddUser() Method <a name="AddUser"></a>
        //    //
        //    // The AddUser method inserts a new user record
        //    // into the users database.  A unique "UserId"
        //    // key is then returned from the method.  
        //    //
        //    //*******************************************************

        //    public int AddUser
        //        (int usgIntNo, string UserSName, string userInit, string userFName, string userPassword,
        //        int userAccessLevel, string userLoginName, string userEmail, ref string errMessage)
        //    {
        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(connectionString);
        //        SqlCommand myCommand = new SqlCommand("UserAdd", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        // Add Parameters to SPROC
        //        SqlParameter parameterUSGIntNo = new SqlParameter("@USGIntNo", SqlDbType.Int, 4);
        //        parameterUSGIntNo.Value = usgIntNo;
        //        myCommand.Parameters.Add(parameterUSGIntNo);

        //        SqlParameter parameterUserSName = new SqlParameter("@UserSName", SqlDbType.VarChar, 20);
        //        parameterUserSName.Value = UserSName;
        //        myCommand.Parameters.Add(parameterUserSName);

        //        SqlParameter parameterUserInit = new SqlParameter("@UserInit", SqlDbType.VarChar, 5);
        //        parameterUserInit.Value = userInit;
        //        myCommand.Parameters.Add(parameterUserInit);

        //        SqlParameter parameterUserFName = new SqlParameter("@UserFName", SqlDbType.VarChar, 15);
        //        parameterUserFName.Value = userFName;
        //        myCommand.Parameters.Add(parameterUserFName);

        //        SqlParameter parameterUserPassword = new SqlParameter("@UserPassword", SqlDbType.VarChar, 50);
        //        parameterUserPassword.Value = userPassword;
        //        myCommand.Parameters.Add(parameterUserPassword);

        //        SqlParameter parameterUserAccessLevel = new SqlParameter("@UserAccessLevel", SqlDbType.Int, 4);
        //        parameterUserAccessLevel.Value = userAccessLevel;
        //        myCommand.Parameters.Add(parameterUserAccessLevel);

        //        SqlParameter parameterUserLoginName = new SqlParameter("@UserLoginName", SqlDbType.VarChar, 10);
        //        parameterUserLoginName.Value = userLoginName;
        //        myCommand.Parameters.Add(parameterUserLoginName);

        //        SqlParameter parameterUserEmail = new SqlParameter("@UserEmail", SqlDbType.VarChar, 50);
        //        parameterUserEmail.Value = userEmail;
        //        myCommand.Parameters.Add(parameterUserEmail);

        //        SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int, 4);
        //        parameterUserIntNo.Direction = ParameterDirection.Output;
        //        myCommand.Parameters.Add(parameterUserIntNo);

        //        try
        //        {
        //            myConnection.Open();
        //            myCommand.ExecuteNonQuery();
        //            myConnection.Dispose();

        //            // Calculate the CustomerID using Output Param from SPROC
        //            int userId = Convert.ToInt32(parameterUserIntNo.Value);

        //            return userId;
        //        }
        //        catch (Exception e)
        //        {
        //            myConnection.Dispose();
        //            errMessage = e.Message;
        //            return 0;
        //        }
        //    }

        //    //*******************************************************
        //    //
        //    // UsersDB.Login() Method <a name="Login"></a>
        //    //
        //    // The Login method validates a login name/password pair
        //    // against credentials stored in the users database.
        //    // If the login/password pair is valid, the method returns
        //    // the "UserId" number of the user.  Otherwise
        //    // it will throw an exception.
        //    //
        //    //*******************************************************

        //    public int Login(string userLoginName, string userEmail, string userPassword, int autIntNo)
        //    {
        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(connectionString);
        //        SqlCommand myCommand = new SqlCommand("UserLogin", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        // Add Parameters to SPROC
        //        SqlParameter parameterUserLoginName = new SqlParameter("@UserLoginName", SqlDbType.VarChar, 10);
        //        parameterUserLoginName.Value = userLoginName;
        //        myCommand.Parameters.Add(parameterUserLoginName);

        //        SqlParameter parameterUserEmail = new SqlParameter("@UserEmail", SqlDbType.VarChar, 50);
        //        parameterUserEmail.Value = userEmail;
        //        myCommand.Parameters.Add(parameterUserEmail);

        //        SqlParameter parameterUserPassword = new SqlParameter("@UserPassword", SqlDbType.VarChar, 50);
        //        parameterUserPassword.Value = userPassword;
        //        myCommand.Parameters.Add(parameterUserPassword);

        //        SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //        parameterAutIntNo.Value = autIntNo;
        //        myCommand.Parameters.Add(parameterAutIntNo);

        //        SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int, 4);
        //        parameterUserIntNo.Direction = ParameterDirection.Output;
        //        myCommand.Parameters.Add(parameterUserIntNo);

        //        // Open the connection and execute the Command
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int userIntNo = (int)(parameterUserIntNo.Value);

        //        if (userIntNo == 0)
        //        {
        //            return 0;
        //        }
        //        else
        //        {
        //            return userIntNo;
        //        }
        //    }

        //    public int AdminLogin(string userLoginName, string userPassword)
        //    {

        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(connectionString);
        //        SqlCommand myCommand = new SqlCommand("AdminUserLogin", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        // Add Parameters to SPROC
        //        SqlParameter parameterUserLoginName = new SqlParameter("@UserLoginName", SqlDbType.VarChar, 10);
        //        parameterUserLoginName.Value = userLoginName;
        //        myCommand.Parameters.Add(parameterUserLoginName);

        //        SqlParameter parameterUserPassword = new SqlParameter("@UserPassword", SqlDbType.VarChar, 50);
        //        parameterUserPassword.Value = userPassword;
        //        myCommand.Parameters.Add(parameterUserPassword);

        //        SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int, 4);
        //        parameterUserIntNo.Direction = ParameterDirection.Output;
        //        myCommand.Parameters.Add(parameterUserIntNo);

        //        SqlParameter parameterBrokerCode = new SqlParameter("@BrokerCode", SqlDbType.VarChar, 10);
        //        parameterBrokerCode.Direction = ParameterDirection.Output;
        //        myCommand.Parameters.Add(parameterBrokerCode);

        //        // Open the connection and execute the Command
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int userId = (int)(parameterUserIntNo.Value);

        //        if (userId == 0)
        //        {
        //            return 0;
        //        }
        //        else
        //        {
        //            string brokerCode = parameterBrokerCode.Value.ToString();

        //            if (brokerCode.Equals("Root"))
        //                return userId;
        //            else
        //                return -1;
        //        }
        //    }

        //    public UserDetails GetUserAccessLevel(int userIntNo)
        //    {

        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(connectionString);
        //        SqlCommand myCommand = new SqlCommand("UserAccessLevel", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        // Add Parameters to SPROC
        //        SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int);
        //        parameterUserIntNo.Value = userIntNo;
        //        myCommand.Parameters.Add(parameterUserIntNo);

        //        SqlParameter parameterUserAccessLevel = new SqlParameter("@UserAccessLevel", SqlDbType.Int, 4);
        //        parameterUserAccessLevel.Direction = ParameterDirection.Output;
        //        myCommand.Parameters.Add(parameterUserAccessLevel);

        //        // Open the connection and execute the Command
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        UserDetails myUserDetails = new UserDetails();

        //public SqlDataReader GetUserList()
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("UserList", myConnection);

        //        return myUserDetails;
        //    }

        public SqlDataReader GetUserList()
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("UserList", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;
        }

        public DataSet GetUserListDS()
        {
            SqlDataAdapter sqlDAUsers = new SqlDataAdapter();
            DataSet dsUsers = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAUsers.SelectCommand = new SqlCommand();
            sqlDAUsers.SelectCommand.Connection = new SqlConnection(connectionString);
            sqlDAUsers.SelectCommand.CommandText = "UserList";

            // Mark the Command as a SPROC
            sqlDAUsers.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Return the dataset result
            return dsUsers;
        }

        //    public DataSet SearchUserListDS(string searchText)
        //    {
        //        SqlDataAdapter sqlDAUsers = new SqlDataAdapter();
        //        DataSet dsUsers = new DataSet();

        //        // Create Instance of Connection and Command Object
        //        sqlDAUsers.SelectCommand = new SqlCommand();
        //        sqlDAUsers.SelectCommand.Connection = new SqlConnection(connectionString);
        //        sqlDAUsers.SelectCommand.CommandText = "SearchUserList";

        //        // Mark the Command as a SPROC
        //        sqlDAUsers.SelectCommand.CommandType = CommandType.StoredProcedure;

        //        SqlParameter parameterSearchText = new SqlParameter("@SearchText", SqlDbType.VarChar, 20);
        //        parameterSearchText.Value = searchText;
        //        sqlDAUsers.SelectCommand.Parameters.Add(parameterSearchText);

        //        // Execute the command and close the connection
        //        sqlDAUsers.Fill(dsUsers);
        //        sqlDAUsers.SelectCommand.Connection.Dispose();

        //        // Return the dataset result
        //        return dsUsers;
        //    }

        //    public DataSet GetUserListByUserGroupDS(string searchText, string colName, int colValue, string type)
        //    {
        //        SqlDataAdapter sqlDAUsers = new SqlDataAdapter();
        //        DataSet dsUsers = new DataSet();

        //        // Create Instance of Connection and Command Object
        //        sqlDAUsers.SelectCommand = new SqlCommand();
        //        sqlDAUsers.SelectCommand.Connection = new SqlConnection(connectionString);
        //        sqlDAUsers.SelectCommand.CommandText = "UserListByUserGroup";

        //        // Mark the Command as a SPROC
        //        sqlDAUsers.SelectCommand.CommandType = CommandType.StoredProcedure;

        //        SqlParameter parameterSearchText = new SqlParameter("@SearchText", SqlDbType.VarChar, 20);
        //        parameterSearchText.Value = searchText;
        //        sqlDAUsers.SelectCommand.Parameters.Add(parameterSearchText);

        //        SqlParameter parameterColName = new SqlParameter("@ColName", SqlDbType.VarChar, 20);
        //        parameterColName.Value = colName;
        //        sqlDAUsers.SelectCommand.Parameters.Add(parameterColName);

        //        SqlParameter parameterColValue = new SqlParameter("@ColValue", SqlDbType.Int);
        //        parameterColValue.Value = colValue;
        //        sqlDAUsers.SelectCommand.Parameters.Add(parameterColValue);

        //        SqlParameter parameterType = new SqlParameter("@Type", SqlDbType.Char, 1);
        //        parameterType.Value = type;
        //        sqlDAUsers.SelectCommand.Parameters.Add(parameterType);

        //        // Execute the command and close the connection
        //        sqlDAUsers.Fill(dsUsers);
        //        sqlDAUsers.SelectCommand.Connection.Dispose();

        //        // Return the dataset result
        //        return dsUsers;
        //    }


        //    public int UpdateUser

        //        //password only updated via change password
        //        (int usgIntNo, int userIntNo, string UserSName, string userInit, string userFName, int userAccessLevel,
        //        string userLoginName, string userEmail)
        //    {

        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(connectionString);
        //        SqlCommand myCommand = new SqlCommand("UserUpdate", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        // Add Parameters to SPROC
        //        SqlParameter parameterUSGIntNo = new SqlParameter("@USGIntNo", SqlDbType.Int, 4);
        //        parameterUSGIntNo.Value = usgIntNo;
        //        myCommand.Parameters.Add(parameterUSGIntNo);

        //        SqlParameter parameterUserSName = new SqlParameter("@UserSName", SqlDbType.VarChar, 20);
        //        parameterUserSName.Value = UserSName;
        //        myCommand.Parameters.Add(parameterUserSName);

        //        SqlParameter parameterUserInit = new SqlParameter("@UserInit", SqlDbType.VarChar, 5);
        //        parameterUserInit.Value = userInit;
        //        myCommand.Parameters.Add(parameterUserInit);

        //        SqlParameter parameterUserFName = new SqlParameter("@UserFName", SqlDbType.VarChar, 15);
        //        parameterUserFName.Value = userFName;
        //        myCommand.Parameters.Add(parameterUserFName);

        //        SqlParameter parameterUserAccessLevel = new SqlParameter("@UserAccessLevel", SqlDbType.Int, 4);
        //        parameterUserAccessLevel.Value = userAccessLevel;
        //        myCommand.Parameters.Add(parameterUserAccessLevel);

        //        SqlParameter parameterUserLoginName = new SqlParameter("@UserLoginName", SqlDbType.VarChar, 10);
        //        parameterUserLoginName.Value = userLoginName;
        //        myCommand.Parameters.Add(parameterUserLoginName);

        //        SqlParameter parameterUserEmail = new SqlParameter("@UserEmail", SqlDbType.VarChar, 50);
        //        parameterUserEmail.Value = userEmail;
        //        myCommand.Parameters.Add(parameterUserEmail);

        //        SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int);
        //        parameterUserIntNo.Direction = ParameterDirection.InputOutput;
        //        parameterUserIntNo.Value = userIntNo;
        //        myCommand.Parameters.Add(parameterUserIntNo);

        //        try
        //        {
        //            myConnection.Open();
        //            myCommand.ExecuteNonQuery();
        //            myConnection.Dispose();

        //            // Calculate the CustomerID using Output Param from SPROC
        //            int UserIntNo = (int)myCommand.Parameters["@UserIntNo"].Value;
        //            //int userId = (int)parameterUserIntNo.Value;

        //            return UserIntNo;
        //        }
        //        catch (Exception e)
        //        {
        //            myConnection.Dispose();
        //            string msg = e.Message;
        //            return 0;
        //        }
        //    }

        //    public String DeleteUser(int userIntNo)
        //    {

        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(connectionString);
        //        SqlCommand myCommand = new SqlCommand("UserDelete", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        // Add Parameters to SPROC
        //        SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int, 4);
        //        parameterUserIntNo.Value = userIntNo;
        //        parameterUserIntNo.Direction = ParameterDirection.InputOutput;
        //        myCommand.Parameters.Add(parameterUserIntNo);

        //        try
        //        {
        //            myConnection.Open();
        //            myCommand.ExecuteNonQuery();
        //            myConnection.Dispose();

        //            // Calculate the CustomerID using Output Param from SPROC
        //            int userId = (int)parameterUserIntNo.Value;

        //            return userId.ToString();
        //        }
        //        catch (Exception e)
        //        {
        //            myConnection.Dispose();
        //            string msg = e.Message;
        //            return String.Empty;
        //        }
        //    }
        // 2013-07-26 add parameter lastUser by Henry
        public bool UpdateUserPasswd(int userIntNo, string userLoginName,
            string userEmail, string userPassword, string loginReq, string emailReq, string lastUser, int expiryDays = 0)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("UserPassword", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            //Heidi 2014-08-19 changed @UserLoginName define the length for fixing issue that Do not change the password.(bontq1393)
            SqlParameter parameterUserLoginName = new SqlParameter("@UserLoginName", SqlDbType.VarChar, 15);
            parameterUserLoginName.Value = userLoginName;
            myCommand.Parameters.Add(parameterUserLoginName);

            SqlParameter parameterUserEmail = new SqlParameter("@UserEmail", SqlDbType.VarChar, 50);
            parameterUserEmail.Value = userEmail;
            myCommand.Parameters.Add(parameterUserEmail);

            SqlParameter parameterUserPassword = new SqlParameter("@UserPassword", SqlDbType.VarChar, 50);
            parameterUserPassword.Value = userPassword;
            myCommand.Parameters.Add(parameterUserPassword);

            SqlParameter parameterLoginReq = new SqlParameter("@LoginReq", SqlDbType.Char, 1);
            parameterLoginReq.Value = loginReq;
            myCommand.Parameters.Add(parameterLoginReq);

            SqlParameter parameterEmailReq = new SqlParameter("@EmailReq", SqlDbType.Char, 1);
            parameterEmailReq.Value = emailReq;
            myCommand.Parameters.Add(parameterEmailReq);

            SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int, 4);
            parameterUserIntNo.Value = userIntNo;
            myCommand.Parameters.Add(parameterUserIntNo);
            //Jerry 2014-01-08 add
            //parameterUserIntNo.Direction = ParameterDirection.Output;   
            //Dawn - changed 2014-01-31
            parameterUserIntNo.Direction = ParameterDirection.InputOutput;

            //Jerry 2012-06-13 add
            SqlParameter parameterExpiryDays = new SqlParameter("@ExpiryDays", SqlDbType.Int, 4);
            parameterExpiryDays.Value = expiryDays;
            myCommand.Parameters.Add(parameterExpiryDays);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            // Open the connection and execute the Command
            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
            }

            myConnection.Dispose();

            int UserIntNo = (int)(parameterUserIntNo.Value);

            if (UserIntNo == -1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //    public String CheckUserLogin(string userLoginName, string brokerCode)
        //    {

        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(connectionString);
        //        SqlCommand myCommand = new SqlCommand("GetUserIntNo", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        // Add Parameters to SPROC
        //        SqlParameter parameterBrokerCode = new SqlParameter("@BrokerCode", SqlDbType.VarChar, 10);
        //        parameterBrokerCode.Value = brokerCode;
        //        myCommand.Parameters.Add(parameterBrokerCode);

        //        SqlParameter parameterUserLoginName = new SqlParameter("@UserLoginName", SqlDbType.VarChar, 10);
        //        parameterUserLoginName.Value = userLoginName;
        //        myCommand.Parameters.Add(parameterUserLoginName);

        //        SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int, 4);
        //        parameterUserIntNo.Direction = ParameterDirection.Output;
        //        myCommand.Parameters.Add(parameterUserIntNo);

        //        // Open the connection and execute the Command
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int userIntNo = (int)(parameterUserIntNo.Value);

        //        if (userIntNo == 0)
        //        {
        //            return null;
        //        }
        //        else
        //        {
        //            return userIntNo.ToString();
        //        }
        //    }

        //    public SqlDataReader GetUserMenuName(int userIntNo)
        //    {

        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(connectionString);
        //        SqlCommand myCommand = new SqlCommand("UserGroupMenuNameByUser", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        // Add Parameters to SPROC
        //        SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int);
        //        parameterUserIntNo.Value = userIntNo;
        //        myCommand.Parameters.Add(parameterUserIntNo);

        //        // Open the connection and execute the Command
        //        myConnection.Open();

        //        SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //        return result;
        //    }

        //    public int UpdateSubGroupForUser(int usgIntNo, int userIntNo)
        //    {

        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(connectionString);
        //        SqlCommand myCommand = new SqlCommand("UserUpdateSubGroup", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        // Add Parameters to SPROC
        //        SqlParameter parameterUSGIntNo = new SqlParameter("@USGIntNo", SqlDbType.Int, 4);
        //        parameterUSGIntNo.Value = usgIntNo;
        //        myCommand.Parameters.Add(parameterUSGIntNo);

        //        SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int);
        //        parameterUserIntNo.Direction = ParameterDirection.InputOutput;
        //        parameterUserIntNo.Value = userIntNo;
        //        myCommand.Parameters.Add(parameterUserIntNo);

        //        try
        //        {
        //            myConnection.Open();
        //            myCommand.ExecuteNonQuery();
        //            myConnection.Dispose();

        //            // Calculate the CustomerID using Output Param from SPROC
        //            int UserIntNo = (int)myCommand.Parameters["@UserIntNo"].Value;
        //            //int userId = (int)parameterUserIntNo.Value;

        //            return UserIntNo;
        //        }
        //        catch (Exception e)
        //        {
        //            myConnection.Dispose();
        //            string msg = e.Message;
        //            return 0;
        //        }
        //    }

        //    public SqlDataReader GetAuthorityListForUser(int userIntNo)
        //    {
        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(connectionString);
        //        SqlCommand myCommand = new SqlCommand("AuthorityListForUser", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        // Add Parameters to SPROC
        //        SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int);
        //        parameterUserIntNo.Value = userIntNo;
        //        myCommand.Parameters.Add(parameterUserIntNo);

        //        // Execute the command
        //        myConnection.Open();
        //        SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //        // Return the data reader result
        //        return result;
        //    }

        //    //Barry Dickson 20080319 need to be able to get user lidt by authority id for printing of cash receipt spreadsheet
        //    //[UserListInGroupForAuthority]
        //    public SqlDataReader GetUserListForAuthority(int AutIntNo)
        //    {
        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(connectionString);
        //        SqlCommand myCommand = new SqlCommand("UserListForAuthority", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        // Add Parameters to SPROC
        //        SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
        //        parameterAutIntNo.Value = AutIntNo;
        //        myCommand.Parameters.Add(parameterAutIntNo);

        //        // Execute the command
        //        myConnection.Open();
        //        SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //        // Return the data reader result
        //        return result;
        //    }

        //BD need a list of users by authority through their groups
        //[UserListInGroupForAuthority]
        public SqlDataReader GetUserListInGroupForAuthority(int AutIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("UserListInGroupForAuthority", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Value = AutIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }


        /// <summary>
        /// Resets the user-must-count-cashbox flag to yes.
        /// </summary>
        /// <param name="userIntNo">The user int no.</param>
        /// <param name="_login">The last user.</param>
        public void ResetUserCountCashBox(int userIntNo, string login)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("UserCountCashBoxReset", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@UserIntNo", SqlDbType.Int, 4).Value = userIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = login;

            try
            {
                con.Open();
                com.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }

        public int UserShiftUpdate(int userIntNo, string sUser)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("UserShiftUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int, 4);
            parameterUserIntNo.Value = userIntNo;
            myCommand.Parameters.Add(parameterUserIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = sUser;
            myCommand.Parameters.Add(parameterLastUser);


            SqlParameter parameterUSIntNo = new SqlParameter("@USIntNo", SqlDbType.Int, 4);
            parameterUSIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterUSIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int USIntNo = (int)myCommand.Parameters["@USIntNo"].Value;

                return USIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }
        // 2013-07-26 add parameter lastUser by Henry 
        public int PriorityDateUpdate(int userIntNo, DateTime pDate, string lastUser)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("SpotFinePriorityDateUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterUserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int, 4);
            parameterUserIntNo.Direction = ParameterDirection.InputOutput;
            parameterUserIntNo.Value = userIntNo;
            myCommand.Parameters.Add(parameterUserIntNo);

            SqlParameter parameterPriorityDate = new SqlParameter("@PriorityDate", SqlDbType.DateTime);
            parameterPriorityDate.Value = pDate;
            myCommand.Parameters.Add(parameterPriorityDate);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int UserIntNo = (int)myCommand.Parameters["@UserIntNo"].Value;

                return UserIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }
        // 2013-07-26 add parameter lastUser by Henry
        public int UserShiftEdit(int nUSIntNo, string sOfficerNo, string sMode, string lastUser)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("UserShiftEdit", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterMode = new SqlParameter("@Mode", SqlDbType.VarChar, 15);
            parameterMode.Value = sMode;
            myCommand.Parameters.Add(parameterMode);

            if (sMode != "Logout")
            {
                SqlParameter parameterLastUser = new SqlParameter("@USOfficerNumber", SqlDbType.VarChar, 10);
                parameterLastUser.Value = sOfficerNo;
                myCommand.Parameters.Add(parameterLastUser);
            }

            SqlParameter parameterUSIntNo = new SqlParameter("@USIntNo", SqlDbType.Int, 4);
            parameterUSIntNo.Direction = ParameterDirection.InputOutput;
            parameterUSIntNo.Value = nUSIntNo;
            myCommand.Parameters.Add(parameterUSIntNo);

            SqlParameter parameterLastUser1 = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser1.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser1);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int USIntNo = (int)myCommand.Parameters["@USIntNo"].Value;

                return USIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        //    public int UserOfficerUpdate(int nUserIntNo, string sOfficerNo, string sUser)
        //    {

        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(connectionString);
        //        SqlCommand myCommand = new SqlCommand("UserOfficerUpdate", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        SqlParameter parameterUserIntNo = new SqlParameter("@UOUserIntNo", SqlDbType.Int, 4);
        //        parameterUserIntNo.Direction = ParameterDirection.InputOutput;
        //        parameterUserIntNo.Value = nUserIntNo;
        //        myCommand.Parameters.Add(parameterUserIntNo);

        //        SqlParameter parameterTO = new SqlParameter("@UOTONo", SqlDbType.VarChar, 10);
        //        parameterTO.Value = sOfficerNo;
        //        myCommand.Parameters.Add(parameterTO);

        //        SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //        parameterLastUser.Value = sUser;
        //        myCommand.Parameters.Add(parameterLastUser);

        //        try
        //        {
        //            myConnection.Open();
        //            myCommand.ExecuteNonQuery();
        //            myConnection.Dispose();

        //            // Calculate the CustomerID using Output Param from SPROC
        //            int USIntNo = (int)myCommand.Parameters["@UOUserIntNo"].Value;

        //            return USIntNo;
        //        }
        //        catch (Exception e)
        //        {
        //            myConnection.Dispose();
        //            string msg = e.Message;
        //            return 0;
        //        }
        //    }

        //    public int UserOfficerAdd(int nUserIntNo, string sOfficerNo, string sUser)
        //    {

        //        // Create Instance of Connection and Command Object
        //        SqlConnection myConnection = new SqlConnection(connectionString);
        //        SqlCommand myCommand = new SqlCommand("UserOfficerAdd", myConnection);

        //        // Mark the Command as a SPROC
        //        myCommand.CommandType = CommandType.StoredProcedure;

        //        SqlParameter parameterUserIntNo = new SqlParameter("@UOUserIntNo", SqlDbType.Int, 4);
        //        parameterUserIntNo.Direction = ParameterDirection.InputOutput;
        //        parameterUserIntNo.Value = nUserIntNo;
        //        myCommand.Parameters.Add(parameterUserIntNo);

        //        SqlParameter parameterTO = new SqlParameter("@UOTONo", SqlDbType.VarChar, 10);
        //        parameterTO.Value = sOfficerNo;
        //        myCommand.Parameters.Add(parameterTO);

        //        SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //        parameterLastUser.Value = sUser;
        //        myCommand.Parameters.Add(parameterLastUser);

        //        try
        //        {
        //            myConnection.Open();
        //            myCommand.ExecuteNonQuery();
        //            myConnection.Dispose();

        //            // Calculate the CustomerID using Output Param from SPROC
        //            int USIntNo = (int)myCommand.Parameters["@UOUserIntNo"].Value;

        //            return USIntNo;
        //        }
        //        catch (Exception e)
        //        {
        //            myConnection.Dispose();
        //            string msg = e.Message;
        //            return 0;
        //        }
        //    }


        public string GetOfficerNo(int nUserIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("GetOfficerNo", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterUserIntNo = new SqlParameter("@UOUserIntNo", SqlDbType.Int, 4);
            parameterUserIntNo.Value = nUserIntNo;
            myCommand.Parameters.Add(parameterUserIntNo);

            SqlParameter parameterTO = new SqlParameter("@UOTONo", SqlDbType.VarChar, 10);
            parameterTO.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterTO);


            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                //Jake 2014-05-12 modified
                //string sOfficerNo = (string)myCommand.Parameters["@UOTONo"].Value;
                string sOfficerNo = myCommand.Parameters["@UOTONo"].Value is DBNull ? "" : myCommand.Parameters["@UOTONo"].Value.ToString();

                return sOfficerNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return "0";
            }
        }
    }
}

