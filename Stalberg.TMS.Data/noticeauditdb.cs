using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    public class noticeauditdb
    {
         // Fields
        private readonly string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public noticeauditdb(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Gets the notice audit results.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="startDate">The start date string (empty for all).</param>
        /// <param name="endDate">The end date string (empty for all).</param>
        /// <param name="ticketNo">The ticket no.</param>
        /// <param name="regNo">The reg no.</param>
        /// <returns>A <see cref="SqlDataReader"/></returns>
        public SqlDataReader GetNoticeAudit(String autIntNo, string startDate, string endDate, string ticketNo, string regNo)
        {
            DateTime startDt = DateTime.Now;
            if (!DateTime.TryParse(startDate, out startDt))
                //startDt = DateTime.Now.Subtract(TimeSpan.FromDays(30));
                startDt = Convert.ToDateTime("1900/01/01");
            DateTime endDt;
            if (!DateTime.TryParse(endDate, out endDt))
                //endDt = DateTime.Now;
                endDt = Convert.ToDateTime("1900/01/01");

            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("GetNoticeAuditByDate", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int).Value = int.Parse(autIntNo);
            com.Parameters.Add("@DateFrom", SqlDbType.SmallDateTime, 4).Value = startDt;
            com.Parameters.Add("@DateTo", SqlDbType.SmallDateTime, 4).Value = endDt;
            com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = ticketNo;
            com.Parameters.Add("@RegNo", SqlDbType.VarChar, 10).Value = regNo;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }
    }
}

