using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Collections;

namespace Stalberg.TMS
{
    //dls 061121 - remove MtrIntNo and CameraID (becomes separate CamUnitID table) from Camera table 

    public partial class CameraDetails 
	{
		//public Int32 MtrIntNo;
        //public string CamID;
		public string CamSerialNo;
        public string MaxSupportingImages;
		public string LastUser;
    }

    public partial class CameraDB {

		string mConstr = "";

		public CameraDB (string vConstr)
		{
			mConstr = vConstr;
		}

        public CameraDetails GetCameraDetails(int camIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CameraDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCamIntNo = new SqlParameter("@CamIntNo", SqlDbType.Int, 4);
            parameterCamIntNo.Value = camIntNo;
            myCommand.Parameters.Add(parameterCamIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			CameraDetails myCameraDetails = new CameraDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				//myCameraDetails.MtrIntNo = Convert.ToInt32(result["MtrIntNo"]);
				myCameraDetails.CamSerialNo = result["CamSerialNo"].ToString();
                myCameraDetails.MaxSupportingImages = result["MaxSupportingImages"].ToString();
				//myCameraDetails.CamID = result["CamID"].ToString();
			}
			result.Close();
            return myCameraDetails;
        }

        //public int AddCamera (int mtrIntNo, string camID, string camSerialNo, string lastUser) 
        public int AddCamera(string camSerialNo, int MaxSupportingImages, string lastUser) 
        {
            // dls 061120 - remove CamID into separate table CameraUnit

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CameraAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            //SqlParameter parameterMtrIntNo = new SqlParameter("@MtrIntNo", SqlDbType.Int, 4);
            //parameterMtrIntNo.Value = mtrIntNo;
            //myCommand.Parameters.Add(parameterMtrIntNo);

			SqlParameter parameterCamSerialNo = new SqlParameter("@CamSerialNo", SqlDbType.VarChar, 18);
			parameterCamSerialNo.Value = camSerialNo;
			myCommand.Parameters.Add(parameterCamSerialNo);

            SqlParameter parameterMaxSupportingImages = new SqlParameter("@MaxSupportingImages", SqlDbType.TinyInt, 4);
            parameterMaxSupportingImages.Value = MaxSupportingImages;
            myCommand.Parameters.Add(parameterMaxSupportingImages);

            //SqlParameter parameterCamID = new SqlParameter("@CamID", SqlDbType.VarChar, 5);
            //parameterCamID.Value = camID;
            //myCommand.Parameters.Add(parameterCamID);
			
			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterCamIntNo = new SqlParameter("@CamIntNo", SqlDbType.Int, 4);
            parameterCamIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterCamIntNo);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int camIntNo = Convert.ToInt32(parameterCamIntNo.Value);
                return camIntNo;
            }
            catch 
			{
				myConnection.Dispose();
                return 0;
            }
        }

        //internal int GetCameraByCameraID(int autIntNo, string cameraID)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand cmd = new SqlCommand("CameraByCameraID", myConnection);

        //    // Mark the Command as a SPROC
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    cmd.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //    cmd.Parameters.Add("@CamID", SqlDbType.VarChar, 5).Value = cameraID;
        //    cmd.Parameters.Add("@CamIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

        //    try
        //    {
        //        myConnection.Open();
        //        cmd.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int camIntNo = (int)cmd.Parameters["@CamIntNo"].Value;

        //        return camIntNo;
        //    }
        //    catch (Exception e)
        //    {

        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

		public SqlDataReader GetCameraList()
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("CameraList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

            //SqlParameter parameterMtrIntNo = new SqlParameter("@MtrIntNo", SqlDbType.Int, 4);
            //parameterMtrIntNo.Value = mtrIntNo;
            //myCommand.Parameters.Add(parameterMtrIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

        public DataSet GetCameraListDS()
        { 
            int totalCount = 0;
            return GetCameraListDS(0, 0, out totalCount);
        }

        //public DataSet GetCameraListDS(int mtrIntNo)
            public DataSet GetCameraListDS(int pageSize, int pageIndex, out int totalCount)
		{
			SqlDataAdapter sqlDACameras = new SqlDataAdapter();
			DataSet dsCameras = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDACameras.SelectCommand = new SqlCommand();
			sqlDACameras.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDACameras.SelectCommand.CommandText = "CameraList";

			// Mark the Command as a SPROC
			sqlDACameras.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int);
            parameterPageSize.Value = pageSize;
            sqlDACameras.SelectCommand.Parameters.Add(parameterPageSize);

            SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int);
            parameterPageIndex.Value = pageIndex;
            sqlDACameras.SelectCommand.Parameters.Add(parameterPageIndex);

            SqlParameter parameterTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            parameterTotalCount.Direction = ParameterDirection.Output;
            sqlDACameras.SelectCommand.Parameters.Add(parameterTotalCount);

            //SqlParameter parameterMtrIntNo = new SqlParameter("@MtrIntNo", SqlDbType.Int, 4);
            //parameterMtrIntNo.Value = mtrIntNo;
            //sqlDACameras.SelectCommand.Parameters.Add(parameterMtrIntNo);

			// Execute the command and close the connection
			sqlDACameras.Fill(dsCameras);
			sqlDACameras.SelectCommand.Connection.Dispose();

            totalCount = (int)(parameterTotalCount.Value == DBNull.Value ? 0 : parameterTotalCount.Value);

			// Return the dataset result
			return dsCameras;		
		}

		
		//public int UpdateCamera (int mtrIntNo,  string camID, string camSerialNo, string lastUser, int camIntNo) 
        public int UpdateCamera(string camSerialNo, int MaxSupportingImages, string lastUser, int camIntNo) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("CameraUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
            //SqlParameter parameterMtrIntNo = new SqlParameter("@MtrIntNo", SqlDbType.Int, 4);
            //parameterMtrIntNo.Value = mtrIntNo;
            //myCommand.Parameters.Add(parameterMtrIntNo);

			SqlParameter parameterCamSerialNo = new SqlParameter("@CamSerialNo", SqlDbType.VarChar, 18);
			parameterCamSerialNo.Value = camSerialNo;
			myCommand.Parameters.Add(parameterCamSerialNo);

            SqlParameter parameterMaxSupportingImages = new SqlParameter("@MaxSupportingImages", SqlDbType.TinyInt, 4);
            parameterMaxSupportingImages.Value = MaxSupportingImages;
            myCommand.Parameters.Add(parameterMaxSupportingImages);

            //SqlParameter parameterCamID = new SqlParameter("@CamID", SqlDbType.VarChar, 5);
            //parameterCamID.Value = camID;
            //myCommand.Parameters.Add(parameterCamID);

			SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
			parameterLastUser.Value = lastUser;
			myCommand.Parameters.Add(parameterLastUser);

			SqlParameter parameterCamIntNo = new SqlParameter("@CamIntNo", SqlDbType.Int);
			parameterCamIntNo.Direction = ParameterDirection.InputOutput;
			parameterCamIntNo.Value = camIntNo;
			myCommand.Parameters.Add(parameterCamIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int CamIntNo = (int)myCommand.Parameters["@CamIntNo"].Value;
				return CamIntNo;
			}
			catch (Exception e)
			{
				myConnection.Dispose();
                string msg = e.Message;
                return 0;
			}
		}

        public String DeleteCamera (int camIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("CameraDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterCamIntNo = new SqlParameter("@CamIntNo", SqlDbType.Int, 4);
			parameterCamIntNo.Value = camIntNo;
			parameterCamIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterCamIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				int CamIntNo = (int)parameterCamIntNo.Value;
				return CamIntNo.ToString();
			}
			catch (Exception e)
			{
				myConnection.Dispose();
				string msg = e.Message;
				return String.Empty;
			}
		}
	}
}

