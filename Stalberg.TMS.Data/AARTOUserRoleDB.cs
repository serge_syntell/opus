﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Stalberg.TMS.Data
{
    public class AARTOUserRoleDB
    {
        protected string connString = string.Empty;

        public AARTOUserRoleDB(string conn)
        {
            this.connString = conn;
        }

        public List<string> GetAARTOUserRoleNameByUserID(int userIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand("AARTO_GetUserRolesByUserIntNo", myConnection);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter paraUserIntNo = new SqlParameter("@userIntNo", SqlDbType.Int, 4);
            paraUserIntNo.Value = userIntNo;
            cmd.Parameters.Add(paraUserIntNo);

            myConnection.Open();

            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            List<string> userRoleNameList = new List<string>();
            string userRoleName = string.Empty;

            while (reader.Read())
            {
                userRoleName = reader["AaUserRoleName"].ToString();
                userRoleNameList.Add(userRoleName);
            }

            return userRoleNameList;
        }
    }
}
