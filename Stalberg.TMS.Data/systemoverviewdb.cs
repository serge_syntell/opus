using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    /// <summary>
    /// Summary description for noticeauditdb
    /// </summary>
    public class SystemOverviewDB
    {
         // Fields
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public SystemOverviewDB(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Gets the relevant info
        /// </summary>
       /// <returns>A <see cref="SqlDataReader"/></returns>
        public SqlDataReader GetData()
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand("SystemOverviewGet", myConnection);
			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;
			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
        }

        public DataSet GetCDLabelDetails(int ARDays,int windowSize)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("GetCDLoadedDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add(new SqlParameter("@ARDays", ARDays));
            com.Parameters.Add(new SqlParameter("@WindowSize", windowSize));

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        public DataSet GetNatisFileDetails(int ARDays, int windowSize)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("GetNatisFileDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add(new SqlParameter("@ARDays", ARDays));
            com.Parameters.Add(new SqlParameter("@WindowSize", windowSize));
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        public DataSet GetVerificationDetails(int ARDays, int windowSize)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("GetVerificationDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add(new SqlParameter("@ARDays", ARDays));
            com.Parameters.Add(new SqlParameter("@WindowSize", windowSize));
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        public DataSet GetAdjudicationDetails(int ARDays, int windowSize)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("GetAdjudicationDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add(new SqlParameter("@ARDays", ARDays));
            com.Parameters.Add(new SqlParameter("@WindowSize", windowSize));
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        public DataSet GetCorrectionDetails(int ARDays, int windowSize)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("GetCorrectionDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add(new SqlParameter("@ARDays", ARDays));
            com.Parameters.Add(new SqlParameter("@WindowSize", windowSize));
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        public DataSet Get1stNoticeDetails(int ARDays, int windowSize)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("Get1stNoticeDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add(new SqlParameter("@ARDays", ARDays));
            com.Parameters.Add(new SqlParameter("@WindowSize", windowSize));
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        public DataSet Get2ndNoticeDetails(int ARDays)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("Get2ndNoticeDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add(new SqlParameter("@ARDays", ARDays));
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        public DataSet GetSummonsDetails(int ARDays)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("GetSummonsDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add(new SqlParameter("@ARDays", ARDays));
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        public DataSet GetCiprusUploadDetails(int ARDays, int windowSize)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("GetCiprusUploadDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add(new SqlParameter("@ARDays", ARDays));
            com.Parameters.Add(new SqlParameter("@WindowSize", windowSize));
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        public String GetNatisFileQty(int ARDays, int windowSize)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("GetNatisFileDetails", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add(new SqlParameter("@ARDays", ARDays));
            com.Parameters.Add(new SqlParameter("@WindowSize", windowSize));
            con.Open();
            SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection);
            int x = 0;
            while (reader.Read())
            {
                x = x + Convert.ToInt32( reader["Quantity"].ToString ());
                
            }
            reader.Close();
            con.Close();
            return x.ToString ();
        }

        public void SystemOverviewCreate(int windowSize, int ARDays, int NoticeDetailDateRule, int SummonsDetailDateRule)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlParameter[] parameters = new SqlParameter[4];
            parameters[0] = new SqlParameter("@WindowSize", windowSize);
            parameters[1] = new SqlParameter("@ARDays", ARDays);
            parameters[2] = new SqlParameter("@NoticeDetailDateRule", NoticeDetailDateRule);
            parameters[3] = new SqlParameter("@SummonsDetailDateRule", SummonsDetailDateRule);

            SqlCommand com = new SqlCommand("SystemOverviewCreate", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddRange(parameters);
            con.Open();
            com.ExecuteNonQuery();
            con.Close();
        }

    }
}

