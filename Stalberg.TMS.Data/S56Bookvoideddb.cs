using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


/* SD : 21.07.2008
 * Page : S56Bookvoideddb.cs
 * Functionality : Datastructure to maintain the S56 Book information
 */

namespace Stalberg.TMS
{
    // SD: 28.07.2008
	//*******************************************************
	//
	// S56 Bookvoided Class
	//
	// A simple data class that encapsulates details about a particular loc 
	//
	//*******************************************************

	public partial class S56BookVoidedDetails
	{      
        public Int32 S56BUIntNo;
        public Int32 S56BIntNo;
        public int S56BUSeqNo;
        public Int32 S56BUToIntNo;
        public string  S56BUSumNo;
        public string S56BUStatus;
        public DateTime S56BUStatusDate;
        public string S56BUStatusComment;
        public DateTime S56BUCourtDate;
        public string S56BUCourtName;
        public string S56BUCourtRoom;
        public Int32 CDIntNo;
	}

    //*******************************************************
    // S56 BookVoidedDB Class
	//*******************************************************

	public partial class S56BookVoidedDB
    {
		string mConstr = "";

        public S56BookVoidedDB(string vConstr)
		{
			mConstr = vConstr;
		}

        // SD : 27.07.2008 
        // Functionality : to fetch the S56BookDetails information from database
        //                 based on the input parameter supplied 


        public S56BookVoidedDetails GetS56BookVoidedDetails(int s56BUIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("S56BookUsageDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameters56BUIntNo = new SqlParameter("@S56BUIntNo", SqlDbType.Int, 4);
            parameters56BUIntNo.Value = s56BUIntNo;
            myCommand.Parameters.Add(parameters56BUIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            S56BookVoidedDetails S56BookVoided = new S56BookVoidedDetails();

            while (result.Read())
            {
                // Populate Struct using Output Params from SPROC
                //S56BUIntNo,S56BUSumNo,S56BUStatus,S56BUStatusDate,S56BUStatusComment
                S56BookVoided.S56BIntNo = Convert.ToInt32( result["S56BIntNo"].ToString());
                if (result["CDIntNo"] != System.DBNull.Value)
                    S56BookVoided.CDIntNo = Convert.ToInt32(result["CDIntNo"].ToString());

                S56BookVoided.S56BIntNo = Convert.ToInt32(result["S56BIntNo"].ToString());
                if (result["S56BUToIntNo"] != System.DBNull.Value)
                    S56BookVoided.S56BUToIntNo = Convert.ToInt32(result["S56BUToIntNo"].ToString());

                S56BookVoided.S56BUSumNo = result["S56BUSumNo"].ToString();
                S56BookVoided.S56BUSeqNo = Convert.ToInt32(result["S56BUSeqNo"].ToString());
                S56BookVoided.S56BUStatus = result["S56BUStatus"].ToString();
                S56BookVoided.S56BUCourtName = result["S56BUCourtName"].ToString();
                S56BookVoided.S56BUCourtRoom = result["S56BUCourtRoom"].ToString();

                if (result["S56BUStatusDate"] != System.DBNull.Value)
                    S56BookVoided.S56BUStatusDate = Convert.ToDateTime(result["S56BUStatusDate"]);

                if (result["S56BUCourtDate"] != System.DBNull.Value)
                    S56BookVoided.S56BUCourtDate = Convert.ToDateTime(result["S56BUCourtDate"]);

                S56BookVoided.S56BUStatusComment = result["S56BUStatusComment"].ToString();

            }
            result.Close();
            return S56BookVoided; 
        }

       //SD: 28.07.2008
       // Functionality: to add s56voided books 
        public int UpdateS56Voided(int s56BIntNo, int s56BUIntNo, int S56BUToIntNo,
            int s56BUSeqNo, char sS56BUStatus, DateTime s56BUStatusDate, string s56BUStatusComment, 
            string lastUser, ref string errMessage )
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("S56_BookUsageAddVoided", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC

            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterS56BIntNo = new SqlParameter("@S56BIntNo", SqlDbType.Int, 4);
            parameterS56BIntNo.Value = s56BIntNo;
            myCommand.Parameters.Add(parameterS56BIntNo);

            SqlParameter parameterS56BUToIntNo = new SqlParameter("@S56BUToIntNo", SqlDbType.Int, 4);
            parameterS56BUToIntNo.Value = S56BUToIntNo;
            myCommand.Parameters.Add(parameterS56BUToIntNo);

            SqlParameter parameterS56BUSeqNo = new SqlParameter("@S56BUSeqNo", SqlDbType.Int, 4);
            parameterS56BUSeqNo.Value = s56BUSeqNo;
            myCommand.Parameters.Add(parameterS56BUSeqNo);

            SqlParameter parameterS56BStatus = new SqlParameter("@S56BUStatus", SqlDbType.Char, 1);
            parameterS56BStatus.Value = sS56BUStatus;
            myCommand.Parameters.Add(parameterS56BStatus);

            SqlParameter parameters56StatusDate = new SqlParameter("@S56BUStatusDate", SqlDbType.SmallDateTime);
            parameters56StatusDate.Value = s56BUStatusDate;
            myCommand.Parameters.Add(parameters56StatusDate);

            SqlParameter parameterS56BStatusComments = new SqlParameter("@S56BUStatusComment", SqlDbType.VarChar,255);
            parameterS56BStatusComments.Value = s56BUStatusComment;
            myCommand.Parameters.Add(parameterS56BStatusComments);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterS56BUIntNo = new SqlParameter("@S56BUIntNo", SqlDbType.Int, 4);
            parameterS56BUIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterS56BUIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the BookID using Output Param from SPROC
                s56BUIntNo = Convert.ToInt32(parameterS56BUIntNo.Value);

                return s56BUIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                errMessage = e.Message;
                return 0;
            }
        }

        public DataSet GetBookVoidedList(int s56BIntNo)
        {
			SqlDataAdapter sqlDAS56Book = new SqlDataAdapter();
            DataSet ds = new DataSet();

			// Create Instance of Connection and Command Object
            sqlDAS56Book.SelectCommand = new SqlCommand();
            sqlDAS56Book.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAS56Book.SelectCommand.CommandText = "S56BookUsageVoidedList";

			// Mark the Command as a SPROC
            sqlDAS56Book.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterS56BIntNo = new SqlParameter("@S56BIntNo", SqlDbType.Int, 4);
            parameterS56BIntNo.Value = s56BIntNo;
            sqlDAS56Book.SelectCommand.Parameters.Add(parameterS56BIntNo);

			// Execute the command and close the connection
            sqlDAS56Book.Fill(ds);
            sqlDAS56Book.SelectCommand.Connection.Dispose();

			// Return the dataset result
            return ds;
     
        }
    }

}

