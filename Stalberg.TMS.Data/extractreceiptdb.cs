using System;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    /// <summary>
    /// Summary description for noticeauditdb
    /// </summary>
    public class extractreceiptdb
    {
        // Fields
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticsDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public extractreceiptdb(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Gets the notice audit results.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="startDate">The start date string (empty for all).</param>
        /// <param name="endDate">The end date string (empty for all).</param>
        /// <returns>A <see cref="SqlDataReader"/></returns>
        public SqlDataReader GetReceiptData(String autIntNo, String UserIntNo, String sYear, String sMonth)
        {

            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("ExtractReceipts", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int).Value = int.Parse(autIntNo);
            com.Parameters.Add("@UserIntNo", SqlDbType.Int).Value = int.Parse(UserIntNo);
            com.Parameters.Add("@InYear", SqlDbType.Int).Value = int.Parse(sYear);
            com.Parameters.Add("@InMonth", SqlDbType.Int).Value = int.Parse(sMonth);

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        /// <summary>
        /// Gets the notice audit results.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="startDate">The start date string (empty for all).</param>
        /// <param name="endDate">The end date string (empty for all).</param>
        /// <returns>A <see cref="SqlDataReader"/></returns>
        public SqlDataReader GetReceiptData(String autIntNo, String UserIntNo, DateTime dtFrom, DateTime dtTo)
        {

            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("ExtractReceiptsByDate", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int).Value = int.Parse(autIntNo);
            com.Parameters.Add("@UserIntNo", SqlDbType.Int).Value = int.Parse(UserIntNo);
            com.Parameters.Add("@DateFrom", SqlDbType.SmallDateTime).Value = dtFrom;
            com.Parameters.Add("@DateTo", SqlDbType.SmallDateTime).Value = dtTo;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }
    }
}

