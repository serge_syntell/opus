﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;

using SIL.eNaTIS.Client;
using SIL.eNaTIS.DAL.Entities;
using SIL.eNaTIS.DAL.Services;
using System.Globalization;


namespace Stalberg.TMS
{
    public class eNatisDB
    {
        string mConstr = "";
        private const int STATUS_NOTICESENT = 101;
        private const int STATUS_ERRORS = 110;
        ImageProcesses imgProcess = null;

        public eNatisDB(string vConstr)
        {
            mConstr = vConstr;
            imgProcess = new ImageProcesses(mConstr);
        }
        // 2013-07-25 add parameter lastUser by Henry
        public bool SendToNTIDatabase(int NotIntNo, string lastUser, out string error)
        {
            bool success = false;
            error = null;
            try
            {
                DataSet ds = this.GetNoticeDataList(NotIntNo);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    SIL.eNaTIS.DAL.Entities.Notice notice = new SIL.eNaTIS.DAL.Entities.Notice();
                    notice.ENaTisGuid = ds.Tables[0].Rows[0]["eNaTISGuid"].ToString() == "" ? Guid.NewGuid() : new Guid(ds.Tables[0].Rows[0]["eNaTISGuid"].ToString());
                    notice.NoticeLoadedDate = DateTime.Parse(ds.Tables[0].Rows[0]["NotLoadDate"].ToString());
                    notice.Filmn = ds.Tables[0].Rows[0]["FilmNo"].ToString();
                    notice.Refn = ds.Tables[0].Rows[0]["FrameNo"].ToString();
                    notice.Province = 4;
                    notice.OffenceDate = DateTime.Parse(ds.Tables[0].Rows[0]["NotOffenceDate"].ToString());
                    notice.OffenceTime = DateTime.Parse(ds.Tables[0].Rows[0]["NotOffenceDate"].ToString());
                    notice.StreetNamea = ds.Tables[0].Rows[0]["StreetA"].ToString();

                    notice.MagisterialCd = ds.Tables[0].Rows[0]["MagisterialCD"] == DBNull.Value ? null : ds.Tables[0].Rows[0]["MagisterialCD"].ToString();

                    if (ds.Tables[0].Rows[0]["StreetB"].ToString().Length > 0)
                        notice.StreetNameb = ds.Tables[0].Rows[0]["StreetB"].ToString();

                    //BD this is not the correct values, the Reg auth comes from notice table
                    //IssAuthority needs to be clarified - at the moment, set to the same.
                    //notice.IssAuthority = Convert.ToInt32(row["AutNo"]);
                    //notice.RegAuthority = Convert.ToInt32(row["AutNo"]);

                    /* Seawen 2013-08-26 changed to use correct one from eNatisUser
                    int notRegisterAuth = 0;//2013-06-28 Heidi changed for 4993
                    int.TryParse(ds.Tables[0].Rows[0]["NotRegisterAuth"].ToString(), out notRegisterAuth);


                    notice.IssAuthority = notRegisterAuth;// Convert.ToInt32(ds.Tables[0].Rows[0]["NotRegisterAuth"]);
                    notice.RegAuthority = notRegisterAuth;// Convert.ToInt32(ds.Tables[0].Rows[0]["NotRegisterAuth"]);*/

                    int chgOffenceCode = 0;//2013-06-28 Heidi changed for 4993
                    int.TryParse(ds.Tables[0].Rows[0]["ChgOffenceCode"].ToString(), out chgOffenceCode);
                    notice.OffenceCode = chgOffenceCode;// Convert.ToInt32(ds.Tables[0].Rows[0]["ChgOffenceCode"]);

                    //update by Rachel 20140812 for 5337
                    //notice.SpeedRead1 = Convert.ToDecimal(ds.Tables[0].Rows[0]["NotSpeed1"]).ToString("f2").PadLeft(6, '0');
                    //notice.SpeedRead2 = Convert.ToDecimal(ds.Tables[0].Rows[0]["NotSpeed2"]).ToString("f2").PadLeft(6, '0');//
                    notice.SpeedRead1 = Convert.ToDecimal(ds.Tables[0].Rows[0]["NotSpeed1"]).ToString("f2", CultureInfo.InvariantCulture).PadLeft(6, '0');
                    notice.SpeedRead2 = Convert.ToDecimal(ds.Tables[0].Rows[0]["NotSpeed2"]).ToString("f2", CultureInfo.InvariantCulture).PadLeft(6, '0');
                    //end update by Rachel 20140812 for 5337

                    notice.Licn = ds.Tables[0].Rows[0]["NotRegNo"].ToString();
                    notice.Infrastructuren = ds.Tables[0].Rows[0]["OfficerNatisNo"].ToString();

                    notice.CityTown = ds.Tables[0].Rows[0]["City"].ToString();

                    //BD need to set AutNo as well.
                    notice.AutNo = Convert.ToString(ds.Tables[0].Rows[0]["AutNo"]);

                    //BD 20120118 - need to get images from file system now and not DB...
                    ScanImageDB scanImageDB = new ScanImageDB(this.mConstr);

                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["VehImageIntNo"].ToString()))
                    {
                        int VehImageIntNo = Convert.ToInt32(ds.Tables[0].Rows[0]["VehImageIntNo"].ToString());
                        //notice.VehImage = scanImageDB.GetScanImageDataFromRemoteServer(VehImageIntNo);
                        //Heidi 20130411 - images must be shrunk before sending to enatis(BD)
                        notice.VehImage = imgProcess.CompressImage(scanImageDB.GetScanImageDataFromRemoteServer(VehImageIntNo), 0);
                    }

                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["LicImageIntNo"].ToString()))
                    {
                        int LicImageIntNo = Convert.ToInt32(ds.Tables[0].Rows[0]["LicImageIntNo"].ToString());
                        notice.LicImage = scanImageDB.GetScanImageDataFromRemoteServer(LicImageIntNo);
                    }

                    notice.Suburb = ds.Tables[0].Rows[0]["Suburb"].ToString();
                    notice.OtherLocInfo = ds.Tables[0].Rows[0]["LocDescr"].ToString();

                    if (!CheckValidNotice(notice, Convert.ToInt32(ds.Tables[0].Rows[0]["NotIntNo"]), out error, lastUser))
                    {
                        error += " Infringement data missing field. NotIntNo:" + Convert.ToInt32(ds.Tables[0].Rows[0]["NotIntNo"]);
                        return false;
                    }

                    // Update charge status values - stamp with filename, date and time
                    eNaTISNotice eNaTISNotice = new eNaTISNotice();

                    //to do: we need to check that we are not saving a duplicate into the SIL_NTI database here
                    notice.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Loaded);
                    success = eNaTISNotice.SaveeNaTISNotice(notice);

                    // Check if there's an error
                    if (!success)
                    {
                        error = "Save notice to database NTI failed";
                    }
                    else
                    {
                        if (this.UpdateInfringementDataChargeStatus(NotIntNo, STATUS_NOTICESENT, notice.ENaTisGuid, out error, lastUser) < 1)  // Barry
                        {
                            success = false;
                            error += " Save notice to database NTI: Update ChargeStatus failed";
                        }
                        else
                        {
                            success = true;
                        }
                    }
                }
                else
                {
                    error = "Cann't find notice data to database NTI";
                }
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                success = false;
            }
            return success;
        }
        // 2013-07-25 add parameter lastUser by Henry
        private bool CheckValidNotice(Notice notice, int NotIntNo,out string error, string lastUser)
        {
            bool bValid = true;
            string strDescription = "";
            error = string.Empty;
            int Code = 0;

            if (notice.OffenceDate == null)
            {
                bValid = false;
                Code = 29;
            }
            if (notice.OffenceTime == null)
            {
                bValid = false;
                Code = 30;
            }
            else if (notice.Filmn.Length < 1)
            {
                bValid = false;
                Code = 37;
            }
            else if (notice.Refn.Length < 1)
            {
                bValid = false;
                Code = 38;
            }
            else if (notice.StreetNamea.Length < 1)
            {
                bValid = false;
                Code = 28;
            }
            else if (notice.Licn.Length < 1)
            {
                bValid = false;
                Code = 31;
            }
            else if (notice.Infrastructuren.Length < 1)
            {
                bValid = false;
                Code = 62;
            }
            else if (notice.SpeedRead1.Length != 6 || notice.SpeedRead2.Length != 6)
            {
                bValid = false;
                Code = 39;
            }
             //Seawen 2013-08-27 this place don't need check the data
            //else if (notice.IssAuthority==0)//Heidi 2013-06-25 added from 72 to 81 for 4993
            //{
            //    bValid = false;
            //    Code = 72;
            //}
            else if (notice.Province == 0)
            {
                bValid = false;
                Code = 73;
            }
            //Seawen 2013-08-27 this place don't need check the data
            //else if (notice.RegAuthority == 0)
            //{
            //    bValid = false;
            //    Code = 74;
            //}
            else if (notice.VehImage==null)
            {
                bValid = false;
                Code = 75;
            }
            else if (notice.LicImage == null)
            {
                bValid = false;
                Code = 76;
            }
            else if (notice.OtherLocInfo.Length<1)
            {
                bValid = false;
                Code = 77;
            }
            else if (notice.StreetNameb.Length < 1)
            {
                bValid = false;
                Code = 78;
            }
            else if (notice.Suburb.Length < 1)
            {
                bValid = false;
                Code = 79;
            }
            else if (string.IsNullOrEmpty(notice.MagisterialCd)||notice.MagisterialCd.Trim().Length < 1)
            {
                bValid = false;
                Code =80;
            }
            else if (notice.OffenceCode==0)
            {
                bValid = false;
                Code = 81;
            }


            if (!bValid)
            {
                ResponseProcessCodeList responseProcessCodeList = new ResponseProcessCodeList();
                strDescription = responseProcessCodeList.GetDescrByCode(Code);

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Suppress))
                {
                    if (this.UpdateMissingFieldInfringementDataChargeStatus(NotIntNo, STATUS_ERRORS, Code, strDescription, out error, lastUser) < 1)
                    {
                        error += "Update missing field infringement data failed. NotIntNo:" + NotIntNo + " Code:" + Code;
                    }
                    scope.Complete();
                }
            }
            return bValid;
        }

        public DataSet GetNoticeDataList(int NotIntNo)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NoticeSearchData_JMPD", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = NotIntNo;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }
            public DataSet GetNoticeDataListWS(int notIntNo) {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("NoticeSearchData_JMPD_WS", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        // 2013-07-25 add parameter lastUser by Henry
        public int UpdateInfringementDataChargeStatus(int NotIntNo, int status, Guid guid, out string error, string lastUser)

        {
            int result = 0;
            error = string.Empty;
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.mConstr);

            SqlCommand myCommand = new SqlCommand("NoticeChargeUpdateStatus_JMPD", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = NotIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
            parameterStatus.Value = status;
            myCommand.Parameters.Add(parameterStatus);

            SqlParameter parameterGuid = new SqlParameter("@Guid", SqlDbType.UniqueIdentifier);
            parameterGuid.Value = guid;
            myCommand.Parameters.Add(parameterGuid);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            try
            {
                myConnection.Open();

                result = myCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                error = e.Message;
                result = -1;
            }
            finally
            {
                myConnection.Close();
            }
            return result;
        }
        // 2013-07-25 add parameter lastUser by Henry
        public int UpdateMissingFieldInfringementDataChargeStatus(int NotIntNo, int status, int Code, string strDesc, out string error, string lastUser)
        {
            int result = 0;
            error = string.Empty;

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.mConstr);
            //if (myConnection.State == ConnectionState.Closed)
            //    myConnection.Open();

            SqlCommand myCommand = new SqlCommand("NoticeChargeMissingFieldUpdateStatus_JMPD", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            myCommand.CommandTimeout = 0;

            // Add Parameters to SPROC
            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int, 4);
            parameterNotIntNo.Value = NotIntNo;
            myCommand.Parameters.Add(parameterNotIntNo);

            SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
            parameterStatus.Value = status;
            myCommand.Parameters.Add(parameterStatus);

            SqlParameter parameterCode = new SqlParameter("@Code", SqlDbType.Int, 4);
            parameterCode.Value = Code;
            myCommand.Parameters.Add(parameterCode);

            SqlParameter parameterDesc = new SqlParameter("@Description", SqlDbType.VarChar, 100);
            parameterDesc.Value = strDesc;
            myCommand.Parameters.Add(parameterDesc);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            try
            {
                myConnection.Open();

                result = myCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                error = e.Message;
                result = -1;
            }
            finally
            {
                myConnection.Close();
            }
            return result;
        }
    }
}
