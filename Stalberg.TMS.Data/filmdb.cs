using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
    public enum ProcessType
    {
        Verification = 1,
        Correction = 2,
        Adjudication = 3
    }

    public class CheckedOutFilm
    {
        // Fields
        private int filmIntNo;
        private string filmNo = string.Empty;
        private int autIntNo;
        private ProcessType processType = 0;
        private List<CheckedOutFrame> frames;
        private CheckedOutFrame currentFrame = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckedOutFilm"/> class.
        /// </summary>
        /// <param name="film">The film.</param>
        /// <param name="connectionString">The connection string.</param>
        public CheckedOutFilm(FilmToConfirm film, string connectionString)
        {
            // Populate the film details
            this.filmIntNo = film.GetFilm();
            this.processType = (ProcessType)film.GetProcessType();

            FilmDB db = new FilmDB(connectionString);
            SqlDataReader reader = db.GetFilmDataForCheckout(this.filmIntNo);
            if (!reader.HasRows)
                throw new ApplicationException("Cannot retrieve the film data!");

            while (reader.Read())
            {
                this.filmNo = (string)reader["FilmNo"];
                this.autIntNo = (int)reader["AutIntNo"];
            }
            reader.NextResult();

            // Get all the frames for the current film
            this.frames = new List<CheckedOutFrame>();
            CheckedOutFrame frame = null;
            while (reader.Read())
            {
                frame = new CheckedOutFrame();
                frame.FrameIntNo = (int)reader["FrameIntNo"];

                this.frames.Add(frame);
            }
            reader.Close();
        }

        /// <summary>
        /// Gets the current frame for editing.
        /// </summary>
        /// <value>The current frame.</value>
        public CheckedOutFrame CurrentFrame
        {
            get { return this.currentFrame; }
        }

        /// <summary>
        /// Gets the frames in the film.
        /// </summary>
        /// <value>The frames.</value>
        public List<CheckedOutFrame> Frames
        {
            get { return this.frames; }
        }

        /// <summary>
        /// Gets the type of process.
        /// </summary>
        /// <value>The type of the process.</value>
        public ProcessType ProcessType
        {
            get { return this.processType; }
        }

        /// <summary>
        /// Gets or sets the aut int no.
        /// </summary>
        /// <value>The aut int no.</value>
        public int AutIntNo
        {
            get { return this.autIntNo; }
            set { this.autIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the film number.
        /// </summary>
        /// <value>The film no.</value>
        public string FilmNumber
        {
            get { return this.filmNo; }
            set { this.filmNo = value; }
        }

        /// <summary>
        /// Gets or sets the film int no.
        /// </summary>
        /// <value>The film int no.</value>
        public int FilmIntNo
        {
            get { return this.filmIntNo; }
            set { this.filmIntNo = value; }
        }

        /// <summary>
        /// Sets the current frame.
        /// </summary>
        /// <param name="frameInt">The frame int.</param>
        public void SetCurrentFrame(int frameInt)
        {
            foreach (CheckedOutFrame frame in this.frames)
            {
                if (frame.FrameIntNo == frameInt)
                {
                    frame.IsCurrent = true;
                    this.currentFrame = frame;
                }
                else
                    frame.IsCurrent = false;
            }
        }
    }

    public class CheckedOutFrame
    {
        // Fields
        private bool isCurrent = false;
        private int frameIntNo;

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckedOutFrame"/> class.
        /// </summary>
        public CheckedOutFrame()
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is current.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is current; otherwise, <c>false</c>.
        /// </value>
        public bool IsCurrent
        {
            get { return this.isCurrent; }
            set { this.isCurrent = value; }
        }

        /// <summary>
        /// Gets or sets the frame int no.
        /// </summary>
        /// <value>The frame int no.</value>
        public int FrameIntNo
        {
            get { return this.frameIntNo; }
            set { this.frameIntNo = value; }
        }
    }

    /// <summary>
    /// Represents the current film to confirm
    /// </summary>
    public class FilmToConfirm
    {
        private int filmIntNo;
        private int userIntNo;
        private int autIntNo;
        private DateTime firstOffenceDate;
        private bool validated;
        private string filmLoadResponseDate;
        private int processType;      //1 = Verification; 2 = Correction; 3 = Adjudication

        /// <summary>
        /// Initializes a new instance of the <see cref="FilmToConfirm"/> class.
        /// </summary>
        /// <param name="filmInt">The film int.</param>
        /// <param name="userInt">The user int.</param>
        /// <param name="autInt">The aut int.</param>
        /// <param name="firstOffDate">The first off date.</param>
        /// <param name="hasbeen">if set to <c>true</c> it has been processed.</param>
        /// <param name="responseDate">The response date.</param>
        /// <param name="type">The type.</param>
        public FilmToConfirm(int filmInt, int userInt, int autInt, DateTime firstOffDate, bool hasBeen,
            string responseDate, int type)
        {
            this.filmIntNo = filmInt;
            this.userIntNo = userInt;
            this.autIntNo = autInt;
            this.firstOffenceDate = firstOffDate;
            this.validated = hasBeen;
            this.filmLoadResponseDate = responseDate;
            this.processType = type;
        }

        public int GetFilm()
        {
            return filmIntNo;
        }

        public int GetUser()
        {
            return userIntNo;
        }

        public int GetAuthority()
        {
            return autIntNo;
        }

        public int GetProcessType()
        {
            return processType;
        }

        public DateTime GetFirstOffenceDate()
        {
            return firstOffenceDate;
        }

        public bool GetValidated()
        {
            return validated;
        }

        public string GetResponseDate()
        {
            return filmLoadResponseDate;
        }
    }

    /// <summary>
    /// Represents the details of a particular film
    /// </summary>
    public class FilmDetails
    {
        public Int32 FilmIntNo;
        public Int32 AutIntNo;
        public Int32 ConIntNo;
        public string FilmNo;
        public string CDLabel;
        public int NoOfFrames;
        public int NoOfScans;
        public string MultipleFrames;
        public string MultipleViolations;
        public string FilmDescr;
        public int LastRefNo;
        public string FilmType;
        public DateTime FilmLoadDateTime;
        public string FilmLoadType;
        public string FilmLoadFileName;
        public string FirstOffenceDate;
        public DateTime FilmConfirmDateTime;
        public string FilmConfirmUser;
        //public DateTime CreateInterfaceDateTime;
        //public string CreateInterfaceUser;
        //public string CreateInterfaceFileName;
        //public DateTime LoadTMSFileDateTime;
        //public string LoadTMSFileUser;
        //public string LoadTMSFileName;
        //public string TMSStatus; 
        public DateTime CreateLABATFileDateTime;
        public string CreateLABATFileUser;
        public string CreateLABATFileName;
        public string ValidateDataUser;
        public DateTime ValidateDataDateTime;
        public string LastUser;
        public string CreateTPExIntUser;
        public string CreateTPExIntFileName;
        public DateTime CreateTPExIntDate;
        public DateTime FilmLoadResponseDate;
        public string FilmLoadResponseFilename;
        public string FilmLoadResponseUser;
        public int FilmLoadResponseStatus;
        public string FirstOffenceYear;
        public string FirstOffenceMonth;
        public string AuthorityCode;
        public string FilmLockUser;
    }

    public class FilmDB
    {
        // Fields
        string mConstr = String.Empty;

        public FilmDB(string vConstr)
        {
            mConstr = vConstr;
        }

        public FilmDetails GetFilmDetails(Int64 filmIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FilmDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Value = filmIntNo;
            myCommand.Parameters.Add(parameterFilmIntNo);

            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                // Create CustomerDetails Struct
                FilmDetails myFilmDetails = new FilmDetails();

                while (result.Read())
                {
                    // Populate Struct using Output Params from SPROC
                    myFilmDetails.FilmIntNo = Convert.ToInt32(result["FilmIntNo"]);
                    myFilmDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
                    myFilmDetails.ConIntNo = Convert.ToInt32(result["ConIntNo"]);
                    myFilmDetails.FilmNo = result["FilmNo"].ToString();
                    myFilmDetails.CDLabel = result["CDLabel"].ToString();
                    myFilmDetails.NoOfFrames = Convert.ToInt32(result["NoOfFrames"]);
                    myFilmDetails.NoOfScans = Convert.ToInt32(result["NoOfScans"]);
                    //myFilmDetails.ConfirmViolations = result["ConfirmViolations"].ToString();
                    myFilmDetails.MultipleFrames = result["MultipleFrames"].ToString();
                    myFilmDetails.MultipleViolations = result["MultipleViolations"].ToString();
                    myFilmDetails.FilmDescr = result["FilmDescr"].ToString();
                    myFilmDetails.LastRefNo = Convert.ToInt32(result["LastRefNo"]);
                    if (result["FilmLoadDateTime"] != System.DBNull.Value)
                        myFilmDetails.FilmLoadDateTime = Convert.ToDateTime(result["FilmLoadDateTime"]);
                    if (result["FilmLoadType"] != System.DBNull.Value)
                        myFilmDetails.FilmLoadType = result["FilmLoadType"].ToString();
                    myFilmDetails.FilmLoadFileName = result["FilmLoadFileName"].ToString();
                    if (result["FirstOffenceDate"] != System.DBNull.Value)                    
                        myFilmDetails.FirstOffenceDate = result["FirstOffenceDate"].ToString();
                    if (result["Film1stOffenceDateYear"] != System.DBNull.Value)
                        myFilmDetails.FirstOffenceYear = result["Film1stOffenceDateYear"].ToString();
                    if (result["Film1stOffenceDateMonth"] != System.DBNull.Value)
                        myFilmDetails.FirstOffenceMonth = result["Film1stOffenceDateMonth"].ToString();
                    //if (result["FilmConfirmDateTime"] != System.DBNull.Value)
                    //    myFilmDetails.FilmConfirmDateTime = Convert.ToDateTime(result["FilmConfirmDateTime"]);
                    //myFilmDetails.FilmConfirmUser = result["FilmConfirmUser"].ToString();
                    if (result["CreateTPExIntDate"] != System.DBNull.Value)
                        myFilmDetails.CreateTPExIntDate = Convert.ToDateTime(result["CreateTPExIntDate"]);
                    myFilmDetails.CreateTPExIntUser = result["CreateTPExIntUser"].ToString();
                    myFilmDetails.CreateTPExIntFileName = result["CreateTPExIntFileName"].ToString();
                    myFilmDetails.FilmLoadResponseUser = result["FilmLoadResponseUser"].ToString();
                    if (result["FilmLoadResponseDate"] != System.DBNull.Value)
                        myFilmDetails.FilmLoadResponseDate = Convert.ToDateTime(result["FilmLoadResponseDate"]);
                    myFilmDetails.FilmLoadResponseFilename = result["FilmLoadResponseFilename"].ToString();
                    if (result["FilmLoadResponseStatus"] != System.DBNull.Value)
                        myFilmDetails.FilmLoadResponseStatus = Convert.ToInt32(result["FilmLoadResponseStatus"]);
                    //if (result["CreateLABATFileDateTime"] != System.DBNull.Value)
                    //    myFilmDetails.CreateLABATFileDateTime = Convert.ToDateTime(result["CreateLABATFileDateTime"]);
                    //myFilmDetails.CreateLABATFileUser = result["CreateLABATFileUser"].ToString();
                    //myFilmDetails.CreateLABATFileName = result["CreateLABATFileName"].ToString();
                    if (result["ValidateDataDateTime"] != System.DBNull.Value)
                        myFilmDetails.ValidateDataDateTime = Convert.ToDateTime(result["ValidateDataDateTime"]);
                    myFilmDetails.ValidateDataUser = result["ValidateDataUser"].ToString();
                    myFilmDetails.FilmType = result["FilmType"].ToString();
                    if (result["FilmLockUser"] != System.DBNull.Value)
                        myFilmDetails.FilmLockUser = result["FilmLockUser"].ToString();
                }

                result.Close();
                return myFilmDetails;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return null;
            }
        }


        public int AddFilm(int autIntNo, int conIntNo, string filmNo, string cdLabel, int noOfFrames,
            int noOfScans, string multipleFrames, string multipleViolations,
            string filmDescr, int lastRefNo, string filmType, DateTime filmLoadDateTime, string filmLoadType, string filmLoadFileName,
            DateTime firstOffenceDate, string lastUser, ref string errorMsg, int filmIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FilmAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterConIntNo = new SqlParameter("@ConIntNo", SqlDbType.Int, 4);
            parameterConIntNo.Value = conIntNo;
            myCommand.Parameters.Add(parameterConIntNo);

            // jerry 2012-02-06 change FilmNo length from 10 to 25
            SqlParameter parameterFilmNo = new SqlParameter("@FilmNo", SqlDbType.VarChar, 25);
            parameterFilmNo.Value = filmNo;
            myCommand.Parameters.Add(parameterFilmNo);

            SqlParameter parameterCDLabel = new SqlParameter("@CDLabel", SqlDbType.VarChar, 25);
            parameterCDLabel.Value = cdLabel;
            myCommand.Parameters.Add(parameterCDLabel);

            SqlParameter parameterNoOfFrames = new SqlParameter("@NoOfFrames", SqlDbType.Int);
            parameterNoOfFrames.Value = noOfFrames;
            myCommand.Parameters.Add(parameterNoOfFrames);

            SqlParameter parameterNoOfScans = new SqlParameter("@NoOfScans", SqlDbType.Int);
            parameterNoOfScans.Value = noOfScans;
            myCommand.Parameters.Add(parameterNoOfScans);

            SqlParameter parameterMultipleFrames = new SqlParameter("@MultipleFrames", SqlDbType.Char, 1);
            parameterMultipleFrames.Value = multipleFrames;
            myCommand.Parameters.Add(parameterMultipleFrames);

            SqlParameter parameterMultipleViolations = new SqlParameter("@MultipleViolations", SqlDbType.Char, 1);
            parameterMultipleViolations.Value = multipleViolations;
            myCommand.Parameters.Add(parameterMultipleViolations);

            SqlParameter parameterFilmDescr = new SqlParameter("@FilmDescr", SqlDbType.VarChar, 255);
            parameterFilmDescr.Value = filmDescr;
            myCommand.Parameters.Add(parameterFilmDescr);

            SqlParameter parameterLastRefNo = new SqlParameter("@LastRefNo", SqlDbType.Int);
            parameterLastRefNo.Value = lastRefNo;
            myCommand.Parameters.Add(parameterLastRefNo);

            SqlParameter parameterTruvellaFlag = new SqlParameter("@FilmType", SqlDbType.Char, 1);
            parameterTruvellaFlag.Value = filmType;
            myCommand.Parameters.Add(parameterTruvellaFlag);

            SqlParameter parameterFilmLoadDateTime = new SqlParameter("@FilmLoadDateTime", SqlDbType.SmallDateTime);
            parameterFilmLoadDateTime.Value = filmLoadDateTime;
            myCommand.Parameters.Add(parameterFilmLoadDateTime);

            SqlParameter parameterFilmLoadType = new SqlParameter("@FilmLoadType", SqlDbType.Char, 1);
            parameterFilmLoadType.Value = filmLoadType;
            myCommand.Parameters.Add(parameterFilmLoadType);

            SqlParameter parameterFilmLoadFileName = new SqlParameter("@FilmLoadFileName", SqlDbType.VarChar, 15);
            parameterFilmLoadFileName.Value = filmLoadFileName;
            myCommand.Parameters.Add(parameterFilmLoadFileName);

            SqlParameter parameterFirstOffenceDate = new SqlParameter("@FirstOffenceDate", SqlDbType.SmallDateTime);
            parameterFirstOffenceDate.Value = firstOffenceDate;
            myCommand.Parameters.Add(parameterFirstOffenceDate);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Direction = ParameterDirection.InputOutput;
            parameterFilmIntNo.Value = filmIntNo;
            myCommand.Parameters.Add(parameterFilmIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int addFilmIntNo = Convert.ToInt32(parameterFilmIntNo.Value);

                return addFilmIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                errorMsg = e.Message;
                return 0;
            }

        }


        //dls 060515 - doesn't seem to be used - commented out for now
        //public SqlDataReader GetFilmList(int autIntNo, string colName, string isNull)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("FilmList", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterColName = new SqlParameter("@ColName", SqlDbType.VarChar, 50);
        //    parameterColName.Value = colName;
        //    myCommand.Parameters.Add(parameterColName);

        //    SqlParameter parameterIsNull = new SqlParameter("@IsNull", SqlDbType.Char, 1);
        //    parameterIsNull.Value = isNull;
        //    myCommand.Parameters.Add(parameterIsNull);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the data reader result
        //    return result;
        //}

        public SqlDataReader GetFilmNoByAuthority(int autIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("GetFilmNoByAuthority", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        public SqlDataReader GetFilmNoByAuthorityForNatis(int autIntNo, int startStatus, int endStatus)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("GetFilmNoByAuthorityForNatis", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterStartStatus = new SqlParameter("@StartStatus", SqlDbType.Int, 4);
            parameterStartStatus.Value = startStatus;
            myCommand.Parameters.Add(parameterStartStatus);

            SqlParameter parameterEndStatus = new SqlParameter("@EndStatus", SqlDbType.Int, 4);
            parameterEndStatus.Value = endStatus;
            myCommand.Parameters.Add(parameterEndStatus);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the data reader result
            return result;
        }

        public int CheckFilmExists(int autIntNo, string filmNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("CheckFilmExists", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            // jerry 2012-02-06 change FilmNo length from 10 to 25
            SqlParameter parameterFilmNo = new SqlParameter("@FilmNo", SqlDbType.VarChar, 25);
            parameterFilmNo.Value = filmNo;
            myCommand.Parameters.Add(parameterFilmNo);

            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterFilmIntNo);
            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int FilmIntNo = Convert.ToInt32(parameterFilmIntNo.Value);

                return FilmIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        // 2013-07-24 add parameter lastUser by Henry
        public int UpdateFilmLoadStatus(int filmIntNo, int status, string errDescr, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FilmLoadStatusUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterStatus = new SqlParameter("@Status", SqlDbType.Int, 4);
            parameterStatus.Value = status;
            myCommand.Parameters.Add(parameterStatus);

            SqlParameter parameterErrDescr = new SqlParameter("@ErrDescr", SqlDbType.VarChar, 255);
            parameterErrDescr.Value = errDescr;
            myCommand.Parameters.Add(parameterErrDescr);

            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Value = filmIntNo;
            parameterFilmIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterFilmIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int FilmIntNo = Convert.ToInt32(parameterFilmIntNo.Value);

                return FilmIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }
        // 2013-07-19 comment by Henry for useless
        //public int GetFramesNotConfirmed(int filmIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("FramesNotConfirmed", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
        //    parameterFilmIntNo.Value = filmIntNo;
        //    myCommand.Parameters.Add(parameterFilmIntNo);

        //    SqlParameter parameterNoOfFrames = new SqlParameter("@NoOfFrames", SqlDbType.Int, 4);
        //    parameterNoOfFrames.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterNoOfFrames);
        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int noOfFrames = Convert.ToInt32(parameterNoOfFrames.Value);

        //        return noOfFrames;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        /// <summary>
        /// Gets the next film for verification.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="status">The status.</param>
        /// <returns>A <see cref="SqlDataReader"/>.</returns>
        public SqlDataReader GetNextFilmForVerification(int autIntNo, ValidationStatus status)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("FilmsToVerify", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@Status", SqlDbType.Int, 4).Value = (int)status;

            // Execute the command
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public SqlDataReader GetNextFilmForVerificationByUser(int autIntNo, ValidationStatus status, string user)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("FilmsToVerifyByUser", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@Status", SqlDbType.Int, 4).Value = (int)status;
            com.Parameters.Add("@User", SqlDbType.VarChar, 50).Value = user;

            // Execute the command
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        /// <summary>
        /// Gets the next film for adjudication.
        /// </summary>
        /// <param name="autIntNo">The authority int no.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        public SqlDataReader GetNextFilmForAdjudication(int autIntNo, ValidationStatus status)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("FilmsToAdjudicate", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@Status", SqlDbType.Int, 4).Value = (int)status;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public SqlDataReader GetNextFilmForAdjudicationByUser(int autIntNo, ValidationStatus status, string user)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("FilmsToAdjudicateByUser", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@Status", SqlDbType.Int, 4).Value = (int)status;
            com.Parameters.Add("@User", SqlDbType.VarChar, 50).Value = user;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public SqlDataReader GetNextFilmForInvestigationByUser(int autIntNo, ValidationStatus status, string user)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("FilmsToInvestigateByUser", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@Status", SqlDbType.Int, 4).Value = (int)status;
            com.Parameters.Add("@User", SqlDbType.VarChar, 50).Value = user;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public DataSet GetFilmStatsDS(int autIntNo)
        {
            SqlDataAdapter sqlDAFilm = new SqlDataAdapter();
            DataSet dsFilm = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAFilm.SelectCommand = new SqlCommand();
            sqlDAFilm.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAFilm.SelectCommand.CommandText = "FilmStatisticsGet";

            // Mark the Command as a SPROC
            sqlDAFilm.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDAFilm.SelectCommand.Parameters.Add(parameterAutIntNo);

            // Execute the command and close the connection
            sqlDAFilm.Fill(dsFilm);
            sqlDAFilm.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsFilm;
        }

        //public SqlDataReader GetFilmStats(int autIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("FilmStatistics", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    // Execute the command
        //    myConnection.Open();
        //    SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

        //    // Return the datareader result
        //    return result;
        //}

        // 2013-07-19 comment by Henry for useless
        //public DataSet GetFilmListDS(int autIntNo, string orderBy)
        //{
        //    SqlDataAdapter sqlDAFilm = new SqlDataAdapter();
        //    DataSet dsFilm = new DataSet();

        //    // Create Instance of Connection and Command Object
        //    sqlDAFilm.SelectCommand = new SqlCommand();
        //    sqlDAFilm.SelectCommand.Connection = new SqlConnection(mConstr);
        //    sqlDAFilm.SelectCommand.CommandText = "FilmList";

        //    // Mark the Command as a SPROC
        //    sqlDAFilm.SelectCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    sqlDAFilm.SelectCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterOrderBy = new SqlParameter("@OrderBy", SqlDbType.VarChar, 10);
        //    parameterOrderBy.Value = orderBy;
        //    sqlDAFilm.SelectCommand.Parameters.Add(parameterOrderBy);

        //    // Execute the command and close the connection
        //    sqlDAFilm.Fill(dsFilm);
        //    sqlDAFilm.SelectCommand.Connection.Dispose();

        //    // Return the dataset result
        //    return dsFilm;
        //}

        // public int UpdateFilm(int filmIntNo, int autIntNo, int conIntNo, string filmNo, string cdLabel, int noOfFrames,
        //     int noOfScans, string confirmViolations, string multipleFrames, string multipleViolations,
        //     string filmDescr, int lastRefNo, string filmType, DateTime filmLoadDateTime, string filmLoadType, string filmLoadFileName,
        //     string firstOffenceDate, DateTime filmConfirmDateTime, string filmConfirmUser, DateTime CreateTPExIntDate,
        //     string CreateTPExIntUser, string CreateTPExIntFileName, DateTime loadTMSFileDateTime, string loadTMSFileUser,
        //     string loadTMSFileName, string tmsStatus, DateTime createLABATFileDateTime, string createLABATFileUser,
        //     string createLABATFileName, string lastUser)
        //{

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("FilmUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterConIntNo = new SqlParameter("@ConIntNo", SqlDbType.Int, 4);
        //    parameterConIntNo.Value = conIntNo;
        //    myCommand.Parameters.Add(parameterConIntNo);

        //    SqlParameter parameterFilmNo = new SqlParameter("@FilmNo", SqlDbType.VarChar, 10);
        //    parameterFilmNo.Value = filmNo;
        //    myCommand.Parameters.Add(parameterFilmNo);

        //    SqlParameter parameterCDLabel = new SqlParameter("@CDLabel", SqlDbType.VarChar, 25);
        //    parameterCDLabel.Value = cdLabel;
        //    myCommand.Parameters.Add(parameterCDLabel);

        //    SqlParameter parameterNoOfFrames = new SqlParameter("@NoOfFrames", SqlDbType.Int);
        //    parameterNoOfFrames.Value = noOfFrames;
        //    myCommand.Parameters.Add(parameterNoOfFrames);

        //    SqlParameter parameterNoOfScans = new SqlParameter("@NoOfScans", SqlDbType.Int);
        //    parameterNoOfScans.Value = noOfScans;
        //    myCommand.Parameters.Add(parameterNoOfScans);

        //    SqlParameter parameterConfirmViolations = new SqlParameter("@ConfirmViolations", SqlDbType.Char, 1);
        //    parameterConfirmViolations.Value = confirmViolations;
        //    myCommand.Parameters.Add(parameterConfirmViolations);

        //    SqlParameter parameterMultipleFrames = new SqlParameter("@MultipleFrames", SqlDbType.Char, 1);
        //    parameterMultipleFrames.Value = multipleFrames;
        //    myCommand.Parameters.Add(parameterMultipleFrames);

        //    SqlParameter parameterMultipleViolations = new SqlParameter("@MultipleViolations", SqlDbType.Char, 1);
        //    parameterMultipleViolations.Value = multipleViolations;
        //    myCommand.Parameters.Add(parameterMultipleViolations);

        //    SqlParameter parameterFilmDescr = new SqlParameter("@FilmDescr", SqlDbType.VarChar, 255);
        //    parameterFilmDescr.Value = filmDescr;
        //    myCommand.Parameters.Add(parameterFilmDescr);

        //    SqlParameter parameterLastRefNo = new SqlParameter("@LastRefNo", SqlDbType.Int);
        //    parameterLastRefNo.Value = lastRefNo;
        //    myCommand.Parameters.Add(parameterLastRefNo);

        //    SqlParameter parameterTruvellaFlag = new SqlParameter("@FilmType", SqlDbType.Char, 1);
        //    parameterTruvellaFlag.Value = filmType;
        //    myCommand.Parameters.Add(parameterTruvellaFlag);

        //    SqlParameter parameterFilmLoadDateTime = new SqlParameter("@FilmLoadDateTime", SqlDbType.SmallDateTime);
        //    parameterFilmLoadDateTime.Value = filmLoadDateTime;
        //    myCommand.Parameters.Add(parameterFilmLoadDateTime);

        //    SqlParameter parameterFilmLoadType = new SqlParameter("@FilmLoadType", SqlDbType.Char, 1);
        //    parameterFilmLoadType.Value = filmLoadType;
        //    myCommand.Parameters.Add(parameterFilmLoadType);

        //    SqlParameter parameterFilmLoadFileName = new SqlParameter("@FilmLoadFileName", SqlDbType.VarChar, 15);
        //    parameterFilmLoadFileName.Value = filmLoadFileName;
        //    myCommand.Parameters.Add(parameterFilmLoadFileName);

        //    SqlParameter parameterFirstOffenceDate = new SqlParameter("@FirstOffenceDate", SqlDbType.SmallDateTime);
        //    parameterFirstOffenceDate.Value = firstOffenceDate;
        //    myCommand.Parameters.Add(parameterFirstOffenceDate);

        //    SqlParameter parameterFilmConfirmDateTime = new SqlParameter("@FilmConfirmDateTime", SqlDbType.SmallDateTime);
        //    parameterFilmConfirmDateTime.Value = filmConfirmDateTime;
        //    myCommand.Parameters.Add(parameterFilmConfirmDateTime);

        //    SqlParameter parameterFilmConfirmUser = new SqlParameter("@FilmConfirmUser", SqlDbType.VarChar, 20);
        //    parameterFilmConfirmUser.Value = filmConfirmUser;
        //    myCommand.Parameters.Add(parameterFilmConfirmUser);

        //    SqlParameter parameterCreateTPExIntDate = new SqlParameter("@CreateTPExIntDate", SqlDbType.SmallDateTime);
        //    parameterCreateTPExIntDate.Value = CreateTPExIntDate;
        //    myCommand.Parameters.Add(parameterCreateTPExIntDate);

        //    SqlParameter parameterCreateTPExIntUser = new SqlParameter("@CreateTPExIntUser", SqlDbType.VarChar, 20);
        //    parameterCreateTPExIntUser.Value = CreateTPExIntUser;
        //    myCommand.Parameters.Add(parameterCreateTPExIntUser);

        //    SqlParameter parameterCreateTPExIntFileName = new SqlParameter("@CreateTPExIntFileName", SqlDbType.VarChar, 15);
        //    parameterCreateTPExIntFileName.Value = CreateTPExIntFileName;
        //    myCommand.Parameters.Add(parameterCreateTPExIntFileName);

        //    SqlParameter parameterLoadTMSFileDateTime = new SqlParameter("@LoadTMSFileDateTime", SqlDbType.SmallDateTime);
        //    parameterLoadTMSFileDateTime.Value = loadTMSFileDateTime;
        //    myCommand.Parameters.Add(parameterLoadTMSFileDateTime);

        //    SqlParameter parameterLoadTMSFileUser = new SqlParameter("@LoadTMSFileUser", SqlDbType.VarChar, 20);
        //    parameterLoadTMSFileUser.Value = loadTMSFileUser;
        //    myCommand.Parameters.Add(parameterLoadTMSFileUser);

        //    SqlParameter parameterLoadTMSFileName = new SqlParameter("@LoadTMSFileName", SqlDbType.VarChar, 15);
        //    parameterLoadTMSFileName.Value = loadTMSFileName;
        //    myCommand.Parameters.Add(parameterLoadTMSFileName);

        //    SqlParameter parameterTMSStatus = new SqlParameter("@TMSStatus", SqlDbType.VarChar, 20);
        //    parameterTMSStatus.Value = tmsStatus;
        //    myCommand.Parameters.Add(parameterTMSStatus);

        //    SqlParameter parameterCreateLABATFileDateTime = new SqlParameter("@CreateLABATFileDateTime", SqlDbType.SmallDateTime);
        //    parameterCreateLABATFileDateTime.Value = createLABATFileDateTime;
        //    myCommand.Parameters.Add(parameterCreateLABATFileDateTime);

        //    SqlParameter parameterCreateLABATFileUser = new SqlParameter("@CreateLABATFileUser", SqlDbType.VarChar, 20);
        //    parameterCreateLABATFileUser.Value = createLABATFileUser;
        //    myCommand.Parameters.Add(parameterCreateLABATFileUser);

        //    SqlParameter parameterCreateLABATFileName = new SqlParameter("@CreateLABATFileName", SqlDbType.VarChar, 15);
        //    parameterCreateLABATFileName.Value = createLABATFileName;
        //    myCommand.Parameters.Add(parameterCreateLABATFileName);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
        //    parameterFilmIntNo.Direction = ParameterDirection.InputOutput;
        //    parameterFilmIntNo.Value = filmIntNo;
        //    myCommand.Parameters.Add(parameterFilmIntNo);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int updFilmIntNo = (int)myCommand.Parameters["@FilmIntNo"].Value;

        //        return updFilmIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

        public void FinaliseFilmAdjudication(int filmIntNo, string natisLast, string lastUser, ref string errMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FilmAdjudicateFinalise", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Value = filmIntNo;
            myCommand.Parameters.Add(parameterFilmIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            myCommand.Parameters.Add("@NatisLast", SqlDbType.Char, 1).Value = natisLast;

            //SqlParameter parameterCVPhase = new SqlParameter("@CVPhase", SqlDbType.Int, 4);
            //parameterCVPhase.Value = cvPhase;
            //myCommand.Parameters.Add(parameterCVPhase);

            //SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int, 4);
            //parameterSuccess.Direction = ParameterDirection.InputOutput;
            //myCommand.Parameters.Add(parameterSuccess);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                errMessage = e.Message;
                return ;
            }
        }

        public int FinaliseFilmVerification(int filmIntNo, int cvPhase, string lastUser, ref string errMessage, string natisLast)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FilmVerifyFinalise", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Value = filmIntNo;
            myCommand.Parameters.Add(parameterFilmIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterNatisLast = new SqlParameter("@NatisLast", SqlDbType.Char, 1);
            parameterNatisLast.Value = natisLast;
            myCommand.Parameters.Add(parameterNatisLast);

            //SqlParameter parameterCVPhase = new SqlParameter("@CVPhase", SqlDbType.Int, 4);
            //parameterCVPhase.Value = cvPhase;
            //myCommand.Parameters.Add(parameterCVPhase);

            SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int, 4);
            parameterSuccess.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterSuccess);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int success = (int)parameterSuccess.Value;

                return success;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                errMessage = e.Message;
                return -1;
            }
        }

        public int DeleteFilm(int filmIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FilmDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Value = filmIntNo;
            parameterFilmIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterFilmIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int delFilmIntNo = (int)parameterFilmIntNo.Value;

                return delFilmIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// Gets the list of films for a Local Authority for Verification of Adjudication
        /// </summary>
        /// <param name="autIntNo">The Authority int no.</param>
        /// <param name="hasBeenVerified">If set to <c>true</c> then the list is of those films that have been verified.</param>
        /// <param name="isFishpond">20130111 Added by Nancy for distinguish frames of verification bin from those of fishpond </param>
        /// <returns>A SQL Data Reader containing the list of films</returns>
        public SqlDataReader GetAuthorityFilmsForVerification(int autIntNo, ValidationStatus status, bool isFishpond = false)
        {
            // Create the data access objects
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("FilmVerificationList", con);
            com.CommandType = CommandType.StoredProcedure;

            // Set the parameters
            int intStatus = (int)status;
            com.Parameters.Add("@AuthIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@Status", SqlDbType.Int, 4).Value = intStatus;
            com.Parameters.Add("@isFishpond", SqlDbType.VarChar, 10).Value = isFishpond.ToString().ToUpper();//20130111 added by Nancy for distinguish frames of verification bin from those of fishpond

            // Get the date
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public DataSet GetAuthorityFilmsForVerificationDS(int autIntNo, ValidationStatus status)
        {
            SqlDataAdapter sqlDAFilm = new SqlDataAdapter();
            DataSet dsFilm = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAFilm.SelectCommand = new SqlCommand();
            sqlDAFilm.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAFilm.SelectCommand.CommandText = "FilmVerificationList";

            // Mark the Command as a SPROC
            sqlDAFilm.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            int intStatus = (int)status;
            sqlDAFilm.SelectCommand.Parameters.Add("@AuthIntNo", SqlDbType.Int, 4).Value = autIntNo;
            sqlDAFilm.SelectCommand.Parameters.Add("@Status", SqlDbType.Int, 4).Value = intStatus;

            // Execute the command and close the connection
            sqlDAFilm.Fill(dsFilm);
            sqlDAFilm.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsFilm;
        }
        /// <summary>
        /// Gets the film data for a checked out film.
        /// </summary>
        /// <param name="filmIntNo">The film int no.</param>
        /// <returns></returns>
        public SqlDataReader GetFilmDataForCheckout(int filmIntNo)
        {
            SqlConnection con = new SqlConnection(this.mConstr);
            SqlCommand com = new SqlCommand("FilmGetDataForCheckout", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@FilmIntNo", SqlDbType.Int, 4).Value = filmIntNo;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        // 2013-07-24 add parameter lastUser by Henry
        public Int32 LockFilm(string filmNo, ref string errMessage, string lastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FilmLockToSystem", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            // jerry 2012-02-06 change FilmNo length from 10 to 25
            myCommand.Parameters.Add("@FilmNo", SqlDbType.VarChar, 25).Value = filmNo;
            myCommand.Parameters.Add("@FilmIntNo", SqlDbType.Int, 4).Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;
            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int updFilmIntNo = (int)myCommand.Parameters["@FilmIntNo"].Value;

                return updFilmIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                errMessage = e.Message;
                return 0;
            }
        }

        //dls 071102 - add an override that checks the rowversion
        public Int32 LockUser(int filmIntNo, string userName, Int64 rowversion)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FilmLockUser", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@FilmIntNo", SqlDbType.Int, 4).Value = filmIntNo;
            myCommand.Parameters.Add("@UserName", SqlDbType.VarChar, 50).Value = userName;
            myCommand.Parameters.Add("@Rowversion", SqlDbType.BigInt, 8).Value = rowversion;
            myCommand.Parameters["@FilmIntNo"].Direction = ParameterDirection.InputOutput;
            
            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int updFilmIntNo = (int)myCommand.Parameters["@FilmIntNo"].Value;

                return updFilmIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

         public Int32 LockUser(int filmIntNo, string userName)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FilmLockUser", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            myCommand.Parameters.Add("@FilmIntNo", SqlDbType.Int, 4).Value = filmIntNo;
            myCommand.Parameters.Add("@UserName", SqlDbType.VarChar, 50).Value = userName;
            myCommand.Parameters["@FilmIntNo"].Direction = ParameterDirection.InputOutput;
            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int updFilmIntNo = (int)myCommand.Parameters["@FilmIntNo"].Value;

                return updFilmIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// Gets the list of users that have locked the film
        /// </summary>
        /// <param name="autIntNo">The Authority int no.</param>
        /// <returns>A SQL Data Reader containing the list of films</returns>
        public SqlDataReader GetFilmLockUserList(int autIntNo)
        {
            // Create the data access objects
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("FilmLockUserList", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AuthIntNo", SqlDbType.Int, 4).Value = autIntNo;
            
            // Get the date
            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        /// <summary>
        /// Gets the list of users that have locked the film
        /// </summary>
        /// <param name="autIntNo">The Authority int no.</param>
        /// <returns>A SQL Data Reader containing the list of films</returns>
        // 2013-07-24 add parameter lastUser by Henry
        public int ReleaseFilmLockUser(int autIntNo, string lastUser)
        {
            // Create the data access objects
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("FilmLockUserRelease", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AuthIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = lastUser;

            // Get the date
            try
            {
                con.Open();
                return com.ExecuteNonQuery();
            }
            catch
            {
                return -1;
            }
            finally
            {
                con.Dispose();
            }
        }


        #region [Remove Images]

        /// <summary>
        /// Get the list of film details by first offence year and month for RemovesImages from database
        /// </summary>
        /// <param name="year">First offence year (e.g. 2007)</param>
        /// <param name="month">First offence month (e.g. 01)</param>
        /// <returns>A SQL Data Reader containing the list of films</returns>
        public List<FilmDetails> GetFilmsByFirstOffenceDate(string year, string month)
        {
            // Create the data access objects
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("FilmGetDataForRemoveImages", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@Year", SqlDbType.NVarChar, 4).Value = year;
            com.Parameters.Add("@Month", SqlDbType.NVarChar, 2).Value = month;

            // Get the date
            con.Open();
            SqlDataReader result = com.ExecuteReader(CommandBehavior.CloseConnection);

            // Create CustomerDetails Struct
            List<FilmDetails> list = new List<FilmDetails>();

            while (result.Read())
            {
                FilmDetails myFilmDetails = new FilmDetails();

                // Populate Struct using Output Params from SPROC                
                myFilmDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
                myFilmDetails.AuthorityCode = result["AutCode"].ToString();
                myFilmDetails.FilmNo = result["FilmNo"].ToString();
                myFilmDetails.FilmIntNo = Convert.ToInt32(result["FilmIntNo"]);
                myFilmDetails.FirstOffenceYear = result["Film1stOffenceDateYear"].ToString();
                myFilmDetails.FirstOffenceMonth = result["Film1stOffenceDateMonth"].ToString();
                myFilmDetails.NoOfFrames = Convert.ToInt32(result["NoOfFrames"]);
                list.Add(myFilmDetails);
            }

            result.Close();
            return list;
        }

        /// <summary>
        /// updte FilmImageRemoved value after remove all images by film
        /// </summary>
        /// <param name="filmIntNo"></param>
        /// <param name="removed"></param>
        /// <returns></returns>
        // 2013-07-19 comment by Henry for useless
        //public int UpdateFilmRemoved(int filmIntNo, bool removed, out string exceptionMessage)
        //{
        //    exceptionMessage = string.Empty;
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("FilmUpdateRemoved", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    myCommand.Parameters.Add("@FilmIntNo", SqlDbType.Int).Value = filmIntNo;
        //    myCommand.Parameters.Add("@IsFinished", SqlDbType.Bit).Value = removed;

        //    try
        //    {
        //        myConnection.Open();
        //        int result = myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        return result;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        exceptionMessage = e.Message;
        //        return -1;
        //    }
        //}

        /// <summary>
        /// Get the film details by FilmIntNo for re-run the remove image progress
        /// </summary>
        /// <param name="filmNo"></param>
        /// <param name="errorMessage">Exception message</param>
        /// <returns></returns>
        public FilmDetails GetFilmDetailsForRemoveImage(string filmNo, out string errorMessage)
        {
            errorMessage = string.Empty;
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("SingleFilmGetDataForRemoveImages", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            // jerry 2012-02-06 change FilmNo length from 10 to 25
            SqlParameter parameterFilmNo = new SqlParameter("@FilmNo", SqlDbType.VarChar, 25);
            parameterFilmNo.Value = filmNo;
            myCommand.Parameters.Add(parameterFilmNo);

            try
            {
                myConnection.Open();
                SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

                // Create CustomerDetails Struct
                FilmDetails myFilmDetails = null;

                while (result.Read())
                {
                    myFilmDetails = new FilmDetails();

                    // Populate Struct using Output Params from SPROC
                    myFilmDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
                    myFilmDetails.AuthorityCode = result["AutCode"].ToString();
                    myFilmDetails.FilmNo = result["FilmNo"].ToString();
                    myFilmDetails.FilmIntNo = Convert.ToInt32(result["FilmIntNo"]);
                    myFilmDetails.FirstOffenceYear = result["Film1stOffenceDateYear"].ToString();
                    myFilmDetails.FirstOffenceMonth = result["Film1stOffenceDateMonth"].ToString();
                    myFilmDetails.NoOfFrames = Convert.ToInt32(result["NoOfFrames"]);
                }

                result.Close();
                return myFilmDetails;
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                return null;
            }
        }

        /// <summary>
        /// Recover disk space after remove images to file system
        /// </summary>
        /// <returns>0:success or reutn undelete ScanImages rows count</returns>
        public int RecoverDiskSpace(string dbName)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RecoverDiskSpaceAfterRemovedImages", myConnection);

            SqlParameter parameterDBName = new SqlParameter("@DBName", SqlDbType.VarChar, 20);
            parameterDBName.Value = dbName;
            myCommand.Parameters.Add(parameterDBName);

            SqlParameter parameterUnDeletedCount = new SqlParameter("@unDeletedCount", SqlDbType.Int, 4);
            parameterUnDeletedCount.Direction = ParameterDirection.ReturnValue;
            parameterUnDeletedCount.Value = 0;
            myCommand.Parameters.Add(parameterUnDeletedCount);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            myCommand.CommandTimeout = 0;
            try
            {
                myConnection.Open();
                int result = myCommand.ExecuteNonQuery();
                result = Convert.ToInt32(parameterUnDeletedCount.Value);
                return result;
            }
            finally
            {
                myConnection.Close();
                myConnection.Dispose();
                myCommand.Dispose();
            }
        }

        #endregion


        /// <summary>
        /// Reject Film
        /// </summary>
        /// <param name="autIntNo">The Authority int no.</param>
        public int RejectFilm(int filmIntNo,  int RejIntNo, int FrameStatus, string lastUser, ref string errMessage)
        {
            // Create the data access objects
            SqlConnection con = new SqlConnection(mConstr);
            SqlCommand com = new SqlCommand("FilmReject", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@filmIntNo", SqlDbType.Int, 4).Value = filmIntNo;
            com.Parameters["@FilmIntNo"].Direction = ParameterDirection.InputOutput;

            com.Parameters.Add("@RejIntNo", SqlDbType.Int, 4).Value = RejIntNo;
            com.Parameters.Add("@FrameStatus", SqlDbType.Int, 4).Value = FrameStatus;
            com.Parameters.Add("@Lastuser", SqlDbType.VarChar, 50).Value = lastUser;

            try
            {
                con.Open();
                return com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                return 0;
            }
            finally
            {
                con.Dispose();
            }
        }
    }
}