BEGIN TRANSACTION
GO
-- Updated DB for new version
ALTER PROCEDURE [dbo].[SILCustom_VersionControl_SIL_VerifySync] 
    -- Add the parameters for the stored procedure here
    @AppName  varchar(100),
    @LastUpdatedDate SMALLDATETIME OUTPUT
AS
-- jerry 2010/10/09 add application name
BEGIN
    SELECT @LastUpdatedDate = 
    CASE @AppName 
       WHEN 'AARTOWebApplication' THEN '2015/07/10'
       WHEN 'AARTOImporterConsole' THEN '2015/04/01'
       WHEN 'AARTOBatchConsole' THEN '2015/04/01'
       WHEN 'AARTOWebService' THEN '2015/04/01'    
       WHEN 'AARTOClockManagerConsole' THEN '2015/04/01'
       WHEN 'HandwrittenOffencesImporter' THEN '2015/04/01' 
       WHEN 'AARTOPrintEngine' THEN '2015/04/01'     
       WHEN 'AARTOPrintEnginePrintFactoryBackService' THEN '2015/04/01'   
       WHEN 'AARTOPrintEnginePrintFactoryConfiguration' THEN '2015/04/01'
		--jerry 2010/10/09 add application name
	   WHEN 'AARTOViolation_Loader' THEN '2015/04/01'
	   WHEN 'AARTOTPExInt' THEN '2015/04/01'
	   WHEN 'AARTOThaboExporter' THEN '2015/04/01'
	   WHEN 'AARTORoadblockExtractor' THEN '2015/04/01'
	   WHEN 'AARTOPayfineExtractor' THEN '2015/04/01'
	   --WHEN 'AARTOThaboAggregator' THEN '2011/12/09'
	   WHEN 'TMS' THEN '2015/04/01'
       WHEN 'ImageService' THEN '2015/04/01'
		--fred 2010/12/21 add application name
	   WHEN 'AARTOReportConsole' THEN '2015/04/01'
	   WHEN 'AARTOIMXImageLoader' THEN '2015/04/01'
	   WHEN 'AARTOXMLLoader' THEN '2015/04/01'
    END 

END
GO



-- SYN 25 Update
GO
/****************************************************************************************
 [DateRule_GetByAutIntNo]

 Purpose_________________________________________________________________________________
 
 Gets records from the Charge_SumCharge table passing page index and page count parameters

 History_________________________________________________________________________________

 2012-05-17 SIL
	Created

 2013-09-12 Henry for mulity row repeat
	Removed DISTINCT 
	Added  page bound varibles
	Modify count() to ROW_NUMBER() for Total Page Count

 2013-06-29 S.Dawson
	Updated to allow for text search on Description and Data Rule Name

*****************************************************************************************/
ALTER PROCEDURE [dbo].[DateRule_GetByAutIntNo]
(
	@AutIntNo INT,
	@PageSize INT = 0,
	@PageIndex INT = 0,
	@SearchText NVARCHAR(250) = N'',
	@TotalCount INT = 0 OUT
)
AS
-- Henry 2013-09-12 modify for mulity row repeat
DECLARE @UpperBound INT, @LowerBound INT
SET @UpperBound = @PageIndex * @PageSize
SET @LowerBound = @UpperBound - @PageSize;

-- 2013-09-12 modify count() to ROW_NUMBER() by Henry for mulity row repeat
SELECT @TotalCount = ROW_NUMBER()OVER(ORDER BY dr.DRID) FROM dbo.DateRule AS DR
INNER JOIN dbo.AuthorityDateRule AS ADR
ON DR.DRID=ADR.DRID
WHERE ADR.AutIntNo=@AutIntNo
 AND	(@SearchText = N'' OR dr.DRName LIKE ( N'%' + @SearchText + N'%') OR dr.DtRDescr LIKE ( N'%' + @SearchText + N'%'))
GROUP BY dr.DRID,dr.DRName,dr.DtRDescr,adr.ADRID,adr.AutIntNo,adr.ADRNoOfDays;-- 2013-09-12 add by Henry for mulity row repeat

WITH Page AS(-- 2013-09-12 remove DISTINCT by Henry for mulity row repeat
SELECT TOP(@UpperBound) ROW_NUMBER()OVER(ORDER BY dr.DRID) AS RowIndex,  -- 2013-09-12 remove DISTINCT by Henry for mulity row repeat
dr.DRID,dr.DRName,dr.DtRDescr,adr.ADRID,adr.AutIntNo,adr.ADRNoOfDays 
FROM dbo.DateRule AS DR
INNER JOIN dbo.AuthorityDateRule AS ADR
ON DR.DRID=ADR.DRID
WHERE ADR.AutIntNo=@AutIntNo
 AND	(@SearchText = N'' OR dr.DRName LIKE ( N'%' + @SearchText + N'%') OR dr.DtRDescr LIKE ( N'%' + @SearchText + N'%'))
GROUP BY dr.DRID,dr.DRName,dr.DtRDescr,adr.ADRID,adr.AutIntNo,adr.ADRNoOfDays)-- 2013-09-12 add by Henry for mulity row repeat
SELECT DRID,DRName,DtRDescr,ADRID,AutIntNo,ADRNoOfDays FROM Page
WHERE RowIndex > @LowerBound AND RowIndex <=@UpperBound
GO
/****************************************************************************************
 [WorkFlowRuleForAuthority_GetByAutIntNo]

 Purpose_________________________________________________________________________________
 


 History_________________________________________________________________________________

 2013-06-29 S.Dawson
	Updated to allow for text search on Description and Data Rule Name

*****************************************************************************************/
ALTER PROCEDURE [dbo].[WorkFlowRuleForAuthority_GetByAutIntNo]
	-- Add the parameters for the stored procedure here
	(
	@AutIntNo INT,
	@PageSize INT = 0,
	@PageIndex INT = 0,
	@SearchText NVARCHAR(250) = N'',
	@TotalCount INT = 0 OUT
	)
AS
BEGIN
-- Henry 2013-09-12 modify for mulity row repeat
-- Jacob 2014-01-28 modify 'order by' from WA.WFRFAID to WA.WFRFAName
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @UpperBound INT, @LowerBound INT
    SET @UpperBound = @PageIndex * @PageSize
    SET @LowerBound = @UpperBound - @PageSize
    -- 2013-09-12 modify count() to ROW_NUMBER() by Henry for mulity row repeat
    SELECT @TotalCount = ROW_NUMBER() OVER(ORDER BY WA.WFRFAID)	
    FROM dbo.WorkFlowRuleForAuthority AS WA
	INNER JOIN dbo.AuthorityRule AS AR
	ON WA.WFRFAID=AR.WFRFAID 
	WHERE AR.AutIntNo=@AutIntNo
	AND	(@SearchText = N'' OR WA.WFRFAName LIKE ( N'%' + @SearchText + N'%') OR WA.WFRFADescr LIKE ( N'%' + @SearchText + N'%') OR WA.WFRFAComment LIKE ( N'%' + @SearchText + N'%'))
	GROUP BY WA.WFRFAID,WA.WFRFAName,WA.WFRFADescr,WA.WFRFAComment,
	AR.ARID,AR.AutIntNo,AR.ARNumeric,AR.ARString ;-- 2013-09-12 add by Henry for mulity row repeat
    
	WITH Page AS(-- 2013-09-12 remove DISTINCT by Henry for mulity row repeat
	--Jacob 2014-01-28 modify 'order by' from WA.WFRFAID to WA.WFRFAName
	--SELECT TOP (@UpperBound) ROW_NUMBER() OVER(ORDER BY WA.WFRFAID) AS RowIndex,
	SELECT TOP (@UpperBound) ROW_NUMBER() OVER(ORDER BY WA.WFRFAName) AS RowIndex,
	WA.WFRFAID,WA.WFRFAName,WA.WFRFADescr,WA.WFRFAComment,
	AR.ARID,AR.AutIntNo,AR.ARNumeric,AR.ARString 
	FROM dbo.WorkFlowRuleForAuthority AS WA
	INNER JOIN dbo.AuthorityRule AS AR
	ON WA.WFRFAID=AR.WFRFAID 
	WHERE AR.AutIntNo=@AutIntNo
	AND	(@SearchText = N'' OR WA.WFRFAName LIKE ( N'%' + @SearchText + N'%') OR WA.WFRFADescr LIKE ( N'%' + @SearchText + N'%') OR WA.WFRFAComment LIKE ( N'%' + @SearchText + N'%'))
	GROUP BY WA.WFRFAID,WA.WFRFAName,WA.WFRFADescr,WA.WFRFAComment,
	AR.ARID,AR.AutIntNo,AR.ARNumeric,AR.ARString)-- 2013-09-12 add by Henry for mulity row repeat
	SELECT WFRFAID,WFRFAName,WFRFADescr,WFRFAComment,
	ARID,AutIntNo,ARNumeric,ARString FROM Page
	WHERE RowIndex > @LowerBound AND RowIndex <= @UpperBound

END













-- SYN 26 Update

ALTER TABLE [dbo].[CourtRoom]
ALTER COLUMN CrtRPrefix NVARCHAR(5);
GO
ALTER TABLE [dbo].[CourtRoomAudit]
ALTER COLUMN CrtRPrefix NVARCHAR(5);
GO
/****************************************************************************************
 CourtRoomUpdate 

 2013-09-23 Heidi
	Added CrtRStatusFlag for distinguish the courtroom is active, inactive or no longer used(5102)

 2013-10-17 Jerry
	Changed CrtRPrefix length from char(1) to NVARCHAR(3)

 2013-06-17 S.Dawson
	Updated CrtRPrefix length from NVARCHAR (3) to NVARCHAR(5)
*****************************************************************************************/
ALTER PROCEDURE [dbo].[CourtRoomUpdate]
(
	@CrtRPrefix NVARCHAR(5),
	@CrtRoomName varchar(50),
    @CrtRNextCaseNo int,
    @CrtRNextWOANo int,
    @CrtRPresidingOfficer varchar(50),
	@CrtRPublicProsecutor varchar(50),
    @CrtRClerk varchar(50),
    @CrtRInterpreter varchar(50),
	@CrtRActive varchar(1),
	@LastUser varchar(50),
	@CrtRStatusFlag CHAR(1),
	@CrtRIntNo INT = 0 OUTPUT
)
AS 
BEGIN 
	SET NOCOUNT ON

	IF EXISTS (SELECT CrtRIntNo FROM [CourtRoom] WHERE [CrtRIntNo] = @CrtRIntNo)
		BEGIN
			UPDATE CourtRoom WITH (ROWLOCK) 
			SET		CrtRPrefix				= @CrtRPrefix, 
					CrtRNextCaseNo			= @CrtRNextCaseNo  , 
					CrtRoomName				= @CrtRoomName ,
					CrtRNextWOANo			= @CrtRNextWOANo, 
					CrtRPresidingOfficer	= @CrtRPresidingOfficer, 
					CrtRPublicProsecutor	= @CrtRPublicProsecutor, 
					CrtRClerkOfTheCourt		= @CrtRClerk, 
					CrtRInterpreter			= @CrtRInterpreter,
					CrtRActive				= @CrtRActive, 
					LastUser				= @LastUser,
					CrtRStatusFlag			= @CrtRStatusFlag
			WHERE CrtRIntNo					= @CrtRIntNo
		END
	ELSE
		BEGIN
			SELECT @CrtRIntNo = -1
		END

	SET NOCOUNT OFF
END
GO
/****************************************************************************************
CourtRoomAdd 
 2008-08-27 MRS
	Should check for the existence of a similar court room

2010-09-08 Jerry
	Changed CourtRoom.CrtRoomNo to nvarchar(5)

2013-09-23 Heidi
	Added CrtRStatusFlag for distinguish the courtroom is active, inactive or no longer used (5102)
	-- Heidi 2013-09-23 added CrtRStatusFlag for court room page CRUD(5102)

2013-10-17 Jerry
	Change CrtRPrefix length from char(1) to NVARCHAR(3)

 2013-06-17 S.Dawson
	Updated CrtRPrefix length from NVARCHAR (3) to NVARCHAR(5)
		
*****************************************************************************************/
ALTER PROCEDURE [dbo].[CourtRoomAdd]
(
	@CrtIntNo int,
	@CrtRoomNo NVARCHAR(5), 
	@CrtRoomName varchar(50),
	@CrtRPrefix NVARCHAR(5),
    @CrtRNextCaseNo int,
    @CrtRNextWOANo int,
    @CrtRPresidingOfficer varchar(50),
	@CrtRPublicProsecutor varchar(50),
    @CrtRClerk varchar(50),
    @CrtRInterpreter varchar(50),
	@CrtRActive varchar(1),
	@LastUser varchar(50),
	@CrtRStatusFlag CHAR(1),
	@CrtRIntNo INT = 0 OUTPUT
)
AS 
BEGIN
	SET NOCOUNT ON

	IF NOT EXISTS (SELECT CrtRIntNo FROM [CourtRoom] WHERE [CrtIntNo] = @CrtIntNo AND [CrtRoomNo] = @CrtRoomNo)
		BEGIN 
			INSERT INTO CourtRoom (CrtIntNo , CrtRoomNo , CrtRoomName , CrtRPrefix , CrtRNextCaseNo , CrtRNextWOANo , CrtRPresidingOfficer , CrtRPublicProsecutor , 
			CrtRClerkOfTheCourt , CrtRInterpreter , CrtRActive , LastUser,CrtRStatusFlag)
			VALUES (@CrtIntNo , @CrtRoomNo , @CrtRoomName , @CrtRPrefix , @CrtRNextCaseNo , @CrtRNextWOANo , @CrtRPresidingOfficer , @CrtRPublicProsecutor , 
			@CrtRClerk , @CrtRInterpreter , @CrtRActive , @LastUser,@CrtRStatusFlag)

			SELECT @CrtRIntNo = SCOPE_IDENTITY()
		END 
	ELSE 
		SELECT @CrtRIntNo = -1

	SET NOCOUNT OFF
END
GO




-- SYN-37 Update

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER  PROCEDURE [dbo].[PostalQuoteReceipts_EM]
	-- Add the parameters for the stored procedure here
	 @Receipts varchar(100) = NULL,       
	 @Authority int,    
	 @Date DATETIME=null,    
	 @BarcodeImages IMAGE = null  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	   
	--DECLARE @TempDate DATETIME, @NextDate DATETIME    
	----SET @NextDate = @Date + 1    
	--SET @TempDate = [dbo].[fnCastDateTimeAsDate](@Date)    
	--SET @NextDate = @TempDate + 1    
	    
	DECLARE @TmpReceipt TABLE    
	(    
	 nId int IDENTITY(0,1),    
	 RctIntNo INT,   
	 QuHeIntNo INT, 
	 PaymentMethod VARCHAR(255),    
	 PaymentAmounts VARCHAR(100),    
	 NoOfPayments TINYINT,    
	 Total Money    
	)    
	    
	   
	DECLARE @Count TINYINT    
	DECLARE @Total_Count TINYINT    
	DECLARE @TmpRctIntNo INT    
	DECLARE @PrevRctIntNo INT    
	    
	DECLARE @PaymentMethod VARCHAR(255)    
	DECLARE @PaymentAmounts VARCHAR(100)    
	DECLARE @NoOfPayments TINYINT    
	    
	-- this temp table is used to work out the summary of payment methods & amounts and then update the receipt table    
	DECLARE @TmpPayments TABLE (row_count INT IDENTITY(0,1), RctIntNo INT, RTAmount MONEY, RTCashType CHAR(1), PaymentDetails VARCHAR(150)) 
	
	
	IF @Receipts IS NOT NULL    
	BEGIN
		INSERT INTO @TmpReceipt(RctIntNo,QuHeIntNo, Total, PaymentMethod, PaymentAmounts, NoOfPayments)     
			 SELECT rt.RctIntNo,rt.QuHeIntNo, SUM(rt.RTAmount) , '', '', 0    
				FROM  Receipt r WITH (NOLOCK)     
				INNER JOIN ReceiptTran rt WITH (NOLOCK) ON rt.RctIntNo = r.RctIntNo  
				WHERE r.RctIntNo IN (SELECT ID FROM dbo.fnIntSplit( @Receipts)) GROUP BY rt.RctIntNo,rt.QuHeIntNo  
				ORDER BY rt.RctIntNo,rt.QuHeIntNo   
				
		
		INSERT INTO @TmpPayments (RctIntNo, RTAmount, RTCashType)    
			 SELECT rt.RctIntNo, SUM(RTAmount), RTCashType    
				FROM ReceiptTran rt  WITH (NOLOCK)     
				INNER JOIN @TmpReceipt t ON t.RctIntNo = rt.RctIntNo     
				GROUP BY rt.RctIntNo, RTCashType    
				ORDER BY rt.RctIntNo, RTCashType
		
		UPDATE @TmpPayments    
			 SET PaymentDetails =  CASE rt.RTCashType     
										WHEN ']' THEN CONVERT(VARCHAR(15),d.ChqNo)     
										WHEN '}'  THEN CONVERT(VARCHAR(15),d.PostalOrderNo) 
									ELSE '' END     
			 FROM @TmpPayments t    
				INNER JOIN ReceiptTran rt  WITH (NOLOCK) ON t.RctIntNo = rt.RctIntNo AND t.RTCashType = rt.RTCashType    
				INNER JOIN DSTran d  WITH (NOLOCK) ON rt.RTIntNo = d.RTIntNo
				
		SELECT @Total_Count = COUNT(row_count), @Count = 0, @PrevRctIntNo = 0, @NoOfPayments = 0 FROM @TmpPayments    
    
		WHILE @Count < @Total_Count    
		BEGIN    
			SELECT @TmpRctIntNo = RctIntNo FROM @TmpPayments WHERE row_count = @Count    

			IF @TmpRctIntNo <> @PrevRctIntNo    
				SELECT @PaymentMethod = '', @PaymentAmounts = '', @NoOfPayments = 0    
			 
			SELECT @PaymentMethod = CASE WHEN @PaymentMethod = '' 
												THEN @PaymentMethod 
										ELSE @PaymentMethod + CHAR(13) END +    
									CASE WHEN RTCashType ='[' 
											THEN 'Cash/Kontant'    
										WHEN RTCashType =']'
											THEN 'Cheque/Tjek # ' + PaymentDetails    
										WHEN RTCashType =')' 
											THEN 'Credit Card/Kredietkaart'    
										WHEN RTCashType = '}' 
											THEN 'Postal Order/Posorder # ' + CAST(PaymentDetails AS VARCHAR(15))    
										WHEN RTCashType = '{' 
											THEN 'Debit Card/Debietkaart' 
										WHEN RTCashType = '*' 
											THEN 'Other'
										WHEN RTCashType = '(' 
											THEN 'Bank Payment ' + CAST(ISNULL(PaymentDetails,'') AS VARCHAR(15))    			   
										--WHEN RTCashType = 'T' 
										--	THEN 'Direct Deposit/Direkte Deposito'    
										--WHEN RTCashType IN ('E', 'K') 
										--	THEN 'Bank Payment'    
										--WHEN RTCashType IN ('J') 
										--	THEN 'Court Payment'    
										--WHEN RTCashType IN ('Z') 
										--	THEN CASE WHEN RTAmount < 0 
										--			THEN 'Administrative Reversal' 
										--			ELSE 'Administrative Receipt' END
										--WHEN RTCashType IN ('O') 
										--	THEN 'Other'
										ELSE 'Unknown' END ,    
					@PaymentAmounts = CASE WHEN @PaymentAmounts = '' 
												THEN 'R ' + @PaymentAmounts     
											ELSE @PaymentAmounts + CHAR(13) + 'R ' END + LTRIM(RTRIM(CAST(RTAmount AS VARCHAR(10)))) ,    
					@NoOfPayments = @NoOfPayments + 1     
					FROM @TmpPayments    
					WHERE row_count = @Count    

			SELECT @PrevRctIntNo = @TmpRctIntNo    

			UPDATE @TmpReceipt    
				SET PaymentMethod = @PaymentMethod, 
				PaymentAmounts = @PaymentAmounts, 
				NoOfPayments = @NoOfPayments    
			WHERE RctIntNo = @TmpRctIntNo    
			  
			SELECT @Count = @Count + 1    
		END
		
		UPDATE @TmpReceipt    
			SET PaymentMethod = CASE NoOfPayments     
									WHEN 1 THEN CHAR(13) + CHAR(13) + PaymentMethod    
									WHEN 2 THEN CHAR(13) + CHAR(13) + PaymentMethod    
									WHEN 3 THEN CHAR(13) + PaymentMethod    
									WHEN 4 THEN CHAR(13) + PaymentMethod    
								ELSE PaymentMethod END,    
				PaymentAmounts = CASE NoOfPayments     
									WHEN 1 THEN CHAR(13) + CHAR(13) + PaymentAmounts    
									WHEN 2 THEN CHAR(13) + CHAR(13) + PaymentAmounts    
									WHEN 3 THEN CHAR(13) + PaymentAmounts    
									WHEN 4 THEN CHAR(13) + PaymentAmounts    
								ELSE PaymentAmounts END   		  		
								END     
    
    
  SELECT     
	  Total = 'R ' + LTRIM(RTRIM(CAST(tr.Total AS VARCHAR(10)))) , 
	  tr.PaymentMethod, tr.PaymentAmounts, r.RctIntNo, 
	  dbo.fnShowOPUSOrHRKReceiptNumber(r.RctIntNo,r.RctNumber,r.ReferenceNumber) AS RctNumber, 
	  RctDate=CASE WHEN DATEPART(hh,r.RctDate)=0 AND DATEPART (mi,r.RctDate)=0 THEN CONVERT(varchar(100),r.RctDate, 23)
				   ELSE SUBSTRING(CONVERT(VARCHAR(100), r.RctDate , 20),1,16) END,
	  RTAmount = 'R ' + LTRIM(RTRIM(CAST(tr.PaymentAmounts AS VARCHAR(10)))) ,    
	  ISNULL(r.ReceivedFrom,'') AS ReceivedFrom,     
	  ISNULL(r.Address1,'') AS Address1, ISNULL(r.Address2,'') AS Address2, ISNULL(r.Address3,'') AS Address3,     
	  ISNULL(r.Address4,'') AS Address4, ISNULL(r.Address5,'') AS Address5, ISNULL(r.AddressCode,'') AS AddressCode,    
	  AddressFull =    
		CASE WHEN LEN(RTRIM(LTRIM(QuHeAddress1))) > 0 THEN RTRIM(LTRIM(QuHeAddress1)) + ', ' ELSE '' END    
		+ CASE WHEN LEN(RTRIM(LTRIM(QuHeAddress2))) > 0 THEN RTRIM(LTRIM(QuHeAddress2)) + ', ' ELSE '' END    
		+ CASE WHEN LEN(RTRIM(LTRIM(QuHeAddress3))) > 0 THEN RTRIM(LTRIM(QuHeAddress3)) + ', ' ELSE '' END      
		+ CASE WHEN LEN(RTRIM(LTRIM(QuHeAddress4))) > 0 THEN RTRIM(LTRIM(QuHeAddress4)) + ', ' ELSE '' END    
		+ CASE WHEN LEN(RTRIM(LTRIM(QuHeAddress5))) > 0 THEN RTRIM(LTRIM(QuHeAddress5)) + ', ' ELSE '' END,   
	   
	  a.AutName, 
	  --a.AutPostAddr1, 
	  --a.AutPostAddr2, 
	  --a.AutPostAddr3, 
	  --a.AutPostCode, 
	  AutPostAddress =ISNULL(a.AutPostAddr1,'')+CHAR(13) 
		+ ISNULL(a.AutPostAddr2,'')+CHAR(13)
		+ ISNULL(a.AutPostAddr3,'')+CHAR(13)
		+ ISNULL(a.AutPostCode,''),
	  AutTel, --jerry 20100416 not use @Authority, becauce now set [Receipt].AutIntNo is null    
	  --ac.AccountNo,
	  r.RctUser,    
	  '' AS CellNo,    
	  '' as HomeNo,    
	  '' AS WorkNo,    
	  QuHeSurName AS Surname,    
	  QuheInitials AS [Name], 
	  QuHeReferenceNumber,   
	  BarcodeImages = @BarcodeImages,     
	  tr.NoOfPayments,     
	 mt.MtrLogo
	 --mt.MtrDocumentFromAfrikaans,mt.MtrDocumentFromEnglish,mt.MtrReceiptAfrikaansAct,mt.MtrReceiptEnglishAct-- add by richard 2011-06-08    
	 FROM Receipt r WITH (NOLOCK)     
	  --INNER JOIN Authority a WITH (NOLOCK) ON r.AutIntNo = a.AutIntNo   --jerry 20100416 not use @Authority, becauce now set [Receipt].AutIntNo is null    
	  INNER JOIN Authority a WITH (NOLOCK) ON a.AutIntNo = @Authority    
	  INNER JOIN dbo.Metro mt WITH(NOLOCK) ON a.MtrIntNo=mt.MtrIntNo--add by richard 2011-06-08     
	  INNER JOIN @TmpReceipt tr ON tr.RctIntNo = r.RctIntNo 
	  INNER JOIN dbo.QuoteHeader q WITH(NOLOCK) ON tr.QuHeIntNo=q.QuHeIntNo    
	  --INNER JOIN Account ac WITH (NOLOCK) ON (at.AccIntNo = ac.AccIntNo) -- AND ac.AutIntNo = r.AutIntNo)    
	 WHERE r.RctIntNo IN (SELECT ID FROM dbo.fnIntSplit( @Receipts))          
	 ORDER BY dbo.fnShowOPUSOrHRKReceiptNumber(r.RctIntNo,r.RctNumber,r.ReferenceNumber)
 
 
 SELECT   NTCCode, NTCDescr,NTCComment,QuDeChargeQuantity,u.UOMName,NTCStandardAmount,QuDeActualAmount,
	QuDeGrossAmount
	FROM dbo.QuoteDetail qd WITH(NOLOCK)
	INNER JOIN dbo.UnitOfMeasure u WITH (NOLOCK) ON qd.UOMIntNo = u.UOMIntNo
	INNER JOIN (SELECT DISTINCT QuHeIntNo FROM @TmpReceipt ) t ON t.QuHeIntNo=qd.QuHeIntNo
	
END
GO

COMMIT TRANSACTION