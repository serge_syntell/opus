﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Threading;

using SIL.AARTO.BLL.InfringerOption;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Services;

using OpenXmlExtention;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.BatchConsole
{
    public class PrintEngineBatch
    {
        public static TList<AartoDocBatch> GetNewBatchList()
        {            
            AartoDocBatchQuery query = new AartoDocBatchQuery();
            query.Append(AartoDocBatchColumn.AaDocStatusId, Convert.ToInt32(AartoDocumentStatusList.New).ToString());
            return new AartoDocBatchService().Find(query as IFilterParameterCollection);
        }

        public static void IssuseForNoticeDocumentBatch(AartoDocBatch batch, ImageFileServer fileServer)
        {
            string strFileServerRootFolder = @"\\" + fileServer.ImageMachineName + @"\" + fileServer.ImageShareName;
            string strDocumentFolder = string.Empty;

            string strAutCode = string.Empty;
            DateTime IssuedDate = DateTime.Now;
            int docSucceed = 0;

            TList<AartoNoticeDocument> docList = GetNoticeDocumentListForBatch(batch);
            if (docList.Count > 0)
            { 
                foreach (AartoNoticeDocument doc in docList)
                {                    
                    // read datareader from notice document  
                    // fill dictionary              
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    
                    XmlDocument xmlDocCustom = new XmlDocument();
                    string strSourceTemplatePath = Program.TemplateFolder + ((AartoDocumentTypeList)doc.AaDocTypeId).ToString() + ".docx";
                    string strCustomXmlPath = Program.CustomXmlFolder + ((AartoDocumentTypeList)doc.AaDocTypeId).ToString() + ".xml";
                    xmlDocCustom.Load(strCustomXmlPath);

                    IDataReader reader = GetDataByDocumentType((AartoDocumentTypeList)doc.AaDocTypeId, doc.AaNotDocId);

                    dic = FillDictionaryWithTextData(xmlDocCustom, reader);

                    if (dic.Count == 0)
                    {                        
                        //ErrorLog.AddErrorLog("Single document Error", "FillDictionaryWithDataReader error, NotDocID = " + doc.AaNotDocId, 5, BatchManager.LastUser);

                        string strErrorMessage = "FillDictionaryWithDataReader error, NotDocID = " + doc.AaNotDocId;
                        Console.WriteLine(strErrorMessage);
                        EntLibLogger.WriteLog(LogCategory.Error, "Single document Error", strErrorMessage);
                        //Logger.Write(strErrorMessage, "Error", (int)TraceEventType.Error, 1, TraceEventType.Error, "Single document Error");

                        AARTODocumentManager.UpdateAARTODocumentAfterGenerateFailed(doc.AaNotDocId, doc.AaDocTypeId, BatchManager.LastUser);                                                
                        continue;
                    }
                    
                    strAutCode = dic["AutCode"].ToString().Trim();

                    strDocumentFolder = @"\" + strAutCode + @"\Issued";
                    //strDocumentFolder = @"\" + "SW" + @"\Issued";
                    strDocumentFolder += @"\" + ((AartoDocumentTypeList)doc.AaDocTypeId).ToString();
                    strDocumentFolder += @"\" + IssuedDate.Year.ToString() + @"\" + IssuedDate.Month.ToString().PadLeft(2, '0') + @"\" + IssuedDate.Day.ToString().PadLeft(2, '0');
                    strDocumentFolder += @"\" + RemoveSplitFileName(batch.AaDocBatchNo);

                    if (Directory.Exists(strFileServerRootFolder + strDocumentFolder) == false)
                    {
                        Directory.CreateDirectory(strFileServerRootFolder + strDocumentFolder);
                    }

                    // create target doc
                    string strTargetDocPath = strDocumentFolder + "\\" + RemoveSplitFileName(dic["NotTicketNo"].ToString()) + ".docx";
                                       
                    bool replaceResult = false;
                    try
                    {
                        replaceResult = WordOperation.ReplaceDocuments(strSourceTemplatePath, strFileServerRootFolder + strTargetDocPath, xmlDocCustom, dic);                                                
                    }
                    catch (Exception ex)
                    {
                        //ErrorLog.AddErrorLog(ex, BatchManager.LastUser);                        
                        Console.WriteLine(ex.Message);
                        //Logger.Write(ex.Message, "Error", (int)TraceEventType.Error, 1, TraceEventType.Error);                        
                        EntLibLogger.WriteLog(LogCategory.Error, "ReplaceDocuments Single document Error", ex.Message);
                        replaceResult = false;
                        return;
                    }
                    

                    if(replaceResult)
                    {
                        // Update single document status and path
                        AARTODocumentManager.UpdateAARTODocumentAfterGenerated(doc.AaNotDocId, doc.AaDocTypeId, fileServer.IfsIntNo, strTargetDocPath.Replace(".docx", ".pdf"), BatchManager.LastUser);
                        //AartoNoticeDocumentService serviceNotDoc = new AartoNoticeDocumentService();
                        //doc.IfsIntNo = fileServer.IfsIntNo;
                        //doc.AaNotDocPath = strTargetDocPath;
                        //serviceNotDoc.Save(doc);
                        docSucceed += 1;
                    }
                }


                // Merge multi document to batch docx file
                string[] strDocxNames = Directory.GetFiles(strFileServerRootFolder + strDocumentFolder, "*.docx");
                bool mergeResult = false;
                string StrBatchFileName = strDocumentFolder + "\\" + RemoveSplitFileName(batch.AaDocBatchNo) + ".docx";

                if (strDocxNames.Length > 0)
                {
                    try
                    {
                        mergeResult = WordOperation.MergeMultiDocuments(strDocxNames, strFileServerRootFolder + StrBatchFileName);
                    }
                    catch (Exception)
                    {
                        mergeResult = false;
                        throw;
                    }
                }
               

                if (mergeResult)
                {
                    // Update batch document status and path
                    StrBatchFileName = StrBatchFileName.Replace(".docx", ".pdf");
                    AARTODocumentManager.UpdateAARTODocumentBatchAfterIssued(batch.AaDocBatchId, StrBatchFileName, fileServer.IfsIntNo, BatchManager.LastUser);                    

                    // After merge, convert all docx to pdf, delete docx
                    strDocxNames = Directory.GetFiles(strFileServerRootFolder + strDocumentFolder, "*.docx");
                    foreach (string docxFile in strDocxNames)
                    {
                        if (BatchManager.ConvertToPDF(docxFile))
                        {
                            File.Delete(docxFile);
                        }
                    }
                }
           
                // if printing type = PrintFactory, create batch support xml doc batch.pebs and send to print factory via indaba.
                if (ConfigurationManager.AppSettings["PrintingType"] == "PrintFactory")
                {
                    string StrPEBSFileName = strFileServerRootFolder + strDocumentFolder + "\\" + RemoveSplitFileName(batch.AaDocBatchNo) + ".pebs";
                    SendToPrintFactory(batch, strAutCode, IssuedDate, docSucceed, StrPEBSFileName, strFileServerRootFolder + StrBatchFileName);                   
                }
            }
            else
            {
                // There are no document in this batch, delete the batch from database
                AARTODocumentManager.DeleteAARTODocumentBatchForNoDocs(batch.AaDocBatchId);
            }
        }

        public static void SendToPrintFactory(AartoDocBatch batch, string autCode, DateTime issuedDate, int docCount, string pebsPath, string pdfFilePath)
        {                        
            // create pebs file
            PebsBatch pb = new PebsBatch();
            pb.LanguageName = ConfigurationManager.AppSettings["LanguageName"];
            pb.BatchId = batch.AaDocBatchId;
            pb.BatchCode = batch.AaDocBatchNo;
            pb.AutCode = autCode;
            pb.DateIssuedValue = issuedDate;
            pb.DocumentCount = docCount;
            pb.DocumentType = ((AartoDocumentTypeList)batch.AaDocTypeId).ToString();
            pb.NeedToSynchronize = false;
            pb.Status = "Issued";

            ObjectSerializer.SaveObjectToXmlFile(pb, pebsPath);

            // send to PF via indaba                    
            using (IndabaClientUtil indabaClientUtil = new IndabaClientUtil())
            {
                // copy 'pdf' to 'pebsdocx' for indaba transport.
                //Regex ex = new Regex(".pdf$");
                //string sendDocxFileName = ex.Replace(pdfFilePath, ".pebsdocx");                
                indabaClientUtil.SendADoc(pdfFilePath);
                indabaClientUtil.SendADoc(pebsPath);                        
            }
        }

        public static IDataReader GetDataByDocumentType(AartoDocumentTypeList docType, long docID)
        {
            switch (docType)
            {
                case AartoDocumentTypeList.AARTO03:

                    return new AartoNoticeDocumentService().GetInfoForPrintedAndPostedInfringementNotice(docID);

                case AartoDocumentTypeList.AARTO04:
                    break;
                case AartoDocumentTypeList.AARTO05a:

                    return new AartoRepDocumentService().GetInfoForAcknowledgementOfReceiptAARTO05a(docID);
                    
                case AartoDocumentTypeList.AARTO05b:

                    return new AartoRepDocumentService().GetInfoForAcknowledgementOfReceiptAARTO05b(docID);
                    
                case AartoDocumentTypeList.AARTO05c:

                    return new AartoRepDocumentService().GetInfoForAcknowledgementOfReceiptAARTO05c(docID);

                case AartoDocumentTypeList.AARTO05d:

                    return new AartoRepDocumentService().GetInfoForAcknowledgementOfReceiptAARTO05d(docID);

                case AartoDocumentTypeList.AARTO06:

                    return new AartoRepDocumentService().GetInfoForResultOfApplicationToPayInInstalments(docID);

                case AartoDocumentTypeList.AARTO07:
                    break;
                case AartoDocumentTypeList.AARTO08:
                    break;
                case AartoDocumentTypeList.AARTO09:

                    return new AartoRepDocumentService().GetInfoForResultOfRepresentation(docID);

                case AartoDocumentTypeList.AARTO10:
                    break;
                case AartoDocumentTypeList.AARTO11:
                    break;
                case AartoDocumentTypeList.AARTO12:

                    return new AartoNoticeDocumentService().GetInfoForCourtesyLetter(docID);

                case AartoDocumentTypeList.AARTO13:

                    return new AartoNoticeDocumentService().GetInfoForEnforcementOrder(docID);

                case AartoDocumentTypeList.AARTO14:
                    break;
                case AartoDocumentTypeList.AARTO15:

                    return new AartoRepDocumentService().GetInfoForResultOfRevocationOfEnforcementOrder(docID);

                case AartoDocumentTypeList.AARTO16:
                    break;
                case AartoDocumentTypeList.AARTO17:
                    break;
                case AartoDocumentTypeList.AARTO18:
                    break;
                case AartoDocumentTypeList.AARTO24:
                    return new AartoNoticeDocumentService().GetInfoForWarrant(docID);                
                default:
                    break;
            }

            return null;
        }

        public static string RemoveSplitFileName(string numbers)
        {
            string strResult = string.Empty;
            string[] strA = numbers.Split('/', '-');
            foreach (string str in strA)
            {
                strResult += str;
            }
            return strResult;
        }

        public static TList<AartoNoticeDocument> GetNoticeDocumentListForBatch(AartoDocBatch batch)
        {
            AartoNoticeDocumentQuery query = new AartoNoticeDocumentQuery();
            query.Append(AartoNoticeDocumentColumn.AaDocBatchId, batch.AaDocBatchId.ToString());
            query.Append(AartoNoticeDocumentColumn.AaDocStatusId, Convert.ToInt32(AartoDocumentStatusList.Issued).ToString());
            return new AartoNoticeDocumentService().Find(query as IFilterParameterCollection);
        }

        /// <summary>
        /// get AartoRepDocument list of batch
        /// </summary>
        /// <param name="batch"></param>
        /// <returns></returns>
        public static TList<AartoRepDocument> GetRepDocumentListForBatch(AartoDocBatch batch)
        {
            AartoRepDocumentQuery query = new AartoRepDocumentQuery();
            query.Append(AartoRepDocumentColumn.AaDocBatchId, batch.AaDocBatchId.ToString());
            query.Append(AartoRepDocumentColumn.AaDocStatusId, Convert.ToInt32(AartoDocumentStatusList.Issued).ToString());
            return new AartoRepDocumentService().Find(query as IFilterParameterCollection);
        }

        /// <summary>
        /// issue AaroRepDocument Batch
        /// </summary>
        /// <param name="batch"></param>
        /// <param name="fileServer"></param>
        public static void IssuseForRepDocumentBatch(AartoDocBatch batch, ImageFileServer fileServer)
        {
            string strFileServerRootFolder = @"\\" + fileServer.ImageMachineName + @"\" + fileServer.ImageShareName;
            string strDocumentFolder = string.Empty;
            string strAutCode = string.Empty;
            DateTime IssuedDate = DateTime.Now;
            int docSucceed = 0;
            
            TList<AartoRepDocument> docList = GetRepDocumentListForBatch(batch);
            if (docList.Count > 0)
            {
                foreach (AartoRepDocument doc in docList)
                {
                    // read datareader from notice document  
                    // fill dictionary              
                    Dictionary<string, object> dic = new Dictionary<string, object>();

                    XmlDocument xmlDocCustom = new XmlDocument();
                    string strSourceTemplatePath = Program.TemplateFolder + ((AartoDocumentTypeList)doc.AaDocTypeId).ToString() + ".docx";
                    string strCustomXmlPath = Program.CustomXmlFolder + ((AartoDocumentTypeList)doc.AaDocTypeId).ToString() + ".xml";
                    xmlDocCustom.Load(strCustomXmlPath);

                    IDataReader reader = GetDataByDocumentType((AartoDocumentTypeList)doc.AaDocTypeId, doc.AaRepDocId);

                    dic = FillDictionaryWithTextData(xmlDocCustom, reader);

                    if (dic.Count == 0)
                    {                        
                        //ErrorLog.AddErrorLog("Single document Error", "FillDictionaryWithDataReader error, RepDocID = " + doc.AaRepDocId, 5, BatchManager.LastUser);
                        string strErrorMessage = "FillDictionaryWithDataReader error, RepDocID = " + doc.AaRepDocId;
                        Console.WriteLine(strErrorMessage);
                        EntLibLogger.WriteLog(LogCategory.Error, "Single document Error", strErrorMessage);
                        //Logger.Write(strErrorMessage, "Error", (int)TraceEventType.Error, 1, TraceEventType.Error, "Single document Error");

                        AARTODocumentManager.UpdateAARTODocumentAfterGenerateFailed(doc.AaRepDocId, doc.AaDocTypeId, BatchManager.LastUser);
                        continue;
                    }

                    strAutCode = dic["AutCode"].ToString().Trim();
                    strDocumentFolder = @"\" + strAutCode + @"\Issued";
                    //strDocumentFolder = @"\" + "SW" + @"\Issued";
                    strDocumentFolder += @"\" + ((AartoDocumentTypeList)doc.AaDocTypeId).ToString();
                    strDocumentFolder += @"\" + IssuedDate.Year.ToString() + @"\" + IssuedDate.Month.ToString().PadLeft(2, '0') + @"\" + IssuedDate.Day.ToString().PadLeft(2, '0');
                    strDocumentFolder += @"\" + RemoveSplitFileName(batch.AaDocBatchNo);

                    if (Directory.Exists(strFileServerRootFolder + strDocumentFolder) == false)
                    {
                        Directory.CreateDirectory(strFileServerRootFolder + strDocumentFolder);
                    }

                    // create target doc
                    string strTargetDocPath = strDocumentFolder + "\\" + RemoveSplitFileName(dic["NotTicketNo"].ToString()) + ".docx";

                    //File.Copy(strSourceTemplatePath, strFileServerRootFolder + strTargetDocPath, true);

                    bool replaceResult = false;
                    try
                    {
                        replaceResult = WordOperation.ReplaceDocuments(strSourceTemplatePath, strFileServerRootFolder + strTargetDocPath, xmlDocCustom, dic);
                    }
                    catch (Exception)
                    {
                        replaceResult = false;
                        throw;
                    }


                    if (replaceResult)
                    {
                        // Update single document status and path
                        AARTODocumentManager.UpdateAARTODocumentAfterGenerated(doc.AaRepDocId, doc.AaDocTypeId, fileServer.IfsIntNo, strTargetDocPath.Replace(".docx", ".pdf"), BatchManager.LastUser);
                        //AartoRepDocumentService serviceRepDoc = new AartoRepDocumentService();
                        //doc.IfsIntNo = fileServer.IfsIntNo;
                        //doc.AaRepDocPath = strTargetDocPath;
                        //serviceRepDoc.Save(doc);
                        docSucceed += 1;
                    }
                }


                // Merge multi document to batch docx file
                string[] strDocxNames = Directory.GetFiles(strFileServerRootFolder + strDocumentFolder, "*.docx");
                bool mergeResult = false;

                string StrBatchFileName = strDocumentFolder + "\\" + RemoveSplitFileName(batch.AaDocBatchNo) + ".docx";
                if (strDocxNames.Length > 0)
                {
                    try
                    {
                        mergeResult = WordOperation.MergeMultiDocuments(strDocxNames, strFileServerRootFolder + StrBatchFileName);
                    }
                    catch (Exception)
                    {
                        mergeResult = false;
                        throw;
                    }
                }                

                if (mergeResult)
                {
                    StrBatchFileName = StrBatchFileName.Replace(".docx", ".pdf");
                    // Update batch document status and path
                    AARTODocumentManager.UpdateAARTODocumentBatchAfterIssued(batch.AaDocBatchId, StrBatchFileName, fileServer.IfsIntNo, BatchManager.LastUser);

                    // After merge, convert all docx to pdf, delete docx
                    strDocxNames = Directory.GetFiles(strFileServerRootFolder + strDocumentFolder, "*.docx");
                    foreach (string docxFile in strDocxNames)
                    {
                        if (BatchManager.ConvertToPDF(docxFile))
                        {
                            File.Delete(docxFile);
                        }
                    }                    
                    //AartoDocBatchService serviceBatch = new AartoDocBatchService();
                    //batch.IfsIntNo = fileServer.IfsIntNo;
                    //batch.AaDocBatchPath = StrBatchFileName;
                    //batch.AaDocIssuedDate = DateTime.Now;
                    //batch.AaDocStatusId = (int)AartoDocumentStatusList.Issued;
                    //serviceBatch.Save(batch);
                }

                // if printing type = PrintFactory, create batch support xml doc batch.pebs and send to print factory via indaba.
                if (ConfigurationManager.AppSettings["PrintingType"] == "PrintFactory")
                {
                    string StrPEBSFileName = strFileServerRootFolder + strDocumentFolder + "\\" + RemoveSplitFileName(batch.AaDocBatchNo) + ".pebs";
                    SendToPrintFactory(batch, strAutCode, IssuedDate, docSucceed, StrPEBSFileName, strFileServerRootFolder + StrBatchFileName);
                }
            }
            else
            {
                // There are no document in this batch, delete the batch from database
                AARTODocumentManager.DeleteAARTODocumentBatchForNoDocs(batch.AaDocBatchId);
            }
        }

        public static void IssusePrintBatches(ImageFileServer fileServer)
        {
            TList<AartoDocBatch> batchList = GetNewBatchList();
            if (batchList.Count > 0)
            {
                foreach (AartoDocBatch batch in batchList)
                {
                    AartoDocumentSourceTableList sourceTable = AARTODocumentSourceTable.IsForNotice(batch.AaDocTypeId);
                    switch (sourceTable)
                    {
                        case AartoDocumentSourceTableList.AARTONoticeDocument:
                            IssuseForNoticeDocumentBatch(batch, fileServer);
                            break;
                        case AartoDocumentSourceTableList.AARTORepDocument:
                            IssuseForRepDocumentBatch(batch, fileServer);
                            break;                        
                        default:
                            break;
                    }
                    Thread.Sleep(Program.SleepMinutes);
                }
            }
        }

        public static Dictionary<string, object> FillDictionaryWithTextData(XmlDocument customXml, IDataReader reader)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            XmlNodeList nodeList = customXml.DocumentElement.ChildNodes;
            while (reader.Read())
            {
                foreach (XmlNode node in nodeList)
                {
                    try
                    {
                        switch (node.Name)
                        {
                            case "Surname":
                                dic.Add(node.Name, reader["Surname"].ToString()); //doc.AaRepDocId;
                                break;
                            case "FirstName":
                                dic.Add(node.Name, reader["FirstName"].ToString());
                                break;
                            case "Initials":
                                dic.Add(node.Name, reader["Initials"].ToString());
                                break;
                            case "IDType":
                                dic.Add(node.Name, reader["IDType"].ToString());
                                break;
                            case "IDNumber":
                                dic.Add(node.Name, reader["IDNumber"].ToString());
                                break;
                            case "CountryOfIssue":
                                dic.Add(node.Name, reader["CountryOfIssue"].ToString());
                                break;
                            case "LicenceCode":
                                dic.Add(node.Name, reader["LicenceCode"].ToString());
                                break;
                            case "PrDPCode":
                                dic.Add(node.Name, reader["PrDPCode"].ToString());
                                break;
                            case "OperatorCardNo":
                                dic.Add(node.Name, reader["OperatorCardNo"].ToString());
                                break;
                            case "StreetAddress1":
                                dic.Add(node.Name, reader["StreetAddress1"].ToString());
                                break;
                            case "StreetAddress2":
                                dic.Add(node.Name, reader["StreetAddress2"].ToString());
                                break;
                            case "StreetAddress3":
                                dic.Add(node.Name, reader["StreetAddress3"].ToString());
                                break;
                            case "StreetAddress4":
                                dic.Add(node.Name, reader["StreetAddress4"].ToString());
                                break;
                            case "StreetAddress5":
                                dic.Add(node.Name, reader["StreetAddress5"].ToString());
                                break;
                            case "StreetAddressCode":
                                dic.Add(node.Name, reader["StreetAddressCode"].ToString());
                                break;
                            case "PostalAddress1":
                                dic.Add(node.Name, reader["PostalAddress1"].ToString());
                                break;
                            case "PostalAddress2":
                                dic.Add(node.Name, reader["PostalAddress2"].ToString());
                                break;
                            case "PostalAddress3":
                                dic.Add(node.Name, reader["PostalAddress3"].ToString());
                                break;
                            case "PostalAddress4":
                                dic.Add(node.Name, reader["PostalAddress4"].ToString());
                                break;
                            case "PostalAddress5":
                                dic.Add(node.Name, reader["PostalAddress5"].ToString());
                                break;
                            case "PostalAddressCode":
                                dic.Add(node.Name, reader["PostalAddressCode"].ToString());
                                break;
                            case "EmployerName":
                                dic.Add(node.Name, reader["EmployerName"].ToString());
                                break;
                            case "EmployerAddress1":
                                dic.Add(node.Name, reader["EmployerAddress1"].ToString());
                                break;
                            case "EmployerAddress2":
                                dic.Add(node.Name, reader["EmployerAddress2"].ToString());
                                break;
                            case "EmployerAddress3":
                                dic.Add(node.Name, reader["EmployerAddress3"].ToString());
                                break;
                            case "EmployerAddress4":
                                dic.Add(node.Name, reader["EmployerAddress4"].ToString());
                                break;
                            case "EmployerAddress5":
                                dic.Add(node.Name, reader["EmployerAddress5"].ToString());
                                break;
                            case "EmployerAddressCode":
                                dic.Add(node.Name, reader["EmployerAddressCode"].ToString());
                                break;
                            case "Gender":
                                dic.Add(node.Name, reader["Gender"].ToString());
                                break;
                            case "DateOfBirth":
                                dic.Add(node.Name, reader["DateOfBirth"].ToString());
                                break;
                            case "TelHome":
                                dic.Add(node.Name, reader["TelHome"].ToString());
                                break;
                            case "TelWork":
                                dic.Add(node.Name, reader["TelWork"].ToString());
                                break;
                            case "FaxNumber":
                                dic.Add(node.Name, reader["FaxNumber"].ToString());
                                break;
                            case "Cell":
                                dic.Add(node.Name, reader["Cell"].ToString());
                                break;
                            case "Email":
                                dic.Add(node.Name, reader["Email"].ToString());
                                break;
                            case "VehicleLicenseNo":
                                dic.Add(node.Name, reader["VehicleLicenseNo"].ToString());
                                break;
                            case "VehicleLicenceDiskNo":
                                dic.Add(node.Name, reader["VehicleLicenceDiskNo"].ToString());
                                break;
                            case "VehicleDesc":
                                dic.Add(node.Name, reader["VehicleDesc"].ToString());
                                break;
                            case "VehicleGVM":
                                dic.Add(node.Name, reader["VehicleGVM"].ToString());
                                break;
                            case "VehicleMake":
                                dic.Add(node.Name, reader["VehicleMake"].ToString());
                                break;
                            case "Series":
                                dic.Add(node.Name, reader["Series"].ToString());
                                break;
                            case "VehicleColour":
                                dic.Add(node.Name, reader["VehicleColour"].ToString());
                                break;
                            case "NotTicketNo":
                                dic.Add(node.Name, reader["NotTicketNo"].ToString());
                                break;
                            case "OffenseDate":
                                dic.Add(node.Name, reader["OffenseDate"].ToString());
                                break;
                            case "OffenceTime":
                                dic.Add(node.Name, reader["OffenceTime"].ToString());
                                break;
                            case "IssuingAuthority":
                                dic.Add(node.Name, reader["IssuingAuthority"].ToString());
                                break;
                            case "ChargeCode":
                                dic.Add(node.Name, reader["ChargeCode"].ToString());
                                break;
                            case "ChargeDescription":
                                dic.Add(node.Name, reader["ChargeDescription"].ToString());
                                break;
                            case "ChgType":
                                dic.Add(node.Name, reader["ChgType"].ToString());
                                break;
                            case "PenaltyAmount":
                                dic.Add(node.Name, reader["PenaltyAmount"].ToString());
                                break;
                            case "DemeritPoints":
                                dic.Add(node.Name, reader["DemeritPoints"].ToString());
                                break;
                            case "ChargeCodeAlt":
                                dic.Add(node.Name, reader["ChargeCode_alt"].ToString());
                                break;
                            case "ChargeDescriptionAlt":
                                dic.Add(node.Name, reader["ChargeDescription_alt"].ToString());
                                break;
                            case "TypeAlt":
                                dic.Add(node.Name, reader["Type_alt"].ToString());
                                break;
                            case "PenaltyAmountAlt":
                                dic.Add(node.Name, reader["PenaltyAmount_alt"].ToString());
                                break;
                            case "DemeritPointsAlt":
                                dic.Add(node.Name, reader["DemeritPoints_alt"].ToString());
                                break;
                            case "PenaltyTotal":
                                dic.Add(node.Name, reader["PenaltyTotal"].ToString());
                                break;
                            case "CourtTot":
                                dic.Add(node.Name, reader["CourtTot"].ToString());
                                break;
                            case "CourtesyLetterFee":
                                dic.Add(node.Name, reader["CourtesyLetterFee"].ToString());
                                break;
                            case "RepFee":
                                dic.Add(node.Name, reader["RepFee"].ToString());
                                break;
                            case "DemeritAllocated":
                                dic.Add(node.Name, reader["DemeritAllocated"].ToString());
                                break;
                            case "CertNo":
                                dic.Add(node.Name, reader["CertNo"].ToString());
                                break;
                            case "CertificateNumber":                                
                                dic.Add(node.Name, reader["CertificateNumber"].ToString());
                                break;
                            case "CreationDate":
                                dic.Add(node.Name, reader["CreationDate"].ToString());
                                break;
                            case "UserGroup":
                                dic.Add(node.Name, reader["UserGroup"].ToString());
                                break;
                            case "AutCode":
                                dic.Add(node.Name, reader["AutCode"].ToString());
                                break;
                            case "Province":
                                dic.Add(node.Name, reader["Province"].ToString());
                                break;
                            case "CityOrTownName":
                                dic.Add(node.Name, reader["CityOrTownName"].ToString());
                                break;
                            case "Suburb":
                                dic.Add(node.Name, reader["Suburb"].ToString());
                                break;
                            case "StreetNameA":
                                dic.Add(node.Name, reader["StreetNameA"].ToString());
                                break;
                            case "StreetNameB":
                                dic.Add(node.Name, reader["StreetNameB"].ToString());
                                break;
                            case "GeneralLocation":
                                dic.Add(node.Name, reader["GeneralLocation"].ToString());
                                break;
                            case "RouteNo":
                                dic.Add(node.Name, reader["RouteNo"].ToString());
                                break;
                            case "FromPlace":
                                dic.Add(node.Name, reader["FromPlace"].ToString());
                                break;
                            case "ToPlace":
                                dic.Add(node.Name, reader["ToPlace"].ToString());
                                break;
                            case "GPSX":
                                dic.Add(node.Name, reader["GPSX"].ToString());
                                break;
                            case "GPSY":
                                dic.Add(node.Name, reader["GPSY"].ToString());
                                break;
                            case "Speed1":
                                dic.Add(node.Name, reader["Speed1"].ToString());
                                break;
                            case "Speed2":
                                dic.Add(node.Name, reader["Speed2"].ToString());
                                break;
                            case "AmberTime":
                                dic.Add(node.Name, reader["AmberTime"].ToString());
                                break;
                            case "RedTime":
                                dic.Add(node.Name, reader["RedTime"].ToString());
                                break;
                            case "Discount":
                                dic.Add(node.Name, reader["Discount"].ToString());
                                break;
                            case "DiscountedAmount":
                                dic.Add(node.Name, reader["DiscountedAmount"].ToString());
                                break;
                            case "DiscountAlt":
                                dic.Add(node.Name, reader["DiscountAlt"].ToString());
                                break;
                            case "DiscountedAmountAlt":
                                dic.Add(node.Name, reader["DiscountedAmountAlt"].ToString());
                                break;
                            case "OfficerName":
                                dic.Add(node.Name, reader["OfficerName"].ToString());
                                break;
                            case "InfrastructureNumber":
                                dic.Add(node.Name, reader["InfrastructureNumber"].ToString());
                                break;
                            case "EnforcementIssueDate":
                                dic.Add(node.Name, reader["EnforcementIssueDate"].ToString());
                                break;
                            case "EnforcementRefNum":
                                dic.Add(node.Name, reader["EnforcementRefNum"].ToString());
                                break;
                            case "EnforcementFee":
                                dic.Add(node.Name, reader["EnforcementFee"].ToString());
                                break;
                            case "TotalAmount":
                                dic.Add(node.Name, reader["TotalAmount"].ToString());
                                break;
                            case "ReceivedDate":
                                dic.Add(node.Name, reader["ReceivedDate"].ToString());
                                break;
                            case "TotalDueFee":
                                dic.Add(node.Name, reader["TotalDueFee"].ToString());
                                break;
                            case "NumOfMonthInstalment":
                                dic.Add(node.Name, reader["NumOfMonthInstalment"].ToString());
                                break;
                            case "MonthInstalmentAmount":
                                dic.Add(node.Name, reader["MonthInstalmentAmount"].ToString());
                                break;
                            case "MethodOfPayment":
                                dic.Add(node.Name, reader["MethodOfPayment"].ToString());
                                break;
                            case "Successful":
                                dic.Add(node.Name, reader["Successful"].ToString());
                                break;
                            case "Unsuccessful":
                                dic.Add(node.Name, reader["Unsuccessful"].ToString());
                                break;
                            case "CanceledReason":
                                dic.Add(node.Name, reader["CanceledReason"].ToString());
                                break;
                            case "NewSurname":
                                dic.Add(node.Name, reader["NewSurname"].ToString());
                                break;
                            case "NewGender":
                                dic.Add(node.Name, reader["NewGender"].ToString());
                                break;
                            case "NewFirstName":
                                dic.Add(node.Name, reader["NewFirstName"].ToString());
                                break;
                            case "NewAge":
                                dic.Add(node.Name, reader["NewAge"].ToString());
                                if (dic["NewAge"] == "0")
                                {
                                    dic["NewAge"] = " ";
                                }
                                break;
                            case "NewInitials":
                                dic.Add(node.Name, reader["NewInitials"].ToString());
                                break;
                            case "NewDateOfBirth":
                                dic.Add(node.Name, reader["NewDateOfBirth"].ToString());
                                break;
                            case "NewIDType":
                                dic.Add(node.Name, reader["NewIDType"].ToString());
                                break;
                            case "NewTelHome":
                                dic.Add(node.Name, reader["NewTelHome"].ToString());
                                break;
                            case "NewIDNumber":
                                dic.Add(node.Name, reader["NewIDNumber"].ToString());
                                break;
                            case "NewTelWork":
                                dic.Add(node.Name, reader["NewTelWork"].ToString());
                                break;
                            case "NewCountryOfIssue":
                                dic.Add(node.Name, reader["NewCountryOfIssue"].ToString());
                                break;
                            case "NewFaxNumber":
                                dic.Add(node.Name, reader["NewFaxNumber"].ToString());
                                break;
                            case "NewLicenceCode":
                                dic.Add(node.Name, reader["NewLicenceCode"].ToString());
                                break;
                            case "NewCell":
                                dic.Add(node.Name, reader["NewCell"].ToString());
                                break;
                            case "NewPrDPCode":
                                dic.Add(node.Name, reader["NewPrDPCode"].ToString());
                                break;
                            case "NewEmail":
                                dic.Add(node.Name, reader["NewEmail"].ToString());
                                break;
                            case "NewLicenceNum":
                                dic.Add(node.Name, reader["NewLicenceNum"].ToString());
                                break;
                            case "NewStreetAddress1":
                                dic.Add(node.Name, reader["NewStreetAddress1"].ToString());
                                break;
                            case "NewStreetAddress2":
                                dic.Add(node.Name, reader["NewStreetAddress2"].ToString());
                                break;
                            case "NewStreetAddress3":
                                dic.Add(node.Name, reader["NewStreetAddress3"].ToString());
                                break;
                            case "NewStreetAddress4":
                                dic.Add(node.Name, reader["NewStreetAddress4"].ToString());
                                break;
                            case "NewStreetAddress5":
                                dic.Add(node.Name, reader["NewStreetAddress5"].ToString());
                                break;
                            case "NewStreetAddressCode":
                                dic.Add(node.Name, reader["NewStreetAddressCode"].ToString());
                                break;
                            case "NewPostalAddress1":
                                dic.Add(node.Name, reader["NewPostalAddress1"].ToString());
                                break;
                            case "NewPostalAddress2":
                                dic.Add(node.Name, reader["NewPostalAddress2"].ToString());
                                break;
                            case "NewPostalAddress3":
                                dic.Add(node.Name, reader["NewPostalAddress3"].ToString());
                                break;
                            case "NewPostalAddress4":
                                dic.Add(node.Name, reader["NewPostalAddress4"].ToString());
                                break;
                            case "NewPostalAddress5":
                                dic.Add(node.Name, reader["NewPostalAddress5"].ToString());
                                break;
                            case "NewPostalAddressCode":
                                dic.Add(node.Name, reader["NewPostalAddressCode"].ToString());
                                break;
                            case "NewEmployerName":
                                dic.Add(node.Name, reader["NewEmployerName"].ToString());
                                break;
                            case "NewEmployerAddress1":
                                dic.Add(node.Name, reader["NewEmployerAddress1"].ToString());
                                break;
                            case "NewEmployerAddress2":
                                dic.Add(node.Name, reader["NewEmployerAddress2"].ToString());
                                break;
                            case "NewEmployerAddress3":
                                dic.Add(node.Name, reader["NewEmployerAddress3"].ToString());
                                break;
                            case "NewEmployerAddress4":
                                dic.Add(node.Name, reader["NewEmployerAddress4"].ToString());
                                break;
                            case "NewEmployerAddress5":
                                dic.Add(node.Name, reader["NewEmployerAddress5"].ToString());
                                break;
                            case "NewEmployerAddressCode":
                                dic.Add(node.Name, reader["NewEmployerAddressCode"].ToString());
                                break;
                            case "FailRevocationFee":
                                dic.Add(node.Name, reader["FailRevocationFee"].ToString());
                                break;
                            case "WarrantFee":
                                dic.Add(node.Name, reader["WarrantFee"].ToString());
                                break;
                            case "MainWarrantTerm":
                                dic.Add(node.Name, reader["MainWarrantTerm"].ToString());
                                break;
                            case "AltWarrantTerm1":
                                dic.Add(node.Name, reader["AltWarrantTerm1"].ToString());
                                break;
                            case "AltWarrantTerm2":
                                dic.Add(node.Name, reader["AltWarrantTerm2"].ToString());
                                break;
                            case "AltWarrantTerm3":
                                dic.Add(node.Name, reader["AltWarrantTerm3"].ToString());
                                break;
                            case "AltWarrantTerm4":
                                dic.Add(node.Name, reader["AltWarrantTerm4"].ToString());
                                break;
                            case "SheriffAddress1":
                                dic.Add(node.Name, reader["SheriffAddress1"].ToString());
                                break;
                            case "SheriffAddress2":
                                dic.Add(node.Name, reader["SheriffAddress2"].ToString());
                                break;
                            case "SheriffAddress3":
                                dic.Add(node.Name, reader["SheriffAddress3"].ToString());
                                break;
                            case "SheriffAddress4":
                                dic.Add(node.Name, reader["SheriffAddress4"].ToString());
                                break;
                            case "SheriffAddress5":
                                dic.Add(node.Name, reader["SheriffAddress5"].ToString());
                                break;
                            case "SheriffAddressCode":
                                dic.Add(node.Name, reader["SheriffAddressCode"].ToString());
                                break;
                            case "Age":
                                dic.Add(node.Name, reader["Age"].ToString());
                                if (dic["Age"] == "0")
                                {
                                    dic["Age"] = " ";
                                }
                                break;
                            case "ApplicationResult":
                                dic.Add(node.Name, reader["ApplicationResult"].ToString());
                                break;
                            case "ResultDate":
                                dic.Add(node.Name, reader["ResultDate"].ToString());
                                break;
                            case "FirstInstalmentDate":
                                dic.Add(node.Name, reader["FirstInstalmentDate"].ToString());
                                break;
                            case "RejectedAdvised":
                                dic.Add(node.Name, reader["RejectedAdvised"].ToString());
                                break;
                            case "GroundRejection":
                                dic.Add(node.Name, reader["GroundRejection"].ToString());
                                break;
                            case "ProofPaymentReceived":
                                dic.Add(node.Name, reader["ProofPaymentReceived"].ToString());
                                break;
                            case "ProofInstalmentReceived":
                                dic.Add(node.Name, reader["ProofInstalmentReceived"].ToString());
                                break;
                            case "ProofRepresentationReceived":
                                dic.Add(node.Name, reader["ProofRepresentationReceived"].ToString());
                                break;
                            case "ProofNominationReceived":
                                dic.Add(node.Name, reader["ProofNominationReceived"].ToString());
                                break;
                            case "ProofNoticeReceived":
                                dic.Add(node.Name, reader["ProofNoticeReceived"].ToString());
                                break;
                            case "ProofAppearanceReceived":
                                dic.Add(node.Name, reader["ProofAppearanceReceived"].ToString());
                                break;
                            case "ProofAffidavitReceived":
                                dic.Add(node.Name, reader["ProofAffidavitReceived"].ToString());
                                break;
                            case "ProofOther":
                                dic.Add(node.Name, reader["ProofOther"].ToString());
                                break;
                            case "ThisNotificationFee":
                                dic.Add(node.Name, reader["ThisNotificationFee"].ToString());
                                break;
                            case "VehicleImage":                                
                                dic.Add(node.Name, string.Empty);
                                break;
                            case "LicencePlateImage":                                                                
                                dic.Add(node.Name, string.Empty);
                                break;
                            default:
                                //dic.Add(node.Name, ""); 
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        //ErrorLog.AddErrorLog(ex, BatchManager.LastUser);                        
                        dic.Add(node.Name, " ");
                        continue;
                    }
                    

                    if (dic[node.Name] == null || dic[node.Name].ToString() == string.Empty)
                    {
                        dic[node.Name] = " ";
                    }
                                        
                    // Do not show the min datetime to user
                    if (dic[node.Name].ToString().ToLower() == DateTime.MinValue.ToString().ToLower())
                    {
                        dic[node.Name] = " ";
                    }
                }
            }
            
            reader.Close();	   
         
            // fill image info by notice
            if (dic.ContainsKey("VehicleImage"))
	        {
                List<string> imgPath = ImageConvert.GetFullImagePathByNotice(dic["NotTicketNo"].ToString(), "1");
                if (imgPath.Count > 0)
                {
                    System.Drawing.Image imgMain = System.Drawing.Image.FromFile(imgPath[0]);
                    dic["VehicleImage"] = ImageConvert.ImageToBase64(imgMain, System.Drawing.Imaging.ImageFormat.Jpeg);
                }                
	        }

            if (dic.ContainsKey("LicencePlateImage"))
            {
                List<string> imgPath = ImageConvert.GetFullImagePathByNotice(dic["NotTicketNo"].ToString(), "R");
                if (imgPath.Count > 0)
                {
                    System.Drawing.Image imgMain = System.Drawing.Image.FromFile(imgPath[0]);
                    dic["LicencePlateImage"] = ImageConvert.ImageToBase64(imgMain, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
            } 
            
            return dic;
        }

    }
}
