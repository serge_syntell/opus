﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Configuration;
using System.Threading;

using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Enum;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.BatchConsole
{
    class Program
    {
        //private static int NumOfABatch = 0;
        public static int SleepMinutes = 0;
        public static string TemplateFolder = string.Empty;
        public static string CustomXmlFolder = string.Empty;

        public static string APP_TITLE = string.Empty;
        public static string APP_NAME = AartoProjectList.AARTOBatchConsole.ToString();

        static void Main(string[] args)
        {           
            string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
            Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);           

            // Check Last updated Version            
            string errorMessage;
            if (!CheckVersionManager.CheckVersion(AartoProjectList.AARTOBatchConsole, out errorMessage))
            {
                Console.WriteLine(errorMessage);
                EntLibLogger.WriteLog(LogCategory.General, null, errorMessage);                                
                return;
            }

            // Write the started Log
            APP_TITLE = string.Format("{0} - Version {1}\n Last Updated {2}\n Started at: {3}",
                APP_NAME, 
                Assembly.GetExecutingAssembly().GetName().Version.ToString(2), 
                ProjectLastUpdated.AARTOBatchConsole, DateTime.Now);

            string strDatabaseInfo = EntLibLogger.GetServerAndDatabaseName(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());                                

            //Logger.Write(string.Format("{0} \n\r {1}", APP_TITLE, strDatabaseInfo), "General", (int)TraceEventType.Information);
            EntLibLogger.WriteLog(LogCategory.General, null, string.Format("{0} \n\r {1}", APP_TITLE, strDatabaseInfo));                
            Console.Title = APP_TITLE;


            //Console.WriteLine("Running BatchConsole at " +  DateTime.Now.ToString());
            //NumOfABatch = Convert.ToInt32(ConfigurationManager.AppSettings["RecordNumberOfOneBatch"]);
            SleepMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["SleepMinutes"]);// *1000 * 60;
                        
            TemplateFolder = @"Templates\";//ConfigurationManager.AppSettings["TemplateFolder"];
            CustomXmlFolder = @"CustomXmls\";//ConfigurationManager.AppSettings["CustomXmlFolder"];

            //AartoDocumentTypeList
            try
            {
                TList<AartoDocumentType> adtList = BatchManager.GetAARTODocumentTypeListByOutgoing(true);
                foreach (AartoDocumentType adt in adtList)
                {
                    BatchManager.AddDocumentToBatchByDocTypeID(adt.AaDocTypeId, (int)adt.AaDocMaxNoInBatch);
                }

                errorMessage = string.Format("AddDocumentToBatch completed at {0}.", DateTime.Now.ToString());
                Console.WriteLine(errorMessage);
                //Logger.Write(errorMessage, "General", (int)TraceEventType.Information);
                EntLibLogger.WriteLog(LogCategory.General, null, errorMessage);                            

                // get default print document file server
                bool blnConnected = false;

                ImageFileServerQuery query = new ImageFileServerQuery();
                query.Append(ImageFileServerColumn.AaSysFileTypeId, Convert.ToInt32(AartoSystemFileTypeList.AARTOPrintDocument).ToString());
                query.Append(ImageFileServerColumn.IsDefault, "True");
                TList<ImageFileServer> serverList = new ImageFileServerService().Find(query as IFilterParameterCollection);
                if (serverList.Count > 0)
                {                    
                    blnConnected = RemoteConnectHelper.CheckFileServerAvaliable(serverList[0]);
                    if (blnConnected)
                    {
                        PrintEngineBatch.IssusePrintBatches(serverList[0]);
                    }
                    else
                    {
                        errorMessage = "Test image not found, please check default printengine file sever sharing permission.";
                        Console.WriteLine(errorMessage);
                        //Logger.Write(errorMessage, "Error", (int)TraceEventType.Error);
                        EntLibLogger.WriteLog(LogCategory.Error, null, errorMessage);  
                        //ErrorLog.AddErrorLog("Test image not found", "Test image not found, please check default printengine file sever sharing permission.", 5, BatchManager.LastUser);
                    }
                }
                else
                {
                    errorMessage = "Default PrintEngine File server was not found.";
                    Console.WriteLine(errorMessage);
                    //Logger.Write(errorMessage, "Error", (int)TraceEventType.Error);
                    EntLibLogger.WriteLog(LogCategory.Error, null, errorMessage);  
                    //ErrorLog.AddErrorLog("File server not found", "Default PrintEngine File server was not found.", 5, BatchManager.LastUser);
                }

                errorMessage = "Done all the job at " + DateTime.Now.ToString();
                Console.WriteLine(errorMessage);
                //Logger.Write(errorMessage, "General", (int)TraceEventType.Information);
                EntLibLogger.WriteLog(LogCategory.General, null, errorMessage);  
            }
            catch (Exception ex)
            {
                //ErrorLog.AddErrorLog(ex, BatchManager.LastUser);
                Console.WriteLine(ex.Message);
                //Logger.Write(ex.Message, "Error", (int)TraceEventType.Error);
                EntLibLogger.WriteLog(LogCategory.Error, null, errorMessage);  
            }

            // Write the ended Log for end of console app.
            EntLibLogger.WriteLog(LogCategory.General, null, string.Format("{0} – Ended at {1}", APP_NAME, DateTime.Now));  
        }   
    }
}
