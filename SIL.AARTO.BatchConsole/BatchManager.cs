﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.InfringerOption;
using Microsoft.Office.Interop.Word;
namespace SIL.AARTO.BatchConsole
{
    public class BatchManager
    {
        public const string LastUser = "BatchConsole";
        static BatchManager()
        {
        }
        public static TList<AartoDocumentType> GetAARTODocumentTypeListByOutgoing(bool outgoing)
        {
            AartoDocumentTypeQuery query = new AartoDocumentTypeQuery();
            query.Append(AartoDocumentTypeColumn.AaRepDocOutgoing, outgoing.ToString().ToLower());
            return new AartoDocumentTypeService().Find(query as IFilterParameterCollection);
        }
        public static TList<AartoRepDocument> GetAARTORepDocumentListByType(int docTypeID,int num)
        {
            int rowCount = -1;
            AartoRepDocumentQuery query = new AartoRepDocumentQuery();
            query.Append(AartoRepDocumentColumn.AaDocTypeId, docTypeID.ToString());
            query.Append(AartoRepDocumentColumn.AaDocStatusId, ((int)AartoDocumentStatusList.New).ToString());
            return new AartoRepDocumentService().Find(query as IFilterParameterCollection, "", 0, num, out rowCount);
        }
        public static TList<AartoNoticeDocument> GetAARTONoticeDocumentListByType(int docTypeID, int num)
        {
            int rowCount = -1;
            AartoNoticeDocumentQuery query = new AartoNoticeDocumentQuery();
            query.Append(AartoNoticeDocumentColumn.AaDocTypeId, docTypeID.ToString());
            query.Append(AartoNoticeDocumentColumn.AaDocStatusId, ((int)AartoDocumentStatusList.New).ToString());
            return new AartoNoticeDocumentService().Find(query as IFilterParameterCollection, "", 0, num, out rowCount);

        }
        public static void AddDocumentToBatchByDocTypeID(int docTypeID, int num)
        {
            string seqNo = ((AartoDocumentTypeList)docTypeID).ToString() + "-" + ConvertDateTime.GetDateTimeNowString() + "-";
            bool continueDo = true;
            int counter = 1;
            AartoDocBatch batch = null;
            if (AARTODocumentSourceTable.IsForNotice(docTypeID) == AartoDocumentSourceTableList.AARTONoticeDocument)
            {
                #region notice doc
                TList<AartoNoticeDocument> ardList = null;
                while (continueDo)
                {
                    ardList = BatchManager.GetAARTONoticeDocumentListByType(docTypeID, num);
                    if (ardList.Count > 0)
                    {
                        using (ConnectionScope.CreateTransaction())
                        {
                            batch = CreateBatch(seqNo + counter.ToString().PadLeft(3, '0'), docTypeID);
                            foreach (AartoNoticeDocument doc in ardList)
                            {
                                //doc.AaDocBatchId = batch.AaDocBatchId;
                                //doc.LastUser = LastUser;
                                //doc.AaDocStatusId = (int)AartoDocumentStatusList.Issued;
                                //doc.AaDocIssuedDate = DateTime.Now;
                                AARTODocumentManager.UpdateAARTODocumentAfterCreateBatch(doc.AaNotDocId, batch.AaDocBatchId, doc.AaDocTypeId, LastUser);
                            }
                            //new AartoNoticeDocumentService().Save(ardList);
                            ConnectionScope.Complete();
                        }
                        counter++;
                    }
                    else continueDo = false;
                }
                #endregion
            }
            else if (AARTODocumentSourceTable.IsForNotice(docTypeID) == AartoDocumentSourceTableList.AARTORepDocument)
            {
                #region rep doc
                TList<AartoRepDocument> ardList = null;
                while (continueDo)
                {
                    ardList = BatchManager.GetAARTORepDocumentListByType(docTypeID, num);
                    if (ardList.Count > 0)
                    {
                        using (ConnectionScope.CreateTransaction())
                        {
                            batch = CreateBatch(seqNo + counter.ToString().PadLeft(3, '0'), docTypeID);
                            foreach (AartoRepDocument doc in ardList)
                            {
                                //doc.AaDocBatchId = batch.AaDocBatchId;
                                //doc.LastUser = LastUser;
                                //doc.AaDocStatusId = (int)AartoDocumentStatusList.Issued;
                                AARTODocumentManager.UpdateAARTODocumentAfterCreateBatch(doc.AaRepDocId, batch.AaDocBatchId, doc.AaDocTypeId, LastUser);
                            }
                            //new AartoRepDocumentService().Save(ardList);
                            ConnectionScope.Complete();
                        }
                        counter++;
                    }
                    else continueDo = false;
                }
                #endregion
            }
        }
        private static AartoDocBatch CreateBatch(string batchNo,int docTypeID)
        {
            AartoDocBatch batch = new AartoDocBatch();
            batch.AaDocBatchNo = batchNo;
            batch.AaDocIssuedDate = ConvertDateTime.GetShortDateTime(DateTime.Now);
            batch.AaDocTypeId = docTypeID;
            batch.AaDocStatusId = (int)AartoDocumentStatusList.New;
            batch.LastUser = LastUser;
            return new AartoDocBatchService().Save(batch);
        }

        public static bool ConvertToPDF(string sourceDocxPath)
        {
            return ConvertToPDF(sourceDocxPath, sourceDocxPath.Replace(".docx", ".pdf"));
        }

        public static bool ConvertToPDF(string sourceDocxPath, string targetFilePath)
        {
            bool result = false;
            // Make sure the source document exists.
            if (!System.IO.File.Exists(sourceDocxPath))
                throw new Exception("The specified source document does not exist.");

            // Create an instance of the Word ApplicationClass object.          
            ApplicationClass wordApplication = new ApplicationClass();
            Microsoft.Office.Interop.Word.Document wordDocument = null;

            // Declare variables for the Documents.Open and ApplicationClass.Quit method parameters. 
            object paramSourceDocPath = sourceDocxPath;
            object paramMissing = Type.Missing;

            // Declare variables for the Document.ExportAsFixedFormat method parameters.
            string paramExportFilePath = targetFilePath;
            WdExportFormat paramExportFormat = WdExportFormat.wdExportFormatPDF;
            bool paramOpenAfterExport = false;
            WdExportOptimizeFor paramExportOptimizeFor = WdExportOptimizeFor.wdExportOptimizeForOnScreen;
            WdExportRange paramExportRange = WdExportRange.wdExportAllDocument;
            int paramStartPage = 0;
            int paramEndPage = 0;
            WdExportItem paramExportItem = WdExportItem.wdExportDocumentContent;
            bool paramIncludeDocProps = true;
            bool paramKeepIRM = true;
            WdExportCreateBookmarks paramCreateBookmarks =
                WdExportCreateBookmarks.wdExportCreateWordBookmarks;
            bool paramDocStructureTags = true;
            bool paramBitmapMissingFonts = true;
            bool paramUseISO19005_1 = false;

            try
            {
                // Open the source document.
                wordDocument = wordApplication.Documents.Open(ref paramSourceDocPath, ref paramMissing, ref paramMissing, ref paramMissing,
                    ref paramMissing, ref paramMissing, ref paramMissing, ref paramMissing, ref paramMissing, ref paramMissing,
                    ref paramMissing, ref paramMissing, ref paramMissing, ref paramMissing, ref paramMissing, ref paramMissing);

                // Export it in the specified format.
                if (wordDocument != null)
                    wordDocument.ExportAsFixedFormat(paramExportFilePath, paramExportFormat, paramOpenAfterExport, paramExportOptimizeFor,
                        paramExportRange, paramStartPage, paramEndPage, paramExportItem, paramIncludeDocProps, paramKeepIRM, paramCreateBookmarks, paramDocStructureTags,
                        paramBitmapMissingFonts, paramUseISO19005_1, ref paramMissing);
                result = true;
            }
            catch (Exception e)
            {                
                throw e;
            }
            finally
            {
                // Close and release the Document object.
                if (wordDocument != null)
                {
                    wordDocument.Close(ref paramMissing, ref paramMissing, ref paramMissing);
                    wordDocument = null;
                }

                // Quit Word and release the ApplicationClass object.
                if (wordApplication != null)
                {
                    wordApplication.Quit(ref paramMissing, ref paramMissing, ref paramMissing);
                    wordApplication = null;
                }

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();                
            }
            return result;
        }
    }
}
