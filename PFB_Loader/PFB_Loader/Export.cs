using System.IO;
using System.Xml;
using Stalberg.TMS.PFB_Loader.Data;

namespace Stalberg.TMS.PFB_Loader
{
    /// <summary>
    /// Contains the methods to prepare and export the film data to TMS by FTP
    /// </summary>
    internal static class Export
    {
        // Constants
        //internal static readonly string[] HASH_INCLUSIONS = new string[] { ".JPG", ".JPEG", ".JP2", ".TS1", Hashing.HASH_EXTENSION.ToUpper() };

        /// <summary>
        /// Gets the server XML data.
        /// </summary>
        public static void GetProgramOptions()
        {
            //load xml file with system parameters
            string currentDir = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Path.Combine(currentDir, "SysParam.xml"));
            XmlNode root = xmlDoc.DocumentElement;

            // Create Struct
            if (root != null)
                if (root.HasChildNodes)
                {
                    foreach (XmlNode node in root.ChildNodes)
                    {
                        switch (node.Name)
                        {
                            case "constr":
                                Options.ProgramOptions.ConnectionString = node.InnerText;
                                break;

                            case "BatchLocation":
                                Options.ProgramOptions.BatchLocation = node.InnerText;
                                break;

                                //case "procstr":
                                //    Options.ProgramOptions.ProcessName = node.InnerText;
                                //    Options.ProgramOptions.FtpParameters.Process = node.InnerText;
                                //    break;

                                //case "contractor":
                                //    Options.ProgramOptions.FtpParameters.Contractor = node.InnerText;
                                //    break;

                                //case "ftpExportServer":
                                //    Options.ProgramOptions.FtpParameters.ExportServer = node.InnerText;
                                //    break;

                                //case "ftpExportPath":
                                //    Options.ProgramOptions.FtpParameters.ExportPath = node.InnerText;
                                //    break;

                                //case "ftpHostServer":
                                //    Options.ProgramOptions.FtpParameters.HostServer = node.InnerText;
                                //    break;

                                //case "ftpHostIP":
                                //    Options.ProgramOptions.FtpParameters.HostIP = node.InnerText;
                                //    break;

                                //case "ftpHostUser":
                                //    Options.ProgramOptions.FtpParameters.HostUser = node.InnerText;
                                //    break;

                                //case "ftpHostPass":
                                //    Options.ProgramOptions.FtpParameters.HostPassword = node.InnerText;
                                //    break;

                                //case "ftpHostPath":
                                //    Options.ProgramOptions.FtpParameters.HostPath = node.InnerText;
                                //    break;

                                //case "ftpEmail":
                                //    Options.ProgramOptions.FtpParameters.Email = node.InnerText;
                                //    break;

                                //case "ftpSMTP":
                                //    Options.ProgramOptions.FtpParameters.SmtpServer = node.InnerText;
                                //    break;

                                //case "mode":
                                //    try
                                //    {
                                //        Options.ProgramOptions.Mode =
                                //            (Mode)Enum.Parse(typeof(Mode), node.InnerText, true);
                                //    }
                                //    catch (ArgumentException ex)
                                //    {
                                //        Beginnings.Writer.WriteError("There was an error parsing the application mode from the SysParam file.", ex);
                                //    }
                                //    break;

                                //case "extractAllImages":
                                //    bool extractAllImages;
                                //    if (bool.TryParse(node.InnerText, out extractAllImages))
                                //        Options.ProgramOptions.ExtractAllImages = extractAllImages;
                                //    break;

                                //case "jp2Conversion":
                                //    bool jp2Conversion;
                                //    if (bool.TryParse(node.InnerText, out jp2Conversion))
                                //        Options.ProgramOptions.Jp2Conversion = jp2Conversion;
                                //    break;

                            default:
                                //Options.ProgramOptions.ConnectionString = "none";
                                break;
                        }
                    }
                }
        }

    }
}
