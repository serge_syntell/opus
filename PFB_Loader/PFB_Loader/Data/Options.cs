namespace Stalberg.TMS.PFB_Loader.Data
{
    /// <summary>
    /// Represents the option data of the program
    /// </summary>
    internal class Options
    {
        // Fields
        private string connectionString = string.Empty;
        private string batchLocation = string.Empty;
        private string procStr = string.Empty;
        private bool doNothing;
        private bool isDebug;
        //private Mode mode;
        //private bool extractAllImages = true;
        //private bool jp2Conversion = true;
        //private string indabaSource = string.Empty;
        //private string indabaConnectionString = string.Empty;

        private static Options singletonOptions;

        /// <summary>
        /// Initializes a new instance of the <see cref="Options"/> class.
        /// </summary>
        static Options()
        {
            Options.singletonOptions = new Options();
        }

        /// <summary>
        /// Gets the program options.
        /// </summary>
        /// <value>The program options.</value>
        public static Options ProgramOptions
        {
            get
            {
                return Options.singletonOptions;
            }
        }

        /// <summary>
        /// Gets or sets the name of the process.
        /// </summary>
        /// <value>The name of the process.</value>
        public string ProcessName
        {
            get { return procStr; }
            set { procStr = value; }
        }

        /// <summary>
        /// Gets or sets the Database connection string.
        /// </summary>
        /// <value>The connection string.</value>
        public string ConnectionString
        {
            get { return connectionString; }
            set { connectionString = value; }
        }

        /// <summary>
        /// Gets or sets the Database connection string.
        /// </summary>
        /// <value>The connection string.</value>
        public string BatchLocation
        {
            get { return batchLocation; }
            set { batchLocation = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to do nothing as a result of a command line argument.
        /// </summary>
        /// <value><c>true</c> if [do nothing]; otherwise, <c>false</c>.</value>
        public bool DoNothing
        {
            get { return doNothing; }
            set { doNothing = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is in debug mode.
        /// </summary>
        /// <value><c>true</c> if this instance is debug; otherwise, <c>false</c>.</value>
        public bool IsDebug
        {
            get { return isDebug; }
            set { isDebug = value; }
        }

        /// <summary>
        /// Emails the log file .
        /// </summary>
        public void EmailLogFile()
        {
            //Beginnings.Writer.Close();

            //try
            //{
            //    MailAddress from = new MailAddress(this.ftpParameters.Email);
            //    MailAddress to = new MailAddress(this.ftpParameters.Email);

            //    MailMessage mail = new MailMessage(from, to);
            //    mail.Subject = this.ftpParameters.HostServer.ToUpper() + " " + this.procStr + " log file";
            //    //mail.Body = message;
            //    mail.BodyEncoding = Encoding.UTF8;
            //    Attachment myAttachment = new Attachment(Beginnings.Writer.LogFileName);
            //    mail.Attachments.Add(myAttachment);

            //    try
            //    {
            //        SmtpClient mailClient = new SmtpClient();
            //        mailClient.Host = this.ftpParameters.SmtpServer;
            //        mailClient.UseDefaultCredentials = true;
            //        mailClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
            //        mailClient.Send(mail);

            //        Beginnings.Writer = new LogWriter(true);
            //        Beginnings.Writer.WriteLine(string.Format("Main: email log file sent to {0} at: {1}", this.ftpParameters.Email, DateTime.Now));
            //        Beginnings.Writer.Close();
            //    }
            //    catch (Exception smtpEx)
            //    {
            //        Beginnings.Writer = new LogWriter(true);
            //        Beginnings.Writer.WriteError("Main: Failed to send email of log file " + smtpEx);
            //        Beginnings.Writer.Close();
            //    }
            //}
            //catch (Exception emailEx)
            //{
            //    Beginnings.Writer = new LogWriter(true);
            //    Beginnings.Writer.WriteError("Main: Failed to send email of log file " + emailEx);
            //    Beginnings.Writer.Close();
            //}
        }

    }
}