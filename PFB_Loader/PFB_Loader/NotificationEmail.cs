﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using Stalberg.TMS.Data;
using Stalberg.TMS.PFB_Loader.Data;

namespace Stalberg.TMS.PFB_Loader
{
    public class NotificationEmail
    {
        // Fields
        private string smtp;
        private string fromName = "";
        private string fromEmail = "";
        private string m_strAARTO_Report_Type = "";
        private string m_fileName = "";
        private int m_number;
        public NotificationEmail(string strAARTO_Report_Type, string strFileName, int number)
        {
            m_strAARTO_Report_Type = strAARTO_Report_Type;
            m_fileName = strFileName;
            m_number = number;

            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(Path.Combine(Environment.CurrentDirectory, "SysParam.xml"));

            XmlNode node = xDoc.SelectSingleNode("/codes/smtp");
            if (node != null)
                this.smtp = node.InnerText;
            node = xDoc.SelectSingleNode("/codes/FromName");
            if (node != null)
                this.fromName = node.InnerText;
            node = xDoc.SelectSingleNode("/codes/FromEmail");
            if (node != null)
                this.fromEmail = node.InnerText;
        }

        public int Send()
        {
            MailMessage msg = new MailMessage();
            msg.IsBodyHtml = false;
            GetParameters(msg);
            GetBody(msg);

            if (msg.To.Count == 0)
            {
                Loagfailure(msg);
                return 0;
            }

            try
            {
                SmtpClient server = new SmtpClient(this.smtp);
                server.Send(msg);
                return 1;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Assert(false, ex.Message);
                Loagfailure(msg);
                return -1;
            }
        }

        private void Loagfailure(MailMessage msg)
        {
            string strInfo = "To:";
            foreach (MailAddress ma in msg.To)
            {
               strInfo +=   ma.DisplayName +  "(" + ma.Address + ");"; 
            }
            strInfo += Environment.NewLine;
            strInfo += "CC:";
            foreach (MailAddress ma in msg.CC)
            {
               strInfo +=   ma.DisplayName +  "(" + ma.Address + ");"; 
            }
            strInfo += Environment.NewLine;
            strInfo += "BCC:";
            foreach (MailAddress ma in msg.Bcc)
            {
               strInfo +=   ma.DisplayName +  "(" + ma.Address + ");"; 
            }
            strInfo += Environment.NewLine;
            strInfo += "Body:" + msg.Body;

            MainLoader.logger.Debug(strInfo);
        }

        private void GetParameters(MailMessage msg)
        {
            AARTO_SAPO_RecipientDB aARTO_SAPO_RecipientDB = new AARTO_SAPO_RecipientDB(Options.ProgramOptions.ConnectionString);

            msg.Subject = "PFB_Loader notification email";
            msg.From = new MailAddress(fromEmail, fromName);

           List<AARTO_SAPO_Recipient> RecipientList =  aARTO_SAPO_RecipientDB.ListAARTO_SAPO_Recipients(m_strAARTO_Report_Type);
            foreach(AARTO_SAPO_Recipient recipient in RecipientList)
            {
               if(recipient.Function == "Send To")
               {
                    msg.To.Add(new MailAddress(recipient.Email, recipient.Name));
               }
               else if(recipient.Function == "CC")
               {
                    msg.CC.Add(new MailAddress(recipient.Email, recipient.Name));
               }
                else if(recipient.Function == "BCC")
               {
                    msg.Bcc.Add(new MailAddress(recipient.Email, recipient.Name));
               }
            }
        }

        private void GetBody(MailMessage msg)
        {
            string strBody = "";

            strBody += "Service Provider: Syntell" + Environment.NewLine;
            strBody += "File name:" + m_fileName + Environment.NewLine;
            strBody += "Date file created:" + DateTime.Now.ToString("yyyy/MM/dd") + Environment.NewLine;
            strBody += "Time file created:" + DateTime.Now.ToString("HH:ss") + Environment.NewLine;
            strBody += "Number of violations:" + m_number.ToString() + Environment.NewLine;
            strBody += "Expected delivery date:" + DateTime.Now.AddDays(1).ToString("yyyy/MM/dd") + Environment.NewLine;
            strBody += "NOTE: If delivery of file has not taken place within 24 hours of this email, please contact  Syntell Contact person:" + fromName + "(" + fromEmail + ")";

            msg.Body = strBody;
        }
    }
}