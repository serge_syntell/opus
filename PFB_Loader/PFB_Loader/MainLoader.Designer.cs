namespace Stalberg.TMS.PFB_Loader
{
    partial class MainLoader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreateBatch = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lbl200Count = new System.Windows.Forms.Label();
            this.btnCloseBatch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dgPrintFileBatches = new System.Windows.Forms.DataGridView();
            this.lblLastUpdated = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrintFileBatches)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCreateBatch
            // 
            this.btnCreateBatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCreateBatch.Location = new System.Drawing.Point(191, 449);
            this.btnCreateBatch.Name = "btnCreateBatch";
            this.btnCreateBatch.Size = new System.Drawing.Size(93, 23);
            this.btnCreateBatch.TabIndex = 0;
            this.btnCreateBatch.Text = "Create Batch";
            this.btnCreateBatch.UseVisualStyleBackColor = true;
            this.btnCreateBatch.Click += new System.EventHandler(this.btnCreateBatch_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.Location = new System.Drawing.Point(397, 449);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lbl200Count
            // 
            this.lbl200Count.AutoSize = true;
            this.lbl200Count.Location = new System.Drawing.Point(188, 9);
            this.lbl200Count.Name = "lbl200Count";
            this.lbl200Count.Size = new System.Drawing.Size(0, 13);
            this.lbl200Count.TabIndex = 2;
            // 
            // btnCloseBatch
            // 
            this.btnCloseBatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCloseBatch.Location = new System.Drawing.Point(302, 449);
            this.btnCloseBatch.Name = "btnCloseBatch";
            this.btnCloseBatch.Size = new System.Drawing.Size(75, 23);
            this.btnCloseBatch.TabIndex = 3;
            this.btnCloseBatch.Text = "Close Batch";
            this.btnCloseBatch.UseVisualStyleBackColor = true;
            this.btnCloseBatch.Click += new System.EventHandler(this.btnCloseBatch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "No. of Infringements to be written:";
            // 
            // dgPrintFileBatches
            // 
            this.dgPrintFileBatches.AllowUserToAddRows = false;
            this.dgPrintFileBatches.AllowUserToDeleteRows = false;
            this.dgPrintFileBatches.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPrintFileBatches.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPrintFileBatches.Location = new System.Drawing.Point(12, 25);
            this.dgPrintFileBatches.Name = "dgPrintFileBatches";
            this.dgPrintFileBatches.ReadOnly = true;
            this.dgPrintFileBatches.Size = new System.Drawing.Size(781, 418);
            this.dgPrintFileBatches.TabIndex = 5;
            // 
            // lblLastUpdated
            // 
            this.lblLastUpdated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastUpdated.AutoSize = true;
            this.lblLastUpdated.Location = new System.Drawing.Point(656, 462);
            this.lblLastUpdated.Name = "lblLastUpdated";
            this.lblLastUpdated.Size = new System.Drawing.Size(77, 13);
            this.lblLastUpdated.TabIndex = 6;
            this.lblLastUpdated.Text = "Last Updated: ";
            this.lblLastUpdated.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MainLoader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 484);
            this.Controls.Add(this.lblLastUpdated);
            this.Controls.Add(this.dgPrintFileBatches);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCloseBatch);
            this.Controls.Add(this.lbl200Count);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnCreateBatch);
            this.Name = "MainLoader";
            this.Text = "Create Print file batches for SAPO";
            ((System.ComponentModel.ISupportInitialize)(this.dgPrintFileBatches)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCreateBatch;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lbl200Count;
        private System.Windows.Forms.Button btnCloseBatch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgPrintFileBatches;
        private System.Windows.Forms.Label lblLastUpdated;
    }
}

