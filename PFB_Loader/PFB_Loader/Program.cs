using System;
using System.Windows.Forms;

namespace Stalberg.TMS.PFB_Loader
{
    static class Program
    {
        // Constants
        internal const string APP_NAME = "PFB_Loader";
        internal const string LAST_USER = "PFB_Loader";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainLoader());
        }
    }
}