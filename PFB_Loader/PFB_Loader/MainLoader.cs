using System;
using System.Data.SqlClient;
using System.Drawing;
using System.Collections;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;
using NLog;
using NLog.Targets;
using Stalberg.TMS;
using Stalberg.TMS.PFB_Loader.Data;
using Stalberg.TMS.PFB_Loader.Components;
using Stalberg.TMS_3P_Loader;
using System.Data;

namespace Stalberg.TMS.PFB_Loader
{
    public partial class MainLoader : Form
    {
        // Static
        public static readonly Logger logger;
        private static readonly Regex folderRegex = new Regex(@"^SYN_20\d{2}(?:01|02|03|04|05|06|07|08|09|10|11|12)\d{2}\d{6}$", RegexOptions.Compiled);
        //private static readonly Regex tempFileRegex = new Regex(@"^SYN_[PV]20\d{2}(?:01|02|03|04|05|06|07|08|09|10|11|12)\d{2}\d{6}\.tmp$", RegexOptions.Compiled);

        private string lastUpdated = "2011-04-13";
        static MainLoader()
        {
            FileTarget target = new FileTarget();
            target.Layout = "${longdate} ${logger} ${message}";
            string fileName = string.Format("PFB_Loader_{0}.log", DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
            target.FileName = "${basedir}/logs/" + fileName;
            target.KeepFileOpen = false;

            NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);

            logger = LogManager.GetCurrentClassLogger();
        }

        // Fields
        private int CountOf200;
        private readonly Hashing hashing;
        private readonly ArrayList noticeList = new ArrayList();
        private readonly NoticeDB noticeDB;
        private readonly InfringementData iData;
        private readonly ChargeDB chargeDB;
        private int maxSizeBatch;

        public MainLoader()
        {
            this.InitializeComponent();

            lblLastUpdated.Text = "Last updated: " + lastUpdated;

            logger.Debug("Started...");

            // Check if this is the only instance running
            Singleton singleton = new Singleton("PFB_Loader");
            singleton.OtherProcessesToCheckFor(new string[]
                                                   {
                                                       "TMS_ViolationLoader",
                                                       "TMS_CD_Loader",
                                                       "TMS_TPExInt",
                                                       "Stalberg.Payfine.Extract",
                                                       "Stalberg.ThaboExporter",
                                                       "RoadblockDataExtract"
                                                   });
            if (singleton.Check())
            {
                logger.Error("Singleton violated!");

                if (Options.ProgramOptions.IsDebug)
                {
                    //alert user that the program can not run at the moment as other programs are running, then exit
                }
                return;
            }
            //Set all program settings
            Export.GetProgramOptions();

            noticeDB = new NoticeDB(Options.ProgramOptions.ConnectionString);
            iData = new InfringementData(Options.ProgramOptions.ConnectionString);
            chargeDB = new ChargeDB(Options.ProgramOptions.ConnectionString);
            hashing = new Hashing();

            ShowData();

            logger.Debug("Initialisation complete");
        }

        private void ShowData()
        {
            CountOf200 = 0;
            //must show how many infringements need to be processed
            SqlDataReader reader = noticeDB.GetNoticeList(0, 200);
            while (reader.Read())
            {
                int NotIntNo = Convert.ToInt32(reader["NotIntNo"]);
                noticeList.Add(NotIntNo);
                CountOf200 += 1;
            }

            reader.Close();

            lbl200Count.Text = CountOf200.ToString();

            //Need to disable the create batch button if there are no infringements to be processed
            //if (CountOf200 == 0)
            //    btnCreateBatch.Enabled = false;

            //need to disable the close batch if there are no open batches
            DataTable dt = new DataTable();
            dt.Load(iData.GetPrintFileBatchList());
            dgPrintFileBatches.DataSource = dt;
            dgPrintFileBatches.DataMember = "";
        }

        private void btnCreateBatch_Click(object sender, EventArgs e)
        {
            logger.Debug("Running create batch...");

            long runningSize = 0;
            int infringementCounter = 0;
            int imageCounter = 0;
            int recordCounter = 0;
            string err = "";

            //get auth rule for max size of batch/DVD
            AuthorityRulesDetails ard_Batch = new AuthorityRulesDetails();

            //the autIntNo does not matter - the infringements get sent for all authorities...
            AuthorityDB authDB = new AuthorityDB(Options.ProgramOptions.ConnectionString);
            SqlDataReader authReader = authDB.GetAuthorityList(0, "AutIntNo");
            while (authReader.Read())
            {
                ard_Batch.AutIntNo = Convert.ToInt32(authReader["AutIntNo"]);
                ard_Batch.ARCode = "2800"; //need to check on what number is best to use
                ard_Batch.LastUser = Program.LAST_USER;
                DefaultAuthRules authRule_Batch = new DefaultAuthRules(ard_Batch, Options.ProgramOptions.ConnectionString);
                authRule_Batch.SetDefaultAuthRule();
                maxSizeBatch = ard_Batch.ARNumeric;

                //autIntNo = Convert.ToInt32(authReader["AutIntNo"]);
            }
            logger.Debug("Max Batch Size: {0}", maxSizeBatch);

            //get total number of infringements that still need to be sent to SAPO
            //Have stored that in CountOf200 already

            //Check for an open batch/file for notice details
            //Check for an open batch/file for image linking
            //string uniqueDocID = "";
            string batchLocation = Options.ProgramOptions.BatchLocation;
            string nName = "";
            logger.Debug("Batch Location: {0}", batchLocation);

            //if no open batch/file create new batch and file for notice details
            string completeCheck = string.Empty;
            DirectoryInfo diBatchLocation = new DirectoryInfo(batchLocation);
            if (!diBatchLocation.Exists)
                diBatchLocation.Create();

            // Check if there is a valid open batch folder
            foreach (DirectoryInfo di in diBatchLocation.GetDirectories())
            {
                if (di.Name.IndexOf("Completed") < 0)
                {
                    if (folderRegex.IsMatch(di.Name))
                    {
                        completeCheck = di.Name;
                        logger.Debug("Existing batch found: {0}", di.FullName);
                        break;
                    }

                    logger.Error("ERROR!!! Found an illegal folder in the PFB output file structure: {0}", di.FullName);
                }
            }

            //BD attempting to make sure that we have no connections hanging onto the file system
            diBatchLocation = null;

            int maxBatchNo = iData.PFB_MaxBatchNo(ref err);
            string newBatchNo = "";
            int batchNo = 0;
            string fileNoticePath;
            string BatchPath;

            //if complete check is set to the file name then there is an open batch and we must use that before creating a new batch
            if (completeCheck.Length > 4)
            {
                BatchPath = batchLocation + completeCheck;
                fileNoticePath = BatchPath + "\\SYN_P" + completeCheck.Substring(4) + ".tmp";
                logger.Debug("Notice File created: {0}", fileNoticePath);
            }
            else
            {
                //if no open batch/file create new batch and file for image linking
                //Need to get file sequence number out of my new table... greatest of batch no

                if (maxBatchNo != -1)
                {
                    batchNo = maxBatchNo + 1; //Convert.ToInt32(maxBatchNo.Substring(12, 6)) + 1;

                    switch (batchNo.ToString().Length)
                    {
                        case 1:
                            newBatchNo = "00000" + batchNo;
                            break;
                        case 2:
                            newBatchNo = "0000" + batchNo;
                            break;
                        case 3:
                            newBatchNo = "000" + batchNo;
                            break;
                        case 4:
                            newBatchNo = "00" + batchNo;
                            break;
                        case 5:
                            newBatchNo = "0" + batchNo;
                            break;
                        case 6:
                            newBatchNo = batchNo.ToString();
                            break;
                    }

                    logger.Debug("Batch number: {0}", newBatchNo);
                }

                // Batch directory
                BatchPath = batchLocation + "SYN_" + DateTime.Now.Year + String.Format("{0:MM}", DateTime.Now) +
                            String.Format("{0:dd}", DateTime.Now) + newBatchNo; //needs sequence number included.
                Directory.CreateDirectory(BatchPath);

                // Batch validation file (.tmp)
                fileNoticePath = BatchPath + "\\SYN_P" + DateTime.Now.Year + String.Format("{0:MM}", DateTime.Now) +
                                 String.Format("{0:dd}", DateTime.Now) + newBatchNo + ".tmp";
            }

            StreamWriter fsNotice = File.CreateText(fileNoticePath);
            logger.Debug("Created notice file: {0}", fileNoticePath);

            //Loop through each infringement
            foreach (int notIntNo in noticeList)
            {
                // (a) check that total size counter and the new image sizes are still less than total size auth rule
                FileInfo vFile;
                FileInfo nFile;
                logger.Debug("Getting data for NotIntNo: {0}", notIntNo);

                InfringementDetails infringeDetails = iData.GetInfringementDetails(notIntNo);
                //ImageProcessing = new ImageProcesses(Options.ProgramOptions.ConnectionString);
                //need to check that the charge legislation is set to 1, else must not add to batch
                if (infringeDetails.ChgLegislation == 1)
                {
                    //extra check added so that both images have to be checked for the process to continue
                    //if (infringeDetails.VehicleImage != null && infringeDetails.NumberPlateImage != null)
                    if (infringeDetails.VehicleOriginalImage != null && infringeDetails.NumberPlateOriginalImage != null)
                    {
                        string vName = "";
                        try
                        {

                            MemoryStream ms_vImage = new MemoryStream(infringeDetails.VehicleOriginalImage);
                            Image vImage = new Bitmap(ms_vImage);

                            //Do I first need to save the image to the file structure before I can calculate the size
                            //The name of the image must now correspond to the unique doc ID
                            //vName = BatchPath + "\\" + newUniqueDocID + "_0.jpg";
                            vName = BatchPath + "\\" + infringeDetails.NotUniqueDocID + "_0.jpg";
                            vImage.Save(vName, System.Drawing.Imaging.ImageFormat.Jpeg);
                            logger.Debug("Saving vehicle image: {0}", vName);

                            vFile = new FileInfo(vName);
                            runningSize += vFile.Length / 1000;

                            imageCounter += 1;
                        }
                        catch (Exception ex)
                        {
                            logger.Debug(ex.Message);
                            logger.Debug("Error saving vehicle image, charge set to 1003");
                            chargeDB.UpdateStatus(infringeDetails.ChgIntNo, 1003);
                            //need to delete image
                            FileInfo vFile_todelete = new FileInfo(vName);
                            vFile_todelete.Delete();
                            nFile = null;
                            vFile = null;
                            vFile_todelete = null;
                            continue;
                        }

                        if (infringeDetails.NumberPlateOriginalImage != null)
                        {
                            try
                            {
                                MemoryStream ms_nImage = new MemoryStream(infringeDetails.NumberPlateOriginalImage);
                                Image nImage = new Bitmap(ms_nImage);

                                nName = BatchPath + "\\" + infringeDetails.NotUniqueDocID + "_1.jpg";
                                nImage.Save(nName, System.Drawing.Imaging.ImageFormat.Jpeg);
                                logger.Debug("Saving number plate image: {0}", nName);

                                nFile = new FileInfo(nName);
                                runningSize += nFile.Length / 1000;

                                imageCounter += 1;
                            }
                            catch (Exception ex)
                            {
                                logger.Debug(ex.Message);
                                logger.Debug("Error saving number plate image, charge set to 1003");
                                chargeDB.UpdateStatus(infringeDetails.ChgIntNo, 1003);
                                //need to delete the first image that was created
                                FileInfo vFile_todelete = new FileInfo(vName);
                                vFile_todelete.Delete();

                                FileInfo nFile_todelete = new FileInfo(nName);
                                nFile_todelete.Delete();

                                nFile = null;
                                vFile = null;
                                vFile_todelete = null;
                                nFile_todelete = null;
                                continue;
                            }
                        }

                        if (runningSize < maxSizeBatch)
                        {
                            //write new line to txt file for notice details
                            string iLine = iData.CreateInfringementLine(infringeDetails);
                            logger.Debug("Notice line: " + iLine.Trim());
                            try
                            {
                                fsNotice.Write(iLine);
                            }
                            catch (Exception ex)
                            {
                                logger.Debug(ex.Message);
                            }

                            //save images to batch folder - has happened already...

                            //Update infringement to 210
                            chargeDB.UpdateStatus(infringeDetails.ChgIntNo, 210);
                            logger.Debug("Setting ChgIntNo: {0}", infringeDetails.ChgIntNo);

                            //Update notice with filename
                            //dont need to do this as the unique id is already set
                            //int notice_id = noticeDB.UpdateNotice by Column("notice", "PFBName", newUniqueDocID, 0, Program.LAST_USER, ref err, notIntNo);
                            //if -1 there was an error

                            //create new line in print file batch table
                            //due to not needing to calculate the unique doc id, am passing 0 into the counter
                            int success = this.iData.CreateNewPrintFileBatch(infringeDetails.NotUniqueDocID, newBatchNo, DateTime.Now, 0, batchNo, ref err);
                            logger.Debug("recorded new print batch file: {0} ({1})", infringeDetails.NotUniqueDocID, success);

                            //add sum of images to size counter - has already been done
                            //add 1 to infringement counter - has already been done
                            recordCounter++;
                        }
                        else
                        {
                            //have to delete the images we have already copied to the folder structure
                            FileInfo nFile_todelete = new FileInfo(nName);
                            nFile_todelete.Delete();

                            FileInfo vFile_todelete = new FileInfo(vName);
                            vFile_todelete.Delete();

                            //BD attempting to make sure that we have no connections hanging onto the file system
                            nFile = null;
                            vFile = null;
                            nFile_todelete = null;
                            vFile_todelete = null;

                            fsNotice.Flush();
                            fsNotice.Close();
                            fsNotice.Dispose();

                            this.CloseBatch(0);

                            lbl200Count.Text = infringementCounter + " " + runningSize;

                            //update screen:
                            //1. set new outstanding infringement total
                            //2. notify user that the batch is complete, batch name, location
                            //update grid with list of batches and dates

                            //need msg to tell user that has been created and closed

                            return;
                        }
                    }
                    else
                    {
                        //if there is no vehicle image then it must not be added to the DVD and the status must be set to 110
                        chargeDB.UpdateStatus(infringeDetails.ChgIntNo, 1003);
                    }
                }
                //BD attempting to make sure that we have no connections hanging onto the file system
                nFile = null;
                vFile = null;
            }

            lbl200Count.Text = infringementCounter + " " + runningSize;

            fsNotice.Flush();
            fsNotice.Close();
            fsNotice.Dispose();

            logger.Debug("... Create batch completed.");

            //need msg to tell user that has been created, and can now be closed
            string batchName = "SYN_" + DateTime.Now.Year + String.Format("{0:MM}", DateTime.Now) +
                            String.Format("{0:dd}", DateTime.Now) + newBatchNo;
            MessageBox.Show("Batch (" + batchName + ") created successfully.", "Create Batch", MessageBoxButtons.OK, MessageBoxIcon.Information);
            ShowData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            logger.Debug("Closing PFB Loader.");

            base.Close();
        }

        private void btnCloseBatch_Click(object sender, EventArgs e)
        {
            logger.Debug("Close batch manually ...");
            this.CloseBatch(1);
            ShowData();
            logger.Debug("... Close batch manually completed");
        }

        private void CloseBatch(int userActioned)
        {
            logger.Debug("Close batch process begun...");
            string batchLocation = Options.ProgramOptions.BatchLocation;

            DirectoryInfo diCheck = new DirectoryInfo(batchLocation);
            foreach (DirectoryInfo diClose in diCheck.GetDirectories())
            {
                if (diClose.Name.IndexOf("Completed") < 0)
                {
                    logger.Debug("Found ({0}) to complete.", diClose.FullName);

                    FileInfo[] files = diClose.GetFiles("SYN_P*.tmp");
                    if (files.Length == 0)
                    {
                        logger.Error("No 'SYN_P*.tmp' files found!");
                        continue;
                    }

                    string fileName = files[0].Name;
                    if (fileName.Length < 5)
                    {
                        logger.Error("File({0}) is not a valid notice file name", fileName);
                        continue;
                    }

                    // Get the temp file name and extract the unique doc id, and create the image text file from it
                    int numOfLines = 0;
                    String fileImagePath = files[0].DirectoryName + "\\" + fileName.Substring(0, 4) + "V";
                    string notUniqueDocID = fileName.Substring(5);
                    fileImagePath += notUniqueDocID;
                    int pos = notUniqueDocID.IndexOf('.');
                    notUniqueDocID = notUniqueDocID.Substring(0, pos);
                    int maxBatchNo = int.Parse(notUniqueDocID.Substring(notUniqueDocID.Length - 6));

                    using (StreamWriter fsImage = File.CreateText(fileImagePath))
                    {
                        logger.Debug("Created image file: {0}", fileImagePath);

                        // Image header row
                        string imageHeader = "00|SYN|" + DateTime.Now.Year + String.Format("{0:MM}", DateTime.Now) +
                                             String.Format("{0:dd}", DateTime.Now) + " " + String.Format("{0:hh}", DateTime.Now) +
                                             ":" + String.Format("{0:mm}", DateTime.Now) + ":" +
                                             String.Format("{0:ss}", DateTime.Now) + "\n";
                        fsImage.Write(imageHeader);

                        // Get all the images
                        files = diClose.GetFiles("*.jpg");
                        foreach (FileInfo fi in files)
                        {
                            // Number plate picture
                            if (fi.Name.EndsWith("_0.jpg"))
                            {
                                string imgVLine = "01|";
                                imgVLine += notUniqueDocID + "|";
                                imgVLine += fi.Name + "|";
                                //md5 hash
                                imgVLine += hashing.CreateFileHash(fi.FullName);
                                imgVLine += "\n";

                                fsImage.Write(imgVLine);
                                logger.Debug("Writing vehicle image line: {0}", imgVLine.Trim());

                                numOfLines++;
                            }
                            // Vehicle picture
                            else if (fi.Name.EndsWith("_1.jpg"))
                            {
                                string imgNLine = "01|";
                                imgNLine += notUniqueDocID + "|";
                                imgNLine += fi.Name + "|";
                                //md5 hash
                                imgNLine += hashing.CreateFileHash(fi.FullName);
                                imgNLine += "\n";

                                fsImage.Write(imgNLine);
                                logger.Debug("Writing number plate image: {0}", imgNLine.Trim());

                                numOfLines++;
                                logger.Debug("Image  Count: {0}", numOfLines);

                                pos = fi.Name.IndexOf('.');
                                string imgFileName = fi.Name.Substring(0, pos - 2);

                                // Update the max batch number
                                this.iData.PFB_UpdateMaxBatchNo(imgFileName, maxBatchNo);
                            }
                        }

                        // Footer row
                        fsImage.Write("99|" + numOfLines);

                        //BD attempting to make sure that we have no connections hanging onto the file system
                        files = null;

                        // Close the stream
                        fsImage.Flush();
                        fsImage.Close();
                        fsImage.Dispose();

                        logger.Debug("Closed image file.");
                    }

                    //TODO: Some sort of cleanup needs to be done here if this is an empty batch
                    if (numOfLines == 0)
                        return;

                    // Need to rename the file to completed
                    Directory.Move(batchLocation + diClose.Name, batchLocation + diClose.Name + "_Completed");
                    logger.Debug("Changed folder name ({0}Completed) to completed.", diClose.Name);

                    //Barry Dickson - moved email send function to after the close so that we can be 100% sure everything has happened correctly.
                    //NotificationEmail notificationEmail = new NotificationEmail("AARTO-03", fileNoticePath, recordCounter);
                    int noInfringements = numOfLines / 2;
                    int iRe = 0;
                    try
                    {
                        NotificationEmail notificationEmail = new NotificationEmail("AARTO-03", fileName, noInfringements);
                        iRe = notificationEmail.Send();
                    }
                    catch (Exception ex)
                    {
                        logger.Debug(ex.Message);
                        logger.Debug("Error sending email.");
                    }

                    string strEmail = "";
                    if (iRe > 0)
                        strEmail = "... Send notification email completed.";
                    else if (iRe == 0)
                        strEmail = "... Send notification email failed. No recipient.";
                    else
                        strEmail = "... Send notification email failed.";

                    logger.Debug(strEmail);

                    if (userActioned == 0)
                    {
                        //the system determined that the batch was full and auto closed it.
                        MessageBox.Show("Batch (" + diClose.Name + ") created and completed successfully." + Environment.NewLine + strEmail, "Create and Close Batch", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        logger.Debug("... Create and Close batch process complete");
                    }
                    else
                    {
                        MessageBox.Show("Batch (" + diClose.Name + ") completed successfully." + Environment.NewLine + strEmail, "Close Batch", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        logger.Debug("... Close batch process complete");
                    }

                }
            }
            diCheck = null;
        }

    }
}