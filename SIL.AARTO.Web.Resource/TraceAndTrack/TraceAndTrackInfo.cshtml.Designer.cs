﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.296
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SIL.AARTO.Web.Resource.TraceAndTrack {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class TraceAndTrackInfo_cshtml {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal TraceAndTrackInfo_cshtml() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SIL.AARTO.Web.Resource.TraceAndTrack.TraceAndTrackInfo.cshtml", typeof(TraceAndTrackInfo_cshtml).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Copy Street Address to Postal Address.
        /// </summary>
        public static string cbCopyAddress_Text {
            get {
                return ResourceManager.GetString("cbCopyAddress_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company Name&apos;s Length  must be within 100.
        /// </summary>
        public static string CompanyName_Is_Length {
            get {
                return ResourceManager.GetString("CompanyName_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email Address&apos;s Length must be within 100.
        /// </summary>
        public static string EmailAddress_Is_Length {
            get {
                return ResourceManager.GetString("EmailAddress_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ForeName&apos;s  Length must be within 100.
        /// </summary>
        public static string ForeNames_Is_Length {
            get {
                return ResourceManager.GetString("ForeNames_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to  Already exists.
        /// </summary>
        public static string iblRegNoExits_Text {
            get {
                return ResourceManager.GetString("iblRegNoExits_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Identity Number&apos;s Length must be within 25.
        /// </summary>
        public static string IdentityNumber_Is_Length {
            get {
                return ResourceManager.GetString("IdentityNumber_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Identity Number Is Required.
        /// </summary>
        public static string IdentityNumber_Is_Required {
            get {
                return ResourceManager.GetString("IdentityNumber_Is_Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Initials&apos;s Length must be within 10.
        /// </summary>
        public static string Initials_Is_Length {
            get {
                return ResourceManager.GetString("Initials_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Initials Is Required.
        /// </summary>
        public static string Initials_Is_Required {
            get {
                return ResourceManager.GetString("Initials_Is_Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company Name.
        /// </summary>
        public static string lblCompanyName_Text {
            get {
                return ResourceManager.GetString("lblCompanyName_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email Address.
        /// </summary>
        public static string lblEmailAddress_Text {
            get {
                return ResourceManager.GetString("lblEmailAddress_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Forenames.
        /// </summary>
        public static string lblForeNames_Text {
            get {
                return ResourceManager.GetString("lblForeNames_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Identity Number.
        /// </summary>
        public static string lblIdentityNumber_Text {
            get {
                return ResourceManager.GetString("lblIdentityNumber_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Initials.
        /// </summary>
        public static string lblInitials_Text {
            get {
                return ResourceManager.GetString("lblInitials_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last User.
        /// </summary>
        public static string lblLastUser_Text {
            get {
                return ResourceManager.GetString("lblLastUser_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mobile Number.
        /// </summary>
        public static string lblMobileNumber_Text {
            get {
                return ResourceManager.GetString("lblMobileNumber_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Surname of Owner/Responsible person.
        /// </summary>
        public static string lblOwnerResponsiblePerson_Text {
            get {
                return ResourceManager.GetString("lblOwnerResponsiblePerson_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Track and Trace.
        /// </summary>
        public static string lblPageTitle_Text {
            get {
                return ResourceManager.GetString("lblPageTitle_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registration Number.
        /// </summary>
        public static string lblRegistrationNumber_Text {
            get {
                return ResourceManager.GetString("lblRegistrationNumber_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        public static string lblTblHeadEditName {
            get {
                return ResourceManager.GetString("lblTblHeadEditName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Telephone Number.
        /// </summary>
        public static string lblTelephoneNumber_Text {
            get {
                return ResourceManager.GetString("lblTelephoneNumber_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postal Address Line 1.
        /// </summary>
        public static string lblTTPoAdd1_Text {
            get {
                return ResourceManager.GetString("lblTTPoAdd1_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postal Address Line 2.
        /// </summary>
        public static string lblTTPoAdd2_Text {
            get {
                return ResourceManager.GetString("lblTTPoAdd2_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postal Address Line 3.
        /// </summary>
        public static string lblTTPoAdd3_Text {
            get {
                return ResourceManager.GetString("lblTTPoAdd3_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postal Address Line 4.
        /// </summary>
        public static string lblTTPoAdd4_Text {
            get {
                return ResourceManager.GetString("lblTTPoAdd4_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postal Address Postal code.
        /// </summary>
        public static string lblTTPoCode_Text {
            get {
                return ResourceManager.GetString("lblTTPoCode_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street  Address Line 1.
        /// </summary>
        public static string lblTTStAdd1_Text {
            get {
                return ResourceManager.GetString("lblTTStAdd1_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street  Address Line 2.
        /// </summary>
        public static string lblTTStAdd2_Text {
            get {
                return ResourceManager.GetString("lblTTStAdd2_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street  Address Line 3.
        /// </summary>
        public static string lblTTStAdd3_Text {
            get {
                return ResourceManager.GetString("lblTTStAdd3_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street  Address Line 4.
        /// </summary>
        public static string lblTTStAdd4_Text {
            get {
                return ResourceManager.GetString("lblTTStAdd4_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street Address  Postal code.
        /// </summary>
        public static string lblTTStCode_Text {
            get {
                return ResourceManager.GetString("lblTTStCode_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type of Owner.
        /// </summary>
        public static string lblTypeofOwner_Text {
            get {
                return ResourceManager.GetString("lblTypeofOwner_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mobile Number&apos;s Length must be within 20.
        /// </summary>
        public static string MobileNumber_Is_Length {
            get {
                return ResourceManager.GetString("MobileNumber_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Owner Responsible Person&apos;s Length must be within 50.
        /// </summary>
        public static string OwnerResponsiblePerson_Is_Length {
            get {
                return ResourceManager.GetString("OwnerResponsiblePerson_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Surname of Owner Responsible Person Is Required.
        /// </summary>
        public static string OwnerResponsiblePerson_Is_Required {
            get {
                return ResourceManager.GetString("OwnerResponsiblePerson_Is_Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registration Number&apos;s Length must be within 10.
        /// </summary>
        public static string RegistrationNumber_Is_Length {
            get {
                return ResourceManager.GetString("RegistrationNumber_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registration Number is required.
        /// </summary>
        public static string RegistrationNumber_Is_Required {
            get {
                return ResourceManager.GetString("RegistrationNumber_Is_Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Telephone Number&apos;s Length must be within 20.
        /// </summary>
        public static string TelephoneNumber_Is_Length {
            get {
                return ResourceManager.GetString("TelephoneNumber_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postal Address Line 1&apos;s Length must be within 100.
        /// </summary>
        public static string TTPoAdd1_Is_Length {
            get {
                return ResourceManager.GetString("TTPoAdd1_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postal Address line 1 is required.
        /// </summary>
        public static string TTPoAdd1_Is_Required {
            get {
                return ResourceManager.GetString("TTPoAdd1_Is_Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postal Address Line 2&apos;s Length must be within 100.
        /// </summary>
        public static string TTPoAdd2_Is_Length {
            get {
                return ResourceManager.GetString("TTPoAdd2_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postal Address line 2 is required.
        /// </summary>
        public static string TTPoAdd2_Is_Required {
            get {
                return ResourceManager.GetString("TTPoAdd2_Is_Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postal Address Line 3&apos;s Length must be within 100.
        /// </summary>
        public static string TTPoAdd3_Is_Length {
            get {
                return ResourceManager.GetString("TTPoAdd3_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postal Address Line 4&apos;s Length must be within 100.
        /// </summary>
        public static string TTPoAdd4_Is_Length {
            get {
                return ResourceManager.GetString("TTPoAdd4_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postal Address Postal code&apos;s Length must be within 10.
        /// </summary>
        public static string TTPoCode_Is_Length {
            get {
                return ResourceManager.GetString("TTPoCode_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postal Address code is required.
        /// </summary>
        public static string TTPoCode_Is_Required {
            get {
                return ResourceManager.GetString("TTPoCode_Is_Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street  Address Line 1&apos;s Length must be within 100.
        /// </summary>
        public static string TTStAdd1_Is_Length {
            get {
                return ResourceManager.GetString("TTStAdd1_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street Address line 1 is required.
        /// </summary>
        public static string TTStAdd1_Is_Required {
            get {
                return ResourceManager.GetString("TTStAdd1_Is_Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street  Address Line 2&apos;s Length must be within 100.
        /// </summary>
        public static string TTStAdd2_Is_Length {
            get {
                return ResourceManager.GetString("TTStAdd2_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street Address line 2 is required.
        /// </summary>
        public static string TTStAdd2_Is_Required {
            get {
                return ResourceManager.GetString("TTStAdd2_Is_Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street  Address Line 3&apos;s Length must be within 100.
        /// </summary>
        public static string TTStAdd3_Is_Length {
            get {
                return ResourceManager.GetString("TTStAdd3_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street  Address Line 4&apos;s Length must be within 100.
        /// </summary>
        public static string TTStAdd4_Is_Length {
            get {
                return ResourceManager.GetString("TTStAdd4_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street Address  Postal code&apos;s Length must be within 10.
        /// </summary>
        public static string TTStCode_Is_Length {
            get {
                return ResourceManager.GetString("TTStCode_Is_Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street Address code is required.
        /// </summary>
        public static string TTStCode_Is_Required {
            get {
                return ResourceManager.GetString("TTStCode_Is_Required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Must choose an item.
        /// </summary>
        public static string TypeofOwner_Is_Required {
            get {
                return ResourceManager.GetString("TypeofOwner_Is_Required", resourceCulture);
            }
        }
    }
}
