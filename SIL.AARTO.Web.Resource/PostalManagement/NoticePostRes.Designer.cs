﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SIL.AARTO.Web.Resource.PostalManagement {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class NoticePostRes {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal NoticePostRes() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SIL.AARTO.Web.Resource.PostalManagement.NoticePostRes", typeof(NoticePostRes).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to process these batch files?.
        /// </summary>
        public static string BatchProcessingConfirmMessage {
            get {
                return ResourceManager.GetString("BatchProcessingConfirmMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to use the date you select?.
        /// </summary>
        public static string ConfirmMessage {
            get {
                return ResourceManager.GetString("ConfirmMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Document withdrawn - not posted within 30 days ; {0}.
        /// </summary>
        public static string EvidencePackDesc1 {
            get {
                return ResourceManager.GetString("EvidencePackDesc1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Document not posted - reverted due to error; Administrative action; {0}.
        /// </summary>
        public static string EvidencePackDesc2 {
            get {
                return ResourceManager.GetString("EvidencePackDesc2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to expire the file?.
        /// </summary>
        public static string ExpireFileConfirmMessage {
            get {
                return ResourceManager.GetString("ExpireFileConfirmMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please only use this page if you have FULL knowledge of the consequences!!!.
        /// </summary>
        public static string lblAccessTipMessage {
            get {
                return ResourceManager.GetString("lblAccessTipMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Actual Post Date.
        /// </summary>
        public static string lblActualPostDate {
            get {
                return ResourceManager.GetString("lblActualPostDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Batch Processing.
        /// </summary>
        public static string lblBatchProcessing {
            get {
                return ResourceManager.GetString("lblBatchProcessing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change Local Authority:.
        /// </summary>
        public static string lblChangeLocalAuthority {
            get {
                return ResourceManager.GetString("lblChangeLocalAuthority", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The printed date is not valid..
        /// </summary>
        public static string lblErrorText {
            get {
                return ResourceManager.GetString("lblErrorText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The actual posted date is not a valid date..
        /// </summary>
        public static string lblErrorText1 {
            get {
                return ResourceManager.GetString("lblErrorText1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can&apos;t find this notice offender&apos;s id number, notice ticket number is {0}.
        /// </summary>
        public static string lblErrorText10 {
            get {
                return ResourceManager.GetString("lblErrorText10", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Can&apos;t find this notice offender&apos;s detail, notice ticket number is {0}.
        /// </summary>
        public static string lblErrorText11 {
            get {
                return ResourceManager.GetString("lblErrorText11", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The  end date cannot be less than the  begin date !!.
        /// </summary>
        public static string lblErrorText12 {
            get {
                return ResourceManager.GetString("lblErrorText12", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Based on the above search criteria did not find the relevant data..
        /// </summary>
        public static string lblErrorText13 {
            get {
                return ResourceManager.GetString("lblErrorText13", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Based on the above search criteria did not find the relevant data or Some of the data has been processed ..
        /// </summary>
        public static string lblErrorText14 {
            get {
                return ResourceManager.GetString("lblErrorText14", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Posted date is set automatically by the system..
        /// </summary>
        public static string lblErrorText15 {
            get {
                return ResourceManager.GetString("lblErrorText15", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The actual posted date can&apos;t be a future date..
        /// </summary>
        public static string lblErrorText16 {
            get {
                return ResourceManager.GetString("lblErrorText16", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Posted date possibly wrong – please double check.
        /// </summary>
        public static string lblErrorText17 {
            get {
                return ResourceManager.GetString("lblErrorText17", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The actual posted date cannot be less than the printed date !!.
        /// </summary>
        public static string lblErrorText2 {
            get {
                return ResourceManager.GetString("lblErrorText2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You have successfully confirmed that the {1} notice print file &apos;{0}&apos; has been posted..
        /// </summary>
        public static string lblErrorText3 {
            get {
                return ResourceManager.GetString("lblErrorText3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to update the status of {1} notice print file &apos;{0}&apos; - {2}.
        /// </summary>
        public static string lblErrorText4 {
            get {
                return ResourceManager.GetString("lblErrorText4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update status of the {1} notice print file &apos;{0}&apos; failed..
        /// </summary>
        public static string lblErrorText5 {
            get {
                return ResourceManager.GetString("lblErrorText5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update status of the {1} notice print file &apos;{0}&apos; failed - Update notice failed..
        /// </summary>
        public static string lblErrorText6 {
            get {
                return ResourceManager.GetString("lblErrorText6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update status of the {1} notice print file &apos;{0}&apos; failed - Update Change failed..
        /// </summary>
        public static string lblErrorText7 {
            get {
                return ResourceManager.GetString("lblErrorText7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update status of the {1} notice print file &apos;{0}&apos; failed - Insert EvidencePack failed..
        /// </summary>
        public static string lblErrorText8 {
            get {
                return ResourceManager.GetString("lblErrorText8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There are no notices for post..
        /// </summary>
        public static string lblErrorText9 {
            get {
                return ResourceManager.GetString("lblErrorText9", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Document withdrawn - not posted within 30 days.
        /// </summary>
        public static string lblEvidencePackDesc1 {
            get {
                return ResourceManager.GetString("lblEvidencePackDesc1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Document not posted - reverted due to error; Administrative action.
        /// </summary>
        public static string lblEvidencePackDesc2 {
            get {
                return ResourceManager.GetString("lblEvidencePackDesc2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Expire Whole Print File.
        /// </summary>
        public static string lblExpireWholePrintFile {
            get {
                return ResourceManager.GetString("lblExpireWholePrintFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First Notice.
        /// </summary>
        public static string lblFirstNotice {
            get {
                return ResourceManager.GetString("lblFirstNotice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First Notice Posted.
        /// </summary>
        public static string lblFirstNoticePosted {
            get {
                return ResourceManager.GetString("lblFirstNoticePosted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Notice Type.
        /// </summary>
        public static string lblNoticeType {
            get {
                return ResourceManager.GetString("lblNoticeType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Number of Notices.
        /// </summary>
        public static string lblNumberofNotices {
            get {
                return ResourceManager.GetString("lblNumberofNotices", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Original Print Date.
        /// </summary>
        public static string lblOriginalPrintDate {
            get {
                return ResourceManager.GetString("lblOriginalPrintDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm Notice Posted.
        /// </summary>
        public static string lblPageName {
            get {
                return ResourceManager.GetString("lblPageName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Expire Whole Print File.
        /// </summary>
        public static string lblPageName_ExpireWholePrintfile {
            get {
                return ResourceManager.GetString("lblPageName_ExpireWholePrintfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Revert Expiry of Whole Print File.
        /// </summary>
        public static string lblPageName_RevertWholePrintfile {
            get {
                return ResourceManager.GetString("lblPageName_RevertWholePrintfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select an Authority.
        /// </summary>
        public static string lblPleaseSelectAuthority {
            get {
                return ResourceManager.GetString("lblPleaseSelectAuthority", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Print Date.
        /// </summary>
        public static string lblPrintDate {
            get {
                return ResourceManager.GetString("lblPrintDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Print File Name.
        /// </summary>
        public static string lblPrintFileName {
            get {
                return ResourceManager.GetString("lblPrintFileName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Revert Whole Print File.
        /// </summary>
        public static string lblRevertWholePrintfile {
            get {
                return ResourceManager.GetString("lblRevertWholePrintfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search.
        /// </summary>
        public static string lblSearch {
            get {
                return ResourceManager.GetString("lblSearch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Second Notice.
        /// </summary>
        public static string lblSecondNotice {
            get {
                return ResourceManager.GetString("lblSecondNotice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Postage Status.
        /// </summary>
        public static string lblUpdatePostageStatus {
            get {
                return ResourceManager.GetString("lblUpdatePostageStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Notice Print Date.
        /// </summary>
        public static string NoticePrintDate {
            get {
                return ResourceManager.GetString("NoticePrintDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Original Notice Print Date.
        /// </summary>
        public static string OriginalNoticePrintDate {
            get {
                return ResourceManager.GetString("OriginalNoticePrintDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select an Authority.
        /// </summary>
        public static string PleaseSelectAuthority {
            get {
                return ResourceManager.GetString("PleaseSelectAuthority", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Print File Name:.
        /// </summary>
        public static string PrintFileName {
            get {
                return ResourceManager.GetString("PrintFileName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Print File Name Prefix.
        /// </summary>
        public static string PrintFileNamePrefix {
            get {
                return ResourceManager.GetString("PrintFileNamePrefix", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to revert the file?.
        /// </summary>
        public static string RevertFileConfirmMessage {
            get {
                return ResourceManager.GetString("RevertFileConfirmMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Postage Status.
        /// </summary>
        public static string UpdatePostageStatus {
            get {
                return ResourceManager.GetString("UpdatePostageStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Succeed!.
        /// </summary>
        public static string UpdateSucceed {
            get {
                return ResourceManager.GetString("UpdateSucceed", resourceCulture);
            }
        }
    }
}
