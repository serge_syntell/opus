﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.239
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SIL.AARTO.Web.Resource.CameraManagement {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AdjudicateFrame {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AdjudicateFrame() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SIL.AARTO.Web.Resource.CameraManagement.AdjudicateFrame", typeof(AdjudicateFrame).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Don&apos;t Prosecute.
        /// </summary>
        public static string btnAccept_Text1 {
            get {
                return ResourceManager.GetString("btnAccept_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Prosecute.
        /// </summary>
        public static string btnAccept_Text2 {
            get {
                return ResourceManager.GetString("btnAccept_Text2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change.
        /// </summary>
        public static string btnChange_Text {
            get {
                return ResourceManager.GetString("btnChange_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CLOSE.
        /// </summary>
        public static string btnClose_Text {
            get {
                return ResourceManager.GetString("btnClose_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Resend NATIS.
        /// </summary>
        public static string btnResendNATIS_Text {
            get {
                return ResourceManager.GetString("btnResendNATIS_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save Image Setting.
        /// </summary>
        public static string btnSaveSettingsForImage_Text {
            get {
                return ResourceManager.GetString("btnSaveSettingsForImage_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BUS LANE OFFENCE.
        /// </summary>
        public static string BusLane_Text {
            get {
                return ResourceManager.GetString("BusLane_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Film no:.
        /// </summary>
        public static string FilmNo_Text {
            get {
                return ResourceManager.GetString("FilmNo_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Frame no / Seq:.
        /// </summary>
        public static string FrameNoSeq_Text {
            get {
                return ResourceManager.GetString("FrameNoSeq_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GPS Date &amp; Time:.
        /// </summary>
        public static string GpsDateTime_Text {
            get {
                return ResourceManager.GetString("GpsDateTime_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Investigation Reason:.
        /// </summary>
        public static string IvestigateReason {
            get {
                return ResourceManager.GetString("IvestigateReason", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save Image Settings for Frames on Film.
        /// </summary>
        public static string labelForSISet_Text {
            get {
                return ResourceManager.GetString("labelForSISet_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Expired. The frame offence date is too old to continue.
        /// </summary>
        public static string labRejectionReason_Text1 {
            get {
                return ResourceManager.GetString("labRejectionReason_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lane Number:.
        /// </summary>
        public static string LaneNo_Text {
            get {
                return ResourceManager.GetString("LaneNo_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Image contrast and/or brightness settings saved.
        /// </summary>
        public static string lblError_Text1 {
            get {
                return ResourceManager.GetString("lblError_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Image contrast and/or brightness settings failed.
        /// </summary>
        public static string lblError_Text2 {
            get {
                return ResourceManager.GetString("lblError_Text2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registration may not be zeroes if it has been accepted.
        /// </summary>
        public static string lblMessage_Text1 {
            get {
                return ResourceManager.GetString("lblMessage_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registration may not be blank.
        /// </summary>
        public static string lblMessage_Text2 {
            get {
                return ResourceManager.GetString("lblMessage_Text2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Adjudication.
        /// </summary>
        public static string lblPageName_Text {
            get {
                return ResourceManager.GetString("lblPageName_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registration Confirmed?.
        /// </summary>
        public static string lblRegistration_Text {
            get {
                return ResourceManager.GetString("lblRegistration_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Next &gt;&gt;.
        /// </summary>
        public static string linkNext_Text {
            get {
                return ResourceManager.GetString("linkNext_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;&lt; Previous.
        /// </summary>
        public static string linkPre_Text {
            get {
                return ResourceManager.GetString("linkPre_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Location:.
        /// </summary>
        public static string Location_Text {
            get {
                return ResourceManager.GetString("Location_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NATIS.
        /// </summary>
        public static string Natis_Text {
            get {
                return ResourceManager.GetString("Natis_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Officer:.
        /// </summary>
        public static string Officer_Text {
            get {
                return ResourceManager.GetString("Officer_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Offence time and date:.
        /// </summary>
        public static string OffienceDate_Text {
            get {
                return ResourceManager.GetString("OffienceDate_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AVERAGE SPEED OVER DISTANCE.
        /// </summary>
        public static string OverSpeed_Text {
            get {
                return ResourceManager.GetString("OverSpeed_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reason for rejection:.
        /// </summary>
        public static string ReasonRej_Text {
            get {
                return ResourceManager.GetString("ReasonRej_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registration no:.
        /// </summary>
        public static string RegistrationNo_Text {
            get {
                return ResourceManager.GetString("RegistrationNo_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Section End:.
        /// </summary>
        public static string SectionEnd_Text {
            get {
                return ResourceManager.GetString("SectionEnd_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Section Start:.
        /// </summary>
        public static string SectionStart_Text {
            get {
                return ResourceManager.GetString("SectionStart_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Speed:.
        /// </summary>
        public static string Speed_Text {
            get {
                return ResourceManager.GetString("Speed_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Speed.
        /// </summary>
        public static string SpeedOnly_Text {
            get {
                return ResourceManager.GetString("SpeedOnly_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Verification Bin Comment:.
        /// </summary>
        public static string VeBiComment_Txt {
            get {
                return ResourceManager.GetString("VeBiComment_Txt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vehicle make:.
        /// </summary>
        public static string VehicleMake_Text {
            get {
                return ResourceManager.GetString("VehicleMake_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vehicle type:.
        /// </summary>
        public static string VehicleType_Text {
            get {
                return ResourceManager.GetString("VehicleType_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zone:.
        /// </summary>
        public static string Zone_Text {
            get {
                return ResourceManager.GetString("Zone_Text", resourceCulture);
            }
        }
    }
}
