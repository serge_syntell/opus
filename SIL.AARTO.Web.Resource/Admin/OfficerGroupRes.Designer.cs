﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.296
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SIL.AARTO.Web.Resource.Admin {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class OfficerGroupRes {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal OfficerGroupRes() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SIL.AARTO.Web.Resource.Admin.OfficerGroupRes", typeof(OfficerGroupRes).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add.
        /// </summary>
        public static string Add {
            get {
                return ResourceManager.GetString("Add", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Traffic officer group has been added.
        /// </summary>
        public static string AddedGroup {
            get {
                return ResourceManager.GetString("AddedGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to add this traffic officer group!.
        /// </summary>
        public static string AddGroupError {
            get {
                return ResourceManager.GetString("AddGroupError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add traffic officer group.
        /// </summary>
        public static string btnAddOfficerGroup_Text {
            get {
                return ResourceManager.GetString("btnAddOfficerGroup_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search.
        /// </summary>
        public static string btnSearch_Text {
            get {
                return ResourceManager.GetString("btnSearch_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string Cancel {
            get {
                return ResourceManager.GetString("Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        public static string Delete {
            get {
                return ResourceManager.GetString("Delete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to delete this row?.
        /// </summary>
        public static string DeleteOrNot {
            get {
                return ResourceManager.GetString("DeleteOrNot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        public static string Edit {
            get {
                return ResourceManager.GetString("Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This Group is in used by traffic officer so can not deleted or some else error!.
        /// </summary>
        public static string FailDelete {
            get {
                return ResourceManager.GetString("FailDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add new traffic officer group:.
        /// </summary>
        public static string lblAddOfficerGroup_Text {
            get {
                return ResourceManager.GetString("lblAddOfficerGroup_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter Partial Traffic Officer Group Description:.
        /// </summary>
        public static string lblSearchOfGrdescr_Text {
            get {
                return ResourceManager.GetString("lblSearchOfGrdescr_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update selected traffic officer group:.
        /// </summary>
        public static string lblUpdOfficerGroup_Text {
            get {
                return ResourceManager.GetString("lblUpdOfficerGroup_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The Traffic Officer Group Code is too long than 25..
        /// </summary>
        public static string LengthOfGrCode_ErrorMessage {
            get {
                return ResourceManager.GetString("LengthOfGrCode_ErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Traffic officer group maintenance.
        /// </summary>
        public static string PageName {
            get {
                return ResourceManager.GetString("PageName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There are no traffic officer group matching your search.
        /// </summary>
        public static string SearchNotFound {
            get {
                return ResourceManager.GetString("SearchNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This row has been deleted successfully!.
        /// </summary>
        public static string SuccessDelete {
            get {
                return ResourceManager.GetString("SuccessDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Traffic Officer Group Code.
        /// </summary>
        public static string TraOffGroCode {
            get {
                return ResourceManager.GetString("TraOffGroCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Traffic Officer Group Description.
        /// </summary>
        public static string TraOffGroDescr {
            get {
                return ResourceManager.GetString("TraOffGroDescr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Traffic officer group has been updated.
        /// </summary>
        public static string Updated {
            get {
                return ResourceManager.GetString("Updated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update traffic officer group.
        /// </summary>
        public static string UpdateGroup {
            get {
                return ResourceManager.GetString("UpdateGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to update this traffic officer group!.
        /// </summary>
        public static string UpdateGroupError {
            get {
                return ResourceManager.GetString("UpdateGroupError", resourceCulture);
            }
        }
    }
}
