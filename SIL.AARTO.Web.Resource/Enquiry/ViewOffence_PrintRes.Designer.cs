﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.296
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SIL.AARTO.Web.Resource.Enquiry {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ViewOffence_PrintRes {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ViewOffence_PrintRes() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SIL.AARTO.Web.Resource.Enquiry.ViewOffence_PrintRes", typeof(ViewOffence_PrintRes).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Close.
        /// </summary>
        public static string hlBackText {
            get {
                return ResourceManager.GetString("hlBackText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Print.
        /// </summary>
        public static string hlPrintText {
            get {
                return ResourceManager.GetString("hlPrintText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First Photo.
        /// </summary>
        public static string imageATitleText {
            get {
                return ResourceManager.GetString("imageATitleText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Second Photo.
        /// </summary>
        public static string imageBTitleText {
            get {
                return ResourceManager.GetString("imageBTitleText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Driver details:.
        /// </summary>
        public static string lblDriverdetailsText {
            get {
                return ResourceManager.GetString("lblDriverdetailsText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Film no. ~ Frame no:.
        /// </summary>
        public static string lblFilmFrameNOText {
            get {
                return ResourceManager.GetString("lblFilmFrameNOText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fine Amount:.
        /// </summary>
        public static string lblFineAmountText {
            get {
                return ResourceManager.GetString("lblFineAmountText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ID:.
        /// </summary>
        public static string lblIDText {
            get {
                return ResourceManager.GetString("lblIDText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Location:.
        /// </summary>
        public static string lblLocationText {
            get {
                return ResourceManager.GetString("lblLocationText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name:.
        /// </summary>
        public static string lblNameText {
            get {
                return ResourceManager.GetString("lblNameText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Offence date and time:.
        /// </summary>
        public static string lblOffenceDateText {
            get {
                return ResourceManager.GetString("lblOffenceDateText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Offence description:.
        /// </summary>
        public static string lblOffencedescrText {
            get {
                return ResourceManager.GetString("lblOffencedescrText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Owner details:.
        /// </summary>
        public static string lblOwnerdetailsText {
            get {
                return ResourceManager.GetString("lblOwnerdetailsText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Offence details.
        /// </summary>
        public static string lblPageNameText {
            get {
                return ResourceManager.GetString("lblPageNameText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postal address:.
        /// </summary>
        public static string lblPostaladdressText {
            get {
                return ResourceManager.GetString("lblPostaladdressText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proxy details:.
        /// </summary>
        public static string lblProxydetailsText {
            get {
                return ResourceManager.GetString("lblProxydetailsText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registration no:.
        /// </summary>
        public static string lblRegistrationnoText {
            get {
                return ResourceManager.GetString("lblRegistrationnoText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Speed (km/h):.
        /// </summary>
        public static string lblSpeedText {
            get {
                return ResourceManager.GetString("lblSpeedText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street address:.
        /// </summary>
        public static string lblStreetaddressText {
            get {
                return ResourceManager.GetString("lblStreetaddressText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ticket No:.
        /// </summary>
        public static string lblTicketNoText {
            get {
                return ResourceManager.GetString("lblTicketNoText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No notice has been supplied!.
        /// </summary>
        public static string WriteMsg {
            get {
                return ResourceManager.GetString("WriteMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No authority has been supplied!.
        /// </summary>
        public static string WriteMsg1 {
            get {
                return ResourceManager.GetString("WriteMsg1", resourceCulture);
            }
        }
    }
}
