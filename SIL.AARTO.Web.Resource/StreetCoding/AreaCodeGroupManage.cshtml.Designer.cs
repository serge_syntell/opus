﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.296
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SIL.AARTO.Web.Resource.StreetCoding {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AreaCodeGroupManage_cshtml {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AreaCodeGroupManage_cshtml() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SIL.AARTO.Web.Resource.StreetCoding.AreaCodeGroupManage.cshtml", typeof(AreaCodeGroupManage_cshtml).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Action.
        /// </summary>
        public static string Action {
            get {
                return ResourceManager.GetString("Action", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add.
        /// </summary>
        public static string Add {
            get {
                return ResourceManager.GetString("Add", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Area Code Group Maintenance.
        /// </summary>
        public static string AreaCodeGroupMaintenance {
            get {
                return ResourceManager.GetString("AreaCodeGroupMaintenance", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string Cancel {
            get {
                return ResourceManager.GetString("Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Code End.
        /// </summary>
        public static string CodeEnd {
            get {
                return ResourceManager.GetString("CodeEnd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Code Start.
        /// </summary>
        public static string CodeStart {
            get {
                return ResourceManager.GetString("CodeStart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Continue to delete?.
        /// </summary>
        public static string ContinueToDelete {
            get {
                return ResourceManager.GetString("ContinueToDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create New.
        /// </summary>
        public static string CreateNew {
            get {
                return ResourceManager.GetString("CreateNew", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create successful!.
        /// </summary>
        public static string CreateSuccessful {
            get {
                return ResourceManager.GetString("CreateSuccessful", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        public static string Delete {
            get {
                return ResourceManager.GetString("Delete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete successful!.
        /// </summary>
        public static string DeleteSuccessful {
            get {
                return ResourceManager.GetString("DeleteSuccessful", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        public static string Edit {
            get {
                return ResourceManager.GetString("Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit successful!.
        /// </summary>
        public static string EditSuccessful {
            get {
                return ResourceManager.GetString("EditSuccessful", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Group Name.
        /// </summary>
        public static string GroupName {
            get {
                return ResourceManager.GetString("GroupName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter code end..
        /// </summary>
        public static string PleaseEnterCodeEnd {
            get {
                return ResourceManager.GetString("PleaseEnterCodeEnd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter code start..
        /// </summary>
        public static string PleaseEnterCodeStart {
            get {
                return ResourceManager.GetString("PleaseEnterCodeStart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter group name..
        /// </summary>
        public static string PleaseEnterGroupName {
            get {
                return ResourceManager.GetString("PleaseEnterGroupName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter valid area code..
        /// </summary>
        public static string PleaseEnterValidAreaCode {
            get {
                return ResourceManager.GetString("PleaseEnterValidAreaCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        public static string Save {
            get {
                return ResourceManager.GetString("Save", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The current record has been modified by someone else, please refresh..
        /// </summary>
        public static string TheCurrentRecordHasBeenModifiedBySomeoneElse {
            get {
                return ResourceManager.GetString("TheCurrentRecordHasBeenModifiedBySomeoneElse", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This group with area codes is already existed..
        /// </summary>
        public static string ThisGroupWithAreaCodesIsAlreadyExisted {
            get {
                return ResourceManager.GetString("ThisGroupWithAreaCodesIsAlreadyExisted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update.
        /// </summary>
        public static string Update {
            get {
                return ResourceManager.GetString("Update", resourceCulture);
            }
        }
    }
}
