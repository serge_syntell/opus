﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Threading;
using System.Globalization;

namespace SIL.AARTO.IMX.ImageLoader
{
    /// <summary>
    /// Language class
    /// </summary>
    public class SetLanguage
    {
        
        /// <summary>
        /// Setting language for current form 
        /// </summary>
        /// <param name="lang">Language </param>
        /// <param name="form">Form</param>
        /// <param name="frmtype">Form Type</param>
        public static void SetLang(string lang, Form form, Type frmtype)
        {
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(lang);
            if (form != null)
            {
                ComponentResourceManager resources = new ComponentResourceManager(frmtype);
                resources.ApplyResources(form, "$this");
                AppLang(form, resources);
            }
        }

        public static string GetString(Type type,string key)
        {
            ComponentResourceManager resources = new ComponentResourceManager(type);
            return resources.GetString(key);
        }
        #region AppLang for Control
        /// <summary>
        ///  
        /// </summary>
        /// <param name="contrl"></param>
        /// <param name="resoureces"></param>
        private static void AppLang(Control control, ComponentResourceManager resources)
        {
            if (control is MenuStrip)
            {
                 
                resources.ApplyResources(control, control.Name);
                MenuStrip ms = (MenuStrip)control;
                if (ms.Items.Count > 0)
                {
                    foreach (ToolStripMenuItem c in ms.Items)
                    {
                         
                        AppLang(c, resources);
                    }
                }
            }

            foreach (Control c in control.Controls)
            {
                resources.ApplyResources(c, c.Name);
                AppLang(c, resources);
            }
        }
        #endregion

        #region AppLang for menuitem
        /// <summary>
        ///  
        /// </summary>
        /// <param name="item"></param>
        /// <param name="resources"></param>
        private static void AppLang(ToolStripMenuItem item, System.ComponentModel.ComponentResourceManager resources)
        {
            if (item is ToolStripMenuItem)
            {
                resources.ApplyResources(item, item.Name);
                ToolStripMenuItem tsmi = (ToolStripMenuItem)item;
                if (tsmi.DropDownItems.Count > 0)
                {
                    foreach (ToolStripMenuItem c in tsmi.DropDownItems)
                    {
                        //if (tsmi != ToolStripSeparator)
                        //{ }
                        AppLang(c, resources);
                    }
                }
            }
        }

     

        #endregion
    }
}

