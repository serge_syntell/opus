﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Globalization;
using SIL.AARTO.IMX.XMLLoader;
using System.Configuration;

namespace SIL.AARTO.IMX.ImageLoader
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }



        private void exportDateFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadImage loadImageForm = new LoadImage();
            loadImageForm.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void loadTheFilmsAndImagesFromDVDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.RootFolder = Environment.SpecialFolder.MyComputer;

            string strDVDFilePath = ConfigurationManager.AppSettings["DVDFilePath"];
            if(strDVDFilePath!= null)
                dialog.SelectedPath = strDVDFilePath;

            DialogResult result = dialog.ShowDialog(this);
            if(result == DialogResult.OK)
            {
                DialogResult msgResult = MessageBox.Show("Are you sure you want to load all the data from " + dialog.SelectedPath + "?", "Message", MessageBoxButtons.OKCancel);
                if(msgResult == DialogResult.OK)
                {
                    if(strDVDFilePath != dialog.SelectedPath)
                    {
                        Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                        config.AppSettings.Settings.Remove("DVDFilePath");
                        config.AppSettings.Settings.Add("DVDFilePath", dialog.SelectedPath);
                        config.Save(ConfigurationSaveMode.Modified);
                        ConfigurationManager.RefreshSection("appSettings");
                    }

                    int failCount = 0;
                    ViolationLoader violationLoader = new ViolationLoader(Program.APP_NAME);
                    int filmCount = violationLoader.LoadViolationsByDVD(dialog.SelectedPath, ref failCount);

                    if (filmCount > 0 && failCount == 0)
                        MessageBox.Show(string.Format("{0} films loaded successfully. Please check the log file for further details.", filmCount.ToString()));
                    else
                        MessageBox.Show(string.Format("The operation has completed with errors: {0} successful film(s); {1} failed film(s). Please check the log file for details.", filmCount.ToString(), failCount.ToString()));
                }
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        //private void englishEToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    // Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("zh-CN");
        //    SetLanguage.SetLang("en-US", this, typeof(MainForm));
        //}

        //private void chineseCToolStripMenuItem_Click(object sender, EventArgs e)
        //{

        //    SetLanguage.SetLang("zh-CN", this, typeof(MainForm));

        //}
    }
}
