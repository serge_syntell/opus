﻿using System;
using System.Windows.Forms;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;

namespace SIL.AARTO.IMX.ImageLoader
{
    public partial class UserLogin : Form
    {
        List<ListItem> lstLanguage = new List<ListItem>() { new ListItem( "English","en-US"), new ListItem("Chinese","zh-CN" ) };

        public UserLogin()
        {
            InitializeComponent();
            //this.Text = Program.APP_TITLE;
            this.cmbLanguage.DataSource = lstLanguage;
            if (cmbLanguage.Items.Count > 0) cmbLanguage.SelectedIndex = 0;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textUserName.Text.Trim()))
            {
                
                MessageBox.Show(SetLanguage.GetString(typeof(UserLogin), "UserNameIsRequired"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (string.IsNullOrEmpty(textPwd.Text.Trim()))
            {
                MessageBox.Show(SetLanguage.GetString(typeof(UserLogin), "PasswordIsRequired"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            LoginResult loginResult;
            GlobalVariates<User>.CurrentUser = Login(textUserName.Text.Trim(), textPwd.Text.Trim(), out loginResult);
            switch (loginResult)
            {
                case LoginResult.ErrorUserName:
                    MessageBox.Show(SetLanguage.GetString(typeof(UserLogin), "NameNotExists"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                case LoginResult.ErrorPassword:
                    MessageBox.Show(SetLanguage.GetString(typeof(UserLogin), "PassworsIsInvalid"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                case LoginResult.IsLocked:
                    MessageBox.Show(SetLanguage.GetString(typeof(UserLogin), "UserIsLocked"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                case LoginResult.Exception:
                    MessageBox.Show(SetLanguage.GetString(typeof(UserLogin), "CanNotConnectDataBaseServer"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                case LoginResult.Succeeded:
                    break;
            }
            string language = ((ListItem)(this.cmbLanguage.SelectedItem)).Value;
            if (String.IsNullOrEmpty(language)) language = "en-US";
            SetLanguage.SetLang(language, this, typeof(UserLogin));

            this.Hide();
            MainForm mainForm = new MainForm();
            mainForm.Show();
        }
        public enum LoginResult
        {
            Succeeded = 0, IsLocked = 1, ErrorUserName = 2, ErrorPassword = 3, Exception = 4
        }
        public User Login(string userName, string pwd, out LoginResult loginResult)
        {
            if (ValidateUser(userName, pwd))
            {
                loginResult = LoginResult.Succeeded;
                UserService userService = new UserService();
                User user = userService.GetByUserLoginName(userName);
                return user;
            }
            loginResult = LoginResult.ErrorPassword;
            return null;
        }
        public bool ValidateUser(string userName, string pwd)
        {
            User user = new UserService().GetByUserLoginName(userName);
            if (user == null)
            {
                return false;
            }
            string storedPwd = user.UserPassword;
            string inputPwd = Encrypt.HashPassword(pwd);
            if (storedPwd == inputPwd)
                return true;
            return false;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

       

        private void cmbLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            string language = ((ListItem)(this.cmbLanguage.SelectedItem)).Value;
            if (String.IsNullOrEmpty(language)) language = "en-US";
            SetLanguage.SetLang(language, this, typeof(UserLogin));

        }
    }
}
