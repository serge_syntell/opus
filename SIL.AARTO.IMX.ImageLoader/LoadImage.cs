﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Management;
using SIL.AARTO.BLL.Export;
using System.Configuration;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.IMX.ImageLoader
{
    public partial class LoadImage : Form
    {
        public LoadImage()
        {
            InitializeComponent();
        }

        private string sourceImageFolder = String.Empty;
        private static ExportDataManager exportDataManager = new ExportDataManager();

        private void LoadImage_Load(object sender, EventArgs e)
        {
            //UserControl control = new UcFolderSelect();
            //control.Dock = DockStyle.Top;
            //control.Height = this.panelFolder.Height;
            //this.panelFolder.Controls.Add(control);

            InitFilmDataGridView();

            int timeout = 0;

            Int32.TryParse(System.Configuration.ConfigurationManager.AppSettings["TimeOut"], out timeout);

            exportDataManager.TimeOutValue = 60 * 1000 * timeout;
        }

        private void btnLoadImages_Click(object sender, EventArgs e)
        {
            this.btnLoadImages.Enabled = false;

            if (exportDataManager.TimeOutValue <= 0)
            {
                MessageBox.Show(SetLanguage.GetString(typeof(LoadImage), "TimeOut.Text"));
                return;
            }

            if (MessageBox.Show(SetLanguage.GetString(typeof(LoadImage), "LoadImagesConfrimMessage"),
                SetLanguage.GetString(typeof(LoadImage), "ComfirmMessageTitle"), MessageBoxButtons.YesNo).Equals(DialogResult.Yes))
            {

                if (String.IsNullOrEmpty(sourceImageFolder))
                {
                    MessageBox.Show(SetLanguage.GetString(typeof(LoadImage), "SelectImageFolder"));
                    return;
                }
                try
                {
                    string errorMessage = String.Empty;

                    foreach (DataGridViewRow row in this.dgvFilms.SelectedRows)
                    {
                        if (row.Cells[0].Value != null)
                        {
                            if (exportDataManager.DoImport(sourceImageFolder, row.Cells[0].Value.ToString(), out errorMessage, 
                                string.Format("{0}_{1}", Program.APP_NAME, GlobalVariates<User>.CurrentUser.UserLoginName)
                                ) == false)
                            {
                                //MessageBox.Show(SetLanguage.GetString(typeof(LoadImage), "LoadImagesFailure"));
                                if (!String.IsNullOrEmpty(errorMessage))
                                {
                                    MessageBox.Show(errorMessage);
                                }
                            }
                            else
                            {
                                MessageBox.Show(SetLanguage.GetString(typeof(LoadImage), "LoadImagesSuccess"));
                            }
                        }
                    }

                    this.btnLoadImages.Enabled = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            if (this.cmbAuthority.SelectedItem != null)
            {
                int authIntNo = Convert.ToInt32(((ListItem)this.cmbAuthority.SelectedItem).Value);
                LoadWaittingForLoadImagesFilmsFromAarto(authIntNo);
            }

        }

        private void LoadWaittingForLoadImagesFilmsFromAarto(int authIntNo)
        {

            this.dgvFilms.DataSource = new ExportDataManager().GetWaittingForLoadImagesFilms(authIntNo);

            if (dgvFilms.Rows.Count > 0 && dgvFilms.Rows[0].Cells[0].Value != null)
            {
                button1.Enabled = true;
            }
            else
            {
                button1.Enabled = false;
            }
        }

        private void InitFilmDataGridView()
        {
            this.dgvFilms.AutoGenerateColumns = false;
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            //column.HeaderText = "Film No"; 
            column.HeaderText = SetLanguage.GetString(typeof(LoadImage), "dgvFilmNo.Text");
            column.DataPropertyName = "FilmNo";

            this.dgvFilms.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            //column.HeaderText = "CD Label";
            column.HeaderText = SetLanguage.GetString(typeof(LoadImage), "dgvCDlabel.Text");
            column.DataPropertyName = "CDlabel";
            this.dgvFilms.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            //column.HeaderText = "No of Frames";
            column.HeaderText = SetLanguage.GetString(typeof(LoadImage), "dgvNoOfFrames.Text");
            column.DataPropertyName = "NoOfFrames";
            this.dgvFilms.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            //column.HeaderText = "No of Scans";
            column.HeaderText = SetLanguage.GetString(typeof(LoadImage), "dgvNoOfScans.Text");
            column.DataPropertyName = "NoOfScans";
            this.dgvFilms.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            //column.HeaderText = "Interface";
            column.HeaderText = SetLanguage.GetString(typeof(LoadImage), "dgvInterface.Text");
            column.DataPropertyName = "LastUser";
            this.dgvFilms.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            //column.HeaderText = "Cameral Load Date";
            column.HeaderText = SetLanguage.GetString(typeof(LoadImage), "dgvCamLoadDate.Text");
            column.DataPropertyName = "CamLoadDate";
            this.dgvFilms.Columns.Add(column);

            ListItem item = new ListItem();

            item.Key = SetLanguage.GetString(typeof(LoadImage), "SelectAuthority");
            item.Value = "";

            this.cmbAuthority.Items.Add(item);

            foreach (SIL.AARTO.DAL.Entities.Authority authority in AuthorityManager.GetAllAuthoritiesFromAarto())
            {
                item = new ListItem();
                item.Key = authority.AutName;
                item.Value = authority.AutIntNo.ToString();

                this.cmbAuthority.Items.Add(item);
            }
            this.cmbAuthority.SelectedIndex = 0;
            this.progressBar.Visible = false;
        }



        private void button1_Click(object sender, EventArgs e)
        {
            //if (folderBrowserDialog.ShowDialog().Equals(DialogResult.OK))
            //{
            //    if (!String.IsNullOrEmpty(folderBrowserDialog.SelectedPath))
            //        sourceImageFolder = folderBrowserDialog.SelectedPath;
            //    this.btnLoadImages.Enabled = true;
            //}
            //else
            //{
            //    sourceImageFolder = String.Empty;
            //}

            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.RootFolder = Environment.SpecialFolder.MyComputer;

            string strDVDFilePath = ConfigurationManager.AppSettings["DVDFilePath"];
            if (strDVDFilePath != null)
                dialog.SelectedPath = strDVDFilePath;

            DialogResult result = dialog.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                if (!String.IsNullOrEmpty(dialog.SelectedPath))
                {
                    sourceImageFolder = dialog.SelectedPath;
                    this.btnLoadImages.Enabled = true;

                    if (strDVDFilePath != dialog.SelectedPath)
                    {
                        Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                        config.AppSettings.Settings.Remove("DVDFilePath");
                        config.AppSettings.Settings.Add("DVDFilePath", dialog.SelectedPath);
                        config.Save(ConfigurationSaveMode.Modified);
                        ConfigurationManager.RefreshSection("appSettings");
                    }
                }
                else
                {
                    sourceImageFolder = String.Empty;
                    this.btnLoadImages.Enabled = true;
                }
            }
        }

        private void cmbAuthority_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (!String.IsNullOrEmpty(((ListItem)this.cmbAuthority.SelectedItem).Value))
            {
                int authIntNo = Convert.ToInt32(((ListItem)this.cmbAuthority.SelectedItem).Value);
                LoadWaittingForLoadImagesFilmsFromAarto(authIntNo);
            }
        }
    }


}
