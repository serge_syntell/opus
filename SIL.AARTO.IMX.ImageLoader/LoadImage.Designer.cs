﻿namespace SIL.AARTO.IMX.ImageLoader
{
    partial class LoadImage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadImage));
            this.btnLoadImages = new System.Windows.Forms.Button();
            this.panelFolder = new System.Windows.Forms.Panel();
            this.lbAuthority = new System.Windows.Forms.Label();
            this.cmbAuthority = new System.Windows.Forms.ComboBox();
            this.dgvFilms = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.panelFolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilms)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLoadImages
            // 
            resources.ApplyResources(this.btnLoadImages, "btnLoadImages");
            this.btnLoadImages.Name = "btnLoadImages";
            this.btnLoadImages.UseVisualStyleBackColor = true;
            this.btnLoadImages.Click += new System.EventHandler(this.btnLoadImages_Click);
            // 
            // panelFolder
            // 
            this.panelFolder.Controls.Add(this.lbAuthority);
            this.panelFolder.Controls.Add(this.cmbAuthority);
            this.panelFolder.Controls.Add(this.dgvFilms);
            resources.ApplyResources(this.panelFolder, "panelFolder");
            this.panelFolder.Name = "panelFolder";
            // 
            // lbAuthority
            // 
            resources.ApplyResources(this.lbAuthority, "lbAuthority");
            this.lbAuthority.Name = "lbAuthority";
            // 
            // cmbAuthority
            // 
            this.cmbAuthority.FormattingEnabled = true;
            resources.ApplyResources(this.cmbAuthority, "cmbAuthority");
            this.cmbAuthority.Name = "cmbAuthority";
            this.cmbAuthority.SelectedIndexChanged += new System.EventHandler(this.cmbAuthority_SelectedIndexChanged);
            // 
            // dgvFilms
            // 
            resources.ApplyResources(this.dgvFilms, "dgvFilms");
            this.dgvFilms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFilms.Name = "dgvFilms";
            this.dgvFilms.ReadOnly = true;
            this.dgvFilms.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // folderBrowserDialog
            // 
            resources.ApplyResources(this.folderBrowserDialog, "folderBrowserDialog");
            // 
            // progressBar
            // 
            resources.ApplyResources(this.progressBar, "progressBar");
            this.progressBar.Name = "progressBar";
            // 
            // LoadImage
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panelFolder);
            this.Controls.Add(this.btnLoadImages);
            this.Name = "LoadImage";
            this.Load += new System.EventHandler(this.LoadImage_Load);
            this.panelFolder.ResumeLayout(false);
            this.panelFolder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilms)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLoadImages;
        private System.Windows.Forms.Panel panelFolder;
        private System.Windows.Forms.DataGridView dgvFilms;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.ComboBox cmbAuthority;
        private System.Windows.Forms.Label lbAuthority;
        private System.Windows.Forms.ProgressBar progressBar;
    }
}