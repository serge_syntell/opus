﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.IMX.DAL.Entities;
using SIL.IMX.DAL.Services;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Export
{
    public class AuthorityManager
    {
        public static List<SIL.IMX.DAL.Entities.Authority> GetAllAuthoritiesFromImx()
        {
            return new SIL.IMX.DAL.Services.AuthorityService().GetAll().ToList();
        }

        public static List<SIL.AARTO.DAL.Entities.Authority> GetAllAuthoritiesFromAarto()
        {
            return new SIL.AARTO.DAL.Services.AuthorityService().GetAll().ToList();
        }
    }
}
