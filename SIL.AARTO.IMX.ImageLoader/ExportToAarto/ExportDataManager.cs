﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.IMX.DAL.Entities;
using SIL.IMX.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.IMX.DAL.Data;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.BLL.DataImport;
using System.IO;
using System.Xml;

namespace SIL.AARTO.BLL.Export
{

    public class ExportDataManager
    {


        public void ExportWithCentralModel()
        {
            //Get Films from IMX
            List<SIL.IMX.DAL.Entities.Film> imxFilms = GetFilmsToExport();

            bool isError = false;
            foreach (SIL.IMX.DAL.Entities.Film imxFilm in imxFilms)
            {
                using (SIL.AARTO.DAL.Services.ConnectionScope.CreateTransaction())
                {
                    isError = false;
                    //Begin export Film
                    if (!CheckFilmInAarto(imxFilm.FilmNo))
                    {
                        // Film No does exists in AARTo
                    }
                    SIL.AARTO.DAL.Entities.Film aartoFilm = new SIL.AARTO.DAL.Entities.Film();
                    if (UpdateFilmToAarto(imxFilm, aartoFilm))
                    {
                        // Insert into Film Table in AARTO
                        aartoFilm = DataService.AartoFilmService.Save(aartoFilm);

                        if (aartoFilm == null)
                        {
                            // Inert failure
                            continue;
                        }

                        // Begin export Frames 
                        foreach (SIL.IMX.DAL.Entities.Frame imxFrame in GetFramesInIMXByFilmIntNo(imxFilm.FilmIntNo))
                        {
                            if (!CheckFrameInAarto(aartoFilm.FilmIntNo, imxFrame.FrameNo))
                            {
                                // Frame does exists in AARTO
                            }

                            SIL.AARTO.DAL.Entities.Frame aartoFrame = new SIL.AARTO.DAL.Entities.Frame();
                            if (UpdateFrameToAarto(imxFrame, aartoFrame, aartoFilm.FilmIntNo))
                            {
                                aartoFrame = DataService.AartoFrameService.Save(aartoFrame);
                                if (aartoFrame == null)
                                {
                                    //Insert failure
                                    isError = true;
                                    break;
                                }
                                //Begin export ScanImages in AARTO
                                foreach (SIL.IMX.DAL.Entities.ScanImage imxScanImage in GetScanImageInIMXByFrameIntNo(imxFrame.FrameIntNo))
                                {
                                    SIL.AARTO.DAL.Entities.ScanImage aartoScanImage = new SIL.AARTO.DAL.Entities.ScanImage();
                                    if (UpdateScanImageToAarto(imxScanImage, aartoScanImage, aartoFrame.FrameIntNo))
                                    {
                                        aartoScanImage = DataService.AartoScanImageService.Save(aartoScanImage);
                                        if (aartoScanImage == null)
                                        {
                                            //Insert failure
                                            isError = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // Assignment value from IMX Frame to AARTO Frame failure 
                            }

                            if (isError) break;
                        }

                        if (!isError)
                            SIL.AARTO.DAL.Services.ConnectionScope.Complete();

                    }
                    else
                    {
                        // Assignment value from IMX Film to AARTO Film failure 
                    }

                }
            }
        }

        public void ExportWithRemoteModel(string filmPath)
        {
            string imageLocalFolder = String.Empty;
            string imageRemoteFolder = String.Empty;
            string authCode = String.Empty;
            List<XmlFilmEntity> xmlFilmEntityList = new List<XmlFilmEntity>();
            foreach (string xmlFileName in Directory.GetFiles(filmPath))
            {
                FileInfo xmlFile = new FileInfo(xmlFileName);
                if (xmlFile.Extension.ToLower().Equals("xml"))
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(File.ReadAllText(xmlFileName, Encoding.UTF8));

                    XmlFilmEntity xmlFilmEntity = XmlLoadManager.GetFilmEntityFromXml(xmlDocument);
                    if (xmlFilmEntity != null)
                        xmlFilmEntityList.Add(xmlFilmEntity);
                }
            }

            foreach (XmlFilmEntity xmlFilmEntity in xmlFilmEntityList)
            {
                FilmEntity filmEntity = new FilmEntity();
                filmEntity = xmlFilmEntity.Film;

                foreach (FrameEntity frameEntity in xmlFilmEntity.Frames)
                {
                    imageLocalFolder = filmPath + "/" + frameEntity.FrameNo;
                    if (Directory.Exists(imageLocalFolder))
                    {
                        authCode = imageLocalFolder.Substring(0, imageLocalFolder.IndexOf(@"\"));
                        SIL.AARTO.DAL.Entities.Authority authority = GetAuthorityFromAarto(authCode);
                        if (authority == null)
                        {
                            // Authority does not exists in Aarto
                            continue;
                        }

                        imageRemoteFolder = string.Format(@"{0}\{1}\{2}\{3}\{4}\{5}",
                            authCode,
                            frameEntity.OffenceDate.Year,
                            frameEntity.OffenceDate.Month,
                            frameEntity.OffenceDate.Day,
                            filmEntity.FilmNo,
                            frameEntity.FrameNo);

                    }
                }
            }
        }

        public void DoImport(string filmPath, string filmNo)
        {
            string targetDirectory = String.Empty;
            string exportDirectory = String.Empty;
            string frameImagePath = String.Empty;
            string authCode = String.Empty;
            SIL.AARTO.DAL.Entities.Film film = DataService.AartoFilmService.GetByFilmNo(filmNo);

            List<SIL.AARTO.DAL.Entities.Authority> authorities = DataService.AartoAuthorityService.GetAll().ToList();
            int processedFrames = 0;
            int processedFramesLastTime = 0;
            if (film != null)
            {
                LoadViolations violation = new LoadViolations();
                authCode = DataService.AartoAuthorityService.GetByAutIntNo(film.AutIntNo).AutCode;
                List<SIL.AARTO.DAL.Entities.Frame> frames = DataService.AartoFrameService.GetByFilmIntNo(film.FilmIntNo).ToList();
                List<SIL.AARTO.DAL.Entities.ImageFileServer> imageServerList = DataService.AartoFileServerService.GetAll().ToList();
                SIL.AARTO.DAL.Entities.ImageFileServer imageFileServer = null;
                SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.ScanImage> scanImageList = new SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.ScanImage>();
                foreach (SIL.AARTO.DAL.Entities.Frame frame in frames)
                {

                    if (frame.FrameStatus.Equals((int)FrameStatusList.FrameLoadedViaImporterSuccessfully))
                    {
                        // IF frame status =1000 , Image loaded for this frame
                        processedFramesLastTime += 1;
                        continue;
                    }
                    targetDirectory = SearchFrameFromCD(filmPath, film.FilmNo, frame.FrameNo);
                    if (String.IsNullOrEmpty(targetDirectory))
                    {
                        // System can not find the image folder
                        EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", String.Format("Could not find the images folder for Film No :{0} FrameNo {1}", film.FilmNo, frame.FrameNo));
                        continue;
                    }
                    frameImagePath = GenerateFrameImagePath(film, frame, authCode);

                    if (Directory.Exists(targetDirectory))
                    {
                        imageFileServer = imageServerList.SingleOrDefault(i => i.ImageShareName.Equals("CameraImages") && i.IsDefault.Equals(true));
                        if (imageFileServer == null)
                        {
                            // Image File Server does not exists in Aarto;
                            EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", String.Format("Image file server does not exists in Aarto, ImageShareName : CameraImages"));
                            break;
                        }
                        exportDirectory = String.Format(@"\\{0}\{1}\{2}", imageFileServer.ImageMachineName, imageFileServer.ImageShareName, frameImagePath);
                        if (!violation.ProcessLocalFolders(targetDirectory, "", exportDirectory))
                        {

                            using (SIL.AARTO.DAL.Services.ConnectionScope.CreateTransaction())
                            {
                                frame.FrameStatus = (int)FrameStatusList.FrameLoadedViaImporterSuccessfully;
                                frame.IfsIntNo = imageFileServer.IfsIntNo;
                                frame.FrameImagePath = frameImagePath;
                                DataService.AartoFrameService.Save(frame);

                                processedFrames++;

                                foreach (FileInfo imageFile in new DirectoryInfo(targetDirectory).GetFiles())
                                {
                                    SIL.AARTO.DAL.Entities.ScanImage image = new SIL.AARTO.DAL.Entities.ScanImage();
                                    image.FrameIntNo = frame.FrameIntNo;
                                    image.JpegName = imageFile.Name;
                                    image.LastUser = "SIL.AARTO.IMX.ImageLoader";

                                    scanImageList.Add(image);
                                }

                                DataService.AartoScanImageService.Save(scanImageList);

                                if (film.NoOfFrames.Equals(processedFrames + processedFramesLastTime))
                                {
                                    film.FilmLoadStatus = (int)FilmLoadStatusList.FilmLoadedSuccessfully;
                                    DataService.AartoFilmService.Save(film);
                                }

                                SIL.AARTO.DAL.Services.ConnectionScope.Complete();
                            }
                        }

                    }
                    else
                    {
                        // Directory does not exists
                        EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", String.Format("Invalid directory :{0}", targetDirectory));
                    }
                }
            }
        }



        public List<SIL.AARTO.DAL.Entities.Film> GetWaittingForLoadImagesFilms(int authIntNo)
        {
            List<SIL.AARTO.DAL.Entities.Film> films = new List<SIL.AARTO.DAL.Entities.Film>();

            SIL.AARTO.DAL.Data.FilmQuery aartoFilmQuery = new SIL.AARTO.DAL.Data.FilmQuery();
            aartoFilmQuery.Append(SIL.AARTO.DAL.Entities.FilmColumn.FilmLoadStatus, ((int)FilmLoadStatusList.FilmCreatedViaImporter).ToString());
            aartoFilmQuery.Append(SIL.AARTO.DAL.Entities.FilmColumn.AutIntNo, authIntNo.ToString());
            try
            {
                return DataService.AartoFilmService.Find(aartoFilmQuery as SIL.AARTO.DAL.Data.IFilterParameterCollection).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string SearchFrameFromCD(string filmPath, string filmNo, string frameNo)
        {
            string framePath = String.Empty;
            if (Directory.Exists(filmPath))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(filmPath);
                framePath = SerachFrameFloderFromCD(frameNo, dirInfo);
            }

            return framePath;
        }

        private string SerachFrameFloderFromCD(string frameNo, DirectoryInfo dirInfo)
        {
            try
            {
                if (dirInfo.FullName.ToLower().Contains(frameNo.ToLower()))
                {
                    return dirInfo.FullName;
                }
                foreach (DirectoryInfo dir in dirInfo.GetDirectories())
                {
                    if (dir.FullName.ToLower().Contains(frameNo.ToLower()))
                    {
                        return dir.FullName;
                    }
                    else
                    {
                        if (dir.GetDirectories().Length > 0)
                        {
                            return SerachFrameFloderFromCD(frameNo, dir);
                        }
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GenerateFrameImagePath(SIL.AARTO.DAL.Entities.Film film, SIL.AARTO.DAL.Entities.Frame frame, string authCode)
        {
            string imageRemoteFolder = string.Format(@"{0}\{1}\{2}\{3}\{4}\{5}",
                            authCode,
                            frame.OffenceDate.Year,
                            frame.OffenceDate.Month,
                            frame.OffenceDate.Day,
                            film.FilmNo,
                            frame.FrameNo);

            return imageRemoteFolder;
        }

        private List<SIL.IMX.DAL.Entities.Film> GetFilmsToExport()
        {
            int totalCount = 0;
            int pageSize = 500;
            //FieldPackQuery query = new FieldPackQuery();
            //query.Append(FieldPackColumn.FpsIntNo, ((int)FieldPackStatusList.ExportedToDVD).ToString());
            //List<FieldPack> fieldPacks = imxFieldPackService.Find(query as SIL.IMX.DAL.Data.IFilterParameterCollection).ToList();
            string sql = String.Format("Inner Join FieldPack On FieldPack.FilmNo =Film.FilmNo Where FieldPack.FPSIntNo={0}", (int)FieldPackStatusList.ExportedToDVD);

            List<SIL.IMX.DAL.Entities.Film> imxFilms = DataService.ImxFilmService.Find(sql, 0, pageSize, out totalCount).ToList();

            return imxFilms;

        }

        private List<SIL.IMX.DAL.Entities.Frame> GetFramesInIMXByFilmIntNo(int filmIntNo)
        {
            List<SIL.IMX.DAL.Entities.Frame> frames = DataService.ImxFrameService.GetByFilmIntNo(filmIntNo).ToList();

            return frames;
        }

        private List<SIL.IMX.DAL.Entities.ScanImage> GetScanImageInIMXByFrameIntNo(int frameIntNo)
        {
            return DataService.ImxScanImageService.GetByFrameIntNo(frameIntNo).ToList();
        }


        private bool UpdateFilmToAarto(SIL.IMX.DAL.Entities.Film imxFilm, SIL.AARTO.DAL.Entities.Film aartoFilm)
        {

            try
            {
                aartoFilm.AutIntNo = imxFilm.AuthIntNo;
                aartoFilm.FilmNo = imxFilm.FilmNo;
                aartoFilm.MultipleFrames = imxFilm.MultipleFrames;
                aartoFilm.NoOfFrames = imxFilm.NoOfFrames;
                aartoFilm.NoOfScans = imxFilm.NoOfScans;
                aartoFilm.CdLabel = imxFilm.CdLabel;
                aartoFilm.MultipleViolations = imxFilm.MultipleViolations;
                aartoFilm.FilmDescr = imxFilm.FilmDescr;
                aartoFilm.LastUser = imxFilm.LastUser;
                aartoFilm.LastRefNo = 0;
                aartoFilm.FilmType = "";
                aartoFilm.FilmLoadDateTime = DateTime.Now.Date;
                aartoFilm.FilmLoadType = "";
                aartoFilm.FilmLoadFileName = "";

                //aartoFilm.TmsLoadStatus = 0;
                aartoFilm.FilmLoadStatus = (int)FilmLoadStatusList.FilmLoadedSuccessfully;
                aartoFilm.CameraType = "";
                aartoFilm.FilmImageRemoved = true;

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private bool UpdateFrameToAarto(SIL.IMX.DAL.Entities.Frame imxFrame, SIL.AARTO.DAL.Entities.Frame aartoFrame, int filmIntNo)
        {
            try
            {
                aartoFrame.FilmIntNo = filmIntNo;
                aartoFrame.FrameNo = imxFrame.FrameNo;
                aartoFrame.Sequence = imxFrame.Sequence;
                aartoFrame.RegNo = imxFrame.RegNo;
                aartoFrame.OffenceDate = imxFrame.OffenceDate.Value;
                aartoFrame.FirstSpeed = (byte)imxFrame.FirstSpeed;
                aartoFrame.SecondSpeed = (byte)imxFrame.SecondSpeed;
                aartoFrame.ElapsedTime = imxFrame.ElapsedTime;
                //aartoFrame.TravelDirection = imxFrame
                aartoFrame.ManualView = imxFrame.ManualView;
                aartoFrame.MultFrames = imxFrame.MultFrames;

                // aartoFrame. FrameVerifyDateTime= imxFrame.frmae
                // aartoFrame. FrameVerifyUser= imxFrame.framec
                aartoFrame.LastUser = "";

                aartoFrame.AllowContinue = imxFrame.AllowContinue;

                aartoFrame.TomsLocIntNo = imxFrame.LocIntNo;
                aartoFrame.TomsVmIntNo = imxFrame.VehMintNo;
                aartoFrame.TomsVtIntNo = imxFrame.VehTintNo;
                // aartoFrame.Toms_RdtIntNo= imxFrame.
                // aartoFrame. Toms_CrtIntNo= imxFrame
                aartoFrame.TomsRejIntNo = imxFrame.RejIntNo;
                aartoFrame.TomsRegNo = imxFrame.RegNo;

                aartoFrame.FrameStatus = (int)FrameStatusList.FrameLoadedViaImporterSuccessfully;


                aartoFrame.NatisProxyIndicator = "";
                aartoFrame.NoCommunicationFailed = 0;


                aartoFrame.AsdTimeDifference = imxFrame.AsdTimeDifference;
                aartoFrame.AsdSectionStartLane = imxFrame.AsdSectionStartLane;
                aartoFrame.AsdSectionStartLane = imxFrame.AsdSectionStartLane;
                aartoFrame.AsdSectionEndLane = imxFrame.AsdSectionEndLane;
                aartoFrame.AsdSectionDistance = imxFrame.AsdSectionDistance;
                aartoFrame.Asd1stCamUnitId = imxFrame.AsdCameraSerialNo1;
                aartoFrame.Asd2ndCamUnitId = imxFrame.AsdCameraSerialNo2;
                aartoFrame.FrameImageRemoved = true;


                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool UpdateScanImageToAarto(SIL.IMX.DAL.Entities.ScanImage imxImage, SIL.AARTO.DAL.Entities.ScanImage aartoImage, int frameIntNo)
        {
            try
            {
                aartoImage.FrameIntNo = frameIntNo;
                aartoImage.ScImType = "";
                aartoImage.JpegName = imxImage.JpegName;
                aartoImage.Xvalue = imxImage.Xvalue;
                aartoImage.Yvalue = imxImage.Yvalue;
                aartoImage.LastUser = "";
                aartoImage.ScImPrintVal = 0;

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private SIL.AARTO.DAL.Entities.Authority GetAuthorityFromAarto(string authCode)
        {
            try
            {
                return new SIL.AARTO.DAL.Services.AuthorityService().GetByAutCode(authCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public bool CheckFilmInAarto(string filmNo)
        {
            try
            {
                return DataService.AartoFilmService.GetByFilmNo(filmNo) == null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CheckFrameInAarto(int filmIntNo, string frameNo)
        {
            try
            {
                return DataService.AartoFrameService.GetByFilmIntNoFrameNo(filmIntNo, frameNo) == null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
