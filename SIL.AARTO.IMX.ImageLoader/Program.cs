﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Windows.Forms;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.DAL.Entities;
using System.Configuration;
using System.Reflection;

namespace SIL.AARTO.IMX.ImageLoader
{
    static class Program
    {
        public static string APP_TITLE = string.Empty;
        public static string APP_NAME = AartoProjectList.AARTOIMXImageLoader.ToString();
        public static string APP_DATABASE = string.Empty;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
            Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);


            // Check Last updated Version            
            string errorMessage;
            if (!CheckVersionManager.CheckVersion(AartoProjectList.AARTOIMXImageLoader, out errorMessage))
            {
                EntLibLogger.WriteLog(LogCategory.General, null, errorMessage);
                MessageBox.Show(errorMessage);
                return;
            }


            errorMessage = string.Empty;

            //if (!CheckVersion(SIL.IMX.DAL.Entities.ImxProjectList.AARTOIMXImageLoader, out errorMessage))
            //{
            //    EntLibLogger.WriteLog(LogCategory.General, null, errorMessage);
            //    MessageBox.Show(errorMessage);
            //    return;
            //}

            // Write the started Log
            APP_TITLE = string.Format("{0} - Version {1} Last Updated {2} Started at: {3}",
                APP_NAME,
                Assembly.GetExecutingAssembly().GetName().Version.ToString(2),
                ProjectLastUpdated.AARTOPrintEngine, DateTime.Now);

            //APP_DATABASE = EntLibLogger.GetServerAndDatabaseName(System.Configuration.ConfigurationManager.ConnectionStrings["IMXConnectionString"].ToString());

            //EntLibLogger.WriteLog(LogCategory.General, null, string.Format("{0}  {1}", APP_TITLE, APP_DATABASE));
            //Console.Title = APP_TITLE; 

            APP_DATABASE = EntLibLogger.GetServerAndDatabaseName(System.Configuration.ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());

            EntLibLogger.WriteLog(LogCategory.General, null, string.Format("{0}  {1}", APP_TITLE, APP_DATABASE));

            Application.Run(new UserLogin());
        }


        //public static bool CheckVersion(SIL.IMX.DAL.Entities.ImxProjectList appName, out string errorMessage)
        //{
        //    DateTime lastUpdated = (DateTime)GetLastUpdatedDatetime(appName);

        //    DateTime? dbLastupdated = DateTime.Parse("1900/01/01");

        //    // 2010/10/13 jerry changed it

        //    SIL.AARTO.DAL.Services.VersionControlSilService x = new SIL.AARTO.DAL.Services.VersionControlSilService();
        //    x.VerifySync(appName.ToString(), ref dbLastupdated);

        //    errorMessage = string.Format("Application {0} version {1:yyyy/MM/dd} is unable to access Database {2} version {3:yyyy/MM/dd}",
        //        appName.ToString(), lastUpdated, "AARTO", dbLastupdated.Value);
        //    return lastUpdated == dbLastupdated.Value;
        //}


        //private static object GetLastUpdatedDatetime(SIL.IMX.DAL.Entities.ImxProjectList appName)
        //{

        //    //Type type = Type.GetType("SIL.AARTO.BLL.Utility.ProjectLastUpdated");
        //    //MemberInfo[] members = type.GetMember(appName.ToString());
        //    //return members[0].

        //    PropertyInfo propertyInfo = typeof(SIL.AARTO.BLL.Utility.ProjectLastUpdated).GetProperty(appName.ToString());
        //    //BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

        //    return propertyInfo.GetValue(null, null);
        //}
        private static object GetLastUpdatedDatetime(string appName)
        {

            //Type type = Type.GetType("SIL.AARTO.BLL.Utility.ProjectLastUpdated");
            //MemberInfo[] members = type.GetMember(appName.ToString());
            //return members[0].
            PropertyInfo propertyInfo = typeof(SIL.AARTO.BLL.Utility.ProjectLastUpdated).GetProperty(appName.ToString());
            //BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

            return propertyInfo.GetValue(null, null);
        }
    }
}
