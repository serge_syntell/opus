using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Stalberg.TMS_3P_Loader.Components
{
    /// <summary>
    /// Represents a class that implements the Singleton pattern
    /// </summary>
    public class Singleton
    {
        // Fields
        private List<ProcessToCheckFor> applicationNames = null;
        private string mainApplicationName = string.Empty;
        private string message = string.Empty;
        private bool showMessage = false;
        private bool checkTitle = false;
        private StringBuilder sb = null;
        private bool useDefaultMessage = true;

        // Constants
        private const string MAIN_APPLICATION_MESSAGE = "You cannot run more than one copy of {0} at the same time.\n";
        private const string OTHER_APPLICATION_MESSAGE = "The process {0} cannot be run at the same time as {1}.\n";

        /// <summary>
        /// Initializes a new instance of the <see cref="Singleton"/> class.
        /// </summary>
        public Singleton()
        {
            this.applicationNames = new List<ProcessToCheckFor>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Singleton"/> class.
        /// </summary>
        /// <param name="applicationName">Name of the application.</param>
        public Singleton(string applicationName)
            : this()
        {
            this.mainApplicationName = applicationName.ToUpper();
            this.applicationNames.Add(new ProcessToCheckFor(this.mainApplicationName, true));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Singleton"/> class.
        /// </summary>
        /// <param name="applicationName">Name of the Title or Process Name of the application depending on whether the CheckTitle property is set.</param>
        /// <param name="message">The message to display when another version of the application is running.</param>
        public Singleton(string applicationName, string message)
            : this()
        {
            this.mainApplicationName = applicationName.ToUpper();
            this.applicationNames.Add(new ProcessToCheckFor(this.mainApplicationName, true));
            this.message = message;
            this.useDefaultMessage = false;
        }

        /// <summary>
        /// Adds other process names to check for.
        /// </summary>
        /// <remarks>
        /// These processes will be checked for and the Check method will fail if a single instance of any of them is running
        /// </remarks>
        /// <param name="applicationNames">The application names.</param>
        public void OtherProcessesToCheckFor(string[] applicationNames)
        {
            ProcessToCheckFor process = null;
            for (int i = 0; i < applicationNames.Length; i++)
            {
                process = new ProcessToCheckFor(applicationNames[i].ToUpper(), false);
                if (!this.applicationNames.Contains(process))
                    this.applicationNames.Add(process);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to check the title of the application or its process name. Default is false.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the class should check the Window Title; otherwise, <c>false</c> to check the Process Name.
        /// </value>
        public bool CheckTitle
        {
            get { return checkTitle; }
            set { checkTitle = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show message box or not. Default is <c>false</c>.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if a message box should be show; otherwise, <c>false</c>.
        /// </value>
        public bool ShowMessage
        {
            get { return showMessage; }
            set { showMessage = value; }
        }

        /// <summary>
        /// Gets or sets the name of the application.
        /// </summary>
        /// <value>The name of the application.</value>
        public string ApplicationName
        {
            get
            {
                foreach (ProcessToCheckFor processName in this.applicationNames)
                {
                    if (processName.IsApplicationProcess)
                        return processName.Name;
                }
                return string.Empty;
            }
            set
            {
                this.mainApplicationName = value.ToUpper();
                this.applicationNames.Add(new ProcessToCheckFor(this.mainApplicationName, true));
            }
        }

        /// <summary>
        /// Gets or sets the message to display if the application is already running.
        /// </summary>
        /// <value>The message.</value>
        public string Message
        {
            get { return message; }
            set
            {
                this.message = value;
                this.useDefaultMessage = (message.Length > 0);
            }
        }

        /// <summary>
        /// Checks whether the supplied application is already running.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if the application is already running.
        /// </returns>
        public bool Check()
        {
            // Check a main application name has been set
            bool found = false;
            foreach (ProcessToCheckFor processName in this.applicationNames)
            {
                if (processName.IsApplicationProcess)
                    found = true;
            }
            if (!found)
                throw new ArgumentException("You haven't supplied the main application's name yet.");

            // Set the Action to perform
            CheckAction action;
            if (this.checkTitle)
                action = CheckAction.CheckWindowTitle;
            else
                action = CheckAction.CheckProcessName;

            // Check the processes
            if (this.IterateProcesses(action))
            {
                if (this.showMessage)
                {
                    System.Windows.Forms.MessageBox.Show(this.message, "Singleton",
                        System.Windows.Forms.MessageBoxButtons.OK,
                        System.Windows.Forms.MessageBoxIcon.Exclamation);
                }
                else
                    Trace.WriteLine(this.message);

                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Iterates the processes (the workhorse method of the class).
        /// </summary>
        /// <param name="action">The action.</param>
        /// <returns><c>true</c> if any of the processes were found that should not be running</returns>
        private bool IterateProcesses(CheckAction action)
        {
            System.Threading.Thread.Sleep(10);

            // Initialise or reset the string builder
            switch (action)
            {
                case CheckAction.OutputProcessNames:
                case CheckAction.OutputWindowTitles:
                    if (this.sb == null)
                        this.sb = new StringBuilder();
                    this.sb.Length = 0;
                    break;
            }

            // Loop through the current processes
            ProcessToCheckFor processName = null;
            foreach (Process process in Process.GetProcesses())
            {
                switch (action)
                {
                    case CheckAction.OutputProcessNames:
                        sb.Append(process.ProcessName + "\n");
                        break;

                    case CheckAction.OutputWindowTitles:
                        sb.Append(process.MainWindowTitle + "\n");
                        break;

                    case CheckAction.CheckProcessName:
                        // Loop through each of the application process names in the list
                        for (int i = 0; i < this.applicationNames.Count; i++)
                        {
                            processName = this.applicationNames[i];
                            if (process.ProcessName.ToUpper().Equals(processName.Name))
                                ++processName.Instances;
                        }
                        break;

                    case CheckAction.CheckWindowTitle:
                        // Loop through each of the application window titles in the list
                        for (int i = 0; i < this.applicationNames.Count; i++)
                        {
                            if (process.MainWindowTitle.ToUpper().Equals(processName.Name))
                                ++processName.Instances;
                        }
                        break;
                }
            }

            // Loop the processes to check for to find if any instances were found
            bool response = false;
            StringBuilder sbMessage = new StringBuilder();
            for (int i = 0; i < this.applicationNames.Count; i++)
            {
                processName = this.applicationNames[i];

                // Check if any of these processes were found
                if (processName.IsApplicationProcess)
                {
                    if (processName.Instances > 1)
                    {
                        sbMessage.Append(String.Format(MAIN_APPLICATION_MESSAGE, processName.Name));
                        response = true;
                    }
                }
                else
                {
                    if (processName.Instances > 0)
                    {
                        sbMessage.Append(String.Format(OTHER_APPLICATION_MESSAGE, processName.Name, this.mainApplicationName));
                        response = true;
                    }
                }

                // Reset instances
                processName.Instances = 0;
            }

            // Set the message variable if the default is to be used
            if (this.useDefaultMessage)
                this.message = sbMessage.ToString();

            return response;
        }

        /// <summary>
        /// Gets a list of the currently open Window's Title bar text.
        /// </summary>
        /// <returns>A <seealso cref="T:String"/> containing the list of the main window's title text.</returns>
        public string GetWindowTitles()
        {
            this.IterateProcesses(CheckAction.OutputWindowTitles);
            return this.sb.ToString();
        }

        /// <summary>
        /// Gets a list of all the current process's names.
        /// </summary>
        /// <returns>A <seealso cref="T:String"/> containing the list of process names.</returns>
        public string GetProcessNames()
        {
            this.IterateProcesses(CheckAction.OutputProcessNames);
            return this.sb.ToString();
        }

        /// <summary>
        /// Lists the actions that can be checked by the singleton
        /// </summary>
        private enum CheckAction
        {
            OutputWindowTitles = 1,
            OutputProcessNames = 2,
            CheckWindowTitle = 3,
            CheckProcessName = 4
        }

        /// <summary>
        /// Represents a process to be searched for
        /// </summary>
        private class ProcessToCheckFor
        {
            // Fields
            public string Name;
            public int Instances;
            public bool IsApplicationProcess;

            /// <summary>
            /// Initializes a new instance of the <see cref="ProcessToCheckFor"/> class.
            /// </summary>
            /// <param name="name">The name.</param>
            /// <param name="isApplicationProcess">if set to <c>true</c> [is application process].</param>
            public ProcessToCheckFor(string name, bool isApplicationProcess)
            {
                this.IsApplicationProcess = isApplicationProcess;
                this.Name = name;
                this.Instances = 0;
            }

            /// <summary>
            /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
            /// </summary>
            /// <returns>
            /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
            /// </returns>
            public override string ToString()
            {
                return this.Name;
            }

            /// <summary>
            /// Determines whether the specified <see cref="T:System.Object"></see> is equal to the current <see cref="T:System.Object"></see>.
            /// </summary>
            /// <param name="obj">The <see cref="T:System.Object"></see> to compare with the current <see cref="T:System.Object"></see>.</param>
            /// <returns>
            /// true if the specified <see cref="T:System.Object"></see> is equal to the current <see cref="T:System.Object"></see>; otherwise, false.
            /// </returns>
            public override bool Equals(object obj)
            {
                ProcessToCheckFor process = (ProcessToCheckFor)obj;
                return process.Name.Equals(this.Name);
            }

            /// <summary>
            /// Serves as a hash function for a particular type. <see cref="M:System.Object.GetHashCode"></see> is suitable for use in hashing algorithms and data structures like a hash table.
            /// </summary>
            /// <returns>
            /// A hash code for the current <see cref="T:System.Object"></see>.
            /// </returns>
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

        }

    }
}
