using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Stalberg.Indaba;
using Stalberg.ThaboAggregator.Objects;
using Stalberg.TMS;
using Stalberg.TMS_TPExInt.Objects;

namespace Stalberg.TMS_TPExInt
{
    /// <summary>
    /// Represents the class that handles processing all the current EasyPay & PayFine data waiting to be sent.
    /// </summary>
    internal class EasyPayProcessing : IDisposable
    {
        // Statis
        private static string _machineName = string.Empty;
        // Fields
        private readonly TPExIntParameters _parameters;
        private readonly string _serverID = string.Empty;
        private Stalberg.TMS.EasyPayDB _db;
        private ICommunicate _indaba;

        /// <summary>
        /// Gets the name of the machine.
        /// </summary>
        /// <value>The name of the machine.</value>
        public static string MachineName
        {
            get { return EasyPayProcessing._machineName; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EasyPayProcessing"/> class.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        public EasyPayProcessing(TPExIntParameters parameters)
        {
            this._parameters = parameters;
            this._db = new Stalberg.TMS.EasyPayDB(this._parameters.ConnectionString);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (this._db != null)
                this._db.Dispose();
        }

        /// <summary>
        /// The main entry point for EasyPay & PayFine transaction processing within TPEx Int.
        /// </summary>
        public bool ProcessTransactions()
        {
            this._indaba = Client.Create();

            // Get data to send to Sipho
            //this.GetHubData();

            // Get data back from Thabo
            bool failed = this.SendHubData();
            return failed;
        }

        #region Extracting and sending data to Thabo

        private bool SendHubData()
        {
            // Retrieve any new data to send and put it into files
            Logger.Write(string.Format("Creating Export Files"), "General", (int)TraceEventType.Information);
            bool failed = this.CreateExportFiles();
            if (failed)
            {
                Logger.Write("Unable to create Export Files - PROCESSING ABORTED.", "General", (int)TraceEventType.Critical);
                return failed;
            }
            else
            {
                Logger.Write("Finished Creating export Files.");
            }

            // Clean up expired EasyPayTran Data
            Logger.Write(string.Format("Cleanup Expired EasyPay Transactions"), "General", (int)TraceEventType.Information);
            this.CleanupExpiredEasyPayTransactions();
            Logger.Write("Finished Cleaning Up Expired EasyPay transactions.");

            // Send any new or unsent export files
            Logger.Write(string.Format("Check For Files To Send"), "General", (int)TraceEventType.Information);
            failed  = this.CheckForFilesToSend();
            if (failed)
            {
                Logger.Write("Unable to complete checking for files to send - PROCESSING ABORTED.", "General", (int)TraceEventType.Critical);
                return failed;
            }
            else
            {
                Logger.Write("Finished Checking for files to send.");
            }

            return failed;
        }

        private void CleanupExpiredEasyPayTransactions()
        {
            try
            {
                _db = new EasyPayDB(this._parameters.ConnectionString);
                _db.CleanupExpiredEasyPayTransactions();
            }
            catch (Exception ex)
            {
                Logger.Write("Cleaning up expired EasyPay transactions " + ex.Message, "General", (int)TraceEventType.Warning);
            }
        }

        private bool CreateExportFiles()
        {
            bool failed = false;

            try
            {
                // Get the list of Authorities with data to export
                _db = new EasyPayDB(this._parameters.ConnectionString);
                List<EasyPayAuthority> authorities = this.GetAuthorities();
                Guid id;
                string fileName;

                foreach (EasyPayAuthority authority in authorities)
                {
                    int noRecords;

                    do
                    {
                        // Create the file name
                        fileName = string.Format("{0}_{1}_{2}{3}",
                            FineExchangeDataFile.FILE_PREFIX,
                            authority.Name.Replace(" ", "_"),
                            DateTime.Now.ToString(FineExchangeDataFile.DATE_FORMAT),
                            FineExchangeDataFile.EXTENSION);

                        // Collect data from local resources
                        noRecords = this.GetFineData(authority, fileName);
                        if (noRecords > 0)
                        {
                            // Check for EasyPay data extraction
                            if (authority.IsEasyPay)
                                this.GetEasyPayData(authority, fileName);

                            // Create the FEDF file
                            try
                            {
                                FineExchangeDataFile file = new FineExchangeDataFile(authority,
                                    this._serverID,
                                    this._parameters.FtpParameters.ExportPath,
                                    fileName);
                                file.MachineName = EasyPayProcessing._machineName;
                                file.Write();

                                // Send to Indaba
                                id = this._indaba.Enqueue(file.FileName);

                                // Check it was enqueued successfully
                                if (!id.Equals(Guid.Empty))
                                {
                                    // Update the sent status 
                                    _db.UpdateSentStatus(Path.GetFileName(file.FileName), Beginnings.APP_NAME);

                                    // Delete the file
                                    File.Delete(file.FileName);
                                }
                            	else
                            	{
                                	//091203 FT Update the sent status 
                                	_db.ResetSentStatus(fileName, Beginnings.APP_NAME);
                                	Logger.Write(string.Format("Error adding file {0} to Indaba.", fileName), "General", (int)TraceEventType.Warning);
                                    failed = true;
                                    return failed;
                            	}
                        	}
                        	catch (Exception ex)
                        	{
                            	_db.ResetSentStatus(fileName, Beginnings.APP_NAME);
                                Logger.Write(string.Format("Error create the FEDF file {0}\n{1}", ex.Message, ex.StackTrace), "General", (int)
                                    TraceEventType.Critical);
                                failed = true;
                                return failed;
                        	}
                        }
                    }
                    while (noRecords > 0);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(string.Format("Error creating Export Files ({0})\n{1}", ex.Message, ex.StackTrace), "General", (int)TraceEventType.Critical);
                failed = true;
                return failed;
            }

            return failed;
        }

        private bool CheckForFilesToSend()
        {
            bool failed = false;

            try
            {
                DirectoryInfo dir = new DirectoryInfo(this._parameters.FtpParameters.ExportPath);
                if (!dir.Exists)
                {
                    failed = true;
                    return failed;
                }

                Guid id;
                foreach (FileInfo file in dir.GetFiles("*" + FineExchangeDataFile.EXTENSION))
                {
                    try
                    {
                        id = this._indaba.Enqueue(file);
                   		if (!id.Equals(Guid.Empty))
                    	{
                        	//091201 FT Update the sent status 
                            _db.UpdateSentStatus(Path.GetFileName(file.Name), Beginnings.APP_NAME);

                        	file.Delete();
						}
                        else
                        {
                            //091203 FT Update the sent status
                            _db.ResetSentStatus(file.Name, Beginnings.APP_NAME);
                            Logger.Write(string.Format("Error adding file {0} to Indaba.", file.Name), "General", (int)TraceEventType.Warning);
                            failed = true;
                            return failed;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Write(string.Format("Error adding file {0} to Indaba ({1}).\n{2}", file.Name, ex.Message, ex.StackTrace), "General", (int)TraceEventType.Critical);
                        failed = true;
                        return failed;
                    }
    
                }
            }
            catch (Exception ex)
            {
                Logger.Write(string.Format("Error checking for files to send: ({0})\n{1}", ex.Message, ex.StackTrace), "General", (int)TraceEventType.Critical);
                failed = true;
                return failed;
            }

            return failed;
        }

        private void GetEasyPayData(EasyPayAuthority authority, string fileName)
        {
            SqlDataReader reader = _db.GetEasyPayTransactions(authority.ID, fileName);
            while (reader.Read())
            {
                EasyPayTransaction tran = new EasyPayTransaction(authority);
                tran.EasyPayAction = ((string)reader["EPAction"])[0];
                tran.EasyPayNumber = ((string)reader["EasyPayNumber"]).Trim();
                tran.ExpiryDate = (DateTime)reader["EPExpiryDate"];
                tran.NoticeID = (int)reader["NotIntNo"];
                tran.Amount = (decimal)reader["EPAmount"];

                authority.EasyPayTransactions.Add(tran);
            }
            reader.Close();
        }

        private string SafeStringReader(SqlDataReader reader, string columName)
        {
            int index = reader.GetOrdinal(columName);
            if (reader.IsDBNull(index))
                return string.Empty;

            string value = reader.GetString(index);
            if (value.Contains(Environment.NewLine))
                throw new ApplicationException(string.Format("The value '{1}' for the {0} column contains new-line(s) in the database and must not.", columName, value.Trim()));

            return value.Trim();
        }
		
        private int GetFineData(EasyPayAuthority authority, string fileName)
        {
            authority.Fines.Clear();
            int noRecords;

            // 2013-07-24 add Beginnings.APP_NAME by Henry
            SqlDataReader reader = _db.GetExportData(authority.ID, fileName, Beginnings.APP_NAME);
            while (reader.Read())
            {
                EasyPayFineData fine = new EasyPayFineData(authority);
                fine.NoticeID = (int)reader["NotIntNo"];
                fine.FilmNumber = this.SafeStringReader(reader, "NotFilmNo");
                fine.FineAmount = decimal.Parse(reader["ChgRevFineAmount"].ToString());
                fine.FrameNumber = this.SafeStringReader(reader, "NotFrameNo");
                fine.Location = this.SafeStringReader(reader, "NotLocDescr");
                fine.OffenceDate = (DateTime)reader["NotOffenceDate"];
                fine.OwnerID = this.SafeStringReader(reader, "IDNumber");
                fine.OwnerName = string.Format("{0} {1}", reader["Initial"].ToString().Trim(), reader["Surname"].ToString().Trim()).Trim();
                if (reader["NotPaymentDate"] != DBNull.Value)
                    fine.PaymentDate = (DateTime)reader["NotPaymentDate"];
                fine.RegNo = this.SafeStringReader(reader, "NotRegNo");
                fine.Status = (int)reader["ChargeStatus"];
                fine.StatusDescription = this.SafeStringReader(reader, "CSDescr");
                if (reader["NotTicketNo"] == DBNull.Value || reader["NotTicketNo"].ToString().Length == 0)
                    continue;

                fine.TicketNumber = this.SafeStringReader(reader, "NotTicketNo");
                fine.CameraSerialNumber = this.SafeStringReader(reader, "NotCamSerialNo");
                fine.OffenceCode = this.SafeStringReader(reader, "ChgOffenceCode");
                fine.OffenceDescription = this.SafeStringReader(reader, "ChgOffenceDescr");
                fine.Speed1 = (int)reader["NotSpeed1"];
                fine.Speed2 = (int)reader["NotSpeed2"];
                fine.SpeedZone = (int)reader["NotSpeedLimit"];
                if (reader["ChgNoAOG"] != DBNull.Value && reader["ChgNoAOG"].ToString().Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                    fine.IsNoAOG = 'Y';

                authority.Fines.Add(fine);
            }

            // Get the number of records in the result set
            reader.NextResult();
            if (reader.Read())
                noRecords = reader.GetInt32(0);
            else
                noRecords = 0;

            reader.Close();

            if (noRecords > 0)
                Logger.Write(string.Format("{0} records batched for {1}", noRecords, authority), "General", (int)TraceEventType.Information);

            return noRecords;
        }

        private List<EasyPayAuthority> GetAuthorities()
        {
            List<EasyPayAuthority> authorities = new List<EasyPayAuthority>();

            SqlDataReader reader = _db.GetAuthoritiesForExport();
            while (reader.Read())
            {
                EasyPayAuthority authority = new EasyPayAuthority();

                // Authority Data
                authority.ID = (int)reader["AutIntNo"];
                authority.Name = (string)reader["AutName"];
                authority.AuthorityCode = ((string)reader["AutCode"]).Trim();
                authority.AuthorityNumber = ((string)reader["AutNo"]).Trim();
                if (reader["EasyPayReceiptID"] != DBNull.Value)
                    authority.ReceiverIdentifier = (int)reader["EasyPayReceiptID"];
                // Metro Data
                authority.MetroCode = ((string)reader["MtrCode"]).Trim();
                authority.MetroName = (string)reader["MtrName"];
                // Region Data
                authority.RegionCode = ((string)reader["RegCode"]).Trim();
                authority.RegionName = (string)reader["RegName"];

                EasyPayProcessing._machineName = (string)reader["MachineName"];

                authorities.Add(authority);
            }
            reader.Close();

            return authorities;
        }

        #endregion

        #region Getting Response data back from Thabo

        public void ImportPaymentFeedback()
        {
            try
            {
                this._indaba = Client.Create();

                // Get any feedback files to process
                List<FineExchangeFeedbackFile> files = this.ProcessFeedBackFiles();
                if (files.Count == 0)
                    return;

                // Update the database
                this._db.SendEmail += db_SendEmail;
                StringBuilder sb = new StringBuilder();
                foreach (FineExchangeFeedbackFile file in files)
                {
                    this._db.GetAuthorityID(file);

                    this._db.ProcessPayments(file, ref sb, Beginnings.APP_NAME);
                }
                this._db.SendEmail -= db_SendEmail;

                if (sb.Length > 0)
                    Logger.Write(sb.ToString(), "General", (int)TraceEventType.Information);
            }
            catch (Exception ex)
            {
                Logger.Write(string.Format("Error Importing Payment Feedback (0)\n{1}", ex.Message, ex.StackTrace), "General", (int)TraceEventType.Critical);
            }
        }

        private void db_SendEmail(object sender, EmailEventArgs e)
        {
            try
            {
                EmailHelper email = new EmailHelper();
                email.Subject = e.Subject;
                email.Message = e.Body;
                email.Attachments.AddRange(e.Attachments);

                if (!email.Send())
                    Logger.Write("ERROR: an email could not be sent.", "General", (int)TraceEventType.Error);
            }
            catch (Exception ex)
            {
                Logger.Write(string.Format("There was a problem sending email ({0})\n{1}", ex.Message, ex.StackTrace), "General", (int)TraceEventType.Warning);
            }
        }

        private List<FineExchangeFeedbackFile> ProcessFeedBackFiles()
        {
            List<FineExchangeFeedbackFile> feedbackFiles = new List<FineExchangeFeedbackFile>();

            this._indaba.Source = "Thabo";
            IndabaFileInfo[] files = this._indaba.GetFileInformation(FineExchangeFeedbackFile.EXTENSION);
            if (files.Length == 0)
                Logger.Write("There were no feedback files to process.", "General", (int)TraceEventType.Information);

            //------DON'T FORGET TO RECOMMENT THIS CODE!!!!!

            ////temporary code here - uncomment this section if you want to import a single file during testing
            //FileStream tempFile = new FileStream("C:\\Temp\\Thabo\\Feedback\\FineExchangeFeedbackFine_ST_2008-11-13_17-11-07-0161.feff", FileMode.Open);
            ////temporary code to load from file system
            //StreamReader sr = new StreamReader(tempFile);
            //Stream stream = sr.BaseStream;
            //// Process the file data into a feedback object
            //FineExchangeFeedbackFile feedbackFile = new FineExchangeFeedbackFile(stream);
            //feedbackFiles.Add(feedbackFile);
            ////temorary code ends here

            ////------DON'T FORGET TO UNCOMMENT THIS CODE!!!!!

            //comment out this section when loading a single file during testing
            foreach (IndabaFileInfo file in files)
            {
                Logger.Write(string.Format("Processing feedback file: {0} from {1}", file.Name, file.Source), "General", (int)TraceEventType.Information);

                try
                {
                    // Get the file data from Indaba
                    Stream stream = this._indaba.Peek(file);

                    // Process the file data into a feedback object
                    FineExchangeFeedbackFile feedbackFile = new FineExchangeFeedbackFile(stream);
                    feedbackFiles.Add(feedbackFile);

                    // Dequeue the file
                    this._indaba.Dequeue(file);

                }
                catch (Exception ex)
                {
                    Logger.Write(string.Format("There was an error processing a feedback file {0} ({1}).\n{2}", file.Name, ex.Message, ex.StackTrace), "General", (int)TraceEventType.Critical);
                }
            }
            //end of comment out section for testing

            return feedbackFiles;
        }

        #endregion

    }
}
