using System;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using Stalberg.TMS_TPExInt.Components;
using System.Text.RegularExpressions;
using Stalberg.TMS;
using System.Collections;
using Stalberg.TMS_TPExInt.Objects;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Diagnostics;

namespace Stalberg.TMS_TPExInt
{
    /// <summary>
    /// Contains all the methods to get the data
    /// </summary>
    internal class GetData
    {
        // Fields
        //private GeneralFunctions general = new GeneralFunctions();

        /// <summary>
        /// Initializes a new instance of the <see cref="GetData"/> class.
        /// </summary>
        public GetData()
        {
        }

        /// <summary>
        /// Gets the start values.
        /// </summary>
        /// <param name="connStr">The conn STR.</param>
        /// <param name="procStr">The proc STR.</param>
        /// <param name="localFtpParm">The local FTP parm.</param>
        /// <param name="writer">The writer.</param>
        /// <returns></returns>
        public bool GetStartValues(TPExIntParameters parameters)
        {
            bool failed = false;
            // get the environmental parameters
            // xml file to get server name & FTP process name
            this.GetServerXML(parameters);
            if (parameters.ConnectionString.Length == 0)
            {
                Logger.Write("GetServerXML failed at: " + DateTime.Now.ToString(), "General", (int)TraceEventType.Error);
                failed = true;
                return failed;
            }
            return failed;
        }

        /// <summary>
        /// Gets the server XML.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        public void GetServerXML(TPExIntParameters parameters)
        {
            //load xml file with system parameters
            string currentDir = Directory.GetCurrentDirectory();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(currentDir + "\\SysParam.xml");
            XmlNode root = xmlDoc.FirstChild;

            // Assign the contents of the child nodes to application variables
            XmlNode node = null;
            if (root.HasChildNodes)
            {
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    switch (root.ChildNodes[i].Name)
                    {
                        case "constr":
                            parameters.ConnectionString = root.ChildNodes[i].InnerText;
                            break;

                        case "procstr":
                            parameters.ProcessName = root.ChildNodes[i].InnerText;
                            parameters.FtpParameters.ProcessName = root.ChildNodes[i].InnerText;
                            break;

                        case "ftpExportServer":
                            parameters.FtpParameters.ExportServer = root.ChildNodes[i].InnerText;
                            break;

                        case "ftpPromunPath":
                            parameters.FtpParameters.PromunPath  = root.ChildNodes[i].InnerText;
                            break;

                        case "ftpExportPath":
                            parameters.FtpParameters.ExportPath = root.ChildNodes[i].InnerText;
                            break;

                        case "ftpHostServer":
                            parameters.FtpParameters.HostServer = root.ChildNodes[i].InnerText;
                            break;

                        case "ftpHostIP":
                            parameters.FtpParameters.HostIP = root.ChildNodes[i].InnerText;
                            break;

                        case "ftpHostUser":
                            parameters.FtpParameters.HostUser = root.ChildNodes[i].InnerText;
                            break;

                        case "ftpHostPass":
                            parameters.FtpParameters.HostPassword = root.ChildNodes[i].InnerText;
                            break;

                        case "ftpHostPath":
                            parameters.FtpParameters.HostPath = root.ChildNodes[i].InnerText;
                            break;

                        case "smtp":
                            parameters.FtpParameters.SMTP = root.ChildNodes[i].InnerText;
                            break;

                        case "systemEmail":
                            parameters.FtpParameters.SystemEmail = root.ChildNodes[i].InnerText;
                            break;

                        case "contractor":
                            parameters.FtpParameters.Contractor = root.ChildNodes[i].InnerText;
                            break;

                        //new path for images stored in file system
                        case "imageFolder":
                            parameters.ImageFolder = root.ChildNodes[i].InnerText;
                            break;

                        case "thabo":
                            node = xmlDoc.SelectSingleNode("/codes/thabo");
                            if (node != null)
                            {
                                XmlAttribute attribute = node.Attributes["connectionString"];
                                if (attribute != null)
                                    parameters.Thabo.ConnectionString = attribute.Value;
                            }
                            break;

                        //case "civitasPI":
                        //    node = xmlDoc.SelectSingleNode("/codes/civitasPI/configurationFilesFolder");
                        //    if (node != null)
                        //        parameters.CivitasPI.ConfigurationFilesFolder = node.InnerText;
                        //    node = xmlDoc.SelectSingleNode("/codes/civitasPI/contraventionFilesFolder");
                        //    if (node != null)
                        //        parameters.CivitasPI.ContraventionFilesFolder = node.InnerText;
                        //    node = xmlDoc.SelectSingleNode("/codes/civitasPI/responseFilesFolder");
                        //    if (node != null)
                        //        parameters.CivitasPI.ResponseFilesFolder = node.InnerText;
                        //    node = xmlDoc.SelectSingleNode("/codes/civitasPI/allocationFilesFolder");
                        //    if (node != null)
                        //        parameters.CivitasPI.AllocationFilesFolder = node.InnerText;
                        //    node = xmlDoc.SelectSingleNode("/codes/civitasPI/exportFolder");
                        //    if (node != null)
                        //        parameters.CivitasPI.ExportFolder = node.InnerText;
                        //    break;
                    }
                }
                parameters.FtpParameters.ID = 0; // not required
            }
        }

    }
}
