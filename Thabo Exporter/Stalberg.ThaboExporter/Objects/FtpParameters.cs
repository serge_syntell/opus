using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Stalberg.TMS_TPExInt.Objects
{
    /// <summary>
    /// Represents all the data necessary to make an FTP connection
    /// </summary>
    public class FtpParameters
    {
        // Fields
        private int id;
        private string processName;
        private string exportServer;
        private string exportPath;
        private string promunPath;
        private string hostServer;
        private string hostIP;
        private string hostUser;
        private string hostPassword;
        private string hostPath;
        private string smtp;
        private string contractor;
        private string systemEmail;

        /// <summary>
        /// Initializes a new instance of the <see cref="FtpParameters"/> class.
        /// </summary>
        public FtpParameters()
        {
        }

        /// <summary>
        /// Gets or sets the system email address, 
        /// i.e. the address that all mail sent from this system use.
        /// </summary>
        /// <value>The system email.</value>
        public string SystemEmail
        {
            get { return systemEmail; }
            set { systemEmail = value; }
        }

        /// <summary>
        /// Gets or sets the contractor code to identify the source and destination of this process.
        /// </summary>
        /// <value>The contractor.</value>
        public string Contractor
        {
            get { return contractor; }
            set { contractor = value; }
        }

        /// <summary>
        /// Gets or sets the address of the SMTP server to send mail to.
        /// </summary>
        /// <value>The SMTP.</value>
        public string SMTP
        {
            get { return smtp; }
            set { smtp = value; }
        }

        /// <summary>
        /// Gets or sets the initial path to switch to on the FTP host server.
        /// </summary>
        /// <value>The host path.</value>
        public string HostPath
        {
            get { return hostPath; }
            set { hostPath = value; }
        }

        /// <summary>
        /// Gets or sets the user password for connecting to the host.
        /// </summary>
        /// <value>The host password.</value>
        public string HostPassword
        {
            get { return hostPassword; }
            set { hostPassword = value; }
        }

        /// <summary>
        /// Gets or sets the user name to use when connecting to the FTP host.
        /// </summary>
        /// <value>The host user.</value>
        public string HostUser
        {
            get { return hostUser; }
            set { hostUser = value; }
        }

        /// <summary>
        /// Gets or sets the host (FTP) IP address.
        /// </summary>
        /// <value>The host IP.</value>
        public string HostIP
        {
            get { return hostIP; }
            set { hostIP = value; }
        }

        /// <summary>
        /// Gets or sets the host (FTP) server.
        /// </summary>
        /// <value>The host server.</value>
        public string HostServer
        {
            get { return hostServer; }
            set { hostServer = value; }
        }

        /// <summary>
        /// Gets or sets the path to export from.
        /// </summary>
        /// <value>The export path.</value>
        public string ExportPath
        {
            get { return exportPath; }
            set { exportPath = value; }
        }

        /// <summary>
        /// Gets or sets the path for the promun interface
        /// </summary>
        /// <value>The promun path.</value>
        public string PromunPath
        {
            get { return promunPath; }
            set { promunPath = value; }
        }

        /// <summary>
        /// Gets or sets the export server name (i.e. the machine that is running the program).
        /// </summary>
        /// <value>The export server.</value>
        public String ExportServer
        {
            get { return exportServer; }
            set { exportServer = value; }
        }

        /// <summary>
        /// Gets or sets the name of the process that is currently underway,
        /// used for logging purposes mainly.
        /// </summary>
        /// <value>The name of the process.</value>
        public String ProcessName
        {
            get { return processName; }
            set { processName = value; }
        }

        /// <summary>
        /// Gets or sets the FTP process ID.
        /// </summary>
        /// <value>The ID.</value>
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

    }
}
