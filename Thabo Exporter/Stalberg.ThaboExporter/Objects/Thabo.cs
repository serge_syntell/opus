using System;
using System.Collections.Generic;
using System.Text;

namespace Stalberg.TMS_TPExInt.Objects
{
    /// <summary>
    /// Represents the details that a client needs to know about Thabo
    /// </summary>
    internal class Thabo
    {
        // Fields
        private string connectionString = string.Empty;
        private int batchSize = 100;

        /// <summary>
        /// Initializes a new instance of the <see cref="Thabo"/> class.
        /// </summary>
        public Thabo()
        {
        }

        /// <summary>
        /// Gets or sets the database connection string for the Thabo Client.
        /// </summary>
        /// <value>The connection string.</value>
        public string ConnectionString
        {
            get { return this.connectionString; }
            set { this.connectionString = value; }
        }

        /// <summary>
        /// Gets or sets the size to batch notices into files for export.
        /// </summary>
        /// <value>The size of the batch.</value>
        public int BatchSize
        {
            get { return this.batchSize; }
            set { this.batchSize = value; }
        }

    }
}
