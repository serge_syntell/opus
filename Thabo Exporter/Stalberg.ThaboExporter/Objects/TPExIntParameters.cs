using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Diagnostics;
using System.IO;

namespace Stalberg.TMS_TPExInt.Objects
{
    /// <summary>
    /// Contains all the TPExInt program parameter data
    /// </summary>
    internal class TPExIntParameters
    {
        // Fields
        private string connectionString = string.Empty;
        private string processName = string.Empty;
        private string imageFolder = string.Empty;
        private FtpParameters localFtpParameters;
        private Thabo thabo;
        //private CivitasPI civitasPI;

        /// <summary>
        /// Initializes a new instance of the <see cref="TPExIntParameters"/> class.
        /// </summary>
        public TPExIntParameters()
        {
            this.localFtpParameters = new FtpParameters();
            this.thabo = new Thabo();
            //this.civitasPI = new CivitasPI();
        }

        #region Properties

        /// <summary>
        /// Gets the settings for communication with Thabo.
        /// </summary>
        /// <value>The thabo.</value>
        public Thabo Thabo
        {
            get { return this.thabo; }
        }

        ////settings for Civitas phase I interface
        //public CivitasPI CivitasPI
        //{
        //    get { return this.civitasPI; }
        //}

        /// <summary>
        /// Gets the FTP parameters object.
        /// </summary>
        /// <value>The FTP parameters.</value>
        public FtpParameters FtpParameters
        {
            get { return localFtpParameters; }
        }

        /// <summary>
        /// Gets or sets the name of the process.
        /// </summary>
        /// <value>The name of the process.</value>
        public string ProcessName
        {
            get { return processName; }
            set { processName = value; }
        }

        /// <summary>
        /// Gets or sets the database connection string.
        /// </summary>
        /// <value>The connection string.</value>
        public string ConnectionString
        {
            get { return connectionString; }
            set { connectionString = value; }
        }

        public string ImageFolder
        {
            get { return imageFolder; }
            set { imageFolder = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sends an Email containing the log file to the System Administrator.
        /// </summary>
        public void SendMail(bool closeLogWriter)
        {
            string message = "On the " + DateTime.Now.ToString() + " the following log file was created by the " + this.processName + " process:\n\n"
                   + "File name: " + Beginnings.LOG_FILE_PATH + "\n\n"
                   + "The file is attached to this email.\n\n"
                   + "Please check the file status.\n\n"
                   + "Thank you very much.\n\n"
                   + "Regards\n"
                   + "TMS System Administrator";

            List<string> fileAttachments = new List<string>();
            fileAttachments.Add(Beginnings.LOG_FILE_PATH);
            this.SendMail(message, this.processName + " log file", fileAttachments, closeLogWriter);
           
        }

        /// <summary>
        /// Sends an E-Mail.
        /// </summary>
        /// <param name="message">The message body.</param>
        /// <param name="subject">The subject of the email.</param>
        /// <param name="fileAttachments">The list of file attachments.</param>
        public void SendMail(string message, string subject, List<string> fileAttachments, bool closeLogWriter)
        {
            try
            {
                MailAddress from = new MailAddress(this.localFtpParameters.SystemEmail);
                MailAddress to = new MailAddress(this.localFtpParameters.SystemEmail);

                MailMessage mail = new MailMessage(from, to);
                mail.Subject = this.localFtpParameters.HostServer.ToUpper () + " " + subject;
                mail.Body = message;
                mail.BodyEncoding = System.Text.Encoding.UTF8;

                foreach (string fileName in fileAttachments)
                {
                    Attachment myAttachment = new Attachment(fileName);
                    mail.Attachments.Add(myAttachment);
                }

                try
                {
                    SmtpClient mailClient = new SmtpClient();
                    mailClient.Host = this.localFtpParameters.SMTP;
                    mailClient.UseDefaultCredentials = true;
                    mailClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;

                    mailClient.Send(mail);

                    if (closeLogWriter)
                    {
                        string strFileName = Path.GetFileNameWithoutExtension(Beginnings.LOG_FILE_PATH) + "_Final.txt";
                        Environment.SetEnvironmentVariable("FILENAME", strFileName, EnvironmentVariableTarget.Process);
                        Logger.Reset();

                        // Create a local log file confirming that the log was sent
                        Logger.Write(string.Format("Main: emailed '{0}' to {1} at: {2}.", subject, this.localFtpParameters.SystemEmail, DateTime.Now), "General", (int)TraceEventType.Information);
                    }
                }
                catch (Exception smtpEx)
                {
                    Logger.Write("Main: Failed to send email file " + smtpEx.Message, "General", (int)TraceEventType.Critical);
                }
            }
            catch (Exception emailEx)
            {
                Logger.Write("Main: Failed to send email file " + emailEx.Message, "General", (int)TraceEventType.Critical);
            }
        }

        #endregion

    }
}
