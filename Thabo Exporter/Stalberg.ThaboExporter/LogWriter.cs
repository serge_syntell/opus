using System;
using System.IO;

namespace Stalberg.TMS_TPExInt
{
    /// <summary>
    /// Contains methods to create and write a log file
    /// </summary>
    internal class LogWriter : IDisposable
    {
        // Fields
        private StreamWriter writer;
        private string logFileName;

        // Constants
        const string LOG_FILENAME = "log";

        /// <summary>
        /// Initializes a new instance of the <see cref="LogWriter"/> class.
        /// </summary>
        public LogWriter()
            : this(string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogWriter"/> class.
        /// </summary>
        /// <param name="isFinal">if set to <c>true</c> crates a final log message.</param>
        public LogWriter(string fileName)
        {
            string tempFileName;
            if (fileName.Length > 0)
                tempFileName = Path.GetFileNameWithoutExtension(fileName) + "_Final.txt";
            else
                tempFileName = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".txt";

            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetCallingAssembly().Location);
            DirectoryInfo directory = new DirectoryInfo(Path.Combine(path, LOG_FILENAME));
            if (!directory.Exists)
                directory.Create();

            FileInfo file = new FileInfo(Path.Combine(directory.FullName, tempFileName));
            this.writer = new StreamWriter(file.Open(FileMode.OpenOrCreate));
            this.logFileName = file.FullName;
            directory = null;
        }

        /// <summary>
        /// Gets the name of the log file.
        /// </summary>
        /// <value>The name of the file.</value>
        public string FileName
        {
            get { return logFileName; }
        }

        /// <summary>
        /// Writes a line in the application log.
        /// </summary>
        /// <param name="message">The message to write to the log.</param>
        /// <param name="ex">The exception to append to the message in the log.</param>
        public void WriteLine(string message, Exception ex)
        {
            this.WriteLine("{0}\n{1}", message, ex.Message);
        }

        /// <summary>
        /// Writes a formatted message, substituting the values in the parameters arguments array for the placeholders in the message.
        /// </summary>
        /// <param name="message">The formattable message to write into the log.</param>
        /// <param name="parameters">The array of parameters to insert into the message.</param>
        public void WriteLine(string message, params object[] parameters)
        {
            this.WriteLine(string.Format(message, parameters));
        }

        /// <summary>
        /// Writes the supplied message line to the log file.
        /// </summary>
        /// <param name="message">The message to write into the log.</param>
        public void WriteLine(string message)
        {
            this.writer.WriteLine(message);
            this.writer.WriteLine();
            this.writer.Flush();
            Console.WriteLine(message);
            System.Diagnostics.Debug.WriteLine(message);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// Disposes the specified disposing.
        /// </summary>
        /// <param name="disposing">if set to <c>true</c> [disposing].</param>
        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.writer != null)
                {
                    this.writer.Close();
                    this.writer.Dispose();
                }
                this.writer = null;
            }
        }

        /// <summary>
        /// Closes this instance.
        /// </summary>
        public void Close()
        {
            this.writer.Close();
        }

    }
}
