﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Stalberg.ThaboExporter.Properties;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Stalberg.ThaboExporter")]
[assembly: AssemblyCompany("Stalberg & Associates")]
[assembly: AssemblyProduct("Stalberg.ThaboExporter")]
[assembly: AssemblyCopyright("Copyright © Stalberg & Associates 2007")]
[assembly: BuildTimeStamp]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c765166c-d83d-4cc1-b7ce-1c273f63917f")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("3.11.2.*")]
//[assembly: AssemblyFileVersion("0.3.0.0")]
