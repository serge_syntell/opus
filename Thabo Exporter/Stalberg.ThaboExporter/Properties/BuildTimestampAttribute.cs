using System;
using System.Reflection;

namespace Stalberg.ThaboExporter.Properties
{
    /// <summary>
    /// Represents a custom assembly attribute that contains the date and time that the assembly was built
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly)]
    public class BuildTimeStampAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Attribute"/> class.
        /// </summary>
        public BuildTimeStampAttribute()
        {
            this.TimeStamp = DateTime.Parse(Resources.Timestamp);
        }

        /// <summary>
        /// Gets or sets the build time stamp.
        /// </summary>
        /// <value>The time stamp.</value>
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// Gets the time stamp from the entry assembly.
        /// </summary>
        /// <returns>The build <see cref="DateTime"/> of the assembly, or <see langword="null"/>.</returns>
        public static DateTime? GetTimeStamp()
        {
            return GetTimeStamp(Assembly.GetEntryAssembly());
        }

        /// <summary>
        /// Gets the time stamp from the supplied assembly.
        /// </summary>
        /// <param name="assembly">The assembly to check for the <see cref="BuildTimeStampAttribute"/>.</param>
        /// <returns>The build <see cref="DateTime"/> of the assembly, or <see langword="null"/>.</returns>
        public static DateTime? GetTimeStamp(Assembly assembly)
        {
            object[] attributes = assembly.GetCustomAttributes(typeof(BuildTimeStampAttribute), false);
            if (attributes.Length == 0)
                return null;

            return ((BuildTimeStampAttribute)attributes[0]).TimeStamp;
        }

    }
}