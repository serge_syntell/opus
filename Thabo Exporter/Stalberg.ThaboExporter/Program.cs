using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Stalberg.ThaboExporter.Properties;
using Stalberg.TMS_TPExInt.Objects;
using System.Reflection;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Stalberg.Indaba;
using Stalberg.TMS;
using Stalberg.TMS_3P_Loader.Components;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.EntLib;

namespace Stalberg.TMS_TPExInt
{
    /// <summary>
    /// Represents a class that contains the Program's main entry point
    /// </summary>
    public class Beginnings
    {
        // Fields
        private static TPExIntParameters parameters;
        internal static ICommunicate indaba;

        // Constants
        internal const string DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
        internal static readonly string APP_NAME = "Thabo Exporter";
        internal static readonly string APP_TITLE = string.Format("{0} - Version {1}\nLast Updated {2}\nStarted at: {3}", APP_NAME, Assembly.GetExecutingAssembly().GetName().Version.ToString(2), ProjectLastUpdated.AARTOThaboExporter.ToShortDateString(), DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
        internal static string LOG_FILE_PATH;

        /// <summary>
        /// The Main entry point of the application, with the specified args.
        /// </summary>
        static void Main()
        {
            // Check Last updated Version            
            string errorMessage;

            // Catch any unhandled system wide crashes
            try
            {
                
                // Setup the Log Writer
                string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
                Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);

                // 2010/10/11 jerry add 
                if (!CheckVersionManager.CheckVersion(AartoProjectList.AARTOThaboExporter, out errorMessage))
                {
                    Console.WriteLine(errorMessage);
                    EntLibLogger.WriteLog(LogCategory.General, "", errorMessage);
                    return;
                }

                LoggingSettings settings = LoggingSettings.GetLoggingSettings(new SystemConfigurationSource());
                string strFileName = ((FlatFileTraceListenerData)(settings.TraceListeners.Get("Flat File Destination"))).FileName;

                LOG_FILE_PATH = System.IO.Path.GetFullPath(strFileName.Replace("%FILENAME%", strDate));

                // Check whether another instance is already running
                Singleton single = new Singleton("Stalberg.ThaboExporter");
                single.OtherProcessesToCheckFor(new string[] { "TMS_ViolationLoader", 
                    "TMS_CD_Loader", 
                    "TMS_3P_Loader", 
                    "Stalberg.Payfine.Extract",
                    "TMS_TPExInt",
                    "RoadblockDataExtract"});

                if (single.Check())
                {
                    Logger.Write(string.Format("An instance of {1} application is already running - processing aborted {0}.", DateTime.Now.ToString(), APP_NAME), "General", (int)TraceEventType.Error);
                    return;
                }

                Logger.Write(string.Format("{0}", APP_TITLE), "General", (int)TraceEventType.Information);
                Console.Title = APP_TITLE;

                // Read the SysParam file
                Beginnings.parameters = new TPExIntParameters();
                GetData data = new GetData();
                if (data.GetStartValues(Beginnings.parameters))
                {
                    Logger.Write("There was an error reading the SysParam file.", "General", (int)TraceEventType.Error);
                    return;
                }

                // Setup the Indaba Client
                Client.ApplicationName = APP_NAME;
                Client.ConnectionString = Beginnings.parameters.Thabo.ConnectionString;
                Client.Initialise();
                Beginnings.indaba = Client.Create();

                // Process exchange data for editing
                EasyPayProcessing processing = new EasyPayProcessing(Beginnings.parameters);
                Logger.Write(string.Format("Processing Transactions"), "General", (int)TraceEventType.Information);
                bool failed = processing.ProcessTransactions();
                if (failed)
                {
                    Logger.Write("Unable to complete processing transactions - PROCESSING ABORTED", "General", (int)TraceEventType.Critical);
                    return;
                }
                else
                {
                    Logger.Write("Finished Processing transactions.");
                }

                // Collect feedback files and import
                Logger.Write(string.Format("Importing Payment Feedback Files"), "General", (int)TraceEventType.Information);
                processing.ImportPaymentFeedback();
                Logger.Write("Finished Importing Payment Feedback.");

                //Process Response File 
                Logger.Write(string.Format("Processing Response Files"), "General", (int)TraceEventType.Information);
                ProcessResponseFile();

                // Cleanup
                processing.Dispose();

                Logger.Write(string.Format("Completed {0} at {1}", APP_NAME, DateTime.Now.ToString(DATE_FORMAT)), "General", (int)TraceEventType.Information);
            }
            catch (Exception ex)
            {
                Logger.Write(string.Format("General System failure: {0}\n{1}", ex, ex.StackTrace), "General", (int)TraceEventType.Critical);
            }
            finally
            {
                Client.Dispose();
            }
        }

        private static void ProcessResponseFile()
        {
            //091030 Get responsefiles
            IndabaFileInfo[] responsefiles = Beginnings.indaba.GetFileInformation(ResponseFile.RESPONSE_FILE_EXTENSION);

            if (responsefiles == null || responsefiles.Length == 0)
            {
                Logger.Write("No response file.", "General", (int)TraceEventType.Information);
                return;
            }

            foreach (IndabaFileInfo file in responsefiles)
            {
                try
                {
                    Logger.Write(string.Format("Retrieving data for File: {0}", file.Name), "General", (int)TraceEventType.Information);

                    Stream stream = Beginnings.indaba.Peek(file);
                    ResponseFile responseFile = ResponseFile.ReadDataFiles(stream, file.Name);

                    if (responseFile == null)
                    {
                        Logger.Write(string.Format("There was no data for File: {0}", file.Name), "General", (int)TraceEventType.Warning);
                        continue;
                    }
                    // Update the TMS database with the responseFile data
                    List<string> failedTickets = new List<string>();
                    responseFile.ProceessResponseFileData(Beginnings.parameters.ConnectionString, ref failedTickets, "Thabo Exporter");

                    if (failedTickets.Count == 0)
                    {
                        Beginnings.indaba.Dequeue(file);
                    }
                    else
                    {
                        Logger.Write(string.Format("Program detected that there was a problem updating the TMS database with response file data for LA: {0}", responseFile.Authority), "General", (int)TraceEventType.Warning);
                        continue;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Write(string.Format("Error processing response file {0} {1}\n{2}", file.Name, ex.Message, ex.StackTrace), "General", (int)TraceEventType.Critical);
                    continue;
                }
            }
        }
    }
}
