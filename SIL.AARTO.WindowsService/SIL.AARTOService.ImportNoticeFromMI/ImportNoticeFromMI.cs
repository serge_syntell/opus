﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.ImportNoticeFromMI
{
    public partial class ImportNoticeFromMI : ServiceHost
    {
        public ImportNoticeFromMI()
            :base("",new Guid("18E0DB71-680D-470D-8445-1CCA6BA24618"),new ImportNoticeFromMI_Service())
        {
            InitializeComponent();
        }
    }
}
