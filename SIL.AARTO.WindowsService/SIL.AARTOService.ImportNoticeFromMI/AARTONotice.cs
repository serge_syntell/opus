﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTO.BLL.Model;
using System.Data.SqlClient;
using System.Data;
using Stalberg.TMS.Data;
using Notice = SIL.AARTO.DAL.Entities.Notice;

namespace SIL.AARTOService.ImportNoticeFromMI
{
    class AARTONotice
    {
        public static Authority GetAuthorityByAutCode(string autcode,ref string errorMsg)
        {
            try
            {
                AuthorityService a_Service = new AuthorityService();
                return a_Service.GetByAutCode(autcode);
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "GetAuthorityByAutCode", "Can't find the Authority code:" + ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        public static Offence GetOffenceByCode(string offCode,ref string errorMsg)
        {
            try
            {
                OffenceService o_Service = new OffenceService();
                OffenceQuery query = new OffenceQuery();
                query.Append(OffenceColumn.OffCode, offCode);
                Offence offence = o_Service.Find(query as IFilterParameterCollection).ToList().First();
                return offence;
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "GetOffenceByCode", "Can't find the Offence code:" + ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        public static VehicleMake GetVehicleMakeByCode(string vmCode,ref string errorMsg)
        {
            try
            {
                VehicleMakeService v_Service = new VehicleMakeService();
                return v_Service.GetByVmCode(vmCode).ToList().FirstOrDefault();
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "GetVehicleMakeByCode", "Can't find the vehicle code:"+ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        public static VehicleType GetVehicleTypeByCode(string typeCode,ref string errorMsg)
        {
            try
            {
                VehicleTypeService v_Service = new VehicleTypeService();
                return v_Service.GetByVtCode(typeCode).ToList().FirstOrDefault();
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "GetVehicleTypeByCode", "Can't find the Vehicle type:" + ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        public static void DealNoticeMobileDetail(int notIntNo,SIL.AARTOService.ImportNoticeFromMI.Notices ImportNotice, ref string errorMsg)
        {
            try
            {
                NoticeMobileDetail noticeMobile = new NoticeMobileDetail();
                NoticeMobileDetailService notice_Service = new NoticeMobileDetailService();
                noticeMobile.NotIntNo = notIntNo;
                noticeMobile.Notes = ImportNotice.Notes;
                noticeMobile.NoticeBarCodeImage = (string.IsNullOrEmpty(ImportNotice.NoticeBarCodeImage) ? null : System.Text.Encoding.Default.GetBytes(ImportNotice.NoticeBarCodeImage));
                noticeMobile.NoticeUid = ImportNotice.NoticeUID;
                noticeMobile.OffenderSignature = (string.IsNullOrEmpty(ImportNotice.OffenderSignature) ? null : System.Text.Encoding.Default.GetBytes(ImportNotice.OffenderSignature));
                noticeMobile.OfficerSignature = (string.IsNullOrEmpty(ImportNotice.OfficerSignature) ? null : System.Text.Encoding.Default.GetBytes(ImportNotice.OfficerSignature));
                noticeMobile.WitnessName = ImportNotice.WitnessName;
                noticeMobile.WitnessComments = ImportNotice.WitnessComments;
                noticeMobile.OffenderComments = ImportNotice.OffenderDetails.OffenderComments;
                noticeMobile.CompanyAddress = ImportNotice.CompanyDetails.CompanyAddress;
                noticeMobile.CompanyName = ImportNotice.CompanyDetails.CompanyName;
                noticeMobile.CompanyRegDate = (ImportNotice.CompanyDetails.CompanyRegDate == null ? null : ImportNotice.CompanyDetails.CompanyRegDate);
                noticeMobile.CompanyRegNumber = ImportNotice.CompanyDetails.CompanyRegNumber;
                noticeMobile.CompanyType = ImportNotice.CompanyDetails.CompanyType;
                noticeMobile.Gps1 = ImportNotice.OffenceDetails.GPS1;
                noticeMobile.Gps2 = ImportNotice.OffenceDetails.GPS2;
                noticeMobile.LastUser = "MoblieOffencesImporter";
                notice_Service.Save(noticeMobile);
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "DealNoticeMobileDetail",errorMsg+ "---"+ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        public static void PushQueue(int notIntNo, int autIntNo, string accIdNumber, string connectStr, ref string errorMsg)
        {
            try
            {
                //Oscar20111219 add push queue
                //Oscar20111229 change & format the codes
                SIL.AARTO.DAL.Entities.TList<NoticeFrame> nfList = new NoticeFrameService().GetByNotIntNo(notIntNo);
                if (nfList != null && nfList.Count > 0)
                {
                    int frameIntNo = nfList[0].FrameIntNo;

                    Frame frame = new FrameService().GetByFrameIntNo(frameIntNo);
                    Notice notice = new NoticeService().GetByNotIntNo(notIntNo);
                    SIL.AARTO.DAL.Entities.Authority authority = new SIL.AARTO.DAL.Services.AuthorityService().GetByAutIntNo(autIntNo);

                    DateTime not1stPostDate = notice.NotPosted1stNoticeDate ?? DateTime.Now;
                    string autCode = authority.AutCode;

                    DateRuleInfo dateRule;
                    //DateRuleInfo dateRuleService = new DateRuleInfo();
                    int daysOfFrameNatis = 0,
                        daysOfGenerateSummons = 0;
                    //daysOfGenerate2ndNotice = 0;

                    //jerry 2011-12-22 add
                    dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "NotSummonsBeforeDate");
                    int daysOfSummonsServicePeriodExpired = dateRule.ADRNoOfDays;

                    //Jerry 2013-03-11 add
                    string aR_2500 = new AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(autIntNo, "2500").ARString.Trim();
                    //Henry 2013-03-27 add
                    AuthorityRuleInfo aR_5050 = new AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(autIntNo, "5050");
                    dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "NotPaymentDate");
                    int daysOffenceDateToPaymentDate = dateRule.ADRNoOfDays;
                    //dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotPosted1stNoticeDate", "NotIssue2ndNoticeDate");
                    //int daysPosted1stTo2ndNoticeDate = dateRule.ADRNoOfDays;
                    dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotIssue2ndNoticeDate", "Not2ndPaymentDate");
                    int daysIssue2ndNoticeTo2ndPaymentDate = dateRule.ADRNoOfDays;

                    bool pushFrameNatisQueue = false;
                    bool pushGenerateSummonsQueue = false;
                    bool pushGenerate2ndNoticeQueue = false;

                    if (!string.IsNullOrEmpty(frame.RegNo) && frame.RegNo != "0000000000")
                    {
                        dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "NotIssue1stNoticeDate");
                        daysOfFrameNatis = dateRule.ADRNoOfDays;

                        pushFrameNatisQueue = true;
                    }
                    if (notice.NoticeStatus == 255 && !string.IsNullOrEmpty(accIdNumber))
                    {
                        dateRule =new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotPosted1stNoticeDate", "NotIssueSummonsDate");
                        daysOfGenerateSummons = dateRule.ADRNoOfDays;

                        //dateRule = dateRuleService.GetByAutIntNoDtRstartDateDtRendDate(autIntNo, "NotPosted1stNoticeDate", "NotIssue2ndNoticeDate").FirstOrDefault();
                        //daysOfGenerate2ndNotice = dateRule.DtRnoOfDays;

                        pushGenerateSummonsQueue = true;
                        pushGenerate2ndNoticeQueue = true;

                    }

                    // Oscar 20120315 changed the Group
                    string[] group = new string[3];
                    group[0] = autCode.Trim();
                    //Jerry 2013-03-07 change
                    //group[1] = notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) ? "H" : "";
                    group[1] = (notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) && !notice.IsVideo) ? "H" : "";
                    group[2] = Convert.ToString(notice.NotCourtName);

                    //Jerry 2013-03-11 add for push 1st notice queue
                    string violationType = GetViolationType(connectStr, frameIntNo);

                    // Oscar 2013-06-17 added
                    var noAogDB = new NoAOGDB(connectStr);
                    var isNoAog = noAogDB.IsNoAOG(notIntNo, NoAOGQueryType.Notice);

                    QueueItemProcessor queProcessor = new QueueItemProcessor();
                    queProcessor.Send(
                        (pushFrameNatisQueue ?
                        new QueueItem()
                        {
                            Body = frameIntNo,
                            Group = autCode,
                            Priority = daysOfFrameNatis - ((TimeSpan)(DateTime.Now - frame.OffenceDate)).Days,
                            QueueType = ServiceQueueTypeList.Frame_Natis
                        } : null),
                        (pushGenerateSummonsQueue ?
                        new QueueItem()
                        {
                            Body = notIntNo,
                            //Group = autCode,
                            // Oscar 20120315 changed the Group
                            Group = string.Join("|", group),
                            //ActDate = not1stPostDate.AddDays(daysOfGenerateSummons), --Jerry 2013-03-11 change
                            ActDate = isNoAog ? noAogDB.GetNoAOGGenerateSummonsActionDate(autIntNo, notice.NotOffenceDate) : (aR_2500.Equals("Y", StringComparison.OrdinalIgnoreCase) ? notice.NotOffenceDate.AddDays(daysOfGenerateSummons) :
                                            notice.NotOffenceDate.AddDays(daysOffenceDateToPaymentDate + daysIssue2ndNoticeTo2ndPaymentDate + 4)),
                            Priority = notice.NotOffenceDate.Subtract(DateTime.Now).Days,   // 2013-09-06, Oscar added
                            //Jerry 2012-05-14 add last user
                            LastUser = "MoblieOffencesImporter",
                            QueueType = ServiceQueueTypeList.GenerateSummons
                        } : null),
                        //Henry 2013-03-27 add
                        (aR_5050.ARString.Trim().ToUpper() == "Y" && pushGenerateSummonsQueue ?
                        new QueueItem()
                        {
                            Body = notIntNo,
                            Group = string.Join("|", group),
                            ActDate = notice.NotOffenceDate.AddMonths(aR_5050.ARNumeric).AddDays(-5),
                            Priority = notice.NotOffenceDate.Subtract(DateTime.Now).Days,   // 2013-09-06, Oscar added
                            LastUser = "MoblieOffencesImporter",
                            QueueType = ServiceQueueTypeList.GenerateSummons
                        } : null),
                        //jerry 2011-12-22 push SummonsServicePeriodExpired
                        new QueueItem()
                        {
                            Body = notIntNo,
                            Group = autCode,
                            ActDate = notice.NotOffenceDate.AddDays(daysOfSummonsServicePeriodExpired),
                            QueueType = ServiceQueueTypeList.SummonsServicePeriodExpired
                        },
                        //Oscar 20111229 add
                        new QueueItem()
                        {
                            Body = notIntNo,
                            QueueType = ServiceQueueTypeList.SearchNameIDUpdate
                        }
                        //Oscar 20111229 add
                        //Jerry 2013-03-11 change it
                        //(pushGenerate2ndNoticeQueue ?
                        //new QueueItem()
                        //{
                        //    Body = notIntNo,
                        //    Group = autCode,
                        //    //ActDate = not1stPostDate.AddDays(daysOfGenerate2ndNotice),
                        //    ActDate = notice.NotOffenceDate.AddDays(daysOffenceDateToPaymentDate + 2),//Jerry 2013-03-11 add
                        //    QueueType = ServiceQueueTypeList.Generate2ndNotice
                        //} : null)
                    );

                    //Jerry 2013-03-12 move it from up to here
                    if (pushGenerate2ndNoticeQueue)
                    {
                        if (aR_2500.Equals("Y", StringComparison.OrdinalIgnoreCase))
                        {
                            // Push queue for 1st notice of HWO
                            queProcessor.Send(new QueueItem()
                            {
                                Body = notIntNo,
                                Group = string.Format("{0}|{1}|{2}", autCode.Trim(), violationType, "HWO"),
                                QueueType = ServiceQueueTypeList.Frame_Generate1stNotice,
                                LastUser = "MoblieOffencesImporter"
                            });
                        }
                        else
                        {
                            queProcessor.Send(new QueueItem()
                            {
                                Body = notIntNo,
                                Group = string.Format("{0}|{1}", autCode.Trim(), "HWO"),
                                ActDate = notice.NotOffenceDate.AddDays(daysOffenceDateToPaymentDate + 2),//Jerry 2013-03-06 add
                                QueueType = ServiceQueueTypeList.Generate2ndNotice
                            });
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "PushQueue", ex.ToString());
                throw new Exception(errorMsg);
            }
        }

        public static Offence GetTheOffence(string mainFlag, string offenceCode)
        {
            OffenceService o_Service = new OffenceService();
            OffenceQuery query = new OffenceQuery();
            if (mainFlag.ToLower() == "y")
                query.Append(OffenceColumn.OffCode, offenceCode);
            else
                query.Append(OffenceColumn.OffCodeAlternate, offenceCode);
            return o_Service.Find(query).ToList().FirstOrDefault();
        }

        public static string GetViolationType(string connString, int frameIntNo)
        {
            string violationType = string.Empty;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand("GetViolationTypeByFrameIntNo", conn) { CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new SqlParameter("@FrameIntNo", frameIntNo) { DbType = DbType.Int32 });
                        if (conn.State != ConnectionState.Open)
                            conn.Open();
                        object value = cmd.ExecuteScalar();
                        if (value != null)
                            violationType = value.ToString().Trim();
                        return violationType;
                    }
                }
                catch (Exception ex)
                {
                    conn.Close();
                    throw ex;
                }
            }
        }

    }
}
