﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTOService.ImportNoticeFromMI.Utility;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.MobileInterface.DAL.Entities;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using Stalberg.TMS.Data.Util;
using mobileEntities = SIL.MobileInterface.DAL.Entities;
using mobileService = SIL.MobileInterface.DAL.Services;

namespace SIL.AARTOService.ImportNoticeFromMI.S341Mobile
{
    public class S341MobileData : MobileData
    {
        #region variable
        SIL.MobileInterface.DAL.Entities.DocumentStatusList docuemntStatus;
        MobileXMLData mobileXMLData = new MobileXMLData();

        ImageFileServer imageFileServer = new ImageFileServer();
        ImageFileServerService imageFileServerService = new ImageFileServerService();
        IList<ImageFileServer> ImageFileServerList;

        NoticeService _noticeService = new NoticeService();
        Notice noticeEntity = new Notice();
        #endregion

        #region validation

        //2014-08-07 Heidi added for import mobile s56(5303)
        public override List<int> ValidatingMobileDataOfficerError(Notices notices)
        {
            List<int> errorlist = new List<int>();

            //Jake 2015-03-27 commeit out check ID number for S341 mobile
            //ID Number
            //if (!CheckOffenderIDNumberOnlyCheckIsNull(notices.OffenderDetails))
            //{
            //    errorlist.Add((int)AartoOfficerErrorList.NoIDNumberOnSection56Document);
            //}

            //valid RegNO
            CheckVehicleDetailForS341(errorlist, notices.VehicleDetails);

            //Valid Officer Code
            if (!CheckMobileGeneralValidationByType("OfficerCode", notices))
            {
                errorlist.Add((int)AartoOfficerErrorList.NoOfficerDetails);
            }

            //Valid Officer Code
            if (!CheckOfficer(notices.OfficerNumber))
            {
                errorlist.Add((int)AartoOfficerErrorList.NoOfficerDetails);
            }

            //valid PlaceOfOffence requird,offenceDate
            CheckOffenceDetailForS341(errorlist, notices.OffenceDetails);

            //valid courtno
            if (!CheckCourtDetail(notices.CourtDetails))
            {
                errorlist.Add((int)AartoOfficerErrorList.IncorrectOrNoCourtDetails);
            }

            //Jake 2015-03-27 S341:if OffenderAge entered and not equal 0 , then validate
            //Offender age must be numeric
            if (notices.OffenderDetails != null
                && !String.IsNullOrEmpty(notices.OffenderDetails.OffenderAge)
                && !notices.OffenderDetails.OffenderAge.Equals("0"))
            {
                if (!CheckOffenderAge(notices))
                {
                    errorlist.Add((int)AartoOfficerErrorList.Other);
                }
            }


            //Charge(1/2/3) Amount ,isByLaw//chargeDropDownList(Description Format)
            //charge1Amount,charge2Amount,charge3Amount against the database fine amount
            //Alternate charge
            CheckChargeDetailForS341(errorlist, notices);

            //RegNo Address1 Surname at the same time
            if (!CheckRegNoAddress1SurnameAtTheSameTime(notices))
            {
                errorlist.Add((int)AartoOfficerErrorList.NoAddressRegistration);
            }

            return errorlist;
        }

        public override List<RejectionReasonsColumn> ValidatingMobileData(Notices notices)
        {
            List<RejectionReasonsColumn> list = new List<RejectionReasonsColumn>();
            if (!S341NoticeNumberMatch(notices.DocumentType, notices))
            {
                list.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_03, "NoticeNumber"));
            }
            MobileValidationBase(list, notices);

            return list;
        }

        public void CheckChargeDetailForS341(List<int> errorlist, Notices notices)
        {
            OffenceDetails offenceDetails = notices.OffenceDetails;
            LocalAuthority localAuthority = notices.LocalAuthority;
            List<ChargeDetails> list = offenceDetails.ChargeDetails;
            decimal amount;
            Offence offence;
            int chargeCounter = 0;
            bool isNoAogflag = false;
            bool isByLaw = false;

            //Jake 2015-03-27 optimize code.
            //List<Offence> offenceList = new List<Offence>();
            OffenceService service = new OffenceService();

            //offenceList = service.GetAll().ToList();

            //three chargeOffenceCode can't the same
            if (checkChargeOffenceCodeSame(list))
            {
                errorlist.Add((int)AartoOfficerErrorList.NoOffenceDescription);
            }

            foreach (var detail in list)
            {
                offence = service.Find(String.Format("OffCode='{0}'", detail.ChargeOffenceCode.Trim())).FirstOrDefault();
                //offence = offenceList.Where(o => o.OffCode == detail.ChargeOffenceCode.Trim()).FirstOrDefault();
                if (offence != null)
                {
                    chargeCounter = chargeCounter + 1;
                    string NoAogStr = detail.IsNoAog == null ? "" : detail.IsNoAog.Trim().ToUpper();

                    if (decimal.TryParse(detail.ChargeAmount.ToString(), out amount))
                    {
                        if ((amount <= 0 || amount >= 10000)) //Heidi 2013-08-01 added for task 4992B
                        {
                            //0<Charge(1/2/3) amount < 10000
                            errorlist.Add((int)AartoOfficerErrorList.NoOffenceDescription);
                        }
                    }
                    else
                    {   //0 < Charge(1/2/3) amount < 10000
                        errorlist.Add((int)AartoOfficerErrorList.NoOffenceDescription);
                    }

                    if (offence.OffNoAog.ToUpper() != NoAogStr)
                    {
                        //NoAog against the database
                        errorlist.Add((int)AartoOfficerErrorList.NoAdmissionOfGuilt);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(NoAogStr) && NoAogStr.ToUpper() == "Y")
                        {
                            isNoAogflag = true;
                        }

                    }

                    if (!string.IsNullOrEmpty(NoAogStr) && NoAogStr.ToUpper() == "Y")//Heidi 2013-08-01 added for task 4992B
                    {
                        if (amount != 0)
                        {   //no AOG -->amount=0
                            errorlist.Add((int)AartoOfficerErrorList.NoAdmissionOfGuilt);
                        }
                    }

                    //if (offence.OffDescr != detail.ChargeDescription)
                    //{
                    //    //valid chargeDropdrowlist
                    //    errorlist.Add((int)AartoOfficerErrorList.NoOffenceDescription);
                    //}

                    if (!string.IsNullOrEmpty(detail.IsMain) && detail.IsMain.ToUpper() == "N")
                    {
                        if (!string.IsNullOrEmpty(offence.OffCode))
                        {
                            //List<Offence> AlternateoffenceList = offenceList.Where(a => a.OffCodeAlternate == offence.OffCode).ToList();
                            List<Offence> AlternateoffenceList = service.Find(String.Format("OffCodeAlternate='{0}'", offence.OffCode)).ToList();
                            if (AlternateoffenceList == null || (AlternateoffenceList != null && AlternateoffenceList.Count == 0))
                            {
                                //This alternate charge related to this main charge must contain a valid alternate offence code.
                                errorlist.Add((int)AartoOfficerErrorList.NoticeSummonsIncomplete);
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(detail.IsMain) && detail.IsMain.ToUpper() == "N")
                    {
                        if (!CheckAlternateCharge(list, detail, offence, service))
                        {
                            //The main charge related to this alternate change must be one of the other charges on the mobile notice.
                            errorlist.Add((int)AartoOfficerErrorList.NoticeSummonsIncomplete);
                        }

                    }

                    if (offence.OffIsByLaw)
                    {
                        isByLaw = true;
                    }
                }
                else
                {
                    //not found offence by ChargeOffenceCode for valid chargeDropdrowlist
                    errorlist.Add((int)AartoOfficerErrorList.NoOffenceDescription);
                }
            }

            //Heidi 2013-08-01 added for task 4992B

            if (isByLaw && notices.VehicleDetails != null)
            {
                notices.VehicleDetails.VehicleRegNumber = "XX000000";
            }

        }

        public void CheckVehicleDetailForS341(List<int> errorlist, VehicleDetails detail)
        {
            if (detail != null)
            {
                //Jake2014-02-21 comment out
                //if (!CheckVechileColour(detail.VehicleColourDescr))
                //{
                //    //2014-02-19 Heidi changed for the VehicleColour validation fails,the ImportNoticeFromMI_Service service can continue to import data.
                //    //eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_08, "VehicleColour"));
                //    detail.VehicleColourDescr ="";
                //}
                //if (!CheckVehicleLicExpDate(detail.VehicleLicExpDate))
                //{
                //    eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_09, "VehicleLicExpDate"));
                //}
                //Jake2014-02-21 comment out
                //if (!CheckVehicleMake(detail.VehicleMakeCode))
                //{
                //    //2014-02-19 Heidi changed for the VehicleMake validation fails,the ImportNoticeFromMI_Service service can continue to import data.
                //    //eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_10, "VehicleMake"));
                //    detail.VehicleMakeCode = "";
                //}
                //Jake2014-02-21 comment out
                //if (!CheckVehicleType(detail.VehicleTypeCode))
                //{
                //    //2014-02-19 Heidi changed for the VehicleType validation fails,the ImportNoticeFromMI_Service service can continue to import data.
                //    //eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_11, "VehicleType"));
                //    detail.VehicleTypeCode = "";
                //}
                if (!CheckVehicleRegNO(detail.VehicleRegNumber))
                {
                    //Heidi 2013-08-01 added for task 4992B
                    //eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_23, "VehicleRegNumber"));
                    errorlist.Add((int)AartoOfficerErrorList.NoRegistrationNumber);
                }

            }
        }

        public void CheckOffenceDetailForS341(List<int> errorlist, OffenceDetails detail)
        {
            if (detail != null)
            {
                if (!CheckObservationTime(detail.ObservationTime))
                {
                    errorlist.Add((int)AartoOfficerErrorList.IncorrectOrNoTime);
                }
                if (!CheckOffenceDate(detail.OffenceDate))
                {
                    errorlist.Add((int)AartoOfficerErrorList.IncorrectOrNoOffenceDate);
                }

                if (string.IsNullOrEmpty(detail.PlaceOfOffence))
                {
                    //place Description requird
                    errorlist.Add((int)AartoOfficerErrorList.NoAddressRegistration);
                }

            }
            else
            {
                errorlist.Add((int)AartoOfficerErrorList.NoOffenceDescription);
            }

        }

        public bool CheckRegNoAddress1SurnameAtTheSameTime(Notices notices)
        {
            bool validationSucceed = false;

            if (notices.OffenderDetails != null && notices.VehicleDetails != null)
            {
                var surname = notices.OffenderDetails.OffenderSurname;
                var address1 = notices.OffenderDetails.Address1;
                var regNo = notices.VehicleDetails.VehicleRegNumber;

                if ((surname != "" && address1 != "") || regNo != "")
                {
                    validationSucceed = true;
                }
            }

            return validationSucceed;
        }

        public bool S341NoticeNumberMatch(string noticeType, Notices notice)
        {
            bool flag = false;
            if (notice.LocalAuthority != null)
            {
                AuthorityService service = new AuthorityService();
                SIL.AARTO.DAL.Entities.Authority authority = service.GetByAutCode(notice.LocalAuthority.AutCode);
                if (authority != null)
                {
                    notice.LocalAuthority.AutIntNo = authority.AutIntNo;
                }
            }
            if (notice.LocalAuthority.AutIntNo > 0)
            {
                flag = ValidateNoticeNumberS341(notice.NoticeNumber, notice.LocalAuthority.AutIntNo);
            }
            return flag;
        }

        public bool ValidateNoticeNumberS341(string NoticeNumber, int autIntNo)
        {
            bool validate = false;

            string validateType = "cdv";
            if (NoticeNumber.IndexOf("/") > 0) { validateType = "cdv"; }
            else { validateType = "verhoeff"; }
            if (!Validation.GetInstance(connectStr).ValidateTicketNumber(NoticeNumber, validateType))
            {
                if (NoticeNumber.IndexOf("/") > 0)
                {
                    if (NoticeNumber.Split('/').Length == 4)
                    {
                        string prefix = NoticeNumber.Split('/')[0];
                        SIL.AARTO.DAL.Entities.TList<NoticePrefixHistory> noticePrefixList = new NoticePrefixHistoryService().GetByNprefixAndAuthIntNo(prefix, autIntNo);

                        NoticePrefixHistory noticePrefix = noticePrefixList.Where(m => m.NstIntNo == (int)NoticeStatisticalTypeList.M341).FirstOrDefault();
                        if (noticePrefix != null)
                        {
                            if (noticePrefix.NphType == "H")
                            {
                                validate = true;
                            }
                        }
                    }
                }
                else
                {
                    if (NoticeNumber.Split('-').Length == 4)
                    {
                        string prefix = NoticeNumber.Split('-')[0];
                        SIL.AARTO.DAL.Entities.TList<NoticePrefixHistory> noticePrefixList = new NoticePrefixHistoryService().GetByNprefixAndAuthIntNo(prefix, autIntNo);

                        NoticePrefixHistory noticePrefix = noticePrefixList.Where(m => m.NstIntNo == (int)NoticeStatisticalTypeList.M341).FirstOrDefault();
                        if (noticePrefix != null)
                        {
                            if (noticePrefix.NphType == "H")
                            {
                                validate = true;
                            }
                        }
                    }
                }
            }
            return validate;
        }

        #endregion

        #region Import

        public override bool DoImport(NoticeReceivedFromMobileDevice item, Notices notice, string officerError, bool isOfficerError, string batchBaseFileName, int delayHours, AARTOServiceBase AARTOBase, List<int> pfnList, ref int notIntNo, ref string errorMsg)
        {
            int autIntNo = 0;
            string conCode = string.Empty, filmNo = string.Empty, accIdNumber = string.Empty;
            //2014-08-29 Heidi added (5303)
            int ifsIntNo = 0;
            string imagePartPath = "";
            string autCode = notice.LocalAuthority.AutCode;
            string imageExtension = "";
            string OfficerSignatureImgType = "";
            string OffenderSignatureImgType = "";
            Byte[] officerSignatureImg = null, offenderSignatureImg = null;
            using (TransactionScope scope = ServiceUtility.CreateTransactionScope())
            {
                try
                {
                    conCode = ConfigurationManager.AppSettings["ContractorCode"].ToString();
                    filmNo = GetFilmNo();
                    accIdNumber = notice.OffenderDetails.OffenderIDNumber == null ? "" : notice.OffenderDetails.OffenderIDNumber.Trim().ToUpper();
                    autIntNo = notice.LocalAuthority.AutIntNo;

                    //2013-05-22 Heidi added for insert into LoSuIntNo to Notice Table
                    int loSuIntNo = GetLoSuIntNoByAutAndLocationSuburb(autIntNo, string.IsNullOrEmpty(notice.LocationSuburb) ? "" : notice.LocationSuburb.Trim());
                    //2014-10-30 Heidi changed(5376)
                    List<ChargeDetails> chargeList = notice.OffenceDetails.ChargeDetails.OrderByDescending(r => r.IsMain).ToList();
                    //List<ChargeDetails> chargeList = getTheDealList(notice.OffenceDetails.ChargeDetails);

                    //need deal the chargeList again
                    int count = chargeList.Count;
                    string chgOffenceCode = "";
                    string chgOffenceDescr = "";
                    float chgFineAmount = 0;
                    string chgAlternateCode1 = "";
                    string chgNoAog1 = "";
                    bool chgIsMain1 = false; //2014-10-30 Heidi added(5376)
                    string chgOffenceCode2 = "";
                    string chgOffenceDescr2 = "";
                    float chgFineAmount2 = 0;
                    string chgAlternateCode2 = "";
                    string chgNoAog2 = "";
                    bool chgIsMain2 = false;//2014-10-30 Heidi added(5376)
                    string chgOffenceCode3 = "";
                    string chgOffenceDescr3 = "";
                    float chgFineAmount3 = 0;
                    string chgAlternateCode3 = "";
                    string chgNoAog3 = "";
                    bool chgIsMain3 = false;//2014-10-30 Heidi added(5376)

                    string chgAlternateOffenceCode = "";

                    string vmCode = string.Empty;
                    string vmDescr = string.Empty;
                    string vtCode = string.Empty;
                    string vtDescr = string.Empty;
                    string vColor = string.Empty;


                    VehicleMake vehicleMake = null;
                    VehicleType vehicleType = null;

                    if (!string.IsNullOrEmpty(notice.VehicleDetails.VehicleMakeCode))
                        vehicleMake = AARTONotice.GetVehicleMakeByCode(notice.VehicleDetails.VehicleMakeCode, ref errorMsg);
                    if (!string.IsNullOrEmpty(notice.VehicleDetails.VehicleTypeCode))
                        vehicleType = AARTONotice.GetVehicleTypeByCode(notice.VehicleDetails.VehicleTypeCode, ref errorMsg);

                    //We are expecting the client to pass in the code however they will probably send us the description.
                    //We will try to do the lookup based on the code firstly and then treat it as the description if no row can be found after the lookup
                    //jake 2014-02-21 added
                    if (vehicleMake == null)
                    {
                        vmDescr = notice.VehicleDetails.VehicleMakeCode;
                        vmCode = "";
                    }
                    else
                    {
                        vmCode = vehicleMake.VmCode;
                        vmDescr = vehicleMake.VmDescr;
                    }

                    if (vehicleType == null)
                    {
                        vtDescr = notice.VehicleDetails.VehicleTypeCode;
                        vtCode = "";
                    }
                    else
                    {
                        vtCode = vehicleType.VtCode;
                        vtDescr = vehicleType.VtDescr;
                    }
                    //Notice table VehicleColor column length = nvarchar(3)
                    vColor = notice.VehicleDetails.VehicleColourDescr == null ? "" : notice.VehicleDetails.VehicleColourDescr;
                    if (vColor.Length > 3)
                    {
                        vColor = vColor.Substring(0, 3);
                    }
                    if (count > 0)
                        GetChargeDetail(chargeList[0], ref chgOffenceCode, ref chgOffenceDescr, ref chgFineAmount, ref chgAlternateOffenceCode, ref chgNoAog1, ref chgIsMain1);
                    if (count > 1)
                        GetChargeDetail(chargeList[1], ref chgOffenceCode2, ref chgOffenceDescr2, ref chgFineAmount2, ref chgAlternateOffenceCode, ref chgNoAog2, ref chgIsMain2);
                    if (count > 2)
                        GetChargeDetail(chargeList[2], ref chgOffenceCode3, ref chgOffenceDescr3, ref chgFineAmount3, ref chgAlternateOffenceCode, ref chgNoAog3, ref chgIsMain3);

                    DateTime notPaymentDate = default(DateTime);
                    DateTime? VehicleLicExpDate = null;
                    DateRuleInfo dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "NotPaymentDate");
                    DateTime.TryParse(notice.OffenceDetails.OffenceDate, out notPaymentDate);
                    notPaymentDate = notPaymentDate.AddDays(dateRule.ADRNoOfDays);

                    if (!string.IsNullOrEmpty(notice.VehicleDetails.VehicleLicExpDate))
                        VehicleLicExpDate = Convert.ToDateTime(notice.VehicleDetails.VehicleLicExpDate);

                    //2014-08-08 Heidi changed for import mobile s56(5303)
                    string notRegNo = notice.VehicleDetails.VehicleRegNumber == null ? "" : notice.VehicleDetails.VehicleRegNumber.Trim().ToUpper();
                    notRegNo = notRegNo == "XX000000" ? string.Empty : notRegNo; // 2013-04-28 add by Henry for by-law

                    //2014-08-08 Heidi added for import mobile s56(5303)
                    string drvAddressTelephone = (string.IsNullOrEmpty(notice.OffenderDetails.AddressTelephone) ? "" : notice.OffenderDetails.AddressTelephone);
                    string drvBusinessTelephone = (string.IsNullOrEmpty(notice.OffenderDetails.BusinessTelephone) ? "" : notice.OffenderDetails.BusinessTelephone);

                    //2014-08-29 Heidi added (5303)
                    #region ImageFileServer

                    ImageFileServerList = imageFileServerService.GetByAaSysFileTypeId((int)AartoSystemFileTypeList.MobileImage);
                    if (ImageFileServerList != null && ImageFileServerList.Count > 0)
                    {
                        ifsIntNo = ImageFileServerList[0].IfsIntNo;
                        imageFileServer = ImageFileServerList[0];
                    }
                    string dateTimeNowStr = DateTime.Now.ToString("yyyy-MM-dd").Replace("-", "\\");
                    imagePartPath = autCode + "\\" + dateTimeNowStr;

                    #endregion

                    #region check ImageFileServer

                    string imageMachineNameDirectory = "\\\\" + imageFileServer.ImageMachineName + "\\" + imageFileServer.ImageShareName + "\\" + imagePartPath;

                    try
                    {
                        if (!Directory.Exists(imageMachineNameDirectory))
                        {
                            Directory.CreateDirectory(imageMachineNameDirectory);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ResourceHelper.GetResource("ImageFileServerErrorWhenImportMobileS341", imageFileServer.ImageShareName, ifsIntNo));
                    }

                    officerSignatureImg = (string.IsNullOrEmpty(notice.OfficerSignature) ? null :
                                    Convert.FromBase64String(notice.OfficerSignature.Trim()));

                    offenderSignatureImg = (string.IsNullOrEmpty(notice.OffenderSignature) ? null :
                         Convert.FromBase64String(notice.OffenderSignature.Trim()));

                    if (officerSignatureImg != null)
                    {
                        try
                        {
                            using (MemoryStream ms = new MemoryStream(officerSignatureImg))
                            {
                                using (Bitmap b = new Bitmap(ms))
                                { }
                            }
                        }
                        catch (Exception ex)
                        {
                            officerSignatureImg = null;
                            //2014-10-30 Heidi changed(5376)
                            //throw new Exception(ResourceHelper.GetResource("CanNotCreateofficerSignatureImgWhenImportMobile", "S341", ex.Message));
                        }
                    }

                    if (offenderSignatureImg != null)
                    {
                        try
                        {
                            using (MemoryStream ms = new MemoryStream(offenderSignatureImg))
                            {
                                using (Bitmap b = new Bitmap(ms))
                                { }
                            }
                        }
                        catch (Exception ex)
                        {
                            offenderSignatureImg = null;
                            //2014-10-30 Heidi changed(5376)
                            //throw new Exception(ResourceHelper.GetResource("CanNotCreateoffenderSignatureImgWhenImportMobile", "S341", ex.Message));
                        }
                    }


                    #endregion

                    #region AARTODocumentImage
                    string AaNoticeDocNo = filmNo;
                    // int AaDocImgOrder = 1;
                    OfficerSignatureImgType = string.IsNullOrEmpty(notice.OfficerSignatureImgType) ? "jpg" : notice.OfficerSignatureImgType.Trim();
                    OffenderSignatureImgType = string.IsNullOrEmpty(notice.OffenderSignatureImgType) ? "jpg" : notice.OffenderSignatureImgType.Trim();
                    imageExtension = OfficerSignatureImgType + "|" + OffenderSignatureImgType;
                    #endregion

                    #region ImportMobileOffencesSection341
                    notIntNo = ImportMobileOffencesS341(
                       autIntNo,
                        notice.NoticeNumber,
                        notice.CourtDetails.CourtCode,//notCourtNo
                        Convert.ToDateTime(notice.OffenceDetails.OffenceDate),
                        notice.OffenceDetails.PlaceOfOffence,//notLocDescr
                        notRegNo, //notregno //Heidi 2013-08-01 changed for task 5050
                        "",//notVehicleRegisterNo // null - register is the internal database id from natis
                        //Jake 2014-02-21 modified
                        vmCode,
                        vmDescr,
                        vtCode,
                        vtDescr,
                        vColor,
                        //(notice.VehicleDetails.VehicleMakeCode == null ? "" : notice.VehicleDetails.VehicleMakeCode),//notVehicleMakeCode //code
                        //(vehicleMake == null ? "" : vehicleMake.VmDescr),//description
                        //(notice.VehicleDetails.VehicleType == null ? "" : notice.VehicleDetails.VehicleType),//notVehicleTypeCode
                        //(vehicleType == null ? "" : vehicleType.VtDescr),
                        //(notice.VehicleDetails.VehicleColourDescr == null ? "" : notice.VehicleDetails.VehicleColourDescr),
                        VehicleLicExpDate,
                        notice.OfficerNumberOnDuty,
                        notice.OfficerNameOnDuty,
                        notice.OfficerGroupCode,//notOfficerGroup
                        notice.OfficerNameOnDuty,//notDMSCaptureClerk //null
                        DateTime.Now,//notDMSCaptureDate
                        chgOffenceCode,
                        chgOffenceCode2,
                        chgOffenceCode3,
                        chgOffenceDescr,
                        chgOffenceDescr2,
                        chgOffenceDescr3,
                        chgFineAmount,
                        chgFineAmount2,
                        chgFineAmount3,
                        filmNo,//filmNo //mob as the prefix mobyyyymmdd
                        "",//frameNo // max+1 sequence
                        ifsIntNo,//ifsIntNo // no images//2014-08-29 Heidi changed for saving Officer and Offender Signature(5303) 
                        isOfficerError,//isOfficerError // no error false as always//2014-08-08 Heidi changed for import mobile s56(5303)
                        "",//docNo //null
                        0,//aaDocImgOrder // no images
                        imagePartPath,//imageFilePath // no images//2014-08-29 Heidi changed for saving Officer and Offender Signature(5303) 
                        imageExtension,//2014-08-29 Heidi added for saving Officer and Offender Signature(5303) 
                        lastUser,//lastUser 
                        conCode, //SI
                        officerError,//officerError //no error //2014-08-08 Heidi changed for import mobile s56(5303)
                        chgAlternateCode1,// isMain = 'n'
                        chgAlternateCode2,
                        chgAlternateCode3,
                        chgNoAog1,//chgNoAog1 // isNoAOG = 'y'
                        chgNoAog2,//chgNoAog2
                        chgNoAog3,//chgNoAog3
                        chgIsMain1, chgIsMain2, chgIsMain3,//2014-10-30 Heidi added(5376)
                        notice.OffenderDetails.OffenderSurname,//sName
                        notice.OffenderDetails.OffenderName,//fNamem
                        accIdNumber,//idNum
                        //2014-08-01 Heidi changed for when import mobile s56 changed XSD(5303)
                        (string.IsNullOrEmpty(notice.OffenderDetails.Address1) ? "" : notice.OffenderDetails.Address1),//poAddr1
                        (string.IsNullOrEmpty(notice.OffenderDetails.Address2) ? "" : notice.OffenderDetails.Address2),//notice.OffenderDetails.PostAddress2,//poAddr2
                        (string.IsNullOrEmpty(notice.OffenderDetails.Address3) ? "" : notice.OffenderDetails.Address3),
                        (string.IsNullOrEmpty(notice.OffenderDetails.Address4) ? "" : notice.OffenderDetails.Address4),
                        (string.IsNullOrEmpty(notice.OffenderDetails.Address5) ? "" : notice.OffenderDetails.Address5),
                        (string.IsNullOrEmpty(notice.OffenderDetails.BusinessAddress1) ? "" : notice.OffenderDetails.BusinessAddress1),//stAddr1
                        (string.IsNullOrEmpty(notice.OffenderDetails.BusinessAddress2) ? "" : notice.OffenderDetails.BusinessAddress2),//notice.OffenderDetails.StAdd2,//stAddr2
                        (string.IsNullOrEmpty(notice.OffenderDetails.BusinessAddress3) ? "" : notice.OffenderDetails.BusinessAddress3),
                        (string.IsNullOrEmpty(notice.OffenderDetails.BusinessAddress4) ? "" : notice.OffenderDetails.BusinessAddress4),
                        (string.IsNullOrEmpty(drvAddressTelephone) ? "" : drvAddressTelephone), //2014-10-30 Heidi checked null(5376)
                        (string.IsNullOrEmpty(drvBusinessTelephone) ? "" : drvBusinessTelephone),
                        (string.IsNullOrEmpty(notice.OffenderDetails.AddressCode) ? "" : notice.OffenderDetails.AddressCode),
                        (string.IsNullOrEmpty(notice.OffenderDetails.BusinessAddressCode) ? "" : notice.OffenderDetails.BusinessAddressCode),
                        (string.IsNullOrEmpty(notice.OffenderDetails.OffenderAge) ? "0" : notice.OffenderDetails.OffenderAge),
                        (string.IsNullOrEmpty(notice.OffenderDetails.OffenderNationality) ? "" : notice.OffenderDetails.OffenderNationality),
                        (string.IsNullOrEmpty(notice.OffenderDetails.OffenderIDType) ? "" : notice.OffenderDetails.OffenderIDType),
                        loSuIntNo, notPaymentDate
                        );
                }
                catch (SqlException ex)
                {
                    scope.Dispose();
                    errorMsg = ResourceHelper.GetResource("MobileImportException", notice.DocumentType, ex.Message);
                    throw new Exception(errorMsg);
                }
                    #endregion

                if (notIntNo <= 0)
                {
                    errorMsg = ResourceHelper.GetResource("MobileImportErrorForNotice", notice.DocumentType, notice.NoticeNumber);
                    scope.Dispose();
                    return Deal341Error(notIntNo, ref errorMsg);
                }
                else
                {
                    #region Save Image
                    noticeEntity = _noticeService.GetByNotIntNo(notIntNo);
                    if (noticeEntity != null && imageFileServer != null)
                    {
                        string noticeNoStr = "";
                        string signatureImageDirectory = "";

                        noticeNoStr = noticeEntity.NotTicketNo.Replace("/", "");
                        signatureImageDirectory = "\\\\" + imageFileServer.ImageMachineName + "\\" + imageFileServer.ImageShareName + "\\" + imagePartPath + "\\" + noticeNoStr;

                        try
                        {
                            if (!Directory.Exists(signatureImageDirectory))
                            {
                                Directory.CreateDirectory(signatureImageDirectory);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ResourceHelper.GetResource("ImageFileServerErrorWhenImportMobileS341", imageFileServer.ImageShareName, ifsIntNo));
                        }

                        if (officerSignatureImg != null)
                        {

                            string officerSignatureImgPath = signatureImageDirectory + "\\" + noticeNoStr + "_TOSig." + OfficerSignatureImgType;
                            using (MemoryStream ms = new MemoryStream(officerSignatureImg))
                            {
                                using (Bitmap b = new Bitmap(ms))
                                {
                                    b.Save(officerSignatureImgPath);
                                }
                            }
                        }

                        if (offenderSignatureImg != null)
                        {
                            string offenderSignatureImgPath = signatureImageDirectory + "\\" + noticeNoStr + "_OffSig." + OffenderSignatureImgType;
                            using (MemoryStream ms = new MemoryStream(offenderSignatureImg))
                            {
                                using (Bitmap b = new Bitmap(ms))
                                {
                                    b.Save(offenderSignatureImgPath);
                                }
                            }
                        }
                    }
                    #endregion

                    #region deal notice mobile detail and push queue

                    try
                    {
                        errorMsg = ResourceHelper.GetResource("ImportOfS341BatchNotice", filmNo, notice.NoticeNumber);
                        AARTONotice.DealNoticeMobileDetail(notIntNo, notice, ref errorMsg);
                        AARTONotice.PushQueue(notIntNo, autIntNo, accIdNumber, connectStr, ref errorMsg);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.ToString());
                    }

                    #endregion

                    #region update NoticeReceivedFromMobileDevice status

                    if (isOfficerError)
                    {
                        docuemntStatus = SIL.MobileInterface.DAL.Entities.DocumentStatusList.Import_OfficerError;
                    }
                    else
                    {
                        docuemntStatus = SIL.MobileInterface.DAL.Entities.DocumentStatusList.Import_Success;
                    }
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("MobileImportSuccessful", notice.DocumentType, notice.NoticeNumber), LogType.Info);
                    mobileXMLData.UpdateReceived(item, docuemntStatus, notice.DocumentType, lastUser);

                    #endregion

                    scope.Complete();
                }
                return true;
            }
        }
        public bool Deal341Error(int notIntNo, ref string errorMsg)
        {
            if (notIntNo == -1)
                errorMsg = String.Format("{0}:{1}", errorMsg, "creating Notice");
            else if (notIntNo == -2)
                errorMsg = String.Format("{0}:{1}", errorMsg, "creating Charge");
            else if (notIntNo == -3)
                errorMsg = String.Format("{0}:{1}", errorMsg, "creating Film");
            else if (notIntNo == -4)
                errorMsg = String.Format("{0}:{1}", errorMsg, "creating Frame");
            else if (notIntNo == -5)
                errorMsg = String.Format("{0}:{1}", errorMsg, "handling Notice_Frame");
            else if (notIntNo == -6)
                errorMsg = String.Format("{0}:{1}", errorMsg, "creating AartoDocument and AartoDocumentImage");
            else if (notIntNo == -9)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to AARTODocumentImage");
            else if (notIntNo == -16)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to AARTONoticeDocument");
            else if (notIntNo == -30)
                errorMsg = String.Format("{0}:{1}", errorMsg, "handling offier errors");
            else if (notIntNo == -50)
                errorMsg = String.Format("{0}:{1}", errorMsg, "creating Driver");
            else if (notIntNo == -51)
                errorMsg = String.Format("{0}:{1}", errorMsg, "creating FrameDriver");
            //Added 2010-10-13  ,update status = ImportedFromDMS in AartobmDocument
            else if (notIntNo == -16)
                errorMsg = String.Format("{0}:{1}", errorMsg, "updating AaBmStatusIs in AartoBMDocument");
            else if (notIntNo == -103 || notIntNo == -104)
                errorMsg = String.Format("{0}:{1}", errorMsg, "handling Search tables");
            else if (notIntNo == -150)
                errorMsg = String.Format("{0}: This notice has been fully paid,NotIntNo:{1} ", errorMsg, notIntNo.ToString());
            else
                errorMsg = String.Format("{0}: Unexpected error code: {1} ", errorMsg, notIntNo.ToString());
            return false;
        }

        //2014-08-07 Heidi changed @DrvPoAddr param name,because changed XSD when Improt mobile s56(5303)
        public int ImportMobileOffencesS341(int autIntNo, string notTicketNo,
            string notCourtNo, DateTime offenceDate, string notLocDescr, string notRegNo, string notVehicleRegisterNo,
            string notVehicleMakeCode, string notVehicleMake, string notVehicleTypeCode, string notVehicleType, string notVehicleColour,
            DateTime? notVehicleLicenceExpiry, string notOfficerNo, string notOfficerSname, string notOfficerGroup,//17
            string notDMSCaptureClerk, DateTime? notDMSCaptureDate,
            string chgOffenceCode, string chgOffenceCode2, string chgOffenceCode3, string chgOffenceDescr, string chgOffenceDescr2, string chgOffenceDescr3, float chgFineAmount, float chgFineAmount2, float chgFineAmount3,

            string filmNo,
            string frameNo, int ifsIntNo, bool isOfficerError,
             string docNo, int aaDocImgOrder, string imageFilePath, string imageExtension, string lastUser, string conCode, string officerError,
            string chgAlternateCode1, string chgAlternateCode2, string chgAlternateCode3, string chgNoAog1, string chgNoAog2, string chgNoAog3,
            bool chgIsMain1, bool chgIsMain2, bool chgIsMain3,//2014-10-30 Heidi added(5376)
            string sName, string fNamem, string idNum, string drvAddress1, string drvAddress2, string drvAddress3, string drvAddress4, string drvAddress5,
            string drvBusinessAddress1, string drvBusinessAddress2, string drvBusinessAddress3, string drvBusinessAddress4, string drvTelHome, string drvTelWork, string drvAddressCode, string drvBusinessAddressCode, string age, string Nationality, string DrvIDType,
            int loSuIntNo,
            DateTime notPaymentDate = default(DateTime))
        {
            SqlConnection myConnection = new SqlConnection(connectStr);
            //SqlTransaction tran = myConnection.BeginTransaction();
            SqlCommand myCommand = new SqlCommand("ImportMobileOffencesSection341", myConnection);

            myCommand.CommandType = CommandType.StoredProcedure;

            #region Notice
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterNotTicketNo = new SqlParameter("@NotTicketNo", SqlDbType.VarChar, 50);
            parameterNotTicketNo.Value = notTicketNo;
            myCommand.Parameters.Add(parameterNotTicketNo);

            SqlParameter parameterNotCourtNo = new SqlParameter("@NotCourtNo", SqlDbType.VarChar, 6);
            parameterNotCourtNo.Value = notCourtNo;
            myCommand.Parameters.Add(parameterNotCourtNo);

            //SqlParameter parameterNotCourtName = new SqlParameter("@NotCourtName", SqlDbType.VarChar, 30);
            //parameterNotCourtName.Value = notCourtName;
            //myCommand.Parameters.Add(parameterNotCourtName);

            SqlParameter parameterOffenceDate = new SqlParameter("@OffenceDate", SqlDbType.DateTime);
            parameterOffenceDate.Value = offenceDate;
            myCommand.Parameters.Add(parameterOffenceDate);

            //SqlParameter parameterNotLocCode = new SqlParameter("@NotLocCode", SqlDbType.VarChar, 50);
            //parameterNotLocCode.Value = notLocCode;
            //myCommand.Parameters.Add(parameterNotLocCode);

            SqlParameter parameterNotLocDescr = new SqlParameter("@NotLocDescr", SqlDbType.VarChar, 150);
            parameterNotLocDescr.Value = notLocDescr;
            myCommand.Parameters.Add(parameterNotLocDescr);

            SqlParameter parameterNotRegNo = new SqlParameter("@NotRegNo", SqlDbType.VarChar, 10);
            parameterNotRegNo.Value = notRegNo;
            myCommand.Parameters.Add(parameterNotRegNo);

            SqlParameter parameterNotVehicleRegisterNo = new SqlParameter("@NotVehicleRegisterNo", SqlDbType.VarChar, 10);
            parameterNotVehicleRegisterNo.Value = notVehicleRegisterNo;
            myCommand.Parameters.Add(parameterNotVehicleRegisterNo);

            SqlParameter parameterNotVehicleMakeCode = new SqlParameter("@NotVehicleMakeCode", SqlDbType.VarChar, 3);
            parameterNotVehicleMakeCode.Value = notVehicleMakeCode;
            myCommand.Parameters.Add(parameterNotVehicleMakeCode);

            SqlParameter parameterNotVehicleMake = new SqlParameter("@NotVehicleMake", SqlDbType.VarChar, 30);
            parameterNotVehicleMake.Value = notVehicleMake;
            myCommand.Parameters.Add(parameterNotVehicleMake);

            SqlParameter parameterNotVehicleTypeCode = new SqlParameter("@NotVehicleTypeCode", SqlDbType.VarChar, 3);
            parameterNotVehicleTypeCode.Value = notVehicleTypeCode;
            myCommand.Parameters.Add(parameterNotVehicleTypeCode);

            SqlParameter parameterNotVehicleType = new SqlParameter("@NotVehicleType", SqlDbType.VarChar, 30);
            parameterNotVehicleType.Value = notVehicleType;
            myCommand.Parameters.Add(parameterNotVehicleType);

            SqlParameter parameterNotVehicleColour = new SqlParameter("@NotVehicleColour", SqlDbType.VarChar, 3);
            parameterNotVehicleColour.Value = notVehicleColour;
            myCommand.Parameters.Add(parameterNotVehicleColour);

            SqlParameter parameterNotVehicleLicenceExpiry = new SqlParameter("@NotVehicleLicenceExpiry", SqlDbType.DateTime);
            if (notVehicleLicenceExpiry == null)
                parameterNotVehicleLicenceExpiry.Value = DBNull.Value;
            else
                parameterNotVehicleLicenceExpiry.Value = notVehicleLicenceExpiry;
            myCommand.Parameters.Add(parameterNotVehicleLicenceExpiry);

            SqlParameter parameterNotOfficerNo = new SqlParameter("@NotOfficerNo", SqlDbType.VarChar, 10);
            parameterNotOfficerNo.Value = notOfficerNo;
            myCommand.Parameters.Add(parameterNotOfficerNo);

            SqlParameter parameterNotOfficerSname = new SqlParameter("@NotOfficerSname", SqlDbType.VarChar, 35);
            parameterNotOfficerSname.Value = notOfficerSname;
            myCommand.Parameters.Add(parameterNotOfficerSname);

            SqlParameter parameterNotOfficerGroup = new SqlParameter("@NotOfficerGroup", SqlDbType.VarChar, 50);
            parameterNotOfficerGroup.Value = notOfficerGroup;
            myCommand.Parameters.Add(parameterNotOfficerGroup);

            SqlParameter parameterIsOfficerError = new SqlParameter("@IsOfficerError", SqlDbType.Bit);
            parameterIsOfficerError.Value = isOfficerError;
            myCommand.Parameters.Add(parameterIsOfficerError);

            SqlParameter parameterOfficerErrorStr = new SqlParameter("@OfficerErrorStr", SqlDbType.VarChar, 1000);
            parameterOfficerErrorStr.Value = officerError;
            myCommand.Parameters.Add(parameterOfficerErrorStr);

            SqlParameter parameterNotDMSCaptureClerk = new SqlParameter("@NotDMSCaptureCLerk", SqlDbType.VarChar, 25);
            parameterNotDMSCaptureClerk.Value = notDMSCaptureClerk;
            myCommand.Parameters.Add(parameterNotDMSCaptureClerk);

            SqlParameter parameterNotDMSCaptureDate = new SqlParameter("@NotDMSCaptureDate", SqlDbType.SmallDateTime);
            parameterNotDMSCaptureDate.Value = notDMSCaptureDate;
            myCommand.Parameters.Add(parameterNotDMSCaptureDate);

            //Heidi 2013-05-20 add
            SqlParameter parameterLoSuIntNo = new SqlParameter("@LoSuIntNo", SqlDbType.Int);
            parameterLoSuIntNo.Value = loSuIntNo;
            myCommand.Parameters.Add(parameterLoSuIntNo);


            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.VarChar, 50);
            parameterNotIntNo.Value = 0;
            parameterNotIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNotIntNo);

            //Jerry 2013-03-26 change it
            //Jerry 2013-02-07 add
            //if (aR_2500.Equals("Y", StringComparison.OrdinalIgnoreCase))
            //{
            SqlParameter parameterNotPaymentDate = new SqlParameter("@NotPaymentDate", SqlDbType.DateTime);
            parameterNotPaymentDate.Value = notPaymentDate;
            myCommand.Parameters.Add(parameterNotPaymentDate);
            //}

            #endregion

            #region Accuse

            SqlParameter parameterDrvIdNumber = new SqlParameter("@DrvIdNumber", SqlDbType.VarChar);
            parameterDrvIdNumber.Value = idNum;
            myCommand.Parameters.Add(parameterDrvIdNumber);

            SqlParameter parameterDrvSName = new SqlParameter("@DrvSName", SqlDbType.VarChar);
            parameterDrvSName.Value = sName;
            myCommand.Parameters.Add(parameterDrvSName);

            SqlParameter parameterDrvFName = new SqlParameter("@DrvFName", SqlDbType.VarChar);
            parameterDrvFName.Value = fNamem;
            myCommand.Parameters.Add(parameterDrvFName);

            //2014-08-07 Heidi changed @DrvPoAddr param name,because changed XSD when Improt mobile s56(5303)
            SqlParameter parameterDrvAddress1 = new SqlParameter("@DrvAddress1", SqlDbType.VarChar);
            parameterDrvAddress1.Value = drvAddress1;
            myCommand.Parameters.Add(parameterDrvAddress1);

            SqlParameter parameterDrvAddress2 = new SqlParameter("@DrvAddress2", SqlDbType.VarChar);
            parameterDrvAddress2.Value = drvAddress2;
            myCommand.Parameters.Add(parameterDrvAddress2);

            //seawen 2012-04-20
            SqlParameter parameterDrvAddress3 = new SqlParameter("@DrvAddress3", SqlDbType.VarChar);
            parameterDrvAddress3.Value = drvAddress3;
            myCommand.Parameters.Add(parameterDrvAddress3);

            SqlParameter parameterDrvAddress4 = new SqlParameter("@DrvAddress4", SqlDbType.VarChar);
            parameterDrvAddress4.Value = drvAddress4;
            myCommand.Parameters.Add(parameterDrvAddress4);

            SqlParameter parameterDrvAddress5 = new SqlParameter("@DrvAddress5", SqlDbType.VarChar);
            parameterDrvAddress5.Value = drvAddress5;
            myCommand.Parameters.Add(parameterDrvAddress5);

            SqlParameter parameterDrvAddressCode = new SqlParameter("@DrvAddressCode", SqlDbType.VarChar);
            parameterDrvAddressCode.Value = drvAddressCode;
            myCommand.Parameters.Add(parameterDrvAddressCode);

            SqlParameter parameterDrvBusinessAddress1 = new SqlParameter("@DrvBusinessAddress1", SqlDbType.VarChar);
            parameterDrvBusinessAddress1.Value = drvBusinessAddress1;
            myCommand.Parameters.Add(parameterDrvBusinessAddress1);

            SqlParameter parameteDrvBusinessAddress2 = new SqlParameter("@DrvBusinessAddress2", SqlDbType.VarChar);
            parameteDrvBusinessAddress2.Value = drvBusinessAddress2;
            myCommand.Parameters.Add(parameteDrvBusinessAddress2);

            //seawen 2012-04-20
            SqlParameter parameterDrvBusinessAddress3 = new SqlParameter("@DrvBusinessAddress3", SqlDbType.VarChar);
            parameterDrvBusinessAddress3.Value = drvBusinessAddress3;
            myCommand.Parameters.Add(parameterDrvBusinessAddress3);

            SqlParameter parameterDrvBusinessAddress4 = new SqlParameter("@DrvBusinessAddress4", SqlDbType.VarChar);
            parameterDrvBusinessAddress4.Value = drvBusinessAddress4;
            myCommand.Parameters.Add(parameterDrvBusinessAddress4);

            SqlParameter parameterDrvBusinessAddressCode = new SqlParameter("@DrvBusinessAddressCode", SqlDbType.VarChar);
            parameterDrvBusinessAddressCode.Value = drvBusinessAddressCode;
            myCommand.Parameters.Add(parameterDrvBusinessAddressCode);

            SqlParameter parameterAccTelHome = new SqlParameter("@TelNo1", SqlDbType.VarChar, 15);
            parameterAccTelHome.Value = drvTelHome;
            myCommand.Parameters.Add(parameterAccTelHome);

            SqlParameter parameterAccTelWork = new SqlParameter("@TelNo2", SqlDbType.VarChar, 15);
            parameterAccTelWork.Value = drvTelWork;
            myCommand.Parameters.Add(parameterAccTelWork);

            #endregion

            #region Charge
            SqlParameter parameterChgOffenceCode = new SqlParameter("@ChgOffenceCode", SqlDbType.VarChar, 15);
            parameterChgOffenceCode.Value = chgOffenceCode;
            myCommand.Parameters.Add(parameterChgOffenceCode);

            SqlParameter parameterChgOffenceCode2 = new SqlParameter("@ChgOffenceCode2", SqlDbType.VarChar, 15);
            parameterChgOffenceCode2.Value = chgOffenceCode2;
            myCommand.Parameters.Add(parameterChgOffenceCode2);

            SqlParameter parameterChgOffenceCode3 = new SqlParameter("@ChgOffenceCode3", SqlDbType.VarChar, 15);
            parameterChgOffenceCode3.Value = chgOffenceCode3;
            myCommand.Parameters.Add(parameterChgOffenceCode3);

            SqlParameter parameterChgOffenceDescr = new SqlParameter("@ChgOffenceDescr", SqlDbType.VarChar);
            parameterChgOffenceDescr.Value = chgOffenceDescr;
            myCommand.Parameters.Add(parameterChgOffenceDescr);

            SqlParameter parameterChgOffenceDescr2 = new SqlParameter("@ChgOffenceDescr2", SqlDbType.VarChar, 200);
            parameterChgOffenceDescr2.Value = chgOffenceDescr2;
            myCommand.Parameters.Add(parameterChgOffenceDescr2);

            SqlParameter parameterChgOffenceDescr3 = new SqlParameter("@ChgOffenceDescr3", SqlDbType.VarChar, 200);
            parameterChgOffenceDescr3.Value = chgOffenceDescr3;
            myCommand.Parameters.Add(parameterChgOffenceDescr3);


            SqlParameter parameterChgFineAmount = new SqlParameter("@ChgFineAmount", SqlDbType.Real);
            parameterChgFineAmount.Value = chgFineAmount;
            myCommand.Parameters.Add(parameterChgFineAmount);

            SqlParameter parameterChgFineAmount2 = new SqlParameter("@ChgFineAmount2", SqlDbType.Real);
            parameterChgFineAmount2.Value = chgFineAmount2;
            myCommand.Parameters.Add(parameterChgFineAmount2);

            SqlParameter parameterChgFineAmount3 = new SqlParameter("@ChgFineAmount3", SqlDbType.Real);
            parameterChgFineAmount3.Value = chgFineAmount3;
            myCommand.Parameters.Add(parameterChgFineAmount3);

            //Jake Added 20101108 AlternateCharge code 
            SqlParameter parameterChgAlternateCode1 = new SqlParameter("@ChgAlternateCode1", SqlDbType.VarChar, 20);
            parameterChgAlternateCode1.Value = chgAlternateCode1;
            myCommand.Parameters.Add(parameterChgAlternateCode1);

            SqlParameter parameterChgAlternateCode2 = new SqlParameter("@ChgAlternateCode2", SqlDbType.VarChar, 20);
            parameterChgAlternateCode2.Value = chgAlternateCode2;
            myCommand.Parameters.Add(parameterChgAlternateCode2);

            SqlParameter parameterChgAlternateCode3 = new SqlParameter("@ChgAlternateCode3", SqlDbType.VarChar, 20);
            parameterChgAlternateCode3.Value = chgAlternateCode3;
            myCommand.Parameters.Add(parameterChgAlternateCode3);

            SqlParameter parameterOffNoAOG1 = new SqlParameter("@OffNoAOG1", SqlDbType.Char, 1);
            parameterOffNoAOG1.Value = chgNoAog1;
            myCommand.Parameters.Add(parameterOffNoAOG1);

            SqlParameter parameterOffNoAOG2 = new SqlParameter("@OffNoAOG2", SqlDbType.Char, 1);
            parameterOffNoAOG2.Value = chgNoAog2;
            myCommand.Parameters.Add(parameterOffNoAOG2);

            SqlParameter parameterOffNoAOG3 = new SqlParameter("@OffNoAOG3", SqlDbType.Char, 1);
            parameterOffNoAOG3.Value = chgNoAog3;
            myCommand.Parameters.Add(parameterOffNoAOG3);

            //2014-10-30 Heidi added(5376)
            SqlParameter parameterOffIsMain1 = new SqlParameter("@OffIsMain1", SqlDbType.Bit, 1);
            parameterOffIsMain1.Value = chgIsMain1;
            myCommand.Parameters.Add(parameterOffIsMain1);
            //2014-10-30 Heidi added(5376)
            SqlParameter parameterOffIsMain2 = new SqlParameter("@OffIsMain2", SqlDbType.Bit, 1);
            parameterOffIsMain2.Value = chgIsMain2;
            myCommand.Parameters.Add(parameterOffIsMain2);
            //2014-10-30 Heidi added(5376)
            SqlParameter parameterOffIsMain3 = new SqlParameter("@OffIsMain3", SqlDbType.Bit, 1);
            parameterOffIsMain3.Value = chgIsMain3;
            myCommand.Parameters.Add(parameterOffIsMain3);


            #endregion

            #region Film & Frame
            SqlParameter parameterFilmNo = new SqlParameter("@FilmNo", SqlDbType.VarChar, 25);
            parameterFilmNo.Value = filmNo;
            myCommand.Parameters.Add(parameterFilmNo);

            SqlParameter parameterFrameNo = new SqlParameter("@FrameNo", SqlDbType.VarChar, 50);
            parameterFrameNo.Value = frameNo;
            myCommand.Parameters.Add(parameterFrameNo);

            //SqlParameter parameterFrameFilePath = new SqlParameter("@FrameFilePath", SqlDbType.VarChar, 255);
            //parameterFrameFilePath.Value = frameFilePath;
            //myCommand.Parameters.Add(parameterFrameFilePath);

            SqlParameter parameterIfsIntNo = new SqlParameter("@IfsIntNo", SqlDbType.Int);
            parameterIfsIntNo.Value = ifsIntNo;
            myCommand.Parameters.Add(parameterIfsIntNo);
            #endregion

            #region AartoDocument and AartoDocumentImage
            SqlParameter parameterDocNo = new SqlParameter("@DocNo", SqlDbType.VarChar, 20);
            parameterDocNo.Value = docNo;
            myCommand.Parameters.Add(parameterDocNo);

            SqlParameter parameterAaDocImgOrder = new SqlParameter("@AaDocImgOrder", SqlDbType.Int);
            parameterAaDocImgOrder.Value = aaDocImgOrder;
            myCommand.Parameters.Add(parameterAaDocImgOrder);

            SqlParameter parameterImageFilePath = new SqlParameter("@ImageFilePath", SqlDbType.VarChar, 100);
            parameterImageFilePath.Value = imageFilePath;
            myCommand.Parameters.Add(parameterImageFilePath);

            //2014-08-29 Heidi added for saving Officer and Offender Signature(5303) 
            SqlParameter parameterimageExtension = new SqlParameter("@ImageExtension", SqlDbType.VarChar, 50);
            parameterimageExtension.Value = imageExtension;
            myCommand.Parameters.Add(parameterimageExtension);

            #endregion

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterConCode = new SqlParameter("@ConCode", SqlDbType.VarChar, 10);
            parameterConCode.Value = conCode;
            myCommand.Parameters.Add(parameterConCode);
            //seawen 20120427
            SqlParameter parameterAge = new SqlParameter("@Age", SqlDbType.VarChar, 3);
            parameterAge.Value = age;
            myCommand.Parameters.Add(parameterAge);

            SqlParameter parameterNationality = new SqlParameter("@Nationality", SqlDbType.VarChar, 3);
            parameterNationality.Value = Nationality;
            myCommand.Parameters.Add(parameterNationality);

            SqlParameter parameterDrvIDType = new SqlParameter("@DrvIDType", SqlDbType.VarChar, 2);
            parameterDrvIDType.Value = DrvIDType;
            myCommand.Parameters.Add(parameterDrvIDType);

            SqlParameter parameterNSTCode = new SqlParameter("@NSTCode", SqlDbType.VarChar, 5);
            parameterNSTCode.Value = "M341";
            myCommand.Parameters.Add(parameterNSTCode);

            SqlParameter parameterPSTTName = new SqlParameter("@PSTTName", SqlDbType.VarChar, 50);
            parameterPSTTName.Value = "S341Mobile";
            myCommand.Parameters.Add(parameterPSTTName);

            SqlTransaction tran = null;

            try
            {
                myConnection.Open();
                //tran = myConnection.BeginTransaction();
                //myCommand.Transaction = tran;
                myCommand.ExecuteNonQuery();

                int notIntNo = Convert.ToInt32(myCommand.Parameters["@NotIntNo"].Value.ToString().Trim());

                return notIntNo;
                //Jake 2010-11-12 Add officer has been moved to storedprocedure
            }
            catch (Exception ex)
            {
                if (tran != null)
                    tran.Rollback();

                throw ex;
            }
            finally
            {
                myConnection.Dispose();
            }
        }


        #endregion

    }
}
