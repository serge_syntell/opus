﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using SIL.AARTOService.Resource;
using mobileEntities = SIL.MobileInterface.DAL.Entities;
using mobileService = SIL.MobileInterface.DAL.Services;
using SIL.AARTOService.ImportNoticeFromMI.Utility;
using SIL.AARTOService.ImportNoticeFromMI.S56Mobile;
using SIL.MobileInterface.DAL.Entities;
using SIL.MobileInterface.DAL.Services;
using SIL.AARTOService.ImportNoticeFromMI.S341Mobile;
using System.IO;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;


namespace SIL.AARTOService.ImportNoticeFromMI
{
    class ImportNoticeFromMI_Service : ClientService
    {
        public ImportNoticeFromMI_Service()
            : base("", "", new Guid("F835B981-CED8-461D-BEDD-01D6BCA3DDBD"))
        {
            this._serviceHelper = new AARTOServiceBase(this, false);
            AARTOBase.OnServiceStarting = OnServiceStarting;

            AARTOBase.OnServiceSleeping = () => Logger.Info("Sleeping...");
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }

        string connectStr;
        readonly string lastUser = "ImportNoticeFromMI";
        NoticeReceivedFromMobileDevice nrfmd = new mobileEntities.NoticeReceivedFromMobileDevice();
        NoticeReceivedFromMobileDeviceService nrfmds = new mobileService.NoticeReceivedFromMobileDeviceService();
        DocumentStatus documentStatus = new DocumentStatus();
        DocumentStatusService documentStatusService = new DocumentStatusService();
        SIL.MobileInterface.DAL.Entities.TList<DocumentStatus> documentStatusList = new mobileEntities.TList<DocumentStatus>();

        ImageFileServer imageFileServer = new ImageFileServer();
        ImageFileServerService imageFileServerService = new ImageFileServerService();
        IList<ImageFileServer> ImageFileServerList;

        bool isOfficerError = false;
        int batchSize = 100;
        int delayHours = 0;
        int batchSizeNoOfficeError=0;
        int batchSizeWithOfficeError = 0;
        DateTime stamp, lastStamp;
        DateTime stampWithOfficeError, lastStampWithOfficeError;
        string batchBaseFileName = "";
        string batchBaseFileNameWithOfficerError = "";
        List<int> pfnList = new List<int>();
        
        public override void PrepareWork()
        {
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            //connectStr = "Data Source=192.168.1.254;Initial Catalog=AARTO_4499A;User ID=Aarto_user;Password=aarto2010";
            ServiceUtility.InitializeNetTier(connectStr);
            //init mobile connect
            string MobileconnectStr = AARTOBase.GetConnectionString(SIL.ServiceQueueLibrary.DAL.Entities.ServiceConnectionNameList.MobileInterface,
                 SIL.ServiceQueueLibrary.DAL.Entities.ServiceConnectionTypeList.DB);
            ServiceUtility.InitializeNetTier(MobileconnectStr, "SIL.MobileInterface");
        }
        public override void InitialWork(ref QueueItem item)
        {
            documentStatusList = documentStatusService.GetAll();
        }
        public override void MainWork(ref List<SIL.QueueLibrary.QueueItem> queueList)
        {
            string errorMsg = string.Empty;
            try
            {
                MobileXMLData mobileXMLData = new MobileXMLData();
                int notIntNo = 0;
                //int count = mobileXMLData.NoticeReceivedFromMobileDeviceList.Count;
                Notices notices;
                MobileData mobileData;
                bool flag = false;
                S56MobileData s56MobileData = new S56MobileData();
                S341MobileData s341MobileData = new S341MobileData();
                s56MobileData.connectStr = connectStr; s341MobileData.connectStr = connectStr;
                List<RejectionReasonsColumn> validationList = new List<RejectionReasonsColumn>();
                List<int> officerErrorList = new List<int>();
                string officerErrorStr = "";
                bool flagValidation = false;
                SIL.MobileInterface.DAL.Entities.DocumentStatusList docuemntStatus;
                
                while (mobileXMLData.NoticeReceivedFromMobileDeviceList.Count > 0)
                {
                    foreach (var item in mobileXMLData.NoticeReceivedFromMobileDeviceList)
                    {
                        notIntNo = 0; errorMsg = string.Empty; officerErrorStr = string.Empty;
                        mobileXMLData.UpdateReceived(item, null, "", AARTOBase.LastUser);
                        try
                        {
                            notices = mobileXMLData.GetXMLNotices(item.Nrfmdxml);
                            //2014-10-30 Heidi changed for Compatible with 7 version(5376)
                            if (notices.XSDVersion == "8")
                            {
                                flagValidation = notices.IsComplete == 1 && notices.PrintedFlag;
                            }
                            else
                            {
                                flagValidation = notices.IsComplete == 1;
                            }

                            if (notices != null && flagValidation)
                            {
                                mobileData = MobileFactory.CreateDataInstance(notices.DocumentType, connectStr);
                                if (mobileData != null)
                                {
                                    #region CheckDocumentType
                                    if (!mobileData.CheckNoticeType(notices.DocumentType))
                                    {
                                        docuemntStatus = SIL.MobileInterface.DAL.Entities.DocumentStatusList.Fatal_IncorrectDocumentType;
                                        AARTOBase.LogProcessing(ResourceHelper.GetResource("MobileDataCouldNotBeImported",notices.DocumentType, notices.NoticeNumber, GetDSDescr((int)SIL.MobileInterface.DAL.Entities.DocumentStatusList.Fatal_IncorrectDocumentType)), LogType.Error, ServiceOption.Continue);
                                        mobileXMLData.UpdateReceived(item, docuemntStatus, notices.DocumentType, AARTOBase.LastUser);
                                        continue;
                                    }
                                    #endregion

                                    #region CheckAutCode
                                    if (!mobileData.CheckAutCode(notices.LocalAuthority))
                                    {
                                        docuemntStatus = SIL.MobileInterface.DAL.Entities.DocumentStatusList.Fatal_IncorrectAuthority;
                                        AARTOBase.LogProcessing(ResourceHelper.GetResource("MobileDataCouldNotBeImported",notices.DocumentType, notices.NoticeNumber, GetDSDescr((int)SIL.MobileInterface.DAL.Entities.DocumentStatusList.Fatal_IncorrectAuthority)), LogType.Error, ServiceOption.Continue);
                                        mobileXMLData.UpdateReceived(item, docuemntStatus, notices.DocumentType, AARTOBase.LastUser);
                                        continue;
                                    }
                                    #endregion

                                    #region check notice Number in pool
                                    if (!mobileData.CheckNoticeNumber(notices.NoticeNumber))
                                    {
                                        docuemntStatus = SIL.MobileInterface.DAL.Entities.DocumentStatusList.Fatal_IncorrectNoticeNumber;
                                        AARTOBase.LogProcessing(ResourceHelper.GetResource("MobileDataCouldNotBeImported", notices.DocumentType, notices.NoticeNumber, GetDSDescr((int)SIL.MobileInterface.DAL.Entities.DocumentStatusList.Fatal_IncorrectNoticeNumber)), LogType.Error, ServiceOption.Continue);
                                        mobileXMLData.UpdateReceived(item, docuemntStatus, notices.DocumentType, AARTOBase.LastUser);
                                        continue;
                                    }
                                    #endregion

                                    #region check duplicate notice Number
                                    if (!mobileData.CheckNoticeNumberDuplicate(notices.NoticeNumber))
                                    {
                                        docuemntStatus = SIL.MobileInterface.DAL.Entities.DocumentStatusList.Fatal_IncorrectNoticeNumber;
                                        AARTOBase.LogProcessing(ResourceHelper.GetResource("MobileDataCouldNotBeImported", notices.DocumentType, notices.NoticeNumber, GetDSDescr((int)SIL.MobileInterface.DAL.Entities.DocumentStatusList.Fatal_IncorrectNoticeNumber)), LogType.Error, ServiceOption.Continue);
                                        mobileXMLData.UpdateReceived(item, docuemntStatus, notices.DocumentType, AARTOBase.LastUser);
                                        continue;
                                    }
                                    #endregion

                                    #region check notice Number CDV

                                    //if (notices.DocumentType == DocumentTypeIdList.S341Mobile.ToString())
                                    //{
                                    //    if (!s341MobileData.S341NoticeNumberMatch(notices.DocumentType, notices))
                                    //    {
                                    //        docuemntStatus = SIL.MobileInterface.DAL.Entities.DocumentStatusList.Fatal_IncorrectNoticeNumber;
                                    //        AARTOBase.LogProcessing(ResourceHelper.GetResource("MobileDataCouldNotBeImported", notices.DocumentType, notices.NoticeNumber, GetDSDescr((int)SIL.MobileInterface.DAL.Entities.DocumentStatusList.Fatal_IncorrectNoticeNumber)), LogType.Error, ServiceOption.Continue);
                                    //        mobileXMLData.UpdateReceived(item, docuemntStatus, notices.DocumentType, AARTOBase.LastUser);
                                    //        continue;
                                    //    }
                                    //}
                                    //if (notices.DocumentType == DocumentTypeIdList.S56Mobile.ToString())
                                    //{
                                    //    if (!s56MobileData.S56NoticeNumberMatch(notices.DocumentType, notices))
                                    //    {
                                    //        docuemntStatus = SIL.MobileInterface.DAL.Entities.DocumentStatusList.Fatal_IncorrectNoticeNumber;
                                    //        AARTOBase.LogProcessing(ResourceHelper.GetResource("MobileDataCouldNotBeImported", notices.DocumentType, notices.NoticeNumber, GetDSDescr((int)SIL.MobileInterface.DAL.Entities.DocumentStatusList.Fatal_IncorrectNoticeNumber)), LogType.Error, ServiceOption.Continue);
                                    //        mobileXMLData.UpdateReceived(item, docuemntStatus, notices.DocumentType, AARTOBase.LastUser);
                                    //        continue;
                                    //    }
                                    //}

                                    
                                    #endregion

                                    #region Get Officer Error List
                                    officerErrorList = mobileData.ValidatingMobileDataOfficerError(notices);
                                    if (officerErrorList.Count > 0)
                                    {
                                        officerErrorList = RemoveDuplicate(officerErrorList);

                                        if (officerErrorList.Count > 1)
                                        {
                                            officerErrorStr = string.Join("~", officerErrorList.ToArray());
                                        }
                                        else
                                        {
                                            officerErrorStr = officerErrorList[0].ToString();
                                        }
                                    }
                                    isOfficerError = (officerErrorStr != null && officerErrorStr.Length > 0) ? true : false;
                                    #endregion

                                    if (notices.DocumentType == DocumentTypeIdList.S341Mobile.ToString())
                                    {
                                        #region s341
                                        #region old
                                            //validationList = mobileData.ValidatingMobileData(notices);
                                            //if (validationList.Count == 0)
                                            //{
                                            //    flag = mobileData.DoImport(item, notices, officerErrorStr,AARTOBase,ref notIntNo, ref errorMsg);
                                            //    if (flag)
                                            //    {
                                            //        Logger.Info(ResourceHelper.GetResource("MobileNoticeImportResult", "MainWork", errorMsg, DateTime.Now));
                                            //    }
                                            //    else
                                            //    {
                                            //        AARTOBase.LogProcessing(ResourceHelper.GetResource("MobileNoticeImportFailure", "MainWork", errorMsg, DateTime.Now), LogType.Error);
                                            //    }
                                            //}
                                            //else
                                            //{
                                            //    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("TheMobileImportDatasAreInvalid"), item.Nrfmdid), LogType.Error);
                                            //}
                                            //RecordMobileNoticeCorrect(item, validationList);
                                            #endregion
                                        flag = mobileData.DoImport(item, notices, officerErrorStr,isOfficerError,"",delayHours, AARTOBase,pfnList, ref notIntNo, ref errorMsg);

                                        if (!flag)
                                        {
                                            #region update NoticeReceivedFromMobileDevice status

                                            AARTOBase.LogProcessing(ResourceHelper.GetResource("MobileImportFailed", notices.DocumentType, notices.NoticeNumber, errorMsg), LogType.Error, ServiceOption.Continue);

                                            if (officerErrorStr.Length > 0)
                                            {
                                                docuemntStatus = SIL.MobileInterface.DAL.Entities.DocumentStatusList.Import_OfficerError;
                                                AARTOBase.LogProcessing(ResourceHelper.GetResource("MobileHasOfficerError", notices.DocumentType, notices.NoticeNumber), LogType.Error, ServiceOption.Continue);
                                                mobileXMLData.UpdateReceived(item, docuemntStatus, notices.DocumentType, AARTOBase.LastUser);
                                                continue;
                                            }
                                            else
                                            {
                                                mobileXMLData.UpdateReceived(item, null, notices.DocumentType, AARTOBase.LastUser);
                                                continue;
                                            }

                                            #endregion
                                        }
                                        #endregion
                                    }
                                    else if (notices.DocumentType == DocumentTypeIdList.S56Mobile.ToString())
                                    {
                                        #region s56

                                        if (isOfficerError)
                                        {
                                            if ((this.batchSize == this.batchSizeWithOfficeError))
                                            {
                                                batchBaseFileNameWithOfficerError = GetNewPrintFileName(isOfficerError);
                                            }
                                        }
                                        else
                                        {
                                            if ((this.batchSize == this.batchSizeNoOfficeError))
                                            {
                                                batchBaseFileName=GetNewPrintFileName(isOfficerError);
                                            }
                                        }


                                        flag = mobileData.DoImport(item, notices, officerErrorStr, isOfficerError, isOfficerError ? batchBaseFileNameWithOfficerError:batchBaseFileName, delayHours, AARTOBase, pfnList,ref notIntNo, ref errorMsg);

                                        if (!flag)
                                        {
                                            #region update NoticeReceivedFromMobileDevice status

                                            AARTOBase.LogProcessing(ResourceHelper.GetResource("MobileImportFailed", notices.DocumentType, notices.NoticeNumber, errorMsg), LogType.Error, ServiceOption.Continue);

                                            if (officerErrorStr.Length > 0)
                                            {
                                                docuemntStatus = SIL.MobileInterface.DAL.Entities.DocumentStatusList.Import_OfficerError;
                                                AARTOBase.LogProcessing(ResourceHelper.GetResource("MobileHasOfficerError", notices.DocumentType, notices.NoticeNumber), LogType.Error, ServiceOption.Continue);
                                                mobileXMLData.UpdateReceived(item, docuemntStatus, notices.DocumentType, AARTOBase.LastUser);
                                                continue;
                                            }
                                            else
                                            {
                                                mobileXMLData.UpdateReceived(item, null, notices.DocumentType, AARTOBase.LastUser);
                                                continue;
                                            }

                                            #endregion
                                        }
                                        else
                                        {
                                            if (isOfficerError)
                                            {
                                                this.batchSizeWithOfficeError--;
                                            }
                                            else
                                            {
                                                this.batchSizeNoOfficeError--;
                                            }
                                        }
                                        if (batchSizeWithOfficeError == 0)
                                        {
                                            batchSizeWithOfficeError = this.batchSize;
                                        }
                                        if (batchSizeNoOfficeError == 0)
                                        {
                                            batchSizeNoOfficeError = this.batchSize;
                                        }

                                        #endregion
                                    }
                                }
                                else
                                {
                                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NotFoundRelevantDocuments"), notices.DocumentType, notices.NoticeNumber), LogType.Error);
                                    mobileXMLData.UpdateReceived(item, null, notices.DocumentType, AARTOBase.LastUser);
                                    continue;
                                }
                            }
                            else
                            {
                                docuemntStatus = SIL.MobileInterface.DAL.Entities.DocumentStatusList.Cancelled;
                                AARTOBase.LogProcessing(ResourceHelper.GetResource("MobileDataCouldNotBeImported",notices.DocumentType, notices.NoticeNumber, GetDSDescr((int)SIL.MobileInterface.DAL.Entities.DocumentStatusList.Cancelled)), LogType.Error);
                                mobileXMLData.UpdateReceived(item, docuemntStatus, notices.DocumentType, AARTOBase.LastUser);
                                continue;
                            }
                        }
                        catch (Exception ex)
                        {
                            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("MobileImportError"), errorMsg, ex.Message, item.NrfmdNoticeNumber), LogType.Error);
                        }
                        
                        
                    }

                    mobileXMLData.NoticeReceivedFromMobileDeviceList.Clear();
                }
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMsg))
                {
                    errorMsg = ex.ToString();
                }
                AARTOBase.LogProcessing(ResourceHelper.GetResource("ErrorMainWork", "MainWork", errorMsg, DateTime.Now), LogType.Error);
            }
            finally
            {
                this.MustSleep = true;
            }
        }

        #region Service Starting & Sleeping

        private void OnServiceStarting()
        {
            SetServiceParameters();
            this.stamp = DateTime.Now;
            this.lastStamp = DateTime.Now;
            this.stampWithOfficeError = DateTime.Now;
            this.lastStampWithOfficeError = DateTime.Now;
            this.batchSizeWithOfficeError =this.batchSize;
            this.batchSizeNoOfficeError = this.batchSize;
            this.batchBaseFileName = "";
            this.batchBaseFileNameWithOfficerError = "";
            this.pfnList.Clear();

        }

        private void SetServiceParameters()
        {
            if (ServiceParameters != null)
            {
                #region check PrintFileNameBatchSize service param

                if (!ServiceParameters.ContainsKey("PrintFileNameBatchSize"))
                {
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("NotSetServicePara", "PrintFileNameBatchSize"), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }
                else
                {
                    if (string.IsNullOrEmpty(ServiceParameters["PrintFileNameBatchSize"]))
                    {
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("ParameterExistsButHasNoAssignedValue", "PrintFileNameBatchSize"), LogType.Error, ServiceOption.BreakAndStop);
                        return;
                    }
                    else
                    {
                        int batchSizeTemp = 0;
                        int.TryParse(ServiceParameters["PrintFileNameBatchSize"], out batchSizeTemp);
                        if (batchSizeTemp > 0)
                        {
                            this.batchSize = batchSizeTemp;
                        }
                        else
                        {
                            AARTOBase.LogProcessing(ResourceHelper.GetResource("ParameterExistsButHasNoAssignedValue", "PrintFileNameBatchSize"), LogType.Error, ServiceOption.BreakAndStop);
                            return;
                        }
                    }
                }

                #endregion

                #region check DelayActionDate_InHours service param

                if (!ServiceParameters.ContainsKey("DelayActionDate_InHours"))
                {
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("NotSetServicePara", "DelayActionDate_InHours"), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }
                else
                {
                    if (string.IsNullOrEmpty(ServiceParameters["DelayActionDate_InHours"]))
                    {
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("ParameterExistsButHasNoAssignedValue", "DelayActionDate_InHours"), LogType.Error, ServiceOption.BreakAndStop);
                        return;
                    }
                    else
                    {
                        int delayHoursTemp = 0;
                        int.TryParse(ServiceParameters["DelayActionDate_InHours"], out delayHoursTemp);
                        if (delayHoursTemp > 0)
                        {
                            this.delayHours = delayHoursTemp;
                        }
                        else
                        {
                            AARTOBase.LogProcessing(ResourceHelper.GetResource("ParameterExistsButHasNoAssignedValue", "DelayActionDate_InHours"), LogType.Error, ServiceOption.BreakAndStop);
                            return;
                        }
                    }
                }

                #endregion
            }
            else
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("NotFoundServiceParameters"), LogType.Error, ServiceOption.BreakAndStop);
                return;
            }

            #region ImageFileServer

            ImageFileServerList = imageFileServerService.GetByAaSysFileTypeId((int)AartoSystemFileTypeList.MobileImage);
            if (ImageFileServerList != null && ImageFileServerList.Count > 0)
            {
                imageFileServer = ImageFileServerList[0];

                if (imageFileServer != null)
                {

                    string imageMachineNameDirectory = "\\\\" + imageFileServer.ImageMachineName + "\\" + imageFileServer.ImageShareName;

                   
                    AARTOBase.CheckFolder(imageMachineNameDirectory);

                    #region check folder permissions
                   
                    //try
                    //{

                    //    string exportFullPath = imageMachineNameDirectory + "\\" + "test.txt";

                    //    //write the exported files
                    //    using (FileStream fs = new FileStream(exportFullPath, FileMode.Create, FileAccess.Write))
                    //    {
                    //        using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.ASCII))
                    //        {
                    //            sw.Write("test");
                    //        }
                    //    }
                    //    if (File.Exists(exportFullPath))
                    //    {
                    //        File.Delete(exportFullPath);
                    //    }

                    //}
                    //catch (Exception ex)
                    //{
                    //    AARTOBase.LogProcessing(ResourceHelper.GetResource("CheckingWriteFilePermissions", imageMachineNameDirectory), LogType.Error, ServiceOption.BreakAndStop);
                    //    return;
                    //}

                    #endregion
                }
                else
                {
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("CanNotFindImageFileServerData"), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }
            }
            else
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("CanNotFindImageFileServerData"), LogType.Error, ServiceOption.BreakAndStop);
                return;

            }

            #endregion
        }


        #endregion

        private string GetNewPrintFileName(bool isOfficerError)
        {
            if (isOfficerError)
            {
                this.stampWithOfficeError = DateTime.Now;
                if (this.stampWithOfficeError < this.lastStampWithOfficeError.AddSeconds(1))
                    this.stampWithOfficeError = this.lastStampWithOfficeError.AddSeconds(1);
                this.lastStampWithOfficeError = this.stampWithOfficeError;
                return string.Format("{0}_OfficerError", this.stampWithOfficeError.ToString("yyyy-MM-dd_HH-mm-ss"));
            }
            else
            {
                this.stamp = DateTime.Now;
                if (this.stamp < this.lastStamp.AddSeconds(1))
                    this.stamp = this.lastStamp.AddSeconds(1);
                this.lastStamp = this.stamp;
                return string.Format(this.stamp.ToString("yyyy-MM-dd_HH-mm-ss"));
            }
        }

        private string GetDSDescr(int documentStatusID)
        {
            string descr = "";

            var search = from c in documentStatusList where c.DocumentStatusId == documentStatusID select c;
            List<DocumentStatus> selectedDocumentStatuss = search.ToList();

            if (selectedDocumentStatuss != null && selectedDocumentStatuss.Count > 0)
            {
                descr = selectedDocumentStatuss[0].DsDescription;
            }

            return descr;
        }

        private List<int> RemoveDuplicate(List<int> officerErrorList)
        {

            List<int> officerList = new List<int>();

            foreach (var item in officerErrorList)
            {
                if (!officerList.Contains(item))
                {
                    officerList.Add(item);
                }

            }

            return officerList;

        }
        private void RecordMobileNoticeCorrect(mobileEntities.NoticeReceivedFromMobileDevice item, List<RejectionReasonsColumn> list)
        {
            mobileService.MobileNoticeCorrectionService service = new mobileService.MobileNoticeCorrectionService();
            DateTime now = DateTime.Now;
            mobileEntities.TList<mobileEntities.MobileNoticeCorrection> correctList = new mobileEntities.TList<mobileEntities.MobileNoticeCorrection>();
            mobileEntities.TList<mobileEntities.MobileNoticeCorrection> deleteList = service.GetByNrfmdid(item.Nrfmdid);
            service.Delete(deleteList);

            foreach (var error in list)
            {
                if (error != null)
                {
                    mobileEntities.MobileNoticeCorrection correction = new mobileEntities.MobileNoticeCorrection();
                    correction.MnrIntNo = (int)error.RejectionReason;
                    correction.RejectedColumnName = error.RejectionColumn; //Heidi 2013-08-01 added for task 4992B
                    correction.Nrfmdid = item.Nrfmdid;
                    correction.Createdate = now;
                    correction.LastUser = lastUser;
                    correctList.Add(correction);
                }
            }
            if (correctList.Count > 0)
                service.Save(correctList);
        }
    }
}
