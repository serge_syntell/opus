﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.ImportNoticeFromMI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor()
            {
                ServiceName = "SIL.AARTOService.ImportNoticeFromMI",
                DisplayName = "SIL.AARTOService.ImportNoticeFromMI",
                Description = "SIL AARTOService ImportNoticeFromMI"
            };
            ProgramRun.InitializeService(new ImportNoticeFromMI(), serviceDescriptor, args);
        }
    }
}
