﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.MobileInterface.DAL.Data;
using SIL.MobileInterface.DAL.Entities;
using SIL.MobileInterface.DAL.Services;
using System.Xml;
using System.Xml.Linq;
using SIL.AARTOService.Resource;

namespace SIL.AARTOService.ImportNoticeFromMI
{
    class MobileXMLData
    {
        public MobileXMLData()
        {

        }
        DocumentTypeIdService documentTypeIdService = new DocumentTypeIdService();
        NoticeReceivedFromMobileDeviceService received_Service = new NoticeReceivedFromMobileDeviceService();
        DocumentTypeId documentTypeId = new DocumentTypeId();
        private List<NoticeReceivedFromMobileDevice> _noticeReceivedFromMobileDeviceList;
        public List<NoticeReceivedFromMobileDevice> NoticeReceivedFromMobileDeviceList
        {
            get
            {
                if (_noticeReceivedFromMobileDeviceList == null || _noticeReceivedFromMobileDeviceList.Count == 0)
                    _noticeReceivedFromMobileDeviceList = GetReceivedNotice();
                return _noticeReceivedFromMobileDeviceList;
            }
        }
        public List<NoticeReceivedFromMobileDevice> GetReceivedNotice()
        {
            NoticeReceivedFromMobileDeviceService service = new NoticeReceivedFromMobileDeviceService();

            //NoticeReceivedFromMobileDeviceQuery query = new NoticeReceivedFromMobileDeviceQuery();
            //query.AppendIsNull(NoticeReceivedFromMobileDeviceColumn.NrfmdSentToAartoDate);
            //query.Append(NoticeReceivedFromMobileDeviceColumn.NrfmdBadNoticeFlag, "false");
            //List<NoticeReceivedFromMobileDevice> list = service.Find(query as IFilterParameterCollection).ToList();
            int totalCount = 0;
            string sql = " NrfmdSentToAartoDate IS NULL AND NrfmdBadNoticeFlag=0 ";
            List<NoticeReceivedFromMobileDevice> list = service.GetPaged(sql,"NRFMDReceivedFromDeviceDate ASC" ,0, 1, out totalCount).ToList();
            return list;
        }
        public Notices GetXMLNotices(string xml)
        {
            XElement xTree = XElement.Parse(xml);
            IEnumerable<XElement> elements = xTree.Elements();
            //first must check the xml is completed
            Notices notices = new Notices();
            XElement isCompleteNode = xTree.Elements().Where(el => el.Name == "IsComplete").First();
            if (isCompleteNode.Value == "1")
            {
                foreach (XElement node in elements)
                {
                    if (node.Name == "XSDVersion") //Heidi 2013-08-12 added for task 5050
                        notices.XSDVersion = node.Value;
                    if (node.Name == "DocumentType")
                        notices.DocumentType = node.Value;
                    else if (node.Name == "IsComplete")
                        notices.IsComplete = 1;
                    else if (node.Name == "Notes")
                        notices.Notes = node.Value;
                    else if (node.Name == "NoticeBarCodeImage")
                    {
                        if (notices.XSDVersion == "8")
                        {
                            //2014-09-10 Heidi changed because changed XSD when Improt mobile s56(5303)
                            if (node.Attribute("ImageType") != null)
                            {
                                notices.NoticeBarCodeImageType = node.Attribute("ImageType").Value;
                            }
                            if (node.Attribute("ImageValue") != null)
                            {
                                notices.NoticeBarCodeImage = node.Attribute("ImageValue").Value;
                            }
                        }
                        else
                        {
                            notices.NoticeBarCodeImage = node.Value;
                        }
                    }
                    else if (node.Name == "NoticeNumber")
                        notices.NoticeNumber = string.IsNullOrEmpty(node.Value) ? node.Value : node.Value.Trim();// Heidi 2014-06-11 Remove extra Spaces for fixing Performance issues(5299)
                    else if (node.Name == "NoticeUID")
                        notices.NoticeUID = node.Value;
                    else if (node.Name == "OffenderSignature")
                    {
                        if (notices.XSDVersion == "8")
                        {
                            //2014-09-10 Heidi changed because changed XSD when Improt mobile s56(5303)
                            if (node.Attribute("ImageType") != null)
                            {
                                notices.OffenderSignatureImgType = node.Attribute("ImageType").Value;
                            }
                            if (node.Attribute("ImageValue") != null)
                            {
                                notices.OffenderSignature = node.Attribute("ImageValue").Value;
                            }
                        }
                        else
                        {
                            notices.OffenderSignature = node.Value;
                        }
                    }
                    else if (node.Name == "OfficerGroupCode")
                        notices.OfficerGroupCode = node.Value;
                    else if (node.Name == "OfficerNameOnDuty")
                        notices.OfficerNameOnDuty = node.Value;
                    else if (node.Name == "OfficerNumber")
                        notices.OfficerNumber = string.IsNullOrEmpty(node.Value) ? node.Value : node.Value.Trim();
                    else if (node.Name == "OfficerNumberOnDuty")
                        notices.OfficerNumberOnDuty = string.IsNullOrEmpty(node.Value) ? "" : node.Value.Trim();
                    else if (node.Name == "OfficerSignature")
                    {
                        if (notices.XSDVersion == "8")
                        {
                            //2014-09-10 Heidi changed because changed XSD when Improt mobile s56(5303)
                            if (node.Attribute("ImageType") != null)
                            {
                                notices.OfficerSignatureImgType = node.Attribute("ImageType").Value;
                            }
                            if (node.Attribute("ImageValue") != null)
                            {
                                notices.OfficerSignature = node.Attribute("ImageValue").Value;
                            }
                        }
                        else
                        {
                            notices.OfficerSignature = node.Value;
                        }
                    }
                    else if (node.Name == "PrintedFlag")
                        notices.PrintedFlag = string.IsNullOrEmpty(node.Value) ? false : node.Value.Trim() == "1" ? true : false;
                    else if (node.Name == "WitnessComments")
                        notices.WitnessComments = node.Value;
                    else if (node.Name == "WitnessName")
                        notices.WitnessName = node.Value;
                    else if (node.Name == "LocationSuburb") //Heidi 2013-08-12 added for task 5050
                        notices.LocationSuburb = node.Value == null ? "" : node.Value.Trim();
                    else if (node.Name == "LocalAuthority")
                    {
                        notices.LocalAuthority = CreateAuthority(node.Elements());
                    }
                    else if (node.Name == "VehicleDetails")
                    {
                        notices.VehicleDetails = CreateVehicleDetails(node.Elements());
                    }
                    else if (node.Name == "OffenceDetails")
                    {
                        notices.OffenceDetails = CreateOffenceDetails(node.Elements());
                    }
                    else if (node.Name == "CompanyDetails")
                    {
                        notices.CompanyDetails = CreateCompanyDetails(node.Elements());
                    }
                    else if (node.Name == "CourtDetails")
                    {
                        notices.CourtDetails = CreateCourtDetails(node.Elements());
                    }
                    else if (node.Name == "OffenderDetails")
                    {
                        notices.OffenderDetails = CreateOffenderDetails(node.Elements());
                    }

                }
            }
            else
            {
                notices.IsComplete = 0;
            }
            return notices;
        }

        // 2013-08-02 add parameter lastUser by Henry
        // 2014-07-31 Heidi changed for import mobile s56(5303)
        /// <summary>
        ///  if docuemntStatus is null,don't update NoticeReceivedFromMobileDeviceService.docuemntStatus
        /// </summary>
        /// <param name="item"></param>
        /// <param name="docuemntStatus"></param>
        /// <param name="notIntNo"></param>
        /// <param name="lastUser"></param>
        public void UpdateReceived(NoticeReceivedFromMobileDevice item, SIL.MobileInterface.DAL.Entities.DocumentStatusList?docuemntStatus,string documentType, string lastUser)
        {
            string errorMsg = "";
            string documenttypestr = documentType == null ? "":documentType;
            if (documenttypestr == "")
            {
                if (item != null)
                {
                    if (item.DocumentType == (int)DocumentTypeIdList.S56Mobile)
                    {
                        documenttypestr = DocumentTypeIdList.S56Mobile.ToString();
                    }
                    if (item.DocumentType == (int)DocumentTypeIdList.S341Mobile)
                    {
                        documenttypestr = DocumentTypeIdList.S341Mobile.ToString();
                    }
                }
            }
            
            try
            {
                item.LastUser = lastUser;
                item.NrfmdSentToAartoDate = DateTime.Now;
                if (docuemntStatus != null)
                {
                    item.DocumentStatus =(int)docuemntStatus;
                }
                received_Service.Update(item);
            }
            catch (Exception ex)
            {
                errorMsg = ResourceHelper.GetResource("UpdateReceivedtException", documenttypestr,ex.Message,item.Nrfmdid);
                throw new Exception(errorMsg);
            }
        }

        // 2013-08-02 add parameter lastUser by Henry
        public void UpdateReceived(NoticeReceivedFromMobileDevice item, string documentType, string lastUser, ref string errorMsg)
        {
            try
            {
                item.NrfmdSentToAartoDate = DateTime.Now;
                item.LastUser = lastUser;
                received_Service.Update(item);
            }
            catch (Exception ex)
            {
                //errorMsg = string.Format("{0}:{1}", "UpdateReceivedState", errorMsg + "---" + ex.ToString());
                errorMsg = ResourceHelper.GetResource("UpdateReceivedSentToAartoDatetException", errorMsg + "---" + ex.ToString(),item.NrfmdNoticeNumber);
                throw new Exception(errorMsg);
            }
        }

        #region create detail table
        public LocalAuthority CreateAuthority(IEnumerable<XElement> elements)
        {
            LocalAuthority authority = new LocalAuthority();
            foreach (XElement element in elements)
            {
                if (element.Name == "AutCode")
                    authority.AutCode = element.Value;
                else if (element.Name == "AutName")
                    authority.AutName = element.Value;
                else if (element.Name == "AutNumber")
                    authority.AutNumber = element.Value;
            }
            return authority;
        }
        public VehicleDetails CreateVehicleDetails(IEnumerable<XElement> elements)
        {
            VehicleDetails vehicleDetails = new VehicleDetails();
            foreach (XElement element in elements)
            {
                if (element.Name == "VehicleColour")
                    vehicleDetails.VehicleColourDescr = element.Value;
                else if (element.Name == "VehicleLicExpDate")
                    vehicleDetails.VehicleLicExpDate = element.Value;
                else if (element.Name == "VehicleMake")
                    vehicleDetails.VehicleMakeCode = element.Value;
                else if (element.Name == "VehicleRegNumber")
                    vehicleDetails.VehicleRegNumber =string.IsNullOrEmpty(element.Value) ? "" : element.Value.Trim();
                else if (element.Name == "VehicleType")
                    vehicleDetails.VehicleTypeCode = element.Value;
                else if (element.Name == "IsTaxi")
                    vehicleDetails.IsTaxi = string.IsNullOrEmpty(element.Value) ? false : (element.Value.Trim()=="1"||element.Value.Trim().ToLower()=="true")?true:false;
            }
            return vehicleDetails;
        }
        public OffenceDetails CreateOffenceDetails(IEnumerable<XElement> elements)
        {
            OffenceDetails offenceDetails = new OffenceDetails();
            List<ChargeDetails> chargeDetailsList = new List<ChargeDetails>();
            foreach (XElement element in elements)
            {
                if (element.Name == "GPS1")
                    offenceDetails.GPS1 = string.IsNullOrEmpty(element.Value) ? "" : element.Value.Trim();
                else if (element.Name == "GPS2")
                    offenceDetails.GPS2 = string.IsNullOrEmpty(element.Value) ? "" : element.Value.Trim();
                else if (element.Name == "ObservationTime")
                    offenceDetails.ObservationTime = element.Value;
                else if (element.Name == "OffenceDate")
                    offenceDetails.OffenceDate = element.Value;
                else if (element.Name == "PlaceOfOffence")
                    offenceDetails.PlaceOfOffence = string.IsNullOrEmpty(element.Value) ? "":element.Value.Trim();
                //2014-07-31 Heidi added for import mobile s56(5303)
                else if (element.Name == "SpeedLimit")
                {
                    offenceDetails.SpeedLimit = string.IsNullOrEmpty(element.Value) ? "" : element.Value.Trim();
                }
                else if (element.Name == "SpeedReading1")
                    offenceDetails.SpeedReading1 = string.IsNullOrEmpty(element.Value) ? "" : element.Value.Trim();
                else if (element.Name == "SpeedReading2")
                    offenceDetails.SpeedReading2 = string.IsNullOrEmpty(element.Value) ? "" : element.Value.Trim();
                else if (element.Name == "IsSpeedOffence")
                    offenceDetails.IsSpeedOffence = string.IsNullOrEmpty(element.Value) ? false : (element.Value.Trim() == "1" || element.Value.Trim().ToLower() == "true") ? true : false;
                else if (element.Name == "ChargeDetails")
                    chargeDetailsList.Add(CreateChargeDetails(element.Elements()));
            }
            offenceDetails.ChargeDetails = chargeDetailsList;
            return offenceDetails;
        }
        public ChargeDetails CreateChargeDetails(IEnumerable<XElement> elements)
        {
            ChargeDetails chargeDetails = new ChargeDetails();
            foreach (XElement element in elements)
            {
                if (element.Name == "ChargeAmount")
                    chargeDetails.ChargeAmount = element.Value;
                else if (element.Name == "ChargeDescription")
                    chargeDetails.ChargeDescription = element.Value;
                else if (element.Name == "ChargeOffenceCode")
                    chargeDetails.ChargeOffenceCode = element.Value;
                else if (element.Name == "IsMain")
                    chargeDetails.IsMain = element.Value;
                else if (element.Name == "IsNoAog")
                    chargeDetails.IsNoAog = element.Value;
            }
            return chargeDetails;
        }
        public CompanyDetails CreateCompanyDetails(IEnumerable<XElement> elements)
        {
            CompanyDetails companyDetails = new CompanyDetails();
            foreach (XElement element in elements)
            {
                if (element.Name == "CompanyAddress")
                    companyDetails.CompanyAddress = element.Value;
                else if (element.Name == "CompanyName")
                    companyDetails.CompanyName = element.Value;
                else if (element.Name == "CompanyRegDate")
                {
                    if (!string.IsNullOrEmpty(element.Value))
                    {
                        DateTime time;
                        DateTime Date1900 = new DateTime(1900, 1, 1);
                        bool flag = DateTime.TryParse(element.Value, out time);
                        if (flag)
                        {
                            if (time > Date1900)
                                companyDetails.CompanyRegDate = Convert.ToDateTime(element.Value);
                        }
                        if (!flag)
                            companyDetails.CompanyRegDate = null;
                    }
                    else
                    {
                        companyDetails.CompanyRegDate = null;
                    }

                }
                else if (element.Name == "CompanyRegNumber")
                    companyDetails.CompanyRegNumber = element.Value;
                else if (element.Name == "CompanyType")
                    companyDetails.CompanyType = element.Value;
            }
            return companyDetails;
        }
        public CourtDetails CreateCourtDetails(IEnumerable<XElement> elements)
        {
            CourtDetails courtDetails = new CourtDetails();
            foreach (XElement element in elements)
            {
                if (element.Name == "CourtCode")
                    courtDetails.CourtCode = element.Value;
                if (element.Name == "CourtName")
                    courtDetails.CourtName = element.Value;
                if (element.Name == "CourtRoom")
                    courtDetails.CourtRoom = element.Value;
                //2014-07-31 Heidi added for import mobile s56(5303)
                if (element.Name == "CourtDate")
                    courtDetails.CourtDate = string.IsNullOrEmpty(element.Value) ? "" : element.Value.Trim();
                if (element.Name == "PaymentDate")
                    courtDetails.PaymentDate = string.IsNullOrEmpty(element.Value) ? "" : element.Value.Trim();
            }
            return courtDetails;
        }
        public OffenderDetails CreateOffenderDetails(IEnumerable<XElement> elements)
        {
            OffenderDetails offenderDetails = new OffenderDetails();
            int postAddressCount = 0;
            int stAddCount = 0;
            foreach (XElement element in elements)
            {
                if (element.Name == "OffenderAddress")
                    offenderDetails.OffenderAddress = element.Value;
                else if (element.Name == "OffenderAge")
                    offenderDetails.OffenderAge = (element.Value == "0" ? null : element.Value);
                else if (element.Name == "OffenderComments")
                    offenderDetails.OffenderComments = element.Value;
                else if (element.Name == "OffenderIDNumber")
                    offenderDetails.OffenderIDNumber = (element.Value == "0" ? string.Empty : element.Value);
                else if (element.Name == "OffenderIDType")
                    offenderDetails.OffenderIDType = element.Value;
                else if (element.Name == "OffenderName")
                    offenderDetails.OffenderName = element.Value==null?"":element.Value.Trim();
                else if (element.Name == "OffenderNationality")
                    offenderDetails.OffenderNationality = element.Value;
                else if (element.Name == "OffenderSex")
                    offenderDetails.OffenderSex = element.Value;
                else if (element.Name == "OffenderSurname")
                    offenderDetails.OffenderSurname = element.Value==null ? "":element.Value.Trim();
                else if (element.Name == "ParentOrGuardian")
                    offenderDetails.ParentOrGuardian = element.Value;
                //2014-08-01 Heidi comment out for import mobile s56(5303)
                //else if (element.Name == "PoCode")
                //    offenderDetails.PoCode = (element.Value == "0" ? null : element.Value);
                //else if (element.Name == "StCode")
                //    offenderDetails.StCode = (element.Value == "0" ? null : element.Value);
                // 2014-10-30 Heidi changed for Compatible with 7 version(5376)
                else if (element.Name == "PoCode")
                {
                    offenderDetails.BusinessAddressCode = (element.Value == "0" ? null : element.Value);
                }
                else if (element.Name == "StCode")
                {
                    offenderDetails.AddressCode = (element.Value == "0" ? null : element.Value);
                }
                else if (element.Name == "PostAddress")
                {
                    offenderDetails.BusinessAddress1 = element.Value == null ? "" : element.Value.Trim();
                }
                else if (element.Name == "StAdd")
                {
                    offenderDetails.Address1 = element.Value == null ? "" : element.Value.Trim();
                }
                //end //2014-10-30 Heidi changed for Compatible with 7 version(5376)
                //2014-07-29 Heidi added for import mobile s56(5303)
                else if (element.Name == "OffenderPDPNumber")
                {
                    offenderDetails.OffenderPDPNumber = element.Value == null ? "" : element.Value.Trim();
                }
                else if (element.Name == "OffenderCellNumber")
                {
                    offenderDetails.OffenderCellNumber = element.Value == null ? "" : element.Value.Trim();
                }
                else if (element.Name == "OffenderDriverLicenceNumber")
                {
                    offenderDetails.OffenderLicenceNumber = element.Value == null ? "" : element.Value.Trim();
                }
                else if (element.Name == "OffenderDriverLicenceCode")
                {
                    offenderDetails.OffenderLicenceCode = element.Value == null ? "" : element.Value.Trim();
                }
                else if (element.Name == "OffenderOccupation")
                {
                    offenderDetails.OffenderOccupation = element.Value == null ? "" : element.Value.Trim();
                }
                else if (element.Name == "OffenderEmailAddress")
                {
                    offenderDetails.OffenderEmailAddress = element.Value == null ? "" : element.Value.Trim();
                }
                else if (element.Name == "Address1")
                {
                    offenderDetails.Address1 = element.Value == null ? "" : element.Value.Trim();
                }
                else if (element.Name == "Address2")
                {
                    offenderDetails.Address2 = element.Value == null ? "" : element.Value.Trim();
                }
                else if (element.Name == "Address3")
                {
                    offenderDetails.Address3 = element.Value == null ? "" : element.Value.Trim();
                }
                else if (element.Name == "Address4")
                {
                    offenderDetails.Address4 = element.Value == null ? "" : element.Value.Trim();
                }
                else if (element.Name == "AddressCode")
                {
                    offenderDetails.AddressCode = (element.Value == "0" ? null : element.Value);
                }
                else if (element.Name == "AddressTelephone")
                {
                    offenderDetails.AddressTelephone = element.Value == null ? "" : element.Value.Trim();
                }
                else if (element.Name == "BusinessAddress1")
                {
                    offenderDetails.BusinessAddress1 = element.Value == null ? "" : element.Value.Trim();
                }
                else if (element.Name == "BusinessAddress2")
                {
                    offenderDetails.BusinessAddress2 = element.Value == null ? "" : element.Value.Trim();
                }
                else if (element.Name == "BusinessAddress3")
                {
                    offenderDetails.BusinessAddress3 = element.Value == null ? "" : element.Value.Trim();
                }
                else if (element.Name == "BusinessAddress4")
                {
                    offenderDetails.BusinessAddress4 = element.Value == null ? "" : element.Value.Trim();
                }
                else if (element.Name == "BusinessAddressCode")
                {
                    offenderDetails.BusinessAddressCode = (element.Value == "0" ? null : element.Value);
                }
                else if (element.Name == "BusinessTelephone")
                {
                    offenderDetails.BusinessTelephone = element.Value == null ? "" : element.Value.Trim();
                }

                #region old address
                //2014-08-01 Heidi comment out for import mobile s56(5303)
                //else if (element.Name == "PostAddress")
                //{
                //    if (postAddressCount == 0)
                //        offenderDetails.PostAddress1 = element.Value;
                //    else if (postAddressCount == 1)
                //        offenderDetails.PostAddress2 = element.Value;
                //    else if (postAddressCount == 2)
                //        offenderDetails.PostAddress3 = element.Value;
                //    else if (postAddressCount == 3)
                //        offenderDetails.PostAddress4 = element.Value;
                //    else if (postAddressCount == 4)
                //        offenderDetails.PostAddress5 = element.Value;
                //    postAddressCount++;
                //}
                //else if (element.Name == "StAdd")
                //{
                //    if (stAddCount == 0)
                //        offenderDetails.StAdd1 = element.Value;
                //    else if (stAddCount == 1)
                //        offenderDetails.StAdd2 = element.Value;
                //    else if (stAddCount == 2)
                //        offenderDetails.StAdd3 = element.Value;
                //    else if (stAddCount == 3)
                //        offenderDetails.StAdd4 = element.Value;
                //    stAddCount++;
                //}
                #endregion
            }
            return offenderDetails;
        }
        #endregion
    }
    #region Defined entity
    public class Notices
    {
        private OffenderDetails _offenderDetails;
        private CourtDetails _courtDetails;
        private OffenceDetails _offenceDetails;
        private VehicleDetails _vehicleDetails;
        private CompanyDetails _companyDetails;
        private LocalAuthority _localAuthority;

        public string XSDVersion { get; set; } //Heidi 2013-08-12 added for task 5050
        public string DocumentType { get; set; }
        public int IsComplete { get; set; }
        public string Notes { get; set; } //nmd nvarchar(max)
        public string NoticeBarCodeImage { get; set; } // nmd
        public string NoticeBarCodeImageType { get; set; } //Heidi 2014-09-10 added for task 5303
        public string NoticeNumber { get; set; }
        public string NoticeUID { get; set; } //nmd nvarchar(50)
        public string OffenderSignature { get; set; } //nmd nvarchar(35)
        public string OfficerGroupCode { get; set; }
        public string OfficerNameOnDuty { get; set; }
        public string OfficerNumber { get; set; } // owner of the device - may be the same as the officer on duty
        public string OfficerNumberOnDuty { get; set; }
        public string OfficerSignature { get; set; } //nmd nvarchar(35)
        public bool PrintedFlag { get; set; } //status code like 255
        public string WitnessComments { get; set; } //nmd nvarchar(max)
        public string WitnessName { get; set; } //nmd nvarchar(35)
        public string LocationSuburb { get; set; } //Heidi 2013-08-12 added for task 5050
        public string OffenderSignatureImgType { get; set; }//Heidi 2014-09-10 added for task 5303
        public string OfficerSignatureImgType { get; set; }//Heidi 2014-09-10 added for task 5303

        public LocalAuthority LocalAuthority
        {
            get
            {
                if (this._localAuthority == null)
                    return new LocalAuthority();
                else
                    return this._localAuthority;
            }
            set { this._localAuthority = value; }
        }
        public OffenderDetails OffenderDetails
        {
            get
            {
                if (this._offenderDetails == null)
                    return new OffenderDetails();
                else
                    return this._offenderDetails;
            }
            set { this._offenderDetails = value; }
        }
        public CourtDetails CourtDetails
        {
            get
            {
                if (this._courtDetails == null)
                    return new CourtDetails();
                else
                    return this._courtDetails;
            }
            set { this._courtDetails = value; }
        }
        public OffenceDetails OffenceDetails
        {
            get
            {
                if (this._offenceDetails == null)
                    return new OffenceDetails();
                else
                    return this._offenceDetails;
            }
            set { this._offenceDetails = value; }
        }
        public VehicleDetails VehicleDetails
        {
            get
            {
                if (this._vehicleDetails == null)
                    return new VehicleDetails();
                else
                    return this._vehicleDetails;
            }
            set { this._vehicleDetails = value; }
        }
        public CompanyDetails CompanyDetails
        {
            get
            {
                if (this._companyDetails == null)
                    return new CompanyDetails();
                else
                    return this._companyDetails;
            }
            set { this._companyDetails = value; }
        }
    }
    public class LocalAuthority
    {
        public string AutCode { get; set; }
        public string AutName { get; set; }
        public string AutNumber { get; set; }
        public int AutIntNo { get; set; }
    }
    public class OffenderDetails
    {
        public string OffenderAddress { get; set; }
        public string OffenderAge { get; set; }
        public string OffenderComments { get; set; } // nmd nvarchar(max)
        public string OffenderIDNumber { get; set; }
        public string OffenderIDType { get; set; }
        public string OffenderName { get; set; }//forename
        public string OffenderNationality { get; set; }
        public string OffenderSex { get; set; } // 
        public string OffenderSurname { get; set; }
        public string ParentOrGuardian { get; set; }

        //2014-07-29 Heidi changed for import mobile s56(5303)
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Address5 { get; set; }
        public string AddressCode { get; set; }
        public string AddressTelephone { get; set; }
        public string BusinessAddress1 { get; set; }
        public string BusinessAddress2 { get; set; }
        public string BusinessAddress3 { get; set; }
        public string BusinessAddress4 { get; set; }
        public string BusinessAddressCode { get; set; }
        public string BusinessTelephone { get; set; }
        //2014-07-29 Heidi added for import mobile s56(5303)
        public string OffenderPDPNumber { get; set; }
        public string OffenderCellNumber { get; set; }
        public string OffenderLicenceNumber { get; set; }
        public string OffenderLicenceCode { get; set; }
        public string OffenderEmailAddress { get; set; }
        public string OffenderOccupation { get; set; }
    }
    public class CompanyDetails //nmd
    {
        public string CompanyAddress { get; set; } //nvarchar(200)
        public string CompanyName { get; set; } //nvarchar(100)
        public DateTime? CompanyRegDate { get; set; } //datetime
        public string CompanyRegNumber { get; set; } //nvarchar(50)
        public string CompanyType { get; set; } //nvarchar(30)
    }
    public class CourtDetails
    {
        public string CourtCode { get; set; } // problem until street coding implemented - s341 does not have court
        public string CourtName { get; set; } // lookup
        public string CourtRoom { get; set; } // s56 only
        public string CourtDate { get; set; } // CourtDate
        public string PaymentDate { get; set; } // PaymentDate
    }
    public class OffenceDetails
    {
        public string GPS1 { get; set; } //nmd nvarchar(30)
        public string GPS2 { get; set; } //nmd nvarchar(30)
        public string ObservationTime { get; set; } // offence time
        public string OffenceDate { get; set; } // offence date
        public string PlaceOfOffence { get; set; } // NotLocdescr
        //2014-07-31 Heidi added for import mobile s56(5303)
        public string SpeedLimit { get; set; } // SpeedLimit
        public string SpeedReading1 { get; set; } // SpeedReading1
        public string SpeedReading2 { get; set; } // SpeedReading2
        public bool IsSpeedOffence { get; set; }//IsSpeedOffence

        private List<ChargeDetails> _chargeDetails;
        public List<ChargeDetails> ChargeDetails
        {
            get
            {
                if (this._chargeDetails == null)
                    return new List<ChargeDetails>();
                else
                    return this._chargeDetails;
            }
            set { this._chargeDetails = value; }
        }
    }
    public class ChargeDetails
    {
        private int _indexId = 1;//default group
        public string ChargeAmount { get; set; }
        public string ChargeDescription { get; set; }
        public string ChargeOffenceCode { get; set; } // add IsMain & IsNoAOG
        public string IsMain { get; set; }
        public string IsNoAog { get; set; }
        public string PublicCode { get; set; }
        public int indexId
        {
            get
            {
                return _indexId;
            }
            set
            {
                _indexId = value;
            }
        }
    }
    public class ComparerChargeDetail : Comparer<ChargeDetails>
    {
        public override int Compare(ChargeDetails x, ChargeDetails y)
        {
            if (!x.IsMain.ToLower().Equals(y.IsMain.ToLower()))
            {
                if (x.IsMain.ToLower() == "y")
                    return -1;
                else
                    return 1;
            }
            else
            {
                return 0;
            }
        }
    }
    public class ComparerChargeDetailByIndex : Comparer<ChargeDetails>
    {
        public override int Compare(ChargeDetails x, ChargeDetails y)
        {
            if (x.indexId.CompareTo(y.indexId) != 0)
            {
                return -x.indexId.CompareTo(y.indexId);
            }
            else
            {
                return 0;
            }
        }
    }
    public class VehicleDetails
    {
        public string VehicleColourDescr { get; set; } // NotVehicleColour
        public string VehicleLicExpDate { get; set; } //NotVehicleLicenceExpiry
        public string VehicleMakeCode { get; set; } //lookup
        public string VehicleMakeDescr { get; set; }
        public string VehicleRegNumber { get; set; } //notregno
        public string VehicleTypeCode { get; set; } // lookup
        public string VehicleTypeDescr { get; set; }
        public bool IsTaxi { get; set; }
    }
    #endregion
}
