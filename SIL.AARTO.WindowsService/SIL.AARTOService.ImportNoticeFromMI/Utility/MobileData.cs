﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using mobileEntities = SIL.MobileInterface.DAL.Entities;
using mobileService = SIL.MobileInterface.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Text.RegularExpressions;
using System.Data;
using SIL.AARTO.BLL.Model;
using System.Data.SqlClient;
using SIL.AARTO.DAL.Data;
using SIL.AARTOService.Library;
using System.Globalization;


namespace SIL.AARTOService.ImportNoticeFromMI.Utility
{
    public abstract class MobileData
    {
        public string connectStr;
        public readonly string lastUser = "ImportNoticeFromMI";
        public DateTime baseDate1900 = new DateTime(1900, 1, 1);
        List<TrafficOfficer> officerList = new List<TrafficOfficer>();

        #region validation

        public abstract List<RejectionReasonsColumn> ValidatingMobileData(Notices notices);
        public abstract List<int> ValidatingMobileDataOfficerError(Notices notices);

        public void MobileValidationBase(List<RejectionReasonsColumn> list, Notices notices)
        {
            //2014-08-07 Heidi comment out for import mobile s56(5303)
            //if (!CheckNoticeType(notices.DocumentType))
            //{
            //    list.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_01, "DocumentType"));
            //}
            //if (!CheckNoticeNumber(notices.NoticeNumber))
            //{
            //    list.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_02, "NoticeNumber"));
            //}

            //if (!CheckAutCode(notices.LocalAuthority))
            //{
            //    list.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_04, "LocalAuthority"));
            //}

            if (!CheckOfficer(notices.OfficerNumber))
            {
                list.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_06, "OfficerNumber"));
            }
            if (!CheckOfficer(notices.OfficerNumberOnDuty))
            {
                list.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_07, "OfficerNumberOnDuty"));
            }

            CheckVehicleDetail(list, notices.VehicleDetails);//vehicle detail

            CheckOffenceDetail(list, notices.OffenceDetails, notices.LocalAuthority); //offence detail

            if (!CheckCourtDetail(notices.CourtDetails))
            {
                list.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_19, "CourtDetails"));
            }
            //Offender age must be numeric
            if (!CheckOffenderDetail(notices.OffenderDetails))
            {
                list.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_20, "OffenderDetails"));
            }

            if (!CheckOffenderIDNumber(notices.OffenderDetails))
            {
                //Heidi 2013-08-01 added for task 4992B
                list.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_21, "OffenderIDNumber"));
            }

            CheckChargeDetail(list, notices);



        }


        public bool CheckNoticeType(string name)
        {
            if (name == mobileEntities.DocumentTypeIdList.S341Mobile.ToString())
                return true;
            else if (name == mobileEntities.DocumentTypeIdList.S56Mobile.ToString())
                return true;
            else
                return false;
        }

        public bool CheckNoticeNumber(string noticeNumber)
        {
            if (!string.IsNullOrEmpty(noticeNumber))
            {
                mobileService.NoticeNumberSentToMobiledeviceService service = new mobileService.NoticeNumberSentToMobiledeviceService();
                var numberEntity = service.GetByNnsmdNoticeNumber(noticeNumber);
                if (numberEntity != null)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public bool CheckNoticeNumberDuplicate(string noticeNumber)
        {
            bool validationSucceed = false;

            if (!string.IsNullOrEmpty(noticeNumber))
            {
                IList<Notice> noticeEntityList = new NoticeService().GetByNotTicketNo(noticeNumber.Trim());
                if (noticeEntityList != null && noticeEntityList.Count > 0)
                {
                    validationSucceed = false;
                }
                else
                {
                    validationSucceed = true;
                }
            }
            else
            {
                validationSucceed = false;
            }

            return validationSucceed;
        }

        //public bool NoticeTypeAndNoticeNumberMatch(string noticeType, Notices notice)
        //{
        //    bool flag = false;
        //    if (notice.LocalAuthority.AutIntNo > 0)
        //    {
        //        if (noticeType == mobileEntities.DocumentTypeIdList.S341Mobile.ToString())
        //        {
        //            flag = importNotice.ValidateNoticeNumberS341(notice.NoticeNumber, notice.LocalAuthority.AutIntNo);
        //        }
        //        else if (noticeType == mobileEntities.DocumentTypeIdList.S56Mobile.ToString())
        //        {
        //            flag = importNotice.ValidateNoticeNumberS56(notice.NoticeNumber, notice.LocalAuthority.AutIntNo);
        //        }
        //    }
        //    return flag;
        //}

        public bool CheckAutCode(LocalAuthority localAuthority)
        {
            if (localAuthority == null)
                return false;
            AuthorityService service = new AuthorityService();
            Authority authority = service.GetByAutCode(localAuthority.AutCode);
            if (authority != null)
            {
                localAuthority.AutIntNo = authority.AutIntNo;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CheckOfficer(string officerNumber)
        {
            if (!string.IsNullOrEmpty(officerNumber))
            {
                TrafficOfficerService service = new TrafficOfficerService();
                if (officerList.Count == 0)
                    officerList = service.GetAll().ToList();
                if (officerList.Where(o => o.ToNo == officerNumber.Trim()).ToList().Count > 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public bool CheckVechileColour(string colour)
        {
            bool flag = true;
            if (!string.IsNullOrEmpty(colour))
            {
                VehicleColourService service = new VehicleColourService();
                var vehicleColour = service.GetByVcCode(colour);
                if (vehicleColour.Count == 0)
                    flag = false;
            }
            return flag;
        }

        public bool CheckVehicleLicExpDate(string date)
        {
            bool flag = true;
            if (!string.IsNullOrEmpty(date))
            {
                DateTime ExpDate;
                flag = DateTime.TryParse(date, out ExpDate);
                flag = ExpDate > baseDate1900;
            }
            return flag;
        }

        public bool CheckVehicleMake(string vehicleMake)
        {
            bool flag = true;
            if (!string.IsNullOrEmpty(vehicleMake))
            {
                VehicleMakeService service = new VehicleMakeService();
                var vehicle = service.GetByVmCode(vehicleMake);
                if (vehicle.Count == 0)
                    flag = false;
            }
            return flag;
        }

        public bool CheckVehicleType(string vehicleType)
        {
            bool flag = true;
            if (!string.IsNullOrEmpty(vehicleType))
            {
                VehicleTypeService service = new VehicleTypeService();
                var vehicle = service.GetByVtCode(vehicleType);
                if (vehicle.Count == 0)
                    flag = false;
            }
            return flag;
        }

        //Heidi 2013-08-01 added for task 4992B
        public bool CheckVehicleRegNO(string regNo)
        {
            bool flag = true;
            if (!string.IsNullOrEmpty(regNo))
            {
                regNo = regNo.Trim().ToUpper();
                if (regNo.Length < 2 || regNo.Length > 10)
                {
                    flag = false;
                }
                Regex re = new Regex("^[A-Z0-9]+$");
                if (!re.IsMatch(regNo))
                {
                    flag = false;
                }
            }
            return flag;
        }

        public bool CheckObservationTime(string date)
        {
            bool flag = false;
            if (!string.IsNullOrEmpty(date))
            {
                DateTime observationTime;
                flag = DateTime.TryParse(date, out observationTime);
                if (flag)
                    flag = (DateTime.Now >= observationTime && observationTime > baseDate1900);
            }
            return flag;
        }


        public bool CheckOffenceDate(string date)
        {
            bool flag = false;
            if (!string.IsNullOrEmpty(date))
            {
                DateTime offenceDate;
                flag = DateTime.TryParse(date, out offenceDate);
                if (flag)
                {
                    flag = (DateTime.Now >= offenceDate && offenceDate > baseDate1900);
                }
                //Heidi 2013-08-01 added for task 4992B
                if (flag)
                {
                    flag = (Convert.ToDateTime(offenceDate.ToShortDateString()).AddMonths(6) >= Convert.ToDateTime(DateTime.Now.ToShortDateString()));
                }

            }
            return flag;
        }

        //Jake 2015-03-27 modified this function to use lamda expression
        public bool checkChargeOffenceCodeSame(List<ChargeDetails> list)
        {
            bool HasSame = false;

            if (list == null) return HasSame;

            var result = list
                .GroupBy(c => c.ChargeOffenceCode)
                .Select(r => (new { Code = r.Key, RowCount = r.Count() }));

            HasSame = result.Where(r => r.RowCount > 1).Count() > 0;

            //if (list != null && list.Count >= 2)
            //{
            //    for (int i = 0; i < list.Count(); i++)
            //    {
            //        for (int j = i + 1; j < list.Count(); j++)
            //        {
            //            if (list[j].ChargeOffenceCode == list[i].ChargeOffenceCode)
            //            {
            //                HasSame = true;
            //                break;
            //            }
            //        }
            //    }
            //}
            return HasSame;
        }

        public void CheckChargeDetail(List<RejectionReasonsColumn> eList, Notices notices)
        {
            OffenceDetails offenceDetails = notices.OffenceDetails;
            LocalAuthority localAuthority = notices.LocalAuthority;
            List<ChargeDetails> list = offenceDetails.ChargeDetails;
            decimal amount;
            Offence offence;
            int chargeCounter = 0;
            string isNoAog = "";

            //List<Offence> offenceList = new List<Offence>();
            OffenceService service = new OffenceService();

            //offenceList = service.GetAll().ToList();

            if (checkChargeOffenceCodeSame(list))
            {
                eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_25, "ChargeOffenceCode"));
            }



            foreach (var detail in list)
            {
                offence = service.Find(string.Format("OffCode='{0}'", detail.ChargeOffenceCode.Trim())).FirstOrDefault();
                //offence = offenceList.Where(o => o.OffCode == detail.ChargeOffenceCode.Trim()).FirstOrDefault();
                if (offence != null)
                {
                    chargeCounter = chargeCounter + 1;
                    isNoAog = detail.IsNoAog == null ? "" : detail.IsNoAog.Trim().ToUpper();

                    if (!decimal.TryParse(detail.ChargeAmount.ToString(), out amount))
                    {
                        eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_15, "ChargeAmount" + chargeCounter));
                    }

                    if ((amount <= 0 || amount >= 10000) && isNoAog == "N") //Heidi 2013-08-01 added for task 4992B
                    {
                        eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_22, "ChargeAmount" + chargeCounter));
                    }

                    if (offence.OffNoAog.ToUpper() != isNoAog)
                    {
                        eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_18, "IsNoAog" + "-Charge" + chargeCounter));
                    }

                    if (!string.IsNullOrEmpty(detail.IsNoAog) && detail.IsNoAog.ToUpper() == "Y")//Heidi 2013-08-01 added for task 4992B
                    {
                        if (amount != 0)
                        {
                            eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_24, "IsNoAog ChargeAmount" + chargeCounter));
                        }
                    }

                    if (!string.IsNullOrEmpty(detail.IsMain) && detail.IsMain.ToUpper() == "N")
                    {
                        if (!string.IsNullOrEmpty(offence.OffCode))
                        {
                            List<Offence> AlternateoffenceList = service.Find(string.Format("OffCodeAlternate='{0}'", offence.OffCode)).ToList();
                            //List<Offence> AlternateoffenceList = offenceList.Where(a => a.OffCodeAlternate == offence.OffCode).ToList();
                            if (AlternateoffenceList == null || (AlternateoffenceList != null && AlternateoffenceList.Count == 0))
                            {
                                eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_26, "IsMain" + "-Charge" + chargeCounter));
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(detail.IsMain) && detail.IsMain.ToUpper() == "N")
                    {
                        if (!CheckAlternateCharge(list, detail, offence, service))
                        {
                            eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_27, "IsMain" + "-Charge" + chargeCounter));
                        }

                    }

                    //Heidi 2013-08-01 added for task 4992B
                    if (offence.OffIsByLaw && notices.VehicleDetails != null)
                    {
                        notices.VehicleDetails.VehicleRegNumber = "";
                    }

                }
                else
                {
                    eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_16, "ChargeOffenceCode"));
                }

            }
        }

        public bool CheckMobileGeneralValidationByType(string tableName, Notices notice)
        {
            bool validationSucceed = false;
            try
            {

                CourtDetails courtDetails = notice.CourtDetails;
                OffenderDetails offenderDetails = notice.OffenderDetails;

                string crtNo = courtDetails.CourtCode;
                string crtRoom = courtDetails.CourtRoom;
                string courtdate = courtDetails.CourtDate;
                string autCode = notice.LocalAuthority.AutCode;
                string postalCode = offenderDetails.AddressCode;
                string officerCode = notice.OfficerNumberOnDuty;

                DateTime _courtdate = DateTime.Parse("1900-01-01");
                DateTime.TryParse(courtdate, out _courtdate);

                if (tableName == "CourtRoom" && _courtdate == DateTime.Parse("1900-01-01"))
                {
                    return false;
                }

                DataSet ds = new DataSet();
                SqlDataAdapter sqlDataAdapterValid = new SqlDataAdapter();

                // Create Instance of Connection and Command Object
                sqlDataAdapterValid.SelectCommand = new SqlCommand();
                sqlDataAdapterValid.SelectCommand.Connection = new SqlConnection(connectStr);
                sqlDataAdapterValid.SelectCommand.CommandText = "MobileGeneralValidationByType";

                // Mark the Command as a SPROC
                sqlDataAdapterValid.SelectCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter parameterType = new SqlParameter("@Type", SqlDbType.NVarChar, 50);
                parameterType.Value = tableName;
                sqlDataAdapterValid.SelectCommand.Parameters.Add(parameterType);

                SqlParameter parameterAutCode = new SqlParameter("@autCode", SqlDbType.VarChar, 3);
                parameterAutCode.Value = autCode;
                sqlDataAdapterValid.SelectCommand.Parameters.Add(parameterAutCode);

                SqlParameter parameterCrtNo = new SqlParameter("@CrtNo", SqlDbType.VarChar, 30);
                parameterCrtNo.Value = crtNo;
                sqlDataAdapterValid.SelectCommand.Parameters.Add(parameterCrtNo);

                SqlParameter parameterCrtRoom = new SqlParameter("@CrtRoom", SqlDbType.VarChar, 50);
                parameterCrtRoom.Value = crtRoom;
                sqlDataAdapterValid.SelectCommand.Parameters.Add(parameterCrtRoom);

                SqlParameter parameterCDate = new SqlParameter("@CDate", SqlDbType.SmallDateTime);
                parameterCDate.Value = _courtdate;
                sqlDataAdapterValid.SelectCommand.Parameters.Add(parameterCDate);

                SqlParameter parameterPostalCode = new SqlParameter("@PostalCode", SqlDbType.VarChar, 30);
                parameterPostalCode.Value = postalCode;
                sqlDataAdapterValid.SelectCommand.Parameters.Add(parameterPostalCode);

                SqlParameter parameterOfficerCode = new SqlParameter("@OfficerCode", SqlDbType.VarChar, 20);
                parameterOfficerCode.Value = officerCode;
                sqlDataAdapterValid.SelectCommand.Parameters.Add(parameterOfficerCode);

                // Execute the command and close the connection
                sqlDataAdapterValid.Fill(ds);
                sqlDataAdapterValid.SelectCommand.Connection.Dispose();

                if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    validationSucceed = true;
                }
            }
            catch (Exception ex)
            {
                validationSucceed = false;
            }

            return validationSucceed;
        }

        public bool CheckAlternateCharge(List<ChargeDetails> list, ChargeDetails detail, Offence offence, OffenceService service)
        {
            bool OffCodeAlternateIsCorrect = false;

            List<ChargeDetails> Otherlist = list.Where(a => a.ChargeOffenceCode != detail.ChargeOffenceCode.Trim() && !string.IsNullOrEmpty(a.IsMain) && a.IsMain.ToUpper() == "Y").ToList();

            if (Otherlist != null && Otherlist.Count > 0)
            {
                string[] OffCodeArr = new string[Otherlist.Count];
                int index = 0;
                foreach (var item in Otherlist)
                {
                    OffCodeArr[index++] = item.ChargeOffenceCode;

                }
                if (Otherlist != null && Otherlist.Count > 0 && OffCodeArr.Length > 0)
                {
                    OffenceQuery offencequery = new OffenceQuery();
                    offencequery.Append(OffenceColumn.OffCodeAlternate, offence.OffCode);
                    offencequery.AppendIn(OffenceColumn.OffCode, OffCodeArr);
                    TList<Offence> offenceTList = service.Find(offencequery as IFilterParameterCollection);
                    if (offenceTList.Count > 0)
                    {
                        OffCodeAlternateIsCorrect = true;
                    }
                }
            }

            return OffCodeAlternateIsCorrect;

        }

        //Heidi 2013-08-01 added for task 4992B
        public decimal GetOffenceFineAmount(int offIntNo, string autCode, string offenceDate)
        {
            DateTime offenceD = new DateTime(1900, 1, 1);
            if (!string.IsNullOrEmpty(offenceDate.Trim()))
            {
                offenceD = Convert.ToDateTime(offenceDate.Trim());
            }
            // 2013-07-26, Oscar added using for IDataReader
            using (IDataReader reader = new SIL.AARTO.DAL.Services.OffenceFineService().GetOffenceFineAmount(offIntNo, autCode.Trim(), offenceD))
            {
                if (reader.Read())
                {
                    return decimal.Parse(reader["FineAmount"].ToString());
                }
                else
                {
                    return 0;
                }
            }

        }

        public bool CheckCourtDetail(CourtDetails detail)
        {
            if (detail != null && !string.IsNullOrEmpty(detail.CourtCode))
            {
                CourtService service = new CourtService();
                var court = service.GetByCrtNo(detail.CourtCode);
                if (court != null)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public bool CheckSignatureImage(Notices notice)
        {
            bool validationSucceed = false;

            if (notice != null)
            {
                if (notice.OffenderSignature != null && notice.OfficerSignature != null)
                {
                    if (notice.OffenderSignature.Trim() != "" && notice.OfficerSignature.Trim() != "")
                    {
                        validationSucceed = true;
                    }
                }
            }

            return validationSucceed;
        }

        public bool CheckOffenderDetail(OffenderDetails detail)
        {
            bool flag = true;
            if (detail != null && !string.IsNullOrEmpty(detail.OffenderAge))
            {
                int result;
                flag = int.TryParse(detail.OffenderAge, out result);
            }
            return flag;
        }

        public bool CheckOffenderAge(Notices notices)
        {
            bool validationSucceed = true;
            OffenderDetails offenderDetail = notices.OffenderDetails;
            int age = 0;

            if (offenderDetail != null)
            {
                if (!int.TryParse(offenderDetail.OffenderAge, out age))
                {
                    validationSucceed = false;
                }
                else
                {
                    if (age <= 0)
                    {
                        validationSucceed = false;
                    }
                }
            }

            return validationSucceed;

        }

        //Heidi 2013-08-01 added for task 4992B
        public bool CheckOffenderIDNumber(OffenderDetails detail)
        {
            bool flag = true;
            string idNumber = "";
            if (detail != null && !string.IsNullOrEmpty(detail.OffenderIDNumber))
            {
                idNumber = detail.OffenderIDNumber.Trim().ToUpper();

                if (idNumber.Length == 13)
                {
                    int d = GetControlDigit(idNumber.Substring(0, 12));
                    if (d == -1)
                    {
                        flag = false;
                    }
                    else
                    {
                        if (idNumber.Substring(12, 1) != d.ToString())
                        {
                            flag = false;
                        }
                    }
                }
                else
                {
                    Regex re = new Regex("^[A-Z0-9]+$");
                    if (!re.IsMatch(idNumber))
                    {
                        flag = false;
                    }
                }

            }

            return flag;

        }

        //Heidi 2013-08-01 added for task 4992B
        public bool CheckOffenderIDNumberOnlyCheckIsNull(OffenderDetails detail)
        {
            bool validationSucceed = false;
            string idNumber = "";
            if (detail != null && !string.IsNullOrEmpty(detail.OffenderIDNumber))
            {
                idNumber = detail.OffenderIDNumber.Trim();
                if (!string.IsNullOrEmpty(idNumber))
                {
                    validationSucceed = true;
                }

            }
            return validationSucceed;

        }

        //Heidi 2013-08-01 added for task 4992B
        public int GetControlDigit(string parsedIdString)
        {
            int d = -1;
            try
            {
                int a = 0;
                for (int i = 0; i < 6; i++)
                {
                    a += int.Parse(parsedIdString[2 * i].ToString());
                }
                int b = 0;
                for (int i = 0; i < 6; i++)
                {
                    b = b * 10 + int.Parse(parsedIdString[2 * i + 1].ToString());
                }
                b *= 2;
                int c = 0;
                do
                {
                    c += b % 10;
                    b = b / 10;
                } while (b > 0);
                c += a;
                d = 10 - (c % 10);
                if (d == 10)
                    d = 0;
            }
            catch {/*ignore*/}
            return d;
        }

        public void CheckOffenceDetail(List<RejectionReasonsColumn> eList, OffenceDetails detail, LocalAuthority localAuthority)
        {
            if (detail != null)
            {
                if (!CheckObservationTime(detail.ObservationTime))
                {
                    eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_12, "ObservationTime"));
                }
                if (!CheckOffenceDate(detail.OffenceDate))
                {
                    eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_13, "OffenceDate"));
                }

                if (string.IsNullOrEmpty(detail.PlaceOfOffence))
                {
                    eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_14, "PlaceOfOffence"));
                }

            }
            else
            {
                eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_12, "OffenceDetails"));
            }

        }

        public void CheckVehicleDetail(List<RejectionReasonsColumn> eList, VehicleDetails detail)
        {
            if (detail != null)
            {
                //Jake2014-02-21 comment out
                //if (!CheckVechileColour(detail.VehicleColourDescr))
                //{
                //    //2014-02-19 Heidi changed for the VehicleColour validation fails,the ImportNoticeFromMI_Service service can continue to import data.
                //    //eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_08, "VehicleColour"));
                //    detail.VehicleColourDescr ="";
                //}
                if (!CheckVehicleLicExpDate(detail.VehicleLicExpDate))
                {
                    eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_09, "VehicleLicExpDate"));
                }
                //Jake2014-02-21 comment out
                //if (!CheckVehicleMake(detail.VehicleMakeCode))
                //{
                //    //2014-02-19 Heidi changed for the VehicleMake validation fails,the ImportNoticeFromMI_Service service can continue to import data.
                //    //eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_10, "VehicleMake"));
                //    detail.VehicleMakeCode = "";
                //}
                //Jake2014-02-21 comment out
                //if (!CheckVehicleType(detail.VehicleTypeCode))
                //{
                //    //2014-02-19 Heidi changed for the VehicleType validation fails,the ImportNoticeFromMI_Service service can continue to import data.
                //    //eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_11, "VehicleType"));
                //    detail.VehicleTypeCode = "";
                //}
                if (!CheckVehicleRegNO(detail.VehicleRegNumber))
                {
                    //Heidi 2013-08-01 added for task 4992B
                    eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_23, "VehicleRegNumber"));
                }

            }
        }
        #endregion

        #region Import
        public abstract bool DoImport(SIL.MobileInterface.DAL.Entities.NoticeReceivedFromMobileDevice item, Notices notice, string officerError, bool isOfficerError, string batchBaseFileName, int delayHours, AARTOServiceBase AARTOBase, List<int> pfnList, ref int notIntNo, ref string errorMsg);
        #endregion


        #region get the base data

        public void GetChargeDetail(ChargeDetails chargeDetail, ref string chgOffenceCode, ref string chgOffenceDescr, ref float chgFineAmount, ref string chgAlternateCode, ref string chgNoAog, ref bool chgIsMain)
        {
            //if (chargeDetail.IsMain.ToLower() == "n")
            //{
            //    SIL.AARTO.DAL.Entities.Offence offence = AARTONotice.GetTheOffence(chargeDetail.IsMain, chargeDetail.ChargeOffenceCode);
            //    if (offence != null)
            //        chgOffenceCode = offence.OffCode;
            //    //
            //    //chgAlternateCode = chargeDetail.ChargeOffenceCode;
            //}
            //else
            float amount;
            chgOffenceCode = chargeDetail.ChargeOffenceCode;
            chgOffenceDescr = chargeDetail.ChargeDescription;
            //2014-10-30 Heidi changed for fixing Data conversion failure problem.(5376)
            float.TryParse(chargeDetail.ChargeAmount, NumberStyles.Currency, CultureInfo.InvariantCulture, out amount);
            chgFineAmount = amount;
            chgNoAog = chargeDetail.IsNoAog;
            if (chargeDetail.IsMain.ToLower() == "n")
            {
                chgAlternateCode = chargeDetail.ChargeOffenceCode;
            }

            if (!string.IsNullOrEmpty(chargeDetail.IsMain) && chargeDetail.IsMain.Trim().ToUpper() == "Y")
            {
                chgIsMain = true;//2014-10-30 Heidi added(5376)
            }
            else
            {
                chgIsMain = false;
            }
            //AARTONotice.GetOffenceByCode(chgOffenceCode).OffCodeAlternate;
        }

        public void GetchgAlternateChangeCode(List<ChargeDetails> chargeList, List<Offence> offenceList, ref string chgAlternateCode1,
            ref string chgAlternateCode2, ref string chgAlternateCode3)
        {
            Offence offence1 = null;
            Offence offence2 = null;
            Offence offence3 = null;

            if (chargeList != null && chargeList.Count > 0)
            {
                if (chargeList.Count == 2)
                {
                    offence1 = offenceList.Where(c => c.OffCode == chargeList[0].ChargeOffenceCode).FirstOrDefault();
                    offence2 = offenceList.Where(c => c.OffCode == chargeList[1].ChargeOffenceCode).FirstOrDefault();

                    if ((!string.IsNullOrEmpty(chargeList[0].IsMain) && chargeList[0].IsMain.Trim().ToUpper() == "Y")
                        && (!string.IsNullOrEmpty(chargeList[1].IsMain) && chargeList[1].IsMain.Trim().ToUpper() == "N"))
                    {

                        if (offence1.OffCodeAlternate == offence2.OffCode)
                        {
                            chgAlternateCode1 = offence2.OffCode;
                        }

                    }

                }
                else if (chargeList.Count == 3)
                {
                    offence1 = offenceList.Where(c => c.OffCode == chargeList[0].ChargeOffenceCode).FirstOrDefault();
                    offence2 = offenceList.Where(c => c.OffCode == chargeList[1].ChargeOffenceCode).FirstOrDefault();
                    offence3 = offenceList.Where(c => c.OffCode == chargeList[2].ChargeOffenceCode).FirstOrDefault();
                    if ((!string.IsNullOrEmpty(chargeList[0].IsMain) && chargeList[0].IsMain.Trim().ToUpper() == "Y")
                         && (!string.IsNullOrEmpty(chargeList[1].IsMain) && chargeList[1].IsMain.Trim().ToUpper() == "Y")
                        && (!string.IsNullOrEmpty(chargeList[2].IsMain) && chargeList[2].IsMain.Trim().ToUpper() == "N"))
                    {
                        if (offence1 != null && offence3 != null)
                        {
                            if (offence1.OffCodeAlternate == offence3.OffCode)
                            {
                                chgAlternateCode1 = offence3.OffCode;
                            }
                        }
                        if (offence2 != null && offence3 != null)
                        {
                            if (offence2.OffCodeAlternate == offence3.OffCode)
                            {
                                chgAlternateCode2 = offence3.OffCode;
                            }
                        }
                    }

                    if ((!string.IsNullOrEmpty(chargeList[0].IsMain) && chargeList[0].IsMain.Trim().ToUpper() == "Y")
                        && (!string.IsNullOrEmpty(chargeList[1].IsMain) && chargeList[1].IsMain.Trim().ToUpper() == "N")
                       && (!string.IsNullOrEmpty(chargeList[2].IsMain) && chargeList[2].IsMain.Trim().ToUpper() == "N"))
                    {
                        if (offence1 != null && offence3 != null)
                        {
                            if (offence1.OffCodeAlternate == offence3.OffCode)
                            {
                                chgAlternateCode1 = offence3.OffCode;
                            }
                        }
                        if (offence2 != null && offence3 != null)
                        {
                            if (offence2.OffCodeAlternate == offence3.OffCode)
                            {
                                chgAlternateCode2 = offence3.OffCode;
                            }
                        }
                        if (offence1 != null && offence2 != null)
                        {
                            if (offence1.OffCodeAlternate == offence2.OffCode)
                            {
                                chgAlternateCode1 = offence2.OffCode;
                            }
                        }

                    }

                }
            }



        }

        public string GetFilmNo()
        {

            string currentDate = DateTime.Now.ToString("yyyy-MM-dd");
            string prefix = "mob";
            string filmNo = prefix + "-" + currentDate.Replace("-", "");
            return filmNo;
        }

        /// <summary>
        /// Get LoSuIntNo
        /// </summary>
        /// <param name="autIntNo"></param>
        /// <param name="locCode"></param>
        /// <returns></returns>
        public int GetLoSuIntNoByAutAndLocationSuburb(int autIntNo, string LoSuDescr)
        {
            int loSuIntNo = 0;
            string violationType = string.Empty;
            using (SqlConnection conn = new SqlConnection(connectStr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand("GetLoSuIntNoByAutAndLocationSuburb", conn) { CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new SqlParameter("@AutIntNo", autIntNo) { DbType = DbType.Int32 });

                        SqlParameter parameterLocDesc = new SqlParameter("@LoSuDescr", SqlDbType.NVarChar, 100);
                        parameterLocDesc.Value = LoSuDescr;
                        cmd.Parameters.Add(parameterLocDesc);

                        if (conn.State != ConnectionState.Open)
                            conn.Open();
                        object value = cmd.ExecuteScalar();
                        if (value != null)
                            loSuIntNo = Convert.ToInt32(value.ToString().Trim());
                        return loSuIntNo;
                    }
                }
                catch (Exception ex)
                {
                    conn.Close();
                    throw ex;
                }
            }


        }

        public List<ChargeDetails> getTheDealList(List<ChargeDetails> chargeList)
        {
            //if not fine the offence code ,the main flag is true
            for (int i = 0; i < chargeList.Count; i++)
            {
                ChargeDetails detail = chargeList[i];
                if (AARTONotice.GetTheOffence(detail.IsMain, detail.ChargeOffenceCode) == null)
                {
                    chargeList[i].IsMain = "y";
                }
            }
            //if the main flag is n but can't find the main offence code then ismain flag is y
            for (int y = 0; y < chargeList.Count; y++)
            {
                ChargeDetails detail = chargeList[y];
                if (detail.IsMain.ToLower() == "n")
                {
                    SIL.AARTO.DAL.Entities.Offence offence = AARTONotice.GetTheOffence(detail.IsMain, detail.ChargeOffenceCode);
                    List<ChargeDetails> list = chargeList.Where(n => n.ChargeOffenceCode == offence.OffCode & n.IsMain.ToLower() == "y").ToList();
                    if (list.Count == 0)
                    {
                        chargeList[y].IsMain = "y";
                    }
                    else
                    {
                        chargeList[y].PublicCode = offence.OffCode;
                    }
                }
            }
            // set group 
            chargeList.Sort(new ComparerChargeDetail());
            List<ChargeDetails> copyList = new List<ChargeDetails>();
            copyList.AddRange(chargeList);
            //create group 
            List<ChargeIsMain> chargeIsMainList = new List<ChargeIsMain>();
            foreach (ChargeDetails detail in chargeList)
            {
                if (detail.IsMain.ToLower() == "y")
                {
                    ChargeIsMain chargeIsMain = new ChargeIsMain();
                    chargeIsMain.MainCharge = detail;
                    List<ChargeDetails> copyList3 = copyList.Where(nn => nn.IsMain.ToLower() == "n").ToList();
                    List<ChargeDetails> alterChargeList = new List<ChargeDetails>();
                    foreach (ChargeDetails copyDetail in copyList3)
                    {
                        SIL.AARTO.DAL.Entities.Offence offence = AARTONotice.GetTheOffence(copyDetail.IsMain, copyDetail.ChargeOffenceCode);
                        if (detail.ChargeOffenceCode == offence.OffCode)
                        {
                            copyDetail.PublicCode = offence.OffCode;
                            alterChargeList.Add(copyDetail);
                        }
                    }
                    if (alterChargeList.Count > 0)
                        chargeIsMain.AlterCharge = alterChargeList;
                    chargeIsMainList.Add(chargeIsMain);
                }

            }
            //count the details 
            int count = chargeIsMainList.Count;
            foreach (ChargeIsMain isMain in chargeIsMainList)
            {
                count += (isMain.AlterCharge == null ? 0 : isMain.AlterCharge.Count);
            }
            chargeList.Clear();
            //add the index each detail
            foreach (ChargeIsMain isMain in chargeIsMainList)
            {
                isMain.MainCharge.indexId = count;
                chargeList.Add(isMain.MainCharge);
                count--;
                if (isMain.AlterCharge != null)
                {
                    foreach (ChargeDetails isDetail in isMain.AlterCharge)
                    {
                        if (isDetail != null)
                        {
                            isDetail.indexId = count;
                            chargeList.Add(isDetail);
                            count--;
                        }
                    }
                }
            }
            //sort the detail
            chargeList.Sort(new ComparerChargeDetailByIndex());
            return chargeList;
        }

        #endregion

    }

    class ChargeIsMain
    {
        public ChargeDetails MainCharge { get; set; }
        public List<ChargeDetails> AlterCharge { get; set; }
    }
}
