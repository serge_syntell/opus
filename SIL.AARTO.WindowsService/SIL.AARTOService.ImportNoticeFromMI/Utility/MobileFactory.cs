﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SIL.AARTOService.ImportNoticeFromMI.Utility
{
    public class MobileFactory
    {
        public static MobileData CreateDataInstance(string DocumentTypeId, string connectStr)
        {
            if (!string.IsNullOrEmpty(DocumentTypeId))
            {
                DocumentTypeId = DocumentTypeId.Trim();

                string assmName = "SIL.AARTOService.ImportNoticeFromMI";
                string className = DocumentTypeId + "Data";
                Assembly assm = Assembly.Load(assmName);
                MobileData mobiledata = (MobileData)assm.CreateInstance(assmName + "." + DocumentTypeId + "." + className);
                if (mobiledata != null)
                {
                    mobiledata.connectStr = connectStr;
                }

                return mobiledata;
            }
            return null;
        }
    }
}
