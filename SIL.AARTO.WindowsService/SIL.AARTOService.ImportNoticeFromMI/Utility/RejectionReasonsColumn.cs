﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.MobileInterface.DAL.Entities;

namespace SIL.AARTOService.ImportNoticeFromMI.Utility
{
    public class RejectionReasonsColumn
    {
        public RejectionReasonsColumn(MobileNoticeRejectionReasonsList rejectionReason, string rejectionColumn)
        {
            this.rejectionReason = rejectionReason;
            this.rejectionColumn = rejectionColumn;
        }

        private MobileNoticeRejectionReasonsList rejectionReason;
        private string rejectionColumn;

        public MobileNoticeRejectionReasonsList RejectionReason
        {
            get
            {
                return rejectionReason;
            }
            set
            {
                rejectionReason = value;
            }

        }
        public string RejectionColumn
        {
            get
            {
                return rejectionColumn;
            }
            set
            {
                rejectionColumn = value;
            }
        }

    }
}
