﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTOService.ImportNoticeFromMI.Utility;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.QueueLibrary;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.ServiceQueueLibrary.DAL.Services;
using Stalberg.TMS;
using Stalberg.TMS.Data;
using Stalberg.TMS.Data.Util;
using mobileEntities = SIL.MobileInterface.DAL.Entities;
using mobileService = SIL.MobileInterface.DAL.Services;

namespace SIL.AARTOService.ImportNoticeFromMI.S56Mobile
{
    public class S56MobileData : MobileData
    {
        #region variable

        readonly Regex REG_CRT_ROOM_NO = new Regex("^[0-9,a-z,A-Z]{0,5}$");
        //public DateTime baseDate1900 = new DateTime(1900, 1, 1);

        List<TrafficOfficer> officerList = new List<TrafficOfficer>();
        TrafficOfficer trafficofficerEntity = new TrafficOfficer();
        TrafficOfficerService service = new TrafficOfficerService();

        List<OfficerGroup> officerGroupList = new List<OfficerGroup>();
        OfficerGroupService officerGroupService = new OfficerGroupService();
        OfficerGroup officerGroupEntity = new OfficerGroup();

        CourtService courtService = new CourtService();
        CourtRoomService courtRoomService = new CourtRoomService();

        AuthorityService authorityService = new AuthorityService();
        SIL.AARTO.DAL.Entities.Authority authorityEntity = new SIL.AARTO.DAL.Entities.Authority();

        NoticeSummons noticeSummons = new NoticeSummons();
        SIL.AARTO.DAL.Entities.Summons summonsEntity = new AARTO.DAL.Entities.Summons();

        SIL.MobileInterface.DAL.Entities.NoticeReceivedFromMobileDevice nrfmd = new mobileEntities.NoticeReceivedFromMobileDevice();
        SIL.MobileInterface.DAL.Services.NoticeReceivedFromMobileDeviceService nrfmds = new mobileService.NoticeReceivedFromMobileDeviceService();

        ImageFileServer imageFileServer = new ImageFileServer();
        ImageFileServerService imageFileServerService = new ImageFileServerService();
        IList<ImageFileServer> ImageFileServerList;

        //Heidi 2014-04-29 added to push queue WithdrawSec56WithOfficerError(5239)
        SIL.AARTO.DAL.Services.NoticeService _noticeService = new NoticeService();
        SIL.AARTO.DAL.Entities.Notice noticeEntity = new AARTO.DAL.Entities.Notice();
        ServiceQueueService _serviceQueueService = new ServiceQueueService();
        NoticeSummonsService _noticeSummonsService = new NoticeSummonsService();
        SummonsService _summonsService = new SummonsService();
        SIL.ServiceQueueLibrary.DAL.Data.ServiceQueueQuery _serviceQueueQuery =
        new SIL.ServiceQueueLibrary.DAL.Data.ServiceQueueQuery();
        AuthorityRuleInfo _authorityRuleInfo = new AuthorityRuleInfo();
        DateRuleInfo _dateRuleInfo = new DateRuleInfo();
        ImportHandwrittenOffences importHandwrittenOffences = null;
        SIL.MobileInterface.DAL.Entities.DocumentStatusList docuemntStatus;
        QueueItemProcessor queProcessor = new QueueItemProcessor();

        PrintFileNameService pfnSvc = new PrintFileNameService();
        PrintFileNameSummonsService pfnsSvc = new PrintFileNameSummonsService();
        PrintFileName pfnEntity = new PrintFileName();

        MobileXMLData mobileXMLData = new MobileXMLData();
        OffenceService offenceService = new OffenceService();
        List<Offence> offenceList;
        #endregion

        #region validation

        public override List<int> ValidatingMobileDataOfficerError(Notices notices)
        {
            List<int> errorlist = new List<int>();

            //ID Number
            if (!CheckOffenderIDNumberOnlyCheckIsNull(notices.OffenderDetails))
            {
                errorlist.Add((int)AartoOfficerErrorList.NoIDNumberOnSection56Document);
            }

            //Reg No
            CheckVehicleDetailForS56(errorlist, notices.VehicleDetails);

            //Valid Officer Code
            if (!CheckMobileGeneralValidationByType("OfficerCode", notices))
            {
                errorlist.Add((int)AartoOfficerErrorList.NoOfficerDetails);
            }

            //Valid Officer Code
            if (!CheckOfficer(notices.OfficerNumber))
            {
                errorlist.Add((int)AartoOfficerErrorList.NoOfficerDetails);
            }

            //offencedate
            CheckOffenceDetailForS56(errorlist, notices.OffenceDetails);

            //Charge(1/2/3) Amount ,isByLaw//chargeDropDownList(Description Format)
            //charge1Amount,charge2Amount,charge3Amount against the database fine amount
            //Alternate charge
            CheckChargeDetailForS56(errorlist, notices);

            if (!ValidateWorkingDateForCourtDate(notices.CourtDetails))
            {
                errorlist.Add((int)AartoOfficerErrorList.IncorrectCourtDate);
            }

            //offencedate<PaymentDate<CourtDate
            if (!CheckOffenceDateCourtDatePamentDate(notices))
            {
                errorlist.Add((int)AartoOfficerErrorList.IncorrectOrNoOffenceDate);
            }

            if (notices.OffenceDetails.IsSpeedOffence)
            {
                if (!CheckSpeedLimit(notices))
                {
                    errorlist.Add((int)AartoOfficerErrorList.NoticeSummonsIncomplete);
                }
            }

            if (notices.OffenceDetails.IsSpeedOffence)
            {
                //SpeedReading
                if (!CheckSpeedReading(notices))
                {
                    errorlist.Add((int)AartoOfficerErrorList.NoticeSummonsIncomplete);
                }
            }

            //regno//Surname//first name//LocationSuburb
            CheckRequird(errorlist, notices);

            //offenderAge
            if (!CheckOffenderAge(notices))
            {
                errorlist.Add((int)AartoOfficerErrorList.Other);
            }

            //Valid Court
            if (!CheckCourtDetail(notices.CourtDetails))
            {
                errorlist.Add((int)AartoOfficerErrorList.IncorrectOrNoCourtDetails);
            }

            //Valid Court,CourtRoom,CourtDate relationship
            if (!CheckMobileGeneralValidationByType("CourtRoom", notices))
            {
                errorlist.Add((int)AartoOfficerErrorList.IncorrectCourtDate);
            }

            //2014-10-28 Heidi remove validation because not need validation it(5376)
            //Valid PostalCode suburb relationship  
            //if (!CheckMobileGeneralValidationByType("PostalCode", notices))
            //{
            //    errorlist.Add((int)AartoOfficerErrorList.NoAddressRegistration);
            //}

            //2014-10-28 Heidi remove validation because not need validation it(5376)
            //if (!CheckSignatureImage(notices)) 
            //{
            //    errorlist.Add((int)AartoOfficerErrorList.NoSignatureSupplied);
            //}

            return errorlist;
        }

        public override List<RejectionReasonsColumn> ValidatingMobileData(Notices notices)
        {
            List<RejectionReasonsColumn> list = new List<RejectionReasonsColumn>();

            if (!S56NoticeNumberMatch(notices.DocumentType, notices))
            {
                list.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_03, "NoticeNumber"));
            }
            MobileValidationBase(list, notices);

            return list;
        }

        public void CheckChargeDetailForS56(List<int> errorlist, Notices notices)
        {
            OffenceDetails offenceDetails = notices.OffenceDetails;
            LocalAuthority localAuthority = notices.LocalAuthority;
            List<ChargeDetails> list = offenceDetails.ChargeDetails;
            string paymentDateStr = notices.CourtDetails.PaymentDate;

            decimal amount;
            Offence offence;
            int chargeCounter = 0;
            bool isNoAogflag = false;
            bool isByLaw = false;

            //List<Offence> offenceList = new List<Offence>();
            OffenceService service = new OffenceService();

            //offenceList = service.GetAll().ToList();

            if (checkChargeOffenceCodeSame(list))
            {
                //three chargeOffenceCode can't the same
                errorlist.Add((int)AartoOfficerErrorList.NoOffenceDescription);
            }

            foreach (var detail in list)
            {
                offence = service.Find(string.Format("OffCode='{0}'", detail.ChargeOffenceCode.Trim())).FirstOrDefault();
                //offence = offenceList.Where(o => o.OffCode == detail.ChargeOffenceCode.Trim()).FirstOrDefault();
                if (offence != null)
                {
                    chargeCounter = chargeCounter + 1;
                    string NoAogStr = detail.IsNoAog == null ? "" : detail.IsNoAog.Trim().ToUpper();

                    if (decimal.TryParse(detail.ChargeAmount.ToString(), out amount))
                    {
                        if ((amount <= 0 || amount >= 10000)) //Heidi 2013-08-01 added for task 4992B
                        {
                            //0<Charge(1/2/3) amount < 10000
                            errorlist.Add((int)AartoOfficerErrorList.NoOffenceDescription);
                        }
                    }
                    else
                    {   //0 < Charge(1/2/3) amount < 10000
                        errorlist.Add((int)AartoOfficerErrorList.NoOffenceDescription);
                    }



                    if (offence.OffNoAog.ToUpper() != NoAogStr)
                    {
                        //NoAog against the database
                        errorlist.Add((int)AartoOfficerErrorList.NoAdmissionOfGuilt);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(NoAogStr) && NoAogStr.ToUpper() == "Y")
                        {
                            isNoAogflag = true;
                        }

                    }

                    if (!string.IsNullOrEmpty(NoAogStr) && NoAogStr.ToUpper() == "Y")//Heidi 2013-08-01 added for task 4992B
                    {
                        if (amount != 0)
                        {   //no AOG -->amount=0
                            errorlist.Add((int)AartoOfficerErrorList.NoAdmissionOfGuilt);
                        }
                    }


                    //if (offence.OffDescr != detail.ChargeDescription)
                    //{
                    //    //valid chargeDropdrowlist
                    //    errorlist.Add((int)AartoOfficerErrorList.NoOffenceDescription);
                    //}


                    decimal dataBaseFineAmount = GetOffenceFineAmount(offence.OffIntNo, localAuthority.AutCode, offenceDetails.OffenceDate);
                    if (!string.IsNullOrEmpty(detail.ChargeAmount) && dataBaseFineAmount != amount)
                    {
                        //charge1Amount,charge2Amount,charge3Amount against the database fine amount
                        errorlist.Add((int)AartoOfficerErrorList.NoAmount);
                    }


                    if (!string.IsNullOrEmpty(detail.IsMain) && detail.IsMain.ToUpper() == "N")
                    {
                        if (!string.IsNullOrEmpty(offence.OffCode))
                        {
                            List<Offence> AlternateoffenceList = service.Find(string.Format("OffCodeAlternate='{0}'", offence.OffCode)).ToList();
                            //List<Offence> AlternateoffenceList = offenceList.Where(a => a.OffCodeAlternate == offence.OffCode).ToList();
                            if (AlternateoffenceList == null || (AlternateoffenceList != null && AlternateoffenceList.Count == 0))
                            {
                                //This alternate charge related to this main charge must contain a valid alternate offence code.
                                errorlist.Add((int)AartoOfficerErrorList.NoticeSummonsIncomplete);
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(detail.IsMain) && detail.IsMain.ToUpper() == "N")
                    {
                        if (!CheckAlternateCharge(list, detail, offence, service))
                        {
                            //The main charge related to this alternate change must be one of the other charges on the mobile notice.
                            errorlist.Add((int)AartoOfficerErrorList.NoticeSummonsIncomplete);
                        }

                    }

                    if (offence.OffIsByLaw)
                    {
                        isByLaw = true;
                    }
                }
                else
                {   //not found offence by ChargeOffenceCode for valid chargeDropdrowlist
                    errorlist.Add((int)AartoOfficerErrorList.NoOffenceDescription);
                }
            }

            if (isByLaw && notices.VehicleDetails != null)
            {
                notices.VehicleDetails.VehicleRegNumber = "XX000000";
            }

            if (isNoAogflag)
            {
                if (!string.IsNullOrEmpty(paymentDateStr))
                {
                    //NoAog no paymentdate
                    errorlist.Add((int)AartoOfficerErrorList.NoAdmissionOfGuilt);
                }

            }
        }

        public void CheckVehicleDetailForS56(List<int> errorlist, VehicleDetails detail)
        {
            if (detail != null)
            {
                //Jake2014-02-21 comment out
                //if (!CheckVechileColour(detail.VehicleColourDescr))
                //{
                //    //2014-02-19 Heidi changed for the VehicleColour validation fails,the ImportNoticeFromMI_Service service can continue to import data.
                //    //eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_08, "VehicleColour"));
                //    detail.VehicleColourDescr ="";
                //}
                //if (!CheckVehicleLicExpDate(detail.VehicleLicExpDate))
                //{
                //    errorlist.Add((int)AartoOfficerErrorList.Other);
                //}
                //Jake2014-02-21 comment out
                //if (!CheckVehicleMake(detail.VehicleMakeCode))
                //{
                //    //2014-02-19 Heidi changed for the VehicleMake validation fails,the ImportNoticeFromMI_Service service can continue to import data.
                //    //eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_10, "VehicleMake"));
                //    detail.VehicleMakeCode = "";
                //}
                //Jake2014-02-21 comment out
                //if (!CheckVehicleType(detail.VehicleTypeCode))
                //{
                //    //2014-02-19 Heidi changed for the VehicleType validation fails,the ImportNoticeFromMI_Service service can continue to import data.
                //    //eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_11, "VehicleType"));
                //    detail.VehicleTypeCode = "";
                //}
                if (!CheckVehicleRegNO(detail.VehicleRegNumber))
                {
                    //eList.Add(new RejectionReasonsColumn(mobileEntities.MobileNoticeRejectionReasonsList.R_23, "VehicleRegNumber"));
                    errorlist.Add((int)AartoOfficerErrorList.NoRegistrationNumber);
                }

            }
        }

        public void CheckOffenceDetailForS56(List<int> errorlist, OffenceDetails detail)
        {
            if (detail != null)
            {
                if (!CheckObservationTime(detail.ObservationTime))
                {
                    errorlist.Add((int)AartoOfficerErrorList.IncorrectOrNoTime);
                }

                if (!CheckOffenceDate(detail.OffenceDate))
                {
                    errorlist.Add((int)AartoOfficerErrorList.IncorrectOrNoOffenceDate);
                }
            }
            else
            {
                errorlist.Add((int)AartoOfficerErrorList.NoOffenceDescription);
            }

        }

        public bool CheckOfficerCodeForS56(string officerNumber, string autCode)
        {
            bool validationSucceed = false;
            if (!string.IsNullOrEmpty(officerNumber))
            {
                officerList = service.GetAll().ToList();
                officerGroupList = officerGroupService.GetAll().ToList();
                authorityEntity = authorityService.GetByAutCode(autCode.Trim());

                if (officerList != null)
                {
                    trafficofficerEntity = officerList.Where(o => o.ToNo == officerNumber.Trim()).FirstOrDefault();

                    if (trafficofficerEntity != null)
                    {
                        officerGroupEntity = officerGroupList.Where(o => o.OfGrIntNo == trafficofficerEntity.OfGrIntNo).FirstOrDefault();
                        if (officerGroupEntity != null)
                        {
                            if (authorityEntity != null && authorityEntity.MtrIntNo == trafficofficerEntity.MtrIntNo)
                            {
                                validationSucceed = true;
                            }
                        }
                    }
                }
            }

            return validationSucceed;
        }

        public bool CheckOffenceDateCourtDatePamentDate(Notices notices)
        {
            bool validationSucceed = false;
            try
            {

                OffenceDetails offenceDetails = notices.OffenceDetails;

                string offenceDateStr = "", courtDateStr = "", paymentDateStr = "";
                DateTime offenceDate, courtDate, paymentDate;

                if (notices.OffenceDetails != null)
                {
                    offenceDateStr = notices.OffenceDetails.OffenceDate;
                }
                if (notices.CourtDetails != null)
                {
                    courtDateStr = notices.CourtDetails.CourtDate;
                    paymentDateStr = notices.CourtDetails.PaymentDate;
                }

                if (!string.IsNullOrEmpty(offenceDateStr) && !string.IsNullOrEmpty(courtDateStr) && !string.IsNullOrEmpty(paymentDateStr))
                {
                    if (DateTime.TryParse(offenceDateStr, out offenceDate) && DateTime.TryParse(courtDateStr, out courtDate) && DateTime.TryParse(paymentDateStr, out paymentDate))
                    {
                        if ((offenceDate.CompareTo(paymentDate) < 0) && (offenceDate.CompareTo(courtDate) < 0) && (paymentDate.CompareTo(courtDate) < 0))
                        {
                            validationSucceed = true;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                validationSucceed = false;
            }

            return validationSucceed;

        }

        public bool CheckSpeedLimit(Notices notices)
        {
            bool validationSucceed = true;

            try
            {
                string speedLimintStr = notices.OffenceDetails.SpeedLimit;

                if (!string.IsNullOrEmpty(speedLimintStr))
                {
                    int speedLimit = 0;
                    if (!int.TryParse(speedLimintStr, out speedLimit))
                    {
                        validationSucceed = false;
                    }
                    else
                    {
                        if (speedLimit > 120 || speedLimit <= 0)
                        {
                            validationSucceed = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                validationSucceed = false;
            }

            return validationSucceed;

        }

        public bool CheckSpeedReading(Notices notices)
        {
            bool validationSucceed = true;

            try
            {
                string speedReading1Str = notices.OffenceDetails.SpeedReading1;
                string speedReading2Str = notices.OffenceDetails.SpeedReading2;

                if (!string.IsNullOrEmpty(speedReading1Str))
                {
                    int speedReading = 0;
                    if (!int.TryParse(speedReading1Str, out speedReading))
                    {
                        validationSucceed = false;
                        return validationSucceed;
                    }
                    else
                    {
                        if (speedReading > 280 || speedReading <= 0)
                        {
                            validationSucceed = false;
                            return validationSucceed;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(speedReading2Str))
                {
                    int speedReading = 0;
                    if (!int.TryParse(speedReading2Str, out speedReading))
                    {
                        validationSucceed = false;
                        return validationSucceed;
                    }
                    else
                    {
                        if (speedReading > 280 || speedReading <= 0)
                        {
                            validationSucceed = false;
                            return validationSucceed;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                validationSucceed = false;
            }

            return validationSucceed;

        }

        public void CheckRequird(List<int> errorlist, Notices notices)
        {
            OffenceDetails offenceDetails = notices.OffenceDetails;
            OffenderDetails offenderDetails = notices.OffenderDetails;
            VehicleDetails vehicleDetails = notices.VehicleDetails;

            if (offenderDetails != null)
            {
                if (string.IsNullOrEmpty(offenderDetails.OffenderSurname))
                {
                    //Surname requird
                    errorlist.Add((int)AartoOfficerErrorList.NoSurnameOrIDNumber);
                }

                if (string.IsNullOrEmpty(offenderDetails.OffenderName))
                {
                    //first name requird
                    errorlist.Add((int)AartoOfficerErrorList.NoSurnameOrIDNumber);
                }
            }
            else
            {
                //Surname requird
                errorlist.Add((int)AartoOfficerErrorList.NoSurnameOrIDNumber);
                //first name requird
                errorlist.Add((int)AartoOfficerErrorList.NoSurnameOrIDNumber);

            }


            if (offenceDetails != null)
            {
                if (string.IsNullOrEmpty(offenceDetails.PlaceOfOffence))
                {
                    //place Description requird
                    errorlist.Add((int)AartoOfficerErrorList.NoAddressRegistration);
                }
            }
            else
            {
                //place Description requird
                errorlist.Add((int)AartoOfficerErrorList.NoAddressRegistration);
            }

            if (string.IsNullOrEmpty(notices.LocationSuburb))
            {
                //LocationSuburb requird
                errorlist.Add((int)AartoOfficerErrorList.NoSuburb);
            }

            if (vehicleDetails != null)
            {
                if (string.IsNullOrEmpty(vehicleDetails.VehicleRegNumber))
                {
                    errorlist.Add((int)AartoOfficerErrorList.NoRegistrationNumber);
                }
            }
            else
            {
                errorlist.Add((int)AartoOfficerErrorList.NoRegistrationNumber);
            }
        }



        public bool S56NoticeNumberMatch(string noticeType, Notices notice)
        {
            bool flag = false;

            if (notice.LocalAuthority != null)
            {
                AuthorityService service = new AuthorityService();
                SIL.AARTO.DAL.Entities.Authority authority = service.GetByAutCode(notice.LocalAuthority.AutCode);
                if (authority != null)
                {
                    notice.LocalAuthority.AutIntNo = authority.AutIntNo;
                }
            }

            if (notice.LocalAuthority.AutIntNo > 0)
            {
                flag = ValidateNoticeNumberS56(notice.NoticeNumber, notice.LocalAuthority.AutIntNo);
            }
            return flag;
        }

        public bool ValidateNoticeNumberS56(string NoticeNumber, int autIntNo)
        {
            bool validate = false;

            string validateType = "cdv";
            if (NoticeNumber.IndexOf("/") > 0) { validateType = "cdv"; }
            else { validateType = "verhoeff"; }
            if (!Validation.GetInstance(connectStr).ValidateTicketNumber(NoticeNumber, validateType))
            {
                if (NoticeNumber.IndexOf("/") > 0)
                {
                    if (NoticeNumber.Split('/').Length == 4)
                    {
                        string prefix = NoticeNumber.Split('/')[0];
                        SIL.AARTO.DAL.Entities.TList<NoticePrefixHistory> noticePrefixList = new NoticePrefixHistoryService().GetByNprefixAndAuthIntNo(prefix, autIntNo);

                        NoticePrefixHistory noticePrefix = noticePrefixList.Where(m => m.NstIntNo == (int)NoticeStatisticalTypeList.M56).FirstOrDefault();
                        if (noticePrefix != null)
                        {
                            if (noticePrefix.NphType == "M")
                            {
                                validate = true;
                            }
                        }
                    }
                }
                else
                {
                    if (NoticeNumber.Split('-').Length == 4)
                    {
                        string prefix = NoticeNumber.Split('-')[0];
                        SIL.AARTO.DAL.Entities.TList<NoticePrefixHistory> noticePrefixList = new NoticePrefixHistoryService().GetByNprefixAndAuthIntNo(prefix, autIntNo);

                        NoticePrefixHistory noticePrefix = noticePrefixList.Where(m => m.NstIntNo == (int)NoticeStatisticalTypeList.M56).FirstOrDefault();
                        if (noticePrefix != null)
                        {
                            if (noticePrefix.NphType == "M")
                            {
                                validate = true;
                            }
                        }
                    }
                }
            }
            return validate;
        }

        public bool ValidateWorkingDateForCourtDate(CourtDetails courtDetails)
        {
            bool validationSucceed = false;
            if (courtDetails != null)
            {

                DateTime _coutdate = DateTime.MinValue;
                string returnValue = "";

                DateTime.TryParse(courtDetails.CourtDate, out _coutdate);

                if (_coutdate == DateTime.MinValue)
                {
                    validationSucceed = false;
                    return validationSucceed;
                }
                else
                {
                    if (_coutdate <= baseDate1900)
                    {
                        validationSucceed = false;
                        return validationSucceed;
                    }
                }

                try
                {
                    using (IDataReader reader = new SIL.AARTO.DAL.Services.CourtDatesService().CheckWorkingDay(_coutdate))
                    {
                        if (reader.Read())
                        {
                            returnValue = reader["WorkDay"].ToString();
                        }
                        else
                        {
                            returnValue = "";
                        }
                    }
                }
                catch (Exception e)
                {
                    validationSucceed = false;
                }

                if (!string.IsNullOrEmpty(returnValue) && returnValue == "N")
                {
                    // ValidationTypeList.InvalidCourtDate
                    validationSucceed = true;
                }
            }

            return validationSucceed;
        }

        #endregion

        #region Import

        public override bool DoImport(mobileEntities.NoticeReceivedFromMobileDevice item, Notices notice, string officerError, bool isOfficerError, string batchBaseFileName, int delayHours, AARTOServiceBase AARTOBase, List<int> pfnList, ref int notIntNo, ref string errorMsg)
        {
            importHandwrittenOffences = new ImportHandwrittenOffences(connectStr);
            string filmNo = GetFilmNo();
            int autIntNo = notice.LocalAuthority.AutIntNo;
            string autCode = notice.LocalAuthority.AutCode;
            int ifsIntNo = 0;
            string imagePartPath = "";
            string notTicketNo = "";
            //offenceList = offenceService.GetAll().ToList();
            try
            {
                using (TransactionScope scope = ServiceUtility.CreateTransactionScope())
                {
                    string conCode = ConfigurationManager.AppSettings["ContractorCode"].ToString();
                    string frameNo = "";

                    //2014-10-30 Heidi changed(5376)
                    List<ChargeDetails> chargeList = notice.OffenceDetails.ChargeDetails.OrderByDescending(r => r.IsMain).ToList();
                    //List<ChargeDetails> chargeList =getTheDealList(notice.OffenceDetails.ChargeDetails);
                    //need deal the chargeList again
                    int count = chargeList.Count;

                    #region Notice
                    notTicketNo = notice.NoticeNumber;

                    int speedLimit = 0;
                    if (!string.IsNullOrEmpty(notice.OffenceDetails.SpeedLimit))
                    {
                        int.TryParse(notice.OffenceDetails.SpeedLimit.Trim(), out speedLimit);
                    }

                    int speedReading1 = 0;
                    if (!string.IsNullOrEmpty(notice.OffenceDetails.SpeedReading1))
                    {
                        int.TryParse(notice.OffenceDetails.SpeedReading1.Trim(), out speedReading1);
                    }
                    int speedReading2 = 0;
                    if (!string.IsNullOrEmpty(notice.OffenceDetails.SpeedReading2))
                    {
                        int.TryParse(notice.OffenceDetails.SpeedReading2.Trim(), out speedReading2);
                    }

                    DateTime offenceDate = DateTime.MinValue;
                    string offenceDateStr = notice.OffenceDetails.OffenceDate;
                    DateTime.TryParse(offenceDateStr, out offenceDate);
                    if (offenceDate.Equals(DateTime.MinValue))
                    {
                        offenceDate = DateTime.Parse("1900-01-01");
                    }

                    string notLocDescr = notice.OffenceDetails.PlaceOfOffence == null ? "" : notice.OffenceDetails.PlaceOfOffence.Trim();

                    string notRegNo = notice.VehicleDetails.VehicleRegNumber == null ? "" : notice.VehicleDetails.VehicleRegNumber.Trim();
                    notRegNo = notRegNo == "XX000000" ? string.Empty : notRegNo; // 2013-04-28 add by Henry for by-law

                    string regNo2 = "";

                    string notVehicleMakeCode = string.Empty;
                    string notVehicleMake = string.Empty;
                    string notVehicleTypeCode = string.Empty;
                    string notVehicleType = string.Empty;
                    string vColor = string.Empty;

                    VehicleMake vehicleMake = null;
                    VehicleType vehicleType = null;

                    if (!string.IsNullOrEmpty(notice.VehicleDetails.VehicleMakeCode))
                    {
                        vehicleMake = AARTONotice.GetVehicleMakeByCode(notice.VehicleDetails.VehicleMakeCode, ref errorMsg);
                    }
                    if (!string.IsNullOrEmpty(notice.VehicleDetails.VehicleTypeCode))
                    {
                        vehicleType = AARTONotice.GetVehicleTypeByCode(notice.VehicleDetails.VehicleTypeCode, ref errorMsg);
                    }

                    //We are expecting the client to pass in the code however they will probably send us the description.
                    //We will try to do the lookup based on the code firstly and then treat it as the description if no row can be found after the lookup
                    //jake 2014-02-21 added
                    if (vehicleMake == null)
                    {
                        notVehicleMake = notice.VehicleDetails.VehicleMakeCode;
                        notVehicleMakeCode = "";
                    }
                    else
                    {
                        notVehicleMakeCode = vehicleMake.VmCode;
                        notVehicleMake = vehicleMake.VmDescr;
                    }

                    if (vehicleType == null)
                    {
                        notVehicleType = notice.VehicleDetails.VehicleTypeCode;
                        notVehicleTypeCode = "";
                    }
                    else
                    {
                        notVehicleTypeCode = vehicleType.VtCode;
                        notVehicleType = vehicleType.VtDescr;
                    }
                    //Notice table VehicleColor column length = nvarchar(3)
                    vColor = notice.VehicleDetails.VehicleColourDescr == null ? "" : notice.VehicleDetails.VehicleColourDescr;
                    if (vColor.Length > 3)
                    {
                        vColor = vColor.Substring(0, 3);
                    }

                    DateTime? VehicleLicExpDate = null;
                    if (!string.IsNullOrEmpty(notice.VehicleDetails.VehicleLicExpDate))
                    {
                        DateTime ExpDate;
                        DateTime.TryParse(notice.VehicleDetails.VehicleLicExpDate, out ExpDate);
                        if (ExpDate > DateTime.Parse("1900-01-01"))
                        {
                            VehicleLicExpDate = Convert.ToDateTime(notice.VehicleDetails.VehicleLicExpDate);
                        }
                    }

                    string notOfficerNo = notice.OfficerNumberOnDuty;
                    string notOfficerSname = notice.OfficerNameOnDuty;//officerSurname
                    string notOfficerGroup = notice.OfficerGroupCode;//notOfficerGroup
                    string notDmsCaptureClerk = notice.OfficerNameOnDuty;//notDMSCaptureClerk //null
                    DateTime? notDmsCaptureDate = DateTime.Now;//notDMSCaptureDate
                    string scanDate = "";

                    #endregion

                    #region Charge

                    string chgOffenceCode1 = "";
                    string chgOffenceDescr1 = "";
                    float chgFineAmount1 = 0;
                    string chgAlternateOffenceCode1 = "";
                    string chgNoAog1 = "";
                    bool chgIsMain1 = false; //2014-10-30 Heidi added(5376)
                    string chgOffenceCode2 = "";
                    string chgOffenceDescr2 = "";
                    float chgFineAmount2 = 0;
                    string chgAlternateOffenceCode2 = "";
                    string chgNoAog2 = "";
                    bool chgIsMain2 = false;//2014-10-30 Heidi added(5376)
                    string chgOffenceCode3 = "";
                    string chgOffenceDescr3 = "";
                    float chgFineAmount3 = 0;
                    string chgAlternateOffenceCode3 = "";
                    string chgNoAog3 = "";
                    bool chgIsMain3 = false;//2014-10-30 Heidi added(5376)

                    string chgAlternateOffenceCode = "";

                    if (count > 0)
                    {
                        GetChargeDetail(chargeList[0], ref chgOffenceCode1, ref chgOffenceDescr1, ref chgFineAmount1, ref chgAlternateOffenceCode, ref chgNoAog1, ref chgIsMain1);
                    }
                    if (count > 1)
                    {
                        GetChargeDetail(chargeList[1], ref chgOffenceCode2, ref chgOffenceDescr2, ref chgFineAmount2, ref chgAlternateOffenceCode, ref chgNoAog2, ref chgIsMain2);
                    }
                    if (count > 2)
                    {
                        GetChargeDetail(chargeList[2], ref chgOffenceCode3, ref chgOffenceDescr3, ref chgFineAmount3, ref chgAlternateOffenceCode, ref chgNoAog3, ref chgIsMain3);
                    }

                    //2014-10-30 Heidi added(5376)
                    //GetchgAlternateChangeCode(chargeList, offenceList, ref chgAlternateOffenceCode1, ref chgAlternateOffenceCode2, ref chgAlternateOffenceCode3);

                    DateTime? notPaymentDate = null;
                    if (!string.IsNullOrEmpty(notice.CourtDetails.PaymentDate))
                    {
                        notPaymentDate = Convert.ToDateTime(notice.CourtDetails.PaymentDate);
                    }

                    string chgOfficerNatisNo = notice.OfficerNumberOnDuty;
                    string chgOfficerName = notice.OfficerNameOnDuty;

                    #endregion

                    #region Summons

                    DateTime? courtDate = null;
                    DateTime tempCourtDate = DateTime.MinValue;
                    string courtDateStr = notice.CourtDetails.CourtDate;
                    DateTime.TryParse(courtDateStr, out tempCourtDate);
                    if (tempCourtDate.Equals(DateTime.MinValue))
                    {
                        tempCourtDate = DateTime.Parse("1901-01-01");
                    }
                    courtDate = tempCourtDate;

                    string sumOfficerNo = notice.OfficerNumberOnDuty;
                    string sumCaseNo = "";
                    string crtNo = notice.CourtDetails.CourtCode;
                    string crtRoomNo = GetCourtRoomNoByCrtNoAndCourtRoom(crtNo, notice.CourtDetails.CourtRoom);

                    #endregion

                    #region Accused

                    string accSurname = notice.OffenderDetails.OffenderSurname == null ? "" : notice.OffenderDetails.OffenderSurname;
                    string accForenames = notice.OffenderDetails.OffenderName == null ? "" : notice.OffenderDetails.OffenderName;
                    string IDType = notice.OffenderDetails.OffenderIDType == null ? "" : notice.OffenderDetails.OffenderIDType;
                    string accIdNumber = notice.OffenderDetails.OffenderIDNumber == null ? "" : notice.OffenderDetails.OffenderIDNumber.Trim().ToUpper();
                    string accSex = notice.OffenderDetails.OffenderSex.Trim();
                    string accOccupation = notice.OffenderDetails.OffenderOccupation == null ? "" : notice.OffenderDetails.OffenderOccupation;
                    int age = 0; int.TryParse(notice.OffenderDetails.OffenderAge, out age);
                    string accAge = age.ToString();

                    bool overAgeStatus = false;
                    try
                    {
                        // Jake 2013-05-27 modified
                        int _age = 0;
                        Int32.TryParse(accAge, out _age);
                        overAgeStatus = _age > 18;
                    }
                    catch { overAgeStatus = false; }

                    #region address

                    string accAddress1 = (string.IsNullOrEmpty(notice.OffenderDetails.Address1) ? "" : notice.OffenderDetails.Address1);
                    string accAddress2 = (string.IsNullOrEmpty(notice.OffenderDetails.Address2) ? "" : notice.OffenderDetails.Address2);
                    string accAddress3 = (string.IsNullOrEmpty(notice.OffenderDetails.Address3) ? "" : notice.OffenderDetails.Address3);
                    string accAddress4 = (string.IsNullOrEmpty(notice.OffenderDetails.Address4) ? "" : notice.OffenderDetails.Address4);
                    string accAddressCode = notice.OffenderDetails.AddressCode == null ? "" : notice.OffenderDetails.AddressCode;

                    string accBusinessAddress1 = (string.IsNullOrEmpty(notice.OffenderDetails.BusinessAddress1) ? "" : notice.OffenderDetails.BusinessAddress1);
                    string accBusinessAddress2 = (string.IsNullOrEmpty(notice.OffenderDetails.BusinessAddress2) ? "" : notice.OffenderDetails.BusinessAddress2);
                    string accBusinessAddress3 = (string.IsNullOrEmpty(notice.OffenderDetails.BusinessAddress3) ? "" : notice.OffenderDetails.BusinessAddress3);
                    string accBusinessAddress4 = (string.IsNullOrEmpty(notice.OffenderDetails.BusinessAddress4) ? "" : notice.OffenderDetails.BusinessAddress4);
                    string accBusinessAddressCode = notice.OffenderDetails.BusinessAddressCode == null ? "" : notice.OffenderDetails.BusinessAddressCode;

                    string accAddressTelephone = (string.IsNullOrEmpty(notice.OffenderDetails.AddressTelephone) ? "" : notice.OffenderDetails.AddressTelephone);
                    string accBusinessTelephone = (string.IsNullOrEmpty(notice.OffenderDetails.BusinessTelephone) ? "" : notice.OffenderDetails.BusinessTelephone);

                    #endregion

                    string nationality = notice.OffenderDetails.OffenderNationality;

                    string accPDPNumber = (string.IsNullOrEmpty(notice.OffenderDetails.OffenderPDPNumber) ? "" : notice.OffenderDetails.OffenderPDPNumber);
                    string accLicenceNumber = (string.IsNullOrEmpty(notice.OffenderDetails.OffenderLicenceNumber) ? "" : notice.OffenderDetails.OffenderLicenceNumber);
                    string accLicenceCode = (string.IsNullOrEmpty(notice.OffenderDetails.OffenderLicenceCode) ? "" : notice.OffenderDetails.OffenderLicenceCode);
                    string accEmailAddress = (string.IsNullOrEmpty(notice.OffenderDetails.OffenderEmailAddress) ? "" : notice.OffenderDetails.OffenderEmailAddress);

                    bool isTaxi = notice.VehicleDetails.IsTaxi == null ? false : notice.VehicleDetails.IsTaxi;
                    #endregion


                    #region locationSuburb

                    //2013-05-22 Heidi added for insert into LoSuIntNo to Notice Table
                    int loSuIntNo = GetLoSuIntNoByAutAndLocationSuburb(autIntNo, string.IsNullOrEmpty(notice.LocationSuburb) ? "" : notice.LocationSuburb.Trim());

                    #endregion

                    #region ImageFileServer

                    ImageFileServerList = imageFileServerService.GetByAaSysFileTypeId((int)AartoSystemFileTypeList.MobileImage);
                    if (ImageFileServerList != null && ImageFileServerList.Count > 0)
                    {
                        ifsIntNo = ImageFileServerList[0].IfsIntNo;
                        imageFileServer = ImageFileServerList[0];
                    }
                    string dateTimeNowStr = DateTime.Now.ToString("yyyy-MM-dd").Replace("-", "\\");
                    imagePartPath = autCode + "\\" + dateTimeNowStr;

                    #endregion

                    #region check ImageFileServer

                    string imageMachineNameDirectory = "\\\\" + imageFileServer.ImageMachineName + "\\" + imageFileServer.ImageShareName + "\\" + imagePartPath;

                    try
                    {
                        if (!Directory.Exists(imageMachineNameDirectory))
                        {
                            Directory.CreateDirectory(imageMachineNameDirectory);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ResourceHelper.GetResource("ImageFileServerErrorWhenImportMobileS56", imageFileServer.ImageShareName, ifsIntNo));
                    }

                    Byte[] officerSignatureImg = (string.IsNullOrEmpty(notice.OfficerSignature) ? null :
                                   Convert.FromBase64String(notice.OfficerSignature.Trim()));

                    Byte[] offenderSignatureImg = (string.IsNullOrEmpty(notice.OffenderSignature) ? null :
                        Convert.FromBase64String(notice.OffenderSignature.Trim()));

                    if (officerSignatureImg != null)
                    {
                        try
                        {
                            using (MemoryStream ms = new MemoryStream(officerSignatureImg))
                            {
                                using (Bitmap b = new Bitmap(ms))
                                { }
                            }
                        }
                        catch (Exception ex)
                        {
                            officerSignatureImg = null;
                            //2014-10-30 Heidi changed(5376)
                            //throw new Exception(ResourceHelper.GetResource("CanNotCreateofficerSignatureImgWhenImportMobile", "S56", ex.Message));
                        }
                    }

                    if (offenderSignatureImg != null)
                    {
                        try
                        {
                            using (MemoryStream ms = new MemoryStream(offenderSignatureImg))
                            {
                                using (Bitmap b = new Bitmap(ms))
                                { }
                            }
                        }
                        catch (Exception ex)
                        {
                            offenderSignatureImg = null;
                            //2014-10-30 Heidi changed(5376)
                            //throw new Exception(ResourceHelper.GetResource("CanNotCreateoffenderSignatureImgWhenImportMobile", "S56", ex.Message));
                        }
                    }


                    #endregion

                    #region AARTODocumentImage
                    string AaNoticeDocNo = filmNo;
                    int AaDocImgOrder = 1;
                    string OfficerSignatureImgType = string.IsNullOrEmpty(notice.OfficerSignatureImgType) ? "jpg" : notice.OfficerSignatureImgType.Trim();
                    string OffenderSignatureImgType = string.IsNullOrEmpty(notice.OffenderSignatureImgType) ? "jpg" : notice.OffenderSignatureImgType.Trim();
                    string imageExtension = OfficerSignatureImgType + "|" + OffenderSignatureImgType;
                    #endregion


                    #region ImportMobileOffencesSection56
                    try
                    {
                        notIntNo = ImportMobileOffencesSection56(
                            autIntNo, filmNo, frameNo, notTicketNo, speedLimit, speedReading1, speedReading2,
                            offenceDate, notLocDescr, notRegNo, notVehicleMakeCode, notVehicleMake,
                            notVehicleTypeCode, notVehicleType, vColor, VehicleLicExpDate, notOfficerNo, notOfficerSname,
                            notOfficerGroup, notDmsCaptureClerk, notDmsCaptureDate, chgOffenceCode1, chgOffenceDescr1,
                            chgOffenceCode2, chgOffenceDescr2, chgOffenceCode3,
                            chgOffenceDescr3, chgFineAmount1, chgFineAmount2, chgFineAmount3,
                            chgAlternateOffenceCode1, chgAlternateOffenceCode2, chgAlternateOffenceCode3,
                            chgNoAog1, chgNoAog2, chgNoAog3,
                            chgIsMain1, chgIsMain2, chgIsMain3,//2014-10-30 Heidi added(5376)
                            notPaymentDate, chgOfficerNatisNo, chgOfficerName, conCode, courtDate, sumOfficerNo, sumCaseNo,
                            crtNo, crtRoomNo, ifsIntNo,
                            accSurname, accForenames, IDType, accIdNumber, accOccupation, accAge, accSex,
                            accAddress1, accAddress2, accAddress3, accAddress4, accAddressCode, accBusinessAddress1, accBusinessAddress2, accBusinessAddress3, accBusinessAddress4, accBusinessAddressCode,
                            accBusinessTelephone, accAddressTelephone, nationality,
                            accPDPNumber, accLicenceNumber, accLicenceCode, accEmailAddress,
                            overAgeStatus, isOfficerError, officerError, lastUser, AaNoticeDocNo, AaDocImgOrder, imagePartPath, isTaxi, imageExtension, batchBaseFileName,
                            "M56", loSuIntNo, scanDate, "S56Mobile", regNo2);

                    }
                    catch (Exception ex)
                    {
                        scope.Dispose();
                        errorMsg = ResourceHelper.GetResource("MobileImportException", notice.DocumentType, ex.Message);
                        throw new Exception(errorMsg);
                    }
                    #endregion

                    if (notIntNo <= 0)
                    {
                        errorMsg = ResourceHelper.GetResource("MobileImportErrorForNotice", notice.DocumentType, notice.NoticeNumber);
                        scope.Dispose();
                        return DealS56Error(notIntNo, ref errorMsg);
                    }
                    else
                    {
                        noticeSummons = _noticeSummonsService.GetByNotIntNo(notIntNo).FirstOrDefault();
                        if (noticeSummons != null)
                        {
                            string summonsNoStr = "";
                            string sumPrintFileName = "";
                            string pdfImageDirectory = "";
                            string signatureImageDirectory = "";
                            int sumIntNo = noticeSummons.SumIntNo;
                            summonsEntity = _summonsService.GetBySumIntNo(sumIntNo);
                            noticeEntity = _noticeService.GetByNotIntNo(notIntNo);

                            #region Save Image

                            if (imageFileServer != null)
                            {
                                if (summonsEntity != null)
                                {
                                    sumPrintFileName = summonsEntity.MobileControlDocumentName;
                                    summonsNoStr = summonsEntity.SummonsNo.Replace("/", "");
                                    pdfImageDirectory = "\\\\" + imageFileServer.ImageMachineName + "\\" + imageFileServer.ImageShareName + "\\" + imagePartPath + "\\" + sumPrintFileName;
                                    signatureImageDirectory = "\\\\" + imageFileServer.ImageMachineName + "\\" + imageFileServer.ImageShareName + "\\" + imagePartPath + "\\" + sumPrintFileName + "\\" + summonsNoStr;

                                    try
                                    {
                                        if (!Directory.Exists(signatureImageDirectory))
                                        {
                                            Directory.CreateDirectory(signatureImageDirectory);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        throw new Exception(ResourceHelper.GetResource("ImageFileServerErrorWhenImportMobileS56", imageFileServer.ImageShareName, ifsIntNo));
                                    }

                                    if (officerSignatureImg != null)
                                    {
                                        string officerSignatureImgPath = signatureImageDirectory + "\\" + summonsNoStr + "_TOSig." + OfficerSignatureImgType;
                                        using (MemoryStream ms = new MemoryStream(officerSignatureImg))
                                        {
                                            using (Bitmap b = new Bitmap(ms))
                                            {
                                                b.Save(officerSignatureImgPath);
                                            }
                                        }
                                    }

                                    if (offenderSignatureImg != null)
                                    {
                                        string offenderSignatureImgPath = signatureImageDirectory + "\\" + summonsNoStr + "_OffSig." + OffenderSignatureImgType;
                                        using (MemoryStream ms = new MemoryStream(offenderSignatureImg))
                                        {
                                            using (Bitmap b = new Bitmap(ms))
                                            {
                                                b.Save(offenderSignatureImgPath);
                                            }
                                        }
                                    }

                                }
                            }
                            #endregion

                            #region SavePrintFileName

                            if (summonsEntity != null)
                            {

                                sumPrintFileName = summonsEntity.MobileControlDocumentName;
                                string documentPath = imagePartPath + "\\" + sumPrintFileName + "\\" + sumPrintFileName + ".pdf";

                                int addSumIntNo = summonsEntity.SumIntNo;
                                pfnEntity = pfnSvc.GetByPrintFileName(sumPrintFileName);
                                if (pfnEntity == null)
                                {
                                    pfnEntity = new PrintFileName()
                                    {
                                        PrintFileName = sumPrintFileName,
                                        MobileControlDocumentPath = documentPath,
                                        IfsIntNo = ifsIntNo,
                                        AutIntNo = autIntNo,
                                        LastUser = lastUser,
                                        EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                                    };
                                    pfnEntity = pfnSvc.Save(pfnEntity);

                                }

                                PrintFileNameSummons pfnsEntity = pfnsSvc.GetBySumIntNoPfnIntNo(addSumIntNo, pfnEntity.PfnIntNo);
                                if (pfnsEntity == null)
                                {
                                    pfnsEntity = new PrintFileNameSummons()
                                    {
                                        SumIntNo = addSumIntNo,
                                        PfnIntNo = pfnEntity.PfnIntNo,
                                        LastUser = lastUser,
                                        EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                                    };
                                    pfnsEntity = pfnsSvc.Save(pfnsEntity);
                                }

                            }
                            #endregion

                            #region push queue
                            if (noticeSummons != null)
                            {
                                #region push CancelExpiredSummons queue

                                //Jake 2015-03-17 use new date rule 
                                //_dateRuleInfo = _dateRuleInfo.GetDateRuleInfoByDRNameAutIntNo(autIntNo, "SumIssueDate", "SumExpiredDate");
                                /* Jake 2015-03-23 comment out ,no need to push this queue items for S56 summons
                                _dateRuleInfo = _dateRuleInfo.GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "SumExpireDate");
                                int noDaysForSummonsExpiry = _dateRuleInfo.ADRNoOfDays;

                                queProcessor.Send(
                                    new QueueItem()
                                    {
                                        Body = sumIntNo,
                                        Group = autCode.Trim(),
                                        ActDate = offenceDate.AddDays(noDaysForSummonsExpiry),
                                        LastUser = lastUser, //Jerry 2013-06-26 add
                                        QueueType = ServiceQueueTypeList.ExpireUnservedSummons
                                    }
                                );
                                 * */
                                #endregion

                                #region push WithdrawSec56WithOfficerError queue
                                //Heidi 2014-04-29 added to push queue WithdrawSec56WithOfficerError(5239)
                                if (summonsEntity != null && summonsEntity.SumCourtDate.HasValue)
                                {
                                    creatWithdrawSec56WithOfficerErrorQueue(notIntNo, autIntNo, autCode, Convert.ToDateTime(summonsEntity.SumCourtDate), queProcessor);
                                }
                                #endregion

                                #region push GenerateMobileS56ControlDocument queue

                                if (pfnEntity != null && noticeEntity != null)
                                {
                                    if (!pfnList.Contains(pfnEntity.PfnIntNo))
                                    {
                                        pfnList.Add(pfnEntity.PfnIntNo);

                                        string courtname = string.IsNullOrEmpty(noticeEntity.NotCourtName) ? "" : noticeEntity.NotCourtName.Trim();
                                        string group = autCode.Trim() + "|" + courtname;
                                        if (isOfficerError)
                                        {
                                            group += "|Error";
                                        }

                                        queProcessor.Send(
                                         new QueueItem()
                                         {
                                             Body = pfnEntity.PfnIntNo,
                                             Group = group,
                                             ActDate = DateTime.Now.AddHours(delayHours),
                                             LastUser = lastUser,
                                             QueueType = ServiceQueueTypeList.GenerateMobileS56ControlDocument
                                         }
                                        );
                                    }
                                }
                                #endregion
                            }
                            #endregion

                        }

                        #region deal notice mobile detail
                        AARTONotice.DealNoticeMobileDetail(notIntNo, notice, ref errorMsg);
                        #endregion

                        #region update NoticeReceivedFromMobileDevice status
                        if (isOfficerError)
                        {
                            docuemntStatus = SIL.MobileInterface.DAL.Entities.DocumentStatusList.Import_OfficerError;
                        }
                        else
                        {
                            docuemntStatus = SIL.MobileInterface.DAL.Entities.DocumentStatusList.Import_Success;
                        }
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("MobileImportSuccessful", notice.DocumentType, notice.NoticeNumber), LogType.Info);
                        mobileXMLData.UpdateReceived(item, docuemntStatus, notice.DocumentType, lastUser);
                        #endregion

                        scope.Complete();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return false;
        }

        #region ImportMobileOffencesSection56
        //2014-07-25 Heidi added for import mobile s56(5303)
        public int ImportMobileOffencesSection56(int autIntNo, string notFilmNo, string notFrameNo, string notTicketNo,
            int speedLimit, int speedReading1, int speedReading2, DateTime offenceDate, string notLocDescr, string notRegNo,
            string notVehicleMakeCode, string notVehicleMake, string notVehicleTypeCode, string notVehicleType, string notVehicleColour, DateTime? notVehicleLicenceExpiry,
            string notOfficerNo, string notOfficerSname, string notOfficerGroup,//17
            string notDMSCaptureClerk, DateTime? notDMSCaptureDate,
            string chgOffenceCode1, string chgOffenceDescr1,
            string chgOffenceCode2, string chgOffenceDescr2,
            string chgOffenceCode3, string chgOffenceDescr3,
            float chgFineAmount1, float chgFineAmount2, float chgFineAmount3,
            string chgAlternateCode1, string chgAlternateCode2, string chgAlternateCode3,
            string chgNoAog1, string chgNoAog2, string chgNoAog3,
            bool chgIsMain1, bool chgIsMain2, bool chgIsMain3,//2014-10-30 Heidi added(5376)
            DateTime? notPaymentDate, string chgOfficerNatisNo, string chgOfficerName, string conCode,

            DateTime? sumCourtDate, string sumOfficerNo, string sumCaseNo,
            string crtNo, string crtRoomNo, int ifsIntNo,//7

            string accSurname, string accForenames, string accIdType, string accIdNumber, string accOccupation, string accAge, string accSex,
            string accAddress1, string accAddress2, string accAddress3, string accAddress4, string accAddressCode, string accBusinessAddress1, string accBusinessAddress2,
            string accBusinessAddress3, string accBusinessAddress4, string accBusinessAddressCode, string accTelWork, string accTelHome, string nationality,
            string accPDPNumber, string accLicenceNumber, string accLicenceCode, string accEmailAddress,
            bool overAge, bool isOfficerError, string officerErrorString, string lastUser, string docNo,
            int aaDocImgOrder, string imageFilePath, bool isTaxi, string imageExtension, string batchBaseFileName,
            string nstCode, int loSuIntNo, string scanDate, string psttName = "S56Mobile", string regNo2 = "")//12
        {
            SqlConnection myConnection = new SqlConnection(connectStr);

            SqlCommand myCommand = new SqlCommand("ImportMobileOffencesSection56", myConnection);

            myCommand.CommandType = CommandType.StoredProcedure;

            #region Notice
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterNotFilmNo = new SqlParameter("@NotFilmNo", SqlDbType.VarChar, 20);
            parameterNotFilmNo.Value = notFilmNo;
            myCommand.Parameters.Add(parameterNotFilmNo);

            SqlParameter parameterNotFrameNo = new SqlParameter("@NotFrameNo", SqlDbType.VarChar, 20);
            parameterNotFrameNo.Value = notFrameNo;
            myCommand.Parameters.Add(parameterNotFrameNo);

            SqlParameter parameterNotTicketNo = new SqlParameter("@NotTicketNo", SqlDbType.VarChar, 50);
            parameterNotTicketNo.Value = notTicketNo;
            myCommand.Parameters.Add(parameterNotTicketNo);

            SqlParameter parameterSpeedLimit = new SqlParameter("@SpeedLimit", SqlDbType.Int);
            parameterSpeedLimit.Value = speedLimit;
            myCommand.Parameters.Add(parameterSpeedLimit);

            SqlParameter parameterSpeedReading1 = new SqlParameter("@SpeedReading1", SqlDbType.Int);
            parameterSpeedReading1.Value = speedReading1;
            myCommand.Parameters.Add(parameterSpeedReading1);

            SqlParameter parameterSpeedReading2 = new SqlParameter("@SpeedReading2", SqlDbType.Int);
            parameterSpeedReading2.Value = speedReading2;
            myCommand.Parameters.Add(parameterSpeedReading2);

            SqlParameter parameterOffenceDate = new SqlParameter("@OffenceDate", SqlDbType.DateTime);
            parameterOffenceDate.Value = offenceDate;
            myCommand.Parameters.Add(parameterOffenceDate);

            //SqlParameter parameterNotLocCode = new SqlParameter("@NotLocCode", SqlDbType.VarChar, 50);
            //parameterNotLocCode.Value = notLocCode;
            //myCommand.Parameters.Add(parameterNotLocCode);

            SqlParameter parameterNotLocDescr = new SqlParameter("@NotLocDescr", SqlDbType.VarChar, 150);
            if (String.IsNullOrEmpty(notLocDescr)) notLocDescr = "";
            else
            {
                if (notLocDescr.Length > 150) notLocDescr = notLocDescr.Substring(0, 150);
            }
            parameterNotLocDescr.Value = notLocDescr;
            myCommand.Parameters.Add(parameterNotLocDescr);

            SqlParameter parameterNotRegNo = new SqlParameter("@NotRegNo", SqlDbType.VarChar, 10);
            parameterNotRegNo.Value = notRegNo;
            myCommand.Parameters.Add(parameterNotRegNo);

            //Jerry 2013-07-09 add for Swartland
            SqlParameter parameterRegNo2 = new SqlParameter("@RegNo2", SqlDbType.VarChar, 10);
            parameterRegNo2.Value = regNo2;
            myCommand.Parameters.Add(parameterRegNo2);

            SqlParameter parameterNotVehicleMakeCode = new SqlParameter("@NotVehicleMakeCode", SqlDbType.VarChar, 3);
            parameterNotVehicleMakeCode.Value = notVehicleMakeCode;
            myCommand.Parameters.Add(parameterNotVehicleMakeCode);

            SqlParameter parameterNotVehicleMake = new SqlParameter("@NotVehicleMake", SqlDbType.VarChar, 30);
            parameterNotVehicleMake.Value = notVehicleMake;
            myCommand.Parameters.Add(parameterNotVehicleMake);

            SqlParameter parameterNotVehicleTypeCode = new SqlParameter("@NotVehicleTypeCode", SqlDbType.VarChar, 3);
            parameterNotVehicleTypeCode.Value = notVehicleTypeCode;
            myCommand.Parameters.Add(parameterNotVehicleTypeCode);

            SqlParameter parameterNotVehicleType = new SqlParameter("@NotVehicleType", SqlDbType.VarChar, 30);
            parameterNotVehicleType.Value = notVehicleType;
            myCommand.Parameters.Add(parameterNotVehicleType);

            //2014-07-31 Heidi added for import mobile s56(5303)
            SqlParameter parameterNotVehicleColour = new SqlParameter("@NotVehicleColour", SqlDbType.VarChar, 3);
            parameterNotVehicleColour.Value = notVehicleColour;
            myCommand.Parameters.Add(parameterNotVehicleColour);

            SqlParameter parameterNotVehicleLicenceExpiry = new SqlParameter("@NotVehicleLicenceExpiry", SqlDbType.DateTime);
            if (notVehicleLicenceExpiry == null)
            {
                parameterNotVehicleLicenceExpiry.Value = DBNull.Value;
            }
            else
            {
                parameterNotVehicleLicenceExpiry.Value = notVehicleLicenceExpiry;
            }
            myCommand.Parameters.Add(parameterNotVehicleLicenceExpiry);

            SqlParameter parameterNotOfficerNo = new SqlParameter("@NotOfficerNo", SqlDbType.VarChar, 10);
            parameterNotOfficerNo.Value = notOfficerNo;
            myCommand.Parameters.Add(parameterNotOfficerNo);


            SqlParameter parameterNotOfficerSname = new SqlParameter("@NotOfficerSname", SqlDbType.VarChar, 35);
            parameterNotOfficerSname.Value = notOfficerSname;
            myCommand.Parameters.Add(parameterNotOfficerSname);

            SqlParameter parameterNotOfficerGroup = new SqlParameter("@NotOfficerGroup", SqlDbType.VarChar, 50);
            parameterNotOfficerGroup.Value = notOfficerGroup;
            myCommand.Parameters.Add(parameterNotOfficerGroup);

            SqlParameter parameterIsOfficerError = new SqlParameter("@IsOfficerError", SqlDbType.Bit);
            parameterIsOfficerError.Value = isOfficerError;
            myCommand.Parameters.Add(parameterIsOfficerError);

            SqlParameter parameterOfficerErrorStr = new SqlParameter("@OfficerErrorStr", SqlDbType.VarChar, 1000);
            parameterOfficerErrorStr.Value = officerErrorString;
            myCommand.Parameters.Add(parameterOfficerErrorStr);

            SqlParameter parameterNotDMSCaptureClerk = new SqlParameter("@NotDMSCaptureCLerk", SqlDbType.VarChar, 25);
            parameterNotDMSCaptureClerk.Value = notDMSCaptureClerk;
            myCommand.Parameters.Add(parameterNotDMSCaptureClerk);

            SqlParameter parameterNotDMSCaptureDate = new SqlParameter("@NotDMSCaptureDate", SqlDbType.SmallDateTime);
            parameterNotDMSCaptureDate.Value = notDMSCaptureDate;
            myCommand.Parameters.Add(parameterNotDMSCaptureDate);

            SqlParameter parameterNSTypeCode = new SqlParameter("@NSTCode", SqlDbType.NVarChar, 5);
            parameterNSTypeCode.Value = nstCode;
            myCommand.Parameters.Add(parameterNSTypeCode);

            //Heidi 2013-05-20 add
            SqlParameter parameterLoSuIntNo = new SqlParameter("@LoSuIntNo", SqlDbType.Int);
            parameterLoSuIntNo.Value = loSuIntNo;
            myCommand.Parameters.Add(parameterLoSuIntNo);

            SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int);
            parameterNotIntNo.Value = 0;
            parameterNotIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterNotIntNo);

            SqlParameter parameterNotScanDate = new SqlParameter("@NotScanDate", SqlDbType.DateTime);
            parameterNotScanDate.Value = (string.IsNullOrEmpty(scanDate) ? null : scanDate);
            myCommand.Parameters.Add(parameterNotScanDate);

            #endregion

            #region Charge
            SqlParameter parameterChgOffenceCode1 = new SqlParameter("@ChgOffenceCode1", SqlDbType.VarChar, 15);
            parameterChgOffenceCode1.Value = chgOffenceCode1;
            myCommand.Parameters.Add(parameterChgOffenceCode1);

            SqlParameter parameterChgOffenceDescr1 = new SqlParameter("@ChgOffenceDescr1", SqlDbType.VarChar);
            parameterChgOffenceDescr1.Value = chgOffenceDescr1;
            myCommand.Parameters.Add(parameterChgOffenceDescr1);

            SqlParameter parameterChgOffenceCode2 = new SqlParameter("@ChgOffenceCode2", SqlDbType.VarChar, 15);
            parameterChgOffenceCode2.Value = chgOffenceCode2;
            myCommand.Parameters.Add(parameterChgOffenceCode2);

            SqlParameter parameterChgOffenceDescr2 = new SqlParameter("@ChgOffenceDescr2", SqlDbType.VarChar);
            parameterChgOffenceDescr2.Value = chgOffenceDescr2;
            myCommand.Parameters.Add(parameterChgOffenceDescr2);

            SqlParameter parameterChgOffenceCode3 = new SqlParameter("@ChgOffenceCode3", SqlDbType.VarChar, 15);
            parameterChgOffenceCode3.Value = chgOffenceCode3;
            myCommand.Parameters.Add(parameterChgOffenceCode3);

            SqlParameter parameterChgOffenceDescr3 = new SqlParameter("@ChgOffenceDescr3", SqlDbType.VarChar);
            parameterChgOffenceDescr3.Value = chgOffenceDescr3;
            myCommand.Parameters.Add(parameterChgOffenceDescr3);

            SqlParameter parameterChgFineAmount1 = new SqlParameter("@ChgFineAmount1", SqlDbType.Real);
            parameterChgFineAmount1.Value = chgFineAmount1;
            myCommand.Parameters.Add(parameterChgFineAmount1);

            SqlParameter parameterChgFineAmount2 = new SqlParameter("@ChgFineAmount2", SqlDbType.Real);
            parameterChgFineAmount2.Value = chgFineAmount2;
            myCommand.Parameters.Add(parameterChgFineAmount2);

            SqlParameter parameterChgFineAmount3 = new SqlParameter("@ChgFineAmount3", SqlDbType.Real);
            parameterChgFineAmount3.Value = chgFineAmount3;
            myCommand.Parameters.Add(parameterChgFineAmount3);

            //Jake Added 20101108 AlternateCharge code 
            SqlParameter parameterChgAlternateCode1 = new SqlParameter("@ChgAlternateCode1", SqlDbType.VarChar, 20);
            parameterChgAlternateCode1.Value = chgAlternateCode1;
            myCommand.Parameters.Add(parameterChgAlternateCode1);

            SqlParameter parameterChgAlternateCode2 = new SqlParameter("@ChgAlternateCode2", SqlDbType.VarChar, 20);
            parameterChgAlternateCode2.Value = chgAlternateCode2;
            myCommand.Parameters.Add(parameterChgAlternateCode2);

            SqlParameter parameterChgAlternateCode3 = new SqlParameter("@ChgAlternateCode3", SqlDbType.VarChar, 20);
            parameterChgAlternateCode3.Value = chgAlternateCode3;
            myCommand.Parameters.Add(parameterChgAlternateCode3);

            SqlParameter parameterOffNoAOG1 = new SqlParameter("@OffNoAOG1", SqlDbType.Char, 1);
            parameterOffNoAOG1.Value = chgNoAog1;
            myCommand.Parameters.Add(parameterOffNoAOG1);

            SqlParameter parameterOffNoAOG2 = new SqlParameter("@OffNoAOG2", SqlDbType.Char, 1);
            parameterOffNoAOG2.Value = chgNoAog2;
            myCommand.Parameters.Add(parameterOffNoAOG2);

            SqlParameter parameterOffNoAOG3 = new SqlParameter("@OffNoAOG3", SqlDbType.Char, 1);
            parameterOffNoAOG3.Value = chgNoAog3;
            myCommand.Parameters.Add(parameterOffNoAOG3);
            //2014-10-30 Heidi added(5376)
            SqlParameter parameterOffIsMain1 = new SqlParameter("@OffIsMain1", SqlDbType.Bit, 1);
            parameterOffIsMain1.Value = chgIsMain1;
            myCommand.Parameters.Add(parameterOffIsMain1);
            //2014-10-30 Heidi added(5376)
            SqlParameter parameterOffIsMain2 = new SqlParameter("@OffIsMain2", SqlDbType.Bit, 1);
            parameterOffIsMain2.Value = chgIsMain2;
            myCommand.Parameters.Add(parameterOffIsMain2);
            //2014-10-30 Heidi added(5376)
            SqlParameter parameterOffIsMain3 = new SqlParameter("@OffIsMain3", SqlDbType.Bit, 1);
            parameterOffIsMain3.Value = chgIsMain3;
            myCommand.Parameters.Add(parameterOffIsMain3);

            //SqlParameter parameterChgPaidDate = new SqlParameter("@ChgPaidDate", SqlDbType.DateTime);
            //if (chgPaidDate == null)
            //{
            //    parameterChgPaidDate.Value = DBNull.Value;
            //}
            //else
            //{
            //    parameterChgPaidDate.Value = chgPaidDate;
            //}
            //myCommand.Parameters.Add(parameterChgPaidDate);

            SqlParameter parameterNotPaymentDate = new SqlParameter("@NotPaymentDate", SqlDbType.DateTime);
            if (notPaymentDate == null)
            {
                parameterNotPaymentDate.Value = DBNull.Value;
            }
            else
            {
                parameterNotPaymentDate.Value = notPaymentDate;
            }
            myCommand.Parameters.Add(parameterNotPaymentDate);

            SqlParameter parameterChgOfficerNatisNo = new SqlParameter("@ChgOfficerNatisNo", SqlDbType.VarChar, 10);
            parameterChgOfficerNatisNo.Value = chgOfficerNatisNo;
            myCommand.Parameters.Add(parameterChgOfficerNatisNo);

            SqlParameter parameterChgOfficerName = new SqlParameter("@ChgOfficerName", SqlDbType.VarChar, 40);
            parameterChgOfficerName.Value = chgOfficerName;
            myCommand.Parameters.Add(parameterChgOfficerName);

            SqlParameter parameterConCode = new SqlParameter("@ConCode", SqlDbType.VarChar, 10);
            parameterConCode.Value = conCode;
            myCommand.Parameters.Add(parameterConCode);



            #endregion

            #region Summons
            SqlParameter parameterSumCourtDate = new SqlParameter("@SumCourtDate", SqlDbType.DateTime);

            if (sumCourtDate == null)
            {
                parameterSumCourtDate.Value = DBNull.Value;
            }
            else if (sumCourtDate == DateTime.Parse("1901-01-01"))//2014-10-30 Heidi added(5376)
            {
                parameterSumCourtDate.Value = DBNull.Value;
            }
            else
            {
                parameterSumCourtDate.Value = sumCourtDate;
            }
            myCommand.Parameters.Add(parameterSumCourtDate);

            SqlParameter parameterSumOfficerNo = new SqlParameter("@SumOfficerNo", SqlDbType.VarChar, 10);
            parameterSumOfficerNo.Value = sumOfficerNo;
            myCommand.Parameters.Add(parameterSumOfficerNo);

            SqlParameter parameterSumCaseNo = new SqlParameter("@SumCaseNo", SqlDbType.VarChar, 50);
            parameterSumCaseNo.Value = sumCaseNo;
            myCommand.Parameters.Add(parameterSumCaseNo);

            SqlParameter parameterCrtIntNo = new SqlParameter("@CrtNo", SqlDbType.VarChar, 20);
            parameterCrtIntNo.Value = crtNo;
            myCommand.Parameters.Add(parameterCrtIntNo);

            SqlParameter parameterCrtRIntNo = new SqlParameter("@CrtRoomNo", SqlDbType.VarChar);
            parameterCrtRIntNo.Value = crtRoomNo;
            myCommand.Parameters.Add(parameterCrtRIntNo);

            //SqlParameter parameterSummonsFilePath = new SqlParameter("@SummonsFilePath", SqlDbType.VarChar, 255);
            //parameterSummonsFilePath.Value = summonsFilePath;
            //myCommand.Parameters.Add(parameterSummonsFilePath);

            SqlParameter parameterIfsIntNo = new SqlParameter("@IfsIntNo", SqlDbType.Int);
            parameterIfsIntNo.Value = ifsIntNo;
            myCommand.Parameters.Add(parameterIfsIntNo);
            #endregion

            #region Accused
            SqlParameter parameterAccSurname = new SqlParameter("@AccSurname", SqlDbType.VarChar, 100);
            parameterAccSurname.Value = accSurname;
            myCommand.Parameters.Add(parameterAccSurname);

            SqlParameter parameterAccForenames = new SqlParameter("@AccForenames", SqlDbType.VarChar, 100);
            parameterAccForenames.Value = accForenames;
            myCommand.Parameters.Add(parameterAccForenames);

            SqlParameter parameterAccIdType = new SqlParameter("@AccIdType", SqlDbType.VarChar, 3);
            parameterAccIdType.Value = accIdType;
            myCommand.Parameters.Add(parameterAccIdType);

            SqlParameter parameterAccIdNumber = new SqlParameter("@AccIdNumber", SqlDbType.VarChar, 25);
            parameterAccIdNumber.Value = accIdNumber;
            myCommand.Parameters.Add(parameterAccIdNumber);

            SqlParameter parameterAccOccupation = new SqlParameter("@AccOccupation", SqlDbType.VarChar, 50);
            parameterAccOccupation.Value = accOccupation;
            myCommand.Parameters.Add(parameterAccOccupation);


            SqlParameter parameterAccAge = new SqlParameter("@AccAge", SqlDbType.VarChar, 25);
            parameterAccAge.Value = accAge;
            myCommand.Parameters.Add(parameterAccAge);

            SqlParameter parameterAccSex = new SqlParameter("@AccSex", SqlDbType.VarChar, 25);
            parameterAccSex.Value = accSex;
            myCommand.Parameters.Add(parameterAccSex);

            SqlParameter parameterAccAddress1 = new SqlParameter("@AccAddress1", SqlDbType.VarChar, 25);
            parameterAccAddress1.Value = accAddress1;
            myCommand.Parameters.Add(parameterAccAddress1);

            SqlParameter parameterAccAddress2 = new SqlParameter("@AccAddress2", SqlDbType.VarChar, 100);
            parameterAccAddress2.Value = accAddress2;
            myCommand.Parameters.Add(parameterAccAddress2);

            SqlParameter parameterAccAddress3 = new SqlParameter("@AccAddress3", SqlDbType.VarChar, 100);
            parameterAccAddress3.Value = accAddress3;
            myCommand.Parameters.Add(parameterAccAddress3);

            SqlParameter parameterAccAddress4 = new SqlParameter("@AccAddress4", SqlDbType.VarChar, 100);
            parameterAccAddress4.Value = accAddress4;
            myCommand.Parameters.Add(parameterAccAddress4);

            SqlParameter parameterAccAddressCode = new SqlParameter("@AccAddressCode", SqlDbType.VarChar, 25);
            parameterAccAddressCode.Value = accAddressCode;
            myCommand.Parameters.Add(parameterAccAddressCode);

            SqlParameter parameterAccBusinessAddress1 = new SqlParameter("@AccBusinessAddress1", SqlDbType.VarChar, 25);
            parameterAccBusinessAddress1.Value = accBusinessAddress1;
            myCommand.Parameters.Add(parameterAccBusinessAddress1);

            SqlParameter parameterAccBusinessAddress2 = new SqlParameter("@AccBusinessAddress2", SqlDbType.VarChar, 25);
            parameterAccBusinessAddress2.Value = accBusinessAddress2;
            myCommand.Parameters.Add(parameterAccBusinessAddress2);

            SqlParameter parameterAccBusinessAddress3 = new SqlParameter("@AccBusinessAddress3", SqlDbType.VarChar, 25);
            parameterAccBusinessAddress3.Value = accBusinessAddress3;
            myCommand.Parameters.Add(parameterAccBusinessAddress3);

            SqlParameter parameterAccBusinessAddress4 = new SqlParameter("@AccBusinessAddress4", SqlDbType.VarChar, 100);
            parameterAccBusinessAddress4.Value = accBusinessAddress4;
            myCommand.Parameters.Add(parameterAccBusinessAddress4);

            SqlParameter parameterAccBusinessAddressCode = new SqlParameter("@AccBusinessAddressCode", SqlDbType.VarChar, 25);
            parameterAccBusinessAddressCode.Value = accBusinessAddressCode;
            myCommand.Parameters.Add(parameterAccBusinessAddressCode);

            SqlParameter parameterAccTelWork = new SqlParameter("@AccTelWork", SqlDbType.VarChar, 15);
            parameterAccTelWork.Value = accTelWork;
            myCommand.Parameters.Add(parameterAccTelWork);

            SqlParameter parameterAccTelHome = new SqlParameter("@AccTelHome", SqlDbType.VarChar, 15);
            parameterAccTelHome.Value = accTelHome;
            myCommand.Parameters.Add(parameterAccTelHome);

            //SqlParameter parameterDrvLicence = new SqlParameter("@DrvLicence", SqlDbType.VarChar, 10);
            //parameterDrvLicence.Value = drvLicence;
            //myCommand.Parameters.Add(parameterDrvLicence);

            SqlParameter parameterNationality = new SqlParameter("@Nationality", SqlDbType.VarChar, 3);
            parameterNationality.Value = nationality;
            myCommand.Parameters.Add(parameterNationality);

            //2014-07-29 Heidi added for import mobile s56(5303)
            SqlParameter parameterAccPDPNumber = new SqlParameter("@AccPDPNumber", SqlDbType.NVarChar, 100);
            parameterAccPDPNumber.Value = accPDPNumber;
            myCommand.Parameters.Add(parameterAccPDPNumber);

            //SqlParameter parameterAccCellNumber = new SqlParameter("@AccCellNumber", SqlDbType.VarChar, 20);
            //parameterAccCellNumber.Value = accCellNumber;
            //myCommand.Parameters.Add(parameterAccCellNumber);

            SqlParameter parameterAccLicenceNumber = new SqlParameter("@AccLicenceNumber", SqlDbType.NVarChar, 25);
            parameterAccLicenceNumber.Value = accLicenceNumber;
            myCommand.Parameters.Add(parameterAccLicenceNumber);

            SqlParameter parameterAccLicenceCode = new SqlParameter("@AccLicenceCode", SqlDbType.NVarChar, 100);
            parameterAccLicenceCode.Value = accLicenceCode;
            myCommand.Parameters.Add(parameterAccLicenceCode);

            SqlParameter parameterAccEmailAddress = new SqlParameter("@AccEmailAddress", SqlDbType.VarChar, 100);
            parameterAccEmailAddress.Value = accEmailAddress;
            myCommand.Parameters.Add(parameterAccEmailAddress);

            SqlParameter parameterIsTaxi = new SqlParameter("@IsTaxi", SqlDbType.Bit);
            parameterIsTaxi.Value = isTaxi;
            myCommand.Parameters.Add(parameterIsTaxi);

            SqlParameter parameterimageExtension = new SqlParameter("@ImageExtension", SqlDbType.VarChar, 50);
            parameterimageExtension.Value = imageExtension;
            myCommand.Parameters.Add(parameterimageExtension);

            SqlParameter parameterBasePrintFileName = new SqlParameter("@BasePrintFileName", SqlDbType.VarChar, 50);
            parameterBasePrintFileName.Value = batchBaseFileName;
            myCommand.Parameters.Add(parameterBasePrintFileName);


            #endregion

            #region AartoDocument and AartoDocumentImage
            SqlParameter parameterDocNo = new SqlParameter("@DocNo", SqlDbType.VarChar, 20);
            parameterDocNo.Value = docNo;
            myCommand.Parameters.Add(parameterDocNo);

            SqlParameter parameterAaDocImgOrder = new SqlParameter("@AaDocImgOrder", SqlDbType.Int);
            parameterAaDocImgOrder.Value = aaDocImgOrder;
            myCommand.Parameters.Add(parameterAaDocImgOrder);

            SqlParameter parameterImageFilePath = new SqlParameter("@ImageFilePath", SqlDbType.VarChar, 100);
            parameterImageFilePath.Value = imageFilePath;
            myCommand.Parameters.Add(parameterImageFilePath);

            #endregion



            //SqlParameter parameterAGNo = new SqlParameter("@AGNo", SqlDbType.VarChar, 20);
            //parameterAGNo.Value = agNo;
            //myCommand.Parameters.Add(parameterAGNo);

            SqlParameter parameterOverAge = new SqlParameter("@OverAge", SqlDbType.Bit);
            parameterOverAge.Value = overAge;
            myCommand.Parameters.Add(parameterOverAge);

            //---------------------------

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterPsttName = new SqlParameter("@PSTTName", SqlDbType.VarChar, 50);
            parameterPsttName.Value = psttName;
            myCommand.Parameters.Add(parameterPsttName);

            //jerry 2011-11-03 add code to get OGIntNo
            CourtService courtService = new CourtService();
            Court court = null;
            court = courtService.GetByCrtNo(crtNo);
            if (court == null)
            {
                court = courtService.GetAll().FirstOrDefault();
            }

            int ogIntNo;
            CourtRulesDetails courtRulesDetails = new CourtRulesDetails();
            courtRulesDetails.CrtIntNo = court.CrtIntNo;
            courtRulesDetails.CRCode = "3010";
            courtRulesDetails.LastUser = lastUser;
            DefaultCourtRules courtRule = new DefaultCourtRules(courtRulesDetails, connectStr);
            KeyValuePair<int, string> rule3010 = courtRule.SetDefaultCourtRule();
            string defaultRule3010 = rule3010.Value;
            if (defaultRule3010 == "N")
            {
                ogIntNo = (int)OriginGroupList.ALL;
            }
            else
            {
                courtRulesDetails = new CourtRulesDetails();
                courtRulesDetails.CrtIntNo = court.CrtIntNo;
                courtRulesDetails.CRCode = "3020";
                courtRulesDetails.LastUser = lastUser;
                courtRule = new DefaultCourtRules(courtRulesDetails, connectStr);
                KeyValuePair<int, string> rule3020 = courtRule.SetDefaultCourtRule();
                string defaultRule3020 = rule3020.Value;
                if (defaultRule3020 == "N")// is HWO or CAM
                {
                    ogIntNo = (int)OriginGroupList.HWO;

                }
                else
                {
                    ogIntNo = (int)OriginGroupList.S56;

                }
            }
            SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int);
            parameterOGIntNo.Value = ogIntNo;//(This parameter is not be used is sp)
            myCommand.Parameters.Add(parameterOGIntNo);

            // Oscar 2013-05-27 added
            var allowNoAOGOffence = new AuthorityRulesDB(connectStr).GetAuthorityRule(autIntNo, "4537").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase);
            myCommand.Parameters.AddWithValue("@AllowNoAOGOffence", allowNoAOGOffence);

            //SqlTransaction tran = null;
            try
            {
                myConnection.Open();

                //jake 2010-11-12 moved sqltransaction
                //tran = myConnection.BeginTransaction();
                //myCommand.Transaction = tran;
                object returnValue = myCommand.ExecuteScalar();
                if (returnValue == null) returnValue = 0;

                //int notIntNo = Convert.ToInt32(myCommand.Parameters["@NotIntNo"].Value.ToString().Trim());
                int notIntNo = Convert.ToInt32(returnValue);
                //Jake 2010-11-12 Add officer error has been moved into storedprocedure

                return notIntNo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                myConnection.Dispose();
            }
        }

        #endregion

        #region WithdrawSec56WithOfficerError Queue

        private void creatWithdrawSec56WithOfficerErrorQueue(int notIntNo, int autIntNo, string autCode, DateTime courtDate, QueueItemProcessor queProcessor)
        {
            //Heidi 2014-04-29 added to push queue WithdrawSec56WithOfficerError(5239)
            var notice = _noticeService.GetByNotIntNo(notIntNo);
            DateRuleInfo dR_sumCourtDate_WithDrawnDate = _dateRuleInfo.GetDateRuleInfoByDRNameAutIntNo(autIntNo, "SumCourtDate", "WithDrawnDate");
            int noDaysForWithDrawnDate = dR_sumCourtDate_WithDrawnDate.ADRNoOfDays;
            if (notice != null)
            {
                if (notice.NoticeStatus == 581)
                {
                    string AR_5610 = _authorityRuleInfo.GetAuthorityRulesInfoByWFRFANameAutoIntNo(autIntNo, "5610").ARString.Trim();
                    if (AR_5610.Equals("Y", StringComparison.OrdinalIgnoreCase))
                    {
                        sendWithdrawSec56WithOfficerErrorQueue(notIntNo, autCode, courtDate, noDaysForWithDrawnDate, queProcessor);
                    }
                }
            }
        }

        private void sendWithdrawSec56WithOfficerErrorQueue(int notIntNo, string autCode, DateTime courtDate, int noDaysForWithDrawnDate, QueueItemProcessor queProcessor)
        {
            queProcessor.Send(
                                new QueueItem()
                                {
                                    Body = notIntNo,
                                    Group = autCode.Trim(),
                                    ActDate = courtDate.AddDays(noDaysForWithDrawnDate).Date,
                                    LastUser = lastUser,
                                    QueueType = ServiceQueueTypeList.WithdrawSec56WithOfficerError
                                }
                             );
        }


        #endregion

        #region Get Court room
        private string GetCourtRoomNoByCrtNoAndCourtRoom(string CrtNo, string CourtRoom)
        {
            string courtRoomNo = "";
            Court court = courtService.GetByCrtNo(CrtNo);
            List<CourtRoom> selectCourtRooms = new List<CourtRoom>();
            if (court != null)
            {
                SIL.AARTO.DAL.Entities.TList<CourtRoom> courtRoomList = courtRoomService.GetByCrtIntNo(court.CrtIntNo);

                //2014-10-30 Heidi changed for geting active room NO By crtNo.(5376)
                //var search1 = from c in courtRoomList where c.CrtRoomNo == CourtRoom && c.CrtRactive != null && c.CrtRactive.ToUpper()=="Y" select c;
                //var search2 = from c in courtRoomList where c.CrtRoomName == CourtRoom && c.CrtRactive != null && c.CrtRactive.ToUpper() == "Y" select c;
                //selectCourtRooms = search1.ToList();
                //if (selectCourtRooms != null && selectCourtRooms.Count > 0)
                //{
                //    courtRoomNo = CourtRoom;
                //    return courtRoomNo;
                //}

                //selectCourtRooms = search2.ToList();
                //if (selectCourtRooms != null && selectCourtRooms.Count > 0)
                //{
                //    courtRoomNo = selectCourtRooms[0].CrtRoomNo;
                //    return courtRoomNo;
                //}

                //2014-10-30 Heidi added(5376)
                if (courtRoomNo == "")
                {
                    var search3 = from c in courtRoomList where c.CrtRactive != null && c.CrtRactive.ToUpper() == "Y" select c;
                    selectCourtRooms = search3.ToList();
                    if (selectCourtRooms != null && selectCourtRooms.Count > 0)
                    {
                        courtRoomNo = selectCourtRooms[0].CrtRoomNo;
                        return courtRoomNo;

                    }
                }


            }

            if (courtRoomNo == "")
            {
                courtRoomNo = CourtRoom;
            }

            return courtRoomNo;

        }
        #endregion

        public bool DealS56Error(int notIntNo, ref string errorMsg)
        {
            if (notIntNo == -1)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to Notice");
            else if (notIntNo == -2)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to Charge");
            else if (notIntNo == -3)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to Summons");
            else if (notIntNo == -4)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to Summons_Charge");
            else if (notIntNo == -5)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to CourtJudgementType");
            else if (notIntNo == -6)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to Film");
            else if (notIntNo == -7)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to Frame");
            else if (notIntNo == -8)
                errorMsg = String.Format("{0}:{1}", errorMsg, "handling Notice_Frame");
            else if (notIntNo == -9)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to AARTODocumentImage");
            else if (notIntNo == -10)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to SumCharge");
            else if (notIntNo == -11)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to Driver");
            else if (notIntNo == -12)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to Accused");
            else if (notIntNo == -13)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to Evidence Pack");
            else if (notIntNo == -14)
                errorMsg = String.Format("{0}:{1}", errorMsg, "handling Court Dates");
            else if (notIntNo == -16)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to AARTONoticeDocument");
            else if (notIntNo == -36)
                errorMsg = String.Format("{0}:{1}", errorMsg, "handling AARTOBMDocument");
            else if (notIntNo == -37)
                errorMsg = String.Format("{0}:{1}", errorMsg, "handling AARTOBMBook");
            else if (notIntNo == -40)
                errorMsg = String.Format("{0}:{1}", errorMsg, "handling Summons Print File Name");
            else if (notIntNo == -101)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to Charge_SumCharge");
            else if (notIntNo == -102)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to Notice_Summons");
            else if (notIntNo == -103)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to SearchID");
            else if (notIntNo == -104)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to SearchSurName");
            else if (notIntNo == -111)
                errorMsg = String.Format("{0}:{1}", errorMsg, "handling offier errors");
            else if (notIntNo == -130)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to CourtRoom");
            else if (notIntNo == -140)
                errorMsg = String.Format("{0}:{1}", errorMsg, "inserting data to CameraUnit");
            else if (notIntNo == -141)
                errorMsg = String.Format("{0}:{1}", errorMsg, "created Summons Print File Name Failed");
            else if (notIntNo == -100)
                errorMsg = String.Format("{0}:{1}", errorMsg, "Notice already exists");
            else
                errorMsg = String.Format("{0}: Unexpected error code: {1} ", errorMsg, notIntNo.ToString());
            return false;
        }

        #region OldImportHandwrittenOffencesSection56



        //public int ImportHandwrittenOffencesSection56(int autIntNo, string notFilmNo, string notFrameNo, string notTicketNo,
        //    int speedLimit, int speedReading, DateTime offenceDate, string notLocDescr, string notRegNo,
        //    string notVehicleMakeCode, string notVehicleMake, string notVehicleTypeCode, string notVehicleType,
        //    string notOfficerNo, string notOfficerSname, string notOfficerGroup,//17
        //    string notDMSCaptureClerk, DateTime? notDMSCaptureDate,
        //    string chgOffenceCode1, string chgOffenceDescr1,
        //    string chgOffenceCode2, string chgOffenceDescr2,
        //    string chgOffenceCode3, string chgOffenceDescr3,
        //    float chgFineAmount1, float chgFineAmount2, float chgFineAmount3, DateTime? notPaymentDate,
        //    string chgOfficerNatisNo, string chgOfficerName, string conCode,

        //    DateTime? sumCourtDate, string sumOfficerNo, string sumCaseNo,
        //    string crtNo, string crtRoomNo, int ifsIntNo,//7

        //    string accSurname, string accForenames, string accIdNumber, string accOccupation, string accAge, string accSex,
        //    string accPoAdd1, string accPoCode, string accTelHome, string accStAdd1, string accStAdd2, string accStCode,
        //    string accTelWork, bool isOfficerError,
        //    string docNo, int aaDocImgOrder, string imageFilePath, string lastUser, string officerErrorString,
        //    string nationality, string drvLicence, string accPoAdd2, string agNo, string accPoSuburb, string accPoTownOrCity,
        //    string accStSuburb, string accStTownOrCity, bool overAge,
        //    string chgAlternateCode1, string chgAlternateCode2, string chgAlternateCode3,
        //    string chgNoAog1, string chgNoAog2, string chgNoAog3)//12
        //{
        //    SqlConnection myConnection = new SqlConnection(_connectStr);

        //    SqlCommand myCommand = new SqlCommand("ImportHandwrittenOffencesSection56", myConnection);

        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    #region Notice
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterNotFilmNo = new SqlParameter("@NotFilmNo", SqlDbType.VarChar, 20);
        //    parameterNotFilmNo.Value = notFilmNo;
        //    myCommand.Parameters.Add(parameterNotFilmNo);

        //    SqlParameter parameterNotFrameNo = new SqlParameter("@NotFrameNo", SqlDbType.VarChar, 20);
        //    parameterNotFrameNo.Value = notFrameNo;
        //    myCommand.Parameters.Add(parameterNotFrameNo);

        //    SqlParameter parameterNotTicketNo = new SqlParameter("@NotTicketNo", SqlDbType.VarChar, 50);
        //    parameterNotTicketNo.Value = notTicketNo;
        //    myCommand.Parameters.Add(parameterNotTicketNo);

        //    SqlParameter parameterSpeedLimit = new SqlParameter("@SpeedLimit", SqlDbType.Int);
        //    parameterSpeedLimit.Value = speedLimit;
        //    myCommand.Parameters.Add(parameterSpeedLimit);

        //    SqlParameter parameterSpeedReading = new SqlParameter("@SpeedReading", SqlDbType.Int);
        //    parameterSpeedReading.Value = speedReading;
        //    myCommand.Parameters.Add(parameterSpeedReading);

        //    SqlParameter parameterOffenceDate = new SqlParameter("@OffenceDate", SqlDbType.DateTime);
        //    parameterOffenceDate.Value = offenceDate;
        //    myCommand.Parameters.Add(parameterOffenceDate);

        //    //SqlParameter parameterNotLocCode = new SqlParameter("@NotLocCode", SqlDbType.VarChar, 50);
        //    //parameterNotLocCode.Value = notLocCode;
        //    //myCommand.Parameters.Add(parameterNotLocCode);

        //    SqlParameter parameterNotLocDescr = new SqlParameter("@NotLocDescr", SqlDbType.VarChar, 150);
        //    parameterNotLocDescr.Value = notLocDescr;
        //    myCommand.Parameters.Add(parameterNotLocDescr);

        //    SqlParameter parameterNotRegNo = new SqlParameter("@NotRegNo", SqlDbType.VarChar, 10);
        //    parameterNotRegNo.Value = notRegNo;
        //    myCommand.Parameters.Add(parameterNotRegNo);

        //    SqlParameter parameterNotVehicleMakeCode = new SqlParameter("@NotVehicleMakeCode", SqlDbType.VarChar, 3);
        //    parameterNotVehicleMakeCode.Value = notVehicleMakeCode;
        //    myCommand.Parameters.Add(parameterNotVehicleMakeCode);

        //    SqlParameter parameterNotVehicleMake = new SqlParameter("@NotVehicleMake", SqlDbType.VarChar, 30);
        //    parameterNotVehicleMake.Value = notVehicleMake;
        //    myCommand.Parameters.Add(parameterNotVehicleMake);

        //    SqlParameter parameterNotVehicleTypeCode = new SqlParameter("@NotVehicleTypeCode", SqlDbType.VarChar, 3);
        //    parameterNotVehicleTypeCode.Value = notVehicleTypeCode;
        //    myCommand.Parameters.Add(parameterNotVehicleTypeCode);

        //    SqlParameter parameterNotVehicleType = new SqlParameter("@NotVehicleType", SqlDbType.VarChar, 30);
        //    parameterNotVehicleType.Value = notVehicleType;
        //    myCommand.Parameters.Add(parameterNotVehicleType);

        //    SqlParameter parameterNotOfficerNo = new SqlParameter("@NotOfficerNo", SqlDbType.VarChar, 10);
        //    parameterNotOfficerNo.Value = notOfficerNo;
        //    myCommand.Parameters.Add(parameterNotOfficerNo);


        //    SqlParameter parameterNotOfficerSname = new SqlParameter("@NotOfficerSname", SqlDbType.VarChar, 35);
        //    parameterNotOfficerSname.Value = notOfficerSname;
        //    myCommand.Parameters.Add(parameterNotOfficerSname);

        //    SqlParameter parameterNotOfficerGroup = new SqlParameter("@NotOfficerGroup", SqlDbType.VarChar, 50);
        //    parameterNotOfficerGroup.Value = notOfficerGroup;
        //    myCommand.Parameters.Add(parameterNotOfficerGroup);

        //    SqlParameter parameterIsOfficerError = new SqlParameter("@IsOfficerError", SqlDbType.Bit);
        //    parameterIsOfficerError.Value = isOfficerError;
        //    myCommand.Parameters.Add(parameterIsOfficerError);

        //    SqlParameter parameterOfficerErrorStr = new SqlParameter("@OfficerErrorStr", SqlDbType.VarChar, 500);
        //    parameterOfficerErrorStr.Value = officerErrorString;
        //    myCommand.Parameters.Add(parameterOfficerErrorStr);

        //    SqlParameter parameterNotDMSCaptureClerk = new SqlParameter("@NotDMSCaptureCLerk", SqlDbType.VarChar, 25);
        //    parameterNotDMSCaptureClerk.Value = notDMSCaptureClerk;
        //    myCommand.Parameters.Add(parameterNotDMSCaptureClerk);

        //    SqlParameter parameterNotDMSCaptureDate = new SqlParameter("@NotDMSCaptureDate", SqlDbType.SmallDateTime);
        //    parameterNotDMSCaptureDate.Value = notDMSCaptureDate;
        //    myCommand.Parameters.Add(parameterNotDMSCaptureDate);


        //    //SqlParameter parameterPayDate = new SqlParameter("@NotPaymentDate", SqlDbType.Bit);
        //    //parameterPayDate.Value = payDate;
        //    //myCommand.Parameters.Add(parameterPayDate);

        //    SqlParameter parameterNotIntNo = new SqlParameter("@NotIntNo", SqlDbType.Int);
        //    parameterNotIntNo.Value = 0;
        //    parameterNotIntNo.Direction = ParameterDirection.InputOutput;
        //    myCommand.Parameters.Add(parameterNotIntNo);

        //    #endregion

        //    #region Charge
        //    SqlParameter parameterChgOffenceCode1 = new SqlParameter("@ChgOffenceCode1", SqlDbType.VarChar, 15);
        //    parameterChgOffenceCode1.Value = chgOffenceCode1;
        //    myCommand.Parameters.Add(parameterChgOffenceCode1);

        //    SqlParameter parameterChgOffenceDescr1 = new SqlParameter("@ChgOffenceDescr1", SqlDbType.VarChar);
        //    parameterChgOffenceDescr1.Value = chgOffenceDescr1;
        //    myCommand.Parameters.Add(parameterChgOffenceDescr1);

        //    SqlParameter parameterChgOffenceCode2 = new SqlParameter("@ChgOffenceCode2", SqlDbType.VarChar, 15);
        //    parameterChgOffenceCode2.Value = chgOffenceCode2;
        //    myCommand.Parameters.Add(parameterChgOffenceCode2);

        //    SqlParameter parameterChgOffenceDescr2 = new SqlParameter("@ChgOffenceDescr2", SqlDbType.VarChar);
        //    parameterChgOffenceDescr2.Value = chgOffenceDescr2;
        //    myCommand.Parameters.Add(parameterChgOffenceDescr2);

        //    SqlParameter parameterChgOffenceCode3 = new SqlParameter("@ChgOffenceCode3", SqlDbType.VarChar, 15);
        //    parameterChgOffenceCode3.Value = chgOffenceCode3;
        //    myCommand.Parameters.Add(parameterChgOffenceCode3);

        //    SqlParameter parameterChgOffenceDescr3 = new SqlParameter("@ChgOffenceDescr3", SqlDbType.VarChar);
        //    parameterChgOffenceDescr3.Value = chgOffenceDescr3;
        //    myCommand.Parameters.Add(parameterChgOffenceDescr3);

        //    SqlParameter parameterChgFineAmount1 = new SqlParameter("@ChgFineAmount1", SqlDbType.Real);
        //    parameterChgFineAmount1.Value = chgFineAmount1;
        //    myCommand.Parameters.Add(parameterChgFineAmount1);

        //    SqlParameter parameterChgFineAmount2 = new SqlParameter("@ChgFineAmount2", SqlDbType.Real);
        //    parameterChgFineAmount2.Value = chgFineAmount2;
        //    myCommand.Parameters.Add(parameterChgFineAmount2);

        //    SqlParameter parameterChgFineAmount3 = new SqlParameter("@ChgFineAmount3", SqlDbType.Real);
        //    parameterChgFineAmount3.Value = chgFineAmount3;
        //    myCommand.Parameters.Add(parameterChgFineAmount3);

        //    //Jake Added 20101108 AlternateCharge code 
        //    SqlParameter parameterChgAlternateCode1 = new SqlParameter("@ChgAlternateCode1", SqlDbType.VarChar, 20);
        //    parameterChgAlternateCode1.Value = chgAlternateCode1;
        //    myCommand.Parameters.Add(parameterChgAlternateCode1);

        //    SqlParameter parameterChgAlternateCode2 = new SqlParameter("@ChgAlternateCode2", SqlDbType.VarChar, 20);
        //    parameterChgAlternateCode2.Value = chgAlternateCode2;
        //    myCommand.Parameters.Add(parameterChgAlternateCode2);

        //    SqlParameter parameterChgAlternateCode3 = new SqlParameter("@ChgAlternateCode3", SqlDbType.VarChar, 20);
        //    parameterChgAlternateCode3.Value = chgAlternateCode3;
        //    myCommand.Parameters.Add(parameterChgAlternateCode3);

        //    SqlParameter parameterOffNoAOG1 = new SqlParameter("@OffNoAOG1", SqlDbType.Char, 1);
        //    parameterOffNoAOG1.Value = chgNoAog1;
        //    myCommand.Parameters.Add(parameterOffNoAOG1);

        //    SqlParameter parameterOffNoAOG2 = new SqlParameter("@OffNoAOG2", SqlDbType.Char, 1);
        //    parameterOffNoAOG2.Value = chgNoAog2;
        //    myCommand.Parameters.Add(parameterOffNoAOG2);

        //    SqlParameter parameterOffNoAOG3 = new SqlParameter("@OffNoAOG3", SqlDbType.Char, 1);
        //    parameterOffNoAOG3.Value = chgNoAog3;
        //    myCommand.Parameters.Add(parameterOffNoAOG3);

        //    //SqlParameter parameterChgPaidDate = new SqlParameter("@ChgPaidDate", SqlDbType.DateTime);
        //    //if (chgPaidDate == null)
        //    //{
        //    //    parameterChgPaidDate.Value = DBNull.Value;
        //    //}
        //    //else
        //    //{
        //    //    parameterChgPaidDate.Value = chgPaidDate;
        //    //}
        //    //myCommand.Parameters.Add(parameterChgPaidDate);

        //    SqlParameter parameterNotPaymentDate = new SqlParameter("@NotPaymentDate", SqlDbType.DateTime);
        //    if (notPaymentDate == null)
        //    {
        //        parameterNotPaymentDate.Value = DBNull.Value;
        //    }
        //    else
        //    {
        //        parameterNotPaymentDate.Value = notPaymentDate;
        //    }
        //    myCommand.Parameters.Add(parameterNotPaymentDate);

        //    SqlParameter parameterChgOfficerNatisNo = new SqlParameter("@ChgOfficerNatisNo", SqlDbType.VarChar, 10);
        //    parameterChgOfficerNatisNo.Value = chgOfficerNatisNo;
        //    myCommand.Parameters.Add(parameterChgOfficerNatisNo);

        //    SqlParameter parameterChgOfficerName = new SqlParameter("@ChgOfficerName", SqlDbType.VarChar, 40);
        //    parameterChgOfficerName.Value = chgOfficerName;
        //    myCommand.Parameters.Add(parameterChgOfficerName);

        //    SqlParameter parameterConCode = new SqlParameter("@ConCode", SqlDbType.VarChar, 10);
        //    parameterConCode.Value = conCode;
        //    myCommand.Parameters.Add(parameterConCode);



        //    #endregion

        //    #region Summons
        //    SqlParameter parameterSumCourtDate = new SqlParameter("@SumCourtDate", SqlDbType.DateTime);
        //    if (sumCourtDate == null)
        //    {
        //        parameterSumCourtDate.Value = DBNull.Value;
        //    }
        //    else
        //    {
        //        parameterSumCourtDate.Value = sumCourtDate;
        //    }
        //    myCommand.Parameters.Add(parameterSumCourtDate);

        //    SqlParameter parameterSumOfficerNo = new SqlParameter("@SumOfficerNo", SqlDbType.VarChar, 10);
        //    parameterSumOfficerNo.Value = sumOfficerNo;
        //    myCommand.Parameters.Add(parameterSumOfficerNo);

        //    SqlParameter parameterSumCaseNo = new SqlParameter("@SumCaseNo", SqlDbType.VarChar, 50);
        //    parameterSumCaseNo.Value = sumCaseNo;
        //    myCommand.Parameters.Add(parameterSumCaseNo);

        //    SqlParameter parameterCrtIntNo = new SqlParameter("@CrtNo", SqlDbType.VarChar, 20);
        //    parameterCrtIntNo.Value = crtNo;
        //    myCommand.Parameters.Add(parameterCrtIntNo);

        //    SqlParameter parameterCrtRIntNo = new SqlParameter("@CrtRoomNo", SqlDbType.VarChar);
        //    parameterCrtRIntNo.Value = crtRoomNo;
        //    myCommand.Parameters.Add(parameterCrtRIntNo);

        //    //SqlParameter parameterSummonsFilePath = new SqlParameter("@SummonsFilePath", SqlDbType.VarChar, 255);
        //    //parameterSummonsFilePath.Value = summonsFilePath;
        //    //myCommand.Parameters.Add(parameterSummonsFilePath);

        //    SqlParameter parameterIfsIntNo = new SqlParameter("@IfsIntNo", SqlDbType.Int);
        //    parameterIfsIntNo.Value = ifsIntNo;
        //    myCommand.Parameters.Add(parameterIfsIntNo);
        //    #endregion

        //    #region Accused
        //    SqlParameter parameterAccSurname = new SqlParameter("@AccSurname", SqlDbType.VarChar, 100);
        //    parameterAccSurname.Value = accSurname;
        //    myCommand.Parameters.Add(parameterAccSurname);

        //    SqlParameter parameterAccForenames = new SqlParameter("@AccForenames", SqlDbType.VarChar, 100);
        //    parameterAccForenames.Value = accForenames;
        //    myCommand.Parameters.Add(parameterAccForenames);

        //    SqlParameter parameterAccIdNumber = new SqlParameter("@AccIdNumber", SqlDbType.VarChar, 25);
        //    parameterAccIdNumber.Value = accIdNumber;
        //    myCommand.Parameters.Add(parameterAccIdNumber);

        //    SqlParameter parameterAccOccupation = new SqlParameter("@AccOccupation", SqlDbType.VarChar, 25);
        //    parameterAccOccupation.Value = accOccupation;
        //    myCommand.Parameters.Add(parameterAccOccupation);


        //    SqlParameter parameterAccAge = new SqlParameter("@AccAge", SqlDbType.VarChar, 25);
        //    parameterAccAge.Value = accAge;
        //    myCommand.Parameters.Add(parameterAccAge);

        //    SqlParameter parameterAccSex = new SqlParameter("@AccSex", SqlDbType.VarChar, 25);
        //    parameterAccSex.Value = accSex;
        //    myCommand.Parameters.Add(parameterAccSex);

        //    SqlParameter parameterAccStAdd1 = new SqlParameter("@AccStAdd1", SqlDbType.VarChar, 25);
        //    parameterAccStAdd1.Value = accStAdd1;
        //    myCommand.Parameters.Add(parameterAccStAdd1);

        //    SqlParameter parameterAccStAdd2 = new SqlParameter("@AccStAdd2", SqlDbType.VarChar, 25);
        //    parameterAccStAdd2.Value = accStAdd2;
        //    myCommand.Parameters.Add(parameterAccStAdd2);

        //    //SqlParameter parameterAccStAdd3 = new SqlParameter("@AccStAdd3", SqlDbType.VarChar, 25);
        //    //parameterAccStAdd3.Value = accStAdd3;
        //    //myCommand.Parameters.Add(parameterAccStAdd3);

        //    SqlParameter parameterAccPoCode = new SqlParameter("@AccPoCode", SqlDbType.VarChar, 25);
        //    parameterAccPoCode.Value = accPoCode;
        //    myCommand.Parameters.Add(parameterAccPoCode);

        //    SqlParameter parameterAccTelHome = new SqlParameter("@AccTelHome", SqlDbType.VarChar, 15);
        //    parameterAccTelHome.Value = accTelHome;
        //    myCommand.Parameters.Add(parameterAccTelHome);

        //    SqlParameter parameterAccPoAdd1 = new SqlParameter("@AccPoAdd1", SqlDbType.VarChar, 25);
        //    parameterAccPoAdd1.Value = accPoAdd1;
        //    myCommand.Parameters.Add(parameterAccPoAdd1);

        //    SqlParameter parameterAccStCode = new SqlParameter("@AccStCode", SqlDbType.VarChar, 25);
        //    parameterAccStCode.Value = accStCode;
        //    myCommand.Parameters.Add(parameterAccStCode);

        //    SqlParameter parameterAccTelWork = new SqlParameter("@AccTelWork", SqlDbType.VarChar, 15);
        //    parameterAccTelWork.Value = accTelWork;
        //    myCommand.Parameters.Add(parameterAccTelWork);


        //    #endregion

        //    #region AartoDocument and AartoDocumentImage
        //    SqlParameter parameterDocNo = new SqlParameter("@DocNo", SqlDbType.VarChar, 20);
        //    parameterDocNo.Value = docNo;
        //    myCommand.Parameters.Add(parameterDocNo);

        //    SqlParameter parameterAaDocImgOrder = new SqlParameter("@AaDocImgOrder", SqlDbType.Int);
        //    parameterAaDocImgOrder.Value = aaDocImgOrder;
        //    myCommand.Parameters.Add(parameterAaDocImgOrder);

        //    SqlParameter parameterImageFilePath = new SqlParameter("@ImageFilePath", SqlDbType.VarChar, 100);
        //    parameterImageFilePath.Value = imageFilePath;
        //    myCommand.Parameters.Add(parameterImageFilePath);

        //    #endregion



        //    //------ 2010-09-26 Jake added some new fields

        //    SqlParameter parameterNationality = new SqlParameter("@Nationality", SqlDbType.VarChar, 3);
        //    parameterNationality.Value = nationality;
        //    myCommand.Parameters.Add(parameterNationality);

        //    SqlParameter parameterDrvLicence = new SqlParameter("@DrvLicence", SqlDbType.VarChar, 10);
        //    parameterDrvLicence.Value = drvLicence;
        //    myCommand.Parameters.Add(parameterDrvLicence);

        //    SqlParameter parameterAccPoAdd2 = new SqlParameter("@AccPoAdd2", SqlDbType.VarChar, 100);
        //    parameterAccPoAdd2.Value = accPoAdd2;
        //    myCommand.Parameters.Add(parameterAccPoAdd2);

        //    SqlParameter parameterAGNo = new SqlParameter("@AGNo", SqlDbType.VarChar, 20);
        //    parameterAGNo.Value = agNo;
        //    myCommand.Parameters.Add(parameterAGNo);

        //    SqlParameter parameterOverAge = new SqlParameter("@OverAge", SqlDbType.Bit);
        //    parameterOverAge.Value = overAge;
        //    myCommand.Parameters.Add(parameterOverAge);


        //    SqlParameter parameterAccPoSuburb = new SqlParameter("@AccPoSuburb", SqlDbType.VarChar, 100);
        //    parameterAccPoSuburb.Value = accPoSuburb;
        //    myCommand.Parameters.Add(parameterAccPoSuburb);

        //    SqlParameter parameterACCPoTownOrCity = new SqlParameter("@AccPoTownOrCity", SqlDbType.VarChar, 100);
        //    parameterACCPoTownOrCity.Value = accPoTownOrCity;
        //    myCommand.Parameters.Add(parameterACCPoTownOrCity);

        //    SqlParameter parameterAccStSuburb = new SqlParameter("@AccStSuburb", SqlDbType.VarChar, 100);
        //    parameterAccStSuburb.Value = accStSuburb;
        //    myCommand.Parameters.Add(parameterAccStSuburb);

        //    SqlParameter parameterACCStTownOrCity = new SqlParameter("@AccStTownOrCity", SqlDbType.VarChar, 100);
        //    parameterACCStTownOrCity.Value = accStTownOrCity;
        //    myCommand.Parameters.Add(parameterACCStTownOrCity);

        //    //---------------------------

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    //jerry 2011-11-03 add code to get OGIntNo
        //    ////CourtService courtService = new CourtService();
        //    ////Court court = null;
        //    ////court = courtService.GetByCrtNo(crtNo);
        //    ////if (court == null)
        //    ////{
        //    ////    court = courtService.GetAll().FirstOrDefault();
        //    ////}

        //    ////int ogIntNo;
        //    ////CourtRulesDetails courtRulesDetails = new CourtRulesDetails();
        //    ////courtRulesDetails.CrtIntNo = court.CrtIntNo;
        //    ////courtRulesDetails.CRCode = "3010";
        //    ////courtRulesDetails.LastUser = lastUser;
        //    ////DefaultCourtRules courtRule = new DefaultCourtRules(courtRulesDetails);
        //    ////KeyValuePair<int, string> rule3010 = courtRule.SetDefaultCourtRule();
        //    ////string defaultRule3010 = rule3010.Value;
        //    ////if (defaultRule3010 == "N")
        //    ////{
        //    ////    ogIntNo = (int)OriginGroupList.ALL;
        //    ////}
        //    ////else
        //    ////{
        //    ////    courtRulesDetails = new CourtRulesDetails();
        //    ////    courtRulesDetails.CrtIntNo = court.CrtIntNo;
        //    ////    courtRulesDetails.CRCode = "3020";
        //    ////    courtRulesDetails.LastUser = lastUser;
        //    ////    courtRule = new DefaultCourtRules(courtRulesDetails);
        //    ////    KeyValuePair<int, string> rule3020 = courtRule.SetDefaultCourtRule();
        //    ////    string defaultRule3020 = rule3020.Value;
        //    ////    if (defaultRule3020 == "N")// is HWO or CAM
        //    ////    {
        //    ////        ogIntNo = (int)OriginGroupList.HWO;

        //    ////    }
        //    ////    else
        //    ////    {
        //    ////        ogIntNo = (int)OriginGroupList.S56;

        //    ////    }
        //    ////}
        //    //SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int);
        //    //parameterOGIntNo.Value = ogIntNo;
        //    //myCommand.Parameters.Add(parameterOGIntNo);

        //    ////SqlTransaction tran = null;
        //    //try
        //    //{
        //    //    myConnection.Open();

        //    //    //jake 2010-11-12 moved sqltransaction
        //    //    //tran = myConnection.BeginTransaction();
        //    //    //myCommand.Transaction = tran;
        //    //    myCommand.ExecuteNonQuery();

        //    //    int notIntNo = Convert.ToInt32(myCommand.Parameters["@NotIntNo"].Value.ToString().Trim());

        //    //    //Jake 2010-11-12 Add officer error has been moved into storedprocedure

        //    //    return notIntNo;
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    throw ex;
        //    //}
        //    //finally
        //    //{

        //    //    myConnection.Dispose();
        //    //}
        //}

        #endregion

        #endregion

    }
}
