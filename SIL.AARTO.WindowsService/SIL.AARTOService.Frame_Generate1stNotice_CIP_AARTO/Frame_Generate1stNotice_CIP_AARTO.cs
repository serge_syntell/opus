﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Frame_Generate1stNotice_CIP_AARTO
{
    partial class Frame_Generate1stNotice_CIP_AARTO : ServiceHost
    {
        public Frame_Generate1stNotice_CIP_AARTO()
            : base("", new Guid("B29C4E3A-E494-44FD-9364-50C5E1176895"), new Frame_Generate1stNotice_CIP_AARTOService())
        {
            InitializeComponent();
        }
    }
}
