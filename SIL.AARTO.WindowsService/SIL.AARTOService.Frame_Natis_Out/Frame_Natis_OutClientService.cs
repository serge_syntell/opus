﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using System.IO;
using Stalberg.TMS;
using SIL.AARTOService.Resource;
using System.Data;
using System.Transactions;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using Stalberg.TMS.Data;
using System.Reflection;
using SIL.AARTO.BLL.CameraManagement;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.DMS.DAL.Services;
using SIL.AARTO.BLL.Model;
namespace SIL.AARTOService.Frame_Natis_Out
{
    public struct FieldPair
    {
        public string fieldNo, fieldValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldPair"/> class.
        /// </summary>
        /// <param name="p1">The p1.</param>
        /// <param name="p2">The p2.</param>
        public FieldPair(string p1, string p2)
        {
            fieldNo = p1;
            fieldValue = p2;
        }
    }

    public class Frame_Natis_OutClientService : ServiceDataProcessViaFileSystem
    {
        FileInfo fileInfo;
        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        AuthorityDetails authDetails = null;
        //NatisParamsDB.NatisParamDetails npDetails = null;

   
        string connAARTODB, connNatis_OutUNC;
        //string currentAutCode, lastAutCode;
        string autCode;
        bool isChecked;
        int autIntNo;
        //string filePath;
        bool natisLast;
        string autName;
        string autNatisCode, allowLookups, showDifferencesOnly;
        int expiryNoOfDays;
        int noOfDaysToSMSExtractOn2ndNoticeDate;
        NoAOGDB noAogDB;
        QueueItem queueItem; // 2013-12-24, Oscar added for error handling
        // 2013-12-24, Oscar promoted dbService to top
        readonly SecondaryOffenceService secondaryOffenceService = new SecondaryOffenceService();
        readonly NoticeService noticeService = new NoticeService();
        readonly ChargeService chargeService = new ChargeService();
        FrameService frameService = new FrameService();
        readonly DocumentErrorMessageService documentErrorMessageService = new DocumentErrorMessageService();
        readonly BatchDocumentService batchDocumentService = new BatchDocumentService();
        readonly QueueItemProcessor queProcessor = new QueueItemProcessor();

        public Frame_Natis_OutClientService()
            : base("", "", new Guid("0C4AF6C0-D8BA-4088-9EAE-DCF27C650975"))
        {
            this._serviceHelper = new AARTOServiceBase(this, false);    // use InternalTransaction
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.connNatis_OutUNC = AARTOBase.GetConnectionString(ServiceConnectionNameList.Natis_Out, ServiceConnectionTypeList.UNC);
            this.DestinationPath = this.connNatis_OutUNC;
            
            ServiceUtility.InitializeNetTier(this.connAARTODB);
            //2015-01-26 Heidi comment out DMS InitializeNetTier code becasue it not be need(bontq1775)
            //string dmsStr = AARTOBase.GetConnectionString(SIL.ServiceQueueLibrary.DAL.Entities.ServiceConnectionNameList.DMS,
            //     SIL.ServiceQueueLibrary.DAL.Entities.ServiceConnectionTypeList.DB);
            //ServiceUtility.InitializeNetTier(dmsStr, "SIL.DMS");
            AARTOBase.OnServiceStarting = () =>
            {
                if (noAogDB == null) noAogDB = new NoAOGDB(connAARTODB);

                this.isChecked = false;
                this.autCode = null;
                AARTOBase.CheckFolder(this.connNatis_OutUNC);
            };
        }


        public override void InitialWork(ref QueueItem item)
        {
            if (item.Body != null && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    // 2013-12-24, Oscar added for error handling
                    this.queueItem = item;

                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;  // default is save to Loaded folder

                    string autCode = item.Group.Trim();
                    this.fileInfo = (FileInfo)item.Body;

                    this.Logger.Info(ResourceHelper.GetResource("ProcessingFile", this.fileInfo.FullName));

                    rules.InitParameter(this.connAARTODB, autCode);

                    if (this.autCode != autCode)
                    {
                        this.autCode = autCode;
                        QueueItemValidation(ref item);
                    }
                    else if (!this.isChecked)
                    {
                        //AARTOBase.ErrorProcessing(ref item);
                        // 2013-12-24, Oscar added for error handling
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("DueToFailingInInitialInformationValidation") + ResourceHelper.GetResource("ThisFileWouldBeMovedIntoFailedFolder"), LogType.Error, ServiceOption.ContinueButQueueFail, this.queueItem);
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                    return;
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                return;
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
             for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                if (item.IsSuccessful)
                {
                    try
                    {
                        bool failed;
                        string fileName = this.fileInfo.Name;
                        //this.Logger.Info(string.Format("GetNatisFiles: Processing file {0}", fileName));

                        if (fileName.Substring(0, 4) == this.autNatisCode)
                        {
                            if (fileName.ToUpper().EndsWith("OUT"))
                            {
                                failed = ImportNaTISDetail();
                                if (failed)
                                {
                                    //AARTOBase.ErrorProcessing(ref item);
                                    // 2013-12-24, Oscar added for error handling
                                    AARTOBase.LogProcessing(ResourceHelper.GetResource("ThisFileWouldBeMovedIntoFailedFolder"), LogType.Error, ServiceOption.ContinueButQueueFail, this.queueItem);
                                }
                            }
                            else if (fileName.ToUpper().EndsWith("ERR"))
                            {
                                //this.Logger.Info(string.Format("GetNatisFiles: Error file returned from NaTis: {0} for {1}", fileName, this.autName));

                                failed = UpdateNatisError();
                                item.Status = QueueItemStatus.PostPone; // save to Error Folder
                            }
                        }
                        else
                        {
                            AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("GetNatisFilesInvalidFilename", fileName, autName));
                        }

                      
                       
                    }
                    catch (Exception ex)
                    {
                        AARTOBase.ErrorProcessing(ref item, ex);
                        return;
                    }

                }
                queueList[i] = item;
            }
        }


        private void QueueItemValidation(ref QueueItem item)
        {
            string msg;
            int statusStart = 0;
            int statusEnd = 0;
            this.isChecked = false;

            this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim().Equals(this.autCode, StringComparison.OrdinalIgnoreCase));

            if (this.authDetails != null && this.authDetails.AutIntNo > 0)
            {
                this.autIntNo = this.authDetails.AutIntNo;
                this.autName = this.authDetails.AutName;

                if (this.rules.Rule("0570").ARString != "N")
                {
                    AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("AuthorityRuleStringNotEqualsN"), autName, this.fileInfo.Name), true);
                    return;
                }

                //this.Logger.Info(string.Format("GetNatisFiles: Classic (INP/OUT) mode set for Authority {0}", autName));

                //this.npDetails = null;
                //this.npDetails = new NatisParamsDB(this.connAARTODB).GetNatisParamsDetails(this.autIntNo);
                //if (this.npDetails.nNpIntNo == 0)
                //{
                //    item.IsSuccessful = false;
                //    item.Status = QueueItemStatus.UnKnown;
                //    this.Logger.Error(string.Format("GetNatisFiles: Could not locate valid NatisParams for Authority {0}", autName));
                //    return;
                //}

                //this.autNatisCode = npDetails.sTerminalID;
                this.autNatisCode = authDetails.AutNatisCode;

                if (string.IsNullOrEmpty(this.autNatisCode) || !ServiceUtility.IsNumeric(this.autNatisCode))
                {
                    AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("NatisCodeNotValid"), autName, this.autNatisCode, ""), true);
                    return;
                }
                this.noOfDaysToSMSExtractOn2ndNoticeDate = rules.Rule("NotIssue2ndNoticeDate", "SMSExtractOn2ndNoticeDate").DtRNoOfDays;
                if (rules.Rule("0400").ARString.Trim().Equals("Y"))
                {
                    this.natisLast = true;
                    msg = ResourceHelper.GetResource("NatisSentAfterAdjudication");
                    statusStart = AARTOServiceBase.STATUS_SENT_TO_NATIS_LAST - 1;
                    statusEnd = AARTOServiceBase.STATUS_NOT_YET_RECEIVED_FROM_NATIS_LAST;
                }
                else
                {
                    this.natisLast = false;
                    msg = ResourceHelper.GetResource("NatisSentBeforeVerification");
                    statusStart = AARTOServiceBase.STATUS_SENT_TO_NATIS_FIRST - 1;
                    statusEnd = AARTOServiceBase.STATUS_NOT_YET_RECEIVED_FROM_NATIS_FIRST;
                }

                this.allowLookups = rules.Rule("0530").ARString;

                //this.Logger.Info(string.Format("GetNatisFiles: {0} for Authority {1}", msg, autName));

                this.showDifferencesOnly = rules.Rule("0550").ARString;

                DataSet natisAuthDS = AARTOBase.GetNatisAuthorities(this.connAARTODB, statusStart, statusEnd, "Frame", this.autIntNo, out msg);
                if (natisAuthDS != null && natisAuthDS.Tables.Count > 0 && natisAuthDS.Tables[0].Rows.Count > 0)
                {
                    // validation is finished
                    //this.lastAutCode = this.currentAutCode;
                    this.isChecked = true;
                    this.expiryNoOfDays = 0;
                }
                else
                {
                    //AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("GetNatisFilesAuthorityNotWaitingForNatisFiles"), autName));
                    // Oscar 20121112 changed to information type.
                    //Logger.Info(string.Format("Queue Key: {0} | {1}", item.Body, ResourceHelper.GetResource("GetNatisFilesAuthorityNotWaitingForNatisFiles", autName)));
                    // 2013-08-01 changed to LogProcessing
                    AARTOBase.LogProcessing(string.Format("Queue Key: {0} | {1}", item.Body, ResourceHelper.GetResource("GetNatisFilesAuthorityNotWaitingForNatisFiles", autName)), LogType.Info, ServiceOption.ContinueButQueueFail, item);
                    return;
                }

            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.autCode));
                return;
            }

        }

        private bool ImportNaTISDetail()
        {
            bool failed = false;
            string srLine = " ";
            char[] delimiter = "=".ToCharArray();
            string[] split = null;
            //int batIntNo = 0;
            //int notIntNo = 0;
            int frameIntNo = 0;
            int notIntNo = 0;
            //int cs150 = 0;
            //int cs151 = 0;
            //int cs152 = 0;
            //int cs153 = 0;
            //int cs154 = 0;
            //int cs200 = 0;
            string error = "";
            string errMsg = "";

            //dls 060805 - add filename to stored proc
            string fileName = this.fileInfo.Name;
            string nfFileName = fileName.Substring(0, 7);

            //dls 061012 - add update of NatisFiles table setting return date
            NaTISFilesDB natis = new NaTISFilesDB(this.connAARTODB);

            int nfIntNo = natis.UpdateNatisFilesReturnDate(autIntNo, nfFileName, AARTOBase.LastUser, ref errMsg);
            if (nfIntNo < 0)
                this.Logger.Info(string.Format(ResourceHelper.GetResource("FailedToSetReturnDateForFile"), AARTOBase.LastUser, nfFileName, errMsg));

            // Need all the status parameters from chargeStatus to avoid calling for them on each row
            // ChargeStatusDB status = new ChargeStatusDB(Application["constr"].ToString());
            // status.ReturnNaTISCSIntNos("150", "Invalid / No details returned from NaTIS", ref cs150, 
            //    "151", "The vehicle make in NaTIS is different", ref cs151, 
            //    "152", "The vehicle type in NaTIS is different", ref cs152, 
            //    "153", "The vehicle make and type in NaTIS are different", ref cs153,
            //    "154", "The registration number from NaTIS is different", ref cs154,
            //    "200", "Received the NaTIS information with no errors", ref cs200, _login);

            FieldPair fields = new FieldPair();
            List<FieldPair> fieldList = new List<FieldPair>();

            try
            {
                // loop through each row in the file
                //Encoding encoding = GetFileEncoding(saveLocation);
                using (StreamReader sr = new StreamReader(this.fileInfo.OpenRead(), true))
                {
                    int row = 0;
                    int nextStart = 0;
                    int length = 0;

                    bool eof = false; //set when row with 9999999999999 is reached

                    int frameStatus;
                    //QueueItem queItem = new QueueItem();
                    //QueueItemProcessor queProcessor = new QueueItemProcessor();
                    //TransactionScope scope;

                    FrameDB updateFrame = new FrameDB(this.connAARTODB);
                    FilmDB updateFilm = new FilmDB(this.connAARTODB);
                    //Heidi 2013-07-09 added for task 4997
                    NoticeDB updateNotice = new NoticeDB(this.connAARTODB);
                    while (sr.Peek() >= 0 && !eof)
                    {
                        try
                        {
                            //using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromSeconds(60)))
                            // 2013-12-24, Oscar changed for error handling
                            //using (var scope = ServiceUtility.CreateTransactionScope(TransactionScopeOption.RequiresNew))
                            //{

                            // Get the line details
                            srLine = sr.ReadLine();
                            row += 1;

                            // 2014-03-06, Oscar added deadlocks handling.
                            AARTOBase.InternalTransaction(null, () =>
                            {
                                failed = false;

                                // 2014-02-08, Oscar moved fieldList.Clear() from bottom to top.
                                fieldList.Clear();
                                return true;
                            }, () =>
                            {

                                // cannot use split as the row has a complex structure rather than a delimiter
                                // need to read one character at a time and build field name, field length and field value
                                // 1st four characters = transaction
                                nextStart = 0;
                                length = 4;
                                fields.fieldNo = "tran";
                                fields.fieldValue = srLine.Substring(0, 4);
                                fieldList.Add(fields); //transaction
                                nextStart = nextStart + length;
                                length = 7;
                                fields.fieldNo = "file";
                                fields.fieldValue = srLine.Substring(nextStart, length);
                                fieldList.Add(fields); //file

                                try
                                {
                                    while (srLine.Substring(nextStart, length) != "999")
                                    {
                                        nextStart = nextStart + length;
                                        length = 3; // length of the field number
                                        fields.fieldNo = srLine.Substring(nextStart, length);
                                        if (fields.fieldNo == "999")
                                            break; // kick out of the while loop and write the row
                                        nextStart = nextStart + length;
                                        length = 2; // length of the field length
                                        length = Convert.ToInt16(srLine.Substring(nextStart, length));
                                        nextStart += 2;
                                        fields.fieldValue = srLine.Substring(nextStart, length);
                                        error = "Row: " + row + " No: " + fields.fieldNo + " Length: " + length + " Value: " + fields.fieldValue;
                                        if (fields.fieldNo.Equals("001") && fields.fieldValue.Equals("F"))
                                            eof = true;
                                        fieldList.Add(fields);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    //this.Logger.Error(string.Format(ResourceHelper.GetResource("ProblemWithInputFileMoveToNextLine"), AARTOBase.LastUser, nfFileName, error, ex.Message));
                                    //continue;

                                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ProblemWithInputFileMoveToNextLine"), AARTOBase.LastUser, nfFileName, error, ex.Message), LogType.Error, ServiceOption.ContinueButQueueFail, queueItem);
                                    return false; // means continue

                                    // FBJ Added (2007-06-15): Changed the logic to only bomb the individual line
                                    // failed = true;
                                    //sr.Close();
                                    // return failed;
                                }

                                // build the database parameters from the array list
                                string f001 = "", f002 = "", f003 = "", f004 = "", f006 = "", f008 = "", f010 = "", f011 = "", f012 = "", f013 = "", f014 = "", f015 = "", f016 = "", f017 = "", f018 = "", f019 = "", f020 = "", f021 = "", f022 = "", f023 = "", f024 = "", f025 = "";
                                string f026 = "", f027 = "", f028 = "", f029 = "", f030 = "", f031 = "", f032 = "", f033 = "", f034 = "", f035 = "", f036 = "", f038 = "", f040 = "", f041 = "", f042 = "", f043 = "", f053 = "", f056 = "", f057 = "", f058 = "", f059 = "", f060 = "";
                                string f061 = "", f062 = "", f063 = "", f064 = "", f066 = "", f067 = "", f068 = "", f069 = "";
                                DateTime f005 = DateTime.Now, f007 = DateTime.Now, f009 = DateTime.Now, f037 = DateTime.Now, f153 = DateTime.Now;

                                string no = string.Empty;
                                string val = string.Empty;

                                try
                                {
                                    bool f003HasValue = false;
                                    foreach (FieldPair f in fieldList)
                                    {
                                        no = f.fieldNo;
                                        val = f.fieldValue;

                                        switch (no)
                                        {
                                            case "001": // returned as information acknowledgement
                                                // y = successfully received and processed
                                                // v = validation error condition on one or more fields specified in 002
                                                // u = data could not be found
                                                // n = not successfully received
                                                // m = unknown record type
                                                // f = missing unknown and/or duplicate fields
                                                // a = not authorised
                                                // r = redundant update record received
                                                // c = communication error
                                                // d = duplicate - more than one record found which meets the request
                                                // e = vehicle state invalid for queries - stolen/scrapped/owned by an insurance company
                                                if (val == "" || val == null)
                                                {
                                                    f001 = "";
                                                }
                                                else
                                                {
                                                    f001 = val;
                                                }
                                                break;

                                            case "002": // the field number(s) (if applicable) that is(are) causing trouble
                                                f002 = val;
                                                break;

                                            case "003": // vehicle licence number
                                                f003 = val;
                                                f003HasValue = true;
                                                break;

                                            case "004": // vehicle usage
                                                f004 = val;
                                                break;

                                            case "005": // date of change of motor vehicle registration qualification
                                                try
                                                {
                                                    val = val.Insert(6, "-");
                                                    val = val.Insert(4, "-");
                                                    f005 = Convert.ToDateTime(val);
                                                    if (f005 < Convert.ToDateTime("1900-01-01"))
                                                        f005 = Convert.ToDateTime("1900-01-01");
                                                }
                                                catch
                                                {
                                                    f005 = Convert.ToDateTime("1900-01-01");
                                                }
                                                break;

                                            case "006": // motor vehicle registration qualification
                                                f006 = val;
                                                break;

                                            case "007": // motor vehicle state change date
                                                try
                                                {
                                                    val = val.Insert(6, "-");
                                                    val = val.Insert(4, "-");
                                                    f007 = Convert.ToDateTime(val);
                                                    if (f007 < Convert.ToDateTime("1900-01-01"))
                                                        f007 = Convert.ToDateTime("1900-01-01");
                                                }
                                                catch
                                                {
                                                    f007 = Convert.ToDateTime("1900-01-01");
                                                }
                                                break;

                                            case "008": // motor vehicle state
                                                f008 = val;
                                                break;

                                            case "009": // date of change of ownership
                                                try
                                                {
                                                    val = val.Insert(6, "-");
                                                    val = val.Insert(4, "-");
                                                    f009 = Convert.ToDateTime(val);
                                                    if (f009 < Convert.ToDateTime("1900-01-01"))
                                                        f009 = Convert.ToDateTime("1900-01-01");
                                                }
                                                catch
                                                {
                                                    f009 = Convert.ToDateTime("1900-01-01");
                                                }
                                                break;

                                            case "010": // vehicle description
                                                f010 = val;
                                                break;

                                            case "011": // vehicle category
                                                f011 = val;
                                                break;

                                            case "012": // clearance certificate number
                                                f012 = val;
                                                break;

                                            case "013": // registering authority where vehicle is licensed
                                                f013 = val;
                                                break;

                                            case "014": // person's identification type
                                                f014 = val;
                                                break;

                                            case "015": // person's identification number
                                                f015 = val;
                                                break;

                                            case "016": // name of person
                                                f016 = val;
                                                break;

                                            case "017": // person's initials
                                                f017 = val;
                                                break;

                                            case "018": // nature of statutory ownership
                                                f018 = val;
                                                break;

                                            case "019": // person's postal address line 1
                                                f019 = val;
                                                break;

                                            case "020": // person's postal address line 2
                                                f020 = val;
                                                break;

                                            case "021": // person's postal address line 3
                                                f021 = val;
                                                break;

                                            case "022": // person's postal address line 4
                                                f022 = val;
                                                break;

                                            case "023": // person's postal address line 5
                                                f023 = val;
                                                break;

                                            case "024": // person's postal code
                                                f024 = val;
                                                break;

                                            case "025": // person's street address line 1
                                                f025 = val;
                                                break;

                                            case "026": // person's street address line 2
                                                f026 = val;
                                                break;

                                            case "027": // person's street address line 3
                                                f027 = val;
                                                break;

                                            case "028": // person's street address line 4
                                                f028 = val;
                                                break;

                                            case "029": // nature of person
                                                f029 = val;
                                                break;

                                            case "030": // person's street postal code
                                                f030 = val;
                                                break;

                                            case "031": // identity type of proxy
                                                f031 = val;
                                                break;

                                            case "032": // name of proxy
                                                f032 = val;
                                                break;

                                            case "033": // initials of proxy
                                                f033 = val;
                                                break;

                                            case "034": // vehicle make
                                                f034 = val;
                                                break;

                                            case "035": // vehicle model name
                                                f035 = val;
                                                break;

                                            case "036": // previous clearance certificate number
                                                f036 = val;
                                                break;

                                            case "037": // date of change of address
                                                try
                                                {
                                                    val = val.Insert(6, "-");
                                                    val = val.Insert(4, "-");
                                                    f037 = Convert.ToDateTime(val);
                                                    if (f037 < Convert.ToDateTime("1900-01-01"))
                                                        f037 = Convert.ToDateTime("1900-01-01");
                                                }
                                                catch
                                                {
                                                    f037 = Convert.ToDateTime("1900-01-01");
                                                }
                                                break;

                                            case "040": // model number
                                                f040 = val;
                                                break;

                                            case "041": // vehicle type
                                                f041 = val;
                                                break;

                                            case "042": // vehicle usage
                                                f042 = val;
                                                break;

                                            case "043": // main colour
                                                f043 = val;
                                                break;

                                            case "053": // identification number of proxy
                                                f053 = val;
                                                break;

                                            case "056": // nationality/population group
                                                f056 = val;
                                                break;

                                            case "061": // vehicle register number
                                                f061 = val;
                                                break;

                                            case "062": // driving licence code
                                                f062 = val;
                                                break;

                                            case "063": // place where licence was issued
                                                f063 = val;
                                                break;

                                            case "064": // age
                                                f064 = val;
                                                break;

                                            case "065": // transaction information (returns the information we sent to uniquely identify the row autIntNo=notIntNo)
                                                if (val.IndexOf("=", 0) > 0)
                                                {
                                                    //try
                                                    //{
                                                    split = val.Split(delimiter, 2);
                                                    autIntNo = Convert.ToInt32(split[0]);
                                                    frameIntNo = Convert.ToInt32(split[1]);

                                                    // 2013-12-24, Oscar added for error handling
                                                    AARTOBase.LogProcessing(ResourceHelper.GetResource("StartProcessingFrame", frameIntNo), LogType.Info, ServiceOption.Continue, this.queueItem, this.queueItem.Status);

                                                    //}
                                                    //catch
                                                    //{
                                                    //    autIntNo = 0;
                                                    //    frameIntNo = 0;
                                                    //    sr.Close();
                                                    //    //System.IO.File.Delete(saveLocation);
                                                    //    //failed = true;
                                                    //    //return failed;
                                                    //}
                                                }
                                                else if (val.Equals("99999999999999999999999"))
                                                {
                                                    //eof - do nothing
                                                }
                                                else
                                                {
                                                    autIntNo = 0;
                                                    frameIntNo = 0;
                                                    sr.Close();
                                                    //System.IO.File.Delete(saveLocation);
                                                    failed = true;
                                                    // 2013-12-24, Oscar added for error handling
                                                    AARTOBase.LogProcessing(ResourceHelper.GetResource("ReadInvalidInformationAtLine", row, no, val), LogType.Error, ServiceOption.ContinueButQueueFail, this.queueItem);
                                                    //return failed;
                                                    return false; // means failed
                                                }
                                                break;

                                            case "066": // VIN/chassis number
                                                f066 = val;
                                                break;

                                            case "067": // engine number
                                                f067 = val;
                                                break;

                                            case "068": // motor vehicle trailer type
                                                f068 = val;
                                                break;

                                            case "069": // proxy/representative indicator
                                                f069 = val;
                                                break;

                                            case "153": // vehicle licence expiry date
                                                try
                                                {
                                                    val = val.Insert(6, "-");
                                                    val = val.Insert(4, "-");
                                                    f153 = Convert.ToDateTime(val);
                                                    if (f153 < Convert.ToDateTime("1900-01-01"))
                                                        f153 = Convert.ToDateTime("1900-01-01");
                                                }
                                                catch
                                                {
                                                    f153 = Convert.ToDateTime("1900-01-01");
                                                }
                                                break;

                                            default:
                                                break;
                                        }
                                    }

                                    //dls 2010-10-04 - only check for missing Regno is successful return from Natis
                                    if (f001 == "" && (!f003HasValue || String.IsNullOrEmpty(f003) || f003.Length < 3)) // Tony added (2009-12-31):
                                    {
                                        this.Logger.Info(string.Format(ResourceHelper.GetResource("ProblemWithInputFileRegNoInvalid"), AARTOBase.LastUser, nfFileName, frameIntNo));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorProcessingFieldPair"), no, val, nfFileName, ex.Message));
                                    //continue;

                                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrorProcessingFieldPair"), no, val, nfFileName, ex.Message), LogType.Error, ServiceOption.ContinueButQueueFail, queueItem);
                                    return false; // means continue
                                }

                                if (!eof)
                                {
                                    //FrameDB updateFrame = new FrameDB(this.connAARTODB);
                                    string errMessage = "";
                                    int natisErrorCount = 0;

                                    // 2014-03-13, Oscar added RegNoHasChanged
                                    if (!FrameDB.CheckChangedRegNoMatched(ref frameService, frameIntNo, f003, fileName, AARTOBase.LastUser))
                                    {
                                        // skip current frame, process next one.
                                        AARTOBase.LogProcessing(ResourceHelper.GetResource("RegNoHasBeenModifiedNatisDetailsNotUpdated"), LogType.Info, ServiceOption.ContinueButQueueFail, queueItem, queueItem.Status);
                                        return false; // means continue
                                    }

                                    // 2013-12-24, Oscar added for error handling
                                    int returnValue;

                                    //dls 060805 - add filename to stored proc
                                    int updIntNo = updateFrame.UpdateFrameFromNATIS(nfFileName, f001, f002, f003, f004, f005, f006, f007, f008, f009, f010, f011,
                                        f012, f013, f014, f015, f016, f017, f018, f019, f020, f021, f022, f023, f024, f025, f026, f027, f028, f029, f030,
                                        f031, f032, f033, f034, f035, f036, f037, f038, f040, f041, f042, f043, f053, f056, f057, f058, f059, f060, f061,
                                        f062, f063, f064, f066, f067, f068, f069, f153, autIntNo, frameIntNo, fileName, AARTOBase.LastUser,
                                        this.showDifferencesOnly, ref errMessage, (this.natisLast ? "Y" : "N"), this.allowLookups, out natisErrorCount, out returnValue);



                                    //System.Diagnostics.Debug.WriteLine(string.Format("{0} - {1}", f016, f032));

                                    if (updIntNo <= 0)
                                    {
                                        ////sr.Close();
                                        ////System.IO.File.Delete(saveLocation);
                                        ////if (updIntNo == -3)
                                        ////    Beginnings.writer.WriteLine("ImportNaTISDetail: Frame " + frameIntNo + " - this frame has already been thru verification - no updates allowed.");
                                        ////else 
                                        //if (updIntNo == -2)
                                        //    Beginnings.writer.WriteLine("ImportNaTISDetail: Frame " + frameIntNo + " - this frame does not exist in the frame table.");
                                        //else if (updIntNo == 0)
                                        //    Beginnings.writer.WriteLine("ImportNaTISDetail: Error on Frame " + frameIntNo + " - " + errMessage);
                                        ////failed = true;
                                        ////return failed;

                                        //dls 071017 - added transaction to proc so that we can trap errors sooner.
                                        switch (updIntNo)
                                        {
                                            case 0:
                                                //this.Logger.Info(string.Format(ResourceHelper.GetResource("ErrorOnFrame"), AARTOBase.LastUser, frameIntNo, errMessage));
                                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrorOnFrame"), AARTOBase.LastUser, frameIntNo, errMessage), LogType.Info, ServiceOption.ContinueButQueueFail, queueItem);
                                                break;
                                            case -2:
                                                this.Logger.Info(string.Format(ResourceHelper.GetResource("FrameNotExistInFrameTable"), AARTOBase.LastUser, frameIntNo));
                                                break;
                                            case -3:
                                                //dls 071017 - remove this as it stops them from reading the rest of the log file!
                                                //Beginnings.writer.WriteLine("ImportNaTISDetail: Frame " + frameIntNo + " - this frame has already been thru verification - no updates allowed.");
                                                //break;
                                                //Jerry 2013-10-16 add
                                                this.Logger.Info(string.Format(ResourceHelper.GetResource("FrameAlreadyBeenUpdated"), AARTOBase.LastUser, frameIntNo));
                                                break;

                                            case -10:
                                            case -11:
                                            case -12:
                                            case -13:
                                            case -14:
                                            case -15:
                                            case -16:
                                            case -17:
                                            case -18:
                                            case -19:
                                            case -20:
                                            case -21:
                                            case -22:
                                            case -23:
                                            case -24:
                                            case -25:
                                            case -26:
                                            case -27:
                                            case -28:
                                            case -29:
                                            case -30:
                                            //this.Logger.Info(string.Format("ImportNaTISDetail: Frame {0} - Error - transaction failed ({1})", frameIntNo, updIntNo.ToString()));
                                            //break;
                                            default:
                                                this.Logger.Info(string.Format(ResourceHelper.GetResource("FrameTransactionFailed"), AARTOBase.LastUser, frameIntNo, updIntNo.ToString()));
                                                break;
                                        }
                                    }
                                    // 2013-12-24, Oscar added for error handling
                                    else if (returnValue < 0 || !string.IsNullOrWhiteSpace(errMessage))
                                    {
                                        AARTOBase.LogProcessing(ResourceHelper.GetResource("UpdateFrameFailed", frameIntNo, returnValue) + (string.IsNullOrWhiteSpace(errMessage) ? "" : " " + errMessage), LogType.Error, ServiceOption.ContinueButQueueFail, this.queueItem);
                                        //continue;
                                        return false; // means continue
                                    }
                                    else
                                    {
                                        FrameDetails frame = updateFrame.GetFrameDetails(frameIntNo);
                                        if (frame != null && ServiceUtility.IsNumeric(frame.FrameStatus))
                                        {
                                            frameStatus = Convert.ToInt32(frame.FrameStatus);

                                            NoticeDetails noticeDetails = updateNotice.GetNoticeDataByFrameIntNo(frameIntNo);
                                            //Heidi 2013-07-09 added for task 4997
                                            //Heidi 2013-10-21 changed for send back to DMS need to check DMS imported S341 Document
                                            if (natisErrorCount > 0 && noticeDetails != null
                                                && noticeDetails.NotStatisticsCode.Equals("H341", StringComparison.OrdinalIgnoreCase))
                                            {
                                                if (natisErrorCount < 5)
                                                {
                                                    SendBackToDMS(noticeDetails.NotIntNo, frameIntNo);
                                                }
                                            }
                                            else
                                            {

                                                //Oscar 20120322 move/add this check
                                                FilmDetails film = updateFilm.GetFilmDetails(frame.FilmIntNo);
                                                if (film != null && film.FilmType.Equals("H", StringComparison.OrdinalIgnoreCase)
                                                    && (frameStatus == 600 || frameStatus == 800)
                                                    )
                                                {
                                                    //Oscar 20120322 move this check
                                                    //FilmDetails film = updateFilm.GetFilmDetails(frame.FilmIntNo);
                                                    //if (!string.IsNullOrEmpty(film.FilmType) && film.FilmType.ToUpper() == "H")
                                                    //{
                                                    DateTime? not1stPostDate = null;
                                                    notIntNo = UpdateNoticeForHandWritten_WS(frameIntNo, out not1stPostDate, out errMessage);
                                                    if (notIntNo > 0 && string.IsNullOrEmpty(errMessage))
                                                    {
                                                        //queItem = new QueueItem()
                                                        //{
                                                        //    Body = notIntNo,
                                                        //    QueueType = ServiceQueueTypeList.Generate2ndNotice
                                                        //};
                                                        //queProcessor.Send(queItem);
                                                        //queItem = new QueueItem()
                                                        //{
                                                        //    Body = notIntNo,
                                                        //    ActDate = (not1stPostDate.HasValue ? not1stPostDate.Value : DateTime.Now).AddDays(rules.Rule("NotPosted1stNoticeDate", "NotIssueSummonsDate").DtRNoOfDays),
                                                        //    QueueType = ServiceQueueTypeList.GenerateSummons
                                                        //};
                                                        //queProcessor.Send(queItem);

                                                        // Oscar 20120315 changed the Group
                                                        NoticeDB noticeDB = new NoticeDB(this.connAARTODB);
                                                        NoticeDetails notice = noticeDB.GetNoticeDetails(notIntNo);
                                                        string[] group = new string[3];
                                                        group[0] = this.autCode.Trim();
                                                        //Jerry 2013-03-07 change
                                                        //group[1] = notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) ? "H" : "";
                                                        group[1] = (notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) && !notice.IsVideo) ? "H" : "";
                                                        group[2] = Convert.ToString(notice.NotCourtName);

                                                        int noOfDaysToSummons = rules.Rule("NotPosted1stNoticeDate", "NotIssueSummonsDate").DtRNoOfDays;
                                                        int daysPosted1stTo2ndNoticeDate = rules.Rule("NotPosted1stNoticeDate", "NotIssue2ndNoticeDate").DtRNoOfDays;
       
                                                        //Jerry 2013-03-06 add
                                                        //int daysPosted1stTo2ndNoticeDate = rules.Rule("NotPosted1stNoticeDate", "NotIssue2ndNoticeDate").DtRNoOfDays;
                                                        int daysIssue2ndNoticeTo2ndPaymentDate = rules.Rule("NotIssue2ndNoticeDate", "Not2ndPaymentDate").DtRNoOfDays;
                                                        int daysNotOffenceDateToNotPaymentDate = rules.Rule("NotOffenceDate", "NotPaymentDate").DtRNoOfDays;
                                                       //2014-11-4 Tommi add
                                                        DateRuleInfo dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotIssue2ndNoticeDate", "SMSExtractOn2ndNoticeDate");
                                                        int noOfDaysToSMSExtractOn2ndNoticeDate = dateRule.ADRNoOfDays;
                                                        //dls 20120424 - handwritten offences must be HWO
                                                        //Oscar 20120412 changed group
                                                        //string violationType = new FrameDB(this.connAARTODB).GetViolationType(frameIntNo);

                                                        // Nick 20120801 added CoCT Report check
                                                        //bool isIbmPrinter = rules.Rule("6209").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase);
                                                        string violationType = new FrameDB(this.connAARTODB).GetViolationType(frameIntNo);

                                                        //Jerry 2013-02-06 change
                                                        //if (isIbmPrinter)
                                                        //Jerry 2013-03-06 change it from && to ||
                                                        if (notice.IsVideo || rules.Rule("2500").ARString == "Y")
                                                        {
                                                            queProcessor.Send(new QueueItem()
                                                            {
                                                                Body = notIntNo,
                                                                Group = string.Format("{0}|{1}|{2}", this.autCode.Trim(), violationType, notice.IsVideo ? "VID" : "HWO"),
                                                                QueueType = ServiceQueueTypeList.Frame_Generate1stNotice,
                                                                LastUser = AARTOBase.LastUser
                                                            });
                                                        }
                                                        else
                                                        {
                                                            // 2013-09-03, Oscar added temporary solution for old notices
                                                            if (DateTime.Now.Subtract(notice.NotOffenceDate).Days > 120)
                                                            {
                                                                Logger.Info(ResourceHelper.GetResource("No2ndNoticeWillBeGenerated", notice.NotTicketNo, notice.NotOffenceDate.ToString("yyyy-MM-dd")));
                                                            }
                                                            // 2014-05-23, Oscar added checking
                                                            else if (!notice.NotFilmType.Equals2("H")
                                                                || !notice.NotStatisticsCode.Equals2("H341")
                                                                || !notice.NotNewOffender.Equals2("Y"))
                                                            {
                                                                queProcessor.Send(new QueueItem()
                                                                {
                                                                    Body = notIntNo,
                                                                    //Group = string.Format("{0}|{1}", this.autCode.Trim(), violationType),
                                                                    Group = string.Format("{0}|{1}", this.autCode.Trim(), "HWO"),
                                                                    ActDate = notice.NotOffenceDate.AddDays(daysNotOffenceDateToNotPaymentDate + 2),
                                                                    QueueType = ServiceQueueTypeList.Generate2ndNotice
                                                                });

                                                                //2014-11-4 Tommi add for SMS extract on2ndnotice
                                                                queProcessor.Send(new QueueItem()
                                                                {
                                                                    Body = notIntNo,
                                                                    Group =this.autCode.Trim(),
                                                                    ActDate = notice.NotOffenceDate.AddDays(daysNotOffenceDateToNotPaymentDate + 2 + noOfDaysToSMSExtractOn2ndNoticeDate),//-
                                                                    LastUser=AARTOBase.LastUser,
                                                                    QueueType = ServiceQueueTypeList.SMSExtractOn2ndNotice
                                                                });


                                                            }

                                                        }

                                                        //Jake 2014-07-22 added push SearchNameIDUpdate queue item for H notice
                                                        //after natis date updated
                                                        if (notice.NotFilmType.Equals2("H") || notice.NotStatisticsCode.Equals2("H341"))
                                                        {
                                                            queProcessor.Send(new QueueItem()
                                                            {
                                                                Body = notIntNo,
                                                                Group = autCode,
                                                                LastUser=AARTOBase.LastUser,
                                                                QueueType = ServiceQueueTypeList.SearchNameIDUpdate
                                                            }
                                                                );
                                                        }

                                                        //Jerry 2013-02-06 add
                                                        //Jerry 2013-03-06 delete if
                                                        //if (!notice.IsVideo && rules.Rule("2500").ARString == "N")
                                                        //{

                                                        // Oscar 2013-06-17 added
                                                        var isNoAog = noAogDB.IsNoAOG(notIntNo, NoAOGQueryType.Notice);

                                                        queProcessor.Send(
                                                            // Nick 20120801 changed for CoCT Report check
                                                            //new QueueItem()
                                                            //{
                                                            //    Body = notIntNo,
                                                            //    //Group = string.Format("{0}|{1}", this.autCode.Trim(), violationType),
                                                            //    Group = string.Format("{0}|{1}", this.autCode.Trim(), "HWO"),
                                                            //    QueueType = ServiceQueueTypeList.Generate2ndNotice
                                                            //},
                                                            new QueueItem()
                                                            {
                                                                Body = notIntNo,
                                                                //Group = this.autCode,
                                                                // Oscar 20120315 changed the Group
                                                                Group = string.Join("|", group),
                                                                //Jerry 2013-03-06 change action date
                                                                //ActDate = (not1stPostDate.HasValue ? not1stPostDate.Value : DateTime.Now).AddDays(noOfDaysToSummons),
                                                                ActDate = isNoAog ? noAogDB.GetNoAOGGenerateSummonsActionDate(autIntNo, notice.NotOffenceDate) : (rules.Rule("2500").ARString == "Y" ? notice.NotOffenceDate.AddDays(noOfDaysToSummons) :
                                                                    notice.NotOffenceDate.AddDays(daysNotOffenceDateToNotPaymentDate + daysIssue2ndNoticeTo2ndPaymentDate + 4)),
                                                                //jerry 2012-03-31 add Priority
                                                                Priority = (notice.NotOffenceDate.Date - DateTime.Now.Date).Days,
                                                                //Jerry 2012-05-14 add LastUser
                                                                LastUser = AARTOBase.LastUser,
                                                                QueueType = ServiceQueueTypeList.GenerateSummons
                                                            }
                                                            );

                                                        // 2013-03-27 Henry add
                                                        if (rules.Rule("5050").ARString.Trim().ToUpper() == "Y")
                                                        {
                                                            queProcessor.Send(
                                                                new QueueItem()
                                                                {
                                                                    Body = notIntNo,
                                                                    Group = string.Join("|", group),
                                                                    ActDate = notice.NotOffenceDate.AddMonths(rules.Rule("5050").ARNumeric).AddDays(-5),
                                                                    Priority = (notice.NotOffenceDate.Date - DateTime.Now.Date).Days,
                                                                    LastUser = AARTOBase.LastUser,
                                                                    QueueType = ServiceQueueTypeList.GenerateSummons
                                                                }
                                                                );
                                                        }
                                                        //}

                                                        //Nick 20120322 add to push data washing queue
                                                        string dwEnabled = rules.Rule("9060").ARString.Trim();
                                                        if (dwEnabled == "Y")
                                                        {
                                                            string IDNum = null;
                                                            bool needDataWash = false;
                                                            int dwRecycle = rules.Rule("9080").ARNumeric;
                                                            string notSendTo = notice.NotSendTo.Trim();
                                                            switch (notSendTo)
                                                            {
                                                                case "D":
                                                                    DriverDB driverDB = new DriverDB(connAARTODB);
                                                                    DriverDetails drvDet = driverDB.GetDriverDetailsByNotice(notIntNo);
                                                                    if (drvDet != null) IDNum = (drvDet.DrvIDNumber + "").Trim();
                                                                    break;
                                                                case "O":
                                                                    OwnerDB ownerDB = new OwnerDB(connAARTODB);
                                                                    OwnerDetails ownDet = ownerDB.GetOwnerDetailsByNotice(notIntNo);
                                                                    if (ownDet != null) IDNum = (ownDet.OwnIDNumber + "").Trim();
                                                                    break;
                                                                case "P":
                                                                    ProxyDB proxyDB = new ProxyDB(connAARTODB);
                                                                    ProxyDetails pxyDet = proxyDB.GetProxyDetailsByNotice(notIntNo);
                                                                    if (pxyDet != null) IDNum = (pxyDet.PrxIDNumber + "").Trim();
                                                                    break;
                                                                default:
                                                                    break;
                                                            }
                                                            if (!String.IsNullOrEmpty(IDNum))
                                                            {
                                                                MI5DB mi5 = new MI5DB(connAARTODB);
                                                                DataSet personDet = mi5.GetPersonaDetailsByID(IDNum, AARTOBase.LastUser);
                                                                if (personDet != null && personDet.Tables.Count > 0 && personDet.Tables[0].Rows.Count > 0)
                                                                {
                                                                    string strLastSentDWDate = personDet.Tables[0].Rows[0]["MIPSentToDataWasherDate"] + "";
                                                                    string strLastDWDate = personDet.Tables[0].Rows[0]["MIPLastWashedDate"] + "";
                                                                    DateTime? lastSentDWDate = strLastSentDWDate == "" ? null : (DateTime?)Convert.ToDateTime(strLastSentDWDate);
                                                                    DateTime? lastDWDate = strLastDWDate == "" ? null : (DateTime?)Convert.ToDateTime(strLastDWDate);

                                                                    if ((!lastSentDWDate.HasValue && !lastDWDate.HasValue) ||
                                                                        (lastSentDWDate.HasValue && !lastDWDate.HasValue && Convert.ToDateTime(lastSentDWDate).AddMonths(dwRecycle) < DateTime.Now))
                                                                    {
                                                                        needDataWash = true;
                                                                    }
                                                                    else
                                                                    {
                                                                        lastSentDWDate = lastSentDWDate.HasValue ? lastSentDWDate : DateTime.Parse("1900-01-01");
                                                                        if (lastSentDWDate < lastDWDate && Convert.ToDateTime(lastDWDate).AddMonths(dwRecycle) < DateTime.Now)
                                                                        {
                                                                            needDataWash = true;
                                                                        }
                                                                    }
                                                                }

                                                                if (needDataWash)
                                                                {
                                                                    int daysDWToSummon = rules.Rule("DataWashDate", "NotIssueSummonDate").DtRNoOfDays;
                                                                    queProcessor.Send(
                                                                        new QueueItem()
                                                                        {
                                                                            Body = notIntNo,
                                                                            Group = this.autCode.Trim(),
                                                                            ActDate = (not1stPostDate.HasValue ? not1stPostDate.Value : DateTime.Now).AddDays(noOfDaysToSummons - daysDWToSummon),
                                                                            QueueType = ServiceQueueTypeList.DataWashingExtractor
                                                                        }
                                                                        );
                                                                }
                                                            }

                                                        }

                                                    }
                                                    // 2013-12-24, Oscar added for error handling
                                                    else
                                                    {
                                                        AARTOBase.LogProcessing(ResourceHelper.GetResource("UpdateNoticeForHandWrittenFailed", frameIntNo, notIntNo) + (string.IsNullOrWhiteSpace(errMessage) ? "" : " " + errMessage), LogType.Error, ServiceOption.ContinueButQueueFail, this.queueItem);
                                                        //continue;
                                                        return false; // means continue
                                                    }
                                                    //}
                                                }
                                                else if (frameStatus == 800)
                                                {
                                                    if (this.expiryNoOfDays == 0)
                                                    {
                                                        this.expiryNoOfDays = rules.Rule("NotOffenceDate", "NotIssue1stNoticeDate").DtRNoOfDays;
                                                    }
                                                    //queItem = new QueueItem()
                                                    //{
                                                    //    Body = frameIntNo,
                                                    //    Priority = this.expiryNoOfDays - ((TimeSpan)(DateTime.Now - frame.OffenceDate)).Days,
                                                    //    QueueType = ServiceQueueTypeList.Frame_Generate1stNotice
                                                    //};
                                                    string violationType = new FrameDB(this.connAARTODB).GetViolationType(frame.FrameIntNo);
                                                    queProcessor.Send(
                                                        new QueueItem()
                                                        {
                                                            Body = frameIntNo,
                                                            Group = string.Format("{0}|{1}", this.autCode.Trim(), violationType),
                                                            Priority = this.expiryNoOfDays - ((TimeSpan)(DateTime.Now - frame.OffenceDate)).Days,
                                                            QueueType = ServiceQueueTypeList.Frame_Generate1stNotice
                                                        }
                                                        );
                                                }

                                                //Jerry 2013-04-10 add for secondary offence for Vehicle license expired
                                                if (!film.FilmType.Equals("H", StringComparison.OrdinalIgnoreCase))
                                                {
                                                    if (frame.ParentFrameIntNo == null && rules.Rule("0590").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase)
                                                        && frame.FrameVehicleLicenceExpiry != null && frame.FrameVehicleLicenceExpiry < frame.OffenceDate)
                                                    {
                                                        //SIL.AARTO.DAL.Entities.SecondaryOffence secondaryOffenceEntity = new SIL.AARTO.DAL.Services.SecondaryOffenceService().GetBySeOfCode("78011"); //78011: Vehicle license expired code
                                                        // 2013-12-24, Oscar promoted dbService to top
                                                        var secondaryOffenceEntity = this.secondaryOffenceService.GetBySeOfCode("78011"); //78011: Vehicle license expired code
                                                        if (secondaryOffenceEntity != null)
                                                        {
                                                            FrameManager.CreateFrameAboutSecondaryOffence(film.FilmNo, frame.FrameNo, secondaryOffenceEntity.SeOfCode, AARTOBase.LastUser);
                                                        }
                                                    }
                                                }

                                            }//end else send back to Dms

                                        }//end frame!=null
                                    }
                                }
                                //fieldList.Clear();    // 2014-02-08, Oscar moved fieldList.Clear() from bottom to top.
                                //if (ServiceUtility.TransactionCanCommit())  // 2013-12-24, Oscar added for error handling
                                //scope.Complete();
                                //scope.Dispose();

                                return true;
                            });
                            if (failed) return failed;

                            //}
                        }
                        catch (Exception ex)
                        {
                            // 2013-12-24, Oscar added for error handling
                            AARTOBase.LogProcessing(ex, LogType.Error, ServiceOption.ContinueButQueueFail, this.queueItem);
                        }
                    }
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                AARTOBase.ErrorProcessing(ex);
                return true;    // means failed
            }
            return failed;
        }

        private void SendBackToDMS(int noticeIntNo, int frameIntNo)
        {
            string frameNaTISErrorFieldList = "";
            string frameNaTISIndicator = "";
            //Heidi 2013-10-21 changed for send back to DMS need to check DMS imported S341 Document
            //NoticeDetails noticeDetails = updateNotice.GetNoticeDataByFrameIntNo(frameIntNo);
            //int noticeIntNo = 0;
            //if (noticeDetails != null)
            //{
            //    noticeIntNo = noticeDetails.NotIntNo;
            //}
            //SIL.AARTO.DAL.Entities.Notice noticeEntity = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(noticeIntNo);
            // 2013-12-24, Oscar promoted dbService to top
            var noticeEntity = this.noticeService.GetByNotIntNo(noticeIntNo);
            if (noticeEntity != null)
            {
                if (noticeEntity.NoticeStatus < 900 && noticeEntity.NoticeStatus != 410)
                {
                    noticeEntity.NoticeStatus = (int)ChargeStatusList.DMSDocumentCorrection;
                    noticeEntity.LastUser = AARTOBase.LastUser;
                    noticeEntity.IsHwoCorrection = true;
                    //new SIL.AARTO.DAL.Services.NoticeService().Update(noticeEntity);
                    // 2013-12-24, Oscar promoted dbService to top
                    this.noticeService.Update(noticeEntity);
                }
                //SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.Charge> chargeEntityList = new SIL.AARTO.DAL.Services.ChargeService().GetByNotIntNo(noticeEntity.NotIntNo);
                // 2013-12-24, Oscar promoted dbService to top
                var chargeEntityList = this.chargeService.GetByNotIntNo(noticeEntity.NotIntNo);
                foreach (var charge in chargeEntityList)
                {
                    if (charge.ChargeStatus < 900 && charge.ChargeStatus != 410)
                    {
                        charge.ChargeStatus = (int)ChargeStatusList.DMSDocumentCorrection;
                        charge.LastUser = AARTOBase.LastUser;
                        //new SIL.AARTO.DAL.Services.ChargeService().Update(charge);
                        // 2013-12-24, Oscar promoted dbService to top
                        this.chargeService.Update(charge);
                    }
                }

                //SIL.AARTO.DAL.Entities.Frame frameEntity = new SIL.AARTO.DAL.Services.FrameService().GetByFrameIntNo(frameIntNo);
                // 2013-12-24, Oscar promoted dbService to top
                var frameEntity = this.frameService.GetByFrameIntNo(frameIntNo);
                if (frameEntity != null)
                {
                    frameNaTISErrorFieldList = string.IsNullOrEmpty(frameEntity.FrameNaTisErrorFieldList) ? "" : frameEntity.FrameNaTisErrorFieldList;
                    frameNaTISIndicator = string.IsNullOrEmpty(frameEntity.FrameNatisIndicator) ? "" : frameEntity.FrameNatisIndicator;
                    frameEntity.FrameStatus = (int)FrameStatusList.NoNatisDataS341HW;
                    //new SIL.AARTO.DAL.Services.FrameService().Update(frameEntity);
                    // 2013-12-24, Oscar promoted dbService to top
                    this.frameService.Update(frameEntity);
                }

                // 2014-03-20, Oscar added
                queProcessor.Send(new QueueItem
                {
                    Body = frameIntNo,
                    Group = autCode,
                    ActDate = DateTime.Now.AddHours(1), // at least 1 hour later
                    QueueType = ServiceQueueTypeList.ReprocessNatisErrorsToDMS,
                    LastUser = AARTOBase.LastUser
                });

                AARTOBase.LogProcessing(ResourceHelper.GetResource("DocumentResentToDMS", frameIntNo), LogType.Info, ServiceOption.Continue, queueItem, queueItem.Status);

                #region removed

                //if (!string.IsNullOrEmpty(frameNaTISIndicator) && frameNaTISIndicator!="Y")
                //{
                //    if (frameNaTISErrorFieldList.Trim() == "003")
                //    {
                //        frameNaTISErrorFieldList = frameNaTISErrorFieldList + ResourceHelper.GetResource("NatisErrorMsg003");
                //    }
                //    else if (frameNaTISErrorFieldList.Trim() == "060")
                //    {
                //        frameNaTISErrorFieldList = frameNaTISErrorFieldList + ResourceHelper.GetResource("NatisErrorMsg060");
                //    }
                //    else if (frameNaTISIndicator.Trim() == "!")
                //    {
                //        frameNaTISIndicator = " ";
                //    }
                //    else
                //    {
                //        frameNaTISIndicator = " ";
                //        frameNaTISErrorFieldList = frameNaTISErrorFieldList + ResourceHelper.GetResource("NatisErrorMsgUnknown");
                //    }
                //}
                //string errorMsg = string.Format(ResourceHelper.GetResource("NatisErrorMsg"), frameNaTISIndicator, frameNaTISErrorFieldList);
                ////update DMS 
                //UpdateDMS(noticeEntity.NotFilmNo.Trim(), noticeEntity.NotFrameNo.Trim(), errorMsg);

                #endregion
            }
            else
            {
                this.Logger.Info(string.Format(ResourceHelper.GetResource("NoNoticeByFrameIntNo"), AARTOBase.LastUser, frameIntNo));
            }
        }

        private void UpdateDMS(string batchID, string batchDocID, string reasonMsg)
        {
            //Insert err msg data into DMS
            SIL.DMS.DAL.Entities.DocumentErrorMessage docErrMsg = new DMS.DAL.Entities.DocumentErrorMessage();
            docErrMsg.BatchId = batchID;
            docErrMsg.BaDoId = batchDocID;
            // Paole 20121031  for display the category of error message on DMS QC page, temporary proposal， not checked by DEV
            docErrMsg.DoErMeCode = "HW Correction reason";
            docErrMsg.DoErMessage = reasonMsg;
            docErrMsg.DoErMeDateCreated = DateTime.Now;
            docErrMsg.LastUser = AARTOBase.LastUser;
            //new SIL.DMS.DAL.Services.DocumentErrorMessageService().Save(docErrMsg);
            // 2013-12-24, Oscar promoted dbService to top
            this.documentErrorMessageService.Save(docErrMsg);

            //update document table
            //SIL.DMS.DAL.Entities.BatchDocument batchDoc = new SIL.DMS.DAL.Services.BatchDocumentService().GetByBaDoId(batchDocID);
            // 2013-12-24, Oscar promoted dbService to top
            var batchDoc = this.batchDocumentService.GetByBaDoId(batchDocID);
            batchDoc.BaDoStId = (int)SIL.DMS.DAL.Entities.BatchDocumentStatusList.DocumentReturnedToQC;
            batchDoc.IsBackFromAarto = true;
            batchDoc.LastUser = AARTOBase.LastUser; //Heidi 2013-10-21 added
            //new SIL.DMS.DAL.Services.BatchDocumentService().Update(batchDoc);
            // 2013-12-24, Oscar promoted dbService to top
            this.batchDocumentService.Update(batchDoc);
        }

        private bool UpdateNatisError()
        {
            bool failed = false;
            string fileName = this.fileInfo.Name;

            //read through file 
            StreamReader sr = new StreamReader(this.fileInfo.OpenRead(), true);
            int row = 0;
            string srLine = " ";
            string nrCode = "";
            string nrError = "";
            string errMessage = "";
            string nfFileName = fileName.Substring(0, 7);

            bool eof = false; //set when row with 9999999999999 is reached
            while (sr.Peek() >= 0 && !eof)
            {
                srLine = sr.ReadLine();
                row += 1;
                if (!srLine.StartsWith("5"))
                {
                    nrCode = srLine.Substring(0, 1);
                    nrError = srLine;
                }
            }
            sr.Close();

            NaTISFilesDB natis = new NaTISFilesDB(this.connAARTODB);

            int nfIntNo = natis.UpdateNatisFilesError(autIntNo, nfFileName, nrCode, nrError, AARTOBase.LastUser, ref errMessage);

            if (nfIntNo < 0)
            {
                this.Logger.Info(ResourceHelper.GetResource("UpdateNatisErrorNoRowForFile", fileName, autName));
                failed = true;
            }
            else if (nfIntNo == 0)
            {
                this.Logger.Info(ResourceHelper.GetResource("UpdateNatisErrorFailedLoadErrorFile", fileName, errMessage));
                failed = true;
            }
            else
                this.Logger.Info(ResourceHelper.GetResource("UpdateNatisErrorFileUpdated", fileName));

            return failed;
        }

        private int UpdateNoticeForHandWritten_WS(int frameIntNo, out DateTime? not1stPostDate, out string message)
        {
            not1stPostDate = DateTime.Now;
            DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));
            SqlParameter[] paras = new SqlParameter[6];
            paras[0] = new SqlParameter("@AutIntNo", this.autIntNo);
            paras[1] = new SqlParameter("@FrameIntNo", frameIntNo);
            paras[2] = new SqlParameter("@StatusNotice", 900);
            paras[3] = new SqlParameter("@NatisLast", this.natisLast ? 'Y' : 'N');
            paras[4] = new SqlParameter("@LastUser", AARTOBase.LastUser);
            paras[5] = new SqlParameter("@NotPosted1stNoticeDate", not1stPostDate) { Direction = ParameterDirection.InputOutput };
            object obj = db.ExecuteScalar("UpdateNoticeForHandWritten_WS", paras, out message);
            int result;
            if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(message))
                result = Convert.ToInt32(obj);
            else
                result = 0;
            if (paras[5].Value != null)
                not1stPostDate = Convert.ToDateTime(paras[5].Value);
            else
                not1stPostDate = null;
            return result;
        }

    }
}
