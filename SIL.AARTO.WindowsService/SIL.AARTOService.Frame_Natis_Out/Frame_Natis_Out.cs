﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Frame_Natis_Out
{
    partial class Frame_Natis_Out : ServiceHost
    {
        public Frame_Natis_Out()
            : base("", new Guid("818EB6E0-3C91-4379-B3CC-2E58DB598B20"), new Frame_Natis_OutClientService())
        {
            InitializeComponent();
        }
    }
}
