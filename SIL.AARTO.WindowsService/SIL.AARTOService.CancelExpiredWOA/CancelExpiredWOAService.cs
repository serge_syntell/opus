﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using SIL.AARTOService.Resource;
using Stalberg.TMS;

namespace SIL.AARTOService.CancelExpiredWOA
{
    public class CancelExpiredWOAService : ServiceDataProcessViaQueue
    {
        public CancelExpiredWOAService()
            : base("", "", new Guid("B02F999D-0A76-432F-8F69-D2F6F7DDD867"), ServiceQueueTypeList.CancelExpiredWOA)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
        }

        private const string TICKET_PROCESSOR_TMS = "TMS";
        private const string TICKET_PROCESSOR_AARTO = "AARTO";
        private const string TICKET_PROCESSOR_JMPD_AARTO = "JMPD_AARTO";

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        AuthorityDetails authDetails = null;
        string connectStr = string.Empty;
        string strAutCode = string.Empty;
        string autName = string.Empty;
        int autIntNo;

        int noDaysForWOAExpiry;

        public sealed override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && ServiceUtility.IsNumeric(body))
            {
                item.IsSuccessful = true;
            }
            else
            {
                // jerry 2012-02-21 change
                //item.IsSuccessful = false;
                //item.Status = QueueItemStatus.UnKnown;
                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.lastUser, item.Body));
                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.LastUser, item.Body), true);
            }

            if (strAutCode != item.Group)
            {
                strAutCode = item.Group.Trim();
                //jerry 2012-04-05 check group value
                if (string.IsNullOrWhiteSpace(strAutCode))
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
                    return;
                }

                this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode);

                //Oscar 20120903 add this validation
                if (this.authDetails == null)
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
                    AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                    return;
                }

                autIntNo = authDetails.AutIntNo;
                autName = authDetails.AutName;

                //Jerry 2012-12-21 change
                int woaIntNo = Convert.ToInt32(item.Body);
                SIL.AARTO.DAL.Entities.Woa woaEntity = new SIL.AARTO.DAL.Services.WoaService().GetByWoaIntNo(woaIntNo);
                SIL.AARTO.DAL.Entities.NoticeSummons noticeSummonsEntity = new SIL.AARTO.DAL.Services.NoticeSummonsService().GetBySumIntNo(woaEntity.SumIntNo).FirstOrDefault();
                SIL.AARTO.DAL.Entities.Notice noticeEntity = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(noticeSummonsEntity.NotIntNo);

                //if (this.authDetails.AutTicketProcessor != TICKET_PROCESSOR_AARTO
                //    && this.authDetails.AutTicketProcessor != TICKET_PROCESSOR_JMPD_AARTO
                //    && this.authDetails.AutTicketProcessor != TICKET_PROCESSOR_TMS)
                if (noticeEntity != null && noticeEntity.NotTicketProcessor != TICKET_PROCESSOR_TMS)
                {
                    //Jake 2013-07-12 add message when discard queue 
                    item.Status = QueueItemStatus.Discard;
                    item.IsSuccessful = false;
                    this.Logger.Info(String.Format("Invalid notice ticket processor {0}", (noticeEntity == null ? "" : noticeEntity.NotTicketProcessor)), item.Body.ToString());
                }

                rules.InitParameter(connectStr, strAutCode);
            }
        }

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            int woaIntNo, success;
            string msg = string.Empty;

            // jerry 2012-02-21 change
            //foreach (QueueItem item in queueList)
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                try
                {
                    if (item.IsSuccessful)
                    {
                        woaIntNo = Convert.ToInt32(item.Body);

                        //get WOA expiry date
                        noDaysForWOAExpiry = rules.Rule("WOAIssueDate", "WOAExpireDate").DtRNoOfDays;

                        success = UpdateExpiredWOA(woaIntNo, ref msg);
                        switch (success)
                        {
                            case -1:
                                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrMsgUpdateChargeInCancelExpiredWOA"), woaIntNo));
                                //item.Status = QueueItemStatus.UnKnown;
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrMsgUpdateChargeInCancelExpiredWOA"), woaIntNo));
                                break;
                            case -2:
                                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrMsgUpdateNoticeInCancelExpiredWOA"), woaIntNo));
                                //item.Status = QueueItemStatus.UnKnown;
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrMsgUpdateNoticeInCancelExpiredWOA"), woaIntNo));
                                break;
                            case -3:
                                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrMsgUpdateWOAInCancelExpiredWOA"), woaIntNo));
                                //item.Status = QueueItemStatus.UnKnown;
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrMsgUpdateWOAInCancelExpiredWOA"), woaIntNo));
                                break;
                            case -4:
                                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrMsgUpdateEvidenceInCancelExpiredWOA"), woaIntNo));
                                //item.Status = QueueItemStatus.UnKnown;
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrMsgUpdateEvidenceInCancelExpiredWOA"), woaIntNo));
                                break;
                            case -5:
                                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrMsgExeSPInCancelExpiredWOA"), msg, woaIntNo));
                                //item.Status = QueueItemStatus.UnKnown;
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrMsgExeSPInCancelExpiredWOA"), msg, woaIntNo), true);
                                break;
                            //Jake 2015-03-05 added return value -6 and -7
                            case -6:
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("WOAExpiryServiceMessage_1"), woaIntNo));
                                item.Status = QueueItemStatus.Discard;
                                break;
                            case -7:
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("WOAExpiryServiceMessage_2"), woaIntNo));
                                item.Status = QueueItemStatus.Discard;
                                break;
                            case 0:
                                item.Status = QueueItemStatus.Discard;
                                break;
                            case 1:
                                this.Logger.Info(string.Format(ResourceHelper.GetResource("SuccessfulMsgInCancelExpiredWOA"), woaIntNo));
                                item.Status = QueueItemStatus.Discard;
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //item.Status = QueueItemStatus.UnKnown;
                    //this.Logger.Error(ex);
                    //throw ex;
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        private int UpdateExpiredWOA(int woaIntNo, ref string errMsg)
        {
            DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));

            SqlParameter[] paras = new SqlParameter[3];
            paras[0] = new SqlParameter("@WOAIntNo", woaIntNo);
            paras[1] = new SqlParameter("@NoDaysForWOAExpiry", noDaysForWOAExpiry);
            paras[2] = new SqlParameter("@LastUser", AARTOBase.LastUser);

            object obj = db.ExecuteScalar("WOAExpired_WS", paras, out errMsg);
            int result;
            if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(errMsg))
                result = Convert.ToInt32(obj);
            else
                result = -5;

            return result;
        }
    }
}
