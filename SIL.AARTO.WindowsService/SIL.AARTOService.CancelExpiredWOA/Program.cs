﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CancelExpiredWOA
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.CancelExpiredWOA"
                ,
                DisplayName = "SIL.AARTOService.CancelExpiredWOA"
                ,
                Description = "SIL AARTOService CancelExpiredWOA"
            };

            ProgramRun.InitializeService(new CancelExpiredWOA(), serviceDescriptor, args);
        }
    }
}
