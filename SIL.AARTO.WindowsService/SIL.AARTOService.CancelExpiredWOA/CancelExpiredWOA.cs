﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CancelExpiredWOA
{
    partial class CancelExpiredWOA : ServiceHost
    {
        public CancelExpiredWOA()
            : base("", new Guid("8E984EC6-BB7A-4937-8AE2-65DAF95D4AB3"), new CancelExpiredWOAService())
        {
            InitializeComponent();
        }
    }
}
