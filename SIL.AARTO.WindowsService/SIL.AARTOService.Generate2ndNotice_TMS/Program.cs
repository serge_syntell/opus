﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Generate2ndNotice_TMS
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.Generate2ndNotice_TMS"
                ,
                DisplayName = "SIL.AARTOService.Generate2ndNotice_TMS"
                ,
                Description = "SIL AARTOService Generate2ndNotice_TMS"
            };

            ProgramRun.InitializeService(new Generate2ndNotice_TMS(), serviceDescriptor, args);
        }
    }
}
