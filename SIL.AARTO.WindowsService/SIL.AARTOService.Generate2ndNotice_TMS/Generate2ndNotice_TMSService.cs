﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using System.Configuration;
using SIL.QueueLibrary;
using SIL.AARTOService.Library;
using System.Data;
using System.Diagnostics;
using Stalberg.TMS;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Collections;
using SIL.AARTOService.Resource;
using Stalberg.TMS_TPExInt.Components;
using SIL.ServiceBase;


namespace SIL.AARTOService.Generate2ndNotice_TMS
{
    public class Generate2ndNotice_TMSService : ServiceDataProcessViaQueue
    {
        public Generate2ndNotice_TMSService()
            : base("", "", new Guid("52130683-8E2D-4506-80E8-CEDD9F0774B5"), ServiceQueueTypeList.Generate2ndNotice)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            this.connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            AARTOBase.OnServiceStarting = () =>
            {
                this.groupStr = null;
                this.strAutCode = null;
                this.stamp = DateTime.Now;
                this.lastStamp = DateTime.Now;
                this.sequence = 0;
                this.pfnList.Clear();
                SetServiceParameters();
            };
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        //NoticeDB noticeDB;
        string strAutCode = null;
        string connectStr;
        int AutIntNo;
        string DataWashingActived = null;
        int DataWashingRecycleMonth;

        List<int> pfnList = new List<int>();
        bool isChecked;
        int sequence;
        DateTime stamp, lastStamp;
        string stampStr;
        int daysOf2ndNotice, daysOf2ndPayment;
        int notIntNo;
        string groupStr;
        bool isIbmPrinter;

        //public override void PrepareWork()
        //{
        //    connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
        //    noticeDB = new NoticeDB(connectStr);
        //    strAutCode = null;
        //}

        public override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrWhiteSpace(body) && !string.IsNullOrWhiteSpace(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;

                    //int notIntNo;
                    if (!int.TryParse(body, out this.notIntNo))
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        AARTOBase.DeQueueInvalidItem(ref item);
                        return;
                    }

                    //NoticeDetails notice = noticeDB.GetNoticeDetails(notIntNo);
                    //if (notice.NoticeStatus != STATUS_1ST_NOTICE_POSTED)
                    //{
                    //    AARTOBase.ErrorProcessing(ref item);
                    //    return;
                    //}

                    //AARTOBase.BatchCount--;

                    string groupStr = item.Group.Trim();
                    string[] group = groupStr.Split('|');
                    string autCode = group[0].Trim();

                    if (groupStr != this.groupStr)
                    {
                        if (this.groupStr != null)
                        {
                            //AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Retry);
                            //AARTOBase.SkipReceivingData = true;
                            AARTOBase.SkipReceivingQueueData(ref item);
                            this.groupStr = null;
                            this.strAutCode = null;
                            return;
                        }

                        this.groupStr = groupStr;
                        this.strAutCode = autCode;
                        QueueGroupValidation(ref item);
                    }
                    else if (!this.isChecked)
                    {
                        AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", autCode));
                        AARTOBase.DeQueueInvalidItem(ref item);
                        return;
                    }

                    QueueItemValidation(ref item);
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                    AARTOBase.DeQueueInvalidItem(ref item);
                    return;
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                AARTOBase.DeQueueInvalidItem(ref item);
                return;
            }
        }

        private void QueueGroupValidation(ref QueueItem item)
        {
            this.isChecked = false;

            AuthorityDetails authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim().Equals(this.strAutCode, StringComparison.OrdinalIgnoreCase));
            if (authDetails == null)
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
                AARTOBase.DeQueueInvalidItem(ref item);
                return;
            }

            this.AutIntNo = authDetails.AutIntNo;
            rules.InitParameter(this.connectStr, this.strAutCode);

            this.DataWashingActived = rules.Rule("9060").ARString.Trim();
            this.DataWashingRecycleMonth = rules.Rule("9080").ARNumeric;
            this.daysOf2ndNotice = rules.Rule("NotPosted1stNoticeDate", "NotIssue2ndNoticeDate").DtRNoOfDays;
            this.daysOf2ndPayment = rules.Rule("NotIssue2ndNoticeDate", "Not2ndPaymentDate").DtRNoOfDays;
            this.isIbmPrinter = rules.Rule("6209").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase);

            this.isChecked = true;
        }

        private void QueueItemValidation(ref QueueItem item)
        {
            if (!isChecked || !item.IsSuccessful) return;
            NoticeDetails notice = new NoticeDB(this.connectStr).GetNoticeDetails(this.notIntNo);
            // change >900 to >=600 
           
            if (notice == null || notice.NoticeStatus <= 0 || notice.NoticeStatus >= 600)
            {
                AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                return;
            }
           

            if (notice.NoticeStatus != STATUS_1ST_NOTICE_POSTED || notice.NotPosted1stNoticeDate == default(DateTime))
            {
                AARTOBase.DeQueueInvalidItem(ref item);
                return;
            }

            //2014.11.04 Tommi add 
            if (notice.SMSFor2ndNotice != default(DateTime) &&notice.SMSFor2ndNotice != null)
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("SuccessSMSSendNoticeToOffenderOn2ndNotice"), LogType.Info, ServiceOption.Continue,item,QueueItemStatus.Discard);
                AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);//2015-03-10 Heidi added for fixing Stop it continue to run the mainwork function(bontq1907).
                return;
            }

         
            if (notice.NotIssue2ndNoticeDate != default(DateTime))
            {
                AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                return;
            }

            DataSet chargeList = new ChargeDB(this.connectStr).GetChargeListByNoticeDS(this.notIntNo, 0);
            if (chargeList != null && chargeList.Tables.Count > 0 && chargeList.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in chargeList.Tables[0].Rows)
                {
                    string chgNoAOG = dr["ChgNoAOG"].ToString().Trim();
                    if (!chgNoAOG.Equals("N", StringComparison.OrdinalIgnoreCase))
                    {
                        AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                        return;
                    }
                }
            }

            //Jerry 2014-01-17 change the action date = NotPaymentDate + 1
            //Jerry 2013-03-06 add
            if (notice.NotPaymentDate != null && DateTime.Now <= notice.NotPaymentDate)
            {
                item.IsSuccessful = false;
                item.ActDate = notice.NotPaymentDate.AddDays(1);
                item.Status = QueueItemStatus.Retry;
                return;
            }

            AARTOBase.BatchCount--;
        }

        string batchFilename;
        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            string msg = string.Empty;
            QueueItemProcessor queueProcessor = new QueueItemProcessor();

            try
            {
                if (queueList.Count > 0) this.stampStr = GetNewPrintFileName("2NB", "yyyy-MM-dd_HH-mm-ss");
            }
            catch(Exception ex)
            {
                // oscar 20120619, add try-catch as Dawn asked, for client testing.
                AARTOBase.ErrorProcessing(ex, "line 210");
            }

            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                if (!item.IsSuccessful)
                    continue;

                try
                {
                    //if (strAutCode != item.Group.Trim())
                    //{
                    //    if (strAutCode != null)
                    //        Counter++;

                    //    strAutCode = item.Group.Trim();
                    //    AutIntNo = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode.Trim()).AutIntNo;
                    //    AARTORules.GetSingleTon().InitParameter(connectStr, strAutCode);

                    //    batchFilename = string.Format("{0}_{1}", fileName, (Counter).ToString("000"));
                    //    DataWashingActived = rules.Rule("9060").ARString.Trim();
                    //    DataWashingRecycleMonth = rules.Rule("9080").ARNumeric;
                    //}

                    //int notIntNo;
                    //if (!int.TryParse(item.Body.ToString(), out notIntNo))
                    //{
                    //    continue;
                    //}

                    this.notIntNo = Convert.ToInt32(item.Body);
                    this.batchFilename = this.stampStr;

                    //bool failed = PrepareSecondNotices(this.notIntNo);
                    // Oscar 20120424, fixed the error message bug
                    int success = 0;
                    try
                    {
                        success = PrepareSecondNotices(this.notIntNo, out msg);
                    }
                    catch(Exception ex)
                    {
                        // oscar 20120619, add try-catch as Dawn asked, for client testing.
                        AARTOBase.ErrorProcessing(ref item, "line 253; " + ex.Message, true);
                        return;
                    }

                    try
                    {
                        if (success < 0)
                        {
                            AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("SecondNotice_Error", msg), false, QueueItemStatus.PostPone);
                        }
                        else if (success == 0)
                        {
                            item.IsSuccessful = false;
                            item.Status = QueueItemStatus.PostPone;
                            this.Logger.Info(msg);
                        }
                        else
                        {
                            string errMsg;
                            int pfnIntNo = AARTOBase.SavePrintFileName(PrintFileNameType.Notice, this.batchFilename, this.AutIntNo, this.notIntNo, out errMsg);

                            if (pfnIntNo <= 0)
                            {
                                AARTOBase.ErrorProcessing(ref item, errMsg, true);
                                return;
                            }

                            if (!isIbmPrinter)
                            {
                                if (!this.pfnList.Contains(pfnIntNo))
                                {
                                    this.pfnList.Add(pfnIntNo);

                                    //Oscar 20120412 changed group
                                    int frameIntNo = new NoticeDB(this.connectStr).GetFrameForNotice(this.notIntNo);
                                    string violationType = new FrameDB(this.connectStr).GetViolationType(frameIntNo);

                                    //Oscar 20120704 added
                                    string groupStr = item.Group.Trim();
                                    string[] group = groupStr.Split('|');
                                    string autCode = group[0].Trim();

                                    QueueItem pushItem = new QueueItem();
                                    pushItem = new QueueItem();
                                    pushItem.Body = pfnIntNo;
                                    //pushItem.Group = string.Format("{0}|{1}", this.strAutCode.Trim(), violationType);
                                    pushItem.Group = string.Format("{0}|{1}", autCode, violationType);
                                    // 2014-08-14 Jerry fixed action date
                                    //pushItem.ActDate = this.printActionDate;
                                    pushItem.ActDate = DateTime.Now.AddHours(this.delayHours);
                                    pushItem.QueueType = ServiceQueueTypeList.Print2ndNotice;
                                    queueProcessor.Send(pushItem);
                                }
                            }
                            else
                            {
                                if (!this.pfnList.Contains(pfnIntNo))
                                {
                                    this.pfnList.Add(pfnIntNo);
                                    //2013-12-24 Heidi added for Push queue for PrintToFile Service (5101)
                                    string _groupStr = item.Group.Trim();
                                    string[] _group = _groupStr.Split('|');
                                    string _autCode = _group[0].Trim();

                                    QueueItem printToFileItem = new QueueItem()
                                    {
                                        Body = pfnIntNo,
                                        Group = _autCode + "|" + (int)SIL.AARTO.DAL.Entities.ReportConfigCodeList.SecondNoticeDirectPrinting,
                                        // 2014-08-14 Jerry fixed action date
                                        //ActDate = this.printActionDate,
                                        ActDate = DateTime.Now.AddHours(this.delayHours),
                                        QueueType = ServiceQueueTypeList.PrintToFile,
                                        LastUser = AARTOBase.LastUser
                                    };
                                    queueProcessor.Send(printToFileItem);
                                }
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        // oscar 20120619, add try-catch as Dawn asked, for client testing.
                        AARTOBase.ErrorProcessing(ref item, "line 265~300; " + ex.Message, true);
                    }

                    //if(!string.IsNullOrEmpty(msg))
                    //{
                    //    this.SendEmail(msg);
                    //}
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        private const string DATE_FORMAT = "yyyy-MM-dd_HH-mm-ss";
        private const int STATUS_1ST_NOTICE_POSTED = 255;
        private const int STATUS_2ND_NOTICE_CREATED = 260;

        //private bool PrepareSecondNotices(int notIntNo)
        // Oscar 20120424, fixed the error message bug
        private int PrepareSecondNotices(int notIntNo, out string msg)
        {
            //bool failed = false;
            string errorMsg = string.Empty;
            msg = string.Empty;

            TMSData create = new TMSData(connectStr);
            //Logger.Info(ResourceHelper.GetResource("SecondNoticePrepare_ForAuth", strAutCode, AutIntNo));

            int success = create.CreateSecondNotice_WS(AutIntNo, notIntNo,
                STATUS_1ST_NOTICE_POSTED, STATUS_2ND_NOTICE_CREATED,
                AARTOBase.LastUser, ref batchFilename, ref errorMsg,
                this.daysOf2ndNotice, this.daysOf2ndPayment, DataWashingActived, DataWashingRecycleMonth);

            switch (success)
            {
                case -1:
                    //Logger.Error(ResourceHelper.GetResource("SecondNoticePrepare_Error", errorMsg, DateTime.Now));
                    msg = ResourceHelper.GetResource("SecondNoticePrepare_Error", errorMsg, DateTime.Now);
                    //failed = true;
                    //return failed;
                    break;
                case -2:
                    //Logger.Error(ResourceHelper.GetResource("SecondNoticePrepare_TransactionError"));
                    msg = ResourceHelper.GetResource("SecondNoticePrepare_TransactionError");
                    //failed = true;
                    //return failed;
                    break;
                case -3://-- Heidi 2013-05-31 add EvidencePack for 4973
                    //Logger.Error(ResourceHelper.GetResource("SecondNoticePrepare_TransactionError"));
                    msg = ResourceHelper.GetResource("SecondNoticePrepare_EvidencePackError");
                    //failed = true;
                    //return failed;
                    break;
                case 0:
                    //Logger.Error(ResourceHelper.GetResource("SecondNoticePrepare_NoNoticeCreated", notIntNo));
                    msg = ResourceHelper.GetResource("SecondNoticePrepare_NoNoticeCreated", notIntNo);
                    //failed = true;
                    break;
                default:
                    Logger.Info(ResourceHelper.GetResource("SecondNoticePrepare_NoticeCreated", notIntNo));
                    break;
            }
            return success;
        }

        private string GetNewPrintFileName(string prefix, string dateFormat)
        {
            this.stamp = DateTime.Now;
            if (this.stamp.Subtract(this.lastStamp).Seconds <= 0)
                this.stamp = this.lastStamp.AddSeconds(1);
            this.lastStamp = this.stamp;
            return string.Format("{0}-{1}_{2}", prefix, this.stamp.ToString(dateFormat), (++this.sequence > 999 ? 1 : this.sequence).ToString("d3"));
        }

        // 2014-08-14 Jerry fixed action date
        //DateTime printActionDate = DateTime.Now;
        int delayHours = 2;
        private void SetServiceParameters()
        {
            //int delayHours = 0;
            if (ServiceParameters != null
                && ServiceParameters.ContainsKey("DelayActionDate_InHours")
                && !string.IsNullOrEmpty(ServiceParameters["DelayActionDate_InHours"]))
                delayHours = Convert.ToInt32(ServiceParameters["DelayActionDate_InHours"]);
            //this.printActionDate = DateTime.Now.AddHours(delayHours);
        }
    }
}

