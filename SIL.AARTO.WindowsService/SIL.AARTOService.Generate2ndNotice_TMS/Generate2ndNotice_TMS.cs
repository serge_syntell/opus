﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Generate2ndNotice_TMS
{
    partial class Generate2ndNotice_TMS : ServiceHost
    {
        public Generate2ndNotice_TMS()
            : base("", new Guid("91F60569-E5A6-446D-B55D-B986F6D8BC12"), new Generate2ndNotice_TMSService())
        {
            InitializeComponent();
        }
    }
}
