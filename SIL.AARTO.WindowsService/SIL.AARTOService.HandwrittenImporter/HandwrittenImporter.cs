﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.HandwrittenImporter
{
    partial class HandwrittenImporter : ServiceHost
    {
        public HandwrittenImporter()
            : base("", new Guid("1CF3F272-B37D-4C5F-8DF9-ECBD88361C37"), new HandwrittenImporterService())
        {
            InitializeComponent();
        }
    }
}
