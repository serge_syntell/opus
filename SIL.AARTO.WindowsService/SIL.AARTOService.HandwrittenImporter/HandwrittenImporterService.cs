﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using SIL.AARTOService.Resource;
using System.Data;
using SIL.DMS.DAL.Services;
using SIL.DMS.DAL.Entities;
using System.Xml;
using System.Collections;
using Stalberg.TMS.Data;
using System.Transactions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Model;
using System.Data.SqlClient;
using SIL.AARTO.DAL.Data;
using System.Text.RegularExpressions;
using SIL.ServiceQueueLibrary.DAL.Services;
using SIL.ServiceBase;
using System.Configuration;

namespace SIL.AARTOService.HandwrittenImporter
{
    public class HandwrittenImporterService : ServiceDataProcessViaQueue
    {
        public HandwrittenImporterService()
            : base("", "", new Guid("053D308A-78C1-4175-8FA2-7C4BAAFA7CB4"), ServiceQueueTypeList.HandwrittenImporter)
        {
            //Jerry 2014-03-20 set this service don't need base transaction
            this._serviceHelper = new AARTOServiceBase(this, false);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.connDMSDB = AARTOBase.GetConnectionString(ServiceConnectionNameList.DMS, ServiceConnectionTypeList.DB);
            SIL.ServiceBase.ServiceUtility.InitializeNetTier(this.connDMSDB, "SIL.DMS");

            importHandwrittenOffences = new ImportHandwrittenOffences(this.connAARTODB);
            AARTOBase.OnServiceStarting = () =>
            {
                if (this.ServiceParameters != null && this.ServiceParameters.ContainsKey("ContractorCode"))
                    this.conCode = this.ServiceParameters["ContractorCode"];
                if (String.IsNullOrEmpty(this.conCode))
                {
                    //AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ParemeterNotExists"), "ContractorCode"), true);
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ParemeterNotExists"), "ContractorCode"),
                        LogType.Error, ServiceBase.ServiceOption.BreakAndStop);
                }
            };
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        ImportHandwrittenOffences importHandwrittenOffences = null;
        NoAOGDB noAogDB;
        AARTORules rules = AARTORules.GetSingleTon();
        string connAARTODB;
        string connDMSDB;
        string clientID = string.Empty;
        string documentID = string.Empty;
        string batchID = string.Empty;
        string conCode = string.Empty;
        readonly Regex REG_CRT_ROOM_NO = new Regex("^[0-9,a-z,A-Z]{0,5}$");    // 2013-07-04 add by Henry for fix bug CrtRoomNo error
        bool IsImportSuccessful = true;// 2014-02-12 add by Henry for Fix a problem when importing data process if there is a problem, queue disappeared.
        QueueItem tempQueueItem;

        //Jerry 2014-04-15 change BatchDocumentService and BatchService to a global variable
        BatchDocumentService documentService = new BatchDocumentService();
        BatchService batchService = new BatchService();
        //Heidi 2014-04-29 added to push queue WithdrawSec56WithOfficerError(5239)
        SIL.AARTO.DAL.Services.NoticeService _noticeService = new NoticeService();
        ServiceQueueService _serviceQueueService = new ServiceQueueService();
        NoticeSummonsService _noticeSummonsService = new NoticeSummonsService();
        SummonsService _summonsService = new SummonsService();
        SIL.ServiceQueueLibrary.DAL.Data.ServiceQueueQuery _serviceQueueQuery =
        new SIL.ServiceQueueLibrary.DAL.Data.ServiceQueueQuery();
        AuthorityRuleInfo _authorityRuleInfo = new AuthorityRuleInfo();
        DateRuleInfo _dateRuleInfo = new DateRuleInfo();
        CourtService courtService = new CourtService();
        AartoNoticeDocumentService noticeDocumentService = new AartoNoticeDocumentService();
        AartoDocumentImageService aartoDocumentImageService = new AartoDocumentImageService();

        public override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString().Trim();
            if (item.Group != null)
                clientID = item.Group.Trim();

            if (!string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(clientID))
            {
                item.IsSuccessful = true;
                item.Status = QueueItemStatus.Discard;

                //Jerry 2014-04-14 check document status
                BatchDocument batchDocument = documentService.GetByBaDoId(body);
                if (batchDocument == null)
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrDocNotExist"), body), LogType.Info, ServiceBase.ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                    return;
                }

                if (batchDocument.BaDoStId == (int)BatchDocumentStatusList.QAFinished || batchDocument.BaDoStId == (int)BatchDocumentStatusList.DifferenceFixedInQA)
                {
                    batchID = batchDocument.BatchId;
                    return;
                }

                //Jerry 2014-10-17 if the document has been cancelled then discard that queue
                if (batchDocument.BaDoStId == (int)BatchDocumentStatusList.ImportSucceeded || batchDocument.BaDoStId == (int)BatchDocumentStatusList.ImportFailed ||
                    batchDocument.BaDoStId == (int)BatchDocumentStatusList.Cancelled)
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrCheckDocStatus"), body, batchDocument.BaDoStId), LogType.Info, ServiceBase.ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                else
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrCheckDocStatus"), body, batchDocument.BaDoStId), LogType.Error, ServiceBase.ServiceOption.ContinueButQueueFail, item, QueueItemStatus.UnKnown);
            }
            else
            {
                if (string.IsNullOrEmpty(body))
                    AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNull"), AARTOBase.LastUser));
                if (string.IsNullOrEmpty(clientID))
                    AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("InvalidClient"), AARTOBase.LastUser));

                AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                return;
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                tempQueueItem = queueList[i];
                if (!item.IsSuccessful) continue;
                IsImportSuccessful = true;
                try
                {
                    documentID = item.Body.ToString().Trim();
                    clientID = item.Group.ToString().Trim();

                    List<ImportDataEntity> list = null;
                    list = GetBatchDocumentListForImport(documentID, clientID);
                    if (list.Count == 1)
                    {
                        foreach (ImportDataEntity idEntity in list)
                        {
                            if (idEntity.DataExportMode.Equals((int)DataExportModeList.Local) || idEntity.DataExportMode.Equals((int)DataExportModeList.Satellite))
                            {
                                if (idEntity.DoTeTypeID.ToString() == "TMS_Section56")
                                {
                                    IsImportSuccessful = DoImportSection56(idEntity);
                                }
                                else if (idEntity.DoTeTypeID.ToString().Trim() == "TMS_Section341")
                                {
                                    IsImportSuccessful = DoImportSection341(idEntity);
                                }
                                else if (idEntity.DoTeTypeID.ToString().Trim() == "TMS_ProvincialSection56")
                                {
                                    IsImportSuccessful = DoImportProvincialSection56(idEntity);
                                }
                                else if (idEntity.DoTeTypeID.ToString().Trim() == "Section56_Cancelled")//2012 0914 Adam: We have used the same template type:"Section56_Cancelled" for Section56_Cancelled and Section56_Provincial_Cancelled documents.
                                {
                                    IsImportSuccessful = DoImportCancelledDocument(idEntity, "H56", "HandwrittenS56Capture");
                                }
                                else if (idEntity.DoTeTypeID.ToString().Trim() == "Section341_Cancelled")
                                {
                                    IsImportSuccessful = DoImportCancelledDocument(idEntity, "H341", "HandwrittenS341Capture");
                                }
                                else if (idEntity.DoTeTypeID.ToString().Trim() == "ChangeOfOffender" || idEntity.DoTeTypeID.ToString().Trim() == "PresentationOfDocument" || idEntity.DoTeTypeID.ToString().Trim() == "Representation")
                                {
                                    IsImportSuccessful = DoImportExternalDocument(idEntity);
                                }
                            }
                        }

                        //Jerry 2014-03-20 comment it out
                        // 2014-02-12 add by Henry for Fix a problem when importing data process if there is a problem, queue disappeared.
                        //if (IsImportSuccessful == false)
                        //{
                        //    item.Status = QueueItemStatus.UnKnown;
                        //}
                    }
                    else
                    {
                        AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("DocumentCountErr"), documentID));
                    }
                }
                catch (Exception ex)
                {
                    //Jerry 2014-03-16 change it
                    //AARTOBase.ErrorProcessing(ref item, ex);
                    AARTOBase.LogProcessing(ex, LogType.Error, ServiceBase.ServiceOption.ContinueButQueueFail, item, QueueItemStatus.UnKnown);
                }
            }
        }


        public bool DoImportSection56(ImportDataEntity importDataEntity)
        {
            string errMessage = string.Empty;
            string notTicketNo = string.Empty;
            string docID = string.Empty;
            //Jerry 2014-04-16 comment out batchID, get it from global variable
            //string batchID = string.Empty; 
            bool dateValidated = true;

            string IDType = "";

            try
            {
                #region loadeXML
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(importDataEntity.BaDoResult);
                XmlElement rootElement = xmlDoc.DocumentElement;
                XmlNodeList dataList = rootElement.SelectNodes(@"//Data");

                Hashtable hsXML = new Hashtable();
                Hashtable hsXMLText = new Hashtable();
                Hashtable hsAlternateCharge = new Hashtable();

                string officerError = string.Empty;
                string filmNo = string.Empty;
                string frameNo = string.Empty;
                bool isOfficerError = false;
                if (rootElement.HasAttribute("OfficerErrorIds"))
                {
                    officerError = rootElement.Attributes["OfficerErrorIds"].Value;
                    isOfficerError = !String.IsNullOrEmpty(officerError);
                }
                if (rootElement.HasAttribute("BatchId"))
                {
                    filmNo = rootElement.Attributes["BatchId"].Value;
                    //Jerry 2014-04-16 comment out batchID, get it from global variable
                    //batchID = filmNo;
                }
                if (rootElement.HasAttribute("DocId"))
                {
                    frameNo = rootElement.Attributes["DocId"].Value;
                }

                foreach (XmlNode xn in dataList)
                {
                    if (!hsXML.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["Value"] != null)
                    {
                        hsXML.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["Value"].Value.Trim());
                    }

                    if (!hsXMLText.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["Text"] != null)
                    {
                        hsXMLText.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["Text"].Value.Trim());
                    }
                    //Jake added 20101108 , accept Alternate charge code
                    if (!hsAlternateCharge.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["AlternateOffenceCode"] != null)
                    {
                        hsAlternateCharge.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["AlternateOffenceCode"].Value.Trim());
                    }

                    //Jake 2014-03-26 modified, continue to have this section of code in order to compatible with previous version
                    //Edge added 20131203
                    if (xn.Attributes["FieldName"].Value.Trim() == "IDNumber" && xn.Attributes["Other"] != null)
                    {
                        if (xn.Attributes["Other"].Value.Trim().IndexOf(',') >= 0)
                        {
                            string other = xn.Attributes["Other"].Value.Trim().Split(',')[1];
                            if (other == "0")
                            {
                                IDType = "02";
                            }
                            else if (other == "1" || other == "2")
                            {
                                IDType = "00";
                            }
                            //else if (other == "2")
                            //{
                            //    IDType = "03";
                            //}
                            else if (other == "3")
                            {
                                IDType = "99";
                            }
                        }
                    }
                    // New version of DMS about Id Type has been changed , so we need to handle this new veirsion
                    if (xn.Attributes["FieldName"].Value.Trim() == "IDNumber")
                    {
                        if (String.IsNullOrEmpty(IDType))
                            IDType = xn.Attributes["Other"].Value.Trim();
                    }
                }
                #endregion

                SIL.DMS.DAL.Entities.TList<BatchDocumentImage> batchDocumentImageList = new BatchDocumentImageService().GetByBaDoId(importDataEntity.BaDoID);
                docID = importDataEntity.BaDoID;
                //Jerry 2014-04-16 comment out batchID, get it from global variable
                //batchID = batchID;

                #region Notice
                int autIntNo = 0;
                if (!String.IsNullOrEmpty(importDataEntity.ClientID))
                {
                    autIntNo = importHandwrittenOffences.GetAuthIntNoByAutCode(importDataEntity.ClientID);
                }

                // IS 02 Jun 2010
                bool fieldFound = true;
                // IS 02 Jun 2010
                notTicketNo = ConvertNoticeTicketNo(getHashTableValue(hsXML, "NoticeNumber", ref fieldFound));

                int speedLimit = 0;
                int.TryParse(getHashTableValue(hsXML, "SpeedLimit", ref fieldFound), out speedLimit);

                int speedReading = 0;
                int.TryParse(getHashTableValue(hsXML, "SpeedReading", ref fieldFound), out speedReading);

                DateTime tempDate;
                DateTime offenceDate;
                DateTime compareStartDate = DateTime.Parse("1900-01-01");
                DateTime compareEndDate = DateTime.Parse("2070-01-01");
                string offenceDateStr = string.Empty, offenceTimeStr = string.Empty;

                offenceDateStr = getHashTableValue(hsXML, "OffenceDate", ref fieldFound);
                offenceTimeStr = getHashTableValue(hsXML, "OffenceTime", ref fieldFound);
                if (DateTime.TryParse(offenceDateStr + " " + offenceTimeStr, out offenceDate))
                {
                    if (offenceDate < compareStartDate || offenceDate > compareEndDate)
                    {
                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, offenceDateStr, offenceTimeStr));
                        WriteErrorMessageIntoDMS("OffenceDate", string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, offenceDateStr, offenceTimeStr));
                        return false;
                    }
                }
                else
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, offenceDateStr, offenceTimeStr));
                    WriteErrorMessageIntoDMS("OffenceDate", string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, offenceDateStr, offenceTimeStr));
                    return false;
                }

                if (offenceDate.Equals(DateTime.MinValue))
                    offenceDate = DateTime.Parse("1900-01-01");

                // Seawen 2012-07-15 added 
                //Get offence location description from DMS for street coding
                string notLocDescr = string.Empty;
                if (hsXML.ContainsKey("LocationDescr"))
                {
                    notLocDescr = getHashTableValue(hsXML, "LocationDescr", ref fieldFound);
                }
                else
                {
                    notLocDescr = getHashTableValue(hsXML, "OffencePlaceDescr", ref fieldFound);
                }

                string notRegNo = getHashTableValue(hsXML, "VehicleRegNo", ref fieldFound);
                notRegNo = notRegNo == "XX000000" ? string.Empty : notRegNo;    // 2013-04-28 add by Henry for by-law
                //Jerry 2013-07-09 add for Swartland
                string regNo2 = getHashTableValue(hsXML, "VehicleRegNo2", ref fieldFound, false);

                string notVehicleMakeCode = getHashTableValue(hsXML, "VehicleMake", ref fieldFound);
                string notVehicleMake = getHashTableValue(hsXMLText, "VehicleMake", ref fieldFound);
                string notVehicleTypeCode = getHashTableValue(hsXML, "VehicleType", ref fieldFound);
                string notVehicleType = getHashTableValue(hsXMLText, "VehicleType", ref fieldFound);
                string notOfficerNo = getHashTableValue(hsXML, "OfficerCode", ref fieldFound);
                string notOfficerSname = getHashTableValue(hsXML, "OfficerName", ref fieldFound);
                string notOfficerGroup = getHashTableValue(hsXML, "OfficerGroupCode", ref fieldFound);
                string notDmsCaptureClerk = getHashTableValue(hsXML, "NotDMSCaptureClerk", ref fieldFound);
                DateTime? notDmsCaptureDate = null;
                string captureDate = getHashTableValue(hsXML, "NotDMSCaptureDate", ref fieldFound);
                if (!String.IsNullOrEmpty(captureDate))
                {
                    if (DateTime.TryParse(captureDate, out tempDate))
                    {
                        notDmsCaptureDate = tempDate;
                        if (tempDate < compareStartDate || tempDate > compareEndDate)
                        {
                            AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("CaptureDateInvalid"), documentID, batchID, captureDate));
                            WriteErrorMessageIntoDMS("NotDMSCaptureDate", string.Format(ResourceHelper.GetResource("CaptureDateInvalid"), documentID, batchID, captureDate));
                            return false;
                        }
                    }
                    else
                    {
                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("CaptureDateInvalid"), documentID, batchID, captureDate));
                        WriteErrorMessageIntoDMS("NotDMSCaptureDate", string.Format(ResourceHelper.GetResource("CaptureDateInvalid"), documentID, batchID, captureDate));
                        return false;
                    }
                    //notDmsCaptureDate = DateTime.Parse(captureDate);
                }
                string scanDate = getHashTableValue(hsXML, "ScanDateTime", ref fieldFound, false);
                #endregion

                #region Charge
                string chgOffenceCode1 = getHashTableValue(hsXML, "Charge1OffenceCode", ref fieldFound);
                //Jake added 20101108 chgOffenceCode ="00~N"
                string chgNoAog1 = string.Empty;
                if (!String.IsNullOrEmpty(chgOffenceCode1))
                {
                    if (chgOffenceCode1.Contains("~"))
                    {
                        chgNoAog1 = chgOffenceCode1.Split('~')[1];
                        chgOffenceCode1 = chgOffenceCode1.Split('~')[0];
                    }
                    else
                    {
                        chgNoAog1 = "N";
                    }
                }
                string chgOffenceDescr1 = getHashTableValue(hsXMLText, "Charge1OffenceCode", ref fieldFound);

                // IF ChargeOffenceCode2 or ChargeOffenceCode3 does not exist, contine to import
                bool fieldNotFound = false;
                string chgOffenceCode2 = getHashTableValue(hsXML, "Charge2OffenceCode", ref fieldNotFound);

                //Jake added 20101108 chgOffenceCode ="00~N"
                string chgNoAog2 = string.Empty;
                if (!String.IsNullOrEmpty(chgOffenceCode2))
                {
                    if (chgOffenceCode2.Contains("~"))
                    {
                        chgNoAog2 = chgOffenceCode2.Split('~')[1];
                        chgOffenceCode2 = chgOffenceCode2.Split('~')[0];
                    }
                    else
                    {
                        chgNoAog2 = "N";
                    }
                }
                string chgOffenceCode3 = getHashTableValue(hsXML, "Charge3OffenceCode", ref fieldNotFound);
                //Jake added 20101108 chgOffenceCode ="00~N"
                string chgNoAog3 = string.Empty;
                if (!String.IsNullOrEmpty(chgOffenceCode3))
                {
                    if (chgOffenceCode3.Contains("~"))
                    {
                        chgNoAog3 = chgOffenceCode3.Split('~')[1];
                        chgOffenceCode3 = chgOffenceCode3.Split('~')[0];
                    }
                    else
                    {
                        chgNoAog3 = "N";
                    }
                }

                string chgOffenceDescr2 = getHashTableValue(hsXMLText, "Charge2OffenceCode", ref fieldNotFound);
                string chgOffenceDescr3 = getHashTableValue(hsXMLText, "Charge3OffenceCode", ref fieldNotFound);

                //Jake added 20101108 ,add alternate charge code 
                string chgAlternateOffenceCode1 = String.Empty;
                string chgAlternateOffenceCode2 = String.Empty;
                string chgAlternateOffenceCode3 = String.Empty;
                if (hsAlternateCharge.ContainsKey("Charge1OffenceCode"))
                {
                    chgAlternateOffenceCode1 = hsAlternateCharge["Charge1OffenceCode"].ToString();
                }
                if (hsAlternateCharge.ContainsKey("Charge2OffenceCode"))
                {
                    chgAlternateOffenceCode2 = hsAlternateCharge["Charge2OffenceCode"].ToString();
                }
                if (hsAlternateCharge.ContainsKey("Charge3OffenceCode"))
                {
                    chgAlternateOffenceCode3 = hsAlternateCharge["Charge3OffenceCode"].ToString();
                }

                float chgFineAmount1 = 0;
                float.TryParse(getHashTableValue(hsXML, "Charge1Amt", ref fieldFound), out chgFineAmount1);

                float chgFineAmount2 = 0;
                float.TryParse(getHashTableValue(hsXML, "Charge2Amt", ref fieldNotFound), out chgFineAmount2);

                float chgFineAmount3 = 0;
                float.TryParse(getHashTableValue(hsXML, "Charge3Amt", ref fieldNotFound), out chgFineAmount3);

                DateTime? notPaymentDate = null;
                string tempPaidDate = getHashTableValue(hsXML, "PayDate", ref fieldFound);
                if (!String.IsNullOrEmpty(tempPaidDate))
                {
                    if (DateTime.TryParse(tempPaidDate, out tempDate))
                    {
                        if (tempDate < compareStartDate || tempDate > compareEndDate)
                        {
                            AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("PayDateInvalid"), documentID, batchID, tempPaidDate));
                            WriteErrorMessageIntoDMS("PayDate", string.Format(ResourceHelper.GetResource("PayDateInvalid"), documentID, batchID, tempPaidDate));
                            return false;
                        }
                    }
                    else
                    {
                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("PayDateInvalid"), documentID, batchID, tempPaidDate));
                        WriteErrorMessageIntoDMS("PayDate", string.Format(ResourceHelper.GetResource("PayDateInvalid"), documentID, batchID, tempPaidDate));
                        return false;
                    }
                    if (tempDate.Equals(DateTime.MinValue))
                    {
                        tempDate = DateTime.Parse("1900-01-01");
                    }

                    notPaymentDate = tempDate;
                }

                string chgOfficerNatisNo = getHashTableValue(hsXML, "OfficerCode", ref fieldFound);
                string chgOfficerName = getHashTableValue(hsXML, "OfficerName", ref fieldFound);
                #endregion

                #region Summons
                DateTime? courtDate = null;
                DateTime tempCourtDate;
                string tempCourtDateValue = getHashTableValue(hsXML, "TrialDate", ref fieldFound);
                if (!String.IsNullOrEmpty(tempCourtDateValue))
                {
                    if (DateTime.TryParse(tempCourtDateValue, out tempCourtDate))
                    {
                        if (tempCourtDate < compareStartDate || tempCourtDate > compareEndDate)
                        {
                            AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("CourtDateInvalid"), documentID, batchID, tempCourtDateValue));
                            WriteErrorMessageIntoDMS("TrialDate", string.Format(ResourceHelper.GetResource("CourtDateInvalid"), documentID, batchID, tempCourtDateValue));
                            return false;
                        }
                    }
                    else
                    {
                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("CourtDateInvalid"), documentID, batchID, tempCourtDateValue));
                        WriteErrorMessageIntoDMS("TrialDate", string.Format(ResourceHelper.GetResource("CourtDateInvalid"), documentID, batchID, tempCourtDateValue));
                        return false;
                    }
                    if (tempCourtDate.Equals(DateTime.MinValue))
                    {
                        tempCourtDate = DateTime.Parse("1900-01-01");
                    }

                    courtDate = tempCourtDate;
                }

                string sumOfficerNo = getHashTableValue(hsXML, "OfficerCode", ref fieldFound);
                string sumCaseNo = getHashTableValue(hsXML, "CaseNo", ref fieldFound);
                string crtNo = getHashTableValue(hsXML, "CrtNo", ref fieldFound);
                string crtRoomNo = getHashTableValue(hsXML, "CrtRoomNo", ref fieldFound);
                bool isCrtRoomNo = REG_CRT_ROOM_NO.IsMatch(crtRoomNo); // 2013-07-04 add by Henry for fix bug CrtRoomNo error

                bool existOfficerNo = GetTrafficOfficer(sumOfficerNo);
                if (!existOfficerNo)
                {
                    string errorMessage = string.Format(ResourceHelper.GetResource("OfficerNumNotExists"), sumOfficerNo);
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrProcessS56"), batchID, docID, notTicketNo, errorMessage),
                        LogType.Error, ServiceBase.ServiceOption.Continue);
                    WriteErrorMessageIntoDMS("OfficerCode", string.Format(ResourceHelper.GetResource("ErrProcessS56"), batchID, docID, notTicketNo, errorMessage),
                        BatchDocumentStatusList.DocumentReturnedToQC);
                    return false;
                }

                #endregion

                #region Accused
                string accSurname = getHashTableValue(hsXML, "Surname", ref fieldFound);
                string accForenames = getHashTableValue(hsXML, "Name", ref fieldFound);
                string accIdNumber = getHashTableValue(hsXML, "IDNumber", ref fieldFound);
                string accSex = getHashTableValue(hsXML, "Sex", ref fieldFound);
                string accOccupation = getHashTableValue(hsXML, "Occupation", ref fieldFound);
                string accAge = getHashTableValue(hsXML, "Age", ref fieldFound);
                string accStAdd1 = getHashTableValue(hsXML, "Address1", ref fieldFound);
                string accStAdd2 = getHashTableValue(hsXML, "Address2", ref fieldFound);
                //string accStAdd3 = getHashTableValue(hsXML, "Address3", ref fieldFound);
                string accStCode = getHashTableValue(hsXML, "PostalCode1", ref fieldFound);
                string accTelHome = getHashTableValue(hsXML, "TelNo1", ref fieldFound);
                string accPoAdd1 = getHashTableValue(hsXML, "BusinessAddress", ref fieldFound);
                string accPoCode = getHashTableValue(hsXML, "PostalCode2", ref fieldFound);
                string accTelWork = getHashTableValue(hsXML, "TelNo2", ref fieldFound);
                #endregion

                #region 2010-09-26 New Fields
                string nationality = getHashTableValue(hsXML, "Nationality", ref fieldFound);
                string driverLicence = getHashTableValue(hsXML, "Licence", ref fieldFound);
                string suburb = getHashTableValue(hsXML, "Suburb", ref fieldFound);
                string townOrCity = getHashTableValue(hsXML, "TownOrCity", ref fieldFound);
                string accPoAdd2 = getHashTableValue(hsXML, "BusinessAddress2", ref fieldFound);
                string businessSuburb = getHashTableValue(hsXML, "BusinessSuburb", ref fieldFound);
                string businessTownOrCity = getHashTableValue(hsXML, "BusinessTownOrCity", ref fieldFound);

                bool status = false;
                try
                {
                    // Jake 2013-05-27 modified
                    int age = 0;
                    Int32.TryParse(accAge, out age);
                    status = age > 18; //2014-08-06 Heidi fixed for AccOver18(5303)
                    //status = Convert.ToInt32(accAge) > 0;
                }
                catch { status = false; }
                string agNo = getHashTableValue(hsXML, "AGNo", ref fieldFound);
                #endregion

                #region 2013-05-17 New Fields
                // 2013-05-17 added by Heidi for locationSuburb 
                int loSuIntNo = 0;
                bool fieldLocationSuburbFound = true;
                string locationSuburbDesc = getHashTableValue(hsXML, "LocationSuburb", ref fieldLocationSuburbFound);
                if (fieldLocationSuburbFound == false)
                {
                    loSuIntNo = GetLoSuIntNoByAutIntNo(autIntNo);
                }
                else
                {
                    if (!String.IsNullOrEmpty(locationSuburbDesc) && locationSuburbDesc.Trim().Length > 0)
                    {
                        if (locationSuburbDesc.Trim().Contains("~"))
                        {
                            int.TryParse(locationSuburbDesc.Trim().Split('~')[0], out loSuIntNo);
                        }
                        else
                        {
                            // Jake 2013-12-03 add get locationsuburb by descr if losuIntNo does not stored in xml
                            int.TryParse(locationSuburbDesc.Trim(), out loSuIntNo);
                            if (loSuIntNo == 0)
                            {
                                loSuIntNo = GetLocationSuburbByDescr(locationSuburbDesc, autIntNo);
                            }
                        }
                    }
                }

                #endregion

                //Jerry 2014-07-31 add for EMPD
                string accTelCell = getHashTableValue(hsXML, "TelNo3", ref fieldFound, false);
                string policeStation = getHashTableValue(hsXML, "StationName", ref fieldFound, false);
                string district = getHashTableValue(hsXML, "District", ref fieldFound, false);
                string licenceNumber = getHashTableValue(hsXML, "LicenceNumber", ref fieldFound, false);
                string licenceTypeName = getHashTableValue(hsXML, "LicenceTypeName", ref fieldFound, false);

                #region ImageFileServer
                if (!fieldFound) { return false; }

                #region  2013-05-17 added by Heidi for locationSuburb
                if (loSuIntNo == 0)
                {
                    //EntLibLogger.WriteLog(LogCategory.General, lastUser, "Not Found Valid LoSuIntNo!");
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoLoSuIntNo"), batchID, docID, notTicketNo), LogType.Error, ServiceBase.ServiceOption.ContinueButQueueFail);
                    WriteErrorMessageIntoDMS("LocationSuburb", string.Format(ResourceHelper.GetResource("NoLoSuIntNo"), batchID, docID, notTicketNo));
                    return false;
                }
                #endregion

                int ifsIntNo = importDataEntity.IFSIntNo;
                SIL.DMS.DAL.Entities.ImageFileServer imageFileServer_DMS = new SIL.DMS.DAL.Services.ImageFileServerService().GetByIfsIntNo(ifsIntNo);
                if (imageFileServer_DMS != null)
                {
                    ifsIntNo = importHandwrittenOffences.CheckImageFileServer("DMSScanImage", imageFileServer_DMS.IfServerName, imageFileServer_DMS.ImageMachineName, imageFileServer_DMS.ImageDrive, imageFileServer_DMS.ImageRootFolder, false, imageFileServer_DMS.ImageShareName, AARTOBase.LastUser);
                }
                else
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ImageFileServerErr"), documentID, batchID, importDataEntity.DoTeTypeID, ifsIntNo));
                    WriteErrorMessageIntoDMS("DMSScanImage", string.Format(ResourceHelper.GetResource("ImageFileServerErr"), documentID, batchID, importDataEntity.DoTeTypeID, ifsIntNo));
                    return false;
                }
                #endregion

                #region OffenceCode
                if (!String.IsNullOrEmpty(chgOffenceCode1)
                    && !String.IsNullOrEmpty(chgOffenceCode2)
                    && chgOffenceCode1 == chgOffenceCode2)
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceCode1EqualOffenceCode2"), documentID, batchID, chgOffenceCode1, chgOffenceCode2));
                    WriteErrorMessageIntoDMS("OffenceCode1", string.Format(ResourceHelper.GetResource("OffenceCode1EqualOffenceCode2"), documentID, batchID, chgOffenceCode1, chgOffenceCode2));
                    return false;
                }
                if (!String.IsNullOrEmpty(chgOffenceCode1)
                   && !String.IsNullOrEmpty(chgOffenceCode3)
                   && chgOffenceCode1 == chgOffenceCode3)
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceCode1EqualOffenceCode3"), documentID, batchID, chgOffenceCode1, chgOffenceCode3));
                    WriteErrorMessageIntoDMS("OffenceCode1", string.Format(ResourceHelper.GetResource("OffenceCode1EqualOffenceCode3"), documentID, batchID, chgOffenceCode1, chgOffenceCode3));
                    return false;
                }
                if (!String.IsNullOrEmpty(chgOffenceCode2)
                   && !String.IsNullOrEmpty(chgOffenceCode3)
                   && chgOffenceCode2 == chgOffenceCode3)
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceCode2EqualOffenceCode3"), documentID, batchID, chgOffenceCode2, chgOffenceCode3));
                    WriteErrorMessageIntoDMS("OffenceCode2", string.Format(ResourceHelper.GetResource("OffenceCode2EqualOffenceCode3"), documentID, batchID, chgOffenceCode2, chgOffenceCode3));
                    return false;
                }
                if (!String.IsNullOrEmpty(chgOffenceCode1)
                    && !String.IsNullOrEmpty(chgOffenceCode2)
                    && !String.IsNullOrEmpty(chgOffenceCode3)
                    && chgOffenceCode2 == chgOffenceCode3
                    && chgOffenceCode1 == chgOffenceCode3)
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceCodeErr"), documentID, batchID, chgOffenceCode1, chgOffenceCode2, chgOffenceCode3));
                    WriteErrorMessageIntoDMS("OffenceCode1", string.Format(ResourceHelper.GetResource("OffenceCodeErr"), documentID, batchID, chgOffenceCode1, chgOffenceCode2, chgOffenceCode3));
                    return false;
                }
                #endregion

                int notIntNo = 0;
                if (batchDocumentImageList != null && batchDocumentImageList.Count > 0)
                {
                    string batchName = string.Empty;
                    //Jerry 2014-04-14 change BatchService to a global variable
                    Batch batch = batchService.GetByBatchId(filmNo);
                    if (batch != null)
                    {
                        batchName = batch.BatchDescription;
                    }

                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromSeconds(300)))
                    {
                        //Jerry 2014-04-14 change BatchDocumentService to a global variable
                        //BatchDocumentService documentService = new BatchDocumentService();
                        BatchDocument batchDocument = documentService.GetByBaDoId(importDataEntity.BaDoID);
                        if (batchDocument != null)
                        {
                            if (!isCrtRoomNo)   // 2013-07-04 add by Henry for fix bug CrtRoomNo error
                            {
                                //EntLibLogger.WriteLog(LogCategory.Error, lastUser, string.Format("An error has occurred while processing a Section 56!\nBatch: {0}, Document: {1}, Notice: {2} - \nError: {3}\n", batchID, docID, notTicketNo, " Invalid court room number \"" + crtRoomNo + "\""));
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrProcessS56"), batchID, docID, notTicketNo, " Invalid court room number \"" + crtRoomNo + "\""), LogType.Error, ServiceBase.ServiceOption.Continue);
                                WriteErrorMessageIntoDMS("CrtRoomNo", string.Format(ResourceHelper.GetResource("ErrProcessS56"), batchID, docID, notTicketNo, " Invalid court room number \"" + crtRoomNo + "\""),
                                    BatchDocumentStatusList.DocumentReturnedToQC);
                                return false;
                            }

                            #region Speed limit
                            if (speedLimit > 150 || speedReading > 255 || dateValidated == false)
                            {
                                if (speedLimit > 150 || speedReading > 255)
                                {
                                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("SpeedLimitReadErr"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, speedLimit.ToString(), speedReading.ToString()));
                                    WriteErrorMessageIntoDMS("SpeedLimit", string.Format(ResourceHelper.GetResource("SpeedLimitReadErr"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, speedLimit.ToString(), speedReading.ToString()));
                                }
                                batchDocument.BaDoStId = (int)BatchDocumentStatusList.ImportFailed;
                                //Jerry 2013-06-26 add last user
                                batchDocument.LastUser = AARTOBase.LastUser;
                                batchDocument = documentService.Save(batchDocument);
                                //scope.Complete();
                                return false;
                            }
                            else
                            {
                                batchDocument.BaDoStId = (int)BatchDocumentStatusList.ImportSucceeded;
                                //batchDocument = documentService.Save(batchDocument);
                            }
                            #endregion

                            #region original Summons
                            //Heidi 2014-04-29 added to push queue WithdrawSec56WithOfficerError(5239)
                            DateTime originalCourtDate = DateTime.Parse("1900-01-01");
                            SIL.AARTO.DAL.Entities.Summons originalSummons = null;
                            SIL.AARTO.DAL.Entities.Notice originalNotice = _noticeService.GetByNotTicketNo(notTicketNo).FirstOrDefault();
                            if (originalNotice != null)
                            {
                                NoticeSummons originalNoticeSummons = _noticeSummonsService.GetByNotIntNo(originalNotice.NotIntNo).FirstOrDefault();
                                if (originalNoticeSummons != null)
                                {
                                    originalSummons = _summonsService.GetBySumIntNo(originalNoticeSummons.SumIntNo);
                                    if (originalSummons != null && originalSummons.SumCourtDate.HasValue)
                                    {
                                        originalCourtDate = Convert.ToDateTime(originalSummons.SumCourtDate);
                                    }
                                }
                            }
                            #endregion

                            #region ImportHandwrittenOffencesSection56
                            try
                            {
                                notIntNo = importHandwrittenOffences.ImportHandwrittenOffencesSection56(
                                    autIntNo, filmNo, frameNo, notTicketNo, speedLimit, speedReading,
                                    offenceDate, notLocDescr, notRegNo, notVehicleMakeCode, notVehicleMake,
                                    notVehicleTypeCode, notVehicleType, notOfficerNo, notOfficerSname,
                                    notOfficerGroup, notDmsCaptureClerk, notDmsCaptureDate, chgOffenceCode1, chgOffenceDescr1,
                                    chgOffenceCode2, chgOffenceDescr2, chgOffenceCode3,
                                    chgOffenceDescr3, chgFineAmount1, chgFineAmount2, chgFineAmount3,
                                    notPaymentDate, chgOfficerNatisNo, chgOfficerName, conCode, courtDate, sumOfficerNo, sumCaseNo,
                                    crtNo, crtRoomNo, ifsIntNo, accSurname, accForenames, IDType, accIdNumber,
                                    accOccupation, accAge, accSex, accPoAdd1, accPoCode, accTelHome, accStAdd1, accStAdd2, accStCode,
                                    accTelWork, isOfficerError, batchDocumentImageList[0].BaDoId,
                                    batchDocumentImageList[0].BaDoImOrder, batchDocumentImageList[0].BaDoImFilePath, AARTOBase.LastUser, officerError,
                                    nationality, driverLicence, accPoAdd2, agNo, businessSuburb, businessTownOrCity, suburb, townOrCity, status,
                                    chgAlternateOffenceCode1, chgAlternateOffenceCode2, chgAlternateOffenceCode3,
                                    chgNoAog1, chgNoAog2, chgNoAog3, "H56", loSuIntNo, scanDate, "HandwrittenS56Capture", regNo2, accTelCell, policeStation,
                                    district, licenceNumber, licenceTypeName);
                            }
                            catch (Exception ex)
                            {
                                AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, ex.Message));
                                WriteErrorMessageIntoDMS("Exception", string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, ex.Message));
                                return false;
                            }
                            #endregion

                            if (notIntNo <= 0)
                            {
                                using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Suppress))
                                {
                                    //Jake 2014-12-24 added return value -1000 for 5383 , court or amount does not match
                                    //Jerry 2014-03-25 change it
                                    //batchDocument.BaDoStId = (int)BatchDocumentStatusList.ImportFailed;
                                    batchDocument.BaDoStId = (notIntNo == -150 || notIntNo == -151) ? (int)BatchDocumentStatusList.ImportSucceeded : (int)BatchDocumentStatusList.ImportFailed;
                                    //Jerry 2013-06-26 add last user
                                    batchDocument.LastUser = AARTOBase.LastUser;
                                    batchDocument = documentService.Save(batchDocument);
                                    ts.Complete();
                                }
                                if (notIntNo == -150)
                                {
                                    errMessage = string.Format(ResourceHelper.GetResource("ImportS56"), batchID, docID, notTicketNo);
                                }
                                // Oscar 2013-03-12 added, we don't update which has case number.
                                else if (notIntNo == -151)
                                    errMessage = string.Format(ResourceHelper.GetResource("ImportS56Failed"), batchID, docID, notTicketNo);
                                else if (notIntNo == -1000)
                                {
                                    errMessage = string.Format(ResourceHelper.GetResource("NoticeFullyPaidAndCourtOrAmountNotMatch"), batchID, docID, notTicketNo);
                                    scope.Complete();
                                }
                                else
                                {
                                    errMessage = string.Format(ResourceHelper.GetResource("ImportS56Error"), batchID, docID, notTicketNo);
                                }
                            }
                            else
                            {
                                //Jerry 2013-06-26 add last user
                                batchDocument.LastUser = AARTOBase.LastUser;
                                batchDocument = documentService.Save(batchDocument);

                                //errMessage = string.Format("Import of S56 successful - Batch: {0}, Document: {1}, Notice: {2}  ", batchID, docID, notTicketNo);
                                errMessage = string.Format(ResourceHelper.GetResource("S56ImportSuccessful"), batchID, docID, notTicketNo);

                                #region push queue
                                QueueItemProcessor queProcessor = new QueueItemProcessor();
                                NoticeSummons noticeSummons = new NoticeSummonsService().GetByNotIntNo(notIntNo).FirstOrDefault();
                                if (noticeSummons != null)
                                {
                                    SIL.AARTO.DAL.Entities.Authority authority = new SIL.AARTO.DAL.Services.AuthorityService().GetByAutIntNo(autIntNo);
                                    int sumIntNo = noticeSummons.SumIntNo;
                                    DateRuleInfo dateRule;
                                    SIL.AARTO.DAL.Entities.Summons summonsEntity = new SummonsService().GetBySumIntNo(sumIntNo);
                                    if (summonsEntity.CrtRintNo.HasValue && summonsEntity.SumCourtDate.HasValue)
                                    {
                                        Court courtEntity = new CourtService().GetByCrtIntNo(summonsEntity.CrtIntNo);
                                        CourtRoom courtRoomEntity = new CourtRoomService().GetByCrtRintNo(summonsEntity.CrtRintNo.Value);
                                        dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "CDate", "FinalCourtRoll");
                                        int daysOfAllocateCaseNo = dateRule.ADRNoOfDays;

                                        string[] group = new string[4];
                                        group[0] = authority.AutCode.Trim();
                                        //group[1] = courtEntity.CrtNo.Trim();
                                        // Oscar 201203012 changed to use CrtName
                                        group[1] = courtEntity.CrtName.Trim();
                                        group[2] = courtRoomEntity.CrtRoomNo.Trim();
                                        group[3] = summonsEntity.SumCourtDate.Value.ToString("yyyy-MM-dd HH:mm:ss");

                                        // Oscar 2013-03-11 added for passed date checking.
                                        var actionDate = summonsEntity.SumCourtDate.Value.AddDays(daysOfAllocateCaseNo);
                                        if (DateTime.Now.Date <= actionDate.Date)
                                        {
                                            queProcessor.Send(
                                                new QueueItem()
                                                {
                                                    Body = sumIntNo,
                                                    Group = string.Join("|", group),
                                                    ActDate = actionDate,
                                                    LastUser = AARTOBase.LastUser, //Jerry 2013-06-26 add
                                                    QueueType = ServiceQueueTypeList.SummonsIsServed
                                                });
                                        }
                                        else
                                        {
                                            //EntLibLogger.WriteLog(LogCategory.Warning, lastUser, string.Format("Court date has already passed. No case number will be allocated for summons \"{0}\".", summonsEntity.SummonsNo));
                                            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CourtDatePassed"), summonsEntity.SummonsNo), LogType.Error, ServiceBase.ServiceOption.Continue);
                                        }
                                    }

                                    //Jake 2015-03-17 use new date rule 
                                    //jerry 2011-12-22 push CancelExpiredSummons
                                    //dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "SumIssueDate", "SumExpiredDate");
                                    /* Jake 2015-03-23 comment out ,no need to push this queue items for S56 summons
                                    dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "SumExpireDate");
                                    int noDaysForSummonsExpiry = dateRule.ADRNoOfDays;

                                    queProcessor.Send(
                                        new QueueItem()
                                        {
                                            Body = sumIntNo,
                                            Group = authority.AutCode.Trim(),
                                            ActDate = offenceDate.AddDays(noDaysForSummonsExpiry + 1),
                                            LastUser = AARTOBase.LastUser, //Jerry 2013-06-26 add
                                            QueueType = ServiceQueueTypeList.ExpireUnservedSummons
                                        }
                                    );
                                    */

                                    #region push WithdrawSec56WithOfficerError queue
                                    //Heidi 2014-04-29 added to push queue WithdrawSec56WithOfficerError(5239)
                                    if (summonsEntity != null && summonsEntity.SumCourtDate.HasValue)
                                    {
                                        creatWithdrawSec56WithOfficerErrorQueue(notIntNo, autIntNo, originalSummons, authority.AutCode, Convert.ToDateTime(summonsEntity.SumCourtDate), originalCourtDate, queProcessor);
                                    }
                                    #endregion
                                }
                                #endregion

                                scope.Complete();
                                //EntLibLogger.WriteLog(LogCategory.General, lastUser, errMessage);
                                AARTOBase.LogProcessing(errMessage, LogType.Info, ServiceBase.ServiceOption.Continue);
                                return true;
                            }
                        }
                    }
                }

                #region record err msg
                if (notIntNo == -1)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Notice"));
                    WriteErrorMessageIntoDMS("Notice", String.Format(errMessage + "creating Notice"));
                    return false;
                }
                else if (notIntNo == -2)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Charge"));
                    WriteErrorMessageIntoDMS("Charge", String.Format(errMessage + "creating Charge"));
                    return false;
                }
                else if (notIntNo == -3)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Summons"));
                    WriteErrorMessageIntoDMS("Summons", String.Format(errMessage + "creating Summons"));
                    return false;
                }
                else if (notIntNo == -4)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Summons_Charge"));
                    WriteErrorMessageIntoDMS("Summons_Charge", String.Format(errMessage + "creating Summons_Charge"));
                    return false;
                }
                else if (notIntNo == -5)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Accused"));
                    WriteErrorMessageIntoDMS("Accused", String.Format(errMessage + "creating Accused"));
                    return false;
                }
                else if (notIntNo == -6)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Film"));
                    WriteErrorMessageIntoDMS("Film", String.Format(errMessage + "creating Film"));
                    return false;
                }
                else if (notIntNo == -7)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Frame"));
                    WriteErrorMessageIntoDMS("Frame", String.Format(errMessage + "creating Frame"));
                    return false;
                }
                else if (notIntNo == -8)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Notice_Frame"));
                    WriteErrorMessageIntoDMS("Notice_Frame", String.Format(errMessage + "creating Notice_Frame"));
                    return false;
                }
                else if (notIntNo == -9)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating AartoDocumentImage"));
                    WriteErrorMessageIntoDMS("AartoDocumentImage", String.Format(errMessage + "creating AartoDocumentImage"));
                    return false;
                }
                else if (notIntNo == -10)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating SumCharge"));
                    WriteErrorMessageIntoDMS("SumCharge", String.Format(errMessage + "creating SumCharge"));
                    return false;
                }
                else if (notIntNo == -11)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Driver"));
                    WriteErrorMessageIntoDMS("Driver", String.Format(errMessage + "creating Driver"));
                    return false;
                }
                else if (notIntNo == -12)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating Notice"));
                    WriteErrorMessageIntoDMS("Notice", String.Format(errMessage + "updating Notice"));
                    return false;
                }
                else if (notIntNo == -13)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Evidence Pack"));
                    WriteErrorMessageIntoDMS("Evidence Pack", String.Format(errMessage + "creating Evidence Pack"));
                    return false;
                }
                else if (notIntNo == -14)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "Court Date S56 Allocation"));
                    WriteErrorMessageIntoDMS("S56 Allocation", String.Format(errMessage + "Court Date S56 Allocation"));
                    return false;
                }
                else if (notIntNo == -15)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating Film"));
                    WriteErrorMessageIntoDMS("Film", String.Format(errMessage + "updating Film "));
                    return false;
                }
                else if (notIntNo == -16)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating AARTONoticeDocument"));
                    WriteErrorMessageIntoDMS("AARTONoticeDocument", String.Format(errMessage + "creating AARTONoticeDocument"));
                    return false;
                }
                else if (notIntNo == -17)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating Frame"));
                    WriteErrorMessageIntoDMS("Frame", String.Format(errMessage + "updating Frame"));
                    return false;
                }
                else if (notIntNo == -18)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "getting offence code"));
                    WriteErrorMessageIntoDMS("Offence Code", String.Format(errMessage + "getting offence code"));
                    return false;
                }
                else if (notIntNo == -19)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating Charge"));
                    WriteErrorMessageIntoDMS("Charge", String.Format(errMessage + "updating Charge"));
                    return false;
                }
                else if (notIntNo == -20)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating Summons"));
                    WriteErrorMessageIntoDMS("Summons", String.Format(errMessage + "updating Summons"));
                    return false;
                }
                else if (notIntNo == -21)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating SumCharge"));
                    WriteErrorMessageIntoDMS("SumCharge", String.Format(errMessage + "updating SumCharge"));
                    return false;
                }
                else if (notIntNo == -22)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating Accused"));
                    WriteErrorMessageIntoDMS("Accused", String.Format(errMessage + "updating Accused"));
                    return false;
                }
                else if (notIntNo == -103 || notIntNo == -104)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating Search tables"));
                    WriteErrorMessageIntoDMS("SearchId", String.Format(errMessage + "updating Search tables"));
                    return false;
                }
                else if (notIntNo == -30)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "handling offier errors"));
                    WriteErrorMessageIntoDMS("Offier Error", String.Format(errMessage + "handling offier errors"));
                    return false;
                }
                //Added 2010-10-13  ,update status = ImportedFromDMS in AartobmDocument
                else if (notIntNo == -36)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating AaBmStatusIs in AartoBMDocument"));
                    WriteErrorMessageIntoDMS("AartoBMDocument", String.Format(errMessage + "updating AaBmStatusIs in AartoBMDocument"));
                    return false;
                }
                else if (notIntNo == -27)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating CourtDate"));
                    WriteErrorMessageIntoDMS("CourtDate", String.Format(errMessage + "creating CourtDate"));
                    return false;
                }
                else if (notIntNo == -101)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Charge_SumCharge"));
                    WriteErrorMessageIntoDMS("Charge_SumCharge", String.Format(errMessage + "creating Charge_SumCharge"));
                    return false;
                }
                else if (notIntNo == -102)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Notice_Summons"));
                    WriteErrorMessageIntoDMS("Notice_Summons", String.Format(errMessage + "creating Notice_Summons"));
                    return false;
                }
                else if (notIntNo == -110)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "officer error message:Incorrect CourtDate does not in AARTOOfficerError table"));
                    WriteErrorMessageIntoDMS("CourtDate", String.Format(errMessage + "officer error message:Incorrect CourtDate does not in AARTOOfficerError table"));
                    return false;
                }
                else if (notIntNo == -130)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating CourtRoom for CourtRoomNo:{0}", crtRoomNo));
                    WriteErrorMessageIntoDMS("CourtRoom", String.Format(errMessage + "creating CourtRoom for CourtRoomNo:{0}", crtRoomNo));
                    return false;
                }
                else if (notIntNo == -150)
                {
                    //Jerry 2014-03-27 changed by Paole required
                    //AARTOBase.ErrorProcessing(String.Format(errMessage + "This notice has been fully paid,NotIntNo: {0}", notIntNo.ToString()));
                    AARTOBase.LogProcessing(String.Format(errMessage + "This notice has been fully paid,NotIntNo: {0}", notIntNo.ToString()), LogType.Info);
                    //Jerry 2014-03-21 don't need set document import failed
                    //WriteErrorMessageIntoDMS("Notice", String.Format(errMessage + "This notice has been fully paid,NotIntNo: {0}", notIntNo.ToString()));
                    return false;
                }
                // Oscar 2013-03-12 added, we don't update which has case number.
                else if (notIntNo == -151)
                {
                    //EntLibLogger.WriteLog(LogCategory.Error, lastUser, String.Format(errMessage + "This notice has a case number,NotIntNo: {0}", notIntNo.ToString()));
                    //Jerry 2014-03-27 changed by Paole required
                    //AARTOBase.ErrorProcessing(String.Format(errMessage + "This notice has a case number,NotIntNo: {0}", notIntNo.ToString()));
                    AARTOBase.LogProcessing(String.Format(errMessage + "This notice has a case number,NotIntNo: {0}", notIntNo.ToString()), LogType.Info);
                    return false;
                }
                else if (notIntNo == -160)
                {
                    //EntLibLogger.WriteLog(LogCategory.Error, lastUser, String.Format(errMessage + "This document can't be reimported if there are existing reps, NotIntNo: {0}", notIntNo.ToString()));
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "This document can't be reimported if there are existing reps, NotIntNo: {0}", notIntNo.ToString()));
                    WriteErrorMessageIntoDMS("Notice", String.Format(errMessage + "This document can't be reimported if there are existing reps, NotIntNo: {0}", notIntNo.ToString()));
                }
                else if (notIntNo == -170)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "Officer error found when re-import,NotIntNo: {0}", notIntNo.ToString()));
                    WriteErrorMessageIntoDMS("Officer Error", String.Format(errMessage + "Officer error found when re-import,NotIntNo: {0}", notIntNo.ToString()));
                    return false;
                }
                //Jake 2014-12-23 added for 5383, create HWO_ImportedWithErrors
                else if (notIntNo == -180)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "handling HWO_ImportedWithErrors"));
                    WriteErrorMessageIntoDMS("ImportedWithErrors", String.Format(errMessage + "handling HWO_ImportedWithErrors"));
                    return false;
                }
                else if (notIntNo == -1000)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "handling HWO_ImportedWithErrors"));
                    WriteErrorMessageIntoDMS("ImportedWithErrors", errMessage);
                    return false;
                }
                else
                {
                    if (notIntNo < 0)
                    {
                        AARTOBase.ErrorProcessing(String.Format(errMessage + "Unknown Error Importing S56, return code: " + notIntNo.ToString()));
                        WriteErrorMessageIntoDMS("Exception", String.Format("Unknown Error Importing S56, return code: " + notIntNo.ToString()));
                        return false;
                    }
                }
                #endregion
            }
            catch (Exception e)
            {
                AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, e.Message));
                WriteErrorMessageIntoDMS("Exception", string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, e.Message));
                return false;
            }

            return false;
        }

        public bool DoImportSection341(ImportDataEntity importDataEntity)
        {
            string errMessage = string.Empty;
            string notTicketNo = string.Empty;
            string docID = string.Empty;
            //Jerry 2014-04-16 comment out batchID, get it from global variable
            //string batchID = string.Empty;
            bool dateValidated = true;
            bool isByLaw = false;//2013-07-03 Heidi added for 4997

            try
            {
                #region loadeXML
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(importDataEntity.BaDoResult);
                XmlElement rootElement = xmlDoc.DocumentElement;
                XmlNodeList dataList = rootElement.SelectNodes(@"//Data");

                Hashtable hsXML = new Hashtable();
                Hashtable hsXMLText = new Hashtable();
                Hashtable hsAlternateCharge = new Hashtable();

                string officerError = string.Empty;
                string filmNo = string.Empty;
                string frameNo = string.Empty;
                bool isOfficerError = false;

                if (rootElement.HasAttribute("OfficerErrorIds"))
                {
                    officerError = rootElement.Attributes["OfficerErrorIds"].Value;
                    isOfficerError = !String.IsNullOrEmpty(officerError);
                }
                if (rootElement.HasAttribute("BatchId"))
                {
                    filmNo = rootElement.Attributes["BatchId"].Value;
                    //Jerry 2014-04-16 comment out batchID, get it from global variable
                    //batchID = filmNo;
                }
                if (rootElement.HasAttribute("DocId"))
                {
                    frameNo = rootElement.Attributes["DocId"].Value;
                }

                foreach (XmlNode xn in dataList)
                {
                    if (!hsXML.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["Value"] != null)
                    {
                        hsXML.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["Value"].Value.Trim());
                    }

                    if (!hsXMLText.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["Text"] != null)
                    {
                        hsXMLText.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["Text"].Value.Trim());
                    }

                    //Jake added 20101108 , accept Alternate charge code
                    if (!hsAlternateCharge.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["AlternateOffenceCode"] != null)
                    {
                        hsAlternateCharge.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["AlternateOffenceCode"].Value.Trim());
                    }
                }
                #endregion

                #region Notice

                bool fieldFound = true;
                int autIntNo = 0;
                if (!String.IsNullOrEmpty(importDataEntity.ClientID))
                {
                    autIntNo = importHandwrittenOffences.GetAuthIntNoByAutCode(importDataEntity.ClientID);
                }
                docID = importDataEntity.BaDoID;
                //Jerry 2014-04-16 comment out batchID, get it from global variable
                //batchID = batchID;

                // IS 02 Jun 2010
                notTicketNo = ConvertNoticeTicketNo(getHashTableValue(hsXML, "NoticeNumber", ref fieldFound));

                string notRegNo = getHashTableValue(hsXML, "RegNo", ref fieldFound);
                isByLaw = notRegNo == "XX000000" ? true : false; //2013-07-03 Heidi added for 4997
                notRegNo = notRegNo == "XX000000" ? string.Empty : notRegNo;    // 2013-04-28 add by Henry for by-law

                string notVehicleRegisterNo = getHashTableValue(hsXML, "VehicleRegisterNo", ref fieldFound);

                string notCourtNo = getHashTableValue(hsXML, "CourtNo", ref fieldFound);
                //Jerry 2014-07-16 validtate court no
                if (!isOfficerError && !ValidateCourtNo(notCourtNo))
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoCourtNo"), batchID, documentID, notTicketNo), LogType.Error, ServiceBase.ServiceOption.ContinueButQueueFail);
                    WriteErrorMessageIntoDMS("CourtNo", string.Format(ResourceHelper.GetResource("NoCourtNo"), batchID, documentID, notTicketNo));
                    return false;
                }

                DateTime offenceDate;
                String offenceDateStr, offenceTimeStr;
                DateTime compareStartDate = DateTime.Parse("1900-01-01");
                DateTime compareEndDate = DateTime.Parse("2070-01-01");

                offenceDateStr = getHashTableValue(hsXML, "OffenceDate", ref fieldFound);
                offenceTimeStr = getHashTableValue(hsXML, "OffenceTime", ref fieldFound);

                if (DateTime.TryParse(offenceDateStr + " " + offenceTimeStr, out offenceDate))
                {
                    if (offenceDate < compareStartDate || offenceDate > compareEndDate)
                    {
                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, offenceDateStr, offenceTimeStr));
                        WriteErrorMessageIntoDMS("OffenceDate", string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, offenceDateStr, offenceTimeStr));
                        return false;
                    }
                }
                else
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, offenceDateStr, offenceTimeStr));
                    WriteErrorMessageIntoDMS("OffenceDate", string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, offenceDateStr, offenceTimeStr));
                    return false;
                }
                if (offenceDate.Equals(DateTime.MinValue))
                    offenceDate = DateTime.Parse("1900-01-01");

                // Jake 2012-03-26 added 
                //Get offence location description from DMS for street coding
                string notLocDescr = string.Empty;

                if (hsXML.ContainsKey("LocationDescr"))
                {
                    notLocDescr = getHashTableValue(hsXML, "LocationDescr", ref fieldFound);
                }
                else
                {
                    notLocDescr = getHashTableValue(hsXML, "OffencePlaceDescr", ref fieldFound);
                }

                string notVehicleMakeCode = getHashTableValue(hsXML, "VehicleMake", ref fieldFound);
                string notVehicleMake = getHashTableValue(hsXMLText, "VehicleMake", ref fieldFound);
                string notVehicleTypeCode = getHashTableValue(hsXML, "VehicleType", ref fieldFound);
                string notVehicleType = getHashTableValue(hsXMLText, "VehicleType", ref fieldFound);
                string notVehicleColour = getHashTableValue(hsXML, "VehicleColour", ref fieldFound);
                DateTime? notVehicleLicenceExpiry = null;
                DateTime expiryDate = DateTime.Parse("1900-01-01"); ;

                string tempLicenceExpory = getHashTableValue(hsXML, "VehicleLicenceExpiry", ref fieldFound);
                if (!String.IsNullOrEmpty(tempLicenceExpory))
                {
                    if (DateTime.TryParse(tempLicenceExpory, out expiryDate))
                    {
                        if (expiryDate < compareStartDate || expiryDate > compareEndDate)
                        {
                            AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("VehicleLicenceExpiryInvalid"), documentID, batchID, tempLicenceExpory));
                            WriteErrorMessageIntoDMS("VehicleLicenceExpiry", string.Format(ResourceHelper.GetResource("VehicleLicenceExpiryInvalid"), documentID, batchID, tempLicenceExpory));
                            return false;
                        }
                    }
                    else
                    {
                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("VehicleLicenceExpiryInvalid"), documentID, batchID, tempLicenceExpory));
                        WriteErrorMessageIntoDMS("VehicleLicenceExpiry", string.Format(ResourceHelper.GetResource("VehicleLicenceExpiryInvalid"), documentID, batchID, tempLicenceExpory));
                        return false;
                    }
                    if (expiryDate.Equals(DateTime.MinValue))
                    {
                        expiryDate = DateTime.Parse("1900-01-01");
                    }
                    notVehicleLicenceExpiry = expiryDate;
                }

                string notOfficerNo = getHashTableValue(hsXML, "OfficerCode", ref fieldFound);
                string notOfficerName = getHashTableValue(hsXML, "OfficerName", ref fieldFound);
                string notOfficerGroup = getHashTableValue(hsXML, "OfficerGroupCode", ref fieldFound);
                string notDmsCaptureClerk = getHashTableValue(hsXML, "NotDMSCaptureClerk", ref fieldFound);

                bool existOfficerNo = GetTrafficOfficer(notOfficerNo);
                if (!existOfficerNo)
                {
                    string errorMessage = string.Format(ResourceHelper.GetResource("OfficerNumNotExists"), notOfficerNo);
                    //EntLibLogger.WriteLog(LogCategory.Error, lastUser, string.Format("An error has occurred while processing a Section 341!\nBatch: {0}, Document: {1}, Notice: {2} - \nError: {3}\n", batchID, docID, notTicketNo, errorMessage));
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrProcessS341"), batchID, docID, notTicketNo, errorMessage), LogType.Error, ServiceBase.ServiceOption.Continue);
                    WriteErrorMessageIntoDMS("OfficerCode", string.Format(ResourceHelper.GetResource("ErrProcessS341"), batchID, docID, notTicketNo, errorMessage),
                        BatchDocumentStatusList.DocumentReturnedToQC);
                    return false;
                }

                DateTime? notDmsCaptureDate = null; DateTime tempDate;
                string captureDate = getHashTableValue(hsXML, "NotDMSCaptureDate", ref fieldFound);
                if (!String.IsNullOrEmpty(captureDate))
                {
                    if (DateTime.TryParse(captureDate, out tempDate))
                    {
                        notDmsCaptureDate = tempDate;
                        if (tempDate < compareStartDate || tempDate > compareEndDate)
                        {
                            AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("CaptureDateInvalid"), documentID, batchID, captureDate));
                            WriteErrorMessageIntoDMS("NotDMSCaptureDate", string.Format(ResourceHelper.GetResource("CaptureDateInvalid"), documentID, batchID, captureDate));
                            return false;
                        }
                    }
                    else
                    {
                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("CaptureDateInvalid"), documentID, batchID, captureDate));
                        WriteErrorMessageIntoDMS("NotDMSCaptureDate", string.Format(ResourceHelper.GetResource("CaptureDateInvalid"), documentID, batchID, captureDate));
                        return false;
                    }
                }

                string scanDate = getHashTableValue(hsXML, "ScanDateTime", ref fieldFound, false);
                #endregion

                #region Charge
                string chgOffenceCode = getHashTableValue(hsXML, "Charge1OffenceCode", ref fieldFound);
                //Jake added 20101108 chgOffenceCode ="00~N"
                string chgNoAog1 = string.Empty;
                if (!String.IsNullOrEmpty(chgOffenceCode))
                {
                    if (chgOffenceCode.Contains("~"))
                    {
                        chgNoAog1 = chgOffenceCode.Split('~')[1];
                        chgOffenceCode = chgOffenceCode.Split('~')[0];
                    }
                    else
                    {
                        chgNoAog1 = "N";
                    }
                }
                string chgOffenceDescr = getHashTableValue(hsXMLText, "Charge1OffenceCode", ref fieldFound);

                // IF ChargeOffenceCode2  does not exist, contine to import
                bool fieldNotFound = false;
                string chgOffenceDescr2 = getHashTableValue(hsXMLText, "Charge2OffenceCode", ref fieldNotFound);
                string chgOffenceCode2 = getHashTableValue(hsXML, "Charge2OffenceCode", ref fieldNotFound);
                // Jake added 20101228, add charge offence code 3
                string chgOffenceDescr3 = getHashTableValue(hsXMLText, "Charge3OffenceCode", ref fieldNotFound);
                string chgOffenceCode3 = getHashTableValue(hsXML, "Charge3OffenceCode", ref fieldNotFound);

                //Jake added 20101108 ,add alternate charge code 
                string chgAlternateOffenceCode1 = String.Empty;
                string chgAlternateOffenceCode2 = String.Empty;
                // Jake added 20101228 , add alternate offence code 3
                string chgAlternateOffenceCode3 = String.Empty;

                if (hsAlternateCharge.ContainsKey("Charge1OffenceCode"))
                {
                    chgAlternateOffenceCode1 = hsAlternateCharge["Charge1OffenceCode"].ToString();
                }
                if (hsAlternateCharge.ContainsKey("Charge2OffenceCode"))
                {
                    chgAlternateOffenceCode2 = hsAlternateCharge["Charge2OffenceCode"].ToString();
                }
                // Jake added 20101228 , add alternate offence code 3
                if (hsAlternateCharge.ContainsKey("Charge3OffenceCode"))
                {
                    chgAlternateOffenceCode3 = hsAlternateCharge["Charge2OffenceCode"].ToString();
                }
                //Jake added 20101108 chgOffenceCode ="00~N"
                string chgNoAog2 = string.Empty;
                if (!String.IsNullOrEmpty(chgOffenceCode2))
                {
                    if (chgOffenceCode2.Contains("~"))
                    {
                        chgNoAog2 = chgOffenceCode2.Split('~')[1];
                        chgOffenceCode2 = chgOffenceCode2.Split('~')[0];
                    }
                    else
                    {
                        chgNoAog2 = "N";
                    }
                }

                string chgNoAog3 = string.Empty;
                if (!String.IsNullOrEmpty(chgOffenceCode3))
                {
                    if (chgOffenceCode3.Contains("~"))
                    {
                        chgNoAog3 = chgOffenceCode3.Split('~')[1];
                        chgOffenceCode3 = chgOffenceCode3.Split('~')[0];
                    }
                    else
                    {
                        chgNoAog3 = "N";
                    }
                }

                float chgFineAmount1 = 0;
                float.TryParse(getHashTableValue(hsXML, "Charge1Amt", ref fieldFound), out chgFineAmount1);

                float chgFineAmount2 = 0;
                float.TryParse(getHashTableValue(hsXML, "Charge2Amt", ref fieldFound), out chgFineAmount2);

                float chgFineAmount3 = 0;
                float.TryParse(getHashTableValue(hsXML, "Charge3Amt", ref fieldFound), out chgFineAmount3);

                #endregion

                #region Film & Frame

                SIL.DMS.DAL.Entities.TList<BatchDocumentImage> batchDocumentImageList = new BatchDocumentImageService().GetByBaDoId(importDataEntity.BaDoID);
                int ifsIntNo = importDataEntity.IFSIntNo;

                #endregion

                #region Accused

                string accSurname = getHashTableValue(hsXML, "Surname", ref fieldFound);
                string accForenames = getHashTableValue(hsXML, "Forenames", ref fieldFound);
                string accIdNumber = getHashTableValue(hsXML, "IDNo", ref fieldFound);
                string accStAdd1 = getHashTableValue(hsXML, "Address1", ref fieldFound);
                string accStAdd2 = getHashTableValue(hsXML, "Address2", ref fieldFound);
                //string accStAdd3 = getHashTableValue(hsXML, "Address3", ref fieldFound);
                string accPoAdd1 = getHashTableValue(hsXML, "BusinessAddress", ref fieldFound);
                string accPoAdd2 = getHashTableValue(hsXML, "BusinessAddress2", ref fieldFound);
                string suburb = getHashTableValue(hsXML, "Suburb", ref fieldFound);
                string townOrCity = getHashTableValue(hsXML, "TownOrCity", ref fieldFound);
                string businessSuburb = getHashTableValue(hsXML, "BusinessSuburb", ref fieldFound);
                string businessTownOrCity = getHashTableValue(hsXML, "BusinessTownOrCity", ref fieldFound);
                string accStCode = getHashTableValue(hsXML, "PostalCode1", ref fieldFound);
                string accTelHome = getHashTableValue(hsXML, "TelNo1", ref fieldFound);
                string accPoCode = getHashTableValue(hsXML, "PostalCode2", ref fieldFound);
                string accTelWork = getHashTableValue(hsXML, "TelNo2", ref fieldFound);

                #endregion

                #region 2013-05-17 New Fields
                // 2013-05-17 added by Heidi for locationSuburb 
                int loSuIntNo = 0;
                bool fieldLocationSuburbFound = true;
                string locationSuburbDesc = getHashTableValue(hsXML, "LocationSuburb", ref fieldLocationSuburbFound);
                if (fieldLocationSuburbFound == false)
                {
                    loSuIntNo = GetLoSuIntNoByAutIntNo(autIntNo);
                }
                else
                {
                    if (!String.IsNullOrEmpty(locationSuburbDesc) && locationSuburbDesc.Trim().Length > 0)
                    {
                        if (locationSuburbDesc.Trim().Contains("~"))
                        {
                            int.TryParse(locationSuburbDesc.Trim().Split('~')[0], out loSuIntNo);
                        }
                        else
                        {
                            // Jake 2013-12-03 add get locationsuburb by descr if losuIntNo does not stored in xml
                            int.TryParse(locationSuburbDesc.Trim(), out loSuIntNo);
                            if (loSuIntNo == 0)
                            {
                                loSuIntNo = GetLocationSuburbByDescr(locationSuburbDesc, autIntNo);
                            }
                        }
                    }
                }

                #endregion

                //Jerry 2014-08-04 add for EMPD
                string otherNationality = getHashTableValue(hsXML, "OtherNationality", ref fieldFound, false);
                string licenceCodeName = getHashTableValue(hsXML, "LicenceCodeName", ref fieldFound, false);
                string licenceClassName = getHashTableValue(hsXML, "LicenceClassName", ref fieldFound, false);
                string licenceTypeName = getHashTableValue(hsXML, "LicenceTypeName", ref fieldFound, false);
                string mvaNo = getHashTableValue(hsXML, "MVANo", ref fieldFound, false);
                string ccNumber = getHashTableValue(hsXML, "CCNumber", ref fieldFound, false);

                if (!fieldFound) { return false; }

                #region  2013-05-17 added by Heidi for locationSuburb
                if (loSuIntNo == 0)
                {
                    //EntLibLogger.WriteLog(LogCategory.General, lastUser, "Not Found Valid LoSuIntNo!");
                    //Jerry 2014-03-21 change it
                    //AARTOBase.LogProcessing("Not Found Valid LoSuIntNo!", LogType.Info, ServiceBase.ServiceOption.Continue);
                    //WriteErrorMessageIntoDMS("LocationSuburb", "Not Found Valid LoSuIntNo!");
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoLoSuIntNo"), batchID, documentID, notTicketNo), LogType.Error, ServiceBase.ServiceOption.ContinueButQueueFail);
                    WriteErrorMessageIntoDMS("LocationSuburb", string.Format(ResourceHelper.GetResource("NoLoSuIntNo"), batchID, documentID, notTicketNo));
                    return false;

                }
                #endregion

                #region Image file server

                //Check Image File Server
                SIL.DMS.DAL.Entities.ImageFileServer imageFileServer_DMS = new SIL.DMS.DAL.Services.ImageFileServerService().GetByIfsIntNo(ifsIntNo);
                if (imageFileServer_DMS != null)
                {
                    ifsIntNo = importHandwrittenOffences.CheckImageFileServer("DMSScanImage", imageFileServer_DMS.IfServerName, imageFileServer_DMS.ImageMachineName, imageFileServer_DMS.ImageDrive, imageFileServer_DMS.ImageRootFolder, false, imageFileServer_DMS.ImageShareName, AARTOBase.LastUser);
                }
                else
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ImageFileServerErr"), documentID, batchID, importDataEntity.DoTeTypeID, ifsIntNo));
                    WriteErrorMessageIntoDMS("DMSScanImage", string.Format(ResourceHelper.GetResource("ImageFileServerErr"), documentID, batchID, importDataEntity.DoTeTypeID, ifsIntNo));
                    return false;
                }

                #endregion

                #region chgOffenceCode
                if (!String.IsNullOrEmpty(chgOffenceCode) && !String.IsNullOrEmpty(chgOffenceCode2)
                    && chgOffenceCode == chgOffenceCode2)
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceCode1EqualOffenceCode2"), documentID, batchID, chgOffenceCode, chgOffenceCode2));
                    WriteErrorMessageIntoDMS("OffenceCode1", string.Format(ResourceHelper.GetResource("OffenceCode1EqualOffenceCode2"), documentID, batchID, chgOffenceCode, chgOffenceCode2));
                    return false;
                }
                #endregion

                int notIntNo = 0;
                //Import data to AartoNoticeDocument (TMS DataBase)
                if (batchDocumentImageList != null && batchDocumentImageList.Count > 0)
                {
                    //Jerry 2014-04-14 change BatchDocumentService to a global variable
                    //BatchDocumentService documentService = new BatchDocumentService();
                    //Jerry 2014-04-16 comment out batchID, get it from global variable
                    //batchID = documentService.GetByBaDoId(importDataEntity.BaDoID).BatchId;
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromSeconds(60)))
                    {
                        BatchDocument batchDocument = documentService.GetByBaDoId(importDataEntity.BaDoID);
                        if (batchDocument != null)
                        {
                            if (dateValidated)
                                batchDocument.BaDoStId = (int)BatchDocumentStatusList.ImportSucceeded;
                            //batchDocument = documentService.Save(batchDocument);
                            else
                            {
                                batchDocument.BaDoStId = (int)BatchDocumentStatusList.ImportFailed;
                                batchDocument.LastUser = AARTOBase.LastUser;//Jerry 2013-06-26 add last user
                                batchDocument = documentService.Save(batchDocument);
                                scope.Complete();
                                return false;
                            }

                            string originalRegNo = string.Empty;
                            DateTime originalOffenceDate = DateTime.MinValue;   // 2013-09-26 add by Henry
                            SIL.AARTO.DAL.Entities.Notice originalNotice = new NoticeService().GetByNotTicketNo(notTicketNo).FirstOrDefault();
                            if (originalNotice != null && !String.IsNullOrEmpty(originalNotice.NotRegNo))
                            {
                                originalRegNo = originalNotice.NotRegNo;
                                originalOffenceDate = originalNotice.NotOffenceDate;   // 2013-09-26 add by Henry
                            }

                            string aR_2500 = new AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(autIntNo, "2500").ARString.Trim();
                            AuthorityRuleInfo aR_5050 = new AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(autIntNo, "5050");

                            #region ImportHandwrittenOffencesSection341
                            try
                            {
                                //Jerry 2013-02-06 add notPaymentDate, aR_2500
                                DateTime notPaymentDate = default(DateTime);
                                DateRuleInfo dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "NotPaymentDate");
                                notPaymentDate = offenceDate.AddDays(dateRule.ADRNoOfDays);

                                notIntNo = importHandwrittenOffences.ImportHandwrittenOffencesSection341(autIntNo, notTicketNo, notCourtNo, //notCourtName,
                                    offenceDate, notLocDescr, notRegNo, notVehicleRegisterNo, notVehicleMakeCode, notVehicleMake, notVehicleTypeCode, notVehicleType, notVehicleColour,
                                    notVehicleLicenceExpiry, notOfficerNo, notOfficerName, notOfficerGroup, notDmsCaptureClerk, notDmsCaptureDate,
                                    chgOffenceCode, chgOffenceCode2, chgOffenceCode3, chgOffenceDescr, chgOffenceDescr2, chgOffenceDescr3, chgFineAmount1, chgFineAmount2, chgFineAmount3,
                                    filmNo, frameNo, ifsIntNo, isOfficerError,
                                    batchDocumentImageList[0].BaDoId, batchDocumentImageList[0].BaDoImOrder,
                                    batchDocumentImageList[0].BaDoImFilePath, AARTOBase.LastUser, conCode, officerError,
                                    chgAlternateOffenceCode1, chgAlternateOffenceCode2, chgAlternateOffenceCode3, chgNoAog1, chgNoAog2, chgNoAog3,
                                    accSurname, accForenames, accIdNumber, accPoAdd1, accPoAdd2, accStAdd1, accStAdd2, businessSuburb, businessTownOrCity, accTelHome, accTelWork, accStCode, accPoCode, suburb, townOrCity, "H341", "HandwrittenS341Capture",
                                    loSuIntNo, scanDate, notPaymentDate, aR_2500, otherNationality, licenceCodeName, licenceClassName, licenceTypeName, mvaNo, ccNumber);
                            }
                            catch (Exception ex)
                            {
                                AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, ex.Message));
                                WriteErrorMessageIntoDMS("Exception", string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, ex.Message));
                                return false;
                            }
                            #endregion

                            // Jake 2012-12-21 added return value !=-150  => a notice fully paid we need to change batch document status to imported
                            if (notIntNo <= 0)
                            {
                                using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Suppress))
                                {
                                    batchDocument.BaDoStId = (notIntNo == -150 || notIntNo == -151) ? (int)BatchDocumentStatusList.ImportSucceeded : (int)BatchDocumentStatusList.ImportFailed;
                                    batchDocument.LastUser = AARTOBase.LastUser;//Jerry 2013-06-26 add last user
                                    documentService.Save(batchDocument);
                                    ts.Complete();
                                }
                                if (notIntNo == -150)
                                {
                                    errMessage = string.Format(ResourceHelper.GetResource("ImportS341"), batchDocument.BatchId, batchDocument.BaDoId, notTicketNo);
                                }
                                // Oscar 2013-03-12 added, we don't update which has case number.
                                else if (notIntNo == -151)
                                    errMessage = string.Format(ResourceHelper.GetResource("ImportS341Failed"), batchID, docID, notTicketNo);
                                else if (notIntNo == -1000)
                                {
                                    errMessage = string.Format(ResourceHelper.GetResource("NoticeFullyPaidAndCourtOrAmountNotMatch"), batchID, docID, notTicketNo);
                                    scope.Complete();
                                }
                                else
                                {
                                    errMessage = string.Format(ResourceHelper.GetResource("ImportS341Error"), batchDocument.BatchId, batchDocument.BaDoId, notTicketNo);
                                }
                                //errMessage = string.Format(ResourceHelper.GetResource("CreateErr"), documentID, BatchID, notTicketNo, importDataEntity.DoTeTypeID);
                            }
                            else
                            {
                                batchDocument.LastUser = AARTOBase.LastUser;//Jerry 2013-06-26 add last user
                                documentService.Save(batchDocument);

                                //errMessage = string.Format("Import of S341 successful - Batch: {0}, Document: {1}, Notice: {2}  ", batchID, docID, notTicketNo);
                                errMessage = string.Format(ResourceHelper.GetResource("S341ImportSuccessful"), batchID, docID, notTicketNo);

                                #region push queue
                                //Oscar20111219 add push queue
                                //Oscar20111229 change & format the codes
                                SIL.AARTO.DAL.Entities.TList<NoticeFrame> nfList = new NoticeFrameService().GetByNotIntNo(notIntNo);
                                if (nfList != null && nfList.Count > 0)
                                {
                                    int frameIntNo = nfList[0].FrameIntNo;
                                    string violationType = string.Empty;

                                    Frame frame = new FrameService().GetByFrameIntNo(frameIntNo);
                                    SIL.AARTO.DAL.Entities.Notice notice = new NoticeService().GetByNotIntNo(notIntNo);
                                    SIL.AARTO.DAL.Entities.Authority authority = new SIL.AARTO.DAL.Services.AuthorityService().GetByAutIntNo(autIntNo);

                                    DateTime not1stPostDate = notice.NotPosted1stNoticeDate ?? DateTime.Now;
                                    string autCode = authority.AutCode;

                                    DateRuleInfo dateRule;
                                    int daysOfFrameNatis = 0,
                                        daysOfGenerateSummons = 0;
                                    //daysOfGenerate2ndNotice = 0;

                                    //jerry 2011-12-22 add
                                    dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "NotSummonsBeforeDate");
                                    int daysOfSummonsServicePeriodExpired = dateRule.ADRNoOfDays;

                                    //Jerry 2013-03-06 add
                                    dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "NotPaymentDate");
                                    int daysOffenceDateToPaymentDate = dateRule.ADRNoOfDays;
                                    //dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotPosted1stNoticeDate", "NotIssue2ndNoticeDate");
                                    //int daysPosted1stTo2ndNoticeDate = dateRule.ADRNoOfDays;
                                    dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotIssue2ndNoticeDate", "Not2ndPaymentDate");
                                    int daysIssue2ndNoticeTo2ndPaymentDate = dateRule.ADRNoOfDays;

                                    //2014-11-5 Tommi add
                                    dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotIssue2ndNoticeDate", "SMSExtractOn2ndNoticeDate");
                                    int noOfDaysToSMSExtractOn2ndNoticeDate = dateRule.ADRNoOfDays;



                                    bool pushFrameNatisQueue = false;
                                    bool pushGenerateSummonsQueue = false;
                                    bool pushGenerate2ndNoticeQueue = false;

                                    //2013-07-03 Heidi added for 4997
                                    QueueItemProcessor queProcessor = new QueueItemProcessor();
                                    if (isByLaw)
                                    {
                                        queProcessor.Send(new QueueItem()
                                        {
                                            Body = notIntNo,
                                            Group = string.Format("{0}|{1}", autCode.Trim(), "HWO"),
                                            ActDate = notice.NotOffenceDate.AddDays(daysOffenceDateToPaymentDate + 2),//Jerry 2013-03-06 add
                                            LastUser = AARTOBase.LastUser, //Jerry 2013-06-26 add
                                            QueueType = ServiceQueueTypeList.Generate2ndNotice
                                        });

                                        //2014-11-4 Tommi add for SMS extract notice on2ndnotice
                                        queProcessor.Send(new QueueItem()
                                        {
                                            Body = notIntNo,
                                            Group = autCode.Trim(),
                                            ActDate = notice.NotOffenceDate.AddDays(daysOffenceDateToPaymentDate + 2 + noOfDaysToSMSExtractOn2ndNoticeDate),//Jerry 2013-03-06 add
                                            LastUser = AARTOBase.LastUser,
                                            QueueType = ServiceQueueTypeList.SMSExtractOn2ndNotice
                                        });

                                        scope.Complete();
                                        //EntLibLogger.WriteLog(LogCategory.General, lastUser, errMessage);
                                        AARTOBase.LogProcessing(errMessage, LogType.Info, ServiceBase.ServiceOption.Continue);
                                        return true;
                                    }

                                    //Jerry 2013-03-11 add && frame.RegNo != "0000000000"
                                    if (!string.IsNullOrEmpty(frame.RegNo) && frame.RegNo != "0000000000" ||
                                        // 2013-09-26 added by Henry
                                        (originalOffenceDate.Date != frame.OffenceDate.Date &&
                                        originalOffenceDate.Date > (frame.FrameSendNatisDateTime ?? DateTime.MinValue).Date))
                                    {
                                        dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "NotIssue1stNoticeDate");
                                        daysOfFrameNatis = dateRule.ADRNoOfDays;

                                        if (String.IsNullOrEmpty(originalRegNo))
                                        {
                                            pushFrameNatisQueue = true;
                                        }
                                        else
                                        {
                                            // jake 2012-11-01 added 
                                            // re-import and regno changed,we need to re send it to natis here
                                            //Jerry 2013-03-11 move frame.RegNo != "0000000000" in front of here
                                            //if ((originalRegNo.Equals(frame.RegNo) == false) && frame.RegNo != "0000000000")
                                            if (originalRegNo.Equals(frame.RegNo) == false || (originalNotice.NoticeStatus == 597 && frame.FrameStatus == 992) || originalOffenceDate.Date != frame.OffenceDate.Date) // 2013-09-26 add by Henry
                                            {
                                                if (originalNotice != null && (originalNotice.NoticeStatus == 597 && frame.FrameStatus == 992))
                                                {
                                                    frame.RegNo = notice.NotRegNo;
                                                    frame.FrameGetNatisDateTime = null;
                                                    frame.LastUser = AARTOBase.LastUser;
                                                }
                                                pushFrameNatisQueue = true;
                                            }
                                        }
                                        // 2014-02-12 Heidi moved for synchronous SIL.AARTOService.HandwrittenImporter and HandwrittenOffencesImporter
                                        if (pushFrameNatisQueue)
                                        {
                                            // 2014-01-07 Heidi moved for fixed 5087 bug
                                            AuthorityRuleInfo authOrityRuleInfo = new AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(autIntNo, "0400");
                                            if (authOrityRuleInfo != null)
                                            {
                                                if (authOrityRuleInfo.ARString.Trim().Equals("Y"))
                                                {
                                                    frame.FrameStatus = (int)FrameStatusList.NL_ToBeResentToNatis;
                                                }
                                                else
                                                {
                                                    frame.FrameStatus = (int)FrameStatusList.ToBeResentToNaTIS;
                                                }
                                                frame = new FrameService().Save(frame);
                                            }
                                            else
                                            {
                                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NotFoundAuthorityRuleAR_0400"), batchID, docID), LogType.Error, ServiceBase.ServiceOption.Continue);
                                                //Jerry 2014-03-21 don't need to set document import failed
                                                //WriteErrorMessageIntoDMS("Exception", string.Format(ResourceHelper.GetResource("NotFoundAuthorityRuleAR_0400"), batchID, docID));
                                                //return false;
                                            }

                                        }
                                    }
                                    if (notice.NoticeStatus == 255 && !string.IsNullOrEmpty(accIdNumber))
                                    {
                                        dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotPosted1stNoticeDate", "NotIssueSummonsDate");
                                        daysOfGenerateSummons = dateRule.ADRNoOfDays;

                                        pushGenerateSummonsQueue = true;
                                        pushGenerate2ndNoticeQueue = true;
                                    }

                                    // Oscar 20120315 changed the Group
                                    string[] group = new string[3];
                                    group[0] = autCode.Trim();
                                    //Jerry 2013-03-07 change
                                    //group[1] = notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) ? "H" : "";
                                    group[1] = (notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) && !notice.IsVideo) ? "H" : "";
                                    group[2] = Convert.ToString(notice.NotCourtName);

                                    // Nick 20120801 added CoCT Report check
                                    bool isIbmPrinter = new AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(autIntNo, "6209").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase);
                                    violationType = GetViolationType(connAARTODB, frameIntNo);

                                    // Oscar 2013-06-17 added
                                    if (noAogDB == null) noAogDB = new NoAOGDB(connAARTODB);
                                    var isNoAog = noAogDB.IsNoAOG(Convert.ToInt32(notIntNo), NoAOGQueryType.Notice);

                                    queProcessor.Send(
                                        (pushFrameNatisQueue ?
                                        new QueueItem()
                                        {
                                            Body = frameIntNo,
                                            Group = autCode,
                                            Priority = daysOfFrameNatis - ((TimeSpan)(DateTime.Now - frame.OffenceDate)).Days,
                                            LastUser = AARTOBase.LastUser, //Jerry 2013-06-26 add
                                            QueueType = ServiceQueueTypeList.Frame_Natis
                                        } : null),
                                        (pushGenerateSummonsQueue ?
                                        new QueueItem()
                                        {
                                            Body = notIntNo,
                                            //Group = autCode,
                                            // Oscar 20120315 changed the Group
                                            Group = string.Join("|", group),
                                            //ActDate = not1stPostDate.AddDays(daysOfGenerateSummons), Jerry 2013-03-06 change
                                            ActDate = isNoAog ? noAogDB.GetNoAOGGenerateSummonsActionDate(autIntNo, notice.NotOffenceDate) : (aR_2500.Equals("Y", StringComparison.OrdinalIgnoreCase) ? notice.NotOffenceDate.AddDays(daysOfGenerateSummons) :
                                            notice.NotOffenceDate.AddDays(daysOffenceDateToPaymentDate + daysIssue2ndNoticeTo2ndPaymentDate + 4)),
                                            QueueType = ServiceQueueTypeList.GenerateSummons,
                                            //jerry 2012-03-31 add Priority
                                            Priority = (notice.NotOffenceDate.Date - DateTime.Now.Date).Days,
                                            //Jerry 2012-05-14 add last user
                                            LastUser = AARTOBase.LastUser
                                        } : null),
                                        //Henry 2013-03-27 add
                                        (aR_5050.ARString.Trim().ToUpper() == "Y" && pushGenerateSummonsQueue ?
                                        new QueueItem()
                                        {
                                            Body = notIntNo,
                                            Group = string.Join("|", group),
                                            ActDate = notice.NotOffenceDate.AddMonths(aR_5050.ARNumeric).AddDays(-5),
                                            QueueType = ServiceQueueTypeList.GenerateSummons,
                                            Priority = (notice.NotOffenceDate.Date - DateTime.Now.Date).Days,
                                            LastUser = AARTOBase.LastUser
                                        } : null),
                                        //jerry 2011-12-22 push SummonsServicePeriodExpired
                                        new QueueItem()
                                        {
                                            Body = notIntNo,
                                            Group = autCode,
                                            ActDate = notice.NotOffenceDate.AddDays(daysOfSummonsServicePeriodExpired),
                                            LastUser = AARTOBase.LastUser, //Jerry 2013-06-26 add
                                            QueueType = ServiceQueueTypeList.SummonsServicePeriodExpired
                                        },
                                        //Oscar 20111229 add
                                        new QueueItem()
                                        {
                                            Body = notIntNo,
                                            LastUser = AARTOBase.LastUser, //Jerry 2013-06-26 add
                                            QueueType = ServiceQueueTypeList.SearchNameIDUpdate
                                        }
                                    );

                                    if (pushGenerate2ndNoticeQueue)
                                    {
                                        // dls 2012-12-05 - move this Q item
                                        //Jerry 2013-03-06 change it from isIbmPrinter to aR_2500.Equals("Y", StringComparison.OrdinalIgnoreCase)
                                        //if (isIbmPrinter)
                                        if (aR_2500.Equals("Y", StringComparison.OrdinalIgnoreCase))
                                        {
                                            // Push queue for 1st notice of HWO
                                            queProcessor.Send(new QueueItem()
                                            {
                                                Body = notIntNo,
                                                Group = string.Format("{0}|{1}|{2}", autCode.Trim(), violationType, "HWO"),
                                                QueueType = ServiceQueueTypeList.Frame_Generate1stNotice,
                                                LastUser = AARTOBase.LastUser
                                            });
                                        }
                                        else
                                        {
                                            queProcessor.Send(new QueueItem()
                                            {
                                                Body = notIntNo,
                                                Group = string.Format("{0}|{1}", autCode.Trim(), "HWO"),
                                                ActDate = notice.NotOffenceDate.AddDays(daysOffenceDateToPaymentDate + 2),//Jerry 2013-03-06 add
                                                LastUser = AARTOBase.LastUser, //Jerry 2013-06-26 add
                                                QueueType = ServiceQueueTypeList.Generate2ndNotice
                                            });
                                            queProcessor.Send(new QueueItem()
                                            {
                                                Body = notIntNo,
                                                Group = autCode.Trim(),
                                                ActDate = notice.NotOffenceDate.AddDays(daysOffenceDateToPaymentDate + 2 + noOfDaysToSMSExtractOn2ndNoticeDate),//Jerry 2013-03-06 add
                                                LastUser = AARTOBase.LastUser,
                                                QueueType = ServiceQueueTypeList.SMSExtractOn2ndNotice
                                            });
                                        }
                                    }

                                }
                                #endregion

                                scope.Complete();
                                //EntLibLogger.WriteLog(LogCategory.General, lastUser, errMessage);
                                AARTOBase.LogProcessing(errMessage, LogType.Info, ServiceBase.ServiceOption.Continue);
                                return true;
                            }
                        }
                    }
                }

                #region record err msg
                if (notIntNo == -1)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Notice"));
                    WriteErrorMessageIntoDMS("Notice", String.Format(errMessage + "creating Notice"));
                    return false;
                }
                else if (notIntNo == -2)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Charge"));
                    WriteErrorMessageIntoDMS("Charge", String.Format(errMessage + "creating Charge"));
                    return false;
                }
                else if (notIntNo == -3)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Film"));
                    WriteErrorMessageIntoDMS("Film", String.Format(errMessage + "creating Film"));
                    return false;
                }
                else if (notIntNo == -4)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "handling Frame"));
                    WriteErrorMessageIntoDMS("Frame", String.Format(errMessage + "handling Frame"));
                    return false;
                }
                else if (notIntNo == -5)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "handling Notice_Frame"));
                    WriteErrorMessageIntoDMS("Notice_Frame", String.Format(errMessage + "handling Notice_Frame"));
                    return false;
                }
                else if (notIntNo == -6)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating AartoDocument and AartoDocumentImage"));
                    WriteErrorMessageIntoDMS("AartoDocumentImage", String.Format(errMessage + "creating AartoDocument and AartoDocumentImage"));
                    return false;
                }
                else if (notIntNo == -30)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "handling offier errors"));
                    WriteErrorMessageIntoDMS("Offier Error", String.Format(errMessage + "handling offier errors"));
                    return false;
                }
                else if (notIntNo == -50)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Driver"));
                    WriteErrorMessageIntoDMS("Driver", String.Format(errMessage + "creating Driver"));
                    return false;
                }
                else if (notIntNo == -51)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating FrameDriver"));
                    WriteErrorMessageIntoDMS("FrameDriver", String.Format(errMessage + "creating FrameDriver"));
                    return false;
                }
                //Added 2010-10-13  ,update status = ImportedFromDMS in AartobmDocument
                else if (notIntNo == -16)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating AaBmStatusIs in AartoBMDocument"));
                    WriteErrorMessageIntoDMS("AartoBMDocument", String.Format(errMessage + "updating AaBmStatusIs in AartoBMDocument"));
                    return false;
                }
                else if (notIntNo == -103 || notIntNo == -104)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "handling Search tables"));
                    WriteErrorMessageIntoDMS("SearchId", String.Format(errMessage + "handling Search tables"));
                    return false;
                }
                else if (notIntNo == -150)
                {
                    //Jerry 2014-03-27 changed by Paole required
                    //AARTOBase.ErrorProcessing(String.Format(errMessage + "This notice has been fully paid,NotIntNo: {0}", notIntNo.ToString()));
                    AARTOBase.LogProcessing(String.Format(errMessage + "This notice has been fully paid,NotIntNo: {0}", notIntNo.ToString()), LogType.Info);
                    //Jerry 2014-03-21 don't need to set document to import failed
                    //WriteErrorMessageIntoDMS("Notice", String.Format(errMessage + "This notice has been fully paid,NotIntNo: {0}", notIntNo.ToString()));
                    return false;
                }
                else if (notIntNo == -151)
                {
                    //EntLibLogger.WriteLog(LogCategory.Error, lastUser, String.Format(errMessage + "This notice has a case number,NotIntNo: {0}", notIntNo.ToString()));
                    //Jerry 2014-03-27 changed by Paole required
                    //AARTOBase.ErrorProcessing(String.Format(errMessage + "This notice has a case number,NotIntNo: {0}", notIntNo.ToString()));
                    AARTOBase.LogProcessing(String.Format(errMessage + "This notice has a case number,NotIntNo: {0}", notIntNo.ToString()), LogType.Info);
                    return false;
                }
                else if (notIntNo == -160)
                {
                    //EntLibLogger.WriteLog(LogCategory.Error, lastUser, String.Format(errMessage + "This document can't be reimported if there is existing reps on it,NotIntNo: {0}", notIntNo.ToString()));
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "This document can't be reimported if there is existing reps on it,NotIntNo: {0}", notIntNo.ToString()));
                    WriteErrorMessageIntoDMS("Notice", String.Format(errMessage + "This document can't be reimported if there is existing reps on it,NotIntNo: {0}", notIntNo.ToString()));
                }
                else if (notIntNo == -170)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "Officer error found when re-import,NotIntNo: {0}", notIntNo.ToString()));
                    WriteErrorMessageIntoDMS("Officer Error", String.Format(errMessage + "Officer error found when re-import,NotIntNo: {0}", notIntNo.ToString()));
                    return false;
                }
                //Jake 2014-12-23 added for 5383, create HWO_ImportedWithErrors
                else if (notIntNo == -180)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "handling HWO_ImportedWithErrors"));
                    WriteErrorMessageIntoDMS("ImportedWithErrors", String.Format(errMessage + "handling HWO_ImportedWithErrors"));
                    return false;
                }
                else if (notIntNo == -1000)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "handling HWO_ImportedWithErrors"));
                    WriteErrorMessageIntoDMS("ImportedWithErrors", errMessage);
                    return false;
                }
                else if (notIntNo == -8)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "handling MVANumber and CCNumber"));
                    WriteErrorMessageIntoDMS("MVANumber", String.Format(errMessage + "handling MVANumber and CCNumber"));
                    return false;
                }
                else
                {
                    if (notIntNo < 0)
                    {
                        AARTOBase.ErrorProcessing(String.Format(errMessage + "Unexpected error code:  " + notIntNo.ToString()));
                        WriteErrorMessageIntoDMS("Exception", String.Format("Unexpected error code:  " + notIntNo.ToString()));
                        return false;
                    }
                }
                #endregion
            }
            catch (Exception e)
            {
                AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, e.Message));
                WriteErrorMessageIntoDMS("Exception", string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, e.Message));
                return false;
            }

            return false;
        }

        public bool DoImportProvincialSection56(ImportDataEntity importDataEntity)
        {
            string errMessage = string.Empty;
            string notTicketNo = string.Empty;
            string docID = string.Empty;
            //Jerry 2014-04-16 comment out batchID, get it from global variable
            //string batchID = string.Empty;
            bool dateValidated = true;

            try
            {
                #region loadeXML
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(importDataEntity.BaDoResult);
                XmlElement rootElement = xmlDoc.DocumentElement;
                XmlNodeList dataList = rootElement.SelectNodes(@"//Data");

                Hashtable hsXML = new Hashtable();
                Hashtable hsXMLText = new Hashtable();
                Hashtable hsAlternateCharge = new Hashtable();

                string officerError = string.Empty;
                string filmNo = string.Empty;
                string frameNo = string.Empty;
                bool isOfficerError = false;
                if (rootElement.HasAttribute("OfficerErrorIds"))
                {
                    officerError = rootElement.Attributes["OfficerErrorIds"].Value;
                    isOfficerError = !String.IsNullOrEmpty(officerError);
                }
                if (rootElement.HasAttribute("BatchId"))
                {
                    filmNo = rootElement.Attributes["BatchId"].Value;
                    //Jerry 2014-04-16 comment out batchID, get it from global variable
                    //batchID = filmNo;
                }
                if (rootElement.HasAttribute("DocId"))
                {
                    frameNo = rootElement.Attributes["DocId"].Value;
                }

                foreach (XmlNode xn in dataList)
                {
                    if (!hsXML.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["Value"] != null)
                    {
                        hsXML.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["Value"].Value.Trim());
                    }

                    if (!hsXMLText.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["Text"] != null)
                    {
                        hsXMLText.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["Text"].Value.Trim());
                    }
                    //Jake added 20101108 , accept Alternate charge code
                    if (!hsAlternateCharge.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["AlternateOffenceCode"] != null)
                    {
                        hsAlternateCharge.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["AlternateOffenceCode"].Value.Trim());
                    }
                }
                #endregion

                SIL.DMS.DAL.Entities.TList<BatchDocumentImage> batchDocumentImageList = new BatchDocumentImageService().GetByBaDoId(importDataEntity.BaDoID);
                docID = importDataEntity.BaDoID;
                //Jerry 2014-04-16 comment out batchID, get it from global variable
                //batchID = batchID;

                #region Notice
                int autIntNo = 0;
                if (!String.IsNullOrEmpty(importDataEntity.ClientID))
                {
                    autIntNo = importHandwrittenOffences.GetAuthIntNoByAutCode(importDataEntity.ClientID);
                }

                // IS 02 Jun 2010
                bool fieldFound = true;
                // IS 02 Jun 2010
                notTicketNo = ConvertNoticeTicketNo(getHashTableValue(hsXML, "NoticeNumber", ref fieldFound));

                //20120906 Adam handle these folowing 2 columns which do not exist on provincial template
                int speedLimit = 0;
                string speedLimitString = getHashTableValue(hsXML, "SpeedLimit", ref fieldFound);
                if (!string.IsNullOrEmpty(speedLimitString))
                {
                    int.TryParse(speedLimitString, out speedLimit);
                }

                int speedReading = 0;
                string speedReadingString = getHashTableValue(hsXML, "SpeedReading", ref fieldFound);
                if (!string.IsNullOrEmpty(speedReadingString))
                {
                    int.TryParse(speedReadingString, out speedReading);
                }

                //--------------------------------------------------------------//

                DateTime offenceDate = DateTime.MinValue;
                DateTime offenceDate2 = DateTime.MinValue;
                DateTime offenceDate3 = DateTime.MinValue;

                DateTime compareStartDate = DateTime.Parse("1900-01-01");
                DateTime compareEndDate = DateTime.Parse("2070-01-01");

                //Jerry 2013-07-09 change
                //string tempDate = getHashTableValue(hsXML, "OffenceDate1", ref fieldFound);
                //string tempTime = getHashTableValue(hsXML, "OffenceTime1", ref fieldFound);
                string tempDate = getHashTableValue(hsXML, "OffenceDate", ref fieldFound);
                string tempTime = getHashTableValue(hsXML, "OffenceTime", ref fieldFound);

                if (DateTime.TryParse(tempDate + " " + tempTime, out offenceDate))
                {
                    if (offenceDate < compareStartDate || offenceDate > compareEndDate)
                    {
                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, tempDate, tempTime));
                        WriteErrorMessageIntoDMS("OffenceDate1", string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, tempDate, tempTime));
                        return false;
                    }
                }
                else
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, tempDate, tempTime));
                    WriteErrorMessageIntoDMS("OffenceDate1", string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, tempDate, tempTime));
                    return false;
                }
                if (offenceDate.Equals(DateTime.MinValue))
                    offenceDate = DateTime.Parse("1900-01-01");


                tempDate = getHashTableValue(hsXML, "OffenceDate2", ref fieldFound);
                tempTime = getHashTableValue(hsXML, "OffenceTime2", ref fieldFound);

                //DateTime.TryParse(getHashTableValue(hsXML, "OffenceDate2", ref fieldFound) + " " + getHashTableValue(hsXML, "OffenceTime2", ref fieldFound), out offenceDate2);
                //Jerry 2014-03-21 add if (!String.IsNullOrEmpty(tempDate))
                if (!String.IsNullOrEmpty(tempDate))
                {
                    if (DateTime.TryParse(tempDate + " " + tempTime, out offenceDate2))
                    {
                        if (offenceDate2 < compareStartDate || offenceDate2 > compareEndDate)
                        {
                            AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, tempDate, tempTime));
                            WriteErrorMessageIntoDMS("OffenceDate2", string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, tempDate, tempTime));
                            return false;
                        }
                    }
                    else
                    {
                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, tempDate, tempTime));
                        WriteErrorMessageIntoDMS("OffenceDate2", string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, tempDate, tempTime));
                        return false;
                    }
                }

                if (offenceDate2.Equals(DateTime.MinValue))
                    offenceDate2 = DateTime.Parse("1900-01-01");

                tempDate = getHashTableValue(hsXML, "OffenceDate3", ref fieldFound);
                tempTime = getHashTableValue(hsXML, "OffenceTime3", ref fieldFound);
                //DateTime.TryParse(getHashTableValue(hsXML, "OffenceDate3", ref fieldFound) + " " + getHashTableValue(hsXML, "OffenceTime3", ref fieldFound), out offenceDate3);
                //Jerry 2014-03-21 add if (!String.IsNullOrEmpty(tempDate))
                if (!String.IsNullOrEmpty(tempDate))
                {
                    if (DateTime.TryParse(tempDate + " " + tempTime, out offenceDate3))
                    {
                        if (offenceDate3 < compareStartDate || offenceDate3 > compareEndDate)
                        {
                            AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, tempDate, tempTime));
                            WriteErrorMessageIntoDMS("OffenceDate3", string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, tempDate, tempTime));
                            return false;
                        }
                    }
                    else
                    {
                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, tempDate, tempTime));
                        WriteErrorMessageIntoDMS("OffenceDate3", string.Format(ResourceHelper.GetResource("OffenceDateTimeInvalid"), documentID, batchID, tempDate, tempTime));
                        return false;
                    }
                }
                if (offenceDate3.Equals(DateTime.MinValue))
                    offenceDate3 = DateTime.Parse("1900-01-01");

                // Seawen 2012-07-15 added 
                //Get offence location description from DMS for street coding
                string notLocDescr = string.Empty;
                if (hsXML.ContainsKey("LocationDescr"))
                {
                    notLocDescr = getHashTableValue(hsXML, "LocationDescr", ref fieldFound);
                }
                else
                {
                    notLocDescr = getHashTableValue(hsXML, "OffencePlaceDescr", ref fieldFound);
                }

                string notRegNo = getHashTableValue(hsXML, "VehicleRegNo", ref fieldFound);
                notRegNo = notRegNo == "XX000000" ? string.Empty : notRegNo;    // 2013-04-28 add by Henry for by-law
                string regNo2 = getHashTableValue(hsXML, "VehicleRegNo2", ref fieldFound);
                string regNo3 = getHashTableValue(hsXML, "VehicleRegNo3", ref fieldFound);

                string notVehicleMakeCode = getHashTableValue(hsXML, "VehicleMake", ref fieldFound);
                string notVehicleMake = getHashTableValue(hsXMLText, "VehicleMake", ref fieldFound);
                string notVehicleTypeCode = getHashTableValue(hsXML, "VehicleType", ref fieldFound);
                string notVehicleType = getHashTableValue(hsXMLText, "VehicleType", ref fieldFound);
                string notOfficerNo = getHashTableValue(hsXML, "OfficerCode", ref fieldFound);
                string officerNo2 = getHashTableValue(hsXML, "OfficerCode2", ref fieldFound);
                string officerNo3 = getHashTableValue(hsXML, "OfficerCode3", ref fieldFound);

                string notOfficerSname = getHashTableValue(hsXML, "OfficerName", ref fieldFound);
                string notOfficerGroup = getHashTableValue(hsXML, "OfficerGroupCode", ref fieldFound);
                string notDmsCaptureClerk = getHashTableValue(hsXML, "NotDMSCaptureClerk", ref fieldFound);

                //20120906 Adam add the new column to capture provincial traffic centre and other new columns
                string pTCCode = getHashTableValue(hsXML, "ProvincialTrafficCentre", ref fieldFound);
                string offenceStatutoryRef1 = getHashTableValue(hsXML, "OffenceStatutoryRef1", ref fieldFound);
                string offenceStatutoryRef2 = getHashTableValue(hsXML, "OffenceStatutoryRef2", ref fieldFound);
                string offenceStatutoryRef3 = getHashTableValue(hsXML, "OffenceStatutoryRef3", ref fieldFound);

                DateTime? notDmsCaptureDate = null; DateTime tempCaptureDate;
                string captureDate = getHashTableValue(hsXML, "NotDMSCaptureDate", ref fieldFound);
                if (!String.IsNullOrEmpty(captureDate))
                {
                    if (DateTime.TryParse(captureDate, out tempCaptureDate))
                    {
                        notDmsCaptureDate = tempCaptureDate;
                        if (tempCaptureDate < compareStartDate || tempCaptureDate > compareEndDate)
                        {
                            AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("CaptureDateInvalid"), documentID, batchID, captureDate));
                            WriteErrorMessageIntoDMS("NotDMSCaptureDate", string.Format(ResourceHelper.GetResource("CaptureDateInvalid"), documentID, batchID, captureDate));
                            return false;
                        }
                    }
                    else
                    {
                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("CaptureDateInvalid"), documentID, batchID, captureDate));
                        WriteErrorMessageIntoDMS("NotDMSCaptureDate", string.Format(ResourceHelper.GetResource("CaptureDateInvalid"), documentID, batchID, captureDate));
                        return false;
                    }
                }

                string scanDate = getHashTableValue(hsXML, "ScanDateTime", ref fieldFound, false);
                #endregion

                #region Charge
                string chgOffenceCode1 = getHashTableValue(hsXML, "Charge1OffenceCode", ref fieldFound);
                //Jake added 20101108 chgOffenceCode ="00~N"
                string chgNoAog1 = string.Empty;
                if (!String.IsNullOrEmpty(chgOffenceCode1))
                {
                    if (chgOffenceCode1.Contains("~"))
                    {
                        chgNoAog1 = chgOffenceCode1.Split('~')[1];
                        chgOffenceCode1 = chgOffenceCode1.Split('~')[0];
                    }
                    else
                    {
                        chgNoAog1 = "N";
                    }
                }

                // IF ChargeOffenceCode2 or ChargeOffenceCode3 does not exist, contine to import
                bool fieldNotFound = false;
                string chgOffenceCode2 = getHashTableValue(hsXML, "Charge2OffenceCode", ref fieldNotFound);
                //Jake added 20101108 chgOffenceCode ="00~N"
                string chgNoAog2 = string.Empty;
                if (!String.IsNullOrEmpty(chgOffenceCode2))
                {
                    if (chgOffenceCode2.Contains("~"))
                    {
                        chgNoAog2 = chgOffenceCode2.Split('~')[1];
                        chgOffenceCode2 = chgOffenceCode2.Split('~')[0];
                    }
                    else
                    {
                        chgNoAog2 = "N";
                    }

                }
                string chgOffenceCode3 = getHashTableValue(hsXML, "Charge3OffenceCode", ref fieldNotFound);
                //Jake added 20101108 chgOffenceCode ="00~N"
                string chgNoAog3 = string.Empty;
                if (!String.IsNullOrEmpty(chgOffenceCode3))
                {
                    if (chgOffenceCode3.Contains("~"))
                    {
                        chgNoAog3 = chgOffenceCode3.Split('~')[1];
                        chgOffenceCode3 = chgOffenceCode3.Split('~')[0];
                    }
                    else
                    {
                        chgNoAog3 = "N";
                    }
                }

                string chgOffenceDescr1 = getHashTableValue(hsXML, "OffenceDesc1", ref fieldFound);
                string chgOffenceDescr2 = getHashTableValue(hsXML, "OffenceDesc2", ref fieldFound);
                string chgOffenceDescr3 = getHashTableValue(hsXML, "OffenceDesc3", ref fieldFound);
                if (string.IsNullOrEmpty(chgOffenceDescr1))
                {
                    chgOffenceDescr1 = getHashTableValue(hsXMLText, "Charge1OffenceCode", ref fieldFound);
                }
                if (string.IsNullOrEmpty(chgOffenceCode2))
                {
                    chgOffenceDescr2 = getHashTableValue(hsXMLText, "Charge2OffenceCode", ref fieldNotFound);
                }
                if (string.IsNullOrEmpty(chgOffenceCode3))
                {
                    chgOffenceDescr3 = getHashTableValue(hsXMLText, "Charge3OffenceCode", ref fieldNotFound);
                }

                //Jake added 20101108 ,add alternate charge code 
                string chgAlternateOffenceCode1 = String.Empty;
                string chgAlternateOffenceCode2 = String.Empty;
                string chgAlternateOffenceCode3 = String.Empty;
                if (hsAlternateCharge.ContainsKey("Charge1OffenceCode"))
                {
                    chgAlternateOffenceCode1 = hsAlternateCharge["Charge1OffenceCode"].ToString();
                }
                if (hsAlternateCharge.ContainsKey("Charge2OffenceCode"))
                {
                    chgAlternateOffenceCode2 = hsAlternateCharge["Charge2OffenceCode"].ToString();
                }
                if (hsAlternateCharge.ContainsKey("Charge3OffenceCode"))
                {
                    chgAlternateOffenceCode3 = hsAlternateCharge["Charge3OffenceCode"].ToString();
                }

                float chgFineAmount1 = 0;
                float.TryParse(getHashTableValue(hsXML, "Charge1Amt", ref fieldFound), out chgFineAmount1);

                float chgFineAmount2 = 0;
                float.TryParse(getHashTableValue(hsXML, "Charge2Amt", ref fieldNotFound), out chgFineAmount2);

                float chgFineAmount3 = 0;
                float.TryParse(getHashTableValue(hsXML, "Charge3Amt", ref fieldNotFound), out chgFineAmount3);

                DateTime? notPaymentDate = null;
                DateTime tempPymtDate;
                string tempPaidDate = getHashTableValue(hsXML, "PayDate", ref fieldFound);
                if (!String.IsNullOrEmpty(tempPaidDate))
                {
                    if (DateTime.TryParse(tempPaidDate, out tempPymtDate))
                    {
                        if (tempPymtDate < compareStartDate || tempPymtDate > compareEndDate)
                        {
                            AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("PayDateInvalid"), documentID, batchID, tempPymtDate));
                            WriteErrorMessageIntoDMS("PayDate", string.Format(ResourceHelper.GetResource("PayDateInvalid"), documentID, batchID, tempPymtDate));
                            return false;
                        }
                    }
                    else
                    {
                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("PayDateInvalid"), documentID, batchID, tempPymtDate));
                        WriteErrorMessageIntoDMS("PayDate", string.Format(ResourceHelper.GetResource("PayDateInvalid"), documentID, batchID, tempPymtDate));
                        return false;
                    }
                    if (tempDate.Equals(DateTime.MinValue))
                    {
                        tempPymtDate = DateTime.Parse("1900-01-01");
                    }

                    notPaymentDate = tempPymtDate;
                }

                string chgOfficerNatisNo = getHashTableValue(hsXML, "OfficerCode", ref fieldFound);
                string chgOfficerName = getHashTableValue(hsXML, "OfficerName", ref fieldFound);
                #endregion

                #region Summons
                DateTime? courtDate = null;
                DateTime tempCourtDate;
                string tempCourtDateValue = getHashTableValue(hsXML, "TrialDate", ref fieldFound);
                if (!String.IsNullOrEmpty(tempCourtDateValue))
                {
                    if (DateTime.TryParse(tempCourtDateValue, out tempCourtDate))
                    {
                        if (tempCourtDate < compareStartDate || tempCourtDate > compareEndDate)
                        {
                            AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("CourtDateInvalid"), documentID, batchID, tempCourtDateValue));
                            WriteErrorMessageIntoDMS("TrialDate", string.Format(ResourceHelper.GetResource("CourtDateInvalid"), documentID, batchID, tempCourtDateValue));
                            return false;
                        }
                    }
                    else
                    {
                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("CourtDateInvalid"), documentID, batchID, tempCourtDateValue));
                        WriteErrorMessageIntoDMS("TrialDate", string.Format(ResourceHelper.GetResource("CourtDateInvalid"), documentID, batchID, tempCourtDateValue));
                        return false;
                    }
                    if (tempCourtDate.Equals(DateTime.MinValue))
                    {
                        tempCourtDate = DateTime.Parse("1900-01-01");
                    }

                    courtDate = tempCourtDate;
                }

                string sumOfficerNo = getHashTableValue(hsXML, "OfficerCode", ref fieldFound);
                string sumCaseNo = getHashTableValue(hsXML, "CaseNo", ref fieldFound);
                string crtNo = getHashTableValue(hsXML, "CrtNo", ref fieldFound);
                string crtRoomNo = getHashTableValue(hsXML, "CrtRoomNo", ref fieldFound);

                bool isCrtRoomNo = REG_CRT_ROOM_NO.IsMatch(crtRoomNo); // 2013-07-04 add by Henry for fix bug CrtRoomNo error
                bool existOfficerNo = GetTrafficOfficer(sumOfficerNo);
                if (!existOfficerNo)
                {
                    string errorMessage = string.Format(ResourceHelper.GetResource("OfficerNumNotExists"), sumOfficerNo);
                    //EntLibLogger.WriteLog(LogCategory.Error, lastUser, string.Format("An error has occurred while processing a Provincial Section 56!\nBatch: {0}, Document: {1}, Notice: {2} - \nError: {3}\n", batchID, docID, notTicketNo, errorMessage));
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrProcessProvincialS56"), batchID, docID, notTicketNo, errorMessage), LogType.Error, ServiceBase.ServiceOption.Continue);
                    WriteErrorMessageIntoDMS("OfficerCode", string.Format(ResourceHelper.GetResource("ErrProcessProvincialS56"), batchID, docID, notTicketNo, errorMessage),
                        BatchDocumentStatusList.DocumentReturnedToQC);
                    return false;
                }
                #endregion

                #region Accused
                string accSurname = getHashTableValue(hsXML, "Surname", ref fieldFound);
                string accForenames = getHashTableValue(hsXML, "Name", ref fieldFound);
                string accIdNumber = getHashTableValue(hsXML, "IDNumber", ref fieldFound);
                string accSex = getHashTableValue(hsXML, "Sex", ref fieldFound);
                string accOccupation = getHashTableValue(hsXML, "Occupation", ref fieldFound);
                string accAge = getHashTableValue(hsXML, "Age", ref fieldFound);
                string accStAdd1 = getHashTableValue(hsXML, "Address1", ref fieldFound);
                string accStAdd2 = getHashTableValue(hsXML, "Address2", ref fieldFound);
                string accStAdd3 = getHashTableValue(hsXML, "Address3", ref fieldFound);
                string accStCode = getHashTableValue(hsXML, "PostalCode1", ref fieldFound);
                string accTelHome = getHashTableValue(hsXML, "TelNo1", ref fieldFound);
                string accPoAdd1 = getHashTableValue(hsXML, "BusinessAddress", ref fieldFound);
                string accPoCode = getHashTableValue(hsXML, "PostalCode2", ref fieldFound);
                string accTelWork = getHashTableValue(hsXML, "TelNo2", ref fieldFound);
                #endregion

                #region 2010-09-26 New Fields

                string nationality = getHashTableValue(hsXML, "Nationality", ref fieldFound);
                string driverLicence = getHashTableValue(hsXML, "Licence", ref fieldFound);

                string suburb = getHashTableValue(hsXML, "Suburb", ref fieldFound);
                string townOrCity = getHashTableValue(hsXML, "TownOrCity", ref fieldFound);

                string accPoAdd2 = getHashTableValue(hsXML, "BusinessAddress2", ref fieldFound);
                string accPoAdd3 = getHashTableValue(hsXML, "BusinessAddress3", ref fieldFound);
                string businessSuburb = getHashTableValue(hsXML, "BusinessSuburb", ref fieldFound);
                string businessTownOrCity = getHashTableValue(hsXML, "BusinessTownOrCity", ref fieldFound);

                bool status = false;
                try
                {
                    // Jake 2013-05-27 modified
                    int age = 0;
                    Int32.TryParse(accAge, out age);
                    status = age > 18;//2014-08-06 Heidi fixed for AccOver18(5303)
                    //status = Convert.ToInt32(accAge) > 0;
                }
                catch { status = false; }
                string agNo = getHashTableValue(hsXML, "AGNo", ref fieldFound);

                #endregion

                #region 2013-05-17 New Fields
                // 2013-05-17 added by Heidi for locationSuburb 
                int loSuIntNo = 0;
                bool fieldLocationSuburbFound = true;
                string locationSuburbDesc = getHashTableValue(hsXML, "LocationSuburb", ref fieldLocationSuburbFound);
                if (fieldLocationSuburbFound == false)
                {
                    loSuIntNo = GetLoSuIntNoByAutIntNo(autIntNo);
                }
                else
                {
                    if (!String.IsNullOrEmpty(locationSuburbDesc) && locationSuburbDesc.Trim().Length > 0)
                    {
                        if (locationSuburbDesc.Trim().Contains("~"))
                        {
                            int.TryParse(locationSuburbDesc.Trim().Split('~')[0], out loSuIntNo);
                        }
                        else
                        {
                            // Jake 2013-12-03 add get locationsuburb by descr if losuIntNo does not stored in xml
                            int.TryParse(locationSuburbDesc.Trim(), out loSuIntNo);
                            if (loSuIntNo == 0)
                            {
                                loSuIntNo = GetLocationSuburbByDescr(locationSuburbDesc, autIntNo);
                            }
                        }
                    }
                }

                #endregion

                if (!fieldFound) { return false; }

                #region  2013-05-17 added by Heidi for locationSuburb
                if (loSuIntNo == 0)
                {
                    //EntLibLogger.WriteLog(LogCategory.General, lastUser, "Not Found Valid LoSuIntNo!");
                    //Jerry 2014-03-21 change it.
                    //AARTOBase.LogProcessing("Not Found Valid LoSuIntNo!", LogType.Info, ServiceBase.ServiceOption.Continue);
                    //WriteErrorMessageIntoDMS("LocationSuburb", "Not Found Valid LoSuIntNo!");
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoLoSuIntNo"), batchID, documentID, notTicketNo), LogType.Error, ServiceBase.ServiceOption.ContinueButQueueFail);
                    WriteErrorMessageIntoDMS("LocationSuburb", string.Format(ResourceHelper.GetResource("NoLoSuIntNo"), batchID, documentID, notTicketNo));
                    return false;
                }
                #endregion

                #region Image File server

                int ifsIntNo = importDataEntity.IFSIntNo;
                SIL.DMS.DAL.Entities.ImageFileServer imageFileServer_DMS = new SIL.DMS.DAL.Services.ImageFileServerService().GetByIfsIntNo(ifsIntNo);
                if (imageFileServer_DMS != null)
                {
                    ifsIntNo = importHandwrittenOffences.CheckImageFileServer("DMSScanImage", imageFileServer_DMS.IfServerName, imageFileServer_DMS.ImageMachineName, imageFileServer_DMS.ImageDrive, imageFileServer_DMS.ImageRootFolder, false, imageFileServer_DMS.ImageShareName, AARTOBase.LastUser);
                }
                else
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ImageFileServerErr"), documentID, batchID, importDataEntity.DoTeTypeID, ifsIntNo));
                    WriteErrorMessageIntoDMS("ImageFileServer", string.Format(ResourceHelper.GetResource("ImageFileServerErr"), documentID, batchID, importDataEntity.DoTeTypeID, ifsIntNo));
                    return false;
                }
                #endregion

                #region Offence code
                if (!String.IsNullOrEmpty(chgOffenceCode1)
                    && !String.IsNullOrEmpty(chgOffenceCode2)
                    && chgOffenceCode1 == chgOffenceCode2)
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceCode1EqualOffenceCode2"), documentID, batchID, chgOffenceCode1, chgOffenceCode2));
                    WriteErrorMessageIntoDMS("OffenceCode1", string.Format(ResourceHelper.GetResource("OffenceCode1EqualOffenceCode2"), documentID, batchID, chgOffenceCode1, chgOffenceCode2));
                    return false;
                }
                if (!String.IsNullOrEmpty(chgOffenceCode1)
                   && !String.IsNullOrEmpty(chgOffenceCode3)
                   && chgOffenceCode1 == chgOffenceCode3)
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceCode1EqualOffenceCode3"), documentID, batchID, chgOffenceCode1, chgOffenceCode3));
                    WriteErrorMessageIntoDMS("OffenceCode1", string.Format(ResourceHelper.GetResource("OffenceCode1EqualOffenceCode3"), documentID, batchID, chgOffenceCode1, chgOffenceCode3));
                    return false;
                }
                if (!String.IsNullOrEmpty(chgOffenceCode2)
                   && !String.IsNullOrEmpty(chgOffenceCode3)
                   && chgOffenceCode2 == chgOffenceCode3)
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceCode2EqualOffenceCode3"), documentID, batchID, chgOffenceCode2, chgOffenceCode3));
                    WriteErrorMessageIntoDMS("OffenceCode2", string.Format(ResourceHelper.GetResource("OffenceCode2EqualOffenceCode3"), documentID, batchID, chgOffenceCode2, chgOffenceCode3));
                    return false;
                }
                if (!String.IsNullOrEmpty(chgOffenceCode1)
                    && !String.IsNullOrEmpty(chgOffenceCode2)
                    && !String.IsNullOrEmpty(chgOffenceCode3)
                    && chgOffenceCode2 == chgOffenceCode3
                    && chgOffenceCode1 == chgOffenceCode3)
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("OffenceCodeErr"), documentID, batchID, chgOffenceCode1, chgOffenceCode2, chgOffenceCode3));
                    WriteErrorMessageIntoDMS("OffenceCode1", string.Format(ResourceHelper.GetResource("OffenceCodeErr"), documentID, batchID, chgOffenceCode1, chgOffenceCode2, chgOffenceCode3));
                    return false;
                }
                #endregion

                int notIntNo = 0;
                if (batchDocumentImageList != null && batchDocumentImageList.Count > 0)
                {
                    string batchName = string.Empty;
                    //Jerry 2014-04-14 change BatchService to a global variable
                    Batch batch = batchService.GetByBatchId(filmNo);
                    if (batch != null)
                    {
                        batchName = batch.BatchDescription;
                    }

                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromSeconds(300)))
                    {
                        //Jerry 2014-04-14 change BatchDocumentService to a global variable
                        //BatchDocumentService documentService = new BatchDocumentService();
                        BatchDocument batchDocument = documentService.GetByBaDoId(importDataEntity.BaDoID);
                        if (batchDocument != null)
                        {
                            if (!isCrtRoomNo)   // 2013-07-04 add by Henry for fix bug CrtRoomNo error
                            {
                                //EntLibLogger.WriteLog(LogCategory.Error, lastUser, string.Format("An error has occurred while processing a Provincial Section 56!\nBatch: {0}, Document: {1}, Notice: {2} - \nError: {3}\n", batchID, docID, notTicketNo, " Invalid court room number \"" + crtRoomNo + "\""));
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrProcessProvincialS56"), batchID, docID, notTicketNo, " Invalid court room number \"" + crtRoomNo + "\""), LogType.Error, ServiceBase.ServiceOption.Continue);
                                WriteErrorMessageIntoDMS("CrtRoomNo", string.Format(ResourceHelper.GetResource("ErrProcessProvincialS56"), batchID, docID, notTicketNo, " Invalid court room number \"" + crtRoomNo + "\""),
                                    BatchDocumentStatusList.DocumentReturnedToQC);
                                return false;
                            }

                            #region Speed limit
                            if (speedLimit > 150 || speedReading > 255 || dateValidated == false)
                            {
                                if (speedLimit > 150 || speedReading > 255)
                                {
                                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("SpeedLimitReadErr"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, speedLimit.ToString(), speedReading.ToString()));
                                    WriteErrorMessageIntoDMS("Speedlimit", string.Format(ResourceHelper.GetResource("SpeedLimitReadErr"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, speedLimit.ToString(), speedReading.ToString()));
                                }
                                batchDocument.BaDoStId = (int)BatchDocumentStatusList.ImportFailed;
                                //Jerry 2013-06-26 add last user
                                batchDocument.LastUser = AARTOBase.LastUser;
                                batchDocument = documentService.Save(batchDocument);
                                scope.Complete();
                                return false;
                            }
                            else
                            {
                                batchDocument.BaDoStId = (int)BatchDocumentStatusList.ImportSucceeded;
                                //batchDocument = documentService.Save(batchDocument);
                            }
                            #endregion

                            #region original Summons
                            //Heidi 2014-04-29 added to push queue WithdrawSec56WithOfficerError(5239)
                            DateTime originalCourtDate = DateTime.Parse("1900-01-01");
                            SIL.AARTO.DAL.Entities.Summons originalSummons = null;
                            SIL.AARTO.DAL.Entities.Notice originalNotice = _noticeService.GetByNotTicketNo(notTicketNo).FirstOrDefault();
                            if (originalNotice != null)
                            {
                                NoticeSummons originalNoticeSummons = _noticeSummonsService.GetByNotIntNo(originalNotice.NotIntNo).FirstOrDefault();
                                if (originalNoticeSummons != null)
                                {
                                    originalSummons = _summonsService.GetBySumIntNo(originalNoticeSummons.SumIntNo);
                                    if (originalSummons != null && originalSummons.SumCourtDate.HasValue)
                                    {
                                        originalCourtDate = Convert.ToDateTime(originalSummons.SumCourtDate);
                                    }
                                }
                            }
                            #endregion


                            #region ImportHandwrittenOffencesProvincialSection56
                            try
                            {
                                notIntNo = importHandwrittenOffences.ImportHandwrittenOffencesProvincialSection56(
                                    autIntNo, filmNo, frameNo, notTicketNo, speedLimit, speedReading,
                                    offenceDate, offenceDate2, offenceDate3, notLocDescr, notRegNo, regNo2, regNo3, notVehicleMakeCode, notVehicleMake,
                                    notVehicleTypeCode, notVehicleType, notOfficerNo, officerNo2, officerNo3, notOfficerSname,
                                    notOfficerGroup, notDmsCaptureClerk, notDmsCaptureDate, chgOffenceCode1, chgOffenceDescr1,
                                    chgOffenceCode2, chgOffenceDescr2, chgOffenceCode3,
                                    chgOffenceDescr3, chgFineAmount1, chgFineAmount2, chgFineAmount3,
                                    notPaymentDate, chgOfficerNatisNo, chgOfficerName, conCode, courtDate, sumOfficerNo, sumCaseNo,
                                    crtNo, crtRoomNo, ifsIntNo, accSurname, accForenames, accIdNumber,
                                    accOccupation, accAge, accSex, accPoAdd1, accPoCode, accTelHome, accStAdd1, accStAdd2, accStAdd3, accStCode,
                                    accTelWork, isOfficerError, batchDocumentImageList[0].BaDoId,
                                    batchDocumentImageList[0].BaDoImOrder, batchDocumentImageList[0].BaDoImFilePath, AARTOBase.LastUser, officerError,
                                    nationality, driverLicence, accPoAdd2, accPoAdd3, agNo, businessSuburb, businessTownOrCity, suburb, townOrCity, status,
                                    chgAlternateOffenceCode1, chgAlternateOffenceCode2, chgAlternateOffenceCode3,
                                    chgNoAog1, chgNoAog2, chgNoAog3, pTCCode, offenceStatutoryRef1, offenceStatutoryRef2, offenceStatutoryRef3, "H56", loSuIntNo, scanDate);
                            }
                            catch (Exception ex)
                            {
                                AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, ex.Message));
                                WriteErrorMessageIntoDMS("Exception", string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, ex.Message));
                                return false;
                            }
                            #endregion

                            if (notIntNo <= 0)
                            {
                                using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Suppress))
                                {
                                    batchDocument.BaDoStId = (notIntNo == -150 || notIntNo == -151) ? (int)BatchDocumentStatusList.ImportSucceeded : (int)BatchDocumentStatusList.ImportFailed;
                                    //Jerry 2013-06-26 add last user
                                    batchDocument.LastUser = AARTOBase.LastUser;
                                    batchDocument = documentService.Save(batchDocument);
                                    ts.Complete();
                                }
                                //errMessage = string.Format(ResourceHelper.GetResource("CreateErr"), documentID, BatchID, notTicketNo, importDataEntity.DoTeTypeID);
                                if (notIntNo == -150)
                                {
                                    //errMessage = string.Format("Section 56 imported, batch: {0}, document: {1}, notice: {2} - ", batchID, docID, notTicketNo);
                                    errMessage = string.Format(ResourceHelper.GetResource("ImportProvincialS56"), batchID, docID, notTicketNo);
                                }
                                // Oscar 2013-03-12 added, we don't update which has case number.
                                else if (notIntNo == -151)
                                    //errMessage = string.Format("Section 56 import failed, fail case number allocated, batch: {0}, document: {1}, notice: {2} - ", batchID, docID, notTicketNo);
                                    errMessage = string.Format(ResourceHelper.GetResource("ImportProvincialS56Failed"), batchID, docID, notTicketNo);
                                else
                                {
                                    //errMessage = string.Format("Section 56 error for batch: {0}, document: {1}, notice: {2} - ", batchID, docID, notTicketNo);
                                    errMessage = string.Format(ResourceHelper.GetResource("ImportProvincialS56Error"), batchID, docID, notTicketNo);
                                }
                            }
                            else
                            {
                                //Jerry 2013-06-26 add last user
                                batchDocument.LastUser = AARTOBase.LastUser;
                                batchDocument = documentService.Save(batchDocument);

                                //errMessage = string.Format("Import of S56 successful - Batch: {0}, Document: {1}, Notice: {2}  ", batchID, docID, notTicketNo);
                                errMessage = string.Format(ResourceHelper.GetResource("ProvincialS56ImportSuccessful"), batchID, docID, notTicketNo);

                                #region push queue
                                QueueItemProcessor queProcessor = new QueueItemProcessor();
                                NoticeSummons noticeSummons = new NoticeSummonsService().GetByNotIntNo(notIntNo).FirstOrDefault();
                                if (noticeSummons != null)
                                {
                                    SIL.AARTO.DAL.Entities.Authority authority = new SIL.AARTO.DAL.Services.AuthorityService().GetByAutIntNo(autIntNo);

                                    int sumIntNo = noticeSummons.SumIntNo;

                                    DateRuleInfo dateRule;

                                    SIL.AARTO.DAL.Entities.Summons summonsEntity = new SummonsService().GetBySumIntNo(sumIntNo);
                                    if (summonsEntity.CrtRintNo.HasValue && summonsEntity.SumCourtDate.HasValue)
                                    {
                                        Court courtEntity = new CourtService().GetByCrtIntNo(summonsEntity.CrtIntNo);
                                        CourtRoom courtRoomEntity = new CourtRoomService().GetByCrtRintNo(summonsEntity.CrtRintNo.Value);
                                        dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "CDate", "FinalCourtRoll");
                                        int daysOfAllocateCaseNo = dateRule.ADRNoOfDays;

                                        string[] group = new string[4];
                                        group[0] = authority.AutCode.Trim();
                                        //group[1] = courtEntity.CrtNo.Trim();
                                        // Oscar 201203012 changed to use CrtName
                                        group[1] = courtEntity.CrtName.Trim();
                                        group[2] = courtRoomEntity.CrtRoomNo.Trim();
                                        group[3] = summonsEntity.SumCourtDate.Value.ToString("yyyy-MM-dd HH:mm:ss");

                                        // Oscar 2013-03-11 added for passed date checking.
                                        var actionDate = summonsEntity.SumCourtDate.Value.AddDays(daysOfAllocateCaseNo);
                                        if (DateTime.Now.Date <= actionDate.Date)
                                        {
                                            queProcessor.Send(
                                                new QueueItem()
                                                {
                                                    Body = sumIntNo,
                                                    Group = string.Join("|", group),
                                                    ActDate = actionDate,
                                                    LastUser = AARTOBase.LastUser, //Jerry 2013-06-26 add
                                                    QueueType = ServiceQueueTypeList.SummonsIsServed
                                                });
                                        }
                                        else
                                        {
                                            //EntLibLogger.WriteLog(LogCategory.Warning, lastUser, string.Format("Court date has already passed. No case number will be allocated for summons \"{0}\".", summonsEntity.SummonsNo));
                                            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CourtDatePassed"), summonsEntity.SummonsNo), LogType.Warning, ServiceBase.ServiceOption.Continue);
                                        }
                                    }

                                    /* Jake 2015-03-23 comment out ,no need to push this queue items for S56 summons
                                    //Jake 2015-03-17 use new date rule 
                                    //jerry 2011-12-22 push CancelExpiredSummons
                                    //dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "SumIssueDate", "SumExpiredDate");
                                    dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "SumExpireDate");
                                    int noDaysForSummonsExpiry = dateRule.ADRNoOfDays;

                                    queProcessor.Send(
                                        new QueueItem()
                                        {
                                            Body = sumIntNo,
                                            Group = authority.AutCode.Trim(),
                                            ActDate = offenceDate.AddDays(noDaysForSummonsExpiry + 1),
                                            LastUser = AARTOBase.LastUser, //Jerry 2013-06-26 add
                                            QueueType = ServiceQueueTypeList.ExpireUnservedSummons
                                        }
                                    );
                                    */
                                    #region push WithdrawSec56WithOfficerError queue
                                    //Heidi 2014-04-29 added to push queue WithdrawSec56WithOfficerError(5239)
                                    if (summonsEntity != null && summonsEntity.SumCourtDate.HasValue)
                                    {
                                        creatWithdrawSec56WithOfficerErrorQueue(notIntNo, autIntNo, originalSummons, authority.AutCode, Convert.ToDateTime(summonsEntity.SumCourtDate), originalCourtDate, queProcessor);
                                    }
                                    #endregion
                                }
                                #endregion

                                scope.Complete();
                                //Logger.Info(string.Format(ResourceHelper.GetResource("ImportSuccessful"), documentID, BatchID, importDataEntity.DoTeTypeID));
                                //EntLibLogger.WriteLog(LogCategory.General, lastUser, errMessage);
                                AARTOBase.LogProcessing(errMessage, LogType.Info, ServiceBase.ServiceOption.Continue);
                                return true;
                            }
                        }
                    }
                }

                #region record err msg
                if (notIntNo == -1)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Notice"));
                    WriteErrorMessageIntoDMS("Notice", String.Format(errMessage + "creating Notice"));
                    return false;
                }
                else if (notIntNo == -2)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Charge"));
                    WriteErrorMessageIntoDMS("Charge", String.Format(errMessage + "creating Charge"));
                    return false;
                }
                else if (notIntNo == -3)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Summons"));
                    WriteErrorMessageIntoDMS("Summons", String.Format(errMessage + "creating Summons"));
                    return false;
                }
                else if (notIntNo == -4)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Summons_Charge"));
                    WriteErrorMessageIntoDMS("Summons_Charge", String.Format(errMessage + "creating Summons_Charge"));
                    return false;
                }
                else if (notIntNo == -5)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Accused"));
                    WriteErrorMessageIntoDMS("Accused", String.Format(errMessage + "creating Accused"));
                    return false;
                }
                else if (notIntNo == -6)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Film"));
                    WriteErrorMessageIntoDMS("Film", String.Format(errMessage + "creating Film"));
                    return false;
                }
                else if (notIntNo == -7)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Frame"));
                    WriteErrorMessageIntoDMS("Frame", String.Format(errMessage + "creating Frame"));
                    return false;
                }
                else if (notIntNo == -8)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Notice_Frame"));
                    WriteErrorMessageIntoDMS("Notice_Frame", String.Format(errMessage + "creating Notice_Frame"));
                    return false;
                }
                else if (notIntNo == -9)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating AartoDocumentImage"));
                    WriteErrorMessageIntoDMS("AartoDocumentImage", String.Format(errMessage + "creating AartoDocumentImage"));
                    return false;
                }
                else if (notIntNo == -10)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating SumCharge"));
                    WriteErrorMessageIntoDMS("SumCharge", String.Format(errMessage + "creating SumCharge"));
                    return false;
                }
                else if (notIntNo == -11)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Driver"));
                    WriteErrorMessageIntoDMS("Driver", String.Format(errMessage + "creating Driver"));
                    return false;
                }
                else if (notIntNo == -12)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Notice"));
                    WriteErrorMessageIntoDMS("Notice", String.Format(errMessage + "updating Notice"));
                    return false;
                }
                else if (notIntNo == -13)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Evidence Pack"));
                    WriteErrorMessageIntoDMS("Evidence Pack", String.Format(errMessage + "creating Evidence Pack"));
                    return false;
                }
                else if (notIntNo == -14)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "Court Date Provincial S56 Allocation"));
                    WriteErrorMessageIntoDMS("S56 Allocation", String.Format(errMessage + "Court Provincial Date S56 Allocation"));
                    return false;
                }
                else if (notIntNo == -15)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating Film"));
                    WriteErrorMessageIntoDMS("Film", String.Format(errMessage + "updating Film"));
                    return false;
                }
                else if (notIntNo == -16)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating AARTONoticeDocument"));
                    WriteErrorMessageIntoDMS("AARTONoticeDocument", String.Format(errMessage + "creating AARTONoticeDocument"));
                    return false;
                }
                else if (notIntNo == -17)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating Frame"));
                    WriteErrorMessageIntoDMS("Frame", String.Format(errMessage + "updating Frame"));
                    return false;
                }
                else if (notIntNo == -18)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "getting offence code"));
                    WriteErrorMessageIntoDMS("Offence code", String.Format(errMessage + "getting offence code"));
                    return false;
                }
                else if (notIntNo == -19)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating Charge"));
                    WriteErrorMessageIntoDMS("Charge", String.Format(errMessage + "updating Charge"));
                    return false;
                }
                else if (notIntNo == -20)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating Summons"));
                    WriteErrorMessageIntoDMS("Summons", String.Format(errMessage + "updating Summons"));
                    return false;
                }
                else if (notIntNo == -21)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating SumCharge"));
                    WriteErrorMessageIntoDMS("SumCharge", String.Format(errMessage + "updating SumCharge"));
                    return false;
                }
                else if (notIntNo == -22)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating Accused"));
                    WriteErrorMessageIntoDMS("Accused", String.Format(errMessage + "updating Accused"));
                    return false;
                }
                else if (notIntNo == -103 || notIntNo == -104)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating Search tables"));
                    WriteErrorMessageIntoDMS("SearchId", String.Format(errMessage + "updating Search tables"));
                    return false;
                }
                else if (notIntNo == -30)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "handling offier errors"));
                    WriteErrorMessageIntoDMS("Offier errors", String.Format(errMessage + "handling offier errors"));
                    return false;
                }
                //Added 2010-10-13  ,update status = ImportedFromDMS in AartobmDocument
                else if (notIntNo == -36)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "updating AaBmStatusIs in AartoBMDocument"));
                    WriteErrorMessageIntoDMS("AartoBMDocument", String.Format(errMessage + "updating AaBmStatusIs in AartoBMDocument"));
                    return false;
                }
                else if (notIntNo == -27)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating CourtDate"));
                    WriteErrorMessageIntoDMS("CourtDate", String.Format(errMessage + "creating CourtDate"));
                    return false;
                }
                else if (notIntNo == -101)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Charge_SumCharge"));
                    WriteErrorMessageIntoDMS("Charge_SumCharge", String.Format(errMessage + "creating Charge_SumCharge"));
                    return false;
                }
                else if (notIntNo == -102)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Notice_Summons"));
                    WriteErrorMessageIntoDMS("Notice_Summons", String.Format(errMessage + "creating Notice_Summons"));
                    return false;
                }
                else if (notIntNo == -110)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "officer error message:Incorrect CourtDate does not in AARTOOfficerError table"));
                    WriteErrorMessageIntoDMS("CourtDate", String.Format(errMessage + "officer error message:Incorrect CourtDate does not in AARTOOfficerError table"));
                    return false;
                }
                else if (notIntNo == -130)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating CourtRoom for CourtRoomNo:{0}", crtRoomNo));
                    WriteErrorMessageIntoDMS("CourtRoomNo", String.Format(errMessage + "creating CourtRoom for CourtRoomNo:{0}", crtRoomNo));
                    return false;
                }
                else if (notIntNo == -150)
                {
                    //Jerry 2014-03-27 changed by Paole required
                    //AARTOBase.ErrorProcessing(String.Format(errMessage + "This notice has been fully paid,NotIntNo: {0}", notIntNo.ToString()));
                    AARTOBase.LogProcessing(String.Format(errMessage + "This notice has been fully paid,NotIntNo: {0}", notIntNo.ToString()), LogType.Info);
                    //Jerry 2014-03-21 don't need to set document import failed
                    //WriteErrorMessageIntoDMS("Notice", String.Format(errMessage + "This notice has been fully paid,NotIntNo: {0}", notIntNo.ToString()));
                    return false;
                }
                // Oscar 2013-03-12 added, we don't update which has case number.
                else if (notIntNo == -151)
                {
                    //EntLibLogger.WriteLog(LogCategory.Error, lastUser, String.Format(errMessage + "This notice has a case number,NotIntNo: {0}", notIntNo.ToString()));
                    //Jerry 2014-03-27 changed by Paole required
                    AARTOBase.LogProcessing(String.Format(errMessage + "This notice has a case number,NotIntNo: {0}", notIntNo.ToString()), LogType.Info);
                    return false;
                }
                else if (notIntNo == -160)
                {
                    //EntLibLogger.WriteLog(LogCategory.Error, lastUser, String.Format(errMessage + "This document can't be reimported if there are existing reps, NotIntNo: {0}", notIntNo.ToString()));
                    AARTOBase.LogProcessing(String.Format(errMessage + "This document can't be reimported if there are existing reps, NotIntNo: {0}", notIntNo.ToString()), LogType.Error, ServiceBase.ServiceOption.Continue);
                    WriteErrorMessageIntoDMS("Notice", String.Format(errMessage + "This document can't be reimported if there are existing reps, NotIntNo: {0}", notIntNo.ToString()));
                }
                else if (notIntNo == -170)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "Officer error found when re-import,NotIntNo: {0}", notIntNo.ToString()));
                    WriteErrorMessageIntoDMS("Officer Error", String.Format(errMessage + "Officer error found when re-import,NotIntNo: {0}", notIntNo.ToString()));
                    return false;
                }
                else
                {
                    if (notIntNo < 0)
                    {
                        AARTOBase.ErrorProcessing(String.Format(errMessage + "Unknown Error Importing S56, return code: ", notIntNo.ToString()));
                        WriteErrorMessageIntoDMS("Unknown Error", String.Format("Unknown Error Importing S56, return code: " + notIntNo.ToString()));
                        return false;
                    }
                }
                #endregion
            }
            catch (Exception e)
            {
                AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, e.Message));
                WriteErrorMessageIntoDMS("Exception", string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, e.Message));
                return false;
            }

            return false;
        }

        public bool DoImportCancelledDocument(ImportDataEntity importDataEntity, string nstCode, string psttName)
        {
            string errMessage = string.Empty;
            string notTicketNo = string.Empty;
            string docID = string.Empty;
            //Jerry 2014-04-16 comment out batchID, get it from global variable
            //string batchID = string.Empty;
            string documentType = string.Empty;

            try
            {
                #region loadeXML

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(importDataEntity.BaDoResult);
                XmlElement rootElement = xmlDoc.DocumentElement;
                XmlNodeList dataList = rootElement.SelectNodes(@"//Data");

                Hashtable hsXML = new Hashtable();
                Hashtable hsXMLText = new Hashtable();

                string officerError = string.Empty;
                string filmNo = string.Empty;
                string frameNo = string.Empty;
                bool isOfficerError = false;

                if (rootElement.HasAttribute("OfficerErrorIds"))
                {
                    officerError = rootElement.Attributes["OfficerErrorIds"].Value;
                    isOfficerError = !String.IsNullOrEmpty(officerError);
                }
                if (rootElement.HasAttribute("BatchId"))
                {
                    filmNo = rootElement.Attributes["BatchId"].Value;
                    //Jerry 2014-04-16 comment out batchID, get it from global variable
                    //batchID = filmNo;
                }
                if (rootElement.HasAttribute("DocId"))
                {
                    frameNo = rootElement.Attributes["DocId"].Value;
                }

                foreach (XmlNode xn in dataList)
                {
                    if (!hsXML.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["Value"] != null)
                    {
                        hsXML.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["Value"].Value.Trim());
                    }

                    if (!hsXMLText.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["Text"] != null)
                    {
                        hsXMLText.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["Text"].Value.Trim());
                    }
                }
                #endregion

                documentType = importDataEntity.DoTeTypeID.Trim();
                bool fieldFound = true;

                int autIntNo = 0;
                if (!String.IsNullOrEmpty(importDataEntity.ClientID))
                {
                    autIntNo = importHandwrittenOffences.GetAuthIntNoByAutCode(importDataEntity.ClientID);
                }

                docID = importDataEntity.BaDoID;
                //Jerry 2014-04-16 comment out batchID, get it from global variable
                //batchID = batchID;

                #region Notice
                notTicketNo = ConvertNoticeTicketNo(getHashTableValue(hsXML, "NoticeNumber", ref fieldFound));
                string notOfficerNo = getHashTableValue(hsXML, "OfficerCode", ref fieldFound);
                //Jerry 2013-07-16 add
                bool existOfficerNo = GetTrafficOfficer(notOfficerNo);
                if (!existOfficerNo)
                {
                    string errorMessage = string.Format(ResourceHelper.GetResource("OfficerNumNotExists"), notOfficerNo);
                    //EntLibLogger.WriteLog(LogCategory.Error, lastUser, string.Format("An error has occurred while processing a Cancelled Document!\nBatch: {0}, Document: {1}, Notice: {2} - \nError: {3}\n", batchID, docID, notTicketNo, errorMessage));
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrProcessCancelDoc"), batchID, docID, notTicketNo, errorMessage), LogType.Error, ServiceBase.ServiceOption.Continue);
                    WriteErrorMessageIntoDMS("OfficerCode", string.Format(ResourceHelper.GetResource("ErrProcessCancelDoc"), batchID, docID, notTicketNo, errorMessage),
                        BatchDocumentStatusList.DocumentReturnedToQC);
                    return false;
                }

                string notOfficerName = getHashTableValue(hsXML, "OfficerName", ref fieldFound);
                string notOfficerGroup = getHashTableValue(hsXML, "OfficerGroupCode", ref fieldFound);
                string notDMSCaptureClerk = getHashTableValue(hsXML, "NotDMSCaptureClerk", ref fieldFound);
                DateTime notDMSCaptureDate = DateTime.Parse(getHashTableValue(hsXML, "NotDMSCaptureDate", ref fieldFound));
                string scanDate = getHashTableValue(hsXML, "ScanDateTime", ref fieldFound, false);
                #endregion

                #region Film & Frame
                SIL.DMS.DAL.Entities.TList<BatchDocumentImage> batchDocumentImageList = new BatchDocumentImageService().GetByBaDoId(importDataEntity.BaDoID);
                #endregion

                if (!fieldFound) { return false; }

                #region Image File Server
                int ifsIntNo = importDataEntity.IFSIntNo;
                SIL.DMS.DAL.Entities.ImageFileServer imageFileServer_DMS = new SIL.DMS.DAL.Services.ImageFileServerService().GetByIfsIntNo(ifsIntNo);
                if (imageFileServer_DMS != null)
                {
                    ifsIntNo = importHandwrittenOffences.CheckImageFileServer("DMSScanImage", imageFileServer_DMS.IfServerName, imageFileServer_DMS.ImageMachineName, imageFileServer_DMS.ImageDrive, imageFileServer_DMS.ImageRootFolder, false, imageFileServer_DMS.ImageShareName, AARTOBase.LastUser);
                }
                else
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ImageFileServerErr"), documentID, batchID, documentType, ifsIntNo));
                    WriteErrorMessageIntoDMS("ImageFileServer", string.Format(ResourceHelper.GetResource("ImageFileServerErr"), documentID, batchID, documentType, ifsIntNo));
                    return false;
                }
                #endregion

                int notIntNo = 0;
                if (batchDocumentImageList != null && batchDocumentImageList.Count > 0)
                {
                    //Jerry 2014-04-14 change BatchDocumentService to a global variable
                    batchID = documentService.GetByBaDoId(importDataEntity.BaDoID).BatchId;
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromSeconds(300)))
                    {
                        //BatchDocumentService documentService = new BatchDocumentService();
                        BatchDocument batchDocument = documentService.GetByBaDoId(importDataEntity.BaDoID);
                        if (batchDocument != null)
                        {
                            batchDocument.BaDoStId = (int)BatchDocumentStatusList.ImportSucceeded;

                            #region ImportHandwrittenOffenceForCancelledDocument
                            try
                            {
                                notIntNo = importHandwrittenOffences.ImportHandwrittenOffenceForCancelledDocument(documentType, autIntNo, notTicketNo, filmNo, frameNo,
                                     ifsIntNo, notOfficerNo, notOfficerName, notOfficerGroup,
                                      isOfficerError, officerError, batchDocumentImageList[0].BaDoId, batchDocumentImageList[0].BaDoImOrder,
                                    batchDocumentImageList[0].BaDoImFilePath, conCode, AARTOBase.LastUser, nstCode, notDMSCaptureClerk, notDMSCaptureDate, psttName, scanDate);
                            }
                            catch (SqlException ex)
                            {
                                AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, documentType, ex.Message));
                                WriteErrorMessageIntoDMS("Exception", string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, documentType, ex.Message));
                                return false;
                            }
                            #endregion

                            if (notIntNo <= 0)
                            {
                                using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Suppress))
                                {
                                    batchDocument.BaDoStId = (int)BatchDocumentStatusList.ImportFailed;
                                    batchDocument.LastUser = AARTOBase.LastUser;
                                    batchDocument = documentService.Save(batchDocument);
                                    ts.Complete();
                                }
                                //errMessage = string.Format("Cancelled {3} import error for Batch: {0}, Document: {1}, Notice: {2} - ", batchDocument.BatchId, batchDocument.BaDoId, notTicketNo, documentType);
                                errMessage = string.Format(ResourceHelper.GetResource("ImportCancelDocError"), batchDocument.BatchId, batchDocument.BaDoId, notTicketNo, documentType);
                            }
                            else
                            {
                                batchDocument.LastUser = AARTOBase.LastUser;//Jerry 2013-06-26 add last user
                                batchDocument = documentService.Save(batchDocument);
                                //errMessage = string.Format("Import of cancelled {3} successful - Batch: {0}, Document: {1}, Notice: {2} - ", batchID, docID, notTicketNo, documentType);
                                errMessage = string.Format(ResourceHelper.GetResource("CancelDocImportSuccessful"), batchID, docID, notTicketNo, documentType);
                            }

                            scope.Complete();
                            //EntLibLogger.WriteLog(LogCategory.General, lastUser, errMessage);
                            AARTOBase.LogProcessing(errMessage, LogType.Info, ServiceBase.ServiceOption.Continue);
                            return true;
                        }
                    }
                }

                #region record err msg
                if (notIntNo == -1)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Notice"));
                    WriteErrorMessageIntoDMS("Notice", String.Format(errMessage + "creating Notice"));
                    return false;
                }
                else if (notIntNo == -2)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Charge"));
                    WriteErrorMessageIntoDMS("Charge", String.Format(errMessage + "creating Charge"));
                    return false;
                }
                else if (notIntNo == -3)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Summons"));
                    WriteErrorMessageIntoDMS("Summons", String.Format(errMessage + "creating Summons"));
                    return false;
                }
                else if (notIntNo == -4)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Summons_Charge"));
                    WriteErrorMessageIntoDMS("Summons_Charge", String.Format(errMessage + "creating Summons_Charge"));
                    return false;
                }
                else if (notIntNo == -5)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Accused"));
                    WriteErrorMessageIntoDMS("Accused", String.Format(errMessage + "creating Accused"));
                    return false;
                }
                else if (notIntNo == -6)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Film"));
                    WriteErrorMessageIntoDMS("Film", String.Format(errMessage + "creating Film"));
                    return false;
                }
                else if (notIntNo == -7)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Frame"));
                    WriteErrorMessageIntoDMS("Frame", String.Format(errMessage + "creating Frame"));
                    return false;
                }
                else if (notIntNo == -8)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Notice_Frame"));
                    WriteErrorMessageIntoDMS("Notice_Frame", String.Format(errMessage + "creating Notice_Frame"));
                    return false;
                }
                else if (notIntNo == -9)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating AartoDocumentImage"));
                    WriteErrorMessageIntoDMS("AartoDocumentImage", String.Format(errMessage + "creating AartoDocumentImage"));
                    return false;
                }
                else if (notIntNo == -10)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating SumCharge"));
                    WriteErrorMessageIntoDMS("SumCharge", String.Format(errMessage + "creating SumCharge"));
                    return false;
                }
                else if (notIntNo == -11)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Driver"));
                    WriteErrorMessageIntoDMS("Driver", String.Format(errMessage + "creating Driver"));
                    return false;
                }
                else if (notIntNo == -13)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating Evidence Pack"));
                    WriteErrorMessageIntoDMS("Evidence Pack", String.Format(errMessage + "creating Evidence Pack"));
                    return false;
                }

                else if (notIntNo == -16)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating AARTONoticeDocument"));
                    WriteErrorMessageIntoDMS("AARTONoticeDocument", String.Format(errMessage + "creating AARTONoticeDocument"));
                    return false;
                }
                else if (notIntNo == -30)
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "handling offier errors"));
                    WriteErrorMessageIntoDMS("Offier Error", String.Format(errMessage + "handling offier errors"));
                    return false;
                }
                else if (notIntNo == -31) //Jake 2014-08-19 added to handle insertint court judgement type error
                {
                    AARTOBase.ErrorProcessing(String.Format(errMessage + "creating CourtJudgementType"));
                    WriteErrorMessageIntoDMS("CourtJudgementType", String.Format(errMessage + "creating CourtJudgementType"));
                    return false;
                }
                else
                {
                    if (notIntNo < 0)
                    {
                        AARTOBase.ErrorProcessing(String.Format(errMessage + "Unexpected error code: " + notIntNo.ToString()));
                        WriteErrorMessageIntoDMS("Exception", String.Format("Unexpected error code: " + notIntNo.ToString()));
                        return false;
                    }
                }
                #endregion
            }
            catch (Exception e)
            {
                AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, documentType, e.Message));
                WriteErrorMessageIntoDMS("Exception", string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, documentType, e.Message));
                return false;
            }
            return false;
        }

        /// <summary>
        /// Import ChangeOfOffender, PresentationOfDocument and Representation document
        /// Jerry 2014-09-11 add
        /// </summary>
        /// <param name="importDataEntity"></param>
        /// <returns></returns>
        public bool DoImportExternalDocument(ImportDataEntity importDataEntity)
        {
            string errMessage = string.Empty;
            string notTicketNo = string.Empty;
            string docID = string.Empty;

            try
            {
                #region loadeXML
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(importDataEntity.BaDoResult);
                XmlElement rootElement = xmlDoc.DocumentElement;
                XmlNodeList dataList = rootElement.SelectNodes(@"//Data");

                Hashtable hsXML = new Hashtable();
                Hashtable hsXMLText = new Hashtable();
                Hashtable hsAlternateCharge = new Hashtable();

                foreach (XmlNode xn in dataList)
                {
                    if (!hsXML.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["Value"] != null)
                    {
                        hsXML.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["Value"].Value.Trim());
                    }

                    if (!hsXMLText.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["Text"] != null)
                    {
                        hsXMLText.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["Text"].Value.Trim());
                    }
                }
                #endregion

                #region Notice

                bool fieldFound = true;

                notTicketNo = ConvertNoticeTicketNo(getHashTableValue(hsXML, "NoticeNumber", ref fieldFound));
                SIL.AARTO.DAL.Entities.Notice noticeEntity = _noticeService.GetByNotTicketNo(notTicketNo.Trim()).FirstOrDefault();
                if (noticeEntity == null)
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("NoticeNotExist"), documentID, batchID, notTicketNo));
                    WriteErrorMessageIntoDMS("NoticeNumber", string.Format(ResourceHelper.GetResource("NoticeNotExist"), documentID, batchID, notTicketNo));
                    return false;
                }
                int notIntNo = noticeEntity.NotIntNo;

                SIL.DMS.DAL.Entities.TList<BatchDocumentImage> batchDocumentImageList = new BatchDocumentImageService().GetByBaDoId(importDataEntity.BaDoID);
                int ifsIntNo = importDataEntity.IFSIntNo;

                #endregion

                if (!fieldFound) { return false; }

                #region Image file server
                //Check Image File Server
                SIL.DMS.DAL.Entities.ImageFileServer imageFileServer_DMS = new SIL.DMS.DAL.Services.ImageFileServerService().GetByIfsIntNo(ifsIntNo);
                if (imageFileServer_DMS != null)
                {
                    ifsIntNo = importHandwrittenOffences.CheckImageFileServer("DMSScanImage", imageFileServer_DMS.IfServerName, imageFileServer_DMS.ImageMachineName, imageFileServer_DMS.ImageDrive, imageFileServer_DMS.ImageRootFolder, false, imageFileServer_DMS.ImageShareName, AARTOBase.LastUser);
                }
                else
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ImageFileServerErr"), documentID, batchID, importDataEntity.DoTeTypeID, ifsIntNo));
                    WriteErrorMessageIntoDMS("DMSScanImage", string.Format(ResourceHelper.GetResource("ImageFileServerErr"), documentID, batchID, importDataEntity.DoTeTypeID, ifsIntNo));
                    return false;
                }
                #endregion

                if (batchDocumentImageList != null && batchDocumentImageList.Count > 0)
                {
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromSeconds(60)))
                    {
                        BatchDocument batchDocument = documentService.GetByBaDoId(importDataEntity.BaDoID);
                        if (batchDocument != null)
                        {
                            #region DoImportDigitalDocument
                            try
                            {
                                AartoNoticeDocument aartoNoticeDocument = noticeDocumentService.GetByNotIntNo(notIntNo).FirstOrDefault();
                                if (aartoNoticeDocument == null)
                                {
                                    aartoNoticeDocument = new AartoNoticeDocument();
                                    aartoNoticeDocument.AaNoticeDocNo = documentID;
                                    aartoNoticeDocument.NotIntNo = notIntNo;
                                    aartoNoticeDocument.AaDocTypeId = (int)AartoDocumentTypeList.ExternalDocument;
                                    aartoNoticeDocument.AaDocStatusId = (int)AartoDocumentStatusList.New;
                                    aartoNoticeDocument.AaDocCreatedDate = DateTime.Now;
                                    aartoNoticeDocument.IfsIntNo = ifsIntNo;
                                    aartoNoticeDocument.LastUser = AARTOBase.LastUser;
                                    aartoNoticeDocument = noticeDocumentService.Save(aartoNoticeDocument);
                                }

                                int aaDocImgTypeId;
                                switch (importDataEntity.DoTeTypeID.ToString().Trim())
                                {
                                    case "ChangeOfOffender":
                                        aaDocImgTypeId = (int)AartoDocumentImageTypeList.ChangeOfOffender;
                                        break;
                                    case "PresentationOfDocument":
                                        aaDocImgTypeId = (int)AartoDocumentImageTypeList.PresentationOfDocument;
                                        break;
                                    case "Representation":
                                        aaDocImgTypeId = (int)AartoDocumentImageTypeList.Representation;
                                        break;
                                    default:
                                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("DoTeTypeIDError"), documentID, batchID, importDataEntity.DoTeTypeID));
                                        WriteErrorMessageIntoDMS("DoTeTypeID", string.Format(ResourceHelper.GetResource("DoTeTypeIDError"), documentID, batchID, importDataEntity.DoTeTypeID));
                                        return false;
                                }

                                AartoDocumentImage aartoDocumentImage = new AartoDocumentImage();
                                aartoDocumentImage.AaDocId = aartoNoticeDocument.AaNotDocId;
                                aartoDocumentImage.AaDocSourceTableId = (int)AartoDocumentSourceTableList.AARTONoticeDocument;
                                aartoDocumentImage.AaDocImgOrder = batchDocumentImageList[0].BaDoImOrder;
                                aartoDocumentImage.AaDocImgPath = batchDocumentImageList[0].BaDoImFilePath;
                                aartoDocumentImage.AaDocImgDateLoaded = DateTime.Now;
                                aartoDocumentImage.LastUser = AARTOBase.LastUser;
                                aartoDocumentImage.AaDocImgTypeId = aaDocImgTypeId;
                                aartoDocumentImageService.Save(aartoDocumentImage);
                            }
                            catch (Exception ex)
                            {
                                AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, ex.Message));
                                WriteErrorMessageIntoDMS("Exception", string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, ex.Message));
                                return false;
                            }

                            batchDocument.BaDoStId = (int)BatchDocumentStatusList.ImportSucceeded;
                            batchDocument.LastUser = AARTOBase.LastUser;
                            documentService.Save(batchDocument);

                            errMessage = string.Format(ResourceHelper.GetResource("S341ImportSuccessful"), batchID, docID, notTicketNo);

                            scope.Complete();
                            AARTOBase.LogProcessing(errMessage, LogType.Info, ServiceBase.ServiceOption.Continue);
                            return true;

                            #endregion
                        }
                    }
                }
            }
            catch (Exception e)
            {
                AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, e.Message));
                WriteErrorMessageIntoDMS("Exception", string.Format(ResourceHelper.GetResource("ImportException"), documentID, batchID, notTicketNo, importDataEntity.DoTeTypeID, e.Message));
                return false;
            }

            return false;
        }

        private string ConvertNoticeTicketNo(string ticketNo)
        {
            // 2014-02-12 Heidi moved for synchronous SIL.AARTOService.HandwrittenImporter and HandwrittenOffencesImporter
            try
            {
                if (String.IsNullOrEmpty(ticketNo))
                {
                    return string.Empty;
                }
                if (ticketNo.IndexOf("/") > 0 || ticketNo.IndexOf("-") > 0)
                {
                    return ticketNo.Trim();//2013-12-02 updated by Nancy for removing the extra spaces in notTicketNo(#795)
                }
                else
                {
                    if (ticketNo.Length <= 16)
                    {
                        return ticketNo.Insert(2, "/").Insert(8, "/").Insert(12, "/").Trim();//2013-12-02 updated by Nancy for removing the extra spaces in notTicketNo(#795)
                    }
                    else //if (ticketNo.Length > 16 && ticketNo.Length <= 20)
                    {
                        return ticketNo.Insert(2, "/").Insert(9, "/").Insert(13, "/").Trim();//2013-12-02 updated by Nancy for removing the extra spaces in notTicketNo(#795)
                    }
                    //if (ticketNo.Length <= 16)
                    //{
                    //    return ticketNo.Insert(2, "/").Insert(8, "/").Insert(12, "/");
                    //}
                    //else if (ticketNo.Length > 16 && ticketNo.Length <= 20)
                    //{
                    //    return ticketNo.Insert(2, "/").Insert(9, "/").Insert(13, "/");
                    //}
                    //else
                    //{
                    //    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("TicketIncorrect"), documentID, BatchID, ticketNo));
                    //    return ticketNo;
                    //}
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(ResourceHelper.GetResource("InvalidNoticeNumberError"), ticketNo, ex.Message));
            }
        }

        public int GetLoSuIntNoByAutIntNo(int AutIntNo)
        {
            int loSuIntNo = 0;
            LocationSuburb entity = new LocationSuburb();
            try
            {
                LocationSuburbQuery query = new LocationSuburbQuery();
                query.Append(LocationSuburbColumn.AutIntNo, AutIntNo.ToString());
                query.Append(LocationSuburbColumn.LoSuDescr, "None");

                SIL.AARTO.DAL.Entities.TList<LocationSuburb> locationSuburbList = new LocationSuburbService().Find(query as SIL.AARTO.DAL.Data.IFilterParameterCollection);
                if (locationSuburbList != null && locationSuburbList.Count > 0)
                {
                    entity = locationSuburbList[0];
                    if (entity != null)
                    {
                        loSuIntNo = entity.LoSuIntNo;
                    }
                }
                return loSuIntNo;
            }
            catch (Exception ex)
            {
                AARTOBase.ErrorProcessing(ex);
                return loSuIntNo;
            }
        }

        //Jerry 2013-07-12 add NodeMustExist = true parameter
        private string getHashTableValue(Hashtable ht, string index, ref bool found, bool nodeMustExist = true)
        {
            if (ht[index] != null)
            {
                return ht[index].ToString().Trim();
            }
            else
            {
                if (nodeMustExist)
                {
                    found = false;
                    if (index != "LocationSuburb") //2013-07-01 Heidi fixed repeat insert log
                    {
                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("FieldIsNotFound"), documentID, batchID, index));
                        WriteErrorMessageIntoDMS(index, string.Format(ResourceHelper.GetResource("FieldIsNotFound"), documentID, batchID, index));
                    }
                }
                return string.Empty;
            }
        }

        public List<ImportDataEntity> GetBatchDocumentListForImport(string documentID, string clientID)
        {
            List<ImportDataEntity> list = new List<ImportDataEntity>();
            IDataReader idr = null;
            try
            {
                //Jerry 2014-03-20 add @DocumentID parameter in custom SP for service, console don't need this parameter, the value is "";
                //The rownumber value is 1 if service use it.
                //idr = new BatchDocumentService().GetBatchDocumentListByDocumentIDAndImportStatus(documentID, clientID, (int)BatchStatusList.QAFinished, (int)BatchDocumentStatusList.ImportFailed, (int)BatchDocumentStatusList.ImportSucceeded, false);
                //Jerry 2014-04-14 change BatchDocumentService to a global variable
                idr = documentService.GetBatchDocumentListByClientIDAndImportStatus(clientID, 1, (int)BatchStatusList.QAFinished, (int)BatchDocumentStatusList.ImportFailed,
                    (int)BatchDocumentStatusList.ImportSucceeded, false, documentID);
                ImportDataEntity bde = null;
                while (idr.Read())
                {
                    bde = new ImportDataEntity();
                    bde.IFSIntNo = idr["IFSIntNo"] is DBNull ? -1 : Convert.ToInt32(idr["IFSIntNo"]);
                    bde.BaDoID = idr["BaDoID"].ToString();
                    bde.BaDoResult = idr["BaDoResult"].ToString();
                    bde.CaptureDate = idr["CaptureDate"] is DBNull ? DateTime.MinValue : Convert.ToDateTime(idr["CaptureDate"]);
                    bde.ClientID = idr["ClientID"].ToString();
                    bde.DoTeID = idr["DoTeID"].ToString();
                    bde.DoTeTypeID = Convert.ToString(idr["DoTeTypeID"]);
                    bde.DataExportMode = Convert.ToInt32(idr["DaExMoID"]);
                    list.Add(bde);
                }
            }
            catch (Exception ex)
            {
                AARTOBase.ErrorProcessing(ex);
            }
            finally
            {
                if (idr != null)
                {
                    idr.Close();
                    idr.Dispose();
                }
            }
            return list;
        }

        #region WithdrawSec56WithOfficerError Queue

        private void creatWithdrawSec56WithOfficerErrorQueue(int notIntNo, int autIntNo, SIL.AARTO.DAL.Entities.Summons originalSummons, string autCode, DateTime courtDate, DateTime originalCourtDate, QueueItemProcessor queProcessor)
        {
            //Heidi 2014-04-29 added to push queue WithdrawSec56WithOfficerError(5239)
            var notice = _noticeService.GetByNotIntNo(notIntNo);
            DateRuleInfo dR_sumCourtDate_WithDrawnDate = _dateRuleInfo.GetDateRuleInfoByDRNameAutIntNo(autIntNo, "SumCourtDate", "WithDrawnDate");
            int noDaysForWithDrawnDate = dR_sumCourtDate_WithDrawnDate.ADRNoOfDays;
            if (notice != null)
            {
                if (notice.NoticeStatus == 581)
                {
                    string AR_5610 = _authorityRuleInfo.GetAuthorityRulesInfoByWFRFANameAutoIntNo(autIntNo, "5610").ARString.Trim();
                    if (AR_5610.Equals("Y", StringComparison.OrdinalIgnoreCase))
                    {
                        if (originalSummons != null)
                        {
                            #region Get existing queue list
                            _serviceQueueQuery.Append(ServiceQueueColumn.SeQuKey, notIntNo.ToString());
                            _serviceQueueQuery.Append(ServiceQueueColumn.SeQuGroup, autCode.Trim());
                            _serviceQueueQuery.Append(ServiceQueueColumn.SqtIntNo, ((int)ServiceQueueTypeList.WithdrawSec56WithOfficerError).ToString());
                            var serviceQueueList = _serviceQueueService.Find((_serviceQueueQuery as SIL.ServiceQueueLibrary.DAL.Data.IFilterParameterCollection)).ToList();
                            #endregion

                            if (courtDate != originalCourtDate)
                            {
                                if (serviceQueueList != null && serviceQueueList.Count > 0)
                                {
                                    //delete all old Queue
                                    DiscardQueues(ServiceQueueTypeList.WithdrawSec56WithOfficerError, notIntNo.ToString(), courtDate.AddDays(noDaysForWithDrawnDate + 2));
                                    var serviceQueueOld = _serviceQueueService.GetBySqtIntNoSeQuKeySeQuActionDateSeQuGroup((int)ServiceQueueTypeList.WithdrawSec56WithOfficerError, notIntNo.ToString(), courtDate.AddDays(noDaysForWithDrawnDate).Date, autCode.Trim());
                                    if (serviceQueueOld == null)
                                    {
                                        sendWithdrawSec56WithOfficerErrorQueue(notIntNo, autCode, courtDate, noDaysForWithDrawnDate, queProcessor);
                                    }
                                }
                                else
                                {
                                    sendWithdrawSec56WithOfficerErrorQueue(notIntNo, autCode, courtDate, noDaysForWithDrawnDate, queProcessor);
                                }
                            }
                            else
                            {
                                if (serviceQueueList != null && serviceQueueList.Count == 0)
                                {
                                    sendWithdrawSec56WithOfficerErrorQueue(notIntNo, autCode, courtDate, noDaysForWithDrawnDate, queProcessor);
                                }
                            }
                        }
                        else
                        {
                            sendWithdrawSec56WithOfficerErrorQueue(notIntNo, autCode, courtDate, noDaysForWithDrawnDate, queProcessor);
                        }
                    }
                }
            }
        }

        private void sendWithdrawSec56WithOfficerErrorQueue(int notIntNo, string autCode, DateTime courtDate, int noDaysForWithDrawnDate, QueueItemProcessor queProcessor)
        {
            queProcessor.Send(
                                new QueueItem()
                                {
                                    Body = notIntNo,
                                    Group = autCode.Trim(),
                                    ActDate = courtDate.AddDays(noDaysForWithDrawnDate).Date,
                                    LastUser = AARTOBase.LastUser,
                                    QueueType = ServiceQueueTypeList.WithdrawSec56WithOfficerError
                                }
                             );
        }

        #region Discard Queues

        private int DiscardQueues(ServiceQueueTypeList queueType, string seQukey, DateTime actionDate, DateTime? ReservedActionDate = null)
        {
            string lastUserAndReason = ResourceHelper.GetResource("LastUserSumCourtDateChanged", AARTOBase.LastUser);
            ServiceDB serviceDB_Queue = new ServiceDB(ConfigurationManager.ConnectionStrings["SIL.ServiceQueueLibrary.DAL.Data.ConnectionString"].ConnectionString);
            var paras = new[]
            {
                new SqlParameter("@SQTIntNo", (int)queueType),
                new SqlParameter("@SeQuKey", seQukey.ToString()),
                new SqlParameter("@ActionDate", actionDate),
                new SqlParameter("@ReservedActionDate", ReservedActionDate ?? (object)DBNull.Value),
                new SqlParameter("@LastUser", lastUserAndReason ?? "")
            };

            int result;
            int.TryParse(Convert.ToString(serviceDB_Queue.ExecuteScalar("DiscardOlderActionDateQueues", paras)), out result);

            if (result < 0)
            {
                throw new Exception(ResourceHelper.GetResource("DeleteQueueFailed", queueType.ToString()));
            }

            return result;
        }

        #endregion

        #endregion

        public string GetViolationType(string connString, int frameIntNo)
        {
            string violationType = string.Empty;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand("GetViolationTypeByFrameIntNo", conn) { CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new SqlParameter("@FrameIntNo", frameIntNo) { DbType = DbType.Int32 });
                        if (conn.State != ConnectionState.Open)
                            conn.Open();
                        object value = cmd.ExecuteScalar();
                        if (value != null)
                            violationType = value.ToString().Trim();
                        return violationType;
                    }
                }
                catch (Exception ex)
                {
                    conn.Close();
                    throw ex;
                }
            }
        }

        void WriteErrorMessageIntoDMS(string fieldName, string message, BatchDocumentStatusList baDoStId = BatchDocumentStatusList.ImportFailed)
        {
            //Jerry 2014-03-25 add try catch, if update BatchDocument or DocumentErrorMessage has error, set queue status to UnKnown
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Suppress))
                {
                    DocumentErrorMessage dem = new DocumentErrorMessage();
                    dem.BatchId = batchID;
                    dem.BaDoId = documentID;
                    dem.DoErMeCode = fieldName;
                    dem.DoErMeDateCreated = DateTime.Now;
                    dem.DoErMessage = message;
                    dem.LastUser = AARTOBase.LastUser;

                    new DocumentErrorMessageService().Insert(dem);

                    //Jerry 2014-04-14 change BatchDocumentService to a global variable
                    BatchDocument batchDocument = documentService.GetByBaDoId(documentID);
                    if (batchDocument != null && batchDocument.BaDoStId != (int)BatchDocumentStatusList.ImportFailed)
                    {
                        batchDocument.BaDoStId = (int)baDoStId;
                        batchDocument = documentService.Save(batchDocument);
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrWriteErrorMessageIntoDMS", batchID, documentID, ex.Message)),
                    LogType.Error, ServiceBase.ServiceOption.ContinueButQueueFail, tempQueueItem, QueueItemStatus.UnKnown);
            }
        }

        bool GetTrafficOfficer(string officerNo)
        {
            bool flag = false;
            if (!string.IsNullOrEmpty(officerNo))
            {
                TrafficOfficerService service = new TrafficOfficerService();
                SIL.AARTO.DAL.Entities.TList<TrafficOfficer> officerList = service.GetByToNo(officerNo);
                if (officerList.Count > 0)
                    flag = true;

            }
            return flag;
        }

        static int GetLocationSuburbByDescr(string locDescr, int autIntNo)
        {
            int loSuIntNo = 0;
            LocationSuburbQuery locQuery = new LocationSuburbQuery();
            locQuery.AppendEquals(LocationSuburbColumn.LoSuDescr, locDescr);
            locQuery.Append(LocationSuburbColumn.AutIntNo, autIntNo.ToString());
            LocationSuburbService locService = new LocationSuburbService();
            LocationSuburb locSuburb = locService.Find(locQuery as IFilterParameterCollection).FirstOrDefault();
            if (locSuburb != null)
            {
                loSuIntNo = locSuburb.LoSuIntNo;
            }
            else
            {
                locQuery.Clear();
                locQuery.Append(LocationSuburbColumn.LoSuDescr, "None");
                locQuery.Append(LocationSuburbColumn.AutIntNo, autIntNo.ToString());
                locSuburb = locService.Find(locQuery as IFilterParameterCollection).FirstOrDefault();
                {
                    loSuIntNo = locSuburb == null ? 0 : locSuburb.LoSuIntNo;
                }
            }

            return loSuIntNo;
        }

        bool ValidateCourtNo(string courtNo)
        {
            if (string.IsNullOrWhiteSpace(courtNo))
                return false;

            Court courtEntity = courtService.GetByCrtNo(courtNo);
            if (courtEntity == null)
                return false;

            if (courtEntity.CrtName.Trim().ToLower() == "unknown")
                return false;

            return true;
        }
    }

    public class ImportDataEntity
    {
        public string ClientID { get; set; }
        public string BaDoID { get; set; }
        public string BaDoResult { get; set; }
        public DateTime CaptureDate { get; set; }
        public string DoTeID { get; set; }
        public string DoTeTypeID { get; set; }
        public int IFSIntNo { get; set; }
        public int DataExportMode { get; set; }
    }

}


