﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.HandwrittenImporter
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.HandwrittenImporter"
                ,
                DisplayName = "SIL.AARTOService.HandwrittenImporter"
                ,
                Description = "SIL.AARTOService.HandwrittenImporter"
            };

            ProgramRun.InitializeService(new HandwrittenImporter(), serviceDescriptor, args);
        }
    }
}
