﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;
using SIL.AARTOService.Library;
using SIL.AARTO.BLL.Utility.PrintFile;
using SIL.AARTOService.DAL;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Resource;
using SIL.QueueLibrary;
using SIL.AARTO.BLL.Utility.Printing;
using Stalberg.TMS;
using SIL.ServiceBase;

namespace SIL.AARTOService.PrintSecondAppearanceCCR
{
    public class PrintSecondAppearanceCCRClientService : ServiceDataProcessViaQueue
    {
        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        SIL.AARTOService.Library.AARTORules rules = SIL.AARTOService.Library.AARTORules.GetSingleTon();
        PrintFileProcess process;
        AuthorityDetails authDetails = null;

        string connAARTODB, connUNC_Export;
        string autCode;
        string exportPath;
        int autIntNo, pfnIntNo;
        bool isChecked;
        string emailToAdministrator = string.Empty;
        List<string> fileList = new List<string>();
        DBHelper db;
        int noOfDays = 0;
        string arChargeSheet, arChargeSheetWithPre;

        public PrintSecondAppearanceCCRClientService()
            : base("", "", new Guid("0AFFB4F6-E7C7-40EF-8A27-CDE3E6E59B90"), ServiceQueueLibrary.DAL.Entities.ServiceQueueTypeList.SecondAppearanceCCR)
        {
            this._serviceHelper = new AARTOServiceBase(this, true, 60);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.connUNC_Export = AARTOBase.GetConnectionString(ServiceConnectionNameList.PrintFilesFolder, ServiceConnectionTypeList.UNC);

            AARTOBase.OnServiceStarting = () =>
            {
                AARTOBase.CheckFolder(this.connUNC_Export);
                this.isChecked = false;
                this.autCode = null;
                if (this.fileList.Count > 0)
                    this.fileList.Clear();
            };
            AARTOBase.OnServiceSleeping = () =>
            {
                if (this.fileList.Count > 0)
                {
                    string title = ResourceHelper.GetResource("2ndAppearanceCourtRoll");
                    SendEmailManager.SendEmail(
                        this.emailToAdministrator,
                        ResourceHelper.GetResource("EmailSubjectOfPrintService", title),
                        SendEmailManager.GetFileListContent(title, this.fileList));
                }
            };

            process = new PrintFileProcess(this.connAARTODB, AARTOBase.LastUser);
            process.ErrorProcessing = (s) => { this.Logger.Error(s); };
            db = new DBHelper(this.connAARTODB);
        }

        public override void InitialWork(ref QueueLibrary.QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;
                    SetServiceParameters();
                    if (!ServiceUtility.IsNumeric(body))
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        return;
                    }
                    string autCode = item.Group.Trim();
                    this.pfnIntNo = Convert.ToInt32(body.Trim());
                    rules.InitParameter(this.connAARTODB, autCode);

                    if (this.autCode != autCode)
                    {
                        this.autCode = autCode;
                        QueueItemValidation(ref item);
                    }
                    else if (!this.isChecked)
                    {
                        AARTOBase.ErrorProcessing(ref item);
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                return;
            }
        }

        public override void MainWork(ref List<QueueLibrary.QueueItem> queueList)
        {
            for (int i = 0; i < queueList.Count; i++)
            {

                QueueItem item = queueList[i];
                if (!item.IsSuccessful) continue;
                try
                {
                    PrintCourtRollFileName fnEntity = new PrintCourtRollFileName(this.connAARTODB);
                    fnEntity = fnEntity.DepartName(this.pfnIntNo);
                    if (fnEntity == null)
                    {
                        AARTOBase.ErrorProcessing(ref item, "PrintFileName is invalid");
                        break;
                    }

                    this.exportPath = AARTOBase.GetPrintFileDirectory(this.connUNC_Export, this.autCode, PrintServiceType.CourtRoll, fnEntity.CourtName, fnEntity.CourtDate);
                    if (!AARTOBase.CheckFolder(this.exportPath)) return;

                    DateTime crtDate = Convert.ToDateTime(fnEntity.CourtDate);
                    //DateTime prtDate = Convert.ToDateTime(fnEntity.PrintDate);

                    CourtDB courtDB = new CourtDB(this.connAARTODB);
                    // jake 2013-10-31 change this function from CourtDateByCourt to CourtDateByCourtForWarrant
                    DateTime dt = courtDB.CourtDateByCourtForWarrant(fnEntity.CrtIntNo, crtDate);
                    if (dt.CompareTo(new DateTime(2000, 1, 1)) <= 0)
                    {
                        AARTOBase.ErrorProcessing(ref item, string.Format("Invalid court date for court : {0}", fnEntity.CourtDate));
                        break;
                    }

                    string reportType = "P";
                    dt = dt.AddDays(noOfDays);
                    if (dt.CompareTo(DateTime.Now) <= 0)
                        reportType = "F";

                    string courtRuleValue = rules.Rule(fnEntity.CrtIntNo, "1010").CRString;
                    if (courtRuleValue == "Y")
                    {
                        BuildPrintFile(ref item, fnEntity, CourtRollType.Summary, reportType, "0");   // 0 is All

                        if (reportType == "F")
                            BuildPrintFile(ref item, fnEntity, CourtRollType.Label, null, "0");

                        if (this.arChargeSheet == "Y"
                            && (reportType != "P" || (reportType == "P" && this.arChargeSheetWithPre == "Y")))
                            BuildPrintFile(ref item, fnEntity, CourtRollType.ChargeSheet, reportType, "0");
                    }
                    else
                    {
                        BuildPrintFile(ref item, fnEntity, CourtRollType.Summary, reportType, "1");   // 1 is CAM
                        BuildPrintFile(ref item, fnEntity, CourtRollType.Summary, reportType, "2");  // 2 is HWO

                        if (reportType == "F")
                        {
                            BuildPrintFile(ref item, fnEntity, CourtRollType.Label, null, "1");
                            BuildPrintFile(ref item, fnEntity, CourtRollType.Label, null, "2");
                        }

                        if (this.arChargeSheet == "Y"
                            && (reportType != "P" || (reportType == "P" && this.arChargeSheetWithPre == "Y")))
                        {
                            BuildPrintFile(ref item, fnEntity, CourtRollType.ChargeSheet, reportType, "1");
                            BuildPrintFile(ref item, fnEntity, CourtRollType.ChargeSheet, reportType, "2");
                        }
                    }

                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ServiceHasError"), AARTOBase.LastUser, item.Body, ex.Message), true);
                }
                queueList[i] = item;
            }
        }


        private void SetServiceParameters()
        {
            if (ServiceParameters != null
                && string.IsNullOrEmpty(this.emailToAdministrator)
                && ServiceParameters.ContainsKey("EmailToAdministrator")
                && !string.IsNullOrEmpty(ServiceParameters["EmailToAdministrator"]))
                this.emailToAdministrator = ServiceParameters["EmailToAdministrator"];
            if (!SendEmailManager.CheckEmailAddress(this.emailToAdministrator))
                AARTOBase.ErrorProcessing(ResourceHelper.GetResource("EmailAddressError"), true);
        }

        private void BuildPrintFile(ref QueueItem item, PrintCourtRollFileName fnEntity, CourtRollType crType, string reportType, string courtRollType)
        {
            if (crType == CourtRollType.Summary)
                fnEntity.Prefix = "2ndCR";
            else if (crType == CourtRollType.Label)
                fnEntity.Prefix = "2ndCRLabels";
            else if (crType == CourtRollType.ChargeSheet)
                fnEntity.Prefix = "2ndCRChargeSheet";

            if (courtRollType == "0")
                fnEntity.Suffix = "";
            else if (courtRollType == "1")
                fnEntity.Suffix = "Camera";
            else if (courtRollType == "2")
                fnEntity.Suffix = "HWO";

            //process.ExportPrintFilePath = this.exportPath + "\\" + fnEntity.CombineName();
            process.BuildPrintFile(
                new PrintFileModuleSecondAppearanceCCR(crType, reportType, fnEntity.CrtIntNo.ToString(), fnEntity.CrtRIntNo.ToString(), fnEntity.CourtDate, fnEntity.PrintDate, courtRollType),
                this.autIntNo, this.pfnIntNo, this.exportPath, fnEntity.CombineName());

            if (process.IsSuccessful)
            {
                this.fileList.Add(process.ExportPrintFileName);
                this.Logger.Info(ResourceHelper.GetResource("PrintFileSavedTo", process.PrintFileName, process.ExportPrintFilePath));
            }
            else
                AARTOBase.ErrorProcessing(ref item);
        }

        private void QueueItemValidation(ref QueueItem item)
        {
            this.isChecked = false;

            this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim().Equals(this.autCode, StringComparison.OrdinalIgnoreCase));
            if (this.authDetails != null && this.authDetails.AutIntNo > 0)
            {
                this.autIntNo = this.authDetails.AutIntNo;

                this.noOfDays = rules.Rule("CDate", "FinalCourtRoll").DtRNoOfDays;
                this.arChargeSheet = rules.Rule("6205").ARString;
                this.arChargeSheetWithPre = rules.Rule("6208").ARString;

                this.isChecked = true;
            }
            else
            {
                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.autCode), false, QueueItemStatus.Discard);
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.autCode));
                return;
            }
        }


    }
}
