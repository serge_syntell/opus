﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PrintSecondAppearanceCCR
{
    public partial class PrintSecondAppearanceCCR : ServiceHost
    {
        public PrintSecondAppearanceCCR()
            : base("", new Guid("C316EEC9-D6AF-4F4C-8FD4-7A29847D857C"), new PrintSecondAppearanceCCRClientService())
        {
            InitializeComponent();
        }

        
    }
}
