﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.ExportToThabo
{
    partial class ExportToThabo : ServiceHost
    {
        public ExportToThabo()
            : base("", new Guid("B09A16E0-C736-4EFE-BC10-849E6B7AF528"), new ExportToThaboService())
        {
            InitializeComponent();
        }
    }
}
