﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.ExportToThabo
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.ExportToThabo"
                ,
                DisplayName = "SIL.AARTOService.ExportToThabo"
                ,
                Description = "SIL AARTOService ExportToThabo"
            };

            ProgramRun.InitializeService(new ExportToThabo(), serviceDescriptor, args);
        }
    }
}
