﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTOService.Library;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using Stalberg.Indaba;
using SIL.AARTOService.Resource;
using Stalberg.TMS;
using System.Data.SqlClient;
using Stalberg.TMS_TPExInt.Objects;
using System.IO;
using SIL.ServiceLibrary;
using SIL.ServiceBase;

namespace SIL.AARTOService.ExportToThabo
{
    public class ExportToThaboService : ClientService
    {
        public ExportToThaboService()
            : base("", "", new Guid("A867841C-D348-4230-86D9-ED97B03E37D3"))
        {
            this._serviceHelper = new AARTOServiceBase(this);
            aartoConnectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            indabaConnectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.Indaba, ServiceConnectionTypeList.DB);
            exportPath = AARTOBase.GetConnectionString(ServiceConnectionNameList.ExportToThaboExportPath, ServiceConnectionTypeList.UNC);

            AARTOBase.OnServiceStarting = () =>
            {
                if (string.IsNullOrEmpty(exportPath))
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoParam"), "ExportToThaboExportPath"), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }

                var exportPathFileDir = new DirectoryInfo(exportPath);
                if (!exportPathFileDir.Exists)
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoExportFilePath"), exportPathFileDir.FullName), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }

                InitIndaba();
                easyPayDB = new EasyPayDB(aartoConnectStr);
            };

            AARTOBase.OnServiceSleeping = () =>
            {
                DisposeIndaba();
            };
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        string aartoConnectStr = string.Empty;
        string indabaConnectStr = string.Empty;
        string _machineName = string.Empty;
        string exportPath = string.Empty;

        ICommunicate indaba;
        EasyPayDB easyPayDB;

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            bool failed = this.SendHubData();
            if (failed)
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("ErrProcessingAborted"), LogType.Error, ServiceOption.BreakAndStop);
            }
            else
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("InfoFinishedTransaction"), LogType.Info);
            }

            //Jerry 2014-05-23 add
            this.MustSleep = true;
        }

        private bool SendHubData()
        {
            // Retrieve any new data to send and put it into files
            AARTOBase.LogProcessing(ResourceHelper.GetResource("InfoCreateFiles"), LogType.Info);

            bool failed = this.CreateExportFiles();
            if (failed)
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("ErrExportFiles"), LogType.Error);
                return failed;
            }
            else
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("InfoFinishedExport"), LogType.Info);
            }

            // Clean up expired EasyPayTran Data
            AARTOBase.LogProcessing(ResourceHelper.GetResource("InfoCleanupExpired"), LogType.Info);

            this.CleanupExpiredEasyPayTransactions();
            AARTOBase.LogProcessing(ResourceHelper.GetResource("InfoFinishedCleanupExpired"), LogType.Info);

            // Send any new or unsent export files
            AARTOBase.LogProcessing(ResourceHelper.GetResource("InfoCheckFilesToSend"), LogType.Info);
            failed = this.CheckForFilesToSend();
            if (failed)
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("ErrCheckFilesToSend"), LogType.Error);
                return failed;
            }
            else
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("InfoFinishedCheckFilesToSend"), LogType.Info);
            }

            return failed;
        }

        private bool CreateExportFiles()
        {
            bool failed = false;

            try
            {
                List<EasyPayAuthority> authorities = this.GetAuthorities();
                Guid id;
                string fileName;

                foreach (EasyPayAuthority authority in authorities)
                {
                    int noRecords;

                    do
                    {
                        // Create the file name
                        fileName = string.Format("{0}_{1}_{2}{3}",
                            FineExchangeDataFile.FILE_PREFIX,
                            authority.Name.Replace(" ", "_"),
                            DateTime.Now.ToString(FineExchangeDataFile.DATE_FORMAT),
                            FineExchangeDataFile.EXTENSION);

                        // Collect data from local resources
                        noRecords = this.GetFineData(authority, fileName);
                        if (noRecords > 0)
                        {
                            // Check for EasyPay data extraction
                            if (authority.IsEasyPay)
                                this.GetEasyPayData(authority, fileName);

                            // Create the FEDF file
                            try
                            {
                                FineExchangeDataFile file = new FineExchangeDataFile(authority,
                                    _machineName,
                                    exportPath,
                                    fileName);
                                file.MachineName = _machineName;
                                file.Write();

                                // Send to Indaba
                                id = this.indaba.Enqueue(file.FileName);

                                // Check it was enqueued successfully
                                if (!id.Equals(Guid.Empty))
                                {
                                    // Update the sent status 
                                    easyPayDB.UpdateSentStatus(Path.GetFileName(file.FileName), AARTOBase.LastUser);

                                    // Delete the file
                                    File.Delete(file.FileName);
                                }
                                else
                                {
                                    //091203 FT Update the sent status 
                                    easyPayDB.ResetSentStatus(fileName, AARTOBase.LastUser);
                                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrFileToIndaba"), fileName), LogType.Error);

                                    failed = true;
                                    return failed;
                                }
                            }
                            catch (Exception ex)
                            {
                                easyPayDB.ResetSentStatus(fileName, AARTOBase.LastUser);
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrCreateFEDFFiles"), ex.Message, ex.StackTrace), LogType.Error, ServiceOption.BreakAndStop);
                                failed = true;
                                return failed;
                            }
                        }
                    }
                    while (noRecords > 0);
                }
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrCreateExportFiles"), ex.Message, ex.StackTrace), LogType.Error, ServiceOption.BreakAndStop);
                failed = true;
                return failed;
            }

            return failed;
        }

        private List<EasyPayAuthority> GetAuthorities()
        {
            List<EasyPayAuthority> authorities = new List<EasyPayAuthority>();

            SqlDataReader reader = easyPayDB.GetAuthoritiesForExport();
            while (reader.Read())
            {
                EasyPayAuthority authority = new EasyPayAuthority();

                // Authority Data
                authority.ID = (int)reader["AutIntNo"];
                authority.Name = (string)reader["AutName"];
                authority.AuthorityCode = ((string)reader["AutCode"]).Trim();
                authority.AuthorityNumber = ((string)reader["AutNo"]).Trim();
                if (reader["EasyPayReceiptID"] != DBNull.Value)
                    authority.ReceiverIdentifier = (int)reader["EasyPayReceiptID"];
                // Metro Data
                authority.MetroCode = ((string)reader["MtrCode"]).Trim();
                authority.MetroName = (string)reader["MtrName"];
                // Region Data
                authority.RegionCode = ((string)reader["RegCode"]).Trim();
                authority.RegionName = (string)reader["RegName"];

                _machineName = (string)reader["MachineName"];

                authorities.Add(authority);
            }
            reader.Close();

            return authorities;
        }

        private int GetFineData(EasyPayAuthority authority, string fileName)
        {
            authority.Fines.Clear();
            int noRecords;

            SqlDataReader reader = easyPayDB.GetExportData(authority.ID, fileName, AARTOBase.LastUser);
            while (reader.Read())
            {
                EasyPayFineData fine = new EasyPayFineData(authority);
                fine.NoticeID = (int)reader["NotIntNo"];
                fine.FilmNumber = this.SafeStringReader(reader, "NotFilmNo");
                fine.FineAmount = decimal.Parse(reader["ChgRevFineAmount"].ToString());
                fine.FrameNumber = this.SafeStringReader(reader, "NotFrameNo");
                fine.Location = this.SafeStringReader(reader, "NotLocDescr");
                fine.OffenceDate = (DateTime)reader["NotOffenceDate"];
                fine.OwnerID = this.SafeStringReader(reader, "IDNumber");
                fine.OwnerName = string.Format("{0} {1}", reader["Initial"].ToString().Trim(), reader["Surname"].ToString().Trim()).Trim();
                if (reader["NotPaymentDate"] != DBNull.Value)
                    fine.PaymentDate = (DateTime)reader["NotPaymentDate"];
                fine.RegNo = this.SafeStringReader(reader, "NotRegNo");
                fine.Status = (int)reader["ChargeStatus"];
                fine.StatusDescription = this.SafeStringReader(reader, "CSDescr");
                if (reader["NotTicketNo"] == DBNull.Value || reader["NotTicketNo"].ToString().Length == 0)
                    continue;

                fine.TicketNumber = this.SafeStringReader(reader, "NotTicketNo");
                fine.CameraSerialNumber = this.SafeStringReader(reader, "NotCamSerialNo");
                fine.OffenceCode = this.SafeStringReader(reader, "ChgOffenceCode");
                fine.OffenceDescription = this.SafeStringReader(reader, "ChgOffenceDescr");
                fine.Speed1 = (int)reader["NotSpeed1"];
                fine.Speed2 = (int)reader["NotSpeed2"];
                fine.SpeedZone = (int)reader["NotSpeedLimit"];
                if (reader["ChgNoAOG"] != DBNull.Value && reader["ChgNoAOG"].ToString().Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                    fine.IsNoAOG = 'Y';

                authority.Fines.Add(fine);
            }

            // Get the number of records in the result set
            reader.NextResult();
            if (reader.Read())
                noRecords = reader.GetInt32(0);
            else
                noRecords = 0;

            reader.Close();

            if (noRecords > 0)
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("InfoExportToThaboFineDataRecord"), noRecords, authority), LogType.Info);

            return noRecords;
        }

        private void GetEasyPayData(EasyPayAuthority authority, string fileName)
        {
            SqlDataReader reader = easyPayDB.GetEasyPayTransactions(authority.ID, fileName);
            while (reader.Read())
            {
                EasyPayTransaction tran = new EasyPayTransaction(authority);
                tran.EasyPayAction = ((string)reader["EPAction"])[0];
                tran.EasyPayNumber = ((string)reader["EasyPayNumber"]).Trim();
                tran.ExpiryDate = (DateTime)reader["EPExpiryDate"];
                tran.NoticeID = (int)reader["NotIntNo"];
                tran.Amount = (decimal)reader["EPAmount"];

                authority.EasyPayTransactions.Add(tran);
            }
            reader.Close();
        }

        private string SafeStringReader(SqlDataReader reader, string columName)
        {
            int index = reader.GetOrdinal(columName);
            if (reader.IsDBNull(index))
                return string.Empty;

            string value = reader.GetString(index);
            if (value.Contains(Environment.NewLine))
                throw new ApplicationException(string.Format(ResourceHelper.GetResource("ErrExportToThaboNewLine"), columName, value.Trim()));

            return value.Trim();
        }

        private void CleanupExpiredEasyPayTransactions()
        {
            try
            {
                easyPayDB.CleanupExpiredEasyPayTransactions();
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrClearupExpiredEasyPay"), ex.Message), LogType.Error, ServiceOption.BreakAndStop);
            }
        }

        private bool CheckForFilesToSend()
        {
            bool failed = false;
            try
            {
                DirectoryInfo dir = new DirectoryInfo(exportPath);
                if (!dir.Exists)
                {
                    failed = true;
                    return failed;
                }

                Guid id;
                foreach (FileInfo file in dir.GetFiles("*" + FineExchangeDataFile.EXTENSION))
                {
                    try
                    {
                        id = this.indaba.Enqueue(file);
                        if (!id.Equals(Guid.Empty))
                        {
                            //091201 FT Update the sent status 
                            easyPayDB.UpdateSentStatus(Path.GetFileName(file.Name), AARTOBase.LastUser);

                            file.Delete();
                        }
                        else
                        {
                            //091203 FT Update the sent status
                            easyPayDB.ResetSentStatus(file.Name, AARTOBase.LastUser);
                            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrFileToIndaba"), file.Name), LogType.Error);
                            failed = true;
                            return failed;
                        }
                    }
                    catch (Exception ex)
                    {
                        AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrAddFileToIndabaCatch"), file.Name, ex.Message, ex.StackTrace), LogType.Error, ServiceOption.BreakAndStop);
                        failed = true;
                        return failed;
                    }
                }
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrCheckForFilesToSend"), ex.Message, ex.StackTrace), LogType.Error, ServiceOption.BreakAndStop);
                failed = true;
                return failed;
            }

            return failed;
        }

        public void InitIndaba()
        {
            Client.ApplicationName = "SIL.AARTOService.ExportToThabo";
            Client.ConnectionString = this.indabaConnectStr;
            Client.Initialise();
            indaba = Client.Create();
        }

        public void DisposeIndaba()
        {
            indaba.Dispose();
            Client.Dispose();
        }

    }
}
