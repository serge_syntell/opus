﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.DocumentStatistics
{
    public partial class DocumentStatistics : ServiceHost
    {
        public DocumentStatistics()
            : base("", new Guid("CF73071D-85AD-46BA-9C1A-8F86E51C7965"), new DocumentStatisticsClientService())
        {
            InitializeComponent();
        }
    }
}
