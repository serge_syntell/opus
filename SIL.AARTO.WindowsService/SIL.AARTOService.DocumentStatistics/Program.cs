﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.DocumentStatistics
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.DocumentStatistics"
                ,
                DisplayName = "SIL.AARTOService.DocumentStatistics"
                ,
                Description = "SIL.AARTOService.DocumentStatistics"
            };

            ProgramRun.InitializeService(new DocumentStatistics(), serviceDescriptor, args);
        }
    }
}
