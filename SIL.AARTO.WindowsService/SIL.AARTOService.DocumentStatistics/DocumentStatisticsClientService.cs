﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.QueueLibrary;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;

namespace SIL.AARTOService.DocumentStatistics
{
    public class DocumentStatisticsClientService : ClientService
    {
        #region 2013-09-30, Oscar rewrote and backup

        //public DocumentStatisticsClientService()
        //    : base("DocumentStatisticsClientService", "", new Guid("BA17BD7B-3E41-4F5C-BB8D-C81007C7BD00"))
        //{
        //    this._serviceHelper = new AARTOServiceBase(this, false);
        //}

        //AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)this._serviceHelper; } }
        //ServiceDB SerDB;
        //public override void InitialWork(ref QueueLibrary.QueueItem item)
        //{
        //    try
        //    {
        //        string conStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.DMS, ServiceConnectionTypeList.DB);
        //        SerDB = new ServiceDB(conStr);
        //        AARTOBase.OnServiceSleeping = () => { Logger.Info("Main work end."); };
        //    }
        //    catch (Exception ex)
        //    {
        //        AARTOBase.ErrorProcessing(string.Format("Initial DMS DB error:\r\n{0}", ex.ToString()), true);
        //    }
        //}

        //public override void MainWork(ref List<QueueLibrary.QueueItem> queueList)
        //{
        //    Logger.Info("Main work start......");
        //    try
        //    {
        //        using (TransactionScope t = new TransactionScope())
        //        {
        //            SerDB.ExecuteNonQuery("DashboardTransaction_MarkFlag");
        //            t.Complete();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        AARTOBase.ErrorProcessing(string.Format("First step process flag 0 to 1 error:\r\n{0}",ex.ToString()), true);
        //    }

        //    try
        //    {
        //        using (TransactionScope t = new TransactionScope())
        //        {
        //            SerDB.ExecuteNonQuery("DashboardTransaction_Write");
        //            t.Complete();
        //            this.MustSleep = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        AARTOBase.ErrorProcessing(string.Format("Second step write statistics error:\r\n{0}", ex.ToString()), true);
        //    }
        //}

        #endregion

        readonly ServiceDB db;
        readonly string lastUser;
        int? processRowCount;

        public DocumentStatisticsClientService()
            : base("DocumentStatisticsClientService", "", new Guid("BA17BD7B-3E41-4F5C-BB8D-C81007C7BD00"))
        {
            _serviceHelper = new AARTOServiceBase(this, false);
            this.lastUser = AARTOBase.LastUser;
            var conStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.DMS, ServiceConnectionTypeList.DB);
            this.db = new ServiceDB(conStr);
            AARTOBase.OnServiceStarting = GetParameters;
        }

        AARTOServiceBase AARTOBase
        {
            get { return (AARTOServiceBase)_serviceHelper; }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            Merge();
            MustSleep = true;
        }

        void Merge()
        {
            var sum = 0;
            var errors = new List<string>();
            var rowCount = this.processRowCount.GetValueOrDefault(100);
            while (true)
            {
                int processed;
                try
                {
                    using (var scope = ServiceUtility.CreateTransactionScope())
                    {
                        var obj = this.db.ExecuteScalar("DocumentStatisticsCounting", new[]
                        {
                            new SqlParameter("@ProcessRowCount", rowCount),
                            new SqlParameter("@LastUser", this.lastUser)
                        }, true, 0);
                        processed = Convert.ToInt32(obj);

                        if (processed < 0)
                        {
                            switch (processed)
                            {
                                case -1:
                                    errors.Add(ResourceHelper.GetResource("FailedInPreparingData"));
                                    break;
                                case -2:
                                    errors.Add(ResourceHelper.GetResource("FailedInLockingData"));
                                    break;
                                case -3:
                                    errors.Add(ResourceHelper.GetResource("FailedInMergingData"));
                                    break;
                                case -4:
                                    errors.Add(ResourceHelper.GetResource("FailedInUpdatingDTSumDate"));
                                    break;
                            }
                            break;
                        }

                        if (processed >= 0
                            && ServiceUtility.TransactionCanCommit())
                            scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    errors.Add(ex.ToString());
                    break;
                }

                if (processed > 0) sum += processed;
                if (processed < rowCount) break;
            }

            if (sum > 0 || errors.Count <= 0)
                AARTOBase.LogProcessing(ResourceHelper.GetResource("SuccessfullyMergedRowsToDocumentStatistics", sum), LogType.Info);

            if (errors.Count > 0)
            {
                errors.ForEach(e => AARTOBase.LogProcessing(e, LogType.Error, ServiceOption.Break));
                errors.Clear();
            }
        }

        void GetParameters()
        {
            string rowCount;
            int number;
            if (!this.processRowCount.HasValue
                && !string.IsNullOrWhiteSpace(rowCount = AARTOBase.GetServiceParameter("ProcessRowCount"))
                && int.TryParse(rowCount, out number))
                this.processRowCount = number;
            AARTOBase.LogProcessing(ResourceHelper.GetResource("CurrentProcessRowCount", this.processRowCount.GetValueOrDefault(100)), LogType.Info);
        }
    }
}
