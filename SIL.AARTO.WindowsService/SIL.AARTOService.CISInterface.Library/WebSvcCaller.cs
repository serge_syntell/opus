﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace SIL.AARTOService.CISInterface.Library
{
    public class WebSvcCaller
    {
        public static XmlDocument QueryPostWebService(string serviceUrl, string funName, string soap, bool security)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(serviceUrl);
            request.Headers.Add("SOAPAction", string.Format("\"http://tempuri.org/ICurator/{0}\"", funName));
            request.ContentType = "text/xml;charset=\"utf-8\"";
            request.Accept = "text/xml";
            request.Method = "POST";
            request.Timeout = 180 * 1000;

            using (Stream stm = request.GetRequestStream())
            {
                using (StreamWriter stream = new StreamWriter(stm))
                {
                    stream.Write(soap);
                }
            }
            if (security)
            {
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(allowCert);
                //request.ClientCertificates.Add(X509Certificate.CreateFromCertFile(ConfigurationManager.AppSettings["CertFilePath"]));
            }

            WebResponse response = request.GetResponse();

            XmlDocument doc = ReadXmlResponse(response);
            return doc;
        }

        private static XmlDocument ReadXmlResponse(WebResponse response)
        {
            StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
            String retXml = sr.ReadToEnd();
            sr.Close();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(retXml);
            return doc;
        }

        private static bool allowCert(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors error)
        {
            var request = sender as HttpWebRequest;
            return true;
        }
    }
}