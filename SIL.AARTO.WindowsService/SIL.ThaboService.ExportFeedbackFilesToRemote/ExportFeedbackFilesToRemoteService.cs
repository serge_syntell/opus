﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using SIL.AARTOService.DAL;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.QueueLibrary;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using Stalberg.Indaba;
using Stalberg.ThaboAggregator.Objects;

namespace SIL.ThaboService.ExportFeedbackFilesToRemote
{
    public class ExportFeedbackFilesToRemoteService : ClientService
    {
        public ExportFeedbackFilesToRemoteService()
            : base("", "", new Guid("56814453-C78F-4CFC-B553-336FE3460E6A"))
        {
            this._serviceHelper = new AARTOServiceBase(this);
            thaboConnectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.Thabo, ServiceConnectionTypeList.DB);
            indabaConnectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.CentralIndaba, ServiceConnectionTypeList.DB);
            feedbackPath = AARTOBase.GetConnectionString(ServiceConnectionNameList.FeedbackPath, ServiceConnectionTypeList.UNC);
            ServiceUtility.InitializeNetTier(thaboConnectStr);

            AARTOBase.OnServiceStarting = () =>
            {
                if (string.IsNullOrEmpty(feedbackPath))
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoParam"), "FeedbackPath"), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }

                var fbPathFileDir = new DirectoryInfo(feedbackPath);
                if (!fbPathFileDir.Exists)
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoFeedbackFilePath"), fbPathFileDir.FullName), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }

                InitIndaba();
            };

            AARTOBase.OnServiceSleeping = () =>
            {
                DisposeIndaba();
            };
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        string thaboConnectStr = string.Empty;
        string indabaConnectStr = string.Empty;
        string feedbackPath = string.Empty;

        ICommunicate indaba;

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            // Generate payment feedback files to send back to individual LAs
            AARTOBase.LogProcessing(ResourceHelper.GetResource("GenerateAndExportFeedback"), LogType.Info);
            GenerateExportFeedbackFiles();
            AARTOBase.LogProcessing(ResourceHelper.GetResource("CompleteFeedback"), LogType.Info);

            // Cleanup expired EasyPay Tran data
            AARTOBase.LogProcessing(ResourceHelper.GetResource("CleanEasyPayTran"), LogType.Info);
            CleanupEasyPayTran();
            AARTOBase.LogProcessing(ResourceHelper.GetResource("FinishedCleanEasyPayTran"), LogType.Info);

            //Jerry 2014-05-23 add
            this.MustSleep = true;
        }

        private void GenerateExportFeedbackFiles()
        {
            // Get the list of authorities with feedback data to send
            List<RemoteAuthority> authorities = new List<RemoteAuthority>();
            GetRemoteAuthoritiesForExport(authorities);

            List<FineExchangeFeedbackFile> files = new List<FineExchangeFeedbackFile>();
            foreach (RemoteAuthority authority in authorities)
            {
                //Logger.Write(string.Format("Processing data for {0} ...", authority.Name), "General", (int)TraceEventType.Information);
                AARTOBase.LogProcessing(ResourceHelper.GetResource("ProcessDataForAuth", authority.Name), LogType.Info);

                try
                {
                    // Make sure the authority has a valid machine name
                    if (authority.MachineName.Length == 0)
                    {
                        //Logger.Write(string.Format("{0} does not have a machine name to send responses back to!", authority.Name), "General", (int)TraceEventType.Warning);
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("AuthNoMachineName", authority.Name), LogType.Warning);
                        continue;
                    }

                    // Process data into feedback files
                    var file = new FineExchangeFeedbackFile(authority);
                    file.WriteHeader(feedbackPath);

                    //2013-11-26 - Please note: This value is never set and this code will never be processed. Suspect that this relates to EasyPay Holdback/Error files ("HB*"). 
                    //If this code is ever implemented, the corresponding code in Thabo Exporter import feedback files will also need to be checked
                    //if (authority.IsEasyPay)
                    //    GetEasyPayAuthorityFeedbackData(file);
                    GetPaymentInformation(file);

                    file.Close();

                    files.Add(file);
                    //Logger.Write(string.Format("\tCreated Feedback File: {0}", Path.GetFileName(file.FileName)), "General", (int)TraceEventType.Information);
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("CreateFeedbackFile", Path.GetFileName(file.FileName)), LogType.Info);

                    DeletePaymentForExport(file);
                }
                catch (Exception ex)
                {
                    //Logger.Write("ERROR creating feedback files." + ex.Message, "General", (int)TraceEventType.Critical);
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("ErrCreateFeedback", ex.Message), LogType.Error);
                }
            }

            // Send all these files
            if (files.Count > 0)
                SendFeedbackFiles(files);

        }

        private void CleanupEasyPayTran()
        {
            try
            {
                int recordsDeleted = DeleteExpiredEasyPayTranRecords(DateTime.Today.AddDays(-1));
                AARTOBase.LogProcessing(ResourceHelper.GetResource("RowsDelete", recordsDeleted), LogType.Info);
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex.Message, LogType.Error);
            }
        }

        private void SendFeedbackFiles(IEnumerable<FineExchangeFeedbackFile> files)
        {
            foreach (FineExchangeFeedbackFile file in files)
            {
                try
                {
                    // Set the file's destination and send it
                    //Program.indaba.Destination = file.Authority.MachineName;
                    //Added by Oscar 2011/09/29
                    //In order to keep the Indaba interface unchanged,we decided to pass client ID as the parameter to take the place of IndabaDestination.
                    //Code changes will happen in Indaba client to look up  the destination site based on  the passed client ID.
                    indaba.Destination = file.Authority.AuthorityCode;

                    var fi = new FileInfo(file.FileName);
                    Guid id = indaba.Enqueue(fi.OpenRead(), fi.Name);
                    fi = null;

                    // Make sure there were no problems adding the file to Indaba
                    if (id.Equals(Guid.Empty))
                        //Logger.Write(string.Format("There was a problem enqueueing the file {0}", file.FileName), "General", (int)TraceEventType.Warning);
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("EnqueueHasProblem", file.FileName), LogType.Warning);
                    else
                        SetFileSent(file);

                    // Cleanup the file
                    file.Dispose();

                    //Logger.Write(string.Format("Sent {0} to {1} ({2}).", file.FileName, file.Authority.MachineName, id.ToString()), "General", (int)TraceEventType.Information);
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("SendToIndaba", file.FileName, file.Authority.MachineName, id.ToString()), LogType.Info);
                }
                catch (Exception ex)
                {
                    //Logger.Write(string.Format("Error Sending Feedback file {0} to {1}.\n{2}\n{3}", file.FileName, file.Authority.MachineName, ex.Message, ex.StackTrace), "General", (int)TraceEventType.Critical);
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("ErrSendFeedback", file.FileName, file.Authority.MachineName, ex.Message, ex.StackTrace), LogType.Error);
                }
            }
        }

        /// <summary>
        /// Gets the list of remote authorities that have feedback data for export.
        /// </summary>
        /// <returns>A list of <see cref="RemoteAuthority"/> objects.</returns>
        public void GetRemoteAuthoritiesForExport(List<RemoteAuthority> authorities)
        {
            //Logger.Write("Getting EasyPay Authority Feedback ...", "General", (int)TraceEventType.Information);
            AARTOBase.LogProcessing(ResourceHelper.GetResource("GetAuthFeedback"), LogType.Info);
            SqlConnection con = new SqlConnection(this.thaboConnectStr);
            using (var com = new SqlCommand("EasyPayAuthorityFeedback_WS", con))
            {
                com.CommandTimeout = 180;
                com.CommandType = CommandType.StoredProcedure;
                try
                {
                    con.Open();
                    //Logger.Write("Connection opened ...", "General", (int)TraceEventType.Information);
                    SqlDataReader reader = com.ExecuteReader();
                    //Logger.Write("Reader opened ...", "General", (int)TraceEventType.Information);
                    while (reader.Read())
                    {
                        RemoteAuthority authority = new RemoteAuthority();
                        authority.ID = (int)reader["AutIntNo"];
                        authority.Name = (string)reader["AutName"];
                        authority.AuthorityCode = reader["AutCode"].ToString().Trim();
                        if (reader["MachineName"] != DBNull.Value)
                            authority.MachineName = (string)reader["MachineName"];

                        authorities.Add(authority);
                    }
                    reader.Close();
                    reader.Dispose();
                }
                catch (Exception ex)
                {
                    AARTOBase.LogProcessing(ex.Message, LogType.Error);
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }

            //Logger.Write("EasyPay Authorities retrieved", "General", (int)TraceEventType.Information);
            AARTOBase.LogProcessing(ResourceHelper.GetResource("EasyPayAuthRetrieved"), LogType.Info);
        }

        /// <summary>
        /// Gets EasyPay feedback data for an authority, writing it directly into a feedback file.
        /// </summary>
        /// <param name="file">The file.</param>
        public void GetEasyPayAuthorityFeedbackData(FineExchangeFeedbackFile file)
        {
            SqlConnection con = new SqlConnection(this.thaboConnectStr);
            using (var com = new SqlCommand("EasyPayAuthorityFeedbackData", con))
            {
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = file.Authority.ID;

                try
                {
                    con.Open();
                    SqlDataReader reader = com.ExecuteReader();
                    while (reader.Read())
                        file.WriteError((string)reader["EasyPayNo"], (string)reader["EasyPayError"]);
                    reader.Close();
                    reader.Dispose();
                }
                catch (Exception ex)
                {
                    AARTOBase.LogProcessing(ex.Message, LogType.Error);
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
                
            }
        }

        //mrs 20081002 3970 added transType as we will have to test for pending in future
        public void GetPaymentInformation(FineExchangeFeedbackFile file)
        {
            SqlConnection con = new SqlConnection(this.thaboConnectStr);

            //mrs 20081002 3970 added transType as we will have to test for pending in future
            SqlCommand com = new SqlCommand("PaymentsUpdates_WS", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("AutIntNo", SqlDbType.Int, 4).Value = file.Authority.ID;

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    file.WriterPaymentFeedback(
                        (string)reader["TicketNo"],
                        (string)reader["PartnerID"],
                        (string)reader["OperatorName"],
                        (DateTime)reader["PaymentDate"],
                        (decimal)reader["Amount"],
                        (DateTime)reader["MsgDateTime"],
                        (byte)reader["PaymentAction"],
                        (string)reader["TransType"]);
                }
                reader.Close();
                reader.Dispose();
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex.Message, LogType.Error);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public void DeletePaymentForExport(FineExchangeFeedbackFile file)
        {
            try
            {
                DBHelper db = new DBHelper(this.thaboConnectStr);
                SqlParameter[] paras = new SqlParameter[1];
                paras[0] = new SqlParameter("@AutIntNo", file.Authority.ID);
                string errMsg = string.Empty;
                db.ExecuteNonQuery("DeletePaymentForExport", paras, out errMsg);
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex.Message, LogType.Error);
            }
        }

        /// <summary>
        /// Sets the file sent status.
        /// </summary>
        /// <param name="file">The file.</param>
        public void SetFileSent(FineExchangeFeedbackFile file)
        {
            try
            {
                DBHelper db = new DBHelper(this.thaboConnectStr);
                SqlParameter[] paras = new SqlParameter[1];
                paras[0] = new SqlParameter("@AutIntNo", file.Authority.ID);
                string errMsg = string.Empty;
                db.ExecuteNonQuery("PaymentsUpdateSent", paras, out errMsg);
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex.Message, LogType.Error);
            }
        }

        /// <summary>
        /// Deletes the expired easy pay tran records.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns>The number of records deleted</returns>
        public int DeleteExpiredEasyPayTranRecords(DateTime dateTime)
        {
            try
            {
                DBHelper db = new DBHelper(this.thaboConnectStr);
                SqlParameter[] paras = new SqlParameter[1];
                paras[0] = new SqlParameter("@ExpiredDate", dateTime);
                string errMsg = string.Empty;
                int recordsDeleted = Convert.ToInt32(db.ExecuteScalar("EasyPayTranDeleteExpired", paras, out errMsg));
                return recordsDeleted;
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex.Message, LogType.Error);
                return -1;
            }
        }

        public void InitIndaba()
        {
            Client.ApplicationName = "SIL.ThaboService.ExportFeedbackFilesToRemote";
            Client.ConnectionString = this.indabaConnectStr;
            Client.Initialise();
            indaba = Client.Create();
            indaba.Source = "Thabo";
        }

        public void DisposeIndaba()
        {
            indaba.Dispose();
            Client.Dispose();
        }
    }
}
