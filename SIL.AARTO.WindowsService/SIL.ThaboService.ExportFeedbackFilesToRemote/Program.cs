﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using SIL.ServiceLibrary;

namespace SIL.ThaboService.ExportFeedbackFilesToRemote
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.ThaboService.ExportFeedbackFilesToRemote"
                ,
                DisplayName = "SIL.ThaboService.ExportFeedbackFilesToRemote"
                ,
                Description = "SIL.ThaboService.ExportFeedbackFilesToRemote"
            };

            ProgramRun.InitializeService(new ExportFeedbackFilesToRemote(), serviceDescriptor, args);
        }
    }
}
