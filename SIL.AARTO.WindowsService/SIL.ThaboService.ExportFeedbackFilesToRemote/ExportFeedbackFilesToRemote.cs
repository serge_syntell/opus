﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.ThaboService.ExportFeedbackFilesToRemote
{
    partial class ExportFeedbackFilesToRemote : ServiceHost
    {
        public ExportFeedbackFilesToRemote()
            : base("", new Guid("F452EC55-C436-450D-8969-0D27C9A313FE"), new ExportFeedbackFilesToRemoteService())
        {
            InitializeComponent();
        }
    }
}
