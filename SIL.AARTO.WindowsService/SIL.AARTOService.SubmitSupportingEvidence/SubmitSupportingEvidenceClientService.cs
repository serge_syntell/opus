﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Transactions;
using System.ServiceModel;
using System.Threading;

using SIL.AARTO.BLL.Utility;
using Gendac.Syntell.Curator.Web.Services.Data;
using SIL.AARTOService.CISInterface.Library;
using SIL.AARTOService.Resource;
using SIL.AARTOService.Library;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.QueueLibrary;
using Stalberg.TMS;
using SIL.ServiceBase;

namespace SIL.AARTOService.SubmitSupportingEvidence
{
    public class SubmitSupportingEvidenceClientService : ServiceDataProcessViaQueue
    {
        string url = "";
        string username = "";
        string password = "";
        int reTryCount = 3;

        CuratorClient client = null;
        string aartoConnectionStr = string.Empty;

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }

        public SubmitSupportingEvidenceClientService()
            : base("SubmitSupportingEvidence", "Submit supporting evidence row to CIS",
            new Guid("CABC0B7C-FFB8-4400-8997-5D6A914BB7F1"),
            ServiceQueueTypeList.SubmitSupportingEvidence)
        {
            this._serviceHelper = new AARTOServiceBase(this);

            AARTOBase.OnServiceStarting = () =>
            {
                url = AARTOBase.GetConnectionString(ServiceConnectionNameList.CISInterfaceURL, ServiceConnectionTypeList.WSU);
                aartoConnectionStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);

                if (string.IsNullOrWhiteSpace(url) || !Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute))
                {
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("CISURLError"), LogType.Fatal, ServiceBase.ServiceOption.BreakAndShutdown);
                    return;
                }

                username = AARTOBase.GetServiceParameter("Username");
                password = AARTOBase.GetServiceParameter("Password");

                System.ServiceModel.BasicHttpBinding binding = new System.ServiceModel.BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential);
                System.ServiceModel.EndpointAddress address = new System.ServiceModel.EndpointAddress(new Uri(url));
                binding.OpenTimeout = new TimeSpan(0, 3, 0);
                client = new CuratorClient(binding, address);
                client.ClientCredentials.UserName.UserName = username;
                client.ClientCredentials.UserName.Password = password;
            };
        }

        public override void InitialWork(ref QueueLibrary.QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString().Trim();
            //Jake 2014-04-10 removed check Group value in queue ,no use
            //if (!string.IsNullOrWhiteSpace(body) && !string.IsNullOrWhiteSpace(item.Group))
            if (!string.IsNullOrWhiteSpace(body))
            {
                item.IsSuccessful = true;
                item.Status = QueueItemStatus.Discard;

                int NotIntNo;
                if (!int.TryParse(body, out NotIntNo))
                {
                    AARTOBase.ErrorProcessing(ref item);
                    AARTOBase.DeQueueInvalidItem(ref item);
                    return;
                }

                Notice notice = new NoticeService().GetByNotIntNo(Convert.ToInt32(item.Body));
                if (notice == null)
                {
                    AARTOBase.ErrorProcessing(ref item);
                    AARTOBase.DeQueueInvalidItem(ref item);
                    return;
                }

                NoticeFrame notFrame = new NoticeFrameService().GetByNotIntNo(notice.NotIntNo).FirstOrDefault();
                if (notFrame == null)
                {
                    AARTOBase.ErrorProcessing(ref item);
                    AARTOBase.DeQueueInvalidItem(ref item);
                    return;
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Result_NoticeNotExist") + " NotIntNo: " + item.Body);
                AARTOBase.DeQueueInvalidItem(ref item);
                return;
            }
        }

        public override void MainWork(ref List<QueueLibrary.QueueItem> queueList)
        {
            if (queueList == null || queueList.Count == 0) return;

            QueueLibrary.QueueItem queue = null;
            for (int index = 0; index < queueList.Count; index++)
            {
                queue = queueList[index];

                Notice notice = new NoticeService().GetByNotIntNo(Convert.ToInt32(queue.Body));

                ProcessEvidence(notice, queue);
            }
        }

        private XDocument GetSoap(string noticeNumber, string originalFilename, byte[] evidence)
        {
            XNamespace wsse = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
            XNamespace soap = "http://schemas.xmlsoap.org/soap/envelope/";
            XNamespace xs = "http://www.w3.org/2001/XMLSchema";
            XNamespace tns = "http://tempuri.org/";

            XDocument requestDocument = new XDocument(
                new XDeclaration("1.0", "UTF-8", null),
                new XElement(soap + "Envelope",
                    new XAttribute(XNamespace.Xmlns + "wsse", wsse),
                    new XAttribute(XNamespace.Xmlns + "soap", soap),
                    new XAttribute(XNamespace.Xmlns + "xs", xs),
                    new XElement(soap + "Header",
                        new XElement(wsse + "Security",
                            new XElement(wsse + "UsernameToken",
                                new XElement(wsse + "Username", username),
                                new XElement(wsse + "Password", password)
                            )
                        )
                    ),
                    new XElement(soap + "Body",
                        new XElement(tns + "SubmitSupportingEvidence",
                            new XElement("noticeNumber", noticeNumber),
                            new XElement("originalFilename", originalFilename),
                            new XElement("evidence", evidence)
                        )
                    )
                )
            );

            return requestDocument;
        }

        private void ProcessEvidence(Notice notice, QueueItem queue)
        {
            NoticeFrame notFrame = new NoticeFrameService().GetByNotIntNo(notice.NotIntNo).FirstOrDefault();
            Frame frame = new FrameService().GetByFrameIntNo(notFrame.FrameIntNo);
            SIL.AARTO.DAL.Entities.TList<ScanImage> imageList = new ScanImageService().GetByFrameIntNo(frame.FrameIntNo);

            ScanImageDB scanImageDB = new ScanImageDB(aartoConnectionStr);
            ImageProcesses imgProcess = new ImageProcesses(aartoConnectionStr);

            bool isSuccess = true;

            for (int i = 0; i < imageList.Count; i++)
            {
                int CIHIntNo = 0;

                string requestXml = string.Empty;
                string responseXml = string.Empty;
                int retryIndex = 1;
                try
                {
                    byte[] evidence = imgProcess.CompressImage(scanImageDB.GetScanImageDataFromRemoteServer(imageList[i].ScImIntNo), 0);

                    SupportingEvidenceRequest request = new SupportingEvidenceRequest();
                    request.NoticeNumber = notice.NotTicketNo;
                    request.OriginalFileName = imageList[i].JpegName;
                    request.Evidence = new byte[0];

                    // create request history file
                    requestXml = ObjectSerializer.Serialize(request);
                    CIHIntNo = AddSupportingEvidenceHistory(CIHIntNo, notice.NotIntNo, notice.NotTicketNo, requestXml);

                    //write send log
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Suppress, TimeSpan.MaxValue))
                    {
                        this.Logger.Info(string.Format(ResourceHelper.GetResource("CISSupportingEvidenceSendLogFile"), notice.NotTicketNo, imageList[i].JpegName));
                        scope.Complete();
                    }

                    SubmitReply response = null;
                    while (retryIndex <= reTryCount)
                    {
                        try
                        {
                            response = client.SubmitSupportingEvidence(notice.NotTicketNo, imageList[i].JpegName, evidence);
                        }
                        catch (Exception ex)
                        {
                            AARTOBase.LogProcessing(ex, LogType.Error, ServiceBase.ServiceOption.Continue, queue, QueueLibrary.QueueItemStatus.UnKnown);
                        }
                        if (response != null)
                        {
                            break;
                        }
                        else
                        {
                            if (retryIndex == reTryCount)
                            {
                                AARTOBase.LogProcessing(String.Format("We have tried {0} times and get no response from CIS interface, Infringement Number: {1}, FileName", reTryCount, notice.NotTicketNo, imageList[i].JpegName), LogType.Error);
                            }
                            retryIndex++;
                            Thread.Sleep(new TimeSpan(0, 0, 3));
                        }
                    }

                    if (response != null)
                    {
                        //write receive log
                        this.Logger.Info(string.Format(ResourceHelper.GetResource("CISSupportingEvidenceReceiveLogFile"), notice.NotTicketNo, imageList[i].JpegName));

                        // create response history file
                        responseXml = ObjectSerializer.Serialize(response);

                        AddSupportingEvidenceHistory(CIHIntNo, notice.NotIntNo, notice.NotTicketNo, responseXml);

                        using (TransactionScope tran = new TransactionScope())
                        {
                            if (response.IsSuccess)
                            {
                                this.Logger.Info(string.Format(ResourceHelper.GetResource("CISSupportingEvidenceSendFileSuccessfully"), notice.NotTicketNo, imageList[i].JpegName));
                            }
                            else
                            {
                                isSuccess = false;

                                ReplyMessage[] message = response.Messages;
                                if (message != null && message.Length > 0)
                                {
                                    string errorMsg = "";
                                    for (int j = 0; j < message.Length; j++)
                                    {
                                        errorMsg += message[j].Code.ToString() + "~" + message[j].Description + ",";
                                    }
                                    if (errorMsg != "")
                                    {
                                        errorMsg = errorMsg.Substring(0, errorMsg.Length - 1);
                                    }

                                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CISSupportingEvidenceResponseError"), notice.NotTicketNo, imageList[i].JpegName, errorMsg),
                                    LogType.Error, ServiceOption.Continue, queue, QueueLibrary.QueueItemStatus.UnKnown);
                                }
                            }

                            tran.Complete();
                        }
                    }
                    else
                    {
                        isSuccess = false;

                        AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CISSupportingEvidenceNoResponseReturn"), notice.NotTicketNo, imageList[i].JpegName),
                            LogType.Error, ServiceBase.ServiceOption.Continue, queue, QueueLibrary.QueueItemStatus.UnKnown);
                    }
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                    AARTOBase.LogProcessing(ex, LogType.Error, ServiceBase.ServiceOption.Continue, queue, QueueLibrary.QueueItemStatus.UnKnown);
                }
            }

            if (isSuccess)
            {
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CISSupportingEvidenceSummmitSuccessfully"), notice.NotTicketNo),
                            LogType.Info, ServiceBase.ServiceOption.Continue, queue, QueueLibrary.QueueItemStatus.Discard);
            }
            else
            {
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CISSupportingEvidenceSummmitFailed"), notice.NotTicketNo),
                            LogType.Error, ServiceBase.ServiceOption.Continue, queue, QueueLibrary.QueueItemStatus.UnKnown);
            }
        }

        private int AddSupportingEvidenceHistory(int CIHIntNo, int NotIntNo, string NoticeNumber, string xmlBody)
        {
            if (CIHIntNo == 0)
            {
                CisInfringementHistoryFile history = new CisInfringementHistoryFile();
                history.NotIntNo = NotIntNo;
                history.NotTicketNo = NoticeNumber;
                history.CihRequestXml = xmlBody;
                history.CihRequestDate = DateTime.Now;
                history.LastUser = AARTOBase.LastUser;

                history = new CisInfringementHistoryFileService().Save(history);
                CIHIntNo = history.CihIntNo;
            }
            else
            {
                CisInfringementHistoryFile history = new CisInfringementHistoryFileService().GetByCihIntNo(CIHIntNo);
                history.CihResponseXml = xmlBody;
                history.CihResponseDate = DateTime.Now;
                history.LastUser = AARTOBase.LastUser;

                history = new CisInfringementHistoryFileService().Save(history);
            }

            return CIHIntNo;
        }
    }

    [Serializable]
    public class SupportingEvidenceRequest
    {
        public string NoticeNumber;
        public string OriginalFileName;
        public byte[] Evidence;
    }
}

