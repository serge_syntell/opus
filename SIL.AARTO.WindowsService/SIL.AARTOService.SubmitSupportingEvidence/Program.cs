﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.SubmitSupportingEvidence
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescr = new ServiceDescriptor()
            {
                Description = "SIL.AARTOService.SubmitSupportingEvidence",
                DisplayName = "SIL.AARTOService.SubmitSupportingEvidence",
                ServiceName = "SIL.AARTOService.SubmitSupportingEvidence"
            };

            ProgramRun.InitializeService(new SubmitSupportingEvidence(), serviceDescr, args);
        }
    }
}
