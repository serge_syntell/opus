﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.SubmitSupportingEvidence
{
    partial class SubmitSupportingEvidence : ServiceHost
    {
        public SubmitSupportingEvidence()
            : base("SIL.AARTOService.SubmitSupportingEvidence",
            new Guid("120F3248-2D73-4F21-B273-9A02FA317443"),
            new SubmitSupportingEvidenceClientService())
        {
            InitializeComponent();
        }

    }
}
