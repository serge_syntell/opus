﻿using System;
using System.Collections.Generic;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.QueueLibrary;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;

namespace SIL.AARTOService.NewCourtDate
{
    class NewCourtDateService : ServiceDataProcessViaQueue
    {
        readonly AuthorityService authorityService = new AuthorityService();
        readonly string connStrAarto;
        readonly CourtService courtService = new CourtService();
        readonly NoticeWithNoCourtDateManager nwncdManager;
        string autCode;
        int autIntNo;
        DateTime? courtDate;
        int crtIntNo;
        string crtName;
        QueueItem queueItem;

        public NewCourtDateService() : base("", "", new Guid("081AA482-4C3E-4650-9D60-BFE2748C0EBA"), ServiceQueueTypeList.NewCourtDate)
        {
            _serviceHelper = new AARTOServiceBase(this, false);
            this.connStrAarto = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.nwncdManager = new NoticeWithNoCourtDateManager(this.connStrAarto, AARTOBase.LastUser)
            {
                OnLog = (s, t) => AARTOBase.LogProcessing(s, t,
                    t == LogType.Error ? ServiceOption.Break : ServiceOption.Continue, this.queueItem,
                    t == LogType.Error ? QueueItemStatus.UnKnown : this.queueItem.Status)
            };
        }

        AARTOServiceBase AARTOBase
        {
            get { return (AARTOServiceBase)_serviceHelper; }
        }

        public override void InitialWork(ref QueueItem item)
        {
            var body = Convert.ToString(item.Body);
            if (!string.IsNullOrWhiteSpace(body)
                && ServiceUtility.IsNumeric(body = body.Trim()))
            {
                this.crtIntNo = Convert.ToInt32(body);

                var court = this.courtService.GetByCrtIntNo(this.crtIntNo);
                if (court == null)
                {
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("CourtDoesNotExist", this.crtIntNo), LogType.Error, ServiceOption.Break, item);
                    return;
                }
                this.crtName = court.CrtName;

                if (QueueGroupValidation(item))
                {
                    item.IsSuccessful = true;
                }
                else
                {
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("QueueGroupValidationFailed"), LogType.Error, ServiceOption.Break, item);
                }
            }
            else
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("QueueItemValidationFailed"), LogType.Error, ServiceOption.Break, item);
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            foreach (var item in queueList)
            {
                if (!item.IsSuccessful) continue;
                this.queueItem = item;
                try
                {
                    if (this.nwncdManager.Process(this.autCode, this.crtIntNo, this.courtDate.Value))
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("NewCourtDateProcessSuccessfully",
                            this.crtName, this.courtDate.Value.ToString("yyyy-MM-dd"),
                            this.nwncdManager.ProcessedCount, this.nwncdManager.PushedCount),
                            LogType.Info, ServiceOption.Continue, item, QueueItemStatus.Discard);
                }
                catch (Exception ex)
                {
                    AARTOBase.LogProcessing(ex.ToString(), LogType.Error, ServiceOption.Break, item);
                    return;
                }
            }
        }

        bool QueueGroupValidation(QueueItem item)
        {
            string[] group;
            string autCode;
            DateTime date;

            if (!string.IsNullOrWhiteSpace(item.Group)
                && (group = item.Group.Trim().Split('|')).Length == 2
                && !string.IsNullOrWhiteSpace(group[0])
                && !string.IsNullOrWhiteSpace(group[1])
                && DateTime.TryParse(group[1].Trim(), out date))
            {
                autCode = group[0].Trim();

                if (autCode != this.autCode)
                {
                    Authority authority;

                    if ((authority = this.authorityService.GetByAutCode(autCode)) != null)
                    {
                        this.autCode = autCode;
                        this.autIntNo = authority.AutIntNo;
                    }
                    else
                        return false;
                }

                this.courtDate = date;

                return true;
            }
            return false;
        }
    }
}
