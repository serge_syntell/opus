﻿using SIL.ServiceLibrary;

namespace SIL.AARTOService.NewCourtDate
{
    static class Program
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ProgramRun.InitializeService(new NewCourtDate(),
                new ServiceDescriptor
                {
                    ServiceName = "SIL.AARTOService.NewCourtDate",
                    DisplayName = "SIL.AARTOService.NewCourtDate",
                    Description = "SIL AARTOService NewCourtDate"
                }, args);
        }
    }
}
