﻿using System;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.NewCourtDate
{
    partial class NewCourtDate : ServiceHost
    {
        public NewCourtDate()
            : base("", new Guid("D3B45FD4-F193-47FB-9708-485371A1C633"), new NewCourtDateService())
        {
            InitializeComponent();
        }
    }
}
