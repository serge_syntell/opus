﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PeriodStatisticsRollUp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor 
            {
                ServiceName = "SIL.AARTOService.PeriodStatisticsRollUp",
                Description = "SIL.AARTOService.PeriodStatisticsRollUp",
                DisplayName = "SIL AARTOService PeriodStatisticsRollUp",
            };
            ProgramRun.InitializeService(new PeriodStatisticsRollUp(), serviceDescriptor, args);
        }
    }
}
