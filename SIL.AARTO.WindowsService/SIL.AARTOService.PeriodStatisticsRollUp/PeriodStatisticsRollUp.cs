﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PeriodStatisticsRollUp
{
    public partial class PeriodStatisticsRollUp : ServiceHost
    {
        public PeriodStatisticsRollUp():
            base("", new Guid("7377FCB7-BBA7-4931-9F72-BDA93DCCB6CD"), new PeriodStatisticsRollUp_Service())
        {
            InitializeComponent();
        }
    }
}
