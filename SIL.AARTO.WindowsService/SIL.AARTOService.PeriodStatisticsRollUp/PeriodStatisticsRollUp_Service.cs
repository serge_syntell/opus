﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.QueueLibrary;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using queue = SIL.ServiceQueueLibrary.DAL.Entities;

namespace SIL.AARTOService.PeriodStatisticsRollUp
{
    class PeriodStatisticsRollUp_Service : ClientService
    {
        #region 2013-10-25, Oscar rewrote and backup

        //public PeriodStatisticsRollUp_Service() :
        //    base("", "", new Guid("395425AD-4842-41F5-B0ED-231452F57D90"))
        //{
        //    this._serviceHelper=new AARTOServiceBase(this,true);
        //}

        //AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)this._serviceHelper; } }
        //string connStr;
        //ServiceDB DBHelper;


        //public override void InitialWork(ref SIL.QueueLibrary.QueueItem item)
        //{

        //}

        //public override void PrepareWork()
        //{
        //    connStr = AARTOBase.GetConnectionString(queue.ServiceConnectionNameList.AARTO, queue.ServiceConnectionTypeList.DB);
        //    DBHelper = new ServiceDB(connStr);
        //    AARTOBase.OnServiceSleeping = SleepingRecord;
        //}
        //public override void MainWork(ref List<SIL.QueueLibrary.QueueItem> queueList)
        //{
        //    try
        //    {
        //        Logger.Info("Work began......");
        //        int flag = DBHelper.ExecuteNonQuery("PeriodStatisticsAllTables");
        //        //
        //        this.MustSleep = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        AARTOBase.ErrorProcessing(ex.Message, true);
        //    }
        //}

        //private void SleepingRecord()
        //{
        //    Logger.Info("Sleeping now......");
        //}

        #endregion

        readonly ServiceDB DBHelper;
        readonly string lastUser;
        int? processRowCount;

        public PeriodStatisticsRollUp_Service() :
            base("", "", new Guid("395425AD-4842-41F5-B0ED-231452F57D90"))
        {
            _serviceHelper = new AARTOServiceBase(this, false);
            this.lastUser = AARTOBase.LastUser;
            var connStr = AARTOBase.GetConnectionString(queue.ServiceConnectionNameList.AARTO, queue.ServiceConnectionTypeList.DB);
            this.DBHelper = new ServiceDB(connStr);
            AARTOBase.OnServiceStarting = GetParameters;
        }

        AARTOServiceBase AARTOBase
        {
            get { return (AARTOServiceBase)_serviceHelper; }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            Statistics();
            MustSleep = true;
        }

        void Statistics()
        {
            var sum = 0;
            var errors = new List<string>();
            var rowCount = this.processRowCount.GetValueOrDefault(100);

            while (true)
            {
                int processed;
                try
                {
                    using (var scope = ServiceUtility.CreateTransactionScope())
                    {
                        var obj = this.DBHelper.ExecuteScalar("PeriodStatisticsAllTables_WS", new[]
                        {
                            new SqlParameter("@ProcessRowCount", rowCount),
                            new SqlParameter("@LastUser", this.lastUser)
                        }, true, 0);
                        processed = Convert.ToInt32(obj);

                        if (processed < 0)
                        {
                            switch (processed)
                            {
                                case -1:
                                    errors.Add(ResourceHelper.GetResource("FailedInPreparingData"));
                                    break;
                                case -2:
                                    errors.Add(ResourceHelper.GetResource("FailedInLockingData"));
                                    break;
                                case -3:
                                    errors.Add(ResourceHelper.GetResource("FailedInCountingSummonsSumCharge"));
                                    break;
                                case -4:
                                    errors.Add(ResourceHelper.GetResource("FailedInCountingFrame"));
                                    break;
                                case -5:
                                    errors.Add(ResourceHelper.GetResource("FailedInCountingChargeReceiptTran"));
                                    break;
                                case -6:
                                    errors.Add(ResourceHelper.GetResource("FailedInCountingWOA"));
                                    break;
                                case -7:
                                    errors.Add(ResourceHelper.GetResource("FailedInUpdatingPSTDateSummed"));
                                    break;
                            }
                            break;
                        }

                        if (processed >= 0
                            && ServiceUtility.TransactionCanCommit())
                            scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    errors.Add(ex.ToString());
                    break;
                }

                if (processed > 0) sum += processed;
                if (processed < rowCount) break;
            }

            if (sum > 0 || errors.Count <= 0)
                AARTOBase.LogProcessing(ResourceHelper.GetResource("SuccessfullyCountedRowsForPeriodStatsTran", sum), LogType.Info);

            if (errors.Count > 0)
            {
                errors.ForEach(e => AARTOBase.LogProcessing(e, LogType.Error, ServiceOption.Break));
                errors.Clear();
            }
        }

        void GetParameters()
        {
            string rowCount;
            int number;
            if (!this.processRowCount.HasValue
                && !string.IsNullOrWhiteSpace(rowCount = AARTOBase.GetServiceParameter("ProcessRowCount"))
                && int.TryParse(rowCount, out number))
                this.processRowCount = number;
            AARTOBase.LogProcessing(ResourceHelper.GetResource("CurrentProcessRowCount", this.processRowCount.GetValueOrDefault(100)), LogType.Info);
        }
    }
}
