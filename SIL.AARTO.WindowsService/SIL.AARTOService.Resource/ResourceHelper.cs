﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Configuration;
using SIL.AARTOService.DAL;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Resource
{
    public class ResourceHelper
    {
        [ThreadStatic]
        static ILogger logger;
        static string language;
        static readonly Dictionary<string, string> resources = new Dictionary<string, string>();
        static readonly object syncObj = new object();

        public static string GetResource(string key, params object[] args)
        {
            var value = GetResource(key);
            if (!value.Equals(key))
                return string.Format(value, args);

            // Oscar 2013-03-13 changed to show args.
            var sb = new StringBuilder(value);
            foreach (var arg in args)
            {
                sb.Append(" | ").Append(arg);
            }
            return sb.ToString();
        }

        public static string GetResource(string key)
        {
            lock (syncObj)
            {
                string result = null;

                try
                {
                    if (resources.ContainsKey(key))
                        result = resources[key];
                    else
                    {
                        var fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, GetLanguage() + ".xml");

                        // Oscar 2013-03-14 add error msg;
                        var fileExists = CheckFileExists(fileName);

                        if (!fileExists)
                        {
                            fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "en-US.xml");
                            fileExists = CheckFileExists(fileName);
                        }

                        if (fileExists)
                        {
                            var xmldoc = new XmlDocument();
                            xmldoc.Load(fileName);
                            var resList = xmldoc.DocumentElement.ChildNodes;

                            var found = false;
                            foreach (XmlNode element in resList)
                            {
                                if (element.NodeType != XmlNodeType.Element) continue;
                                var resKey = element.Attributes["key"].Value;
                                var resValue = element.InnerText.Replace(@"\r", "\r").Replace(@"\n", "\n");

                                if (!string.IsNullOrEmpty(resKey))
                                {
                                    if (resKey.Equals(key))
                                    {
                                        found = true;
                                        result = resValue;
                                    }

                                    if (!resources.ContainsKey(resKey))
                                        resources.Add(resKey, resValue);
                                }
                            }

                            if (!found)
                                AddErrorMessage(string.Format("'{0}' is not in the resource file", key));
                        }
                    }
                }
                catch (Exception ex)
                {
                    AddErrorMessage(ex.Message);
                }

                return result ?? key;
            }
        }

        static string GetLanguage()
        {
            if (string.IsNullOrEmpty(language))
            {
                if (ConfigurationManager.ConnectionStrings["SIL.ServiceQueueLibrary.DAL.Data.ConnectionString"] != null)
                {
                    var connstr = ConfigurationManager.ConnectionStrings["SIL.ServiceQueueLibrary.DAL.Data.ConnectionString"].ConnectionString;
                    if (!string.IsNullOrWhiteSpace(connstr))
                    {
                        string msg;
                        var db = new DBHelper(connstr);
                        var obj = db.ExecuteScalar("SILCustom_ServiceHost_GetLanguage", null, out msg);
                        if (obj != null)
                            language = Convert.ToString(obj);
                        if (!string.IsNullOrWhiteSpace(msg))
                            AddErrorMessage(msg);
                    }
                    else
                        AddErrorMessage("Can not get connection string for queue database.");
                }
                if (string.IsNullOrEmpty(language))
                    //language = System.Globalization.CultureInfo.CurrentCulture.ToString();
                    language = "en-US";
            }
            return language;
        }

        #region Oscar 2013-03-14 added, fixed issue in getting resource

        static void AddErrorMessage(string msg)
        {
            if (logger != null && !string.IsNullOrWhiteSpace(msg))
                logger.Error(string.Format("Resource error: {0}", msg));
        }

        static bool CheckFileExists(string fileName)
        {
            var exists = !string.IsNullOrWhiteSpace(fileName) && File.Exists(fileName);
            if (!exists)
                AddErrorMessage(string.Format("'{0}' doesn't exist", fileName));
            return exists;
        }

        public static void SetLogger(ILogger log)
        {
            logger = log;
        }

        #endregion
    }
}