﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using System.IO;
using Stalberg.TMS;
using SIL.AARTOService.Resource;
using System.Data;
using System.Transactions;
using System.Configuration;
using System.Data.SqlClient;
using Stalberg.TMS_TPExInt.Components;
using Stalberg.TMS.Data;
using System.Text.RegularExpressions;

namespace SIL.AARTOService.CIP_CofCT_InfringementFileCreate
{
    public class CIP_CofCT_InfringementFileCreateService : ServiceDataProcessViaQueue
    {
        //FileInfo fileInfo;
        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        string connAARTODB, connCIP_CofCTUNC;
        int notIntNo, autIntNo;
        string strAutNo = string.Empty;
        string camUnitID = string.Empty;

        public CIP_CofCT_InfringementFileCreateService()
            : base("", "", new Guid("D90EE783-5151-491E-A728-E2287CDFA717"), ServiceQueueTypeList.InfringementFileCreate)
        {
            this._serviceHelper = new AARTOServiceBase(this, false);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            // 2012-02-21 jerry change
            this.connCIP_CofCTUNC = AARTOBase.GetConnectionString(ServiceConnectionNameList.CiprusCofCT_ExportFolder, ServiceConnectionTypeList.UNC);
            AARTOBase.OnServiceStarting = () =>
            {
                AARTOBase.CheckFolder(this.connCIP_CofCTUNC);
                //Jerry 2012-04-13 add
                AARTOBase.CheckFolder(Path.Combine(connCIP_CofCTUNC, @"Completed"));
            };
        }

        //Jerry 2012-04-12 change
        //public override void PrepareWork()
        //{
        //    this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
        //    //this.connCIP_CofCTUNC = AARTOBase.GetConnectionString(ServiceConnectionNameList.CiprusCofCT_LoadFiles, ServiceConnectionTypeList.UNC);
        //    infringementFolder = Path.Combine(ServiceParameters["CiprusCofCTExportFolder"], "InfringementFiles");
        //    //this.GetDataFilesFromTP_CofCT(connCIP_CofCTUNC);
        //}

        /// <summary>
        /// Jerry 2012-04-12 add
        /// </summary>
        /// <param name="item"></param>
        public override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;

            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && ServiceUtility.IsNumeric(body))
            {
                item.IsSuccessful = true;
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.LastUser, item.Body), true);
            }

            try
            {
                item.IsSuccessful = true;
                item.Status = QueueItemStatus.Discard;
                this.notIntNo = Convert.ToInt32(item.Body);

                NoticeDB noticeDB = new NoticeDB(this.connAARTODB);
                NoticeDetails notice = noticeDB.GetNoticeDetails(notIntNo);
                this.autIntNo = notice.AutIntNo;

                string[] group = item.Group.Trim().Split('|');
                if (group == null || group.Length != 2)
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("QueueGroupValidationFailed"), true);
                    return;
                }
                this.strAutNo = group[0].Trim();
                this.camUnitID = group[1].Trim();

                if (!Directory.Exists(Path.Combine(connCIP_CofCTUNC, @"Completed", this.strAutNo)))
                    Directory.CreateDirectory(Path.Combine(connCIP_CofCTUNC, @"Completed", this.strAutNo));

            }
            catch (Exception ex)
            {
                AARTOBase.ErrorProcessing(ref item, ex);
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                if (!item.IsSuccessful)
                    continue;

                try
                {
                    this.CreateInfringementFiles(item.Body.ToString());
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        private const string CONFIG_FILE_PREFIX_CC = "cocfg";
        private const string INFRINGEMENT_FILE_PREFIX_CC = "cocon";
        private const string RESPONSE_FILE_PREFIX_CC = "cores";
        private const string REQUEST_FILE_PREFIX_CC = "cippp";

        private const string TICKET_PROCESSOR_CC = "Cip_CofCT";
        private const int STATUS_LOADED = 100;
        private const int STATUS_ERRORS = 110;
        private const int STATUS_ERRORS_FIXED = 120;
        private const int STATUS_SENT_TO_CIVITAS = 140;
        private const string FILE_EXT = ".txt";

        #region Jerry 2012-04-13 change this method
        //private void CreateInfringementFiles(string authCode)
        //{
        //    string errMessage = "";
        //    int prevAutIntNo = 0;

        //    Logger.Info(ResourceHelper.GetResource("StartCreationInfringementFiles", TICKET_PROCESSOR_CC, DateTime.Now.ToString()));

        //    //save to folders
        //    //string exportFolder = connCIP_CofCTUNC;

        //    // going to need a save to sub-folder as well as a completed for each file type - check that it exists

        //    if (!Directory.Exists(infringementFolder))
        //        Directory.CreateDirectory(infringementFolder);

        //    if (!Directory.Exists(Path.Combine(infringementFolder, @"\Completed")))
        //        Directory.CreateDirectory(Path.Combine(infringementFolder, @"\Completed"));

        //    SqlDataReader reader = this.GetAuthForCreatingInfringements(authCode, ref errMessage);

        //    if (reader == null)
        //    {
        //        Logger.Error("CreateInfringementFiles - error: " + errMessage);
        //        reader.Dispose();
        //        return;
        //    }
        //    else if (!reader.HasRows)
        //    {
        //        Logger.Info(ResourceHelper.GetResource("CreateInfringementFilesNoNotice"));
        //    }
        //    else
        //    {
        //        while (reader.Read())
        //        {
        //            int autIntNo = Convert.ToInt32(reader["AutIntNo"]);
        //            string autName = reader["AutName"].ToString();
        //            string autNo = reader["AutNo"].ToString().Trim();
        //            string camUnitID = reader["NotCameraID"].ToString();
        //            string fileName = "";

        //            if (!Directory.Exists(Path.Combine(infringementFolder, @"\Completed", autNo)))
        //                Directory.CreateDirectory(Path.Combine(infringementFolder, @"\Completed", autNo));

        //            int noOfRecords = 0;

        //            bool failed = this.WriteInfringementFile(autIntNo, autNo, camUnitID, ref noOfRecords, infringementFolder, ref fileName);

        //            //if (fatalError)
        //            //    return true;

        //            if (!failed)
        //            {
        //                string exportFullPath = Path.Combine(infringementFolder, fileName);

        //                // copy the file to the Configuration files folder for export to Civitas
        //                this.FileToLocalFolder(exportFullPath, connCIP_CofCTUNC + @"\", "contravention", "copy");

        //                // move the file to the Configuration files completed folder
        //                if (!Directory.Exists(Path.Combine(infringementFolder, @"Completed", autNo)))
        //                    Directory.CreateDirectory(Path.Combine(infringementFolder, @"Completed", autNo));

        //                this.FileToLocalFolder(exportFullPath, Path.Combine(infringementFolder, @"Completed", autNo), "infringement", "move");
        //            }

        //            prevAutIntNo = autIntNo;
        //        }

        //        Logger.Info(ResourceHelper.GetResource("CompletedCreationInfringementFiles", TICKET_PROCESSOR_CC, DateTime.Now.ToString()));
        //    }

        //    reader.Dispose();

        //    return;
        //}
        #endregion

        private void CreateInfringementFiles(string authCode)
        {
            string fileName = "";
            int noOfRecords = 0;

            bool failed = this.WriteInfringementFile(autIntNo, this.strAutNo, camUnitID, ref noOfRecords, connCIP_CofCTUNC, ref fileName);

            if (!failed)
            {
                string exportFullPath = Path.Combine(connCIP_CofCTUNC, fileName);

                this.FileToLocalFolder(exportFullPath, Path.Combine(connCIP_CofCTUNC, @"Completed", this.strAutNo), "infringement", "move");
                    
            }

            return;
        }

        //internal SqlDataReader GetAuthForCreatingInfringements(string authCode, ref string errMessage)
        //{
        //    TMSData list = new TMSData(this.connAARTODB);
        //    SqlDataReader authList = list.GetAuthForInfringements(authCode, TICKET_PROCESSOR_CC, STATUS_LOADED, STATUS_ERRORS_FIXED, ref errMessage);
        //    return authList;
        //}

        private DataSet GetInfringementData(int autIntNo, string camUnitID)
        {
            CiprusCofCTData CiprusCofCTData = new CiprusCofCTData(this.connAARTODB);
            DataSet infringementData = CiprusCofCTData.GetCiprusCofCTInfringementData(autIntNo, camUnitID, STATUS_LOADED, STATUS_ERRORS_FIXED);
            return infringementData;
        }

        internal bool WriteInfringementFile(int autIntNo, string autNo, string camUnitID, ref int noOfRecords, string infringementFolder, ref string fileName)
        {
            bool failed = false;
            string errMessage = "";
            int cubIntNo = 0;

            NoticeDB notice = new NoticeDB(this.connAARTODB);
            CameraUnitBatchDB batch = new CameraUnitBatchDB(this.connAARTODB);
            CameraUnitTranNoDB tranNo = new CameraUnitTranNoDB(this.connAARTODB);
            ChargeDB charge = new ChargeDB(this.connAARTODB);

            // Initialise and create a Character Translator for Civitas data
            CharacterTranslator.Initialise(this.connAARTODB);
            CharacterTranslator translator = new CharacterTranslator();

            //dls 090124 - not sure whether we can allow more than one contravention file for a camera to be out at the same time
            //              as we no longer have a problem with the panels

            DataSet noticeDS = this.GetInfringementData(autIntNo, camUnitID);

            // Check for empty recordset
            if (noticeDS.Tables[0].Rows.Count > 0)
            {
                string exportPath = connCIP_CofCTUNC;
                char pad0Char = Convert.ToChar("0");
                char padSpaceChar = Convert.ToChar(" ");
                string sNumber = "";
                string tempFilmNo = "";
                int counter = 0;
                int notIntNo = 0;
                DateTime fileDate = DateTime.Now;

                autNo = autNo.PadLeft(3, pad0Char).Substring(0, 3);
                camUnitID = camUnitID.Trim().PadLeft(3, pad0Char).Substring(0, 3);

                // construct Ciprus 66 filename
                // camera id is required in the file name. get it from the film     
                // 999 overflow handled in the sp
                int cutNextNo = tranNo.GetNextCameraUnitTranNo(autNo, camUnitID, 999, this.AARTOBase.LastUser);

                if (cutNextNo > 0)
                    sNumber = cutNextNo.ToString().PadLeft(4, pad0Char);
                else
                {
                    failed = true;
                    return failed;
                }

                string exportFullPath = Path.Combine(exportPath, INFRINGEMENT_FILE_PREFIX_CC + autNo + camUnitID + sNumber + FILE_EXT);

                fileName = Path.GetFileName(exportFullPath);

                //create batch for file
                cubIntNo = batch.AddCameraUnitBatch_CC(autNo, camUnitID, fileName, this.AARTOBase.LastUser);

                if (cubIntNo < 1)
                {
                    Logger.Error(ResourceHelper.GetResource("WriteInfringementFile_UnableCreateBatch", autNo, fileName));
                    failed = true;
                    return failed;
                }

                // create the csv file
                FileStream fs = new FileStream(exportFullPath, FileMode.Create, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.Default);
                string name = string.Empty;
                string address = string.Empty;
                string temp;
                CiprusCofCT66Record rec66;

                foreach (DataRow row in noticeDS.Tables[0].Rows)
                {
                    if (failed)
                        break;

                    try
                    {
                        rec66 = new CiprusCofCT66Record();

                        notIntNo = Convert.ToInt32(row["ReferenceNo"]);        //using NotIntNo as reference no

                        if (!failed)
                        {
                            rec66.recType = "66";
                            rec66.interfaceType = "C";
                            //rec66.versionNo = "01";
                            rec66.versionNo = "02"; // updated from spec 1.09
                            rec66.supplierCode = "Q";
                            rec66.cameraID = camUnitID;
                            rec66.autNo = autNo;

                            //ticketNo = changed to filler
                            rec66.Filler1 = string.Empty.PadRight(7, padSpaceChar);

                            rec66.filmNo = row["FilmNo"].ToString().Trim();
                            if (Regex.IsMatch(row["FilmNo"].ToString().Trim(), @"^\d+$")) //^\d+$ A string containing all digits (e.g. "012345")
                            {
                                // filmNo is already numeric
                                tempFilmNo = rec66.filmNo;
                            }
                            else
                                tempFilmNo = Regex.Replace(rec66.filmNo, @"[\D]", ""); // find all not numeric

                            rec66.filmNo = tempFilmNo.PadLeft(8, pad0Char).Substring(0, 8);
                            rec66.refNo = row["ReferenceNo"].ToString().Trim().PadLeft(9, pad0Char).Substring(0, 9);

                            if (Convert.ToInt32(row["NotSpeed1"]) == 0)
                                rec66.firstSpeed = "0000";
                            else
                                rec66.firstSpeed = ((Convert.ToDouble(row["NotSpeed1"]) * 10).ToString()).PadLeft(4, pad0Char).Substring(0, 4); // might need substring too - 0789 for 78.9 km, 0000 for no speed

                            if (Convert.ToInt32(row["NotSpeed2"]) == 0)
                                rec66.secondSpeed = "0000";
                            else
                                rec66.secondSpeed = ((Convert.ToDouble(row["NotSpeed2"]) * 10).ToString()).PadLeft(4, pad0Char).Substring(0, 4); // might need substring too - 0789 for 78.9 km, 0000 for no speed

                            rec66.officerNo = row["NotOfficerNo"].ToString().Trim().PadRight(9, padSpaceChar).Substring(0, 9);
                            rec66.cameraLoc = row["NotLocCode"].ToString().Trim().PadLeft(6, pad0Char).Substring(0, 6);
                            rec66.courtNo = row["NotCourtNo"].ToString().Trim().PadLeft(6, pad0Char).Substring(0, 6);
                            rec66.offenceDate = Convert.ToDateTime(row["NotOffenceDate"]).ToString("yyyyMMdd").Substring(0, 8);

                            //offence time has moved and change to N6
                            rec66.Filler2 = string.Empty.PadRight(4, padSpaceChar);

                            //VehMCode & VehTCode = changed to fillers
                            //rec66.vehMCode = row["NotVehicleMakeCode"].ToString().Trim().PadLeft(2, pad0Char).Substring(0, 2);
                            //rec66.vehTCode = row["NotVehicleTypeCode"].ToString().Trim().PadLeft(2, pad0Char).Substring(0, 2);

                            rec66.Filler3 = string.Empty.PadRight(2, padSpaceChar);
                            rec66.Filler4 = string.Empty.PadRight(2, padSpaceChar);

                            rec66.regNo = row["NotRegNo"].ToString().PadRight(10, padSpaceChar).Substring(0, 10);

                            rec66.travelDirection = row["NotTravelDirection"].ToString().PadRight(1, padSpaceChar).Substring(0, 1);
                            rec66.offenceCode = row["ChgOffenceCode"].ToString().Trim().PadLeft(6, pad0Char).Substring(0, 6);
                            rec66.typeVehOwner = row["NotStatutoryOwner"].ToString().Trim().Substring(0, 1);

                            rec66.idNo = row["OffIdNumber"].ToString().Trim().PadLeft(13, pad0Char).Substring(0, 13);

                            // Process illegal characters in the offender's name and address
                            temp = row["OffSurname"].ToString().Trim().Replace(@"0", @"O").PadRight(30, padSpaceChar).Substring(0, 30);
                            if (!translator.TryParseName(temp, out name))
                                Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_ErrorTranslatingOffenderSurname", temp, name));
                            rec66.surname = name;
                            name = string.Empty; //added for safety

                            //new - optional
                            rec66.forenames = string.Empty.PadRight(40, padSpaceChar);

                            temp = row["OffIntials"].ToString().Trim().Replace(@"0", @"O").PadRight(5, padSpaceChar).Substring(0, 5);
                            
                            if (!translator.TryParseName(temp, out name))
                                Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_ErrorTranslatingOffenderInitials", temp, name));
                            //mrs 20090205 what would happen if the surname was blank - would name end up in initials?
                            rec66.initials = name;
                            name = string.Empty; //added for safety

                            //dls 090617
                            rec66.businessIDNo = row["BusinessIDNo"].ToString().Trim().PadRight(13, padSpaceChar).Substring(0, 13);

                            temp = row["BusinessName"].ToString().Trim().Replace(@"0", @"O").PadRight(40, padSpaceChar).Substring(0, 40);
                            if (!translator.TryParseAddress(temp, out name))
                                Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_ErrorTranslatingOffenderInitials", temp, address));
                            rec66.businessName = name;

                            rec66.Filler5 = string.Empty.PadRight(30, padSpaceChar);
                            rec66.gender = string.Empty.PadLeft(2, pad0Char);
                            rec66.organisationType = string.Empty.PadRight(30, padSpaceChar);
                            rec66.dateOfBirth = string.Empty.PadLeft(8, pad0Char);

                            temp = row["StreetAddrLine1"].ToString().Trim().PadRight(40, padSpaceChar).Substring(0, 40);
                            if (!translator.TryParseAddress(temp, out address))
                                Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_ErrorTranslating1stSTR", temp, address));
                            rec66.streetAddrLine1 = address;
                            address = string.Empty; //added for safety

                            temp = row["StreetAddrLine2"].ToString().Trim().PadRight(40, padSpaceChar).Substring(0, 40);
                            if (!translator.TryParseAddress(temp, out address))
                                Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_ErrorTranslating2ndSTR", temp, address));
                            rec66.streetAddrLine2 = address;
                            address = string.Empty; //added for safety

                            temp = row["StreetAddrLine3"].ToString().Trim().PadRight(40, padSpaceChar).Substring(0, 40);
                            if (!translator.TryParseAddress(temp, out address))
                                Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_ErrorTranslating3rdSTR", temp, address));
                            rec66.streetAddrLine3 = address;
                            address = string.Empty; //added for safety

                            temp = row["StreetAddrLine4"].ToString().Trim().PadRight(40, padSpaceChar).Substring(0, 40);
                            if (!translator.TryParseAddress(temp, out address))
                                Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_ErrorTranslating4thSTR", temp, address));
                            rec66.streetAddrLine4 = address;
                            address = string.Empty; //added for safety

                            temp = row["StreetAddrLine5"].ToString().Trim().PadRight(40, padSpaceChar).Substring(0, 40);
                            if (!translator.TryParseAddress(temp, out address))
                                Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_ErrorTranslating5thSTR", temp, address));
                            rec66.streetAddrLine5 = address;
                            address = string.Empty; //added for safety

                            temp = row["StreetAddrCode"].ToString().Trim().PadLeft(4, pad0Char).Substring(0, 4);
                            if (!translator.TryParseAddress(temp, out address))
                                Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_ErrorTranslatingSTRCode", temp, address));
                            rec66.streetAddrCode = address;
                            address = string.Empty; //added for safety

                            temp = row["PostAddrLine1"].ToString().Trim().PadRight(40, padSpaceChar).Substring(0, 40);
                            if (!translator.TryParseAddress(temp, out address))
                                Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_ErrorTranslating1stPOS", temp, address));
                            rec66.postAddrLine1 = address;
                            address = string.Empty; //added for safety

                            temp = row["PostAddrLine2"].ToString().Trim().PadRight(40, padSpaceChar).Substring(0, 40);
                            if (!translator.TryParseAddress(temp, out address))
                                Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_ErrorTranslating2ndPOS", temp, address));
                            rec66.postAddrLine2 = address;
                            address = string.Empty; //added for safety

                            temp = row["PostAddrLine3"].ToString().Trim().PadRight(40, padSpaceChar).Substring(0, 40);
                            if (!translator.TryParseAddress(temp, out address))
                                Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_ErrorTranslating3rdPOS", temp, address));
                            rec66.postAddrLine3 = address;
                            address = string.Empty; //added for safety

                            temp = row["PostAddrLine4"].ToString().Trim().PadRight(40, padSpaceChar).Substring(0, 40);
                            if (!translator.TryParseAddress(temp, out address))
                                Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_ErrorTranslating4thPOS", temp, address));
                            rec66.postAddrLine4 = address;
                            address = string.Empty; //added for safety

                            temp = row["PostAddrLine5"].ToString().Trim().PadRight(40, padSpaceChar).Substring(0, 40);
                            if (!translator.TryParseAddress(temp, out address))
                                Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_ErrorTranslating5thPOS", temp, address));
                            rec66.postAddrLine5 = address;

                            rec66.postAddrCode = row["PostAddrCode"].ToString().Trim().PadLeft(4, pad0Char).Substring(0, 4);

                            rec66.telHome = string.Empty.PadRight(12, padSpaceChar);
                            rec66.telWork = string.Empty.PadRight(12, padSpaceChar);
                            rec66.fax = string.Empty.PadRight(12, padSpaceChar);
                            rec66.cell = string.Empty.PadRight(12, padSpaceChar);
                            rec66.email = string.Empty.PadRight(30, padSpaceChar);

                            rec66.natisVehMCode = row["NotNaTISVMCode"].ToString().Trim().PadRight(3, padSpaceChar).Substring(0, 3);
                            rec66.natisVehDescrCode = row["NotVehicleDescr"].ToString().Trim().PadRight(2, padSpaceChar).Substring(0, 2);
                            rec66.idType = row["IDType"].ToString().Trim().PadLeft(2, pad0Char).Substring(0, 2);
                            rec66.countryOfIssue = string.Empty.PadLeft(3, pad0Char);
                            rec66.licenceDiscNo = row["NotClearanceCert"].ToString().Trim().PadRight(20, padSpaceChar).Substring(0, 20); //row["??????"].ToString().Trim().PadRight(20, padSpaceChar).Substring(0, 20);
                            rec66.prDPCode = string.Empty.PadRight(20, padSpaceChar);
                            rec66.driverLicenceCode = string.Empty.PadRight(3, padSpaceChar);
                            rec66.operatorCardNo = string.Empty.PadRight(20, padSpaceChar);
                            rec66.vehicleGVM = string.Empty.PadLeft(8, pad0Char);

                            rec66.offenceTime = Convert.ToDateTime(row["NotOffenceDate"]).ToString("HHmmss").Substring(0, 6);
                            if (rec66.offenceTime == "    " || rec66.offenceTime == "")
                                rec66.offenceTime = "000000";

                            rec66.amberTime = row["AmberTime"].ToString().Trim().PadLeft(6, pad0Char).Substring(0, 6);
                            rec66.redTime = row["RedTime"].ToString().Trim().PadLeft(6, pad0Char).Substring(0, 6);

                            rec66.dateCaptured = Convert.ToDateTime(row["DateCaptured"]).ToString("yyyyMMdd").Substring(0, 8);
                            rec66.dateVerified = Convert.ToDateTime(row["DateVerified"]).ToString("yyyyMMdd").Substring(0, 8);
                            rec66.dateNatisReceived = Convert.ToDateTime(row["DateNatisReceived"]).ToString("yyyyMMdd").Substring(0, 8);
                            rec66.dateAdjudicated = Convert.ToDateTime(row["DateAdjudicated"]).ToString("yyyyMMdd").Substring(0, 8);
                            rec66.adjOfficerNo = row["adjOfficerNo"].ToString().Trim().PadRight(9, padSpaceChar).Substring(0, 9);

                            rec66.tranData = (rec66.refNo.PadLeft(15, pad0Char) + "~").Trim().PadRight(50, padSpaceChar).Substring(0, 50);

                            //dls 090708 - added from spec 1.06
                            rec66.branchCode = row["BranchCode"].ToString().Trim().PadLeft(2, pad0Char).Substring(0, 2);

                            // added from spec 1.09
                            rec66.taxiIndicatorFlag = row["TaxiIndicatorFlag"].ToString().Trim().PadLeft(1, pad0Char).Substring(0, 1);

                            //dls 090618 - added
                            //rec66.Filler6 = string.Empty.PadRight(100, padSpaceChar);
                            //rec66.Filler6 = string.Empty.PadRight(98, padSpaceChar);
                            rec66.Filler6 = string.Empty.PadRight(97, padSpaceChar);

                            // Output the record line
                            sw.WriteLine(rec66.recType + rec66.interfaceType + rec66.versionNo + rec66.supplierCode +
                                rec66.cameraID + rec66.autNo + rec66.Filler1 + rec66.filmNo + rec66.refNo + rec66.firstSpeed +
                                rec66.secondSpeed + rec66.officerNo + rec66.cameraLoc + rec66.courtNo +
                                rec66.offenceDate + rec66.Filler2 + rec66.Filler3 + rec66.Filler4 + rec66.regNo +
                                rec66.travelDirection + rec66.offenceCode + rec66.typeVehOwner + rec66.idNo + rec66.surname +
                                rec66.forenames + rec66.initials +
                                rec66.businessIDNo + rec66.businessName + rec66.Filler5 + rec66.gender +
                                rec66.organisationType + rec66.dateOfBirth +
                                rec66.streetAddrLine1 + rec66.streetAddrLine2 + rec66.streetAddrLine3 +
                                rec66.streetAddrLine4 + rec66.streetAddrLine5 + rec66.streetAddrCode +
                                rec66.postAddrLine1 + rec66.postAddrLine2 + rec66.postAddrLine3 +
                                rec66.postAddrLine4 + rec66.postAddrLine5 + rec66.postAddrCode +
                                rec66.telHome + rec66.telWork + rec66.fax + rec66.cell + rec66.email +
                                rec66.natisVehMCode + rec66.natisVehDescrCode + rec66.idType + rec66.countryOfIssue +
                                rec66.licenceDiscNo + rec66.prDPCode + rec66.driverLicenceCode +
                                rec66.operatorCardNo + rec66.vehicleGVM + rec66.offenceTime + rec66.amberTime + rec66.redTime +
                                rec66.dateCaptured + rec66.dateVerified + rec66.dateNatisReceived +
                                rec66.dateAdjudicated + rec66.adjOfficerNo + rec66.tranData + rec66.branchCode + rec66.taxiIndicatorFlag + rec66.Filler6);  //dls 090708 - added BranchNo

                            counter += 1;

                            // Update charge status values - stamp with filename, date and time
                            int success = notice.UpdateNoticeChargeStatus_CC(autNo, camUnitID, notIntNo, fileName, fileDate, STATUS_SENT_TO_CIVITAS, ref errMessage);

                            // Check if there's an error
                            if (success == -1)
                            {
                                Logger.Warning(ResourceHelper.GetResource("WriteInfringementFile_UpdateNoticeChargeStatusFailed") + errMessage);
                                failed = true;
                                sw.Close();
                                fs.Close();
                            }
                        }
                        else
                        {
                            sw.Close();
                            fs.Close();
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Error(ResourceHelper.GetResource("WriteInfringementFile_Failed") + e.Message + " " + exportFullPath + " " + DateTime.Now.ToString());
                        failed = true;
                        sw.Close();
                    }

                }   //end while

                if (!failed)
                {
                    if (counter > 0)
                    {
                        // Write out the up control record - add 1 to the counter to include the 54
                        sw.WriteLine("99" + "C" + "01" + (counter).ToString().PadLeft(6, pad0Char) + "000");
                        sw.Close();
                        fs.Close();

                        // Update batch with contravention date
                        errMessage = "";

                        int updCUBIntNo = batch.UpdateCameraUnitBatchColumn(autNo, camUnitID, fileName, this.AARTOBase.LastUser, cubIntNo, fileDate.ToString(), "CUBContraventionDate", ref errMessage);

                        if (updCUBIntNo < 1)
                        {
                            Logger.Warning(ResourceHelper.GetResource("WriteInfringementFile_UnableUpdateInfringement") + fileName + " - " + errMessage);
                            failed = true;
                        }

                        Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_WritingSucceeded", fileName, camUnitID.ToString()));
                    }
                    else
                    {
                        Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_NoRowsSucceeded", camUnitID.ToString()));
                    }
                }
                else
                {
                    Logger.Warning(ResourceHelper.GetResource("WriteInfringementFile_WritingInfringementFileFailed", fileName, camUnitID.ToString()));
                    failed = true;
                }
            }
            else
            {
                Logger.Info(ResourceHelper.GetResource("WriteInfringementFile_NoInfringementFile", camUnitID.ToString()));
            }

            noticeDS.Dispose();
            return failed;
        }
    }
}
