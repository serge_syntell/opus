﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CIP_CofCT_InfringementFileCreate
{
    partial class CIP_CofCT_InfringementFileCreate : ServiceHost
    {
        public CIP_CofCT_InfringementFileCreate()
            : base("", new Guid("ACF1966C-D0C5-4E3A-93F9-769628A943A8"), new CIP_CofCT_InfringementFileCreateService())
        {
            InitializeComponent();
        }
    }
}
