﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CancelExpiredNotice
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.CancelExpiredNotice"
                ,
                DisplayName = "SIL.AARTOService.CancelExpiredNotice"
                ,
                Description = "SIL AARTOService CancelExpiredNotice"
            };

            ProgramRun.InitializeService(new CancelExpiredNotice(), serviceDescriptor, args);
        }
    }
}
