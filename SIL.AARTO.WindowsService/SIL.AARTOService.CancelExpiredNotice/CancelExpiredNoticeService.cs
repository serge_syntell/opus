﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using SIL.AARTOService.Resource;
using Stalberg.TMS;

namespace SIL.AARTOService.CancelExpiredNotice
{
    public class CancelExpiredNoticeService : ServiceDataProcessViaQueue
    {
        public CancelExpiredNoticeService()
            : base("", "", new Guid("09744DB3-61FD-493B-BF76-C12169C4CDB0"), ServiceQueueTypeList.CancelExpiredNotice)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
        }

        private const string TICKET_PROCESSOR_AARTO = "AARTO";
        private const string TICKET_PROCESSOR_JMPD_AARTO = "JMPD_AARTO";

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        AuthorityDetails authDetails = null;
        string connectStr = string.Empty;
        string strAutCode = string.Empty;
        string autName = string.Empty;
        int autIntNo;

        int noDaysForNoticeExpiry;
        //int noDaysBeforeSummonsExpiry;

        public sealed override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && ServiceUtility.IsNumeric(body))
            {
                item.IsSuccessful = true;
            }
            else
            {
                // jerry 2012-02-21 change
                //item.IsSuccessful = false;
                //item.Status = QueueItemStatus.UnKnown;
                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.lastUser, item.Body));
                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.LastUser, item.Body), true);
            }

            if (strAutCode != item.Group)
            {
                strAutCode = item.Group.Trim();
                //jerry 2012-04-05 check group value
                if (string.IsNullOrWhiteSpace(strAutCode))
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
                    return;
                }

                this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode);
                autIntNo = authDetails.AutIntNo;
                autName = authDetails.AutName;

                //Jerry 2012-12-20 change
                int notIntNo = Convert.ToInt32(item.Body);
                SIL.AARTO.DAL.Entities.Notice noticeEntity = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(notIntNo);
                //if (this.authDetails.AutTicketProcessor == TICKET_PROCESSOR_AARTO ||
                //    this.authDetails.AutTicketProcessor == TICKET_PROCESSOR_JMPD_AARTO)
                if (noticeEntity != null && noticeEntity.NotTicketProcessor != "TMS")
                {
                    // jake 2013-07-12 added message when discard queue
                    item.Status = QueueItemStatus.Discard;
                    item.IsSuccessful = false;
                    this.Logger.Info(String.Format("Invalid notice ticket processor  {0}", (noticeEntity == null ? "" : noticeEntity.NotTicketProcessor)), item.Body.ToString());
                }

                rules.InitParameter(connectStr, strAutCode);
            }
        }

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            int notIntNo, success;
            string msg = string.Empty;

            // jerry 2012-02-21 change
            //foreach (QueueItem item in queueList)
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                try
                {
                    if (item.IsSuccessful)
                    {
                        notIntNo = Convert.ToInt32(item.Body);

                        //get notice expiry date
                        noDaysForNoticeExpiry = rules.Rule("NotOffenceDate", "NotExpireDate").DtRNoOfDays;

                        //get no longer able to summons
                        //noDaysBeforeSummonsExpiry = rules.Rule("NotOffenceDate", "NotSummonsBeforeDate").DtRNoOfDays;

                        success = UpdateExpiredNotice(notIntNo, ref msg);
                        switch (success)
                        {
                            case -1:
                                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrMsgUpdateChargeInCancelExpiredNotices"), notIntNo));
                                //item.Status = QueueItemStatus.UnKnown;
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrMsgUpdateChargeInCancelExpiredNotices"), notIntNo));
                                break;
                            case -2:
                                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrMsgUpdateNoticeInCancelExpiredNotices"), notIntNo));
                                //item.Status = QueueItemStatus.UnKnown;
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrMsgUpdateNoticeInCancelExpiredNotices"), notIntNo));
                                break;
                            case -3:
                                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrMsgUpdateEvidenceInCancelExpiredNotices"), notIntNo));
                                //item.Status = QueueItemStatus.UnKnown;
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrMsgUpdateEvidenceInCancelExpiredNotices"), notIntNo));
                                break;
                            case -4:
                                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrMsgExeSPInCancelExpiredNotices"), msg, notIntNo));
                                //item.Status = QueueItemStatus.UnKnown;
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrMsgExeSPInCancelExpiredNotices"), msg, notIntNo), true);
                                break;
                            case 0:
                                item.Status = QueueItemStatus.Discard;
                                this.Logger.Info("Update Expired Notice failed and function returned 0; Queue key:" + item.Body.ToString());
                                break;
                            case 1:
                                this.Logger.Info(string.Format(ResourceHelper.GetResource("SuccessfulMsgInCancelExpiredNotices"), notIntNo));
                                item.Status = QueueItemStatus.Discard;
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //item.Status = QueueItemStatus.UnKnown;
                    //this.Logger.Error(ex);
                    //throw ex;
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        private int UpdateExpiredNotice(int notIntNo, ref string errMsg)
        {
            DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));

            SqlParameter[] paras = new SqlParameter[3];
            paras[0] = new SqlParameter("@NotIntNo", notIntNo);
            paras[1] = new SqlParameter("@NoDaysForNoticeExpiry", noDaysForNoticeExpiry);
            paras[2] = new SqlParameter("@LastUser", AARTOBase.LastUser);

            object obj = db.ExecuteScalar("NoticeExpired_WS", paras, out errMsg);
            int result;
            if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(errMsg))
                result = Convert.ToInt32(obj);
            else
                result = -4;

            return result;
        }
    }
}
