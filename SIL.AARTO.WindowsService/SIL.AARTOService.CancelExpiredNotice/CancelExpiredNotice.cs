﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CancelExpiredNotice
{
    partial class CancelExpiredNotice : ServiceHost
    {
        public CancelExpiredNotice()
            : base("", new Guid("CFD6E220-2953-41CA-A17E-270CE70B0BA7"), new CancelExpiredNoticeService())
        {
            InitializeComponent();
        }
    }
}
