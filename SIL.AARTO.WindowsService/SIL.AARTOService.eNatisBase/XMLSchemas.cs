﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.IO;
using System.Web;
using System.Text;

namespace SIL.AARTOService.eNatisBase
{
    public class XMLSchemas
    {
        /// <summary>
        /// Lists the XSD types
        /// </summary>
        public enum XmlSchemaSetType
        {
            Default,
            X1001Req,
            X1001Resp,
            X1002Req,
            X1002Resp,
            ProcessCameraFileReq,
            ProcessCameraFileResp,
            X4000Resp
        }

        // Static
        static XmlSchemaSet m_XmlSchemaSet_X1001Req;
        static XmlSchemaSet m_XmlSchemaSet_X1001Resp;
        static XmlSchemaSet m_XmlSchemaSet_X3048Req;
        static XmlSchemaSet m_XmlSchemaSet_X3048Resp;
        static XmlSchemaSet m_XmlSchemaSet_X4000Resp;
        static XmlSchemaSet m_XmlSchemaSet_X1002Req;
        static XmlSchemaSet m_XmlSchemaSet_X1002Resp;
        static bool bReadOK = true;

        public static XmlSchemaSet GetXSDset(XmlSchemaSetType xmlSchemaSetType)
        {
            XmlSchemaSet m_XmlSchemaSet = null;

            if (xmlSchemaSetType == XmlSchemaSetType.X1001Req)
                m_XmlSchemaSet = m_XmlSchemaSet_X1001Req;
            else if (xmlSchemaSetType == XmlSchemaSetType.X1001Resp)
                m_XmlSchemaSet = m_XmlSchemaSet_X1001Resp;
            else if (xmlSchemaSetType == XmlSchemaSetType.ProcessCameraFileReq)
                m_XmlSchemaSet = m_XmlSchemaSet_X3048Req;
            else if (xmlSchemaSetType == XmlSchemaSetType.ProcessCameraFileResp)
                m_XmlSchemaSet = m_XmlSchemaSet_X3048Resp;
            else if (xmlSchemaSetType == XmlSchemaSetType.X4000Resp)
                m_XmlSchemaSet = m_XmlSchemaSet_X4000Resp;
            else if (xmlSchemaSetType == XmlSchemaSetType.X1002Req)
                m_XmlSchemaSet = m_XmlSchemaSet_X1002Req;
            else if (xmlSchemaSetType == XmlSchemaSetType.X1002Resp)
                m_XmlSchemaSet = m_XmlSchemaSet_X1002Resp;

            if (m_XmlSchemaSet == null)
            {
                m_XmlSchemaSet = new XmlSchemaSet();

                string strFolder = xmlSchemaSetType.ToString().Substring(0, 5);

                if (xmlSchemaSetType == XmlSchemaSetType.ProcessCameraFileReq || xmlSchemaSetType == XmlSchemaSetType.ProcessCameraFileResp)
                    strFolder = "X3048";

                string strFile = Directory.GetCurrentDirectory() + "\\XSD\\" + strFolder + "\\" + xmlSchemaSetType.ToString() + ".xsd";
                LoadSchema(strFile, m_XmlSchemaSet);
            }

            return m_XmlSchemaSet;
        }

        private static void LoadSchema(string strFile, XmlSchemaSet m_XmlSchemaSet)
        {
            try
            {
                bReadOK = true;
                XmlTextReader reader = new XmlTextReader(strFile);

                XmlSchema xmlSchema = XmlSchema.Read(reader, ValidationCallBack);

                if (bReadOK)
                    m_XmlSchemaSet.Add(xmlSchema);
            }
            catch (Exception ex)
            {
                Console.WriteLine("\tWarning: schema error." + ex.Message);
                throw ex;
            }
        }
        // Display any warnings or errors.
        private static void ValidationCallBack(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
                Console.WriteLine("\tWarning: Matching schema not found.  No validation occurred." + args.Message);
            else
            {
                bReadOK = false;
                Console.WriteLine("\tValidation error: " + args.Message);
            }

        }



         private static void ProcessSchemaComplexType(XmlSchemaComplexType ct)
         {      
                 //XmlSchemaComplexType ct = parentElement.ElementType as XmlSchemaComplexType; //Casting to complex type
                 if (ct != null)
                 {
                     XmlSchemaSequence seq = (XmlSchemaSequence)ct.ContentTypeParticle; 

                     foreach (XmlSchemaParticle p in seq.Items)
                     {
                         XmlSchemaElement elem = p as XmlSchemaElement;
                         string strName = elem.Name;
                         if (elem != null)
                         {
                             if (elem.ElementSchemaType.DerivedBy == XmlSchemaDerivationMethod.Restriction)
                             {
                                 XmlSchemaSimpleType st = elem.ElementSchemaType as XmlSchemaSimpleType;
                                 if (st != null)
                                 {
                                     ProcessSchemaSimpleType(st);
                                 }

                                 XmlSchemaComplexType ctype = elem.ElementSchemaType as XmlSchemaComplexType;
                                 if (ctype != null)
                                     ProcessSchemaComplexType(ctype);

                             }

                             foreach (XmlSchemaObject xmlObject in elem.Constraints)
                             {
                                 XmlSchemaComplexType ctype = xmlObject as XmlSchemaComplexType; //Casting to complex type
                                 if (ctype != null)
                                     ProcessSchemaComplexType(ctype);

                                 XmlSchemaSimpleType stype = xmlObject as XmlSchemaSimpleType;
                                 if (stype != null)
                                     ProcessSchemaSimpleType(stype);


                             }
                         }
                     }
                 }
             
         }


         private static void ProcessSchemaSimpleType(XmlSchemaSimpleType st)
         {
             XmlSchemaSimpleTypeRestriction restriction = st.Content as XmlSchemaSimpleTypeRestriction;
             foreach (XmlSchemaObject xmlObject in restriction.Facets)
             {
                 if (xmlObject.GetType().Name == "XmlSchemaPatternFacet")
                 {
                     XmlSchemaPatternFacet facet = xmlObject as XmlSchemaPatternFacet;

                     string value = HttpUtility.HtmlDecode(facet.Value);
                     value = value.Replace('‘', '\'').Replace('’', '\'').Replace('，', ',');
                     
                     facet.Value = value;
                 }
             }

             XmlSchemaSimpleType stype = restriction.BaseType as XmlSchemaSimpleType;
             if (stype != null)
                 ProcessSchemaSimpleType(stype);
             
         }


        private static void ProcessSchema(XmlSchema xmlSchema)
        {
            xmlSchema.Compile(new ValidationEventHandler(ValidationCallBack));
            if (xmlSchema.IsCompiled)
            {
                foreach (XmlSchemaElement parentElement in xmlSchema.Elements.Values)
                {
                    XmlSchemaComplexType ct = parentElement.ElementType as XmlSchemaComplexType; //Casting to complex type
                    ProcessSchemaComplexType(ct);
                }

                //foreach (XmlSchemaInclude parentObject in xmlSchema.Includes)
                //{
                //   XmlSchema xmlSchemaInclude = parentObject.Schema;
                //   if (xmlSchemaInclude != null)
                //       ProcessSchema(xmlSchemaInclude);
                //}
 
            }
        }

        public static void TestValidation()
        {

            try
            {
                bReadOK = true;

                XmlTextReader reader = new XmlTextReader(@"E:\Code\SIL.eNaTIS\SIL.eNaTIS.WinService\SIL.eNaTIS.WinService\XSD\X1001\X1001Resp.xsd");
                XmlSchema xmlSchema = XmlSchema.Read(reader, ValidationCallBack);

                ProcessSchema(xmlSchema);

                XmlSchemaSet xmlSchemaSet = new XmlSchemaSet();
                xmlSchemaSet.Add(xmlSchema);
                

                //Validate 
                try
                {
                    bReadOK = true;
                    //xml.Save(stream);

                    Byte[] bytes = ASCIIEncoding.UTF8.GetBytes(@"
<X1001Resp><StdRespHeader><TxanName>X1001</TxanName><ESTxanID>32</ESTxanID><ESTermID>00000010</ESTermID></StdRespHeader>
<Vehicle><VehicleDet><MVRegN>BBB002F</MVRegN><VinOrChassis>AA932100000000000</VinOrChassis><EngineN>AA9332211</EngineN>
<SAPMark><Code>06</Code><Desc>Theft of vehicle</Desc></SAPMark><ModelName><Code>A997</Code><Desc>Toyota Hino 15258</Desc></ModelName>
<MVCat><Code>L</Code><Desc>Heavy load veh(GVM&gt;3500Kg, not to draw)</Desc></MVCat><Driven><Code>1</Code><Desc>Self-propelled</Desc></Driven><MVDesc><Code>52</Code><Desc>Truck tractor</Desc></MVDesc><Make><Code>M66</Code><Desc>M&amp;P Bodies</Desc></Make><MainColour><Code>03</Code><Desc>White</Desc></MainColour><GVM>15500</GVM><EngineDisp>7961</EngineDisp><Tare>99999</Tare><FuelType><Code>02</Code><Desc>Diesel</Desc></FuelType><NetPower>184</NetPower><MVRegtD>2006-10-20</MVRegtD><MVRegtTypeQual><Code>9</Code><Desc>None</Desc></MVRegtTypeQual><RWStat><Code>2</Code><Desc>Not roadworthy</Desc></RWStat><MVState><Code>06</Code><Desc>Registered (Liable for licensing)</Desc></MVState><MVLifeStat><Code>2</Code><Desc>Used</Desc></MVLifeStat><NModelN>536062</NModelN><SAPClrStat><Code>9</Code><Desc>None</Desc></SAPClrStat><MVUsage><Code>00</Code><Desc>Unknown</Desc></MVUsage><MVEconSector><Code>00</Code><Desc>Unknown</Desc></MVEconSector><PrevMVCertN>100100000008</PrevMVCertN><VehType><Code>09</Code><Desc>Truck - articulated</Desc></VehType><VehUsage><Desc>Unknown</Desc></VehUsage><MVRegtType><Code>2</Code><Desc>Registered</Desc></MVRegtType><Exemption><Code>99</Code><Desc>None</Desc></Exemption><RoadUseInd>Y</RoadUseInd></VehicleDet><Owner><PerDet><PerId><IdDocType><Code>04</Code><Desc>Business reg certificate</Desc></IdDocType><IdDocN>9587654070016</IdDocN>
<BusOrSurname>DEALER' ËÉÄÖÜçNEL</BusOrSurname></PerId><PerIdN>203700000006</PerIdN><NatOfPer><Code>10</Code><Desc>Public company</Desc></NatOfPer><NatPopGrp><Code>ZA</Code><Desc>South Africa</Desc></NatPopGrp><PostAddr><Addr1>PO BOX 98</Addr1><PostalCd><Code>8000</Code><Desc>Cape Town-City of Cape Town</Desc></PostalCd></PostAddr><DCEEAddr>1</DCEEAddr><ChgOfAddrD>2006-11-02</ChgOfAddrD><Exemption><Code>99</Code><Desc>None</Desc></Exemption><OperCatg><Code>G</Code><Desc>Goods</Desc></OperCatg></PerDet><RelDet><RelStartD>2006-10-20</RelStartD><CurrStat><Code>3</Code><Desc>Current</Desc></CurrStat><StatNat><Code>3</Code><Desc>MD Stock</Desc></StatNat></RelDet></Owner><TitleHolder><PerDet><OperCatg><Code>G</Code><Desc>Goods</Desc></OperCatg></PerDet></TitleHolder><OwnerProxy><PerDet><PerId><IdDocType><Code>03</Code><Desc>Foreign ID document</Desc></IdDocType><IdDocN>BLUE1</IdDocN><Initials>B</Initials><BusOrSurname>BLUE1</BusOrSurname></PerId><PerIdN>203700000007</PerIdN><NatOfPer><Code>02</Code><Desc>Female</Desc></NatOfPer><NatPopGrp><Code>ES</Code><Desc>Spain</Desc></NatPopGrp><PostAddr><Addr1>POSTAL 1</Addr1><Addr4>PRETORIA</Addr4><Addr5>PRETORIA</Addr5><PostalCd><Code>0010</Code><Desc>Pretoria-Pretoria</Desc></PostalCd></PostAddr><StreetAddr><Addr1>POSTAL 1</Addr1><Addr2>PRETORIA</Addr2><Addr3>PRETORIA</Addr3><PostalCd><Code>0010</Code><Desc>Pretoria-Pretoria</Desc></PostalCd></StreetAddr><DCEEAddr>2</DCEEAddr><ChgOfAddrD>2010-03-25</ChgOfAddrD></PerDet></OwnerProxy><OwnerRep><PerDet><PerId><IdDocType><Code>03</Code><Desc>Foreign ID document</Desc></IdDocType><IdDocN>BLUE1</IdDocN><Initials>B</Initials><BusOrSurname>BLUE1</BusOrSurname></PerId><PerIdN>203700000007</PerIdN><NatOfPer><Code>02</Code><Desc>Female</Desc></NatOfPer><NatPopGrp><Code>ES</Code><Desc>Spain</Desc></NatPopGrp><PostAddr><Addr1>POSTAL 1</Addr1><Addr4>PRETORIA</Addr4><Addr5>PRETORIA</Addr5><PostalCd><Code>0010</Code><Desc>Pretoria-Pretoria</Desc></PostalCd></PostAddr><StreetAddr><Addr1>POSTAL 1</Addr1><Addr2>PRETORIA</Addr2><Addr3>PRETORIA</Addr3><PostalCd><Code>0010</Code><Desc>Pretoria-Pretoria</Desc></PostalCd></StreetAddr><DCEEAddr>2</DCEEAddr><ChgOfAddrD>2010-03-25</ChgOfAddrD></PerDet></OwnerRep><PrevOwner><IdDocN>9732165070010</IdDocN><OwnerShipD>2006-09-08</OwnerShipD></PrevOwner></Vehicle></X1001Resp>");
                    MemoryStream mstream = new MemoryStream(bytes);


                    XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
                    xmlReaderSettings.ValidationType = ValidationType.Schema;
                    //xmlReaderSettings.Schemas = GetXSDset(XmlSchemaSetType.X1001Resp);
                    xmlReaderSettings.Schemas = xmlSchemaSet;
                    xmlReaderSettings.ValidationEventHandler += new ValidationEventHandler(ValidationCallBack);

                    XmlReader xmlReader = XmlReader.Create(mstream, xmlReaderSettings);
                    while (xmlReader.Read()) ;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("\tWarning: schema error." + ex.Message);
                throw ex;
            }
        
        }

        
    }
}
