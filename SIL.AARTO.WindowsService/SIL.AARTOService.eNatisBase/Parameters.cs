﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTOService.eNatisBase
{
    public class Parameters
    {
        #region X3048 Notice

        static string emailToAdministrator;
        public static string EmailToAdministrator
        {
            get { return emailToAdministrator; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("EmailToAdministrator", "Invalid service parameter 'EmailToAdministrator'");
                emailToAdministrator = value;
            }
        }

        //static string authNo212;
        //public static string AuthNo212
        //{
        //    get { return authNo212; }
        //    set
        //    {
        //        if (string.IsNullOrWhiteSpace(value))
        //            throw new ArgumentNullException("AuthNo212", "Invalid service parameter 'AuthNo212'");
        //        authNo212 = value;
        //    }
        //}

        //static string authNo214;
        //public static string AuthNo214
        //{
        //    get { return authNo214; }
        //    set
        //    {
        //        if (string.IsNullOrWhiteSpace(value))
        //            throw new ArgumentNullException("AuthNo214", "Invalid service parameter 'AuthNo214'");
        //        authNo214 = value;
        //    }
        //}

        //static string authNo166;
        //public static string AuthNo166
        //{
        //    get { return authNo166; }
        //    set
        //    {
        //        if (string.IsNullOrWhiteSpace(value))
        //            throw new ArgumentNullException("AuthNo166", "Invalid service parameter 'AuthNo166'");
        //        authNo166 = value;
        //    }
        //}

        static string url_CF;
        public static string URL_CF
        {
            get { return url_CF; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("URL_CF", "Invalid service parameter 'URL_CF'");
                url_CF = value;
            }
        }

        #endregion


        #region X3048 & X1007

        static string uriBase;
        public static string URIBase
        {
            get { return uriBase; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("URIBase", "Invalid service parameter 'URIBase'");
                uriBase = value;
            }
        }

        static string userBase;
        public static string UserBase
        {
            get { return userBase; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("UserBase", "Invalid service parameter 'UserBase'");
                userBase = value;
            }
        }

        #endregion


        #region X1007 Frame

        static string url;
        public static string URL
        {
            get { return url; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("URL", "Invalid service parameter 'URL'");
                url = value;
            }
        }

        static int port;
        public static int Port
        {
            get { return port; }
            set
            {
                if (value <= 0)
                    throw new ArgumentNullException("Port", "Invalid service parameter 'Port'");
                port = value;
            }
        }

        static string certFile;
        public static string CertFile
        {
            get { return certFile; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("CertFile", "Invalid service parameter 'CertFile'");
                certFile = value;
            }
        }

        #endregion
    }
}
