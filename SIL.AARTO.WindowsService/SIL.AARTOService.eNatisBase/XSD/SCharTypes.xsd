<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Module Name    :  SCharTypes
 * Description    	:  XML schema document for the SCHAR data types
 * File Name      	:  SCharTypes.xsd
 * Date Created   	:  2007-07-26
 * Author         		:  Jacques Nortje
 * Version				:	1.0
 -->
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
	<xsd:simpleType name="ALPHA">
		<xsd:annotation>
			<xsd:documentation>Only characters allowed: A-Z</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:pattern value="[A-Z]+"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="NUM">
		<xsd:annotation>
			<xsd:documentation>Only characters allowed: 0-9</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:pattern value="[\d]+"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="SChar1">
		<xsd:annotation>
			<xsd:documentation>Only characters allowed: SPACE, A-Z, 0-9,-,',Ë,É,Ä,Ö,Ü,Ç,amp</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:whiteSpace value="preserve"/>
			<xsd:pattern value="[\s\dA-Z\-\'\(\)&#8216;&#8217;&amp;&#203;&#201;&#196;&#214;&#220;&#231;]+"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="SChar2">
		<xsd:annotation>
			<xsd:documentation>Only characters allowed: A-Z, 0-9</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:whiteSpace value="collapse"/>
			<xsd:pattern value="[\dA-Z]+"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="SChar3">
		<xsd:annotation>
			<xsd:documentation>Only characters allowed: SPACE, A-Z, 0-9</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:whiteSpace value="preserve"/>
			<xsd:pattern value="[\s\dA-Z]+"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="SChar4">
		<xsd:annotation>
			<xsd:documentation>Only characters allowed: ., A-Z, 0-9, @, _, -</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:whiteSpace value="preserve"/>
			<xsd:pattern value="[\dA-Z\-&#95;&#64;&#46;]+"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="SChar5">
		<xsd:annotation>
			<xsd:documentation>Only alphabetic characters are allowed, i.e. A-Z, excluding A, E, I, O, U, Q</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:pattern value="[BCDFGHJKLMNPRSTVWXYZ]+"/>
			<!--<xsd:pattern value="[A-Z^&#65;^&#69;^&#73;^&#79;^&#85;^&#81;]+"/>-->
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="SChar6">
		<xsd:annotation>
			<xsd:documentation>Only characters allowed: Y, N</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:pattern value="[&#89;&#78;]{1}"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="SChar7">
		<xsd:annotation>
			<xsd:documentation>Only characters allowed: Y, N, SPACE</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:pattern value="[&#89;&#78;]{1}\s+"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="SChar8">
		<xsd:annotation>
			<xsd:documentation>Alphanumeric, i.e A-Z, 0-9 excluding E, I, O, U, Q</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:pattern value="[\dABCDFGHJKLMNPRSTVWXYZ]+"/>
			<!--<xsd:pattern value="[\dA-Z^&#69;^&#73;^&#79;^&#85;^&#81;]+"/>-->
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="SChar9">
		<xsd:annotation>
			<xsd:documentation>Alphanumeric, i.e A-Z, 0-9 excluding I, O, U, Q</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:pattern value="[\dABCDEFGHJKLMNPRSTVWXYZ]+"/>
			<!--<xsd:pattern value="[\dA-Z^&#73;^&#79;^&#85;^&#81;]+"/>-->
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="SChar10">
		<xsd:annotation>
			<xsd:documentation>Alphanumeric, i.e A-Z, 0-9 excluding A, E, I, O, U, Q (LastN generated values)</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:whiteSpace value="collapse"/>
			<xsd:pattern value="[\dBCDFGHJKLMNPRSTVWXYZ]+"/>
			<!--<xsd:pattern value="[\dA-Z^&#65;^&#69;^&#73;^&#79;^&#85;^&#81;]+"/>-->
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="SChar11">
		<xsd:annotation>
			<xsd:documentation>Format AAANNNB</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:pattern value="[BCDFGHJKLMNPRSTVWXYZ]{3}[0-9]{3}[A-Z]{1}"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="SChar12">
		<xsd:annotation>
			<xsd:documentation>Format ANNNNNN</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:pattern value="[A-Z]{1}[\dA-Z]{6}"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="SChar13">
		<xsd:annotation>
			<xsd:documentation>Only characters allowed: A-Z, 0-9, SPACE, !, ", #, $, %, amp, ', (, ), *, +, ,, -, ., /, :, ;, lt, =,gt, ?, @, [, \, ], ^, _</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:pattern value="[\s\dA-Z&#33;&#34;&#35;&#36;&#37;&#38;&#39;&#40;&#41;&#42;&#43;&#44;&#45;&#46;&#47;&#58;&#59;&#60;&#61;&#62;&#63;&#64;&#92;&#94;&#95;\]\[]+"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="SChar14">
		<xsd:annotation>
			<xsd:documentation>Only characters allowed: A-Z, 0-9, .,-</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:pattern value="[\dA-Z\-\.]+"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="Money">
		<xsd:annotation>
			<xsd:documentation>Decimal amounts. Greater or equal to 0.00 and less or equal to 99999999.99</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:decimal">
			<xsd:whiteSpace value="collapse"/>
			<xsd:minInclusive value="0.00"/>
			<xsd:maxInclusive value="99999999.99"/>
			<xsd:fractionDigits value="2"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="DateTime">
		<xsd:annotation>
			<xsd:documentation>Date and time</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:pattern value="[\d]{4}[\-]{1}[\d]{2}[\-]{1}[\d]{2}[\s]{1}[\d]{2}[:]{1}[\d]{2}[:]{1}[\d]{2}"/>
		</xsd:restriction>
	</xsd:simpleType>
</xsd:schema>
