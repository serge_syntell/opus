﻿using System;
using System.Text;
using SIL.AARTOService.eNatisBase.Client;
using SIL.eNaTIS.DAL.Entities;
//using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Diagnostics;
using System.Xml;
using System.Xml.Schema;
using System.IO;
using SIL.ServiceBase;

namespace SIL.AARTOService.eNatisBase
{
    class ENaTISResponse
    {
        //Fields
        private string sAutNo = "";
        private TList<ENatisUser> _users = new TList<ENatisUser>();

        /// <summary>
        ///  6a. Send eNaTIS response data
        /// </summary>
        public bool SendeNaTISResponseData(string strRequest, string strResponse, Guid guid, DateTime dtSend, DateTime dtResponse, string autNo)
        {
            //BD changes for autNo and not Authority ID

            bool failed = false;

            //GeteNaTISUser
            eNaTISUser eNaTISUser = new eNaTISUser();
            //TList<ENatisUser> users = eNaTISUser.geteNaTISUser(autNo);
            //BD need to check for the correct eNatis process
            TList<ENatisUser> users = eNaTISUser.geteNaTISUser(autNo, "ifprod");

            if (users.Count < 1)
            {
                Logger.Write("SendeNaTISResponseData: Get eNaTISUser failed!", "General", (int)TraceEventType.Error, 0, TraceEventType.Error);
                failed = true;
                return failed;
            }
            ENatisUser user = users[0];

            //6b. Update eNaTIS Queue record with response xml
            eNaTISQueue eNaTISQueue = new eNaTISQueue();
            ENatisQueue eNatisQueue;
            string queueType = "";
            if (strRequest.IndexOf("X1001Req") > 0)
                queueType = "X1001";
            if (strRequest.IndexOf("X1002Req") > 0)
                queueType = "X1002";
            //if (strResponse.IndexOf("X4000Resp") > 0)
            //    queueType = "X4000";
            TList<ENatisQueue> eNatisQueues = eNaTISQueue.GeteNaTISQueueByGuid(guid);
            //TList<ENatisQueue> eNatisQueues = eNaTISQueue.GeteNaTISQueueByGuidByStatus(guid, queueType);
            if (eNatisQueues.Count > 0)
            {
                eNatisQueue = eNatisQueues[0];
                //if (queueType != "X4000")
                //{
                for (int i = 0; i < eNatisQueues.Count; i++)
                {
                    if (eNatisQueues[i].SentData.IndexOf(queueType) > 0)
                    {
                        eNatisQueue = eNatisQueues[i];
                        continue;
                    }
                }
                //}
                //foreach (eNaTISQueue eQueue in eNatisQueues)
                //{
                //    if ((eQueue.ToString().IndexOf("X1001") > 0) && (queueType = "X1001"))
                //    {

                //    }
                //}
            }
            else
            {
                eNatisQueue = new ENatisQueue();
                eNatisQueue.ENaTisGuid = guid;
            }

            eNatisQueue.ENatisUserId = user.ENatisUserId;
            eNatisQueue.SentDateTime = dtSend;
            eNatisQueue.SentData = strRequest;

            int iXmlHead = strResponse.IndexOf(">");
            if (iXmlHead > 0)
            {
                strResponse = strResponse.Substring(iXmlHead + 1, strResponse.Length - iXmlHead - 1);
            }

            eNatisQueue.RespDataRaw = strResponse;
            eNatisQueue.RespDateTime = dtResponse;

            if (strResponse == null)
            {
                eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.NOT_LOGGED_IN);
                eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Failed);
                Logger.Write("SendeNaTISResponseData: connect to eNaTIS failed, NOT_LOGGED_IN!", "General", (int)TraceEventType.Error, 0, TraceEventType.Warning);
            }
            else if (strRequest.IndexOf("X1001") > 0)
            {
                if (strResponse.IndexOf("X4000Resp") > 0)
                {
                    eNatisQueue.RespTranName = "X4000";
                    if (strResponse.IndexOf(RespTranStateList.NOT_LOGGED_IN.ToString()) > 0)
                    {
                        //6d. Add to queue for reprocessing
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.NOT_LOGGED_IN);
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Sent);
                    }
                    else if (strResponse.IndexOf(RespTranStateList.FIELD_INVALID.ToString()) > 0)
                    {
                        //Go to TMS
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.FIELD_INVALID);
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Failed);
                    }
                    else if (strResponse.IndexOf(RespTranStateList.MAX_SESSIONS_IN_USE.ToString()) > 0)
                    {
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.MAX_SESSIONS_IN_USE);
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Sent);
                    }
                    else if (strResponse.IndexOf(RespTranStateList.NATIS_INTERNAL_ERROR.ToString()) > 0)
                    {
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.NATIS_INTERNAL_ERROR);
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Sent);
                    }
                    else if (strResponse.IndexOf(RespTranStateList.TXAN_NOT_AUTHORISED.ToString()) > 0)
                    {
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.TXAN_NOT_AUTHORISED);
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Sent);
                    }
                    else if (strResponse.IndexOf(RespTranStateList.USER_AUTHENTICATION_ERROR.ToString()) > 0)
                    {
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.USER_AUTHENTICATION_ERROR);
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Sent);
                    }
                    else if (strResponse.IndexOf(RespTranStateList.XML_INVALID.ToString()) > 0)
                    {
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.XML_INVALID);
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Failed);
                    }
                }
                else
                {
                    if (!eNaTISQueue.ValidateXml(strResponse, XMLSchemas.GetXSDset(XMLSchemas.XmlSchemaSetType.X1001Resp)))
                    {
                        //6d. Add to queue for reprocessing
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.XML_INVALID);
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Sent);
                        Logger.Write("SendeNaTISResponseData: eNatis response XML invalid", "General", (int)TraceEventType.Error, 0, TraceEventType.Warning);
                    }
                    else
                    {
                        //BD if the owner is not linked to a proxy/rep, create a queue item for the person queue
                        //check for OwnerProxy or OwnerRep
                        //if (strResponse.IndexOf("X1001") > 0)
                        //{
                        //if (!(strResponse.IndexOf("OwnerProxy") > 0) && !(strResponse.IndexOf("OwnerRep") > 0))
                        //{
                        //    //Need to send the ID number for x1002
                        //    //<Owner>
                        //    //  <PerDet>
                        //    //    <PerId>
                        //    //      <IdDocType>
                        //    //        <Code>02</Code>
                        //    //        <Desc>RSA ID document</Desc>
                        //    //      </IdDocType>
                        //    //      <IdDocN>4049167460007</IdDocN>
                        //    string PersonId = strResponse.Substring(strResponse.IndexOf("<IdDocN>") + 8, 13);

                        //    //Create xml file to be sent to eNaTIS for x1002
                        //    string strX1002 = CreateXMLForX1002(PersonId, autNo);
                        //    //Add xml file to a new thread
                        //    SendX1002QueueRecord(guid, autNo, strX1002);
                        //}

                        //6e. Validate eNaTIS response
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.SUCCESS);
                        eNatisQueue.RespTranName = GetRespTranName(strResponse);

                        //6f. Update Queue record
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Success);
                        eNatisQueue.RespDataValidated = AddValidateScheme(strResponse, XMLSchemas.GetXSDset(XMLSchemas.XmlSchemaSetType.X1001Resp));
                    }
                }
            }
            //need to now check for X1001 and X1002
            //else if (strResponse.IndexOf("X1001Resp") > 0)
            //{
            //    if (!eNaTISQueue.ValidateXml(strResponse, XMLSchemas.GetXSDset(XMLSchemas.XmlSchemaSetType.X1001Resp)))
            //    {
            //        //6d. Add to queue for reprocessing
            //        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.XML_INVALID);
            //        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Sent);
            //        Logger.Write("SendeNaTISResponseData: eNatis response XML invalid", "General", (int)TraceEventType.Error, 0, TraceEventType.Warning);
            //    }
            //    else
            //    {
            //        //BD if the owner is not linked to a proxy/rep, create a queue item for the person queue
            //        //check for OwnerProxy or OwnerRep
            //        //if (strResponse.IndexOf("X1001") > 0)
            //        //{
            //        if (!(strResponse.IndexOf("OwnerProxy") > 0) && !(strResponse.IndexOf("OwnerRep") > 0))
            //        {
            //            //Need to send the ID number for x1002
            //            //<Owner>
            //            //  <PerDet>
            //            //    <PerId>
            //            //      <IdDocType>
            //            //        <Code>02</Code>
            //            //        <Desc>RSA ID document</Desc>
            //            //      </IdDocType>
            //            //      <IdDocN>4049167460007</IdDocN>
            //            string PersonId = strResponse.Substring(strResponse.IndexOf("<IdDocN>") + 8, 13);

            //            //Create xml file to be sent to eNaTIS for x1002
            //            string strX1002 = CreateXMLForX1002(PersonId, autNo);
            //            //Add xml file to a new thread
            //            SendX1002QueueRecord(guid, autNo, strX1002);
            //        }
            //        //}

            //        //6e. Validate eNaTIS response
            //        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.SUCCESS);
            //        eNatisQueue.RespTranName = GetRespTranName(strResponse);

            //        //6f. Update Queue record
            //        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Success);
            //        eNatisQueue.RespDataValidated = AddValidateScheme(strResponse, XMLSchemas.GetXSDset(XMLSchemas.XmlSchemaSetType.X1001Resp));
            //    }
            //}
            else if (strRequest.IndexOf("X1002") > 0)
            {
                if (strResponse.IndexOf("X4000Resp") > 0)
                {
                    eNatisQueue.RespTranName = "X4000";
                    if (strResponse.IndexOf(RespTranStateList.NOT_LOGGED_IN.ToString()) > 0)
                    {
                        //6d. Add to queue for reprocessing
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.NOT_LOGGED_IN);
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Person);
                    }
                    else if (strResponse.IndexOf(RespTranStateList.FIELD_INVALID.ToString()) > 0)
                    {
                        //Go to TMS
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.FIELD_INVALID);
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.PFailed);
                    }
                    else if (strResponse.IndexOf(RespTranStateList.MAX_SESSIONS_IN_USE.ToString()) > 0)
                    {
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.MAX_SESSIONS_IN_USE);
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Person);
                    }
                    else if (strResponse.IndexOf(RespTranStateList.NATIS_INTERNAL_ERROR.ToString()) > 0)
                    {
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.NATIS_INTERNAL_ERROR);
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Person);
                    }
                    else if (strResponse.IndexOf(RespTranStateList.TXAN_NOT_AUTHORISED.ToString()) > 0)
                    {
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.TXAN_NOT_AUTHORISED);
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Person);
                    }
                    else if (strResponse.IndexOf(RespTranStateList.USER_AUTHENTICATION_ERROR.ToString()) > 0)
                    {
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.USER_AUTHENTICATION_ERROR);
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Person);
                    }
                    else if (strResponse.IndexOf(RespTranStateList.XML_INVALID.ToString()) > 0)
                    {
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.XML_INVALID);
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.PFailed);
                    }
                }
                else
                {
                    if (!eNaTISQueue.ValidateXml(strResponse, XMLSchemas.GetXSDset(XMLSchemas.XmlSchemaSetType.X1002Resp)))
                    {
                        //6d. Failed set accordingly
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.XML_INVALID);
                        //eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Person);
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.PFailed);
                        eNatisQueue.RespTranName = "X1002";
                        Logger.Write("SendeNaTISResponseData: eNatis response XML invalid", "General", (int)TraceEventType.Error, 0, TraceEventType.Warning);
                    }
                    else
                    {
                        //6e. Validate eNaTIS response
                        eNatisQueue.RespTranStateId = Convert.ToInt32(RespTranStateList.SUCCESS);
                        eNatisQueue.RespTranName = "X1002";

                        //6f. Update Queue record
                        eNatisQueue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.PSuccess);
                        eNatisQueue.RespDataValidated = AddValidateScheme(strResponse, XMLSchemas.GetXSDset(XMLSchemas.XmlSchemaSetType.X1002Resp));
                    }
                }
            }

            //record the request
            eNaTISQueue.RecordeNatisHistoryFile("X1007", "Response", autNo,eNatisQueue.RespDataValidated, eNatisQueue.ENaTisGuid.ToString());

            if (eNaTISQueue.UpdateeNaTISQueueRecord(eNatisQueue) == null)
            {
                Logger.Write("SendeNaTISResponseData: Get eNaTISUser failed!", "General", (int)TraceEventType.Error, 0, TraceEventType.Error);
                failed = true;
                return failed;
            }

            // 2014-01-13, Oscar added for push eNatis_NTI queue
            if (eNatisQueue.ENatisQueueStatusId.In(
                (int)ENatisQueueStatusList.Failed,
                (int)ENatisQueueStatusList.Success,
                (int)ENatisQueueStatusList.PSuccess))
            {
                new QueueLibrary.QueueItemProcessor().Send(new QueueLibrary.QueueItem
                {
                    Body = eNatisQueue.ENaTisGuid,
                    Group = autNo,
                    QueueType = ServiceQueueLibrary.DAL.Entities.ServiceQueueTypeList.Frame_eNatis_OUT
                });
            }

            return failed;
        }

        private XmlElement CreateStdReqHeader(XmlDocument xmlDoc, string autNo)
        {
            //BD changes to handle autNo and not Authority

            XmlElement element_StdReqHeader = xmlDoc.CreateElement("StdReqHeader");

            if (sAutNo != autNo)
            {
                eNaTISUser eNaTISUser = new eNaTISUser();
                //_users = eNaTISUser.geteNaTISUser(autNo);
                //BD need to handle different users for different eNatis processes
                _users = eNaTISUser.geteNaTISUser(autNo, "ifprod");
            }

            if (_users.Count > 0)
            {
                ENatisUser user = _users[0];

                XmlElement element_OpModInd = xmlDoc.CreateElement("OpModInd");
                XmlElement element_ESTxanID = xmlDoc.CreateElement("ESTxanID");
                XmlElement element_ESUserN = xmlDoc.CreateElement("ESUserN");
                XmlElement element_ESTermID = xmlDoc.CreateElement("ESTermID");

                element_OpModInd.InnerText = user.OperationalMode;
                element_ESTxanID.InnerText = user.TransactionId;
                element_ESUserN.InnerText = user.UserNumber;
                element_ESTermID.InnerText = user.TerminalId;

                element_StdReqHeader.AppendChild(element_OpModInd);
                element_StdReqHeader.AppendChild(element_ESTxanID);
                element_StdReqHeader.AppendChild(element_ESUserN);
                element_StdReqHeader.AppendChild(element_ESTermID);
            }

            return element_StdReqHeader;
        }

        private string CreateXMLForX1002(string PersonId, string AutNo)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlElement element_X1002Req = xmlDoc.CreateElement("X1002Req");
            XmlElement element_StdReqHeader = CreateStdReqHeader(xmlDoc, AutNo);

            XmlElement element_Person = xmlDoc.CreateElement("Person");
            //<IdDocTypeCd>02</IdDocTypeCd><IdDocN>8303150662088</IdDocN>
            XmlElement element_IdDocTypeCd = xmlDoc.CreateElement("IdDocTypeCd");
            element_IdDocTypeCd.InnerText = "02";
            XmlElement element_IdDocN = xmlDoc.CreateElement("IdDocN");
            element_IdDocN.InnerText = PersonId;

            element_Person.AppendChild(element_IdDocTypeCd);
            element_Person.AppendChild(element_IdDocN);

            element_X1002Req.AppendChild(element_StdReqHeader);
            element_X1002Req.AppendChild(element_Person);

            string strXML = element_X1002Req.OuterXml;

            eNaTISQueue eNaTISQueue = new eNaTISQueue();
            if (!eNaTISQueue.ValidateXml(strXML, XMLSchemas.GetXSDset(XMLSchemas.XmlSchemaSetType.X1002Req)))
            {
                Logger.Write("ValidateXml failed!", "General", (int)TraceEventType.Error, 0, TraceEventType.Error);
                return null;
            }

            return strXML;
        }

        /// <summary>
        /// 5a. Send eNaTIS Queue record
        /// </summary>
        public bool SendX1002QueueRecord(Guid guid, string AutNo, string strXML)
        {
            //5b. Insert eNaTIS Queue record with sent data xml
            ENatisQueue queue = new ENatisQueue();

            //GeteNaTISUser
            eNaTISUser eNaTISUser = new eNaTISUser();
            //BD changes for autNo
            //TList<ENatisUser> users = eNaTISUser.geteNaTISUser(frame.AutNo);
            //BD need to handle different users for different eNatis processes.
            TList<ENatisUser> users = eNaTISUser.geteNaTISUser(AutNo, "ifprod");

            if (users.Count < 1)
            {
                Logger.Write("SendeNaTISQueueRecord: Get eNaTISUser failed!", "General", (int)TraceEventType.Error, 0, TraceEventType.Error);
                return true;
            }
            ENatisUser user = users[0];

            //BD 2010-07 need to check for null. strXML can be null if the validation on the XML has failed. 
            //If this is the case, then we need to set the queue record to status error straight away.

            queue.ENaTisGuid = guid;
            if (strXML == null)
            {
                queue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Failed);
            }
            else
            {
                queue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Person);
            }
            queue.ENatisUserId = user.ENatisUserId;
            queue.SentData = strXML;
            //queue.SentDateTime = DateTime.Now;

            eNaTISQueue eNaTISQueue = new eNaTISQueue();

            if (!eNaTISQueue.InserteNaTISQueueRecord(queue))
            {
                Logger.Write("SendeNaTISQueueRecord: InserteNaTISQueueRecord failed!", "General", (int)TraceEventType.Error, 0, TraceEventType.Error);
                return true;
            }

            return false;
        }

        public string AddValidateScheme(string strXML, XmlSchemaSet xmlSchemaSet)
        {
            XmlNode element_Resp = null;
            try
            {
                Byte[] bytes = ASCIIEncoding.ASCII.GetBytes(strXML);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(strXML);

                if (strXML.IndexOf("X1002Resp") > 0)
                    element_Resp = xmlDoc.SelectSingleNode("X1002Resp");

                if (strXML.IndexOf("X1001Resp") > 0)
                    element_Resp = xmlDoc.SelectSingleNode("X1001Resp");

                //element_Resp = xmlDoc.SelectSingleNode("X1001Resp");
                XmlElement element_Scheam = xmlDoc.CreateElement("xsd:schema");


                XmlSchema[] scheams = new XmlSchema[1];
                xmlSchemaSet.CopyTo(scheams, 0);

                MemoryStream mstream = new MemoryStream();
                scheams[0].Write(mstream);

                byte[] bytes_Schema = mstream.GetBuffer();
                string strSchema = ASCIIEncoding.ASCII.GetString(bytes_Schema);


                XmlDocument xmlDoc_Scheam = new XmlDocument();
                xmlDoc_Scheam.LoadXml(strSchema);

                element_Scheam.InnerXml = xmlDoc_Scheam.InnerXml;

                element_Resp.AppendChild(element_Scheam.LastChild);
            }
            catch (Exception ex)
            {
                Logger.Write(ex, "General", (int)TraceEventType.Critical, 0, TraceEventType.Critical);
            }

            if (element_Resp != null)
            {
                return element_Resp.OuterXml;
            }
            else
            {
                return null;
            }

        }


        public string GetRespTranName(string StrResponse)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.InnerXml = StrResponse;

            XmlNode node = xmlDoc.SelectSingleNode("X1001Resp//StdRespHeader//TxanName");

            if (node.Name == "TxanName")
            {
                string strValue = node.InnerText;
                return strValue;
            }
            return "";
        }

        private string GetRespErrDesc(string StrResponse)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.InnerXml = StrResponse;

            XmlNode node = xmlDoc.SelectSingleNode("X4000Resp\\ErrDesc");

            if (node.Name == "ErrDesc")
            {
                string strValue = node.InnerText;

                if (strValue.Length > 0)
                {

                    eNaTISResponse eNaTISResponse = new eNaTISResponse();
                    string strErrorDesc = eNaTISResponse.GetENatisError(strValue);

                    if (strErrorDesc != null)
                        return strErrorDesc;
                }
                return strValue;
            }

            return null;
        }

        private string GetRespTranState(string StrResponse)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.InnerXml = StrResponse;

            XmlNode node = xmlDoc.SelectSingleNode("X4000Resp\\TxState");

            if (node.Name == "TxState")
            {
                string strValue = node.InnerText;
                return strValue;
            }
            return null;
        }

        private void ProcessENaTISError()
        {
            eNaTISResponse eNaTISResponse = new eNaTISResponse();
            eNaTISResponse.GetENatisError("");
        }
    }
}
