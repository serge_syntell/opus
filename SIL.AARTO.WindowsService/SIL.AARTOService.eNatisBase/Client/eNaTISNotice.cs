﻿using System;
using System.Collections.Generic;
using System.Text;
using SIL.eNaTIS.DAL.Services;
using SIL.eNaTIS.DAL.Entities;
using SIL.eNaTIS.DAL.Data;

namespace SIL.AARTOService.eNatisBase.Client
{
	public class eNaTISNotice
	{
        /// <summary>
        /// 1b. Save eNaTIS Notice object to DB
        /// </summary>
        /// <param name="Notices"></param>
        /// <returns></returns>
        public TList<Notice> SaveeNaTISNotice(TList<Notice> notices)
        {
            NoticeService NoticeService = new NoticeService();
            return NoticeService.Save(notices);
        }


        public Notice SaveeNaTISNotice(Notice notice)
        {
            NoticeService NoticeService = new NoticeService();
            return NoticeService.Save(notice);
        }

        /// <summary>
        ///  3b.Request eNaTIS Notice data from DB
        /// </summary>
        /// <returns></returns>
        public TList<Notice> RequesteNaTISNoticeData()
        {
            NoticeService NoticeService = new NoticeService();

            NoticeQuery noticeQuery = new NoticeQuery();

            noticeQuery.AppendIn(NoticeColumn.ENatisQueueStatusId, new string[]{
                Convert.ToInt32(ENatisQueueStatusList.Sent).ToString()
                , Convert.ToInt32(ENatisQueueStatusList.Loaded).ToString()
            });

            return NoticeService.Find(noticeQuery);
        }

        public TList<Notice> GetNoticeByGuid(Guid guid)
        {
            NoticeService NoticeService = new NoticeService();
            return NoticeService.Find("eNaTISGuid = '" + guid.ToString() + "'");
        }

	}
}
