using System;
using System.Collections.Generic;
using System.Text;
using SIL.eNaTIS.DAL.Services;
using SIL.eNaTIS.DAL.Entities;

namespace SIL.AARTOService.eNatisBase.Client
{
	public class eNaTISResponse
	{
        public int? GetResponseStatusID(string RespTranState)
        {
            RespTranStateService respTranStateService = new RespTranStateService();
            TList<RespTranState> respTranStates =  respTranStateService.Find("RespTranState = '" + RespTranState +  "'");
            if (respTranStates.Count < 1)
                return null;

            return respTranStates[0].RespTranStateId;
        }

        public string GetENatisError(string strENaTISCode)
        {
            ENatisErrorService eNatisErrorService = new ENatisErrorService();


            TList<ENatisError> errors = eNatisErrorService.Find(ENatisErrorColumn.ENatisErrorCode.ToString() + " = '" + strENaTISCode + "'");

            if(errors.Count > 0)
            {
                return errors[0].ENatisError;
            }
            return null; 
        }


        //6d. Add to queue for reprocessing

        //6e. Validate eNaTIS response
	}
}
