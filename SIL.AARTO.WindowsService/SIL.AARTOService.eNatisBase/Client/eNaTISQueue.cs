using System;
using System.Text;
using System.Xml;
using SIL.eNaTIS.DAL.Services;
using SIL.eNaTIS.DAL.Entities;
using SIL.eNaTIS.DAL.Data;
using System.Xml.Schema;
using System.IO;

namespace SIL.AARTOService.eNatisBase.Client
{
    public class eNaTISQueue
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="eNaTISQueue"/> class.
        /// </summary>
        public eNaTISQueue()
        {
        }

        public void SeteNaTISRespData(XmlDocument eNaTISResponse)
        {
            throw new NotImplementedException();
        }

        public eNaTISResponse[] GeteNaTISResponse(Guid[] eNaTISGuid)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        ///  2b. Request valid eNaTIS data
        /// </summary>
        public TList<ENatisQueue> RequestValideNaTISdata()
        {
            ENatisQueueService eNatisQueueService = new ENatisQueueService();

            return eNatisQueueService.Find("ENatisQueueStatusId = " + ENatisQueueStatusList.Success.ToString());
        }

        /// <summary>
        /// 4b. For each eNaTIS queue record that is marked as failed
        /// </summary>
        public TList<ENatisQueue> GetFailedeNaTISQueue()
        {
            ENatisQueueService eNatisQueueService = new ENatisQueueService();

            ENatisQueueQuery eNatisQueueQuery = new ENatisQueueQuery();

            eNatisQueueQuery.AppendIn(ENatisQueueColumn.ENatisQueueStatusId, new string[]{
                Convert.ToInt32(ENatisQueueStatusList.Sent).ToString()
             ,   Convert.ToInt32(ENatisQueueStatusList.Loaded).ToString()
             });

            return eNatisQueueService.Find(eNatisQueueQuery);
        }

        /// <summary>
        /// 4b. For each eNaTIS queue record that is marked as failed
        /// </summary>
        public TList<ENatisQueue> GetPersoneNaTISQueue()
        {
            ENatisQueueService eNatisQueueService = new ENatisQueueService();

            ENatisQueueQuery eNatisQueueQuery = new ENatisQueueQuery();

            eNatisQueueQuery.AppendIn(ENatisQueueColumn.ENatisQueueStatusId, new string[]{
                Convert.ToInt32(ENatisQueueStatusList.Person).ToString()
             });

            return eNatisQueueService.Find(eNatisQueueQuery);
        }

        public void DeepLoad(ENatisQueue queue)
        {
            ENatisQueueService eNatisQueueService = new ENatisQueueService();

            eNatisQueueService.DeepLoad(queue);
        }


        /// <summary>
        /// 5b. Insert eNaTIS Queue record with sent data xml
        /// </summary>
        public bool InserteNaTISQueueRecord(ENatisQueue queue, Frame frame)
        {
            TransactionManager trans = ConnectionScope.CreateTransaction();
            //trans.BeginTransaction();

            ENatisQueueService eNatisQueueService = new ENatisQueueService();
            if (!eNatisQueueService.Insert(queue))
            {
                trans.Rollback();
                return false;
            }

            FrameService frameService = new FrameService();
            if (!frameService.Update(frame))
            {
                trans.Rollback();
                return false;
            }

            trans.Commit();

            return true;
        }

        /// <summary>
        /// Insert eNaTIS Queue record with sent data xml - X1002
        /// </summary>
        public bool InserteNaTISQueueRecord(ENatisQueue queue)
        {
            TransactionManager trans = ConnectionScope.CreateTransaction();
            //trans.BeginTransaction();

            ENatisQueueService eNatisQueueService = new ENatisQueueService();
            if (!eNatisQueueService.Insert(queue))
            {
                trans.Rollback();
                return false;
            }

            trans.Commit();

            return true;
        }

        /// <summary>
        /// 5b. Insert eNaTIS Queue record with sent data xml
        /// </summary>
        public bool InserteNaTISQueueRecord(ENatisQueue queue, Notice notice)
        {
            TransactionManager trans = ConnectionScope.CreateTransaction();
            //trans.BeginTransaction();

            ENatisQueueService eNatisQueueService = new ENatisQueueService();
            if (!eNatisQueueService.Insert(queue))
            {
                trans.Rollback();
                return false;
            }

            NoticeService noticeService = new NoticeService();
            if (!noticeService.Delete(notice))
            {
                trans.Rollback();
                return false;
            }

            trans.Commit();

            return true;
        }

        public ENatisQueue UpdateeNaTISQueueRecord(ENatisQueue queue)
        {
            ENatisQueueService eNatisQueueService = new ENatisQueueService();

            return eNatisQueueService.Save(queue);
        }


        public TList<ENatisQueue> GeteNaTISQueueByGuid(Guid guid)
        {
            ENatisQueueService eNatisQueueService = new ENatisQueueService();
            return eNatisQueueService.Find("ENaTisGuid = '" + guid.ToString() + "'");
        }

        public TList<ENatisQueue> GeteNaTISQueueByGuidByStatus(Guid guid, string QueueType)
        {
            ENatisQueueService eNatisQueueService = new ENatisQueueService();
            return eNatisQueueService.Find("ENaTisGuid = '" + guid.ToString() + "' AND RespTranName = " + QueueType);
        }

        static bool bReadOK = true;

        /// <summary>
        /// 6c. Validate xml  against database xml schema
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public bool ValidateXml(string xml, XmlSchemaSet m_XmlSchemaSet)
        {
            bReadOK = true;
            //xml.Save(stream);

                 Byte[] bytes = ASCIIEncoding.UTF8.GetBytes(xml);
                 MemoryStream mstream = new MemoryStream(bytes);

            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            xmlReaderSettings.ValidationType = ValidationType.Schema;
            xmlReaderSettings.Schemas = m_XmlSchemaSet;
            xmlReaderSettings.ValidationEventHandler += ValidationCallBack;

            XmlReader xmlReader = XmlReader.Create(mstream, xmlReaderSettings);
            while (xmlReader.Read()) { }

            xmlReaderSettings.ValidationEventHandler -= ValidationCallBack;

            return bReadOK;
        }

        // Display any warnings or errors.
        private static void ValidationCallBack(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
                Console.WriteLine("\tWarning: Matching schema not found.  No validation occurred." + args.Message);
            else
            {
                bReadOK = false;
                Console.WriteLine("\tValidation error: " + args.Message);
            }
        }

        private bool ValidateeNaTISResponse(string xml)
        {
            return false;
        }

        public void SeteNaTISSentData(XmlDocument eNaTISSentData, Guid eNaTISGuid, int eNatusUserID, DateTime SentDateTime)
        {
            throw new NotImplementedException();
        }

        public void SeteNaTISFailure(Guid eNaTISGuid)
        {

            throw new NotImplementedException();
        }

        public void RecordeNatisHistoryFile(string serviceType, string sendType, string autNo, string xmlData, string skey)
        {
            ENatisHistoryFile file = new ENatisHistoryFile();
            ENatisHistoryFileService service = new ENatisHistoryFileService();
            file.EhfServiceType = serviceType;
            file.EhfType = sendType;
            file.EhfAutNo = autNo;
            file.EhfKey = skey;
            file.EhfxmlData = xmlData;
            file.EhfCreateDate = DateTime.Now;

            service.Save(file);
        }
    }
}
