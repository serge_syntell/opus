using System;
using System.Collections.Generic;
using System.Text;
using SIL.eNaTIS.DAL.Services;
using SIL.eNaTIS.DAL.Entities;
using SIL.eNaTIS.DAL.Data;

namespace SIL.AARTOService.eNatisBase.Client
{
	public class eNaTISFrame
	{
        /// <summary>
        /// 1b. Save eNaTIS frame object to DB
        /// </summary>
        /// <param name="frames"></param>
        /// <returns></returns>
        public TList<Frame> SaveeNaTISFrame(TList<Frame> frames)
        {
            FrameService frameService = new FrameService();
            return frameService.Save(frames);
        }

        /// <summary>
        ///  3b.Request eNaTIS frame data from DB
        /// </summary>
        /// <returns></returns>
        public TList<Frame> RequesteNaTISFrameData()
        {
            FrameService frameService = new FrameService();

            FrameQuery frameQuery = new FrameQuery();
            frameQuery.AppendIn(FrameColumn.TmsFrameStatus, new string[] { "110", "735" }); 

            return frameService.Find(frameQuery);
        }

        public TList<Frame> GetFrameByGuid(Guid guid)
        {
            FrameService frameService = new FrameService();
            return frameService.Find("eNaTISGuid = '" + guid.ToString() + "'");
        }

	}
}
