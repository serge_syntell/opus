using SIL.eNaTIS.DAL.Services;
using SIL.eNaTIS.DAL.Entities;

namespace SIL.AARTOService.eNatisBase.Client
{
    public class eNaTISUser
    {
        public TList<ENatisUser> geteNaTISUser()
        {
            ENatisUserService eNatisUserService = new ENatisUserService();

            TList<ENatisUser> eNatisUsers = eNatisUserService.GetAll();

            return eNatisUsers;
        }

        public TList<ENatisUser> geteNaTISUser(int authorityID)
        {
            ENatisUserService eNatisUserService = new ENatisUserService();

            TList<ENatisUser> eNatisUsers = eNatisUserService.Find("AuthorityId = " + authorityID);
            return eNatisUsers;
        }

        public TList<ENatisUser> geteNaTISUser(string autNo)
        {
            ENatisUserService eNatisUserService = new ENatisUserService();

            TList<ENatisUser> eNatisUsers = eNatisUserService.Find("AutNo = " + autNo);
            return eNatisUsers;
        }

        public TList<ENatisUser> geteNaTISUser(string autNo, string eNatisProcess)
        {
            ENatisUserService eNatisUserService = new ENatisUserService();

            TList<ENatisUser> eNatisUsers = eNatisUserService.Find("AutNo = " + autNo + " AND eNatisProcess = '" + eNatisProcess + "'");
            return eNatisUsers;
        }

    }
}
