﻿using SIL.eNaTIS.DAL.Entities;
using SIL.eNaTIS.DAL.Services;

namespace SIL.AARTOService.eNatisBase.Client
{
    public class eNatisAuthorityNumber
    {
        readonly ENatisAuthorityNumberService eanService = new ENatisAuthorityNumberService();

        public TList<ENatisAuthorityNumber> GetAll()
        {
            var eanList = this.eanService.GetAll();

            //eanList.ForEach(e =>
            //{
            //    e.AutNo = e.AutNo.Trim();
            //    if (!string.IsNullOrEmpty(e.EnatisAutNo))
            //        e.EnatisAutNo = e.EnatisAutNo.Trim();
            //});

            return eanList;
        }

        public TList<ENatisAuthorityNumber> Save(TList<ENatisAuthorityNumber> eanList)
        {
            if (eanList == null || eanList.Count <= 0) return eanList;

            //eanList.ForEach(e =>
            //{
            //    if (e.EntityState == EntityState.Unchanged
            //        || e.EntityState == EntityState.Deleted)
            //        return;

            //    e.AutNo = e.AutNo.Trim();
            //    if (!string.IsNullOrEmpty(e.EnatisAutNo))
            //        e.EnatisAutNo = e.EnatisAutNo.Trim();
            //});

            return this.eanService.Save(eanList);
        }

        public string GetENatisAutNo(string autNo)
        {
            var eNatisAutNo = string.Empty;
            if (!string.IsNullOrEmpty(autNo))
            {
                var ean = eanService.GetByAutNo(autNo.Trim());
                if (ean != null && !string.IsNullOrEmpty(ean.EnatisAutNo))
                    eNatisAutNo = ean.EnatisAutNo.Trim();
            }
            return eNatisAutNo;
        }
    }
}
