﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.eNatisBase
{
    public class Logger
    {
        //static Logger logger;
        //Logger() {}

        //public static Logger GetSingleton(ILogger serviceLogger)
        //{
        //    return logger ?? (logger = new Logger { serviceLogger = serviceLogger });
        //}

        [ThreadStatic]
        static ClientService clientService;

        public static void SetLoggerInstance(ClientService service)
        {
            clientService = service;
        }

        public static void Write(object message, string category, int priority, int eventId, TraceEventType severity)
        {
            var isEx = message is Exception;
            switch (severity)
            {
                case TraceEventType.Information:
                    if (isEx)
                        clientService.Logger.Info((Exception)message);
                    else
                        clientService.Logger.Info(message.ToString());
                    break;

                case TraceEventType.Warning:
                    if (isEx)
                        clientService.Logger.Warning((Exception)message);
                    else
                        clientService.Logger.Warning(message.ToString());
                    break;

                case TraceEventType.Error:
                    if (isEx)
                        clientService.Logger.Error((Exception)message);
                    else
                        clientService.Logger.Error(message.ToString());
                    break;

                case TraceEventType.Critical:
                    if (isEx)
                        clientService.Logger.Fatal((Exception)message);
                    else
                        clientService.Logger.Fatal(message.ToString());

                    if (!clientService.MustStop)
                    {
                        clientService.MustStop = true;
                        ((ServiceHost)clientService.ServiceHost).Stop();
                    }

                    break;
            }
        }
    }
}
