﻿using System;
using System.Transactions;
using SIL.AARTOService.eNatisBase.Client;
using SIL.eNaTIS.DAL.Entities;
using System.Xml;
//using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

namespace SIL.AARTOService.eNatisBase
{
    public class eNaTISRequest
    {
        // Fields
        private string sAutNo = "";
        private TList<ENatisUser> _users = new TList<ENatisUser>();
        private readonly ENatisCertificate eNatisCertificate = new ENatisCertificate();
        private int _retryCount;
        //public static string UserBase = System.Configuration.ConfigurationSettings.AppSettings["UserBase"].ToString();
        public static string UserBase = Parameters.UserBase;

        eNatisAuthorityNumber eanService = new eNatisAuthorityNumber();

        /// <summary>
        /// 4c. Create xml file sent to <see cref="eNaTIS"/>
        /// </summary>
        public void ProcessFrame()
        {
            //-----------------------------------------------------------------------
            // Frame
            //-----------------------------------------------------------------------
            try
            {
                //4a. For each frame object received in 3d.
                eNaTISFrame eNaTISFrame = new eNaTISFrame();
                TList<Frame> frames = eNaTISFrame.RequesteNaTISFrameData();

                foreach (Frame frame in frames)
                {
                    //4c. Create xml file to be sent to eNaTIS
                    string strXML = CreateXMLByFrame(frame);

                    //4d. Add xml file to a new thread
                    SendeNaTISQueueRecord(frame, strXML);
                }

                //4b. For each eNaTIS queue record that is marked as failed
                eNaTISQueue eNaTISQueue = new eNaTISQueue();
                TList<ENatisQueue> eNatisQueues = eNaTISQueue.GetFailedeNaTISQueue();

                foreach (ENatisQueue queue in eNatisQueues)
                {
                    eNaTISQueue.DeepLoad(queue);
                    //4d. Add xml file to a new thread
                    HandleRequest(queue.SentData, queue.ENaTisGuid, (string)queue.ENatisUserIdSource.AutNo);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(ex, "General", (int)TraceEventType.Critical, 0, TraceEventType.Critical);
            }
            finally
            {
                ENatisCertificate.ShutDownConnection();
            }
        }

        /// <summary>
        /// Send XML file that needs to be sent to enatis for the processing of person details.
        /// This follows the same connection as the process frame, just uses the different interface called X1002
        /// </summary>
        public void ProcessPerson()
        {
            try
            {
                eNaTISQueue eNaTISQueue = new eNaTISQueue();
                TList<ENatisQueue> eNatisQueues = eNaTISQueue.GetPersoneNaTISQueue();

                foreach (ENatisQueue queue in eNatisQueues)
                {
                    eNaTISQueue.DeepLoad(queue);
                    //Add xml file to a new thread
                    HandleRequest(queue.SentData, queue.ENaTisGuid, (string)queue.ENatisUserIdSource.AutNo);
                }
            }
            catch (Exception ex)
            {
                Logger.Write(ex, "General", (int)TraceEventType.Critical, 0, TraceEventType.Critical);
            }
            finally
            {
                ENatisCertificate.ShutDownConnection();
            }
        }

        /// <summary>
        /// Processes notices in the eNatis queue using the WebService interface
        /// </summary>
        public void ProcessNotice()
        {
            try
            {
                //  For each notice object received
                eNaTISNotice eNaTISNotice = new eNaTISNotice();
                TList<Notice> notices = eNaTISNotice.RequesteNaTISNoticeData();

                foreach (Notice notice in notices)
                {
                    //test data
                    //notice.Licn = "BBC112GP";
                    //notice.Infrastructuren = "40570003";   
                    //notice.OtherLocInfo = "testing...";
                    //notice.Suburb = "Fourways";
                    //notice.CityTown = "Johannesburg";
                    if (!HandleNotice(notice))
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Write(ex, "General", (int)TraceEventType.Critical, 0, TraceEventType.Critical);
            }
        }

        #region Frame processing

        /// <summary>
        /// 4c. Create xml file to be sent to eNaTIS
        /// </summary>
        /// <param name="frame"></param>
        /// <returns></returns>
        private string CreateXMLByFrame(Frame frame)
        {
            XmlDocument xmlDoc = new XmlDocument();

            XmlElement element_X1001Req = xmlDoc.CreateElement("X1001Req");

            XmlElement element_StdReqHeader = CreateStdReqHeader(xmlDoc, frame.AutNo);

            XmlElement element_VehicleId = xmlDoc.CreateElement("Vehicle");
            //Barry - the MVRegN ist the 7 digit number on the license disk of the car. We need to be populating the LicN element for the registration number
            //XmlElement element_MVRegN = xmlDoc.CreateElement("MVRegN");
            XmlElement element_LicN = xmlDoc.CreateElement("LicN");
            element_LicN.InnerText = frame.RegNo;
            //element_MVRegN.InnerText = frame.RegNo;
            //element_VehicleId.AppendChild(element_MVRegN);
            element_VehicleId.AppendChild(element_LicN);

            XmlElement element_RefD = xmlDoc.CreateElement("RefD");
            element_RefD.InnerText = frame.OffenceDate.ToString("yyyy-MM-dd");

            element_X1001Req.AppendChild(element_StdReqHeader);
            element_X1001Req.AppendChild(element_VehicleId);
            element_X1001Req.AppendChild(element_RefD);

            //"<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            string strXML = element_X1001Req.OuterXml;

            eNaTISQueue eNaTISQueue = new eNaTISQueue();
            if (!eNaTISQueue.ValidateXml(strXML, XMLSchemas.GetXSDset(XMLSchemas.XmlSchemaSetType.X1001Req)))
            {
                Logger.Write("ValidateXml failed!", "General", (int)TraceEventType.Error, 0, TraceEventType.Error);
                return null;
            }

            return strXML;
        }

        

        private XmlElement CreateStdReqHeader(XmlDocument xmlDoc, string autNo)
        {
            //BD changes to handle autNo and not Authority

            XmlElement element_StdReqHeader = xmlDoc.CreateElement("StdReqHeader");

            if (sAutNo != autNo)
            {
                eNaTISUser eNaTISUser = new eNaTISUser();
                //_users = eNaTISUser.geteNaTISUser(autNo);
                //BD need to handle different users for different eNatis processes
                _users = eNaTISUser.geteNaTISUser(autNo, "ifprod");
            }

            if (_users.Count > 0)
            {
                ENatisUser user = _users[0];

                XmlElement element_OpModInd = xmlDoc.CreateElement("OpModInd");
                XmlElement element_ESTxanID = xmlDoc.CreateElement("ESTxanID");
                XmlElement element_ESUserN = xmlDoc.CreateElement("ESUserN");
                XmlElement element_ESTermID = xmlDoc.CreateElement("ESTermID");

                element_OpModInd.InnerText = user.OperationalMode;
                element_ESTxanID.InnerText = user.TransactionId;
                element_ESUserN.InnerText = user.UserNumber;
                element_ESTermID.InnerText = user.TerminalId;

                element_StdReqHeader.AppendChild(element_OpModInd);
                element_StdReqHeader.AppendChild(element_ESTxanID);
                element_StdReqHeader.AppendChild(element_ESUserN);
                element_StdReqHeader.AppendChild(element_ESTermID);
            }

            return element_StdReqHeader;
        }

        public void HandleRequest(string strXML, Guid guild, string AutNo)
        {
            //BD changes to handle autNo and not AuthorityID
            //Interaction between SIL.eNaTIS.WinService and eNaTIS

            //BD 20100901 Need to be able to handle the extra step of getting the person data. X1002 process.

            DateTime dtSend = DateTime.Now;
            string strResponse = "";

            //BD test x1002
            //create xml for x1002
            //strXML = "<X1002Req><StdReqHeader><OpModInd>LIVE</OpModInd><ESTxanID>32</ESTxanID><ESUserN>SYNTXML</ESUserN><ESTermID>10</ESTermID></StdReqHeader><Person><IdDocTypeCd>02</IdDocTypeCd><IdDocN>8303150662088</IdDocN></Person></X1002Req>";

            int iRetryCount = 0;
            while (iRetryCount < 3)
            {
                //strResponse = eNatisCertificate.SendToeNaTIS(strXML, AuthorityId);
                strResponse = eNatisCertificate.SendToeNaTIS(strXML, AutNo);
                if (strResponse == null)
                {
                    iRetryCount++;  // Oscar 20121226 added for endless loop
                    continue;
                }

                if (strResponse.IndexOf("X4000Resp") > 0)
                {
                    iRetryCount++;
                    if (strResponse.IndexOf(RespTranStateList.NOT_LOGGED_IN.ToString()) > 0)
                    {
                        System.Threading.Thread.Sleep(1000 * 180);
                        ENatisCertificate.SetUpConnection();
                        //Retry
                        continue;
                    }
                    else if (strResponse.IndexOf(RespTranStateList.FIELD_INVALID.ToString()) > 0)
                    {
                        iRetryCount = 3;
                        continue;
                    }
                    else if (strResponse.IndexOf(RespTranStateList.MAX_SESSIONS_IN_USE.ToString()) > 0)
                    {
                        System.Threading.Thread.Sleep(1000 * 180);
                        continue;
                    }
                    else if (strResponse.IndexOf(RespTranStateList.NATIS_INTERNAL_ERROR.ToString()) > 0)
                    {
                        iRetryCount = 3;
                        continue;
                    }
                    else if (strResponse.IndexOf(RespTranStateList.TXAN_NOT_AUTHORISED.ToString()) > 0)
                    {
                        iRetryCount = 3;
                        continue;
                    }
                    else if (strResponse.IndexOf(RespTranStateList.USER_AUTHENTICATION_ERROR.ToString()) > 0)
                    {
                        iRetryCount = 3;
                        continue;
                    }
                    else if (strResponse.IndexOf(RespTranStateList.XML_INVALID.ToString()) > 0)
                    {
                        iRetryCount = 3;
                        continue;
                    }
                }
                else
                    break;
            }

            // Oscar 20121226 added for endless loop
            if (strResponse == null)
            {
                Logger.Write(string.Format("Send Frame to eNaTIS failed. Response: '{0}'", strResponse), null, 0, 0, TraceEventType.Error);
                return;
            }

            if (iRetryCount == 3)
            {
                //Email email = new Email();
                //email.Send("Send Frame to eNaTIS failed.", strResponse);
            }

            DateTime dtResponse = DateTime.Now;
            //record request
            eNaTISQueue eNaTISQueue = new eNaTISQueue();
            eNaTISQueue.RecordeNatisHistoryFile("X1007", "Request", AutNo, strXML, guild.ToString());
            //6a. Send eNaTIS response data
            ENaTISResponse eNaTISResponse = new ENaTISResponse();
            eNaTISResponse.SendeNaTISResponseData(strXML, strResponse, guild, dtSend, dtResponse, AutNo);
        }

        /// <summary>
        /// 5a. Send eNaTIS Queue record
        /// </summary>
        public bool SendeNaTISQueueRecord(Frame frame, string strXML)
        {
            //5b. Insert eNaTIS Queue record with sent data xml
            ENatisQueue queue = new ENatisQueue();

            //GeteNaTISUser
            eNaTISUser eNaTISUser = new eNaTISUser();
            //BD changes for autNo
            //TList<ENatisUser> users = eNaTISUser.geteNaTISUser(frame.AutNo);
            //BD need to handle different users for different eNatis processes.
            TList<ENatisUser> users = eNaTISUser.geteNaTISUser(frame.AutNo, "ifprod");

            if (users.Count < 1)
            {
                Logger.Write("SendeNaTISQueueRecord: Get eNaTISUser failed!", "General", (int)TraceEventType.Error, 0, TraceEventType.Error);
                return true;
            }
            ENatisUser user = users[0];

            //BD 2010-07 need to check for null. strXML can be null if the validation on the XML has failed. 
            //If this is the case, then we need to set the queue record to status error straight away.

            queue.ENaTisGuid = frame.ENaTisGuid;
            if (strXML == null)
            {
                queue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Failed);
            }
            else
            {
                queue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Loaded);
            }
            queue.ENatisUserId = user.ENatisUserId;
            queue.SentData = strXML;
            //queue.SentDateTime = DateTime.Now;

            eNaTISQueue eNaTISQueue = new eNaTISQueue();

            if (frame.TmsFrameStatus == "110")
                frame.TmsFrameStatus = "130";
            else if (frame.TmsFrameStatus == "735")
                frame.TmsFrameStatus = "736";
            if (!eNaTISQueue.InserteNaTISQueueRecord(queue, frame))
            {
                Logger.Write("SendeNaTISQueueRecord: InserteNaTISQueueRecord failed!", "General", (int)TraceEventType.Error, 0, TraceEventType.Error);
                return true;
            }

            return false;
        }

        

        #endregion

        #region Notice processing

        private void Resend(Notice notice, string errMsg)
        {
            _retryCount++;
            if (_retryCount < 3)
            {
                HandleNotice(notice);
            }
            else
            {
                Email email = new Email();
                email.Send("Send notice to eNaTIS failed.", errMsg);
            }
        }

        private bool HandleNotice(Notice notice)
        {
            eNaTISNotice eNaTISNotice = new eNaTISNotice();
            //6b. Create CameraFileRequest to be sent to eNaTIS
            PayFine_InfringementsSoapHttpPort.processCameraFileRequest cfRequest = CreateCFRequestByNotice(notice);
            PayFine_InfringementsSoapHttpPort.processCameraFileRequest cfRequestCopy = CreateCFRequestByNotice(notice);
            
            //5. Set eNaTIS Sent data
            notice.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Sent);
            eNaTISNotice.SaveeNaTISNotice(notice);
            //6d. Add xml file to a new thread
            //HandleRequest(strXML, notice.ENaTisGuid, notice.IssAuthority, XMLSchemas.XmlSchemaSetType.ProcessCameraFileResp);

            //BD Changed to handle AutNo and not AuthorityID

            //Interaction between SIL.eNaTIS.WinService and eNaTIS
            string strErrorXML = "";
            //string URIBase = System.Configuration.ConfigurationSettings.AppSettings["URIBase"].ToString();
            string URIBase = Parameters.URIBase;
            //BD 20120122 Need to check and change the AutNo.
            string eNatisAutNo = "";
            //if (notice.AutNo.Trim() == "212")
            //    //eNatisAutNo = System.Configuration.ConfigurationSettings.AppSettings["212"].ToString();
            //    eNatisAutNo = Parameters.AuthNo212;
            //else if (notice.AutNo.Trim() == "214")
            //    //eNatisAutNo = System.Configuration.ConfigurationSettings.AppSettings["214"].ToString();
            //    eNatisAutNo = Parameters.AuthNo214;
            //else
            //    //eNatisAutNo = System.Configuration.ConfigurationSettings.AppSettings["166"].ToString();
            //    eNatisAutNo = Parameters.AuthNo166;
            eNatisAutNo = eanService.GetENatisAutNo(notice.AutNo);

            //record request
            eNaTISQueue eNatisQueue = new eNaTISQueue();
            //set 
            if (cfRequestCopy.Infringement != null)
            {
                cfRequestCopy.Infringement.VehImage = null;
                cfRequestCopy.Infringement.LicImage = null;
            }
            string requestXML = SerializationXML<PayFine_InfringementsSoapHttpPort.processCameraFileRequest>(cfRequestCopy).InnerXml;

            eNatisQueue.RecordeNatisHistoryFile("X3048", "Request", notice.AutNo, requestXML, notice.NoticeId.ToString());

            //PayFine_InfringementsSoapHttpPort.processCameraFileResponse strResponse = eNatisCertificate.SendCFRequest(cfRequest, "eNaTIS_PEI", notice.IssAuthority, ref strErrorXML);
            PayFine_InfringementsSoapHttpPort.processCameraFileResponse strResponse = eNatisCertificate.SendCFRequest(cfRequest, URIBase, eNatisAutNo, ref strErrorXML);

            //record response
            string responseXML = SerializationXML<PayFine_InfringementsSoapHttpPort.processCameraFileResponse>(strResponse).InnerXml;
            eNatisQueue.RecordeNatisHistoryFile("X3048", "Response", notice.AutNo, responseXML, notice.NoticeId.ToString());
            //eNaTISNotice.SaveeNaTISNotice(notice);
            
            //BD Testing - when you need an xml output
            //System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(cfRequest.GetType());
            //using (Stream stream = File.OpenWrite("e:\\enatis.xml"))
            //{
            //    x.Serialize(stream, cfRequest);
            //}


            if (strErrorXML.Length == 0)
            {
                _retryCount = 0;
                notice.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Success);
                notice.InfringeNoticen = strResponse.Infringement.InfringeNoticeN;

                //add in the new response field
                notice.InfringeStat = strResponse.InfringeStat;
            }
            else
            {
                if (strErrorXML == "error: Doesn't find eNaTIS user")
                {
                    //resend
                    Resend(notice, strErrorXML);
                }
                else if (strErrorXML == "error:Unable to connect to the remote server")
                {
                    //resend
                    Resend(notice, strErrorXML);
                }
                else if (strErrorXML.Substring(0, 5) == "error")
                {
                    //unknow error resend
                    Resend(notice, strErrorXML);
                }
                else
                {
                    //error with error code
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(strErrorXML);

                    XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                    nsmgr.AddNamespace("ns0", "https://ws.enatis.co.za");
                    nsmgr.AddNamespace("types", "https://ws.enatis.co.za/types");

                    XmlNode xmlNode = xmlDoc.SelectSingleNode("ns0:eNaTISBusinessFault//types:BusinessError", nsmgr);
                    if (xmlNode != null)
                    {
                        _retryCount = 0;
                        notice.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Failed);
                        notice.ErrorCode = xmlNode.SelectSingleNode("//types:ErrorCode", nsmgr).InnerText;
                        notice.ErrorDesc = xmlNode.SelectSingleNode("//types:ErrorDesc", nsmgr).InnerText;
                    }
                    else
                    {
                        xmlNode = xmlDoc.SelectSingleNode("detail//ns0:eNaTISInternalFault", nsmgr);
                        if (xmlNode != null)
                        {
                            notice.ErrorCode = xmlNode.SelectSingleNode("//types:ErrorCode", nsmgr).InnerText;
                            notice.ErrorDesc = xmlNode.SelectSingleNode("//types:ErrorDesc", nsmgr).InnerText;
                            notice.Referencen = xmlNode.SelectSingleNode("//types:ReferenceN", nsmgr).InnerText;

                            //Resend
                            Resend(notice, strErrorXML);
                        }
                        else
                        {
                            _retryCount = 0;
                            xmlNode = xmlDoc.SelectSingleNode("ns0:eNaTISInternalFault//types:EnatisInternalError", nsmgr);
                            //notice.ErrorCode = xmlNode.SelectSingleNode("//types:ErrorCode", nsmgr).InnerText;
                            notice.ErrorDesc = xmlNode.SelectSingleNode("//types:ErrorDesc", nsmgr).InnerText;
                            notice.Referencen = xmlNode.SelectSingleNode("//types:ReferenceN", nsmgr).InnerText;
                        }
                    }
                }
            }

            eNaTISNotice.SaveeNaTISNotice(notice);
            // 2014-01-13, Oscar removed push queue from here!

            if (_retryCount == 3)
            {
                return false;
            }
            return true;
        }

        private PayFine_InfringementsSoapHttpPort.processCameraFileRequest CreateCFRequestByNotice(Notice notice)
        {
            //dls - we have to "hardcode" some of these values for now, otherwise they are not going to get into the test data

            PayFine_InfringementsSoapHttpPort.processCameraFileRequest cfRequest = new PayFine_InfringementsSoapHttpPort.processCameraFileRequest();
            PayFine_InfringementsSoapHttpPort.processCameraFileRequestInfringement cfReqInfringement = new PayFine_InfringementsSoapHttpPort.processCameraFileRequestInfringement();
            PayFine_InfringementsSoapHttpPort.processCameraFileRequestCharge cfReqCharge = new PayFine_InfringementsSoapHttpPort.processCameraFileRequestCharge();
            PayFine_InfringementsSoapHttpPort.processCameraFileRequestOfficer cfReqOfficer = new PayFine_InfringementsSoapHttpPort.processCameraFileRequestOfficer();
            PayFine_InfringementsSoapHttpPort.processCameraFileRequestVehicle cfReqVehicle = new PayFine_InfringementsSoapHttpPort.processCameraFileRequestVehicle();

            //** need to handle image manipulation
            //** need to check fields for null or "" and if so dont set the fields

            //BD need to handle the correct Iss Auth - the one in the notice table is not correct. Need to use the one in the users table
            eNaTISUser eNaTISUser = new eNaTISUser();
            //TList<ENatisUser> users = eNaTISUser.geteNaTISUser(notice.AutNo);
            //BD need to handle different users for different processes.

            string eNatisAutNo = "";
            //if (notice.AutNo.Trim() == "212")
            //    //eNatisAutNo = System.Configuration.ConfigurationSettings.AppSettings["212"].ToString();
            //    eNatisAutNo = Parameters.AuthNo212;
            //else if (notice.AutNo.Trim() == "214")
            //    //eNatisAutNo = System.Configuration.ConfigurationSettings.AppSettings["214"].ToString();
            //    eNatisAutNo = Parameters.AuthNo214;
            //else
            //    //eNatisAutNo = System.Configuration.ConfigurationSettings.AppSettings["166"].ToString();
            //    eNatisAutNo = Parameters.AuthNo166;
            eNatisAutNo = eanService.GetENatisAutNo(notice.AutNo);

            TList<ENatisUser> users = eNaTISUser.geteNaTISUser(eNatisAutNo, UserBase);
            ENatisUser user = users[0];

            //infringement
            cfReqInfringement.Date = notice.OffenceDate;
            cfReqInfringement.FilmN = notice.Filmn;
            cfReqInfringement.IssAuthority = user.IssAuthority.ToString(); //notice.IssAuthority.ToString(); BD changed to use correct one from user
            cfReqInfringement.Province = notice.Province.ToString();
            cfReqInfringement.RefN = notice.Refn;
            cfReqInfringement.RegAuthority = user.RegAuthority.ToString(); //notice.RegAuthority.ToString(); BD changed to use correct one from user
            cfReqInfringement.StreetNameA = notice.StreetNamea.ToUpper();
            cfReqInfringement.Time = Convert.ToDateTime(notice.OffenceTime.ToString("HH:mm:ss"));
            //this needs to be changed - the image must just come out of the DB as a byte array
            cfReqInfringement.VehImage = notice.VehImage;       //Convert.FromBase64String(notice.VehImage);
            cfReqInfringement.LicImage = notice.LicImage;
            //cfReqInfringement.DirectionFrom = notice.DirectionFrom;
            //cfReqInfringement.DirectionTo = notice.DirectionTo;
            //cfReqInfringement.GPSXCoord = notice.GpsxCoord;
            //cfReqInfringement.GPSYCoord = notice.GpsyCoord;
            //cfReqInfringement.LicImage = notice.LicImage;
            cfReqInfringement.OtherLocInfo = notice.OtherLocInfo.ToUpper();
            //cfReqInfringement.RouteN = notice.Routen;
            cfReqInfringement.StreetNameB = notice.StreetNameb.ToUpper();
            cfReqInfringement.Suburb = notice.Suburb.ToUpper();
            //BD 20120619 this si not needed anymore... Need to check for Randburg, Sandton, Modrand
            //if (notice.CityTown == "City of Johannesburg")
            //    cfReqInfringement.CityTown = "JOHANNESBURG";
            //else
            //    cfReqInfringement.CityTown = notice.CityTown.ToUpper();
            if (notice.Filmn.StartsWith("RB"))
            {
                cfReqInfringement.CityTown = "RANDBURG";
            }
            else if (notice.Filmn.StartsWith("SD"))
            {
                cfReqInfringement.CityTown = "SANDTON";
            }
            else if (notice.Filmn.StartsWith("MD"))
            {
                cfReqInfringement.CityTown = "MIDRAND";
            }
            else
            {
                cfReqInfringement.CityTown = "JOHANNESBURG";
            }

            //Need to add in Magisterial court district...
            //cfReqInfringement.MagisterialCd = "121945";
            cfReqInfringement.MagisterialCd = notice.MagisterialCd;

            //charge
            cfReqCharge.Code = notice.OffenceCode.ToString();
            cfReqCharge.SpeedRead1 = notice.SpeedRead1;
            cfReqCharge.SpeedRead2 = notice.SpeedRead2;
            //cfReqCharge.AmberTime = notice.AmberTime.ToString();
            //cfReqCharge.RedTime = notice.RedTime.ToString();
            //cfReqCharge.Description = notice.OffenceDescription;

            //vehicle
            cfReqVehicle.LicN = notice.Licn;
            //cfReqVehicle.LicN = notice.MvTypeCd;

            //officer
            cfReqOfficer.InfrastructureN = notice.Infrastructuren;

            cfRequest.Infringement = cfReqInfringement;
            cfRequest.Charge = cfReqCharge;
            cfRequest.Officer = cfReqOfficer;
            cfRequest.Vehicle = cfReqVehicle;

            //test data - commment out when we go live
            //cfReqInfringement.IssAuthority = "4001";
            //cfReqInfringement.RegAuthority = "4093";
            //cfReqCharge.Code = "4549";

            return cfRequest;
        }

        public static XmlDocument SerializationXML<T>(T t)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                MemoryStream ms = new MemoryStream();
                serializer.Serialize(ms, t);
                XmlDocument doc = new XmlDocument();
                ms.Position = 0;
                doc.Load(ms);
                return doc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Old Code

        ///// <summary>
        ///// 5a. Send eNaTIS Queue record
        ///// </summary>
        //public bool SendeNaTISQueueRecord(Notice notice, string strXML)
        //{
        //    bool failed = false;
        //    //5b. Insert eNaTIS Queue record with sent data xml
        //    ENatisQueue queue = new ENatisQueue();

        //    //GeteNaTISUser
        //    eNaTISUser eNaTISUser = new eNaTISUser();
        //    TList<ENatisUser> users = eNaTISUser.geteNaTISUser(notice.IssAuthority);

        //    if (users.Count < 1)
        //    {
        //        Logger.Write("SendeNaTISQueueRecord: Get eNaTISUser failed!", "General", (int)TraceEventType.Error, 0, TraceEventType.Error);
        //        failed = true;
        //        return failed;
        //    }
        //    ENatisUser user = users[0];


        //    queue.ENaTisGuid = notice.ENaTisGuid;
        //    queue.ENatisQueueStatusId = Convert.ToInt32(ENatisQueueStatusList.Sent);
        //    queue.ENatisUserId = user.ENatisUserId;
        //    queue.SentData = strXML;
        //    queue.ENatisQueueType = "N";

        //    queue.SentDateTime = DateTime.Now;


        //    queue.RespTranStateId = Convert.ToInt32(RespTranStateList.SUCCESS);

        //    eNaTISQueue eNaTISQueue = new eNaTISQueue();

        //    if (!eNaTISQueue.InserteNaTISQueueRecord(queue, notice))
        //    {
        //        Logger.Write("SendeNaTISQueueRecord: InserteNaTISQueueRecord failed!", "General", (int)TraceEventType.Error, 0, TraceEventType.Error);
        //        failed = true;
        //        return failed;
        //    }
        //    return failed;
        //}


        //private byte[] GetBytesFromBase64Binary(string strBase64)
        //{
        //    try
        //    {
        //        byte[] decodedData = Convert.FromBase64String(strBase64);
        //        return decodedData;
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.Write("Get bytes from Base64Binary failed!" + e.Message, "General", (int)TraceEventType.Error, 0, TraceEventType.Error);
        //        return null;
        //    }
        //}


        //private Notice GetNoticeFromXML(string strXml)
        //{
        //    Notice notice = new Notice();
        //    XmlDocument xmlDoc = new XmlDocument();
        //    xmlDoc.LoadXml(strXml);

        //    notice.Filmn = xmlDoc.SelectSingleNode("Infringement/FilmN").InnerText;
        //    notice.Refn = xmlDoc.SelectSingleNode("Infringement/RefN").InnerText;
        //    notice.Province = Convert.ToByte(xmlDoc.SelectSingleNode("Infringement/Province").InnerText);
        //    notice.OffenceDate = Convert.ToDateTime(xmlDoc.SelectSingleNode("Infringement/Date").InnerText);
        //    notice.OffenceTime = Convert.ToDateTime(xmlDoc.SelectSingleNode("Infringement/Time").InnerText);
        //    notice.CityTown = xmlDoc.SelectSingleNode("Infringement/CityTown").InnerText;
        //    //notice.Suburb
        //    notice.StreetNamea = xmlDoc.SelectSingleNode("Infringement/StreetNameA").InnerText;
        //    notice.StreetNameb = xmlDoc.SelectSingleNode("Infringement/StreetNameB").InnerText;
        //    //notice.Routen
        //    //notice.DirectionFrom
        //    //notice.DirectionTo
        //    //notice.GpsxCoord
        //    //notice.GpsyCoord
        //    //notice.OtherLocInfo
        //    notice.IssAuthority = Convert.ToInt32(xmlDoc.SelectSingleNode("Infringement/IssAuthority").InnerText);
        //    notice.RegAuthority = Convert.ToInt32(xmlDoc.SelectSingleNode("Infringement/RegAuthority").InnerText);
        //    notice.VehImage = GetBytesFromBase64Binary(xmlDoc.SelectSingleNode("Infringement/MvImage").InnerText);
        //    notice.LicImage = GetBytesFromBase64Binary(xmlDoc.SelectSingleNode("Infringement/LicImage").InnerText);

        //    //Charge
        //    notice.OffenceCode = Convert.ToInt32(xmlDoc.SelectSingleNode("Charge/Code").InnerText);
        //    //notice.OffenceDescription = xmlDoc.SelectSingleNode("Infringement/IssAuthority").InnerText;
        //    //notice.SpeedRead1
        //    //notice.SpeedRead2
        //    //notice.RedTime
        //    //notice.AmberTime

        //    //Vehicle
        //    notice.Licn = xmlDoc.SelectSingleNode("Vehicle/LicN").InnerText;
        //    //notice.MvTypeCd

        //    //Officer
        //    notice.Infrastructuren = xmlDoc.SelectSingleNode("Officer/InfrastructureN").InnerText;

        //    return notice;
        //}

        //private string CreateXMLByNotice(Notice notice)
        //{
        //    XmlDocument xmlDoc = new XmlDocument();

        //    XmlElement element_Request = xmlDoc.CreateElement("processCameraFileRequest");

        //    XmlElement element_StdReqHeader = CreateStdReqHeader(xmlDoc, notice.IssAuthority);

        //    //Infringement
        //    XmlElement element_Infringement = xmlDoc.CreateElement("Infringement");


        //    XmlElement element_FilmN = xmlDoc.CreateElement("FilmN");
        //    XmlElement element_RefN = xmlDoc.CreateElement("RefN");
        //    XmlElement element_Province = xmlDoc.CreateElement("Province");
        //    XmlElement element_Date = xmlDoc.CreateElement("Date");
        //    XmlElement element_Time = xmlDoc.CreateElement("Time");
        //    XmlElement element_CityTown = xmlDoc.CreateElement("CityTown");
        //    XmlElement element_Suburb = xmlDoc.CreateElement("Suburb");
        //    XmlElement element_StreetNameA = xmlDoc.CreateElement("StreetNameA");
        //    XmlElement element_StreetNameB = xmlDoc.CreateElement("StreetNameB");
        //    XmlElement element_RouteN = xmlDoc.CreateElement("RouteN");
        //    XmlElement element_DirectionFrom = xmlDoc.CreateElement("DirectionFrom");
        //    XmlElement element_DirectionTo = xmlDoc.CreateElement("DirectionTo");
        //    XmlElement element_GPSXCoord = xmlDoc.CreateElement("GPSXCoord");
        //    XmlElement element_GPSYCoord = xmlDoc.CreateElement("GPSYCoord");
        //    XmlElement element_OtherLocInfo = xmlDoc.CreateElement("OtherLocInfo");
        //    XmlElement element_IssAuthority = xmlDoc.CreateElement("IssAuthority");
        //    XmlElement element_RegAuthority = xmlDoc.CreateElement("RegAuthority");
        //    XmlElement element_MvImage = xmlDoc.CreateElement("MvImage");
        //    XmlElement element_LicImage = xmlDoc.CreateElement("LicImage");

        //    AppendChild(element_Infringement, element_FilmN, notice.Filmn);
        //    AppendChild(element_Infringement, element_RefN, notice.Refn);
        //    AppendChild(element_Infringement, element_Province, notice.Province.ToString());
        //    AppendChild(element_Infringement, element_Date, notice.OffenceDate.ToString("yyyy-MM-dd"));
        //    AppendChild(element_Infringement, element_Time, notice.OffenceTime.ToString("hh:ss"));
        //    AppendChild(element_Infringement, element_CityTown, notice.CityTown);
        //    //AppendChild(element_Infringement, element_Suburb, notice.Suburb);
        //    AppendChild(element_Infringement, element_StreetNameA, notice.StreetNamea);
        //    AppendChild(element_Infringement, element_StreetNameB, notice.StreetNameb);
        //    //AppendChild(element_Infringement, element_RouteN, notice.Routen);
        //    //AppendChild(element_Infringement, element_DirectionFrom, notice.DirectionFrom);
        //    //AppendChild(element_Infringement, element_DirectionTo, notice.DirectionTo);
        //    //AppendChild(element_Infringement, element_GPSXCoord, notice.GpsxCoord);
        //    //AppendChild(element_Infringement, element_GPSYCoord, notice.GpsyCoord);
        //    //AppendChild(element_Infringement, element_OtherLocInfo, notice.OtherLocInfo);
        //    //AppendChild(element_Infringement, element_IssAuthority, notice.IssAuthority.ToString());
        //    //AppendChild(element_Infringement, element_RegAuthority, notice.RegAuthority.ToString());
        //    AppendChild(element_Infringement, element_IssAuthority, "4001");
        //    AppendChild(element_Infringement, element_RegAuthority, "4093");
        //    AppendChild(element_Infringement, element_MvImage, GetBase64Binary(notice.VehImage));
        //    AppendChild(element_Infringement, element_LicImage, GetBase64Binary(notice.LicImage));

        //    //AppendChild(element_Infringement, element_MvImage, notice.VehImage.ToString());
        //    //AppendChild(element_Infringement, element_LicImage, notice.LicImage.ToString());

        //    //Charge
        //    XmlElement element_Charge = xmlDoc.CreateElement("Charge");

        //    XmlElement element_Code = xmlDoc.CreateElement("Code");
        //    XmlElement element_Description = xmlDoc.CreateElement("Description");
        //    XmlElement element_SpeedRead1 = xmlDoc.CreateElement("SpeedRead1");
        //    XmlElement element_SpeedRead2 = xmlDoc.CreateElement("SpeedRead2");
        //    XmlElement element_RedTime = xmlDoc.CreateElement("RedTime");
        //    XmlElement element_AmberTime = xmlDoc.CreateElement("AmberTime");

        //    AppendChild(element_Charge, element_Code, notice.OffenceCode.ToString());
        //    //AppendChild(element_Charge, element_Description, notice.OffenceDescription);
        //    //AppendChild(element_Charge, element_SpeedRead1, notice.SpeedRead1);
        //    //AppendChild(element_Charge, element_SpeedRead2, notice.SpeedRead2);
        //    //AppendChild(element_Charge, element_RedTime, notice.RedTime.ToString());
        //    //AppendChild(element_Charge, element_AmberTime, notice.AmberTime.ToString());

        //    //Vehicle
        //    XmlElement element_Vehicle = xmlDoc.CreateElement("Vehicle");

        //    XmlElement element_LicN = xmlDoc.CreateElement("LicN");
        //    XmlElement element_MvTypeCd = xmlDoc.CreateElement("MvTypeCd");

        //    AppendChild(element_Vehicle, element_LicN, notice.Licn);
        //    //AppendChild(element_Vehicle, element_MvTypeCd, notice.MvTypeCd);

        //    //Officer
        //    XmlElement element_Officer = xmlDoc.CreateElement("Officer");

        //    XmlElement element_InfrastructureN = xmlDoc.CreateElement("InfrastructureN");
        //    AppendChild(element_Officer, element_InfrastructureN, notice.Infrastructuren);

        //    element_Request.AppendChild(element_StdReqHeader);
        //    element_Request.AppendChild(element_Infringement);
        //    element_Request.AppendChild(element_Charge);
        //    element_Request.AppendChild(element_Vehicle);
        //    element_Request.AppendChild(element_Officer);

        //    //"<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
        //    string strXML = element_Request.OuterXml;

        //    eNaTISQueue eNaTISQueue = new eNaTISQueue();
        //    if (!eNaTISQueue.ValidateXml(strXML, XMLSchemas.GetXSDset(XMLSchemas.XmlSchemaSetType.ProcessCameraFileReq)))
        //    {
        //        Logger.Write("ValidateXml failed!", "General", (int)TraceEventType.Error, 0, TraceEventType.Error);
        //        return null;
        //    }

        //    return strXML;
        //}

        //private void AppendChild(XmlElement parent, XmlElement child, string strValue)
        //{
        //    if (strValue != null && strValue.Length > 0)
        //    {
        //        child.InnerText = strValue;
        //        parent.AppendChild(child);
        //    }
        //}

        //private string GetBase64Binary(byte[] bytes)
        //{
        //    try
        //    {
        //        string encodedData = Convert.ToBase64String(bytes);
        //        return encodedData;
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.Write("GetBase64Binary failed!" + e.Message, "General", (int)TraceEventType.Error, 0, TraceEventType.Error);
        //        return null;
        //    }
        //}

        #endregion

    }
}
