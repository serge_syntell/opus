﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Security;
using System.Net;
using System.IO;
using SIL.AARTOService.eNatisBase.Client;
using SIL.AARTOService.eNatisBase.PayFine_InfringementsSoapHttpPort;
using SIL.eNaTIS.DAL.Entities;
using System.Text.RegularExpressions;
using System.Xml;
using Microsoft.Web.Services3;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
//using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Diagnostics;
using System.Web.Services;

using Microsoft.Web.Services3.Design;
using Microsoft.Web.Services3.Security;
using Microsoft.Web.Services3.Security.Tokens;

namespace SIL.AARTOService.eNatisBase
{
    class ENatisCertificate
    {
        public enum MessageType { X0000, X1001, X1002, X3048, X4000 };
        
        //string URL_MIB = "mib.enatis.co.za";
        //string URL_CVD = "cvd.enatis.co.za";
        //string URL_NTR = "ntr.enatis.co.za";
        //string URL_LCMS = "lcms.enatis.co.za";
        //string URL_QFI = "qfi.enatis.co.za";
        //string URL_PO = "postoffice.enatis.co.za";
        public static string HOST = "";
        private static int Port = 0;
        public static string URL_CF = "";
        public static string URIBase = "";
        public static string UserBase = "";

        eNaTISQueue eNaTISQueue = new eNaTISQueue();

        //Barry 
        static string SessionCookie = null;
        private static DateTime dtConnectTime;
        private static int INTERVAL = 30;
        //private static int m_AutIntNo = 0;
        private static string m_AutNo = "";
        
        public ENatisCertificate()
        {
            //HOST = System.Configuration.ConfigurationSettings.AppSettings["URL"].ToString();
            //Port = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["Port"]);
            //URIBase = System.Configuration.ConfigurationSettings.AppSettings["URIBase"].ToString();
            //URL_CF = System.Configuration.ConfigurationSettings.AppSettings["URL_CF"].ToString();
            //UserBase = System.Configuration.ConfigurationSettings.AppSettings["UserBase"].ToString();

            HOST = Parameters.URL;
            Port = Parameters.Port;
            URIBase = Parameters.URIBase;
            URL_CF = Parameters.URL_CF;
            UserBase = Parameters.UserBase;

        }


        #region HttpRequest
        public static string GetConnect(string autNo, bool Open)
        {
            //BD changes to handle autNo and not AuthorityID

            eNaTISUser eNaTISUser = new eNaTISUser();
            //TList<ENatisUser> users = eNaTISUser.geteNaTISUser(AutIntNo);
            //TList<ENatisUser> users = eNaTISUser.geteNaTISUser(autNo);
            //BD - need to handlde different users for different processes
            TList<ENatisUser> users = eNaTISUser.geteNaTISUser(autNo, "ifprod");
            m_AutNo = autNo;

            if (users.Count > 0)
            {
                ENatisUser user = users[0];

                string strOperation = "OPEN";
                if (!Open)
                    strOperation = "CLOSE";


                string strContent = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

                strContent += string.Format(@"<X0000Req>
                    <StdReqHeader>
                        <OpModInd>{0}</OpModInd>
                        <ESTxanID>{1}</ESTxanID>
                        <ESUserN>{2}</ESUserN>
                        <ESUID>{4}</ESUID>
                        <ESTermID>{3}</ESTermID>
                    </StdReqHeader>
                    <UserID>{4}</UserID>
                    <Password>{5}</Password>
                    <TxanType>{6}</TxanType>
                    </X0000Req>", user.OperationalMode, user.TransactionId, user.UserNumber, user.TerminalId, user.UserId, user.ENatisPassword, strOperation);

                //strContent = strContent.Trim().Replace(",", "&apos;").Replace("\"", "&quot;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("&", "&amp;");  // For xml special character.
                return SendMessage(MessageType.X0000, ENatisCertificate.URIBase, strContent);
            }
            return "error: Doesn't find eNaTIS user";
        }

        public class TrustAllCertificatePolicy : System.Net.ICertificatePolicy
        {

            public TrustAllCertificatePolicy()
            { }

            public bool CheckValidationResult(ServicePoint sp, System.Security.Cryptography.X509Certificates.X509Certificate cert,
                WebRequest req, int problem)
            {
                return true;
            }

        }

        public string SendToeNaTIS(string strContent, string autNo)
        {
            //BD changes to handle autNo and not AuthorityID
            
            string Msg = "";
            //Connect
            if (dtConnectTime.AddMinutes(INTERVAL) < DateTime.Now)
            {
                //Msg = GetConnect(AuthorityId, true);
                Msg = GetConnect(autNo, true);
            }

            if (Msg == "Unable to connect to the remote server")
            {
                Logger.Write("Opening connection failed.", "General", (int)TraceEventType.Warning, 0, TraceEventType.Warning);
                //Error = "Opening connection failed.";
                return null;
            }
            else
            {
                //string Content = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + strContent;
                //BD need to check if X1001 or X1002
                if (strContent.IndexOf("X1001") > 0)
                {
                    return SendMessage(MessageType.X1001, ENatisCertificate.URIBase, strContent);
                }
                else if (strContent.IndexOf("X1002") > 0)
                {
                    return SendMessage(MessageType.X1002, ENatisCertificate.URIBase, strContent);
                }
                else
                {
                    return null;
                }
            }
        }

        public static void SetUpConnection()
        {
            if (m_AutNo != "" && dtConnectTime.AddMinutes(INTERVAL) > DateTime.Now)
            {
                GetConnect(m_AutNo, true);
            }
        }

        public static void ShutDownConnection()
        {
            if (m_AutNo != "" && dtConnectTime.AddMinutes(INTERVAL) > DateTime.Now)
            {
                GetConnect(m_AutNo, false);
            }
        }

        public static string SendMessage(MessageType RequestType, string URIBase, string strContent)
        {
            //string Host = HOST;
            //Get message body and parse Request Type, e.g. “X0000”, “X2905”
            //string  RequestType = GetRequestType(RichTextBox1.Text);

            //Barry - this needs to be global
            //string SessionCookie = null;
            string Response = null;

            Uri URL = new Uri("https://" + ENatisCertificate.HOST + ":" + ENatisCertificate.Port.ToString() + "/" + URIBase + "/secure/" + RequestType);

            Logger.Write("Opening connection: " + URL.ToString(), "General", (int)TraceEventType.Information, 0, TraceEventType.Information);

            try
            {
                //string strCert = System.Configuration.ConfigurationSettings.AppSettings["CertFile"].ToString();
                string strCert = Parameters.CertFile;
                System.Security.Cryptography.X509Certificates.X509Certificate Cert = System.Security.Cryptography.X509Certificates.X509Certificate.CreateFromCertFile(strCert);
                //System.Security.Cryptography.X509Certificates.X509Certificate Cert2 = System.Security.Cryptography.X509Certificates.X509Certificate.CreateFromCertFile(@"C:\JMPD\enatisca2028.crt");


                System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                HttpWebRequest myRequest = (HttpWebRequest)System.Net.WebRequest.Create(URL);

                myRequest.ClientCertificates.Add(Cert);
                //myRequest.ClientCertificates.Add(Cert2);
                myRequest.ContentType = "text/xml";
                myRequest.UserAgent = "eNaTIS HTTP Test Client";
                myRequest.Accept = "text/xml";
                myRequest.KeepAlive = false; //True by default
                myRequest.AllowAutoRedirect = true;
                myRequest.Method = "POST";

                if (SessionCookie != null)
                {
                    myRequest.Headers.Add("Cookie", SessionCookie);
                }

                UTF8Encoding encoding = new UTF8Encoding();
                Byte[] bytes = encoding.GetBytes(strContent);

                myRequest.ContentLength = bytes.Length;

                Stream so;
                so = myRequest.GetRequestStream();
                //Write the request body (XML message)
                so.Write(bytes, 0, bytes.Length);

                //Change cursor to hourglass
                //System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

                HttpWebResponse MyResponse = (HttpWebResponse)myRequest.GetResponse();

                //Change cursor to default
                //Cursor.Current = System.Windows.Forms.Cursors.Default

                //Print the response headers.
                Logger.Write("Response received", "General", (int)TraceEventType.Information, 0, TraceEventType.Information);

                //Read the response body
                StreamReader sr = new StreamReader(MyResponse.GetResponseStream(), Encoding.Default);
                Response = sr.ReadToEnd();

                //Capture session ID from response header and save in globale string variable SessionCookie
                if (RequestType == MessageType.X0000)
                {
                    if (strContent.IndexOf("OPEN") > 0)
                    {
                        //This is a lazy way, it is much better to get the proper XML <TxanType> element
                        if (Response.IndexOf("SUCCESS") > 0)
                        {
                            if (MyResponse.Headers["Set-Cookie"] != null)
                            {
                                dtConnectTime = DateTime.Now;
                                SessionCookie = MyResponse.Headers["Set-Cookie"].Split(';')[0];
                                Logger.Write(SessionCookie, "General", (int)TraceEventType.Information, 0, TraceEventType.Information);
                            }
                            else
                            {
                                //Login failed, reset session
                                SessionCookie = null;
                                Logger.Write("Login failed, session closed", "General", (int)TraceEventType.Warning, 0, TraceEventType.Warning);
                            }
                        }
                        else
                        {
                            //Login failed, reset session
                            SessionCookie = null;
                            Logger.Write("Login failed, session closed", "General", (int)TraceEventType.Warning, 0, TraceEventType.Warning);
                        }
                    }
                    else if (strContent.IndexOf("CLOSE") > 0)
                    {
                        if (Response.IndexOf("SUCCESS") > 0)
                        {
                            dtConnectTime = DateTime.MinValue;
                            //Reset session
                            SessionCookie = null;
                            Logger.Write("Session closed", "General", (int)TraceEventType.Information, 0, TraceEventType.Information);
                        }
                        else
                        {
                            //CLOSE failed (may need to resend), reset session in any case since session probably expired on server
                            SessionCookie = null;
                            Logger.Write("CLOSE failed (may need to resend)", "General", (int)TraceEventType.Warning, 0, TraceEventType.Warning);
                        }
                    }

                }

                //Since we have keepalive=false, close streams to terminate connection
                so.Close();
                sr.Close();
            }
            catch (Exception ex)
            {
                Logger.Write(ex, "General", (int)TraceEventType.Critical, 0, TraceEventType.Critical);
            }


            return Response;
        }
        #endregion

        #region WebService
        public PayFine_InfringementsSoapHttpPort.processCameraFileResponse SendCFRequest(PayFine_InfringementsSoapHttpPort.processCameraFileRequest cfRequest, string URIBase, string AutNo, ref string ErrorXML)
        {
            //BD changed to handle AutNo and not AuthorityID
            
            //string Host = URL_CF;
            //System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            ServicePointManager.ServerCertificateValidationCallback += delegate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            {
                bool validationResult = true;
                return validationResult;
            };
            PayFine_InfringementsService infringeService = new PayFine_InfringementsSoapHttpPort.PayFine_InfringementsService();
            PayFine_InfringementsSoapHttpPort.processCameraFileResponse cfResponse = new PayFine_InfringementsSoapHttpPort.processCameraFileResponse();

            try
            {
                eNaTISUser eNaTISUser = new eNaTISUser();
                //TList<ENatisUser> users = eNaTISUser.geteNaTISUser(AuthorityId);
                //TList<ENatisUser> users = eNaTISUser.geteNaTISUser(AutNo);
                //BD need to handle different users for different eNatis processes
                TList<ENatisUser> users = eNaTISUser.geteNaTISUser(AutNo, UserBase);

                if (users.Count > 0)
                {
                    ENatisUser user = users[0];

                    infringeService.Url = "https://" + ENatisCertificate.URL_CF + ":" + "/" + URIBase + "/PayFine_InfringementsSoapHttpPort";
                    //infringeService.SoapVersion = System.Web.Services.Protocols.SoapProtocolVersion.Soap11;
                    //infringeService.CookieContainer = new CookieContainer();
                    //ICredentials cred = new NetworkCredential("4468A004", "TESTER01");
                    //infringeService.Credentials = cred;

                    SoapContext sc = infringeService.RequestSoapContext;

                    Microsoft.Web.Services3.Security.Tokens.UsernameToken uToken = new Microsoft.Web.Services3.Security.Tokens.UsernameToken(user.UserId, user.ENatisPassword, Microsoft.Web.Services3.Security.Tokens.PasswordOption.SendPlainText);
                    sc.Security.Tokens.Add(uToken);

                    try
                    {
                        cfResponse = infringeService.processCameraFile(cfRequest);
                    }
                    catch (System.Web.Services.Protocols.SoapException soapExcetion)
                    {
                        Logger.Write(soapExcetion, "General", (int)TraceEventType.Critical, 0, TraceEventType.Critical);
                        ErrorXML = soapExcetion.Detail.InnerXml;
                    }
                    catch (Exception ex)
                    {
                        //** need to handle error messages
                        Logger.Write(ex, "General", (int)TraceEventType.Critical, 0, TraceEventType.Critical);
                        ErrorXML = "error:" + ex.Message;
                    }

                }
                else
                {
                    ErrorXML = "error: Doesn't find eNaTIS user";
                }
            }
            catch (Exception ex)
            {
                Logger.Write(ex, "General", (int)TraceEventType.Critical, 0, TraceEventType.Critical);
            }

            return cfResponse;

        }

        //public void Call_WS()
        //{
        //    SIL.eNaTIS.PayFine_InfringementsSoapHttpPort.PayFine_InfringementsService infringeService = new SIL.eNaTIS.PayFine_InfringementsSoapHttpPort.PayFine_InfringementsService();
        //    PayFine_InfringementsSoapHttpPort.processCameraFileResponse cfResponse = new SIL.eNaTIS.PayFine_InfringementsSoapHttpPort.processCameraFileResponse();
        //    PayFine_InfringementsSoapHttpPort.processCameraFileRequest cfRequest = new SIL.eNaTIS.PayFine_InfringementsSoapHttpPort.processCameraFileRequest();
        //    PayFine_InfringementsSoapHttpPort.processCameraFileRequestInfringement cfReqInfringement = new SIL.eNaTIS.PayFine_InfringementsSoapHttpPort.processCameraFileRequestInfringement();
        //    PayFine_InfringementsSoapHttpPort.processCameraFileRequestCharge cfReqCharge = new SIL.eNaTIS.PayFine_InfringementsSoapHttpPort.processCameraFileRequestCharge();
        //    PayFine_InfringementsSoapHttpPort.processCameraFileRequestOfficer cfReqOfficer = new SIL.eNaTIS.PayFine_InfringementsSoapHttpPort.processCameraFileRequestOfficer();
        //    PayFine_InfringementsSoapHttpPort.processCameraFileRequestVehicle cfReqVehicle = new SIL.eNaTIS.PayFine_InfringementsSoapHttpPort.processCameraFileRequestVehicle();

        //    try
        //    {
        //        //System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
        //        ServicePointManager.ServerCertificateValidationCallback += delegate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        //        {
        //            bool validationResult = true;

        //            //
        //            // policy code here ...
        //            //

        //            return validationResult;
        //        };


        //        //Infringement
        //        //cfReqInfringement.CityTown = "";
        //        DateTime dtInfringement = new DateTime();
        //        DateTime.TryParse("2010/03/31 12:10:00 AM", out dtInfringement); //DateTime.Now;

        //        cfReqInfringement.Date = DateTime.Now; 
        //        //cfReqInfringement.DirectionFrom = "";
        //        //cfReqInfringement.DirectionTo = "";
        //        cfReqInfringement.FilmN = "0";
        //        //cfReqInfringement.GPSXCoord = "";
        //        //cfReqInfringement.GPSYCoord = "";
        //        cfReqInfringement.IssAuthority = "4001";
        //        //cfReqInfringement.LicImage = "";
        //        //cfReqInfringement.OtherLocInfo = "";
        //        cfReqInfringement.Province = "4";
        //        cfReqInfringement.RefN = "384";
        //        cfReqInfringement.RegAuthority = "4093";
        //        //cfReqInfringement.RouteN = "";
        //        cfReqInfringement.StreetNameA = "N1";
        //        //cfReqInfringement.StreetNameB = "";
        //        //cfReqInfringement.Suburb = "";
        //        //need the time component, except the time field is of date type
        //        cfReqInfringement.Time = DateTime.Now; 
        //        cfReqInfringement.VehImage = ReadImage();

        //        //charge
        //        cfReqCharge.Code = "4549";
        //        cfReqCharge.SpeedRead1 = "100.00";
        //        cfReqCharge.SpeedRead2 = "110.00";

        //        //vehicle
        //        cfReqVehicle.LicN = "CA10051";

        //        //officer
        //        cfReqOfficer.InfrastructureN = "10010034";

        //        cfRequest.Infringement = cfReqInfringement;
        //        cfRequest.Charge = cfReqCharge;
        //        cfRequest.Officer = cfReqOfficer;
        //        cfRequest.Vehicle = cfReqVehicle;

        //        infringeService.Url = "https://wstst.enatis.co.za/eNaTIS_PEI/PayFine_InfringementsSoapHttpPort";
        //        //infringeService.SoapVersion = System.Web.Services.Protocols.SoapProtocolVersion.Soap11;
        //        //infringeService.CookieContainer = new CookieContainer();

        //        //ICredentials cred = new NetworkCredential("4468A004", "TESTER01");
        //        //infringeService.Credentials = cred;

        //        SoapContext sc = infringeService.RequestSoapContext;

        //        Microsoft.Web.Services3.Security.Tokens.UsernameToken uToken = new Microsoft.Web.Services3.Security.Tokens.UsernameToken("4468A004", "TESTER01", Microsoft.Web.Services3.Security.Tokens.PasswordOption.SendPlainText);
        //        sc.Security.Tokens.Add(uToken);

        //        //sc.Security.Timestamp.TtlInSeconds = 60;
                
        //        cfResponse = infringeService.processCameraFile(cfRequest);
        //        //ServerName.WebServiceName CallWebService = new ServerName.WebServiceName();
        //        //String sGetValue = CallWebService.MethodName();
        //        //Label1.Text = sGetValue;
        //    }
        //    catch (Exception ex)
        //    {
        //        string strMsg = ex.Message;
        //    }

        //}

        //public byte[] ReadImage()
        //{
        //    byte[] img = new byte[0];

        //    using (FileStream fileStream = File.Open(@"C:\Development\eNATIS\test images\2868_0.jpg", FileMode.Open))
        //    {
        //        using (BinaryReader binaryReader = new BinaryReader(fileStream))
        //        {
        //            byte[] buff = new byte[100];
        //            int bytesRead;
        //            while ((bytesRead = binaryReader.Read(buff, 0, 100)) > 0)
        //            {
        //                int arrSize = img.Length;
        //                Array.Resize(ref img, arrSize + bytesRead);
        //                Array.Copy(buff, 0, img, arrSize, bytesRead);
        //            }
        //        }
        //    }

        //    return img;
        //}
        #endregion
    }
}
