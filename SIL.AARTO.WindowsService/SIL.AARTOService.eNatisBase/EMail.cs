﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
//using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Diagnostics;
using SIL.AARTOService.Library;

namespace SIL.AARTOService.eNatisBase
{
    public class Email
    {
        // Fields
        private string smtp;
        private string fromName = "";
        private string fromEmail = "";

        private string toName = "";
        private string toEmail = "";

        public Email()
        {
            try
            {
                //smtp = System.Configuration.ConfigurationSettings.AppSettings["smtp"].ToString();
                //fromName = System.Configuration.ConfigurationSettings.AppSettings["EmailFromName"].ToString();
                //fromEmail = System.Configuration.ConfigurationSettings.AppSettings["EmailFrom"].ToString();
                //toName = System.Configuration.ConfigurationSettings.AppSettings["ReceiverName"].ToString();
                //toEmail = System.Configuration.ConfigurationSettings.AppSettings["EmailTo"].ToString();

                toEmail = Parameters.EmailToAdministrator;
                if (!SendEmailManager.CheckEmailAddress(toEmail))
                    Logger.Write("Email address is invalid", "General", (int)TraceEventType.Error, 0, TraceEventType.Error);

            }
            catch (Exception ex)
            {
                Logger.Write("Email initalize failed!" + ex.Message, "General", (int)TraceEventType.Error, 0, TraceEventType.Error);
            }
        }

        public int Send(string strSubject, string message)
        {
            //MailAddress from = new MailAddress(fromEmail, fromName);
            //MailAddress to = new MailAddress(toEmail, toName);

            //MailMessage mail = new MailMessage(from, to);
            //mail.Subject = strSubject;
            //mail.Body = message;
            //mail.BodyEncoding = System.Text.Encoding.UTF8;
            //mail.Sender = from;

            try
            {
                //SmtpClient mailClient = new SmtpClient();
                //mailClient.Host = smtp;
                //mailClient.UseDefaultCredentials = true;
                //mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                ////Send delivers the message to the mail server


                //mailClient.Send(mail);

                SendEmailManager.SendEmail(toEmail, strSubject, message);
            }
            catch (Exception smtpEx)
            {
                Logger.Write("Send email failed!" + smtpEx.Message, "General", (int)TraceEventType.Error, 0, TraceEventType.Error);
            }
            return 0;
        }
    }
}