﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data;
using System.Configuration;
using System.Transactions;
using SIL.ServiceLibrary;
using SIL.AARTOService;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.ServiceQueueLibrary.DAL.Data;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;
using System.Xml;
using System.Text.RegularExpressions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data.SqlClient;
using System.Collections.Specialized;
using SIL.AARTO.DAL.Data;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.BLL.Utility.Printing;
using System.Data.SqlClient;
using System.Globalization;
using SIL.AARTO.BLL.Report;
using DataRepository = SIL.AARTO.DAL.Data.DataRepository;
using EntityState = SIL.AARTO.DAL.Entities.EntityState;
using IsolationLevel = System.Transactions.IsolationLevel;

namespace SIL.AARTOService.ReportEngine
{
    public class ReportEngineClientService : ClientService
    {
        public static string artoConnectionString = string.Empty;
        string currentAutCode = string.Empty;
        string _lastUser = "Report Engine";
        AARTORules rules = AARTORules.GetSingleTon();
        AARTOServiceBase aartoBase { get { return (AARTOServiceBase)_serviceHelper; } }
        readonly ReportAdditionalRequestService rarService = new ReportAdditionalRequestService();
        PrintAdditionalReport parService = new PrintAdditionalReport();

        public ReportEngineClientService()
            : base("", "", new Guid("68C5671E-8427-4613-BC02-8668A767BF22"))
        {
            this._serviceHelper = new AARTOServiceBase(this, false);
            artoConnectionString = aartoBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            InitializeAartoConnectionStringForNetTier(artoConnectionString);
        }

        public override void InitialWork(ref QueueLibrary.QueueItem item)
        {
            
        }

        public override void MainWork(ref List<QueueLibrary.QueueItem> queueList)
        {
            // Oscar 2013-04-02 added
            Logger.Info(ResourceHelper.GetResource("StartProcessingReportList"));
            Logger.SaveToDB();

            SIL.AARTO.DAL.Entities.TList<ReportConfig> rcList = SIL.AARTO.BLL.Report.ReportManager.GetAllReportConfig();
            foreach (ReportConfig rc in rcList)
            {
                try
                {
                    var listPrint = EngineFactory.GetByReportConfig(rc);
                    listPrint.LastUser = aartoBase.LastUser;    // 2013-07-23 add by Henry
                    ProcessAdditionalRequest(rc, listPrint);

                    bool isPrint = false;
                    if (rc.AutomaticGenerate && !rc.IsCustom && !rc.IsFreeStyle)
                    {
                        DateTime? dt = rc.AutomaticGenerateLastDatetime == null ? DateTime.MinValue : rc.AutomaticGenerateLastDatetime;
                        DateTime dtCompare = DateTime.MinValue;
                        switch (rc.AutomaticGenerateIntervalType.Trim().ToLower())
                        {
                            case "hours":
                                dtCompare = dt.Value.AddHours(rc.AutomaticGenerateInterval.Value);
                                if (dtCompare <= DateTime.Now)
                                    isPrint = true;
                                break;
                            case "days":
                                dtCompare = dt.Value.AddDays(rc.AutomaticGenerateInterval.Value);
                                if (Convert.ToDateTime(dtCompare.ToShortDateString()) <= Convert.ToDateTime(DateTime.Now.ToShortDateString()))
                                    isPrint = true;
                                break;
                            case "months":
                                dtCompare = dt.Value.AddMonths(rc.AutomaticGenerateInterval.Value);
                                if (dtCompare <= DateTime.Now && DateTime.Now.Date.Day == 1)
                                    isPrint = true;
                                break;
                            case "daily":
                                if (dt.Value.Date != DateTime.Now.Date)
                                {
                                    isPrint = true;
                                }
                                break;
                            case "weekly":
                                int iCurrentWeek = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFullWeek, DayOfWeek.Saturday);
                                int iCompareWeek = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(dt.Value.Date, CalendarWeekRule.FirstFullWeek, DayOfWeek.Saturday);

                                if (iCurrentWeek != iCompareWeek && DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                                {
                                    isPrint = true;
                                }
                                break;
                            case "monthly":
                                if ((dt.Value.Date.Month != DateTime.Now.Date.Month || dt.Value.Date.Year != DateTime.Now.Date.Year) && DateTime.Now.Date.Day == 1)
                                {
                                    isPrint = true;
                                }
                                break;
                        }

                        if (isPrint)
                        {
                            // Oscar 2013-04-02 added
                            Logger.Info(ResourceHelper.GetResource("StartProcessingReport", rc.ReportCode, rc.ReportName));
                            Logger.SaveToDB();

                            listPrint.WriteErrorMessage = s => this.Logger.Error(s);

                            // Oscar 2013-04-02 added
                            listPrint.WriteLog = (logType, message, flush) =>
                            {
                                var exception = message as Exception;
                                switch (logType)
                                {
                                    case 1:
                                        if (exception != null)
                                            Logger.Warning(exception);
                                        else
                                            Logger.Warning(Convert.ToString(message));
                                        break;
                                    case 2:
                                        if (exception != null)
                                            Logger.Error(exception);
                                        else
                                            Logger.Error(Convert.ToString(message));
                                        break;
                                    case 0:
                                    default:
                                        if (exception != null)
                                            Logger.Info(exception);
                                        else
                                            Logger.Info(Convert.ToString(message));
                                        break;
                                }

                                if (flush)
                                    Logger.SaveToDB();
                            };

                            listPrint.connStr = artoConnectionString;

                            if (rc.ReportCode.Equals(((int)ReportConfigCodeList.PunchStatics).ToString()))
                                listPrint.onCreated = e => SendEmailManager.SendEmail(e.ReceiverList, e.Subject, e.Content, e.AttachmentList, e.AttachmentList[0].Replace(".xls", ""));

                            listPrint.CreateEngine(rc.RcIntNo);

                            rc.AutomaticGenerateLastDatetime = DateTime.Now;
                            // 2013-07-16 add by Henry for LastUser
                            rc.LastUser = aartoBase.LastUser;
                            ReportManager.UpdateReport(rc);

                            this.Logger.Info(ResourceHelper.GetResource("ReportEngineServicePrinted", rc.ReportCode, rc.ReportName));
                            Logger.SaveToDB();
                        }
                    }
                }
                catch (Exception ex)
                {
                    aartoBase.ErrorProcessing(ResourceHelper.GetResource("ReportEngineService_Error", BuildMessageForException(ex), rc.ReportCode));
                }
            }

            // Oscar 2013-04-02 added
            Logger.Info(ResourceHelper.GetResource("ReportEngineIsGoingToSleep"));
            Logger.SaveToDB();

            //edited by Jacob 20121115 start
            //change stop to sleep.
            //// 2012.10.22 Nick added for the service will loop the mainwork for no queue to read if not stop it.
            //aartoBase.StopService();
            MustSleep = true;
            //edited by Jacob 20121115 end
        }
        
        void InitializeAartoConnectionStringForNetTier(string dynamicConnectionStr)
        {
            SqlNetTiersProvider provider = new SqlNetTiersProvider();
            NameValueCollection collection = new NameValueCollection();
            collection.Add("UseStoredProcedure", "false");
            collection.Add("EnableEntityTracking", "true");
            collection.Add("EntityCreationalFactoryType", "SIL.AARTO.DAL.Entities.EntityFactory");
            collection.Add("EnableMethodAuthorization", "false");
            collection.Add("ConnectionString", dynamicConnectionStr);
            collection.Add("ConnectionStringName", "SIL.AARTO.DAL.Data.ConnectionString");
            collection.Add("ProviderInvariantName", "System.Data.SqlClient");

            provider.Initialize("SqlNetTiersProvider", collection);

            DataRepository.LoadProvider(provider, true);
        }

        /// <summary>
        /// Builds the message for an exception. 2012.11.07 Nick add
        /// </summary>
        /// <param name="ex">The exception.</param>
        /// <returns>A string containing the full exception message.</returns>
        protected string BuildMessageForException(Exception ex)
        {
            var sb = new StringBuilder();
            sb.Append("Type: ")
                .AppendLine(ex.GetType().FullName)
                .Append("Message: ")
                .AppendLine(ex.Message)
                .Append("Thrown by Method: ")
                .AppendFormat("{0}.{1}", ex.Source.GetType().FullName, ex.TargetSite.Name)
                .AppendLine()
                .Append("Stack Trace: ")
                .AppendLine(ex.StackTrace);

            if (ex.InnerException != null)
            {
                sb.AppendLine("Inner Exception: ")
                    .AppendLine(this.BuildMessageForException(ex.InnerException));
            }

            return sb.ToString();
        }

        // Oscar 2013-02-26 added additional requests
        void ProcessAdditionalRequest(ReportConfig rc, ListPrintEngine listEngine)
        {
            if (rc == null
                || !rc.RcAllowAddRequest
                || rc.RcIntNo <= 0
                || listEngine == null)
                return;

            var query = new ReportAdditionalRequestQuery();
            query.Append(ReportAdditionalRequestColumn.RcIntNo, rc.RcIntNo.ToString(CultureInfo.InvariantCulture));
            query.AppendLessThanOrEqual(ReportAdditionalRequestColumn.RarActionDate, DateTime.Now.ToString(CultureInfo.InvariantCulture));
            query.AppendIsNull(ReportAdditionalRequestColumn.PrintedDate);
            var rarList = rarService.Find(query, "RequestedUser, RARActionDate, RARIntNo");

            if (rarList != null && rarList.Count > 0)
            {
                listEngine.WriteErrorMessage = s => Logger.Error(s);
                listEngine.connStr = artoConnectionString;

                rarList.ForEach(r =>
                {
                    try
                    {
                        r.RcIntNoSource = rc;

                        parService.PrintAditionalReport(r);

                        listEngine.AdditionalRequest = r;
                        listEngine.CreateEngine(rc.RcIntNo);

                        UpdateAdditionalRequest(r);
                    }
                    catch (Exception ex)
                    {
                        aartoBase.ErrorProcessing(ex.Message);
                    }
                });
            }
            listEngine.AdditionalRequest = null;
        }

        void UpdateAdditionalRequest(ReportAdditionalRequest rar, bool locked = true)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Suppress, new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadCommitted,
                    Timeout = System.Transactions.TransactionManager.MaximumTimeout
                }))
                {
                    if (rar == null
                        || rar.RarIntNo <= 0
                        || (!locked
                            && (rar = this.rarService.GetByRarIntNo(rar.RarIntNo)) == null))
                        return;

                    rar.PrintedDate = locked ? DateTime.Now : (DateTime?)null;
                    rar.LastUser = rar.RcIntNoSource != null ? rar.RcIntNoSource.ReportName : "ReportEngine";
                    rar.EntityState = EntityState.Changed;
                    this.rarService.Save(rar);

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                aartoBase.ErrorProcessing(ex.Message);
            }
        }

    }

}
