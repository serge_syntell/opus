﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;
using System.Text.RegularExpressions;

namespace SIL.AARTOService.ReportEngine
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {

            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.ReportEngine"
                ,
                DisplayName = "SIL.AARTOService.ReportEngine"
                ,
                Description = "SIL.AARTOService.ReportEngine"
            };

            ProgramRun.InitializeService(new ReportEngine(), serviceDescriptor, args);
        }
    }
}