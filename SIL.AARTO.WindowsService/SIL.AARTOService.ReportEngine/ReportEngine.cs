﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.ReportEngine
{
    public partial class ReportEngine : ServiceHost
    {
        public ReportEngine()
            : base("", new Guid("82357F9A-C959-4920-91A7-9244CBE9FF1C"), new ReportEngineClientService())
        {
            InitializeComponent();
        }
    }
}
