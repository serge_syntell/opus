﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SIL.AARTOService.DAL;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.QueueLibrary;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using Stalberg.ThaboAggregator.Objects;

namespace SIL.ThaboService.ImportEasyPayFiles
{
    public class ImportEasyPayFilesService : ClientService
    {
        public ImportEasyPayFilesService()
            : base("", "", new Guid("8A19DF09-449E-4CBE-A30F-5B9633DDA0DB"))
        {

            this._serviceHelper = new AARTOServiceBase(this);
            thaboConnectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.Thabo, ServiceConnectionTypeList.DB);
            easyPayExportPath = AARTOBase.GetConnectionString(ServiceConnectionNameList.EasyPayExportPath, ServiceConnectionTypeList.UNC);
            easyPayPickupFolder = AARTOBase.GetConnectionString(ServiceConnectionNameList.EasyPayPickupFolder, ServiceConnectionTypeList.UNC);
            easyPayDropFolder = AARTOBase.GetConnectionString(ServiceConnectionNameList.EasyPayDropFolder, ServiceConnectionTypeList.UNC);
            ServiceUtility.InitializeNetTier(thaboConnectStr);

            AARTOBase.OnServiceStarting = () =>
            {
                if (string.IsNullOrEmpty(easyPayExportPath))
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoParam"), "EasyPayExportPath"), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }

                var exportDir = new DirectoryInfo(easyPayExportPath);
                if (!exportDir.Exists)
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoEasyPayExportPath"), exportDir.FullName), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }

                if (string.IsNullOrEmpty(easyPayPickupFolder))
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoParam"), "EasyPayPickupFolder"), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }

                var pickupDir = new DirectoryInfo(easyPayPickupFolder);
                if (!pickupDir.Exists)
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoEasyPayPickupPath"), pickupDir.FullName), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }

                if (string.IsNullOrEmpty(easyPayDropFolder))
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoParam"), "EasyPayDropFolder"), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }

                var dropDir = new DirectoryInfo(easyPayDropFolder);
                if (!dropDir.Exists)
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoEasyPayDropPath"), dropDir.FullName), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }

                emailToAdministrator = AARTOBase.GetServiceParameter("EmailToAdministrator");
                if (string.IsNullOrEmpty(emailToAdministrator))
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoEmailAddress"), "EmailToAdministrator"), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }

            };

        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        string thaboConnectStr = string.Empty;
        string easyPayExportPath = string.Empty;
        string easyPayPickupFolder = string.Empty;
        string easyPayDropFolder = string.Empty;
        string emailToAdministrator = string.Empty;

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            // Send EasyPay Files to EasyPay
            AARTOBase.LogProcessing(ResourceHelper.GetResource("SendingEasyPay"), LogType.Info);
            SendEasyPayFiles();
            AARTOBase.LogProcessing(ResourceHelper.GetResource("FinishedSendEasyPay"), LogType.Info);

            // Get Holdback Files from EasyPay
            AARTOBase.LogProcessing(ResourceHelper.GetResource("GettingHoldbackFiles"), LogType.Info);
            var holdbackFiles = new List<FileInfo>();
            GetHoldbackFiles(holdbackFiles);

            // Process the EasyPay HoldBack files
            AARTOBase.LogProcessing(ResourceHelper.GetResource("ProcessingHoldBack"), LogType.Info);
            ProcessHoldBackFiles(holdbackFiles);
            AARTOBase.LogProcessing(ResourceHelper.GetResource("HoldBackComplete"), LogType.Info);

            holdbackFiles.Clear();
            // Get any SOF Files
            AARTOBase.LogProcessing(ResourceHelper.GetResource("ProcessSOF"), LogType.Info);
            ProcessSOFFiles();
            AARTOBase.LogProcessing(ResourceHelper.GetResource("ProcessSOFComplete"), LogType.Info);

            //Jerry 2014-05-23 add
            this.MustSleep = true;
        }

        private void SendEasyPayFiles()
        {
            DirectoryInfo exportDir = new DirectoryInfo(easyPayExportPath);

            // Loop the Traffic Data Files and send them the the EasyPay pickup folder
            string destination;
            foreach (FileInfo file in exportDir.GetFiles("*" + TrafficDataFile.EXTENSION))
            {
                destination = Path.Combine(easyPayPickupFolder, file.Name);
                try
                {
                    file.CopyTo(destination, true);
                    file.Delete();
                    //Logger.Write(string.Format("Easypay file moved to Pickup folder: {0}", destination), "General", (int)TraceEventType.Information);
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("MoveEasyPayFile", destination), LogType.Info);
                }
                catch (Exception ex)
                {
                    //Logger.Write(ex.Message, "General", (int)TraceEventType.Critical);
                    AARTOBase.LogProcessing(ex.Message, LogType.Error);
                }
            }
        }

        private void GetHoldbackFiles(List<FileInfo> files)
        {
            try
            {
                // Make sure the EasyPay Pickup Folder exists
                var dir = new DirectoryInfo(easyPayPickupFolder);
                if (!dir.Exists)
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoEasyPayPickupPath"), dir.FullName), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }

                // Get the Holdback files in the EasyPay Pickup Folder
                foreach (FileInfo file in dir.GetFiles("HB*", SearchOption.TopDirectoryOnly))
                    files.Add(file);
            }
            catch (Exception ex)
            {
                //Logger.Write(string.Format("Error in GetHoldbackFiles: {0} ", ex), "General", (int)TraceEventType.Error);
                AARTOBase.LogProcessing(ResourceHelper.GetResource("ErrGetHoldbackFile", ex), LogType.Error);
            }
        }

        private void ProcessHoldBackFiles(IEnumerable<FileInfo> files)
        {
            try
            {
                var sb = new StringBuilder();

                foreach (FileInfo file in files)
                {
                    var tdf = new TrafficDataFile(file.FullName);
                    tdf.Read();
                    if (tdf.HasErrors)
                    {
                        // Log the Files to the logger and email them
                        BuildErrorMessage(sb, tdf);

                        // Process files into the database
                        SetHoldbackErrors(tdf);
                    }

                    // Cleanup the processed File
                    try
                    {
                        file.Delete();
                    }
                    catch (IOException e)
                    {
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("ErrDelHoldbackFile", file.Name, e), LogType.Error);
                    }
                }

                // Email the error messages
                if (sb.Length > 0)
                {
                    string subject = ResourceHelper.GetResource("EmailSubjectErrHoldBack");
                    sb.Insert(0, ResourceHelper.GetResource("EmailContentErrHoldBack"));
                    SendEmailManager.SendEmail(emailToAdministrator, subject, sb.ToString());

                    AARTOBase.LogProcessing(ResourceHelper.GetResource("SendEmailAboutHoldback", emailToAdministrator), LogType.Info);
                }
            }
            catch (Exception ex)
            {
                //Logger.Write(string.Format("Error in ProcessHoldBackFiles: {0} ", ex), "General", (int)TraceEventType.Error);
                AARTOBase.LogProcessing(ResourceHelper.GetResource("ErrProcessHoldBack", ex), LogType.Error, ServiceOption.BreakAndStop);
            }
        }

        private void BuildErrorMessage(StringBuilder sb, TrafficDataFile file)
        {
            try
            {
                sb.Append("\nFile: ");
                sb.Append(Path.GetFileName(file.FileName));
                sb.Append(":\n");

                foreach (HoldbackFileError error in file.Errors)
                {
                    sb.Append("Error          - ");
                    sb.Append(error.Error);
                    sb.Append("\nAt line         - ");
                    sb.Append(error.LineNumber);
                    sb.Append('\n');
                    if (error.Transaction != null)
                    {
                        sb.Append("EasyPay number  - ");
                        sb.Append(error.Transaction.EasyPayNumber);
                        sb.Append("\nAction             - ");
                        sb.Append(error.Transaction.EasyPayAction);
                        sb.Append("\n\n");
                    }
                }
            }
            catch (Exception ex)
            {
                //Logger.Write(string.Format("Error in BuildErrorMessage: {0} ", ex), "General", (int)TraceEventType.Error);
                AARTOBase.LogProcessing(ResourceHelper.GetResource("ErrBuildErrMessage", ex), LogType.Error);
            }
        }

        /// <summary>
        /// Sets the EasyPay hold-back file errors in the database.
        /// </summary>
        /// <param name="tdf">The TDF.</param>
        public void SetHoldbackErrors(TrafficDataFile tdf)
        {
            try
            {
                DBHelper db = new DBHelper(this.thaboConnectStr);
                SqlParameter[] paras;

                // Update the row-level errors
                foreach (HoldbackFileError err in tdf.Errors)
                {
                    if (err.Transaction == null)
                        continue;

                    paras = new SqlParameter[3];
                    paras[0] = new SqlParameter("@EasyPayNumber", err.Transaction.EasyPayNumber);
                    paras[1] = new SqlParameter("@EasyPayAction", err.Transaction.EasyPayAction);
                    paras[2] = new SqlParameter("@Error", err.Error);
                    string errMsg = string.Empty;
                    db.ExecuteNonQuery("EasyPayTranSetError", paras, out errMsg);
                }

                // Update any file level errors
                foreach (HoldbackFileError err in tdf.Errors)
                {
                    if (err.Transaction != null)
                        continue;
                }
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex.Message, LogType.Error);
            }
        }

        private void ProcessSOFFiles()
        {
            DirectoryInfo dir = new DirectoryInfo(easyPayDropFolder);
            if (!dir.Exists)
            {
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoEasyPayDropPath"), dir.FullName), LogType.Error, ServiceOption.BreakAndStop);
                return;
            }

            foreach (FileInfo file in dir.GetFiles())
            {
                 //jerry 2012-1-16 add transaction
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(10)))
                {
                    try
                    {
                        // Parse them into the SOF File object
                        StandardOutputFile sof = new StandardOutputFile(file);
                        sof.FileName = file.Name;

                        // Check that this is the next file in order for the LA
                        CheckSequenceNumber(sof);
                        if (!sof.IsValid)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append(string.Format(ResourceHelper.GetResource("EmailContentErrSOF"), sof.FileName, sof.Header.FileGenerationNumber, AARTOBase.LastUser));
                            AARTOBase.LogProcessing(sb.ToString(), LogType.Warning);
                            
                            SendEmailManager.SendEmail(emailToAdministrator, ResourceHelper.GetResource("EmailSubjectErrSOF"), sb.ToString());
                            continue;
                        }

                        // Record the SOF record details
                        RecordSOFContents(sof);

                        // Check and link notices to the SOF Transactions
                        CheckEasyPayNumbers(sof);
                        foreach (SOFTransaction tran in sof.Transactions)
                        {
                            if (tran.Payment == null)
                                continue;

                            if (tran.Payment.NotIntNo < 0)
                            {
                                AARTOBase.LogProcessing(ResourceHelper.GetResource("ErrEasyPayNum", tran.Payment.EasyPayNumber), LogType.Error);
                                continue;
                            }

                            // See if there's been a transaction already for this EasyPay number
                            if (!CheckDuplicatePayment(tran))
                            {
                                // Record it as a problem Payment
                                RecordEasyPayProblemPayment(tran);

                                //mrs 20081001 Can't send a payment notification in this case as a payment (of some nature) has previously been processed
                                //Logger.Write(string.Format("The EasyPay number {0} already has at least one payment processed for it.", tran.Payment.EasyPayNumber), "General", (int)TraceEventType.Warning);
                                AARTOBase.LogProcessing(ResourceHelper.GetResource("EasyPayNumHasProcessed", tran.Payment.EasyPayNumber), LogType.Warning);
                                continue;
                            }

                            // Check if the payment amount matches the original fine amount
                            if (tran.Payment.Amount == tran.Payment.OriginalAmount)
                            {
                                // Record the payment details
                                RecordEasyPayPayment(tran.Payment);
                            }
                            else
                            {
                                // mrs 20081002 3970 Record a remote payment pending
                                RecordEasyPayPaymentProblemPending(tran.Payment, "payment and fine amount do not match");

                                // Record it as a problem Payment
                                RecordEasyPayProblemPayment(tran);
                            }

                        }
                        // Log the file before deleting it
                        //Logger.Write(string.Format("Successfully Processed SOF File: {0} at {1}", sof.FileName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")), "General", (int)TraceEventType.Information);
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("ProcessSOFSucess", sof.FileName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")), LogType.Info);

                        // Cleanup
                        file.Delete();
                    }
                    catch (Exception ex)
                    {
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("ErrProcessSOF", file.Name, ex.Message, ex.StackTrace), LogType.Error);
                    }

                    scope.Complete();
                }
            }
        }

        /// <summary>
        /// Checks that the sequence number on the SOF file is the next in sequence for the LA
        /// </summary>
        /// <param name="sof">The SOF file.</param>
        private void CheckSequenceNumber(StandardOutputFile sof)
        {
            SqlConnection con = new SqlConnection(this.thaboConnectStr);
            var com = new SqlCommand("EasyPayCheckSequenceNo", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@ReceiverIdentifier", SqlDbType.Int, 4).Value = sof.Header.ReceiverID;

            try
            {
                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                if (reader.Read())
                {
                    int lastSequenceNo = (int)reader["LastSequence"];
                    sof.IsValid = (lastSequenceNo + 1 == sof.Header.FileGenerationNumber);

                    if (reader["AutIntNo"] != DBNull.Value)
                    {
                        sof.Authority = new SOFAuthority();
                        sof.Authority.ID = (int)reader["AutIntNo"];
                        sof.Authority.Name = (string)reader["AutName"];
                        sof.Authority.Email = reader["Email"].ToString();
                    }
                    reader.Close();
                    reader.Dispose();
                }
                com.Dispose();
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex.Message, LogType.Error, ServiceOption.BreakAndStop);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        /// <summary>
        /// Records the contents of the SOF file into the database.
        /// </summary>
        /// <param name="sof">The SOF object.</param>
        private void RecordSOFContents(StandardOutputFile sof)
        {
            DBHelper db = new DBHelper(this.thaboConnectStr);

            // Record the SOF File header and footer
            SqlParameter[] paras = new SqlParameter[11];
            paras[0] = new SqlParameter("@FileName", sof.FileName);
            paras[1] = new SqlParameter("@Version", sof.Header.Version);
            paras[2] = new SqlParameter("@ReceiverIdentifier", sof.Header.ReceiverID);
            paras[3] = new SqlParameter("@TimeStamp", sof.Header.TimeStamp);
            paras[4] = new SqlParameter("@FileGenerationNo", sof.Header.FileGenerationNumber);
            paras[5] = new SqlParameter("@NoPayments", sof.Footer.NumberOfPayments);
            paras[6] = new SqlParameter("@ValuePayments", sof.Footer.ValueOfPayments);
            paras[7] = new SqlParameter("@ValueFees", sof.Footer.ValueOfFees);
            paras[8] = new SqlParameter("@NoTenders", sof.Footer.NumberOfTenders);
            paras[9] = new SqlParameter("@ValueTenders", sof.Footer.ValueOfTenders);
            paras[10] = new SqlParameter("@ValueBankCosts", sof.Footer.ValueOfBankCharges);

            string errMsg = string.Empty;
            int sofIntNo = 0;
            sofIntNo = Convert.ToInt32(db.ExecuteScalar("EasyPaySOFFileAdd", paras, out errMsg));
            sof.ID = sofIntNo;

            // Record each line in the database 
            int estIntNo = 0;
            foreach (SOFTransaction tran in sof.Transactions)
            {
                paras = new SqlParameter[9];
                paras[0] = new SqlParameter("@SOFIntNo", sofIntNo);
                paras[1] = new SqlParameter("@CollectorIdentifier", tran.CollectorID);
                paras[2] = new SqlParameter("@Date", tran.TimeStamp);
                paras[3] = new SqlParameter("@PointOfService", tran.PointOfSale);
                if (tran.Trace.Length > 0)
                    paras[4] = new SqlParameter("@Trace", tran.Trace);
                else
                    paras[4] = new SqlParameter("@Trace", null);
                paras[5] = new SqlParameter("@Amount", tran.Payment.Amount);
                paras[6] = new SqlParameter("@Fee", tran.Payment.Fee);
                paras[7] = new SqlParameter("@EasyPayNo", tran.Payment.EasyPayNumber);
                paras[8] = new SqlParameter("@LastUser", AARTOBase.LastUser);

                estIntNo = Convert.ToInt32(db.ExecuteScalar("EasyPaySOFTransactionAdd", paras, out errMsg));
                tran.ID = estIntNo;

                // Record each tender amount
                foreach (SOFTender tender in tran.Payment.Tendered)
                {
                    paras = new SqlParameter[5];
                    paras[0] = new SqlParameter("@ESTIntNo", estIntNo);
                    paras[1] = new SqlParameter("@Amount", tender.Amount);
                    paras[2] = new SqlParameter("@BankCharge", tender.BankCharge);
                    paras[3] = new SqlParameter("@TenderType", tender.TenderType);
                    if (tender.AccountNumber.Length > 0)
                        paras[4] = new SqlParameter("@AccountNumber", tender.AccountNumber);
                    else
                        paras[4] = new SqlParameter("@AccountNumber", null);

                    Convert.ToInt32(db.ExecuteScalar("EasyPaySOFTenderAdd", paras, out errMsg));
                }
            }

        }

        /// <summary>
        /// Checks the SOF file transactions for referenced notices.
        /// </summary>
        /// <param name="sof">The sof.</param>
        private void CheckEasyPayNumbers(StandardOutputFile sof)
        {
            SqlConnection con = new SqlConnection(this.thaboConnectStr);
            using (var cmd = new SqlCommand("EasyPayCheckNotice", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param = cmd.Parameters.Add("@EPNo", SqlDbType.VarChar, 30);
                con.Open();

                foreach (SOFTransaction tran in sof.Transactions)
                {
                    if (tran.Payment == null)
                        continue;

                    param.Value = tran.Payment.EasyPayNumber;

                    try
                    {
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            tran.Payment.NotIntNo = Convert.ToInt32(reader[0]);
                            tran.Payment.OriginalAmount = Convert.ToDecimal(reader[1]);
                        }
                        reader.Close();
                        reader.Dispose();
                    }
                    catch (Exception ex)
                    {
                        AARTOBase.LogProcessing(ex.Message, LogType.Error);
                    }
                    //finally
                    //{
                    //    con.Close();
                    //    con.Dispose();
                    //}
                }
                if (con != null)
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        /// <summary>
        /// Checks if there is a duplicate payment.
        /// </summary>
        /// <param name="tran">The easy pay transaction object.</param>
        /// <returns>
        /// 	<c>false</c> if there have already been payments on the EasyPay number
        /// </returns>
        private bool CheckDuplicatePayment(SOFTransaction tran)
        {
            DBHelper db = new DBHelper(this.thaboConnectStr);
            SqlParameter[] paras = new SqlParameter[2];
            paras[0] = new SqlParameter("@EasyPayNo", tran.Payment.EasyPayNumber);
            paras[1] = new SqlParameter("@SOFIntNo", tran.SOFFile.ID);
            string errMsg = string.Empty;
            int transactions = Convert.ToInt32(db.ExecuteScalar("EasyPayCheckDuplicateTran", paras, out errMsg));
            return (transactions == 1);
        }

        private void RecordEasyPayProblemPayment(SOFTransaction tran)
        {
            DBHelper db = new DBHelper(this.thaboConnectStr);
            SqlParameter[] paras = new SqlParameter[2];
            paras[0] = new SqlParameter("@NotIntNo", tran.Payment.NotIntNo);
            paras[1] = new SqlParameter("@ESEIntNo", tran.ID);
            string errMsg = string.Empty;

            try
            {
                int id = Convert.ToInt32(db.ExecuteScalar("ProblemPaymentAdd", paras, out errMsg));
                if (id < 1)
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("ErrPaymentForNotice", tran.Payment.NotIntNo), LogType.Error);
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex.Message, LogType.Error);
            }
        }

        /// <summary>
        /// Records the easy pay payment.
        /// </summary>
        /// <param name="payment">The payment.</param>
        private void RecordEasyPayPayment(SOFPayment payment)
        {
            DBHelper db = new DBHelper(this.thaboConnectStr);
            SqlParameter[] paras = new SqlParameter[11];
            paras[0] = new SqlParameter("@NotIntNo", payment.NotIntNo);
            paras[1] = new SqlParameter("@PartnerID", "EP");
            paras[2] = new SqlParameter("@TerminalID", string.Empty);
            paras[3] = new SqlParameter("@OperatorName", "EasyPay");
            paras[4] = new SqlParameter("@MsgDateTime", DateTime.Now);
            paras[5] = new SqlParameter("@UniqueNumber", DateTime.Now.ToString("yyyyMMddHHmmssff"));
            paras[6] = new SqlParameter("@Source", "EP");
            paras[7] = new SqlParameter("@PaymentDate", payment.Transaction.TimeStamp);
            paras[8] = new SqlParameter("@StatusDescr", string.Empty);
            paras[9] = new SqlParameter("@Amount", payment.Amount);
            paras[10] = new SqlParameter("@TransType", "REC");
            string errMsg = string.Empty;

            try
            {
                int id = Convert.ToInt32(db.ExecuteScalar("PaymentAdd", paras, out errMsg));
                if (id < 1)
                {
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("ErrPaymentForNotice", payment.NotIntNo), LogType.Error);
                    return;
                }

                //paras = new SqlParameter[2];
                //paras[0] = new SqlParameter("@PayIntNo", id);
                //paras[1] = new SqlParameter("@LastUser", AARTOBase.LastUser);
                //db.ExecuteNonQuery("PaymentForExportAdd", paras, out errMsg);
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex.Message, LogType.Error);
            }
        }

        /// <summary>
        /// Records a pending payment for  transmission to the LA
        /// </summary>
        /// <param name="tran">The tran.</param>
        private void RecordEasyPayPaymentProblemPending(SOFPayment payment, string error)
        {
            DBHelper db = new DBHelper(this.thaboConnectStr);
            SqlParameter[] paras = new SqlParameter[11];
            paras[0] = new SqlParameter("@NotIntNo", payment.NotIntNo);
            paras[1] = new SqlParameter("@PartnerID", "EP");
            paras[2] = new SqlParameter("@TerminalID", string.Empty);
            paras[3] = new SqlParameter("@OperatorName", "EasyPay");
            paras[4] = new SqlParameter("@MsgDateTime", DateTime.Now);
            paras[5] = new SqlParameter("@UniqueNumber", DateTime.Now.ToString("yyyyMMddHHmmssff"));
            paras[6] = new SqlParameter("@Source", "EP");
            paras[7] = new SqlParameter("@PaymentDate", payment.Transaction.TimeStamp);
            paras[8] = new SqlParameter("@StatusDescr", error);
            paras[9] = new SqlParameter("@Amount", 0); //send no value
            paras[10] = new SqlParameter("@TransType", "PEN");
            string errMsg = string.Empty;

            try
            {
                int id = Convert.ToInt32(db.ExecuteScalar("PaymentAddPaymentPending", paras, out errMsg));
                if (id < 1)
                {
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("ErrPaymentPending", payment.NotIntNo), LogType.Error);
                }

                //paras = new SqlParameter[2];
                //paras[0] = new SqlParameter("@PayIntNo", id);
                //paras[1] = new SqlParameter("@LastUser", AARTOBase.LastUser);
                //db.ExecuteNonQuery("PaymentForExportAdd", paras, out errMsg);
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex.Message, LogType.Error);
            }
        }

    }
}
