﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using SIL.ServiceLibrary;

namespace SIL.ThaboService.ImportEasyPayFiles
{
    partial class ImportEasyPayFiles : ServiceHost
    {
        public ImportEasyPayFiles()
            : base("", new Guid("89705C5C-094D-464E-B2BD-D10607745473"), new ImportEasyPayFilesService())
        {
            InitializeComponent();
        }        
    }
}
