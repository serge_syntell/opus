﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using SIL.ServiceLibrary;

namespace SIL.ThaboService.ImportEasyPayFiles
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.ThaboService.ImportEasyPayFiles"
                ,
                DisplayName = "SIL.ThaboService.ImportEasyPayFiles"
                ,
                Description = "SIL.ThaboService.ImportEasyPayFiles"
            };

            ProgramRun.InitializeService(new ImportEasyPayFiles(), serviceDescriptor, args);
        }
    }
}
