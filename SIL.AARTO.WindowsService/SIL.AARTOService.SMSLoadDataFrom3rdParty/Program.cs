﻿using SIL.ServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace SIL.AARTOService.SMSLoadDataFrom3rdParty
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.SMSLoadDataFrom3rdParty"
                ,
                DisplayName = "SIL.AARTOService.SMSLoadDataFrom3rdParty"
                ,
                Description = "SIL AARTOService SMSLoadDataFrom3rdParty"
            };

            ProgramRun.InitializeService(new SMSLoadDataFrom3rdParty(), serviceDescriptor, args);
      
        }
    }
}
