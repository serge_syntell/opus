﻿
using SIL.AARTO.BLL.InfringerOption;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTOService.DAL;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.AARTOService.SMSLoadDataFrom3rdParty;
using SIL.QueueLibrary;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using Stalberg.TMS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SIL.AARTOService.SMSExtractOn2ndNotice
{
    public class SMSLoadDataFrom3rdPartyService : ServiceDataProcessViaFileSystem
    {
        public SMSLoadDataFrom3rdPartyService()
            : base("", "", new Guid("5D405666-A8F6-41AB-A74C-BC9ADE2AF87B"))
        {
            this._serviceHelper = new AARTOServiceBase(this);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.DestinationPath = AARTOBase.GetConnectionString(ServiceConnectionNameList.SMSDataFrom3rdParty, ServiceConnectionTypeList.UNC);
       
            AARTOBase.OnServiceStarting = () =>
            {
                noticeService = new NoticeService();
                chgService = new ChargeService();

                ServiceUtility.InitializeNetTier(this.connAARTODB);

                if (string.IsNullOrEmpty(DestinationPath))
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoParam"), "Load from SMSDataFrom3rdParty"), LogType.Error, ServiceOption.BreakAndShutdown);//--
                    return;
                } 
                AARTOBase.CheckFolder(this.DestinationPath);
              

            };
         
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        FileInfo fileInfo;
        NoticeService noticeService;
        ChargeService chgService;
        string connAARTODB = string.Empty;
        string SMSLoadDataFrom3rdPartyFolder = string.Empty;
        EvidencePackService evidencePackService = new EvidencePackService();
        List<EvidencePack> evidencePackList;
        Stalberg.TMS.NoticeDB noticedb;
     
         public override void InitialWork(ref QueueItem item)
        {
            if (item.Body == null)
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("InvalidQueueBody", item.Body), LogType.Error, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                return;
            }
            this.fileInfo = item.Body as FileInfo;
            if (!fileInfo.Extension.Equals(".csv"))
            {
             
                 item.Status = QueueItemStatus.Discard;
                 AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSFrom3rdPartyFileExtensionInvalid"), fileInfo.Name), LogType.Error, ServiceOption.Continue,item,QueueItemStatus.PostPone);
                 return;
            }
            item.IsSuccessful = true;
        }

        public override void MainWork(ref List<QueueLibrary.QueueItem> queueList)
        {

            foreach (QueueItem item in queueList.Where(q=>q.IsSuccessful))
            {
             
                this.fileInfo = item.Body as FileInfo;
                DataTable table = null;
                try
                {
                    table = CSVReader.ReadCSVFile(this.fileInfo.FullName, true);
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSFrom3rdPartyDataOpenedFileSuccessfully"), fileInfo.Name), LogType.Info, ServiceOption.Continue, item, QueueItemStatus.Discard);
                }
                catch (Exception ex)
                {
                    table = null;
                    item.Status = QueueItemStatus.PostPone;
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSFrom3rdPartyDataOpenedFileFailed"), fileInfo.Name,ex.Message), LogType.Error, ServiceOption.Continue, item, QueueItemStatus.UnKnown);

                }

                if (table!=null&&table.Rows.Count > 0)
                {

                    string notTicketNo = string.Empty;
                    string idNumber="";
                    string _TelephoneNumber="";
                    string _SuccessStatus = "";
                    bool send = false;
                    string errorMsg="";
                    foreach (DataRow dr in table.Rows)
                    {
                        try
                        {
                            notTicketNo = Convert.ToString(dr["Notice Number"]);
                            idNumber=Convert.ToString(dr["ID number"]);
                            _TelephoneNumber = Convert.ToString(dr["Telephone Number"]);
                            _SuccessStatus = Convert.ToString(dr["SuccessStatus"]);
                           
                        }
                        catch (Exception ex)
                        {
                            item.Status = QueueItemStatus.PostPone;
                            //2015-03-10 Heidi changed ServiceOption.BreakAndShutdown to ServiceOption.Continue (bontq1907)
                            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSFrom3rdPartyNoticeNumberError"), ex.Message,fileInfo.Name), LogType.Error, ServiceOption.Continue);
                            //break;
                            continue;//2015-03-10 Heidi changed break to continue (bontq1907)
                        }
                        try
                        {
                               //Deal with a blank line
                                if (notTicketNo == "" && idNumber == "" && _TelephoneNumber == "" && _SuccessStatus == "")
                                {
                                    continue;
                                }
                              
                                if (!string.IsNullOrEmpty(idNumber))
                                {
                                    if (idNumber.Contains("E+"))
                                    {
                                        //2015-03-10 Heidi added (bontq1907)
                                        item.Status = QueueItemStatus.PostPone;
                                        AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSFrom3rdPartyInvalidIDNumberContainsScientificNotation"), idNumber, fileInfo.Name), LogType.Error, ServiceOption.Continue);
                                        continue;
                                    }
                                }
                                else
                                {
                                    //2015-03-10 Heidi added (bontq1907)
                                    item.Status = QueueItemStatus.PostPone;
                                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSFrom3rdPartyInvalidIDNumber"), notTicketNo, fileInfo.Name), LogType.Error, ServiceOption.Continue);
                                    continue;
                                }

                                Notice notice = noticeService.GetByNotTicketNo(notTicketNo).FirstOrDefault();
                                if (notice != null)
                                {
                                    noticedb = new Stalberg.TMS.NoticeDB(connAARTODB);
                                    int validFlag = noticedb.CheckNoticeNumberAndIDNumber(notice.NotIntNo, idNumber, out errorMsg);
                                    
                                    if (validFlag <= 0)
                                    {
                                        if (validFlag == -1)
                                        {
                                            //2015-03-10 Heidi added (bontq1907)
                                            item.Status = QueueItemStatus.PostPone;
                                            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSFrom3rdPartyNoticeAndIDNumberDoesNotMatch"), notTicketNo,idNumber, fileInfo.Name), LogType.Error, ServiceOption.Continue);
                                            continue;
                                        }
                                        if (validFlag == 0)
                                        {
                                            //2015-03-10 Heidi added (bontq1907)
                                            item.Status = QueueItemStatus.PostPone;
                                            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSFrom3rdPartyNoticeAndIDNumberMatchExceptionError"), notTicketNo, idNumber, fileInfo.Name, errorMsg), LogType.Error, ServiceOption.Continue);
                                            continue;
                                        }
                                    }

                                    if (!notice.SmsFor2ndNotice.HasValue)
                                    {
                                        notice.SmsFor2ndNotice = DateTime.Now;
                                        notice.LastUser = AARTOBase.LastUser;
                                        noticeService.Save(notice);

                                        SIL.AARTO.DAL.Entities.TList<Charge> chargeList = chgService.GetByNotIntNo(notice.NotIntNo);

                                        var charge = chargeList.Where(chg => chg.ChgIsMain).OrderBy(chg => chg.ChgSequence).FirstOrDefault();
                                        if (charge != null)
                                        {
                                            try
                                            {
                                                //2015-03-10 Heidi added checked evidencePack exists (bontq1907)
                                                evidencePackList = evidencePackService.Find("ChgIntNo=" + charge.ChgIntNo + " AND EPItemDescr='" + ResourceHelper.GetResource("SuccessSMSSendNoticeAddEvidencePack") + "' ").ToList();
                                                if (evidencePackList == null || evidencePackList.Count == 0)
                                                {
                                                    DecisionManager.AddEvidencePack(ResourceHelper.GetResource("SuccessSMSSendNoticeAddEvidencePack"),
                                                            charge.ChgIntNo, "Notice", notice.NotIntNo, AARTOBase.LastUser, (int)EpActionTypeList.Others);
                                                }

                                            }
                                            catch (Exception ex)
                                            {
                                                item.Status = QueueItemStatus.PostPone;//2015-03-10 Heidi added (bontq1907)
                                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSFrom3rdPartyAddEvidencePackError"), ex.Message, notice.NotIntNo, fileInfo.Name), LogType.Error, ServiceOption.Continue);
                                                continue;//2015-03-10 Heidi added continue (bontq1907)
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //2015-03-10 Heidi added (bontq1907)
                                    item.Status = QueueItemStatus.PostPone;
                                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSFrom3rdPartyNotFoundNoticeByNotTicketNoError"), notTicketNo,fileInfo.Name), LogType.Error, ServiceOption.Continue);
                                    continue;
                                }
                                
                          
                        }
                        catch (Exception ex)
                        {

                            item.Status = QueueItemStatus.PostPone;
                            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSFrom3rdPartyUpdateNoticeError"),ex.Message, notTicketNo,fileInfo.Name), LogType.Error, ServiceOption.Continue);
                            continue;//2015-03-10 Heidi added continue (bontq1907)
                        }

                    }
                   

                }

           
            }
           
        }



   
     }
}
