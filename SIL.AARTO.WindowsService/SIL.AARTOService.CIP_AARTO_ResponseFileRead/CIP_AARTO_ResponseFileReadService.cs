﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using System.IO;
using Stalberg.TMS;
using SIL.AARTOService.Resource;
using System.Data;
using System.Transactions;
using System.Configuration;
using System.Data.SqlClient;
using Stalberg.TMS_TPExInt.Components;
using Stalberg.TMS.Data;
using System.Text.RegularExpressions;

namespace SIL.AARTOService.CIP_AARTO_ResponseFileRead
{
    public class CIP_AARTO_ResponseFileReadService : ServiceDataProcessViaFileSystem
    {
        private const string RESPONSE_FILE_PREFIX = "crpha";
        private const string INFRINGEMENT_FILE_PREFIX = "ccpha";
        private const int STATUS_EXPIRED = 921;
        private const int STATUS_ERRORS = 110;
        private const int STATUS_NO_AOG_CA = 500;
        private const int STATUS_RESPONSE_LOADED = 200;
        private const string FILE_EXT = ".txt";
        private const string NAME = "Aarto";

        FileInfo fileInfo;
        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        string connAARTODB, connCIP_AARTOUNC;

        public CIP_AARTO_ResponseFileReadService()
            : base("", "", new Guid("15C9C60F-9B45-4CF9-98C9-C743ECF251E3"))
        {
            this._serviceHelper = new AARTOServiceBase(this, false);
            this.Recursion = false;
            // 2012-02-21 jerry change
            this.connCIP_AARTOUNC = AARTOBase.GetConnectionString(ServiceConnectionNameList.CiprusAARTO_LoadFiles, ServiceConnectionTypeList.UNC);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);

            AARTOBase.OnServiceStarting = () =>
            {
                AARTOBase.CheckFolder(this.connCIP_AARTOUNC);
            };
        }

        public override void PrepareWork()
        {
            //this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            //this.connCIP_AARTOUNC = AARTOBase.GetConnectionString(ServiceConnectionNameList.CiprusAARTO_LoadFiles, ServiceConnectionTypeList.UNC);
            //Jerry 2012-04-17 change
            //connResponseFolder = Path.Combine(ServiceParameters["CiprusAartoExportFolder"], "ResponseFiles");
            this.DestinationPath = connCIP_AARTOUNC;

            this.GetDataFilesFromTP_AARTO(connCIP_AARTOUNC);
        }

        public override void InitialWork(ref QueueItem item)
        {
            if (item.Body != null && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    //Jerry 2012-04-20 change
                    //string file = fileInfo.FullName;
                    fileInfo = (FileInfo)item.Body;
                    string file = fileInfo.FullName;

                    if (Path.GetFileName(file).ToLower().StartsWith(RESPONSE_FILE_PREFIX) && Path.GetExtension(file).ToLower() == FILE_EXT)
                    {
                        item.IsSuccessful = true;
                        item.Status = QueueItemStatus.Discard;
                        //this.fileInfo = (FileInfo)item.Body;
                    }
                    else
                        AARTOBase.ErrorProcessing(ref item);
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                return;
            }
        }

        string printFilename;
        public override void MainWork(ref List<QueueItem> queueList)
        {
            // Changed by Tony Sun on 20100221
            string dateStr = DateTime.Now.ToString();
            dateStr = dateStr.Replace("/", "-");
            dateStr = dateStr.Replace(":", "-");
            printFilename = "NBS-" + dateStr;

            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                if (!item.IsSuccessful)
                    continue;

                if (item.IsSuccessful)
                {
                    try
                    {
                        bool failed;
                        FileInfo fileInfo = (FileInfo)item.Body;

                        // load the  latest response files
                        Logger.Info(ResourceHelper.GetResource("ResponseFile_StartingLoadFiles", NAME) + DateTime.Now.ToString());
                        failed = GetResponseDataFromTP(fileInfo);

                        if (failed)
                            Logger.Info(ResourceHelper.GetResource("ResponseFile_LoadFileFailed", NAME) + DateTime.Now.ToString());
                        else
                            Logger.Info(ResourceHelper.GetResource("ResponseFile_LoadFileCompleted", NAME) + DateTime.Now.ToString());

                    }
                    catch (Exception ex)
                    {
                        item.IsSuccessful = false;
                        item.Status = QueueItemStatus.UnKnown;
                        this.Logger.Error(string.Format(ResourceHelper.GetResource("ServiceHasError"), AARTOBase.LastUser, ex.Message));
                        return;
                    }

                }
            }
        }

        public override void FinaliseData(List<QueueItem> queueList) { }

        private ResponseProcessCodeList responseCodeList = new ResponseProcessCodeList();
        private bool GetResponseDataFromTP(FileInfo fileInfo)
        {
            bool failed = false;
            int countFailed = 0;
            int countOK = 0;
            int cubIntNo = 0;
            int countExpired = 0;
            string errMessage = "";
            DateTime fileDate = DateTime.Now;

            CameraUnitBatchDB batch = new CameraUnitBatchDB(connAARTODB);
            NoticeDB notice = new NoticeDB(connAARTODB);
            CameraUnitPanelDB panel = new CameraUnitPanelDB(connAARTODB);
            ChargeDB charge = new ChargeDB(connAARTODB);

            int autIntNo = 0;
            string autNo = "";
            string srLine = "";
            string file = fileInfo.FullName;
            string fileName = fileInfo.Name;


            //infringementFile = INFRINGEMENT_FILE_PREFIX + fileName.Substring(5, 9) + FILE_EXT;
            string infringementFile = INFRINGEMENT_FILE_PREFIX + fileName.Substring(5, 10) + FILE_EXT;
            string camUnitID = fileName.Substring(8, 3);


            // open the file and update the each database table separately
            FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs, System.Text.Encoding.Default);
            while (!sr.EndOfStream)
            {
                srLine = sr.ReadLine();

                if (srLine.Length > 2)
                {
                    string recordType = srLine.Substring(0, 2);

                    switch (recordType)
                    {
                        case "61":
                            // process the response records
                            failed = ProcessResponseRecord(file, srLine, ref countFailed, ref countOK, ref autNo, ref autIntNo, ref countExpired, printFilename);
                            break;

                        case "99":
                            //failed = true;
                            // 99=file traier record
                            // 99|C|01|000193
                            // -2n|interface-1a|version-2n|count-6n
                            break;
                    }
                }
            } // end of while sr

            sr.Close();
            fs.Close();

            //dls 071207 - need to get the batch details regardless of whether there are failures or not
            SqlDataReader reader = batch.CameraUnitBatchDetails(autNo, camUnitID, infringementFile);

            if (countFailed > 0)
            {
                failed = false;
                while (reader.Read() && !failed)
                {
                    if (reader["CUBFileName"].ToString().Equals(""))
                    {
                        failed = true;
                        Logger.Info(ResourceHelper.GetResource("ResponseFile_UnableFindBatch") + infringementFile);
                    }

                    if (countExpired > 0)
                    {
                        //dls 071220 - - new CiprusPI code = 53 Offence date too old (30-day rule)
                        int noOfRows = notice.UpdateExpiredNotices(autIntNo, camUnitID, AARTOBase.LastUser, infringementFile, STATUS_EXPIRED, ref errMessage, STATUS_ERRORS);

                        if (noOfRows < 0)
                        {
                            Logger.Info(ResourceHelper.GetResource("ResponseFile_UableUpdateExpiredNotice", NAME, infringementFile, errMessage));
                            failed = true;
                        }
                        else if (noOfRows > 0)
                        {
                            Logger.Info(ResourceHelper.GetResource("ResponseFile_NoticesExpired", noOfRows.ToString(), infringementFile));
                        }
                    }
                }

                reader.Dispose();
            }

            if (countOK > 0 || countFailed > 0)
            {
                cubIntNo = batch.UpdateCameraUnitBatch(autNo, camUnitID, infringementFile, AARTOBase.LastUser, fileDate, countFailed, countOK, ref errMessage);

                if (cubIntNo < 1)
                {
                    Logger.Warning(ResourceHelper.GetResource("ResponseFile_UnableUpdateBatch") + file);
                }
            }

            Logger.Info(ResourceHelper.GetResource("ResponseFile_ProcessedOK") + file);

            // move the file to the response files subfolder
            if (!Directory.Exists(connCIP_AARTOUNC + @"\ResponseFiles\Completed\" + autNo))
                Directory.CreateDirectory(connCIP_AARTOUNC + @"\ResponseFiles\Completed\" + autNo);

            // move the file to the response files error subfolder
            if (!Directory.Exists(connCIP_AARTOUNC + @"\ResponseFiles\Error\"))
                Directory.CreateDirectory(connCIP_AARTOUNC + @"\ResponseFiles\Error\");

            if (failed)
            {
                Logger.Info(ResourceHelper.GetResource("ResponseFile_ProcessedFailed") + file);
                this.FileToLocalFolder(file, connCIP_AARTOUNC + @"\ResponseFiles\Error\", "response", "move");
            }
            else
            {
                this.FileToLocalFolder(file, connCIP_AARTOUNC + @"\ResponseFiles\Completed\" + autNo + @"\", "response", "move");
            }

            return failed;
        }

        private bool ProcessResponseRecord(string file, string srLine, ref int countFailed, ref int countOK, ref string autNo, ref int autIntNo, ref int countExpired, string printFilename)
        {
            bool failed = false;

            int processingCode = 0;
            int severityCode = 0;

            int notIntNo = 0;
            string processDescr = "";

            CiprusAarto61Record rec61 = new CiprusAarto61Record();

            // read a row get the response and the tms key from the original row
            // processing code = 01-99 position 5 length 2
            // severity code 0 = OK; 2 = rejected in position 7 length 1

            // original record 66 starts in position 149 + 27 = 176 start of reference no length 15 notintno
            // 

            if (autIntNo == 0)
            {
                //for the first record, we need to find the authority
                autNo = Path.GetFileName(file).Substring(5, 3);

                // if the local authority is missing bail out immediately swearing
                TMSData tmsData = new TMSData(connAARTODB);
                autIntNo = tmsData.GetAutIntFromAuthorityFromAutNo(autNo);
                if (autIntNo <= 0)
                {
                    Logger.Error(ResourceHelper.GetResource("ResponseFile_ErrorAuthName", file, autNo));
                    failed = true;
                    return failed;
                }

                //// need to check whether this file is the correct one in the sequence to be loaded
                ////*******************************************************************************

                //check that this is the response file that we are expecting for this camera/autno
                CameraUnitBatchDB batch = new CameraUnitBatchDB(connAARTODB);

                string fileName = Path.GetFileName(file);
                //int batchNo = Convert.ToInt32(fileName.Substring(11, 3));
                int batchNo = Convert.ToInt32(fileName.Substring(11, 4));
                char pad0Char = Convert.ToChar("0");

                string camUnitID = fileName.Substring(8, 3);

                string infringementFile = INFRINGEMENT_FILE_PREFIX + autNo + camUnitID + batchNo.ToString().PadLeft(4, pad0Char) + FILE_EXT;

                SqlDataReader batchReader = batch.CameraUnitBatchCheckResponse(autNo, camUnitID, infringementFile);

                while (batchReader.Read())
                {
                    if (batchReader["CUBFileName"].ToString().Equals(""))
                    {
                        //there is no infringement file for this response file
                        Logger.Error(ResourceHelper.GetResource("ResponseFile_NoInfrigementFileCreated") + file);
                        failed = true;
                    }
                    else if (batchReader["CUBReasonFailed"].ToString().Equals("Cancelled"))
                    {
                        //there is no infringement file for this response file
                        Logger.Error(ResourceHelper.GetResource("ResponseFile_InfrigementFileCancelled") + file);
                        failed = true;
                    }
                    //else if (!batchReader["CUBResponseDate"].Equals(""))
                    //{
                    //     Logger.Info("ProcessResponseRecord: Response file " + file + " has already been loaded");
                    //     failed = true;
                    //}
                    //dls 091022 - they seem to be incapable of loading the files in the correct order, so don't bother to check!
                    else if (batchReader["CUBFileName"].ToString().Equals(infringementFile) && !batchReader["CUBResponseDate"].Equals(""))
                    {
                        Logger.Error(ResourceHelper.GetResource("ResponseFile_AlreadyLoaded", file));
                        failed = true;
                    }
                    break;
                }

                batchReader.Close();

                if (failed) return failed;
            }

            try
            {
                rec61.processCode = srLine.Substring(5, 2); // process code returned by ciprus indicates action required if any

                processingCode = int.Parse(rec61.processCode);

                if (processingCode == 53)
                    countExpired++;
            }
            catch
            {
                Logger.Error(ResourceHelper.GetResource("ResponseFile_ProcessCodeInvalid", file));
                failed = true;
                return failed;
            }

            try
            {
                rec61.severityCode = srLine.Substring(7, 1); // 0=processed OK; 2=record rejected

                severityCode = int.Parse(rec61.severityCode);
            }
            catch
            {
                Logger.Error(ResourceHelper.GetResource("ResponseFile_SeverityCodeInvalid", file));
                failed = true;
                return failed;
            }

            try
            {
                if (srLine.Length > 15)
                {
                    rec61.recType = srLine.Substring(0, 2);

                    //all records from up file are included here - need to allow for control records
                    if (rec61.recType.Equals("61"))
                    {
                        //mrs 20090206 the transaction data field is 50 length, we should add code to process the notintno out of whatever is there
                        notIntNo = Convert.ToInt32(srLine.Substring(164, 15)); // notIntNo is the unique reference sent to ciprus

                        if (severityCode == 0)
                        {
                            rec61.recordNumber = srLine.Substring(8, 6);
                            rec61.legislation = srLine.Substring(14, 1); //1= print aarto-03
                            rec61.noticeNoType = srLine.Substring(15, 1); //1=aarto notice no; 2=ciprus notice no
                            rec61.noticeNumber = srLine.Substring(16, 16);
                            rec61.issuingAuthority = srLine.Substring(32, 40).Trim();
                            rec61.officeNatisNo = srLine.Substring(72, 9).Trim(); //infrastructure no
                            rec61.officerName = srLine.Substring(81, 40).Trim();
                            rec61.officerInit = srLine.Substring(121, 5).Trim();
                            rec61.penaltyAmount = srLine.Substring(126, 8).Trim();
                            rec61.discountAmount = srLine.Substring(134, 8).Trim();
                            rec61.discountedAmount = srLine.Substring(142, 8).Trim();
                            rec61.chargeType = srLine.Substring(150, 10).Trim(); //minor or Offence
                            rec61.demeritPoints = srLine.Substring(160, 4).Trim();
                            rec61.tranData = srLine.Substring(164, 50).Trim();
                            //rec61.uniqueDocID = srLine.Substring(208, 12);
                            //mrs 20080205 1.07 changed length of trandata
                            rec61.uniqueDocID = srLine.Substring(214, 12);

                            //rec61.infringementRecord = srLine.Substring(221, 900);
                            //rec61.infringementRecord = srLine.Substring(226, 939);          //dls 090324 - field length should only be 973
                            rec61.infringementRecord = srLine.Substring(226, 973);

                            if (rec61.noticeNoType.Equals("1"))
                            {
                                rec61.noticeNumber = string.Format("{0}-{1}-{2}-{3}",
                                    rec61.noticeNumber.Substring(0, 2), rec61.noticeNumber.Substring(2, 4),
                                    rec61.noticeNumber.Substring(6, 9), rec61.noticeNumber.Substring(15, 1));
                            }
                            else if (rec61.noticeNoType.Equals("2"))
                            {
                                rec61.noticeNumber = string.Format("{0}/{1}/{2}/{3}",
                                    rec61.noticeNumber.Substring(0, 2), rec61.noticeNumber.Substring(2, 5),
                                    rec61.noticeNumber.Substring(7, 3), rec61.noticeNumber.Substring(10, 6));
                            }

                            rec61.ciprusLoadDate = string.Empty;

                            if (srLine.Length > 1198)
                            {
                                //dls 090324 - add date that notice was successfully loaded into Ciprus system
                                try
                                {
                                    rec61.filler1 = srLine.Substring(1199, 146);
                                    //Changed by Tod Zhang on the 090701
                                    rec61.adjOfficerSurname = srLine.Substring(1345, 40).Trim();
                                    rec61.adjOfficerInitials = srLine.Substring(1385, 5).Trim();
                                    rec61.adjOfficerInfrastructureNo = srLine.Substring(1390, 9).Trim();
                                    rec61.ciprusLoadDate = string.Format("{0}-{1}-{2}",
                                            srLine.Substring(1399, 4), srLine.Substring(1403, 2), srLine.Substring(1405, 2));

                                }
                                catch (Exception e)
                                {
                                    Logger.Error(ResourceHelper.GetResource("ResponseFile_UnableDetermineLoadDate") + e.Message);
                                }
                            }
                        }
                        else
                        {
                            //the notice has been rejected
                            rec61.recordNumber = srLine.Substring(8, 6);
                            rec61.legislation = string.Empty;
                            rec61.noticeNoType = string.Empty;
                            rec61.noticeNumber = string.Empty;
                            rec61.issuingAuthority = string.Empty;
                            rec61.officeNatisNo = string.Empty;
                            rec61.officerName = string.Empty;
                            rec61.officerInit = string.Empty;
                            rec61.penaltyAmount = "0";
                            rec61.discountAmount = "0";
                            rec61.discountedAmount = "0";
                            rec61.chargeType = string.Empty;
                            rec61.demeritPoints = "0";
                            //mrs 20090205 added
                            rec61.uniqueDocID = string.Empty;

                            rec61.tranData = srLine.Substring(164, 50).Trim();
                            //mrs 20090205 1.07 length change
                            //rec61.infringementRecord = srLine.Substring(208, 900);
                            //rec61.infringementRecord = srLine.Substring(226, 939); //dls 090324 - field length should only be 973
                            rec61.infringementRecord = srLine.Substring(226, 973);

                            //Changed by Tod Zhang on the 090701
                            rec61.adjOfficerSurname = string.Empty;
                            rec61.adjOfficerInitials = string.Empty;
                            rec61.adjOfficerInfrastructureNo = string.Empty;

                            rec61.ciprusLoadDate = string.Empty;

                            //dls 090324 - no date to upload if record wasn't successful
                            ////dls 090324 - add date that notice was successfully loaded into Ciprus system
                            //if (srLine.Length > 1198)
                            {
                                //rec61.filler1 = srLine.Substring(1199, 200);
                                //rec61.ciprusLoadDate = string.Format("{0}-{1}-{2}",
                                //        srLine.Substring(1399, 4), srLine.Substring(1403, 2), srLine.Substring(1405, 2));
                            }
                        }
                    }
                    else
                        notIntNo = 0;
                }
                else
                    notIntNo = 0;
            }
            catch (Exception ex)
            {
                Logger.Error(ResourceHelper.GetResource("ResponseFile_ProcessError", file, ex.Message));
                failed = true;
                return failed;
            }

            if (notIntNo > 0)
            {
                // update the database
                if (severityCode > 1) // ciprus found an error
                    countFailed++;
                else if (notIntNo > 0)      // Don't want to add the 'File balances OK count!
                    countOK++;

                // get the description
                processDescr = responseCodeList.GetDescrByCode(processingCode); // crashed here null reference on 14 list is null

                // update frame
                string errMessage = string.Empty;

                //mrs 20080205 need to add the uniqueDocumentID to the notice table here
                NoticeDB notice = new NoticeDB(connAARTODB);
                int updNotIntNo = notice.UpdateCiprusAartoResponse(printFilename, rec61, processDescr, AARTOBase.LastUser, notIntNo, STATUS_ERRORS, STATUS_RESPONSE_LOADED, STATUS_NO_AOG_CA, ref errMessage);
                if (updNotIntNo <= 0)
                {
                    failed = true;

                    switch (updNotIntNo)
                    {
                        case -1:
                            errMessage = ResourceHelper.GetResource("Result_NoticeNotExist");
                            break;

                        case -2:
                            errMessage = ResourceHelper.GetResource("Result_UnableUpdateNotice");
                            break;

                        case -3:
                            errMessage = ResourceHelper.GetResource("Result_UnableUpdateCharge");
                            break;

                        case -4:
                            errMessage = ResourceHelper.GetResource("Result_ErrorChargeStatus");
                            break;
                    }

                    Logger.Error(ResourceHelper.GetResource("ResponseFile_FaildedUpdateNotice", file, notIntNo, errMessage));
                }
                //Oscar 20120322 remove , we don't need push print servie q in here.
                //else
                //{
                //    QueueItemProcessor queueProcessor = new QueueItemProcessor();
                //    QueueItem pushItem = new QueueItem();
                //    pushItem = new QueueItem();
                //    pushItem.Body = updNotIntNo.ToString();
                //    pushItem.Group = autNo;
                //    pushItem.QueueType = ServiceQueueTypeList.Print1stNotice;
                //    queueProcessor.Send(pushItem);
                //}
            }

            return failed;
        }
    }
}
