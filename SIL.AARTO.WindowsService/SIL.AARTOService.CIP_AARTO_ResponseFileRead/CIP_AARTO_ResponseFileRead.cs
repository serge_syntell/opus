﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CIP_AARTO_ResponseFileRead
{
    partial class CIP_AARTO_ResponseFileRead : ServiceHost
    {
        public CIP_AARTO_ResponseFileRead()
            : base("", new Guid("6CDA6CFF-3750-4C57-AE39-043E93BDB045"), new CIP_AARTO_ResponseFileReadService())
        {
            InitializeComponent();
        }
    }
}
