﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.GenerateCOOFromCityApproval
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.GenerateCOOFromCityApproval"
                ,
                DisplayName = "SIL.AARTOService.GenerateCOOFromCityApproval"
                ,
                Description = "SIL.AARTOService.GenerateCOOFromCityApproval"
            };

            ProgramRun.InitializeService(new GenerateCOOFromCityApproval(), serviceDescriptor, args);
        }
    }
}
