﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using System.Configuration;
using SIL.QueueLibrary;
using SIL.AARTOService.Library;
using System.Data;
using System.Transactions;
using System.Diagnostics;
using Stalberg.TMS;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Collections;
using SIL.AARTOService.Resource;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;


namespace SIL.AARTOService.GenerateCOOFromCityApproval
{
    public class GenerateCOOFromCityApprovalService : ServiceDataProcessViaQueue
    {
        readonly string connectStr;
        CooForCityApprovalService cfcaService = new CooForCityApprovalService();

        public GenerateCOOFromCityApprovalService()
            : base("", "", new Guid("8F3DF174-3535-41A2-AD15-4E4DCBF2F267"), ServiceQueueTypeList.COOForApproval)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            this.connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);

            AARTOBase.OnServiceStarting = () =>
            {
                this.groupStr = null;
            };
        }

        AARTOServiceBase AARTOBase
        {
            get { return (AARTOServiceBase)_serviceHelper; }
        }
        AARTORules rules = AARTORules.GetSingleTon();

        string groupStr;
        int AutIntNo;
        int noOfDaysForPayment = 0;
        int repAddrExpiryPeriod = 0;
        string dataWashingActived = null;
        string useRepAddress = null;

        private const int CODE_REP_LOGGED_CHARGE = 410;
        private const int REPCODE_CHANGEOFFENDER = 9;

        // 2015-01-20, Oscar added (bontq 1797, ref 1796)
        readonly NoticeService noticeService = new NoticeService();

        public override void InitialWork(ref QueueItem item) 
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString().Trim();
            if (!string.IsNullOrWhiteSpace(body) && !string.IsNullOrWhiteSpace(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;

                    int CFCAIntNo;
                    if (!int.TryParse(body, out CFCAIntNo))
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        AARTOBase.DeQueueInvalidItem(ref item);
                        return;
                    }

                    string groupStr = item.Group.Trim();

                    if (groupStr != this.groupStr)
                    {
                        if (this.groupStr != null)
                        {
                            AARTOBase.SkipReceivingQueueData(ref item);
                            this.groupStr = null;
                            return;
                        }

                        this.groupStr = groupStr;
                        QueueGroupValidation(ref item);
                    }

                    QueueItemValidation(ref item);
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                    AARTOBase.DeQueueInvalidItem(ref item);
                    return;
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                AARTOBase.DeQueueInvalidItem(ref item);
                return;
            }
        }

        private void QueueGroupValidation(ref QueueItem item)
        {
            AuthorityDetails authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim().Equals(this.groupStr, StringComparison.OrdinalIgnoreCase));
            if (authDetails == null)
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.groupStr));
                AARTOBase.DeQueueInvalidItem(ref item);
                return;
            }
            this.AutIntNo = authDetails.AutIntNo;

            AuthorityRulesDetails arule = new AuthorityRulesDetails();
            arule.AutIntNo = this.AutIntNo;
            arule.ARCode = "4660";
            arule.LastUser = AARTOBase.LastUser;

            DefaultAuthRules defaultRule = new DefaultAuthRules(arule, this.connectStr);
            KeyValuePair<int, string> key = defaultRule.SetDefaultAuthRule();
            noOfDaysForPayment = key.Key;

            arule = new AuthorityRulesDetails();
            arule.ARCode = "9060";
            arule.AutIntNo = this.AutIntNo;
            arule.LastUser = AARTOBase.LastUser;
            defaultRule = new DefaultAuthRules(arule, this.connectStr);
            key = defaultRule.SetDefaultAuthRule();
            dataWashingActived = key.Value.ToUpper().Trim();

            arule = new AuthorityRulesDetails();
            arule.ARCode = "9120";
            arule.AutIntNo = this.AutIntNo;
            arule.LastUser = AARTOBase.LastUser;
            defaultRule = new DefaultAuthRules(arule, this.connectStr);
            key = defaultRule.SetDefaultAuthRule();
            useRepAddress = key.Value.ToUpper().Trim();

            arule = new AuthorityRulesDetails();
            arule.ARCode = "9130";
            arule.AutIntNo = this.AutIntNo;
            arule.LastUser = AARTOBase.LastUser;
            defaultRule = new DefaultAuthRules(arule, this.connectStr);
            key = defaultRule.SetDefaultAuthRule();
            repAddrExpiryPeriod = key.Key;
        }

        private void QueueItemValidation(ref QueueItem item)
        {
            CooForCityApproval approval = new CooForCityApprovalService().GetByCfcaIntNo(int.Parse(item.Body.ToString()));
            if (approval == null)
            {
                AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                return;
            }

            if (!approval.ActionCode.HasValue || approval.ActionCode.Value != (int)CooActionCodeList.AcceptedByOfficer)
            {
                AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                return;
            }

            if (approval.DateCooAutomationProcessed.HasValue)
            {
                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("COODuplicateProcessed"), approval.CfcaIntNo));
                return;
            }

            int NotIntNo = approval.NotIntNo;
            //Notice notice = new NoticeService().GetByNotIntNo(NotIntNo);
            Notice notice = this.noticeService.GetByNotIntNo(NotIntNo);
            if (notice == null)
            {
                AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                return;
            }

            // 2014-03-14, Oscar added, Coo only allows NotFilmType != 'M'
            if (notice.NotFilmType.Equals2("M"))
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("COOInvalidNotFilmType", NotIntNo), LogType.Warning, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                AARTOBase.DeQueueInvalidItem(ref item);
                return;
            }

            if (notice.NoticeStatus >= 600 || notice.NoticeStatus < 255 || notice.NoticeStatus == 410 || notice.NoticeStatus == 420 || notice.NoticeStatus == 450)
            {
                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("COOInvalidNoticeStatus"), NotIntNo));
                AARTOBase.DeQueueInvalidItem(ref item);
                return;
            }

            if (notice.NotSendTo != "P")
            {
                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("COOInvalidNoticeSendTo"), NotIntNo));
                AARTOBase.DeQueueInvalidItem(ref item);
                return;
            }
        }

        public override void MainWork(ref List<SIL.QueueLibrary.QueueItem> queueList)
        {
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                if (!item.IsSuccessful) continue;

                try
                {
                    CooForCityApproval approval = new CooForCityApprovalService().GetByCfcaIntNo(int.Parse(item.Body.ToString()));

                    int NotIntNo = approval.NotIntNo;

                    SIL.AARTO.DAL.Entities.TList<Charge> chgList = new AARTO.DAL.Entities.TList<Charge>();
                    chgList.AddRange(new ChargeService().GetByNotIntNo(NotIntNo).Where(c => c.ChargeStatus < (int)ChargeStatusList.Completion && c.ChgIsMain).OrderBy(c => c.ChgSequence).ToList());

                    if (chgList.Count > 0)
                    {
                        int ChgIntNo = chgList[0].ChgIntNo;
                        decimal OriginalAmount = decimal.Parse(chgList[0].ChgFineAmount.ToString());
                        int RepIntNo = 0;
                        string errMsg = null;

                        using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                        {
                            Stalberg.TMS.RepresentationDB represent = new Stalberg.TMS.RepresentationDB(this.connectStr);

                            string address = approval.PhysicalAddress1.Trim() + " ";
                            if(!string.IsNullOrWhiteSpace(approval.PhysicalAddress2))
                            {
                                address += approval.PhysicalAddress2.Trim() + " ";
                            }
                            if (!string.IsNullOrWhiteSpace(approval.PhysicalAddress3))
                            {
                                address += approval.PhysicalAddress3.Trim() + " ";
                            }
                            if (!string.IsNullOrWhiteSpace(approval.PhysicalAddress4))
                            {
                                address += approval.PhysicalAddress4.Trim() + " ";
                            }
                            if (!string.IsNullOrWhiteSpace(approval.PhysicalCode))
                            {
                                address += approval.PhysicalCode.Trim();
                            }

                            //RepIntNo = represent.NewUpdateRepresentation(RepIntNo, ChgIntNo, "", DateTime.Now, approval.DrvForenames.Trim(), address, "", "", AARTOBase.LastUser, this.AutIntNo, CODE_REP_LOGGED_CHARGE, OriginalAmount, "Y", "A", ref errMsg, false);

                            // 2014-01-26, Oscar changed
                            //RepIntNo = represent.NewRepresentationForCooOrPod(ChgIntNo, "", DateTime.Now, approval.DrvForenames.Trim(), address, "", "", AARTOBase.LastUser, OriginalAmount, "Y", "A", false);

                            //if (RepIntNo == -1)
                            //{
                            //    AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("COOSameRepresentationCode"), NotIntNo));
                            //}
                            //else if (RepIntNo == -2 || RepIntNo == -7)
                            //{
                            //    AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("COOErrorAddRepresentation"),errMsg,  NotIntNo));
                            //}
                            //else if (RepIntNo == -11)
                            //{
                            //    AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("COOCorrectedRepresentation"), NotIntNo));
                            //}

                            //if (RepIntNo > 0)
                            //{
                                ChargeDB chgDB = new ChargeDB(this.connectStr);
                                ChargeDetails chgDetail = chgDB.GetChargeDetails(ChgIntNo);

                                DriverDetails driver = new DriverDetails();
                                driver.LastUser = AARTOBase.LastUser;
                                driver.DrvSurname = approval.DrvSurname.Trim();
                                driver.DrvForeNames = approval.DrvForenames.Trim();
                                driver.DrvInitials = approval.DrvInitials.Trim();
                                driver.DrvAge = null;
                                driver.DrvIDNumber = approval.DrvIdNumber.Trim();
                                driver.DrvIDType = "02";
                                driver.DrvNationality = null;
                                driver.DrvPOAdd1 = approval.PostalAddress1.Trim();
                                driver.DrvPOAdd2 = approval.PostalAddress2.Trim();
                                driver.DrvPOAdd3 = approval.PostalAddress3.Trim();
                                driver.DrvPOAdd4 = approval.PostalAddress4.Trim();
                                driver.DrvPOAdd5 = approval.PostalAddress5.Trim();
                                driver.DrvPOCode = approval.PostalCode.Trim();
                                driver.DrvStAdd1 = approval.PhysicalAddress1.Trim();
                                driver.DrvStAdd2 = approval.PhysicalAddress2.Trim();
                                driver.DrvStAdd3 = approval.PhysicalAddress3.Trim();
                                driver.DrvStAdd4 = approval.PhysicalAddress4.Trim();
                                driver.DrvStCode = approval.PhysicalCode.Trim();
                                driver.DrvIntNo = 0;
                                driver.DrvEmail = approval.EmailAddress.Trim();

                                //List<int> result = new List<int>();
                                //result = represent.DecideRepresentation(RepIntNo, REPCODE_CHANGEOFFENDER, approval.OfficerName, "Change Offender",
                                //    DateTime.Now, AARTOBase.LastUser, ChgIntNo, decimal.Parse(chgDetail.ChgFineAmount.ToString()), 410, chgDetail.RowVersion, decimal.Parse(chgDetail.ChgFineAmount.ToString()),
                                //    "Y", "Change Offender", "D", false, "N",
                                //    NotIntNo, "N", approval.RegNo.Trim(), 0, 0, "", driver,
                                //    false, 0, true, "Represent",
                                //    false, ref errMsg, this.noOfDaysForPayment, dataWashingActived, useRepAddress, repAddrExpiryPeriod);

                                // 2014-02-16, Oscar added
                                var result = represent.DecideRepresentationForCoo(out errMsg, REPCODE_CHANGEOFFENDER, approval.OfficerName, "Change Offender", DateTime.Now, approval.DrvForenames.Trim(), address, "", "A", AARTOBase.LastUser, ChgIntNo, chgDetail.ChargeStatus, chgDetail.RowVersion, "Y", "Change Offender", "D", false, "N", NotIntNo, approval.RegNo.Trim(), "N", 0, 0, "", "Represent", this.noOfDaysForPayment, dataWashingActived, useRepAddress, repAddrExpiryPeriod, false, driver);

                                if (result == null || result.Count <= 0 || result[0] <= 0)
                                {
                                    AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("COODecidedRepresentation"), errMsg, NotIntNo));
                                    return;
                                }

                                approval.DateCooAutomationProcessed = DateTime.Now;
                                approval.LastUser = AARTOBase.LastUser;
                                approval = new CooForCityApprovalService().Save(approval);

                                QueueItemProcessor queProcessor = new QueueItemProcessor();
                                item = new QueueItem();
                                item.Body = NotIntNo.ToString();
                                item.QueueType = ServiceQueueTypeList.SearchNameIDUpdate;
                                queProcessor.Send(item);

                                // 2015-01-20, Oscar added (bontq 1797, ref 1796)
                                Notice notice;
                                if ((notice = this.noticeService.GetByNotIntNo(NotIntNo)) != null
                                    && !string.IsNullOrWhiteSpace(notice.NewOffenderPrintFileName))
                                {
                                    notice.NewOffenderPrintFileName = null;
                                    notice.LastUser = AARTOBase.LastUser;
                                    this.noticeService.Save(notice);
                                }

                                // 2014-04-14, Oscar added
                                NoticeDB.SetPendingNewOffender(NotIntNo, AARTOBase.LastUser);

                                item = new QueueItem();
                                item.Body = NotIntNo.ToString();
                                item.QueueType = ServiceQueueTypeList.ProcessNewOffenderNotices;
                                item.Group = this.groupStr;
                                queProcessor.Send(item);

                                scope.Complete();
                            //}
                        }
                    }
                    else
                    {
                        AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("COONotFindCharge"), NotIntNo));
                    }
                }
                catch(Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

    }
}
