﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.GenerateCOOFromCityApproval
{
    public partial class GenerateCOOFromCityApproval : ServiceHost
    {
        public GenerateCOOFromCityApproval()
            : base("", new Guid("2A57A3BB-E123-4636-8682-03A11EC3FB27"), new GenerateCOOFromCityApprovalService())
        {
            InitializeComponent();
        }
    }
}
