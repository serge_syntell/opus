﻿using System;
using System.Collections.Generic;
using System.Linq;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.DMS.DAL.Entities;
using SIL.DMS.DAL.Services;
using SIL.QueueLibrary;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;

namespace SIL.AARTOService.ReprocessNatisErrorsToDMS
{
    class ReprocessNatisErrorsToDMSService : ServiceDataProcessViaQueue
    {
        #region variables

        const int Status_DMSDocumentCorrection = (int)ChargeStatusList.DMSDocumentCorrection;
        const int Status_NoNatisDataS341HW = (int)FrameStatusList.NoNatisDataS341HW;

        readonly BatchDocumentService bdService = new BatchDocumentService();
        readonly DocumentErrorMessageService demService = new DocumentErrorMessageService();
        readonly FilmService filmService = new FilmService();
        readonly FrameService frameService = new FrameService();
        readonly string lastUser;
        readonly NoticeFrameService noticeFrameService = new NoticeFrameService();
        readonly NoticeService noticeService = new NoticeService();

        string constructorError;
        Frame frame;
        int frameIntNo;
        QueueItem item;
        Notice notice;

        #endregion

        public ReprocessNatisErrorsToDMSService()
            : base("", "", new Guid("9CE62DE3-A4C3-4CA4-9FD8-E82861D41F07"), ServiceQueueTypeList.ReprocessNatisErrorsToDMS)
        {
            try
            {
                _serviceHelper = new AARTOServiceBase(this);
                AARTOBase.OnServiceStarting = () =>
                {
                    if (!string.IsNullOrWhiteSpace(this.constructorError))
                    {
                        Logger.Error(this.constructorError);
                        this.constructorError = null;
                        AARTOBase.StopService();
                    }
                };

                this.lastUser = AARTOBase.LastUser;
                ServiceUtility.InitializeNetTier(AARTOBase.GetConnectionString(ServiceConnectionNameList.DMS, ServiceConnectionTypeList.DB), "SIL.DMS");
            }
            catch (Exception ex)
            {
                this.constructorError = ex.ToString();
            }
        }

        AARTOServiceBase AARTOBase
        {
            get { return (AARTOServiceBase)_serviceHelper; }
        }

        #region Initial & main work

        public override void InitialWork(ref QueueItem item)
        {
            item.IsSuccessful = true;
            this.item = item;

            try
            {
                if (!int.TryParse(Convert.ToString(item.Body), out this.frameIntNo)
                    || this.frameIntNo <= 0)
                {
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("InvalidQueueBody", item.Body),
                        LogType.Error, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                    return;
                }

                QueueItemValidation();
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex, LogType.Error, ServiceOption.Break, item);
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            foreach (var item in queueList.Where(q => q.IsSuccessful))
            {
                this.item = item;

                try
                {
                    UpdateDMS();
                }
                catch (Exception ex)
                {
                    AARTOBase.LogProcessing(ex, LogType.Error, ServiceOption.Break, item);
                }
            }
        }

        #endregion

        Frame GetFrame(int frameIntNo)
        {
            return this.frameService.GetByFrameIntNo(frameIntNo);
        }

        Film GetFilm(int filmIntNo)
        {
            return this.filmService.GetByFilmIntNo(filmIntNo);
        }

        Notice GetNotice(int frameIntNo)
        {
            AARTO.DAL.Entities.TList<NoticeFrame> noticeFrame;
            Notice notice;
            return (noticeFrame = this.noticeFrameService.GetByFrameIntNo(frameIntNo)).IsNullOrEmpty()
                || (notice = this.noticeService.GetByNotIntNo(noticeFrame[0].NotIntNo)) == null
                ? null : notice;
        }

        bool QueueItemValidation()
        {
            this.frame = GetFrame(this.frameIntNo);
            if (this.frame == null
                || !this.frame.FrameStatus.Equals2(Status_NoNatisDataS341HW))
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("FrameStatusIsInvalidItShouldBeInsteadOf", Status_NoNatisDataS341HW, this.frame != null ? this.frame.FrameStatus : 0),
                    LogType.Error, ServiceOption.ContinueButQueueFail, this.item, QueueItemStatus.Discard);
                return false;
            }

            this.notice = GetNotice(this.frameIntNo);
            if (this.notice == null
                || !this.notice.NoticeStatus.Equals2(Status_DMSDocumentCorrection))
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("NoticeStatusIsInvalidItShouldBeInsteadOf", Status_DMSDocumentCorrection, this.notice != null ? this.notice.NoticeStatus : 0),
                    LogType.Error, ServiceOption.ContinueButQueueFail, this.item, QueueItemStatus.Discard);
                return false;
            }

            //if (!this.frame.FrameNo.Equals2(this.notice.NotFrameNo))
            //{
            //    AARTOBase.LogProcessing(ResourceHelper.GetResource("FrameNoAreNotEqual", this.frame.FrameNo, this.notice.NotFrameNo),
            //        LogType.Error, ServiceOption.ContinueButQueueFail, this.item, QueueItemStatus.Discard);
            //    return false;
            //}

            //var film = GetFilm(this.frame.FilmIntNo);
            //if (film == null
            //    || !film.FilmNo.Equals2(this.notice.NotFilmNo))
            //{
            //    AARTOBase.LogProcessing(ResourceHelper.GetResource("FilmNoAreNotEqual", film != null ? film.FilmNo : "", this.notice.NotFilmNo),
            //        LogType.Error, ServiceOption.ContinueButQueueFail, this.item, QueueItemStatus.Discard);
            //    return false;
            //}

            return true;
        }

        string GetNatisErrorMessage()
        {
            var field = this.frame.FrameNaTisErrorFieldList.Trim2() ?? "";
            var indicator = this.frame.FrameNatisIndicator.Trim2() ?? "";

            if (!string.IsNullOrEmpty(indicator) && !indicator.Equals2("Y"))
            {
                if (field.Equals2("003"))
                    field += ResourceHelper.GetResource("NatisErrorMsg003");
                else if (field.Equals2("060"))
                    field += ResourceHelper.GetResource("NatisErrorMsg060");
                else if (indicator.Equals2("!"))
                    indicator = " ";
                else
                {
                    indicator = " ";
                    field += ResourceHelper.GetResource("NatisErrorMsgUnknown");
                }
            }
            return ResourceHelper.GetResource("NatisErrorMsg", indicator, field);
        }

        bool UpdateDMS()
        {
            var batchId = this.notice.NotFilmNo.Trim2();
            var baDoId = this.notice.NotFrameNo.Trim2();

            var bdEntity = this.bdService.GetByBaDoId(baDoId);
            if (bdEntity == null)
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("UnableToFindBatchDocument", baDoId),
                    LogType.Error, ServiceOption.Break, this.item);
                return false;
            }

            this.demService.Save(new DocumentErrorMessage
            {
                BatchId = batchId,
                BaDoId = baDoId,
                DoErMeCode = "HW Correction reason",
                DoErMessage = GetNatisErrorMessage(),
                DoErMeDateCreated = DateTime.Now,
                LastUser = this.lastUser
            });

            bdEntity.BaDoStId = (int)BatchDocumentStatusList.DocumentReturnedToQC;
            bdEntity.IsBackFromAarto = true;
            bdEntity.LastUser = this.lastUser;
            this.bdService.Save(bdEntity);

            AARTOBase.LogProcessing(ResourceHelper.GetResource("BatchDocumentHasBeenUpdatedToReturnToQC", baDoId),
                LogType.Info, ServiceOption.Continue, this.item, QueueItemStatus.Discard);

            return true;
        }
    }
}
