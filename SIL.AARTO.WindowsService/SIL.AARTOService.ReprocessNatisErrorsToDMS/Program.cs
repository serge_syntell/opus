﻿using SIL.ServiceLibrary;

namespace SIL.AARTOService.ReprocessNatisErrorsToDMS
{
    static class Program
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ProgramRun.InitializeService(new ReprocessNatisErrorsToDMS(),
                new ServiceDescriptor
                {
                    ServiceName = "SIL.AARTOService.ReprocessNatisErrorsToDMS",
                    DisplayName = "SIL.AARTOService.ReprocessNatisErrorsToDMS",
                    Description = "SIL AARTOService ReprocessNatisErrorsToDMS"
                }, args);
        }
    }
}
