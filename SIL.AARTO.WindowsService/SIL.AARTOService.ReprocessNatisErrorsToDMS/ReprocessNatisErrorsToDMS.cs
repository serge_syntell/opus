﻿using System;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.ReprocessNatisErrorsToDMS
{
    partial class ReprocessNatisErrorsToDMS : ServiceHost
    {
        public ReprocessNatisErrorsToDMS()
            : base("", new Guid("3E447AA5-B553-41BD-8604-D3C06B16E7D1"), new ReprocessNatisErrorsToDMSService())
        {
            InitializeComponent();
        }
    }
}
