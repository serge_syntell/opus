﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;
using SIL.AARTOService.Resource;
using Stalberg.TMS;
using NoticeEntity = SIL.AARTO.DAL.Entities.Notice;
using ChargeEntity = SIL.AARTO.DAL.Entities.Charge;
using RepresentationEntity = SIL.AARTO.DAL.Entities.Representation;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTOService.RepresentationGracePeriod_Notice
{
    public class RepresentationGracePeriod_NoticeService : ServiceDataProcessViaQueue
    {
        public RepresentationGracePeriod_NoticeService()
            : base("", "", new Guid("AA14D06C-B0B3-4B6A-8D67-C6D49855C737"), ServiceQueueTypeList.RepresentationGracePeriod_Notice)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
        }

        private const string TICKET_PROCESSOR_TMS = "TMS";

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        AuthorityDetails authDetails = null;
        string strAutCode = string.Empty;
        int autIntNo;
        int noOfDaysGracePeriod;
        bool cancelLoggedReps;
        string connectStr = string.Empty;

        RepresentationService repService = new RepresentationService();
        ChargeService chgService = new ChargeService();
        NoticeService notService = new NoticeService();

        public sealed override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && ServiceUtility.IsNumeric(body))
            {
                item.IsSuccessful = true;
            }
            else
            {
                // jerry 2012-02-21 change
                //item.IsSuccessful = false;
                //item.Status = QueueItemStatus.UnKnown;
                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.lastUser, item.Body));
                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.LastUser, item.Body), true);
            }

            if (strAutCode != item.Group)
            {
                strAutCode = item.Group.Trim();
                //jerry 2012-04-05 check group value
                if (string.IsNullOrWhiteSpace(strAutCode))
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
                    return;
                }

                this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode);
                autIntNo = authDetails.AutIntNo;

                //Jerry 2012-12-21 change
                // 2013-07-25 modify by Henry
                NoticeEntity noticeEntity = GetNoticeEntity(Convert.ToInt32(item.Body));
                //if (this.authDetails.AutTicketProcessor != TICKET_PROCESSOR_TMS)
                if (noticeEntity != null && noticeEntity.NotTicketProcessor != TICKET_PROCESSOR_TMS)
                {
                    // Jake 2013-07-12 added message when discard queue
                    item.Status = QueueItemStatus.Discard;
                    item.IsSuccessful = false;
                    this.Logger.Info("Invalid notice ticker processor:" + (noticeEntity == null ? "" : noticeEntity.NotTicketProcessor));
                }

                rules.InitParameter(connectStr, strAutCode);
            }
        }

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            int repIntNo, success;
            string msg = string.Empty;
            NoticeEntity noticeEntity;
            // jerry 2012-02-21 change
            //foreach (QueueItem item in queueList)
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                try
                {
                    if (item.IsSuccessful)
                    {
                        repIntNo = Convert.ToInt32(item.Body);

                        //get authority rules
                        noOfDaysGracePeriod = rules.Rule("4100").ARNumeric;
                        cancelLoggedReps = rules.Rule("4110").ARString.Equals("Y") ? true : false;
                        if (!cancelLoggedReps)
                        {
                            item.Status = QueueItemStatus.Discard;
                            noticeEntity = GetNoticeEntity(repIntNo);
                            this.Logger.Info(string.Format(ResourceHelper.GetResource("InfoInRepresentationGracePeriod"), repIntNo, noticeEntity.NotTicketNo, authDetails.AutName));
                            return;
                        }

                        success = LoggedRepsRunOutOfGrace(repIntNo, ref msg);
                        if (success < 0)
                        {
                            //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorOfRepGrace"), msg));
                            //item.Status = QueueItemStatus.UnKnown;
                            AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorOfRepGrace"), msg));
                        }
                        else
                        {
                            noticeEntity = GetNoticeEntity(repIntNo);
                            this.Logger.Info(string.Format(ResourceHelper.GetResource("SuccessfulInRepresentationGracePeriod"), repIntNo, noticeEntity.NotTicketNo, authDetails.AutName));
                            item.Status = QueueItemStatus.Discard;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //item.Status = QueueItemStatus.UnKnown;
                    //this.Logger.Error(ex);
                    //throw ex;
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        private int LoggedRepsRunOutOfGrace(int repIntNo, ref string errMsg)
        {
            DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));

            SqlParameter[] paras = new SqlParameter[4];
            paras[0] = new SqlParameter("@RepIntNo", repIntNo);
            //paras[1] = new SqlParameter("@AutIntNo", autIntNo);
            paras[1] = new SqlParameter("@NoOfDaysGracePeriod", noOfDaysGracePeriod);
            paras[2] = new SqlParameter("@CancelLoggedReps", cancelLoggedReps);
            paras[3] = new SqlParameter("@LastUser", AARTOBase.LastUser);

            object obj = db.ExecuteScalar("RepresentationsLoggedOutOfGrace_WS", paras, out errMsg);
            int result;
            if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(errMsg))
                result = Convert.ToInt32(obj);
            else
                result = -6;
            switch (result)
            {
                case 0:
                    errMsg = ResourceHelper.GetResource("ErrorNoRowsOfRepGrace");
                    break;
                case -1:
                    errMsg = ResourceHelper.GetResource("ErrorUpdateRepOfRepGrace");
                    break;
                case -2:
                    errMsg = ResourceHelper.GetResource("ErrorUpdateEvPaOfRepGrace");
                    break;
                case -3:
                    errMsg = ResourceHelper.GetResource("ErrorUpdateChargeOfRepGrace");
                    break;
                case -4:
                    errMsg = ResourceHelper.GetResource("ErrorUpdateNoticeOfRepGrace");
                    break;
                case -5:
                    errMsg = ResourceHelper.GetResource("ErrorCancelOfRepGrace");
                    break;
            }
            return result;
        }

        // 2013-07-25 add by Henry for get notice entity
        private NoticeEntity GetNoticeEntity(int repIntNo)
        {
            return notService.GetByNotIntNo(
                chgService.GetByChgIntNo(
                        repService.GetByRepIntNo(repIntNo).ChgIntNo
                    ).NotIntNo
                );
        }
    }
}
