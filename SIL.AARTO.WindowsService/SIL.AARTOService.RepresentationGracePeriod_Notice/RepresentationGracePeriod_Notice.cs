﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.RepresentationGracePeriod_Notice
{
    partial class RepresentationGracePeriod_Notice : ServiceHost
    {
        public RepresentationGracePeriod_Notice()
            : base("", new Guid("16C92CBE-A23F-41EA-8E1F-25A2D05363EA"), new RepresentationGracePeriod_NoticeService())
        {
            InitializeComponent();
        }

    }
}
