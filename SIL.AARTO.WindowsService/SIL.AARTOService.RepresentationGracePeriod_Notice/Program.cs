﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.RepresentationGracePeriod_Notice
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.RepresentationGracePeriod_Notice"
                ,
                DisplayName = "SIL.AARTOService.RepresentationGracePeriod_Notice"
                ,
                Description = "SIL AARTOService RepresentationGracePeriod_Notice"
            };

            ProgramRun.InitializeService(new RepresentationGracePeriod_Notice(), serviceDescriptor, args);
        }
    }
}
