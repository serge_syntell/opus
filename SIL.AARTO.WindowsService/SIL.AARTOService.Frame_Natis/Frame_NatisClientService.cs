﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.AARTOService.Library;
using Stalberg.TMS;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;
using SIL.AARTOService.DAL;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using System.Reflection;
using System.IO;
using SIL.AARTOService.Resource;

namespace SIL.AARTOService.Frame_Natis
{
    public class NatisSendDetails
    {
        public int AutIntNo { get; set; }
        public int FrameIntNo { get; set; }
        public string RegNo { get; set; }
        public string OffenceDate { get; set; }
        public string IDNo { get; set; }
        //int FrameStatus { get; set; }
        public DateTime OffenceDateDT { get; set; }
        public int Flag { get; set; }
        public byte[] RowVersion { get; set; }
    }

    public class Frame_NatisClientService : ServiceDataProcessViaQueue
    {
        public Frame_NatisClientService()
            : base("", "", new Guid("A9325DF7-49BB-4E7D-9F53-8D72E2409EA0"), ServiceQueueTypeList.Frame_Natis)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.connNatis_INPUNC = AARTOBase.GetConnectionString(ServiceConnectionNameList.Natis_INP, ServiceConnectionTypeList.UNC);
            AARTOBase.OnServiceStarting = () =>
            {
                AARTOBase.CheckFolder(this.connNatis_INPUNC);
            };
        }

        string connAARTODB;
        string connNatis_INPUNC;

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }

        int autIntNo;
        int frameIntNo;
        string currentAutCode, lastAutCode;
        bool natisLast = true;
        string emailToAdministrator = string.Empty;
        bool batchIsFinished = true;
        bool authorityIsFinished = true;
        //string showDifferencesOnly = "Y";
        //int noOfRecords;
        string exportPath;
        string exportFullPath;
        string fileName;
        //DateTime dtLastRun;
        bool isChecked = true;

        AARTORules rules = AARTORules.GetSingleTon();
        AuthorityDetails authDetails = null;
        //NatisParamsDB.NatisParamDetails npDetails = null;
        List<NatisSendDetails> sendDetails = new List<NatisSendDetails>();
        List<NatisSendDetails> validSendDetails = new List<NatisSendDetails>();
        List<string> fileList = new List<string>();
        FrameService frameService = new FrameService();

        public override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.PostPone;

                    SetServiceParameters();

                    if (!ServiceUtility.IsNumeric(body))
                    {
                        AARTOBase.ErrorProcessing(ref item, "Body of this queue is null and this queue will be discarded", false, QueueItemStatus.Discard);
                        return;
                    }
                    this.currentAutCode = item.Group.Trim();

                    rules.InitParameter(this.connAARTODB, this.currentAutCode);

                    if (this.lastAutCode != this.currentAutCode)
                    {
                        if (this.batchIsFinished)
                        {
                            this.frameIntNo = Convert.ToInt32(body.Trim());

                            QueueItemValidation(ref item);

                            if (this.isChecked)
                            {
                                this.sendDetails.Clear();
                                this.validSendDetails.Clear();

                                AddNatisSendDetails(ref item);

                                this.batchIsFinished = false;
                                this.authorityIsFinished = false;
                            }
                        }
                        else
                        {
                            this.authorityIsFinished = true;
                            //item.Status = QueueItemStatus.Retry;
                            //AARTOBase.SkipReceivingData = true;
                            AARTOBase.SkipReceivingQueueData(ref item);
                        }
                    }
                    else
                    {
                        this.frameIntNo = Convert.ToInt32(body.Trim());

                        if (this.batchIsFinished)
                        {
                            this.sendDetails.Clear();
                            this.validSendDetails.Clear();
                        }

                        AddNatisSendDetails(ref item);

                        this.batchIsFinished = false;
                        this.authorityIsFinished = false;
                    }

                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
            else
            {
                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.currentAutCode));
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                return;
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            if (!WriteToINPFile()) return;

            if (!UpdateFrameStatus()) return;

            if (!WriteEmail()) return;

            this.batchIsFinished = true;
        }


        private void QueueItemValidation(ref QueueItem item)
        {
            string msg;
            int statusLoaded = 0;
            int statusSent = 0;
            this.isChecked = true;

            //AuthorityRulesDetails details;
            //DefaultAuthRules ar;
            //KeyValuePair<int, string> defaultAutRule;

            if (this.lastAutCode != this.currentAutCode)
            {
                this.isChecked = false;
                this.exportPath = this.connNatis_INPUNC;
                if (this.exportPath.IndexOf(@"\" + this.currentAutCode.Trim()) < 0)
                    this.exportPath += @"\" + this.currentAutCode.Trim();
                if (!CheckNatisFolder(this.exportPath))
                    return;

                this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim().Equals(this.currentAutCode, StringComparison.OrdinalIgnoreCase));
                if (this.authDetails != null && this.authDetails.AutIntNo > 0)
                {
                    this.autIntNo = this.authDetails.AutIntNo;
                    string autName = this.authDetails.AutName;

                    //details = new AuthorityRulesDetails();
                    //details.AutIntNo = this.autIntNo;
                    //details.ARCode = "0570";
                    //details.LastUser = AARTOBase.lastUser;

                    //ar = new DefaultAuthRules(details, this.connAARTODB);
                    //defaultAutRule = ar.SetDefaultAuthRule();

                    if (rules.Rule("0570").ARString.Trim() != "N")
                    {
                        AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("AuthorityRuleStringNotEqualsN"), autName, this.frameIntNo), true);
                        return;
                    }

                    //this.npDetails = null;
                    //this.npDetails = new NatisParamsDB(this.connAARTODB).GetNatisParamsDetails(autIntNo);
                    //if (this.npDetails.nNpIntNo == 0)
                    //{
                    //    item.IsSuccessful = false;
                    //    item.Status = QueueItemStatus.UnKnown;
                    //    this.Logger.Error(string.Format(ResourceHelper.GetResource("NotLocateValidNatisParams"), autName, this.frameIntNo));
                    //    StopService();
                    //    return;
                    //}

                    //if (string.IsNullOrEmpty(npDetails.sTerminalID) || !AARTOBase.IsNumeric(npDetails.sTerminalID))
                    if (string.IsNullOrEmpty(this.authDetails.AutNatisCode) || !ServiceUtility.IsNumeric(this.authDetails.AutNatisCode))
                    {
                        AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("NatisCodeNotValid"), autName, this.authDetails.AutNatisCode, this.frameIntNo), true);
                        return;
                    }

                    //if (!SendEmailManager.CheckEmailAddress(npDetails.sSendNatisEmail))
                    if (!SendEmailManager.CheckEmailAddress(this.authDetails.AutSendNatisEmail))
                    {
                        AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("EmailAddressError"), true);
                        return;
                    }

                    try
                    {
                        //details = new AuthorityRulesDetails();
                        //details.AutIntNo = this.autIntNo;
                        //details.ARCode = "0400";
                        //details.LastUser = AARTOBase.lastUser;

                        //ar = new DefaultAuthRules(details, this.connAARTODB);
                        //defaultAutRule = ar.SetDefaultAuthRule();

                        if (rules.Rule("0400").ARString.Trim().Equals("Y"))
                        {
                            this.natisLast = true;
                            msg = ResourceHelper.GetResource("NatisSentAfterAdjudication");
                            statusLoaded = AARTOServiceBase.STATUS_LOADED_LAST - 1;
                            statusSent = AARTOServiceBase.STATUS_SENT_TO_NATIS_LAST;
                        }
                        else
                        {
                            this.natisLast = false;
                            msg = ResourceHelper.GetResource("NatisSentBeforeVerification");
                            statusLoaded = AARTOServiceBase.STATUS_LOADED_FIRST;
                            statusSent = AARTOServiceBase.STATUS_SENT_TO_NATIS_FIRST;
                        }
                        this.Logger.Info(string.Format(ResourceHelper.GetResource("ErrorForAuthorityFrame"), AARTOBase.currentMethod, msg, autName, this.frameIntNo));

                        //this.noOfRecords = AARTOServiceBase.NO_OF_NATIS_RECORDS;

                        //details = new AuthorityRulesDetails();
                        //details.AutIntNo = this.autIntNo;
                        //details.ARCode = "1510";
                        //details.LastUser = AARTOBase.lastUser;

                        //ar = new DefaultAuthRules(details, this.connAARTODB);
                        //ar.SetDefaultAuthRule();
                        //this.noOfRecords = details.ARNumeric;

                        //NaTISFilesDB natis = new NaTISFilesDB(this.connAARTODB);
                        //DateTime lastDate = natis.GetLastNatisDate(this.autIntNo);

                        bool sendNow = true;

                        //// we do some added checking (not in this order though)
                        //// 1 - when is the actual start date for today
                        //// 2 - what is the interval between runs
                        //// 3 - how many runs in the day
                        //DateTime dtStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, this.npDetails.nStartRunTime, 0, 0);
                        //if (DateTime.Now < dtStart)
                        //{
                        //    sendNow = false;
                        //    this.Logger.Info(string.Format(ResourceHelper.GetResource("BypassingCreationAndSendNatisINPStartTimeLessThanExecuteTime"), DateTime.Now.ToString(), autName, this.frameIntNo));
                        //}

                        //// now we check to see th total amount of time this has run
                        //int nCount = natis.GetNatisDateCount(this.autIntNo);
                        //if (nCount > this.npDetails.nNoRuns)
                        //{
                        //    sendNow = false;
                        //    this.Logger.Info(string.Format(ResourceHelper.GetResource("BypassingCreationAndSendNatisINPRunCountExceeds"), this.npDetails.nNoRuns, autName, this.frameIntNo));
                        //}

                        //double noOfHours = Convert.ToDouble(this.npDetails.nInterval);
                        //this.dtLastRun = lastDate.AddHours(noOfHours);

                        //if (this.dtLastRun > DateTime.Now)
                        //{
                        //    sendNow = false;
                        //    this.Logger.Info(string.Format(ResourceHelper.GetResource("BypassingCreationAndSendNatisINPIntervalGapNotPassed"), DateTime.Now.ToString(), autName, this.frameIntNo));
                        //}

                        if (sendNow)
                        {
                            this.Logger.Info(string.Format(ResourceHelper.GetResource("StartingCreationAndSendNatisINP"), DateTime.Now.ToString(), autName, this.frameIntNo));

                            //details = new AuthorityRulesDetails();
                            //details.AutIntNo = this.autIntNo;
                            //details.ARCode = "0550";
                            //details.LastUser = AARTOBase.lastUser;

                            //ar = new DefaultAuthRules(details, this.connAARTODB);
                            //ar.SetDefaultAuthRule();
                            //this.showDifferencesOnly = details.ARString;

                            //DataSet natisAuthDS = GetNatisAuthorities(statusLoaded, statusSent, "Frame", this.autIntNo, out msg);
                            //DataSet natisAuthDS = AARTOBase.GetNatisAuthorities(this.connAARTODB, statusLoaded, statusSent, "Frame", this.autIntNo, out msg);
                            //if (natisAuthDS != null && natisAuthDS.Tables.Count > 0 && natisAuthDS.Tables[0].Rows.Count > 0)
                            //{
                            //    // validation is finished
                            this.lastAutCode = this.currentAutCode;
                            this.isChecked = true;
                            //}
                            //else
                            //{
                            //    AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("FailedGetNatisAuthorities"), autName, this.frameIntNo, msg));
                            //}
                        }
                        else
                        {
                            AARTOBase.ErrorProcessing(ref item, null, false, QueueItemStatus.Retry);
                        }
                    }
                    catch (Exception ex)
                    {
                        AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorForAuthorityFrame2"), AARTOBase.currentMethod, autName, this.frameIntNo, ex.Message, DateTime.Now.ToString()), true);
                        return;
                    }
                }
                else
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("NotOfWOAFailedToGetAutInfo"));
                    return;
                }
            }

        }

        private void AddNatisSendDetails(ref QueueItem item)
        {
            if (item.IsSuccessful)
            {
                string msg;
                int frameStatus = 100;
                if (this.natisLast)
                    frameStatus = AARTOServiceBase.STATUS_SENT_TO_NATIS_LAST;
                else
                    frameStatus = AARTOServiceBase.STATUS_SENT_TO_NATIS_FIRST;

                // Oscar 20120313 add check for duplicate frame.
                if (this.sendDetails.FirstOrDefault(p => p.FrameIntNo == this.frameIntNo) != null)
                {
                    // Jake 2013-07-12 added message when discard queue
                    //AARTOBase.ErrorProcessing(ref item, String.Format("Duplicate frame found FrameIntNo : {0} and this queue will be discarded",this.frameIntNo), false, QueueItemStatus.Discard);
                    // 2014-02-07, Oscar has moved message into resource file.
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("DuplicateFrameFoundAndQueueWillBeDiscarded", this.frameIntNo),
                        LogType.Info, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                    return;
                }

                NatisSendDetails send = GetNaTISSendData_WS(this.autIntNo, this.frameIntNo, AARTOBase.LastUser, frameStatus, out msg);
                if (send != null)
                {
                    // Jake 2013-07-12 added message when discard queue
                    if (send.Flag.Equals(3))
                        //AARTOBase.ErrorProcessing(ref item, string.Format("This frame has been set to natis or dead frameIntNo: {0}", this.frameIntNo), false, QueueItemStatus.Discard);  //successfully
                        // 2014-02-07, Oscar has moved message into resource file.
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("ThisFrameHasAlreadyBeenSentToNatisOrAtAStatusThatCannotBeSentToNatis", this.frameIntNo),
                            LogType.Info, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard); //successfully
                    else
                    {
                        this.sendDetails.Add(send);
                        if (send.Flag.Equals(1))
                        {
                            // 2014-03-13, Oscar added RegNoHasChanged
                            // 2014-12-02, Removed by Dawn.
                            //Frame frame;
                            //if ((frame = frameService.GetByFrameIntNo(frameIntNo)) != null
                            //    && frame.RegNoHasChanged.GetValueOrDefault())
                            //{
                            //    // discard this frame queue to avoid loading again.
                            //    AARTOBase.LogProcessing(ResourceHelper.GetResource("RegNoHasBeenChangedOnlySendOnce"), LogType.Info, ServiceOption.Continue, item, QueueItemStatus.Discard);
                            //}

                            this.validSendDetails.Add(send);
                            AARTOBase.BatchCount--;
                        }
                        else if (send.Flag.Equals(0))
                        {
                            //AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("IneligibleDataForBeingSentOutToNaits"), this.authDetails.AutName, this.frameIntNo));
                            //Oscar 20120405 change to Info
                            item.IsSuccessful = false;
                            item.Status = QueueItemStatus.Discard;
                            this.Logger.Info(ResourceHelper.GetResource("IneligibleDataForBeingSentOutToNaits", this.authDetails.AutName, this.frameIntNo));
                        }
                        else if (send.Flag.Equals(4))
                        {
                            //dls 2012-09-01-need to keep rows where FilmLockUser is not NULL and FilmLoadStatus != 1000 on the Q to try again later
                            item.IsSuccessful = false;
                            item.Status = QueueItemStatus.UnKnown;
                            this.Logger.Info(ResourceHelper.GetResource("IneligibleDataForBeingSentOutToNaits_TryAgain", this.authDetails.AutName, this.frameIntNo));
                        }
                        else
                        {
                            // Jake 2013-07-12 added message when discard queue
                            item.Status = QueueItemStatus.Discard; //basket
                            this.Logger.Info(String.Format("Invalid data and throw into the Basket frameIntNo:{0}", this.frameIntNo));
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(msg))
                        item.Status = QueueItemStatus.Discard;
                    else
                    {
                        AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("GetNatisSendDataHasError"), this.authDetails.AutName, this.frameIntNo, msg), true);
                    }
                }
            }
        }

        private bool WriteToINPFile()
        {
            // 2014-02-07, Oscar reset fileName at beginning
            this.fileName = null;

            if (this.validSendDetails.Count > 0)
            {
                //string exportPath
                int seqNoLength = 3;
                char cTemp = Convert.ToChar("0");
                string tnType = "NIN";
                string sNumber = "0";
                int maxNo = 999;

                TranNoDB number = new TranNoDB(this.connAARTODB);
                int tNumber = number.GetNextTranNoByType(tnType, this.autIntNo, AARTOBase.LastUser, maxNo);
                if (tNumber > 0)
                    sNumber = tNumber.ToString().PadLeft(seqNoLength, cTemp);
                else
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("NoTranNumberForTypeMIN"), AARTOBase.currentMethod, this.frameIntNo), true);
                    return false;
                }
                //sNumber = npDetails.sTerminalID.Trim() + sNumber;
                sNumber = this.authDetails.AutNatisCode.Trim() + sNumber;

                this.fileName = sNumber + @".INP";

                // check again.
                if (!CheckNatisFolder(this.exportPath)) return false;

                this.exportFullPath = exportPath + @"\" + fileName;
                if (!DeleteNatisFile(this.exportFullPath)) return false;

                string sLenRegNo = "";
                string sLenOffDate = "";
                string sLenIDNo = "";
                string sLenNumber = sNumber.Length.ToString().PadLeft(2, cTemp);

                StreamWriter sw = null;

                try
                {
                    sw = File.CreateText(exportFullPath);

                    foreach (NatisSendDetails nsDetail in this.validSendDetails)
                    {
                        sLenRegNo = nsDetail.RegNo.Length.ToString().PadLeft(2, cTemp);
                        sLenOffDate = nsDetail.OffenceDate.Length.ToString().PadLeft(2, cTemp);
                        sLenIDNo = nsDetail.IDNo.Length.ToString().PadLeft(2, cTemp);
                        sw.WriteLine("5040" + sNumber + "003" + sLenRegNo + nsDetail.RegNo + "060" + sLenOffDate + nsDetail.OffenceDate + "065" + sLenIDNo + nsDetail.IDNo + "99901");
                    }
                    sw.WriteLine("5040" + sNumber + "065" + "23" + "99999999999999999999999" + "99901");
                    sw.WriteLine("5040" + sNumber + "065" + "29" + "99999999999999999999999999999" + "99901");
                    sw.WriteLine("5990" + sNumber + "900" + sLenNumber + sNumber + "99901");

                    sw.Flush();
                    sw.Close();
                    sw.Dispose();

                    // 2014-02-07, Oscar added info message
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("FileHasBeenCreatedSuccessfully", this.fileName),
                        LogType.Info, status: QueueItemStatus.PostPone);

                    this.fileList.Add(this.exportFullPath);
                }
                catch (Exception ex)
                {
                    if (sw != null)
                    {
                        sw.Close();
                        sw.Dispose();
                    }
                    DeleteNatisFile(this.exportFullPath);
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ErrorOnWritingINP"), AARTOBase.currentMethod, this.exportFullPath, ex.Message), true);
                    return false;
                }
            }
            return true;
        }

        private bool UpdateFrameStatus()
        {
            if (this.sendDetails.Count <= 0) return true;

            DateTime? offenceDate = null;
            int? noOfFrames = null;
            int frameStatus = 100;
            string msg;
            int currentFrame = 0;

            if (this.natisLast)
                frameStatus = AARTOServiceBase.STATUS_SENT_TO_NATIS_LAST;
            else
                frameStatus = AARTOServiceBase.STATUS_SENT_TO_NATIS_FIRST;

            if (this.validSendDetails.Count > 0)
            {
                offenceDate = this.validSendDetails.Min(p => p.OffenceDateDT);
                noOfFrames = this.validSendDetails.Count;
            }

            try
            {
                foreach (NatisSendDetails nsDetail in this.sendDetails)
                {
                    currentFrame = nsDetail.FrameIntNo; // for error msg
                    FrameUpdateAfterNatisSend_WS(nsDetail.AutIntNo, nsDetail.FrameIntNo, frameStatus, this.fileName, nsDetail.Flag, AARTOBase.LastUser, nsDetail.RowVersion, out msg);
                    if (!string.IsNullOrEmpty(msg))
                    {
                        // Oscar 20120312 ignore rowversion error.
                        AARTOBase.ErrorProcessing(msg);
                        if (!msg.Equals(ResourceHelper.GetResource("RowVersionHasBeenChanged", frameIntNo), StringComparison.OrdinalIgnoreCase))
                        {
                            DeleteNatisFile(this.exportFullPath);
                            //AARTOBase.ErrorProcessing(msg);
                            return false;
                        }
                    }
                }

                currentFrame = -1;   //cleanup
                if (this.validSendDetails.Count > 0)
                {
                    NaTISFilesUpdateAfterNatisSend_WS(this.autIntNo, this.fileName, noOfFrames, offenceDate, AARTOBase.LastUser, out msg);
                    if (!string.IsNullOrEmpty(msg))
                    {
                        DeleteNatisFile(this.exportFullPath);
                        AARTOBase.ErrorProcessing(msg);
                        return false;
                    }

                    // 2014-02-07, Oscar added info message
                    if (!string.IsNullOrEmpty(this.fileName))
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("FileHasBeenUpdatedInNaTISFiles", this.fileName),
                            LogType.Info, status: QueueItemStatus.PostPone);
                }

            }
            catch (Exception ex)
            {
                DeleteNatisFile(this.exportFullPath);
                //AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ErrorOnUpdatingFrame"), AARTOBase.currentMethod, this.exportFullPath, ex.Message), true);
                if (currentFrame >= 0)
                    AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorOnUpdatingFrame", currentFrame, ex.Message), true);
                else
                    AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorOnUpdatingNatisFiles", this.exportFullPath, ex.Message), true);
                return false;
            }
            finally
            {
                this.sendDetails.Clear();
                this.validSendDetails.Clear();
            }

            return true;
        }

        private bool WriteEmail()
        {
            if (this.fileList.Count <= 0) return true;
            if (this.authorityIsFinished || this.MustSleep)
            {
                StringBuilder sb = new StringBuilder();
                string emailPart1 = ResourceHelper.GetResource("Email_FrameNatisPart1");
                string emailPart2 = "{0};";
                //string emailPart3 = "\r\n\r\nThe file(s) are attached to this email.\r\n\r\nPlease submit the file(s) to NaTIS and on receipt of their response import their file(s) into the Opus system.\r\n\r\nThank you very much.\r\n\r\nRegards\r\nOpus System Administrator";
                string emailPart3 = ResourceHelper.GetResource("Email_FrameNatisPart2");
                sb.AppendFormat(emailPart1, DateTime.Now);
                foreach (string s in this.fileList)
                {
                    sb.AppendFormat(emailPart2, Path.GetFileName(s));
                }
                sb.Append(emailPart3);

                string mailFrom = authDetails.AutEmail;
                //string mailTo = npDetails.sSendNatisEmail;
                string mailTo = this.authDetails.AutSendNatisEmail;
                string mailSubject = string.Format(ResourceHelper.GetResource("Email_FrameNatisPart"), authDetails.AutName.ToUpper());
                string mailBody = sb.ToString();

                SendEmailManager.SendEmail(mailTo, mailSubject, mailBody);

                this.fileList.Clear();
            }

            return true;
        }


        //private DataSet GetNatisAuthorities(int startStatus, int endStatus, string type, int autIntNo, out string message)
        //{
        //    DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));
        //    SqlParameter[] paras = new SqlParameter[4];
        //    paras[0] = new SqlParameter("@StartStatus", startStatus);
        //    paras[1] = new SqlParameter("@EndStatus", endStatus);
        //    paras[2] = new SqlParameter("@Type", type);
        //    paras[3] = new SqlParameter("@AutIntNo", autIntNo);
        //    return db.ExecuteDataSet("GetNatisAuthData", paras, out message);
        //}

        private NatisSendDetails GetNaTISSendData_WS(int autIntNo, int frameIntNo, string lastUser, int status, out string message)
        {
            DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));
            SqlParameter[] paras = new SqlParameter[4];
            //paras[0] = new SqlParameter("@ShowDifferencesOnly", showDifferencesOnly);
            paras[0] = new SqlParameter("@AutIntNo", autIntNo);
            paras[1] = new SqlParameter("@FrameIntNo", frameIntNo);
            paras[2] = new SqlParameter("@LastUser", lastUser);
            paras[3] = new SqlParameter("@Status", status);
            //paras[5] = new SqlParameter("@NoOFRecords", noOfRecords);
            //paras[4] = new SqlParameter("@DtLastRun", dtLastRun);
            DataSet result = db.ExecuteDataSet("FrameSendNaTIS_WS", paras, out message);
            NatisSendDetails detail = null;
            if (result != null && result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
            {
                detail = new NatisSendDetails();
                PropertyInfo[] pis = detail.GetType().GetProperties();
                DataRow row = result.Tables[0].Rows[0];
                DataColumnCollection columns = result.Tables[0].Columns;
                foreach (PropertyInfo pi in pis)
                {
                    if (columns.Contains(pi.Name) && row[pi.Name] != DBNull.Value)
                        pi.SetValue(detail, row[pi.Name], null);
                }
            }
            return detail;
        }

        private int FrameUpdateAfterNatisSend_WS(int autIntNo, int frameIntNo, int status, string frameSendNatisFileName, int flag, string lastUser, byte[] rowVersion, out string message)
        {
            DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));
            SqlParameter[] paras = new SqlParameter[7];
            paras[0] = new SqlParameter("@AutIntNo", autIntNo);
            paras[1] = new SqlParameter("@FrameIntNo", frameIntNo);
            paras[2] = new SqlParameter("@Status", status);
            paras[3] = new SqlParameter("@FrameSendNatisFileName", frameSendNatisFileName ?? "");
            //paras[4] = new SqlParameter("@NoOfFrames", noOfFrames);
            //paras[3] = new SqlParameter("@FirstOffenceDate", firstOffenceDate);
            paras[4] = new SqlParameter("@Flag", flag);
            paras[5] = new SqlParameter("@LastUser", lastUser);
            paras[6] = new SqlParameter("@RowVersion", rowVersion);
            object obj = db.ExecuteScalar("FrameUpdateAfterNatisSend_WS", paras, out message);
            int result;
            if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(message))
                result = Convert.ToInt32(obj);
            else
                result = 0;
            switch (result)
            {
                case -1:
                    message = ResourceHelper.GetResource("UpdatingNatisSendFiles");
                    break;
                case -2:
                    message = ResourceHelper.GetResource("UpdatingNatisBasket");
                    break;
                case -3:
                    message = ResourceHelper.GetResource("RowVersionHasBeenChanged", frameIntNo);
                    //AARTOBase.AbortCurrentBatch = true;
                    break;
            }
            return result;
        }
        // 2013-07-29 add parameter lastUser by Henry
        private int NaTISFilesUpdateAfterNatisSend_WS(int autIntNo, string frameSendNatisFileName, int? noOfFrames, DateTime? firstOffenceDate, string lastUser, out string message)
        {
            DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));
            SqlParameter[] paras = new SqlParameter[5];
            paras[0] = new SqlParameter("@AutIntNo", autIntNo);
            paras[1] = new SqlParameter("@FrameSendNatisFileName", frameSendNatisFileName);
            paras[2] = new SqlParameter("@NoOfFrames", noOfFrames);
            paras[3] = new SqlParameter("@FirstOffenceDate", firstOffenceDate);
            paras[4] = new SqlParameter("@LastUser", lastUser);
            object obj = db.ExecuteScalar("NaTISFilesUpdateAfterNatisSend_WS", paras, out message);
            int result;
            if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(message))
                result = Convert.ToInt32(obj);
            else
                result = 0;
            switch (result)
            {
                case -1:
                    message = ResourceHelper.GetResource("UpdatingTableNatisFiles");
                    break;
            }
            return result;
        }

        private void SetServiceParameters()
        {
            if (ServiceParameters != null
                && string.IsNullOrEmpty(this.emailToAdministrator)
                && ServiceParameters.ContainsKey("EmailToAdministrator")
                && !string.IsNullOrEmpty(ServiceParameters["EmailToAdministrator"]))
                this.emailToAdministrator = ServiceParameters["EmailToAdministrator"];
            if (!SendEmailManager.CheckEmailAddress(this.emailToAdministrator))
                AARTOBase.ErrorProcessing(ResourceHelper.GetResource("EmailAddressError"), true);
        }

        bool CheckNatisFolder(string destinationPath)
        {
            bool result = AARTOBase.CheckFolder(destinationPath);
            if (!result)
                this.Logger.Error(string.Format(ResourceHelper.GetResource("FolderNotExistOrInaccessible"), AARTOBase.currentMethod, destinationPath));
            return result;
        }
        bool DeleteNatisFile(string fullPath)
        {
            if (File.Exists(fullPath))
            {
                try
                {
                    File.Delete(fullPath);
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ex);
                    return false;
                }
            }
            return true;
        }

    }

}
