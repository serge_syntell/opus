﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Frame_Natis
{
    partial class Frame_Natis : ServiceHost
    {
        public Frame_Natis()
            : base("", new Guid("0358B86C-9B71-4E18-9CD7-3318A4DC6A1C"), new Frame_NatisClientService())
        {
            InitializeComponent();
        }
    }
}
