﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Frame_Natis
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.Frame_Natis"
                ,
                DisplayName = "SIL.AARTOService.Frame_Natis"
                ,
                Description = "SIL AARTOService Frame_Natis"
            };

            ProgramRun.InitializeService(new Frame_Natis(), serviceDescriptor, args);
        }
    }
}
