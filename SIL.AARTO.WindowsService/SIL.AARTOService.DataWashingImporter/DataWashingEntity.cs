﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTOService.DataWashingImporter
{
    public class DataWashingEntity
    {
        public string IdNumber { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string ThirdName { get; set; }
        public string Surname { get; set; }
        public string OtherID { get; set; }
        public string Gender { get; set; }
        public string Title { get; set; }
        public string MaritalStatus { get; set; }
        public string SpouseName { get; set; }
        public string FirstAddressLine1 { get; set; }
        public string FirstAddressLine2 { get; set; }
        public string FirstAddressLine3 { get; set; }
        public string FirstAddressLine4 { get; set; }
        public string FirstAddressPostalCode { get; set; }
        public string FirstAddressDate { get; set; }
        public string FirstAddressProvCode { get; set; }
        public string FirstAddressCountryCode { get; set; }
        public string SecondAddressLine1 { get; set; }
        public string SecondAddressLine2 { get; set; }
        public string SecondAddressLine3 { get; set; }
        public string SecondAddressLine4 { get; set; }
        public string SecondAddressPostalCode { get; set; }
        public string SecondAddressDate { get; set; }
        public string SecondAddressProvCode { get; set; }
        public string SecondAddressCountryCode { get; set; }
        public string SecondAddressOTInd { get; set; }
        public string ThirdAddressLine1 { get; set; }
        public string ThirdAddressLine2 { get; set; }
        public string ThirdAddressLine3 { get; set; }
        public string ThirdAddressLine4 { get; set; }
        public string ThirdAddressPostalCode { get; set; }
        public string ThirdAddressDate { get; set; }
        public string ThirdAddressProvCode { get; set; }
        public string ThirdAddressCountryCode { get; set; }
        public string FourthAddressLine1 { get; set; }
        public string FourthAddressLine2 { get; set; }
        public string FourthAddressLine3 { get; set; }
        public string FourthAddressLine4 { get; set; }
        public string FourthAddressPostalCode { get; set; }
        public string FourthAddressDate { get; set; }
        public string FourthAddressProvCode { get; set; }
        public string FourthAddressCountryCode { get; set; }
        public string Employer1 { get; set; }
        public string Emp1Occupation { get; set; }
        public string Employer2 { get; set; }
        public string Emp2Occupation { get; set; }
        public string Employer3 { get; set; }
        public string Emp3Occupation { get; set; }
        public string WorkTel1Code { get; set; }
        public string WorkTel1No { get; set; }
        public string WorkTel1Date { get; set; }
        public string WorkTel2Code { get; set; }
        public string WorkTel2No { get; set; }
        public string WorkTel2Date { get; set; }
        public string WorkTel3Code { get; set; }
        public string WorkTel3No { get; set; }
        public string WorkTel3Date { get; set; }
        public string HomeTel1Code { get; set; }
        public string HomeTel1No { get; set; }
        public string HomeTel1Date { get; set; }
        public string HomeTel2Code { get; set; }
        public string HomeTel2No { get; set; }
        public string HomeTel2Date { get; set; }
        public string HomeTel3Code { get; set; }
        public string HomeTel3No { get; set; }
        public string HomeTel3Date { get; set; }
        public string Cell1No { get; set; }
        public string Cell1Date { get; set; }
        public string Cell2No { get; set; }
        public string Cell2Date { get; set; }
        public string Cell3No { get; set; }
        public string Cell3Date { get; set; }
        public string EMailAdd { get; set; }

    }
}
