﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;
using System.Text.RegularExpressions;

namespace SIL.AARTOService.DataWashingImporter
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
           
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.DataWashingImporter"
                ,
                DisplayName = "SIL.AARTOService.DataWashingImporter"
                ,
                Description = "SIL.AARTOService.DataWashingImporter"
            };

            ProgramRun.InitializeService(new DataWashingImporter(), serviceDescriptor, args);
        }
    }
}
