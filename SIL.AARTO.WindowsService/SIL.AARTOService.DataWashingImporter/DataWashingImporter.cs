﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.DataWashingImporter
{
    public partial class DataWashingImporter : ServiceHost
    {
         
         public DataWashingImporter()
            : base("", new Guid("B41FFD07-FB4A-4C5F-B6C6-44C4CE693C2C"), new DataWashingImporterClientService())
        {
            InitializeComponent();
        }

         
    }
}
