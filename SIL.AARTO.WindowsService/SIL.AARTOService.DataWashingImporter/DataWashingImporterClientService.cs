﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities.Validation;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.AARTOService;
using SIL.AARTOService.Library;
using SIL.ServiceQueueLibrary.DAL.Data;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using System.IO;
using System.Data;
using SIL.AARTOService.Resource;
using System.Text.RegularExpressions;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data.SqlClient;
using System.Collections.Specialized;
using SIL.ServiceQueueLibrary.DAL.Services;


namespace SIL.AARTOService.DataWashingImporter
{
    public class DataWashingImporterClientService : ServiceDataProcessViaFileSystem
    {
        string artoConnectionString = string.Empty;
        //string dataWashingFolder = string.Empty;
        string currentAutCode = string.Empty;
        string _lastUser = "DataWashingImporter";
        FileInfo fileInfo = null;
        AARTORules rules = AARTORules.GetSingleTon();
        AARTOServiceBase aartoBase { get { return (AARTOServiceBase)_serviceHelper; } }

        // 2014-07-28, Oscar added, for status 601
        readonly ServiceQueueService queueService = new ServiceQueueService();
        readonly NoticeService noticeService = new NoticeService();
        readonly AuthorityService authorityService = new AuthorityService();
        readonly QueueItemProcessor queueProcessor = new QueueItemProcessor();

        public DataWashingImporterClientService()
            : base("", "", new Guid("079CD32C-78BE-408C-982A-B844705E4127"))
        {
            this._serviceHelper = new AARTOServiceBase(this, false);
            this.artoConnectionString = aartoBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.DestinationPath = aartoBase.GetConnectionString(ServiceConnectionNameList.DataWashing_Imported, ServiceConnectionTypeList.UNC);
            SIL.AARTO.DAL.Data.DataRepository.AddConnection("SIL.AARTO.DAL.Data.ConnectionString", this.artoConnectionString);

            //InitializeAartoConnectionStringForNetTier(this.artoConnectionString);
            // 2014-07-29, Oscar changed
            ServiceUtility.InitializeNetTier(this.artoConnectionString);

            aartoBase.OnServiceStarting = () =>
            {
                aartoBase.CheckFolder(this.DestinationPath);
            };
        }

        public override void InitialWork(ref QueueLibrary.QueueItem item)
        {
            if (item.Body != null && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;

                    //this.currentAutCode = item.Group;

                    this.fileInfo = (FileInfo)item.Body;

                    if (this.fileInfo != null)
                        this.currentAutCode = GetAutCodeFromDataWashingFile(this.fileInfo);

                    if (String.IsNullOrEmpty(this.currentAutCode))
                    {
                        item.IsSuccessful = false;
                        this.Logger.Error(string.Format(ResourceHelper.GetResource("DataWashOfInvalidFileName"), aartoBase.LastUser, this.fileInfo.FullName, this.fileInfo.Name));
                    }
                    else
                    {
                        Stalberg.TMS.AuthorityDetails autDetail = aartoBase.AuthorityList.Where(a => a.AutCode.Trim() == this.currentAutCode.Trim()).FirstOrDefault();

                        if (autDetail == null)
                        {
                            item.IsSuccessful = false;
                            aartoBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", String.Format("AutCode: {0}", this.currentAutCode.Trim())));
                            return;
                        }

                        rules.InitParameter(this.artoConnectionString, this.currentAutCode);

                        if (rules.Rule("9060").ARString != "Y") // Data washing is active
                        {
                            item.IsSuccessful = false;
                        }

                        item.Group = this.currentAutCode;
                    }

                }
                catch (Exception ex)
                {
                    item.IsSuccessful = false;
                    item.Status = QueueItemStatus.UnKnown;
                    this.Logger.Error(string.Format(ResourceHelper.GetResource("ServiceHasError"), aartoBase.LastUser, item.Body, ex.ToString()));
                    //StopService();
                    // Oscar 2013-04-10 changed
                    aartoBase.StopService();
                    return;
                }
            }
            else
            {
                aartoBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                return;
            }
        }

        public override void MainWork(ref List<QueueLibrary.QueueItem> queueList)
        {
            if (queueList != null)
            {
                string strLine = string.Empty;
                string[] dataWashingArry = null;
                //bool importFailed = false;
                foreach (QueueItem item in queueList)
                {
                    if (item.IsSuccessful == false)
                    {
                        // Jake 2013-07-12 added message when discard queue 
                        item.Status = QueueItemStatus.Discard;
                        this.Logger.Info("Invalid data or data washing is not active queue key:" + item.Body.ToString());
                        continue;
                    }

                    this.fileInfo = item.Body as FileInfo;

                    /* Disable by Edge 20130801
                    StreamReader reader = new StreamReader(this.fileInfo.OpenRead(), true);

                    try
                    {
                        bool firstRow = true;
                        while (reader.Peek() >= 0)
                        {

                            strLine = reader.ReadLine();

                            //Jake 2012-04-16
                            // We need to skip the first row for the header inforamtion
                            if (firstRow) { firstRow = false; continue; }

                            dataWashingArry = strLine.Split(',');

                            DataWashingEntity dse = BindWashDataToEntity(dataWashingArry);

                            if (dse != null)
                            {
                                //Update Data washing data into DB here
                                try
                                {
                                    if (dse.IdNumber != null && dse.IdNumber.Length == 13)
                                    {
                                        if (ImportWashindData(dse) == false)
                                        {
                                            //this.Logger.Info(String.Format(ResourceHelper.GetResource("WashedDataSuccessfullyImported"), dse.IdNumber));

                                            this.Logger.Info(String.Format(ResourceHelper.GetResource("WashedDataImportedFailed"), dse.IdNumber));
                                        }
                                    }
                                    else
                                    {
                                        this.Logger.Error(String.Format(ResourceHelper.GetResource("WashedDataInvalid"), dse.IdNumber));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorOnUpdateDataWashToAartoDB"), dse.IdNumber, ex.ToString(), item.Body));
                                    //throw ex;
                                }

                            }
                        }

                        //if (importFailed == false)
                        //    item.Status = QueueItemStatus.Discard;
                    }
                    catch (Exception ex)
                    {
                        item.IsSuccessful = false;
                        item.Status = QueueItemStatus.UnKnown;
                        this.Logger.Error(string.Format(ResourceHelper.GetResource("ServiceHasError"), aartoBase.LastUser, item.Body, ex.ToString()));
                        //StopService();
                        // Oscar 2013-04-10 changed
                        aartoBase.StopService();

                    }
                    finally
                    {
                        reader.Close();
                        reader.Dispose();
                    }
                     */

                    DataTable table = null;
                    try
                    {
                        table = CSVReader.ReadCSVFile(this.fileInfo.FullName, true);

                        this.Logger.Info(String.Format(ResourceHelper.GetResource("WashedDataOpenedFileSuccessfully"), item.Body));
                    }
                    catch (Exception ex)
                    {
                        table = null;

                        item.IsSuccessful = false;
                        item.Status = QueueItemStatus.UnKnown;
                        this.Logger.Error(String.Format(ResourceHelper.GetResource("WashedDataOpenedFileFailed"), item.Body, ex.ToString()));
                    }

                    try
                    {
                        if (table != null)
                        {
                            if (table.Columns.Count >= 74)
                            {
                                for (int i = 0; i < table.Rows.Count; i++)
                                {
                                    DataWashingEntity dse = BindWashDataToEntity(table.Rows[i]);

                                    //Update Data washing data into DB here
                                    try
                                    {
                                        if (dse.IdNumber != null && dse.IdNumber.Length == 13)
                                        {
                                            try
                                            {
                                                ImportWashindData(dse);
                                                
                                                this.Logger.Info(String.Format(ResourceHelper.GetResource("WashedDataImportedSuccessfully"), dse.IdNumber));
                                            }
                                            catch (Exception ex)
                                            {
                                                this.Logger.Error(String.Format(ResourceHelper.GetResource("WashedDataImportedFailed"), dse.IdNumber, ex.ToString()));
                                            }
                                        }
                                        else
                                        {
                                            this.Logger.Error(String.Format(ResourceHelper.GetResource("WashedDataInvalid"), dse.IdNumber));
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorOnUpdateDataWashToAartoDB"), dse.IdNumber, ex.ToString()));
                                        //throw ex;
                                    }
                                }
                            }
                            else
                            {
                                item.IsSuccessful = false;
                                item.Status = QueueItemStatus.UnKnown;
                                this.Logger.Error(String.Format(ResourceHelper.GetResource("WashedDataColumnsError"), aartoBase.LastUser, item.Body));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        item.IsSuccessful = false;
                        item.Status = QueueItemStatus.UnKnown;
                        this.Logger.Error(string.Format(ResourceHelper.GetResource("ServiceHasError"), aartoBase.LastUser, item.Body, ex.ToString()));
                        //StopService();
                        // Oscar 2013-04-10 changed
                        aartoBase.StopService();
                    }
                }
            }
        }

        //void StopService()
        //{
        //    this.MustStop = true;
        //    ((SIL.ServiceLibrary.ServiceHost)this.ServiceHost).Stop();
        //}

        string GetAutCodeFromDataWashingFile(FileInfo fileInfo)
        {
            string autCode = string.Empty;
            string fileName = fileInfo.Name;
            if (!String.IsNullOrEmpty(fileName)
                && fileName.ToLower().Trim().StartsWith("datawashin") && fileName.Length > 15)
            {
                autCode = fileName.Substring(10, 2);
            }

            return autCode;

            //string pattern = @"^\w+(?=\d{8}[.][a-zA-Z]+)";

            //try
            //{
            //    return Regex.Match(fileInfo.Name, pattern).Value;
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

        }

        DataWashingEntity BindWashDataToEntity(DataRow dr)
        {
            DataWashingEntity dse = null;

            dse = new DataWashingEntity();
            dse.IdNumber = dr[0].ToString();
            dse.FirstName = dr[1].ToString();
            dse.SecondName = dr[2].ToString();
            dse.ThirdName = dr[3].ToString();
            dse.Surname = dr[4].ToString();
            dse.OtherID = dr[5].ToString();
            dse.Gender = dr[6].ToString();
            dse.Title = dr[7].ToString();
            dse.MaritalStatus = dr[8].ToString();
            dse.SpouseName = dr[9].ToString();
            dse.FirstAddressLine1 = dr[10].ToString();
            dse.FirstAddressLine2 = dr[11].ToString();
            dse.FirstAddressLine3 = dr[12].ToString();
            dse.FirstAddressLine4 = dr[13].ToString();
            dse.FirstAddressPostalCode = dr[14].ToString();
            dse.FirstAddressDate = dr[15].ToString();
            dse.FirstAddressProvCode = dr[16].ToString();
            dse.FirstAddressCountryCode = dr[17].ToString();
            dse.SecondAddressLine1 = dr[18].ToString();
            dse.SecondAddressLine2 = dr[19].ToString();
            dse.SecondAddressLine3 = dr[20].ToString();
            dse.SecondAddressLine4 = dr[21].ToString();
            dse.SecondAddressPostalCode = dr[22].ToString();
            dse.SecondAddressDate = dr[23].ToString();
            dse.SecondAddressProvCode = dr[24].ToString();
            dse.SecondAddressCountryCode = dr[25].ToString();
            dse.SecondAddressOTInd = dr[26].ToString();
            dse.ThirdAddressLine1 = dr[27].ToString();
            dse.ThirdAddressLine2 = dr[28].ToString();
            dse.ThirdAddressLine3 = dr[29].ToString();
            dse.ThirdAddressLine4 = dr[30].ToString();
            dse.ThirdAddressPostalCode = dr[31].ToString();
            dse.ThirdAddressDate = dr[32].ToString();
            dse.ThirdAddressProvCode = dr[33].ToString();
            dse.ThirdAddressCountryCode = dr[34].ToString();
            dse.FourthAddressLine1 = dr[35].ToString();
            dse.FourthAddressLine2 = dr[36].ToString();
            dse.FourthAddressLine3 = dr[37].ToString();
            dse.FourthAddressLine4 = dr[38].ToString();
            dse.FourthAddressPostalCode = dr[39].ToString();
            dse.FourthAddressDate = dr[40].ToString();
            dse.FourthAddressProvCode = dr[41].ToString();
            dse.FourthAddressCountryCode = dr[42].ToString();
            dse.Employer1 = dr[43].ToString();
            dse.Emp1Occupation = dr[44].ToString();
            dse.Employer2 = dr[45].ToString();
            dse.Emp2Occupation = dr[46].ToString();
            dse.Employer3 = dr[47].ToString();
            dse.Emp3Occupation = dr[48].ToString();
            dse.WorkTel1Code = dr[49].ToString();
            dse.WorkTel1No = dr[50].ToString();
            dse.WorkTel1Date = dr[51].ToString();
            dse.WorkTel2Code = dr[52].ToString();
            dse.WorkTel2No = dr[53].ToString();
            dse.WorkTel2Date = dr[54].ToString();
            dse.WorkTel3Code = dr[55].ToString();
            dse.WorkTel3No = dr[56].ToString();
            dse.WorkTel3Date = dr[57].ToString();
            dse.HomeTel1Code = dr[58].ToString();
            dse.HomeTel1No = dr[59].ToString();
            dse.HomeTel1Date = dr[60].ToString();
            dse.HomeTel2Code = dr[61].ToString();
            dse.HomeTel2No = dr[62].ToString();
            dse.HomeTel2Date = dr[63].ToString();
            dse.HomeTel3Code = dr[64].ToString();
            dse.HomeTel3No = dr[65].ToString();
            dse.HomeTel3Date = dr[66].ToString();
            dse.Cell1No = dr[67].ToString();
            dse.Cell1Date = dr[68].ToString();
            dse.Cell2No = dr[69].ToString();
            dse.Cell2Date = dr[70].ToString();
            dse.Cell3No = dr[71].ToString();
            dse.Cell3Date = dr[72].ToString();
            dse.EMailAdd = dr[73].ToString();

            return dse;
        }

        DataWashingEntity BindWashDataToEntity(string[] dataWashingArry)
        {
            DataWashingEntity dse = null;
            if (dataWashingArry.Length == 74)
            {
                dse = new DataWashingEntity();
                dse.IdNumber = dataWashingArry[0];
                dse.FirstName = dataWashingArry[1];
                dse.SecondName = dataWashingArry[2];
                dse.ThirdName = dataWashingArry[3];
                dse.Surname = dataWashingArry[4];
                dse.OtherID = dataWashingArry[5];
                dse.Gender = dataWashingArry[6];
                dse.Title = dataWashingArry[7];
                dse.MaritalStatus = dataWashingArry[8];
                dse.SpouseName = dataWashingArry[9];
                dse.FirstAddressLine1 = dataWashingArry[10];
                dse.FirstAddressLine2 = dataWashingArry[11];
                dse.FirstAddressLine3 = dataWashingArry[12];
                dse.FirstAddressLine4 = dataWashingArry[13];
                dse.FirstAddressPostalCode = dataWashingArry[14];
                dse.FirstAddressDate = dataWashingArry[15];
                dse.FirstAddressProvCode = dataWashingArry[16];
                dse.FirstAddressCountryCode = dataWashingArry[17];
                dse.SecondAddressLine1 = dataWashingArry[18];
                dse.SecondAddressLine2 = dataWashingArry[19];
                dse.SecondAddressLine3 = dataWashingArry[20];
                dse.SecondAddressLine4 = dataWashingArry[21];
                dse.SecondAddressPostalCode = dataWashingArry[22];
                dse.SecondAddressDate = dataWashingArry[23];
                dse.SecondAddressProvCode = dataWashingArry[24];
                dse.SecondAddressCountryCode = dataWashingArry[25];
                dse.SecondAddressOTInd = dataWashingArry[26];
                dse.ThirdAddressLine1 = dataWashingArry[27];
                dse.ThirdAddressLine2 = dataWashingArry[28];
                dse.ThirdAddressLine3 = dataWashingArry[29];
                dse.ThirdAddressLine4 = dataWashingArry[30];
                dse.ThirdAddressPostalCode = dataWashingArry[31];
                dse.ThirdAddressDate = dataWashingArry[32];
                dse.ThirdAddressProvCode = dataWashingArry[33];
                dse.ThirdAddressCountryCode = dataWashingArry[34];
                dse.FourthAddressLine1 = dataWashingArry[35];
                dse.FourthAddressLine2 = dataWashingArry[36];
                dse.FourthAddressLine3 = dataWashingArry[37];
                dse.FourthAddressLine4 = dataWashingArry[38];
                dse.FourthAddressPostalCode = dataWashingArry[39];
                dse.FourthAddressDate = dataWashingArry[40];
                dse.FourthAddressProvCode = dataWashingArry[41];
                dse.FourthAddressCountryCode = dataWashingArry[42];
                dse.Employer1 = dataWashingArry[43];
                dse.Emp1Occupation = dataWashingArry[44];
                dse.Employer2 = dataWashingArry[45];
                dse.Emp2Occupation = dataWashingArry[46];
                dse.Employer3 = dataWashingArry[47];
                dse.Emp3Occupation = dataWashingArry[48];
                dse.WorkTel1Code = dataWashingArry[49];
                dse.WorkTel1No = dataWashingArry[50];
                dse.WorkTel1Date = dataWashingArry[51];
                dse.WorkTel2Code = dataWashingArry[52];
                dse.WorkTel2No = dataWashingArry[53];
                dse.WorkTel2Date = dataWashingArry[54];
                dse.WorkTel3Code = dataWashingArry[55];
                dse.WorkTel3No = dataWashingArry[56];
                dse.WorkTel3Date = dataWashingArry[57];
                dse.HomeTel1Code = dataWashingArry[58];
                dse.HomeTel1No = dataWashingArry[59];
                dse.HomeTel1Date = dataWashingArry[60];
                dse.HomeTel2Code = dataWashingArry[61];
                dse.HomeTel2No = dataWashingArry[62];
                dse.HomeTel2Date = dataWashingArry[63];
                dse.HomeTel3Code = dataWashingArry[64];
                dse.HomeTel3No = dataWashingArry[65];
                dse.HomeTel3Date = dataWashingArry[66];
                dse.Cell1No = dataWashingArry[67];
                dse.Cell1Date = dataWashingArry[68];
                dse.Cell2No = dataWashingArry[69];
                dse.Cell2Date = dataWashingArry[70];
                dse.Cell3No = dataWashingArry[71];
                dse.Cell3Date = dataWashingArry[72];
                dse.EMailAdd = dataWashingArry[73];
            }

            return dse;
        }

        bool ImportWashindData(DataWashingEntity dataWashingEntity)
        {
            bool imported = false;
            Mi5PersonaService personalService = new Mi5PersonaService();
            Mi5PersonaAddressStreetService psAddressService = new Mi5PersonaAddressStreetService();
            Mi5PersonaAddressPostService ppAddressService = new Mi5PersonaAddressPostService();
            Mi5PersonaEmployerService employerService = new Mi5PersonaEmployerService();
            Mi5PersonaCommunicationService communicationService = new Mi5PersonaCommunicationService();

            try
            {

                //ConnectionScope.Current.ConnectionStringKey = "SIL.AARTO.DAL.Data.ConnectionString";
                //ConnectionScope.Current.DynamicConnectionString = this.artoConnectionString;


                //using (ConnectionScope.CreateTransaction())
                // 2014-07-29, Oscar changed
                using (var scope = ServiceUtility.CreateTransactionScope())
                {

                    #region MI5Person Table
                    Mi5Persona persona = null;
                    bool needNew = false; bool needUpdate = false;
                    //if (String.IsNullOrEmpty(dataWashingEntity.IdNumber))
                    //{
                    //    return imported;
                    //}
                    //persona = SIL.AARTO.DAL.Data.DataRepository.Connections["SIL.AARTO.DAL.Data.ConnectionString"].Provider.Mi5PersonaProvider.GetByMipidNumber(dataWashingEntity.IdNumber);
                    persona = personalService.GetByMipidNumber(dataWashingEntity.IdNumber);
                    if (persona == null)
                    {
                        needNew = true;
                        persona = new Mi5Persona();
                        persona.MipDateLastUpdated = DateTime.Now;
                    }

                    //persona.MipForeNames = String.Format("{0} {1} {2}", dataWashingEntity.FirstName, dataWashingEntity.SecondName, dataWashingEntity.ThirdName);
                    persona.MipForeNames = ""; persona.MipInitials = "";
                    if (!String.IsNullOrEmpty(dataWashingEntity.FirstName))
                    {
                        persona.MipForeNames += dataWashingEntity.FirstName;
                        persona.MipInitials += dataWashingEntity.FirstName.Trim().Substring(0, 1).ToUpper();
                    }
                    if (!String.IsNullOrEmpty(dataWashingEntity.SecondName))
                    {
                        persona.MipForeNames += " " + dataWashingEntity.SecondName;
                        persona.MipInitials += dataWashingEntity.SecondName.Trim().Substring(0, 1).ToUpper();
                    }
                    if (!String.IsNullOrEmpty(dataWashingEntity.SecondName) && !String.IsNullOrEmpty(dataWashingEntity.ThirdName))
                    {
                        persona.MipForeNames += " " + dataWashingEntity.ThirdName;
                        persona.MipInitials += dataWashingEntity.ThirdName.Trim().Substring(0, 1).ToUpper();
                    }

                    //persona.MipInitials = dataWashingEntity.FirstName == "" ? "" : dataWashingEntity.FirstName.Trim().Substring(0, 1).ToUpper();
                    persona.MipSurname = dataWashingEntity.Surname;
                    persona.MipidNumber = dataWashingEntity.IdNumber;
                    persona.MipOtherId = dataWashingEntity.OtherID;
                    persona.MipGender = dataWashingEntity.Gender;
                    persona.MipTitle = dataWashingEntity.Title;
                    persona.MipMaritalStatus = dataWashingEntity.MaritalStatus;
                    persona.MipSpouseName = dataWashingEntity.SpouseName;

                    persona.MipLastWashedDate = DateTime.Now;
                    persona.LastUser = _lastUser;
                    persona.MiPoAddInvalid = "N";
                    persona = personalService.Save(persona);

                    #endregion

                    #region MI5PersonaAddress Table
                    TimeSpan timeSpan = new TimeSpan();
                    DateTime? latestUpdateDate = null;
                    DateTime? previousUpdateDate = null;

                    #region Mi5PersonaAddressStreet
                    Mi5PersonaAddressStreet latestPersonaAddr = null;
                    Mi5PersonaAddressStreet previousPersonaAddr = null;

                    SIL.AARTO.DAL.Entities.TList<Mi5PersonaAddressStreet> addrStreetList = psAddressService.GetByMipIntNo(persona.MipIntNo);

                    //----------------------------------------------------------
                    latestPersonaAddr = addrStreetList.Where(p => p.MiatIntNo == (int)Mi5AddressTypeList.Option_2).FirstOrDefault();

                    if (!String.IsNullOrEmpty(dataWashingEntity.FirstAddressDate))
                    {
                        //latestUpdateDate = Convert.ToDateTime(dataWashingEntity.FirstAddressDate);
                        latestUpdateDate = ConvertToDateTime(dataWashingEntity.FirstAddressDate);
                    }

                    if (latestPersonaAddr == null && !String.IsNullOrEmpty(dataWashingEntity.FirstAddressDate))
                    {
                        latestPersonaAddr = new Mi5PersonaAddressStreet();
                        needNew = true;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(dataWashingEntity.FirstAddressDate))
                        {
                            //latestUpdateDate = Convert.ToDateTime(dataWashingEntity.FirstAddressDate);
                            latestUpdateDate = ConvertToDateTime(dataWashingEntity.FirstAddressDate);
                            previousUpdateDate = latestPersonaAddr.MipaDateLastUpdated;

                            timeSpan = latestUpdateDate.Value - previousUpdateDate.Value;

                            if (timeSpan.TotalDays > 0)
                            {
                                needUpdate = true;
                            }
                        }
                        else
                            latestUpdateDate = null;
                    }

                    if (needNew || needUpdate)
                    {
                        latestPersonaAddr.MipIntNo = persona.MipIntNo;
                        latestPersonaAddr.MipaSource = "DataWash";
                        latestPersonaAddr.MiatIntNo = (int)Mi5AddressTypeList.Option_2;
                        latestPersonaAddr.MipaStAdd1 = dataWashingEntity.FirstAddressLine1;
                        latestPersonaAddr.MipaStAdd2 = dataWashingEntity.FirstAddressLine2;
                        latestPersonaAddr.MipaStAdd3 = dataWashingEntity.FirstAddressLine3;
                        latestPersonaAddr.MipaStAdd4 = dataWashingEntity.FirstAddressLine4;
                        latestPersonaAddr.MipaStCode = dataWashingEntity.FirstAddressPostalCode;
                        latestPersonaAddr.MipaStProvinceCode = dataWashingEntity.FirstAddressProvCode;
                        latestPersonaAddr.MipaStCountryCode = dataWashingEntity.FirstAddressCountryCode;
                        latestPersonaAddr.MipaDateLastUpdated = latestUpdateDate ?? DateTime.Now;
                        latestPersonaAddr.MipaStLastUpdated = latestUpdateDate ?? DateTime.Now;
                        latestPersonaAddr.LastUser = _lastUser;
                        latestPersonaAddr = psAddressService.Save(latestPersonaAddr);
                    }

                    //--------------------------------------------------------
                    needNew = needUpdate = false;

                    previousPersonaAddr = addrStreetList.Where(p => p.MiatIntNo == (int)Mi5AddressTypeList.Option_3).FirstOrDefault();

                    if (!String.IsNullOrEmpty(dataWashingEntity.SecondAddressDate))
                    {
                        latestUpdateDate = ConvertToDateTime(dataWashingEntity.SecondAddressDate);
                    }

                    if (previousPersonaAddr == null && !String.IsNullOrEmpty(dataWashingEntity.SecondAddressDate))
                    {
                        previousPersonaAddr = new Mi5PersonaAddressStreet();
                        needNew = true;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(dataWashingEntity.SecondAddressDate))
                        {
                            latestUpdateDate = ConvertToDateTime(dataWashingEntity.SecondAddressDate);
                            previousUpdateDate = previousPersonaAddr.MipaDateLastUpdated;

                            timeSpan = latestUpdateDate.Value - previousUpdateDate.Value;

                            if (timeSpan.TotalDays > 0)
                            {
                                needUpdate = true;
                            }
                        }
                        else
                            latestUpdateDate = null;
                    }

                    if (needNew || needUpdate)
                    {
                        previousPersonaAddr.MipIntNo = persona.MipIntNo;
                        previousPersonaAddr.MipaSource = "DataWash";
                        previousPersonaAddr.MiatIntNo = (int)Mi5AddressTypeList.Option_3;
                        previousPersonaAddr.MipaStAdd1 = dataWashingEntity.SecondAddressLine1;
                        previousPersonaAddr.MipaStAdd2 = dataWashingEntity.SecondAddressLine2;
                        previousPersonaAddr.MipaStAdd3 = dataWashingEntity.SecondAddressLine3;
                        previousPersonaAddr.MipaStAdd4 = dataWashingEntity.SecondAddressLine4;
                        previousPersonaAddr.MipaStCode = dataWashingEntity.SecondAddressPostalCode;
                        previousPersonaAddr.MipaStProvinceCode = dataWashingEntity.SecondAddressProvCode;
                        previousPersonaAddr.MipaStCountryCode = dataWashingEntity.SecondAddressCountryCode;
                        previousPersonaAddr.MipaDateLastUpdated = latestUpdateDate ?? DateTime.Now;
                        previousPersonaAddr.MipaStLastUpdated = latestUpdateDate ?? DateTime.Now;
                        previousPersonaAddr.LastUser = _lastUser;
                        previousPersonaAddr = psAddressService.Save(previousPersonaAddr);
                    }
                    //-----------------------------------------------------------------
                    #endregion

                    #region Mi5PersonaAddressPost

                    Mi5PersonaAddressPost latestPersonaPostAddr = null;
                    Mi5PersonaAddressPost previousPersonaPostAddr = null;

                    needNew = needUpdate = false;

                    SIL.AARTO.DAL.Entities.TList<Mi5PersonaAddressPost> addrPostList = ppAddressService.GetByMipIntNo(persona.MipIntNo);

                    //----------------------------------------------------------
                    latestPersonaPostAddr = addrPostList.Where(p => p.MiatIntNo == (int)Mi5AddressTypeList.Option_2).FirstOrDefault();

                    if (!String.IsNullOrEmpty(dataWashingEntity.ThirdAddressDate))
                    {
                        latestUpdateDate = ConvertToDateTime(dataWashingEntity.ThirdAddressDate);
                    }

                    if (latestPersonaPostAddr == null && !String.IsNullOrEmpty(dataWashingEntity.ThirdAddressDate))
                    {
                        needNew = true;
                        latestPersonaPostAddr = new Mi5PersonaAddressPost();
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(dataWashingEntity.ThirdAddressDate))
                        {
                            latestUpdateDate = ConvertToDateTime(dataWashingEntity.ThirdAddressDate);
                            previousUpdateDate = latestPersonaPostAddr.MipaDateLastUpdated;

                            timeSpan = latestUpdateDate.Value - previousUpdateDate.Value;

                            if (timeSpan.TotalDays > 0)
                            {
                                needUpdate = true;
                            }
                        }
                        else
                            latestUpdateDate = null;
                    }

                    if (needNew || needUpdate)
                    {
                        latestPersonaPostAddr.MipIntNo = persona.MipIntNo;
                        latestPersonaPostAddr.MipaSource = "DataWash";
                        latestPersonaPostAddr.MiatIntNo = (int)Mi5AddressTypeList.Option_2;
                        latestPersonaPostAddr.MipaPoAdd1 = dataWashingEntity.ThirdAddressLine1;
                        latestPersonaPostAddr.MipaPoAdd2 = dataWashingEntity.ThirdAddressLine2;
                        latestPersonaPostAddr.MipaPoAdd3 = dataWashingEntity.ThirdAddressLine3;
                        latestPersonaPostAddr.MipaPoAdd4 = dataWashingEntity.ThirdAddressLine4;
                        latestPersonaPostAddr.MipaPoCode = dataWashingEntity.ThirdAddressPostalCode;
                        latestPersonaPostAddr.MipaPoProvinceCode = dataWashingEntity.ThirdAddressProvCode;
                        latestPersonaPostAddr.MipaPoCountryCode = dataWashingEntity.ThirdAddressCountryCode;
                        latestPersonaPostAddr.MipaDateLastUpdated = latestUpdateDate ?? DateTime.Now;
                        latestPersonaPostAddr.MipaPoLastUpdated = latestUpdateDate ?? DateTime.Now;
                        latestPersonaPostAddr.LastUser = _lastUser;
                        latestPersonaPostAddr = ppAddressService.Save(latestPersonaPostAddr);
                    }
                    //------------------------------------------------------

                    needNew = needUpdate = false;

                    previousPersonaPostAddr = addrPostList.Where(p => p.MiatIntNo == (int)Mi5AddressTypeList.Option_3).FirstOrDefault();

                    if (!String.IsNullOrEmpty(dataWashingEntity.FourthAddressDate))
                    {
                        latestUpdateDate = ConvertToDateTime(dataWashingEntity.FourthAddressDate);
                    }

                    if (previousPersonaPostAddr == null && !String.IsNullOrEmpty(dataWashingEntity.FourthAddressDate))
                    {
                        needNew = true;
                        previousPersonaPostAddr = new Mi5PersonaAddressPost();
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(dataWashingEntity.FourthAddressDate))
                        {
                            latestUpdateDate = ConvertToDateTime(dataWashingEntity.FourthAddressDate);
                            previousUpdateDate = previousPersonaPostAddr.MipaDateLastUpdated;

                            timeSpan = latestUpdateDate.Value - previousUpdateDate.Value;

                            if (timeSpan.TotalDays > 0)
                            {
                                needUpdate = true;
                            }
                        }
                        else
                            latestUpdateDate = null;
                    }
                    if (needNew || needUpdate)
                    {
                        previousPersonaPostAddr.MipIntNo = persona.MipIntNo;
                        previousPersonaPostAddr.MipaSource = "DataWash";
                        previousPersonaPostAddr.MiatIntNo = (int)Mi5AddressTypeList.Option_3;
                        previousPersonaPostAddr.MipaPoAdd1 = dataWashingEntity.FourthAddressLine1;
                        previousPersonaPostAddr.MipaPoAdd2 = dataWashingEntity.FourthAddressLine2;
                        previousPersonaPostAddr.MipaPoAdd3 = dataWashingEntity.FourthAddressLine3;
                        previousPersonaPostAddr.MipaPoAdd4 = dataWashingEntity.FourthAddressLine4;
                        previousPersonaPostAddr.MipaPoCode = dataWashingEntity.FourthAddressPostalCode;
                        previousPersonaPostAddr.MipaPoProvinceCode = dataWashingEntity.FourthAddressProvCode;
                        previousPersonaPostAddr.MipaPoCountryCode = dataWashingEntity.FourthAddressCountryCode;
                        previousPersonaPostAddr.MipaDateLastUpdated = latestUpdateDate ?? DateTime.Now;
                        previousPersonaPostAddr.MipaPoLastUpdated = latestUpdateDate ?? DateTime.Now;
                        previousPersonaPostAddr.LastUser = _lastUser;
                        previousPersonaPostAddr = ppAddressService.Save(previousPersonaPostAddr);
                    }
                    //------------------------------------------------------

                    #endregion

                    #endregion

                    #region MI5PersonaEmployer Table
                    Mi5PersonaEmployer employer = null;
                    needNew = needUpdate = false;

                    SIL.AARTO.DAL.Entities.TList<Mi5PersonaEmployer> employerList = employerService.GetByMipIntNo(persona.MipIntNo);

                    employer = employerList.Where(e => e.MipeType == 1).FirstOrDefault();

                    if (!String.IsNullOrEmpty(dataWashingEntity.Emp1Occupation))
                    {
                        latestUpdateDate = ConvertToDateTime(dataWashingEntity.Emp1Occupation);
                    }

                    if (employer == null && !String.IsNullOrEmpty(dataWashingEntity.Employer1))
                    {
                        needNew = true;
                        employer = new Mi5PersonaEmployer();
                    }
                    else
                    {
                        //if (!String.IsNullOrEmpty(dataWashingEntity.Emp1Occupation))
                        // 2014-07-11, Oscar changed, we need to check "dataWashingEntity.Employer" as well.
                        if (!string.IsNullOrEmpty(dataWashingEntity.Employer1)
                            && !String.IsNullOrEmpty(dataWashingEntity.Emp1Occupation))
                        {
                            latestUpdateDate = ConvertToDateTime(dataWashingEntity.Emp1Occupation);
                            previousUpdateDate = employer.MipeLastUpdated ?? Convert.ToDateTime("1900-01-01");

                            timeSpan = latestUpdateDate.Value - previousUpdateDate.Value;

                            if (timeSpan.TotalDays > 0)
                            {
                                needUpdate = true;
                            }
                        }
                        else
                            latestUpdateDate = null;
                    }

                    if (needNew || needUpdate)
                    {
                        employer.MipEmployerName = dataWashingEntity.Employer1;
                        employer.MiatIntNo = null;
                        employer.MipIntNo = persona.MipIntNo;
                        employer.MipeLastUpdated = latestUpdateDate;
                        employer.MipeType = 1;
                        employer.LastUser = _lastUser;
                        employer = employerService.Save(employer);
                    }

                    //--------------------------------------------------------------

                    needNew = needUpdate = false;
                    employer = employerList.Where(e => e.MipeType == 2).FirstOrDefault();

                    if (!String.IsNullOrEmpty(dataWashingEntity.Emp2Occupation))
                    {
                        latestUpdateDate = ConvertToDateTime(dataWashingEntity.Emp2Occupation);
                    }

                    if (employer == null && !String.IsNullOrEmpty(dataWashingEntity.Employer2))
                    {
                        needNew = true;
                        employer = new Mi5PersonaEmployer();
                    }
                    else
                    {
                        //if (!String.IsNullOrEmpty(dataWashingEntity.Emp2Occupation))
                        // 2014-07-11, Oscar changed, we need to check "dataWashingEntity.Employer" as well.
                        if (!string.IsNullOrEmpty(dataWashingEntity.Employer2)
                            && !String.IsNullOrEmpty(dataWashingEntity.Emp2Occupation))
                        {
                            latestUpdateDate = ConvertToDateTime(dataWashingEntity.Emp2Occupation);
                            previousUpdateDate = employer.MipeLastUpdated ?? Convert.ToDateTime("1900-01-01");

                            timeSpan = latestUpdateDate.Value - previousUpdateDate.Value;

                            if (timeSpan.TotalDays > 0)
                            {
                                needUpdate = true;
                            }
                        }
                        else
                            latestUpdateDate = null;
                    }

                    if (needNew || needUpdate)
                    {
                        employer.MipEmployerName = dataWashingEntity.Employer2;
                        employer.MiatIntNo = null;
                        employer.MipIntNo = persona.MipIntNo;
                        employer.MipeLastUpdated = latestUpdateDate;
                        employer.MipeType = 2;
                        employer.LastUser = _lastUser;
                        employer = employerService.Save(employer);
                    }


                    //--------------------------------------------------------------

                    needNew = needUpdate = false;
                    employer = employerList.Where(e => e.MipeType == 3).FirstOrDefault();

                    if (!String.IsNullOrEmpty(dataWashingEntity.Emp3Occupation))
                    {
                        latestUpdateDate = ConvertToDateTime(dataWashingEntity.Emp3Occupation);
                    }

                    if (employer == null && !String.IsNullOrEmpty(dataWashingEntity.Employer3))
                    {
                        needNew = true;
                        employer = new Mi5PersonaEmployer();
                    }
                    else
                    {
                        //if (!String.IsNullOrEmpty(dataWashingEntity.Emp3Occupation))
                        // 2014-07-11, Oscar changed, we need to check "dataWashingEntity.Employer" as well.
                        if (!string.IsNullOrEmpty(dataWashingEntity.Employer3)
                            && !String.IsNullOrEmpty(dataWashingEntity.Emp3Occupation))
                        {
                            latestUpdateDate = ConvertToDateTime(dataWashingEntity.Emp3Occupation);
                            previousUpdateDate = employer.MipeLastUpdated ?? Convert.ToDateTime("1900-01-01");

                            timeSpan = latestUpdateDate.Value - previousUpdateDate.Value;

                            if (timeSpan.TotalDays > 0)
                            {
                                needUpdate = true;
                            }
                        }
                        else
                            latestUpdateDate = null;
                    }

                    if (needNew || needUpdate)
                    {
                        employer.MipEmployerName = dataWashingEntity.Employer3;
                        employer.MiatIntNo = null;
                        employer.MipIntNo = persona.MipIntNo;
                        employer.MipeLastUpdated = latestUpdateDate;
                        employer.MipeType = 3;
                        employer.LastUser = _lastUser;
                        employer = employerService.Save(employer);
                    }

                    #endregion

                    #region MI5PersonaCommunication
                    Mi5PersonaCommunication personaCommunication = null;

                    needNew = needUpdate = false;

                    SIL.AARTO.DAL.Entities.TList<Mi5PersonaCommunication> communicationList = communicationService.GetByMipIntNo(persona.MipIntNo);

                    #region MI5PersonaCommunication 1

                    personaCommunication = communicationList.Where(c => c.MipcType == 1).FirstOrDefault();

                    if (personaCommunication == null)
                    {
                        if (!String.IsNullOrEmpty(dataWashingEntity.WorkTel1Date))
                        {
                            needNew = true;
                            personaCommunication = new Mi5PersonaCommunication();
                        }
                        else if (!String.IsNullOrEmpty(dataWashingEntity.HomeTel1Date))
                        {
                            needNew = true;
                            personaCommunication = new Mi5PersonaCommunication();
                        }
                        else if (!String.IsNullOrEmpty(dataWashingEntity.Cell1Date))
                        {
                            needNew = true;
                            personaCommunication = new Mi5PersonaCommunication();
                        }
                    }

                    if (!String.IsNullOrEmpty(dataWashingEntity.WorkTel1Date))
                    {
                        if (needNew) personaCommunication.MipcWorkLastUpdated = ConvertToDateTime(dataWashingEntity.WorkTel1Date);

                        latestUpdateDate = ConvertToDateTime(dataWashingEntity.WorkTel1Date);
                        previousUpdateDate = personaCommunication.MipcWorkLastUpdated ?? Convert.ToDateTime("1900-01-01");

                        timeSpan = latestUpdateDate.Value - previousUpdateDate.Value;

                        if (timeSpan.TotalDays > 0 || needNew)
                        {
                            needUpdate = true;
                            personaCommunication.MipcWorkAreaCode = dataWashingEntity.WorkTel1Code;
                            personaCommunication.MipcWorkNumber = dataWashingEntity.WorkTel1No;
                        }

                    }
                    if (!String.IsNullOrEmpty(dataWashingEntity.HomeTel1Date))
                    {
                        if (needNew) personaCommunication.MipcHomeLastUpdated = ConvertToDateTime(dataWashingEntity.HomeTel1Date);

                        latestUpdateDate = ConvertToDateTime(dataWashingEntity.HomeTel1Date);
                        previousUpdateDate = personaCommunication.MipcHomeLastUpdated ?? Convert.ToDateTime("1900-01-01");

                        timeSpan = latestUpdateDate.Value - previousUpdateDate.Value;

                        if (timeSpan.TotalDays > 0 || needNew)
                        {
                            needUpdate = true;
                            personaCommunication.MipcHomeAreaCode = dataWashingEntity.HomeTel1Code;
                            personaCommunication.MipcHomeNumber = dataWashingEntity.HomeTel1No;
                        }
                    }
                    if (!String.IsNullOrEmpty(dataWashingEntity.Cell1Date))
                    {
                        if (needNew) personaCommunication.MipcMobileLastUpdated = ConvertToDateTime(dataWashingEntity.Cell1Date);

                        latestUpdateDate = ConvertToDateTime(dataWashingEntity.Cell1Date);
                        previousUpdateDate = personaCommunication.MipcMobileLastUpdated ?? Convert.ToDateTime("1900-01-01");

                        timeSpan = latestUpdateDate.Value - previousUpdateDate.Value;

                        if (timeSpan.TotalDays > 0 || needNew)
                        {
                            needUpdate = true;
                            //personaCommunication.MipcMobileAreaCode = dataWashingEntity.Cell1No;
                            personaCommunication.MipcMobileNumber = dataWashingEntity.Cell1No;

                        }
                    }

                    if (needNew || needUpdate)
                    {
                        personaCommunication.MipIntNo = persona.MipIntNo;
                        personaCommunication.MipcEmailAddress = dataWashingEntity.EMailAdd;
                        personaCommunication.MipcType = 1;
                        personaCommunication.LastUser = _lastUser;

                        if (latestUpdateDate.HasValue)
                            personaCommunication.MipcLastUpdated = latestUpdateDate.Value;
                        personaCommunication = communicationService.Save(personaCommunication);
                    }
                    #endregion

                    //----------------------------------------------------------------

                    #region MI5PersonaCommunication 2
                    needNew = needUpdate = false;

                    personaCommunication = communicationList.Where(c => c.MipcType == 2).FirstOrDefault();

                    if (personaCommunication == null)
                    {
                        if (!String.IsNullOrEmpty(dataWashingEntity.WorkTel2Date))
                        {
                            needNew = true;
                            personaCommunication = new Mi5PersonaCommunication();
                        }
                        else if (!String.IsNullOrEmpty(dataWashingEntity.HomeTel2Date))
                        {
                            needNew = true;
                            personaCommunication = new Mi5PersonaCommunication();

                        }
                        else if (!String.IsNullOrEmpty(dataWashingEntity.Cell2Date))
                        {
                            needNew = true;
                            personaCommunication = new Mi5PersonaCommunication();
                        }
                    }

                    if (!String.IsNullOrEmpty(dataWashingEntity.WorkTel2Date))
                    {
                        if (needNew) personaCommunication.MipcWorkLastUpdated = ConvertToDateTime(dataWashingEntity.WorkTel2Date);

                        latestUpdateDate = ConvertToDateTime(dataWashingEntity.WorkTel2Date);
                        previousUpdateDate = personaCommunication.MipcWorkLastUpdated ?? Convert.ToDateTime("1900-01-01");

                        timeSpan = latestUpdateDate.Value - previousUpdateDate.Value;

                        if (timeSpan.TotalDays > 0 || needNew)
                        {
                            needUpdate = true;
                            personaCommunication.MipcWorkAreaCode = dataWashingEntity.WorkTel2Code;
                            personaCommunication.MipcWorkNumber = dataWashingEntity.WorkTel2No;
                        }

                    }
                    if (!String.IsNullOrEmpty(dataWashingEntity.HomeTel2Date))
                    {
                        if (needNew) personaCommunication.MipcHomeLastUpdated = ConvertToDateTime(dataWashingEntity.HomeTel2Date);

                        latestUpdateDate = ConvertToDateTime(dataWashingEntity.HomeTel2Date);
                        previousUpdateDate = personaCommunication.MipcHomeLastUpdated ?? Convert.ToDateTime("1900-01-01");

                        timeSpan = latestUpdateDate.Value - previousUpdateDate.Value;

                        if (timeSpan.TotalDays > 0 || needNew)
                        {
                            needUpdate = true;
                            personaCommunication.MipcHomeAreaCode = dataWashingEntity.HomeTel2Code;
                            personaCommunication.MipcHomeNumber = dataWashingEntity.HomeTel2No;
                        }
                    }
                    if (!String.IsNullOrEmpty(dataWashingEntity.Cell2Date))
                    {
                        if (needNew) personaCommunication.MipcMobileLastUpdated = ConvertToDateTime(dataWashingEntity.Cell2Date);

                        latestUpdateDate = ConvertToDateTime(dataWashingEntity.Cell2Date);
                        previousUpdateDate = personaCommunication.MipcMobileLastUpdated ?? Convert.ToDateTime("1900-01-01");

                        timeSpan = latestUpdateDate.Value - previousUpdateDate.Value;

                        if (timeSpan.TotalDays > 0 || needNew)
                        {
                            needUpdate = true;
                            //personaCommunication.MipcMobileAreaCode = dataWashingEntity.Cell2No;
                            personaCommunication.MipcMobileNumber = dataWashingEntity.Cell2No;

                        }
                    }


                    if (needNew || needUpdate)
                    {
                        personaCommunication.MipIntNo = persona.MipIntNo;
                        personaCommunication.MipcEmailAddress = dataWashingEntity.EMailAdd;
                        personaCommunication.MipcType = 2;
                        personaCommunication.LastUser = _lastUser;

                        if (latestUpdateDate.HasValue)
                            personaCommunication.MipcLastUpdated = latestUpdateDate.Value;

                        personaCommunication = communicationService.Save(personaCommunication);
                    }
                    #endregion

                    //----------------------------------------------------------------

                    #region MI5PersonaCommunication 3
                    needNew = needUpdate = false;

                    personaCommunication = communicationList.Where(c => c.MipcType == 3).FirstOrDefault();

                    if (personaCommunication == null)
                    {
                        if (!String.IsNullOrEmpty(dataWashingEntity.WorkTel3Date))
                        {
                            needNew = true;
                            personaCommunication = new Mi5PersonaCommunication();
                        }
                        else if (!String.IsNullOrEmpty(dataWashingEntity.HomeTel3Date))
                        {
                            needNew = true;
                            personaCommunication = new Mi5PersonaCommunication();

                        }
                        else if (!String.IsNullOrEmpty(dataWashingEntity.Cell3Date))
                        {
                            needNew = true;
                            personaCommunication = new Mi5PersonaCommunication();
                        }
                    }

                    if (!String.IsNullOrEmpty(dataWashingEntity.WorkTel3Date))
                    {
                        if (needNew) personaCommunication.MipcWorkLastUpdated = ConvertToDateTime(dataWashingEntity.WorkTel3Date);

                        latestUpdateDate = ConvertToDateTime(dataWashingEntity.WorkTel3Date);
                        previousUpdateDate = personaCommunication.MipcWorkLastUpdated ?? Convert.ToDateTime("1900-01-01");

                        timeSpan = latestUpdateDate.Value - previousUpdateDate.Value;

                        if (timeSpan.TotalDays > 0 || needNew)
                        {
                            needUpdate = true;
                            personaCommunication.MipcWorkAreaCode = dataWashingEntity.WorkTel3Code;
                            personaCommunication.MipcWorkNumber = dataWashingEntity.WorkTel3No;
                        }

                    }
                    if (!String.IsNullOrEmpty(dataWashingEntity.HomeTel3Date))
                    {
                        if (needNew) personaCommunication.MipcHomeLastUpdated = ConvertToDateTime(dataWashingEntity.HomeTel3Date);

                        latestUpdateDate = ConvertToDateTime(dataWashingEntity.HomeTel3Date);
                        previousUpdateDate = personaCommunication.MipcHomeLastUpdated ?? Convert.ToDateTime("1900-01-01");

                        timeSpan = latestUpdateDate.Value - previousUpdateDate.Value;

                        if (timeSpan.TotalDays > 0 || needNew)
                        {
                            needUpdate = true;
                            personaCommunication.MipcHomeAreaCode = dataWashingEntity.HomeTel3Code;
                            personaCommunication.MipcHomeNumber = dataWashingEntity.HomeTel3No;
                        }
                    }
                    if (!String.IsNullOrEmpty(dataWashingEntity.Cell3Date))
                    {
                        if (needNew) personaCommunication.MipcMobileLastUpdated = ConvertToDateTime(dataWashingEntity.Cell3Date);

                        latestUpdateDate = ConvertToDateTime(dataWashingEntity.Cell3Date);
                        previousUpdateDate = personaCommunication.MipcMobileLastUpdated ?? Convert.ToDateTime("1900-01-01");

                        timeSpan = latestUpdateDate.Value - previousUpdateDate.Value;

                        if (timeSpan.TotalDays > 0 || needNew)
                        {
                            needUpdate = true;
                            //personaCommunication.MipcMobileAreaCode = dataWashingEntity.Cell3No;
                            personaCommunication.MipcMobileNumber = dataWashingEntity.Cell3No;

                        }
                    }

                    if (needNew || needUpdate)
                    {
                        personaCommunication.MipIntNo = persona.MipIntNo;
                        personaCommunication.MipcEmailAddress = dataWashingEntity.EMailAdd;
                        personaCommunication.MipcType = 3;
                        personaCommunication.LastUser = _lastUser;

                        if (latestUpdateDate.HasValue)
                            personaCommunication.MipcLastUpdated = latestUpdateDate.Value;

                        personaCommunication = communicationService.Save(personaCommunication);
                    }
                    #endregion

                    #endregion

                    // 2014-07-28, Oscar added, for status 601
                    PushQueuesForGenerateSummons(dataWashingEntity.IdNumber);

                    //ConnectionScope.Complete();
                    // 2014-07-29, Oscar changed
                    scope.Complete();

                    imported = true;
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                throw;
            }

            return imported;
        }

        //void InitializeAartoConnectionStringForNetTier(string dynamicConnectionStr)
        //{
        //    SqlNetTiersProvider provider = new SqlNetTiersProvider();
        //    NameValueCollection collection = new NameValueCollection();
        //    collection.Add("UseStoredProcedure", "false");
        //    collection.Add("EnableEntityTracking", "true");
        //    collection.Add("EntityCreationalFactoryType", "SIL.AARTO.DAL.Entities.EntityFactory");
        //    collection.Add("EnableMethodAuthorization", "false");
        //    collection.Add("ConnectionString", dynamicConnectionStr);
        //    collection.Add("ConnectionStringName", "SIL.AARTO.DAL.Data.ConnectionString");
        //    collection.Add("ProviderInvariantName", "System.Data.SqlClient");

        //    provider.Initialize("SqlNetTiersProvider", collection);

        //    DataRepository.LoadProvider(provider, true);

        //    //DataRepository.AddConnection("SIL.AARTO.DAL.Data.ConnectionString", dynamicConnectionStr);

        //}

        DateTime ConvertToDateTime(string input)
        {
            try
            {
                if (input.IndexOf("/") > 0 || input.IndexOf("-") > 0)
                {
                    return Convert.ToDateTime(input);
                }
                StringBuilder sbDate = new StringBuilder();
                if (!String.IsNullOrEmpty(input))
                {
                    sbDate.Append(input.Substring(0, 4)).Append("-").Append(input.Substring(4, 2)).Append("-").Append(input.Substring(6, 2));

                }

                return Convert.ToDateTime(sbDate.ToString());
            }
            catch (Exception ex)
            {
                //throw ex;
                // 2014-07-11, Oscar changed exception.
                throw new Exception(ResourceHelper.GetResource("InvalidDateFormatString", input));
            }
        }

        #region 2014-07-28, Oscar added, for status 601

        void PushQueuesForGenerateSummons(string idNumber)
        {
            if (string.IsNullOrWhiteSpace(idNumber)) return;

            var noticeList = GetReadyForSummonsNotIntNoListByIdNumber(idNumber);

            noticeList.ForEach(CheckAndPushGenerateSummonsQueue);
        }

        List<int> GetReadyForSummonsNotIntNoListByIdNumber(string idNumber)
        {
            var result = new List<int>();
            var paras = new[]
            {
                new SqlParameter("@IdNumber", idNumber),
                new SqlParameter("@Status", 601)
            };
            using (var conn = new SqlConnection(this.artoConnectionString))
            {
                using (var cmd = new SqlCommand("GetReadyForSummonsNotIntNoListByIdNumber", conn)
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = 0
                })
                {
                    cmd.Parameters.AddRange(paras);
                    conn.Open();
                    using (var dr = cmd.ExecuteReader())
                        while (dr.Read())
                            result.Add((int)dr["NotIntNo"]);
                }
            }
            return result;
        }

        void CheckAndPushGenerateSummonsQueue(int notIntNo)
        {
            var actionDate = DateTime.Now.AddHours(2);
            var queues = queueService.GetExistingQueues((int)ServiceQueueTypeList.GenerateSummons, notIntNo.ToString(CultureInfo.InvariantCulture), null, actionDate);

            if (queues.Count <= 0)
            {
                var notice = noticeService.GetByNotIntNo(notIntNo);
                var authority = authorityService.GetByAutIntNo(notice.AutIntNo);
                var group = string.Format("{0}|{1}|{2}",
                    authority.AutCode.Trim(),
                    notice.NotFilmType.Equals2("H") && !notice.IsVideo ? "H" : "",
                    (notice.NotCourtName ?? "").Trim());

                queueProcessor.Send(new QueueItem
                {
                    Body = notIntNo,
                    Group = group,
                    ActDate = actionDate,
                    Priority = (notice.NotOffenceDate - DateTime.Now).Days,
                    QueueType = ServiceQueueTypeList.GenerateSummons,
                    LastUser = aartoBase.LastUser
                });
            }
        }

        #endregion

    }
}
