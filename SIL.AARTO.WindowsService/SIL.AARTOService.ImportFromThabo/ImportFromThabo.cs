﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.ImportFromThabo
{
    partial class ImportFromThabo : ServiceHost
    {
        public ImportFromThabo()
            : base("", new Guid("3CC506FE-D082-4B03-91FD-87C517A02A43"), new ImportFromThaboService())
        {
            InitializeComponent();
        }
    }
}
