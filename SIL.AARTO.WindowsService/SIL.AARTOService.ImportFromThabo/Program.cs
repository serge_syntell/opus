﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.ImportFromThabo
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.ImportFromThabo"
                ,
                DisplayName = "SIL.AARTOService.ImportFromThabo"
                ,
                Description = "SIL.AARTOService.ImportFromThabo"
            };

            ProgramRun.InitializeService(new ImportFromThabo(), serviceDescriptor, args);
        }
    }
}
