﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;
using SIL.AARTOService.Library;
using SIL.ServiceQueueLibrary.DAL.Entities;
using Stalberg.TMS;
using Stalberg.Indaba;
using SIL.QueueLibrary;
using Stalberg.ThaboAggregator.Objects;
using SIL.AARTOService.Resource;
using System.IO;
using Stalberg;
using Stalberg.TMS_TPExInt.Objects;
using SIL.ServiceBase;
using System.Globalization;

namespace SIL.AARTOService.ImportFromThabo
{
    public class ImportFromThaboService : ClientService
    {
        public ImportFromThaboService()
            : base("", "", new Guid("C4F9FBC4-5603-4988-B86A-327A25D41C62"))
        {
            this._serviceHelper = new AARTOServiceBase(this);
            aartoConnectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            indabaConnectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.Indaba, ServiceConnectionTypeList.DB);
            //exportPath = AARTOBase.GetConnectionString(ServiceConnectionNameList.ExportToThaboExportPath, ServiceConnectionTypeList.UNC);

            AARTOBase.OnServiceStarting = () =>
            {
                InitIndaba();
                easyPayDB = new EasyPayDB(aartoConnectStr);
                //SetServiceParameters();

                emailToAdministrator = AARTOBase.GetServiceParameter("EmailToAdministrator");
                if (string.IsNullOrEmpty(emailToAdministrator))
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoEmailAddress"), "EmailToAdministrator"), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }
            };

            AARTOBase.OnServiceSleeping = () =>
            {
                DisposeIndaba();
            };
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        string aartoConnectStr = string.Empty;
        string indabaConnectStr = string.Empty;
        string _machineName = string.Empty;
        //string exportPath = string.Empty;
        string emailToAdministrator = string.Empty;

        ICommunicate indaba;
        EasyPayDB easyPayDB;
        ResponseFile responseFile;

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            ImportPaymentFeedback();
            ProcessResponseFile();

            //Jerry 2014-05-23 add
            this.MustSleep = true;
        }

        public void ImportPaymentFeedback()
        {
            try
            {
                // Get any feedback files to process
                List<FineExchangeFeedbackFile> files = this.ProcessFeedBackFiles();
                if (files.Count == 0)
                    return;

                // Update the database
                StringBuilder sb = new StringBuilder();
                foreach (FineExchangeFeedbackFile file in files)
                {
                    this.easyPayDB.GetAuthorityID(file);

                    ProcessPayments(file, ref sb, AARTOBase.LastUser);
                }

                if (sb.Length > 0)
                    Logger.Info(sb.ToString());
            }
            catch (Exception ex)
            {
                AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ErrImportingPaymentFeedback"), ex.Message, ex.StackTrace), true);
            }
        }

        private List<FineExchangeFeedbackFile> ProcessFeedBackFiles()
        {
            List<FineExchangeFeedbackFile> feedbackFiles = new List<FineExchangeFeedbackFile>();

            IndabaFileInfo[] files = this.indaba.GetFileInformation(FineExchangeFeedbackFile.EXTENSION);
            if (files.Length == 0)
                AARTOBase.LogProcessing(ResourceHelper.GetResource("InfoFinishedTransaction"), LogType.Info);

             //------DON'T FORGET TO RECOMMENT THIS CODE!!!!!

            ////temporary code here - uncomment this section if you want to import a single file during testing
            //FileStream tempFile = new FileStream("C:\\Temp\\Thabo\\Feedback\\FineExchangeFeedbackFine_ST_2008-11-13_17-11-07-0161.feff", FileMode.Open);
            ////temporary code to load from file system
            //StreamReader sr = new StreamReader(tempFile);
            //Stream stream = sr.BaseStream;
            //// Process the file data into a feedback object
            //FineExchangeFeedbackFile feedbackFile = new FineExchangeFeedbackFile(stream);
            //feedbackFiles.Add(feedbackFile);
            ////temorary code ends here

            ////------DON'T FORGET TO UNCOMMENT THIS CODE!!!!!

            //comment out this section when loading a single file during testing
            foreach (IndabaFileInfo file in files)
            {
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ProcessingFeedbackFile"), file.Name, file.Source), LogType.Info);

                try
                {
                    // Get the file data from Indaba
                    Stream stream = this.indaba.Peek(file);

                    // Process the file data into a feedback object
                    FineExchangeFeedbackFile feedbackFile = new FineExchangeFeedbackFile(stream);
                    feedbackFiles.Add(feedbackFile);

                    // Dequeue the file
                    this.indaba.Dequeue(file);
                }
                catch (Exception ex)
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrProcessingFeedbackFiles"), file.Name, ex.Message, ex.StackTrace), LogType.Error, ServiceOption.BreakAndStop);
                }
            }
            //end of comment out section for testing

            return feedbackFiles;
        }

        /// <summary>
        /// Processes feedback the payments.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="sb">The sb.</param>
        /// <param name="applicationName">Name of the application.</param>
        private void ProcessPayments(FineExchangeFeedbackFile file, ref StringBuilder sb, string applicationName)
        {
            FeedbackAuthority authority = (FeedbackAuthority)file.Authority;

            //SqlCommand com = new SqlCommand();
            //com.Connection = this.con;

            RemotePaymentDB db = new RemotePaymentDB(this.aartoConnectStr);

            // Process EasyPay payments
            //TODO remove the Easypay section at some point - everything seems to be in the Payfine section
            //com.CommandType = CommandType.StoredProcedure;

            // Declare temp variables
            CashType cashType = CashType.EasyPay;
            decimal originalAmount = 0M;

            foreach (EasyPayTransaction payment in authority.EasyPayPayments)
            {
                // Check if the payment has already been recorded
                string strPaymentExists = db.CheckIfPaymentExists(payment.NoticeID);
                if (strPaymentExists == "F")
                {
                    sb.Append(string.Format(ResourceHelper.GetResource("NoticeDoesNotExist"), payment.NoticeID));
                    continue;
                }
                if (strPaymentExists == "N")
                {
                    sb.Append(string.Format(ResourceHelper.GetResource("NoticeHasBeenPaid"), payment.NoticeID));
                    continue;
                }
                else if (strPaymentExists == "D")
                {
                    sb.Append(string.Format(ResourceHelper.GetResource("NoticeDuplicatelyPaid"), payment.NoticeID));
                    continue;
                }

                // Get original payment amount
                originalAmount = db.GetOriginalFineAmount(string.Empty, payment.NoticeID);
                if (originalAmount != payment.Amount)
                {
                    throw new NotImplementedException(ResourceHelper.GetResource("EasyPayPaymentImportHasNotBeenImplemented"));

                    sb.Append(string.Format(ResourceHelper.GetResource("TicketHasDifferentFineAmount"),
                        payment.NoticeID, originalAmount, cashType, payment.Amount));

                    try
                    {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.Append(string.Format(ResourceHelper.GetResource("PaymentMismatch"), applicationName));
                            sb2.Append(ResourceHelper.GetResource("ProcessEasyPayNumber"));
                            sb2.Append(payment.EasyPayNumber);
                            sb2.Append(ResourceHelper.GetResource("AmountInOpus"));
                            //update by Rachel 20140812 for 5337
                            //sb2.Append(originalAmount.ToString("N"));
                            sb2.Append(originalAmount.ToString("N",CultureInfo.InvariantCulture));
                            //end update by Rachel 20140812 for 5337
                            sb2.Append(ResourceHelper.GetResource("AmountPaid"));
                            sb2.Append(cashType.ToString());
                            sb2.Append(ResourceHelper.GetResource("AmountPaidValue"));
                            //update by Rachel 20140812 for 5337
                            //sb2.Append(payment.Amount.ToString("N"));
                            sb2.Append(payment.Amount.ToString("N",CultureInfo.InvariantCulture));
                            //end update by Rachel 20140812 for 5337
                            sb2.Append(ResourceHelper.GetResource("ProblemNeedsToBeRectified"));
                            sb2.Append(applicationName);
                            
                            SendEmailManager.SendEmail(emailToAdministrator, string.Format(ResourceHelper.GetResource("PaymentDiscrepancy"), cashType), sb2.ToString());
                    }
                    catch (Exception ex)
                    {
                        //System.Diagnostics.Debug.WriteLine(ex);
                        AARTOBase.LogProcessing(ex.Message, LogType.Error);
                    }
                }

                // TODO: Import and Receipt EasyPay payments
            }

            //dls 090701 - moved the udpate of the SumComments to the SP SummonsChargeUpdatePayment which is called from ProcessThaboPayment
            // 20090531 tf Update column SumComments in Summons
            //SummonsDB summonsDb = new SummonsDB(this.connectionString);

            // Process PayFine payments
            foreach (FeedbackPayment payment in authority.Payments)
            {
                // Check if the payment has already been recorded
                string strPaymentExists = db.CheckIfPaymentExists(payment.TicketNo);
                if (strPaymentExists == "F")
                {
                    sb.Append(string.Format(ResourceHelper.GetResource("TicketDoesNotExist"), payment.TicketNo));
                    continue;
                }
                if (strPaymentExists == "N")
                {
                    sb.Append(string.Format(ResourceHelper.GetResource("TicketHasBeenPaid"), payment.TicketNo));
                    continue;
                }
                if (strPaymentExists == "D")
                {
                    sb.Append(string.Format(ResourceHelper.GetResource("TicketHasBeenPaidDuplicate"), payment.TicketNo));
                    //Notice number = AutCode eg. “ST” + sequence number. 
                    payment.TicketNo = authority.AuthorityCode + payment.TicketNo.Substring(2, 6);

                    this.easyPayDB.ProcessRejectedPayment(payment, authority, ref sb, applicationName);
                    continue;
                }

                // mrs 20081003 3970 we have a new payment type to handle (PEN) caused by a remote payment being received but not able to process
                if (payment.Type == "PEN")
                {
                    //process remote payment pending
                    this.easyPayDB.ProcessPaymentPending(payment, authority, applicationName, ref sb);
                    sb.Append(string.Format(ResourceHelper.GetResource("TicketPaymentPending"), payment.TicketNo, payment.Amount));
                    continue;
                }

                // Get current fine amount in the system
                originalAmount = db.GetOriginalFineAmount(payment.TicketNo, 0);

                // Only check for mismatched payments if this is a normal payment
                if (payment.Action == ActionPayment.Normal && originalAmount != payment.Amount)
                {
                    sb.Append(string.Format(ResourceHelper.GetResource("TicketHasDifferentFineAmount"),
                        payment.TicketNo, originalAmount, cashType, payment.Amount));

                    try
                    {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.Append(string.Format(ResourceHelper.GetResource("PaymentMismatch"), applicationName));
                            sb2.Append(ResourceHelper.GetResource("ProcessTicketNumber"));
                            sb2.Append(payment.TicketNo);
                            sb2.Append(ResourceHelper.GetResource("AmountInOpus"));
                            //update by Rachel 20140812 for 5337
                            //sb2.Append(originalAmount.ToString("N"));
                            sb2.Append(originalAmount.ToString("N", CultureInfo.InvariantCulture));
                            //end update by Rachel 20140812 for 5337
                            sb2.Append(ResourceHelper.GetResource("AmountPaid"));
                            sb2.Append(cashType.ToString());
                            sb2.Append(ResourceHelper.GetResource("AmountPaidValue"));
                            //update by Rachel 20140812 for 5337
                            //sb2.Append(payment.Amount.ToString("N"));
                            sb2.Append(payment.Amount.ToString("N",CultureInfo.InvariantCulture));
                            //end update by Rachel 20140812 for 5337
                            sb2.Append(ResourceHelper.GetResource("ProblemNeedsToBeRectified"));
                            sb2.Append(applicationName);

                            SendEmailManager.SendEmail(emailToAdministrator, string.Format(ResourceHelper.GetResource("PaymentDiscrepancy"), cashType), sb2.ToString());

                            continue;
                    }
                    catch (Exception ex)
                    {
                        //System.Diagnostics.Debug.WriteLine(ex);
                        AARTOBase.LogProcessing(ex.Message, LogType.Error);
                        sb.Append(ex);
                    }
                }

                // Check the payment action and process
                switch (payment.Action)
                {
                    case ActionPayment.Normal:
                        easyPayDB.ProcessNormalPayment(payment, authority, applicationName, ref sb);
                        break;

                    case ActionPayment.Rejected:
                        easyPayDB.ProcessRejectedPayment(payment, authority, ref sb, applicationName);
                        break;

                    case ActionPayment.Representation:
                        //dls 081113 - if the payment has already been rep'd at the LA, but there was a delay in the reduced amount getting sent to Thabo
                        //             and the incoming rep amount = the current amount for the notice, process it as a normal payment and don't do a rep

                        if (originalAmount != payment.Amount)
                            this.easyPayDB.ProcessRepresentationPayment(payment, authority, applicationName, originalAmount, ref sb);
                        else
                            this.easyPayDB.ProcessNormalPayment(payment, authority, applicationName, ref sb);

                        break;

                    case ActionPayment.Hybrid:
                        this.easyPayDB.ProcessHybridPayment(payment, authority, applicationName, originalAmount, ref sb);
                        break;
                }

                //dls 090701 - moved the udpate of the SumComments to the SP SummonsChargeUpdatePayment which is called from ProcessThaboPayment
                //summonsDb.UpdateSummonsComments(payment.TicketNo, "Case finalized – payment of R rrrr.cc was received on yyyy/mm/dd HH:MM (receipt # 999999)", applicationName);
            }
        }

        public void ProcessResponseFile()
        {
            IndabaFileInfo[] responsefiles = this.indaba.GetFileInformation(ResponseFile.RESPONSE_FILE_EXTENSION);

            if (responsefiles == null || responsefiles.Length == 0)
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("NoResponseFile"), LogType.Info);
                return;
            }

            foreach (IndabaFileInfo file in responsefiles)
            {
                try
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("RetrievingData"), file.Name), LogType.Info);

                    Stream stream = this.indaba.Peek(file);
                    responseFile = ResponseFile.ReadDataFiles(stream, file.Name);

                    if (responseFile == null)
                    {
                        AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoDataForFile"), file.Name), LogType.Info);
                        continue;
                    }

                    // Update the TMS database with the responseFile data
                    List<string> failedTickets = new List<string>();
                    ProceessResponseFileData(aartoConnectStr, ref failedTickets, AARTOBase.LastUser);

                    if (failedTickets.Count == 0)
                    {
                        this.indaba.Dequeue(file);
                    }
                    else
                    {
                        AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("UpdateDatabaseHasProblem"), responseFile.Authority), LogType.Info);
                        continue;
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrProcessingResponseFile"), file.Name, ex.Message, ex.StackTrace), LogType.Error);
                    continue;
                }
            }

        }

        public void ProceessResponseFileData(string strConnectionStr, ref List<string> failedTickets, string strLastUser)
        {
            try
            {
                //Check response data
                string strMsg = responseFile.Check(strConnectionStr);
                if (strMsg.Length > 0)
                {
                    SendEmailManager.SendEmail(emailToAdministrator, string.Format(ResourceHelper.GetResource("ResponseFileProcessdFailed"), responseFile.fileName, responseFile.authority.AuthorityCode), strMsg);
                    throw new Exception(strMsg);
                }

                responseFile.UpdateWithResponseData(aartoConnectStr, ref failedTickets, strLastUser);

                if (failedTickets.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (string str in failedTickets)
                    {
                        sb.Append(string.Format("Tickert Number {0} ", str));
                        sb.Append(Environment.NewLine);
                    }

                    SendEmailManager.SendEmail(emailToAdministrator, string.Format(ResourceHelper.GetResource("ResponseFileProcessdFailed"), responseFile.fileName, responseFile.authority.AuthorityCode), strMsg);
                }
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex.Message, LogType.Error);
                //throw ex;
            }
        }

        public void InitIndaba()
        {
            Client.ApplicationName = "SIL.AARTOService.ImportFromThabo";
            Client.ConnectionString = this.indabaConnectStr;
            Client.Initialise();
            indaba = Client.Create();
            indaba.Source = "Thabo";
        }

        public void DisposeIndaba()
        {
            indaba.Dispose();
            Client.Dispose();
        }
        
    }
}
