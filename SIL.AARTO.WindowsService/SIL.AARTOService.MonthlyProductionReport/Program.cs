﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.DailyProductionReport
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.MonthlyProductionReport",
                Description = "SIL.AARTOService.MonthlyProductionReport",
                DisplayName = "SIL.AARTOService.MonthlyProductionReport"
            };
            ProgramRun.InitializeService(new MonthlyProductionReport(), serviceDescriptor, args);
        }
    }
}
