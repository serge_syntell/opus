﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.DailyProductionReport
{
    public partial class MonthlyProductionReport : ServiceHost
    {
        public MonthlyProductionReport()
            : base("SIL.AARTOService.MonthlyProductionReport", new Guid("225D696F-837C-4323-AE91-612A1A07C81E"), new MonthlyProductionReportClientService())
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
        }

        protected override void OnStop()
        {
            base.OnStop();
        }
    }
}
