﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTOService.Library;
using SIL.ServiceLibrary;
using System.Data.SqlClient;
using System.Data;
using SIL.AARTOService.DAL;

namespace SIL.AARTOService.DailyProductionReport
{
    //enum ProductionReportOffenceTypeList
    //{
    //    Camera = 1,
    //    Railway = 2,
    //    Sec341 = 3,
    //    Sec56 = 4,
    //    Video = 5,
    //    BusLane = 6
    //}

    public class MonthlyProductionReportClientService : ClientService
    {
        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        string aartoConnectStr = string.Empty;

        //int generateReportOfMaxPreviousDays = 10;

        Dictionary<int, string> productionReportTypeList = new Dictionary<int, string>();

        public MonthlyProductionReportClientService() :
            base("SIL.AARTOService.MonthlyProductionReport", "Generate daily production report data", new Guid("85CF95CD-F64C-46FD-B5BB-D3C6A2B971C8"))
        {
            this._serviceHelper = new AARTOServiceBase(this, false);

            AARTOBase.OnServiceStarting = () =>
            {
                try
                {
                    aartoConnectStr = AARTOBase.GetConnectionString(ServiceQueueLibrary.DAL.Entities.ServiceConnectionNameList.AARTO, ServiceQueueLibrary.DAL.Entities.ServiceConnectionTypeList.DB);

                    productionReportTypeList = GetProductionReportType();
                }
                catch (Exception e)
                {
                    AARTOBase.LogProcessing(e, LogType.Error, ServiceBase.ServiceOption.BreakAndStop);
                }
            };
        }

        public override void MainWork(ref List<QueueLibrary.QueueItem> queueList)
        {
            try
            {
                DateTime earliestOffenceDate = GetEarliestOffenceDate();

                AARTOBase.LogProcessing(
                        String.Format("Beginning process production report"), LogType.Info);

                foreach (int repType in this.productionReportTypeList.Keys)
                {
                    ProcessReport(repType, earliestOffenceDate);
                }

                AARTOBase.LogProcessing(
                        String.Format("Generate production report process finished!"), LogType.Info);
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex, LogType.Error, ServiceBase.ServiceOption.BreakAndStop);
            }
            finally
            {
                this.MustStop = true;
            }
        }

        void ProcessReport(int repType, DateTime startOffenceDate)
        {
            DateTime date = Convert.ToDateTime(startOffenceDate.ToString("yyyy-MM-01"));
            DateTime currentDate = DateTime.Now.Date;
            bool succeed = false;
            do
            {

                AARTOBase.LogProcessing(
                        String.Format("Starting generate daily production report, report type : {0} , year : {1} , month : {2}",
                        productionReportTypeList[repType],
                        date.Year, date.Month), LogType.Info);

                succeed = GenerateDailyProductionReport(repType, date.Year, date.Month, AARTOBase.LastUser);

                if (succeed)
                {
                    AARTOBase.LogProcessing(
                       String.Format("Generate daily production report, report type : {0} , year : {1} , month : {2} succeed!",
                       productionReportTypeList[repType],
                       date.Year, date.Month), LogType.Info);
                }
                else
                {
                    AARTOBase.LogProcessing(
                          String.Format("Generate daily production report, report type : {0} ,  year : {1} , month : {2} failed!",
                          productionReportTypeList[repType],
                          date.Year, date.Month), LogType.Info);
                }

                date = date.AddMonths(1);

            } while (date <= currentDate);


        }

        DateTime GetEarliestOffenceDate()
        {

            DateTime offenceDate = DateTime.Now.Date;
            //SILCustom_DailyProductionReport_GetLastGenerateDateByRepType
            using (SqlConnection conn = new SqlConnection(this.aartoConnectStr))
            {

                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SILCustom_Notice_GetEarliestOffenceDate";
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }
                    object obj = cmd.ExecuteScalar();
                    offenceDate = obj == null ? DateTime.Now.Date : (DateTime)obj;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
            return offenceDate;
        }

        bool GenerateDailyProductionReport(int repType, int year, int month, string lastUser)
        {
            bool flag = false;
            using (SqlConnection conn = new SqlConnection(this.aartoConnectStr))
            {
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "MonthlyProductionReportUpdate";
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter[] paras = new SqlParameter[] { 
                    new SqlParameter("@MPRYear", SqlDbType.SmallInt),
                    new SqlParameter("@MPRMonth", SqlDbType.TinyInt),
                    new SqlParameter("@PROTIntNo", SqlDbType.TinyInt),
                    new SqlParameter("@LastUser", SqlDbType.NVarChar, 50)
                };
                paras[0].Value = year;
                paras[1].Value = month;
                paras[2].Value = repType;
                paras[3].Value = lastUser;

                cmd.Parameters.AddRange(paras);

                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }
                    object obj = cmd.ExecuteScalar();

                    flag = (int)obj == 1;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }

            return flag;
        }

        Dictionary<int, string> GetProductionReportType()
        {
            Dictionary<int, string> returnList = new Dictionary<int, string>();
            using (SqlConnection conn = new SqlConnection(this.aartoConnectStr))
            {

                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SILCustom_ProductionReportOffenceType_GetAll";
                cmd.CommandType = CommandType.StoredProcedure;
                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }
                    using (IDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            returnList.Add(Convert.ToInt32(reader.GetValue(0)), reader.GetString(1));
                        }
                        reader.Close();
                    }

                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }

            return returnList;
        }
    }



}
