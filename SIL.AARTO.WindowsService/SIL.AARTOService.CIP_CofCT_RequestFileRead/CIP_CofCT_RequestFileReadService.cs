﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using System.IO;
using Stalberg.TMS;
using SIL.AARTOService.Resource;
using System.Data;
using System.Transactions;
using System.Configuration;
using System.Data.SqlClient;
using Stalberg.TMS_TPExInt.Components;
using Stalberg.TMS.Data;
using System.Text.RegularExpressions;
using SIL.AARTOService.DAL;

namespace SIL.AARTOService.CIP_CofCT_RequestFileRead
{
    public class CIP_CofCT_RequestFileReadService : ServiceDataProcessViaFileSystem
    {
        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        string connAARTODB, connCIP_CofCTUNC;

        private const string REQUEST_FILE_PREFIX_CC = "cippp";   //090923 FT add REQUEST
        private const int PRINTFILE_NO_OF_NOTICES = 100;
        private const string FILE_EXT = ".txt";

        private TMSErrorLogDetails tmsErrorLog = new TMSErrorLogDetails();
        private TMSErrorLogDB errorLog = null;

        List<int> pfnList = new List<int>();

        public CIP_CofCT_RequestFileReadService()
            : base("", "", new Guid("86207B0C-1A67-4500-8021-9FF8A6FE687A"))
        {
            this._serviceHelper = new AARTOServiceBase(this, false);
            this.Recursion = false;
            // 2012-02-21 jerry change
            this.connCIP_CofCTUNC = AARTOBase.GetConnectionString(ServiceConnectionNameList.CiprusCofCT_LoadFiles, ServiceConnectionTypeList.UNC);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);

            AARTOBase.OnServiceStarting = () =>
            {
                this.pfnList.Clear();
                AARTOBase.CheckFolder(this.connCIP_CofCTUNC);
                SetServiceParameters();
            };
        }

        public override void PrepareWork()
        {
            //this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            //this.connCIP_CofCTUNC = AARTOBase.GetConnectionString(ServiceConnectionNameList.CiprusCofCT_LoadFiles, ServiceConnectionTypeList.UNC);
            //Jerry 2012-04-18 change
            //connRequestFolder = Path.Combine(ServiceParameters["CiprusCofCTExportFolder"], "RequestFiles");
            this.DestinationPath = connCIP_CofCTUNC;

            this.GetDataFilesFromTP_CofCT(connCIP_CofCTUNC);
            errorLog = new TMSErrorLogDB(connAARTODB);
        }

        //Jerry 2012-04-20 add
        public override void InitialWork(ref QueueItem item)
        {
            if (item.Body != null && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    FileInfo fileInfo = (FileInfo)item.Body;
                    string file = fileInfo.FullName;

                    if (Path.GetFileName(file).ToLower().StartsWith(REQUEST_FILE_PREFIX_CC) && Path.GetExtension(file).ToLower() == FILE_EXT)
                    {
                        item.IsSuccessful = true;
                        item.Status = QueueItemStatus.Discard;
                    }
                    else
                        AARTOBase.ErrorProcessing(ref item);
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                if (!item.IsSuccessful)
                    continue;

                try
                {
                    bool failed;
                    FileInfo fileInfo = (FileInfo)item.Body;
                    item.Status = QueueItemStatus.Discard;
                    failed = GetRequestDataFromTP(fileInfo);
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        public override void FinaliseData(List<QueueItem> queueList) { }

        private bool GetRequestDataFromTP(FileInfo fileInfo)
        {
            bool failed = false;
            bool fileFailed = false;
            int failedcount = 0;
            int noOfRecords = 0;

            NoticeDB notice = new NoticeDB(connAARTODB);
            ChargeDB charge = new ChargeDB(connAARTODB);
            TMSData tmsData = new TMSData(connAARTODB);

            int autIntNo = 0;
            string srLine = "";
            string file = fileInfo.FullName;
            string fileName = fileInfo.Name;

            string autNo = fileName.Substring(5, 3);

            // if the local authority is missing bail out immediately swearing 
            autIntNo = tmsData.GetAutIntFromAuthorityFromAutNo(autNo);
            if (autIntNo <= 0)
            {
                Logger.Info(ResourceHelper.GetResource("RequestFile_ErrorAuthName", file, autNo));
                failed = true;
            }

            // move the file to the request files subfolder
            if (!Directory.Exists(connCIP_CofCTUNC + @"\RequestFiles\Completed\" + autNo))
                Directory.CreateDirectory(connCIP_CofCTUNC + @"\RequestFiles\Completed\" + autNo);

            // move the file to the request files subfolder
            if (!Directory.Exists(connCIP_CofCTUNC + @"\RequestFiles\Error\"))
                Directory.CreateDirectory(connCIP_CofCTUNC + @"\RequestFiles\Error\");

            if (CheckPrintRequestFile(Path.GetFileName(file)))
            {
                this.FileToLocalFolder(file, connCIP_CofCTUNC + @"\RequestFiles\Error\", "request", "move");
                failed = true;
                return failed;
            }

            if (Path.GetFileName(file).ToLower().StartsWith(REQUEST_FILE_PREFIX_CC) && Path.GetExtension(file).ToLower() == FILE_EXT)
            {
                if (!failed)
                {
                    // open the file and update the each database table separately
                    FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read);
                    StreamReader sr = new StreamReader(fs, System.Text.Encoding.Default);
                    while (!sr.EndOfStream && !fileFailed)
                    {
                        srLine = sr.ReadLine();

                        if (srLine.Length > 2)
                        {
                            string recordType = srLine.Substring(0, 2);

                            switch (recordType)
                            {
                                case "27":
                                    // Print request record.
                                    failed = ProcessRequestRecord(file, srLine, ref failedcount, autNo, autIntNo, ref fileFailed, ref noOfRecords);
                                    break;
                            }
                        }
                    } // end of while sr

                    sr.Close();
                    fs.Close();

                    if (failedcount > 0)
                    {
                        Logger.Error(ResourceHelper.GetResource("RequestFile_UpdateFailed") + failedcount);
                    }

                    if (fileFailed)
                    {
                        Logger.Error(ResourceHelper.GetResource("RequestFile_UnableProcessFileName") + file);
                        this.FileToLocalFolder(file, connCIP_CofCTUNC + @"\RequestFiles\Error\", "request", "move");
                    }
                    else
                    {
                        Logger.Info(ResourceHelper.GetResource("RequestFile_ProcessedOK") + file);
                        this.FileToLocalFolder(file, connCIP_CofCTUNC + @"\RequestFiles\Completed\" + autNo + @"\", "request", "move");
                    }
                }
            }

            failed = AddPrintRequestFile(Path.GetFileName(file), autIntNo, noOfRecords, failedcount);
            return failed;
        }

        #region
        int iRequestRecordCount = 0;
        int iSequenceNO = 1;
        /// <summary>
        /// 090926 FT process RequestRecord
        /// </summary>
        /// <param name="file"></param>
        /// <param name="srLine"></param>
        /// <param name="countOK"></param>
        /// <param name="autNo"></param>
        /// <param name="autIntNo"></param>
        /// <param name="fileFailed"></param>
        /// <returns></returns>
        private bool ProcessRequestRecord(string file, string srLine, ref int failedcount, string autNo, int autIntNo, ref bool fileFailed, ref int noOfRecords)
        {
            tmsErrorLog.TELProcName = "ProcessRequestRecord";
            tmsErrorLog.TELDateTime = DateTime.MinValue;

            bool failed = false;
            int notIntNo = 0;
            string strRequestRecord = "";
            string strNoticeNumber = "";

            int iDocumentType = Convert.ToInt32(Path.GetFileName(file).Substring(10, 2));
            string FileSequenceNO = Path.GetFileName(file).Substring(12, 4);

            noOfRecords++;

            if (iRequestRecordCount == PRINTFILE_NO_OF_NOTICES)
            {
                iRequestRecordCount = 0;
                iSequenceNO++;
            }

            FileSequenceNO = FileSequenceNO + iSequenceNO.ToString().PadLeft(4, '0');

            try
            {
                if (srLine.Length > 2)
                {
                    //all records from up file are included here - need to allow for control records
                    if (srLine.Substring(0, 2).Equals("27"))
                    {
                        string[] strRecord = Regex.Split(srLine, "\t");
                        StringBuilder sb = new StringBuilder(string.Empty);

                        sb = new StringBuilder("<root>");
                        sb.Append("<request NotTicketNo=\"");
                        sb.Append(strRecord[3]);
                        sb.Append("\"");
                        strNoticeNumber = strRecord[3];

                        AddParameter(sb, "AutIntNo", autIntNo.ToString());
                        AddParameter(sb, "RecordType", strRecord[0]);
                        AddParameter(sb, "OffenderType ", strRecord[5]);

                        //dls 091016 - Offender surname & Initials?
                        AddParameter(sb, "Surname", strRecord[6]);
                        AddParameter(sb, "Initials", strRecord[7]);
                        AddParameter(sb, "Forenames", strRecord[8]);
                        AddParameter(sb, "BusinessName", strRecord[9]);
                        AddParameter(sb, "IDType", strRecord[10]);
                        AddParameter(sb, "IDNumber", strRecord[11]);

                        AddParameter(sb, "PoAdd1", strRecord[12]);
                        AddParameter(sb, "PoAdd2", strRecord[13]);
                        AddParameter(sb, "PoAdd3", strRecord[14]);
                        AddParameter(sb, "PoAdd4", strRecord[15]);
                        AddParameter(sb, "PoAdd5", strRecord[16]);
                        AddParameter(sb, "PoCode", strRecord[17]);
                        AddParameter(sb, "StAdd1", strRecord[18]);
                        AddParameter(sb, "StAdd2", strRecord[19]);
                        AddParameter(sb, "StAdd3", strRecord[20]);
                        AddParameter(sb, "StAdd4", strRecord[21] + " " + strRecord[22]);
                        AddParameter(sb, "StCode", strRecord[23]);

                        //AddParameter(sb, "NotVehicleRegisterNo", strRecord[4]);
                        AddParameter(sb, "NotRegNo", strRecord[4]);
                        AddParameter(sb, "NotOffenceDate", strRecord[24] + " " + strRecord[25].Insert(4, ":").Insert(2, ":"));
                        AddParameter(sb, "NotLocDescr", strRecord[26] + " " + strRecord[27]);
                        AddParameter(sb, "NotSpeed1", strRecord[28]);
                        AddParameter(sb, "NotVehicleMakeCode", strRecord[29]);
                        AddParameter(sb, "NotVehicleTypeCode", strRecord[30]);
                        AddParameter(sb, "NotOfficerNo", strRecord[31]);
                        AddParameter(sb, "NotPaymentDate", strRecord[32]);
                        AddParameter(sb, "NotCourtNo", strRecord[33]);
                        AddParameter(sb, "NotCourtName", strRecord[34]);
                        AddParameter(sb, "NotEasyPayNumber", strRecord[36]);

                        //dls - the field no 43 in the spec is wrong - everything after 43 needs to decr by 1.
                        //    - also need to be careful about what we are checkign when updating Rev Fine amount
                        //if(strRecord[44] == "Y")
                        //    AddParameter(sb, "ChgRevFineAmount", strRecord[46]);
                        //else
                        //    AddParameter(sb, "ChgRevFineAmount", "0");
                        AddParameter(sb, "FineAmountChanged", strRecord[44]);
                        AddParameter(sb, "ChgRevFineAmount", strRecord[46]);

                        string strChgOffenceDescr = "";
                        for (int i = 0; i < 12 && 47 + i < strRecord.Length; i++)
                        {
                            strChgOffenceDescr += strRecord[47 + i];
                        }
                        AddParameter(sb, "ChgOffenceDescr", strChgOffenceDescr);

                        AddParameter(sb, "DocumentType", iDocumentType.ToString());
                        switch (iDocumentType)
                        {
                            case 1:  //Final Sec 341 notices
                                AddParameter(sb, "ChargeStatus", "260");
                                break;
                            case 3: //Re-issue Section 341 notice
                                AddParameter(sb, "ChargeStatus", "210");
                                break;
                            case 2: //Warrant letters:
                                AddParameter(sb, "ChargeStatus", "830");
                                AddParameter(sb, "SummonsNo", strRecord[37]);
                                AddParameter(sb, "SumCourtDate", strRecord[38]);
                                AddParameter(sb, "WOANumber", strRecord[39]);
                                AddParameter(sb, "SChContemptAmount", strRecord[40]);
                                break;
                        }

                        iRequestRecordCount++;
                        AddParameter(sb, "SequenceNO", FileSequenceNO);

                        sb.Append(" />");

                        sb.Append("</root>");
                        strRequestRecord = sb.ToString();
                    }
                }
            }
            catch
            {
                Logger.Error(ResourceHelper.GetResource("RequestFile_Error", file, strNoticeNumber));
                failed = true;
                return failed;
            }

            if (strNoticeNumber.Length > 0)
            {
                // update the database
                string errMessage = string.Empty;

                //NoticeDB notice = new NoticeDB(connAARTODB);
                //int updNotIntNo = notice.UpdateCiprusCofCTRequest(strRequestRecord, AARTOBase.lastUser, notIntNo, ref errMessage);
                //Oscar 20120320 changed
                string printFileName = UpdateCiprusCofCTRequest(strRequestRecord, AARTOBase.LastUser, ref notIntNo, ref errMessage);
                if (notIntNo <= 0)
                {
                    failed = true;
                    switch (notIntNo)
                    {
                        case 0:
                            break;

                        case -1:
                            errMessage = ResourceHelper.GetResource("Result_NoticeNotExist");
                            break;

                        case -2:
                            errMessage = ResourceHelper.GetResource("Result_UnableUpdateNotice");
                            break;

                        case -3:
                            errMessage = ResourceHelper.GetResource("Result_UnableUpdateCharge");
                            break;

                        case -4:
                            errMessage = ResourceHelper.GetResource("Result_ErrorChargeStatus");
                            break;

                        case -5:
                            errMessage = ResourceHelper.GetResource("Result_UnableUpdateAdd");
                            break;

                        case -6:
                            errMessage = ResourceHelper.GetResource("Result_CourtNotExist");
                            break;

                        case -7:
                            errMessage = ResourceHelper.GetResource("Result_FailedAddCourtRoom");
                            break;

                        case -8:
                            errMessage = ResourceHelper.GetResource("Result_FailedAddCourtDate");
                            break;

                        case -9:
                            errMessage = ResourceHelper.GetResource("Result_FailedAddSummons");
                            break;

                        case -10:
                            errMessage = ResourceHelper.GetResource("Result_FailedAddAddCJT");
                            break;

                        case -11:
                            errMessage = ResourceHelper.GetResource("Result_FailedInsertSummons_Charge");
                            break;

                        case -12:
                            errMessage = ResourceHelper.GetResource("Result_FailedInsertSumCharge");
                            break;

                        case -13:
                            errMessage = ResourceHelper.GetResource("Result_FailedInsertAccused");
                            break;

                        case -14:
                            errMessage = ResourceHelper.GetResource("Result_FailedAddNotSumPrintFile");
                            break;

                        case -15:
                            errMessage = ResourceHelper.GetResource("Result_FailedUpdateChgStatus");
                            break;

                        case -16:
                            errMessage = ResourceHelper.GetResource("Result_FailedAddWOA");
                            break;

                        case -17:
                            errMessage = ResourceHelper.GetResource("Result_FailedInsertEP");
                            break;

                        default:
                            errMessage = ResourceHelper.GetResource("Result_FailedUpdateNoticeWithRequestRecord");
                            break;
                    }
                    failedcount++;
                    Logger.Error(ResourceHelper.GetResource("RequestFile_FailedUpdateNotice", file, strNoticeNumber, errMessage));

                    tmsErrorLog.TELErrMessage = file + " failed to update notice: " + strNoticeNumber + " error - " + errMessage;

                    Int32 err = errorLog.AddUpdateTMSErrorLog(tmsErrorLog, ref errMessage);

                    if (err <= 0)
                    {
                        Logger.Error(ResourceHelper.GetResource("RequestFile_FailedToWriteError", strNoticeNumber, errMessage));
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(printFileName) && autIntNo > 0)
                    {
                        string errMsg;
                        int pfnIntNo = AARTOBase.SavePrintFileName(PrintFileNameType.Notice, printFileName, autIntNo, notIntNo, out errMsg);
                        if (pfnIntNo <= 0)
                        {
                            AARTOBase.ErrorProcessing(errMsg, true);
                            return true;
                        }

                        if (!this.pfnList.Contains(pfnIntNo))
                        {
                            this.pfnList.Add(pfnIntNo);

                            //Oscar 20120412 changed group
                            AuthorityDetails authority = new AuthorityDB(this.connAARTODB).GetAuthorityDetails(autIntNo);
                            int frameIntNo = new NoticeDB(this.connAARTODB).GetFrameForNotice(notIntNo);
                            string violationType = new FrameDB(this.connAARTODB).GetViolationType(frameIntNo);

                            QueueItemProcessor queueProcessor = new QueueItemProcessor();
                            QueueItem pushItem = new QueueItem();
                            pushItem = new QueueItem();
                            pushItem.Body = pfnIntNo;
                            pushItem.Group = string.Format("{0}|{1}", authority.AutCode.Trim(), violationType);
                            pushItem.ActDate = this.printActionDate;

                            switch (iDocumentType)
                            {
                                case 1:  //Final Sec 341 notices
                                    pushItem.QueueType = ServiceQueueTypeList.Print2ndNotice;

                                    //added by jacob 20120726 start
                                    //new authority rule(print using IBM info printer), 
                                    //prevent pushing Print2ndNotice queue.
                                    //not tested yet.
                                    bool notAllowedPushingQueue =  rules.Rule("6209").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase);
                                    if (notAllowedPushingQueue)
                                    {
                                        break;
                                    }
                                    //added by jacob 20120726 end 
                                    queueProcessor.Send(pushItem);
                                    break;
                                case 3: //Re-issue Section 341 notice
                                    pushItem.QueueType = ServiceQueueTypeList.Print1stNotice;
                                    queueProcessor.Send(pushItem);
                                    break;
                            }
                        }
                    }

                    Logger.Info(ResourceHelper.GetResource("RequestFile_UpdatedSuccessfully", strNoticeNumber));
                }
            }
            return failed;
        }

        //090925 FT  AddParameter into XML parameter
        private void AddParameter(StringBuilder sb, string strName, string value)
        {
            value = value.Replace(",", "&apos;").Replace("\"", "&quot;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("&", "&amp;");  // For xml special character.  
            sb.Append(" " + strName + "=\"");
            sb.Append(value);
            sb.Append("\"");
        }
        #endregion

        private bool CheckPrintRequestFile(string printRequestFileName)
        {
            bool failed = false;
            string errMessage = string.Empty;
            string processLogDesc = string.Empty;

            TMSData tmsData = new TMSData(connAARTODB);

            if (printRequestFileName.Length == 20)
            {
                string prefix = printRequestFileName.Substring(0, 5).ToLower();
                string localAutcode = printRequestFileName.Substring(5, 3);
                string branchCode = printRequestFileName.Substring(8, 2);
                string documentType = printRequestFileName.Substring(10, 2);
                string sequenceNumber = printRequestFileName.Substring(12, 4);

                if (prefix == REQUEST_FILE_PREFIX_CC)
                {
                    int isDuplicate = tmsData.CheckDuplicatePrintFile(localAutcode, branchCode, documentType, sequenceNumber, ref errMessage);
                    if (isDuplicate > 0)
                    {
                        failed = false;
                        return failed;
                    }
                    else
                    {
                        switch (isDuplicate)
                        {
                            case -1:
                                processLogDesc = ResourceHelper.GetResource("RequestFile_AlreadyLoaded");
                                break;
                            case 2:
                                processLogDesc = errMessage;
                                break;
                        }

                        Logger.Error(ResourceHelper.GetResource("RequestFile_CheckPrintRequestFile") + processLogDesc);
                        failed = true;
                        return failed;
                    }

                }
                else
                {
                    processLogDesc = ResourceHelper.GetResource("RequestFile_NotCIPResquestFile");
                    Logger.Error(ResourceHelper.GetResource("RequestFile_CheckPrintRequestFile") + processLogDesc);
                    failed = true;
                    return failed;
                }
            }
            else if (printRequestFileName.Length == 16)
            {
                processLogDesc = ResourceHelper.GetResource("RequestFile_SequenceNumberNotFound") + printRequestFileName;
                Logger.Error(ResourceHelper.GetResource("RequestFile_CheckPrintRequestFile") + processLogDesc);
                failed = true;
                return failed;
            }
            else
            {
                processLogDesc = "The file name of " + printRequestFileName + " is incorrect.";
                Logger.Error(ResourceHelper.GetResource("RequestFile_CheckPrintRequestFile") + processLogDesc);
                failed = true;
                return failed;
            }

        }

        private bool AddPrintRequestFile(string printRequestFileName, int autIntNo, int noOfRecords, int failedCount)
        {
            bool failed = false;

            string errMessage = string.Empty;

            TMSData tmsData = new TMSData(connAARTODB);

            if (printRequestFileName.Length == 20)
            {
                string prefix = printRequestFileName.Substring(0, 5).ToLower();
                string branchCode = printRequestFileName.Substring(8, 2);
                string documentType = printRequestFileName.Substring(10, 2);
                string sequenceNumber = printRequestFileName.Substring(12, 4);

                int success = tmsData.AddRequestPrintFile(printRequestFileName, branchCode, documentType, sequenceNumber, autIntNo, ref errMessage, noOfRecords, failedCount, AARTOBase.LastUser);

                if (success < 1)
                {
                    Logger.Error(ResourceHelper.GetResource("RequestFile_CheckPrintRequestFile") + errMessage);
                    failed = true;
                }
            }

            return failed;
        }

        private string UpdateCiprusCofCTRequest(string requestRecord, string lastUser, ref int notIntNo, ref string msg)
        {
            string printFileName = string.Empty;
            msg = string.Empty;
            DBHelper db = new DBHelper(this.connAARTODB);
            SqlParameter[] paras = new SqlParameter[3];
            paras[0] = new SqlParameter("@RequestRecord", requestRecord);
            paras[1] = new SqlParameter("@LastUser", lastUser);
            paras[2] = new SqlParameter("@NotIntNo", notIntNo) { Direction = ParameterDirection.InputOutput };
            object result = db.ExecuteScalar("NoticeUpdateRequest_CC_WS", paras, out msg);
            if (result != null && result.ToString().Length > 0)
                printFileName = result.ToString();
            notIntNo = Convert.ToInt32(paras[2].Value);
            return printFileName;
        }

        DateTime printActionDate = DateTime.Now;
        private void SetServiceParameters()
        {
            int delayHours = 0;
            if (ServiceParameters != null
                && ServiceParameters.ContainsKey("DelayActionDate_InHours")
                && !string.IsNullOrEmpty(ServiceParameters["DelayActionDate_InHours"]))
                delayHours = Convert.ToInt32(ServiceParameters["DelayActionDate_InHours"]);
            this.printActionDate = DateTime.Now.AddHours(delayHours);
        }
    }
}
