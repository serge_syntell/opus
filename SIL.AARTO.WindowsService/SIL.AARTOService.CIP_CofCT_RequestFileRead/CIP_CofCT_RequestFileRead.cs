﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CIP_CofCT_RequestFileRead
{
    partial class CIP_CofCT_RequestFileRead : ServiceHost
    {
        public CIP_CofCT_RequestFileRead()
            : base("", new Guid("EDEB95A5-403C-4798-9893-C75D15C86DAB"), new CIP_CofCT_RequestFileReadService())
        {
            InitializeComponent();
        }
    }
}
