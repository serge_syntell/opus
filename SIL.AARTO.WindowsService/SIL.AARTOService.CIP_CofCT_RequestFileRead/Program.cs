﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CIP_CofCT_RequestFileRead
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.CIP_CofCT_RequestFileRead"
                ,
                DisplayName = "SIL.AARTOService.CIP_CofCT_RequestFileRead"
                ,
                Description = "SIL AARTOService CIP_CofCT_RequestFileRead"
            };

            ProgramRun.InitializeService(new CIP_CofCT_RequestFileRead(), serviceDescriptor, args);
        }
    }
}
