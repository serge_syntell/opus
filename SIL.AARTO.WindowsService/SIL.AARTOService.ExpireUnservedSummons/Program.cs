﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.ExpireUnservedSummons
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.ExpireUnservedSummons"
                ,
                DisplayName = "SIL.AARTOService.ExpireUnservedSummons"
                ,
                Description = "SIL AARTOService ExpireUnservedSummons"
            };

            ProgramRun.InitializeService(new ExpireUnservedSummons(), serviceDescriptor, args);
        }
    }
}
