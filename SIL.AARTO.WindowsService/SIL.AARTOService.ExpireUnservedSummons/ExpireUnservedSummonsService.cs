﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using SIL.AARTOService.Resource;
using Stalberg.TMS;

namespace SIL.AARTOService.ExpireUnservedSummons
{
    public class ExpireUnservedSummonsService : ServiceDataProcessViaQueue
    {
        public ExpireUnservedSummonsService()
            : base("", "", new Guid("FC8C9062-CC58-4FF5-81CC-953C598C1B86"), ServiceQueueTypeList.ExpireUnservedSummons)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
 
        }

        private const string TICKET_PROCESSOR_TMS = "TMS";
        //private const string TICKET_PROCESSOR_AARTO = "AARTO";
        //private const string TICKET_PROCESSOR_JMPD_AARTO = "JMPD_AARTO";

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        AuthorityDetails authDetails = null;
        string connectStr = string.Empty;
        string strAutCode = string.Empty;
        string autName = string.Empty;

        SIL.AARTO.DAL.Services.NoticeService noticeService = new AARTO.DAL.Services.NoticeService();
        SIL.AARTO.DAL.Services.NoticeSummonsService noticeSummonsService = new AARTO.DAL.Services.NoticeSummonsService();

        int autIntNo;

        int noDaysForSummonsExpiry;

        public sealed override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && ServiceUtility.IsNumeric(body))
            {
                item.IsSuccessful = true;
            }
            else
            {
                // jerry 2012-02-21 change
                //item.IsSuccessful = false;
                //item.Status = QueueItemStatus.UnKnown;
                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.lastUser, item.Body));
                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.LastUser, item.Body), true);
            }

            if (strAutCode != item.Group)
            {
                strAutCode = item.Group.Trim();
                //jerry 2012-04-05 check group value
                if (string.IsNullOrWhiteSpace(strAutCode))
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
                    return;
                }

                this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode);
                autIntNo = authDetails.AutIntNo;
                autName = authDetails.AutName;

                //modified by linda 2012-12-21(replace AutTicketProcessor by NotTicketProcessor)
                int sumIntNo = Convert.ToInt32(item.Body);
                SIL.AARTO.DAL.Entities.NoticeSummons noticeSummons = noticeSummonsService.GetBySumIntNo(sumIntNo).FirstOrDefault();
                SIL.AARTO.DAL.Entities.Notice notice = new SIL.AARTO.DAL.Entities.Notice();
                if (noticeSummons != null)
                {
                    notice = noticeService.GetByNotIntNo(noticeSummons.NotIntNo);
                }

                if (notice.NotTicketProcessor != TICKET_PROCESSOR_TMS)
                {
                    // jake 2013-07-12 added message when discard queue 
                    item.Status = QueueItemStatus.Discard;
                    item.IsSuccessful = false;
                    this.Logger.Info(String.Format("Invalid notice ticket processor  {0}", (notice == null ? "" : notice.NotTicketProcessor)), item.Body.ToString());
                }

                rules.InitParameter(connectStr, strAutCode);
            }
        }

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            int sumIntNo, success;
            string msg = string.Empty;

            // jerry 2012-02-21 change
            //foreach (QueueItem item in queueList)
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                try
                {
                    if (item.IsSuccessful)
                    {
                        sumIntNo = Convert.ToInt32(item.Body);

                        noDaysForSummonsExpiry = rules.Rule("NotOffenceDate", "SumExpireDate").DtRNoOfDays; 
                        //success = UpdateExpiredSummons(sumIntNo, ref msg);
                        success = UpdateExpiredUnServedSummons(sumIntNo, out msg);
                        switch (success)
                        {
                                //Jake 2015-04-10 set queue status from postpone to discard
                                // summons has been servered or type is not S54 or finalized
                            case -1:
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ExpiredUnservedSummonsErrMsg1"), sumIntNo),LogType.Info,ServiceOption.Continue,item, QueueItemStatus.Discard);
                                break;
                            case -2:
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ExpiredUnservedSummonsErrMsg2"), sumIntNo));
                                break;
                            case -3:
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ExpiredUnservedSummonsErrMsg3"), sumIntNo));
                                break;
                            case -4:
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ExpiredUnservedSummonsErrMsg4"), sumIntNo));
                                break;
                            case -5:
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ExpiredUnservedSummonsErrMsg5"), sumIntNo));
                                break;
                            case -6:
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ExpiredUnservedSummonsErrMsg6"), msg, sumIntNo), true);
                                break;
                            case -7:
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ExpiredUnservedSummonsErrMsg7"), msg, sumIntNo), true);
                                break;
                            case -8:
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ExpiredUnservedSummonsErrMsg8"), msg, sumIntNo), false, QueueItemStatus.Discard);
                                break;
                            case 0:
                                this.Logger.Info("Update Expire Unserved Summons falid, function returned 0; Queue key:" + item.Body.ToString());
                                if (!String.IsNullOrEmpty(msg))
                                    AARTOBase.ErrorProcessing(ref item, msg);
                                break;
                            case 1:
                                this.Logger.Info(string.Format(ResourceHelper.GetResource("SuccessfulMsgInExpireUnservedSummons"), sumIntNo));
                                item.Status = QueueItemStatus.Discard;
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //item.Status = QueueItemStatus.UnKnown;
                    //this.Logger.Error(ex);
                    //throw ex;
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        //private int UpdateExpiredSummons(int sumIntNo, ref string errMsg)
        //{
        //    DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));

        //    SqlParameter[] paras = new SqlParameter[3];
        //    paras[0] = new SqlParameter("@SumIntNo", sumIntNo);
        //    paras[1] = new SqlParameter("@NoDaysForSummonsExpiry", noDaysForSummonsExpiry);
        //    paras[2] = new SqlParameter("@LastUser", AARTOBase.LastUser);

        //    object obj = db.ExecuteScalar("SummonsExpired_WS", paras, out errMsg);
        //    int result;
        //    if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(errMsg))
        //        result = Convert.ToInt32(obj);
        //    else
        //        result = -6;

        //    return result;
        //}

        int UpdateExpiredUnServedSummons(int sumIntNo, out string errMsg)
        {
            errMsg = string.Empty;
            DBHelper db = new DBHelper(connectStr);
            SqlParameter[] paras = new SqlParameter[3];
            paras[0] = new SqlParameter("@SumIntNo", sumIntNo);
            paras[1] = new SqlParameter("@NumOfDaysToExpire", noDaysForSummonsExpiry);
            paras[2] = new SqlParameter("@LastUser", AARTOBase.LastUser);

            object obj = db.ExecuteScalar("ExpireUnservedSummons_WS", paras, out errMsg);
            int result;
            if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(errMsg))
                result = Convert.ToInt32(obj);
            else
                result = 0;

            return result;
        }
    }
}
