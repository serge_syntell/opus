﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.ExpireUnservedSummons
{
    partial class ExpireUnservedSummons : ServiceHost
    {
        public ExpireUnservedSummons()
            :base("", new Guid("962148DB-97F9-460E-84AA-C9CDD2A43C66"), new ExpireUnservedSummonsService())
        {
            InitializeComponent();
        }
    }
}
