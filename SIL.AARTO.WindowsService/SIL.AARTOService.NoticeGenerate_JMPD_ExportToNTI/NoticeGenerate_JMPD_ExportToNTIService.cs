﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using System.Configuration;
using SIL.QueueLibrary;
using SIL.AARTOService.Library;
using System.Data;
using System.Diagnostics;
using Stalberg.TMS;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Collections;
using SIL.AARTOService.Resource;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Transactions;
using SIL.ServiceBase;

namespace SIL.AARTOService.NoticeGenerate_JMPD_ExportToNTI
{
    public class NoticeGenerate_JMPD_ExportToNTIService : ServiceDataProcessViaQueue
    {
        public NoticeGenerate_JMPD_ExportToNTIService()
            : base("", "", new Guid("5CD7CA9D-8A3C-4121-AD7E-FAACB05C3B37"), ServiceQueueTypeList.Frame_Generate1stNotice)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);

            ServiceUtility.InitializeNetTier(connectStr);
            string eNaTISStr = AARTOBase.GetConnectionString(SIL.ServiceQueueLibrary.DAL.Entities.ServiceConnectionNameList.SIL_NTI,
                 SIL.ServiceQueueLibrary.DAL.Entities.ServiceConnectionTypeList.DB);
            ServiceUtility.InitializeNetTier(eNaTISStr, "SIL.eNaTIS");

            AARTOBase.OnServiceStarting = () =>
            {
                this.groupStr = null;
                this.strAutCode = null;
                this.stamp = DateTime.Now;
                this.lastStamp = DateTime.Now;
                this.pfnList.Clear();
                SetServiceParameters();
            };
        }



        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        string strAutCode = null;
        string connectStr;
        int AutIntNo;
        int NotIntNo;
        int frameIntNo;
        string DataWashingActived = null;
        string IsUseDataWashingAddr = null;
        int DataWashingRecycleMonth;
        int daysOfSummonsServicePeriodExpired;
        bool isHWOForPrint = false;      // Nick 20120801 added CoCT Report check

        List<int> pfnList = new List<int>();
        bool isChecked = true;
        string batchFilename;
        DateTime stamp, lastStamp;
        string groupStr;
        string stampStr, hwoStampStr;

        #region Oscar 20120406 disabled
        //public override void InitialWork(ref QueueItem item)
        //{
        //    string body = string.Empty;
        //    if (item.Body != null)
        //        body = item.Body.ToString();
        //    if (!string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(item.Group))
        //    {
        //        try
        //        {
        //            item.IsSuccessful = true;
        //            item.Status = QueueItemStatus.Discard;

        //            if (!int.TryParse(body, out frameIntNo))
        //            {
        //                AARTOBase.ErrorProcessing(ref item);
        //                return;
        //            }

        //            FilmDB filmDB = new FilmDB(connectStr);
        //            FrameDB frameDB = new FrameDB(connectStr);

        //            FrameDetails frame = frameDB.GetFrameDetails(frameIntNo);
        //            FilmDetails film = filmDB.GetFilmDetails(frame.FilmIntNo);

        //            if (!string.IsNullOrEmpty(film.FilmLockUser) || string.IsNullOrEmpty(film.FilmNo))
        //            {
        //                AARTOBase.ErrorProcessing(ref item);
        //                return;
        //            }

        //            AARTOBase.BatchCount--;
        //        }
        //        catch (Exception ex)
        //        {
        //            AARTOBase.ErrorProcessing(ref item, ex);
        //        }
        //    }
        //}
        #endregion

        public override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString().Trim();
            if (!string.IsNullOrWhiteSpace(body) && !string.IsNullOrWhiteSpace(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;

                    if (!int.TryParse(body, out this.frameIntNo))
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        AARTOBase.DeQueueInvalidItem(ref item);
                        return;
                    }

                    string groupStr = item.Group.Trim();
                    string[] group = groupStr.Split('|');
                    string autCode = group[0].Trim();

                    if (groupStr != this.groupStr)
                    {
                        if (this.groupStr != null)
                        {
                            //AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Retry);
                            //AARTOBase.SkipReceivingData = true;
                            AARTOBase.SkipReceivingQueueData(ref item);
                            this.groupStr = null;
                            this.strAutCode = null;
                            return;
                        }

                        this.groupStr = groupStr;
                        this.strAutCode = autCode;
                        rules.InitParameter(this.connectStr, this.strAutCode);

                        QueueGroupValidation(ref item);

                        // Nick 20120801 added CoCT Report check
                        if (group.Length == 3 && group[2].Trim() == "HWO")
                            isHWOForPrint = true;
                        else
                            isHWOForPrint = false;
                    }
                    else if (!this.isChecked)
                    {
                        AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", autCode));
                        AARTOBase.DeQueueInvalidItem(ref item);
                        return;
                    }

                    QueueItemValidation(ref item);

                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                    AARTOBase.DeQueueInvalidItem(ref item);
                    return;
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                AARTOBase.DeQueueInvalidItem(ref item);
                return;
            }
        }

        private void QueueGroupValidation(ref QueueItem item)
        {
            this.isChecked = false;

            AuthorityDetails authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim().Equals(this.strAutCode, StringComparison.OrdinalIgnoreCase));
            if (authDetails == null)
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
                AARTOBase.DeQueueInvalidItem(ref item);
                return;
            }

            this.AutIntNo = authDetails.AutIntNo;

            this.DataWashingActived = rules.Rule("9060").ARString.Trim();
            this.IsUseDataWashingAddr = rules.Rule("9110").ARString.Trim();
            this.DataWashingRecycleMonth = rules.Rule("9080").ARNumeric;
            this.daysOfSummonsServicePeriodExpired = rules.Rule("NotOffenceDate", "NotSummonsBeforeDate").DtRNoOfDays;

            this.isChecked = true;
        }

        private void QueueItemValidation(ref QueueItem item)
        {
            if (!isChecked || !item.IsSuccessful) return;
            if (!isHWOForPrint)
            {
                FrameDetails frame = new FrameDB(connectStr).GetFrameDetails(this.frameIntNo);

                // Oscar 20121112 drop the "dead" frame
                int frameStatus;
                int.TryParse(frame.FrameStatus, out frameStatus);
                if (frameStatus >= 900)
                {
                    AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                    return;
                }

                //Jerry 2012-05-29 add VerificationBin, check the 2310,2311 authority rules when frame status is just 600
                if (frame.FrameStatus.Trim() == "600" && rules.Rule("2310").ARString == "Y")
                {
                    SIL.AARTO.DAL.Entities.VerificationBin verificationBin = new SIL.AARTO.DAL.Services.VerificationBinService().Find("VeBiRegNo=" + frame.RegNo.Trim()).FirstOrDefault();
                    if (verificationBin != null)
                    {
                        int daysOfCutOff = rules.Rule("2311").ARNumeric;
                        if (((TimeSpan)(DateTime.Now - frame.OffenceDate)).Days <= daysOfCutOff)
                        {
                            //set frame status to 850
                            SIL.AARTO.DAL.Entities.Frame frameEntity = new SIL.AARTO.DAL.Services.FrameService().GetByFrameIntNo(this.frameIntNo);
                            frameEntity.FrameStatus = 850;
                            new SIL.AARTO.DAL.Services.FrameService().Save(frameEntity);

                            //set current queue status
                            item.IsSuccessful = false;
                            item.Status = QueueItemStatus.Discard;

                            this.Logger.Info("(DateTime.Now - frame.OffenceDate)).Days <= daysOfCutOff please check authority rule 2311 or OffenceDate; Queke key:" + item.Body.ToString());

                            //push current queue
                            QueueItem pushItem = null;
                            pushItem = new QueueItem()
                            {
                                Body = item.Body,
                                Group = item.Group,
                                ActDate = DateTime.Now.AddHours(72),
                                QueueType = ServiceQueueTypeList.Frame_Generate1stNotice
                            };
                            QueueItemProcessor queueProcessor = new QueueItemProcessor();
                            queueProcessor.Send(pushItem);

                            return;
                        }
                    }
                }

                //Jerry 2012-05-29 add 860, 850 status
                if (frame == null || (frame.FrameStatus.Trim() != "600" && frame.FrameStatus.Trim() != "800" && frame.FrameStatus.Trim() != "850" && frame.FrameStatus.Trim() != "860"))
                {
                    AARTOBase.DeQueueInvalidItem(ref item);
                    return;
                }

                FilmDetails film = new FilmDB(connectStr).GetFilmDetails(frame.FilmIntNo);
                if (film == null || !string.IsNullOrEmpty(film.FilmLockUser) || string.IsNullOrEmpty(film.FilmNo))
                {
                    //AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.PostPone);
                    // Oscar 20121112 chaned to Unknown for FilmLockUser
                    AARTOBase.DeQueueInvalidItem(ref item);
                    return;
                }

                int notIntNo = new NoticeDB(connectStr).GetNoticeByFrame(this.frameIntNo);
                if (notIntNo > 0)
                {
                    AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                    return;
                }
            }
            else
            {
                NoticeDB noticeDB = new NoticeDB(connectStr);
                NoticeDetails notice = noticeDB.GetNoticeDetails(Convert.ToInt32(item.Body));
                if (notice.NoticeStatus != 255)
                {
                    AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                    return;
                }
            }

            AARTOBase.BatchCount--;
        }

        #region Oscar 20120406 disabled
        //public sealed override void MainWork(ref List<QueueItem> queueList)
        //{
        //    string msg = string.Empty;
        //    QueueItemProcessor queueProcessor = new QueueItemProcessor();

        //    //string dateStr = DateTime.Now.ToString("yyyy-MM-dd hh-mm-ss");
        //    dateStr = DateTime.Now.ToString("yyyy-MM-dd hh-mm-ss");
        //    dateStr = dateStr.Replace("/", "-");
        //    dateStr = dateStr.Replace(":", "-");
        //    batchFilename = "NBS-" + dateStr;

        //    for (int i = 0; i < queueList.Count; i++)
        //    {
        //        QueueItem item = queueList[i];

        //        if (!item.IsSuccessful)
        //            continue;

        //        try
        //        {
        //            if (strAutCode != item.Group.Trim())
        //            {
        //                strAutCode = item.Group.Trim();
        //                //jerry 2012-04-05 check group value
        //                if (string.IsNullOrWhiteSpace(strAutCode))
        //                {
        //                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
        //                    return;
        //                }

        //                AutIntNo = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode.Trim()).AutIntNo;
        //                AARTORules.GetSingleTon().InitParameter(connectStr, strAutCode);

        //                dateStr = DateTime.Now.ToString("yyyy-MM-dd hh-mm-ss");
        //                dateStr = dateStr.Replace("/", "-");
        //                dateStr = dateStr.Replace(":", "-");
        //                //batchFilename = "NBS-" + dateStr;

        //                DataWashingActived = rules.Rule("9060").ARString.Trim();
        //                IsUseDataWashingAddr = rules.Rule("9110").ARString.Trim();
        //                DataWashingRecycleMonth = rules.Rule("9080").ARNumeric;
        //            }

        //            if (!int.TryParse(item.Body.ToString(), out frameIntNo))
        //            {
        //                continue;
        //            }

        //            //Oscar 20120406 have to reset file name every time!
        //            batchFilename = "NBS-" + dateStr;

        //            //jerry 2012-04-05 change
        //            bool failed = CreatingNotice(frameIntNo, ref msg, item);
        //            //jerry 2012-03-28 if this frame is not eligible for notice generation
        //            //if (failed && item.Status == QueueItemStatus.Discard)
        //            if (failed)
        //            {
        //                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.Discard);
        //                //item.IsSuccessful = false;
        //                //Logger.Info(msg);
        //                return;
        //            }
        //            //jerry 2012-03-29 if FilmLockUser IS NOT NULL
        //            //if (failed && item.Status == QueueItemStatus.UnKnown)
        //            //{
        //            //    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false);
        //            //    return;
        //            //}

        //            if (!failed)
        //                failed = CreateTicket(frameIntNo, ref msg);

        //            if (failed)
        //            {
        //                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
        //            }
        //            else
        //            {
        //                QueueItem pushItem = new QueueItem();

        //                NoticeDB noticeDB = new NoticeDB(connectStr);
        //                NoticeDetails notice = noticeDB.GetNoticeDetails(NotIntNo);

        //                pushItem = new QueueItem();
        //                pushItem.Body = NotIntNo.ToString();
        //                pushItem.Group = strAutCode;
        //                pushItem.QueueType = ServiceQueueTypeList.SearchNameIDUpdate;
        //                queueProcessor.Send(pushItem);

        //                pushItem = new QueueItem();
        //                pushItem.Body = NotIntNo.ToString();
        //                pushItem.Group = strAutCode;
        //                pushItem.ActDate = notice.NotOffenceDate.AddDays(rules.Rule("NotOffenceDate", "NotSummonsBeforeDate").DtRNoOfDays);
        //                pushItem.QueueType = ServiceQueueTypeList.SummonsServicePeriodExpired;
        //                queueProcessor.Send(pushItem);

        //                string errMsg;
        //                int pfnIntNo = AARTOBase.SavePrintFileName(PrintFileNameType.Notice, this.batchFilename, this.AutIntNo, NotIntNo, out errMsg);

        //                if (pfnIntNo <= 0)
        //                {
        //                    AARTOBase.ErrorProcessing(ref item, errMsg, true);
        //                    return;
        //                }

        //                if (!this.pfnList.Contains(pfnIntNo))
        //                {
        //                    this.pfnList.Add(pfnIntNo);

        //                    pushItem = new QueueItem();
        //                    pushItem.Body = pfnIntNo;
        //                    pushItem.Group = strAutCode;
        //                    pushItem.QueueType = ServiceQueueTypeList.Print1stNotice;
        //                    pushItem.ActDate = this.printActionDate;
        //                    queueProcessor.Send(pushItem);
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            AARTOBase.ErrorProcessing(ref item, ex);
        //        }
        //    }
        //}
        #endregion

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            string msg = string.Empty;
            QueueItemProcessor queueProcessor = new QueueItemProcessor();
            NoticeDB noticeDB = new NoticeDB(connectStr);
            // Nick 20120917 added for report Non-summons registrations
            string frameStatus;
            bool isNonSum = false;
            FrameDB frameDB = new FrameDB(connectStr);
            NoticeMobileDetailService notMobDetServ = new NoticeMobileDetailService();
            NoticeMobileDetail notMobDet;

            if (queueList.Count > 0)
            {
                if (!isHWOForPrint)
                    this.stampStr = GetNewPrintFileName("NBS", "yyyy-MM-dd HH-mm-ss");
                else
                {
                    var groupStr = queueList[0].Group.Trim();
                    var group = groupStr.Split('|');
                    var autCode = group[0].Trim();
                    this.hwoStampStr = GetNewPrintFileName(string.Format("HWO-1ST-{0}", autCode.Trim()), "yyyy-MM-dd HH-mm-ss");
                }
            }

            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                if (!item.IsSuccessful) continue;

                bool failed = false;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    try
                    {
                        if (!isHWOForPrint)
                        {
                            this.frameIntNo = Convert.ToInt32(item.Body);
                            this.batchFilename = this.stampStr;

                            // Nick 20120917 added for report Non-summons registrations
                            //isNonSum = false;
                            //frameStatus = frameDB.GetFrameDetails(this.frameIntNo).FrameStatus.Trim();
                            //if (frameStatus == "850")
                            //{
                            //    isNonSum = true;
                            //}

                            failed = CreatingNotice(this.frameIntNo, ref msg, item);
                            if (failed) continue;

                            #region Edge 2012-12-20 disabled
                            /*
                            failed = CreateTicket(this.frameIntNo, ref msg);
                            if (failed)
                            {
                                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
                                continue;
                            }

                            List<QueueItem> pushItemList = new List<QueueItem>();
                            QueueItem pushItem = null;

                            NoticeDetails notice = noticeDB.GetNoticeDetails(this.NotIntNo);

                            // Oscar 20121008 add for not null authCode
                            string groupStr = item.Group.Trim();
                            string[] group = groupStr.Split('|');
                            string autCode = group[0].Trim();

                            pushItem = new QueueItem()
                            {
                                Body = NotIntNo,
                                //Group = strAutCode,
                                Group = autCode,
                                QueueType = ServiceQueueTypeList.SearchNameIDUpdate
                            };
                            pushItemList.Add(pushItem);

                            pushItem = new QueueItem()
                            {
                                Body = NotIntNo,
                                //Group = strAutCode,
                                Group = autCode,
                                ActDate = notice.NotOffenceDate.AddDays(this.daysOfSummonsServicePeriodExpired),
                                QueueType = ServiceQueueTypeList.SummonsServicePeriodExpired
                            };
                            pushItemList.Add(pushItem);

                            int pfnIntNo = AARTOBase.SavePrintFileName(PrintFileNameType.Notice, this.batchFilename, this.AutIntNo, NotIntNo, out msg);
                            if (pfnIntNo <= 0)
                            {
                                AARTOBase.ErrorProcessing(ref item, msg, true);
                                return;
                            }

                            if (!this.pfnList.Contains(pfnIntNo))
                            {
                                this.pfnList.Add(pfnIntNo);

                                pushItem = new QueueItem()
                                {
                                    Body = pfnIntNo,
                                    Group = item.Group,
                                    ActDate = this.printActionDate,
                                    QueueType = ServiceQueueTypeList.Print1stNotice
                                };
                                pushItemList.Add(pushItem);
                            }

                            queueProcessor.Send(pushItemList);

                            // Nick 20120917 added for report Non-summons registrations
                            if (isNonSum)
                            {
                                notMobDet = notMobDetServ.GetByNotIntNo(NotIntNo).FirstOrDefault();
                                if (notMobDet == null)
                                {
                                    notMobDet = new NoticeMobileDetail();
                                    notMobDet.NotIntNo = NotIntNo;
                                    notMobDet.IsNonSummonsRegNo = true;
                                    notMobDet.LastUser = AARTOBase.lastUser;
                                    notMobDetServ.Insert(notMobDet);
                                }
                                else
                                {
                                    notMobDet.IsNonSummonsRegNo = true;
                                    notMobDet.LastUser = AARTOBase.lastUser;
                                    notMobDetServ.Update(notMobDet);
                                }
                            }
                            */
                            #endregion
                        }
                        #region Edge 2012-12-20 disabled
                        /*
                        else
                        {
                            failed = SetHWO1stNoticePrintStatus(Convert.ToInt32(item.Body), this.hwoStampStr, ref msg);
                            if (failed)
                            {
                                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_UpdateHWOFirstNotice", msg));
                                continue;
                            }
                        }
                        */
                        #endregion

                        if (!failed)
                        {
                            scope.Complete();
                        }
                    }
                    catch (Exception ex)
                    {
                        failed = true;
                        AARTOBase.ErrorProcessing(ref item, ex);
                    }
                }
                if (!failed)
                {
                    NoticeDB not = new NoticeDB(connectStr);

                    NotIntNo = not.GetNoticeByFrame(frameIntNo);

                    string error = null;
                    eNatisDB db = new eNatisDB(connectStr);
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                    {
                        if (!db.SendToNTIDatabase(this.NotIntNo, AARTOBase.LastUser, out error))
                        {
                            Logger.Error(ResourceHelper.GetResource("Error_NoticeSendToeNatis", error, this.frameIntNo.ToString()));
                        }
                        else
                        {
                            scope.Complete();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// jerry 2012-04-05 change method
        /// Jerry 2012-04-16 when success is less than 0 should include FrameIntno 
        /// </summary>
        /// <param name="frameIntNo"></param>
        /// <param name="msg"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        private bool CreatingNotice(int frameIntNo, ref string msg, QueueItem item)
        {
            bool failed = false;
            string errorMsg = "";

            int success = AddNoticeChargeFromFrame(frameIntNo, ref errorMsg);

            if (success == 1)
            {
                errorMsg = ResourceHelper.GetResource("NoDateRule");
                msg = "CreatingNotice: " + errorMsg + " " + DateTime.Now;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg));
                failed = true;
                return failed;
            }

            if (success == 4)
            {
                errorMsg = ResourceHelper.GetResource("NoticeExpired");
                msg = "CreatingNotice: " + errorMsg + " " + DateTime.Now;
                item.IsSuccessful = false;
                item.Status = QueueItemStatus.UnKnown;
                Logger.Info(msg);
                failed = true;
            }
            else if (success == 8)
            {
                errorMsg = string.Format(ResourceHelper.GetResource("NoticeNoChargeRecords"), errorMsg);
                msg = "CreatingNotice: " + errorMsg + " " + DateTime.Now;
                //item.Status = QueueItemStatus.PostPone;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
                failed = true;
            }
            else if (success == -1)
            {
                //msg = "CreatingNotice: + errorMsg + " " + DateTime.Now;
                msg = "CreatingNotice: (frameIntNo:" + frameIntNo + ") " + errorMsg + " " + DateTime.Now;
                //item.Status = QueueItemStatus.PostPone;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
                failed = true;
            }
            else if (success == -2)
            {
                //msg = ResourceHelper.GetResource("DuplicateNumberPlates");
                msg = string.Format(ResourceHelper.GetResource("DuplicateNumberPlates"), frameIntNo);
                //item.Status = QueueItemStatus.Discard;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.Discard);
                failed = true;
            }
            else if (success == -3)
            {
                //msg = ResourceHelper.GetResource("NoticeAlreadyExists");
                msg = string.Format(ResourceHelper.GetResource("NoticeAlreadyExists"), frameIntNo);
                item.Status = QueueItemStatus.Discard;
                item.IsSuccessful = false;
                Logger.Info(msg);
                failed = true;
            }
            else if (success == -4)
            {
                //msg = ResourceHelper.GetResource("NoFineAmount");
                msg = string.Format(ResourceHelper.GetResource("NoFineAmount"), frameIntNo);
                //item.Status = QueueItemStatus.PostPone;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
                failed = true;
            }
            else if (success == -5)
            {
                msg = ResourceHelper.GetResource("InsertNoticeFailed", frameIntNo);
                //item.Status = QueueItemStatus.PostPone;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
                failed = true;
            }
            //jerry 2012-03-28 add
            else if (success == -6)
            {
                msg = ResourceHelper.GetResource("NoticeNotEligible", frameIntNo);
                item.Status = QueueItemStatus.Discard;
                item.IsSuccessful = false;
                Logger.Info(msg);
                failed = true;
            }
            //jerry 2012-03-29 add
            else if (success == -7)
            {
                msg = ResourceHelper.GetResource("NoticeFilmLocked", frameIntNo);
                item.Status = QueueItemStatus.UnKnown;
                item.IsSuccessful = false;
                Logger.Info(msg);
                failed = true;
            }

            return failed;
        }

        private const int STATUS_PROSECUTE_FIRST = 600;
        private const int STATUS_PROSECUTE_LAST = 800;
        private const int STATUS_LOADED = 5;
        private const int STATUS_NO_FINE = 6;
        private const int STATUS_EXPIRED = 921;
        private const int STATUS_TICKETS = 10;
        private const int STATUS_NO_AOG = 500;
        private const int STATUS_NOTICE = 900;
        private const string CTYPE = "CAM";
        private const string NT_CODE = "6";
        private const int STATUS_AMOUNT_ADDED = 7;

        private int AddNoticeChargeFromFrame(int frameIntNo, ref string errMsg)
        {
            DBHelper db = new DBHelper(connectStr);
            SqlParameter[] paras = new SqlParameter[16];
            paras[0] = new SqlParameter("@AutIntNo", AutIntNo);
            paras[1] = new SqlParameter("@FrameIntNo", frameIntNo);
            paras[2] = new SqlParameter("@StatusLoaded", STATUS_LOADED);
            paras[3] = new SqlParameter("@StatusNoFine", STATUS_NO_FINE);
            paras[4] = new SqlParameter("@StatusExpired", STATUS_EXPIRED);
            paras[5] = new SqlParameter("@StatusNotice", STATUS_NOTICE);
            paras[6] = new SqlParameter("@StatusAmountAdded", STATUS_AMOUNT_ADDED);
            paras[7] = new SqlParameter("@PrintFileName", this.batchFilename) { Direction = ParameterDirection.InputOutput, Size = 100 };
            paras[8] = new SqlParameter("@CType", CTYPE);
            //paras[9] = new SqlParameter("@LastUser", Process.GetCurrentProcess().ProcessName);
            //Oscar 20120405 change to AARTOBase.lastUser
            paras[9] = new SqlParameter("@LastUser", AARTOBase.LastUser);
            //paras[10] = new SqlParameter("@Success", 0);
            paras[10] = new SqlParameter("@NatisLast", rules.Rule("0400").ARString == "Y");
            paras[11] = new SqlParameter("@NoOfDaysIssue", rules.Rule("NotOffenceDate", "NotIssue1stNoticeDate").DtRNoOfDays);
            paras[12] = new SqlParameter("@NoOfDaysPayment", rules.Rule("NotOffenceDate", "NotPaymentDate").DtRNoOfDays);
            paras[13] = new SqlParameter("@DataWashingActived", DataWashingActived);
            paras[14] = new SqlParameter("@IsUseDataWashingAddr", IsUseDataWashingAddr);
            paras[15] = new SqlParameter("@DataWashingRecycleMonth", DataWashingRecycleMonth);

            //paras[10].Direction = ParameterDirection.Output;

            //db.ExecuteNonQuery("NoticeChargeAddFromFrame_WS", paras, out errMsg);
            object tempSuccess = db.ExecuteScalar("NoticeChargeAddFromFrame_JMPD", paras, out errMsg);

            // Oscar 20120322 add print file name
            if (paras[7].Value != null && !string.IsNullOrWhiteSpace(paras[7].Value.ToString()))
                this.batchFilename = paras[7].Value.ToString();
            int success = Convert.ToInt32(tempSuccess);
            return success;
        }

        private bool CreateTicket(int frameIntNo, ref string msg)
        {
            bool failed = false;

            NoticeNumbers notice = new NoticeNumbers(connectStr);
            string error = "";
            ImageProcesses imgProc = new ImageProcesses(connectStr);
            ScanImageDB scan = new ScanImageDB(connectStr);

            //get all charges 
            NoticeDB not = new NoticeDB(connectStr);
            ChargeDB charge = new ChargeDB(connectStr);

            NotIntNo = not.GetNoticeByFrame(frameIntNo);
            //TicketNo ticket = notice.GenerateNoticeNumber(Process.GetCurrentProcess().ProcessName, NT_CODE, AutIntNo);
            //Oscar 20120405 change to AARTOBase.lastUser
            TicketNo ticket = notice.GenerateNoticeNumber(AARTOBase.LastUser, NT_CODE, AutIntNo);
            ticket.NotIntNo = NotIntNo;

            if (ticket.NotTicketNo.Equals("0")
                // 2015-02-26, Oscar added (1868, ref 1866)
                || ticket.ErrorCode < 0
                || !string.IsNullOrWhiteSpace(ticket.Error))
            {
                // 2015-02-26, Oscar added. (1868, ref 1866)
                var errorMsg = ticket.Error;
                switch (ticket.ErrorCode)
                {
                    case -1:
                        errorMsg = ResourceHelper.GetResource("InvalidValueOfAuthorityRule", "9300");
                        break;
                    case -2:
                        errorMsg = ResourceHelper.GetResource("GettingNoticeTypeFailed");
                        break;
                    case -3:
                        errorMsg = ResourceHelper.GetResource("AddingNoticePrefixFailed");
                        break;
                    case -4:
                        errorMsg = ResourceHelper.GetResource("ResettingTranNumberFailed");
                        break;
                    case -5:
                        errorMsg = ResourceHelper.GetResource("UpdatingNoticePrefixFailed");
                        break;
                    case -6:
                        errorMsg = ResourceHelper.GetResource("GettingAuthorityFailed");
                        break;
                    case -7:
                        errorMsg = ResourceHelper.GetResource("GettingTranNumberFailed");
                        break;
                }

                error = ResourceHelper.GetResource("ErrorGeneratingNoticeNumber");
                //msg = "CreateTickets: " + error + " " + DateTime.Now.ToString();
                // 2015-02-26, Oscar changed. (1868, ref 1866)
                msg = string.Format("CreateTickets:  {0}; {1}", error, errorMsg);
                failed = true;
                return failed;
            }

            char cTemp = Convert.ToChar("0");

            if (ticket.NPrefix.Equals("0"))
            {
                error = ResourceHelper.GetResource("ErrorGeneratingPayNumber");
                msg = "CreateTicket: " + error + " " + DateTime.Now.ToString();
                failed = true;
                return failed;
            }

            //update each charge and status
            //int updNotIntNo = not.UpdateNoticeTicketNo(ticket, STATUS_TICKETS, STATUS_AMOUNT_ADDED, STATUS_NO_AOG, Process.GetCurrentProcess().ProcessName);
            //Oscar 20120405 change to AARTOBase.lastUser
            int updNotIntNo = not.UpdateNoticeTicketNo(ticket, STATUS_TICKETS, STATUS_AMOUNT_ADDED, STATUS_NO_AOG, AARTOBase.LastUser, out msg);

            //if (updNotIntNo < 0)
            if (updNotIntNo <= 0)   //Oscar 20120704 change to <=0
                failed = true;

            return failed;
        }

        private string GetNewPrintFileName(string prefix, string dateFormat)
        {
            this.stamp = DateTime.Now;
            if (this.stamp.Subtract(this.lastStamp).Seconds <= 0)
                this.stamp = this.lastStamp.AddSeconds(1);
            this.lastStamp = this.stamp;
            return string.Format("{0}-{1}", prefix, this.stamp.ToString(dateFormat));
        }

        private bool SetHWO1stNoticePrintStatus(int notIntNo, string printFileName, ref string msg)
        {
            bool failed = false;
            int success = SetHWO1stNoticePrintStatusAndFilename(notIntNo, printFileName, ref msg);

            if (success == -1)
            {
                msg = ResourceHelper.GetResource("UpdateHWO1stNoticeFailed", notIntNo);
                failed = true;
            }
            else if (success == -2)
            {
                msg = ResourceHelper.GetResource("InsertPrintFileNameFailed", notIntNo);
                failed = true;
            }
            else if (success == -3)
            {
                msg = ResourceHelper.GetResource("InsertPrintFileName_NoticeFailed", notIntNo);
                failed = true;
            }
            return failed;
        }

        private int SetHWO1stNoticePrintStatusAndFilename(int notIntNo, string printFileName, ref string errMsg)
        {
            DBHelper db = new DBHelper(connectStr);
            SqlParameter[] paras = new SqlParameter[3];
            paras[0] = new SqlParameter("@NotIntNo", notIntNo);
            paras[1] = new SqlParameter("@LastUser", AARTOBase.LastUser);
            //paras[2] = new SqlParameter("@Success", 0);
            paras[2] = new SqlParameter("@PrintFileName", printFileName);

            //paras[2].Direction = ParameterDirection.Output;
            //db.ExecuteNonQuery("SetHWO1stNoticePrintStatusAndFilename_WS", paras, out errMsg);

            //int success = Convert.ToInt32(paras[2].Value);
            var obj = db.ExecuteScalar("SetHWO1stNoticePrintStatusAndFilename_WS", paras, out errMsg);
            int success = 0;
            if (obj != null)
                int.TryParse(obj.ToString(), out success);
            return success;
        }

        DateTime printActionDate = DateTime.Now;
        private void SetServiceParameters()
        {
            int delayHours = 0;
            if (ServiceParameters != null
                && ServiceParameters.ContainsKey("DelayActionDate_InHours")
                && !string.IsNullOrEmpty(ServiceParameters["DelayActionDate_InHours"]))
                delayHours = Convert.ToInt32(ServiceParameters["DelayActionDate_InHours"]);
            this.printActionDate = DateTime.Now.AddHours(delayHours);
        }

    }
}

