﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.NoticeGenerate_JMPD_ExportToNTI
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.NoticeGenerate_JMPD_ExportToNTI"
                ,
                DisplayName = "SIL.AARTOService.NoticeGenerate_JMPD_ExportToNTI"
                ,
                Description = "SIL.AARTOService.NoticeGenerate_JMPD_ExportToNTI"
            };

            ProgramRun.InitializeService(new NoticeGenerate_JMPD_ExportToNTI(), serviceDescriptor, args);
        }
    }
}
