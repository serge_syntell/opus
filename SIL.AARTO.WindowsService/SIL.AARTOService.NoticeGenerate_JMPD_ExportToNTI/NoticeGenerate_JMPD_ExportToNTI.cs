﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.NoticeGenerate_JMPD_ExportToNTI
{
    partial class NoticeGenerate_JMPD_ExportToNTI : ServiceHost
    {
        public NoticeGenerate_JMPD_ExportToNTI()
            : base("", new Guid("35AFCF9B-BB47-4FF7-AFAA-39D369117C5D"), new NoticeGenerate_JMPD_ExportToNTIService())
        {
            InitializeComponent();
        }
    }
}
