﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CIP_CofCT
{
    partial class CIP_CofCT : ServiceHost
    {
        public CIP_CofCT()
            : base("", new Guid("650CBCE3-E25F-44A7-A010-416E772ABD5D"), new CIP_CofCTService())
        {
            InitializeComponent();
        }
    }
}
