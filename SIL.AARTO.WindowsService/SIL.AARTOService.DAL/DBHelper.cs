﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace SIL.AARTOService.DAL
{
    public class DBHelper
    {
        string connStr;

        public DBHelper(string connStr)
        {
            this.connStr = connStr;
        }

        public object ExecuteScalar(string spName, SqlParameter[] parameters, out string errorMsg)
        {
            object result = null;
            errorMsg = string.Empty;
            if (string.IsNullOrWhiteSpace(spName)) return result;

            using (SqlConnection conn = new SqlConnection(this.connStr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(spName, conn) { CommandType = CommandType.StoredProcedure })
                    {
                        if (parameters != null && parameters.Length > 0)
                            cmd.Parameters.AddRange(parameters);

                        if (conn.State != ConnectionState.Open)
                            conn.Open();
                        result = cmd.ExecuteScalar();
                    }
                }
                catch (Exception ex)
                {
                    //log add ex
                    errorMsg = ex.Message;
                }
                finally
                {
                    conn.Close();
                }
            }
            return result;
        }

        public int ExecuteNonQuery(string spName, SqlParameter[] parameters, out string errorMsg)
        {
            errorMsg = string.Empty;
            int result = -1;
            if (string.IsNullOrWhiteSpace(spName)) return result;

            using (SqlConnection conn = new SqlConnection(this.connStr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(spName, conn) { CommandType = CommandType.StoredProcedure })
                    {
                        if (parameters != null && parameters.Length > 0)
                            cmd.Parameters.AddRange(parameters);

                        if (conn.State != ConnectionState.Open)
                            conn.Open();
                        result = cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    //log add ex
                    errorMsg = ex.Message;
                }
                finally
                {
                    conn.Close();
                }
            }
            return result;
        }

        public DataSet ExecuteDataSet(string spName, SqlParameter[] parameters, out string errorMsg)
        {
            DataSet result = null;
            errorMsg = string.Empty;
            if (string.IsNullOrWhiteSpace(spName)) return result;

            using (SqlConnection conn = new SqlConnection(this.connStr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(spName, conn) { CommandType = CommandType.StoredProcedure })
                    {
                        if (parameters != null && parameters.Length > 0)
                            cmd.Parameters.AddRange(parameters);

                        if (conn.State != ConnectionState.Open)
                            conn.Open();
                        using (SqlDataAdapter dr = new SqlDataAdapter(cmd))
                        {
                            result = new DataSet();
                            dr.Fill(result);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //log add ex
                    errorMsg = ex.Message;
                }
                finally
                {
                    conn.Close();
                }
            }
            return result;
        }

        public static T GetDataRowValue<T>(DataRow row, string columnName, object alternateValue = null)
        {
            if (row[columnName] == null || row[columnName] == DBNull.Value)
            {
                if (alternateValue == null)
                    return default(T);
                else
                    return (T)Convert.ChangeType(alternateValue, typeof(T));
            }
            else
                return (T)Convert.ChangeType(row[columnName], typeof(T));
        }
    }
}
