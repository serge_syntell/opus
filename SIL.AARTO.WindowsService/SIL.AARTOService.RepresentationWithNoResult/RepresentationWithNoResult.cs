﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.RepresentationWithNoResult
{
    partial class RepresentationWithNoResult : ServiceHost
    {
        public RepresentationWithNoResult()
            : base("", new Guid("91FB661F-287E-4CE6-AB23-1FBCC26E9627"), new RepresentationWithNoResultService())
        {
            InitializeComponent();
        }
    }
}
