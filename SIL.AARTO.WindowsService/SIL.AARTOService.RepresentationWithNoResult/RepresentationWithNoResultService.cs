﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using System.IO;
using System.Reflection;
using CrystalDecisions.CrystalReports.Engine;
using Stalberg.TMS;
using Stalberg.TMS.Data.Datasets;
using CrystalDecisions.Shared;
using SIL.AARTOService.Resource;

namespace SIL.AARTOService.RepresentationWithNoResult
{
    public class RepresentationWithNoResultService : ClientService
    {
        // Constants
        private const string REPORT_NAME = "Representations With No Results Report";
        private const int START_YEAR = 2012;
        public RepresentationWithNoResultService()
            : base("", "", new Guid("D6154289-0D00-46C1-AA6B-5EC7EE3B4003"))
        {
            this._serviceHelper = new AARTOServiceBase(this);
            //get connect string
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);

            AARTOBase.OnServiceStarting = () =>
            {
                repDB = new RepresentationDB(connectStr);
            };
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        string connectStr = string.Empty;
        string emailToAdministrator = string.Empty;
        RepresentationDB repDB = null;
        public override void InitialWork(ref QueueItem item)
        {
            //get to email address
            GetServiceParameters();
        }

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {

            //if (this.emailToAdministrator.Equals(string.Empty))
            //{

            //    StopService();
            //    return;
            //}
            for (int year = START_YEAR; year <= DateTime.Now.Year; year++)
            {
                ProcessReport(year);
            }

            #region
            /* Jake comment out
            //get file name
            string fileName = Path.Combine(Path.GetTempPath(), string.Format("{0}.pdf", REPORT_NAME));
            FileInfo file = new FileInfo(fileName);
            if (file.Exists)
                file.Delete();
            file = null;

            // Create a report object
            //Jerry 2012-06-29 get report file from Report folder
            //string reportFile = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Reports/RepresentationsWithNoResults.rpt");
            string reportFile = Path.Combine(AARTOBase.GetConnectionString(ServiceConnectionNameList.ReportFolder, ServiceConnectionTypeList.UNC), "RepresentationsWithNoResults.rpt");

            if (!File.Exists(reportFile))
            {
                //this.Logger.Error(ResourceHelper.GetResource("ErrorRepresentationWithNoResultNoExists"));
                //StopService();
                AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorRepresentationWithNoResultNoExists"), true);
                return;
            }

            // Set the report's data source
            RepresentationDB db = new RepresentationDB(connectStr);
            dsRepresentationsWithNoResults ds = db.RepresentationsWithNoResults(0);

            if (ds.Tables[1].Rows.Count > 0)
            {
                ReportDocument report = new ReportDocument();
                report.Load(reportFile);

                report.SetDataSource(ds.Tables[1]);

                // Set up export options
                DiskFileDestinationOptions fileOptions = new DiskFileDestinationOptions();
                fileOptions.DiskFileName = fileName;

                ExportOptions exportOptions = report.ExportOptions;
                exportOptions.DestinationOptions = fileOptions;
                exportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                exportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;

                // Export the PDF file
                report.Export();

                string message = ResourceHelper.GetResource("EmailMsgInRepWithNoResult");
                string subject = REPORT_NAME;
                List<string> attachments = new List<string>();
                attachments.Add(fileName);

                //send email
                try
                {
                    SendEmailManager.SendEmail(this.emailToAdministrator, subject, message, attachments);
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ex);
                }
            }
            ds.Dispose();
            */
            #endregion

            MustSleep = true;// jerry 2012-02-24 change
        }

        void ProcessReport(int year)
        {
            try
            {
                string fileName = Path.Combine(Path.GetTempPath(), string.Format("{0}.pdf", String.Format(REPORT_NAME + "_{0}", year)));
                FileInfo file = new FileInfo(fileName);
                if (file.Exists)
                    file.Delete();
                file = null;

                // Create a report object
                //Jerry 2012-06-29 get report file from Report folder
                //string reportFile = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Reports/RepresentationsWithNoResults.rpt");
                string reportFile = Path.Combine(AARTOBase.GetConnectionString(ServiceConnectionNameList.ReportFolder, ServiceConnectionTypeList.UNC), "RepresentationsWithNoResults.rpt");

                if (!File.Exists(reportFile))
                {
                    //this.Logger.Error(ResourceHelper.GetResource("ErrorRepresentationWithNoResultNoExists"));
                    //StopService();
                    AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorRepresentationWithNoResultNoExists"), true);
                    return;
                }

                // Set the report's data source
                dsRepresentationsWithNoResults ds = repDB.RepresentationsWithNoResults(0, year);

                if (ds.Tables[1].Rows.Count > 0)
                {
                    ReportDocument report = new ReportDocument();
                    report.Load(reportFile);

                    report.SetDataSource(ds.Tables[1]);

                    // Set up export options
                    DiskFileDestinationOptions fileOptions = new DiskFileDestinationOptions();
                    fileOptions.DiskFileName = fileName;

                    ExportOptions exportOptions = report.ExportOptions;
                    exportOptions.DestinationOptions = fileOptions;
                    exportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    exportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;

                    // Export the PDF file
                    report.Export();

                    string message = ResourceHelper.GetResource("EmailMsgInRepWithNoResult");
                    string subject = REPORT_NAME;
                    List<string> attachments = new List<string>();
                    attachments.Add(fileName);

                    //send email
                    try
                    {
                        SendEmailManager.SendEmail(this.emailToAdministrator, subject, message, attachments);
                    }
                    catch (Exception ex)
                    {
                        AARTOBase.ErrorProcessing(ex);
                    }
                }
                ds.Dispose();
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex, LogType.Error);
            }
        }

        private void GetServiceParameters()
        {
            if (ServiceParameters != null)
            {
                if (string.IsNullOrEmpty(this.emailToAdministrator)
&& ServiceParameters.ContainsKey("EmailToAdministrator") && !string.IsNullOrEmpty(ServiceParameters["EmailToAdministrator"]))
                {
                    this.emailToAdministrator = ServiceParameters["EmailToAdministrator"];
                    if (!SendEmailManager.CheckEmailAddress(this.emailToAdministrator))
                    {
                        //this.Logger.Error(ResourceHelper.GetResource("ErrorEmailAddrInRepresentationWithNoResult"));
                        //StopService();
                        AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorEmailAddrInRepresentationWithNoResult"), true);
                    }
                }
                else if (string.IsNullOrEmpty(this.emailToAdministrator) || !ServiceParameters.ContainsKey("EmailToAdministrator") || string.IsNullOrEmpty(ServiceParameters["EmailToAdministrator"]))
                {
                    //this.Logger.Error(ResourceHelper.GetResource("ErrorNoEmailInRepresentationWithNoResult"));
                    //StopService();
                    AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorNoEmailInRepresentationWithNoResult"), true);
                }
            }
            else
            {
                //this.Logger.Error(ResourceHelper.GetResource("ErrorNoEmailInRepresentationWithNoResult"));
                //StopService();
                AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorNoEmailInRepresentationWithNoResult"), true);
            }
        }

        //private void StopService()
        //{
        //    this.MustStop = true;
        //    ((SIL.ServiceLibrary.ServiceHost)this.ServiceHost).Stop();
        //}
    }
}
