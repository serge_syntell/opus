﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.AARTOService.Library;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using SIL.AARTO.BLL.Utility.PrintFile;
using Stalberg.TMS;
using SIL.AARTOService.Resource;
using System.IO;
using Stalberg.TMS.Data;
using SIL.AARTO.BLL.PostalManagement;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Model;
namespace SIL.AARTOService.PrintFirstNotice
{
    public class PrintFirstNoticeClientService : ServiceDataProcessViaQueue
    {
        public PrintFirstNoticeClientService()
            : base("", "", new Guid("04935CCF-02A4-4FBD-A965-8E3372767D40"), ServiceQueueTypeList.Print1stNotice)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.connUNC_Export = AARTOBase.GetConnectionString(ServiceConnectionNameList.PrintFilesFolder, ServiceConnectionTypeList.UNC);
            AARTOBase.OnServiceStarting = () =>
            {
                CheckImageServer(this.connAARTODB);

                if (this.fileList.Count > 0)
                    this.fileList.Clear();
            };
            AARTOBase.OnServiceSleeping = OnServiceSleeping;
            process = new PrintFileProcess(this.connAARTODB, AARTOBase.LastUser);
            process.ErrorProcessing = (s) => { this.Logger.Error(s); };
            queueProcessor = new QueueItemProcessor();
           

            // 2014-12-02, Oscar added (bontq 1726, ref 1725)
            noticeDB = new NoticeDB(this.connAARTODB);
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        SIL.AARTOService.Library.AARTORules rules = SIL.AARTOService.Library.AARTORules.GetSingleTon();
        PrintFileProcess process;
        QueueItemProcessor queueProcessor;
        AuthorityDetails authDetails = null;
     
        string connAARTODB;
        string connUNC_Export;
        string currentAutCode, lastAutCode;
        string exportPath;
        int autIntNo;
        int pfnIntNo;
        //int noOfDaysToSMSExtractOn2ndNoticeDate;
        //int daysPosted1stTo2ndNoticeDate;
        string emailToAdministrator = string.Empty;
        List<string> fileList = new List<string>();
        string autTicketProcessor;
        readonly int statusLoaded = 10, statusPrinted = 250;
        readonly int NAG_STATUS = 500, NAG_PRINTED_STATUS = 510, EXPIRED_STATUS = 921;
        //int notIntNo;
        //bool SecNotice=false;
        // 2014-12-02, Oscar added (bontq 1726, ref 1725)
        readonly NoticeDB noticeDB;

        public override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;

            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    //2014-12-18 Heidi comment out becaust it has push queue after set posted status(bontq1765)
                    //if (!int.TryParse(body, out this.notIntNo))
                    //{
                    //    AARTOBase.ErrorProcessing(ref item);
                    //    AARTOBase.DeQueueInvalidItem(ref item);
                    //    return;
                    //}
                    
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;
                    SetServiceParameters();
                    if (!ServiceUtility.IsNumeric(body))
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        return;
                    }
                    this.pfnIntNo = Convert.ToInt32(body.Trim());

                    string groupStr = item.Group.Trim();
                    string[] group = groupStr.Split('|');
                    this.currentAutCode = group[0].Trim();

                    if (this.lastAutCode != this.currentAutCode)
                    {
                        //this.pfnIntNo = Convert.ToInt32(body.Trim());
                        QueueItemValidation(ref item);
                        //Jake 2014-12-10 added InitParameter here
                        //rules.InitParameter(this.connAARTODB, this.currentAutCode);
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex.Message, true);
                }
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            string msg;
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                if (!item.IsSuccessful) continue;
                try
                {
                    //2014-12-18 Heidi comment out code becaust it has push queue after set posted status(bontq1765)
                   // this.SecNotice = rules.Rule("4250").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase);

                   //DateRuleInfo dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotPosted1stNoticeDate", "NotIssue2ndNoticeDate");
                   //this.daysPosted1stTo2ndNoticeDate = dateRule.ADRNoOfDays;

                   //dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotIssue2ndNoticeDate", "SMSExtractOn2ndNoticeDate");
                   //this.noOfDaysToSMSExtractOn2ndNoticeDate = dateRule.ADRNoOfDays;
                   //SIL.AARTO.DAL.Entities.Notice notice = new NoticeService().GetByNotIntNo(notIntNo);

                   // Tommi 20141029 add PushSMSExtractOn2ndNotice
                   // QueueItem pushSMSExtractItem = new QueueItem();
                   // int frameIntNo = new NoticeDB(this.connAARTODB).GetFrameForNotice(this.notIntNo);
                   // string violationType = new FrameDB(this.connAARTODB).GetViolationType(frameIntNo);
                   // pushSMSExtractItem.Body = item.Body;
                   // pushSMSExtractItem.Group = this.currentAutCode;
                   // pushSMSExtractItem.ActDate = notice.NotOffenceDate.AddDays(daysPosted1stTo2ndNoticeDate + noOfDaysToSMSExtractOn2ndNoticeDate);
                   // pushSMSExtractItem.LastUser = AARTOBase.LastUser;
                   // pushSMSExtractItem.QueueType = ServiceQueueTypeList.SMSExtractOn2ndNotice;
                   // queueProcessor.Send(pushSMSExtractItem);  //Jake 2014-12-10 fixed a bug, pushSMSExtractItem instead of item

                    //Oscar 20120525 add exportPath switch
                    string printFileName = AARTOBase.GetPrintFileName(this.pfnIntNo, out msg);
                    string section = (printFileName ?? (printFileName = "")).Substring(0, Math.Min(11, printFileName.Length));
                    if (section.IndexOf("REG", StringComparison.OrdinalIgnoreCase) >= 0)
                        this.exportPath = AARTOBase.GetPrintFileDirectory(this.connUNC_Export, this.currentAutCode, PrintServiceType.ChangeRegNo);
                    else if (section.IndexOf("RNO", StringComparison.OrdinalIgnoreCase) >= 0)
                        this.exportPath = AARTOBase.GetPrintFileDirectory(this.connUNC_Export, this.currentAutCode, PrintServiceType.NewOffender);
                    else  // Oscar 20120704 added
                        this.exportPath = AARTOBase.GetPrintFileDirectory(this.connUNC_Export, this.currentAutCode, PrintServiceType.FirstNotice);

                    process.BuildPrintFile(new PrintFileModuleFirstNotice(), this.autIntNo, this.pfnIntNo, this.exportPath);
                    if (process.IsSuccessful)
                    {
                        this.fileList.Add(process.ExportPrintFileName);

                        // Oscar 20120510 add update status
                        int length = process.PrintFileName.Length;
                        if (length > 11) length = 11;
                        string prefix = process.PrintFileName.Substring(0, length);
                        if (prefix.IndexOf("REG", StringComparison.OrdinalIgnoreCase) < 0
                            && prefix.IndexOf("RNO", StringComparison.OrdinalIgnoreCase) < 0
                        )
                        {
                            if (!UpdateStatus(ref item, process.PrintFileName))
                                return; // 2014-12-02, Oscar changed (bontq 1726, ref 1725)
                        }
                        else
                        {
                            // Heidi 2014-10-15 added for New Offender printing process(bontq1505)
                            //NoticeDB noticeDB = new NoticeDB(this.connAARTODB);
                            //noticeDB.SetNewOffenderPrintedDate(process.PrintFileName, AARTOBase.LastUser, this.autIntNo);
                            // 2014-12-02, Oscar changed (bontq 1726, ref 1725)
                            if (!SetNewOffenderPrintedDate(item, process.PrintFileName, AARTOBase.LastUser, this.autIntNo))
                                return;
                        }

                        this.Logger.Info(ResourceHelper.GetResource("PrintFileSavedTo", process.PrintFileName, process.ExportPrintFilePath));


                        // for expired warning message
                        if (!string.IsNullOrEmpty(process.Message))
                            this.Logger.Warning(process.Message);
                    }
                    else
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ServiceHasError"), AARTOBase.LastUser, item.Body, ex.Message), true);
                }
                queueList[i] = item;
            }
        }

    
        private void QueueItemValidation(ref QueueItem item)
        {
            if (this.lastAutCode != this.currentAutCode)
            {
                this.exportPath = AARTOBase.GetPrintFileDirectory(this.connUNC_Export, this.currentAutCode, PrintServiceType.FirstNotice);

                this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim().Equals(this.currentAutCode, StringComparison.OrdinalIgnoreCase));
                if (this.authDetails != null && this.authDetails.AutIntNo > 0)
                {
                    this.autIntNo = this.authDetails.AutIntNo;
                    this.lastAutCode = this.currentAutCode;
                    this.autTicketProcessor = this.authDetails.AutTicketProcessor.ToLower();
                 
                }
                else
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.currentAutCode), false, QueueItemStatus.Discard);
                    return;
                }
          
                           }

        }

        private void OnServiceStarting()
        {
            if (this.fileList.Count > 0)
                this.fileList.Clear();
        }

        private void OnServiceSleeping()
        {
            if (this.fileList.Count > 0)
            {
                string title = ResourceHelper.GetResource("1stNotice");
                SendEmailManager.SendEmail(
                    this.emailToAdministrator,
                    ResourceHelper.GetResource("1stEmailSubjectOfPrintService", title, DateTime.Now.ToString("yyyy-MM-dd HH:mm")) ,
                    SendEmailManager.GetFileListContent(title, this.fileList));
            }
        }

        private int[] GetStatusLoadedAndPrinted()
        {
            int[] result = new int[2];
            int statusLoaded = this.statusLoaded;
            int statusPrinted = this.statusPrinted;
            //Jerry 2012-12-28 changed
            //switch (this.autTicketProcessor)
            //{
            //    case "cipruspi":
            //        statusLoaded = 210;
            //        statusPrinted = 220;
            //        break;
            //    case "cip_cofct":
            //        statusLoaded = 210;
            //        statusPrinted = 220;
            //        break;
            //    case "cip_aarto":
            //        statusLoaded = NAG_STATUS;
            //        statusPrinted = NAG_PRINTED_STATUS;
            //        break;
            //}
            result[0] = statusLoaded;
            result[1] = statusPrinted;
            return result;
        }

        private bool UpdateStatus(ref QueueItem item, string printFileName)
        {
            int[] statusList = GetStatusLoadedAndPrinted();
            int statusLoaded = statusList[0];
            int statusPrinted = statusList[1];

            string updateType = "FirstNotice";

            //Print File Name NAG_S35 can be handed by this rule
            if (printFileName.Substring(0, 3).Equals("NAG", StringComparison.OrdinalIgnoreCase))
            {
                statusPrinted = NAG_PRINTED_STATUS;
                statusLoaded = NAG_STATUS;
                updateType = "NoAOG";
            }

            else if (printFileName.ToUpper().Substring(0, 3).Equals("EXP", StringComparison.OrdinalIgnoreCase))
            {
                statusPrinted = EXPIRED_STATUS;
                updateType = "Expired";
            }

            string errMessage = string.Empty;

            NoticeDB noticeDB = new NoticeDB(this.connAARTODB);
            int rows = noticeDB.UpdateNoticeChargeStatus(this.autIntNo, printFileName, statusLoaded, statusPrinted, updateType, AARTOBase.LastUser, ref errMessage);

            if (rows < 1)
            {
                //errMessage += errMessage.Length > 0 ? " - Error: " + errMessage : string.Empty;

                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("UnableToSetPrintStatus"));
                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("UnableToSetPrintStatus"), false, rows == 0 ? QueueItemStatus.Discard : QueueItemStatus.UnKnown);
                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("UnableToSetPrintStatus"), false, QueueItemStatus.Discard);

                // Oscar 2013-04-12 added
                if (rows == -999)
                {
                    Logger.Info(ResourceHelper.GetResource("UnableToSetPrintStatus", rows, updateType, ResourceHelper.GetResource("ThisNoticeHasAlreadyBeenPrinted")));
                    return true;
                }

                // Oscar 2013-03-18 changed for error message
                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("UnableToSetPrintStatus", rows, updateType, errMessage), false, QueueItemStatus.Discard);
                // 2014-12-02, Oscar changed (bontq 1726, ref 1725)
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("UnableToSetPrintStatus", rows, updateType, errMessage));
                return false;
            }
            else
            {
                if (updateType != "Expired")
                {
                    AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = autIntNo;
                    arDetails.ARCode = "4200";
                    arDetails.LastUser = AARTOBase.LastUser;
                    DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connAARTODB);
                    bool isPostDate = ar.SetDefaultAuthRule().Value.Equals("Y");

                    if (isPostDate)
                    {
                        PostDateNotice postDate = new PostDateNotice(this.connAARTODB);
                        rows = postDate.SetNoticePostedDate(printFileName, autIntNo, DateTime.Now.AddDays(arDetails.ARNumeric), AARTOBase.LastUser, ref errMessage);
                        if (rows < 1)
                        {
                            //AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("UnableToSetPostDate"), printFileName, errMessage), false, QueueItemStatus.Discard);
                            // 2014-12-02, Oscar changed (bontq 1726, ref 1725)
                            AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("UnableToSetPostDate"), printFileName, errMessage));
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private void SetServiceParameters()
        {
            if (ServiceParameters != null
                && string.IsNullOrEmpty(this.emailToAdministrator)
                && ServiceParameters.ContainsKey("EmailToAdministrator")
                && !string.IsNullOrEmpty(ServiceParameters["EmailToAdministrator"]))
                this.emailToAdministrator = ServiceParameters["EmailToAdministrator"];
            if (!SendEmailManager.CheckEmailAddress(this.emailToAdministrator))
                AARTOBase.ErrorProcessing(ResourceHelper.GetResource("EmailAddressError"), true);
        }

        void CheckImageServer(string connStr)
        {
            try
            {
                var imageFileServerDB = new ImageFileServerDB(connStr);
                var fileServerList = imageFileServerDB.GetAllImageFileServers();
                if (fileServerList.Count > 0)
                {
                    foreach (var imageFileServer in fileServerList)
                    {
                        // a temp check, until we find a solution for robot.
                        if (string.IsNullOrWhiteSpace(imageFileServer.ImageShareName)) continue;

                        string strRootPath = @"\\" + imageFileServer.ImageMachineName
                                             + Path.DirectorySeparatorChar + imageFileServer.ImageShareName;

                        string strTestImagePath = strRootPath + Path.DirectorySeparatorChar + "test.jpg";

                        if (!File.Exists(strTestImagePath))
                        {
                            AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ImageFileServerNotAccessible", strRootPath), true);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AARTOBase.ErrorProcessing(ex);
            }
        }

        // 2014-12-02, Oscar changed (bontq 1726, ref 1725)
        bool SetNewOffenderPrintedDate(QueueItem item, string printFileName, string lastUser, int autIntNo)
        {
            try
            {
                noticeDB.SetNewOffenderPrintedDate(printFileName, lastUser, autIntNo);
                return true;
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("SettingNewOffenderPrintedDateFailed", ex.ToString()), LogType.Error, ServiceOption.Break, item);
            }

            return false;
        }
    }
}