﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PrintFirstNotice
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.PrintFirstNotice"
                ,
                DisplayName = "SIL.AARTOService.PrintFirstNotice"
                ,
                Description = "SIL AARTOService PrintFirstNotice"
            };

            ProgramRun.InitializeService(new PrintFirstNotice(), serviceDescriptor, args);
        }
    }
}
