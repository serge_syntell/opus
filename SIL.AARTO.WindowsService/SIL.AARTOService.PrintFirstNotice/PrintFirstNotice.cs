﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PrintFirstNotice
{
    partial class PrintFirstNotice : ServiceHost
    {
        public PrintFirstNotice()
            : base("", new Guid("178C50F9-6E6D-4FB7-840C-3380FB4BF615"), new PrintFirstNoticeClientService())
        {
            InitializeComponent();
        }
    }
}
