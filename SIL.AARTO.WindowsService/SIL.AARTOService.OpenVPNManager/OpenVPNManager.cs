﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.OpenVPNManager
{
    partial class OpenVPNManager : ServiceHost
    {
        public OpenVPNManager() : base("", new Guid("B6D1B4B6-A086-4682-82F9-4F60BEEC1CD1"), new OpenVPNManagerService())
        {
            InitializeComponent();
        }
    }
}
