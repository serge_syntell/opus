﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using SIL.AARTOService.Library;
using SIL.OpenVpnAutomation;
using SIL.QueueLibrary;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using LogType = SIL.OpenVpnAutomation.LogType;
using SIL.ServiceBase;

namespace SIL.AARTOService.OpenVPNManager
{
    class OpenVPNManagerService : ClientService
    {
        public OpenVPNManagerService() : base("", "", new Guid("C34E76EF-FE02-4818-8E49-33B463F2E605"))
        {
            _serviceHelper = new AARTOServiceBase(this);
            var ntiConnString = serviceBase.GetConnectionString(ServiceConnectionNameList.SIL_NTI, ServiceConnectionTypeList.DB);
            ServiceUtility.InitializeNetTier(ntiConnString, "SIL.eNaTIS");

            serviceBase.OnServiceStarting = () =>
            {
                try
                {
                    var hostPort = int.Parse(serviceBase.GetServiceParameter("HostPort"));


                    double establishTimeoutMins = 15, clientTimeoutMins = 360, tmpTimeout;

                    if (double.TryParse(serviceBase.GetServiceParameter("EstablishConnectionTimeoutMins"), out tmpTimeout))
                        establishTimeoutMins = tmpTimeout;

                    if (double.TryParse(serviceBase.GetServiceParameter("ClientTimeoutMins"), out tmpTimeout))
                        clientTimeoutMins = tmpTimeout;


                    if (manager == null)
                        manager = OpenVpnManager.CreateManger(IPAddress.Loopback.ToString(), hostPort, establishTimeoutMins, clientTimeoutMins, OnLogging);

                    manager.Start();
                }
                catch (Exception ex)
                {
                    serviceBase.ErrorProcessing(ex);
                }
            };
            serviceBase.OnServiceSleeping = () =>
            {

            };
        }

        OpenVpnManager manager;

        AARTOServiceBase serviceBase
        {
            get { return (AARTOServiceBase)_serviceHelper; }
        }

        void OnLogging(object sender, LogEventArgs e)
        {
            switch (e.LogType)
            {
                case LogType.Info:
                    if (e.Exception != null)
                        Logger.Info(e.Exception);
                    else
                        Logger.Info(e.Message);
                    break;

                case LogType.Warning:
                    if (e.Exception != null)
                        Logger.Warning(e.Exception);
                    else
                        Logger.Warning(e.Message);
                    break;

                case LogType.Error:
                    if (e.Exception != null)
                        Logger.Error(e.Exception);
                    else
                        Logger.Error(e.Message);
                    break;
            }
        }

        //DateTime? lastStamp;
        public override void MainWork(ref List<QueueItem> queueList)
        {
            //var now = DateTime.Now;
            //if (!lastStamp.HasValue)
            //{
            //    lastStamp = now;
            //    Logger.Info(string.Format("[{0}]: log starts", now.ToString("HH:mm:ss fff")));
            //}
            //else
            //{
            //    var stamp = now - lastStamp;
            //    lastStamp = now;
            //    Logger.Info(string.Format("[{0}]: record last {1} log.", now.ToString("HH:mm:ss fff"), stamp.Value.ToString("g")));
            //}

            MustSleep = true;
        }
    }
}
