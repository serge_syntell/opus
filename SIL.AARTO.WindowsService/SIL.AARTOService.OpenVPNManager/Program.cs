﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.OpenVPNManager
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ProgramRun.InitializeService(new OpenVPNManager(), new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.OpenVPNManager",
                DisplayName = "SIL.AARTOService.OpenVPNManager",
                Description = "SIL AARTOService OpenVPNManager"
            }, args);
        }
    }
}
