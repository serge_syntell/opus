﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using SIL.AARTOService.Resource;
using Stalberg.TMS;

namespace SIL.AARTOService.SummonsLock
{
    public class SummonsLockService : ServiceDataProcessViaQueue
    {
        public SummonsLockService()
            : base("", "", new Guid("267ADDA5-37E8-4F33-B31C-636FCD49535D"), ServiceQueueTypeList.SummonsLock)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
        }

        private const string TICKET_PROCESSOR_TMS = "TMS";
        private const string TICKET_PROCESSOR_AARTO = "AARTO";
        private const string TICKET_PROCESSOR_JMPD_AARTO = "JMPD_AARTO";

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        AuthorityDetails authDetails = null;
        string connectStr = string.Empty;
        string strAutCode = string.Empty;
        int autIntNo;
        string autName = string.Empty;
        int noOfDaysCourtRollCaptured;

        public sealed override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && ServiceUtility.IsNumeric(body))
            {
                item.IsSuccessful = true;
            }
            else
            {
                // jerry 2012-02-21 change
                //item.IsSuccessful = false;
                //item.Status = QueueItemStatus.UnKnown;
                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.lastUser, item.Body));
                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.LastUser, item.Body), true);
            }

            if (strAutCode != item.Group)
            {
                strAutCode = item.Group.Trim();
                //jerry 2012-04-05 check group value
                if (string.IsNullOrWhiteSpace(strAutCode))
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
                    return;
                }

                this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode);
                autIntNo = authDetails.AutIntNo;
                autName = authDetails.AutName;

                //modified by linda 2012-12-21(replace AutTicketProcessor by NotTicketProcessor)
                int sumIntNo = Convert.ToInt32(item.Body);
                SIL.AARTO.DAL.Entities.NoticeSummons noticeSummons = new SIL.AARTO.DAL.Services.NoticeSummonsService().GetBySumIntNo(sumIntNo).FirstOrDefault();
                SIL.AARTO.DAL.Entities.Notice notice = new SIL.AARTO.DAL.Entities.Notice();
                if (noticeSummons != null)
                {
                    notice = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(noticeSummons.NotIntNo);
                }  

                if (notice.NotTicketProcessor != TICKET_PROCESSOR_TMS)
                {                
                    item.Status = QueueItemStatus.Discard;
                    item.IsSuccessful = false;
                    this.Logger.Warning(String.Format("Invalid notice ticket processor {0} ; Queue key:{1}", notice.NotTicketProcessor, item.Body));
                }

                rules.InitParameter(connectStr, strAutCode);
            }
        }

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            int sumIntNo, lockSet;
            string msg = string.Empty;

            // jerry 2012-02-21 change
            //foreach (QueueItem item in queueList)
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                try
                {
                    if (item.IsSuccessful)
                    {
                        sumIntNo = Convert.ToInt32(item.Body);

                        //get authority rules
                        noOfDaysCourtRollCaptured = rules.Rule("6100").ARNumeric;

                        lockSet = SetSummonsLock(sumIntNo, ref msg);
                        if (lockSet == 1)
                        {
                            this.Logger.Info(string.Format(ResourceHelper.GetResource("SucessfulInfoInSummonsLock"), sumIntNo));
                            item.Status = QueueItemStatus.Discard;
                        }
                        //jerry 2012-03-05 change it
                        else if (lockSet == 2)
                        {
                            this.Logger.Info(string.Format(ResourceHelper.GetResource("SucessfulInfoInSummonsUnLock"), sumIntNo));
                            item.Status = QueueItemStatus.Discard;
                        }
                        else if (lockSet == -1)
                        {
                            //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorInSummonsLockSet"), sumIntNo));
                            //item.Status = QueueItemStatus.UnKnown;
                            AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorInSummonsLockSet"), sumIntNo));
                        }
                        else if (lockSet == -2)
                        {
                            //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorInSummonsLockUnSet"), sumIntNo));
                            //item.Status = QueueItemStatus.UnKnown;
                            AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorInSummonsLockUnSet"), sumIntNo));
                        }
                        else if (lockSet == -3)
                        {
                            //this.Logger.Error(msg);
                            //item.Status = QueueItemStatus.UnKnown;
                            AARTOBase.ErrorProcessing(ref item, msg, true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //item.Status = QueueItemStatus.UnKnown;
                    //this.Logger.Error(ex);
                    //throw ex;
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        private int SetSummonsLock(int sumIntNo, ref string errMsg)
        {
            DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));

            SqlParameter[] paras = new SqlParameter[3];
            paras[0] = new SqlParameter("@SumIntNo", sumIntNo);
            paras[1] = new SqlParameter("@JudgementPeriod", noOfDaysCourtRollCaptured);
            paras[2] = new SqlParameter("@LastUser", AARTOBase.LastUser);

            object obj = db.ExecuteScalar("SummonsSetLock_WS", paras, out errMsg);
            int result;
            if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(errMsg))
                result = Convert.ToInt32(obj);
            else
                result = -3;

            return result;
        }
    }
}
