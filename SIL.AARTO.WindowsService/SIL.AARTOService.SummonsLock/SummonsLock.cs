﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.SummonsLock
{
    partial class SummonsLock : ServiceHost
    {
        public SummonsLock()
            : base("", new Guid("05089BCD-3A4D-40A2-B46D-A4EEF853728D"), new SummonsLockService())
        {
            InitializeComponent();
        }
    }
}
