﻿using SIL.ServiceLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SIL.AARTOService.RoadblockExtractor
{
    partial class RoadblockExtractor : ServiceHost
    {
        public RoadblockExtractor()
            : base("", new Guid("4432AC38-924F-4976-AE70-900E84D14C49"), new RoadblockExtractorService())
        {
            InitializeComponent();
        }
    }
}
