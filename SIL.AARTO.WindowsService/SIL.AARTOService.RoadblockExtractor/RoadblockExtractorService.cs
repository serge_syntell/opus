﻿using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Resource;
using SIL.ServiceBase;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.DAL.Entities;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;
using System.Data.SqlClient;
using Stalberg.TMS;
using Stalberg.TMS.Data;
using Stalberg.TMS_TPExInt.Components;
using System.Drawing;

namespace SIL.AARTOService.RoadblockExtractor
{
    //image file name info
    public struct imageDetail
    {
        public Int32 scImIntNoA;
        public Int32 scImIntNoB;
        public string filmNo;
        public string frameNo;
        public string imageNoA;
        public string imageNoB;
    }

    public class RoadblockExtractorService:ClientService
    {
        public RoadblockExtractorService() 
            :base("","",new Guid("952C486A-D97C-42BF-86F4-A1DA15F3093D"))
        {
            this._serviceHelper = new AARTOServiceBase(this, false);
            aartoConnectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            roadBlackFilesFolder = AARTOBase.GetConnectionString(ServiceConnectionNameList.RoadBlockFilesFolder , ServiceConnectionTypeList.UNC);

            AARTOBase.OnServiceStarting = () =>
            {
                if (string.IsNullOrEmpty(roadBlackFilesFolder))
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoParam"), roadBlackFilesFolder), LogType.Error, ServiceOption.BreakAndStop);
                    //return;
                }

                var roadBlackFilesFolderDir = new DirectoryInfo(roadBlackFilesFolder);
                if (!roadBlackFilesFolderDir.Exists)
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoRoadBlockFilesFolder"), roadBlackFilesFolderDir.FullName), LogType.Error, ServiceOption.BreakAndStop);
                    //return;
                }

                //emailToAdministrator = AARTOBase.GetServiceParameter("EmailToAdministrator");
                //if (string.IsNullOrEmpty(emailToAdministrator))
                //{
                //    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoEmailAddress"), "EmailToAdministrator"), LogType.Error, ServiceOption.BreakAndStop);
                //    return;
                //}

                generateFalseNumberImages = AARTOBase.GetServiceParameter("GenerateFalseNumberImages");
                if (string.IsNullOrEmpty(generateFalseNumberImages))
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoEmailAddress"), "GenerateFalseNumberImages"), LogType.Info, ServiceOption.Continue);
                }
            };
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        string aartoConnectStr = string.Empty;
        string roadBlackFilesFolder = string.Empty;
        //string emailToAdministrator = string.Empty;
        string generateFalseNumberImages = string.Empty;

        //Jake 2014-05-21 add code to set client service to sleep if there is no data need to process
        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            if (this.MustStop) return;
            try
            {

                //************************************************************************************************************************
                //          Check whether ALL image servers are accessible 
                //************************************************************************************************************************

                ImageFileServerDB imageDB = new ImageFileServerDB(aartoConnectStr);
                List<ImageFileServerDetails> imageServers = imageDB.GetAllImageFileServers();
                if (imageServers.Count == 0)
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ImageFileServerNotFound"), DateTime.Now.ToString()), LogType.Error, ServiceOption.BreakAndStop);
                    //return;
                }

                if (imageServers.Count > 0)
                {
                    foreach (ImageFileServerDetails server in imageServers)
                    {
                        string strTestImagePath = string.Format(@"\\{0}\{1}\test.gif", server.ImageMachineName, server.ImageShareName);
                        string strTestImagePath2 = string.Format(@"\\{0}\{1}\test.jpg", server.ImageMachineName, server.ImageShareName);

                        if (File.Exists(strTestImagePath) == false && File.Exists(strTestImagePath2) == false)
                        {
                            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("TestImagePathNotExists"), DateTime.Now.ToString()), LogType.Error, ServiceOption.BreakAndStop);
                            //return;
                        }
                        //}
                    }
                }

                //************************************************************************************************************************
                //     1.     Remove all of the expired ( older than 7 days till the date time of app execution) road block,expired road block 
                //            false images and expired( older than 7 days till the date time of app execution) road block flase number plate files 
                //************************************************************************************************************************
                RemoveFiles(roadBlackFilesFolder);

                //************************************************************************************************************************
                //     2.     RBDExtract_AARTO - Road block data extract  
                //************************************************************************************************************************
                RBDExtract_Processing();

                //************************************************************************************************************************
                //     3.     RBFNPExtract_AARTO - Road block false number plate data extract  
                //************************************************************************************************************************
                RBFNPExtract_Processing();

            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex.Message + ex.StackTrace + ex.GetBaseException(), LogType.Error, ServiceOption.RollbackAndStop);
            }
            finally {
                this.MustStop = true;
            }
        }

        #region RBDExtract
        // Constants
        private const string ROADBLOCK_PROCESSOR_RBDE = "RDBE";           //"RDBE_Aarto";
        private const string ROADBLOCK_FILE_PREFIX = "ade";
        private GeneralFunctions gFunction = new GeneralFunctions();

        internal void RBDExtract_Processing()
        {
            if (!roadBlackFilesFolder.Equals(""))
            {
                // Save the road block files to the folders specified in sysparam.xml file 
                this.CreateRBDExtractFiles();
            }
            return;
        }

        private void CreateRBDExtractFiles()
        {
            string errMessage = "";
            int prevAutIntNo = 0;
            int countRoadBlockFiles = 0;
            string fileName = "";

            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("StartCreateRBDExtractFiles"), ROADBLOCK_PROCESSOR_RBDE, DateTime.Now.ToString()), LogType.Info, ServiceOption.Continue);

            //save to folders
            string exportFolder = roadBlackFilesFolder;

            SqlDataReader reader = this.GetAuthListForCreatingRoadBlockDataExtract(ref errMessage);

            if (reader == null)
            {
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("GetAuthListError"), errMessage), LogType.Error, ServiceOption.BreakAndStop);
                //return;
            }
            else if (!reader.HasRows)
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("GetAuthListNoData"), LogType.Info, ServiceOption.Continue);
            }
            else
            {
                while (reader.Read())
                {
                    int autIntNo = Convert.ToInt32(reader["AutIntNo"]);
                    string mtrCode = reader["MtrCode"].ToString();
                    //string autCode = reader["AutCode"].ToString();
                    string autNo = reader["autNo"].ToString();

                    int noOfRecords = 0;

                    //Beginnings.writer.WriteLine("Create Road Block Files started for metro: " + reader["MtrName"].ToString());
                    bool bGenerate = gFunction.IsGenerateRBDFile(prevAutIntNo, reader, autIntNo, "2610", AARTOBase.LastUser, aartoConnectStr, 0);

                    if (bGenerate)
                    {
                        AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CreateRBFilesStarted"), reader["MtrName"].ToString()), LogType.Info, ServiceOption.Continue);
                        bool failed = this.WriteRoadBlockFile(mtrCode, autNo, autIntNo, ref noOfRecords, exportFolder, ref fileName);

                        if (!failed)
                        {
                            countRoadBlockFiles++;
                            //string exportFullPath = exportFolder + "\\" + fileName;
                            //if (!Directory.Exists(exportFolder))
                            //    Directory.CreateDirectory(exportFolder);

                        }

                        if (noOfRecords == 0)
                        {
                            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoRecordsforRB"), reader["MtrName"].ToString()), LogType.Info, ServiceOption.Continue);
                        }

                    }

                    prevAutIntNo = autIntNo;
                }

                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CreateRBFilesCount"), countRoadBlockFiles.ToString()), LogType.Info, ServiceOption.Continue);

                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CompletedCreationOfRBDExtractFiles"), ROADBLOCK_PROCESSOR_RBDE, DateTime.Now.ToString()), LogType.Info, ServiceOption.Continue);

            }

            reader.Dispose();

            return;
        }

        internal bool WriteRoadBlockFile(string mtrCode, string autNo, int autIntNo, ref int noOfRecords, string roadBlockfileFolder, ref string fileName)
        {
            bool failed = false;
            SqlConnection connection = new SqlConnection();
            string rbdRec = string.Empty;

            //check rule for each metro
            AuthorityRulesDetails ard = new AuthorityRulesDetails();
            ard.AutIntNo = autIntNo;
            ard.ARCode = "2600";
            ard.LastUser = AARTOBase.LastUser;

            DefaultAuthRules ar = new DefaultAuthRules(ard, aartoConnectStr);
            KeyValuePair<int, string> rbde = ar.SetDefaultAuthRule();


            // Initialise and create a Character Translator for Civitas data
            CharacterTranslator.Initialise(aartoConnectStr);
            CharacterTranslator translator = new CharacterTranslator();
            RoadBlockData recRBD = new RoadBlockData();
            SqlDataReader reader = this.GetRoadBlockExtractData(rbde.Value, mtrCode, out connection);

            // Check for empty recordset


            // create the csv file
            if (reader.HasRows)
            {
                if (!Directory.Exists(roadBlockfileFolder))
                    Directory.CreateDirectory(roadBlockfileFolder);

                string fileGenerateDate = DateTime.Now.ToString("yyyyMMdd");
                string exportFullPath = roadBlockfileFolder + "\\" + ROADBLOCK_FILE_PREFIX + "_" + mtrCode.Trim() + "_" + fileGenerateDate + ".txt";
                fileName = Path.GetFileName(exportFullPath);

                FileStream fs = new FileStream(exportFullPath, FileMode.Create, FileAccess.Write);
                //FT 091209 add using for StreamWriter
                using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.ASCII))
                {
                    string strTemp;
                    string strName;

                    //dls 2011-12-14 - change request from M Roets (Roadblock Data extract of active offences Ref 007)
                    try
                    {
                        while (reader.Read())
                        {
                            //upload the value from database to the road block DTO
                            recRBD.recType = "02";
                            recRBD.filter1 = "00";                          //dls 2011-12-14   string.Empty;
                            recRBD.localauthCode = autNo.Trim().Substring(0, 3);            //dls 2011-12-14autCode.Trim();
                            recRBD.filter2 = "00";                          //dls 2011-12-14 string.Empty;
                            recRBD.filter3 = "0";                           //dls 2011-12-14 string.Empty;
                            recRBD.noticeNo = gFunction.GetValue(reader["NotTicketNo"], 0).Replace("/", "").Replace("-", "");
                            recRBD.recIdentifier = gFunction.GetValue(reader["CSRoadblockIdentifier"], 0);
                            recRBD.filmNo = gFunction.GetValue(reader["NotFilmNo"], 0);
                            recRBD.frameNo = gFunction.GetValue(reader["NotFrameNo"], 0);
                            recRBD.OffenderIdentityNo = gFunction.GetValue(reader["Offender Identity number"], 0);
                            strTemp = gFunction.GetValue(reader["Offender Surname"], 0);
                            if (!translator.TryParseName(strTemp, out strName))
                            {
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("WriteInfringementFile_ErrorTranslatingOffenderSurname"), strTemp, strName), LogType.Info, ServiceOption.Continue);
                            }
                            recRBD.surname = strName;
                            strName = "";

                            strTemp = gFunction.GetValue(reader["Offender Initials"], 0);
                            if (!translator.TryParseName(strTemp, out strName))
                            {
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("WriteInfringementFile_ErrorTranslatingOffenderInitials"), strTemp, strName), LogType.Info, ServiceOption.Continue);
                            }
                            recRBD.initials = strName;
                            strName = "";

                            recRBD.vehicleNo = gFunction.GetValue(reader["Vehicle Registration number"], 0);
                            recRBD.offenceDate = gFunction.GetValue(reader["DateTime of offence"], 0);
                            recRBD.offenceTime = gFunction.GetValue(reader["DateTime of offence"], 1); ;
                            recRBD.firstSpeedReading = gFunction.GetValue(reader["First Speed reading"], 0);

                            //strTemp = gFunction.GetValue(reader["Location where offender offended"], 0).Replace("/", " ");
                            //if (!translator.TryParseName(strTemp, out strName))
                            //{
                            //    Beginnings.writer.WriteLine("There was an error translating the Location where offender offended. It was {0}, {1} was sent to Civitas.", strTemp, strName);
                            //}
                            //recRBD.locationOffended = strName;
                            recRBD.locationOffended = gFunction.GetValue(reader["Location where offender offended"], 0);
                            strName = "";

                            recRBD.courtCode = gFunction.GetValue(reader["Court code"], 0);
                            recRBD.courtDate = gFunction.GetValue(reader["Court date"], 0);
                            recRBD.caseNo = gFunction.GetValue(reader["Case number"], 0).Replace("/", "");
                            recRBD.summonsNo = gFunction.GetValue(reader["Summons number"], 0).Replace("/", "");
                            recRBD.summonsIssueDate = gFunction.GetValue(reader["Summons issue date"], 0);
                            recRBD.woaNo = gFunction.GetValue(reader["Warrant of Arrest number"], 0).Replace("/", "");
                            recRBD.woaPrintedDate = gFunction.GetValue(reader["Warrant of Arrest date printed"], 0);
                            recRBD.representation = gFunction.GetValue(reader["Representation"], 0);
                            recRBD.offenceCode1 = gFunction.GetValue(reader["Offence code"], 0);
                            recRBD.chargeType1 = gFunction.GetValue(reader["Type of charge"], 0);
                            recRBD.fineAmount1 = gFunction.GetValue(reader["Fine amount(cents)"], 0);
                            recRBD.chargeResult1 = string.Empty;
                            recRBD.offenceCode2 = string.Empty;
                            recRBD.chargeType2 = string.Empty;
                            recRBD.fineAmount2 = string.Empty;
                            recRBD.chargeResult2 = string.Empty;
                            recRBD.offenceCode3 = string.Empty;
                            recRBD.chargeType3 = string.Empty;
                            recRBD.fineAmount3 = string.Empty;
                            recRBD.chargeResult3 = string.Empty;
                            recRBD.offenceCode4 = string.Empty;
                            recRBD.chargeType4 = string.Empty;
                            recRBD.fineAmount4 = string.Empty;
                            recRBD.chargeResult4 = string.Empty;
                            recRBD.offenceCode5 = string.Empty;
                            recRBD.chargeType5 = string.Empty;
                            recRBD.fineAmount5 = string.Empty;
                            recRBD.chargeResult5 = string.Empty;
                            sw.Write(recRBD.Write());
                            noOfRecords++;
                            if (noOfRecords <= 0)
                                failed = false;
                        }
                    }
                    catch (Exception e)
                    {
                        AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("RoadBlockFileFailed"), e.Message, exportFullPath, DateTime.Now.ToString()), LogType.Error, ServiceOption.Continue);
                        failed = true;
                        sw.Close();
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                            connection.Dispose();
                    }

                    if (!failed)
                    {
                        if (noOfRecords > 0)
                        {
                            char cTemp = Convert.ToChar("0");

                            // Write out the up control record - add 1 to the counter to include the 54
                            sw.Write("99" + "000" + (noOfRecords + 1).ToString().PadLeft(8, cTemp));
                            sw.Close();
                            fs.Close();

                            //road block file is generated successfully,set AutRoadblockExtractDate to the current time.
                            string AutRoadblockExtractDate = DateTime.Now.ToString();
                            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("RoadBlockFileIsGenerated"), AutRoadblockExtractDate, mtrCode), LogType.Info, ServiceOption.Continue);

                            int affectedCount = this.UpdateAutRoadblockExtractDate(AutRoadblockExtractDate, mtrCode);
                            if (affectedCount < 0)
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("AuthorityRBDextractDateUpdateFiled"), mtrCode), LogType.Info, ServiceOption.Continue);
                            else if (affectedCount == 0)
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoAvailableAuthForMetroCode"), mtrCode), LogType.Info, ServiceOption.Continue);
                            else
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("AuthorityRBDextractDateUpdated"), mtrCode), LogType.Info, ServiceOption.Continue);
                            // Update batch with contravention date
                        }
                    }
                    else
                    {
                        failed = true;
                    }
                }
            }
            else
                failed = true;



            return failed;
        }

        private SqlDataReader GetRoadBlockExtractData(string arString, string mtrCode, out SqlConnection connection)
        {
            //get road block extract data 
            RoadBlockRecord roadBlockRecord = new RoadBlockRecord(aartoConnectStr);
            SqlDataReader reader = roadBlockRecord.GetRoadBlockData(arString, mtrCode, out connection);
            return reader;
        }

        internal int UpdateAutRoadblockExtractDate(string AutRoadblockExtractDate, string mtrCode)
        {
            TMSData db = new TMSData(aartoConnectStr);
            int affectedCount = db.UpdateAutRoadblockExtractDate(AutRoadblockExtractDate, mtrCode, AARTOBase.LastUser);
            return affectedCount;
        }

        #endregion

        #region RBFNPExtract
        private List<imageDetail> imageFileInfos = new List<imageDetail>();

        // Constants
        private const string ROADBLOCKFALSENUMBERPLATE_PROCESSOR_RBDE = "RBFNPE";               //"RBFNPE_Aarto";
        private const string ROADBLOCKFALSENUMBERPLATE_FILE_PREFIX = "Falseplate_SI";

        internal void RBFNPExtract_Processing()
        {
            if (!roadBlackFilesFolder.Equals(""))
            {
                // Save the road block files to the folders specified in sysparam.xml file 
                this.CreateRBFNPExtractFiles();

                //switching on/off the generation of images for false number plates
                if (generateFalseNumberImages.ToUpper()=="TRUE")
                    this.CreateScImageFiles();
            }
            return;
        }

        private void CreateRBFNPExtractFiles()
        {
            string errMessage = "";
            int prevAutIntNo = 0;
            int countRoadBlockFalseNumberFiles = 0;
            string fileName = "";
            imageFileInfos.Clear();

            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("StartCreateRBFNPExtract"), ROADBLOCKFALSENUMBERPLATE_PROCESSOR_RBDE, DateTime.Now.ToString()), LogType.Info, ServiceOption.Continue);

            //save to folders
            string exportFolder = roadBlackFilesFolder;


            SqlDataReader reader = this.GetAuthListForCreatingRoadBlockDataExtract(ref errMessage);

            if (reader == null)
            {
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("GetAuthListError"), errMessage), LogType.Error, ServiceOption.BreakAndStop);
                //return;
            }
            else if (!reader.HasRows)
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("GetAuthListNoData"), LogType.Error, ServiceOption.Continue);
            }
            else
            {
                while (reader.Read())
                {
                    int autIntNo = Convert.ToInt32(reader["AutIntNo"]);
                    string mtrCode = reader["MtrCode"].ToString();
                    //string autCode = reader["AutCode"].ToString();

                    int noOfRecords = 0;

                    bool bGenerate = gFunction.IsGenerateRBDFile(prevAutIntNo, reader, autIntNo, "2620", AARTOBase.LastUser, aartoConnectStr, 1);

                    if (bGenerate)
                    {
                        AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CreateRBFNPFilesStarted"), reader["MtrName"].ToString()), LogType.Info, ServiceOption.Continue);

                        bool failed = this.WriteRoadBlockFile(mtrCode, autIntNo, ref noOfRecords, exportFolder, ref fileName);

                        if (!failed)
                        {
                            countRoadBlockFalseNumberFiles++;
                            //string exportFullPath = exportFolder + fileName;
                            //if (!Directory.Exists(exportFolder))
                            //    Directory.CreateDirectory(exportFolder);

                        }

                        if (noOfRecords == 0)
                        {
                            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoRecordsforRBFNP"), reader["MtrName"].ToString()), LogType.Info, ServiceOption.Continue);
                        }
                    }

                    prevAutIntNo = autIntNo;
                }

                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CreateRBFNPFilesCount"), countRoadBlockFalseNumberFiles.ToString()), LogType.Info, ServiceOption.Continue);

                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CompletedCreationOfRBFNPExtractFiles"), ROADBLOCKFALSENUMBERPLATE_PROCESSOR_RBDE, DateTime.Now.ToString()), LogType.Info, ServiceOption.Continue);
            }

            reader.Dispose();

            return;

        }

        internal bool WriteRoadBlockFile(string mtrCode, int autIntNo, ref int noOfRecords, string roadBlockfileFolder, ref string fileName)
        {

            bool failed = false;
            SqlConnection connection = new SqlConnection();

            //not necessary to check Aarto rule for falso no. plate extract
            //string rbdRec = string.Empty;
            ////check rule for each metro
            //AuthorityRulesDetails ard = new AuthorityRulesDetails();
            //ard.AutIntNo = autIntNo;
            //ard.ARCode = "2600";
            //ard.LastUser = parameters.ProcessName;

            //DefaultAuthRules ar = new DefaultAuthRules(ard, parameters.ConnectionString);
            //KeyValuePair<int, string> rbde = ar.SetDefaultAuthRule();

            // Initialise and create a Character Translator for Civitas data
            CharacterTranslator.Initialise(aartoConnectStr);
            CharacterTranslator translator = new CharacterTranslator();
            RoadBlockFalseNumberPlateData recRBFNPD = new RoadBlockFalseNumberPlateData();
            //SqlDataReader reader = this.GetRoadBlockFalseNumberPlateData(rbde.Value, mtrCode, out connection);
            SqlDataReader reader = this.GetRoadBlockFalseNumberPlateData(mtrCode, out connection);

            // Check for empty recordset

            // create the csv file
            if (reader.HasRows)
            {
                if (!Directory.Exists(roadBlockfileFolder))
                    Directory.CreateDirectory(roadBlockfileFolder);

                string fileGenerateDate = DateTime.Now.ToString("dd-MM-yyyy");
                string exportFullPath = roadBlockfileFolder + "\\" + ROADBLOCKFALSENUMBERPLATE_FILE_PREFIX + "_" + mtrCode.Trim() + "_" + fileGenerateDate + ".csv";
                fileName = Path.GetFileName(exportFullPath);

                FileStream fs = new FileStream(exportFullPath, FileMode.Create, FileAccess.Write);
                //FT 091209 add using for StreamWriter
                using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.ASCII))
                {
                    string strTemp;
                    //string strName;


                    try
                    {
                        while (reader.Read())
                        {
                            //upload the value from database to the road block DTO
                            recRBFNPD.filmNo = gFunction.GetValue(reader["Film Number"], 0);
                            recRBFNPD.frameNo = gFunction.GetValue(reader["Frame Number"], 0);
                            recRBFNPD.registrationNo = gFunction.GetValue(reader["Registration number"], 0);
                            strTemp = gFunction.GetValue(reader["Location description"], 0);
                            //if (!translator.TryParseAddress(strTemp, out strName))
                            //{
                            //    Beginnings.writer.WriteLine("There was an error translating the Location descriptionn. It was {0}, {1} was sent to Civitas.", strTemp, strName);
                            //}
                            //recRBFNPD.locationDesc = strName;
                            //strName = "";

                            recRBFNPD.locationDesc = gFunction.GetValue(reader["Location description"], 0);
                            recRBFNPD.offenceDateTime = gFunction.GetValue(reader["Offence Date and Time"], 3);
                            recRBFNPD.offenceVehicleType = gFunction.GetValue(reader["Vehicle Type on Offence"], 0);
                            recRBFNPD.offenceVehicleDesc = gFunction.GetValue(reader["Vehicle description on offence"], 0);
                            recRBFNPD.natisVehicleType = gFunction.GetValue(reader["Vehicle type on NATIS"], 0);
                            recRBFNPD.natisVehicleDesc = gFunction.GetValue(reader["Vehicle description on NATIS"], 0);
                            recRBFNPD.natisVehicleLicence = gFunction.GetValue(reader["Vehicle Licence on NATIS"], 2);
                            recRBFNPD.natisVehicleLicenceExpire = gFunction.GetValue(reader["Vehicle Licence Expire on NATIS"], 2);
                            recRBFNPD.natisVINNo = gFunction.GetValue(reader["VIN Number on NATIS"], 0);
                            recRBFNPD.natisEngineNo = gFunction.GetValue(reader["Engine Number on NATIS"], 0);
                            recRBFNPD.natisVehicleColour = gFunction.GetValue(reader["Vehicle Colour on NATIS"], 0);
                            recRBFNPD.recType = gFunction.GetValue(reader["Type of record"], 0);
                            recRBFNPD.offenceDesc = gFunction.GetValue(reader["Offence description"], 0);
                            recRBFNPD.AuthName = gFunction.GetValue(reader["AutName"], 0);//2013-11-06 added by Nancy
                            recRBFNPD.imageNoA = gFunction.GetValue(reader["ImageNumberA"], 0);
                            recRBFNPD.imageNoB = gFunction.GetValue(reader["ImageNumberB"], 0);

                            //dls 090911 - need to handle A and B scans for the same frame
                            imageDetail imageDetail = new imageDetail();

                            imageDetail.scImIntNoA = Convert.ToInt32(reader["ScImIntNoA"]);
                            imageDetail.scImIntNoB = Convert.ToInt32(reader["ScImIntNoB"]);
                            imageDetail.filmNo = recRBFNPD.filmNo;
                            imageDetail.frameNo = recRBFNPD.frameNo;
                            imageDetail.imageNoA = recRBFNPD.imageNoA;
                            imageDetail.imageNoB = recRBFNPD.imageNoB;
                            imageFileInfos.Add(imageDetail);

                            String strRecord = recRBFNPD.Write();
                            sw.Write(strRecord);
                            noOfRecords++;
                            if (noOfRecords <= 0)
                                failed = false;
                        }
                    }
                    catch (Exception e)
                    {
                        AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("RBFNPFailed"), e.Message, exportFullPath, DateTime.Now.ToString()), LogType.Error, ServiceOption.Continue);
                        failed = true;
                        sw.Close();
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                            connection.Dispose();
                    }

                    if (!failed)
                    {
                        if (noOfRecords > 0)
                        {
                            //FT 091209	Add the record count
                            sw.Write(noOfRecords.ToString());
                            //Road block file is generated successfully,set AutRoadblockExtractDate to the current time.
                            string AutRoadBlockFalseNumberPlateExtractDate = DateTime.Now.ToString();
                            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("RBFNPFileIsGenerated"), AutRoadBlockFalseNumberPlateExtractDate, mtrCode), LogType.Info, ServiceOption.Continue);
                            int affectedCount = this.UpdateAutRoadBlockFalseNumberPlateExtractDate(AutRoadBlockFalseNumberPlateExtractDate, mtrCode);
                            if (affectedCount < 0)
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("AuthorityRBFNPExtractDateUpdateFiled"), mtrCode), LogType.Info, ServiceOption.Continue);
                            else if (affectedCount == 0)
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoAvailableAuthForMetroCode"), mtrCode), LogType.Info, ServiceOption.Continue);
                            else
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("AuthorityRBFNPExtractDateUpdated"), mtrCode), LogType.Info, ServiceOption.Continue);
                            //Update batch with contravention date
                        }
                    }
                    else
                    {
                        failed = true;
                    }
                }
            }
            else
                failed = true;



            return failed;
        }

        private SqlDataReader GetRoadBlockFalseNumberPlateData(string mtrCode, out SqlConnection connection)
        {
            //get road block extract data 
            RoadBlockFalseNumberPlateRecord rbfnpRecord = new RoadBlockFalseNumberPlateRecord(aartoConnectStr);
            //SqlDataReader reader = rbfnpRecord.GetRoadBlockFalseNumberPlateData(arString,mtrCode, out connection);
            SqlDataReader reader = rbfnpRecord.GetRoadBlockFalseNumberPlateData(mtrCode, out connection);
            return reader;
        }

        internal int UpdateAutRoadBlockFalseNumberPlateExtractDate(string AutRoadBlockFalseNumberPlateExtractDate, string mtrCode)
        {
            TMSData db = new TMSData(aartoConnectStr);
            int affectedCount = db.UpdateAutRoadBlockFalseNumberPlateExtractDate(AutRoadBlockFalseNumberPlateExtractDate, mtrCode, AARTOBase.LastUser);
            return affectedCount;
        }

        private void CreateScImageFiles()
        {
            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("StartingCreationOfImageFiles"), DateTime.Now.ToString()), LogType.Info, ServiceOption.Continue);

            //save to folders
            foreach (imageDetail imageFileInfo in imageFileInfos)
            {
                if (imageFileInfo.scImIntNoA == 0)
                    continue;

                SaveImage(imageFileInfo, "A");

                if (imageFileInfo.scImIntNoB == 0)
                    continue;

                SaveImage(imageFileInfo, "B");
            }

            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CompletedCreationOfImageFiles"), DateTime.Now.ToString()), LogType.Info, ServiceOption.Continue);
        }

        private void SaveImage(imageDetail imageFileInfo, string imageType)
        {
            string exportFolder = roadBlackFilesFolder;

            string errMessage = "";

            //string vName = exportFolder + "\\" + imageFileInfo.filmNo + "_" + imageFileInfo.frameNo + "_" + imageFileInfo.imageNoA + ".jpg";
            string vName = exportFolder + "\\" + imageFileInfo.filmNo + "_" + imageFileInfo.frameNo + "_" + imageType + ".jpg";

            if (!File.Exists(vName))
            {
                Int32 scImIntNo = (imageType == "A") ? imageFileInfo.scImIntNoA : imageFileInfo.scImIntNoB;

                //SqlDataReader reader = this.GetImageFileData(Convert.ToInt32(imageFileInfo.scImIntNoA), ref errMessage);
                SqlDataReader reader = this.GetImageFileData(Convert.ToInt32(scImIntNo), ref errMessage);

                if (reader == null)
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("GetImageFilesError"), errMessage), LogType.Info, ServiceOption.Continue);
                }
                else if (!reader.HasRows)
                {
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("NoAvailableImageFiles"), LogType.Info, ServiceOption.Continue);
                }
                else
                {
                    while (reader.Read())
                    {
                        try
                        {
                            // David Lin 20100329 Remove images from database
                            //FT 091209 Check DBNull
                            //if (reader["ScanImage"] == DBNull.Value)
                            //{
                            //    Beginnings.writer.WriteLine("Empty image data of filmNo:" + imageFileInfo.filmNo + " frameNo:" + imageFileInfo.frameNo);
                            //    continue;
                            //}
                            //byte[] imageFile = (byte[])reader["ScanImage"];
                            ScanImageDB imgDB = new ScanImageDB(aartoConnectStr);
                            byte[] imageFile = imgDB.GetScanImageDataFromRemoteServer(Convert.ToInt32(reader["ScImIntNo"]));
                            if (imageFile == null)
                            {
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("EmptyImageData"), imageFileInfo.filmNo, imageFileInfo.frameNo), LogType.Info, ServiceOption.Continue);
                                //continue;
                            }
                            MemoryStream ms_vImage = new MemoryStream(imageFile);
                            Image vImage = new Bitmap(ms_vImage);
                            vImage.Save(vName, System.Drawing.Imaging.ImageFormat.Jpeg);
                        }
                        catch (Exception e)
                        {
                            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ImageFileGenerationFailed"), imageFileInfo.filmNo, imageFileInfo.frameNo, imageType, e.Message), LogType.Error, ServiceOption.Continue);
                        }
                    }
                }
                reader.Dispose();
            }
        }

        internal SqlDataReader GetImageFileData(int scImIntNo, ref string errMessage)
        {
            TMSData list = new TMSData(aartoConnectStr);
            SqlDataReader imageFileData = list.GetImageFileData(scImIntNo, ref errMessage);
            return imageFileData;
        }
        #endregion

        #region GeneralFunctions
        internal void RemoveFiles(string exportFolder)
        {
            //Delete all files with the roadblock extract signature older than 7 days from the FTP folder
            try
            {
                if (Directory.Exists(exportFolder))
                {
                    string[] files = Directory.GetFiles(exportFolder);
                    foreach (string file in files)
                    {
                        if (Path.GetExtension(file) != ".jpg")
                        {
                            string fileName = Path.GetFileNameWithoutExtension(file);
                            string[] array = fileName.Split('_');
                            DateTime dt = new DateTime();
                            if (array.Length == 3 && DateTime.TryParse(array[2].Insert(4, "-").Insert(7, "-"), out dt) && dt.AddDays(7) < DateTime.Now)
                            {
                                File.Delete(file);
                            }
                            else if (array.Length == 4 && DateTime.TryParse(array[3], CultureInfo.CreateSpecificCulture("fr-FR"), DateTimeStyles.None, out dt) && dt.AddDays(7) < DateTime.Now)
                            {
                                File.Delete(file);
                            }
                        }
                        else
                        {
                            FileInfo imageFileInfo = new FileInfo(file);
                            if (imageFileInfo.CreationTime.AddDays(7) < DateTime.Now)
                                File.Delete(file);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("DeleteFilesFailed"), ex.Message), LogType.Error, ServiceOption.Continue);
            }

        }

        #endregion

        internal SqlDataReader GetAuthListForCreatingRoadBlockDataExtract(ref string errMessage)
        {
            TMSData list = new TMSData(aartoConnectStr);
            SqlDataReader authList = list.GetAuthListForCreatingRoadBlockDataExtract(ref errMessage);
            return authList;
        }
    }
}
