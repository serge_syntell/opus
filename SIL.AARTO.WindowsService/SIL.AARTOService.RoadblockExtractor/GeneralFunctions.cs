﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Stalberg.TMS;

namespace SIL.AARTOService.RoadblockExtractor
{
    /// <summary>
    /// Represents a group of General Functions for the road block file generation.
    /// </summary>
    internal class GeneralFunctions
    {
        public GeneralFunctions()
        { }

        /// <summary>
        /// Convert the format of value to the designated format.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="iFlag">Format 1:Hour+Month 2:Year/Month/Day Others:Default</param>
        /// <returns>return the value in form of designated format.</returns>
        internal string GetValue(object value, int iFlag)
        {
            if (value.GetType() == typeof(DateTime))
            {
                if (iFlag == 1)
                    return (value == DBNull.Value ? "" : Convert.ToDateTime(value).ToString("HHmm"));
                else if (iFlag == 2)
                    return (value == DBNull.Value ? "" : Convert.ToDateTime(value).ToString("yyyy/MM/dd"));
                else if (iFlag == 3)
                    //FT 091209 We don’t store the seconds in the OffenceDate
                    return (value == DBNull.Value ? "" : Convert.ToDateTime(value).ToString("dd/MM/yyyy HH:mm"));
                else
                    return (value == DBNull.Value ? "" : Convert.ToDateTime(value).ToString("yyyyMMdd"));
            }
            else
                //dls 090914 - can't do a replace on "-" - it is needed for the addresses - only remove on the notices, so add inline
                //return (value == DBNull.Value ? string.Empty : value.ToString().Replace(",", " ").Replace("\"", "").Replace("'", "").Replace("/", "").Replace("-", ""));
                return (value == DBNull.Value ? string.Empty : value.ToString().Replace(",", " ").Replace("\"", "").Replace("'", "").Replace(System.Environment.NewLine, " ").Trim());
        }

        internal bool IsGenerateRBDFile(int prevAutIntNo, SqlDataReader reader, int autIntNo, string AuthRuleCode, string strLastUser, string strConn, int flag)
        {
            //QA - dls 090902 - bad use of variable name
            bool generateFile = false;
            DateTime day = DateTime.MinValue;
            AuthorityRulesDB authRules = new AuthorityRulesDB(strConn);
            if (prevAutIntNo != autIntNo)
            {
                //QA - dls 090902 - this rule is in the wrong place

                AuthorityRulesDetails ard = new AuthorityRulesDetails();
                ard.AutIntNo = autIntNo;
                ard.ARCode = AuthRuleCode;
                ard.LastUser = strLastUser;

                DefaultAuthRules ar = new DefaultAuthRules(ard, strConn);
                KeyValuePair<int, string> rbde = ar.SetDefaultAuthRule();

                int intHours = Convert.ToInt32(rbde.Key);
                if (AuthRuleCode == "2610")
                {
                    if (flag == 0 && reader["AutRoadblockExtractDate"] != DBNull.Value)
                        day = Convert.ToDateTime(reader["AutRoadblockExtractDate"]);
                }
                else
                {
                    if (reader["AutRoadBlockExtractFalseNumberPlateDate"] != DBNull.Value)
                        day = Convert.ToDateTime(reader["AutRoadBlockExtractFalseNumberPlateDate"]);
                }

                //QA - dls 090902 - if day is still = DateTime.MinValue (null dates - Extract has never been run) then this will never be set
                if (day.AddHours(24).Date <= DateTime.Now.Date)//&& DateTime.Now.Hour == intHours)
                    generateFile = true;
                else
                    generateFile = false;

            }
            return generateFile;
        }
    }
}
