﻿using SIL.ServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SIL.AARTOService.RoadblockExtractor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.RoadblockExtractor"
                ,
                DisplayName = "SIL.AARTOService.RoadblockExtractor"
                ,
                Description = "SIL AARTOService RoadblockExtractor"
            };

            ProgramRun.InitializeService(new RoadblockExtractor(), serviceDescriptor, args);
        }
    }
}
