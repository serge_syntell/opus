﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceQueueLibrary.DAL.Data;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.ServiceQueueLibrary.DAL.Services;
using System.Collections;

namespace SIL.AARTOService.ExportMaintenanceData
{
    class QueueMaintenanceData
    {
        private string _sepKey;
        public string SepKey
        {
            get
            {
                return GetTheMetroForMoblie(_sepKey);
            }
            set
            {
                _sepKey = value;
            }
        }
        private List<ServiceParameter> list = new List<ServiceParameter>();
        public QueueMaintenanceData()
        {
            InitServiceParams(ref list);
        }
        public string MetroForMoblie
        {
            get
            {
                return GetTheMetroForMoblie("MetroForMobile");
            }
        }
        public string GetTheMetroForMoblie(string sepKey)
        {
            string flag=string.Empty;
            foreach (ServiceParameter item in list)
            {
                if (item.SePaKey.ToLower() == sepKey.ToLower())
                {
                    flag = item.SePaValue;
                    break;
                }
            }
            return flag;
        }
        public void InitServiceParams(ref  List<ServiceParameter> spList)
        {
            ServiceParameterService sps = new ServiceParameterService();
            int serviceHostId = GetHostId(new Guid("DB318263-0E44-4BFE-85A5-DBBBB43E60B7"));
            spList = sps.GetBySeHoIntNo(serviceHostId).ToList();
        }
        public int GetHostId(Guid guid)
        {
            ServiceHost sh = new ServiceHost();
            ServiceHostService shs = new ServiceHostService();
            sh = shs.GetBySeHoUuid(guid);
            if (sh != null)
                return sh.SeHoIntNo;
            else
                return 0;
        }
        public static Hashtable  GetServiceParams()
        {
             List<ServiceParameter> list=new List<ServiceParameter>();
            ServiceParameterService sps = new ServiceParameterService();
            int serviceHostId = Static_GetHostId(new Guid("DB318263-0E44-4BFE-85A5-DBBBB43E60B7"));
            list = sps.GetBySeHoIntNo(serviceHostId).ToList();
            Hashtable ht = new Hashtable();
            foreach (ServiceParameter paramter in list)
            {
                ht.Add(paramter.SePaKey, paramter.SePaValue);
            }
            return ht;
        }
        public static int Static_GetHostId(Guid guid)
        {
            ServiceHost sh = new ServiceHost();
            ServiceHostService shs = new ServiceHostService();
            sh = shs.GetBySeHoUuid(guid);
            if (sh != null)
                return sh.SeHoIntNo;
            else
                return 0;
        }
    }
}
