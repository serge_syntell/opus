﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Reflection;
using SIL.AARTOService.Library;

namespace SIL.AARTOService.ExportMaintenanceData
{
    class GeneratedXML
    {
        public string FilePath
        {
            get
            {
                ExportMaintenanceData_Service service = new ExportMaintenanceData_Service();
                return service.GetFileTempleSavePath();
            }
        }
        public void GeneratedXMLFile(List<Maintenance> maintenanceList,ref List<string> errorMsgList)
        {
            string errorMsg = string.Empty;
            try
            {
                int flag = 0;
                object[] rowCount = new object[maintenanceList.Count];
                foreach (Maintenance maintenance in maintenanceList)
                {
                    object[] objs = new object[34];
                    int objFlag = 4;
                    objs[0] = new XElement("MdMaintenanceDataId", maintenance.MdMaintenanceDataId);
                    objs[1] = new XElement("AuthorityId", "");//maintenance.AuthorityId);
                    objs[2] = new XElement("MdmdAction", maintenance.MdmdAction);
                    string tableName = maintenance.MdmdTableName;
                    objs[3] = new XElement("MdmdTableName", tableName);
                    for (int i = 0; i < 15; i++)
                    {
                        string columnName = "MDMDColumn" + i + "Name";
                        string cloumnNameValue = getTheProperInfo(maintenance, columnName, ref errorMsg);
                        objs[objFlag++] = new XElement(columnName, cloumnNameValue);
                        string column="MDMDColumn"+i.ToString()+"DataValue";
                        string columnValue=getTheProperInfo(maintenance,column,ref errorMsg);
                        objs[objFlag++] = new XElement(column, columnValue);
                    }
                    rowCount[flag] = new XElement("RowInfo", objs);
                    flag++;
                }
                if (rowCount.Length > 0)
                    SaveXMLFile(rowCount);
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "GeneratedXMLFile", ex.ToString());
                errorMsgList.Add(errorMsg);
                throw new Exception(errorMsg);
            }
        }
        private string getTheProperInfo(Object obj, string name,ref string errorMsg)
        {
            string propertyName = "";
            try
            {
                string flag = null;
                PropertyInfo propertyInfo = obj.GetType().GetProperty(name, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                if (propertyInfo != null)
                {
                    object property = propertyInfo.GetValue(obj, null);
                    if(property!=null)
                        flag = property.ToString();
                    propertyName = propertyInfo.Name.ToString();
                }
                return flag;
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "getTheProperInfo", "Property name: " + propertyName +" "+ ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        private void SaveXMLFile(object[] rowCount)
        {
            string prefix = "AARTODataForIMX_";
            string metroName = ExportMaintenanceData_Service.GetTopOneMetroName();
            string fileTime = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-ff");
            XElement elements = new XElement("TableInfo", rowCount);
            if (!Directory.Exists(FilePath))
                Directory.CreateDirectory(FilePath);
            string saveFileName = FilePath + prefix + metroName+"_"+fileTime+ ".imx";
            elements.Save(saveFileName);
        }
    }
}
