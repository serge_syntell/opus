﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.AARTOService.DAL;
using SIL.AARTOService.Resource;
using SIL.AARTO.DAL;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Data.SqlClient;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using System.Reflection;
using System.Collections.Specialized;
using System.IO;

namespace SIL.AARTOService.ExportMaintenanceData
{
    class ExportMaintenanceData_Service : ClientService
    {
        public ExportMaintenanceData_Service()
            : base("", "", new Guid("B6E3C8BA-47AA-4331-AAA1-E300ED82A8FC"))
        {
            this._serviceHelper = new AARTOServiceBase(this, false);
            AARTOBase.OnServiceSleeping = SleepRecord;
        }

       public AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
       string[] baseTables = { "Metro", "Authority", "TrafficOfficer" };
       int pageLength = 1000;
        //

        public override void PrepareWork()
        {
            Initialize();
        }

        public override void InitialWork(ref QueueLibrary.QueueItem item)
        {

        }
        //do export data
        public override void MainWork(ref List<QueueLibrary.QueueItem> queueList)
        {
            string errorMsg = string.Empty;
            try
            {
                bool mobileFlag = CheckMobileFlag("Mobile", "y");
                bool IMXFlag = CheckMobileFlag("BackOffice", "imx");
                MobileMaintenanceData MMD = new MobileMaintenanceData();
                MMD.Export_Service = this;
                ExportData(MMD, mobileFlag, IMXFlag);
            }
            catch (Exception ex)
            {
                AARTOBase.ErrorProcessing(ex.Message, true);
            }
            finally
            {
                this.MustSleep = true;
            }
        }

        #region To improve
        private void ExportData(MobileMaintenanceData MMD,bool mobileFlag,bool IMXFlag)
        {
            int total = 0;
            List<MaintenanceData> aartoList = new List<MaintenanceData>();
            int pageSize = GetPageSize();
            for (int i = 0; i < pageSize; i++)
            {
                aartoList.Clear();
                aartoList = GetMaintenanceDataList(ref total);
                //
                if (aartoList.Count > 0)
                {
                    if (mobileFlag)
                        ExportToMobile(aartoList, MMD);
                    if (IMXFlag)
                        ExportToIMX(aartoList);
                }
            }
        }
        private void ExportToMobile(List<MaintenanceData> aartoList,MobileMaintenanceData MMD)
        {
            DateTime CurrentTime = DateTime.Now;
            string errorMsg = string.Empty;
            if (aartoList.Count > 0)
            {
                List<MobileMaintenanceData> mobileList = GetMobileList(aartoList, CurrentTime);
                List<MobileMaintenanceData> baseTableList = GetBaseTableList(mobileList);
                //first we need prepare basetable data
                UpdateBaseTable(baseTableList,MMD);
                //second do other tables
                MMD.Export_Service = this;
                //mobileList = mobileList.FindAll(n => !baseTables.Contains(n.MobileDeviceobj.MdmdTableName));
                MMD.InsertDataToMobile(mobileList, ref errorMsg);
            }
        }
        public List<MaintenanceData> GetMaintenanceDataList(ref int total)
        {
            string errorMsg=string.Empty;
            List<MaintenanceData> aartoList = new List<MaintenanceData>();
            MaintenanceDataService md_Service = new MaintenanceDataService();
            string whereStr = "MaDaVersionDate is null";
            aartoList = md_Service.GetPaged(whereStr, "MaDaIntNo", 0, pageLength, out total).ToList();

            RemoveOtherMetroBaseTables(aartoList, ref errorMsg);
            return aartoList;
        }
        //get the maintenance data list
        private int GetPageSize()
        {
            int total = 0;
            int pageCount = 0;
            int count = 0;

            MaintenanceDataService md_Service = new MaintenanceDataService();
            string whereStr = "MaDaVersionDate is null";
            md_Service.GetPaged(whereStr, "MaDaIntNo", 0, pageLength, out total).ToList();

            count = total % pageLength;
            if (count > 0)
                pageCount = total / pageLength + 1;
            else
                pageCount = total / pageLength;

            return pageCount;
        }
        private List<MobileMaintenanceData> GetBaseTableList(List<MobileMaintenanceData> mobileList)
        {
            List<MobileMaintenanceData> baseTableList = new List<MobileMaintenanceData>();

            baseTableList = mobileList.FindAll(n => baseTables.Contains(n.MobileDeviceobj.MdmdTableName));

            return baseTableList;
        }
        private List<MobileMaintenanceData> GetMobileList(List<MaintenanceData> aartoList, DateTime CurrentTime)
        {
            try
            {
                List<MobileMaintenanceData> mList = new List<MobileMaintenanceData>();
                foreach (MaintenanceData item in aartoList)
                {
                    MobileMaintenanceData mobileData = new MobileMaintenanceData();
                    if (!string.IsNullOrEmpty(item.AutCode))
                        mobileData.AutCode = item.AutCode;
                    mobileData.MobileDeviceobj.MdmdAction = item.MaDaRowAction;
                    //moblieData.MobileDeviceobj.MdmddbVersion = md.MaDaVersionNumber;
                    item.MaDaVersionDate = CurrentTime;
                    item.MaDaSentToMiDate = CurrentTime;
                    mobileData.MobileDeviceobj.MdmddbDate = item.MaDaVersionDate;
                    mobileData.MobileDeviceobj.MdmdTableName = item.MaDaTablename.Trim();
                    mobileData.MobileDeviceobj.MdmdColumn0Name = item.MaDaColName0.Trim();
                    mobileData.MobileDeviceobj.MdmdColumn0DataValue = item.MaDaColValue0.Trim();
                    mobileData.MobileDeviceobj.MdmdColumn1Name = item.MaDaColName1;
                    mobileData.MobileDeviceobj.MdmdColumn1DataValue = item.MaDaColValue1;
                    mobileData.MobileDeviceobj.MdmdColumn2Name = item.MaDaColName2;
                    mobileData.MobileDeviceobj.MdmdColumn2DataValue = item.MaDaColValue2;
                    mobileData.MobileDeviceobj.MdmdColumn3Name = item.MaDaColName3;
                    mobileData.MobileDeviceobj.MdmdColumn3DataValue = item.MaDaColValue3;
                    mobileData.MobileDeviceobj.MdmdColumn4Name = item.MaDaColName4;
                    mobileData.MobileDeviceobj.MdmdColumn4DataValue = item.MaDaColValue4;
                    mobileData.MobileDeviceobj.MdmdColumn5Name = item.MaDaColName5;
                    mobileData.MobileDeviceobj.MdmdColumn5DataValue = item.MaDaColValue5;
                    mobileData.MobileDeviceobj.MdmdColumn6Name = item.MaDaColName6;
                    mobileData.MobileDeviceobj.MdmdColumn6DataValue = item.MaDaColValue6;
                    mobileData.MobileDeviceobj.MdmdColumn7Name = item.MaDaColName7;
                    mobileData.MobileDeviceobj.MdmdColumn7DataValue = item.MaDaColValue7;
                    mobileData.MobileDeviceobj.MdmdColumn8Name = item.MaDaColName8;
                    mobileData.MobileDeviceobj.MdmdColumn8DataValue = item.MaDaColValue8;
                    mobileData.MobileDeviceobj.MdmdColumn9Name = item.MaDaColName9;
                    mobileData.MobileDeviceobj.MdmdColumn9DataValue = item.MaDaColValue9;
                    mobileData.MobileDeviceobj.MdmdColumn10Name = item.MaDaColName10;
                    mobileData.MobileDeviceobj.MdmdColumn10DataValue = item.MaDaColValue10;
                    mobileData.MobileDeviceobj.MdmdColumn11Name = item.MaDaColName11;
                    mobileData.MobileDeviceobj.MdmdColumn11DataValue = item.MaDaColValue11;
                    mobileData.MobileDeviceobj.MdmdColumn12Name = item.MaDaColName12;
                    mobileData.MobileDeviceobj.MdmdColumn12DataValue = item.MaDaColValue12;
                    mobileData.MobileDeviceobj.MdmdColumn13Name = item.MaDaColName13;
                    mobileData.MobileDeviceobj.MdmdColumn13DataValue = item.MaDaColValue13;
                    mobileData.MobileDeviceobj.MdmdColumn14Name = item.MaDaColName14;
                    mobileData.MobileDeviceobj.MdmdColumn14DataValue = item.MaDaColValue14;
                    mobileData.AartoMaintenanceDataIntNo = item.MaDaIntNo;
                    mList.Add(mobileData);
                }
                return mList;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", "CreateMobileList", ex.ToString()));
            }
        }
        private List<Maintenance> GetIMXList(List<MaintenanceData> aartoList, DateTime CurrentTime)
        {
            try
            {
                List<Maintenance> mList = new List<Maintenance>();
                foreach (MaintenanceData item in aartoList)
                {
                    Maintenance baseMaintenance = new Maintenance();
                    MobileMaintenanceData mobileData = new MobileMaintenanceData();
                    //if (!string.IsNullOrEmpty(item.AutCode))
                    //    baseMaintenance.AuthorityId = item.AutCode;
                    baseMaintenance.MdmdAction = item.MaDaRowAction;
                    item.MaDaVersionDate = CurrentTime;
                    item.MaDaSentToMiDate = CurrentTime;
                    baseMaintenance.MdMaintenanceDataId = item.MaDaIntNo;
                    baseMaintenance.MdmddbDate = CurrentTime;
                    baseMaintenance.MdmdTableName = item.MaDaTablename.Trim();
                    baseMaintenance.MdmdColumn0Name = item.MaDaColName0.Trim();
                    baseMaintenance.MdmdColumn0DataValue = item.MaDaColValue0.Trim();
                    baseMaintenance.MdmdColumn1Name = item.MaDaColName1;
                    baseMaintenance.MdmdColumn1DataValue = item.MaDaColValue1;
                    baseMaintenance.MdmdColumn2Name = item.MaDaColName2;
                    baseMaintenance.MdmdColumn2DataValue = item.MaDaColValue2;
                    baseMaintenance.MdmdColumn3Name = item.MaDaColName3;
                    baseMaintenance.MdmdColumn3DataValue = item.MaDaColValue3;
                    baseMaintenance.MdmdColumn4Name = item.MaDaColName4;
                    baseMaintenance.MdmdColumn4DataValue = item.MaDaColValue4;
                    baseMaintenance.MdmdColumn5Name = item.MaDaColName5;
                    baseMaintenance.MdmdColumn5DataValue = item.MaDaColValue5;
                    baseMaintenance.MdmdColumn6Name = item.MaDaColName6;
                    baseMaintenance.MdmdColumn6DataValue = item.MaDaColValue6;
                    baseMaintenance.MdmdColumn7Name = item.MaDaColName7;
                    baseMaintenance.MdmdColumn7DataValue = item.MaDaColValue7;
                    baseMaintenance.MdmdColumn8Name = item.MaDaColName8;
                    baseMaintenance.MdmdColumn8DataValue = item.MaDaColValue8;
                    baseMaintenance.MdmdColumn9Name = item.MaDaColName9;
                    baseMaintenance.MdmdColumn9DataValue = item.MaDaColValue9;
                    baseMaintenance.MdmdColumn10Name = item.MaDaColName10;
                    baseMaintenance.MdmdColumn10DataValue = item.MaDaColValue10;
                    baseMaintenance.MdmdColumn11Name = item.MaDaColName11;
                    baseMaintenance.MdmdColumn11DataValue = item.MaDaColValue11;
                    baseMaintenance.MdmdColumn12Name = item.MaDaColName12;
                    baseMaintenance.MdmdColumn12DataValue = item.MaDaColValue12;
                    baseMaintenance.MdmdColumn13Name = item.MaDaColName13;
                    baseMaintenance.MdmdColumn13DataValue = item.MaDaColValue13;
                    baseMaintenance.MdmdColumn14Name = item.MaDaColName14;
                    baseMaintenance.MdmdColumn14DataValue = item.MaDaColValue14;
                    mList.Add(baseMaintenance);
                }
                return mList;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", "GetIMXList", ex.ToString()));
            }
        }

        //***************************************
        private void ExportToIMX(List<MaintenanceData> aartoList)
        {
            IMXMaintenanceData imx = new IMXMaintenanceData();
            string errorMsg = string.Empty;
            List<string> errorMsgList = new List<string>();
            DateTime CurrentTime = DateTime.Now;
            if (aartoList.Count > 0)
            {
                List<Maintenance> baseLibraryList = GetIMXList(aartoList, CurrentTime);

                imx.ARRTOMappingIMX(baseLibraryList, ref errorMsgList);
                //
                foreach (var item in baseLibraryList)
                {
                    UpdateMaintenanceDataState(item.MdMaintenanceDataId);
                }
            }
        }
        #endregion

        #region not base method
        public void SleepRecord()
        {
            Logger.Info("Sleeping......");
        }
        
        private void RemoveOtherMetroBaseTables(List<MaintenanceData> ListMaintenance, ref string errorMsg)
        {
            try
            {
                List<MaintenanceDataMetroCode> metroList = new List<MaintenanceDataMetroCode>();
                string[] tables = { "Metro", "Authority", "TrafficOfficer", "OffenceFine" };
                foreach (MaintenanceData md in ListMaintenance)
                {
                    MaintenanceDataMetroCode metro = new MaintenanceDataMetroCode();
                    metro.MaDaIntNo = md.MaDaIntNo;
                    metro.MaDaTablename = md.MaDaTablename;
                    switch (md.MaDaTablename)
                    {
                        case "Metro":
                        case "Authority":
                        case "TrafficOfficer":
                            metro.MetroCode = md.MaDaColValue0.Trim();
                            break;
                        case "OffenceFine":
                            Metro entityMetro = GetMetroCodeFromAut(md.MaDaColValue0);
                            metro.MetroCode = (entityMetro != null ? entityMetro.MtrCode : "-1");
                            break;
                        default:
                            metro.MetroCode = "-1";
                            break;
                    }
                    metroList.Add(metro);
                }
                QueueMaintenanceData QMD = new QueueMaintenanceData();
                string metroCode = QMD.MetroForMoblie;
                if (!string.IsNullOrEmpty(metroCode))
                {
                    var query_linq = from metro in metroList
                                     where metro.MetroCode.Trim() != metroCode & tables.Contains(metro.MaDaTablename)
                                     select metro;
                    List<MaintenanceDataMetroCode> getList = query_linq.ToList();
                    //
                    foreach (MaintenanceDataMetroCode item in getList)
                    {
                        var query_item = from moveItem in ListMaintenance
                                         where moveItem.MaDaIntNo == item.MaDaIntNo
                                         select moveItem;
                        ListMaintenance.Remove(query_item.ToList()[0]);
                        //if remove update state
                        UpdateMaintenanceDataState(item.MaDaIntNo);
                    }
                }
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "CreateMetroCodeList", ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        public Metro GetMetroCodeFromAut(string AutCode)
        {
            AuthorityService aut_Service = new AuthorityService();
            Authority aut = aut_Service.GetByAutCode(AutCode);
            MetroService met_Service = new MetroService();
            return met_Service.GetByMtrIntNo(aut.MtrIntNo);
        }
        public static string GetTopOneMetroName()
        {
            MetroService met_Service = new MetroService();
            List<Metro> list = met_Service.GetAll().ToList();
            if (list.Count > 0)
                return list[0].MtrCode.Trim();
            else
                return "";
        }
        private void UpdateBaseTable(List<MobileMaintenanceData> baseTableList, MobileMaintenanceData mainData)
        {
            List<MobileMaintenanceData> list = new List<MobileMaintenanceData>();
            foreach (string table in baseTables)
            {
                list.Clear();
                list = baseTableList.FindAll(n => n.MobileDeviceobj.MdmdTableName == table);
                foreach (MobileMaintenanceData item in list)
                {
                    switch(table)
                    {
                        case"Metro":
                            mainData.DealMetro(item,item.MobileDeviceobj.MdmdAction);
                            break;
                        case"Authority":
                            mainData.DealAuthority(item,item.MobileDeviceobj.MdmdAction);
                            break;
                        case"TrafficOfficer":
                            mainData.DealTrafficOfficer(item,item.MobileDeviceobj.MdmdAction);
                            break;
                    }
                    //update state
                    UpdateMaintenanceDataState(item.AartoMaintenanceDataIntNo);
                }
            }
        }
        public void UpdateMaintenanceDataState(int MaDaIntNo)
        {
            MaintenanceDataService mdService = new MaintenanceDataService();
            SIL.AARTO.DAL.Entities.MaintenanceData main = mdService.GetByMaDaIntNo(MaDaIntNo);
            main.MaDaVersionDate = DateTime.Now;
            mdService.Update(main);
        }
        public static bool CheckMobileFlag(string parameter, string parameterValue)
        {
            bool flag = false;
            QueueMaintenanceData queue = new QueueMaintenanceData();
            queue.SepKey = parameter;
            string mobileFlag = queue.SepKey.ToLower();
            if (mobileFlag == parameterValue.ToLower())
                flag = true;
            return flag;
        }
        public string GetFileTempleSavePath()
        {
            //get indaba path
            string InPath = AARTOBase.GetConnectionString(ServiceConnectionNameList.FolderForIndabaSentFile, ServiceConnectionTypeList.UNC);
            return InPath;
        }
        public string GetIndabaConnectstring()
        {
            return AARTOBase.GetConnectionString(ServiceConnectionNameList.Indaba, ServiceConnectionTypeList.DB);
        }
        public void RecordLog(string message)
        {
            AARTOBase.ErrorProcessing(message, false);
        }
        //InitializeComponent
        private void Initialize()
        {
            string connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            ServiceUtility.InitializeNetTier(connectStr);
            //init mobile connect
            string MobileconnectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.MobileInterface, ServiceConnectionTypeList.DB);
            ServiceUtility.InitializeNetTier(MobileconnectStr, "SIL.MobileInterface");
        }
        #endregion
    }
    class MaintenanceDataMetroCode
    {
        public int MaDaIntNo
        {
            get;
            set;
        }
        public string MetroCode
        {
            get;
            set;
        }
        public string MaDaTablename
        {
            get;
            set;
        }
    }
}
