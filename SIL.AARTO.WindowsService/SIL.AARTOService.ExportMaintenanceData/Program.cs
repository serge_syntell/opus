﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.ExportMaintenanceData
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor {
                ServiceName = "SIL.AARTOService.ExportMaintenanceData",
                DisplayName = "SIL.AARTOService.ExportMaintenanceData",
                Description = "SIL AARTOService ExportMaintenanceData"
            };
            ProgramRun.InitializeService(new ExportMaintenanceData(), serviceDescriptor, args);
        }
    }
}
