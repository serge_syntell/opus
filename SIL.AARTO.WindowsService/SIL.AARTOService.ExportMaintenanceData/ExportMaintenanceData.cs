﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.ExportMaintenanceData
{
    public partial class ExportMaintenanceData : ServiceHost
    {
        public ExportMaintenanceData()
            : base("", new Guid("DB318263-0E44-4BFE-85A5-DBBBB43E60B7"), new ExportMaintenanceData_Service())
        {
            InitializeComponent();
        }
    }
}
