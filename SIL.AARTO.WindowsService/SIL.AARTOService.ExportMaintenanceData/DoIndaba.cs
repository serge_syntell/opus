﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.Indaba;
using System.Configuration;
using System.IO;
using System.Collections;

namespace SIL.AARTOService.ExportMaintenanceData
{
    class DoIndaba
    {
        public static Stalberg.Indaba.ICommunicate indaba;
        static string tempDirectory;

        public static void InitIndaba()
        {
            Stalberg.Indaba.Client.ApplicationName = "PushStaticDataFromAARTOToIMX";
            Stalberg.Indaba.Client.ConnectionString = GetIndabaConn();
            Stalberg.Indaba.Client.Initialise();
            indaba=Stalberg.Indaba.Client.Create();
        }
        public static void DisposeIndaba()
        {
            indaba.Dispose();
            Stalberg.Indaba.Client.Dispose();
        }
        public static void PackData(ref List<string> errorMsgList)
        {
            string errorMsg = string.Empty;
            try
            {
                ExportMaintenanceData_Service service = new ExportMaintenanceData_Service();
                //int processCount = Convert.ToInt32(GetConfigInfo("PackCount"));
                tempDirectory = GetSaveDirection();
                indaba.Destination = GetKeyValue("IndabaDestination");
                Guid guid = Guid.Empty;
                foreach (string fileName in GetFileName())
                {
                   guid=indaba.Enqueue(fileName);
                   if (guid != Guid.Empty)
                       ClearFile(fileName);
                   else
                   {
                       service.RecordLog("Import indaba error "+fileName + "");
                   }
                }
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "PackData", ex.ToString());
                errorMsgList.Add(errorMsg);
                throw new Exception(errorMsg);
            }
        }

        #region base code 
        public static void ClearFile(string fileName)
        {
            if (File.Exists(fileName))
                File.Delete(fileName);
        }
        public static List<string> GetFileName()
        {
            List<string> files = new List<string>();
            DirectoryInfo dir = new DirectoryInfo(tempDirectory);
            foreach (FileInfo file in dir.GetFiles("*.imx"))
            {
                files.Add(file.FullName);
            }
            return files;
        }
        public static string GetIndabaConn()
        {
            ExportMaintenanceData_Service mainService = new ExportMaintenanceData_Service();
            return mainService.GetIndabaConnectstring();
        }
        private static string GetConfigInfo(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
        public static string GetSaveDirection()
        {
            ExportMaintenanceData_Service mainService = new ExportMaintenanceData_Service();
           return mainService.GetFileTempleSavePath();
        }
        public static string GetKeyValue(string key)
        {
           Hashtable ht=QueueMaintenanceData.GetServiceParams();
           if (ht.Count > 0)
               return ht[key].ToString();
           else
               return "";
            
        }
        #endregion
    }
}
