﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.QueueLibrary;
using SIL.MobileInterface.DAL;
using SIL.MobileInterface.DAL.Data;
using SIL.MobileInterface.DAL.Data.SqlClient;
using SIL.MobileInterface.DAL.Services;
using SIL.MobileInterface.DAL.Entities;
using System.Data.SqlClient;
using System.Collections.Specialized;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.ServiceLibrary;
using System.Reflection;

namespace SIL.AARTOService.ExportMaintenanceData
{
    class MobileMaintenanceData : ClientService
    {
        private MobileDeviceMaintenanceData _MobileDeviceMaintenanceData;
        private ExportMaintenanceData_Service _Export_Service;
        public MobileDeviceMaintenanceData MobileDeviceobj
        {
            get 
            {
                return _MobileDeviceMaintenanceData;
            }
        }
        public ExportMaintenanceData_Service Export_Service
        {
            get
            {
                return _Export_Service;
            }
            set
            {
                _Export_Service = value;
            }
        }
        public override void MainWork(ref List<QueueItem> queueList)
        {
            //throw new NotImplementedException();
        }
        public string AutCode
        {
            get;
            set;
        }
        public int AartoMaintenanceDataIntNo { get; set; }

        public MobileMaintenanceData()
            :base("","",new Guid("0A7B7FB2-2623-46F2-86DD-E8324BDCC65F"))
        {
            _MobileDeviceMaintenanceData= new MobileDeviceMaintenanceData();
        }
        public void InsertDataToMobile(List<MobileMaintenanceData> mobileData,ref string errorMsg)
        {
            MobileDeviceMaintenanceDataService dmSave = new MobileDeviceMaintenanceDataService();
            TList<MobileDeviceMaintenanceData> tList=new TList<MobileDeviceMaintenanceData>();
            foreach (MobileMaintenanceData item in mobileData)
            {
                    if (!string.IsNullOrEmpty(item.AutCode))
                    {
                        if (GetAuthorityItem(item.AutCode.Trim()) != null)
                            item.MobileDeviceobj.AuthorityId = GetAuthorityItem(item.AutCode.Trim()).AuthorityId;
                    }
                    OneDataOneTranScope(item.MobileDeviceobj, "Save");
                    //update state
                   Export_Service.UpdateMaintenanceDataState(item.AartoMaintenanceDataIntNo);
            }
        }
        public static TList<Authority> GetAuthorityList()
        {
            AuthorityService authService = new AuthorityService();
            return authService.GetAll();
        }
        public static Authority GetAuthorityItem(string AutCode)
        {
            Authority authItem = new Authority();
            AuthorityService authService = new AuthorityService();
            authItem = authService.GetByAutCode(AutCode);
            return authItem;
        }
        #region update base table
        public void DealMetro(MobileMaintenanceData item,string action)
        {
            Metro mt = new Metro();
            MetroQuery query = new MetroQuery();
            MetroService service = new MetroService();
            query.Append(MetroColumn.MetCode, item.MobileDeviceobj.MdmdColumn0DataValue);
            List<Metro> metroList = service.Find(query as IFilterParameterCollection).ToList();
            try
            {
                switch (action)
                {
                    case "I":
                        mt.MetCode = item.MobileDeviceobj.MdmdColumn0DataValue;
                        mt.MetName = item.MobileDeviceobj.MdmdColumn2DataValue;
                        if (metroList.Count == 0)
                            OneDataOneTranScope(mt, "Save");
                        break;
                    case "U":
                        if (metroList.Count > 0)
                        {
                            metroList[0].MetName = item.MobileDeviceobj.MdmdColumn2DataValue;
                            OneDataOneTranScope(metroList[0], "Update");
                        }
                        break;
                    case "D":
                        AuthorityService a_Serivce = new AuthorityService();
                        AuthorityQuery aut_Query = new AuthorityQuery();
                        aut_Query.Append(AuthorityColumn.MetroId, metroList[0].MetroId.ToString());
                        if (a_Serivce.Find(aut_Query as IFilterParameterCollection).Count == 0)
                            OneDataOneTranScope(metroList[0], "Delete");
                        break;
                }
            }
            catch (Exception ex)
            {
                item._Export_Service.Logger.Error(ex.Message);
            }
        }
        public void DealAuthority(MobileMaintenanceData item, string act)
        {
            TList<Authority> AuthorityoList = new TList<Authority>();
            AuthorityService a_Service = new AuthorityService();
            Authority aut = new Authority();
            try
            {
                switch (act)
                {
                    case "I":
                        string autCode = item.MobileDeviceobj.MdmdColumn1DataValue.Trim();
                        aut.AutName = item.MobileDeviceobj.MdmdColumn3DataValue.Trim();
                        aut.AutNo = item.MobileDeviceobj.MdmdColumn2DataValue.Trim();
                        aut.AutCode = autCode;
                        aut.MetroId = GetMetro(item.MobileDeviceobj.MdmdColumn0DataValue).MetroId;
                        aut.AutBlockSizeSendToMobileDevice = 50;
                        if (a_Service.GetByAutCode(autCode) == null)
                            OneDataOneTranScope(aut, "Save");
                        break;
                    case "U":
                        aut = a_Service.GetByAutCode(item.MobileDeviceobj.MdmdColumn1DataValue);
                        if (aut != null)
                        {
                            aut.AutName = item.MobileDeviceobj.MdmdColumn3DataValue.Trim();
                            aut.MetroId = GetMetro(item.MobileDeviceobj.MdmdColumn0DataValue).MetroId;
                            OneDataOneTranScope(aut, "Update");
                            //a_Service.Update(aut);
                        }
                        break;
                    case "D":
                        aut = a_Service.GetByAutCode(item.MobileDeviceobj.MdmdColumn1DataValue);
                        if (aut != null)
                        {
                            OneDataOneTranScope(aut, "Delete");
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                item._Export_Service.Logger.Error(ex.Message);
            }
        }
        public void DealTrafficOfficer(MobileMaintenanceData item, string act)
        {
            TList<TrafficOfficer> TrafficOfficerList = new TList<TrafficOfficer>();
            TrafficOfficerService t_Service = new TrafficOfficerService();
            TrafficOfficer to;
            Metro metro = GetMetro(item.MobileDeviceobj.MdmdColumn0DataValue);
            int metroId = (metro == null ? -1 : metro.MetroId);
            try
            {
                if (metroId > 0)
                {
                    to = t_Service.GetByTrOfForceNumberMetroId(item.MobileDeviceobj.MdmdColumn1DataValue, metroId);
                    switch (act)
                    {
                        case "I":
                            string forceNumber = item.MobileDeviceobj.MdmdColumn1DataValue;
                            to = new TrafficOfficer();
                            to.MetroId = metroId;
                            to.TrOfForceNumber = forceNumber;
                            to.TrOfGroup = item.MobileDeviceobj.MdmdColumn2DataValue;
                            to.TrOfSurname = item.MobileDeviceobj.MdmdColumn3DataValue;
                            to.TrOfInitials = item.MobileDeviceobj.MdmdColumn4DataValue;
                            to.TrOfInfrastructureNumber = item.MobileDeviceobj.MdmdColumn5DataValue;
                            if (t_Service.GetByTrOfForceNumberMetroId(forceNumber, metroId) == null)
                                OneDataOneTranScope(to, "Save");
                            break;
                        case "U":
                            if (to != null)
                            {
                                to.TrOfGroup = item.MobileDeviceobj.MdmdColumn2DataValue;
                                to.TrOfSurname = item.MobileDeviceobj.MdmdColumn3DataValue;
                                to.TrOfInitials = item.MobileDeviceobj.MdmdColumn4DataValue;
                                to.TrOfInfrastructureNumber = item.MobileDeviceobj.MdmdColumn5DataValue;
                                OneDataOneTranScope(to, "Update");
                            }
                            break;
                        case "D":
                            if (to != null)
                            {
                                OneDataOneTranScope(to, "Delete");
                            }
                            break;
                    }
                }
                else
                {
                    item._Export_Service.Logger.Error(string.Format("Can't find Metro code {0} in MobileInterface", item.MobileDeviceobj.MdmdColumn0DataValue));
                }
            }
            catch (Exception ex)
            {
                item._Export_Service.Logger.Error(ex.Message);
            }
        }
        public Metro GetMetro(string metroCode)
        {
            MetroService ms = new MetroService();
            MetroQuery query = new MetroQuery();
            Metro metro = null;
            query.Append(MetroColumn.MetCode, metroCode);
            List<Metro> list = ms.Find(query).ToList();
            if (list.Count > 0)
                metro = list[0];

            return metro;
        }
        //one data on scope
        public void OneDataOneTranScope(object obj, string methodName)
        {
            try
            {
                string service = "SIL.MobileInterface.DAL.Services";
                Assembly assembly = Assembly.Load(service); //Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SIL.AARTO.DAL.Services.dll"));
                Object objService = assembly.CreateInstance(service + "." + obj.GetType().Name + "Service");
                MethodInfo method = objService.GetType().GetMethod(methodName, new Type[] { obj.GetType() });
                object flag = method.Invoke(objService, new object[] { obj });
                if (flag == null)
                    Export_Service.Logger.Info(obj.GetType().Name + " method:" + methodName + "affected rows 0 .");
            }
            catch (TargetInvocationException sqlex)
            {
                Export_Service.Logger.Error(sqlex.InnerException.Message);
            }
            catch (Exception ex)
            {
                Export_Service.Logger.Error(ex.Message);
            }
        }
        #endregion
    }
}
