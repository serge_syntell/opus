﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.IMX.DAL.Data;
using SIL.IMX.DAL.Data.SqlClient;
using SIL.IMX.DAL.Entities;
using SIL.IMX.DAL.Services;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Collections;
using SIL.AARTOService.Library;

namespace SIL.AARTOService.ExportMaintenanceData
{
    class IMXMaintenanceData
    {
        public void ARRTOMappingIMX(List<Maintenance> maintenanceList, ref List<string> errorMsgList)
        {
           bool localFlag= ExportMaintenanceData_Service.CheckMobileFlag("BackOfficeLocation", "local");
           bool Indaba = ExportMaintenanceData_Service.CheckMobileFlag("BackOfficeLocation", "Remote");
           string errorMsg = string.Empty;
           if (localFlag)
            {
               //List<Maintenance> maintenanceList=new List<Maintenance>();
               //MappingMobileObject(mobileList, ref maintenanceList, ref errorMsg);
               //string currentPath = AppDomain.CurrentDomain.BaseDirectory + "AARTOMappingIMX.xml";
               //XElement xTree = XElement.Load(currentPath);

               PushDataToIMX pushDataToIMX = new PushDataToIMX(ref errorMsg);
               pushDataToIMX.ARRTOMappingIMX(maintenanceList,ref errorMsgList);
            }
           if (Indaba)
           {
               GeneratedXML generatedXML = new GeneratedXML();
               generatedXML.GeneratedXMLFile(maintenanceList, ref errorMsgList);
               //insert into indaba
               DoIndaba.InitIndaba();
               DoIndaba.PackData(ref errorMsgList);

               DoIndaba.DisposeIndaba();
           }
        }
        /*
        public void MappingMobileObject(List<MobileMaintenanceData> mobileList,ref List<Maintenance> mainList, ref string errorMsg)
        {
            try
            {
                foreach (MobileMaintenanceData mobile in mobileList)
                {
                    Maintenance baseMaintenance = new Maintenance();
                    PropertyInfo[] properties = baseMaintenance.GetType().GetProperties(BindingFlags.Instance | BindingFlags.IgnoreCase | BindingFlags.Public);
                    foreach (PropertyInfo property in properties)
                    {
                        string basePropertyName = property.Name;
                        PropertyInfo mobileProperty = mobile.MobileDeviceobj.GetType().GetProperty(basePropertyName, BindingFlags.Instance | BindingFlags.IgnoreCase | BindingFlags.Public);
                        if (mobileProperty != null)
                        {
                            object mobilePropertyValue = mobileProperty.GetValue(mobile.MobileDeviceobj, null);
                            if (mobilePropertyValue != null)
                                property.SetValue(baseMaintenance, mobilePropertyValue.ToString(), null);
                        }
                    }
                    mainList.Add(baseMaintenance);
                }
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "MappingMobileObject", ex.ToString());
                throw new Exception(errorMsg);
            }
        }*/
    }
         
}
