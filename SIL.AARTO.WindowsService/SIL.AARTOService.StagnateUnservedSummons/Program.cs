﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.StagnateUnservedSummons
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.StagnateUnservedSummons"
                ,
                DisplayName = "SIL.AARTOService.StagnateUnservedSummons"
                ,
                Description = "SIL AARTOService StagnateUnservedSummons"
            };

            ProgramRun.InitializeService(new StagnateUnservedSummons(), serviceDescriptor, args);
        }
    }
}
