﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.AARTOService.DAL;
using SIL.QueueLibrary;
using SIL.AARTOService.Library;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Collections;
using Stalberg.TMS;
using SIL.AARTOService.Resource;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using System.Transactions;
namespace SIL.AARTOService.StagnateUnservedSummons
{
    class StagnateUnservedSummonsService : ServiceDataProcessViaQueue
    {
        public StagnateUnservedSummonsService() :
            base("", "", new Guid("ACB3F2B9-F184-45EC-95E3-76A482E58B59"), ServiceQueueTypeList.StagnantSummons)
        {
            this._serviceHelper = new AARTOServiceBase(this, false);
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        SummonsService summonsService = new SummonsService();
        NoticeSummonsService noticeSummonsService = new NoticeSummonsService();
        NoticeService noticeService = new NoticeService();
        ChargeService chargeService = new ChargeService();
        SumChargeService sumChargeService = new SumChargeService();
        EvidencePackService evidencePackService = new EvidencePackService();

        public sealed override void InitialWork(ref QueueItem item)
        {
           string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString().Trim();

            if (!string.IsNullOrEmpty(body) && ServiceUtility.IsNumeric(body))
            {
                item.IsSuccessful = true;
                item.Status = QueueItemStatus.Discard;
            }
            else
            {
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.LastUser, item.Body), 
                    LogType.Error, ServiceOption.Continue, item, QueueItemStatus.Discard);
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            int sumIntNo;
            string msg = string.Empty;

            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                try
                {
                    if (item.IsSuccessful)
                    {
                        AARTOBase.InternalTransaction(item, null, () =>
                        {
                            sumIntNo = Convert.ToInt32(item.Body);
                            Summons summonsEntity = summonsService.GetBySumIntNo(sumIntNo);

                            //Summons Status == 640 && Court date < now

                            //Jerry 2015-01-13 change
                            //update by Rachel 2014-09-19 for 5332
                            //if (summonsEntity != null && summonsEntity.SummonsStatus == (int)ChargeStatusList.SummonsSentToSummonsServer
                            //    && summonsEntity.SumCourtDate < DateTime.Now)
                            //end update by Rachel 2014-09-19 for 5332
                           //if (summonsEntity.SumCourtDate < DateTime.Now)
                            //Jerry 2015-03-12 change the check <= 640
                            if (summonsEntity != null && summonsEntity.SummonsStatus <= (int)ChargeStatusList.SummonsSentToSummonsServer
                                && summonsEntity.SumCourtDate < DateTime.Now)
                            {
                                //Update Summons Status
                                summonsEntity.SumPrevSummonsStatus = summonsEntity.SummonsStatus;
                                summonsEntity.SummonsStatus = (int)ChargeStatusList.SummonsNotServedNotCaptured; //649
                                summonsEntity.LastUser = AARTOBase.LastUser;
                                summonsService.Update(summonsEntity);

                                //Update SumCharge Status
                                SIL.AARTO.DAL.Entities.TList<SumCharge> sumChargeList = sumChargeService.GetBySumIntNo(sumIntNo);
                                //SumChargeStatus = 640
                                //Jerry 2015-03-12 change the check to <= 640
                                foreach (SumCharge sumChargeEntity in sumChargeList.Where(s => s.SumChargeStatus <= (int)ChargeStatusList.SummonsSentToSummonsServer))
                                {
                                    sumChargeEntity.SchPrevSumChargeStatus = sumChargeEntity.SumChargeStatus;
                                    sumChargeEntity.SumChargeStatus = (int)ChargeStatusList.SummonsNotServedNotCaptured; //649
                                    sumChargeEntity.LastUser = AARTOBase.LastUser;
                                    sumChargeService.Update(sumChargeEntity);
                                }

                                //Update Notice Status
                                Notice noticeEntity = noticeService.GetByNotIntNo(noticeSummonsService.GetBySumIntNo(sumIntNo).First().NotIntNo);
                                //Jerry 2015-03-12 add status check <= 640
                                if (noticeEntity != null && noticeEntity.NoticeStatus <= (int)ChargeStatusList.SummonsSentToSummonsServer)
                                {
                                    noticeEntity.NotPrevNoticeStatus = noticeEntity.NoticeStatus;
                                    noticeEntity.NoticeStatus = (int)ChargeStatusList.SummonsNotServedNotCaptured; //649
                                    noticeEntity.LastUser = AARTOBase.LastUser;
                                    noticeService.Update(noticeEntity);
                                }

                                if (noticeEntity != null)
                                {
                                    //Update Charge Status
                                    SIL.AARTO.DAL.Entities.TList<Charge> chargeList = chargeService.GetByNotIntNo(noticeEntity.NotIntNo);
                                    //ChargeStatus = 640
                                    //Jerry 2015-03-12 change the check to <= 640
                                    foreach (Charge chargeEntity in chargeList.Where(ch => ch.ChargeStatus <= (int)ChargeStatusList.SummonsSentToSummonsServer))
                                    {
                                        chargeEntity.PreviousStatus = chargeEntity.ChargeStatus;
                                        chargeEntity.ChargeStatus = (int)ChargeStatusList.SummonsNotServedNotCaptured; //649
                                        chargeEntity.LastUser = AARTOBase.LastUser;
                                        chargeService.Update(chargeEntity);
                                    }

                                    //Add EvidencePack 
                                    EvidencePack evidencePack = new EvidencePack();
                                    evidencePack.ChgIntNo = chargeList.FirstOrDefault().ChgIntNo;
                                    evidencePack.EpItemDate = DateTime.Now;
                                    evidencePack.EpItemDescr = ResourceHelper.GetResource("EvidencePackStagnantSummons");
                                    evidencePack.EpSourceTable = "Summons";
                                    evidencePack.EpSourceId = sumIntNo;
                                    evidencePack.LastUser = AARTOBase.LastUser;
                                    evidencePack.EpTransactionDate = DateTime.Now;
                                    evidencePack.EpItemDateUpdated = true;
                                    evidencePack.EpatIntNo = (int)EpActionTypeList.Others;
                                    evidencePackService.Insert(evidencePack);
                                }

                                AARTOBase.LogProcessing(ResourceHelper.GetResource("InfoStagnantSummons", sumIntNo), LogType.Info, ServiceOption.Continue, item, item.Status);
                            }
                            return true;
                        });
                        
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.LogProcessing(ex, LogType.Error, ServiceOption.ContinueButQueueFail, item);
                }
            }

        }
    }
}
