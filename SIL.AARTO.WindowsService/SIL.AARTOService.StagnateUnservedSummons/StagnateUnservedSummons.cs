﻿using SIL.ServiceLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SIL.AARTOService.StagnateUnservedSummons
{
    partial class StagnateUnservedSummons : ServiceHost
    {
        public StagnateUnservedSummons()
            : base("", new Guid("6DA032BE-251C-4800-9AE7-B8C2CBB7CB04"), new StagnateUnservedSummonsService())
        {
            InitializeComponent();
        }
    }
}
