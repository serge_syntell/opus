﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.SummonsGracePeriodCheck
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.SummonsGracePeriodCheck"
                ,
                DisplayName = "SIL.AARTOService.SummonsGracePeriodCheck"
                ,
                Description = "SIL AARTOService SummonsGracePeriodCheck"
            };

            ProgramRun.InitializeService(new SummonsGracePeriodCheck(), serviceDescriptor, args);
        }
    }
}
