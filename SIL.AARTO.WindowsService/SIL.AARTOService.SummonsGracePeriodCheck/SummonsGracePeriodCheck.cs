﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.SummonsGracePeriodCheck
{
    partial class SummonsGracePeriodCheck : ServiceHost
    {
        public SummonsGracePeriodCheck()
            : base("", new Guid("E03F777E-1BEE-4466-BD4A-44D89375CE10"), new SummonsGracePeriodCheckService())
        {
            InitializeComponent();
        }
    }
}
