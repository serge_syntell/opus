﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using Stalberg.TMS;
using System.Data.SqlClient;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using System.Reflection;
using Stalberg.TMS.Data.Datasets;
using CrystalDecisions.Shared;
using SIL.AARTOService.Resource;

namespace SIL.AARTOService.SummonsGracePeriodCheck
{
    public class SummonsGracePeriodCheckService : ClientService
    {
        public SummonsGracePeriodCheckService() 
            : base("", "", new Guid("76138C9D-A9A9-4231-84EB-0B251A6FD7F5"))
        {
            this._serviceHelper = new AARTOServiceBase(this);
            //get connect string
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
        }

        // Constants
        private const string REPORT_NAME = "Summons with Expired Grace Periods";
        private const string TICKET_PROCESSOR_TMS = "TMS";
        private const string TICKET_PROCESSOR_AARTO = "AARTO";
        private const string TICKET_PROCESSOR_JMPD_AARTO = "JMPD_AARTO";

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AuthorityDB authorityDB = null;

        string connectStr = string.Empty;
        //Jerry 2012-04-11 add EmailToAdministrator
        //string emailToCustomer = string.Empty;
        string emailToAdministrator = string.Empty;

        // jerry 2012-02-27 add, in order to send one email
        List<string> attachments = null;

        public override void InitialWork(ref QueueItem item)
        {
            //get to email address
            GetServiceParameters();

            authorityDB = new AuthorityDB(this.connectStr);
            // jerry 2012-02-27 add
            attachments = new List<string>();
        }

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            //if (emailToCustomer.Equals(string.Empty))
            //{
            //    StopService();
            //    return;
            //}

            //get authority list
            List<AuthorityDetails> authorityList = AARTOBase.AuthorityList;

            foreach (AuthorityDetails authority in authorityList)
            {

                //if (authority.AutTicketProcessor != TICKET_PROCESSOR_AARTO
                //       && authority.AutTicketProcessor != TICKET_PROCESSOR_TMS
                //       && authority.AutTicketProcessor != TICKET_PROCESSOR_JMPD_AARTO)
                //    continue;
                
                //1. get report datasource
                SummonsDB db = new SummonsDB(this.connectStr);
                RepresentationDB rdb = new RepresentationDB(connectStr);
                dsSummonsGraceExpiry_WS ds = db.SummonsWithExpiredGracePeriod_WS(authority.AutIntNo);

                //2. set report
                if (ds != null && ds.Tables[1].Rows.Count > 0)
                {
                    // jerry 2012-02-28 change
                    //string fileName = Path.Combine(Path.GetTempPath(), string.Format("{0}.pdf", REPORT_NAME));
                    string fileName = Path.Combine(Path.GetTempPath(), string.Format("{0}_{1}.pdf", REPORT_NAME, authority.AutName));
                    FileInfo file = new FileInfo(fileName);
                    if (file.Exists)
                        file.Delete();
                    file = null;

                    // create report
                    //Jerry 2012-06-29 get report file from Report folder
                    //string reportFile = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Reports/SummonsGracePeriodExpired.rpt");
                    string reportFile = Path.Combine(AARTOBase.GetConnectionString(ServiceConnectionNameList.ReportFolder, ServiceConnectionTypeList.UNC), "SummonsGracePeriodExpired.rpt");

                    if (!File.Exists(reportFile))
                    {
                        //this.Logger.Error(ResourceHelper.GetResource("ErrorSummonsGracePeriodExpiredNoExists"));
                        //StopService();
                        AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorSummonsGracePeriodExpiredNoExists"), true);
                        return;
                    }

                    ReportDocument report = new ReportDocument();
                    report.Load(reportFile);

                    report.SetDataSource(ds.Tables[1]);

                    // Set up export options
                    DiskFileDestinationOptions fileOptions = new DiskFileDestinationOptions();
                    fileOptions.DiskFileName = fileName;

                    ExportOptions exportOptions = report.ExportOptions;
                    exportOptions.DestinationOptions = fileOptions;
                    exportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    exportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;

                    // Export the PDF file
                    report.Export();

                    //string message = ResourceHelper.GetResource("EmailMsgInSummonsGracePeriod");
                    //string subject = REPORT_NAME;
                    //List<string> attachments = new List<string>();
                    attachments.Add(fileName);

                    ////send email
                    //try
                    //{
                    //    SendEmailManager.SendEmail(emailToCustomer, subject, message, attachments);
                    //}
                    //catch (Exception ex)
                    //{
                    //    AARTOBase.ErrorProcessing(ex);
                    //}
                }
                if(ds != null)
                    ds.Dispose();
            }
            // jerry 2012-02-27 add, in order to send one email
            if (attachments.Count > 0)
            {
                string message = ResourceHelper.GetResource("EmailMsgInSummonsGracePeriod");
                string subject = REPORT_NAME;
                //send email
                try
                {
                    SendEmailManager.SendEmail(emailToAdministrator, subject, message, attachments);
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ex);
                }

                //delete file in temporary folder
                foreach (string fileName in attachments)
                {
                    FileInfo file = new FileInfo(fileName);
                    if (file.Exists)
                        file.Delete();
                }

                attachments.Clear();
            }
            MustSleep = true;// jerry 2012-02-24 change
        }

        private void GetServiceParameters()
        {
            if (ServiceParameters != null)
            {
                if (string.IsNullOrEmpty(this.emailToAdministrator)
&& ServiceParameters.ContainsKey("EmailToAdministrator") && !string.IsNullOrEmpty(ServiceParameters["EmailToAdministrator"]))
                {
                    this.emailToAdministrator = ServiceParameters["EmailToAdministrator"];
                    if (!SendEmailManager.CheckEmailAddress(this.emailToAdministrator))
                    {
                        //this.Logger.Error(ResourceHelper.GetResource("ErrorEmailAddrInSummonsGracePeriod"));
                        //StopService();
                        AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorEmailAddrInSummonsGracePeriod"), true);
                    }
                }
                else if (string.IsNullOrEmpty(this.emailToAdministrator) || !ServiceParameters.ContainsKey("EmailToAdministrator") || string.IsNullOrEmpty(ServiceParameters["EmailToAdministrator"]))
                {
                    //this.Logger.Error(ResourceHelper.GetResource("ErrorNoEmailAddrInSummonsGracePeriod"));
                    //StopService();
                    AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorNoEmailAddrInSummonsGracePeriod"), true);
                }
            }
            else
            {
                //this.Logger.Error(ResourceHelper.GetResource("ErrorNoEmailAddrInSummonsGracePeriod"));
                //StopService();
                AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorNoEmailAddrInSummonsGracePeriod"), true);
            }
        }

        //private void StopService()
        //{
        //    this.MustStop = true;
        //    ((SIL.ServiceLibrary.ServiceHost)this.ServiceHost).Stop();
        //}
    }
}
