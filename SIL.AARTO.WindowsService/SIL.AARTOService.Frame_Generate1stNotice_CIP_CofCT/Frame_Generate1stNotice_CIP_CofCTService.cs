﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using System.Configuration;
using SIL.QueueLibrary;
using SIL.AARTOService.Library;
using System.Data;
using System.Diagnostics;
using Stalberg.TMS;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Collections;
using SIL.AARTOService.Resource;
using SIL.ServiceQueueLibrary.DAL.Services;


namespace SIL.AARTOService.Frame_Generate1stNotice_CIP_CofCT
{
    public class Frame_Generate1stNotice_CIP_CofCTService : ServiceDataProcessViaQueue
    {
        public Frame_Generate1stNotice_CIP_CofCTService()
            : base("", "", new Guid("375DCD07-18C5-4E73-A0DE-9FE6FCA2D89E"), ServiceQueueTypeList.Frame_Generate1stNotice)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            AARTOBase.OnServiceStarting = () =>
            {
                this.strAutCode = null;
            };
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        string strAutCode = null;
        string connectStr;
        int AutIntNo;
        int NotIntNo;
        int frameIntNo;
        string DataWashingActived = null;
        string IsUseDataWashingAddr = null;
        int DataWashingRecycleMonth;
        string strAutNo = string.Empty;
        int delayActionDate_InHours;

        //Jerry 2012-04-11 change
        //public override void PrepareWork()
        //{
        //    connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
        //    strAutCode = null;
        //}

        #region Jerry 2012-04-11 change
        //public override void InitialWork(ref QueueItem item)
        //{
        //    string body = string.Empty;
        //    if (item.Body != null)
        //        body = item.Body.ToString();
        //    if (!string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(item.Group))
        //    {
        //        try
        //        {
        //            item.IsSuccessful = true;
        //            item.Status = QueueItemStatus.Discard;

        //            if (!int.TryParse(body, out frameIntNo))
        //            {
        //                AARTOBase.ErrorProcessing(ref item);
        //                return;
        //            }

        //            FilmDB filmDB = new FilmDB(connectStr);
        //            FrameDB frameDB = new FrameDB(connectStr);

        //            FrameDetails frame = frameDB.GetFrameDetails(frameIntNo);
        //            FilmDetails film = filmDB.GetFilmDetails(frame.FilmIntNo);

        //            if (!string.IsNullOrEmpty(film.FilmLockUser) || string.IsNullOrEmpty(film.FilmNo))
        //            {
        //                AARTOBase.ErrorProcessing(ref item);
        //                return;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            AARTOBase.ErrorProcessing(ref item, ex);
        //        }
        //    }
        //}
        #endregion

        public override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;

                    if (!int.TryParse(body, out frameIntNo))
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        return;
                    }

                    if (strAutCode != item.Group.Trim())
                    {
                        strAutCode = item.Group.Trim();
                        //jerry 2012-04-05 check group value
                        if (string.IsNullOrWhiteSpace(strAutCode))
                        {
                            AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
                            return;
                        }

                        AuthorityDetails auth = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode.Trim());
                        rules.InitParameter(connectStr, strAutCode);
                        if (auth.AutTicketProcessor.ToLower() != "cip_cofct")
                        {
                            AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("WrongTicketProcessor"), true);
                            return;
                        }

                        AutIntNo = auth.AutIntNo;
                        strAutNo = auth.AutNo.Trim();
                        DataWashingActived = rules.Rule("9060").ARString.Trim();
                        IsUseDataWashingAddr = rules.Rule("9110").ARString.Trim();
                        DataWashingRecycleMonth = rules.Rule("9080").ARNumeric;
                    }

                    FilmDB filmDB = new FilmDB(connectStr);
                    FrameDB frameDB = new FrameDB(connectStr);
                    FrameDetails frame = frameDB.GetFrameDetails(frameIntNo);

                    //Jerry 2012-05-29 add VerificationBin, check the 2310,2311 authority rules when frame status is just 600
                    if (frame.FrameStatus.Trim() == "600" && rules.Rule("2310").ARString == "Y")
                    {
                        SIL.AARTO.DAL.Entities.VerificationBin verificationBin = new SIL.AARTO.DAL.Services.VerificationBinService().Find("VeBiRegNo=" + frame.RegNo.Trim()).FirstOrDefault();
                        if (verificationBin != null)
                        {
                            int daysOfCutOff = rules.Rule("2311").ARNumeric;
                            if (((TimeSpan)(DateTime.Now - frame.OffenceDate)).Days <= daysOfCutOff)
                            {
                                //set frame status to 850
                                SIL.AARTO.DAL.Entities.Frame frameEntity = new SIL.AARTO.DAL.Services.FrameService().GetByFrameIntNo(this.frameIntNo);
                                frameEntity.FrameStatus = 850;
                                new SIL.AARTO.DAL.Services.FrameService().Save(frameEntity);

                                //set current queue status
                                item.IsSuccessful = false;
                                item.Status = QueueItemStatus.Discard;

                                //push current queue
                                QueueItem pushItem = null;
                                pushItem = new QueueItem()
                                {
                                    Body = item.Body,
                                    Group = item.Group,
                                    ActDate = DateTime.Now.AddHours(72),
                                    QueueType = ServiceQueueTypeList.Frame_Generate1stNotice
                                };
                                QueueItemProcessor queueProcessor = new QueueItemProcessor();
                                queueProcessor.Send(pushItem);

                                return;
                            }
                        }
                    }

                    //Jerry 2012-05-29 add 860, 850 status
                    if (frame == null || (frame.FrameStatus.Trim() != "600" && frame.FrameStatus.Trim() != "800" && frame.FrameStatus.Trim() != "850" && frame.FrameStatus.Trim() != "860"))
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        return;
                    }

                    FilmDetails film = filmDB.GetFilmDetails(frame.FilmIntNo);
                    if (film == null || !string.IsNullOrEmpty(film.FilmLockUser) || string.IsNullOrEmpty(film.FilmNo))
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        return;
                    }

                    int notIntNo = new NoticeDB(connectStr).GetNoticeByFrame(this.frameIntNo);
                    if (notIntNo > 0)
                    {
                        AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("NoticeAlreadyExists"), frameIntNo), false, QueueItemStatus.Discard);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
            GetServiceParameters();
        }

        #region Jerry 2012-04-11 change
        //public sealed override void MainWork(ref List<QueueItem> queueList)
        //{
        //    ServiceQueueService queueService = new ServiceQueueService();
        //    ServiceQueue queue = queueService.GetBySeQuId(31182);

        //    string msg = string.Empty;
        //    QueueItemProcessor queueProcessor = new QueueItemProcessor();

        //    int index = 0;
        //    for (int i = 0; i < queueList.Count; i++)
        //    {
        //        QueueItem item = queueList[i];
        //        if (!item.IsSuccessful)
        //            continue;

        //        index++;
        //        try
        //        {
        //            if (strAutCode != item.Group.Trim())
        //            {
        //                //the new batch is coming, push queue for old batch
        //                if(!String.IsNullOrEmpty(strAutCode) && index > 1)
        //                    PushQuueForInfringementFile(strAutCode);

        //                strAutCode = item.Group.Trim();
        //                //jerry 2012-04-05 check group value
        //                if (string.IsNullOrWhiteSpace(strAutCode))
        //                {
        //                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
        //                    return;
        //                }

        //                AuthorityDetails auth = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode.Trim());
        //                AARTORules.GetSingleTon().InitParameter(connectStr, strAutCode);
        //                if (auth.AutTicketProcessor.ToLower() != "cip_cofct")
        //                {
        //                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("WrongTicketProcessor"), true);
        //                    return;
        //                }

        //                AutIntNo = auth.AutIntNo;
        //                DataWashingActived = rules.Rule("9060").ARString.Trim();
        //                IsUseDataWashingAddr = rules.Rule("9110").ARString.Trim();
        //                DataWashingRecycleMonth = rules.Rule("9080").ARNumeric;
        //            }

        //            bool failed = CreatingNotice_CC(frameIntNo, ref msg, item);

        //            //jerry 2012-04-05 change
        //            //if (failed)
        //            //{
        //            //    //jerry 2012-03-28 if this frame is not eligible for notice generation
        //            //    if (item.Status == QueueItemStatus.Discard)
        //            //    {
        //            //        AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.Discard);
        //            //        return;
        //            //    }
        //            //    //jerry 2012-03-29 if FilmLockUser IS NOT NULL
        //            //    else if (item.Status == QueueItemStatus.UnKnown)
        //            //    {
        //            //        AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false);
        //            //        return;
        //            //    }
        //            //    else
        //            //    {
        //            //        AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
        //            //    }
        //            //}
        //            //else
        //            if(!failed)
        //            {
        //                QueueItem pushItem = new QueueItem();

        //                NoticeDB noticeDB = new NoticeDB(connectStr);
        //                NoticeDetails notice = noticeDB.GetNoticeDetails(NotIntNo);

        //                pushItem = new QueueItem();
        //                pushItem.Body = NotIntNo.ToString();
        //                pushItem.Group = strAutCode;
        //                pushItem.QueueType = ServiceQueueTypeList.SearchNameIDUpdate;
        //                queueProcessor.Send(pushItem);

        //                pushItem = new QueueItem();
        //                pushItem.Body = NotIntNo.ToString();
        //                pushItem.Group = strAutCode;
        //                pushItem.ActDate = notice.NotOffenceDate.AddDays(rules.Rule("NotOffenceDate", "NotSummonsBeforeDate").DtRNoOfDays);
        //                pushItem.QueueType = ServiceQueueTypeList.SummonsServicePeriodExpired;
        //                queueProcessor.Send(pushItem);

        //                item.Status = QueueItemStatus.Discard;
        //            }

        //            //if(!string.IsNullOrEmpty(msg))
        //            //{
        //            //    this.SendEmail(msg);
        //            //}
        //        }
        //        catch (Exception ex)
        //        {
        //            AARTOBase.ErrorProcessing(ref item, ex);
        //        }
        //    }

        //    //batch finished, push queue
        //    if (queueList.Count > 0)
        //        PushQuueForInfringementFile(strAutCode);
        //}
        #endregion

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            string msg = string.Empty;
            QueueItemProcessor queueProcessor = new QueueItemProcessor();

            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                if (!item.IsSuccessful)
                    continue;

                try
                {
                    bool failed = CreatingNotice_CC(frameIntNo, ref msg, item);
                    if (failed) continue;

                    if (!failed)
                    {
                        QueueItem pushItem = new QueueItem();

                        NoticeDB noticeDB = new NoticeDB(connectStr);
                        NoticeDetails notice = noticeDB.GetNoticeDetails(NotIntNo);

                        pushItem = new QueueItem();
                        pushItem.Body = NotIntNo.ToString();
                        pushItem.Group = strAutCode;
                        pushItem.QueueType = ServiceQueueTypeList.SearchNameIDUpdate;
                        queueProcessor.Send(pushItem);

                        pushItem = new QueueItem();
                        pushItem.Body = NotIntNo.ToString();
                        pushItem.Group = strAutCode;
                        pushItem.ActDate = notice.NotOffenceDate.AddDays(rules.Rule("NotOffenceDate", "NotSummonsBeforeDate").DtRNoOfDays);
                        pushItem.QueueType = ServiceQueueTypeList.SummonsServicePeriodExpired;
                        queueProcessor.Send(pushItem);

                        //Jerry 2012-04-11 add
                        pushItem = new QueueItem();
                        pushItem.Body = NotIntNo.ToString();
                        pushItem.Group = strAutNo + "|" + notice.NotCameraID.Trim();
                        pushItem.QueueType = ServiceQueueTypeList.InfringementFileCreate;
                        pushItem.ActDate = DateTime.Now.AddHours(this.delayActionDate_InHours);
                        queueProcessor.Send(pushItem);
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        //Jerry 2012-04-11 change
        //private void PushQuueForInfringementFile(string strAutCode)
        //{
        //    QueueItemProcessor queueProcessor = new QueueItemProcessor();
        //    QueueItem push = new QueueItem();
        //    push = new QueueItem();
        //    push.Body = strAutCode;
        //    push.QueueType = ServiceQueueTypeList.InfringementFileCreate;
        //    queueProcessor.Send(push);
        //}

        /// <summary>
        /// Jerry 2012-04-16 when success is less than 0 should include FrameIntno 
        /// </summary>
        /// <param name="frameIntNo"></param>
        /// <param name="msg"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        private bool CreatingNotice_CC(int frameIntNo, ref string msg, QueueItem item)
        {
            bool failed = false;
            string errorMsg = "";

            int success = AddNoticeChargeFromFrame_CC(frameIntNo, ref errorMsg);

            if (success == 1)
            {
                errorMsg = ResourceHelper.GetResource("NoDateRule");
                msg = "CreatingNotice: " + errorMsg + " " + DateTime.Now;
                failed = true;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg));
                return failed;
            }

            if (success == 4)
            {
                errorMsg = ResourceHelper.GetResource("NoticeExpired");
                msg = "CreatingNotice: " + errorMsg + " " + DateTime.Now;
                item.IsSuccessful = false;
                item.Status = QueueItemStatus.UnKnown;
                Logger.Info(msg);
                failed = true;
            }
            else if (success == 8)
            {
                errorMsg = string.Format(ResourceHelper.GetResource("NoticeNoChargeRecords"), errorMsg);
                msg = "CreatingNotice_CC: " + errorMsg + " " + DateTime.Now;
                //item.Status = QueueItemStatus.PostPone;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
                failed = true;
            }
            else if (success == -1)
            {
                msg = "CreatingNotice_CC: (frameIntNo:" + frameIntNo + ") " + errorMsg + " " + DateTime.Now;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
                failed = true;
            }
            else if (success == -2)
            {
                //msg = ResourceHelper.GetResource("DuplicateNumberPlates");
                msg = string.Format(ResourceHelper.GetResource("DuplicateNumberPlates"), frameIntNo);
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.Discard);
                failed = true;
            }
            else if (success == -3)
            {
                //msg = ResourceHelper.GetResource("NoticeAlreadyExists");
                msg = string.Format(ResourceHelper.GetResource("NoticeAlreadyExists"), frameIntNo);
                item.Status = QueueItemStatus.Discard;
                item.IsSuccessful = false;
                Logger.Info(msg);
                failed = true;
            }
            else if (success == -4)
            {
                //msg = ResourceHelper.GetResource("NoFineAmount");
                msg = string.Format(ResourceHelper.GetResource("NoFineAmount"), frameIntNo);
                //item.Status = QueueItemStatus.PostPone;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
                failed = true;
            }
            else if (success == -5)
            {
                msg = ResourceHelper.GetResource("InsertNoticeFailed", frameIntNo);
                //item.Status = QueueItemStatus.PostPone;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
                failed = true;
            }
            //jerry 2012-03-28 add
            else if (success == -6)
            {
                msg = ResourceHelper.GetResource("NoticeNotEligible", frameIntNo);
                item.Status = QueueItemStatus.Discard;
                item.IsSuccessful = false;
                Logger.Info(msg);
                failed = true;
            }
            //jerry 2012-03-29 add
            else if (success == -7)
            {
                msg = ResourceHelper.GetResource("NoticeFilmLocked", frameIntNo);
                item.Status = QueueItemStatus.UnKnown;
                item.IsSuccessful = false;
                Logger.Info(msg);
                failed = true;
            }

            return failed;
        }

        private const int STATUS_LOADED = 100;
        private const int STATUS_EXPIRED = 921;
        private const int STATUS_NOTICE = 900;
        private const string CTYPE = "CAM";

        private int AddNoticeChargeFromFrame_CC(int frameIntNo, ref string errMsg)
        {
            DBHelper db = new DBHelper(connectStr);
            SqlParameter[] paras = new SqlParameter[15];
            paras[0] = new SqlParameter("@AutIntNo", AutIntNo);
            paras[1] = new SqlParameter("@FrameIntNo", frameIntNo);
            paras[2] = new SqlParameter("@StatusLoaded", STATUS_LOADED);
            paras[3] = new SqlParameter("@StatusExpired", STATUS_EXPIRED);
            paras[4] = new SqlParameter("@StatusNotice", STATUS_NOTICE);
            paras[5] = new SqlParameter("@CType", CTYPE);
            //paras[6] = new SqlParameter("@LastUser", Process.GetCurrentProcess().ProcessName);
            //Oscar 20120405 change to AARTOBase.lastUser
            paras[6] = new SqlParameter("@LastUser", AARTOBase.LastUser);
            paras[7] = new SqlParameter("@Success", 0);
            paras[8] = new SqlParameter("@NatisLast", rules.Rule("0400").ARString == "Y");
            paras[9] = new SqlParameter("@NoOfDaysIssue", rules.Rule("NotOffenceDate", "NotIssue1stNoticeDate").DtRNoOfDays);
            paras[10] = new SqlParameter("@NoOfDaysPayment", rules.Rule("NotOffenceDate", "NotPaymentDate").DtRNoOfDays);
            paras[11] = new SqlParameter("@NotIntNo", 0);
            paras[12] = new SqlParameter("@DataWashingActived", DataWashingActived);
            paras[13] = new SqlParameter("@IsUseDataWashingAddr", IsUseDataWashingAddr);
            paras[14] = new SqlParameter("@DataWashingRecycleMonth", DataWashingRecycleMonth);

            paras[7].Direction = ParameterDirection.Output;
            paras[11].Direction = ParameterDirection.Output;

            db.ExecuteNonQuery("NoticeChargeAddFromFrame_CC_WS", paras, out errMsg);
            int success = Convert.ToInt32(paras[7].Value);
            if (paras[11].Value != DBNull.Value)
                NotIntNo = Convert.ToInt32(paras[11].Value);
            return success;
        }

        /// <summary>
        /// Jerry 2012-04-11 add
        /// </summary>
        private void GetServiceParameters()
        {
            if (ServiceParameters != null)
            {
                if (this.delayActionDate_InHours == 0 && ServiceParameters.ContainsKey("DelayActionDate_InHours") && !string.IsNullOrEmpty(ServiceParameters["DelayActionDate_InHours"]))
                {
                    this.delayActionDate_InHours = Convert.ToInt32(ServiceParameters["DelayActionDate_InHours"]);
                }
            }
        }
    }
}

