﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Frame_Generate1stNotice_CIP_CofCT
{
    partial class Frame_Generate1stNotice_CIP_CofCT : ServiceHost
    {
        public Frame_Generate1stNotice_CIP_CofCT()
            : base("", new Guid("E41FF4DF-115C-4032-B0D7-0D1A4014CF63"), new Frame_Generate1stNotice_CIP_CofCTService())
        {
            InitializeComponent();
        }
    }
}
