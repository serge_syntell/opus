﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Frame_Generate1stNotice_CIP_CofCT
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.Frame_Generate1stNotice_CIP_CofCT"
                ,
                DisplayName = "SIL.AARTOService.Frame_Generate1stNotice_CIP_CofCT"
                ,
                Description = "SIL AARTOService Frame_Generate1stNotice_CIP_CofCT"
            };

            ProgramRun.InitializeService(new Frame_Generate1stNotice_CIP_CofCT(), serviceDescriptor, args);
        }
    }
}
