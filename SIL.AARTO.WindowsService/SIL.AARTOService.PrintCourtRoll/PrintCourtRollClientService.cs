﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.AARTO.BLL.Utility.PrintFile;
using Stalberg.TMS;
using SIL.AARTOService.Resource;
using SIL.QueueLibrary;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using System.Data;

namespace SIL.AARTOService.PrintCourtRoll
{
    public class PrintCourtRollFileName
    {
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public string AutCode { get; set; }
        public string CourtName { get; set; }
        public int CrtIntNo { get; set; }
        public string CourtRoomNo { get; set; }
        public int CrtRIntNo { get; set; }
        public string CourtDate { get; set; }
        public string PrintDate { get; set; }
        DBHelper db;
        string connStr;

        public PrintCourtRollFileName(string connStr)
        {
            this.connStr = connStr;
            db = new DBHelper(connStr);
        }

        public PrintCourtRollFileName DepartName(int pfnIntNo)
        {
            PrintCourtRollFileName entity = null;
            string msg;
            SqlParameter[] paras = new SqlParameter[1];
            paras[0] = new SqlParameter("@PFNIntNo", pfnIntNo);
            object obj = db.ExecuteScalar("GetPrintFileName_WS", paras, out msg);
            if (!string.IsNullOrWhiteSpace(msg)) throw new Exception(msg);
            if (obj == null) return entity;
            string printFileName = Convert.ToString(obj);
            entity = DepartName(printFileName);
            return entity;
        }
        public PrintCourtRollFileName DepartName(string printFileName)
        {
            PrintCourtRollFileName entity = null;
            string msg;
            if (!string.IsNullOrWhiteSpace(printFileName))
            {
                printFileName = printFileName.Trim();
                if (printFileName.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase))
                    printFileName = printFileName.Remove(printFileName.Length - 4, 4);
                if (printFileName.EndsWith("_", StringComparison.OrdinalIgnoreCase))
                    printFileName = printFileName.Remove(printFileName.Length - 1, 1);
                string[] info = printFileName.Split('_');
                if (info != null && (info.Length == 6 || info.Length == 7))
                {
                    entity = new PrintCourtRollFileName(this.connStr);
                    entity.Prefix = info[0];
                    entity.AutCode = info[1];
                    entity.CourtName = info[2];
                    entity.CourtRoomNo = info[3];
                    entity.CourtDate = info[4];
                    entity.PrintDate = info[5];
                    string[] printDate = info[5].Split('-', ' ', ':');
                    if (printDate != null && printDate.Length == 5)
                        entity.PrintDate = string.Format("{0}-{1}-{2} {3}:{4}:00", printDate[0], printDate[1], printDate[2], printDate[3], printDate[4]);
                    if (info.Length == 7)
                        entity.Suffix = info[6];

                    SqlParameter[] paras = new SqlParameter[2];
                    paras[0] = new SqlParameter("@CrtName", entity.CourtName);
                    paras[1] = new SqlParameter("@CrtRoomNo", entity.CourtRoomNo);
                    DataSet ds = db.ExecuteDataSet("GetCrtIntNoCrtRIntNoByCrtNameCrtRoomNo_WS", paras, out msg);
                    if (!string.IsNullOrWhiteSpace(msg)) throw new Exception(msg);
                    if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0) return null;
                    DataRow dr = ds.Tables[0].Rows[0];
                    entity.CrtIntNo = DBHelper.GetDataRowValue<int>(dr, "CrtIntNo");
                    entity.CrtRIntNo = DBHelper.GetDataRowValue<int>(dr, "CrtRIntNo");
                }
            }
            return entity;
        }

        public string CombineName()
        {
            string[] info = new string[7];
            info[0] = this.Prefix;
            info[1] = this.AutCode;
            info[2] = this.CourtName;
            info[3] = this.CourtRoomNo;
            info[4] = this.CourtDate;
            info[5] = this.PrintDate.Replace(":", "-");
            info[6] = this.Suffix;
            string printFileName = string.Join("_", info);
            if (printFileName.EndsWith("_"))
                printFileName = printFileName.Remove(printFileName.Length - 1, 1);
            printFileName += ".pdf";
            return printFileName;
        }
    }

    public class PrintCourtRollClientService : ServiceDataProcessViaQueue
    {
        public PrintCourtRollClientService()
            : base("", "", new Guid("CF1FB971-CFA3-4B53-98E2-35DB31B1DF76"), ServiceQueueTypeList.PrintCourtRoll)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.connUNC_Export = AARTOBase.GetConnectionString(ServiceConnectionNameList.PrintFilesFolder, ServiceConnectionTypeList.UNC);

            AARTOBase.OnServiceStarting = () =>
            {
                AARTOBase.CheckFolder(this.connUNC_Export);
                this.isChecked = false;
                this.autCode = null;
                if (this.fileList.Count > 0)
                    this.fileList.Clear();
            };
            AARTOBase.OnServiceSleeping = () =>
            {
                if (this.fileList.Count > 0)
                {
                    string title = ResourceHelper.GetResource("CourtRoll");
                    SendEmailManager.SendEmail(
                        this.emailToAdministrator,
                        ResourceHelper.GetResource("EmailSubjectOfPrintService", title),
                        SendEmailManager.GetFileListContent(title, this.fileList));
                }
            };

            process = new PrintFileProcess(this.connAARTODB, AARTOBase.LastUser);
            process.ErrorProcessing = (s) => { this.Logger.Error(s); };
            db = new DBHelper(this.connAARTODB);
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        SIL.AARTOService.Library.AARTORules rules = SIL.AARTOService.Library.AARTORules.GetSingleTon();
        PrintFileProcess process;
        AuthorityDetails authDetails = null;
        string connAARTODB, connUNC_Export;
        string autCode;
        string exportPath;
        int autIntNo, pfnIntNo;
        bool isChecked;
        string emailToAdministrator = string.Empty;
        List<string> fileList = new List<string>();
        DBHelper db;
        int noOfDays = 0;
        string arChargeSheet, arChargeSheetWithPre;

        public override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;
                    SetServiceParameters();
                    if (!ServiceUtility.IsNumeric(body))
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        return;
                    }
                    string autCode = item.Group.Trim();
                    this.pfnIntNo = Convert.ToInt32(body.Trim());
                    rules.InitParameter(this.connAARTODB, autCode);

                    if (this.autCode != autCode)
                    {
                        this.autCode = autCode;
                        QueueItemValidation(ref item);
                    }
                    else if (!this.isChecked)
                    {
                        AARTOBase.ErrorProcessing(ref item);
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                return;
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            for (int i = 0; i < queueList.Count; i++)
            {

                QueueItem item = queueList[i];
                if (!item.IsSuccessful) continue;
                try
                {
                    PrintCourtRollFileName fnEntity = new PrintCourtRollFileName(this.connAARTODB);
                    fnEntity = fnEntity.DepartName(this.pfnIntNo);
                    if (fnEntity == null)
                    {
                        AARTOBase.ErrorProcessing(ref item, "PrintFileName is invalid");
                        break;
                    }

                    this.exportPath = AARTOBase.GetPrintFileDirectory(this.connUNC_Export, this.autCode, PrintServiceType.CourtRoll, fnEntity.CourtName, fnEntity.CourtDate);
                    if (!AARTOBase.CheckFolder(this.exportPath)) return;

                    DateTime crtDate = Convert.ToDateTime(fnEntity.CourtDate);
                    //DateTime prtDate = Convert.ToDateTime(fnEntity.PrintDate);

                    CourtDB courtDB = new CourtDB(this.connAARTODB);
                    DateTime dt = courtDB.CourtDateByCourt(fnEntity.CrtIntNo, crtDate);
                    if (dt.CompareTo(new DateTime(2000, 1, 1)) <= 0)
                    {
                        AARTOBase.ErrorProcessing(ref item, string.Format("Invalid court date for court : {0}", fnEntity.CourtDate));
                        break;
                    }

                    string reportType = "P";
                    dt = dt.AddDays(noOfDays);
                    if (dt.CompareTo(DateTime.Now) <= 0)
                        reportType = "F";

                    string courtRuleValue = rules.Rule(fnEntity.CrtIntNo, "1010").CRString;
                    if (courtRuleValue == "Y")
                    {
                        BuildPrintFile(ref item, fnEntity, CourtRollType.Summary, reportType, "0");   // 0 is All

                        if (reportType == "F")
                            BuildPrintFile(ref item, fnEntity, CourtRollType.Label, null, "0");

                        if (this.arChargeSheet == "Y"
                            && (reportType != "P" || (reportType == "P" && this.arChargeSheetWithPre == "Y")))
                            BuildPrintFile(ref item, fnEntity, CourtRollType.ChargeSheet, reportType, "0");
                    }
                    else
                    {
                        BuildPrintFile(ref item, fnEntity, CourtRollType.Summary, reportType, "1");   // 1 is CAM
                        BuildPrintFile(ref item, fnEntity, CourtRollType.Summary, reportType, "2");  // 2 is HWO

                        if (reportType == "F")
                        {
                            BuildPrintFile(ref item, fnEntity, CourtRollType.Label, null, "1");
                            BuildPrintFile(ref item, fnEntity, CourtRollType.Label, null, "2");
                        }

                        if (this.arChargeSheet == "Y"
                            && (reportType != "P" || (reportType == "P" && this.arChargeSheetWithPre == "Y")))
                        {
                            BuildPrintFile(ref item, fnEntity, CourtRollType.ChargeSheet, reportType, "1");
                            BuildPrintFile(ref item, fnEntity, CourtRollType.ChargeSheet, reportType, "2");
                        }
                    }

                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ServiceHasError"), AARTOBase.LastUser, item.Body, ex.Message), true);
                }
                queueList[i] = item;
            }
        }

        private void QueueItemValidation(ref QueueItem item)
        {
            this.isChecked = false;

            this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim().Equals(this.autCode, StringComparison.OrdinalIgnoreCase));
            if (this.authDetails != null && this.authDetails.AutIntNo > 0)
            {
                this.autIntNo = this.authDetails.AutIntNo;

                this.noOfDays = rules.Rule("CDate", "FinalCourtRoll").DtRNoOfDays;
                this.arChargeSheet = rules.Rule("6205").ARString;
                this.arChargeSheetWithPre = rules.Rule("6208").ARString;

                this.isChecked = true;
            }
            else
            {
                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.autCode), false, QueueItemStatus.Discard);
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.autCode));
                return;
            }
        }

        private void BuildPrintFile(ref QueueItem item, PrintCourtRollFileName fnEntity, CourtRollType crType, string reportType, string courtRollType)
        {
            if (crType == CourtRollType.Summary)
                fnEntity.Prefix = "CR";
            else if (crType == CourtRollType.Label)
                fnEntity.Prefix = "CRLabels";
            else if (crType == CourtRollType.ChargeSheet)
                fnEntity.Prefix = "CRChargeSheet";

            if (courtRollType == "0")
                fnEntity.Suffix = "";
            else if (courtRollType == "1")
                fnEntity.Suffix = "Camera";
            else if (courtRollType == "2")
                fnEntity.Suffix = "HWO";

            //process.ExportPrintFilePath = this.exportPath + "\\" + fnEntity.CombineName();
            process.BuildPrintFile(
                new PrintFileModuleCourtRoll(crType, reportType, fnEntity.CrtIntNo.ToString(), fnEntity.CrtRIntNo.ToString(), fnEntity.CourtDate, fnEntity.PrintDate, courtRollType),
                this.autIntNo, this.pfnIntNo, this.exportPath, fnEntity.CombineName());

            if (process.IsSuccessful)
            {
                this.fileList.Add(process.ExportPrintFileName);
                this.Logger.Info(ResourceHelper.GetResource("PrintFileSavedTo", process.PrintFileName, process.ExportPrintFilePath));
            }
            else
                AARTOBase.ErrorProcessing(ref item);
        }


        private void SetServiceParameters()
        {
            if (ServiceParameters != null
                && string.IsNullOrEmpty(this.emailToAdministrator)
                && ServiceParameters.ContainsKey("EmailToAdministrator")
                && !string.IsNullOrEmpty(ServiceParameters["EmailToAdministrator"]))
                this.emailToAdministrator = ServiceParameters["EmailToAdministrator"];
            if (!SendEmailManager.CheckEmailAddress(this.emailToAdministrator))
                AARTOBase.ErrorProcessing(ResourceHelper.GetResource("EmailAddressError"), true);
        }
    }
}
