﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PrintCourtRoll
{
    partial class PrintCourtRoll : ServiceHost
    {
        public PrintCourtRoll()
            : base("", new Guid("02B2283B-C8BB-4345-BF58-B05FBBEC5C28"), new PrintCourtRollClientService())
        {
            InitializeComponent();
        }
    }
}
