﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PrintCourtRoll
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.PrintCourtRoll"
                ,
                DisplayName = "SIL.AARTOService.PrintCourtRoll"
                ,
                Description = "SIL AARTOService PrintCourtRoll"
            };

            ProgramRun.InitializeService(new PrintCourtRoll(), serviceDescriptor, args);
        }
    }
}
