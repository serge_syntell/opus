﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;

namespace SIL.AARTOService.CreateFrameStatusStatistics
{
    class CreateFrameStatusStatistics_Service:ClientService
    {
        int WindowSize;

        public CreateFrameStatusStatistics_Service()
            : base("", "", new Guid("47439DBA-E78C-4A2B-8244-35F9142E5E69"))
        {
            this._serviceHelper=new AARTOServiceBase(this);
            connStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            serviceDB = new ServiceDB(connStr);

            AARTOBase.OnServiceStarting = () =>
            {
                if (this.ServiceParameters != null && this.ServiceParameters.ContainsKey("WindowSize"))
                {
                    string getWindowSize = this.ServiceParameters["WindowSize"];
                    WindowSize = string.IsNullOrEmpty(getWindowSize) ? 31 : Convert.ToInt32(getWindowSize);
                }
            };
            AARTOBase.OnServiceSleeping = ServiceSleepingLog;
        }

        string connStr;
        ServiceDB serviceDB;
        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)this._serviceHelper; } }

        public override void MainWork(ref List<SIL.QueueLibrary.QueueItem> queueList)
        {
            string errorMsg = string.Empty;
            try
            {
                //
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@WindowSize", WindowSize);

                serviceDB.ExecuteNonQuery("CreateFrameStatusStatistics", parameters);
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "MainWork", ex.ToString());
                AARTOBase.ErrorProcessing(errorMsg);
            }
            finally
            {
                this.MustSleep = true;
            }
        }
        private void ServiceSleepingLog()
        {
            Logger.Info("Sleeping...");
        }
    }
}
