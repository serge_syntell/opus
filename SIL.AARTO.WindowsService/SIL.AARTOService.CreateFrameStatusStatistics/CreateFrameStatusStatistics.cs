﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CreateFrameStatusStatistics
{
    public partial class CreateFrameStatusStatistics : ServiceHost
    {
        public CreateFrameStatusStatistics()
            : base("", new Guid("761778C3-B455-4F46-AF73-309BE10D798C"),new CreateFrameStatusStatistics_Service())
        {
            InitializeComponent();
        }
    }
}
