﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CreateFrameStatusStatistics
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.CreateFrameStatusStatistics",
                Description = "SIL.AARTOService.CreateFrameStatusStatistics",
                DisplayName = "SIL AARTOService CreateFrameStatusStatistics"
            };
            ProgramRun.InitializeService(new CreateFrameStatusStatistics(), serviceDescriptor, args);
        }
    }
}
