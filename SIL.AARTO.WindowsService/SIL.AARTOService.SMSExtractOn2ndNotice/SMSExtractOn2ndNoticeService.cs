﻿
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTOService.DAL;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.QueueLibrary;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using Stalberg.TMS;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SIL.AARTOService.SMSExtractOn2ndNotice
{
    public class SMSExtractOn2ndNoticeService : ServiceDataProcessViaQueue
    {
        public SMSExtractOn2ndNoticeService()
            : base("", "", new Guid("F689903F-AFB6-48D4-9697-11159FEBBBA2"), ServiceQueueTypeList.SMSExtractOn2ndNotice)
        {
            this._serviceHelper =new AARTOServiceBase(this);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.SMSfor2ndNoticeFolder = AARTOBase.GetConnectionString(ServiceConnectionNameList.ExtractOn2ndNoticeForSMSFolder, ServiceConnectionTypeList.UNC);
            serviceDB = new ServiceDB(connAARTODB);
            dt = new DataTable();
            noticeService = new NoticeService();
            AARTOBase.OnServiceStarting = () =>
            {
           
                rollback = false;
                if (string.IsNullOrEmpty(SMSfor2ndNoticeFolder))
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoParam"), "Export to SMSfor2ndNoticeFoler"), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }
                AARTOBase.CheckFolder(SMSfor2ndNoticeFolder);
             
             
            };
        }

        DataTable dt;
        ServiceDB serviceDB;
        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AuthorityDetails authDetails = null;
        AARTORules rules = AARTORules.GetSingleTon();
       
        NoticeService noticeService;

        FileStream fs = null;
        StreamWriter sw = null;
    

        private string connAARTODB = string.Empty;
        private string SMSfor2ndNoticeFolder = string.Empty;
        string autTicketProcessor;
        string currentAutCode, lastAutCode;
        string emailToAdministrator = string.Empty;
        int autIntNo = 0;
        bool rollback = false;
       
        int notIntNo;
        public override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;

            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    if (!int.TryParse(body, out this.notIntNo))
                    {
                        AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSExtractOn2ndNoticeQueueGroupInvalid"),body), LogType.Error, ServiceOption.Continue, item, QueueItemStatus.UnKnown);
                        AARTOBase.DeQueueInvalidItem(ref item);
                        return;
                    }

                 
                    if (!ServiceUtility.IsNumeric(body))
                    {
                        AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSExtractOn2ndNoticeQueueGroupInvalid"), body), LogType.Error, ServiceOption.Continue, item, QueueItemStatus.UnKnown);
                        return;
                    }
                    this.autIntNo = Convert.ToInt32(body.Trim());

                    string groupStr = item.Group.Trim();
                    string[] group = groupStr.Split('|');
                    this.currentAutCode = group[0].Trim();

                    if (this.lastAutCode != this.currentAutCode)
                    {
                        QueueItemValidation(ref item);
                    }
                   

                }
                catch (Exception ex)
                {
                    AARTOBase.LogProcessing(ex.Message, LogType.Error, ServiceOption.Continue, item, QueueItemStatus.UnKnown);
                    return;
                }
                
            } 
            item.IsSuccessful = true;
            AARTOBase.BatchCount--;
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            FileStream fs = null;
            StreamWriter sw = null;
            StringBuilder stringB = new StringBuilder();
            string ts = DateTime.Now.ToString("yyyyMMddHHmmss");
            string tempfileName = string.Format("{0}{1}", ts, ".temp");
            string destFileName = string.Format("SMSExtractOn2ndNotice_{0}{1}", ts, ".csv");
            for (int index = 0; index < queueList.Count; index++)
            {
             
                QueueItem item = queueList[index];

                if (!item.IsSuccessful)
                {
                    continue;
                }
             
                string body = item.Body.ToString();
                int notIntNo = 0;
                if (int.TryParse(body, out notIntNo))
                {
                    Notice notice = noticeService.GetByNotIntNo(notIntNo);
                    if (notice == null)
                    {
                        AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSExtractOn2ndNoticeisNull"), notIntNo), LogType.Info, ServiceOption.Continue, item, QueueItemStatus.Discard);
                        continue;
                    }
                    int status = notice.NoticeStatus;
                    if (255 != status)
                    {
                        item.Status = QueueItemStatus.Discard;
                        AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSExtractOn2ndNoticeInvalidStatus"), notIntNo, status), LogType.Info, ServiceOption.Break, item, QueueItemStatus.Discard);
                        continue;
                    }
                }

                string IDNumber = string.Empty;
                string NotIntNo = string.Empty;
                decimal ChgFineAmount = 0;
                string NotRegNo = string.Empty;
                bool isContinue = false;
                try
                {
                    string errorMsg = string.Empty;

                    SqlParameter spNotIntNo = new SqlParameter("@NotIntNo", DbType.Int32);
                    spNotIntNo.Value = notIntNo;

                    SqlParameter[] paras = new SqlParameter[] { spNotIntNo };
                    int ss = 0;
                    this.serviceDB.ExecuteReader("GetNoticeOfExtractOn2ndNoticeForSMS", paras, dr =>
                    {
                        if (dr.HasRows)
                        {
                            if (dr.Read())
                            {
                                IDNumber = Helper.GetReaderValue<string>(dr, "IDNumber");
                                NotIntNo = Helper.GetReaderValue<string>(dr, "NotTicketNo");
                                ChgFineAmount = Helper.GetReaderValue<decimal>(dr, "FineAmount");
                                NotRegNo = Helper.GetReaderValue<string>(dr, "NotRegNo");
                                ss++;
                            }
                        }
                        else
                        {
                            isContinue = true;
                        }

                    });
                }
                catch (Exception ex)
                {
                    AARTOBase.LogProcessing(ex.Message, LogType.Error, ServiceOption.BreakAndShutdown, item, QueueItemStatus.UnKnown);
                    break;
                }
                if (isContinue)
                {
                    item.Status = QueueItemStatus.Discard;
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSExtractOn2ndNoticeStoreReturnNoData"), notIntNo), LogType.Info, ServiceOption.Continue);
                    continue;
                }

                if (!CheckOffenderIDNumber(IDNumber))
                {
                    item.Status = QueueItemStatus.UnKnown;
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSExtractOn2ndNoticeInvalidIDNumber"), IDNumber), LogType.Info, ServiceOption.Continue);
                    continue;
                }

                try
                {
                    if (index == 0 || fs==null)
                    {
                     
                       
                        fs = new FileStream(SMSfor2ndNoticeFolder + "\\" + tempfileName, FileMode.OpenOrCreate, FileAccess.Write);
                        sw = new StreamWriter(fs);
                        stringB.Clear();
                        stringB.Append("ID number" + ",");
                        stringB.Append("Notice Number" + ",");
                        stringB.Append("Fine amount(All main charges)" + ",");
                        stringB.Append("RegNo");   
                        sw.WriteLine(stringB.ToString());
                     
                    }
                    
                    stringB.Clear();
                    stringB.Append(IDNumber + ",");
                    stringB.Append(NotIntNo + ",");
                    stringB.Append(ChgFineAmount.ToString("F2") + ",");
                    stringB.Append(NotRegNo );

                    sw.WriteLine(stringB.ToString());
                   

                    item.Status = QueueItemStatus.Discard;

                }
                catch (Exception ex)
                {
                    if (sw != null)
                    {
                        sw.Close();
                    }
                    if (fs != null)
                    {
                        fs.Close();
                    }
                    rollback = true;
                    AARTOBase.LogProcessing(ex.Message, LogType.Error, ServiceOption.BreakAndShutdown, item, QueueItemStatus.UnKnown);
                    break;
                }


            }

            if (sw != null)
            {
                sw.Close();
            }
            if (fs != null)
            {
                fs.Close();
            }
            try
            {
                if (File.Exists(SMSfor2ndNoticeFolder + "\\" + tempfileName))
                {
                    File.Move(SMSfor2ndNoticeFolder + "\\" + tempfileName, SMSfor2ndNoticeFolder + "\\" + destFileName);
                }
            }
            catch (Exception ex)
            {
                rollback = true;
                AARTOBase.LogProcessing(ex.Message, LogType.Error, ServiceOption.BreakAndShutdown);
            }
            if (rollback)
            {
                foreach (QueueItem item in queueList)
                {
                    item.Status = QueueItemStatus.UnKnown;
                }
            }
           
        }

        private void QueueItemValidation(ref QueueItem item)
        {

            if (this.lastAutCode != this.currentAutCode)
            {
              
                this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim().Equals(this.currentAutCode, StringComparison.OrdinalIgnoreCase));
                if (this.authDetails != null && this.authDetails.AutIntNo > 0)
                {
                    this.autIntNo = this.authDetails.AutIntNo;
                    this.lastAutCode = this.currentAutCode;
                    this.autTicketProcessor = this.authDetails.AutTicketProcessor.ToLower();
                }
                else
                {
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("InvalidAuthority",this.currentAutCode), LogType.Info, ServiceOption.Break, item, QueueItemStatus.Discard);
                    return;
                }
            }
         
            string body = item.Body.ToString();
            int notIntNo = 0;
            if (int.TryParse(body, out notIntNo))
            {
                Notice notice = noticeService.GetByNotIntNo(notIntNo);
                if (notice==null)
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSExtractOn2ndNoticeisNull"),notIntNo), LogType.Info, ServiceOption.Break, item, QueueItemStatus.Discard);
                    return;
                }
                int status = notice.NoticeStatus;
                if (255 != status)
                {
                    item.Status = QueueItemStatus.Discard;
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SMSExtractOn2ndNoticeInvalidStatus"),notIntNo,status), LogType.Info, ServiceOption.Break, item, QueueItemStatus.Discard);
                    return;
                }
            }


        }



        #region CheckOffenderIDNumber

        public bool CheckOffenderIDNumber(string OffenderIDNumber)
        {
            bool flag = true;
            string idNumber = "";
            if (!string.IsNullOrEmpty(OffenderIDNumber))
            {
                idNumber = OffenderIDNumber.Trim().ToUpper();

                if (idNumber.Length == 13)
                {
                    int d = GetControlDigit(idNumber.Substring(0, 12));
                    if (d == -1)
                    {
                        flag = false;
                    }
                    else
                    {
                        if (idNumber.Substring(12, 1) != d.ToString())
                        {
                            flag = false;
                        }
                    }
                }
                else
                {
                    Regex re = new Regex("^[A-Z0-9]+$");
                    if (!re.IsMatch(idNumber))
                    {
                        flag = false;
                    }
                }

            }

            return flag;

        }

      

        public int GetControlDigit(string parsedIdString)
        {
            int d = -1;
            try
            {
                int a = 0;
                for (int i = 0; i < 6; i++)
                {
                    a += int.Parse(parsedIdString[2 * i].ToString());
                }
                int b = 0;
                for (int i = 0; i < 6; i++)
                {
                    b = b * 10 + int.Parse(parsedIdString[2 * i + 1].ToString());
                }
                b *= 2;
                int c = 0;
                do
                {
                    c += b % 10;
                    b = b / 10;
                } while (b > 0);
                c += a;
                d = 10 - (c % 10);
                if (d == 10)
                    d = 0;
            }
            catch {/*ignore*/}
            return d;
        }

        #endregion
    }
}
