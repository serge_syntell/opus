﻿using SIL.ServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SIL.AARTOService.SMSExtractOn2ndNotice
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.SMSExtractOn2ndNotice"
                ,
                DisplayName = "SIL.AARTOService.SMSExtractOn2ndNotice"
                ,
                Description = "SIL AARTOService SMSExtractOn2ndNotice"
            };

            ProgramRun.InitializeService(new SMSExtractOn2ndNotice(), serviceDescriptor, args);
        }
    }
}
