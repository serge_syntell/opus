﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.DataWashingExtractor
{
    public partial class DataWashingExtractor : ServiceHost
    {
        public DataWashingExtractor()
            : base("", new Guid("4C50507A-8B75-4AD0-ACC8-D651D656EA3D"), new DataWashingExtractorClientService())
        {
            InitializeComponent();
        }
    }
}
