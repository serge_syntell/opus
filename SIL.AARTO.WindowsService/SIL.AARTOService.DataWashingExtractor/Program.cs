﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;
using System.Text.RegularExpressions;
using SIL.AARTO.DAL.Services;
using Stalberg.TMS.Data;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTOService.DataWashingExtractor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.DataWashingExtractor"
                ,
                DisplayName = "SIL.AARTOService.DataWashingExtractor"
                ,
                Description = "SIL.AARTOService.DataWashingExtractor"
            };

            ProgramRun.InitializeService(new DataWashingExtractor(), serviceDescriptor, args);
        }
    }
}
