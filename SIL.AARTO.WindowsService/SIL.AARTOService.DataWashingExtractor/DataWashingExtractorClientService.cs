﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Data.SqlClient;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.AARTOService;
using SIL.AARTOService.Library;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using SIL.AARTOService.Resource;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Specialized;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Data.SqlClient;

namespace SIL.AARTOService.DataWashingExtractor
{
    public class DataWashingExtractorClientService : ServiceDataProcessViaQueue
    {
       


        string artoConnectStr = string.Empty;
        string dataWashingFolder = string.Empty;
        int currentNotIntNo;
        string currentAutCode = string.Empty;
        Dictionary<int, string> dictDataWashIDs = new Dictionary<int, string>();
        AARTORules rules = AARTORules.GetSingleTon();
        AARTOServiceBase aartoBase { get { return (AARTOServiceBase)_serviceHelper; } }

        public DataWashingExtractorClientService()
            : base("", "", new Guid("644352A7-8105-4DAC-9851-95B21F9D0C0D"), ServiceQueueTypeList.DataWashingExtractor)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            this.artoConnectStr = aartoBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.dataWashingFolder = aartoBase.GetConnectionString(ServiceConnectionNameList.DataWashing_Exported, ServiceConnectionTypeList.UNC);
            
            InitializeAartoConnectionStringForNetTier(this.artoConnectStr);

            aartoBase.ReceiveToEnd = true; 
            aartoBase.OnServiceStarting = () =>
            {
                aartoBase.CheckFolder(this.dataWashingFolder);
            };
        }

        //Jacob added 20130731 start, temporary batch size control.
        private int ReceivedCount = 0;
        private int InnerBatchSize = Convert.ToInt32(ConfigurationManager.AppSettings["DataWashBatchSize"]);

        //Jacob added 20130731 end
        public override void InitialWork(ref QueueItem item)
        {
          
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && ServiceUtility.IsNumeric(body) && !string.IsNullOrEmpty(item.Group))
            //Jacob edited 20130731 start
            // support batch size
            // item.IsSuccessful = true;
            {
                item.IsSuccessful = true;
                ReceivedCount += 1;
               

                if (ReceivedCount >= InnerBatchSize)
                {
                    this._serviceHelper.SkipReceivingData = true; //for temporary batch size control, 
                    //long term wiil use formal batch size parameter.
                    ReceivedCount = 0; // clear count for next time running
                }
            }
            //Jacob edited 20130731 end
            else
            {
                aartoBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                return;
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            Mi5PersonaService personaServ = new Mi5PersonaService();
            int qlistIdex, qlistCount; //Jacob
            qlistCount = queueList.Count;//Jacob
            qlistIdex = 0;//Jacob
            foreach (QueueItem item in queueList)
            {
                qlistIdex += 1;//Jacob
                if (item.IsSuccessful == false) continue;
                try
                {
                    this.currentNotIntNo = Convert.ToInt32(item.Body);

                    //Jacob each 100 items, insert one log record.
                    if ( qlistIdex%100 == 0)
                         this.Logger.Info(string.Format("Queue Key: {0} |  Inner loop   {1}/{2}, at {3}",
                              currentNotIntNo, qlistIdex, qlistCount, DateTime.Now.ToString("yyyyMMddHHmmss")));
                       
                    if (currentAutCode != item.Group.Trim())
                    {
                        if (dictDataWashIDs != null && dictDataWashIDs.Count > 0)
                        {
                            if (!SaveExtractFile()) return;
                            if (!UpdatePersonaStatus(ref queueList)) return;
                            dictDataWashIDs.Clear();
                        }
                    }
                    this.currentAutCode = item.Group.Trim();

                    rules.InitParameter(this.artoConnectStr, this.currentAutCode);

                    // Check if data washing is active
                    if (rules.Rule("9060").ARString != "Y")
                    {
                        item.Status = QueueItemStatus.Discard;
                        this.Logger.Info("Queue Key: " + currentNotIntNo + " | " + ResourceHelper.GetResource("DatawashNotActived", aartoBase.currentMethod));
                        continue;
                    }
                   

                    NoticeService noticeServ = new NoticeService();
                    Notice not = noticeServ.GetByNotIntNo(currentNotIntNo);
                    string notSendTo = null;
                    if (not != null) notSendTo = not.NotSendTo;
                    if (!String.IsNullOrEmpty(notSendTo)) notSendTo = notSendTo.Trim();

                    string IDNum = null;
                    switch (notSendTo)
                    {
                        case "D":
                            DriverService drvSrv = new DriverService();
                            Driver drv = drvSrv.GetByNotIntNo(currentNotIntNo).FirstOrDefault();
                            if (drv != null) IDNum = drv.DrvIdNumber;
                            break;
                        case "O":
                            OwnerService onrSrv = new OwnerService();
                            Owner owner = onrSrv.GetByNotIntNo(currentNotIntNo).FirstOrDefault();
                            if (owner != null) IDNum = owner.OwnIdNumber;
                            break;
                        case "P":
                            ProxyService pxySrv = new ProxyService();
                            Proxy proxy = pxySrv.GetByNotIntNo(currentNotIntNo).FirstOrDefault();
                            if (proxy != null) IDNum = proxy.PrxIdNumber;
                            break;
                        default:
                            break;
                    }
                     
                    if (!String.IsNullOrEmpty(IDNum)) IDNum = IDNum.Trim();
                    // Validate RSA ID
                    if (IDNum == null || string.IsNullOrEmpty(IDNum) || !ValidateIdNumber(IDNum))
                    {
                        item.Status = QueueItemStatus.Discard;
                        this.Logger.Info("Queue Key: " + currentNotIntNo + " | " + ResourceHelper.GetResource("InvalidIDNum", aartoBase.currentMethod));
                        continue;
                    }

                    // Check duplicate ID
                    if (dictDataWashIDs.ContainsValue(IDNum))
                    {
                        item.Status = QueueItemStatus.Discard;
                        this.Logger.Info("Queue Key: " + currentNotIntNo + " | " + ResourceHelper.GetResource("DatawashDuplicateIDNum", aartoBase.currentMethod));
                        continue;
                    }

                    // Check whether exclude proxies
                    if (rules.Rule("9100").ARString == "Y" && notSendTo == "P")
                    {
                        item.Status = QueueItemStatus.Discard;
                        this.Logger.Info("Queue Key: " + currentNotIntNo + " | " + ResourceHelper.GetResource("DatawashExcludeProxies", aartoBase.currentMethod));
                        continue;
                    }
                   
                    Mi5Persona persona = personaServ.GetByMipidNumber(IDNum);
                    if (persona == null)
                    {
                        persona = new Mi5Persona();
                        persona.MipidNumber = IDNum;
                        persona.MipDateLastUpdated = DateTime.Now;
                        persona.LastUser = aartoBase.LastUser;
                        persona = personaServ.Save(persona);
                    }

                    DateTime? sentToWashDate = persona.MipSentToDataWasherDate;
                    DateTime? lastWashDate = persona.MipLastWashedDate;
                    int DWRecycle = rules.Rule("9080").ARNumeric;
                   

                    if (sentToWashDate.HasValue && !lastWashDate.HasValue
                        && Convert.ToDateTime(sentToWashDate).AddMonths(DWRecycle) >= DateTime.Now)
                    {
                        item.Status = QueueItemStatus.Discard;
                        this.Logger.Info("Queue Key: " + currentNotIntNo + " | " + ResourceHelper.GetResource("InDatawashRecycleDate", aartoBase.currentMethod));
                        continue;
                    }
                    sentToWashDate = sentToWashDate.HasValue ? sentToWashDate : DateTime.Parse("1900-01-01");
                    if (lastWashDate.HasValue)
                    {
                        DateTime nextWashDate = Convert.ToDateTime(lastWashDate).AddMonths(DWRecycle);
                        // Check whether the data wash was previously requested and no response received, 
                        // or if need next data wash
                        if (sentToWashDate > lastWashDate || nextWashDate >= DateTime.Now)
                        {
                            item.Status = QueueItemStatus.Discard;
                            this.Logger.Info("Queue Key: " + currentNotIntNo + " | " + ResourceHelper.GetResource("InDatawashRecycleDate", aartoBase.currentMethod));
                            continue;
                        }
                    }

                    // Get the date of data washing by add number of days before printing
                    DateTime WashDate = Convert.ToDateTime(sentToWashDate).AddDays(rules.Rule("9070").ARNumeric);
                    if (WashDate >= DateTime.Now)
                    {
                        item.Status = QueueItemStatus.Discard;
                        this.Logger.Info("Queue Key: " + currentNotIntNo + " | " + ResourceHelper.GetResource("DatawashNotInDaysBeforePrinting", aartoBase.currentMethod));
                        continue;
                    }

                    // Check last paid status by add paid status valid months
                    DateTime? validPaidDate = persona.MipLastPaidDate;
                    validPaidDate = validPaidDate.HasValue ? validPaidDate : DateTime.Parse("1900-01-01");
                    if (Convert.ToDateTime(validPaidDate).AddMonths(rules.Rule("9090").ARNumeric) >= DateTime.Now)
                    {
                        item.Status = QueueItemStatus.Discard;
                        this.Logger.Info("Queue Key: " + currentNotIntNo + " | " + ResourceHelper.GetResource("DatawashNotInValidMonthOfPayment", aartoBase.currentMethod));
                        continue;
                    }
                  

                    dictDataWashIDs.Add(currentNotIntNo, IDNum);
                }
                catch (Exception ex)
                {
                    item.Status = QueueItemStatus.UnKnown;
                    this.Logger.Error(string.Format("{0} : {1}", aartoBase.currentMethod, ex.Message));
                    //StopService();
                    // Oscar 2013-04-10 changed
                    aartoBase.StopService();
                }
            }
            //Jacob
            this.Logger.Info(string.Format("End loop, {0},{1} ",
                               dictDataWashIDs.Count, DateTime.Now.ToString("yyyyMMddHHmmss")));
            if (dictDataWashIDs != null && dictDataWashIDs.Count > 0)
            {
                 //Jacob
                this.Logger.Info(string.Format("SaveExtractFile,{0} ",
                                    DateTime.Now.ToString("yyyyMMddHHmmss")));
                if (!SaveExtractFile()) return;
                if (!UpdatePersonaStatus(ref queueList)) return;
                 //Jacob
                this.Logger.Info(string.Format("after UpdatePersonaStatus,{0} ",
                              DateTime.Now.ToString("yyyyMMddHHmmss")));
                dictDataWashIDs.Clear();
            }

        }

        private bool SaveExtractFile()
        {
            string fileName = null;
            string fileFolder = null;
            StreamWriter sw = null;
            FileStream fs = null;
            try
            {
                fileFolder = dataWashingFolder + "\\" + currentAutCode;
                if (!Directory.Exists(fileFolder))
                {
                    Directory.CreateDirectory(fileFolder);
                }

                StringBuilder sbIDs = new StringBuilder(); 

                //Jacob edited 20130731 start
                // batch size setting causes multiple files.
                //fileName = "datawashout" + currentAutCode + DateTime.Now.ToString("yyyyMMdd") + ".csv";
                fileName = "datawashout" + currentAutCode + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                //Jacob edited 20130731 end

                fs = new FileStream(fileFolder + "\\" + fileName, FileMode.Create, FileAccess.Write);
                sw = new StreamWriter(fs, System.Text.Encoding.Default);

                foreach (var pid in dictDataWashIDs.Values)
                {
                    sbIDs.Append(pid + "\n");
                }

                sw.WriteLine(sbIDs);
                //sw.Close();
                //fs.Close();
            }
            catch (Exception ex)
            {
                DeleteExtractFile(fileFolder + "\\" + fileName);
                this.Logger.Error(ResourceHelper.GetResource("ErrorOnWritingDataWashExtractFile", aartoBase.currentMethod, fileName, ex.Message));
                //StopService();
                // Oscar 2013-04-10 changed
                aartoBase.StopService();
                return false;
            }
            finally
            {
                if (sw != null)
                    sw.Close();
                if (fs != null)
                    fs.Close();
            }
            return true;
        }

        private bool DeleteExtractFile(string fullPath)
        {
            if (File.Exists(fullPath))
            {
                try
                {
                    File.Delete(fullPath);
                }
                catch (Exception ex)
                {
                    this.Logger.Error(string.Format("{0} : {1}", aartoBase.currentMethod, ex.Message));
                    //StopService();
                    // Oscar 2013-04-10 changed
                    aartoBase.StopService();
                    return false;
                }
            }
            return true;
        }

        private bool UpdatePersonaStatus(ref List<QueueItem> queueList)
        {
            try
            {
                Mi5Persona personToDW = null;
                Mi5PersonaService personaServ = new Mi5PersonaService();
                foreach (var dItem in dictDataWashIDs)
                {
                    string strId = dItem.Value;
                    personToDW = personaServ.GetByMipidNumber(strId);
                    if (personToDW == null)
                    {
                        personToDW = new Mi5Persona();
                        personToDW.MipidNumber = strId;
                        personToDW.MipDateLastUpdated = DateTime.Now;
                        personToDW = personaServ.Save(personToDW);
                    }
                    personToDW.MipSentToDataWasherDate = DateTime.Now;
                    personToDW.LastUser = aartoBase.LastUser;
                    personToDW = personaServ.Save(personToDW);
                    QueueItem que = queueList.Find(delegate(QueueItem q) { return Convert.ToInt32(q.Body) == dItem.Key; });
                    if (que != null) que.Status = QueueItemStatus.Discard;
                }
            }
            catch (Exception ex)
            {
                this.Logger.Error(ResourceHelper.GetResource("ErrorOnUpdateDataWashExtractStatus", aartoBase.currentMethod, ex.Message));
                //StopService();
                // Oscar 2013-04-10 changed
                aartoBase.StopService();
                return false;
            }
            return true;
        }

        // Validate RSA ID 
        public static bool ValidateIdNumber(string idNum)
        {
            //return true;
            bool hasError = false;
            if (idNum.Length == 13)
            {
                int d = GetControlDigit(idNum.Substring(0, 12));
                if (d == -1)
                {
                    hasError = true;
                }
                else
                {
                    if (idNum.Substring(12, 1) != d.ToString())
                    {
                        hasError = true;
                    }
                }
            }
            else
            {
                hasError = true;
            }
            return !hasError;
        }

        private static int GetControlDigit(string parsedIdString)
        {
            int d = -1;
            try
            {
                int a = 0;
                for (int i = 0; i < 6; i++)
                {
                    a += int.Parse(parsedIdString[2 * i].ToString());
                }
                int b = 0;
                for (int i = 0; i < 6; i++)
                {
                    b = b * 10 + int.Parse(parsedIdString[2 * i + 1].ToString());
                }
                b *= 2;
                int c = 0;
                do
                {
                    c += b % 10;
                    b = b / 10;
                } while (b > 0);
                c += a;
                d = 10 - (c % 10);
                if (d == 10)
                    d = 0;
            }
            catch {/*ignore*/}
            return d;
        }

        //void StopService()
        //{
        //    this.MustStop = true;
        //    ((SIL.ServiceLibrary.ServiceHost)this.ServiceHost).Stop();
        //}

        void InitializeAartoConnectionStringForNetTier(string dynamicConnectionStr)
        {
            SqlNetTiersProvider provider = new SqlNetTiersProvider();
            NameValueCollection collection = new NameValueCollection();
            collection.Add("UseStoredProcedure", "false");
            collection.Add("EnableEntityTracking", "true");
            collection.Add("EntityCreationalFactoryType", "SIL.AARTO.DAL.Entities.EntityFactory");
            collection.Add("EnableMethodAuthorization", "false");
            collection.Add("ConnectionString", dynamicConnectionStr);
            collection.Add("ConnectionStringName", "SIL.AARTO.DAL.Data.ConnectionString");
            collection.Add("ProviderInvariantName", "System.Data.SqlClient");

            provider.Initialize("SqlNetTiersProvider", collection);

            DataRepository.LoadProvider(provider, true);

            //DataRepository.AddConnection("SIL.AARTO.DAL.Data.ConnectionString", dynamicConnectionStr);

        }
    }
}
