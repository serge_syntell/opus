﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CancelExpiredViolations
{
    partial class CancelExpiredViolations : ServiceHost
    {
        public CancelExpiredViolations()
            : base("", new Guid("396269B5-9FE1-454F-BF3F-9996A750EDC8"), new CancelExpiredViolationsService())
        {
            InitializeComponent();
        }
    }
}
