﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using System.Configuration;
using SIL.QueueLibrary;
using SIL.AARTOService.Library;
using System.Data;
using System.Diagnostics;
using Stalberg.TMS;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Resource;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTOService.CancelExpiredViolations
{
    public class CancelExpiredViolationsService : ServiceDataProcessViaQueue
    {
        public CancelExpiredViolationsService()
            : base("", "", new Guid("F5561304-09E5-4B5A-B4E3-670ED724EE43"), ServiceQueueTypeList.CancelExpiredViolations_Frame)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
        }

        string connectStr;
        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        const string ValidFrameStatus = "10";
        NoticeFrame noticeFrameEntiry;
        NoticeFrameService nfService = new NoticeFrameService();
        Notice noticeEntiry;
        NoticeService noticeService = new NoticeService();
        DateRulesDetails dateRule_OffenceDate_1stNoticePostedDate = new DateRulesDetails();
        DefaultDateRules DefaultDateRule_OffenceDate_1stNoticePostedDate;
        public override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;

                    #region Discard the queue without expiring if print file name ends with _PostCanPost

                    //2014-11-28 heidi added (5314)
                    int frameIntNo = 0;
                    int.TryParse(body, out frameIntNo);
                    noticeFrameEntiry = nfService.GetByFrameIntNo(frameIntNo).FirstOrDefault();
                    if (noticeFrameEntiry != null)
                    {
                        noticeEntiry = noticeService.GetByNotIntNo(noticeFrameEntiry.NotIntNo);
                        if (noticeEntiry != null)
                        {
                            if (noticeEntiry.NotCiprusPrintReqName.Contains("_PostCanPost"))
                            {
                                item.Status = QueueItemStatus.Discard;
                                item.IsSuccessful = false;
                            }
                        }
                    }


                    #endregion

                    //dls 2012-04-03 - don't understand what this is supposed to be doing - we do not want anything we need to discard anything with frame status < 900

                    //item.Status = QueueItemStatus.Discard;

                    //int frameIntNo;
                    //if (!int.TryParse(body, out frameIntNo))
                    //{
                    //    AARTOBase.ErrorProcessing(ref item);
                    //    return;
                    //}

                    //FrameDB frameDB = new FrameDB(connectStr);
                    //FrameDetails frame = frameDB.GetFrameDetails(frameIntNo);

                    //if (frame.FrameStatus.Trim() != ValidFrameStatus)
                    //{
                    //    AARTOBase.ErrorProcessing(ref item);
                    //    return;
                    //}
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
            else
            {
                // dls 2012-04-03
                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.LastUser, item.Body), true);
            }
        }


        string strAutCode = null;
        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            string msg = string.Empty;
            //2013-01-17 add by Henry for Video Capture.
            string[] queueGroup = null;
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                if (!item.IsSuccessful)
                    continue;

                try
                {
                    item.Status = QueueItemStatus.Discard;
                    int frameIntNo = Convert.ToInt32(item.Body);
                    queueGroup = item.Group.Split('|');

                    if (strAutCode != queueGroup[0])
                    {
                        strAutCode = queueGroup[0].Trim();
                        //jerry 2012-04-05 check group value
                        if (string.IsNullOrWhiteSpace(strAutCode))
                        {
                            AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
                            return;
                        }

                        AARTORules.GetSingleTon().InitParameter(connectStr, strAutCode);
                    }

                    int success = UpdateExpiredFrames(frameIntNo, queueGroup.Length > 1 ? queueGroup[1].Trim() : null, ref msg);

                    if (!string.IsNullOrEmpty(msg))
                    {
                        AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("CancelExpiredViolationsFrameFailed", frameIntNo, msg), false, QueueItemStatus.UnKnown);
                    }
                    else if (success < 0)
                    {
                        switch (success)
                        {
                            case -1:
                                this.Logger.Error(string.Format(ResourceHelper.GetResource("UpdateFrameStatusFailed"), frameIntNo));
                                break;
                            case -2:
                                this.Logger.Error(string.Format(ResourceHelper.GetResource("UpdateNoticeStatusFailed"), frameIntNo));
                                break;
                            case -3:
                                this.Logger.Error(string.Format(ResourceHelper.GetResource("UpdateChargeStatusFailed"), frameIntNo));
                                break;
                            case -4:
                                this.Logger.Error(string.Format(ResourceHelper.GetResource("UpdateNaTISFileFailed"), frameIntNo));
                                break;
                        }
                    }
                }
                catch (AuthorityNullException ex)
                {
                    item.Status = QueueItemStatus.UnKnown;
                    AARTOBase.ErrorProcessing(ex.Message);
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        private const int STATUS_EXPIRED = 921;  //this is used to expire an notice that has not been printed

        //2013-01-17 add filmType by Henry for Video Capture.
        private int UpdateExpiredFrames(int frameIntNo, string filmType, ref string errMsg)
        {
            string connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            string LastUser = AARTOBase.LastUser;
            DBHelper db = new DBHelper(connectStr);

            SqlParameter[] paras = new SqlParameter[7];
            paras[0] = new SqlParameter("@frameIntNo", frameIntNo);
            paras[1] = new SqlParameter("@LastUser", LastUser);
            paras[2] = new SqlParameter("@Success", 0);
            paras[3] = new SqlParameter("@AdjAt100", rules.Rule("0600").ARString);
            paras[4] = new SqlParameter("@NoOfDays", rules.Rule("NotOffenceDate", "NotIssue1stNoticeDate").DtRNoOfDays);

            RejectionDB rejReason = new RejectionDB(connectStr);
            string expiredFrame = "Expired. The frame offence date is too old to continue";
            int rejIntNoExpired = rejReason.UpdateRejection(0, expiredFrame, "N", LastUser, 0, "Y");
            paras[5] = new SqlParameter("@RejIntNoExpired", rejIntNoExpired);
            paras[6] = new SqlParameter("@StatusExpired", STATUS_EXPIRED);

            paras[2].Direction = ParameterDirection.Output;

            string sp = filmType == "H" ? "FrameUpdateExpiredVideoCapture_WS" : "FrameUpdateExpired_WS";
            db.ExecuteNonQuery(sp, paras, out errMsg);

            int success = Convert.ToInt32(paras[2].Value);
            return success;
        }
    }
}

