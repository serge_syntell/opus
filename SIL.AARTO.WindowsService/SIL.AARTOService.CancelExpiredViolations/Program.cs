﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CancelExpiredViolations
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.CancelExpiredViolations"
                ,
                DisplayName = "SIL.AARTOService.CancelExpiredViolations"
                ,
                Description = "SIL AARTOService CancelExpiredViolations"
            };

            ProgramRun.InitializeService(new CancelExpiredViolations(), serviceDescriptor, args);
        }
    }
}
