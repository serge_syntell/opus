﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using SIL.ServiceLibrary;
using SIL.QueueLibrary;
using SIL.AARTO.DAL.Entities;
using SIL.AARTOService;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.BLL.Report;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.DAL.Services;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.ServiceBase;
using System.Data;
using System.Collections;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;


namespace SIL.AARTOService.PrintToFile
{
    public class PrintToFileClientService : ServiceDataProcessViaQueue
    {
        public static string artoConnectionString = string.Empty;
        string ExportedFilesFolder = string.Empty;
        int pfnIntNo;
        string ReportCode;
        string currentAutCode, lastAutCode;
        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        FreeStylePrintEngineForService CurrentEngine = new FreeStylePrintEngineForService();
        public ReportModel rptModel;
        List<string> fileList = new List<string>();
        private readonly FreeStylePrintEngineForServiceCpa5 flpEngineServiceCPA5; //new FreeStylePrintEngineForCpa5DirectPrinting();
        private readonly FreeStylePrintEngineForServiceCpa6 flpEngineServiceCPA6; //new FreeStylePrintEngineForCpa6Directprinting();
        private readonly FreeStylePrintEngineForService2ndNotice flpEngineService2ndNotice;//2013-12-25 Heidi added for 2nd Notice Direct Printing (5101)
        private readonly FreeStylePrintEngineForServiceWOA flpEngineServiceWOA; //2013-12-25 Heidi added for WOA Direct Printing (5101)
        private FreeStylePrintEngineForPrintSummaryWOAToFileService flpEngineServiceWOARegister;//Jacob
        private SIL.AARTO.DAL.Entities.TList<ReportConfig> repConfigList = new AARTO.DAL.Entities.TList<ReportConfig>();
        public PrintToFileClientService()
            : base("", "", new Guid("98363F85-BB8C-41B8-A079-7A2FF693A864"), ServiceQueueTypeList.PrintToFile)
        {
            this._serviceHelper = new AARTOServiceBase(this,true);//2014-09-09 Heidi open transaction.(bontq1488)
            artoConnectionString = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);//Config.ConnectionString = artoConnectionString;
            ExportedFilesFolder = AARTOBase.GetConnectionString(ServiceConnectionNameList.ExportedFilesFolder, ServiceConnectionTypeList.UNC);
            AARTOBase.OnServiceStarting = OnServiceStarting;
            GlobalVariates<User>.CurrentUser = new User()
            {
                UserLoginName = AARTOBase.LastUser
            };
            try
            {
                flpEngineServiceCPA5 = new FreeStylePrintEngineForServiceCpa5(artoConnectionString);
                flpEngineServiceCPA6 = new FreeStylePrintEngineForServiceCpa6(artoConnectionString);
                //2013-12-25 Heidi added for 2nd Notice Direct Printing (5101)
                flpEngineService2ndNotice = new FreeStylePrintEngineForService2ndNotice(artoConnectionString);
                //2013-12-25 Heidi added for WOA Direct Printing (5101)
                flpEngineServiceWOA = new FreeStylePrintEngineForServiceWOA(artoConnectionString);
                flpEngineServiceWOARegister = new FreeStylePrintEngineForPrintSummaryWOAToFileService(artoConnectionString);//Jacob
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex.Message, LogType.Error, ServiceOption.BreakAndStop);
            }
        }

        public override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;

                    if (!ServiceUtility.IsNumeric(body))
                    {
                        //AARTOBase.LogProcessing(item,LogType.Error,ServiceOption.BreakAndStop);
                        // 2014-02-17, Oscar changed log
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("QueueItemKeyShouldBeNumeric", body), LogType.Error, ServiceOption.Break, item);
                        return;
                    }

                    this.pfnIntNo = Convert.ToInt32(body.Trim());
                    string[] group = item.Group.Trim().Split('|');
                    if (group == null || group.Length != 2)
                    {
                        //AARTOBase.LogProcessing(ResourceHelper.GetResource("QueueGroupValidationFailed"), LogType.Error, ServiceOption.BreakAndStop);
                        // 2014-02-17, Oscar changed log
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("QueueGroupValidationFailed"), LogType.Error, ServiceOption.Break, item);
                        return;
                    }
                    this.currentAutCode = group[0].Trim();
                    this.ReportCode = group[1].Trim();


                }
                catch (Exception ex)
                {
                    //AARTOBase.LogProcessing(ex, LogType.Error, ServiceOption.BreakAndStop);
                    // 2014-02-17, Oscar changed log
                    AARTOBase.LogProcessing(ex, LogType.Error, ServiceOption.Break, item);
                }
            }
            else
            {
                //AARTOBase.LogProcessing(ResourceHelper.GetResource("InvalidAuthority"),LogType.Error,ServiceOption.BreakAndStop);
                // 2014-02-17, Oscar changed log
                AARTOBase.LogProcessing(ResourceHelper.GetResource("InvalidAuthority"), LogType.Error, ServiceOption.Break, item);
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            if (queueList.Count() > 0)
                AARTOBase.LogProcessing(ResourceHelper.GetResource("StartSpoolingFreeReports"), LogType.Info, ServiceOption.Continue);

            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                string strExportedfilefolder = string.Empty;
                try
                {
                    if (item.IsSuccessful)
                    {
                        //ReportConfig rep = ReportManager.GetAllReportConfig().FirstOrDefault();
                        ReportConfig rep = null;
                        if (this.ReportCode == ((int)ReportConfigCodeList.CPA5DirectPrinting).ToString())
                        {
                            //rep = ReportManager.GetAllReportConfig().FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.CPA5DirectPrinting).ToString());
                            rep = repConfigList.FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.CPA5DirectPrinting).ToString());
                            if (rep != null)
                            {
                                flpEngineServiceCPA5.CreateEngine(rep.RcIntNo);
                                CurrentEngine = flpEngineServiceCPA5;
                                if (string.IsNullOrEmpty(strExportedfilefolder))
                                    strExportedfilefolder = ExportedFilesFolder + "\\CPA5";
                                CurrentEngine.ReportFilesFolder = strExportedfilefolder;
                            }
                            else
                            {
                                //AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("LoadReprotCodeError"), ReportConfigCodeList.CPA5DirectPrinting.ToString(), (int)ReportConfigCodeList.CPA5DirectPrinting), LogType.Error, ServiceOption.Stop);
                                // 2014-02-17, Oscar changed log
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("LoadReprotCodeError"), ReportConfigCodeList.CPA5DirectPrinting.ToString(), (int)ReportConfigCodeList.CPA5DirectPrinting), LogType.Error, ServiceOption.Break, item);
                            }
                        }
                        if (this.ReportCode == ((int)ReportConfigCodeList.CPA6DirectPrinting).ToString())
                        {
                            //rep = ReportManager.GetAllReportConfig().FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.CPA6DirectPrinting).ToString());
                            rep = repConfigList.FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.CPA6DirectPrinting).ToString());
                            if (rep != null)
                            {
                                flpEngineServiceCPA6.CreateEngine(rep.RcIntNo);
                                CurrentEngine = flpEngineServiceCPA6;
                                if (string.IsNullOrEmpty(strExportedfilefolder))
                                    strExportedfilefolder = ExportedFilesFolder + "\\CPA6";
                                CurrentEngine.ReportFilesFolder = strExportedfilefolder;
                            }
                            else
                            {
                                //AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("LoadReprotCodeError"), ReportConfigCodeList.CPA6DirectPrinting.ToString(), (int)ReportConfigCodeList.CPA6DirectPrinting), LogType.Error, ServiceOption.Stop);
                                // 2014-02-17, Oscar changed log
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("LoadReprotCodeError"), ReportConfigCodeList.CPA6DirectPrinting.ToString(), (int)ReportConfigCodeList.CPA6DirectPrinting), LogType.Error, ServiceOption.Break, item);
                            }
                        }
                        //2013-12-25 Heidi added for 2nd Notice Direct Printing (5101)
                        if (this.ReportCode == ((int)ReportConfigCodeList.SecondNoticeDirectPrinting).ToString())
                        {
                            //rep = ReportManager.GetAllReportConfig().FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.SecondNoticeDirectPrinting).ToString());
                            rep = repConfigList.FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.SecondNoticeDirectPrinting).ToString());
                            if (rep != null)
                            {
                                flpEngineService2ndNotice.CreateEngine(rep.RcIntNo);
                                CurrentEngine = flpEngineService2ndNotice;
                                if (string.IsNullOrEmpty(strExportedfilefolder))
                                    strExportedfilefolder = ExportedFilesFolder + "\\Second Notice";
                                CurrentEngine.ReportFilesFolder = strExportedfilefolder;
                            }
                            else
                            {
                                //AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("LoadReprotCodeError"), ReportConfigCodeList.SecondNoticeDirectPrinting.ToString(), (int)ReportConfigCodeList.SecondNoticeDirectPrinting), LogType.Error, ServiceOption.Stop);
                                // 2014-02-17, Oscar changed log
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("LoadReprotCodeError"), ReportConfigCodeList.SecondNoticeDirectPrinting.ToString(), (int)ReportConfigCodeList.SecondNoticeDirectPrinting), LogType.Error, ServiceOption.Break, item);
                            }
                        }
                        //2013-12-25 Heidi added for WOA Direct Printing (5101)
                        if (this.ReportCode == ((int)ReportConfigCodeList.WOADirectPrinting).ToString())
                        {
                            //rep = ReportManager.GetAllReportConfig().FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.WOADirectPrinting).ToString());
                            rep = repConfigList.FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.WOADirectPrinting).ToString());
                            if (rep != null)
                            {
                                flpEngineServiceWOA.CreateEngine(rep.RcIntNo);
                                CurrentEngine = flpEngineServiceWOA;
                                if (string.IsNullOrEmpty(strExportedfilefolder))
                                    strExportedfilefolder = ExportedFilesFolder + "\\WOA";
                                CurrentEngine.ReportFilesFolder = strExportedfilefolder;
                            }
                            else
                            {
                                //AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("LoadReprotCodeError"), ReportConfigCodeList.WOADirectPrinting.ToString(), (int)ReportConfigCodeList.WOADirectPrinting), LogType.Error, ServiceOption.Stop);
                                // 2014-02-17, Oscar changed log
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("LoadReprotCodeError"), ReportConfigCodeList.WOADirectPrinting.ToString(), (int)ReportConfigCodeList.WOADirectPrinting), LogType.Error, ServiceOption.Break, item);
                            }
                        }
                        //Jacob added for WOA Register Direct Printing (5275)
                        if (this.ReportCode == ((int)ReportConfigCodeList.WOARegisterDirectPrinting).ToString())
                        {
                            //rep = ReportManager.GetAllReportConfig().FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.WOARegisterDirectPrinting).ToString());
                            rep = repConfigList.FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.WOARegisterDirectPrinting).ToString());
                            if (rep != null)
                            {
                                flpEngineServiceWOARegister.CreateEngine(rep.RcIntNo);
                                CurrentEngine = flpEngineServiceWOARegister;
                                if (string.IsNullOrEmpty(strExportedfilefolder))
                                    strExportedfilefolder = ExportedFilesFolder + "\\WOA Register";
                                CurrentEngine.ReportFilesFolder = strExportedfilefolder;
                            }
                            else
                            {
                                //AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("LoadReprotCodeError"), ReportConfigCodeList.WOADirectPrinting.ToString(), (int)ReportConfigCodeList.WOADirectPrinting), LogType.Error, ServiceOption.Stop);
                                // 2014-02-17, Oscar changed log
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("LoadReprotCodeError"), ReportConfigCodeList.WOADirectPrinting.ToString(), (int)ReportConfigCodeList.WOADirectPrinting), LogType.Error, ServiceOption.Break, item);
                            }
                        }

                        if (rep.IsFreeStyle && rep.AutomaticGenerate)//free report
                        {
                            //AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("StartSpoolingReport"), rep.ReportCode, rep.ReportName), LogType.Info, ServiceOption.Continue);
                            // 2014-02-17, Oscar changed log
                            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("StartSpoolingReport"), rep.ReportCode, rep.ReportName), LogType.Info, ServiceOption.Continue, item, item.Status);

                            CurrentEngine.CreateEngine(rep.RcIntNo);
                            rptModel = CurrentEngine.PrintModel;
                            //rptModel.PageSize = pageSize;
                            //rptModel.PageIndex = 1;
                            rptModel.AutIntNo = rep.AutIntNo.ToString();
                            CurrentEngine.CurrentReportDataService.InitializeReportConfig(rep.RcIntNo);
                            //set last user
                            CurrentEngine.CurrentReportDataService.LastUser = AARTOBase.LastUser;

                            PrintFileName pfn = new PrintFileNameService().GetByPfnIntNo(this.pfnIntNo);
                            string strPrintFileName = "";
                            //2014-09-02 Heidi changed for fixing the issue that Print file name data cannot be found.The Service log Prompt is not accurate.(bontq1471)
                            if (pfn != null && pfn.PrintFileName!=null)
                            {
                                strPrintFileName = pfn.PrintFileName.Trim();
                            }
                            if (string.IsNullOrEmpty(strPrintFileName))
                            {
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("PrintFileNameByQueueKey"), this.pfnIntNo, rep.ReportCode), LogType.Info, ServiceOption.Continue, item, QueueItemStatus.Discard);
                                continue;
                            }

                            if (!string.IsNullOrEmpty(strPrintFileName))
                            {
                                fileList.Clear();//2014-03-05 Heidi added for fixed writing the same notice data multiple times in different files
                                fileList.Add(strPrintFileName);
                                //Get print details
                                DataTable dtDetails = CurrentEngine.CurrentReportDataService.LoadReportDataByPrintFiles(fileList, rep.AutIntNo);

                                if (this.ReportCode == ((int)ReportConfigCodeList.WOADirectPrinting).ToString())
                                {
                                    new QueueItemProcessor().Send(new QueueItem()
                                    {
                                        Body = this.pfnIntNo.ToString(),
                                        Group = this.currentAutCode.Trim() + "|" + ((int)ReportConfigCodeList.WOARegisterDirectPrinting).ToString(),
                                        QueueType = ServiceQueueTypeList.PrintToFile
                                    });
                                }
                                //Modified date&status, save history
                                CurrentEngine.CurrentReportDataService.SaveHistory_ModifyPrintDateAndStatus(dtDetails, true, true);
                                //Get printing string with data and style
                                CurrentEngine.PrintFileName = strPrintFileName;
                                CurrentEngine.PrintReportWithData(dtDetails);


                                ////Jake 2014-06-11 added ,  print woa register after print woa
                                //if (this.ReportCode == ((int)ReportConfigCodeList.WOADirectPrinting).ToString())
                                //{
                                //    rep = ReportManager.GetAllReportConfig().FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.WOARegisterDirectPrinting).ToString());
                                //    if (rep != null)
                                //    {
                                //        flpEngineServiceWOARegister.CreateEngine(rep.RcIntNo);
                                //        CurrentEngine = flpEngineServiceWOARegister;

                                //        CurrentEngine.CurrentReportDataService.InitializeReportConfig(rep.RcIntNo);
                                //        CurrentEngine.CurrentReportDataService.LastUser = AARTOBase.LastUser;
                                //        strExportedfilefolder = ExportedFilesFolder + "\\WOA Register";
                                //        CurrentEngine.ReportFilesFolder = strExportedfilefolder;

                                //        //Contiune to print woa register report
                                //        if (fileList != null && fileList.Count > 0)
                                //        {
                                //            dtDetails = CurrentEngine.CurrentReportDataService.LoadReportDataByPrintFiles(fileList, rep.AutIntNo);
                                //            //Modified date&status, save history
                                //            CurrentEngine.CurrentReportDataService.SaveHistory_ModifyPrintDateAndStatus(dtDetails, true, true);
                                //            //Get printing string with data and style
                                //            CurrentEngine.PrintFileName = strPrintFileName;
                                //            CurrentEngine.PrintReportWithData(dtDetails);

                                //            // 2014-02-14, Oscar added creating file message.
                                //            //AARTOBase.LogProcessing(ResourceHelper.GetResource("FileHasBeenCreatedSuccessfully", CurrentEngine.ExportedFile), LogType.Info, ServiceOption.Continue, item, item.Status);

                                //        }
                                //    }
                                //    else
                                //    {
                                //        //AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("LoadReprotCodeError"), ReportConfigCodeList.WOADirectPrinting.ToString(), (int)ReportConfigCodeList.WOADirectPrinting), LogType.Error, ServiceOption.Stop);
                                //        // 2014-02-17, Oscar changed log
                                //        AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("LoadReprotCodeError"), ReportConfigCodeList.WOARegisterDirectPrinting.ToString(), (int)ReportConfigCodeList.WOARegisterDirectPrinting), LogType.Error, ServiceOption.Break, item);
                                //    }
                                //}

                                // 2014-02-14, Oscar added creating file message.
                                AARTOBase.LogProcessing(ResourceHelper.GetResource("FileHasBeenCreatedSuccessfully", CurrentEngine.ExportedFile), LogType.Info, ServiceOption.Continue, item, item.Status);
                            }
                            //AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SpoolingFreeReportCompleted"), rep.ReportCode, rep.ReportName), LogType.Info, ServiceOption.Continue);
                            // 2014-02-17, Oscar changed log
                            AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("SpoolingFreeReportCompleted"), rep.ReportCode, rep.ReportName), LogType.Info, ServiceOption.Continue, item, item.Status);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //AARTOBase.LogProcessing(ResourceHelper.GetResource("ReportEngineService_Error",                   BuildMessageForException(ex), this.ReportCode), LogType.Error, ServiceOption.BreakAndStop);
                    // 2014-02-17, Oscar changed log
                    AARTOBase.LogProcessing(ex, LogType.Error, ServiceOption.Break, item);
                }

                // 2014-02-14, Oscar removed redundant sleeping message
                //AARTOBase.LogProcessing(ResourceHelper.GetResource("SpoolingFreeReportIsGoingToSleep"),LogType.Info, ServiceOption.Continue);
                queueList[i] = item;
                //MustSleep = true; //removed by Jacob 20140212 move out of the loop.
            }

        }



        private void OnServiceStarting()
        {
            // 2014-02-17, Oscar added folder checking.
            AARTOBase.CheckFolder(ExportedFilesFolder);

            #region check folder permissions
            //2014-09-09 Heidi added for checking Writing file permissions.(bontq1488)
            try
            {

                string exportFullPath = ExportedFilesFolder + "\\" + "test.txt";

                //write the exported files
                using (FileStream fs = new FileStream(exportFullPath, FileMode.Create, FileAccess.Write))
                {
                    using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.ASCII))
                    {
                        sw.Write("test"); 
                    }
                }
                if (File.Exists(exportFullPath))
                {
                    File.Delete(exportFullPath);
                }
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("NoPermissionToTheFolder", ExportedFilesFolder), LogType.Error, ServiceOption.BreakAndStop);
            }

            #endregion

            if (this.fileList.Count > 0)
                this.fileList.Clear();
            //Jake 2014-08-28 added to get all report config when service starting
            try
            {
                repConfigList = ReportManager.GetAllReportConfig();
            }
            catch (Exception ex) {
                AARTOBase.LogProcessing(ex.ToString(), LogType.Error, ServiceOption.Stop);
            }
        }



        /// <summary>
        /// Builds the message for an exception. 2012.11.07 Nick add
        /// </summary>
        /// <param name="ex">The exception.</param>
        /// <returns>A string containing the full exception message.</returns>
        protected string BuildMessageForException(Exception ex)
        {
            var sb = new StringBuilder();
            sb.Append("Type: ")
                .AppendLine(ex.GetType().FullName)
                .Append("Message: ")
                .AppendLine(ex.Message)
                .Append("Thrown by Method: ")
                .AppendFormat("{0}.{1}", ex.Source.GetType().FullName, ex.TargetSite.Name)
                .AppendLine()
                .Append("Stack Trace: ")
                .AppendLine(ex.StackTrace);

            if (ex.InnerException != null)
            {
                sb.AppendLine("Inner Exception: ")
                    .AppendLine(this.BuildMessageForException(ex.InnerException));
            }

            return sb.ToString();
        }
    }
}
