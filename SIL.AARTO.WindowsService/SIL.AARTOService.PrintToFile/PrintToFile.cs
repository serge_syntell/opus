﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PrintToFile
{
    public partial class PrintToFile : ServiceHost
    {
        public PrintToFile()
            : base("", new Guid("4BFCEE59-D733-4018-95DA-6ADD8930964A"), new PrintToFileClientService())
        {
            InitializeComponent();
        }
    }
}
