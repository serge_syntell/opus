﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.GenerateNoticeOfWOA
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.GenerateNoticeOfWOA"
                ,
                DisplayName = "SIL.AARTOService.GenerateNoticeOfWOA"
                ,
                Description = "SIL AARTOService GenerateNoticeOfWOA"
            };

            ProgramRun.InitializeService(new GenerateNoticeOfWOA(), serviceDescriptor, args);
        }
    }
}
