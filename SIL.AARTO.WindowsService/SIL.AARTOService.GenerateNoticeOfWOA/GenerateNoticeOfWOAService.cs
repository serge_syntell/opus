﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.AARTOService.DAL;
using SIL.QueueLibrary;
using SIL.AARTOService.Library;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Collections;
using Stalberg.TMS;
using SIL.AARTOService.Resource;

namespace SIL.AARTOService.GenerateNoticeOfWOA
{
    public class GenerateNoticeOfWOAService : ServiceDataProcessViaQueue
    {
        public GenerateNoticeOfWOAService()
            : base("", "", new Guid("4EF0D11E-27F9-4A0A-B2E5-A43B9D2F2017"), ServiceQueueTypeList.GenerateNoticeOfWOA)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.AARTOBase.OnServiceSleeping = OnServiceSleeping;

            // Oscar 2013-04-09 added
            AARTOBase.OnServiceStarting = () =>
            {
                this.lastStamp = DateTime.Now;
            };
        }

        int sumIntNo;
        string connectStr;
        string strAutCode = null;
        string strCourtNo = null;
        string strCourtDate = null;
        string emailToAuth = string.Empty;
        string batchFilename;
        private const string TICKET_PROCESSOR_TMS = "TMS";
        private const string TICKET_PROCESSOR_AARTO = "AARTO";
        private const string TICKET_PROCESSOR_JMPD_AARTO = "JMPD_AARTO";
        const string DATE_FORMAT = "yyyy-MM-dd";

        // Oscar 2013-04-09 added
        const string FileNameDateFormat = "yyyy-MM-dd_HH-mm-ss";
        DateTime stamp, lastStamp;

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();

        // jerry 2012-03-13 change, in order to push Batch queue
        List<Int32> pfnIntNoList = new List<Int32>();
        int delayActionDate_InHours;

        public override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            string strGroup = null;
            string[] strArrGroup = null;
            string autTicketProcessor = null;

            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && ServiceUtility.IsNumeric(body) && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    // Nick 20120411 add the autcode in queue group
                    sumIntNo = Convert.ToInt32(item.Body);
                    strGroup = item.Group.Trim();
                    strArrGroup = strGroup.Split('~');
                    if (strArrGroup.Length != 2 || string.IsNullOrEmpty(strArrGroup[0]) || string.IsNullOrEmpty(strArrGroup[1]))
                    {
                        AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("CreatingNotOfWOA") + ": " + ResourceHelper.GetResource("QueueGroupValidationFailed"));
                        return;
                    }
                    strAutCode = strArrGroup[0].Trim();

                    //Jerry 2012-12-21 change
                    SIL.AARTO.DAL.Entities.NoticeSummons noticeSummonsEntity = new SIL.AARTO.DAL.Services.NoticeSummonsService().GetBySumIntNo(sumIntNo).FirstOrDefault();
                    SIL.AARTO.DAL.Entities.Notice noticeEntity = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(noticeSummonsEntity.NotIntNo);
                    //autTicketProcessor = (AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode).AutTicketProcessor + "").Trim();
                    //if (autTicketProcessor != TICKET_PROCESSOR_AARTO
                    //&& autTicketProcessor != TICKET_PROCESSOR_TMS
                    //&& autTicketProcessor != TICKET_PROCESSOR_JMPD_AARTO)
                    if (noticeEntity != null  && noticeEntity.NotTicketProcessor != TICKET_PROCESSOR_TMS)
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        return;
                    }

                    rules.InitParameter(connectStr, strAutCode);
                    int daysLoadNotOfWOA = rules.Rule("5800").ARNumeric;
                    NoticeOfWoaDB noticeOfWOA = new NoticeOfWoaDB(connectStr);
                    Int32 noOfSummons = noticeOfWOA.CheckSummonForNoticeOfWOA(sumIntNo, daysLoadNotOfWOA);
                    //Jerry 2012-04-25 change
                    //if (noOfSummons <= 0)
                    //{
                    //    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("CreatingNotOfWOA") + ": " + ResourceHelper.GetResource("SumNotRightForNotOfWOA"));
                    //    return;
                    //}
                    if (noOfSummons == -1)
                    {
                        Logger.Info("Queue Key: " + body + " | " + ResourceHelper.GetResource("CreatingNotOfWOA") + ": " + ResourceHelper.GetResource("SumStatNotRightForNotOfWOA"));
                        item.IsSuccessful = false;
                        item.Status = QueueItemStatus.Discard;
                        return;
                    }
                    else if (noOfSummons == -2)
                    {
                        Logger.Info("Queue Key: " + body + " | " + ResourceHelper.GetResource("CreatingNotOfWOA") + ": " + ResourceHelper.GetResource("SumAlreadyHasNotOfWOA"));
                        item.IsSuccessful = false;
                        item.Status = QueueItemStatus.Discard;
                        return;
                    }
                    else if (noOfSummons == -3)
                    {
                        Logger.Info("Queue Key: " + body + " | " + ResourceHelper.GetResource("CreatingNotOfWOA") + ": " + ResourceHelper.GetResource("SumAutNotRightForNotOfWOA"));
                        item.IsSuccessful = false;
                        item.Status = QueueItemStatus.Discard;
                        return;
                    }
                    else if (noOfSummons == 0)
                    {
                        Logger.Info("Queue Key: " + body + " | " + ResourceHelper.GetResource("CreatingNotOfWOA") + ": " + ResourceHelper.GetResource("SumNotRightForNotOfWOA"));
                        item.IsSuccessful = false;
                        item.Status = QueueItemStatus.Discard;
                        return;
                    }
                    item.IsSuccessful = true;
                    AARTOBase.BatchCount--;
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
            }
        }

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            string strGroup = null;
            string[] strArrGroup = null;
            string errMsg = "";

            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                try
                {
                    if (item.IsSuccessful)
                    {
                        sumIntNo = Convert.ToInt32(item.Body);
                        SummonsDB sum = new SummonsDB(connectStr);
                        strCourtDate = sum.GetCourtDateBySumIntNo(sumIntNo).ToString(DATE_FORMAT);

                        if (strGroup != item.Group.Trim())
                        {
                            strGroup = item.Group.Trim();
                            strArrGroup = strGroup.Split('~');
                            strAutCode = strArrGroup[0].Trim();
                            strCourtNo = strArrGroup[1].Trim();
                            // jerry 2012-03-02 change the name to add AutCode
                            //string pdfReport = "NoticeOfWOA_" + strCourtNo;
                            string pdfReport = "NoticeOfWOA_" + strAutCode.Trim() + "_" + strCourtNo;
                            batchFilename = ReturnTimeStampedReportString(pdfReport);
                        }

                        GetServiceParameters();

                        int daysLoadNotOfWOA = rules.Rule("5800").ARNumeric;
                        NoticeOfWoaDB noticeOfWOA = new NoticeOfWoaDB(connectStr);

                        int returnWOA = noticeOfWOA.UpdateSummonsForNoticeOfWOA(sumIntNo, daysLoadNotOfWOA, batchFilename, AARTOBase.LastUser);

                        if (returnWOA <= 0)
                        {
                            if (returnWOA < 0)
                            {
                                errMsg = "FailedToCreateNotOfWOA";
                            }
                            else if (returnWOA == 0)
                            {
                                errMsg = "NoSumForWOAToPrinting";
                            }

                            AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("CreatingNotOfWOA") + ": " + ResourceHelper.GetResource(errMsg));
                            continue;
                        }

                        if (!emailToAuth.Equals(string.Empty))
                        {
                            string message = string.Format(ResourceHelper.GetResource("ReadyToPrintNotOfWOA"), DateTime.Now.ToString(), batchFilename);

                            //send email
                            try
                            {
                                SendEmailManager.SendEmail(emailToAuth, "Notices of Warrant Of Arrest", message, null);
                            }
                            catch (Exception ex)
                            {
                                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("CreatingNotOfWOA") + ": " + ResourceHelper.GetResource("UnableToSendEmail", ex.StackTrace), true);
                            }
                        }
                        else
                        {
                            Logger.Warning("Queue Key: " + sumIntNo + " | " + ResourceHelper.GetResource("CreatingNotOfWOA") + ": " + ResourceHelper.GetResource("AutEmailWarrantIsNull"));
                        }

                        item.Status = QueueItemStatus.Discard;

                        // Push generate WOA queue
                        QueueItemProcessor queProcessor = new QueueItemProcessor();
                        int daysLoadWOA = rules.Rule("SumCourtDate", "WOALoadDate").DtRNoOfDays;
                        int daysToAct = daysLoadWOA > daysLoadNotOfWOA ? daysLoadWOA - daysLoadNotOfWOA : 0;
                        queProcessor.Send(
                            new QueueItem()
                            {
                                Body = sumIntNo,
                                Group = strAutCode + "~" + strCourtNo + "~" + strCourtDate,
                                ActDate = DateTime.Now.AddDays(daysToAct),
                                QueueType = ServiceQueueTypeList.GenerateWOA
                            }
                        );

                        // jerry 2012-03-02 push PrintNoticeOfWOA 
                        AuthorityDetails authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode.Trim());
                        int pfnIntNo = AARTOBase.SavePrintFileName(PrintFileNameType.Summons, batchFilename, authDetails.AutIntNo, sumIntNo, out errMsg);
                        if (pfnIntNo <= 0)
                        {
                            AARTOBase.ErrorProcessing(errMsg, true);
                        }
                        else
                        {
                            QueueItem queueItem = new QueueItem();
                            queueItem.Body = pfnIntNo.ToString();
                            queueItem.Group = strAutCode.Trim();
                            queueItem.ActDate = DateTime.Now.AddHours(this.delayActionDate_InHours);
                            queueItem.QueueType = ServiceQueueTypeList.PrintNoticeOfWOA;
                            if (!this.pfnIntNoList.Contains(pfnIntNo))
                            {
                                this.pfnIntNoList.Add(pfnIntNo);
                                queProcessor.Send(queueItem);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        private void OnServiceSleeping()
        {
            if (this.pfnIntNoList.Count > 0)
            {
                this.pfnIntNoList.Clear();
            }
        }

        private string ReturnTimeStampedReportString(string pdfReport)
        {
            // 2011.12.30 nick changed dateString format
            //string dateString = DateTime.Now.ToString(DATE_FORMAT);
            //pdfReport = pdfReport + "_" + dateString;
            //return pdfReport;

            // Oscar 2013-04-09 changed
            this.stamp = DateTime.Now;
            if (this.stamp < this.lastStamp.AddSeconds(1))
                this.stamp = this.lastStamp.AddSeconds(1);
            this.lastStamp = this.stamp;
            return string.Format("{0}_{1}", pdfReport, this.stamp.ToString(FileNameDateFormat));
        }

        private void GetServiceParameters()
        {
            if (ServiceParameters != null)
            {
                if (string.IsNullOrEmpty(this.emailToAuth)
&& ServiceParameters.ContainsKey("AutEmailWarrant") && !string.IsNullOrEmpty(ServiceParameters["AutEmailWarrant"]))
                    this.emailToAuth = ServiceParameters["AutEmailWarrant"];

                //jerry 2012-03-13 add
                if (this.delayActionDate_InHours == 0 && ServiceParameters.ContainsKey("DelayActionDate_InHours") && !string.IsNullOrEmpty(ServiceParameters["DelayActionDate_InHours"]))
                {
                    this.delayActionDate_InHours = Convert.ToInt32(ServiceParameters["DelayActionDate_InHours"]);
                }
            }
        }

        //void StopService()
        //{
        //    this.MustStop = true;
        //    ((SIL.ServiceLibrary.ServiceHost)this.ServiceHost).Stop();
        //}
    }
}
