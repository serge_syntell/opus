﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.GenerateNoticeOfWOA
{
    partial class GenerateNoticeOfWOA : ServiceHost
    {
        public GenerateNoticeOfWOA()
            : base("", new Guid("83C59950-DB59-4435-8F0F-3ED07023D52F"), new GenerateNoticeOfWOAService())
        {
            InitializeComponent();
        }
    }
}
