﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Print2ndNotice
{
    partial class Print2ndNotice : ServiceHost
    {
        public Print2ndNotice()
            : base("", new Guid("7742FE6E-1F82-4319-AF49-0029ED1BBC7A"), new Print2ndNoticeClientService())
        {
            InitializeComponent();
        }
    }
}
