﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Print2ndNotice
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.Print2ndNotice"
                ,
                DisplayName = "SIL.AARTOService.Print2ndNotice"
                ,
                Description = "SIL AARTOService Print2ndNotice"
            };

            ProgramRun.InitializeService(new Print2ndNotice(), serviceDescriptor, args);
        }
    }
}
