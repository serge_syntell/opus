﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.AARTO.BLL.Utility.PrintFile;
using SIL.AARTOService.DAL;
using SIL.QueueLibrary;
using System.Data.SqlClient;
using Stalberg.TMS;
using SIL.AARTOService.Resource;
using SIL.AARTO.BLL.PostalManagement;

namespace SIL.AARTOService.Print2ndNotice
{
    public class Print2ndNoticeClientService : ServiceDataProcessViaQueue
    {
        public Print2ndNoticeClientService()
            : base("", "", new Guid("7CC2D8AA-5486-47FE-8D0C-55960B23272C"), ServiceQueueTypeList.Print2ndNotice)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.connUNC_Export = AARTOBase.GetConnectionString(ServiceConnectionNameList.PrintFilesFolder, ServiceConnectionTypeList.UNC);

            AARTOBase.OnServiceStarting = () =>
            {
                AARTOBase.CheckFolder(this.connUNC_Export);
                this.isChecked = false;
                this.autCode = null;
                if (this.fileList.Count > 0)
                    this.fileList.Clear();
            };
            AARTOBase.OnServiceSleeping = () =>
            {
                if (this.fileList.Count > 0)
                {
                    string title = ResourceHelper.GetResource("2ndNotice");
                    SendEmailManager.SendEmail(
                        this.emailToAdministrator,
                        ResourceHelper.GetResource("EmailSubjectOfPrintService", title),
                        SendEmailManager.GetFileListContent(title, this.fileList));
                }
            };

            process = new PrintFileProcess(this.connAARTODB, AARTOBase.LastUser);
            process.ErrorProcessing = (s) => { this.Logger.Error(s); };
            db = new DBHelper(this.connAARTODB);

            // 2014-12-02, Oscar added (bontq 1726, ref 1725)
            noticeDB = new NoticeDB(this.connAARTODB);
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        string connAARTODB, connUNC_Export;
        string autCode;
        int pfnIntNo, autIntNo;
        string printFileName, exportPath;
        bool isChecked;
        List<string> fileList = new List<string>();
        PrintFileProcess process;
        DBHelper db;
        AuthorityDetails authDetails = null;

        // 2014-12-02, Oscar added (bontq 1726, ref 1725)
        readonly NoticeDB noticeDB;


        public override void InitialWork(ref QueueItem item)
        {
            string msg;
            try
            {
                string body = string.Empty;
                if (item.Body != null)
                    body = item.Body.ToString();
                if (!string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(item.Group))
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;
                    SetServiceParameters();
                    if (!ServiceUtility.IsNumeric(body))
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        return;
                    }
                    this.pfnIntNo = Convert.ToInt32(body.Trim());

                    string groupStr = item.Group.Trim();
                    string[] group = groupStr.Split('|');
                    string autCode = group[0].Trim();

                    if (this.autCode != autCode)
                    {
                        this.autCode = autCode;
                        QueueItemValidation(ref item);
                    }

                    if (this.isChecked)
                    {
                        this.printFileName = AARTOBase.GetPrintFileName(this.pfnIntNo, out msg);
                        if (string.IsNullOrWhiteSpace(this.printFileName) || !string.IsNullOrWhiteSpace(msg))
                        {
                            AARTOBase.ErrorProcessing(ref item, msg);
                            return;
                        }

                        //2013-05-21 added by Nancy start
                        if (AARTOBase.GetPrintFileCount(this.pfnIntNo,this.printFileName, "N", out msg) <= 0)
                        {
                            AARTOBase.LogProcessing(msg, LogType.Warning, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);//2013-05-23 added by Nancy
                            //AARTOBase.ErrorProcessing(ref item, msg, false, QueueItemStatus.Discard); 2013-05-23 removed by Nancy
                            return;
                        }
                        //2013-05-21 added by Nancy end
                    }
                }
                else
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                    return;
                }
            }
            catch (Exception ex)
            {
                AARTOBase.ErrorProcessing(ex);
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            bool regDirectory = false, rnoDirectory = false, secondDirectory = false;
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                try
                {
                    if (item.IsSuccessful)
                    {
                        string section = this.printFileName.Substring(0, 11);
                        if (section.IndexOf("REG", StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            this.exportPath = AARTOBase.GetPrintFileDirectory(this.connUNC_Export, this.autCode, PrintServiceType.ChangeRegNo);
                            if (!regDirectory)
                            {
                                if (AARTOBase.CheckFolder(this.exportPath))
                                    regDirectory = true;
                                else
                                    return;
                            }
                            process.BuildPrintFile(new PrintFileModuleChangeRegNo(), this.autIntNo, this.printFileName, this.exportPath);
                        }
                        else
                        {
                            if (section.IndexOf("RNO", StringComparison.OrdinalIgnoreCase) >= 0)
                            {
                                this.exportPath = AARTOBase.GetPrintFileDirectory(this.connUNC_Export, this.autCode, PrintServiceType.NewOffender);
                                if (!rnoDirectory)
                                {
                                    if (AARTOBase.CheckFolder(this.exportPath))
                                        rnoDirectory = true;
                                    else
                                        return;
                                }
                                if (section.IndexOf("1st", StringComparison.OrdinalIgnoreCase) >= 0)
                                    process.BuildPrintFile(new PrintFileModuleFirstNotice(), this.autIntNo, this.printFileName, this.exportPath);
                                else
                                    process.BuildPrintFile(new PrintFileModule2ndNotice(), this.autIntNo, this.printFileName, this.exportPath);
                            }
                            else
                            {
                                this.exportPath = AARTOBase.GetPrintFileDirectory(this.connUNC_Export, this.autCode, PrintServiceType.SecondNotice);
                                if (!secondDirectory)
                                {
                                    if (AARTOBase.CheckFolder(this.exportPath))
                                        secondDirectory = true;
                                    else
                                        return;
                                }
                                process.BuildPrintFile(new PrintFileModule2ndNotice(), this.autIntNo, this.printFileName, this.exportPath);
                            }
                        }

                        if (process.IsSuccessful)
                        {
                            this.fileList.Add(process.ExportPrintFileName);

                            // Oscar 20120510 add update status
                            int length = process.PrintFileName.Length;
                            if (length > 11) length = 11;
                            string prefix = process.PrintFileName.Substring(0, length);
                            if (prefix.IndexOf("REG", StringComparison.OrdinalIgnoreCase) < 0
                                && prefix.IndexOf("RNO", StringComparison.OrdinalIgnoreCase) < 0
                            )
                            {
                                if (!UpdateStatus(ref item, process.PrintFileName))
                                    return; // 2014-12-02, Oscar changed (bontq 1726, ref 1725)
                            }
                            else
                            {
                                // Heidi 2014-10-15 added for New Offender printing process(bontq1505)
                                //NoticeDB noticeDB = new NoticeDB(this.connAARTODB);
                                //noticeDB.SetNewOffenderPrintedDate(process.PrintFileName, AARTOBase.LastUser, this.autIntNo);
                                // 2014-12-02, Oscar changed (bontq 1726, ref 1725)
                                if (!SetNewOffenderPrintedDate(item, process.PrintFileName, AARTOBase.LastUser, this.autIntNo))
                                    return;
                            }

                            this.Logger.Info(ResourceHelper.GetResource("PrintFileSavedTo", process.PrintFileName, process.ExportPrintFilePath));
                        }
                        else
                        {
                            AARTOBase.ErrorProcessing(ref item);
                            return;
                        }

                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
                queueList[i] = item;
            }
        }


        private void QueueItemValidation(ref QueueItem item)
        {
            this.isChecked = false;
            this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim().Equals(this.autCode, StringComparison.OrdinalIgnoreCase));
            if (this.authDetails != null && this.authDetails.AutIntNo > 0)
            {
                this.autIntNo = this.authDetails.AutIntNo;
                this.isChecked = true;
            }
            else
            {
                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.autCode), false, QueueItemStatus.Discard);
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.autCode));
                return;
            }
        }

        private bool UpdateStatus(ref QueueItem item, string printFileName)
        {
            string errMessage = string.Empty;
            int status2ndLoaded = 260;
            int status2ndPrinted = 270;
            NoticeDB noticeDB = new NoticeDB(this.connAARTODB);

            int rows = noticeDB.UpdateNoticeChargeStatus(this.autIntNo, printFileName, status2ndLoaded, status2ndPrinted, "SecondNotice", AARTOBase.LastUser, ref errMessage);

            if (rows < 1)
            {
                errMessage += errMessage.Length > 0 ? " - Error: " + errMessage : string.Empty;
                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("UnableToSetPrintStatus"));
                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("UnableToSetPrintStatus"), false, rows == 0 ? QueueItemStatus.Discard : QueueItemStatus.UnKnown);
                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("UnableToSetPrintStatus"), false, QueueItemStatus.Discard);
                // 2014-12-02, Oscar changed (bontq 1726, ref 1725)
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("UnableToSetPrintStatus"));
                return false;
            }
            else
            {
                AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                arDetails.AutIntNo = autIntNo;
                arDetails.ARCode = "4200";
                arDetails.LastUser = AARTOBase.LastUser;
                DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connAARTODB);
                bool isPostDate = ar.SetDefaultAuthRule().Value.Equals("Y");

                if (isPostDate)
                {
                    PostDateNotice postDate = new PostDateNotice(this.connAARTODB);
                    rows = postDate.SetNoticePostedDate(printFileName, autIntNo, DateTime.Now.AddDays(arDetails.ARNumeric), AARTOBase.LastUser, ref errMessage);

                    if (rows < 1)
                    {
                        //AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("UnableToSetPostDate"), printFileName, errMessage), false, QueueItemStatus.Discard);
                        // 2014-12-02, Oscar changed (bontq 1726, ref 1725)
                        AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("UnableToSetPostDate"), printFileName, errMessage));
                        return false;
                    }
                }
            }

            return true;
        }

        string emailToAdministrator = string.Empty;
        private void SetServiceParameters()
        {
            if (ServiceParameters != null
                && string.IsNullOrEmpty(this.emailToAdministrator)
                && ServiceParameters.ContainsKey("EmailToAdministrator")
                && !string.IsNullOrEmpty(ServiceParameters["EmailToAdministrator"]))
                this.emailToAdministrator = ServiceParameters["EmailToAdministrator"];
            if (!SendEmailManager.CheckEmailAddress(this.emailToAdministrator))
                AARTOBase.ErrorProcessing(ResourceHelper.GetResource("EmailAddressError"), true);
        }

        // 2014-12-02, Oscar changed (bontq 1726, ref 1725)
        bool SetNewOffenderPrintedDate(QueueItem item, string printFileName, string lastUser, int autIntNo)
        {
            try
            {
                noticeDB.SetNewOffenderPrintedDate(printFileName, lastUser, autIntNo);
                return true;
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("SettingNewOffenderPrintedDateFailed", ex.ToString()), LogType.Error, ServiceOption.Break, item);
            }

            return false;
        }
    }
}
