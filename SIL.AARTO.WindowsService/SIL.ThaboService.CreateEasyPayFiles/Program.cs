﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using SIL.ServiceLibrary;

namespace SIL.ThaboService.CreateEasyPayFiles
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.ThaboService.CreateEasyPayFiles"
                ,
                DisplayName = "SIL.ThaboService.CreateEasyPayFiles"
                ,
                Description = "SIL.ThaboService.CreateEasyPayFiles"
            };

            ProgramRun.InitializeService(new CreateEasyPayFiles(), serviceDescriptor, args);
        }
    }
}
