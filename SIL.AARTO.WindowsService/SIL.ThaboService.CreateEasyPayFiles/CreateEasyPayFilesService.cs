﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using SIL.AARTOService.DAL;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.QueueLibrary;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using Stalberg.ThaboAggregator.Objects;
using Stalberg.TMS;
using Stalberg.TMS_TPExInt.Objects;

namespace SIL.ThaboService.CreateEasyPayFiles
{
    public class CreateEasyPayFilesService : ClientService
    {
        public CreateEasyPayFilesService()
            : base("", "", new Guid("FE83577B-3310-42F8-BD45-14E5F6F7CEB4"))
        {
            this._serviceHelper = new AARTOServiceBase(this);
            thaboConnectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.Thabo, ServiceConnectionTypeList.DB);
            easyPayExportPath = AARTOBase.GetConnectionString(ServiceConnectionNameList.EasyPayExportPath, ServiceConnectionTypeList.UNC);
            ServiceUtility.InitializeNetTier(thaboConnectStr);

            AARTOBase.OnServiceStarting = () =>
            {
                if (string.IsNullOrEmpty(easyPayExportPath))
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoParam"), "EasyPayExportPath"), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }

                var exportDir = new DirectoryInfo(easyPayExportPath);
                if (!exportDir.Exists)
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoEasyPayExportPath"), exportDir.FullName), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }
            };

        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        string thaboConnectStr = string.Empty;
        string easyPayExportPath = string.Empty;

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            AARTOBase.LogProcessing(ResourceHelper.GetResource("ProcessEasyPay", AARTOBase.LastUser), LogType.Info);
            ProcessEasyPayData();
            AARTOBase.LogProcessing(ResourceHelper.GetResource("ProcessedEasyPay", AARTOBase.LastUser), LogType.Info);

            //Jerry 2014-05-23 add
            this.MustSleep = true;
        }

        private void ProcessEasyPayData()
        {
            var authorities = new List<EasyPayAuthority>();

            try
            {
                //Logger.Write(string.Format("\tProcessing EasyPay Data ({0})...", exportPath), "General", (int)TraceEventType.Information);
                AARTOBase.LogProcessing(ResourceHelper.GetResource("ProcessingEasyPay", easyPayExportPath), LogType.Info);

                // Create aggregated EasyPay files
                GetEasyPayDataToSend(authorities);

                //Logger.Write(string.Format("Got {0} EasyPay Authorities with data to send.", authorities.Count), "General", (int)TraceEventType.Information);
                AARTOBase.LogProcessing(ResourceHelper.GetResource("GetAuthToSend", authorities.Count), LogType.Info);
                DateTime dt = DateTime.Now;
                foreach (EasyPayAuthority authority in authorities)
                {
                    //jerry 2012-1-16 add transaction
                    using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(10)))
                    {
                        if (authority.EasyPayTransactions.Count == 0)
                        {
                            //Logger.Write(string.Format("No transactions for {0}.", authority), "General", (int)TraceEventType.Information);
                            AARTOBase.LogProcessing(ResourceHelper.GetResource("NoTrans", authority), LogType.Info);
                            continue;
                        }

                        TrafficDataFile tdf = new TrafficDataFile(authority);
                        tdf.CleanExportData();

                        string fileName = Path.Combine(easyPayExportPath, string.Format("TF{0:0000}{1}{2}",
                            authority.ReceiverIdentifier, DateTime.Now.ToString(TrafficDataFile.DATE_FORMAT), TrafficDataFile.EXTENSION));
                        FileInfo file = new FileInfo(fileName);
                        if (!file.Directory.Exists)
                            file.Directory.Create();

                        // FBJ (2009-05-26): Added a try/catch to catch the throwing of a new exception in the CreateFile Method when the file already exists for the day
                        //Logger.Write(string.Format("Creating EasyPay file {0}", fileName), "General", (int)TraceEventType.Information);
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("CreateEasyPayFile", fileName), LogType.Info);
                        try
                        {
                            FileInfo fi = new FileInfo(fileName);
                            if (fi.Exists && fi.Length > 0)
                            {
                                //Logger.Write("The file: " + fileName + " already exists and has data in it, it cannot be overwritten.");
                                AARTOBase.LogProcessing(ResourceHelper.GetResource("FileHasExists", fileName), LogType.Info);
                                continue;
                            }
                            else
                            {
                                tdf.CreateFile(fileName);
                            }

                            foreach (EasyPayTransaction tran in tdf.Authority.EasyPayTransactions)
                                SetEasyPayDataFileName(tran.EasyPayNumber, tran.EasyPayAction, fileName);

                            //Logger.Write(string.Format("Created Daily Traffic Data File: {0}", tdf.FileName), "General", (int)TraceEventType.Information);
                            AARTOBase.LogProcessing(ResourceHelper.GetResource("CreateTrafficFile", tdf.FileName), LogType.Info);
                        }
                        catch (Exception ex)
                        {
                            //jerry 2012-1-18 add delete file
                            File.Delete(fileName);
                            //Logger.Write(ex.StackTrace, "General", (int)TraceEventType.Critical);
                            AARTOBase.LogProcessing(ex.StackTrace, LogType.Error);
                            if (scope != null)
                                scope.Dispose();
                            continue;
                        }

                        //jerry 2012-1-19 add
                        //db.SetEasyPaySentDateByAutIntNo(authority.ID);
                        SetEasyPaySentDateByAutIntNo(authority.ReceiverIdentifier);

                        scope.Complete();
                    }
                }

                TimeSpan ts = DateTime.Now - dt;
                //Logger.Write(string.Format("Traffic data files created in {0} - {1}", exportPath, ts), "General", (int)TraceEventType.Information);
                AARTOBase.LogProcessing(ResourceHelper.GetResource("TrafficFileCreated", easyPayExportPath, ts), LogType.Info);
            }
            catch (Exception ex)
            {
                //Logger.Write(ex.StackTrace, "General", (int)TraceEventType.Critical);
                AARTOBase.LogProcessing(ex.StackTrace, LogType.Error, ServiceOption.BreakAndStop);
            }
        }

        /// <summary>
        /// Gets the data to send to EasyPay.
        /// </summary>
        /// <param name="authorities">The list of authorities to be populate with data to send.</param>
        private void GetEasyPayDataToSend(List<EasyPayAuthority> authorities)
        {
            SqlConnection con = new SqlConnection(thaboConnectStr);

            DateTime dt = DateTime.Now;
            //Logger.Write("Executing SQL: EasyPayDataToSend...", "General", (int)TraceEventType.Information);
            AARTOBase.LogProcessing(ResourceHelper.GetResource("ExecSQL"), LogType.Info);

            using (var com = new SqlCommand("EasyPayDataToSend_WS", con))
            {
                com.CommandType = CommandType.StoredProcedure;
                com.CommandTimeout = 1800;

                try
                {
                    con.Open();
                    SqlDataReader reader = com.ExecuteReader();

                    // Process the Authorities
                    EasyPayAuthority authority = null;
                    int count = 0;
                    while (reader.Read())
                    {
                        authority = new EasyPayAuthority();
                        authority.ID = (int)reader["AutIntNo"];
                        authority.Name = (string)reader["AutName"];
                        authority.ReceiverIdentifier = (int)reader["EasyPayReceiptID"];
                        authorities.Add(authority);

                        //Logger.Write(string.Format("EasyPay Authority - {0}", authority), "General", (int)TraceEventType.Information);
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("EasyPayAuth", authority), LogType.Info);
                    }

                    //Logger.Write(string.Format("Authorities received: " + authorities.Count), "General", (int)TraceEventType.Information);
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("ReceivedAuth", authorities.Count), LogType.Info);

                    // Process the EasyPay Transactions
                    int autIntNo;
                    int receiverId;
                    reader.NextResult();
                    while (reader.Read())
                    {
                        if (reader["AutIntNo"] == null || string.IsNullOrEmpty(reader["AutIntNo"].ToString()))
                            continue;

                        autIntNo = (int)reader["AutIntNo"];
                        receiverId = (int)reader["EasyPayReceiptID"];

                        // Check if this EasyPayTran is for a different Authority
                        //if (!autIntNo.Equals(authority.ID) || !receiverId.Equals(authority.ReceiverIdentifier))
                        if (!receiverId.Equals(authority.ReceiverIdentifier))
                        {
                            authority = null;
                            foreach (EasyPayAuthority epAuthority in authorities)
                            {
                                //dls 2012-05-23 - stop checking for specific Authority - use only receiverid
                                //if (epAuthority.ID.Equals(autIntNo) && epAuthority.ReceiverIdentifier.Equals(receiverId))
                                if (epAuthority.ReceiverIdentifier.Equals(receiverId))
                                {
                                    authority = epAuthority;
                                    break;
                                }
                            }
                            if (authority == null)
                            {
                                //Logger.Write(string.Format("There was no authority found for ID {0}.", autIntNo), "General", (int)TraceEventType.Error);
                                AARTOBase.LogProcessing(ResourceHelper.GetResource("NoAuthority", autIntNo), LogType.Error);
                                continue;
                            }
                        }


                        var tran = new EasyPayTransaction(authority);
                        tran.EasyPayAction = ((string)reader["EPAction"])[0];
                        tran.Cents = (Int32)((decimal)reader["Amount"] * 100);
                        tran.EasyPayNumber = (string)reader["EasyPayNo"];
                        tran.ExpiryDate = (DateTime)reader["ExpiryDate"];
                        tran.RegNo = (string)reader["RegNo"];

                        // Add it to the authority's transaction list
                        authority.EasyPayTransactions.Add(tran);
                        count++;

                    }
                    reader.Close();
                    reader.Dispose();

                    TimeSpan ts = DateTime.Now - dt;
                    //Logger.Write(string.Format("Completed - EasyPayDataToSend{0} in {1})", count.ToString("###,###,###"), ts), "General", (int)TraceEventType.Information);
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("ComplatedEasyPayDataToSend", count.ToString("###,###,###"), ts), LogType.Info);
                }
                catch (Exception ex)
                {
                    AARTOBase.LogProcessing(ex.Message, LogType.Error);
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Sets the name of the easy pay data file.
        /// </summary>
        /// <param name="easyPayNumber">The easy pay number.</param>
        /// <param name="easyPayAction">The easy pay action.</param>
        /// <param name="fileName">Name of the file.</param>
        public void SetEasyPayDataFileName(string easyPayNumber, char easyPayAction, string fileName)
        {
            DBHelper db = new DBHelper(this.thaboConnectStr);
            SqlParameter[] paras = new SqlParameter[4];
            paras[0] = new SqlParameter("@EasyPayNumber", easyPayNumber);
            paras[1] = new SqlParameter("@EasyPayAction", easyPayAction);
            paras[2] = new SqlParameter("@FileName", fileName);
            paras[3] = new SqlParameter("@LastUser", AARTOBase.LastUser);

            string errMsg = string.Empty;
            db.ExecuteNonQuery("EasyPayTranSetDataFileName_WS", paras, out errMsg);
        }

        /// <summary>
        /// jerry 2012-1-19 add
        /// set AuthorityEasyPay.EasyPaySentDate column value to today by AutIntNo 
        /// </summary>
        /// <param name="autIntNo"></param>
        //public void SetEasyPaySentDateByAutIntNo(int autIntNo)
        public void SetEasyPaySentDateByAutIntNo(int easyPayReceiptID)
        {
            DBHelper db = new DBHelper(this.thaboConnectStr);
            SqlParameter[] paras = new SqlParameter[1];
            paras[0] = new SqlParameter("@EasyPayReceiptID", easyPayReceiptID);

            string errMsg = string.Empty;
            db.ExecuteNonQuery("SetEasyPaySentDate", paras, out errMsg);
        }

    }
}
