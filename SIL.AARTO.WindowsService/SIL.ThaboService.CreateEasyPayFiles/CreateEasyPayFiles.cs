﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.ThaboService.CreateEasyPayFiles
{
    partial class CreateEasyPayFiles : ServiceHost
    {
        public CreateEasyPayFiles()
            : base("", new Guid("CEE4EA11-298B-43FA-8D2A-C3B8BE7988B0"), new CreateEasyPayFilesService())
        {
            InitializeComponent();
        }
    }
}
