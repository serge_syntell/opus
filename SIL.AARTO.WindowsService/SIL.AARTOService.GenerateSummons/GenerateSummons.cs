﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.GenerateSummons
{
    partial class GenerateSummons : ServiceHost
    {
        public GenerateSummons()
            : base("", new Guid("FA75ED0F-BDAE-4139-A890-BAADDD1FE4F3"), new GenerateSummonsClientService())
        {
            InitializeComponent();
        }
    }
}
