﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.GenerateSummons
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.GenerateSummons"
                ,
                DisplayName = "SIL.AARTOService.GenerateSummons"
                ,
                Description = "SIL AARTOService GenerateSummons"
            };

            ProgramRun.InitializeService(new GenerateSummons(), serviceDescriptor, args);
        }
    }
}
