﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using SIL.AARTOService.Resource;
using SIL.ServiceQueueLibrary.DAL.Services;
using Stalberg.TMS;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using System.Data;
using Stalberg.TMS_TPExInt.Components;

namespace SIL.AARTOService.GenerateSummons
{
    #region copy from Stalberg.TMS_TPExInt.Objects
    internal class IDNumber
    {
        // Fields
        private DateTime dob;
        private int sequence;
        private bool isCitizen;
        private byte overflowIndicator;
        private byte checksum;
        private string value = string.Empty;
        private string error = string.Empty;
        private bool isMale;

        /// <summary>
        /// Tries to parse the string value to an ID Number.
        /// </summary>
        /// <param name="value">The string containing and ID Number.</param>
        /// <param name="type">The string containing and ID Type.</param> //2013-06-26 the parameter is added by Nancy for 5007
        /// <param name="idNumber">The id number.</param>
        /// <returns><c>true</c> if the string is successfully parsed to an ID Number</returns>
        public static bool TryParse(string value, string type, out IDNumber idNumber)
        {
            // Initialise the OUT variable
            idNumber = new IDNumber();

            // Make sure there's a value
            if (string.IsNullOrEmpty(value))
            {
                idNumber.error = "The value string cannot be null or empty.";
                return false;
            }

            // Clean the value up
            value = value.Trim();
            //2013-06-26 updated by Nancy for 5007 (Add if..else.. for the type value ,because the different check in different type) start
            type = type.Trim();
            if (type == "01" || type == "04")
            {
                if (value.Length <= 2)
                {
                    idNumber.error = string.Format("The value '{0}' must be more than 2 digits long", value);
                    return false;
                }
            }
            else if (type == "03")
            {
                AARTORules rules = AARTORules.GetSingleTon();
                bool IsCheck = rules.Rule("5080").ARString.ToUpper().Equals("Y");
                if (IsCheck)
                {
                    if (value.Length <= 2)
                    {
                        idNumber.error = string.Format("The value '{0}' must be more than 2 digits long", value);
                        return false;
                    }
                }
            }
            else
            {
                if (value.Length != 13)
                {
                    idNumber.error = string.Format("The value '{0}' must be exactly 13 digits long", value);
                    return false;
                }

                // Make sure that all the letters in the string are numbers
                foreach (char c in value)
                {
                    if (!char.IsDigit(c))
                    {
                        idNumber.error = string.Format("The character '{0}' in '{1}' is not a digit. ID Numbers are 13 digit sequences.", c, value);
                        return false;
                    }
                }

                //2013-06-26 updated by Nancy for 5007 (Add if..else.. for the type value ,because the different check in different type) end

                // Set the internal values
                try
                {
                    idNumber.value = value;
                    int year = int.Parse(value.Substring(0, 2));
                    year += 2000;
                    if (year > DateTime.Today.Year)
                        year -= 100;
                    idNumber.dob = new DateTime(year, int.Parse(value.Substring(2, 2)), int.Parse(value.Substring(4, 2)));
                    idNumber.overflowIndicator = byte.Parse(value.Substring(11, 1));
                    idNumber.Sequence = int.Parse(value.Substring(6, 4));
                    idNumber.isCitizen = value.Substring(10, 1)[0].Equals('0');
                    idNumber.checksum = byte.Parse(value.Substring(12, 1));
                }
                catch (Exception ex)
                {
                    idNumber.error = ex.Message;
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IDNumber"/> class.
        /// </summary>
        private IDNumber()
        {
        }

        /// <summary>
        /// Determines whether the checksum digit at the end of the ID Number is valid.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if the checksum is valid; otherwise, <c>false</c>.
        /// </returns>
        public bool IsValidChecksum()
        {
            int odd = 0;
            string even = string.Empty;

            int val;
            for (int i = 0; i < this.value.Length - 1; i++)
            {
                val = int.Parse(this.value[i].ToString());

                if ((i % 2) != 0) // Even
                    even += val.ToString();
                else // Odd
                    odd += val;
            }

            int evenNumber = int.Parse(even);
            evenNumber = evenNumber * 2;
            even = evenNumber.ToString();
            evenNumber = 0;

            foreach (char c in even)
                evenNumber += int.Parse(c.ToString());

            evenNumber += odd;
            even = evenNumber.ToString();
            evenNumber = int.Parse(even[even.Length - 1].ToString());
            evenNumber = 10 - evenNumber;
            if (evenNumber == 10)
                evenNumber = 0;

            return (this.checksum == evenNumber);
        }

        /// <summary>
        /// Gets any error message generated when the ID number was created.
        /// </summary>
        /// <value>The error.</value>
        public string Error
        {
            get { return this.error; }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return this.value;
        }

        /// <summary>
        /// Gets the overflow indicator.
        /// </summary>
        /// <value>The overflow indicator.</value>
        public byte OverflowIndicator
        {
            get { return this.overflowIndicator; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is citizen.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is citizen; otherwise, <c>false</c>.
        /// </value>
        public bool IsCitizen
        {
            get { return this.isCitizen; }
        }

        /// <summary>
        /// Gets a value indicating whether this ID is for a male.
        /// </summary>
        /// <value><c>true</c> if this ID is for a male; otherwise, <c>false</c>.</value>
        public bool IsMale
        {
            get { return this.isMale; }
        }

        /// <summary>
        /// Gets the sequence.
        /// </summary>
        /// <value>The sequence.</value>
        public int Sequence
        {
            get { return this.sequence; }
            private set
            {
                if (value < 0 || value > 9999)
                    throw new Exception("The sequence number cannot be less than zero or greater than 5000.");

                if (value > 4999)
                    this.isMale = true;

                this.sequence = value;
            }
        }

        /// <summary>
        /// Gets the date of birth.
        /// </summary>
        /// <value>The date of birth.</value>
        public DateTime DateOfBirth
        {
            get { return this.dob; }
        }


    }

    internal class SumCharge
    {
        /// <summary>
        /// Gets or sets the Database charge ID.
        /// </summary>
        public int ChargeIntNo { get; set; }

        public long ChargeRowVersion { get; set; }

        /// <summary>
        /// Gets or sets the amount of the fine.
        /// </summary>
        public double FineAmount { get; set; }

        public bool ChgIsMain { get; set; }

        public int ChargeStatus { get; set; }

        //jerry 2012-03-29 add
        public string ChgNoAOG { get; set; }
    }

    internal class Summons
    {
        // Fields
        private int notIntNo;
        private int chargeIntNo;
        private string ticketNo;
        //dls 070714 - need to add separate summons no
        private string summonsNo;
        private string sendTo;
        private string proxyFlag = string.Empty;
        private DateTime posted1stNotice;
        private double fineAmount;
        private int crtIntNo;
        //changed areacode from int to varchar(8)
        //private short postCode = -1;
        private string postCode = "";
        private Authority authority = null;
        private int asIntNo;
        private DateTime courtDate;
        private string caseNo = string.Empty;
        private int acIntNo;
        //dls 070707 - need two separate date rules for issuing summons and selecting court date
        //private DateRulesDetails dateRule = null;
        //private DateRulesDetails summonsDateRule = null;
        //private DateRulesDetails courtDateRule = null;
        private int noOfDaysSummonsIssue = 0;
        private int noOfDaysCourtDate = 0;
        private bool isValid = true;
        private string offenderIdNumber = string.Empty;
        private string offenderForeNames = string.Empty;
        private string offenderIdType = string.Empty; //2013-06-26 added by Nancy for 5007 
        // Added LMZ 2008-08-27
        //private DateRulesDetails lastAOGDateRule;
        //private DateRulesDetails sumServeByDateRule;
        private int noOfDaysLastAOGDate = 0;
        private int noOfDaysSumServByDate = 0;
        private long chargeRowVersion;
        // mrs 20080906 added
        private int crtRIntNo;
        private string filmType = string.Empty;
        private int summonsPrintFileBatchSize = 200;
        private bool isSection35 = false; // jake 2013-12-13 added 

        private List<SumCharge> sumCharges = new List<SumCharge>();  // Oscar added

        /// <summary>
        /// Initializes a new instance of the <see cref="Summons"/> class.
        /// </summary>
        public Summons()
        {

        }

        public bool IsSection35
        {
            get { return isSection35; }
            set { isSection35 = value; }
        }

        public int SummonsPrintFileBatchSize
        {
            get { return this.summonsPrintFileBatchSize; }
            set { this.summonsPrintFileBatchSize = value; }
        }

        public int NoOfDaysCourtDate
        {
            get { return this.noOfDaysCourtDate; }
            set { this.noOfDaysCourtDate = value; }
        }

        /// <summary>
        /// Gets or sets the date rule for the authority.
        /// </summary>
        /// <value>The date rule.</value>
        public int NoOfDaysSummonsIssue
        {
            get { return this.noOfDaysSummonsIssue; }
            set { this.noOfDaysSummonsIssue = value; }
        }

        public int NoOfDaysSumServByDate
        {
            get { return this.noOfDaysSumServByDate; }
            set { this.noOfDaysSumServByDate = value; }
        }

        public int NoOfDaysLastAOGDate
        {
            get { return this.noOfDaysLastAOGDate; }
            set { this.noOfDaysLastAOGDate = value; }
        }

        /// <summary>
        /// Gets or sets the Authority-Court int no.
        /// </summary>
        /// <value>The ac int no.</value>
        public int AcIntNo
        {
            get { return this.acIntNo; }
            set { this.acIntNo = value; }
        }

        #region Oscar 20101101 - disabled
        //public Int64 ChargeRowVersion
        //{
        //    get { return this.chargeRowVersion; }
        //    set { this.chargeRowVersion = value; }
        //}
        #endregion

        /// <summary>
        /// Gets or sets the case number of the summons.
        /// </summary>
        /// <value>The case number.</value>
        public string CaseNumber
        {
            get { return caseNo; }
            set { caseNo = value; }
        }

        /// <summary>
        /// Gets or sets the court date for the Summons.
        /// </summary>
        /// <value>The court date.</value>
        public DateTime CourtDate
        {
            get { return courtDate; }
            set { courtDate = value; }
        }

        /// <summary>
        /// Gets or sets the authority summons server int no.
        /// </summary>
        /// <value>The authority summons server int no.</value>
        public int AuthoritySummonsServerIntNo
        {
            get { return asIntNo; }
            set { asIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the authority int no.
        /// </summary>
        /// <value>The authority int no.</value>
        public Authority Authority
        {
            get { return this.authority; }
            set { this.authority = value; }
        }

        /// <summary>
        /// Gets or sets the post code.
        /// </summary>
        /// <value>The post code.</value>
        //changed areacode from int to varchar(8)
        //public short PostCode
        //{
        //    get { return postCode; }
        //    set { postCode = value; }
        //}
        public string PostCode
        {
            get { return postCode; }
            set { postCode = value; }
        }

        public string FilmType
        {
            get { return filmType; }
            set { filmType = value; }
        }

        /// <summary>
        /// Gets or sets the database court ID.
        /// </summary>
        /// <value>The court int no.</value>
        public int CourtIntNo
        {
            get { return crtIntNo; }
            set { crtIntNo = value; }
        }

        #region Oscar 20101101 - disabled
        ///// <summary>
        ///// Gets or sets the amount of the fine.
        ///// </summary>
        ///// <value>The fine amount.</value>
        //public double FineAmount
        //{
        //    get { return fineAmount; }
        //    set { fineAmount = value; }
        //}
        #endregion

        /// <summary>
        /// Gets or sets the date the 1st notice was posted.
        /// </summary>
        /// <value>The date posted1st notice.</value>
        public DateTime DatePosted1stNotice
        {
            get { return posted1stNotice; }
            set { posted1stNotice = value; }
        }

        /// <summary>
        /// Gets or sets the proxy flag.
        /// </summary>
        /// <value>The proxy flag.</value>
        public string ProxyFlag
        {
            get { return proxyFlag; }
            set { proxyFlag = value; }
        }

        /// <summary>
        /// Gets or sets who to send the summons to.
        /// </summary>
        /// <value>The send to.</value>
        public string SendTo
        {
            get { return sendTo; }
            set { sendTo = value; }
        }

        /// <summary>
        /// Gets or sets the original ticket number of the summons.
        /// </summary>
        /// <value>The ticket number.</value>
        public string SummonsNumber
        {
            get { return summonsNo; }
            set { summonsNo = value; }
        }

        //dls 070714 - added (separate to Summons No)
        public string TicketNo
        {
            get { return ticketNo; }
            set { ticketNo = value; }
        }

        #region Oscar 20101101 - disabled
        ///// <summary>
        ///// Gets or sets the Database charge ID.
        ///// </summary>
        ///// <value>The charge int no.</value>
        //public Int32 ChargeIntNo
        //{
        //    get { return chargeIntNo; }
        //    set { chargeIntNo = value; }
        //}
        #endregion

        // mrs 20080906 added
        public int CourtRoomIntNo
        {
            get { return crtRIntNo; }
            set { crtRIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the database ID of the underlying Notice.
        /// </summary>
        /// <value>The ID.</value>
        public int ID
        {
            get { return notIntNo; }
            set { notIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the offender ID number.
        /// </summary>
        /// <value>The offender ID number.</value>
        public string OffenderIDNumber
        {
            get { return this.offenderIdNumber; }
            set { this.offenderIdNumber = value.Trim(); }
        }

        /// <summary>
        /// Gets or sets the offender ID type.
        /// </summary>
        /// <value>The offender ID type.</value>
        public string OffenderIDType //2013-06-26 added by Nancy for 5007
        {
            get { return this.offenderIdType; }
            set { this.offenderIdType = value.Trim(); }
        }

        /// <summary>
        /// Gets or sets the offender fore names.
        /// </summary>
        /// <value>The offender fore names.</value>
        public string OffenderForeNames
        {
            get { return this.offenderForeNames; }
            set { this.offenderForeNames = value.Trim(); }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is valid.
        /// </summary>
        /// <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
        public bool IsValid
        {
            get { return this.isValid; }
        }

        /// <summary>
        /// Determines whether this instance is valid.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </returns>
        public bool ValidateIDNumber()
        {
            this.isValid = false;

            // Check the ID Number
            IDNumber id;
            if (!IDNumber.TryParse(this.offenderIdNumber, this.offenderIdType, out id)) //2013-06-26 updated by Nancy for 5007
                return false;

            // Check CDV digit
            if (this.offenderIdType == "02") //2013-07-02 added by Nancy for 5007
                if (!id.IsValidChecksum())
                    return false;

            this.isValid = true;
            return true;
        }

        /// <summary>
        /// Validates the offender's names.
        /// </summary>
        /// <returns></returns>
        public bool ValidateOffenderNames()
        {
            // If the new authRule=Y then do not summons the offender
            if (this.authority.MustHaveFullNamesForSummonsIssue && this.offenderForeNames.Length == 0)
            {
                this.isValid = false;
                return false;
            }

            return true;
        }

        // Oscar added
        public List<SumCharge> SumCharges
        {
            get { return this.sumCharges; }
            set { this.sumCharges = value; }
        }

        public string CrtName { get; set; }
    }

    internal enum ChargeStatus
    {
        /// <summary>
        /// The charge has been promoted to Summons status, but has not been prepared
        /// </summary>
        EligibleForSummons = 600,
        /// <summary>
        /// The Summons was prepared but no Summons Server was allocated for the postal code
        /// for the Authority in the area that the summons needed to be served in.
        /// </summary>
        NoSummonsServerForArea = 603,
        /// <summary>
        /// When he Summons was processed no Court dates were available to allocate to it.
        /// </summary>
        NoCourtDatesForSummons = 606
    }
    #endregion

    public class GenerateSummonsClientService : ServiceDataProcessViaQueue
    {
        public GenerateSummonsClientService()
            : base("", "", new Guid("D39A26C1-8111-412F-B728-0E0DC9A153A8"), ServiceQueueTypeList.GenerateSummons)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);

            // 2013-08-28, Oscar added
            this.nwncdManager = new NoticeWithNoCourtDateManager(this.connAARTODB, AARTOBase.LastUser)
            {
                OnLog = (s, t) => AARTOBase.LogProcessing(s, t,
                    t == LogType.Error ? ServiceOption.Break : ServiceOption.Continue, this.queueItem,
                    t == LogType.Error ? QueueItemStatus.UnKnown : this.queueItem.Status)
            };

            AARTOBase.OnServiceStarting = () =>
            {
                if (ServiceParameters != null
                && this.DelayActionDate_InHours <= 0
                && ServiceParameters.ContainsKey("DelayActionDate_InHours")
                && !string.IsNullOrEmpty(ServiceParameters["DelayActionDate_InHours"]))
                    this.DelayActionDate_InHours = Convert.ToInt32(ServiceParameters["DelayActionDate_InHours"]);

                ssAllocator.Reset();
            };
            AARTOBase.OnServiceSleeping = PushPrintServiceQueue;
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        AuthorityDetails authDetails = null;
        public SIL.AARTO.DAL.Entities.Notice noticeEntity = new SIL.AARTO.DAL.Entities.Notice();
        Authority authority;
        Summons summons = new Summons();

        string SUM_STATUS = "Pending";

        string connAARTODB;
        string currentAutCode, lastAutCode;
        int notIntNo, autIntNo;
        string autName;
        DateTime processDate = DateTime.Now;

        //jerry 2012-03-15 add
        int noOfDaysTo1stNotice;
        int noOfDaysToSummons;
        string DataWashingActived = null;
        int DataWashingRecycleMonth;

        int DelayActionDate_InHours;
        private bool isIbmPrinter;

        private string S35NoAOGPermitted = null;

        // Oscar 2013-05-02 added
        readonly SummonsServerAllocator ssAllocator = new SummonsServerAllocator(false);
        bool allocatedByCourt;

        // Oscar 2013-06-26 added, for log message
        string noticeTicketNumber;

        // 2013-08-28, Oscar added
        readonly NoticeWithNoCourtDateManager nwncdManager;
        QueueItem queueItem;

        // 2014-07-11, Oscar added, MI5->Driver updating should happen before forename check.
        readonly Mi5PersonaService mi5pService = new Mi5PersonaService();

        // 2014-07-28, Oscar added, for status 601
        readonly NoticeService noticeService = new NoticeService();
        readonly ChargeService chargeService = new ChargeService();
        readonly ServiceQueueService queueService = new ServiceQueueService();
        readonly QueueItemProcessor queProcessor = new QueueItemProcessor();

        public override void InitialWork(ref QueueItem item)
        {
            if (item.Body != null && !string.IsNullOrWhiteSpace(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;

                    if (ServiceUtility.IsNumeric(item.Body))
                        this.notIntNo = Convert.ToInt32(item.Body);
                    //this.currentAutCode = item.Group.Trim();
                    // Oscar 20120315 change the group
                    string[] group = item.Group.Trim().Split('|');
                    if (group == null || group.Length != 3)
                    {
                        //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("QueueGroupValidationFailed"), true);
                        AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("QueueGroupValidationFailed"));
                        return;
                    }
                    this.currentAutCode = group[0].Trim();

                    rules.InitParameter(this.connAARTODB, this.currentAutCode);

                    // Oscar 2013-06-24, short term solution for 410 generate summons. (with Adam)
                    // Edge  2013-07-16, long term solution if 410 discard queue
                    //var notice = new AARTO.DAL.Services.NoticeService().GetByNotIntNo(this.notIntNo);
                    // 2014-07-28, Oscar changed
                    var notice = noticeService.GetByNotIntNo(this.notIntNo);

                    if (notice != null && notice.NoticeStatus == 410)
                    {
                        //item.ActDate = DateTime.Now.AddDays(5);
                        //item.Status = QueueItemStatus.Retry;
                        //item.IsSuccessful = false;

                        AARTOBase.LogProcessing(ResourceHelper.GetResource("InvalidNoticeStatus410", notice.NotTicketNo), LogType.Info, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                        return;
                    }

                    // Oscar 2013-06-26 added, for log message
                    this.noticeTicketNumber = string.Empty;
                    if (notice != null)
                    {
                        this.noticeTicketNumber = notice.NotTicketNo;

                        // Oscar 2013-06-26 added, put NotTicketProcessor checking in right place 
                        if (notice.NotTicketProcessor != "TMS")
                        {
                            AARTOBase.LogProcessing(ResourceHelper.GetResource("NoticeTicketProcessorIsNotTMS", noticeTicketNumber), LogType.Info, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                            return;
                        }
                    }

                    // Oscar 20121008 this isIbmPrinter should be puted into QueueItemValidation, not here.
                    //this.isIbmPrinter = rules.Rule("6209").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase);
                    if (this.lastAutCode != this.currentAutCode)
                    {
                        if (QueueItemValidation(ref item))
                        {
                            this.lastAutCode = this.currentAutCode;
                            this.processDate = DateTime.Now;

                            DataWashingActived = rules.Rule("9060").ARString.ToUpper().Trim();
                            DataWashingRecycleMonth = rules.Rule("9080").ARNumeric;
                        }
                    }

                    //Jerry 2014-01-17 add
                    //SIL.AARTO.DAL.Entities.Notice noticeEntity = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(this.notIntNo);
                    if (notice != null)
                    {
                        if (notice.Not2ndPaymentDate != null && DateTime.Now <= notice.Not2ndPaymentDate.Value)
                        {
                            item.IsSuccessful = false;
                            item.ActDate = notice.Not2ndPaymentDate.Value.AddDays(1);
                            item.Status = QueueItemStatus.Retry;
                            return;
                        }
                        else if (notice.NotPaymentDate != null && DateTime.Now <= notice.NotPaymentDate.Value)
                        {
                            item.IsSuccessful = false;
                            item.ActDate = notice.NotPaymentDate.Value.AddDays(1);
                            item.Status = QueueItemStatus.Retry;
                            return;
                        }
                    }

                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                    return;
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority"));
                return;
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                this.queueItem = item;

                // 2013-08-28, Oscar added
                queueItem = item;

                try
                {
                    if (!item.IsSuccessful) return;
                    PrepareForSummons(ref item);

                    if (!item.IsSuccessful) return;
                    SummonsValidation(ref item);

                    if (!item.IsSuccessful) return;
                    if (!WriteSummons())
                    {
                        //AARTOBase.ErrorProcessing(ref item);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                    return;
                }

                queueList[i] = item;
            }

        }


        private bool QueueItemValidation(ref QueueItem item)
        {
            string msg;
            if (this.lastAutCode != this.currentAutCode)
            {
                if (this.notIntNo != null)
                {
                    //noticeEntity = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(this.notIntNo);
                    // 2014-07-28, Oscar changed
                    noticeEntity = noticeService.GetByNotIntNo(this.notIntNo);
                }

                this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim().Equals(this.currentAutCode, StringComparison.OrdinalIgnoreCase));
                if (this.authDetails != null && this.authDetails.AutIntNo > 0)
                {
                    this.autIntNo = this.authDetails.AutIntNo;
                    this.autName = this.authDetails.AutName;

                    //modified by linda 2012-12-21(replace AutTicketProcessor by NotTicketProcessor)
                    if (this.noticeEntity.NotTicketProcessor == "TMS")
                    {
                        this.authority = GetAuthorityForSummons_WS(out msg);
                        if (this.authority != null)
                        {
                            //this.authority.TicketProcessor = this.authDetails.AutTicketProcessor;

                            if (this.authority.sLastCreateDate.AddDays(1) > DateTime.Now)
                            {
                                AARTOBase.ErrorProcessing(ref item);
                                this.Logger.Warning(string.Format(ResourceHelper.GetResource("NoSummonsCreatedDateLessThan24H"), this.autName));
                                return false;
                            }

                            this.authority.MustHaveFullNamesForSummonsIssue = rules.Rule("4900").ARString.ToUpper().Equals("Y");

                            // Oscar 2013-05-07 added
                            this.allocatedByCourt = rules.Rule("5070").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase);

                            if (!GetSummonsServerForAuthority(ref this.authority, out msg))
                            {
                                AARTOBase.ErrorProcessing(ref item);
                                this.Logger.Warning(string.Format(ResourceHelper.GetResource("ProcessSummonsNoSummonsServers"), this.autName, this.autIntNo));
                                return false;
                            }

                            this.authority.UseOriginalFineAmnt = rules.Rule("5030").ARString.ToUpper().Equals("Y");

                            this.authority.DisplayAccusedCompany = rules.Rule("5065").ARString.ToUpper().Equals("Y");

                            this.authority.SummonsPrintStationeryType = "CPA5";

                            this.authority.NoOfDaysSummonsIssue = rules.Rule("NotPosted1stNoticeDate", "NotIssueSummonsDate").DtRNoOfDays;

                            this.authority.NoOfDaysCourtDate = rules.Rule("NotIssueSummonsDate", "CDate").DtRNoOfDays;

                            this.authority.NoOfDaysLastAOGDate = rules.Rule("CDate", "LastAOGDate").DtRNoOfDays;

                            this.authority.NoOfDaysSumServByDate = rules.Rule("CDate", "SumServeByDate").DtRNoOfDays;

                            this.authority.MinAmountForSummonsProcessing = rules.Rule("5010").ARNumeric;

                            int key5100 = rules.Rule("5100").ARNumeric;
                            this.authority.SummonsPrintFileBatchSize = key5100 == 0 ? 200 : key5100;

                            this.isIbmPrinter = rules.Rule("6209").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase);

                            // jake 2013-11-28 added
                            this.S35NoAOGPermitted = rules.Rule("6600").ARString.Trim();

                            //Jerry 2014-01-17 move this check into InitialWork method
                            //Jerry 2013-03-06 add
                            //SIL.AARTO.DAL.Entities.Notice noticeEntity = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(this.notIntNo);
                            //if (noticeEntity != null)
                            //{
                            //    if (noticeEntity.Not2ndPaymentDate != null && DateTime.Now < noticeEntity.Not2ndPaymentDate)
                            //    {
                            //        item.IsSuccessful = false;
                            //        item.ActDate = noticeEntity.Not2ndPaymentDate.Value.AddDays(2);
                            //        item.Status = QueueItemStatus.Retry;
                            //        return false;
                            //    }
                            //}

                            return true;
                        }
                        else
                        {
                            AARTOBase.ErrorProcessing(ref item);
                            this.Logger.Warning(string.Format(ResourceHelper.GetResource("ProcessSummonsNoNoticesEligibleForSummons"), this.autName));
                            return false;
                        }
                    }
                    else
                    {
                        // Oscar 2013-06-26 added, for log message
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("NoticeTicketProcessorIsNotTMS", noticeTicketNumber), LogType.Info, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);

                        //item.IsSuccessful = false;
                        //item.Status = QueueItemStatus.Discard;
                        return false;
                    }
                }
                else
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.currentAutCode));
                    return false;
                }
            }
            else
                return true;
        }

        private void PrepareForSummons(ref QueueItem item)
        {
            string msg;
            if (item.IsSuccessful)
            {
                //this.Logger.Info(string.Format(ResourceHelper.GetResource("ProcessingSummonsForAuthority"), this.autName, this.autIntNo));

                int notIntNo = this.notIntNo;
                DataSet ds = GetSummonsListByAuth_WS(ref notIntNo, out msg);
                if (notIntNo <= 0)
                {
                    //AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("CatastrophicErrorGettingSummonsServers"), this.autIntNo, this.notIntNo, msg), true);
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("UnabelToCheckNoticeForSummonsGeneration", this.notIntNo), false, QueueItemStatus.PostPone);
                    return;
                }
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    SumCharge sumCharge;
                    DataRow row;
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        row = ds.Tables[0].Rows[i];
                        if (i == 0)
                        {
                            //jerry 2012-03-15 add condition
                            string newRegNoDetails = DBHelper.GetDataRowValue<string>(row, "NewRegNoDetails");
                            string notNewOffender = DBHelper.GetDataRowValue<string>(row, "NotNewOffender");
                            bool offenderDetailsChanged = DBHelper.GetDataRowValue<bool>(row, "OffenderDetailsChanged");
                            if (newRegNoDetails == "Y" || notNewOffender == "Y")
                            {
                                if (!offenderDetailsChanged)
                                {
                                    // update Notice.OffenderDetailsChanged
                                    int success = UpdateOffenderDetailsChangedOfNotice(notIntNo, true, ref msg, AARTOBase.LastUser);
                                    if (success <= 0)
                                    {
                                        AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrMsgUpdateNoticeInGenerateSummons"), notIntNo));
                                        return;
                                    }

                                    // set queue status and action date
                                    //noOfDaysTo1stNotice = rules.Rule("NotOffenceDate", "NotPosted1stNoticeDate").DtRNoOfDays;
                                    //noOfDaysToSummons = rules.Rule("NotOffenceDate", "NotIssueSummonsDate").DtRNoOfDays;   -- dls 2012-04-04 - this is the wrong rule!! and actually we only need to use the single rule of NotPosted1stNoticeDate --> NotIssueSummonsDate
                                    noOfDaysToSummons = rules.Rule("NotPosted1stNoticeDate", "NotIssueSummonsDate").DtRNoOfDays;

                                    // Oscar 2013-07-02 added log
                                    Logger.Info(ResourceHelper.GetResource("ThereIsAnNewOffenderRepOnThisNotice", noticeTicketNumber, noOfDaysToSummons));

                                    item.IsSuccessful = false;
                                    //item.ActDate = DateTime.Now.AddDays(noOfDaysToSummons - noOfDaysTo1stNotice);
                                    item.ActDate = DateTime.Now.AddDays(noOfDaysToSummons);
                                    //Jerry 2012-05-14 add
                                    item.Status = QueueItemStatus.Retry;
                                    return;
                                }
                            }

                            this.summons = new Summons();
                            this.summons.TicketNo = DBHelper.GetDataRowValue<string>(row, "NotTicketNo");
                            this.summons.Authority = this.authority;
                            this.summons.NoOfDaysSummonsIssue = this.authority.NoOfDaysSummonsIssue;
                            this.summons.NoOfDaysCourtDate = this.authority.NoOfDaysCourtDate;
                            this.summons.NoOfDaysLastAOGDate = this.authority.NoOfDaysLastAOGDate;
                            this.summons.NoOfDaysSumServByDate = this.authority.NoOfDaysSumServByDate;
                            this.summons.ID = DBHelper.GetDataRowValue<int>(row, "NotIntNo");
                            this.summons.ProxyFlag = DBHelper.GetDataRowValue<string>(row, "NotProxyFlag");
                            this.summons.DatePosted1stNotice = DBHelper.GetDataRowValue<DateTime>(row, "NotPosted1stNoticeDate", this.processDate);
                            this.summons.CourtIntNo = DBHelper.GetDataRowValue<int>(row, "CrtIntNo");
                            this.summons.FilmType = DBHelper.GetDataRowValue<string>(row, "NotFilmType");
                            this.summons.SummonsPrintFileBatchSize = this.authority.SummonsPrintFileBatchSize;
                            this.summons.CrtName = DBHelper.GetDataRowValue<string>(row, "CrtName");
                            // Jake 2013-12-13 added
                            this.summons.IsSection35 = DBHelper.GetDataRowValue<bool>(row, "IsSection35");
                        }

                        sumCharge = new SumCharge();
                        sumCharge.ChargeIntNo = DBHelper.GetDataRowValue<int>(row, "ChgIntNo");
                        if (!this.authority.UseOriginalFineAmnt)
                            sumCharge.FineAmount = DBHelper.GetDataRowValue<double>(row, "ChgRevFineAmount");
                        else
                            sumCharge.FineAmount = DBHelper.GetDataRowValue<double>(row, "ChgFineAmount");
                        sumCharge.ChargeRowVersion = DBHelper.GetDataRowValue<long>(row, "ChargeRowVersion");
                        sumCharge.ChgIsMain = DBHelper.GetDataRowValue<bool>(row, "ChgIsMain");
                        sumCharge.ChargeStatus = DBHelper.GetDataRowValue<int>(row, "ChargeStatus");

                        //jerry 2012-03-29 add
                        sumCharge.ChgNoAOG = DBHelper.GetDataRowValue<string>(row, "ChgNoAOG");

                        if (sumCharge.FineAmount >= authority.MinAmountForSummonsProcessing || sumCharge.FineAmount == 0 || summons.FilmType.Equals("H"))
                        {
                            this.summons.SumCharges.Add(sumCharge);
                        }
                    }

                    if (this.summons.SumCharges.Count <= 0)
                    {
                        AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("CreateSummonsForAuthorityNoValidSumcharge"), this.autName, this.notIntNo));
                        return;
                    }

                }
                else
                {
                    //AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("CreateSummonsForAuthorityNoNoticesReadyForSummons"), this.notIntNo, this.autName), false, QueueItemStatus.Discard);
                    //this.Logger.Info(ResourceHelper.GetResource("CreateSummonsForAuthorityNoNoticesReadyForSummons", this.notIntNo, this.autName));

                    // Oscar 2013-06-26 added, for log message
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("CreateSummonsForAuthorityNoNoticesReadyForSummons", this.notIntNo, this.autName), LogType.Info, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);

                    //item.IsSuccessful = false;
                    //item.Status = QueueItemStatus.Discard;
                    return;
                }
            }
        }

        private void SummonsValidation(ref QueueItem item)
        {
            // 2014-07-11, Oscar added, MI5->Driver updating should happen before forename check.
            if (!item.IsSuccessful) return;
            GetLatestAddress();

            if (!item.IsSuccessful) return;
            CheckSummonsOffenderDetails(ref item);

            if (!item.IsSuccessful) return;
            if (!summons.ValidateOffenderNames())
            {
                //string msg;
                //int chgIntNo;
                //long chgRowVersion;
                //foreach (SumCharge sumCharge in this.summons.SumCharges)
                //{
                //    chgRowVersion = sumCharge.ChargeRowVersion;
                //    //chgIntNo = db.SummonsSetInvalidOffenderDetailsForSummons(sumCharge.ChargeIntNo, ref chgRowVersion);
                //    chgIntNo = ChargeUpdateStatus(sumCharge.ChargeIntNo, 591, ref chgRowVersion, out msg);
                //    sumCharge.ChargeRowVersion = chgRowVersion;

                //    if (chgIntNo < 0)
                //    {
                //        this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorUpdatingChargeStatusRowversionChanged"), summons.TicketNo));
                //        return false;
                //    }
                //}
                //QueueItemProcessor queProcessor = new QueueItemProcessor();
                //queProcessor.Send(
                //    new QueueItem()
                //    {
                //        Body = this.notIntNo,
                //        QueueType = ServiceQueueTypeList.DataWashing_ID_or_Names
                //    }
                //);

                // 2014-07-28, Oscar added, for status 601
                PushQueueForDataWashingExtractor();

                // Oscar 2013-06-26 added, for log message
                AARTOBase.LogProcessing(ResourceHelper.GetResource("FailedToValidateOffenderForename", summons.OffenderForeNames, noticeTicketNumber), LogType.Info, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);

                //item.IsSuccessful = false;
                //item.Status = QueueItemStatus.Discard;
                return;
            }

            if (!item.IsSuccessful) return;
            CheckSummonsServerForAuthority(ref item);


            if (!item.IsSuccessful) return;
            CheckCourtDateForAuthority(ref item);

        }

        private void CheckSummonsOffenderDetails(ref QueueItem item)
        {
            string msg;
            DataSet ds = CheckSummonsOffenderDetails(out msg);

            if (!string.IsNullOrEmpty(msg))
            {
                AARTOBase.ErrorProcessing(ref item, msg, true);
                return;
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                this.summons.OffenderForeNames = DBHelper.GetDataRowValue<string>(dr, "ForeNames");
                this.summons.OffenderIDNumber = DBHelper.GetDataRowValue<string>(dr, "IDNumber");
                this.summons.OffenderIDType = DBHelper.GetDataRowValue<string>(dr, "IDType");
            }

            if (!this.summons.ValidateIDNumber())
            {
                this.Logger.Warning(string.Format(ResourceHelper.GetResource("FailedToValidateIDNumber"), summons.TicketNo, summons.OffenderForeNames, summons.OffenderIDNumber));  //2013-09-03, Oscar modify message
                TMSData db = new TMSData(this.connAARTODB);
                int chgIntNo;
                long chgRowVersion;
                foreach (SumCharge sumCharge in this.summons.SumCharges)
                {
                    chgRowVersion = sumCharge.ChargeRowVersion;
                    chgIntNo = db.SummonsSetInvalidOffenderDetailsForSummons(sumCharge.ChargeIntNo, AARTOBase.LastUser, ref chgRowVersion);
                    //chgIntNo = ChargeUpdateStatus(sumCharge.ChargeIntNo, 592, ref chgRowVersion, out msg);
                    sumCharge.ChargeRowVersion = chgRowVersion;

                    if (chgIntNo < 0)
                    {
                        AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorUpdatingChargeStatusRowversionChanged"), summons.TicketNo), true);
                        return;
                    }
                }

                //QueueItemProcessor queProcessor = new QueueItemProcessor();
                //queProcessor.Send(
                //    new QueueItem()
                //    {
                //        Body = this.notIntNo,
                //        QueueType = ServiceQueueTypeList.DataWashing_ID_or_Names
                //    }
                //);
            }

            if (!this.summons.IsValid)
            {
                item.IsSuccessful = false;
                item.Status = QueueItemStatus.Discard;
                return;
            }
        }

        private void CheckSummonsServerForAuthority(ref QueueItem item)
        {
            string msg;
            DataSet ds = GetSummonsChargePostCode(out msg);
            if (!string.IsNullOrEmpty(msg))
            {
                AARTOBase.ErrorProcessing(ref item, msg, true);
                return;
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                summons.PostCode = DBHelper.GetDataRowValue<string>(dr, "StCode");
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item);
                this.Logger.Warning(string.Format(ResourceHelper.GetResource("CreateSummonsForAuthorityNoPostalCode"), this.summons.SummonsNumber));
                return;
            }

            long chgRowVersion;
            TMSData db = new TMSData(this.connAARTODB);
            bool result = true;
            foreach (SumCharge sumCharge in this.summons.SumCharges)
            {
                #region Oscar 2013-05-02 disabled
                //if (!string.IsNullOrEmpty(this.summons.PostCode))
                //{
                //    // Check if there's a summons server for that authority
                //    if (!this.summons.Authority.AreaCodes.ContainsKey(this.summons.PostCode))
                //    {
                //        chgRowVersion = sumCharge.ChargeRowVersion;
                //        db.SetChargeStatus((int)ChargeStatus.NoSummonsServerForArea, sumCharge.ChargeIntNo, AARTOBase.lastUser, ref chgRowVersion);
                //        sumCharge.ChargeRowVersion = chgRowVersion;

                //        this.Logger.Warning(string.Format(ResourceHelper.GetResource("CreateSummonsForAuthorityNoSummonsServer"), this.summons.Authority.Name, this.summons.PostCode));
                //        result = false;
                //    }
                //    else
                //        this.summons.AuthoritySummonsServerIntNo = this.summons.Authority.AreaCodes[this.summons.PostCode];
                //}
                //else
                //{
                //    chgRowVersion = sumCharge.ChargeRowVersion;
                //    db.SetChargeStatus((int)ChargeStatus.NoSummonsServerForArea, sumCharge.ChargeIntNo, AARTOBase.lastUser, ref chgRowVersion);
                //    sumCharge.ChargeRowVersion = chgRowVersion;

                //    this.Logger.Warning(string.Format(ResourceHelper.GetResource("CreateSummonsForAuthorityNoSummonsServer"), this.summons.Authority.Name, this.summons.PostCode));

                //    result = false;
                //}
                #endregion

                // Oscar 2013-05-02 simplify the logic.
                int asIntNo;
                //if (!string.IsNullOrWhiteSpace(this.summons.PostCode)
                //    //&& this.summons.Authority.AreaCodes.ContainsKey(this.summons.PostCode))
                //    && (!this.allocatedByCourt && (asIntNo = this.ssAllocator.Allocate(this.autIntNo, this.summons.PostCode)) > 0
                //        || (this.allocatedByCourt && (asIntNo = ssAllocator.Allocate(this.autIntNo, this.summons.CourtIntNo)) > 0)))
                // 2014-02-26, Oscar changed checking
                if (!this.allocatedByCourt && !string.IsNullOrWhiteSpace(this.summons.PostCode) && (asIntNo = this.ssAllocator.Allocate(this.autIntNo, this.summons.PostCode)) > 0
                    || (this.allocatedByCourt && (asIntNo = this.ssAllocator.Allocate(this.autIntNo, this.summons.CourtIntNo)) > 0))
                {
                    //this.summons.AuthoritySummonsServerIntNo = this.summons.Authority.AreaCodes[this.summons.PostCode];
                    this.summons.AuthoritySummonsServerIntNo = asIntNo;
                }
                else
                {
                    chgRowVersion = sumCharge.ChargeRowVersion;
                    db.SetChargeStatus((int)ChargeStatus.NoSummonsServerForArea, sumCharge.ChargeIntNo, AARTOBase.LastUser, ref chgRowVersion);
                    sumCharge.ChargeRowVersion = chgRowVersion;

                    //if (!this.allocatedByCourt)
                    //    this.Logger.Warning(string.Format(ResourceHelper.GetResource("CreateSummonsForAuthorityNoSummonsServer"), this.summons.Authority.Name, this.summons.PostCode, summons.TicketNo));  // 2013-09-05, Oscar removed "PostCode.PadLeft(4,'0')", we don't need that.
                    //else
                    //    Logger.Warning(string.Format(ResourceHelper.GetResource("CreateSummonsForAuthorityNoSummonsServerCourt"), this.summons.CrtName));

                    // 2014-02-26, Oscar changed to use LogProcessing
                    AARTOBase.LogProcessing(!this.allocatedByCourt
                        ? string.Format(ResourceHelper.GetResource("CreateSummonsForAuthorityNoSummonsServer"), this.summons.Authority.Name, this.summons.PostCode, this.summons.TicketNo)
                        : string.Format(ResourceHelper.GetResource("CreateSummonsForAuthorityNoSummonsServerCourt"), this.summons.CrtName),
                        LogType.Warning, ServiceOption.Break, item);

                    result = false;
                }
            }

            if (!result)
            {
                AARTOBase.ErrorProcessing(ref item);
                return;
            }
        }

        private void CheckCourtDateForAuthority(ref QueueItem item)
        {
            if (DateTime.Compare(this.summons.DatePosted1stNotice, Convert.ToDateTime("2006-01-01")) < 0)
            {
                AARTOBase.ErrorProcessing(ref item);
                this.Logger.Warning(string.Format(ResourceHelper.GetResource("CreateSummonsForAuthorityFirstNoticeDateInvalid"), summons.DatePosted1stNotice, summons.TicketNo));
                return;
            }

            //jerry 2012-03-30 add
            bool isNoAOG = false;
            if (this.summons.IsSection35)
            {
                isNoAOG = this.summons.IsSection35;
            }
            else
            {
                foreach (SumCharge sumCharge in summons.SumCharges)
                {
                    if (sumCharge.ChgNoAOG == "Y")
                    {
                        isNoAOG = true;
                    }
                }
            }
            string needRestrictNoAOG = null;
            //Jerry 2012-05-21 add
            string needSeparateNoAOG = null;
            if (rules.Rule(summons.CourtIntNo, "3040").CRString == "Y")
            {
                if (isNoAOG)
                {
                    needSeparateNoAOG = "Y";
                }
            }
            else
            {
                if (isNoAOG && rules.Rule(summons.CourtIntNo, "3030").CRString == "Y")
                {
                    needRestrictNoAOG = "Y";
                }
            }

            int crtRIntNo = 0;
            TMSData db = new TMSData(this.connAARTODB);
            DateTime courtDate = db.GetSummonsCourtDate(summons.Authority.ID, summons.CourtIntNo, summons.NoOfDaysCourtDate, this.rules.Rule("5020").ARNumeric, AARTOBase.LastUser, summons.FilmType, ref crtRIntNo, needRestrictNoAOG, needSeparateNoAOG, this.summons.IsSection35 == true ? "Y" : "N");

            bool result = true;
            long chgRowVersion;
            foreach (SumCharge sumCharge in summons.SumCharges)
            {
                switch (crtRIntNo)
                {
                    case -2:
                        this.Logger.Warning(string.Format(ResourceHelper.GetResource("CreateSummonsForAuthorityUnableToFindValidCourtDate"), summons.Authority.Name, summons.TicketNo, summons.NoOfDaysCourtDate, DateTime.Today.ToShortDateString(), summons.CourtIntNo));
                        // Set the charge status for the notice to 606 - No court date for summons
                        chgRowVersion = sumCharge.ChargeRowVersion;
                        db.SetChargeStatus((int)ChargeStatus.NoCourtDatesForSummons, sumCharge.ChargeIntNo, AARTOBase.LastUser, ref chgRowVersion);
                        sumCharge.ChargeRowVersion = chgRowVersion;
                        result = false;
                        break;
                }
            }

            // 2013-08-28, Oscar added
            if (crtRIntNo == -2)
            {
                item.Status = this.nwncdManager.InsertNoticeWithNoCourtDate(this.autIntNo, this.notIntNo, this.summons.CourtIntNo, this.queueItem.ActDate, this.queueItem.Priority)
                    ? QueueItemStatus.Discard : QueueItemStatus.UnKnown;
                item.IsSuccessful = false;
            }

            this.summons.AcIntNo = 0; //no longer used
            this.summons.CourtRoomIntNo = crtRIntNo;
            this.summons.CourtDate = courtDate;

            //if (!result)
            //{
            //AARTOBase.ErrorProcessing(ref item);
            //return;
            //}
        }

        private bool WriteSummons()
        {
            string sumStatus = SUM_STATUS;
            string errMessage = string.Empty;

            DateTime dtLastAOG;
            DateTime dtSumServeBy;

            dtLastAOG = summons.CourtDate.AddDays((double)summons.NoOfDaysLastAOGDate);
            dtSumServeBy = summons.CourtDate.AddDays((double)summons.NoOfDaysSumServByDate);

            string s35Authorityrule = this.rules.Rule("6600").ARString;

            List<string> chgIntNoList = new List<string>();
            List<string> chgRowVersion = new List<string>();

            foreach (SumCharge sumCharge in summons.SumCharges)
            {
                chgIntNoList.Add(sumCharge.ChargeIntNo.ToString());
                chgRowVersion.Add(sumCharge.ChargeRowVersion.ToString());
            }

            TMSData db = new TMSData(this.connAARTODB);
            string lastUser = AARTOBase.LastUser;
            if (lastUser.Length > 50)
                lastUser = lastUser.Substring(0, 50);
            string printFileName;
            int addSumIntNo = db.AddSummons(summons.ID, sumStatus, summons.TicketNo, summons.ProxyFlag, summons.CourtDate, dtLastAOG,
                dtSumServeBy, lastUser, summons.AuthoritySummonsServerIntNo, lastUser, summons.CourtIntNo,
                summons.Authority.ID, summons.CourtRoomIntNo, this.authority.UseOriginalFineAmnt == true ? "Y" : "N",
                ref errMessage, 0, chgIntNoList, chgRowVersion, summons.SummonsPrintFileBatchSize,
                string.Empty, out printFileName, DataWashingActived, DataWashingRecycleMonth,
                this.authority.DisplayAccusedCompany, this.allocatedByCourt, s35Authorityrule);


            switch (addSumIntNo)
            {
                case -1:
                    //this.Logger.Error(ResourceHelper.GetResource("WriteSummonsFailedChargeRowversionChanged"));
                    // 2013-09-24, Oscar changed Logger.Error
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedChargeRowversionChanged"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -2:
                    //this.Logger.Error(string.Format(ResourceHelper.GetResource("CouldnotCreateSummonsSequenceNoMatchingCourtNo"), this.autName, this.summons.TicketNo));
                    // 2013-09-24, Oscar changed Logger.Error
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("CouldnotCreateSummonsSequenceNoMatchingCourtNo", this.autName, this.summons.TicketNo), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -3:
                    //this.Logger.Error(string.Format(ResourceHelper.GetResource("CouldnotCreateSummonsSequenceCannotBeFound"), this.autName, this.summons.CourtIntNo));
                    // 2013-09-24, Oscar changed Logger.Error
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("CouldnotCreateSummonsSequenceCannotBeFound", this.autName, this.summons.CourtIntNo), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -4:
                    //this.Logger.Error(ResourceHelper.GetResource("WriteSummonsFailedUnableToInsertSummons"));
                    // 2013-09-24, Oscar changed Logger.Error
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedUnableToInsertSummons"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -5:
                    //this.Logger.Error(ResourceHelper.GetResource("WriteSummonsFailedUnableToInsertJudgementType"));
                    // 2013-09-24, Oscar changed Logger.Error
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedUnableToInsertJudgementType"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -6:
                    //this.Logger.Error(ResourceHelper.GetResource("WriteSummonsFailedUnableToAddSummonsServer"));
                    // 2013-09-24, Oscar changed Logger.Error
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedUnableToAddSummonsServer"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -7:
                    //this.Logger.Error(ResourceHelper.GetResource("WriteSummonsFailedUnableToInsertSummonsCharge"));
                    // 2013-09-24, Oscar changed Logger.Error
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedUnableToInsertSummonsCharge"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -8:
                    //this.Logger.Error(ResourceHelper.GetResource("WriteSummonsFailedUnableToInsertSumCharge"));
                    // 2013-09-24, Oscar changed Logger.Error
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedUnableToInsertSumCharge"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -9:
                    //this.Logger.Error(ResourceHelper.GetResource("WriteSummonsFailedUnableToInsertAccused"));
                    // 2013-09-24, Oscar changed Logger.Error
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedUnableToInsertAccused"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -10:
                    //this.Logger.Error(ResourceHelper.GetResource("WriteSummonsFailedUnableToUpdatePrintFileName"));
                    // 2013-09-24, Oscar changed Logger.Error
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedUnableToUpdatePrintFileName"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -11:
                    //this.Logger.Error(ResourceHelper.GetResource("WriteSummonsFailedUnableToUpdateChargeStatus"));
                    // 2013-09-24, Oscar changed Logger.Error
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedUnableToUpdateChargeStatus"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -12:
                    //this.Logger.Error(ResourceHelper.GetResource("WriteSummonsFailedUnableToInsertEvidencePack"));
                    // 2013-09-24, Oscar changed Logger.Error
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedUnableToInsertEvidencePack"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -13:
                    //this.Logger.Error(ResourceHelper.GetResource("CreateSummonsPrintFileNameFailedUnableToGenerateFileName"));
                    // 2013-09-24, Oscar changed Logger.Error
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("CreateSummonsPrintFileNameFailedUnableToGenerateFileName"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -14:
                case -15:
                    //this.Logger.Error(ResourceHelper.GetResource("CreateSummonsNoFailedUnableToGeneratePrintSequenceNo"));
                    // 2013-09-24, Oscar changed Logger.Error
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("CreateSummonsNoFailedUnableToGeneratePrintSequenceNo"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -16:
                    //this.Logger.Error(ResourceHelper.GetResource("CreateSummonsNoFailedUnableToIncrementCaseAllocatedNo"));
                    // 2013-09-24, Oscar changed Logger.Error
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("CreateSummonsNoFailedUnableToIncrementCaseAllocatedNo"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -17:
                    // 2013-09-24, Oscar added
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedS54AllocatedFailed"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -18:
                    // 2013-09-24, Oscar added
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedSummonsWithdrawForReissueFailed"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                //Jerry 2013-12-02 add error handling
                case -19:
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedUpdateNoticeStatusTo641"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -20:
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedUpdateChargeStatusTo641"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -21:
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedUpdateSummonsStatusTo641"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -22:
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedUpdateSumChargeStatusTo641"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -23:
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedInsertEvidencePack"), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                case -30:
                    //this.Logger.Error(string.Format(ResourceHelper.GetResource("CreateSummonsNoFailed"), errMessage));
                    // 2013-09-24, Oscar changed Logger.Error
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("CreateSummonsNoFailed", errMessage), LogType.Error, ServiceOption.Break, queueItem);
                    return false;
                default:
                    // 2013-09-24, Oscar added
                    if (addSumIntNo < 0)
                    {
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("WriteSummonsFailedReturnedValue", addSumIntNo), LogType.Error, ServiceOption.Break, queueItem);
                        return false;
                    }
                    break;
            }

            if (addSumIntNo > 0)
            {
                //QueueItemProcessor queProcessor = new QueueItemProcessor();   // 2014-07-28, Oscar removed
                SIL.AARTO.DAL.Entities.Notice notice = noticeService.GetByNotIntNo(summons.ID);
                string msg;
                int pfnIntNo = AARTOBase.SavePrintFileName(PrintFileNameType.Summons, printFileName, this.autIntNo, addSumIntNo, out msg);
                if (pfnIntNo <= 0)
                {
                    //AARTOBase.ErrorProcessing(msg, true);
                    // 2013-09-24, Oscar changed ErrorProcessing
                    AARTOBase.LogProcessing(msg, LogType.Error, ServiceOption.BreakAndStop, queueItem);
                    return false;
                }

                else if (isIbmPrinter && !this.printQList.ContainsKey(pfnIntNo))
                {
                    //2013-12-12 Added by Nancy(Push queue for PrintToFile Service) 
                    //2014-07-25 added Data rule DR_SumPrintDate_SumCourtDate
                    QueueItem item = new QueueItem()
                    {
                        Body = pfnIntNo,
                        Group = currentAutCode + "|" + ((printFileName.IndexOf("CPA6") >= 0) ? (int)SIL.AARTO.DAL.Entities.ReportConfigCodeList.CPA6DirectPrinting : (int)SIL.AARTO.DAL.Entities.ReportConfigCodeList.CPA5DirectPrinting),
                        ActDate = summons.CourtDate.AddDays(-1 * rules.Rule("SumPrintDate", "SumCourtDate").DtRNoOfDays),
                        QueueType = ServiceQueueTypeList.PrintToFile,
                        LastUser = AARTOBase.LastUser
                    };
                    queProcessor.Send(item);

                    this.printQList.Add(pfnIntNo, currentAutCode);
                }
                else if (!this.printQList.ContainsKey(pfnIntNo))
                {
                    // Oscar 20120705 move push queue from OnServiceSleeping to here
                    QueueItem item = new QueueItem()
                    {
                        Body = pfnIntNo,
                        Group = currentAutCode,
                        ActDate = summons.CourtDate.AddDays(-1 * rules.Rule("SumPrintDate", "SumCourtDate").DtRNoOfDays),
                        QueueType = ServiceQueueTypeList.PrintSummons,
                        LastUser = AARTOBase.LastUser
                    };
                    queProcessor.Send(item);

                    this.printQList.Add(pfnIntNo, currentAutCode);
                }

                //QueueItemProcessor queProcessor = new QueueItemProcessor();
                queProcessor.Send(
                    new QueueItem()
                    {
                        Body = addSumIntNo,
                        Group = currentAutCode,
                        //Jake 2015-03-17 use new date rule 
                        //ActDate = DateTime.Now.AddDays(rules.Rule("SumIssueDate", "SumExpiredDate").DtRNoOfDays),
                        ActDate = notice.NotOffenceDate.AddDays(rules.Rule("NotOffenceDate", "SumExpireDate").DtRNoOfDays + 1),
                        QueueType = ServiceQueueTypeList.ExpireUnservedSummons
                    },
                    new QueueItem()
                    {
                        Body = this.notIntNo,
                        QueueType = ServiceQueueTypeList.SearchNameIDUpdate
                    }
                );

                //Jerry 2014-05-22 push StagnantSummons queue
                queProcessor.Send(
                    new QueueItem()
                    {
                        Body = addSumIntNo,
                        ActDate = summons.CourtDate.AddDays(1),
                        LastUser = AARTOBase.LastUser,
                        QueueType = ServiceQueueTypeList.StagnantSummons
                    }
                );

                // 2014-02-26, Oscar added successful log.
                AARTOBase.LogProcessing(ResourceHelper.GetResource("SummonsCreatedSuccessfully", printFileName, summons.TicketNo), LogType.Info, item: queueItem, status: queueItem.Status);
            }

            return true;
        }


        Dictionary<int, string> printQList = new Dictionary<int, string>();
        private void PushPrintServiceQueue()
        {
            if (this.printQList.Count > 0)
            {
                //QueueItemProcessor processor = new QueueItemProcessor();
                //List<QueueItem> qList = new List<QueueItem>();
                //foreach (KeyValuePair<int, string> q in this.printQList)
                //{
                //    QueueItem item = new QueueItem()
                //    {
                //        Body = q.Key,
                //        Group = q.Value,
                //        QueueType = ServiceQueueTypeList.PrintSummons
                //    };
                //    qList.Add(item);
                //}
                //processor.Send(qList);
                this.printQList.Clear();
            }
        }

        //Jerry 2013-01-03 delete ticketProcessor param
        //private Authority GetAuthorityForSummons_WS(string ticketProcessor, out string message)
        private Authority GetAuthorityForSummons_WS(out string message)
        {
            DBHelper db = new DBHelper(this.connAARTODB);
            SqlParameter[] paras = new SqlParameter[1];
            paras[0] = new SqlParameter("@AutIntNo", this.autIntNo);
            //paras[1] = new SqlParameter("@Processor", ticketProcessor);
            DataSet ds = db.ExecuteDataSet("GetAuthorityForSummons_WS", paras, out message);
            Authority auth = null;
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                auth = new Authority();
                DataRow dr = ds.Tables[0].Rows[0];
                auth.ID = DBHelper.GetDataRowValue<int>(dr, "AutIntNo");
                auth.Name = DBHelper.GetDataRowValue<string>(dr, "AutName");
                auth.sLastCreateDate = DBHelper.GetDataRowValue<DateTime>(dr, "SummonsLastCreateDate", DateTime.MinValue);
            }
            return auth;
        }

        private bool GetSummonsServerForAuthority(ref Authority auth, out string message)
        {
            DBHelper db = new DBHelper(this.connAARTODB);
            //SqlParameter[] paras = new SqlParameter[1];
            //paras[0] = new SqlParameter("@AutIntNo", this.autIntNo);

            // Oscar 2013-05-07 changed
            var paras = new[]
            {
                new SqlParameter("@AutIntNo", this.autIntNo),
                new SqlParameter("@AllocatedByCourt", this.allocatedByCourt),
            };

            DataSet ds = db.ExecuteDataSet("SummonsServerByPostCode", paras, out message);
            string areaCode;
            int asIntNo;

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (!this.allocatedByCourt)
                    {
                        areaCode = dr["AreaCode"].ToString();
                        asIntNo = Convert.ToInt32(dr["ASIntNo"]);
                        //if (!auth.AreaCodes.ContainsKey(areaCode))
                        //    auth.AreaCodes.Add(areaCode, asIntNo);

                        // Oscar 2013-05-02 added
                        var priority = Convert.ToInt32(dr["ASSPriority"]);
                        var percentage = Convert.ToInt32(dr["ASSPercentage"]);
                        ssAllocator.Add(this.autIntNo, areaCode, asIntNo, percentage, priority);
                    }
                    else
                    {
                        var crtIntNo = Convert.ToInt32(dr["CrtIntNo"]);
                        var ascIntNo = Convert.ToInt32(dr["ASCIntNo"]);
                        ssAllocator.Add(this.autIntNo, crtIntNo, ascIntNo);
                    }
                }
                return true;
            }
            else
                return false;
        }

        private DataSet GetSummonsListByAuth_WS(ref int notIntNo, out string message)
        {
            DBHelper db = new DBHelper(this.connAARTODB);
            SqlParameter[] paras = new SqlParameter[5];
            paras[0] = new SqlParameter("@AutIntNo", this.autIntNo);
            paras[1] = new SqlParameter("@Processor", this.noticeEntity.NotTicketProcessor);
            paras[2] = new SqlParameter("@NoDaysFromNotPosted1stNoticeDate", this.authority.NoOfDaysSummonsIssue);
            paras[3] = new SqlParameter("@NotIntNo", notIntNo) { Direction = ParameterDirection.InputOutput };
            paras[4] = new SqlParameter("@LastUser", AARTOBase.LastUser);   // 2013-07-26 add by Henry
            DataSet ds = db.ExecuteDataSet("SummonsCreateListByAuth_WS", paras, out message);
            object obj = paras[3].Value;
            if (ServiceUtility.IsNumeric(obj))
                notIntNo = Convert.ToInt32(obj);
            else
                notIntNo = 0;
            return ds;
        }

        private DataSet CheckSummonsOffenderDetails(out string message)
        {
            DBHelper db = new DBHelper(this.connAARTODB);
            SqlParameter[] paras = new SqlParameter[1];
            paras[0] = new SqlParameter("@NotIntNo", this.notIntNo);
            return db.ExecuteDataSet("SummonsGetOffenderDetails", paras, out message);
        }

        private DataSet GetSummonsChargePostCode(out string message)
        {
            DBHelper db = new DBHelper(this.connAARTODB);
            SqlParameter[] paras = new SqlParameter[1];
            paras[0] = new SqlParameter("@NotIntNo", this.notIntNo);
            return db.ExecuteDataSet("SummonsChargePostCode", paras, out message);
        }

        // 2013-07-19 comment by Henry for useless
        //private int ChargeUpdateStatus(int chgIntNo, int status, ref long chgRowversion, out string message)
        //{
        //    DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));
        //    SqlParameter[] paras = new SqlParameter[3];
        //    paras[0] = new SqlParameter("@ChgIntNo", chgIntNo) { Direction = ParameterDirection.InputOutput };
        //    paras[1] = new SqlParameter("@Status", status);
        //    paras[2] = new SqlParameter("@ChargeRowVersion", chgRowversion) { Direction = ParameterDirection.InputOutput };
        //    db.ExecuteNonQuery("ChargeUpdateStatus", paras, out message);
        //    chgRowversion = Convert.ToInt32(paras[2].Value);
        //    return Convert.ToInt32(paras[1].Value);
        //}
        /// 2013-07-26 add parameter lastUser by Henry
        private int UpdateOffenderDetailsChangedOfNotice(int notIntNo, bool offenderDetailsChanged, ref string errMsg, string lastUser)
        {
            DBHelper db = new DBHelper(this.connAARTODB);
            SqlParameter[] paras = new SqlParameter[3];
            paras[0] = new SqlParameter("@NotIntNo", notIntNo);
            paras[1] = new SqlParameter("@OffenderDetailsChanged", offenderDetailsChanged);
            paras[2] = new SqlParameter("@LastUser", lastUser);

            object obj = db.ExecuteScalar("UpdateOffenderDetailsChangedOfNotice", paras, out errMsg);
            int result;
            if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(errMsg))
                result = Convert.ToInt32(obj);
            else
                result = -1;

            return result;
        }

        // 2014-07-11, Oscar added, MI5->Driver updating should happen before forename check.
        void GetLatestAddress()
        {
            if (DataWashingActived == "Y" && this.notIntNo > 0)
                mi5pService.GetLatestAddress(this.notIntNo, DataWashingRecycleMonth);
        }

        // 2014-07-28, Oscar added, for status 601
        void PushQueueForDataWashingExtractor()
        {
            var notice = noticeService.GetByNotIntNo(this.notIntNo);
            if (notice.NoticeStatus != 601)
            {
                notice.NoticeStatus = 601;
                notice.LastUser = AARTOBase.LastUser;
                noticeService.Save(notice);
            }

            var charges = chargeService.GetByNotIntNo(this.notIntNo);
            charges.ForEach(c =>
            {
                if (c.ChargeStatus >= 600
                    && c.ChargeStatus < 610
                    && c.ChargeStatus != 601)
                {
                    c.ChargeStatus = 601;
                    c.LastUser = AARTOBase.LastUser;
                    chargeService.Save(c);
                }
            });

            var actionDate = notice.NotPosted1stNoticeDate.GetValueOrDefault(notice.NotOffenceDate)
                .AddDays(this.authority.NoOfDaysSummonsIssue - rules.Rule("DataWashDate", "NotIssueSummonDate").DtRNoOfDays);

            var queues = queueService.GetExistingQueues((int)ServiceQueueTypeList.DataWashingExtractor, this.notIntNo.ToString(CultureInfo.InvariantCulture), null, actionDate);

            if (queues.Count <= 0)
            {
                queProcessor.Send(new QueueItem
                {
                    Body = this.notIntNo,
                    Group = currentAutCode,
                    ActDate = actionDate,
                    QueueType = ServiceQueueTypeList.DataWashingExtractor,
                    LastUser = AARTOBase.LastUser
                });
            }
        }

    }
}