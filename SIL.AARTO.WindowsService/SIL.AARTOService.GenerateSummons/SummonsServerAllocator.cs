﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace SIL.AARTOService.GenerateSummons
{
    public class SummonsServerAllocator
    {
        readonly List<SummonsServerCollection> cache = new List<SummonsServerCollection>();
        readonly bool throwException;
        SummonsServerCollection current;

        public SummonsServerAllocator(bool throwException)
        {
            this.throwException = throwException;
        }

        public void Reset()
        {
            this.cache.ForEach(c => c.Reset());
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void Clear()
        {
            this.cache.Clear();
        }

        public void Add(int autIntNo, string areaCode, int ssIntNo, int percentage, int priority)
        {
            var ssc = this.current;
            if (ssc != null
                && ssc.AllocationType == AllocationType.AreaCode
                && ssc.AutIntNo == autIntNo
                && ssc.AreaCode.Equals(areaCode, StringComparison.OrdinalIgnoreCase)
                || (ssc = this.cache.FirstOrDefault(ss =>
                    ss.AllocationType == AllocationType.AreaCode
                        && ss.AutIntNo == autIntNo
                        && ss.AreaCode.Equals(areaCode, StringComparison.OrdinalIgnoreCase))) != null)
            {
                ssc.Add(ssIntNo, percentage, priority);
            }
            else
            {
                ssc = new SummonsServerCollection(this.throwException, AllocationType.AreaCode, this.cache, autIntNo, areaCode);
                ssc.Add(ssIntNo, percentage, priority);
                this.cache.Add(ssc);
                this.current = ssc;
            }
        }

        public void Add(int autIntNo, int crtIntNo, int ssIntNo)
        {
            var ssc = this.current;
            if (ssc != null
                && ssc.AllocationType == AllocationType.Court
                && ssc.AutIntNo == autIntNo
                && ssc.CrtIntNo == crtIntNo
                || (ssc = this.cache.FirstOrDefault(ss =>
                    ss.AllocationType == AllocationType.Court
                        && ss.AutIntNo == autIntNo
                        && ss.CrtIntNo == crtIntNo)) != null)
            {
                ssc.Add(ssIntNo);
            }
            else
            {
                ssc = new SummonsServerCollection(this.throwException, AllocationType.Court, this.cache, autIntNo, crtIntNo);
                ssc.Add(ssIntNo);
                this.cache.Add(ssc);
                this.current = ssc;
            }
        }

        public int Allocate(int autIntNo, string areaCode)
        {
            var ssc = this.current;
            if (ssc != null
                && ssc.AllocationType == AllocationType.AreaCode
                && ssc.AutIntNo == autIntNo
                && ssc.AreaCode.Equals(areaCode, StringComparison.OrdinalIgnoreCase)
                || (ssc = this.cache.FirstOrDefault(ss =>
                    ss.AllocationType == AllocationType.AreaCode
                        && ss.AutIntNo == autIntNo
                        && ss.AreaCode.Equals(areaCode, StringComparison.OrdinalIgnoreCase))) != null)
            {
                return ssc.Allocate();
            }
            else
            {
                if (this.throwException)
                    throw new InvalidOperationException(string.Format("There is no summons sever for this authority and area code. [Info - AutIntNo:{0}, AreaCode:\"{1}\"]", autIntNo, areaCode));
                else
                    return -1;
            }
        }

        public int Allocate(int autIntNo, int crtIntNo)
        {
            var ssc = this.current;
            if (ssc != null
                && ssc.AllocationType == AllocationType.Court
                && ssc.AutIntNo == autIntNo
                && ssc.CrtIntNo == crtIntNo
                || (ssc = this.cache.FirstOrDefault(ss =>
                    ss.AllocationType == AllocationType.Court
                        && ss.AutIntNo == autIntNo
                        && ss.CrtIntNo == crtIntNo)) != null)
            {
                return ssc.Allocate();
            }
            else
            {
                if (this.throwException)
                    throw new InvalidOperationException(string.Format("There is no summons sever for this authority and court. [Info - AutIntNo:{0}, CrtIntNo:{1}]", autIntNo, crtIntNo));
                else
                    return -1;
            }
        }
    }

    class SummonsServerCollection
    {
        readonly List<SummonsServer> summonsServers = new List<SummonsServer>();
        List<SummonsServerCollection> cache;

        bool standardCalculated;
        bool throwException;
        int totalCount, totalPercentage;

        public SummonsServerCollection(bool throwException, AllocationType allocationType, List<SummonsServerCollection> cache, int autIntNo, string areaCode)
        {
            Initialization(throwException, allocationType, cache, autIntNo);
            AreaCode = areaCode;
        }

        public SummonsServerCollection(bool throwException, AllocationType allocationType, List<SummonsServerCollection> cache, int autIntNo, int crtIntNo)
        {
            Initialization(throwException, allocationType, cache, autIntNo);
            CrtIntNo = crtIntNo;
        }

        public AllocationType AllocationType { get; private set; }
        public int AutIntNo { get; private set; }
        public int CrtIntNo { get; private set; }
        public string AreaCode { get; private set; }

        void Initialization(bool throwException, AllocationType allocationType, List<SummonsServerCollection> cache, int autIntNo)
        {
            this.throwException = throwException;
            AllocationType = allocationType;
            this.cache = cache;
            AutIntNo = autIntNo;
        }

        public void Reset()
        {
            this.standardCalculated = false;
            this.totalCount = 0;
            this.totalPercentage = 0;
            this.summonsServers.ForEach(ss => ss.Count = 0);
        }

        void CalculateStandard()
        {
            if (AllocationType != AllocationType.AreaCode
                || this.standardCalculated) return;

            this.summonsServers.ForEach(s => s.Standard = s.Percentage/(double)this.totalPercentage);
            this.standardCalculated = true;
        }

        bool CalculateOverview(int ssIntNo1, int ssIntNo2)
        {
            if (ssIntNo1 == ssIntNo2 || ssIntNo1 == 0)
                return false;

            int count1 = 0, count2 = 0,
                percentage1 = 0, percentage2 = 0,
                quantity1 = 0, quantity2 = 0;

            this.cache.ForEach(ss =>
            {
                if (AllocationType != AllocationType.AreaCode)
                    return;

                ss.summonsServers.ForEach(s =>
                {
                    if (s.SSIntNo == ssIntNo1)
                    {
                        count1 += s.Count;
                        percentage1 += s.Percentage;
                        quantity1++;
                    }
                    else if (s.SSIntNo == ssIntNo2)
                    {
                        count2 += s.Count;
                        percentage2 += s.Percentage;
                        quantity2++;
                    }
                });
            });

            return count1 != count2
                ? count1.CompareTo(count2) < 0
                : (percentage1/quantity1).CompareTo(percentage2/quantity2) > 0;
        }

        public void Add(int ssIntNo, int percentage = 0, int priority = 0)
        {
            if (AllocationType == AllocationType.AreaCode
                && percentage < 0)
            {
                if (this.throwException)
                    throw new ArgumentException(string.Format("Percentager must be greater than or equal to 0. [Info - AutIntNo:{0}, AreaCode:\"{1}\", SSIntNo:{2}, Percentage:{3}]", AutIntNo, AreaCode, ssIntNo, percentage), "percentage");
                else
                    return;
            }

            var ss = this.summonsServers.FirstOrDefault(s => s.SSIntNo == ssIntNo);
            if (ss != null)
            {
                if (ss.Percentage == percentage
                    && ss.Priority == priority)
                    return;

                this.totalPercentage += percentage - ss.Percentage;
                ss.Percentage = percentage;
                ss.Priority = priority;
            }
            else
            {
                ss = new SummonsServer
                {
                    SSIntNo = ssIntNo,
                    Percentage = percentage,
                    Priority = priority
                };
                this.summonsServers.Add(ss);
                this.totalPercentage += ss.Percentage;
            }

            this.standardCalculated = false;
        }

        public int Allocate()
        {
            if (this.summonsServers.Count <= 0)
            {
                if (this.throwException)
                {
                    switch (AllocationType)
                    {
                        case AllocationType.AreaCode:
                            throw new InvalidOperationException(string.Format("There is no summons sever for this authority and area code. [Info - AutIntNo:{0}, AreaCode:\"{1}\"]", AutIntNo, AreaCode));
                        case AllocationType.Court:
                            throw new InvalidOperationException(string.Format("There is no summons sever for this authority and court. [Info - AutIntNo:{0}, CrtIntNo:{1}]", AutIntNo, CrtIntNo));
                    }
                }
                else
                    return -1;
            }

            CalculateStandard();

            SummonsServer target = null;

            switch (AllocationType)
            {
                case AllocationType.AreaCode:
                {
                    var maxDeficit = 0d;
                    for (var i = 0; i < this.summonsServers.Count; i++)
                    {
                        var ss = this.summonsServers[i];
                        var deficit = ss.Standard - ((ss.Count + 1)/((double)this.totalCount + 1));

                        if (ss.Priority == 1)
                        {
                            target = ss;
                            break;
                        }

                        if (i == 0
                            || (deficit >= maxDeficit
                                && (deficit > maxDeficit || CalculateOverview(ss.SSIntNo, target.SSIntNo))))
                        {
                            maxDeficit = deficit;
                            target = ss;
                        }
                    }
                    target.Count++;
                    this.totalCount++;
                    break;
                }

                case AllocationType.Court:
                {
                    target = this.summonsServers[0];
                    break;
                }
            }

            return target.SSIntNo;
        }
    }

    class SummonsServer
    {
        public int SSIntNo { get; set; }
        public int Percentage { get; set; }
        public double Standard { get; set; }
        public int Count { get; set; }
        public int Priority { get; set; }
    }

    public enum AllocationType
    {
        Court = 1,
        AreaCode = 10
    }
}
