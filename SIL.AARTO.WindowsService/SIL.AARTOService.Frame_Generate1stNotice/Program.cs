﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Frame_Generate1stNotice
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.Frame_Generate1stNotice"
                ,
                DisplayName = "SIL.AARTOService.Frame_Generate1stNotice"
                ,
                Description = "SIL AARTOService Frame_Generate1stNotice"
            };

            ProgramRun.InitializeService(new Frame_Generate1stNotice(), serviceDescriptor, args);
        }
    }
}
