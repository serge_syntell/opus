﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using System.Configuration;
using SIL.QueueLibrary;
using SIL.AARTOService.Library;
using System.Data;
using System.Diagnostics;
using Stalberg.TMS;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Collections;
using SIL.AARTOService.Resource;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Transactions;
using SIL.AARTO.DAL.Data;


namespace SIL.AARTOService.Frame_Generate1stNotice
{
    public class Frame_Generate1stNoticeService : ServiceDataProcessViaQueue
    {
        public Frame_Generate1stNoticeService()
            : base("", "", new Guid("D4E4FC84-E483-4627-AB1B-052515FB74F3"), ServiceQueueTypeList.Frame_Generate1stNotice)
        {
            this._serviceHelper = new AARTOServiceBase(this,false);
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);

            AARTOBase.OnServiceStarting = () =>
            {
                ResourceHelper.SetLogger(Logger);

                this.groupStr = null;
                this.strAutCode = null;
                this.stamp = DateTime.Now;
                this.lastStamp = DateTime.Now;
                this.pfnList.Clear();
                SetServiceParameters();
            };
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        string strAutCode = null;
        string connectStr;
        int AutIntNo;
        int NotIntNo;
        int frameIntNo;
        string DataWashingActived = null;
        string IsUseDataWashingAddr = null;
        string SeperatePrintFileNameForS35 = null;  // Jake 2013-11-28 add new authority  6600 check
        int DataWashingRecycleMonth;
        int daysOfSummonsServicePeriodExpired;
        bool isHWOForPrint = false;      // Nick 20120801 added CoCT Report check
        bool isVIDForPrint = false;      // Linda 2013-02-04 added

        List<int> pfnList = new List<int>();
        bool isChecked = true;
        string batchFilename;
        DateTime stamp, lastStamp;
        string groupStr;
        string stampStr, hwoStampStr;
        bool isCreateTicket = true;

        // 2015-03-12, Oscar added (1916, ref 1915)
        readonly NoticeService noticeService = new NoticeService();
        readonly FrameService frameService = new FrameService();

        #region Oscar 20120406 disabled
        //public override void InitialWork(ref QueueItem item)
        //{
        //    string body = string.Empty;
        //    if (item.Body != null)
        //        body = item.Body.ToString();
        //    if (!string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(item.Group))
        //    {
        //        try
        //        {
        //            item.IsSuccessful = true;
        //            item.Status = QueueItemStatus.Discard;

        //            if (!int.TryParse(body, out frameIntNo))
        //            {
        //                AARTOBase.ErrorProcessing(ref item);
        //                return;
        //            }

        //            FilmDB filmDB = new FilmDB(connectStr);
        //            FrameDB frameDB = new FrameDB(connectStr);

        //            FrameDetails frame = frameDB.GetFrameDetails(frameIntNo);
        //            FilmDetails film = filmDB.GetFilmDetails(frame.FilmIntNo);

        //            if (!string.IsNullOrEmpty(film.FilmLockUser) || string.IsNullOrEmpty(film.FilmNo))
        //            {
        //                AARTOBase.ErrorProcessing(ref item);
        //                return;
        //            }

        //            AARTOBase.BatchCount--;
        //        }
        //        catch (Exception ex)
        //        {
        //            AARTOBase.ErrorProcessing(ref item, ex);
        //        }
        //    }
        //}
        #endregion

        public override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString().Trim();
            if (!string.IsNullOrWhiteSpace(body) && !string.IsNullOrWhiteSpace(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;

                    if (!int.TryParse(body, out this.frameIntNo))
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        AARTOBase.DeQueueInvalidItem(ref item);
                        return;
                    }

                    string groupStr = item.Group.Trim();
                    string[] group = groupStr.Split('|');
                    string autCode = group[0].Trim();

                    if (groupStr != this.groupStr)
                    {
                        if (this.groupStr != null)
                        {
                            //AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Retry);
                            //AARTOBase.SkipReceivingData = true;
                            AARTOBase.SkipReceivingQueueData(ref item);
                            this.groupStr = null;
                            this.strAutCode = null;
                            return;
                        }

                        this.groupStr = groupStr;
                        this.strAutCode = autCode;
                        rules.InitParameter(this.connectStr, this.strAutCode);

                        QueueGroupValidation(ref item);

                        // Oscar 2013-04-09 reset these 2 parameters.
                        isHWOForPrint = false;
                        isVIDForPrint = false;

                        // Nick 20120801 added CoCT Report check
                        if (group.Length == 3 && group[2].Trim() == "HWO")
                            isHWOForPrint = true;
                        else if (group.Length == 3 && group[2].Trim() == "VID")
                            isVIDForPrint = true;
                        //else
                        //    isHWOForPrint = false;
                    }
                    else if (!this.isChecked)
                    {
                        AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", autCode));
                        AARTOBase.DeQueueInvalidItem(ref item);
                        return;
                    }

                    QueueItemValidation(ref item);

                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                    AARTOBase.DeQueueInvalidItem(ref item);
                    return;
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                AARTOBase.DeQueueInvalidItem(ref item);
                return;
            }
        }

        private void QueueGroupValidation(ref QueueItem item)
        {
            this.isChecked = false;

            AuthorityDetails authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim().Equals(this.strAutCode, StringComparison.OrdinalIgnoreCase));
            if (authDetails == null)
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
                AARTOBase.DeQueueInvalidItem(ref item);
                return;
            }

            this.AutIntNo = authDetails.AutIntNo;

            this.DataWashingActived = rules.Rule("9060").ARString.Trim();
            this.IsUseDataWashingAddr = rules.Rule("9110").ARString.Trim();
            this.DataWashingRecycleMonth = rules.Rule("9080").ARNumeric;
            // Jake 2013-11-28 add new authority rule for seperate print file name for section 35
            this.SeperatePrintFileNameForS35 = rules.Rule("6600").ARString.Trim();
            this.daysOfSummonsServicePeriodExpired = rules.Rule("NotOffenceDate", "NotSummonsBeforeDate").DtRNoOfDays;

            this.isChecked = true;
        }

        private void QueueItemValidation(ref QueueItem item)
        {
            if (!isChecked || !item.IsSuccessful) return;
            if (!isHWOForPrint && !isVIDForPrint)
            {
                //FrameDetails frame = new FrameDB(connectStr).GetFrameDetails(this.frameIntNo);
                // 2015-03-12, Oscar changed (1916, ref 1915)
                var frame = this.frameService.GetByFrameIntNo(this.frameIntNo);

                // Oscar 2013-02-21 add check for frame dosen't exist.
                //if (string.IsNullOrWhiteSpace(frame.FrameStatus))
                // 2015-03-12, Oscar changed (1916, ref 1915)
                if (frame == null)
                {
                    // Oscar 2013-04-07 added message
                    Logger.Info(ResourceHelper.GetResource("QueueStatusWillBeSetToBecauseOf", "Discard", ResourceHelper.GetResource("FrameDoseNotExist"), item.Body));

                    AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                    return;
                }

                // Oscar 20121112 drop the "dead" frame
                //int frameStatus;
                //int.TryParse(frame.FrameStatus, out frameStatus);
                // 2015-03-12, Oscar changed (1916, ref 1915)
                var frameStatus = frame.FrameStatus;
                if (frameStatus >= 900)
                {
                    //add by linda 2013-01-17
                    NoticeDetails notice = new NoticeDB(connectStr).GetNoticeByFrameIntNo(this.frameIntNo);
                    //if (!(frameStatus == 900 && notice != null && notice.NoticeStatus == 5))
                    // Oscar 2013-04-09 changed condition.
                    if (frameStatus > 900 || (notice != null && notice.NoticeStatus != 5))
                    {
                        // Oscar 2013-04-07 added message
                        Logger.Info(ResourceHelper.GetResource("QueueStatusWillBeSetToBecauseOf", "Discard", ResourceHelper.GetResource(frameStatus > 900 ? "FrameHasBeenCancelled" : "NoticeAlreadyExistsForThisFrame"), item.Body));

                        AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                        return;
                    }
                }
                else
                {
                    //Jerry 2012-05-29 add 860, 850 status
                    if (frame == null || (frameStatus != 600 && frameStatus != 800 && frameStatus != 850 && frameStatus != 860))
                    {
                        // 2014-09-04, Oscar added log
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("FrameIsAtWrongStatus", this.frameIntNo, frame.FrameStatus), LogType.Info, ServiceOption.ContinueButQueueFail, item);

                        AARTOBase.DeQueueInvalidItem(ref item);
                        return;
                    }

                    FilmDetails film = new FilmDB(connectStr).GetFilmDetails(frame.FilmIntNo);
                    if (film == null || !string.IsNullOrEmpty(film.FilmLockUser) || string.IsNullOrEmpty(film.FilmNo))
                    {
                        // 2014-09-04, Oscar added log
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("FilmOfFrameIsInvalid", this.frameIntNo, film != null ? film.FilmNo : "", film != null ? film.FilmLockUser : ""), LogType.Info, ServiceOption.ContinueButQueueFail, item);

                        //AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.PostPone);
                        // Oscar 20121112 chaned to Unknown for FilmLockUser
                        AARTOBase.DeQueueInvalidItem(ref item);
                        return;
                    }

                    int notIntNo = new NoticeDB(connectStr).GetNoticeByFrame(this.frameIntNo);
                    if (notIntNo > 0)
                    {
                        // Oscar 2013-04-07 added message
                        Logger.Info(ResourceHelper.GetResource("QueueStatusWillBeSetToBecauseOf", "Discard", ResourceHelper.GetResource("NoticeAlreadyExistsForThisFrame"), item.Body));

                        AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                        return;
                    }
                }

                //Jerry 2012-05-29 add VerificationBin, check the 2310,2311 authority rules when frame status is just 600
                if (frameStatus == 600 && rules.Rule("2310").ARString == "Y")
                {
                    SIL.AARTO.DAL.Entities.VerificationBin verificationBin = new SIL.AARTO.DAL.Services.VerificationBinService().Find("VeBiRegNo=" + frame.RegNo.Trim()).FirstOrDefault();
                    if (verificationBin != null)
                    {
                        int daysOfCutOff = rules.Rule("2311").ARNumeric;
                        if (((TimeSpan)(DateTime.Now - frame.OffenceDate)).Days <= daysOfCutOff)
                        {
                            //set frame status to 850
                            SIL.AARTO.DAL.Entities.Frame frameEntity = new SIL.AARTO.DAL.Services.FrameService().GetByFrameIntNo(this.frameIntNo);
                            frameEntity.FrameStatus = 850;
                            frameEntity.LastUser = AARTOBase.LastUser; // 2013-07-19 add by Henry
                            new SIL.AARTO.DAL.Services.FrameService().Save(frameEntity);

                            //set current queue status
                            item.IsSuccessful = false;
                            item.Status = QueueItemStatus.Discard;

                            // Oscar 2013-04-07 added message
                            Logger.Info(ResourceHelper.GetResource("QueueStatusWillBeSetToBecauseOf", "Discard", ResourceHelper.GetResource("FrameIsNotReadyForVerificationBin"), item.Body));

                            //push current queue
                            QueueItem pushItem = null;
                            pushItem = new QueueItem()
                            {
                                Body = item.Body,
                                Group = item.Group,
                                ActDate = DateTime.Now.AddHours(72),
                                QueueType = ServiceQueueTypeList.Frame_Generate1stNotice
                            };
                            QueueItemProcessor queueProcessor = new QueueItemProcessor();
                            queueProcessor.Send(pushItem);

                            return;
                        }
                    }
                }

                ////Jerry 2012-05-29 add 860, 850 status
                //if (frame == null || (frame.FrameStatus.Trim() != "600" && frame.FrameStatus.Trim() != "800" && frame.FrameStatus.Trim() != "850" && frame.FrameStatus.Trim() != "860"))
                //{
                //    AARTOBase.DeQueueInvalidItem(ref item);
                //    return;
                //}

                //FilmDetails film = new FilmDB(connectStr).GetFilmDetails(frame.FilmIntNo);
                //if (film == null || !string.IsNullOrEmpty(film.FilmLockUser) || string.IsNullOrEmpty(film.FilmNo))
                //{
                //    //AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.PostPone);
                //    // Oscar 20121112 chaned to Unknown for FilmLockUser
                //    AARTOBase.DeQueueInvalidItem(ref item);
                //    return;
                //}

                //int notIntNo = new NoticeDB(connectStr).GetNoticeByFrame(this.frameIntNo);
                //if (notIntNo > 0)
                //{
                //    AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                //    return;
                //}
            }
            else
            {
                NoticeDB noticeDB = new NoticeDB(connectStr);
                NoticeDetails notice = noticeDB.GetNoticeDetails(Convert.ToInt32(item.Body));
                //if (notice.NoticeStatus != 255)
                // 2013-11-12, Oscar changed for status = 8
                if (notice.NoticeStatus != 255
                    && (!isVIDForPrint || notice.NoticeStatus != 8 ))
                {
                    // Oscar 2013-04-07 added message
                    Logger.Info(ResourceHelper.GetResource("QueueStatusWillBeSetToBecauseOf", "Discard", ResourceHelper.GetResource("StatusIsNotEqualToFirstNoticePosted"), item.Body));

                    AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                    return;
                }
            }

            AARTOBase.BatchCount--;
        }

        #region Oscar 20120406 disabled
        //public sealed override void MainWork(ref List<QueueItem> queueList)
        //{
        //    string msg = string.Empty;
        //    QueueItemProcessor queueProcessor = new QueueItemProcessor();

        //    //string dateStr = DateTime.Now.ToString("yyyy-MM-dd hh-mm-ss");
        //    dateStr = DateTime.Now.ToString("yyyy-MM-dd hh-mm-ss");
        //    dateStr = dateStr.Replace("/", "-");
        //    dateStr = dateStr.Replace(":", "-");
        //    batchFilename = "NBS-" + dateStr;

        //    for (int i = 0; i < queueList.Count; i++)
        //    {
        //        QueueItem item = queueList[i];

        //        if (!item.IsSuccessful)
        //            continue;

        //        try
        //        {
        //            if (strAutCode != item.Group.Trim())
        //            {
        //                strAutCode = item.Group.Trim();
        //                //jerry 2012-04-05 check group value
        //                if (string.IsNullOrWhiteSpace(strAutCode))
        //                {
        //                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
        //                    return;
        //                }

        //                AutIntNo = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode.Trim()).AutIntNo;
        //                AARTORules.GetSingleTon().InitParameter(connectStr, strAutCode);

        //                dateStr = DateTime.Now.ToString("yyyy-MM-dd hh-mm-ss");
        //                dateStr = dateStr.Replace("/", "-");
        //                dateStr = dateStr.Replace(":", "-");
        //                //batchFilename = "NBS-" + dateStr;

        //                DataWashingActived = rules.Rule("9060").ARString.Trim();
        //                IsUseDataWashingAddr = rules.Rule("9110").ARString.Trim();
        //                DataWashingRecycleMonth = rules.Rule("9080").ARNumeric;
        //            }

        //            if (!int.TryParse(item.Body.ToString(), out frameIntNo))
        //            {
        //                continue;
        //            }

        //            //Oscar 20120406 have to reset file name every time!
        //            batchFilename = "NBS-" + dateStr;

        //            //jerry 2012-04-05 change
        //            bool failed = CreatingNotice(frameIntNo, ref msg, item);
        //            //jerry 2012-03-28 if this frame is not eligible for notice generation
        //            //if (failed && item.Status == QueueItemStatus.Discard)
        //            if (failed)
        //            {
        //                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.Discard);
        //                //item.IsSuccessful = false;
        //                //Logger.Info(msg);
        //                return;
        //            }
        //            //jerry 2012-03-29 if FilmLockUser IS NOT NULL
        //            //if (failed && item.Status == QueueItemStatus.UnKnown)
        //            //{
        //            //    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false);
        //            //    return;
        //            //}

        //            if (!failed)
        //                failed = CreateTicket(frameIntNo, ref msg);

        //            if (failed)
        //            {
        //                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
        //            }
        //            else
        //            {
        //                QueueItem pushItem = new QueueItem();

        //                NoticeDB noticeDB = new NoticeDB(connectStr);
        //                NoticeDetails notice = noticeDB.GetNoticeDetails(NotIntNo);

        //                pushItem = new QueueItem();
        //                pushItem.Body = NotIntNo.ToString();
        //                pushItem.Group = strAutCode;
        //                pushItem.QueueType = ServiceQueueTypeList.SearchNameIDUpdate;
        //                queueProcessor.Send(pushItem);

        //                pushItem = new QueueItem();
        //                pushItem.Body = NotIntNo.ToString();
        //                pushItem.Group = strAutCode;
        //                pushItem.ActDate = notice.NotOffenceDate.AddDays(rules.Rule("NotOffenceDate", "NotSummonsBeforeDate").DtRNoOfDays);
        //                pushItem.QueueType = ServiceQueueTypeList.SummonsServicePeriodExpired;
        //                queueProcessor.Send(pushItem);

        //                string errMsg;
        //                int pfnIntNo = AARTOBase.SavePrintFileName(PrintFileNameType.Notice, this.batchFilename, this.AutIntNo, NotIntNo, out errMsg);

        //                if (pfnIntNo <= 0)
        //                {
        //                    AARTOBase.ErrorProcessing(ref item, errMsg, true);
        //                    return;
        //                }

        //                if (!this.pfnList.Contains(pfnIntNo))
        //                {
        //                    this.pfnList.Add(pfnIntNo);

        //                    pushItem = new QueueItem();
        //                    pushItem.Body = pfnIntNo;
        //                    pushItem.Group = strAutCode;
        //                    pushItem.QueueType = ServiceQueueTypeList.Print1stNotice;
        //                    pushItem.ActDate = this.printActionDate;
        //                    queueProcessor.Send(pushItem);
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            AARTOBase.ErrorProcessing(ref item, ex);
        //        }
        //    }
        //}
        #endregion

        bool failed;

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            string msg = string.Empty;
            QueueItemProcessor queueProcessor = new QueueItemProcessor();
            NoticeDB noticeDB = new NoticeDB(connectStr);
            // Nick 20120917 added for report Non-summons registrations
            int frameStatus;
            bool isNonSum = false;
            FrameDB frameDB = new FrameDB(connectStr);
            NoticeMobileDetailService notMobDetServ = new NoticeMobileDetailService();
            NoticeMobileDetail notMobDet;

            if (queueList.Count > 0)
            {
                if (!isHWOForPrint && !isVIDForPrint)
                    this.stampStr = GetNewPrintFileName("NBS", "yyyy-MM-dd HH-mm-ss");
                else
                {
                    var groupStr = queueList[0].Group.Trim();
                    var group = groupStr.Split('|');
                    var autCode = group[0].Trim();
                    //add by linda 2013-02-04
                    if (isVIDForPrint)
                        this.hwoStampStr = GetNewPrintFileName(string.Format("VID-1ST-{0}", autCode.Trim()), "yyyy-MM-dd HH-mm-ss");
                    else
                        this.hwoStampStr = GetNewPrintFileName(string.Format("HWO-1ST-{0}", autCode.Trim()), "yyyy-MM-dd HH-mm-ss");
                }
            }

            // Oscar 2013-04-16 implement deadlock handling
            AARTOBase.InternalTransaction(queueList,
                item =>
                {
                    isCreateTicket = true;
                    failed = false;
                    return true;
                },
                (item, index) =>
                {
                    try
                    {
                        if (!isHWOForPrint && !isVIDForPrint)
                        {
                            this.frameIntNo = Convert.ToInt32(item.Body);
                            this.batchFilename = this.stampStr;

                            // Nick 20120917 added for report Non-summons registrations
                            isNonSum = false;
                            //frameStatus = frameDB.GetFrameDetails(this.frameIntNo).FrameStatus.Trim();
                            // 2015-03-12, Oscar changed (1916, ref 1915)
                            var frame = this.frameService.GetByFrameIntNo(this.frameIntNo);
                            if (frame == null)
                            {
                                AARTOBase.LogProcessing(ResourceHelper.GetResource("QueueStatusWillBeSetToBecauseOf", "Discard", ResourceHelper.GetResource("FrameDoseNotExist"), this.frameIntNo)
                                    , LogType.Info, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                                AARTOBase.DeQueueInvalidItem(ref item, QueueItemStatus.Discard);
                                return false;
                            }
                            frameStatus = frame.FrameStatus;

                            if (frameStatus == 850)
                            {
                                isNonSum = true;
                            }


                            //add by linda 2013-01-17
                            NoticeDetails noticeDetail = noticeDB.GetNoticeByFrameIntNo(this.frameIntNo);
                            bool isNoticeExist = frameStatus == 900 && noticeDetail != null && noticeDetail.NoticeStatus == 5;
                            if (isNoticeExist)
                            {
                                failed = UpdateCorrectedNotice(this.frameIntNo, ref msg, item);
                            }
                            else
                            {
                                //Adam 20130319:The batch file name has been renamed with the defined algorithem in the S.P.:NoticeChargeAddFromFrame_WS !!
                                failed = CreatingNotice(this.frameIntNo, ref msg, item);
                            }
                            if (failed)
                            {
                                //continue;
                                return false;
                            }

                            if (isCreateTicket)
                            {
                                failed = CreateTicket(this.frameIntNo, ref msg);
                                if (failed)
                                {
                                    //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
                                    // Oscar 2013-04-16 implement deadlock handling
                                    AARTOBase.LogProcessing(ResourceHelper.GetResource("Error_CreateFirstNotice", msg), LogType.Error, ServiceOption.Rollback, item);

                                    //continue;
                                    return false;
                                }

                                List<QueueItem> pushItemList = new List<QueueItem>();
                                QueueItem pushItem = null;

                                NoticeDetails notice = noticeDB.GetNoticeDetails(this.NotIntNo);

                                // Oscar 20121008 add for not null authCode
                                string groupStr = item.Group.Trim();
                                string[] group = groupStr.Split('|');
                                string autCode = group[0].Trim();

                                pushItem = new QueueItem()
                                {
                                    Body = NotIntNo,
                                    //Group = strAutCode,
                                    Group = autCode,
                                    QueueType = ServiceQueueTypeList.SearchNameIDUpdate
                                };
                                pushItemList.Add(pushItem);

                                pushItem = new QueueItem()
                                {
                                    Body = NotIntNo,
                                    //Group = strAutCode,
                                    Group = autCode,
                                    ActDate = notice.NotOffenceDate.AddDays(this.daysOfSummonsServicePeriodExpired),
                                    QueueType = ServiceQueueTypeList.SummonsServicePeriodExpired
                                };
                                pushItemList.Add(pushItem);

                                // 2014-07-04, Oscar added
                                if (string.IsNullOrWhiteSpace(this.batchFilename))
                                {
                                    AARTOBase.LogProcessing(ResourceHelper.GetResource("UnableToGetValidPrintFileName"), LogType.Error, ServiceOption.Break, item);
                                    return false;
                                }

                                int pfnIntNo = AARTOBase.SavePrintFileName(PrintFileNameType.Notice, this.batchFilename, this.AutIntNo, NotIntNo, out msg);
                                if (pfnIntNo <= 0)
                                {
                                    AARTOBase.ErrorProcessing(ref item, msg, true);
                                    return false;
                                }

                                //Edge 2013-11-06 update PrintFileName Document count
                                PrintFileName printEntity = new PrintFileNameService().GetByPfnIntNo(pfnIntNo);
                                SIL.AARTO.DAL.Entities.TList<PrintFileNameNotice> printNoticeList = new PrintFileNameNoticeService().GetByPfnIntNo(pfnIntNo);

                                if (printNoticeList.Count > 0)
                                {
                                    string[] notIntNoList = new string[printNoticeList.Count];
                                    for (int i = 0; i < printNoticeList.Count; i++)
                                    {
                                        notIntNoList[i] = printNoticeList[i].NotIntNo.ToString();
                                    }
                                    NoticeQuery query = new NoticeQuery();
                                    query.AppendIn(NoticeColumn.NotIntNo, notIntNoList);
                                    //SIL.AARTO.DAL.Entities.Notice noticeEntity = new NoticeService().Find(query as IFilterParameterCollection).OrderBy(m => m.NotOffenceDate).FirstOrDefault();
                                    // 2015-03-12, Oscar changed (1916, ref 1915)
                                    int total;
                                    var noticeEntity = this.noticeService.Find(query, "NotOffenceDate", 0, 1, out total).FirstOrDefault();
                                    if (notice != null)
                                    {
                                        printEntity.BatDocOldestOffDate = noticeEntity.NotOffenceDate;
                                    }
                                }

                                printEntity.DocumentCount = printNoticeList.Count;
                                printEntity.LastUser = AARTOBase.LastUser;
                                printEntity = new PrintFileNameService().Save(printEntity);

                                if (!this.pfnList.Contains(pfnIntNo))
                                {
                                    this.pfnList.Add(pfnIntNo);

                                    pushItem = new QueueItem()
                                    {
                                        Body = pfnIntNo,
                                        Group = item.Group,
                                        //ActDate = this.printActionDate,
                                        // 2014-08-08, Oscar fixed action date
                                        ActDate = DateTime.Now.AddHours(this.delayHours),
                                        QueueType = ServiceQueueTypeList.Print1stNotice
                                    };
                                    pushItemList.Add(pushItem);
                                }

                                queueProcessor.Send(pushItemList);

                                // Nick 20120917 added for report Non-summons registrations
                                if (isNonSum)
                                {
                                    notMobDet = notMobDetServ.GetByNotIntNo(NotIntNo).FirstOrDefault();
                                    if (notMobDet == null)
                                    {
                                        notMobDet = new NoticeMobileDetail();
                                        notMobDet.NotIntNo = NotIntNo;
                                        notMobDet.IsNonSummonsRegNo = true;
                                        notMobDet.LastUser = AARTOBase.LastUser;
                                        notMobDetServ.Insert(notMobDet);
                                    }
                                    else
                                    {
                                        notMobDet.IsNonSummonsRegNo = true;
                                        notMobDet.LastUser = AARTOBase.LastUser;
                                        notMobDetServ.Update(notMobDet);
                                    }
                                }
                                // Edge 2012-11-23 ChangeOfOwner
                                if (notice.NotSendTo == "P")
                                {
                                    AddNoticeProxy(this.NotIntNo);
                                }
                                // Oscar 2013-04-07 added
                                Logger.Info(ResourceHelper.GetResource("FrameHasCreatedNoticeSuccessfully", item.Body, this.NotIntNo));

                            }
                        }
                        else
                        {
                            failed = SetHWO1stNoticePrintStatus(Convert.ToInt32(item.Body), this.hwoStampStr, ref msg);
                            if (failed)
                            {
                                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_UpdateHWOFirstNotice", msg));
                                AARTOBase.LogProcessing(ResourceHelper.GetResource("Error_UpdateHWOFirstNotice", msg), LogType.Error, ServiceOption.Break, item);
                                //continue;
                                return false;
                            }

                            // Oscar 2013-04-07 added
                            Logger.Info(ResourceHelper.GetResource("HWOFirstNoticeNoticeHasUpdatedPrintStatusSuccessfully", item.Body));
                        }
                        return true;
                    }
                    catch (Exception ex)
                    {
                        AARTOBase.LogProcessing(ex, LogType.Error, ServiceOption.Rollback, item);
                        return false;
                    }
                });

        }

        /// <summary>
        /// jerry 2012-04-05 change method
        /// Jerry 2012-04-16 when success is less than 0 should include FrameIntno 
        /// </summary>
        /// <param name="frameIntNo"></param>
        /// <param name="msg"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        private bool CreatingNotice(int frameIntNo, ref string msg, QueueItem item)
        {
            bool failed = false;
            string errorMsg = "";

            int success = AddNoticeChargeFromFrame(frameIntNo, ref errorMsg);

            // Oscar 2013-03-13 added errorMsg checking
            if (!string.IsNullOrWhiteSpace(errorMsg))
            {
                //Logger.Error(ResourceHelper.GetResource("NoticeChargeAddFromFrame_WSThrowError", frameIntNo, errorMsg));
                // Oscar 2013-04-16 implement deadlock handling
                AARTOBase.LogProcessing(ResourceHelper.GetResource("NoticeChargeAddFromFrame_WSThrowError", frameIntNo, errorMsg), LogType.Error, ServiceOption.Rollback, item);
                failed = true;

                // Osacar 2013-04-07 added set Unknown for DeadLock
                //item.IsSuccessful = false;
                //item.Status = QueueItemStatus.UnKnown;
                return failed;

            }

            if (success == 1)
            {
                errorMsg = ResourceHelper.GetResource("NoDateRule");
                msg = "CreatingNotice: " + errorMsg + " " + DateTime.Now;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg));
                failed = true;
                return failed;
            }

            if (success == 4)
            {
                errorMsg = ResourceHelper.GetResource("NoticeExpired");
                msg = "CreatingNotice: " + errorMsg + " " + DateTime.Now;
                item.IsSuccessful = false;
                item.Status = QueueItemStatus.UnKnown;
                Logger.Info(msg);
                failed = true;
            }
            else if (success == 8)
            {
                errorMsg = string.Format(ResourceHelper.GetResource("NoticeNoChargeRecords"), errorMsg);
                msg = "CreatingNotice: " + errorMsg + " " + DateTime.Now;
                //item.Status = QueueItemStatus.PostPone;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
                failed = true;
            }
            else if (success == -1)
            {
                //msg = "CreatingNotice: + errorMsg + " " + DateTime.Now;
                msg = "CreatingNotice: (frameIntNo:" + frameIntNo + ") " + errorMsg + " " + DateTime.Now;
                //item.Status = QueueItemStatus.PostPone;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
                failed = true;
            }
            else if (success == -2)
            {
                //msg = ResourceHelper.GetResource("DuplicateNumberPlates");
                msg = string.Format(ResourceHelper.GetResource("DuplicateNumberPlates"), frameIntNo);
                //item.Status = QueueItemStatus.Discard;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.Discard);
                //failed = true;
                isCreateTicket = false;
            }
            else if (success == -3)
            {
                //msg = ResourceHelper.GetResource("NoticeAlreadyExists");
                msg = string.Format(ResourceHelper.GetResource("NoticeAlreadyExists"), frameIntNo);
                item.Status = QueueItemStatus.Discard;
                item.IsSuccessful = false;
                Logger.Info(msg);
                //failed = true;
                isCreateTicket = false;
            }
            else if (success == -4)
            {
                //msg = ResourceHelper.GetResource("NoFineAmount");
                msg = string.Format(ResourceHelper.GetResource("NoFineAmount"), frameIntNo);
                //item.Status = QueueItemStatus.PostPone;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
                failed = true;
            }
            else if (success == -5)
            {
                msg = ResourceHelper.GetResource("InsertNoticeFailed", frameIntNo);
                //item.Status = QueueItemStatus.PostPone;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg), false, QueueItemStatus.PostPone);
                failed = true;
            }
            //jerry 2012-03-28 add
            else if (success == -6)
            {
                msg = ResourceHelper.GetResource("NoticeNotEligible", frameIntNo);
                item.Status = QueueItemStatus.Discard;
                item.IsSuccessful = false;
                Logger.Info(msg);
                failed = true;
            }
            //jerry 2012-03-29 add
            else if (success == -7)
            {
                msg = ResourceHelper.GetResource("NoticeFilmLocked", frameIntNo);
                item.Status = QueueItemStatus.UnKnown;
                item.IsSuccessful = false;
                Logger.Info(msg);
                failed = true;
            }
            //linda 2013-01-17 add
            else if (success == -8)
            {
                msg = ResourceHelper.GetResource("UpdateNoticeChargeStates",frameIntNo);
                Logger.Info(msg);
                isCreateTicket = false;
            }

            return failed;
        }
        /// <summary>
        /// linda 2013-01-17 add method
        /// linda 2013-01-17 when success is less than 0 should include FrameIntno 
        /// </summary>
        /// <param name="frameIntNo"></param>
        /// <param name="msg"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        private bool UpdateCorrectedNotice(int frameIntNo, ref string msg, QueueItem item)
        {
            bool failed = false;
            string errorMsg = "";

            int success = NoticeUpdateFixedAfterCorrection(frameIntNo, ref errorMsg);

            // Oscar 2013-03-13 added errorMsg checking
            if (success < 0 || !string.IsNullOrWhiteSpace(errorMsg))
            {
                errorMsg = ResourceHelper.GetResource("ModifyNoticeStatus")
                    + (!string.IsNullOrWhiteSpace(errorMsg) ? " | " + errorMsg : "");
                msg = "Modify notice status: " + errorMsg + " " + DateTime.Now;
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("Error_CreateFirstNotice", msg));
                failed = true;
            }
            return failed;
        }

        private const int STATUS_PROSECUTE_FIRST = 600;
        private const int STATUS_PROSECUTE_LAST = 800;
        private const int STATUS_LOADED = 5;
        private const int STATUS_NO_FINE = 6;
        private const int STATUS_EXPIRED = 921;
        private const int STATUS_TICKETS = 10;
        private const int STATUS_NO_AOG = 500;
        private const int STATUS_NOTICE = 900;
        private const string CTYPE = "CAM";
        private const string NT_CODE = "6";
        private const int STATUS_AMOUNT_ADDED = 7;

        private int AddNoticeChargeFromFrame(int frameIntNo, ref string errMsg)
        {
            DBHelper db = new DBHelper(connectStr);
            SqlParameter[] paras = new SqlParameter[17];
            paras[0] = new SqlParameter("@AutIntNo", AutIntNo);
            paras[1] = new SqlParameter("@FrameIntNo", frameIntNo);
            paras[2] = new SqlParameter("@StatusLoaded", STATUS_LOADED);
            paras[3] = new SqlParameter("@StatusNoFine", STATUS_NO_FINE);
            paras[4] = new SqlParameter("@StatusExpired", STATUS_EXPIRED);
            paras[5] = new SqlParameter("@StatusNotice", STATUS_NOTICE);
            paras[6] = new SqlParameter("@StatusAmountAdded", STATUS_AMOUNT_ADDED);
            paras[7] = new SqlParameter("@PrintFileName", this.batchFilename) { Direction = ParameterDirection.InputOutput, Size = 100 };
            paras[8] = new SqlParameter("@CType", CTYPE);
            //paras[9] = new SqlParameter("@LastUser", Process.GetCurrentProcess().ProcessName);
            //Oscar 20120405 change to AARTOBase.lastUser
            paras[9] = new SqlParameter("@LastUser", AARTOBase.LastUser);
            //paras[10] = new SqlParameter("@Success", 0);
            paras[10] = new SqlParameter("@NatisLast", rules.Rule("0400").ARString == "Y");
            paras[11] = new SqlParameter("@NoOfDaysIssue", rules.Rule("NotOffenceDate", "NotIssue1stNoticeDate").DtRNoOfDays);
            paras[12] = new SqlParameter("@NoOfDaysPayment", rules.Rule("NotOffenceDate", "NotPaymentDate").DtRNoOfDays);
            paras[13] = new SqlParameter("@DataWashingActived", DataWashingActived);
            paras[14] = new SqlParameter("@IsUseDataWashingAddr", IsUseDataWashingAddr);
            paras[15] = new SqlParameter("@DataWashingRecycleMonth", DataWashingRecycleMonth);
            paras[16] = new SqlParameter("@SeparatePrintFileNameForS35", SeperatePrintFileNameForS35);
            //paras[10].Direction = ParameterDirection.Output;

            //db.ExecuteNonQuery("NoticeChargeAddFromFrame_WS", paras, out errMsg);
            object tempSuccess = db.ExecuteScalar("NoticeChargeAddFromFrame_WS", paras, out errMsg);

            // 2014-07-04, Oscar added
            this.batchFilename = null;

            // Oscar 20120322 add print file name
            if (paras[7].Value != null && !string.IsNullOrWhiteSpace(paras[7].Value.ToString()))
                this.batchFilename = paras[7].Value.ToString();

            int success = Convert.ToInt32(tempSuccess);
            return success;
        }

        //add by linda 2013-01-17
        // Jake 2013-12-09 added new parameter SeperatePrintFileNameForS35
        private int NoticeUpdateFixedAfterCorrection(int frameIntNo, ref string errMsg)
        {
            DBHelper db = new DBHelper(connectStr);
            SqlParameter[] paras = new SqlParameter[9];
            
            paras[0] = new SqlParameter("@AutIntNo", AutIntNo);
            paras[1] = new SqlParameter("@FrameIntNo", frameIntNo);
            paras[2] = new SqlParameter("@StatusLoaded", STATUS_LOADED);
            paras[3] = new SqlParameter("@LastUser", AARTOBase.LastUser);
            paras[4] = new SqlParameter("@CType", CTYPE);
            paras[5] = new SqlParameter("@StatusAmountAdded", STATUS_AMOUNT_ADDED);
            paras[6] = new SqlParameter("@PrintFileName", this.batchFilename) { Direction = ParameterDirection.InputOutput, Size = 100 };
            paras[7] = new SqlParameter("@Success",SqlDbType.Int) { Direction = ParameterDirection.Output };
            paras[8] = new SqlParameter("@SeparatePrintFileNameForS35", SeperatePrintFileNameForS35);
            object tempSuccess = db.ExecuteScalar("NoticeUpdateFixedAfterCorrection_WS", paras, out errMsg);

            // 2014-07-04, Oscar added
            this.batchFilename = null;

            if (paras[6].Value != null && !string.IsNullOrWhiteSpace(paras[6].Value.ToString()))
                this.batchFilename = paras[6].Value.ToString();
            int success = Convert.ToInt32( paras[7].Value);
            return success;
        }


        private bool CreateTicket(int frameIntNo, ref string msg)
        {
            bool failed = false;

            NoticeNumbers notice = new NoticeNumbers(connectStr);
            string error = "";
            ImageProcesses imgProc = new ImageProcesses(connectStr);
            ScanImageDB scan = new ScanImageDB(connectStr);

            //get all charges 
            NoticeDB not = new NoticeDB(connectStr);
            ChargeDB charge = new ChargeDB(connectStr);

            NotIntNo = not.GetNoticeByFrame(frameIntNo);
            //TicketNo ticket = notice.GenerateNoticeNumber(Process.GetCurrentProcess().ProcessName, NT_CODE, AutIntNo);
            //Oscar 20120405 change to AARTOBase.lastUser
            TicketNo ticket = notice.GenerateNoticeNumber(AARTOBase.LastUser, NT_CODE, AutIntNo);
            ticket.NotIntNo = NotIntNo;

            if (ticket.NotTicketNo.Equals("0")
                // 2015-02-26, Oscar added (1868, ref 1866)
                || ticket.ErrorCode < 0
                || !string.IsNullOrWhiteSpace(ticket.Error))
            {
                // 2015-02-26, Oscar added. (1868, ref 1866)
                var errorMsg = ticket.Error;
                switch (ticket.ErrorCode)
                {
                    case -1:
                        errorMsg = ResourceHelper.GetResource("InvalidValueOfAuthorityRule", "9300");
                        break;
                    case -2:
                        errorMsg = ResourceHelper.GetResource("GettingNoticeTypeFailed");
                        break;
                    case -3:
                        errorMsg = ResourceHelper.GetResource("AddingNoticePrefixFailed");
                        break;
                    case -4:
                        errorMsg = ResourceHelper.GetResource("ResettingTranNumberFailed");
                        break;
                    case -5:
                        errorMsg = ResourceHelper.GetResource("UpdatingNoticePrefixFailed");
                        break;
                    case -6:
                        errorMsg = ResourceHelper.GetResource("GettingAuthorityFailed");
                        break;
                    case -7:
                        errorMsg = ResourceHelper.GetResource("GettingTranNumberFailed");
                        break;
                }

                error = ResourceHelper.GetResource("ErrorGeneratingNoticeNumber");
                //msg = "CreateTickets: " + error + " " + DateTime.Now.ToString();
                // 2015-02-26, Oscar changed. (1868, ref 1866)
                msg = string.Format("CreateTickets:  {0}; {1}", error, errorMsg);
                failed = true;
                return failed;
            }

            char cTemp = Convert.ToChar("0");

            if (ticket.NPrefix.Equals("0"))
            {
                error = ResourceHelper.GetResource("ErrorGeneratingPayNumber");
                msg = "CreateTicket: " + error + " " + DateTime.Now.ToString();
                failed = true;
                return failed;
            }

            //update each charge and status
            //int updNotIntNo = not.UpdateNoticeTicketNo(ticket, STATUS_TICKETS, STATUS_AMOUNT_ADDED, STATUS_NO_AOG, Process.GetCurrentProcess().ProcessName);
            //Oscar 20120405 change to AARTOBase.lastUser
            int updNotIntNo = not.UpdateNoticeTicketNo(ticket, STATUS_TICKETS, STATUS_AMOUNT_ADDED, STATUS_NO_AOG, AARTOBase.LastUser, out msg);

            // Oscar 2013-03-15 added error message
            if (!string.IsNullOrWhiteSpace(msg))
            {
                msg = ResourceHelper.GetResource("UpdateNoticeTicketNoThrowError", updNotIntNo, ticket.NotIntNo, ticket.NotTicketNo, msg);
                failed = true;
            }
            else
            //if (updNotIntNo < 0)
            if (updNotIntNo <= 0)   //Oscar 20120704 change to <=0
            {
                failed = true;

                // Oscar 2013-04-07 added
                if (updNotIntNo == 0)
                {
                    msg = ResourceHelper.GetResource("UpdateNoticeTicketNoThrowError", updNotIntNo, ticket.NotIntNo, ticket.NotTicketNo, ResourceHelper.GetResource("TicketNoHasAlreadyExisted"));
                }
                else

                // Oscar 2013-03-15 added error message
                switch (updNotIntNo)
                {
                    case -1:
                        msg = ResourceHelper.GetResource("UpdateNoticeTicketNoThrowError", updNotIntNo, ticket.NotIntNo, ticket.NotTicketNo, ResourceHelper.GetResource("FailUpdateNotice"));
                        break;
                    case -2:
                        msg = ResourceHelper.GetResource("UpdateNoticeTicketNoThrowError", updNotIntNo, ticket.NotIntNo, ticket.NotTicketNo, ResourceHelper.GetResource("FailUpdateCharge"));
                        break;
                }
            }

            return failed;
        }

        private string GetNewPrintFileName(string prefix, string dateFormat)
        {
            this.stamp = DateTime.Now;
            //if (this.stamp.Subtract(this.lastStamp).Seconds <= 0)
            if (this.stamp < this.lastStamp.AddSeconds(1))
                this.stamp = this.lastStamp.AddSeconds(1);
            this.lastStamp = this.stamp;
            return string.Format("{0}-{1}", prefix, this.stamp.ToString(dateFormat));
        }

        private bool SetHWO1stNoticePrintStatus(int notIntNo, string printFileName, ref string msg)
        {
            bool failed = false;
            int success = SetHWO1stNoticePrintStatusAndFilename(notIntNo, printFileName, ref msg);

            // Oscar 2013-04-07 added
            if (!string.IsNullOrEmpty(msg))
            {
                msg = string.Format("Notice: {0} - {1}", notIntNo, msg);
                failed = true;
            }
            else

            if (success == -1)
            {
                msg = ResourceHelper.GetResource("UpdateHWO1stNoticeFailed", notIntNo);
                failed = true;
            }
            else if (success == -2)
            {
                msg = ResourceHelper.GetResource("InsertPrintFileNameFailed", notIntNo);
                failed = true;
            }
            else if (success == -3)
            {
                msg = ResourceHelper.GetResource("InsertPrintFileName_NoticeFailed", notIntNo);
                failed = true;
            }
            // 2013-11-12, Oscar added
            else if (success == -4)
            {
                msg = ResourceHelper.GetResource("UpdatingChargeStatusForVideoFailed", notIntNo);
                failed = true;
            }
            return failed;
        }

        private int SetHWO1stNoticePrintStatusAndFilename(int notIntNo, string printFileName, ref string errMsg)
        {
            DBHelper db = new DBHelper(connectStr);
            SqlParameter[] paras = new SqlParameter[6];
            paras[0] = new SqlParameter("@NotIntNo", notIntNo);
            paras[1] = new SqlParameter("@LastUser", AARTOBase.LastUser);
            //paras[2] = new SqlParameter("@Success", 0);
            paras[2] = new SqlParameter("@PrintFileName", printFileName);
            // Iris 20140312 added Data washing for Video.
            paras[3] = new SqlParameter("@DataWashingActived", this.DataWashingActived);
            paras[4] = new SqlParameter("@IsUseDataWashingAddr", this.IsUseDataWashingAddr);
            paras[5] = new SqlParameter("@DataWashingRecycleMonth", this.DataWashingRecycleMonth);
           
            //paras[2].Direction = ParameterDirection.Output;
            //db.ExecuteNonQuery("SetHWO1stNoticePrintStatusAndFilename_WS", paras, out errMsg);

            //int success = Convert.ToInt32(paras[2].Value);
            var obj = db.ExecuteScalar("SetHWO1stNoticePrintStatusAndFilename_WS", paras, out errMsg);
            int success = 0;
            if (obj != null)
                int.TryParse(obj.ToString(), out success);
            return success;
        }

        private int AddNoticeProxy(int notIntNo)
        {
            DBHelper db = new DBHelper(connectStr);
            SqlParameter[] paras = new SqlParameter[2];
            paras[0] = new SqlParameter("@NotIntNo", notIntNo);
            paras[1] = new SqlParameter("@LastUser", AARTOBase.LastUser);

            string errMsg;
            try
            {
                return db.ExecuteNonQuery("COONoticeProxyAdd", paras, out errMsg);
            }
            catch(Exception ex)
            {
                Logger.Error(ex);
                return -1;
            }
        }

        //DateTime printActionDate = DateTime.Now;
        // 2014-08-08, Oscar fixed action date
        int delayHours = 2;
        private void SetServiceParameters()
        {
            //int delayHours = 0;
            if (ServiceParameters != null
                && ServiceParameters.ContainsKey("DelayActionDate_InHours")
                && !string.IsNullOrEmpty(ServiceParameters["DelayActionDate_InHours"]))
                this.delayHours = Convert.ToInt32(ServiceParameters["DelayActionDate_InHours"]);
            //this.printActionDate = DateTime.Now.AddHours(delayHours);
        }
    }
}

