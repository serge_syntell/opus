﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Frame_Generate1stNotice
{
    partial class Frame_Generate1stNotice : ServiceHost
    {
        public Frame_Generate1stNotice()
            : base("", new Guid("AEAB0ABC-3C8A-4E54-8658-27472144318A"), new Frame_Generate1stNoticeService())
        {
            InitializeComponent();
        }
    }
}
