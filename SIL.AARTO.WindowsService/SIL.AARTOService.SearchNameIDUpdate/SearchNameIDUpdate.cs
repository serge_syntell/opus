﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;


namespace SIL.AARTOService.SearchNameIDUpdate
{
    partial class SearchNameIDUpdate : ServiceHost
    {

        public SearchNameIDUpdate()
            : base("", new Guid("ED8B7150-8E51-464E-A5C9-55921228F150"), new SearchNameIDUpdateClientService())
        {
            InitializeComponent();
        }
    }
}
