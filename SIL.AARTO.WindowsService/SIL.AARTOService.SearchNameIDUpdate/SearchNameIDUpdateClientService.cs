﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using SIL.QueueLibrary;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.ServiceQueueLibrary.DAL.Entities;

namespace SIL.AARTOService.SearchNameIDUpdate
{
    public class SearchNameIDUpdateClientService : ServiceDataProcessViaQueue
    {
        public SearchNameIDUpdateClientService()
            : base("", "", new Guid("267CAF11-725D-4090-A3DE-6CC1CDCE4F25"), ServiceQueueTypeList.SearchNameIDUpdate)
        {
            this._serviceHelper = new AARTOServiceBase(this);
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }

        public sealed override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && ServiceUtility.IsNumeric(body))
                item.IsSuccessful = true;
            else
                item.IsSuccessful = false;
        }

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            int notIntNo, success;
            string msg = string.Empty;
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                try
                {
                    if (item.IsSuccessful)
                    {
                        item.Status = QueueItemStatus.Discard;  //this case is always discarded
                        notIntNo = Convert.ToInt32(item.Body);

                        success = UpdateSearchID(notIntNo, out msg);
                        if (success <= 0)
                        {
                            AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("SearchNameIDUpdateHasError") + msg, true);
                            return;
                        }

                        success = UpdateSearchSurName(notIntNo, out msg);
                        if (success <= 0)
                        {
                            AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("SearchNameIDUpdateHasError") + msg, true);
                            return;
                        }

                        success = UpdateSearchRegno(notIntNo, out msg);
                        if (success <= 0)
                        {
                            AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("SearchRegnoUpdateHasError") + msg, true);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
                queueList[i] = item;
            }
        }

        private int UpdateSearchID(int notIntNo, out string message)
        {
            DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));
            SqlParameter[] paras = new SqlParameter[1];
            paras[0] = new SqlParameter("@notIntNo", notIntNo);
            object obj = db.ExecuteScalar("InsertSearchID_WS", paras, out message);
            int result;
            if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(message))
                result = Convert.ToInt32(obj);
            else
                result = 0;
            switch (result)
            {
                case -1:
                    message = ResourceHelper.GetResource("InsertingOwnerID");
                    break;
                case -2:
                    message = ResourceHelper.GetResource("InsertingDriverID");
                    break;
                case -3:
                    message = ResourceHelper.GetResource("InsertingProxyID");
                    break;
                case -4:
                    message = ResourceHelper.GetResource("InsertingAccusedID");
                    break;
            }
            return result;
        }

        private int UpdateSearchSurName(int notIntNo, out string message)
        {
            DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));
            SqlParameter[] paras = new SqlParameter[1];
            paras[0] = new SqlParameter("@notIntNo", notIntNo);
            object obj = db.ExecuteScalar("InsertSearchSurName_WS", paras, out message);
            int result;
            if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(message))
                result = Convert.ToInt32(obj);
            else
                result = 0;
            switch (result)
            {
                case -1:
                    message = ResourceHelper.GetResource("InsertingOwnerSurnameInitials");
                    break;
                case -2:
                    message = ResourceHelper.GetResource("InsertingDriverSurnameInitials");
                    break;
                case -3:
                    message = ResourceHelper.GetResource("InsertingProxySurnameInitials");
                    break;
                case -4:
                    message = ResourceHelper.GetResource("InsertingAccusedSurnameInitials");
                    break;
            }
            return result;
        }

        private int UpdateSearchRegno(int notIntNo, out string message)
        {
            DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));
            SqlParameter[] paras = new SqlParameter[1];
            paras[0] = new SqlParameter("@notIntNo", notIntNo);
            object obj = db.ExecuteScalar("InsertSearchRegno", paras, out message);
            int result;
            if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(message))
                result = Convert.ToInt32(obj);
            else
                result = 0;

            if(result == -1)
                message = ResourceHelper.GetResource("InsertingRegno");

            return result;
        }
    }
}

