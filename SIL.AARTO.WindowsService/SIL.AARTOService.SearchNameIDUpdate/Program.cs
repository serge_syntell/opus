﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.SearchNameIDUpdate
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.SearchNameIDUpdate"
                ,
                DisplayName = "SIL.AARTOService.SearchNameIDUpdate"
                ,
                Description = "SIL AARTOService SearchNameIDUpdate"
            };

            ProgramRun.InitializeService(new SearchNameIDUpdate(), serviceDescriptor, args);
        }
    }
}
