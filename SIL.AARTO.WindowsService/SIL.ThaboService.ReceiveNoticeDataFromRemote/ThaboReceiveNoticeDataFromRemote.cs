﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.ThaboService.ReceiveNoticeDataFromRemote
{
    partial class ReceiveNoticeDataFromRemote : ServiceHost
    {
        public ReceiveNoticeDataFromRemote()
            : base("", new Guid("FF4F8670-AB8D-46F8-88C5-085FA2E4120B"), new ReceiveNoticeDataFromRemoteService())
        {
            InitializeComponent();
        }
    }
}
