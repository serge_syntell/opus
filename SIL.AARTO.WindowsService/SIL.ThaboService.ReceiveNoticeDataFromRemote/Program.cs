﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.ThaboService.ReceiveNoticeDataFromRemote
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.ThaboService.ReceiveNoticeDataFromRemote"
                ,
                DisplayName = "SIL.ThaboService.ReceiveNoticeDataFromRemote"
                ,
                Description = "SIL.ThaboService.ReceiveNoticeDataFromRemote"
            };

            ProgramRun.InitializeService(new ReceiveNoticeDataFromRemote(), serviceDescriptor, args);
        }
    }
}
