﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;
using SIL.QueueLibrary;
using SIL.AARTOService.Library;
using SIL.ServiceQueueLibrary.DAL.Entities;
using Stalberg.Indaba;
using System.IO;
using SIL.ServiceBase;
using SIL.AARTOService.Resource;
using Stalberg.TMS_TPExInt.Objects;
using System.Diagnostics;
using System.Data.SqlClient;
using SIL.AARTOService.DAL;
using System.Data;
using Stalberg.TMS;

namespace SIL.ThaboService.ReceiveNoticeDataFromRemote
{
    public class ReceiveNoticeDataFromRemoteService : ClientService
    {
        public ReceiveNoticeDataFromRemoteService()
            : base("", "", new Guid("73CD449C-19A3-461F-B2D2-00CB1B80ED1A"))
        {
            this._serviceHelper = new AARTOServiceBase(this);
            thaboConnectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.Thabo, ServiceConnectionTypeList.DB);
            indabaConnectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.CentralIndaba, ServiceConnectionTypeList.DB);
            responseFilePath = AARTOBase.GetConnectionString(ServiceConnectionNameList.ResponseFilePath, ServiceConnectionTypeList.UNC);
            ServiceUtility.InitializeNetTier(thaboConnectStr);

            AARTOBase.OnServiceStarting = () =>
            {
                if (string.IsNullOrEmpty(responseFilePath))
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoParam"), "ResponseFilePath"), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }

                var resFileDir = new DirectoryInfo(responseFilePath);
                if (!resFileDir.Exists)
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoResponseFilePath"), resFileDir.FullName), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }

                InitIndaba();
            };

            AARTOBase.OnServiceSleeping = () =>
            {
                DisposeIndaba();
            };
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        string thaboConnectStr = string.Empty;
        string indabaConnectStr = string.Empty;
        string responseFilePath = string.Empty;
        string LastBatchNumber = string.Empty;
        int increment;

        ICommunicate indaba;

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            AARTOBase.LogProcessing(ResourceHelper.GetResource("GetFineExchangeFiles", AARTOBase.LastUser), LogType.Info);
            List<Stream> files = GetFineExchangeDataFiles();
            // Checked for files ready (null > return)
            if (files == null)
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("NoValidFile", AARTOBase.LastUser), LogType.Warning);
                return;
            }
            AARTOBase.LogProcessing(ResourceHelper.GetResource("FinishedProcess", AARTOBase.LastUser), LogType.Info);

            //Jerry 2014-05-23 add
            this.MustSleep = true;
        }

        private List<Stream> GetFineExchangeDataFiles()
        {
            var files = new List<Stream>();

            IndabaFileInfo[] fileInfo = indaba.GetFileInformation(FineExchangeDataFile.EXTENSION);
            if (fileInfo == null || fileInfo.Length == 0)
                return files;

            var timer = new Stopwatch();
            foreach (IndabaFileInfo file in fileInfo)
            {
                FineExchangeDataFile fine = null;
                try
                {
                    timer.Reset();
                    timer.Start();
                    bool isFullyProcessed = false;

                    Stream stream = indaba.Peek(file);
                    timer.Stop();
                    //Logger.Write(string.Format("Peeked file File: {0}, in {1}", file.Name, timer.Elapsed), "General", (int)TraceEventType.Information);
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("IndabaPeekFile"), file.Name, timer.Elapsed), LogType.Info);
                    timer.Reset();

                    timer.Start();
                    fine = ProcessFineDataFiles(stream);
                    timer.Stop();
                    //Logger.Write(string.Format("Processing fine data file for: {0}, in {1}", file.Name, timer.Elapsed), "General", (int)TraceEventType.Information);
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ProcessFineFile"), file.Name, timer.Elapsed), LogType.Info);
                    timer.Reset();

                    if (fine == null || fine.Authority == null)
                    {
                        //Logger.Write(string.Format("There was no data for File: {0}", file.Name), "General", (int)TraceEventType.Warning);
                        AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("NoDataForFile"), file.Name), LogType.Warning);
                        continue;
                    }

                    // Update the Thabo database with the fine data
                    timer.Start();
                    ProcessFinesIntoDatabase(fine, ref isFullyProcessed);
                    timer.Stop();
                    //Logger.Write(string.Format("Processing fines into database for File: {0}, in {1}", file.Name, timer.Elapsed), "General", (int)TraceEventType.Information);
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("ProcessFineIntoDB", file.Name, timer.Elapsed), LogType.Info);
                    timer.Reset();

                    timer.Start();

                    //Set FileName
                    // Get the authority's machine name 
                    GetMachineName(fine);
                    if (string.IsNullOrEmpty(fine.MachineName))
                    {
                        //Logger.Write(string.Format("The machine name for auth code {0} could not be found.", fine.MachineName), "General", (int)TraceEventType.Warning);
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("CannotFindMachine", fine.MachineName), LogType.Warning);
                        continue;
                    }

                    //Program.indaba.Destination = fine.MachineName;
                    //Added by Oscar 2011/09/29
                    //In order to keep the Indaba interface unchanged,we decided to pass client ID as the parameter to take the place of IndabaDestination.
                    //Code changes will happen in Indaba client to look up  the destination site based on  the passed client ID.
                    indaba.Destination = fine.Authority.AuthorityCode;

                    // Create the response file
                    string strfileName = responseFilePath + fine.Authority.AuthorityCode + "_" +
                                         DateTime.Now.ToString(ResponseFile.DATE_FORMAT) +
                                         ResponseFile.RESPONSE_FILE_EXTENSION;

                    //Logger.Write(string.Format("Creating response file for File: {0}", file.Name), "General", (int)TraceEventType.Information);
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("CreateRespFile", file.Name), LogType.Info);

                    var responseFile = new ResponseFile(fine, strfileName);
                    if (responseFile.CreateFile())
                    {
                        Guid id = indaba.Enqueue(strfileName);

                        // Check it was enqueued successfully
                        if (!id.Equals(Guid.Empty))
                        {
                            // Delete the file
                            File.Delete(strfileName);
                        }
                        else
                        {
                            timer.Stop();
                            //Logger.Write(string.Format("Response file {0} enqueue failed for ({1}), in {2}.", strfileName, fine.MachineName, timer.Elapsed), "General", (int)TraceEventType.Warning);
                            AARTOBase.LogProcessing(ResourceHelper.GetResource("EnqueueFailed", strfileName, fine.MachineName, timer.Elapsed), LogType.Warning);
                            timer.Reset();
                        }
                    }

                    if (isFullyProcessed)
                    {
                        indaba.Dequeue(file);
                        //Logger.Write(string.Format("De-queued File: {0}", file.Name), "General", (int)TraceEventType.Information);
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("IndabaDeQueue", file.Name), LogType.Info);
                    }
                    else
                    {
                        //Logger.Write(
                        //    string.Format(
                        //        "A program was detected updating the Thabo database with fine data for LA: {0}",
                        //        fine.Authority), "General", (int)TraceEventType.Warning);
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("DetectUpdating", fine.Authority), LogType.Warning);
                        continue;
                    }
                }
                catch (Exception ex)
                {
                    //Logger.Write(string.Format("There was a general error trying to send an IRF files to: {0}.\r\n{1}", fine == null ? null : fine.Authority, ex), "General", (int)TraceEventType.Warning);
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("ErrSendAnIRF", fine == null ? null : fine.Authority, ex), LogType.Warning);
                }
            }

            return files;
        }

        private void ProcessFinesIntoDatabase(FineExchangeDataFile fine, ref bool isFullyProcessed)
        {
            try
            {
                string errorMessage = "";
                UpdateData(fine, ref isFullyProcessed, ref errorMessage);

                if (isFullyProcessed)
                {
                    //Logger.Write(string.Format("\tImported data for {0} fines for {1}", fine.NoticeCount, fine.Authority), "General", (int)TraceEventType.Information);
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ImportDataForAuthority"), fine.NoticeCount, fine.Authority), LogType.Info);
                }
                else
                {
                    //Logger.Write(string.Format("\tThe process fines routine returned an error: {0} ", errorMessage), "General", (int)TraceEventType.Warning);
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ProcessFinesReturenErr"), errorMessage), LogType.Warning);
                }
            }
            catch (Exception ex)
            {
                //Logger.Write(string.Format("Error in ProcessFinesIntoDatabase: {0} ", ex), "General", (int)TraceEventType.Error);
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ProcessFinesHasErr"), ex), LogType.Error, ServiceOption.BreakAndStop);
            }
        }

        /// <summary>
        /// Updates all the data in the Authority.
        /// </summary>
        /// <param name="data">The data to update.</param>
        /// <param name="isFullyProcessed">if set to <c>true</c> [is fully processed].</param>
        /// <param name="errorMessage">The error message.</param>
        //public void UpdateData(FineExchangeDataFile data, ref bool isFullyProcessed)
        public void UpdateData(FineExchangeDataFile data, ref bool isFullyProcessed, ref string errorMessage)
        {
            // Check the Authority
            bool failed = this.UpdateAuthority(data, ref errorMessage);
            if (failed)
            {
                //mrs 20090115 by ref returns any system error messages
                isFullyProcessed = false;
                return;
            }

            // mrs 20080714 need some error checking here
            // mrs 20090115 added the errorMessage
            switch (data.Authority.ID) // there was a major problem finding the authority
            {
                case 0:
                    //errorMessage = "Unknown error in stored procedure";
                    errorMessage = ResourceHelper.GetResource("UnknownErr");
                    isFullyProcessed = false;
                    return;
                case -1:
                    //errorMessage = "problem with insert in region table";
                    errorMessage = ResourceHelper.GetResource("ProblemInsertRegion");
                    isFullyProcessed = false;
                    return;
                case -2:
                    //errorMessage = "problem with insert in metro table";
                    errorMessage = ResourceHelper.GetResource("ProblemInsertMetro");
                    isFullyProcessed = false;
                    return;
                case -3:
                    //errorMessage = "problem with update in authority table";
                    errorMessage = ResourceHelper.GetResource("ProblemUpdateAuth");
                    isFullyProcessed = false;
                    return;
                case -4:
                    //errorMessage = "problem with insert in authority table";
                    errorMessage = ResourceHelper.GetResource("ProblemInsertAuth");
                    isFullyProcessed = false;
                    return;
                case -5:
                    //errorMessage = "problem with insert in authorityEasyPay table";
                    errorMessage = ResourceHelper.GetResource("ProblemInsertAutEasyPay");
                    isFullyProcessed = false;
                    return;
                default:
                    // we likes it
                    isFullyProcessed = true;
                    break;
            } //end of switch

            // Check the Notice
            failed = this.UpdateNotice(data);
            if (failed)
            {
                isFullyProcessed = false;
                return;
            }

            // Add the EasyPay transaction
            failed = this.UpdateEasyPay(data);
            if (failed)
            {
                isFullyProcessed = false;
                return;
            }

            //it got all the way through without errors
            isFullyProcessed = true;
        }

        private bool UpdateEasyPay(FineExchangeDataFile data)
        {
            bool failed = false;
            string errMsg = string.Empty;

            DBHelper db = new DBHelper(this.thaboConnectStr);
            foreach (EasyPayTransaction tran in data.Authority.EasyPayTransactions)
            {
                SqlParameter[] paras = new SqlParameter[6];
                paras[0] = new SqlParameter("@NotIntNo", tran.NoticeID);
                paras[1] = new SqlParameter("@EasyPayNo", tran.EasyPayNumber);
                paras[2] = new SqlParameter("@ExpiryDate", tran.ExpiryDate);
                paras[3] = new SqlParameter("@EPAction", tran.EasyPayAction);
                paras[4] = new SqlParameter("@Amount", tran.Amount);
                paras[5] = new SqlParameter("@LastUser", AARTOBase.LastUser);
                    
                db.ExecuteNonQuery("ImportEasyPayTran", paras, out errMsg);
                if (!string.IsNullOrEmpty(errMsg))
                {
                    //AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("UpdateEasyPayErr"), ex.Message, ex.StackTrace), LogType.Error, ServiceOption.BreakAndStop);
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("UpdateEasyPayErr"), errMsg), LogType.Error, ServiceOption.BreakAndStop);
                    failed = true;
                    break;
                }
            }
            
            return failed;
        }

        private bool UpdateNotice(FineExchangeDataFile data)
        {
            int notIntNo = 0;
            int oldNotIntNo = 0;
            bool failed = false;
            try
            {
                //FT Set up Batch Number
                string strBatchNumber = "TBO_" + DateTime.Now.ToString("yyyyMMdd_HHmm");
                if (LastBatchNumber == strBatchNumber)
                {
                    increment++;
                }
                else
                {
                    LastBatchNumber = strBatchNumber;
                }
                data.BatchNumber = LastBatchNumber + increment.ToString().PadLeft(2, '0');

                DBHelper db = new DBHelper(this.thaboConnectStr);

                //SqlCommand com = new SqlCommand("ImportNotice", this.con);
                //com.CommandType = CommandType.StoredProcedure;
                //this.con.Open();

                foreach (EasyPayFineData fine in data.Authority.Fines)
                {
                    fine.UpdateDate = DateTime.Now;

                    // Check if the Notice exists and update it if necessary, return the ID
                    SqlParameter[] paras = new SqlParameter[17];
                    paras[0] = new SqlParameter("@AutIntNo", fine.Authority.ID);
                    paras[1] = new SqlParameter("@TicketNo", fine.TicketNumber);
                    paras[2] = new SqlParameter("@OffenceDate", fine.OffenceDate);
                    if (DateTime.Compare(fine.PaymentDate, DateTime.MinValue) > 0)
                        paras[3] = new SqlParameter("@PaymentDate", fine.PaymentDate);
                    else
                        paras[3] = new SqlParameter("@PaymentDate", null);
                    paras[4] = new SqlParameter("@FilmNo", fine.FilmNumber);
                    paras[5] = new SqlParameter("@FrameNo", fine.FrameNumber);
                    paras[6] = new SqlParameter("@Location", fine.Location);
                    paras[7] = new SqlParameter("@AccusedName", fine.OwnerName);
                    paras[8] = new SqlParameter("@AccusedID", fine.OwnerID);
                    paras[9] = new SqlParameter("@RegNo", fine.RegNo);
                    paras[10] = new SqlParameter("@CameraSerialNo", fine.CameraSerialNumber);
                    paras[11] = new SqlParameter("@Speed1", fine.Speed1);
                    paras[12] = new SqlParameter("@Speed2", fine.Speed2);
                    paras[13] = new SqlParameter("@SpeedZone", fine.SpeedZone);
                    paras[14] = new SqlParameter("@NotTEBatchNo", data.BatchNumber);
                    paras[15] = new SqlParameter("@NotRowChanged", fine.UpdateDate);
                    paras[16] = new SqlParameter("@LastUser", AARTOBase.LastUser);

                    oldNotIntNo = fine.NoticeID;
                    string errMsg = string.Empty;
                    notIntNo = Convert.ToInt32(db.ExecuteScalar("ImportNotice", paras, out errMsg));
                    fine.NoticeID = notIntNo;

                    // Check the Charge and update it if necessary
                    paras = new SqlParameter[9];
                    paras[0] = new SqlParameter("@NotIntNo", notIntNo);
                    paras[1] = new SqlParameter("@Amount", fine.FineAmount);
                    paras[2] = new SqlParameter("@Status", fine.Status);
                    paras[3] = new SqlParameter("@StatusDescr", fine.StatusDescription);
                    paras[4] = new SqlParameter("@OffenceCode", fine.OffenceCode);
                    paras[5] = new SqlParameter("@OffenceDescr", fine.OffenceDescription);
                    paras[6] = new SqlParameter("@ChgTEBatchNo", data.BatchNumber);
                    paras[7] = new SqlParameter("@ChgRowChanged", fine.UpdateDate);
                    paras[8] = new SqlParameter("@LastUser", AARTOBase.LastUser);

                    if (db.ExecuteNonQuery("ImportCharge", paras, out errMsg) > 0)
                        fine.UpdateSuccess = true;

                    // Update the ID for all the EasyPay transactions that relate to this notice
                    foreach (EasyPayTransaction tran in data.Authority.EasyPayTransactions)
                    {
                        if (tran.NoticeID.Equals(oldNotIntNo))
                            tran.NoticeID = notIntNo;
                    }

                    //Jerry 2013-09-16 Import NoticesForEasyPayExport table
                    if (data.Authority.IsEasyPay)
                    {
                        paras = new SqlParameter[2];
                        paras[0] = new SqlParameter("@NotIntNo", notIntNo);
                        paras[1] = new SqlParameter("@LastUser", AARTOBase.LastUser);
                        db.ExecuteNonQuery("ImportNoticesForEasyPayExport_WS", paras, out errMsg);
                    }
                }

            }
            catch (Exception ex)
            {
                //Logger.Write(string.Format("Error while updating Notice ({0})\n{1}", ex.Message, ex.StackTrace), "General", (int)TraceEventType.Critical);
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("UpdateNoticeErr"), ex.Message, ex.StackTrace), LogType.Error, ServiceOption.BreakAndStop);
                failed = true;
            }
            
            return failed;
        }

        private bool UpdateAuthority(FineExchangeDataFile data, ref string errorMessage)
        {
            bool failed = false;

            try
            {
                DBHelper db = new DBHelper(this.thaboConnectStr);

                SqlParameter[] paras = new SqlParameter[12];
                paras[0] = new SqlParameter("@AutCode", data.Authority.AuthorityCode);//this is returning null
                paras[1] = new SqlParameter("@AutName", data.Authority.Name);
                paras[2] = new SqlParameter("@AutNo", data.Authority.AuthorityNumber);
                paras[3] = new SqlParameter("@MachineName", data.MachineName);
                if (data.Authority.IsEasyPay)
                    paras[4] = new SqlParameter("@EasyPayReceiptID", data.Authority.ReceiverIdentifier);
                else
                    paras[4] = new SqlParameter("@EasyPayReceiptID", null);
                paras[5] = new SqlParameter("@RegCode", data.Authority.RegionCode);
                paras[6] = new SqlParameter("@RegName", data.Authority.RegionName);
                paras[7] = new SqlParameter("@RegTel", data.Authority.RegionPhone);
                paras[8] = new SqlParameter("@MetroCode", data.Authority.MetroCode);
                paras[9] = new SqlParameter("@MetroName", data.Authority.MetroName);
                paras[10] = new SqlParameter("@MetroNo", data.Authority.MetroNumber);
                paras[11] = new SqlParameter("@LastUser", AARTOBase.LastUser);

                string errMsg = string.Empty;
                data.Authority.ID = Convert.ToInt32(db.ExecuteScalar("ImportUpdateAuthority", paras, out errMsg).ToString());
            }
            catch (Exception ex)
            {
                failed = true;
                errorMessage = ex.Message;
                //Logger.Write(string.Format("Error while updating Authority ({0})\n{1}", ex.Message, ex.StackTrace), "General", (int)TraceEventType.Critical);
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("UpdateAuthorityErr"), ex.Message, ex.StackTrace), LogType.Error, ServiceOption.BreakAndStop);
            }

            return failed;
        }

        private FineExchangeDataFile ProcessFineDataFiles(Stream stream)
        {
            // Import Files into Thabo Database
            //mrs 20080714 made it handle a single stream
            var fine = new FineExchangeDataFile(stream);

            try
            {
                if (fine.Read())
                {
                    return fine;
                }
            }
            catch (Exception ex)
            {
                //Logger.Write("An error occurred reading the stream." + ex.Message, "General", (int)TraceEventType.Critical);
                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("ReadStreamErr"), ex.Message), LogType.Error, ServiceOption.BreakAndStop);
            }

            return null;
        }

        /// <summary>
        /// Gets the name of the machine from the LA code in the fine.
        /// </summary>
        /// <param name="fine">The fine.</param>
        /// <returns>true if a machine name was found; else false</returns>
        public bool GetMachineName(FineExchangeDataFile fine)
        {
            bool response = false;
            
            SqlConnection con = new SqlConnection(thaboConnectStr);
            using (var cmd = new SqlCommand("MachineNameFromAuthCode", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@AutCode", SqlDbType.Char, 3).Value = fine.Authority.AuthorityCode;
                try
                {
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        response = true;
                        fine.MachineName = reader.GetString(0);
                    }
                    reader.Dispose();
                }
                catch (Exception ex)
                {
                    fine.MachineName = string.Empty;
                    AARTOBase.LogProcessing(ex.Message, LogType.Error);
                }
                finally
                {
                    con.Close();
                }
            }

            return response;
        }

        public void InitIndaba()
        {
            Client.ApplicationName = "SIL.ThaboService.ReceiveNoticeDataFromRemote";
            Client.ConnectionString = this.indabaConnectStr;
            Client.Initialise();
            indaba = Client.Create();
        }

        public void DisposeIndaba()
        {
            indaba.Dispose();
            Client.Dispose();
        }

    }
}
