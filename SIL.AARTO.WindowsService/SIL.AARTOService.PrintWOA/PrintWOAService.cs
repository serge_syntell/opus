﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using SIL.AARTOService.Resource;
using Stalberg.TMS;
using SIL.AARTO.BLL.Utility.PrintFile;
using SIL.ServiceQueueLibrary.DAL.Services;

namespace SIL.AARTOService.PrintWOA
{
    public class PrintWOAService : ServiceDataProcessViaQueue
    {
        public PrintWOAService()
            : base("", "", new Guid("96E8979B-794D-416B-A602-4248E8E1AA55"), ServiceQueueTypeList.PrintWOA)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            this.connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.exportPath = AARTOBase.GetConnectionString(ServiceConnectionNameList.PrintFilesFolder, ServiceConnectionTypeList.UNC);
            this.process = new PrintFileProcess(this.connectStr, AARTOBase.LastUser);
            this.AARTOBase.OnServiceSleeping = OnServiceSleeping;
            this.AARTOBase.OnServiceStarting = OnServiceStarting;
            process.ErrorProcessing = (s) => { this.Logger.Error(s); };
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }

        AuthorityDetails authDetails = null;
        PrintFileProcess process = null;
        string connectStr = string.Empty;
        //Jerry 2012-04-11 add EmailToAdministrator
        //string emailToCustomer = string.Empty;
        string emailToAdministrator = string.Empty;

        string strAutCode = string.Empty;
        int autIntNo;
        string exportPath;
        List<string> attachments = new List<string>();

        public sealed override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && ServiceUtility.IsNumeric(body))
            {
                item.IsSuccessful = true;
                item.Status = QueueItemStatus.Discard;
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.LastUser, item.Body), true);
            }

            if (strAutCode != item.Group)
            {
                strAutCode = item.Group.Trim();
                if (string.IsNullOrWhiteSpace(strAutCode))
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
                }

                this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode);
                autIntNo = authDetails.AutIntNo;
            }

            //get to email address
            GetServiceParameters();
        }

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            int pfnIntNo;
            //foreach (QueueItem item in queueList)
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                try
                {
                    if (item.IsSuccessful)
                    {
                        pfnIntNo = Convert.ToInt32(item.Body);
                        process.BuildPrintFile(new PrintFileModuleWOA(AARTOBase.LastUser), this.autIntNo, pfnIntNo, AARTOBase.GetPrintFileDirectory(exportPath, strAutCode, PrintServiceType.WOA));
                        if (process.IsSuccessful)
                        {
                            attachments.Add(process.ExportPrintFileName);
                            this.Logger.Info(ResourceHelper.GetResource("PrintFileSavedTo", process.PrintFileName, process.ExportPrintFilePath));
                        }
                        else
                        {
                            AARTOBase.ErrorProcessing(ref item);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        private void OnServiceStarting()
        {
            if (attachments.Count > 0)
                attachments.Clear();
        }

        private void OnServiceSleeping()
        {
            string subject = ResourceHelper.GetResource("EmailSubjectInPrintWOAService");
            string message = ResourceHelper.GetResource("EmailMsgInPrintWOAService");
            //send email
            if (attachments.Count > 0)
            {
                for (int i = 0; i < attachments.Count; i++)
                {
                    message = message + attachments[i] + "\n";
                }
                SendEmailManager.SendEmail(emailToAdministrator, subject, message);
            }
                
        }

        private void GetServiceParameters()
        {
            if (ServiceParameters != null)
            {
                if (string.IsNullOrEmpty(this.emailToAdministrator)
&& ServiceParameters.ContainsKey("EmailToAdministrator") && !string.IsNullOrEmpty(ServiceParameters["EmailToAdministrator"]))
                {
                    this.emailToAdministrator = ServiceParameters["EmailToAdministrator"];
                    if (!SendEmailManager.CheckEmailAddress(this.emailToAdministrator))
                    {
                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ErrorEmailAddr"), AARTOBase.LastUser), true);
                    }
                }
                else if (string.IsNullOrEmpty(this.emailToAdministrator) || !ServiceParameters.ContainsKey("EmailToAdministrator") || string.IsNullOrEmpty(ServiceParameters["EmailToAdministrator"]))
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ErrorEmailAddr"), AARTOBase.LastUser), true);
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ErrorEmailAddr"), AARTOBase.LastUser), true);
            }
        }

        //private void StopService()
        //{
        //    this.MustStop = true;
        //    ((SIL.ServiceLibrary.ServiceHost)this.ServiceHost).Stop();
        //}

    }
}
