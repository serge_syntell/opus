﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PrintWOA
{
    partial class PrintWOA : ServiceHost
    {
        public PrintWOA()
            : base("", new Guid("4DBCCF2D-CD89-4CA9-9354-09D28A6CDD68"), new PrintWOAService())
        {
            InitializeComponent();
        }
    }
}
