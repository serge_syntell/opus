﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PrintWOA
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.PrintWOA"
                ,
                DisplayName = "SIL.AARTOService.PrintWOA"
                ,
                Description = "SIL AARTOService PrintWOA"
            };

            ProgramRun.InitializeService(new PrintWOA(), serviceDescriptor, args);
        }
    }
}
