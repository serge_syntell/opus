﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.SubmitLocations
{
    partial class SubmitLocations : ServiceHost
    {
        public SubmitLocations()
            : base("SIL.AARTOService.SubmitLocations",
            new Guid("8CC3B093-AFB2-4F31-85B9-FFA71F9A27FB"), new SubmitLocationsClientService())
        {
            InitializeComponent();
        }

      
    }
}
