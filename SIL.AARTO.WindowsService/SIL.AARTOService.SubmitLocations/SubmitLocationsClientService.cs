﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Transactions;
using System.ServiceModel;

using SIL.AARTO.BLL.Utility;
using Gendac.Syntell.Curator.Web.Services.Data;
using SIL.AARTOService.CISInterface.Library;
using SIL.AARTOService.Resource;
using SIL.AARTOService.Library;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTOService.SubmitLocations
{
    class SubmitLocationsClientService : ClientService
    {
        string url = "";
        string province = "";
        string city = "";
        string suburb = "";
        string username = "";
        string password = "";
        string folderpath = "";
        string emailaddress = "";
        string fileName = "Registered Location List.csv";

        int CLHIntNo = 0;
        CuratorClient client = null;

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }

        public SubmitLocationsClientService() :
            base("SubmitLocationsClientService", "SubmitLocationsClientService",
            new Guid("8D1A1385-7955-4BA4-BC16-04CC13A0C85B"))
        {
            this._serviceHelper = new AARTOServiceBase(this);

            AARTOBase.OnServiceStarting = () =>
            {
                url = AARTOBase.GetConnectionString(ServiceConnectionNameList.CISInterfaceURL, ServiceConnectionTypeList.WSU);

                if (string.IsNullOrWhiteSpace(url) || !Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute))
                {
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("CISURLError"), LogType.Fatal, ServiceBase.ServiceOption.BreakAndShutdown);
                    return;
                }

                province = AARTOBase.GetServiceParameter("Province");
                city = AARTOBase.GetServiceParameter("City");
                suburb = AARTOBase.GetServiceParameter("Suburb");
                username = AARTOBase.GetServiceParameter("Username");
                password = AARTOBase.GetServiceParameter("Password");
                folderpath = AARTOBase.GetServiceParameter("Folderpath");
                emailaddress = AARTOBase.GetServiceParameter("Emailaddress");

                if (!Directory.Exists(folderpath))
                {
                    AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CISShareFolderError"), folderpath), LogType.Fatal, ServiceBase.ServiceOption.BreakAndShutdown);
                    return;
                }

                CLHIntNo = 0;

                System.ServiceModel.BasicHttpBinding binding = new System.ServiceModel.BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential);
                System.ServiceModel.EndpointAddress address = new System.ServiceModel.EndpointAddress(new Uri(url));
                binding.OpenTimeout = new TimeSpan(0, 3, 0);
                client = new CuratorClient(binding, address);
                client.ClientCredentials.UserName.UserName = username;
                client.ClientCredentials.UserName.Password = password;
            };
        }

        public override void InitialWork(ref QueueLibrary.QueueItem item)
        {
            
        }

        public override void MainWork(ref List<QueueLibrary.QueueItem> queueList)
        {
            LocationReply locReply = null;

            RetrieveLocationsRequest request = new RetrieveLocationsRequest();
            request.Province = province;
            request.City = city;
            request.Suburb = suburb;

            string requestXml = ObjectSerializer.Serialize(request);
            AddLocationHistory(requestXml);

            //write send log
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Suppress, TimeSpan.MaxValue))
            {
                this.Logger.Info(string.Format(ResourceHelper.GetResource("CISSubmitLocationsSendLogFile"), province, city, suburb));
                scope.Complete();
            }

            try
            {
                locReply = client.RetrieveLocations(province, city, suburb); 
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex, LogType.Error, ServiceBase.ServiceOption.Continue);
            }

            if (locReply != null)
            {
                //write receive log
                this.Logger.Info(string.Format(ResourceHelper.GetResource("CISSubmitLocationsReceiveLogFile"), province, city, suburb));

                string responseXml = ObjectSerializer.Serialize(locReply);
                AddLocationHistory(responseXml);

                if (locReply.IsSuccess)
                {
                    using (TransactionScope tran = new TransactionScope())
                    {
                        LocationQuery query = new LocationQuery();
                        query.AppendEquals(LocationColumn.Province, province);
                        query.AppendEquals(LocationColumn.City, city);
                        query.AppendEquals(LocationColumn.Suburb, suburb);

                        SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.Location> locationList = new LocationService().Find(query as IFilterParameterCollection);

                        List<Gendac.Syntell.Curator.Web.Services.Data.Location> compareList = this.CompareDate(locationList, locReply.Data);

                        if (compareList.Count > 0)
                        {
                            byte[] buffer = WriteCSVFile(compareList);
                            if (buffer != null)
                            {
                                SentEmail(buffer);

                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CISSubmitLocationsWriteCSVSuccessfully"), province, city, suburb), LogType.Info, ServiceBase.ServiceOption.Continue);

                                tran.Complete();
                            }
                            else
                            {
                                AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CISSubmitLocationsWriteCSVFailed"), province, city, suburb), LogType.Error, ServiceBase.ServiceOption.Continue);
                            }
                        }
                    }
                }
                else
                {
                    ReplyMessage[] message = locReply.Messages;
                    if (message != null && message.Length > 0)
                    {
                        string errorMsg = "";
                        for (int i = 0; i < message.Length; i++)
                        {
                            errorMsg += message[i].Code.ToString() + "~" + message[i].Description + ",";
                        }
                        if (errorMsg != "")
                        {
                            errorMsg = errorMsg.Substring(0, errorMsg.Length - 1);
                        }

                        AARTOBase.LogProcessing(string.Format(ResourceHelper.GetResource("CISSubmitLocationsResponseError"), province, city, suburb, errorMsg), LogType.Error, ServiceBase.ServiceOption.Continue);
                    }
                }
            }

            this.MustSleep = true;
        }

        private XDocument GetSoap()
        {
            XNamespace wsse = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
            XNamespace soap = "http://schemas.xmlsoap.org/soap/envelope/";
            XNamespace xs = "http://www.w3.org/2001/XMLSchema";
            XNamespace tns = "http://tempuri.org/";

            XDocument requestDocument = new XDocument(
                new XDeclaration("1.0", "UTF-8", null),
                new XElement(soap + "Envelope",
                    new XAttribute(XNamespace.Xmlns + "wsse", wsse),
                    new XAttribute(XNamespace.Xmlns + "soap", soap),
                    new XAttribute(XNamespace.Xmlns + "xs", xs),
                    new XElement(soap + "Header",
                        new XElement(wsse + "Security",
                            new XElement(wsse + "UsernameToken",
                                new XElement(wsse + "Username", username),
                                new XElement(wsse + "Password", password)
                            )
                        )
                    ),
                    new XElement(soap + "Body",
                        new XElement(tns + "RetrieveLocations",
                            new XElement("province", province),
                            new XElement("city", city),
                            new XElement("suburb", suburb)
                        )
                    )
                )
            );

            return requestDocument;
        }

        private void AddLocationHistory(string xmlBody)
        {
            if (CLHIntNo == 0)
            {
                CisLocationHistoryFile history = new CisLocationHistoryFile();
                history.ClhRequestXml = xmlBody;
                history.ClhRequestDate= DateTime.Now;
                history.LastUser = AARTOBase.LastUser;

                history = new CisLocationHistoryFileService().Save(history);
                CLHIntNo = history.ClhIntNo;
            }
            else
            {
                CisLocationHistoryFile history = new CisLocationHistoryFileService().GetByClhIntNo(CLHIntNo);
                history.ClhResponseXml = xmlBody;
                history.ClhResponseDate = DateTime.Now;
                history.LastUser = AARTOBase.LastUser;

                history = new CisLocationHistoryFileService().Save(history);
            }
        }

        private List<Gendac.Syntell.Curator.Web.Services.Data.Location> CompareDate(SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.Location> AARTOLocationList, Gendac.Syntell.Curator.Web.Services.Data.Location[] CISLocationList)
        {
            List<Gendac.Syntell.Curator.Web.Services.Data.Location> list = new List<Gendac.Syntell.Curator.Web.Services.Data.Location>();

            for (int i = 0; i < AARTOLocationList.Count; i++)
            {
                Gendac.Syntell.Curator.Web.Services.Data.Location location = new Gendac.Syntell.Curator.Web.Services.Data.Location();
                if (CISLocationList != null && CISLocationList.Length > 0)
                {
                    int j;
                    bool isExist = false;
                    for (j = 0; j < CISLocationList.Length; j++)
                    {
                        if (!string.IsNullOrWhiteSpace(AARTOLocationList[i].LocCode) && AARTOLocationList[i].LocCode == CISLocationList[j].ReferenceCode)
                        {
                            isExist = true;
                            break;
                        }
                    }

                    if (isExist)
                    {
                        bool isSame = true;
                        if (!StringCompare(AARTOLocationList[i].LocRegion, CISLocationList[j].Region))
                        {
                            isSame = false;
                        }

                        if (!StringCompare(AARTOLocationList[i].Province, CISLocationList[j].Province))
                        {
                            isSame = false;
                        }

                        if (!StringCompare(AARTOLocationList[i].City, CISLocationList[j].City))
                        {
                            isSame = false;
                        }

                        if (!StringCompare(AARTOLocationList[i].Suburb, CISLocationList[j].Suburb))
                        {
                            isSame = false;
                        }

                        if (!StringCompare(AARTOLocationList[i].Streeta, CISLocationList[j].StreetNameA))
                        {
                            isSame = false;
                        }

                        if (!StringCompare(AARTOLocationList[i].Streetb, CISLocationList[j].StreetNameB))
                        {
                            isSame = false;
                        }

                        if (!StringCompare(AARTOLocationList[i].Route, CISLocationList[j].RouteName))
                        {
                            isSame = false;
                        }

                        if (!StringCompare(AARTOLocationList[i].Gpsy, CISLocationList[j].GPSLatitude))
                        {
                            isSame = false;
                        }

                        if (!StringCompare(AARTOLocationList[i].Gpsx, CISLocationList[j].GPSLongitude))
                        {
                            isSame = false;
                        }

                        string locType = AARTOLocationList[i].LocType == "F" ? "Fixed camera" : "Mobile camera";
                        //if (!StringCompare(locType, CISLocationList[j].Region))
                        //{
                        //    isSame = false;
                        //}

                        //if (!StringCompare(AARTOLocationList[i].LocDescr, CISLocationList[j].Region))
                        //{
                        //    isSame = false;
                        //}

                        //RoadType roadtype = new RoadTypeService().GetByRdTintNo(AARTOLocationList[i].RdTintNo);
                        //if (!StringCompare(roadtype.RdTypeDescr, CISLocationList[j].Region))
                        //{
                        //    isSame = false;
                        //}

                        //SpeedZone zone = new SpeedZoneService().GetBySzIntNo(AARTOLocationList[i].SzIntNo);
                        //if (!StringCompare(zone.SzSpeed.ToString(), CISLocationList[j].Region))
                        //{
                        //    isSame = false;
                        //}

                        //if (!StringCompare(AARTOLocationList[i].LocOffenceSpeedStart.ToString(), CISLocationList[j].Region))
                        //{
                        //    isSame = false;
                        //}

                        if (!isSame)
                        {
                            location.Region = StringTrim(AARTOLocationList[i].LocRegion);
                            location.Province = StringTrim(AARTOLocationList[i].Province);
                            location.City = StringTrim(AARTOLocationList[i].City);
                            location.Suburb = StringTrim(AARTOLocationList[i].Suburb);
                            location.StreetNameA = StringTrim(AARTOLocationList[i].Streeta);
                            location.StreetNameB = StringTrim(AARTOLocationList[i].Streetb);
                            location.RouteName = StringTrim(AARTOLocationList[i].Route);
                            location.GPSLatitude = StringTrim(AARTOLocationList[i].Gpsy);
                            location.GPSLongitude = StringTrim(AARTOLocationList[i].Gpsx);
                            location.ReferenceCode = StringTrim(AARTOLocationList[i].LocCode);
                            //location.Region = StringTrim(locType);
                            //location.Region = StringTrim(AARTOLocationList[i].LocDescr);
                            //location.Region = StringTrim(roadtype.RdTypeDescr);
                            //location.Region = StringTrim(zone.SzSpeed.ToString());
                            //location.Region = StringTrim(AARTOLocationList[i].LocOffenceSpeedStart.ToString());

                            list.Add(location);
                        }
                    }
                    else
                    {
                        string locType = AARTOLocationList[i].LocType == "F" ? "Fixed camera" : "Mobile camera";
                        RoadType roadtype = new RoadTypeService().GetByRdTintNo(AARTOLocationList[i].RdTintNo);
                        SpeedZone zone = new SpeedZoneService().GetBySzIntNo(AARTOLocationList[i].SzIntNo);

                        location.Region = StringTrim(AARTOLocationList[i].LocRegion);
                        location.Province = StringTrim(AARTOLocationList[i].Province);
                        location.City = StringTrim(AARTOLocationList[i].City);
                        location.Suburb = StringTrim(AARTOLocationList[i].Suburb);
                        location.StreetNameA = StringTrim(AARTOLocationList[i].Streeta);
                        location.StreetNameB = StringTrim(AARTOLocationList[i].Streetb);
                        location.RouteName = StringTrim(AARTOLocationList[i].Route);
                        location.GPSLatitude = StringTrim(AARTOLocationList[i].Gpsy);
                        location.GPSLongitude = StringTrim(AARTOLocationList[i].Gpsx);
                        location.ReferenceCode = StringTrim(AARTOLocationList[i].LocCode);
                        //location.Region = StringTrim(locType);
                        //location.Region = StringTrim(AARTOLocationList[i].LocDescr);
                        //location.Region = StringTrim(roadtype.RdTypeDescr);
                        //location.Region = StringTrim(zone.SzSpeed.ToString());
                        //location.Region = StringTrim(AARTOLocationList[i].LocOffenceSpeedStart.ToString());

                        list.Add(location);
                    }
                }
            }

            return list;
        }

        private byte[] WriteCSVFile(List<Gendac.Syntell.Curator.Web.Services.Data.Location> locationList)
        {
            byte[] buffer = null;
            StreamWriter sw = null;
            FileStream fs = null;
            try
            {
                StringBuilder sbIDs = new StringBuilder();
                
                fs = new FileStream(folderpath + "\\" + fileName, FileMode.Create, FileAccess.Write);
                sw = new StreamWriter(fs, System.Text.Encoding.UTF8);

                sbIDs.Append("JMPD Region|Province|City|Suburb|StreetNameA|StreetNameB|RouteName|GPSLatitude|GPSLongitude|Service Provider Location Code|"
                    + "Location Type|Service Provider Location Description|Road Type|Car Speed limit|Car Fine Limit|HMV Speed Limit|HMV Fine Limit" + "\n");
                for (int i = 0; i < locationList.Count; i++)
                {
                    sbIDs.Append(locationList[i].Region + "|" + locationList[i].Province + "|" + locationList[i].City + "|" + locationList[i].Suburb + "|" + locationList[i].StreetNameA + "|" + locationList[i].StreetNameB + "|" + locationList[i].RouteName + "|" + locationList[i].GPSLatitude + "|" + locationList[i].GPSLongitude + "|" + locationList[i].ReferenceCode
                         //+ "|" + locationList[i].Region + "|" + locationList[i].Region + "|" + locationList[i].Region + "|" + locationList[i].Region + "|" + locationList[i].Region + "|" + locationList[i].Region + "|" + locationList[i].Region + "\n");
                         + "|" + "" + "|" + "" + "|" + "" + "|" + "" + "|" + "" + "|" + "" + "|" + "" + "\n");
                }

                sw.WriteLine(sbIDs);

                buffer = System.Text.Encoding.UTF8.GetBytes(sbIDs.ToString());
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex, LogType.Error, ServiceBase.ServiceOption.Continue);
                return null;
            }
            finally
            {
                if (sw != null)
                    sw.Close();
                if (fs != null)
                    fs.Close();
            }
            return buffer;
        }

        private void SentEmail(byte[] buffer)
        {
            EmailTransaction email = new EmailTransaction();

            email.EmTrAttachment = buffer;
            email.EmTrAttachmentName = fileName;

            email.EmTrSubject = string.Format("CIS Registered Location Update ({0})", DateTime.Now.ToString("yyyy-MM-dd"));
            email.EmTrTo = emailaddress;
            string content = string.Format("Please find the attached file ({0}) for modified locations from for loading into CIS.", fileName);
            content += "\r\nSyntell";
            email.EmTrContent = content;

            EmailManager emailManager = new EmailManager();
            emailManager.SendMail(email);
        }


        private bool StringCompare(string obj1, string obj2)
        {
            bool comp = true;

            if (string.IsNullOrEmpty(obj1) && string.IsNullOrEmpty(obj2))
            {
                comp = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(obj1))
                {
                    obj1 = obj1.Trim();
                }
                if (!string.IsNullOrEmpty(obj2))
                {
                    obj2 = obj2.Trim();
                }

                comp = string.Equals(obj1, obj2);
            }

            return comp;
        }

        private string StringTrim(string obj)
        {
            if (!string.IsNullOrEmpty(obj))
            {
                obj = obj.Trim();
            }

            return obj;
        }
    }

    [Serializable]
    public class RetrieveLocationsRequest
    {
        public string Province;
        public string City;
        public string Suburb;
    }
} 