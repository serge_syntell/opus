﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.SubmitLocations
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescr = new ServiceDescriptor()
            {
                Description = "SIL.AARTOService.SubmitLocations",
                DisplayName = "SIL.AARTOService.SubmitLocations",
                ServiceName = "SIL.AARTOService.SubmitLocations"
            };

            ProgramRun.InitializeService(new SubmitLocations(), serviceDescr, args);
        }
    }
}
