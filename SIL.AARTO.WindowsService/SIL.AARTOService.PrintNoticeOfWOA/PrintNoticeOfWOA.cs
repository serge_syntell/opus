﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PrintNoticeOfWOA
{
    partial class PrintNoticeOfWOA : ServiceHost
    {
        public PrintNoticeOfWOA()
            : base("", new Guid("D6D66ED7-023F-43D2-85F1-3870B98E2B54"), new PrintNoticeOfWOAService())
        {
            InitializeComponent();
        }
    }
}
