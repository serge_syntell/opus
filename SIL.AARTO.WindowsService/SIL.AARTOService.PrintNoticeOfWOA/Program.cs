﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PrintNoticeOfWOA
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.PrintNoticeOfWOA"
                ,
                DisplayName = "SIL.AARTOService.PrintNoticeOfWOA"
                ,
                Description = "SIL AARTOService PrintNoticeOfWOA"
            };

            ProgramRun.InitializeService(new PrintNoticeOfWOA(), serviceDescriptor, args);
        }
    }
}
