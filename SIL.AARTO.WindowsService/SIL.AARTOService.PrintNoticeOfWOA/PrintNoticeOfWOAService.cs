﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using Stalberg.TMS;
using SIL.AARTO.BLL.Utility.PrintFile;
using SIL.AARTOService.Resource;
using SIL.QueueLibrary;

namespace SIL.AARTOService.PrintNoticeOfWOA
{
    public class PrintNoticeOfWOAService : ServiceDataProcessViaQueue
    {
        public PrintNoticeOfWOAService()
            : base("", "", new Guid("9F7D0221-40FD-40A9-A13D-137BB453109E"), ServiceQueueTypeList.PrintNoticeOfWOA)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            this.connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.exportPath = AARTOBase.GetConnectionString(ServiceConnectionNameList.PrintFilesFolder, ServiceConnectionTypeList.UNC);
            this.process = new PrintFileProcess(this.connectStr, AARTOBase.LastUser);
            //Jerry 2012-04-28 add
            this.noticeOfWoaDB = new NoticeOfWoaDB(this.connectStr);
            this.AARTOBase.OnServiceSleeping = OnServiceSleeping;
            this.AARTOBase.OnServiceStarting = OnServiceStarting;
            process.ErrorProcessing = (s) => { this.Logger.Error(s); };
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }

        AuthorityDetails authDetails = null;
        PrintFileProcess process = null;
        string connectStr = string.Empty;
        //Jerry 2012-04-11 add EmailToAdministrator
        //string emailToCustomer = string.Empty;
        string emailToAdministrator = string.Empty;

        string strAutCode = string.Empty;
        int autIntNo;
        string exportPath;
        List<string> attachments = new List<string>();

        //Jerry 2012-04-28 add
        NoticeOfWoaDB noticeOfWoaDB = null;

        public sealed override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && ServiceUtility.IsNumeric(body))
            {
                item.IsSuccessful = true;
                item.Status = QueueItemStatus.Discard;
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.LastUser, item.Body), true);
            }

            if (strAutCode != item.Group)
            {
                strAutCode = item.Group.Trim();
                if(string.IsNullOrWhiteSpace(strAutCode))
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
                }

                this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode);
                autIntNo = authDetails.AutIntNo;
            }

            //get to email address
            GetServiceParameters();
        }

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            int pfnIntNo;
            //foreach (QueueItem item in queueList)
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                try
                {
                    if (item.IsSuccessful)
                    {
                        pfnIntNo = Convert.ToInt32(item.Body);
                        process.BuildPrintFile(new PrintFileModuleNoticeOfWOA(), this.autIntNo, pfnIntNo, AARTOBase.GetPrintFileDirectory(exportPath, strAutCode, PrintServiceType.NoticeOfWOA));
                        if (process.IsSuccessful)
                        {
                            attachments.Add(process.ExportPrintFileName);

                            //Jerry 2012-04-28 add code to update printed status
                            string errorMsg = string.Empty;
                            int success = noticeOfWoaDB.SetNoticeOfWoaAsPrinted(process.ExportPrintFileName, true, AARTOBase.LastUser, ref errorMsg);

                            if (success < 1)
                            {
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorUpdateStatusOfPrintNoticeOfWOA"), errorMsg));
                                return; // 2014-12-02, Oscar added (bontq 1726, ref 1725)
                            }

                            this.Logger.Info(ResourceHelper.GetResource("PrintFileSavedTo", process.PrintFileName, process.ExportPrintFilePath));
                        }
                        else
                        {
                            AARTOBase.ErrorProcessing(ref item);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        private void OnServiceStarting()
        {
            if (attachments.Count > 0)
                attachments.Clear();
        }

        private void OnServiceSleeping()
        {
            string subject = ResourceHelper.GetResource("EmailSubjectInPrintNoticeOfWOAService");
            string message = ResourceHelper.GetResource("EmailMsgInPrintNoticeOfWOAService");
            //send email
            if (attachments.Count > 0)
            {
                for (int i = 0; i < attachments.Count; i++)
                {
                    message = message + attachments[i] + "\n";
                }
                SendEmailManager.SendEmail(emailToAdministrator, subject, message);
            }
        }

        private void GetServiceParameters()
        {
            if (ServiceParameters != null)
            {
                if (string.IsNullOrEmpty(this.emailToAdministrator)
&& ServiceParameters.ContainsKey("EmailToAdministrator") && !string.IsNullOrEmpty(ServiceParameters["EmailToAdministrator"]))
                {
                    this.emailToAdministrator = ServiceParameters["EmailToAdministrator"];
                    if (!SendEmailManager.CheckEmailAddress(this.emailToAdministrator))
                    {
                        AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ErrorEmailAddr"), AARTOBase.LastUser), true);
                    }
                }
                else if (string.IsNullOrEmpty(this.emailToAdministrator) || !ServiceParameters.ContainsKey("EmailToAdministrator") || string.IsNullOrEmpty(ServiceParameters["EmailToAdministrator"]))
                {
                    AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ErrorEmailAddr"), AARTOBase.LastUser), true);
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ErrorEmailAddr"), AARTOBase.LastUser), true);
            }
        }

        //private void StopService()
        //{
        //    this.MustStop = true;
        //    ((SIL.ServiceLibrary.ServiceHost)this.ServiceHost).Stop();
        //}

    }
}
