﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.AARTOService.eNatisBase;
using SIL.OpenVpnAutomation;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.eNaTIS.DAL.Data;
using SIL.eNaTIS.DAL.Entities;
using SIL.eNaTIS.DAL.Services;
using LogType = SIL.OpenVpnAutomation.LogType;
using Logger = SIL.ServiceLibrary.ILogger;
using VpnConfig = SIL.eNaTIS.DAL.Entities.VpnConfig;
using SIL.ServiceBase;

namespace SIL.AARTOService.eNatisInterfaceX3048
{
    class eNatisInterfaceX3048Service : ClientService
    {
        public eNatisInterfaceX3048Service() : base("", "", new Guid("61243A6D-3DE4-40C2-AF83-876E0D4A247E"))
        {
            this._serviceHelper = new AARTOServiceBase(this);
            var ntiConnString = serviceBase.GetConnectionString(ServiceConnectionNameList.SIL_NTI, ServiceConnectionTypeList.DB);
            ServiceUtility.InitializeNetTier(ntiConnString, "SIL.eNaTIS");

            serviceBase.OnServiceStarting = () =>
            {
                eNatisBase.Logger.SetLoggerInstance(this);
                Parameters.EmailToAdministrator = serviceBase.GetServiceParameter("EmailToAdministrator");
                //Parameters.AuthNo212 = serviceBase.GetServiceParameter("AuthNo212");
                //Parameters.AuthNo214 = serviceBase.GetServiceParameter("AuthNo214");
                //Parameters.AuthNo166 = serviceBase.GetServiceParameter("AuthNo166");
                Parameters.URL_CF = serviceBase.GetServiceParameter("URL_CF");
                Parameters.URIBase = serviceBase.GetServiceParameter("URIBase");
                Parameters.UserBase = serviceBase.GetServiceParameter("UserBase");

                try
                {
                    var port = int.Parse(serviceBase.GetServiceParameter("RemoteHostPort"));
                    var configName = serviceBase.GetServiceParameter("VpnConfigName");

                    config = OpenVpnClient.GetFirstActivedConfig(configName);
                    if (config == null)
                        throw new ArgumentNullException("VpnConfig", ResourceHelper.GetResource("CouldNotFindActivedVpnConfigPleaseCheckDatabaseOrConfigName"));

                    if (vpnClient == null)
                        vpnClient = new OpenVpnClient(IPAddress.Loopback.ToString(), port, OnLogging);

                    if (!vpnClient.Connect(config))
                    {
                        throw new InvalidOperationException((ResourceHelper.GetResource("CouldNotConnectToVpnServiceWillBeClosed")));
                    }

                }
                catch (Exception ex)
                {
                    if (ex is InvalidOperationException
                        || ex is ArgumentNullException)
                        serviceBase.ErrorProcessing(ex.Message, true);
                    else
                        serviceBase.ErrorProcessing(ex);

                    CloseConnection();
                }

            };
            serviceBase.OnServiceSleeping = () =>
            {
                CloseConnection();
            };
        }

        OpenVpnClient vpnClient;
        VpnConfig config;

        AARTOServiceBase serviceBase
        {
            get { return (AARTOServiceBase)_serviceHelper; }
        }

        void OnLogging(object sender, LogEventArgs e)
        {
            switch (e.LogType)
            {
                case LogType.Info:
                    if (e.Exception != null)
                        Logger.Info(e.Exception);
                    else
                        Logger.Info(e.Message);
                    break;

                case LogType.Warning:
                    if (e.Exception != null)
                        Logger.Warning(e.Exception);
                    else
                        Logger.Warning(e.Message);
                    break;

                case LogType.Error:
                    if (e.Exception != null)
                        Logger.Error(e.Exception);
                    else
                        Logger.Error(e.Message);
                    break;
            }
        }

        void CloseConnection()
        {
            try
            {
                if (vpnClient != null && (config == null || !config.CanNotClose))
                {
                    vpnClient.Disconnect(config);
                    vpnClient.Dispose();
                    vpnClient = null;
                }
            }
            catch {}
        }

        public override void MainWork(ref List<QueueLibrary.QueueItem> queueList)
        {
            eNatisBase.Logger.Write(string.Format("Checking at {0}", DateTime.Now.ToString("HH:mm:ss")), "General", (int)TraceEventType.Information, 0, TraceEventType.Information);
            try
            {
                eNaTISRequest eNatisRequest = new eNaTISRequest();
                eNatisRequest.ProcessNotice();
            }
            catch (Exception ex)
            {
                eNatisBase.Logger.Write(string.Format("General Error: {0}\n{1}", ex, ex.InnerException), "General", (int)TraceEventType.Critical, 0, TraceEventType.Critical);
            }
            MustSleep = true;
        }

    }
}
