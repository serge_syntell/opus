﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.eNatisInterfaceX3048
{
    partial class eNatisInterfaceX3048 : ServiceHost
    {
        public eNatisInterfaceX3048() : base("", new Guid("F2A32711-8931-4DFD-9344-49D862A0615D"), new eNatisInterfaceX3048Service())
        {
            InitializeComponent();
        }
    }
}
