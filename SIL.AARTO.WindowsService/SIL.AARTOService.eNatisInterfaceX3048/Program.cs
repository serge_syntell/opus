﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.eNatisInterfaceX3048
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ProgramRun.InitializeService(new eNatisInterfaceX3048(), new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.eNatisInterfaceX3048",
                DisplayName = "SIL.AARTOService.eNatisInterfaceX3048",
                Description = "SIL AARTOService eNatis Interface X3048"
            }, args);
        }
    }
}
