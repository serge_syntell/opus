﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CIP_AARTO_InfringementFileCreate
{
    partial class CIP_AARTO_InfringementFileCreate : ServiceHost
    {
        public CIP_AARTO_InfringementFileCreate()
            : base("", new Guid("7DBF1B02-89CC-498D-9AC5-BED03CDC3500"), new CIP_AARTO_InfringementFileCreateService())
        {
            InitializeComponent();
        }
    }
}
