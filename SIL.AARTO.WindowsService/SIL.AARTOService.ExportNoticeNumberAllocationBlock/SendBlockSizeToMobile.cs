﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIL.MobileInterface.DAL.Data;
using SIL.MobileInterface.DAL.Entities;
using SIL.MobileInterface.DAL.Services;
using TMS = Stalberg.TMS;
using Stalberg.TMS.Data.Util;
using SIL.AARTOService.Library;

namespace SIL.AARTOService.ExportNoticeNumberAllocationBlock
{
   public class SendBlockSizeToMobile
    {
       public SendBlockSizeToMobile(string connStr)
       {
           this.connStr = connStr;
           npUpdate = new Stalberg.TMS.NoticePrefixDB(this.connStr);
           validation = Validation.GetInstance(this.connStr);
       }
       string connStr;
       public AARTORules rules = AARTORules.GetSingleTon();
       Stalberg.TMS.NoticePrefixDB npUpdate;
       public Validation validation;
       string lastUser = "ExportNoticeNumberAllocationBlock";

       public List<AllocationPool> GetTheSendList(int imprest,ref string errorMsg)
       {
           List<AllocationPool> noticeExistsList = GetNoticeNumberCount();
           List<Authority> authorityList = GetAuthorityList();
           List<DocumentTypeId> documentTypeIdList = GetDocumentType();
           List<AllocationPool> noticeAllList = GetDocumentAndAuthorityList(documentTypeIdList, authorityList);
           RemoveTheEnoughNoticeNo(noticeAllList, noticeExistsList, imprest, ref errorMsg);

           return noticeAllList;
       }
       private void RemoveTheEnoughNoticeNo(List<AllocationPool> noticeAllList, List<AllocationPool> noticeExistsList, int imprest, ref string errorMsg)
       {
           try
           {
               for (int i = 0; i < noticeExistsList.Count; i++)
               {
                   int id = noticeExistsList[i].MobileAutIntNo;
                   int number = noticeExistsList[i].Count;
                   int documentId = noticeExistsList[i].MobileDocumentId;
                   if (number < imprest)
                       continue;
                   var query = from noticeObj in noticeAllList
                               where noticeObj.MobileAutIntNo == id & noticeObj.MobileDocumentId == documentId
                               select noticeObj;
                   noticeAllList.Remove((AllocationPool)query.First());
               }
           }
           catch (Exception ex)
           {
               errorMsg = string.Format("{0}:{1}", "FinishTheNeedAuthority", ex.ToString());
               throw new Exception(errorMsg);
           }
       }
       private List<AllocationPool> GetNoticeNumberCount()
       {
           List<AllocationPool> noticeList = new List<AllocationPool>();
           NoticeNumberAllocationPoolService numberCount = new NoticeNumberAllocationPoolService();
           IDataReader iRead = numberCount.GetNoticeNumberCount();
           while (iRead.Read())
           {
               AllocationPool pool = new AllocationPool();
               pool.MobileAutIntNo = iRead.GetInt32(0);
               pool.MobileDocumentId = iRead.GetInt32(1);
               pool.Count = iRead.GetInt32(2);
               noticeList.Add(pool);
           }
           iRead.Close();
           return noticeList;
       }
       public int InsertMobileAllocationPool(AllocationPool item, ref string errorMsg)
       {
           try
           {
               NoticeNumberAllocationPoolService number_Service = new NoticeNumberAllocationPoolService();
               NoticeNumberAllocationPool pool = new NoticeNumberAllocationPool();
               pool.DocumentTypeId = item.MobileDocumentId;
               pool.AuthorityId = item.MobileAutIntNo;
               pool.NnapStartNumber = item.StartNumber;// (item.StartNumber == item.MaxNumber ? 1 : (item.StartNumber + 1));
               pool.NnapFinishNumber = item.EndNumber;
               pool.NnapNumberPrefix = item.Prefix;
               pool.LastUser = lastUser;
               //pool.NnapDateReceived = item.CurrentDate; use for deal status
               number_Service.Save(pool);
               return pool.Nnapid;
           }
           catch (Exception ex)
           {
               errorMsg = string.Format("{0}:{1}", "InsertMobileAllocationPool", ex.ToString());
               throw new Exception(errorMsg);
           }
       }
       //get authority
       private List<Authority> GetAuthorityList()
       {
           AuthorityService au_Service = new AuthorityService();
           return au_Service.GetAll().ToList();
       }
       //get document type
       private List<DocumentTypeId> GetDocumentType()
       {
           DocumentTypeIdService dts = new DocumentTypeIdService();
           return dts.GetAll().ToList();
       }
       private List<AllocationPool> GetDocumentAndAuthorityList(List<DocumentTypeId> documentTypeIdList, List<Authority> authorityList)
       {
           List<AllocationPool> ncoList = new List<AllocationPool>();
           for (int i = 0; i < documentTypeIdList.Count; i++)
           {
               for (int j = 0; j < authorityList.Count; j++)
               {
                   AllocationPool nco = new AllocationPool();
                   nco.MobileDocumentId = documentTypeIdList[i].DocumentTypeId;
                   nco.MobileDocumentCode = documentTypeIdList[i].DtCode;
                   nco.MobileAutIntNo = authorityList[j].AuthorityId;
                   nco.AutCode = authorityList[j].AutCode;
                   if (nco.MobileDocumentCode == "S341Mobile")
                       nco.TranNoType = "M3";
                   else if (nco.MobileDocumentCode == "S56Mobile")
                       nco.TranNoType = "M5";
                   else if (nco.MobileDocumentCode == "AA31Mobile")
                       nco.TranNoType = "M01";
                   ncoList.Add(nco);
               }
           }
           return ncoList;
       }

       public int GetMaxNumber(int length)
       {
           int reNumber = 1;
           for (int i = 0; i < length; i++)
           {
               reNumber *= 10;
           }
           return (reNumber - 1);
       }
    }
  public class AllocationPool
   {
       //AuthorityID,DocumentTypeID,Number
       private int mobileAutIntNo;

       public int MobileAutIntNo
       {
           get { return mobileAutIntNo; }
           set { mobileAutIntNo = value; }
       }
       private int count;

       public int Count
       {
           get { return count; }
           set { count = value; }
       }
       private int mobileDocumentId;

       public int MobileDocumentId
       {
           get { return mobileDocumentId; }
           set { mobileDocumentId = value; }
       }
       private string mobileDocumentCode;

       public string MobileDocumentCode
       {
           get { return mobileDocumentCode; }
           set { mobileDocumentCode = value; }
       }

       private string tranNoType;

       public string TranNoType
       {
           get { return tranNoType; }
           set { tranNoType = value; }
       }
      

       private string autCode;

       public string AutCode
       {
           get { return autCode; }
           set { autCode = value; }
       }
       private int aartoAutIntNo;

       public int AartoAutIntNo
       {
           get { return aartoAutIntNo; }
           set { aartoAutIntNo = value; }
       }
       private int arrtoDocumentId;

       public int ArrtoDocumentId
       {
           get { return arrtoDocumentId; }
           set { arrtoDocumentId = value; }
       }
       private string prefix;

       public string Prefix
       {
           get { return prefix; }
           set { prefix = value; }
       }

      //Jerry 2014-05-15 add PrefixHistory
       private string prefixHistory;
       public string PrefixHistory
       {
           get { return prefixHistory; }
           set { prefixHistory = value; }
       }

       private int startNumber;

       public int StartNumber
       {
           get { return startNumber; }
           set { startNumber = value; }
       }
       private int endNumber;

       public int EndNumber
       {
           get { return endNumber; }
           set { endNumber = value; }
       }
       private DateTime currentDate;

       public DateTime CurrentDate
       {
           get { return DateTime.Now; }
       }
       private int blockSize;

       public int BlockSize
       {
           get { return blockSize; }
           set { blockSize = value; }
       }
       private int maxNumber;

       public int MaxNumber
       {
           get { return maxNumber; }
           set { maxNumber = value; }
       }
       private TMS.AuthorityRulesDetails autRuleDetail;

       public TMS.AuthorityRulesDetails AutRuleDetail
       {
           get { return autRuleDetail; }
           set { autRuleDetail = value; }
       }
       private string autNo;

       public string AutNo
       {
           get { return autNo; }
           set { autNo = value; }
       }

       private int ntIntNo;

       public int NtIntNo
       {
           get { return ntIntNo; }
           set { ntIntNo = value; }
       }

   }
}
