﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTOService.DAL;


namespace SIL.AARTOService.ExportNoticeNumberAllocationBlock
{
    class ExportNoticeNumberAllocationBlock_Service : ClientService
    {
        public ExportNoticeNumberAllocationBlock_Service()
            : base("", "", new Guid("AD4BA8C0-A779-4F8C-8A72-A150B0773565"))
        {
            this._serviceHelper = new AARTOServiceBase(this);
            connectStr = AARTOBase.GetConnectionString(SIL.ServiceQueueLibrary.DAL.Entities.ServiceConnectionNameList.AARTO, SIL.ServiceQueueLibrary.DAL.Entities.ServiceConnectionTypeList.DB);
            sendBlocksize = new SendBlockSizeToMobile(this.connectStr);
            noticeNumberMobile = new NoticeNumberMoblie(this.connectStr);
            dbHelper = new DBHelper(this.connectStr);
        }
        string connectStr;
        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        SendBlockSizeToMobile sendBlocksize;
        NoticeNumberMoblie noticeNumberMobile;
        DBHelper dbHelper;
        List<NoticePrefix> prefixList;

        int blockSize, imprest;
        string lastUser = "ExportNoticeNumberAllocationBlock";
        //List<NoticeCountObj> noticeList = new List<NoticeCountObj>();//The rest of the notice number


        public override void PrepareWork()
        {
            //connectStr = "Data Source=192.168.1.254;Initial Catalog=AARTO_4499A;User ID=Aarto_user;Password=aarto2010";
            ServiceUtility.InitializeNetTier(connectStr);
            //init mobile connect
            string MobileconnectStr = AARTOBase.GetConnectionString(SIL.ServiceQueueLibrary.DAL.Entities.ServiceConnectionNameList.MobileInterface,
                SIL.ServiceQueueLibrary.DAL.Entities.ServiceConnectionTypeList.DB);
            ServiceUtility.InitializeNetTier(MobileconnectStr, "SIL.MobileInterface");
        }

        public override void InitialWork(ref SIL.QueueLibrary.QueueItem item)
        {
            try
            {   //get queue params
                NoticeNumberQueue numberQueue = new NoticeNumberQueue();
                blockSize = numberQueue.BlockSize;
                imprest = numberQueue.Imprest;
                //Initial value
                blockSize = (blockSize == 0 ? 500 : blockSize);
                imprest = (imprest == 0 ? 100 : imprest);
                //set the transaction time
                //AARTOBase.TransactionTimeoutSeconds = int.MaxValue;
            }
            catch (Exception ex)
            {
                AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorExportNumberInitialWork", "ExportNumberInitialWork", ex, DateTime.Now), true);
            }
        }

        public override void MainWork(ref List<SIL.QueueLibrary.QueueItem> queueList)
        {
            string errorMsg = string.Empty;
            string newPrefix;
            try
            {
                if (CheckMobileFlag("Mobile", "y"))
                {
                    List<AllocationPool> originalList = sendBlocksize.GetTheSendList(imprest, ref errorMsg);
                    List<AllocationPool> list = AllocationPoolAddMoreInfo(originalList, ref errorMsg);
                    prefixList = GetAllNoticePrefix();
                    foreach (var item in list)
                    {
                        item.StartNumber = GetStartNumber(item.AartoAutIntNo, item.TranNoType, ref errorMsg);
                        if (item.StartNumber < 0)
                        {
                            AARTOBase.ErrorProcessing(string.Format("We can't find the document type: {0}", item.MobileDocumentCode));
                            continue;
                        }
                        if (item.StartNumber == 0)//if start number is zero need check the new perfix
                        {
                            int currentPrefixInt = sendBlocksize.validation.GetIntValue(item.Prefix) + 1;
                            newPrefix = sendBlocksize.validation.GetCharFromNumber(currentPrefixInt, item.NtIntNo);
                            if (prefixList.Where(m => m.Nprefix == newPrefix.ToString() && m.AutIntNo != item.AartoAutIntNo).Count() > 0)
                            {
                                string message = "We can't generate notice ticket number pool because the next prefix exists already.Please investigate and create a new prefix!";
                                AARTOBase.ErrorProcessing(string.Format(message + " AutCode:{0} ,Prefix:{1}", item.AutCode, newPrefix));
                                continue;
                            }
                            item.PrefixHistory = item.Prefix; //Jerry 2014-05-15 add PrefixHistory
                            item.Prefix = newPrefix;
                            AddNewPrefix(item, ref errorMsg);
                        }
                        item.StartNumber = (item.StartNumber == 0 ? 1 : item.StartNumber);
                        int totalItems = (item.StartNumber - 1) + item.BlockSize;
                        //Jerry 2014-05-15 change it
                        //if (totalItems > item.MaxNumber)
                        if (totalItems >= item.MaxNumber)
                            GenerateMoreThanMax(item, totalItems, ref errorMsg);
                        else
                            GenerateLessThanMax(item, totalItems, ref errorMsg);
                    }
                }
            }
            catch (Exception ex)
            {
                AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorExportNumberMainWork", "ExportNumberMainWork", errorMsg, DateTime.Now), true);
            }
            finally
            {
                this.MustSleep = true;
            }
        }
        public void GenerateMoreThanMax(AllocationPool item, int totalItems, ref string errorMsg)
        {
            try
            {
                item.EndNumber = item.MaxNumber;
                InsertAARTOAllocationPool(item, ref errorMsg);
                sendBlocksize.InsertMobileAllocationPool(item, ref errorMsg);
                UpdateTranNo(item, ref errorMsg);
                noticeNumberMobile.GenerateNoticeNumber(item, ref errorMsg);

                string newPrefix;
                int currentPrefixInt = sendBlocksize.validation.GetIntValue(item.Prefix) + 1;
                newPrefix = sendBlocksize.validation.GetCharFromNumber(currentPrefixInt, item.NtIntNo);
                if (prefixList.Where(m => m.Nprefix == newPrefix.ToString() && m.AutIntNo != item.AartoAutIntNo).Count() == 0)
                {
                    item.PrefixHistory = item.Prefix; //Jerry 2014-05-15 add PrefixHistory
                    item.Prefix = newPrefix;
                    item.StartNumber = 1;
                    item.EndNumber = totalItems - item.MaxNumber;
                    //
                    AddNewPrefix(item, ref errorMsg);
                    //
                    //Jerry 2014-05-15 don't insert mobile allocation pool if EndNumber == 0
                    if (item.EndNumber != 0)
                    {
                        InsertAARTOAllocationPool(item, ref errorMsg);
                        sendBlocksize.InsertMobileAllocationPool(item, ref errorMsg);
                        noticeNumberMobile.GenerateNoticeNumber(item, ref errorMsg);
                    }

                    UpdateTranNo(item, ref errorMsg);
                }
                else
                {
                    item.EndNumber = -1;//
                    UpdateTranNo(item, ref errorMsg);
                    string message = "We can't generate notice ticket number pool because the next prefix exists already.Please investigate and create a new prefix!";
                    AARTOBase.ErrorProcessing(string.Format(message + " AutCode:{0} ,Prefix:{1}", item.AutCode, newPrefix));
                }
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0} {1}", "GenerateLessThanMax", ex.Message);
                throw new Exception(errorMsg);
            }
        }
        public void GenerateLessThanMax(AllocationPool item, int totalItems, ref string errorMsg)
        {
            try
            {
                item.EndNumber = totalItems;
                //
                InsertAARTOAllocationPool(item, ref errorMsg);
                sendBlocksize.InsertMobileAllocationPool(item, ref errorMsg);
                UpdateTranNo(item, ref errorMsg);
                noticeNumberMobile.GenerateNoticeNumber(item,ref errorMsg);
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0} {1}", "GenerateLessThanMax", ex.Message);
                throw new Exception(errorMsg);
            }
        }
        public void AddNewPrefix(AllocationPool pool,ref string errorMsg)
        {
            try
            {
                NoticePrefix noticePrefix = GetPrefix(pool.AartoAutIntNo, pool.MobileDocumentCode, ref errorMsg);
                NoticePrefixService service = new NoticePrefixService();
                string nphType = string.Empty;
                noticePrefix.Nprefix = pool.Prefix;
                noticePrefix.LastUser = lastUser;
                service.Save(noticePrefix);
                //
                NoticeStatisticalTypeService statisticalService = new NoticeStatisticalTypeService();
                string statisticalCode = string.Empty;
                if (pool.MobileDocumentCode == "S341Mobile")
                {
                    statisticalCode = NoticeStatisticalTypeList.M341.ToString();
                    nphType = "H";
                }
                else if (pool.MobileDocumentCode == "S56Mobile")
                {
                    statisticalCode = NoticeStatisticalTypeList.M56.ToString();
                    nphType = "M";
                }

                if (!string.IsNullOrEmpty(statisticalCode))
                {
                    NoticeStatisticalType statistical = statisticalService.GetByNstCode(statisticalCode);
                    NoticePrefixHistory history = new NoticePrefixHistory();
                    NoticePrefixHistoryService historyService = new NoticePrefixHistoryService();

                    //Jerry 2014-05-15 add history prefix
                    if (historyService.GetByNprefixAndAuthIntNo(pool.PrefixHistory, pool.AartoAutIntNo).Count == 0)
                    {
                        history.NphType = nphType;
                        //history.Nprefix = pool.Prefix;
                        history.Nprefix = pool.PrefixHistory; //Jerry 2014-05-15 add PrefixHistory
                        history.AuthIntNo = pool.AartoAutIntNo;
                        history.NstIntNo = statistical.NstIntNo;
                        history.LastUser = lastUser;

                        historyService.Insert(history);
                    }

                    //Jerry 2014-05-15 add current prefix
                    if (historyService.GetByNprefixAndAuthIntNo(pool.Prefix, pool.AartoAutIntNo).Count == 0)
                    {
                        history.NphType = nphType;
                        history.Nprefix = pool.Prefix;
                        history.AuthIntNo = pool.AartoAutIntNo;
                        history.NstIntNo = statistical.NstIntNo;
                        history.LastUser = lastUser;

                        historyService.Insert(history);
                    }
                }
            }
            catch (SqlException ex)
            {
                errorMsg = string.Format("The prefix is exsits.Prefix:{0} ,AutCode:{1},AutIntNo:{2}, message:{3}",
                    pool.Prefix, pool.AutCode, pool.AartoAutIntNo, ex.Message);
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0} : {1}", "AddNewPrefix", ex.Message);
                throw new Exception(errorMsg);
            }
        }

        public List<AllocationPool> AllocationPoolAddMoreInfo(List<AllocationPool> list, ref string errorMsg)
        {
            List<AllocationPool> flagList = new List<AllocationPool>();
            foreach (var item in list)
            {
                try
                {
                    Authority authority = GetAuthrityByAutCode(item.AutCode, ref errorMsg);
                    TransactionType documentType = GetTransactionType(item.MobileDocumentCode);
                    NoticePrefix noticePrefix = GetPrefix(authority.AutIntNo, documentType.TtCode, ref errorMsg);
                    //Seawen 2013-08-28 write correct error logs
                    if (authority == null)
                    {
                        throw new NullReferenceException(string.Format("{0}:Can't find autcode {1} in aarto database \r\n", "AllocationPoolAddMoreInfo", item.AutCode));
                    }
                    if (documentType == null)
                    {
                        throw new NullReferenceException(string.Format("{0}:Can't find docment type code {1} in aarto database \r\n", "AllocationPoolAddMoreInfo", item.MobileDocumentCode));
                    }
                    if (noticePrefix == null)
                    {
                        throw new NullReferenceException(string.Format("{0}:Can't find notice prefix code in aarto database by autcode {1} and document type code {2} \r\n", "AllocationPoolAddMoreInfo", item.AutCode, item.MobileDocumentCode));
                    }
                    item.AartoAutIntNo = authority.AutIntNo;
                    item.AutNo = authority.AutNo;
                    item.ArrtoDocumentId = documentType.TtIntNo;
                    item.Prefix = noticePrefix.Nprefix;
                    item.NtIntNo = noticePrefix.NtIntNo;
                    item.BlockSize = blockSize;

                    sendBlocksize.rules.InitParameter(this.connectStr, item.AutCode);
                    item.AutRuleDetail = sendBlocksize.rules.Rule("9300");
                    item.MaxNumber = sendBlocksize.GetMaxNumber(item.AutRuleDetail.ARNumeric);
                    flagList.Add(item);
                }
                catch (NullReferenceException ex)
                {
                    AARTOBase.ErrorProcessing(ex.Message);
                }
                catch (Exception ex)
                {
                    errorMsg = string.Format("{0}:{1}\r\n", "AllocationPoolAddMoreInfo", ex.Message);
                    throw new Exception(errorMsg);
                }
            }
            return flagList;
        }

        #region not base method
        public void InsertAARTOAllocationPool(AllocationPool item,ref string errorMsg)
        {
            try
            {
                NoticeNumberAllocationPoolService number_Service = new NoticeNumberAllocationPoolService();
                NoticeNumberAllocationPool pool = new NoticeNumberAllocationPool();
                pool.AutIntNo = item.AartoAutIntNo;
                pool.DoTyIntNo = item.ArrtoDocumentId;
                pool.NnapStartingNumber = item.StartNumber;
                pool.NnapFinishingNumber = item.EndNumber;
                pool.NnapSentDate = item.CurrentDate;
                pool.NnapNoticePrefix = item.Prefix;
                pool.LastUser = AARTOBase.LastUser;
                number_Service.Save(pool);
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "InsertAARTOAllocationPool",ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        public NoticePrefix GetPrefix(int authorityId, string DtCode, ref string errorMsg)
        {
            try
            {
                NoticePrefixService prefix_Service = new NoticePrefixService();
                NoticePrefixQuery query = new NoticePrefixQuery();
                int DocumentTypeId = GetAARTONoticeTypeId(DtCode).NtIntNo;
                query.Append(NoticePrefixColumn.NtIntNo, DocumentTypeId.ToString());
                query.Append(NoticePrefixColumn.AutIntNo, authorityId.ToString());
                TList<NoticePrefix> list = prefix_Service.Find(query as IFilterParameterCollection);
                if (list.Count > 0)
                    return list.ToList()[0];
                else
                    return null;
            }
            catch (Exception ex)
            {
                if (errorMsg == "")
                    errorMsg = string.Format("{0}:{1}", "GetPrefix", "GetPrefix:no data find " + ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        public List<NoticePrefix> GetAllNoticePrefix()
        {
            NoticePrefixService Service = new NoticePrefixService();
            return Service.GetAll().ToList();
        }
        public static NoticeType GetAARTONoticeTypeId(string DtCode)
        {
            NoticeTypeService nt_Service = new NoticeTypeService();
            NoticeType nt=nt_Service.GetByNtCode(DtCode);
            return nt;
        }
        public int GetStartNumber(int authorityId, string DtCode, ref string errorMsg)
        {
            try
            {
                /*
                int mtrNo = GetSuthorityNo(authorityId,ref errorMsg).MtrIntNo;
                int traType = GetTransactionType(DtCode).TtIntNo;
                MetroTransactionService mt_Service = new MetroTransactionService();
                MetroTransactionQuery query = new MetroTransactionQuery();
                query.Append(MetroTransactionColumn.MetroIntNo, mtrNo.ToString());
                query.Append(MetroTransactionColumn.TtIntNo, traType.ToString());
                TList<MetroTransaction> mt = mt_Service.Find(query as IFilterParameterCollection);
                if (mt.Count > 0)
                    return mt[0].MtNumber;
                else
                    errorMsg = "GetStartNumber:no data find";*/
                int startNo;
                SqlParameter[] parameters=new SqlParameter[3];
                parameters[0]=new SqlParameter("@AutIntNo",authorityId);
                parameters[1]=new SqlParameter("@TNType",DtCode);
                parameters[2]=new SqlParameter("@LastUser",lastUser);
                startNo=Convert.ToInt32(dbHelper.ExecuteScalar("GetTranStartNoOfAuthority", parameters, out errorMsg));
                return startNo;
            }
            catch (Exception ex)
            {
                if(string.IsNullOrEmpty(errorMsg))
                errorMsg = string.Format("{0}:{1}", "GetStartNumber", ex.ToString());
            }
            throw new Exception(errorMsg);
        }
        public static TransactionType GetTransactionType(string DtCode)
        {
            TransactionTypeService Service = new TransactionTypeService();
            TransactionType tType=Service.GetByTtCode(DtCode);
            return tType;
        }
        private NoticeType GetNoticeType(string code)
        {
            NoticeTypeService service = new NoticeTypeService();
            return service.GetByNtCode(code);
        }
        public static Authority GetSuthorityNo(int authorityId,ref string errorMsg)
        {
            try
            {
                AuthorityService aut_Service = new AuthorityService();
                return aut_Service.GetByAutIntNo(authorityId);
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "GetSuthorityNo", ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        public static Authority GetAuthrityByAutCode(string autCode, ref string errorMsg)
        {
            try
            {
                AuthorityService aut_Service = new AuthorityService();
                return aut_Service.GetByAutCode(autCode);
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "GetSuthorityNo", ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        public void UpdateTranNo(AllocationPool pool,ref string errorMsg)
        {
            try
            {
                /*
                int mtrNo = GetSuthorityNo(pool.AartoAutIntNo, ref errorMsg).MtrIntNo;
                int traType = pool.ArrtoDocumentId;
                MetroTransactionService mt_Service = new MetroTransactionService();
                MetroTransactionQuery query = new MetroTransactionQuery();
                query.Append(MetroTransactionColumn.MetroIntNo, mtrNo.ToString());
                query.Append(MetroTransactionColumn.TtIntNo, traType.ToString());
                TList<MetroTransaction> mt = mt_Service.Find(query as IFilterParameterCollection);
                mt[0].MtNumber = pool.EndNumber; //(pool.EndNumber == pool.MaxNumber ? 1 : (pool.EndNumber + 1));
                mt_Service.Update(mt[0]);*/
                int startNumber = ((pool.EndNumber + 1) > pool.MaxNumber ? 1 : (pool.EndNumber + 1));
                TranNoService service = new TranNoService();
                TranNoQuery query = new TranNoQuery();
                query.Append(TranNoColumn.AutIntNo, pool.AartoAutIntNo.ToString());
                query.Append(TranNoColumn.TnType, pool.TranNoType);
                TranNo tranNo = service.Find(query as IFilterParameterCollection)[0];
                tranNo.Tnumber = startNumber;//store the next number
                tranNo.LastUser = lastUser;

                service.Update(tranNo);
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "UpdateMetroTransaction", ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        public static bool CheckMobileFlag(string parameter, string parameterValue)
        {
            bool flag = false;
            NoticeNumberQueue queue = new NoticeNumberQueue();
            queue.SepKey = parameter;
            string mobileFlag = queue.SepKey.ToLower();
            if (mobileFlag.ToLower() == parameterValue)
                flag = true;
            return flag;
        }
        #endregion

    }
}
