﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIL.MobileInterface.DAL.Data;
using SIL.MobileInterface.DAL.Entities;
using SIL.MobileInterface.DAL.Services;
using SIL.AARTOService.Library;
using SIL.AARTO.BLL.Utility;
using TMS = Stalberg.TMS;
using Stalberg.TMS.Data.Util;
using AARTOEntities = SIL.AARTO.DAL.Entities;

namespace SIL.AARTOService.ExportNoticeNumberAllocationBlock
{
    class NoticeNumberMoblie
    {
        public NoticeNumberMoblie(string connStr)
        {
            this.connStr = connStr;
            sendBlocksize = new SendBlockSizeToMobile(this.connStr);
        }
        string connStr;
        string lastUser = "ExportNoticeNumberAllocationBlock";
        SendBlockSizeToMobile sendBlocksize;
        TMS.AuthorityRulesDetails autRuleDetail;

        public List<NoticeNumberAllocationPool> GetNoticeNumberPoolList()
        {
            NoticeNumberAllocationPool pool = new NoticeNumberAllocationPool();
            NoticeNumberAllocationPoolService service = new NoticeNumberAllocationPoolService();
            NoticeNumberAllocationPoolQuery query = new NoticeNumberAllocationPoolQuery();
            query.AppendIsNull(NoticeNumberAllocationPoolColumn.NnapDateReceived);
            return service.Find(query as IFilterParameterCollection).ToList();
        }
        public void GenerateNoticeNumber(AllocationPool itemPool, ref string errorMsg)
        {
            NoticeNumberAllocationPool currentItem = new NoticeNumberAllocationPool();
            try
            {
                NoticeNumberAllocationPoolService service = new NoticeNumberAllocationPoolService();
                List<NoticeNumberAllocationPool> list = GetNoticeNumberPoolList();
                foreach (var item in list)
                {
                    currentItem = item;
                    Authority authority = GetAuthority(item.AuthorityId);
                    sendBlocksize.rules.InitParameter(this.connStr, authority.AutCode);
                    autRuleDetail=sendBlocksize.rules.Rule("9300");

                    NoticeNumberSentToMobiledeviceService notice_Service = new NoticeNumberSentToMobiledeviceService();
                    TList<NoticeNumberSentToMobiledevice> noticeNumberlist = new TList<NoticeNumberSentToMobiledevice>();
                    for (int i = itemPool.StartNumber; i <= item.NnapFinishNumber; i++)
                    {
                        NoticeNumberSentToMobiledevice noticeNumber = new NoticeNumberSentToMobiledevice();
                        noticeNumber.Nnapid = item.Nnapid;
                        noticeNumber.NnsmdNoticeNumber = GetCompleteServiceNo(autRuleDetail, item.NnapNumberPrefix, i, authority.AutNo);
                        noticeNumber.LastUser = lastUser;
                        noticeNumberlist.Add(noticeNumber);
                    }
                    if (noticeNumberlist.Count > 0)
                        notice_Service.Save(noticeNumberlist);
                    //
                    item.NnapDateReceived=DateTime.Now;
                    item.LastUser = lastUser;   // 2013-07-19 add by Henry
                    service.Update(item);
                }
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("Mobile notice number repeat,{0} AutIntNo:{1} Prefix:{2} message:{3}", "GenerateNoticeNumber", currentItem.AuthorityId, currentItem.NnapNumberPrefix, ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        private string GetCompleteServiceNo(TMS.AuthorityRulesDetails ruleDetail, string prefix, int number, string authorityNo)
        {
            char cTemp='0';
            int cdvLength = 6;//fixed value
            int length = ruleDetail.ARNumeric;
            string serialNumber = number.ToString().PadLeft(length, cTemp);
            string cdvStr = string.Empty;
            TicketNumber.ConnectionString = this.connStr;//2014-11-06 Heidi added for generating correct TicketNumber
            int cdvNumber = TicketNumber.GetCdvOfTicketNumber(number, prefix, authorityNo); 
            if (ruleDetail.ARString.ToUpper().Equals("Y"))
                cdvStr = cdvNumber.ToString().PadLeft(cdvLength, cTemp);
            else
                cdvStr = cdvNumber.ToString();
            return string.Format("{0}/{1}/{2}/{3}", prefix.Trim(), serialNumber, authorityNo.Trim(), cdvStr);
        }
  
        public Authority GetAuthority(int AutId)
        {
            AuthorityService au_Service=new AuthorityService();
            return au_Service.GetByAuthorityId(AutId);
        }
    }
}
