﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.ExportNoticeNumberAllocationBlock
{
    public partial class ExportNoticeNumberAllocationBlock : ServiceHost
    {
        public ExportNoticeNumberAllocationBlock()
            : base("", new Guid("D51E77EC-2516-4E2C-A9BB-802C1E29597E"), new ExportNoticeNumberAllocationBlock_Service())
        {
            InitializeComponent();
        }
    }
}
