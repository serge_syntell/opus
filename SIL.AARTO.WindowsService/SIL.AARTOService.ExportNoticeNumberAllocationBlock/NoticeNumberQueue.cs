﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceQueueLibrary.DAL.Data;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.ServiceQueueLibrary.DAL.Services;

namespace SIL.AARTOService.ExportNoticeNumberAllocationBlock
{
    class NoticeNumberQueue
    {
        private int _blockSize;
        private int _imprest;
        private string _sepKey;
        public string SepKey
        {
            get
            {
                return GetTheSepKey(_sepKey);
            }
            set
            {
                _sepKey = value;
            }
        }
        private List<ServiceParameter> list = new List<ServiceParameter>();
        public NoticeNumberQueue()
        {
            GetInitialParammeters(ref _blockSize, ref _imprest);
            InitServiceParams(ref list);
        }

        #region Member
        public int BlockSize
        {
            get
            {
                return _blockSize;
            }
        }
        public int Imprest
        {
            get
            {
                return _imprest;
            }
        }
        #endregion

        public string GetTheSepKey(string sepKey)
        {
            string flag = string.Empty;
            foreach (ServiceParameter item in list)
            {
                if (item.SePaKey.ToLower() == sepKey.ToLower())
                {
                    flag = item.SePaValue;
                    break;
                }
            }
            return flag;
        }
        public void InitServiceParams(ref  List<ServiceParameter> spList)
        {
            ServiceParameterService sps = new ServiceParameterService();
            int serviceHostId = GetHostId(new Guid("D51E77EC-2516-4E2C-A9BB-802C1E29597E"));
            spList = sps.GetBySeHoIntNo(serviceHostId).ToList();
        }
        public void GetInitialParammeters(ref int blockSize, ref int imprest)
        {
            List<ServiceParameter> spList = new List<ServiceParameter>();
            ServiceParameterService sps = new ServiceParameterService();
            int serviceHostId = GetHostId(new Guid("D51E77EC-2516-4E2C-A9BB-802C1E29597E"));
            spList = sps.GetBySeHoIntNo(serviceHostId).ToList();
            for (int i = 0; i < spList.Count; i++)
            {
                if (spList[i].SePaKey.ToLower() == "blocksize")
                    blockSize = Convert.ToInt32(spList[i].SePaValue);
                else if (spList[i].SePaKey.ToLower() == "imprest")
                    imprest = Convert.ToInt32(spList[i].SePaValue);
            }
        }
        public int GetHostId(Guid guid)
        {
            ServiceHost sh = new ServiceHost();
            ServiceHostService shs = new ServiceHostService();
            sh = shs.GetBySeHoUuid(guid);
            if (sh != null)
                return sh.SeHoIntNo;
            else
                return 0;
        }
    }
}
