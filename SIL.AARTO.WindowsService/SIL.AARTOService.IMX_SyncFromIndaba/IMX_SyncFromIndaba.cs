﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.IMX_SyncFromIndaba
{
    public partial class IMX_SyncFromIndaba : ServiceHost
    {
        public IMX_SyncFromIndaba():
            base("",new Guid("71A5BCA8-F783-42CA-BE88-C230EEC72307"),new IMX_SyncFromIndaba_Service())
        {
            InitializeComponent();
        }
    }
}
