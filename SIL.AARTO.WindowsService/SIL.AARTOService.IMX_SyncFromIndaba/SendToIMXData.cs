﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Reflection;
using SIL.AARTOService.Library;

namespace SIL.AARTOService.IMX_SyncFromIndaba
{
    class SendToIMXData
    {
        public static void ImportTOIMX(ref List<string> errorMsgList)
        {
            string errorMsg = string.Empty;
            try
            {
                //get all the files and mapping to maintenance object
                List<Maintenance> list = new List<Maintenance>();
                foreach (string file in GetFileName())
                {
                    CreateMapping(file, ref list, ref errorMsg);
                    ClearFile(file);
                }
                //push to imx data
                //string currentPath = AppDomain.CurrentDomain.BaseDirectory + "AARTOMappingIMX.xml";
                //XElement xTree = XElement.Load(currentPath);

                PushDataToIMX puchDataToIMX = new PushDataToIMX(ref errorMsg);
                puchDataToIMX.ARRTOMappingIMX(list, ref errorMsgList);
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    throw new Exception(errorMsg);
                else
                    throw new Exception(ex.ToString());
            }
        }
        public static void CreateMapping(string file, ref List<Maintenance> list,ref string errorMsg)
        {
            try
            {
                XElement xTree = XElement.Load(file);
                IEnumerable<XElement> elements = xTree.Elements();
                foreach (XElement element in elements)
                {
                    if (element.Name == "RowInfo")
                    {
                        Maintenance maintenance = new Maintenance();
                        IEnumerable<XElement> rowNodes = element.Elements();
                        foreach (XElement node in rowNodes)
                        {
                            PropertyInfo properInfo = maintenance.GetType().GetProperty(node.Name.ToString(), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                            if (properInfo != null)
                            {
                                if (node.Name.ToString().ToLower() == properInfo.Name.ToLower())
                                {
                                    if (properInfo.PropertyType.Name == "Int32")
                                        properInfo.SetValue(maintenance, Convert.ToInt32(node.Value), null);
                                    else if (properInfo.PropertyType.Name == "Boolean")
                                        properInfo.SetValue(maintenance, ConvertStringToBool(node.Value), null);
                                    else
                                        properInfo.SetValue(maintenance, node.Value, null);
                                }
                            }
                        }
                        list.Add(maintenance);
                    }
                }
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "CreateMapping", ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        #region base code
        public static void ClearFile(string fileName)
        {
            if (File.Exists(fileName))
                File.Delete(fileName);
        }
        public static List<string> GetFileName()
        {
            string tempDirectory =DoIndaba.GetIndabaTemplePath();
            List<string> files = new List<string>();
            if (!Directory.Exists(tempDirectory))
                Directory.CreateDirectory(tempDirectory);
            DirectoryInfo dir = new DirectoryInfo(tempDirectory);
            foreach (FileInfo file in dir.GetFiles("*.imx"))
            {
                files.Add(file.FullName);
            }
            return files;
        }
        public static bool ConvertStringToBool(string str)
        {
            bool flag = false;
            str = str.ToLower();
            switch (str)
            {
                case "1":
                case "y":
                    flag = true;
                    break;
            }
            return flag;
        }
        #endregion
    }
}
