﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;
using SIL.AARTOService.Library;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Resource;

namespace SIL.AARTOService.IMX_SyncFromIndaba
{
    class IMX_SyncFromIndaba_Service:ClientService
    {
        public IMX_SyncFromIndaba_Service()
            : base("", "", new Guid("2E08E3D2-3BBA-40F3-BCD4-719ED7498F15"))
        {
            this._serviceHelper =new AARTOServiceBase(this,false);
        }
        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }

        public override void PrepareWork()
        {
            //base.PrepareWork();
        }
        public override void InitialWork(ref QueueLibrary.QueueItem item)
        {
            //base.InitialWork(ref item);
        }
        public override void MainWork(ref List<QueueLibrary.QueueItem> queueList)
        {
            string errorMsg = string.Empty;
            List<string> errorMsgList = new List<string>();
            int importCount = 0;
            try
            {
                DoIndaba.InitIndaba(ref errorMsg);
                //generated xml files
                DoIndaba.retrieveFileDataFromIndabaDB(ref errorMsg,ref importCount);
                DoIndaba.DisposeIndaba();
                //parse the xlm files and send to imx data
                if (importCount > 0)
                    SendToIMXData.ImportTOIMX(ref errorMsgList);
                foreach (string error in errorMsgList)
                {
                    AARTOBase.ErrorProcessing(error);
                }
                Logger.Info("Success importing "+importCount.ToString()+" files !");
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(errorMsg))
                    AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorMainWork", "IMX_SyncFromIndaba", errorMsg, DateTime.Now), true);
                else
                    AARTOBase.ErrorProcessing(ex.ToString(), true);
            }
            finally
            {
                errorMsgList.Clear();
                this.MustSleep = true;
            }
        }

        #region base method
        public string GetIndabaConnectstring()
        {
            return AARTOBase.GetConnectionString(ServiceConnectionNameList.Indaba, ServiceConnectionTypeList.DB);
        }
        public string GetFileTempleSavePath()
        {
            return AARTOBase.GetConnectionString(ServiceConnectionNameList.FolderForIndabaReceivedFile, ServiceConnectionTypeList.UNC);
        }
        public void LogginInfo(string message)
        {
            Logger.Info(message); 
        }
        #endregion
    }
}
