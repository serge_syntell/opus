﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.IMX_SyncFromIndaba
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.IMX_SyncFromIndaba",
                DisplayName = "SIL.AARTOService.IMX_SyncFromIndaba",
                Description = "SIL AARTOService IMX_SyncFromIndaba"
            };
            ProgramRun.InitializeService(new IMX_SyncFromIndaba() , serviceDescriptor, args);
        }
    }
}
