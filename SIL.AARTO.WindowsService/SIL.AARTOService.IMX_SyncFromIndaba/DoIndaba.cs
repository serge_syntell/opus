﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.Indaba;
using System.Configuration;
using System.IO;
using System.Collections;

namespace SIL.AARTOService.IMX_SyncFromIndaba
{
    class DoIndaba
    {
        public static Stalberg.Indaba.ICommunicate indaba;
        public static void InitIndaba(ref string errorMsg)
        {
            try
            {
                Stalberg.Indaba.Client.ApplicationName = "ExportStaticDataFromIndabaToIMX";
                //string con = "Data Source=192.168.1.254;Initial Catalog=Indaba;User ID=indaba1;Password=c$y@W@m!hLT1";
                Stalberg.Indaba.Client.ConnectionString = GetIndabaConn();
                Stalberg.Indaba.Client.Initialise();
                indaba = Stalberg.Indaba.Client.Create();
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "InitIndaba", ex.ToString());
                throw new Exception(errorMsg);
            }
        }

        public static void DisposeIndaba()
        {
            indaba.Dispose();
            Stalberg.Indaba.Client.Dispose();
        }
        public static void retrieveFileDataFromIndabaDB(ref string errorMsg,ref int iImportCount)
        {
            try
            {
                IndabaFileInfo[] fileInfoList;
                fileInfoList = indaba.GetFileInformation(".imx");
                int unpackCount = Convert.ToInt32(GetKeyValue("UnPackCount"));
                iImportCount = ImprotToDB(indaba, fileInfoList, unpackCount,ref errorMsg);
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "retrieveFileDataFromIndabaDB", ex.ToString());
                throw new Exception(errorMsg);
            }
        }

        private static int ImprotToDB(Stalberg.Indaba.ICommunicate indaba, IndabaFileInfo[] fileInfoList, int unpackCount, ref string errorMsg)
        {
            string fullFileName;
            int i = 0;
            IMX_SyncFromIndaba_Service service = new IMX_SyncFromIndaba_Service();
            foreach (IndabaFileInfo indabaFile in fileInfoList)
            {
                fullFileName = GetFileFromIndabaDB(indabaFile, indaba);
                try
                {
                    DequeueIndabaFileData(indabaFile, indaba);
                    i++;
                    if (i >= unpackCount)
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    service.LogginInfo(ex.ToString());
                    continue;
                }
            }
            return i;
        }
        public static string GetFileFromIndabaDB(IndabaFileInfo indabaFile, Stalberg.Indaba.ICommunicate indaba)
        {
            string tempDirectory = GetIndabaTemplePath();
            string fullFileName = Path.Combine(tempDirectory, indabaFile.Name);
            try
            {
                if (!Directory.Exists(tempDirectory))
                {
                    Directory.CreateDirectory(tempDirectory);
                }

                FileStream file = File.Create(fullFileName);
                MemoryStream stream = (MemoryStream)indaba.Peek(indabaFile);

                stream.WriteTo(file);
                file.Flush();
                file.Close();
            }
            catch (Exception ex)
            {
                return null;
            }
            return fullFileName;
        }
        public static void DequeueIndabaFileData(IndabaFileInfo indabaFile, Stalberg.Indaba.ICommunicate indaba)
        {
            indaba.Dequeue(indabaFile);
        }
        #region base method
        public static string GetIndabaConn()
        {
            IMX_SyncFromIndaba_Service service = new IMX_SyncFromIndaba_Service();
            return service.GetIndabaConnectstring();
        }
        public static string GetIndabaTemplePath()
        {
            IMX_SyncFromIndaba_Service service = new IMX_SyncFromIndaba_Service();
            return service.GetFileTempleSavePath();
        }
        public static string GetKeyValue(string key)
        {
            Hashtable ht = GetSIL_QueueInfo.GetServiceParams();
            if (ht.Count > 0)
                return ht[key].ToString();
            else
                return "";

        }
        #endregion
    }
}
