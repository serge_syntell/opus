﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceQueueLibrary.DAL.Data;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.ServiceQueueLibrary.DAL.Services;
using System.Collections;

namespace SIL.AARTOService.IMX_SyncFromIndaba
{
    class GetSIL_QueueInfo
    {
        public static Hashtable GetServiceParams()
        {
            List<ServiceParameter> list = new List<ServiceParameter>();
            ServiceParameterService sps = new ServiceParameterService();
            int serviceHostId = Static_GetHostId(new Guid("71A5BCA8-F783-42CA-BE88-C230EEC72307"));
            list = sps.GetBySeHoIntNo(serviceHostId).ToList();
            Hashtable ht = new Hashtable();
            foreach (ServiceParameter paramter in list)
            {
                ht.Add(paramter.SePaKey, paramter.SePaValue);
            }
            return ht;
        }
        public static int Static_GetHostId(Guid guid)
        {
            ServiceHost sh = new ServiceHost();
            ServiceHostService shs = new ServiceHostService();
            sh = shs.GetBySeHoUuid(guid);
            if (sh != null)
                return sh.SeHoIntNo;
            else
                return 0;
        }
    }
}
