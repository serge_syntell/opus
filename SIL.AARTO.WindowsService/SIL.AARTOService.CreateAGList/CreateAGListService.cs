﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using Stalberg.TMS;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF;
using SIL.AARTOService.Resource;

namespace SIL.AARTOService.CreateAGList
{
    public class CreateAGListService : ClientService
    {
        private const string TICKET_PROCESSOR = "TMS";

        public CreateAGListService()
            : base("", "", new Guid("04F5A636-2569-4E11-B578-9B556BAF0FD8"))
        {
            this._serviceHelper = new AARTOServiceBase(this, false);
            // Add the DynamicPDF License
            ceTe.DynamicPDF.Document.AddLicense("DPS70NPDFJJGEJFZ9ikRGHBua2M3Sl0Pwtw63QxBds5agxfAQMSTfCfEtiI2O6I13jHEsL6smMhYn63QJBLCZ3B8XtMbaTRJ+C1w");
            //get connect string
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);

            agListDB = new AGListdb(this.connectStr);
            authorityDB = new AuthorityDB(this.connectStr);
            metroDB = new MetroDB(this.connectStr);

            AARTOBase.OnServiceStarting = GetServiceParameters;
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        string connectStr = string.Empty;

        //bool canRunCreateAGList = false;

        AGListdb agListDB = null;
        AuthorityDB authorityDB = null;
        MetroDB metroDB = null;

        string autName = string.Empty;
        int autIntNo = 0;
        //Jerry 2012-04-11 add EmailToAdministrator
        //string emailToCustomer = string.Empty;
        string emailToAdministrator = string.Empty;
        string autTicketProcessor = string.Empty;
        string mtrName = string.Empty;
        int mtrIntNo = 0;

        DateTime currentDateTime = DateTime.Now;
        string currentDate = DateTime.Today.ToString("yyyy-MM-dd");

        // jerry 2012-02-24 add
        readonly List<string> attachments = new List<string>();
        string fileFullName = string.Empty;
        // Constants
        private const string SUBJECT = "Create AG List";

        int processRowCount;

        public override void InitialWork(ref QueueItem item)
        {
            #region 2013-08-22, Oscar moved them into onServiceStarting
            //get to email address
            //GetServiceParameters();

            //agListDB = new AGListdb(this.connectStr);
            //authorityDB = new AuthorityDB(this.connectStr);
            //metroDB = new MetroDB(this.connectStr);
            //attachments = new List<string>();
            #endregion
            attachments.Clear();
        }

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            bool failed = true;

            //get metro list
            List<MetroDetails> metroDetailsList = new List<MetroDetails>();
            using (SqlDataReader reader = metroDB.GetMetroList("MtrCode"))
            {
                while (reader.Read())
                {
                    MetroDetails metroDetails = new MetroDetails();
                    metroDetails.MtrIntNo = Convert.ToInt32(reader["MtrIntNo"]);
                    metroDetails.MtrName = reader["MtrName"].ToString();
                    metroDetailsList.Add(metroDetails);
                }
            }

            foreach (MetroDetails metroDetails in metroDetailsList)
            {
                mtrName = metroDetails.MtrName;
                mtrIntNo = metroDetails.MtrIntNo;

                //remove valid run date time, now schedule control it
                //canRunCreateAGList = this.IsAGListRunDateTimeValid();

                //if (canRunCreateAGList)
                //{
                //failed = this.PopulateAGListTable();
                // 2013-08-06, Oscar added batch
                failed = PopulateAGListTable_Batched();

                //This service only generates AGNumber by Seawen 2013-04-18
                //If want to see history code please see svn version
            }
            
            MustSleep = true;// jerry 2012-02-24 change
        }

        //Jerry 2012-04-11 change from emailToCustomer to emailToAdministrator
        private void GetServiceParameters()
        {
            if (ServiceParameters != null)
            {
                if (string.IsNullOrEmpty(this.emailToAdministrator)
&& ServiceParameters.ContainsKey("EmailToAdministrator") && !string.IsNullOrEmpty(ServiceParameters["EmailToAdministrator"]))
                {
                    this.emailToAdministrator = ServiceParameters["EmailToAdministrator"];
                    if (!SendEmailManager.CheckEmailAddress(this.emailToAdministrator))
                    {
                        // jerry 2012-02-21 change
                        //this.Logger.Error(ResourceHelper.GetResource("ErrorEmailAddrInCreateAGList"));
                        //StopService();
                        AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorEmailAddrInCreateAGList"), true);
                    }
                }
                else if (string.IsNullOrEmpty(this.emailToAdministrator) || !ServiceParameters.ContainsKey("EmailToAdministrator") || string.IsNullOrEmpty(ServiceParameters["EmailToAdministrator"]))
                {
                    //this.Logger.Error(ResourceHelper.GetResource("ErrorNoAGListEmailInCreateAGList"));
                    //StopService();
                    AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorNoAGListEmailInCreateAGList"), true);
                }
            }
            else
            {
                //this.Logger.Error(ResourceHelper.GetResource("ErrorNoAGListEmailInCreateAGList"));
                //StopService();
                AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorNoAGListEmailInCreateAGList"), true);
            }

            // 2013-08-06, Oscar added batch
            if (this.processRowCount <= 0)
            {
                string rowCount;
                this.processRowCount = ServiceParameters != null
                    && ServiceParameters.TryGetValue("ProcessRowCount", out rowCount)
                    && !string.IsNullOrWhiteSpace(rowCount)
                    && ServiceUtility.IsNumeric(rowCount = rowCount.Trim())
                    ? Convert.ToInt32(rowCount) : 500;
            }
            Logger.Info(ResourceHelper.GetResource("CurrentProcessRowCount", this.processRowCount));
        }

        //private bool IsAGListRunDateTimeValid()
        //{
        //    bool runProc = false;
        //    string errMessage = string.Empty;

        //    DateTime lastRunDate = agListDB.GetAGListLastRunDate(this.autIntNo, ref errMessage);

        //    int diffDay = DateTime.Compare(this.currentDateTime, lastRunDate);

        //    //set the run time
        //    if (diffDay >= 1 & currentDateTime.Hour > 18 || currentDateTime.Hour < 6)
        //    {
        //        runProc = true;
        //    }

        //    return runProc;
        //}

        private bool PopulateAGListTable(out int noOfRecords)
        {
            bool failed = false;

            string errMessage = string.Empty;
            //noOfRecords = agListDB.PopulateAGList_WS(this.mtrIntNo, ref errMessage, this.currentDateTime.Date, this.AARTOBase.LastUser, this.processRowCount);
            // 2013-11-19, Oscar changed
            noOfRecords = agListDB.PopulateAGList_WS2(this.mtrIntNo, this.processRowCount, AARTOBase.LastUser, out errMessage);

            if (noOfRecords < 0)
            {
                // 2013-11-19, Oscar added
                switch (noOfRecords)
                {
                    case -1:
                        errMessage = string.Format(ResourceHelper.GetResource("ErrorPopulateAGListInCreateAGList", ResourceHelper.GetResource("FailedInProcessingReversedReceipts"))) + errMessage;
                        break;
                    case -2:
                        errMessage = string.Format(ResourceHelper.GetResource("ErrorPopulateAGListInCreateAGList", ResourceHelper.GetResource("FailedInPreparingReceiptsData"))) + errMessage;
                        break;
                    case -3:
                        errMessage = string.Format(ResourceHelper.GetResource("ErrorPopulateAGListInCreateAGList", ResourceHelper.GetResource("FailedInGettingTransactionType"))) + errMessage;
                        break;
                    case -4:
                        errMessage = string.Format(ResourceHelper.GetResource("ErrorPopulateAGListInCreateAGList", ResourceHelper.GetResource("FailedInGettingTransactionNumber"))) + errMessage;
                        break;
                    case -5:
                        errMessage = string.Format(ResourceHelper.GetResource("ErrorPopulateAGListInCreateAGList", ResourceHelper.GetResource("FailedInUpdatingCourtTranNoListAvailable"))) + errMessage;
                        break;
                    case -6:
                        errMessage = string.Format(ResourceHelper.GetResource("ErrorPopulateAGListInCreateAGList", ResourceHelper.GetResource("FailedInCreatingAGNumberForReceipt"))) + errMessage;
                        break;
                    case -99:
                    default:
                        errMessage = string.Format(ResourceHelper.GetResource("ErrorPopulateAGListInCreateAGList", noOfRecords)) + errMessage;
                        break;
                }

                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorPopulateAGListInCreateAGList"), errMessage));
                //AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ErrorPopulateAGListInCreateAGList"), errMessage));
                AARTOBase.LogProcessing(errMessage, LogType.Error, ServiceOption.Break);
                failed = true;
            }
            else if (noOfRecords == 0)
            {
                this.Logger.Info(string.Format(ResourceHelper.GetResource("InfoNoRowsInCreateAGList"), this.mtrName));
                //failed = true;
            }
            else
            {
                this.Logger.Info(string.Format(ResourceHelper.GetResource("InfoSucessfulInCreateAGList"), noOfRecords.ToString()));
            }

            return failed;
        }

        // 2013-08-06, Oscar added batch
        bool PopulateAGListTable_Batched()
        {
            bool failed;
            var rowCount = -1;

            while (true)
            {
                try
                {
                    using (var scope = ServiceUtility.CreateTransactionScope())
                    {
                        failed = PopulateAGListTable(out rowCount);
                        if (!failed
                            && Transaction.Current != null
                            && Transaction.Current.TransactionInformation.Status == TransactionStatus.Active)
                            scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ex.ToString());
                    failed = true;
                }

                if (failed || rowCount < this.processRowCount)
                    break;
            }

            return failed;
        }

        /// <summary>
        /// Creates the Acknowledgement of Guilt (AG) PDF Report using DynamicPDF Software
        /// BD include Spot Fine report - actually might be rewriting this.....
        /// </summary>
        private bool CreateAGReport(out string fileFullName)
        {
            bool failed = false;
            fileFullName = string.Empty;

            try
            {
                //if (emailToCustomer.Equals(string.Empty))
                //{
                //    this.Logger.Error(ResourceHelper.GetResource("ErrorNoAGListEmailInCreateAGList"));
                //    StopService();
                //    failed = true;
                //    return failed;
                //}

                string tempFilePath = Path.Combine(Path.GetTempPath(), "AGList_" + autName + "_" + currentDate + ".pdf");
                FileInfo file = new FileInfo(tempFilePath);
                if (file.Exists)
                    file.Delete();

                // Get the DynamicPDF Report template with witch to create a new Report.
                //Jerry 2012-06-29 get report file from Report folder
                //string reportName =
                //    Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) +
                //    "\\Reports\\AGList.dplx";
                string reportName = Path.Combine(AARTOBase.GetConnectionString(ServiceConnectionNameList.ReportFolder, ServiceConnectionTypeList.UNC), "AGList.dplx");

                if (!File.Exists(reportName))
                {
                    //this.Logger.Error(ResourceHelper.GetResource("ErrorAGListNoExists"));
                    //StopService();
                    AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorAGListNoExists", reportName), true);
                    return true;
                }

                // Create a DynamicPDF DocumentLayout object based on the DynamicPDF Report supplied
                DocumentLayout document = new DocumentLayout(reportName);
                StoredProcedureQuery query = (StoredProcedureQuery)document.GetQueryById("Query");
                query.ConnectionString = this.connectStr;

                // Add params for Report.
                ParameterDictionary parameters = new ParameterDictionary();

                parameters.Add("ReceiptStartDate", currentDateTime.Date);
                parameters.Add("AutIntNo", autIntNo);
                parameters.Add("ReceiptEndDate", System.DBNull.Value);
                parameters.Add("CrtIntNo", 0);
                parameters.Add("CrtRIntNo", 0);
                parameters.Add("SortOrder", "Receipt");
                parameters.Add("AGorSpot", "A");

                FormattedRecordArea printDate =
                    (FormattedRecordArea)document.GetElementById("printDate");
                printDate.LaidOut += new FormattedRecordAreaLaidOutEventHandler(printDate_LaidOut);

                Document report = document.Run(parameters);

                FileStream fs = file.Open(FileMode.OpenOrCreate);
                report.Draw(fs);
                fs.Close();
                fs.Dispose();

                printDate.LaidOut -= new FormattedRecordAreaLaidOutEventHandler(printDate_LaidOut);

                // jerry 2012-02-24 change
                //DEV:  Comment/Uncomment this to send the reports via email.
                //string message = string.Format(ResourceHelper.GetResource("EmailMsgAGListInCreateAGList"), DateTime.Now.ToString());

                //List<string> attachments = new List<string>();
                //attachments.Add(file.FullName);

                //send email
                //SendEmailManager.SendEmail(emailToCustomer, file.Name, message, attachments);

                //DEV:  Comment/Uncomment this to delete any generated reports.
                //file.Delete();

                fileFullName = file.FullName;
            }
            catch (Exception ex)
            {
                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorCreateAGListEmailInCreateAGList"), ex.Message));
                AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ErrorCreateAGListEmailInCreateAGList"), ex.Message), true);
                failed = true;
            }

            return failed;
        }

        /// <summary>
        /// BD created based on AG for spot fine report
        /// </summary>
        private bool CreateSFReport(out string fileFullName)
        {
            bool failed = false;
            fileFullName = string.Empty;

            //if (emailToCustomer.Equals(string.Empty))
            //{
            //    this.Logger.Error(ResourceHelper.GetResource("ErrorNoSFListEmailInCreateAGList"));
            //    StopService();
            //    failed = true;
            //    return failed;
            //}

            try
            {

                string tempFilePath = Path.Combine(Path.GetTempPath(), "SFList_" + autName + "_" + currentDate + ".pdf");
                FileInfo file = new FileInfo(tempFilePath);
                if (file.Exists)
                    file.Delete();

                // Get the DynamicPDF Report template with witch to create a new Report.
                //Jerry 2012-06-29 get report file from Report folder
                //string reportName = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) +
                //    "\\Reports\\SpotFineList.dplx";
                string reportName = Path.Combine(AARTOBase.GetConnectionString(ServiceConnectionNameList.ReportFolder, ServiceConnectionTypeList.UNC), "SpotFineList.dplx");

                if (!File.Exists(reportName))
                {
                    //this.Logger.Error(ResourceHelper.GetResource("ErrorSpotFineListNoExists"));
                    //StopService();
                    AARTOBase.ErrorProcessing(ResourceHelper.GetResource("ErrorSpotFineListNoExists", reportName), true);
                    return true;
                }

                // Create a DynamicPDF DocumentLayout object based on the DynamicPDF Report supplied
                DocumentLayout document = new DocumentLayout(reportName);
                StoredProcedureQuery query = (StoredProcedureQuery)document.GetQueryById("Query");
                query.ConnectionString = this.connectStr;

                // Add params for Report.
                ParameterDictionary parameters = new ParameterDictionary();
                parameters.Add("ReceiptStartDate", currentDateTime.Date);
                parameters.Add("AutIntNo", autIntNo);
                parameters.Add("ReceiptEndDate", System.DBNull.Value);
                parameters.Add("CrtIntNo", 0);
                parameters.Add("CrtRIntNo", 0);
                parameters.Add("SortOrder", "Receipt");
                parameters.Add("AGorSpot", "S");

                FormattedRecordArea printDate =
                    (FormattedRecordArea)document.GetElementById("printDate");

                printDate.LaidOut += new FormattedRecordAreaLaidOutEventHandler(printDate_LaidOut);

                Document report = document.Run(parameters);

                FileStream fs = file.Open(FileMode.OpenOrCreate);
                report.Draw(fs);
                fs.Close();
                fs.Dispose();

                printDate.LaidOut -= new FormattedRecordAreaLaidOutEventHandler(printDate_LaidOut);

                // jerry 2012-02-24 change
                //string message = string.Format(ResourceHelper.GetResource("EmailMsgSFListInCreateAGList"), DateTime.Now.ToString());

                //List<string> attachments = new List<string>();
                //attachments.Add(file.FullName);

                ////send email
                //SendEmailManager.SendEmail(emailToCustomer, file.Name, message, attachments);

                //file.Delete();
                fileFullName = file.FullName;
            }
            catch (Exception x)
            {
                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorCreateSFListEmailInCreateAGList"), x.Message));
                AARTOBase.ErrorProcessing(string.Format(ResourceHelper.GetResource("ErrorCreateSFListEmailInCreateAGList"), x.Message), true);
                failed = true;
            }

            return failed;
        }

        private void printDate_LaidOut(object sender, FormattedRecordAreaLaidOutEventArgs e)
        {
            e.FormattedTextArea.Text = "Printed: " + DateTime.Now.ToString("yyyy-MM-dd HH:mm");
        }

        //private void StopService()
        //{
        //    this.MustStop = true;
        //    ((SIL.ServiceLibrary.ServiceHost)this.ServiceHost).Stop();
        //}

        //private bool SetAGListLastRunDate()
        //{
        //    bool failed = false;
        //    string errMessage = string.Empty;

        //    failed = agListDB.SetAGListLastRunDate(this.autIntNo, currentDateTime, ref errMessage);

        //    if (!failed)
        //        this.Logger.Info("SetAGListLastRunDate: Successfully updated AGList date last run for " + autName);
        //    else
        //        this.Logger.Error("SetAGListLastRunDate: error - " + errMessage);

        //    return failed;
        //}

    }
}
