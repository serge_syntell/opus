﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CreateAGList
{
    partial class CreateAGList : ServiceHost
    {
        public CreateAGList()
            : base("", new Guid("38DE2B23-9BEC-49E5-9BCB-42FB1347A749"), new CreateAGListService())
        {
            InitializeComponent();
        }
    }
}
