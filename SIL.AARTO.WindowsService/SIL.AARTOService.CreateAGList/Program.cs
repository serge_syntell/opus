﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CreateAGList
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.CreateAGList"
                ,
                DisplayName = "SIL.AARTOService.CreateAGList"
                ,
                Description = "SIL AARTOService CreateAGList"
            };

            ProgramRun.InitializeService(new CreateAGList(), serviceDescriptor, args);
        }
    }
}
