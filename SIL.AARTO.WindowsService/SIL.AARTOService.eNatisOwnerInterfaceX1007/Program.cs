﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.eNatisOwnerInterfaceX1007
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ProgramRun.InitializeService(new eNatisOwnerInterfaceX1007(), new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.eNatisOwnerInterfaceX1007",
                DisplayName = "SIL.AARTOService.eNatisOwnerInterfaceX1007",
                Description = "SIL AARTOService eNatis Owner Interface X1007"
            }, args);
        }
    }
}
