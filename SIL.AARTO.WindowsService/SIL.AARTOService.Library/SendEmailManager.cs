﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Text.RegularExpressions;
using SIL.AARTOService.Resource;

namespace SIL.AARTOService.Library
{
    public class SendEmailManager
    {
        public static void SendEmail(string emailToAdress, string subject, string content, List<string> attachmentsList = null, string newAttachmentName = null)
        {
            EmailTransaction email = new EmailTransaction();
            //create zip file
            if (attachmentsList != null && attachmentsList.Count > 0)
            {
                string zipFileName = Path.Combine(Path.GetTempPath(), string.Format("{0}.zip", string.IsNullOrWhiteSpace(newAttachmentName) ? "Attachment" : newAttachmentName));
                if (File.Exists(zipFileName))
                    File.Delete(zipFileName);
                ZipUtility.Zip(zipFileName, attachmentsList, String.Empty);
                FileInfo zipFile = new FileInfo(zipFileName);
                FileStream fs = zipFile.OpenRead();
                byte[] zipByte = new byte[(int)zipFile.Length];
                fs.Read(zipByte, 0, (int)zipFile.Length);

                email.EmTrAttachment = zipByte;
                email.EmTrAttachmentName = Path.GetFileName(zipFileName);

                fs.Close();
                fs.Dispose();
                File.Delete(zipFileName);
            }

            email.EmTrSubject = subject;
            email.EmTrTo = emailToAdress;
            email.EmTrContent = content;

            EmailManager emailManager = new EmailManager();
            emailManager.SendMail(email);


        }

        public static bool CheckEmailAddress(string emailAddress)
        {
            List<string> addressList = new List<string>();
            if (!string.IsNullOrWhiteSpace(emailAddress))
            {
                emailAddress = emailAddress.Trim();

                //support mutiple-emailaddress
                if (emailAddress.Contains(";") || emailAddress.Contains(","))
                    addressList = emailAddress.Split(';', ',').ToList();
                else
                    addressList.Add(emailAddress);

                foreach (string s in addressList)
                {
                    if (!Regex.IsMatch(s, @"^(\w)+(\.\w+)*@(\w)+((\.\w+)+)$"))
                        return false;
                }
                return true;
            }
            else
                return false;
        }

        public static string GetFileListContent(string fileTitle, List<string> fileList)
        {
            string content = string.Empty;
            if (fileList != null && fileList.Count > 0)
            {
                StringBuilder sb = new StringBuilder(Environment.NewLine);
                sb.AppendFormat(ResourceHelper.GetResource("EmailContentOfPrintService"), fileTitle);
                sb.AppendLine();
                sb.AppendLine();
                sb.Append(string.Join(Environment.NewLine + "    ", fileList.ToArray()));
                content = sb.ToString();
            }
            return content;
        }
    }
}