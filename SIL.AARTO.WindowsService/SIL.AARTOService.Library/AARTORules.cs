﻿using System;
using System.Collections.Generic;
using System.Linq;
using SIL.ServiceBase;
using Stalberg.TMS;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Model;

namespace SIL.AARTOService.Library
{
    enum RuleType {AuthRule, DateRule, CourtRule}

    public class AARTORules
    {
        static AARTORules _AARTORules = null;

        // 2014-05-13, Oscar added lock for mutiple-threads.
        static readonly object syncObj = new object();

        private string connectStr, connectStrCache;
        private string autCode;
        private int crtIntNo;

        #region 2014-03-06, Oscar added caching features

        readonly Dictionary<string, Dictionary<string, AuthorityRulesDetails>> authRules = new Dictionary<string, Dictionary<string, AuthorityRulesDetails>>();
        readonly Dictionary<string, Dictionary<string, DateRulesDetails>> dateRules = new Dictionary<string, Dictionary<string, DateRulesDetails>>();
        readonly Dictionary<int, Dictionary<string, CourtRulesDetails>> courtRules = new Dictionary<int, Dictionary<string, CourtRulesDetails>>();

        AuthorityRulesDetails GetCachedRule(string arCode, Func<string, AuthorityRulesDetails> retrieval)
        {
            Dictionary<string, AuthorityRulesDetails> rules;
            AuthorityRulesDetails rule;

            var key = arCode.Replace("AR_", "");
            if (this.authRules.TryGetValue(this.autCode, out rules)
                && rules.TryGetValue(key, out rule))
                return rule;

            if (rules == null)
            {
                rules = new Dictionary<string, AuthorityRulesDetails>();
                this.authRules.Add(this.autCode, rules);
            }

            rule = retrieval(key);
            if (rule != null) rules.Add(key, rule);

            return rule;
        }

        DateRulesDetails GetCachedRule(string startDate, string endDate, Func<string, string, DateRulesDetails> retrieval)
        {
            Dictionary<string, DateRulesDetails> rules;
            DateRulesDetails rule;

            var key = startDate + "_" + endDate;
            if (this.dateRules.TryGetValue(this.autCode, out rules)
                && rules.TryGetValue(key, out rule))
                return rule;

            if (rules == null)
            {
                rules = new Dictionary<string, DateRulesDetails>();
                this.dateRules.Add(this.autCode, rules);
            }

            rule = retrieval(startDate, endDate);
            if (rule != null) rules.Add(key, rule);

            return rule;
        }

        CourtRulesDetails GetCachedRule(string crCode, Func<string, CourtRulesDetails> retrieval)
        {
            Dictionary<string, CourtRulesDetails> rules;
            CourtRulesDetails rule;

            var key = crCode.Replace("CR_", "");
            if (this.courtRules.TryGetValue(this.crtIntNo, out rules)
                && rules.TryGetValue(key, out rule))
                return rule;

            if (rules == null)
            {
                rules = new Dictionary<string, CourtRulesDetails>();
                this.courtRules.Add(this.crtIntNo, rules);
            }

            rule = retrieval(key);
            if (rule != null) rules.Add(key, rule);

            return rule;
        }

        #endregion

        private AARTORules()
        {

        }

        public static AARTORules GetSingleTon()
        {
            if (_AARTORules == null)
                //_AARTORules = new AARTORules();

                // 2014-05-13, Oscar added lock for mutiple-threads.
                lock (syncObj)
                    return _AARTORules ?? (_AARTORules = new AARTORules());

            return _AARTORules;
        }

        public void InitParameter(string _ConnectStr, string _AutCode)
        {
            // 2014-05-13, Oscar added lock for mutiple-threads.
            lock (syncObj)
            {
                autCode = _AutCode.Trim();
                connectStr = _ConnectStr;

                if (this.connectStrCache != this.connectStr)
                {
                    this.authRules.Clear();
                    this.dateRules.Clear();
                    this.courtRules.Clear();
                    this.connectStrCache = this.connectStr;
                }
            }
        }

        public AuthorityRulesDetails Rule(string ARCode)
        {
            // 2014-05-13, Oscar added lock for mutiple-threads.
            lock (syncObj)
            {
                //AuthorityRulesDetails rule = GetAuthRules(ARCode);
                var rule = GetCachedRule(ARCode, (Func<string, AuthorityRulesDetails>)GetAuthRules);
                if (rule == null)
                    throw new Exception(string.Format("Can't find rule with {0}", ARCode));
                return rule;
            }
        }

        public DateRulesDetails Rule(string DtRStartDate, string DtREndDate)
        {
            // 2014-05-13, Oscar added lock for mutiple-threads.
            lock (syncObj)
            {
                //DateRulesDetails rule = GetDateRules(DtRStartDate,DtREndDate);
                var rule = GetCachedRule(DtRStartDate, DtREndDate, GetDateRules);
                if (rule == null)
                    throw new Exception(string.Format("Can't find rule with {0} {1}", DtRStartDate, DtREndDate));
                return rule;
            }
        }

        public CourtRulesDetails Rule(int crtIntNo,string CRCode)
        {
            lock (syncObj)
            {
                this.crtIntNo = crtIntNo;
                //CourtRulesDetails rule = GetCourtRules(CRCode);
                var rule = GetCachedRule(CRCode, (Func<string, CourtRulesDetails>)GetCourtRules);
                if (rule == null)
                    throw new Exception(string.Format("Can't find court rule with {0}", CRCode));
                return rule;
            }
        }

        private AuthorityRulesDetails GetAuthRules(string ARCode)
        {
            AuthorityService autService=new AuthorityService();
            int autIntNo;
            SIL.AARTO.DAL.Entities.Authority autEntity = autService.GetByAutCode(autCode);
            if (autEntity != null)
                autIntNo = autEntity.AutIntNo;
            else
                throw new AuthorityNullException(string.Format("No authority can be found in AARTO database with AutCode {0} from queue group", autCode));

            AuthorityRuleInfo autInfo = new AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(autIntNo, ARCode);
            //Jerry 2012-06-07 add
            if (autInfo == null)
                return null;

            AuthorityRulesDetails autDetails = new AuthorityRulesDetails();
            autDetails.ARCode=autInfo.WFRFAName;
            autDetails.ARComment=autInfo.WFRFAComment;
            autDetails.ARDescr=autInfo.WFRFADescr;
            autDetails.ARNumeric=autInfo.ARNumeric;
            autDetails.ARString=autInfo.ARString;
            autDetails.AutIntNo=autInfo.AutIntNo;
            return autDetails;

        }

        private DateRulesDetails GetDateRules(string DtRStartDate, string DtREndDate)
        {
            AuthorityService autService = new AuthorityService();
            SIL.AARTO.DAL.Entities.Authority autEntity = autService.GetByAutCode(autCode);
            int autIntNo = autEntity.AutIntNo;

            DateRuleInfo dateInfo = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo,DtRStartDate,DtREndDate);
            //Jerry 2012-06-07 add
            if (dateInfo == null)
                return null;

            DateRulesDetails dateDetails = new DateRulesDetails();
            dateDetails.AutIntNo=dateInfo.AutIntNo;
            dateDetails.DtRDescr=dateInfo.DtRDescr;
            dateDetails.DtRNoOfDays=dateInfo.ADRNoOfDays;
            return dateDetails;
        }

        private CourtRulesDetails GetCourtRules(string CRCode)
        {
            CourtRuleInfo courtInfo = new CourtRuleInfo().GetCourtRuleInfoByWFRFCNameCrtIntNo(crtIntNo,CRCode);
            //Jerry 2012-06-07 add
            if (courtInfo == null)
                return null;

            CourtRulesDetails courtDetail = new CourtRulesDetails();
            courtDetail.CRCode=courtInfo.WFRFCName;
            courtDetail.CRComment=courtInfo.WFRFCComment;
            courtDetail.CRDescr=courtInfo.WFRFCDescr;
            courtDetail.CRNumeric=courtInfo.CRNumeric;
            courtDetail.CRString=courtInfo.CRString;
            courtDetail.CrtIntNo = courtInfo.CrtIntNo;
            return courtDetail;
        }

        public string ViolationCutOff()
        {
            SysParamDB sp = new SysParamDB(connectStr);

            string violationCutOff = "Y";
            int spValue = 0;

            bool found = sp.CheckSysParam("ViolationCutOff", ref spValue, ref violationCutOff);

            if (!found)
            {
                return "N";
            }
            else
                return violationCutOff;
        }
    }
    //add by Seawen 2013-04-16
    public class AuthorityNullException : Exception
    {
        public AuthorityNullException(string message)
            :base(message)
        {
            
        }
    }
}
