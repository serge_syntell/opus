﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Stalberg.TMS.Data;
using Stalberg.TMS;
using System.Data.SqlClient;
using SIL.AARTOService.Library;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Resource;

namespace SIL.ServiceLibrary
{
    public static class ClientServiceExtern
    {
        private const string FILE_EXT = ".txt";

        private const string TICKET_PROCESSOR_CA = "Cip_Aarto";
        private const string CONTRAVENTION_FILE_PREFIX = "ccph1";
        private const string CONFIG_FILE_PREFIX = "cfph1";
        private const string INFRINGEMENT_FILE_PREFIX = "ccpha";
        private const string RESPONSE_FILE_PREFIX = "crpha";

        private const string TICKET_PROCESSOR_CC = "Cip_CofCT";
        private const string CONFIG_FILE_PREFIX_CC = "cocfg";
        private const string INFRINGEMENT_FILE_PREFIX_CC = "cocon";
        private const string RESPONSE_FILE_PREFIX_CC = "cores";
        private const string REQUEST_FILE_PREFIX_CC = "cippp";   //090923 FT add REQUEST
        private const string SUMMONS_IDENTIFIER = "999";

        private const string CivitasPISummonsPickupFolder = "";
        private const string CivitasPISpotFinePickupFolder = "";

        public static void SendEmail(this ClientService service, string msg)
        {
            if (!SendEmailManager.CheckEmailAddress(service.ServiceParameters["EmailAddress"]))
            {
                service.Logger.Error(ResourceHelper.GetResource("ErrorEmailAddr"), ((AARTOServiceBase)service._serviceHelper).LastUser);
                service.Stop();
            }
            else
            {
                EmailTransaction tran = new EmailTransaction();
                tran.EmTrTo = service.ServiceParameters["EmailAddress"];
                tran.EmTrSubject = "Error";
                tran.EmTrContent = msg;

                new EmailManager().SendMail(tran);
            }
        }


        public static void FileToLocalFolder(this ClientService service, string receivedFile, string source, string type, string function)
        {
            try
            {
                string savedFile = Path.Combine(source, Path.GetFileName(receivedFile));

                if (File.Exists(savedFile))
                    File.Delete(savedFile);

                if (function.Equals("move"))
                    File.Move(receivedFile, savedFile);
                else if (function.Equals("copy"))
                    File.Copy(receivedFile, savedFile);
            }
            catch (System.IO.IOException e)
            {
                service.Logger.Info("FileToLocalFolder: Failed to " + function + " file name : " + receivedFile + " to folder " + source + " - " + e.Message);
            }
        }


        public static bool GetDataFilesFromTP_CofCT(this ClientService service, string LoadFiles)
        {
            bool failed = false;
            bool singleFolder = false;

            //Jerry 2012-04-17 change
            //save to folders
            //string exportFolder = service.ServiceParameters["CiprusCofCTExportFolder"];
            string exportFolder = LoadFiles;

            //get from folders
            string configFrom = LoadFiles;
            string responseFrom = LoadFiles;
            string requestFrom = LoadFiles;

            int countConfigFiles = 0;
            int countResponseFiles = 0;
            int countRequestFiles = 0;

            if (configFrom.Equals(responseFrom))
                singleFolder = true;

            // going to need a save to subfolder as well as a completed for each file type - check that it exists
            string configTo = exportFolder + @"\ConfigurationFiles\";
            if (!Directory.Exists(configTo))
                Directory.CreateDirectory(configTo);
            if (!Directory.Exists(configTo + @"\Completed"))
                Directory.CreateDirectory(configTo + "Completed");

            string responseTo = exportFolder + @"\ResponseFiles\";
            if (!Directory.Exists(responseTo))
                Directory.CreateDirectory(responseTo);
            if (!Directory.Exists(responseTo + @"\Completed"))
                Directory.CreateDirectory(responseTo + @"\Completed");

            string requestTo = exportFolder + @"\RequestFiles\";
            if (!Directory.Exists(requestTo))
                Directory.CreateDirectory(requestTo);
            if (!Directory.Exists(requestTo + @"\Completed"))
                Directory.CreateDirectory(requestTo + @"\Completed");

            //BD 3946 need to have an error subfolder for when there is a problem with the file
            string configError = configTo + @"Error\";
            if (!Directory.Exists(configError))
                Directory.CreateDirectory(configError);

            string responseError = responseTo + @"Error\";
            if (!Directory.Exists(responseError))
                Directory.CreateDirectory(responseError);

            string requestError = requestTo + @"Error\";
            if (!Directory.Exists(requestError))
                Directory.CreateDirectory(requestError);

            //if they are going to dump everything into a single folder, we can do this loop once, checking for each file type
            DirectoryInfo dir = null;
            if (singleFolder)
            {
                //process all files in input folder
                dir = new DirectoryInfo(configFrom);
                if (dir.Exists)
                {
                    string[] fileEntries = Directory.GetFiles(configFrom);

                    foreach (string file in fileEntries)
                    {
                        //BD 3946 process for checking that the file is comeplete and valid. Doing it here so that if there is an error then the file
                        //moves to an error folder and will not get processed further down the line.

                        if (Path.GetFileName(file).ToLower().StartsWith(CONFIG_FILE_PREFIX_CC) && Path.GetExtension(file).ToLower().Equals(FILE_EXT))
                        {
                            //The old TpexInt will loop through the sub folder to find the configuration file but windows service will only try to find the files in the root folder
                            // move the file to the Configuration files subfolder
                            //service.FileToLocalFolder(file, configTo, "configuration", "move");
                            //BD 3946 
                            //configTo needs to be exportFolder
                            //service.FileCheck(configTo + Path.GetFileName(file), "configuration");
                            service.FileCheck(file, "configuration");
                            countConfigFiles++;
                        }
                        //Jerry 2012-04-19 add
                        else if (Path.GetFileName(file).ToLower().StartsWith(RESPONSE_FILE_PREFIX_CC) && Path.GetExtension(file).ToLower() == FILE_EXT && Path.GetFileName(file).ToLower().IndexOf(SUMMONS_IDENTIFIER, 0) == 8)
                        {
                            //no need to do any validation on response files where Cam Unit = 999
                            countResponseFiles++;
                        }
                        else if (Path.GetFileName(file).ToLower().StartsWith(RESPONSE_FILE_PREFIX_CC) && Path.GetExtension(file).ToLower().Equals(FILE_EXT))
                        {//The old TpexInt will loop through the sub folder to find the response file but windows service will only try to find the files in the root folder

                            //// move the file to the response files subfolder
                            //service.FileToLocalFolder(file, responseTo, "response", "move");
                            ////BD 3946 
                            //responseTo needs to be exportFolder
                            //service.FileCheck(responseTo + Path.GetFileName(file), "response", TICKET_PROCESSOR_CC);
                            service.FileCheck(file, "response", TICKET_PROCESSOR_CC);
                            countResponseFiles++;
                        }
                        else if (Path.GetFileName(file).ToLower().StartsWith(REQUEST_FILE_PREFIX_CC) && Path.GetExtension(file).ToLower().Equals(FILE_EXT))
                        {
                            //The old TpexInt will loop through the sub folder to find the request file but windows service will only try to find the files in the root folder
                            // 090923 FT move the file to the request files subfolder
                            //service.FileToLocalFolder(file, requestTo, "request", "move");
                            countRequestFiles++;
                        }
                    } // end of files in directory
                }
                dir = null;
            }
            else
            {
                dir = new DirectoryInfo(configFrom);
                if (dir.Exists)
                {
                    //process all config files in ConfigurationFilesFolder
                    string[] fileEntries = Directory.GetFiles(configFrom);

                    foreach (string file in fileEntries)
                    {
                        if (Path.GetFileName(file).ToLower().StartsWith(CONFIG_FILE_PREFIX_CC) && Path.GetExtension(file).ToLower().Equals(FILE_EXT))
                        {
                            //Jerry 2012-04-19 change
                            //service.FileToLocalFolder(file, configTo, "configuration", "move");
                            //BD 3946 
                            service.FileCheck(file, "configuration", TICKET_PROCESSOR_CC);
                            countConfigFiles++;
                        }
                    } // end of files in directory


                    //process all config files in ResponseFilesFolder
                    if (!responseFrom.Trim().Equals(""))
                    {
                        fileEntries = Directory.GetFiles(responseFrom);

                        foreach (string file in fileEntries)
                        {
                            if (Path.GetFileName(file).ToLower().StartsWith(RESPONSE_FILE_PREFIX_CC) && Path.GetExtension(file).ToLower() == FILE_EXT && Path.GetFileName(file).ToLower().IndexOf(SUMMONS_IDENTIFIER, 0) == 8)
                            {
                                //no need to do any validation on response files where Cam Unit = 999
                                countResponseFiles++;
                            }
                            else if (Path.GetFileName(file).ToLower().StartsWith(RESPONSE_FILE_PREFIX_CC) && Path.GetExtension(file).ToLower().Equals(FILE_EXT))
                            {
                                //Jerry 2012-04-19 change
                                // move the file to the response files subfolder
                                //service.FileToLocalFolder(file, responseTo, "response", "move");
                                //BD 3946 
                                service.FileCheck(file, "response", TICKET_PROCESSOR_CC);
                                countResponseFiles++;
                            }
                        } // end of files in directory
                    }

                    //process all request files in RequestFilesFolder
                    if (!requestFrom.Trim().Equals(""))
                    {
                        fileEntries = Directory.GetFiles(requestFrom);

                        foreach (string file in fileEntries)
                        {
                            if (Path.GetFileName(file).ToLower().StartsWith(REQUEST_FILE_PREFIX_CC) && Path.GetExtension(file).ToLower().Equals(FILE_EXT))
                            {
                                //Jerry 2012-04-19 change
                                // move the file to the request files subfolder
                                //service.FileToLocalFolder(file, requestTo, "request", "move");
                                //BD 4113 
                                service.FileCheck(file, "request", TICKET_PROCESSOR_CC);
                                countRequestFiles++;
                            }
                        } // end of files in directory
                    }
                }
            }

            service.Logger.Info("Found " + countConfigFiles.ToString() + " configuration files");
            service.Logger.Info("Found " + countResponseFiles.ToString() + " response files");
            service.Logger.Info("Found " + countRequestFiles.ToString() + " request files");

            if (failed)
                service.Logger.Info("Failed to complete copy of Ciprus CofCT files at: " + DateTime.Now.ToString());
            else
                service.Logger.Info("Completed copy of Ciprus CofCT files at: " + DateTime.Now.ToString());

            return failed;
        }


        public static bool GetDataFilesFromTP_AARTO(this ClientService service, string LoadFiles)
        {
            bool failed = false;
            bool singleFolder = false;

            //Jerry 2012-04-17 change
            //save to folders
            //string exportFolder = service.ServiceParameters["CiprusAartoExportFolder"];
            string exportFolder = LoadFiles;

            //get from folders
            string configFrom = LoadFiles;
            string responseFrom = LoadFiles;

            int countConfigFiles = 0;
            int countResponseFiles = 0;

            if (configFrom.Equals(responseFrom))
                singleFolder = true;

            // going to need a save to subfolder as well as a completed for each file type - check that it exists
            string configTo = exportFolder + @"\ConfigurationFiles\";
            if (!Directory.Exists(configTo))
                Directory.CreateDirectory(configTo);
            if (!Directory.Exists(configTo + @"\Completed"))
                Directory.CreateDirectory(configTo + "Completed");

            string responseTo = exportFolder + @"\ResponseFiles\";
            if (!Directory.Exists(responseTo))
                Directory.CreateDirectory(responseTo);
            if (!Directory.Exists(responseTo + @"\Completed"))
                Directory.CreateDirectory(responseTo + @"\Completed");

            //BD 3946 need to have an error subfolder for when there is a problem with the file
            string configError = configTo + @"Error\";
            if (!Directory.Exists(configError))
                Directory.CreateDirectory(configError);

            string responseError = responseTo + @"Error\";
            if (!Directory.Exists(responseError))
                Directory.CreateDirectory(responseError);

            //if they are going to dump everything into a single folder, we can do this loop once, checking for each file type
            DirectoryInfo dir = null;
            if (singleFolder)
            {
                //process all files in input folder
                dir = new DirectoryInfo(configFrom);
                if (dir.Exists)
                {
                    string[] fileEntries = Directory.GetFiles(configFrom);

                    foreach (string file in fileEntries)
                    {
                        //BD 3946 process for checking that the file is comeplete and valid. Doing it here so that if there is an error then the file
                        //moves to an error folder and will not get processed further down the line.

                        if (Path.GetFileName(file).ToLower().StartsWith(CONFIG_FILE_PREFIX) && Path.GetExtension(file).ToLower().Equals(FILE_EXT))
                        {
                            //Jerry 2012-04-20 change
                            // move the file to the Configuration files subfolder
                            //service.FileToLocalFolder(file, configTo, "configuration", "move");
                            //BD 3946 
                            //service.FileCheck(configTo + Path.GetFileName(file), "configuration");
                            service.FileCheck(file, "configuration");
                            countConfigFiles++;
                        }
                        else if (Path.GetFileName(file).ToLower().StartsWith(RESPONSE_FILE_PREFIX) && Path.GetExtension(file).ToLower().Equals(FILE_EXT))
                        {
                            //Jerry 2012-04-20 change
                            // move the file to the response files subfolder
                            //service.FileToLocalFolder(file, responseTo, "response", "move");
                            //BD 3946 
                            //service.FileCheck(responseTo + Path.GetFileName(file), "response", TICKET_PROCESSOR_CA);
                            service.FileCheck(file, "response", TICKET_PROCESSOR_CA);
                            countResponseFiles++;
                        }

                    } // end of files in directory
                }
                dir = null;
            }
            else
            {
                dir = new DirectoryInfo(configFrom);
                if (dir.Exists)
                {
                    //process all config files in ConfigurationFilesFolder
                    string[] fileEntries = Directory.GetFiles(configFrom);

                    foreach (string file in fileEntries)
                    {
                        if (Path.GetFileName(file).ToLower().StartsWith(CONFIG_FILE_PREFIX) && Path.GetExtension(file).ToLower().Equals(FILE_EXT))
                        {
                            //Jerry 2012-04-20 change
                            //service.FileToLocalFolder(file, configTo, "configuration", "move");
                            //BD 3946 
                            service.FileCheck(file, "configuration", TICKET_PROCESSOR_CA);
                            countConfigFiles++;
                        }
                    } // end of files in directory


                    //process all config files in ResponseFilesFolder
                    if (!responseFrom.Trim().Equals(""))
                    {
                        fileEntries = Directory.GetFiles(responseFrom);

                        foreach (string file in fileEntries)
                        {
                            if (Path.GetFileName(file).ToLower().StartsWith(RESPONSE_FILE_PREFIX) && Path.GetExtension(file).ToLower().Equals(FILE_EXT))
                            {
                                //Jerry 2012-04-20 change
                                // move the file to the response files subfolder
                                //service.FileToLocalFolder(file, responseTo, "response", "move");
                                //BD 3946 
                                service.FileCheck(file, "response", TICKET_PROCESSOR_CA);
                                countResponseFiles++;
                            }
                        } // end of files in directory
                    }
                }
            }

            service.Logger.Info("Found " + countConfigFiles.ToString() + " configuration files");
            service.Logger.Info("Found " + countResponseFiles.ToString() + " response files");

            if (failed)
                service.Logger.Info("Failed to complete copy of Ciprus Aarto files at: " + DateTime.Now.ToString());
            else
                service.Logger.Info("Completed copy of Ciprus Aarto files at: " + DateTime.Now.ToString());

            return failed;
        }


        public static bool FileCheck(this ClientService service, string file, string fileType)
        {
            return service.FileCheck(file, fileType, "CiprusPI");
        }

        //BD 3858 checking of files - is the base from 3946 with summons and receipt functionality added.
        public static bool FileCheck(this ClientService service, string file, string fileType, string processor)
        {
            FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs, System.Text.Encoding.Default);

            string ConnectionString =((AARTOServiceBase)service._serviceHelper).GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            CiprusData cp = new CiprusData(ConnectionString);

            int lineCount = 0;
            int trailerLine = 0;
            int trailerLineCount = 0;
            string srLine = "";
            bool validFile = true;
            string autNo = "";

            //Jerry 2012-04-18
            //string exportFolder = service.ServiceParameters["CiprusCofCTExportFolder"];
            string exportFolder = ((AARTOServiceBase)service._serviceHelper).GetConnectionString(ServiceConnectionNameList.CiprusCofCT_LoadFiles, ServiceConnectionTypeList.UNC);

            if (processor.Equals("Cip_Aarto"))
                //Jerry 2012-04-18
                //exportFolder = service.ServiceParameters["CiprusAartoExportFolder"];
                exportFolder = ((AARTOServiceBase)service._serviceHelper).GetConnectionString(ServiceConnectionNameList.CiprusAARTO_LoadFiles, ServiceConnectionTypeList.UNC);

            string configError = exportFolder + @"\ConfigurationFiles\Error\";
            string responseError = exportFolder + @"\ResponseFiles\Error\";
            string allocError = exportFolder + @"\AllocationFiles\Error\";
            string summonsError = Path.Combine(CivitasPISummonsPickupFolder, "Error") + @"\";
            string paymentError = Path.Combine(CivitasPISpotFinePickupFolder, "Error") + @"\";

            string moveDest = "";
            if (fileType == "configuration")
            {
                moveDest = configError;
            }
            else if (fileType == "response")
            {
                moveDest = responseError;
            }
            else if (fileType == "allocation")
            {
                moveDest = allocError;
            }
            else if (fileType == "summons")
            {
                moveDest = summonsError;
            }
            else if (fileType == "payment")
            {
                moveDest = paymentError;
            }

            //response file and allocation file
            if (fileType == "response" || fileType == "allocation" || fileType == "summons")
            {
                while (!sr.EndOfStream)
                {
                    srLine = sr.ReadLine();

                    if (srLine.Length > 2)
                    {
                        string recordType = srLine.Substring(0, 2);
                        switch (recordType)
                        {
                            case "01":
                                //summons file
                                lineCount += 1;
                                break;
                            case "61":
                                //response file
                                //autNo = srLine.Substring(5, 3);
                                lineCount += 1;
                                break;
                            case "63":
                                //allocation file
                                autNo = srLine.Substring(5, 3);
                                lineCount += 1;
                                break;
                            case "82":
                                //payment file
                                lineCount += 1;
                                break;
                            case "89":
                                //trailer record for payment files
                                trailerLine = 1;
                                trailerLineCount = Convert.ToInt32(srLine.Substring(3, 6)); //need to check position in line
                                break;
                            case "99":
                                trailerLine = 1;
                                if (fileType == "summons")
                                {
                                    trailerLineCount = Convert.ToInt32(srLine.Substring(4, 6));
                                }
                                else
                                {
                                    trailerLineCount = Convert.ToInt32(srLine.Substring(5, 6));
                                }
                                // 99|C|01|000193
                                // -2n|interface-1a|version-2n|count-6n
                                break;
                        }
                    }
                } // end of while sr
            }
            //configuration file
            if (fileType == "configuration")
            {
                while (!sr.EndOfStream)
                {
                    srLine = sr.ReadLine();

                    if (srLine.Length > 2)
                    {
                        string recordType = srLine.Substring(0, 2);
                        switch (recordType)
                        {
                            case "99":
                                trailerLine = 1;
                                //trailerLineCount = Convert.ToInt32(srLine.Substring(8,6));
                                // 99|C|01|000193
                                // -2n|interface-1a|version-2n|count-6n
                                break;
                        }
                    }
                } // end of while sr
            }

            sr.Close();
            fs.Close();

            string receivedFile = Path.GetFullPath(file);
            string fileName = Path.GetFileName(receivedFile);

            int ContraLineCount = 0;
            if (fileType != "summons" && fileType != "payment")
            {
                CameraUnitBatchDB batch = new CameraUnitBatchDB(ConnectionString);
                // if the local authority is missing bail out immediately swearing
                //Components.TMSData tmsData = new Components.TMSData(this.parameters.ConnectionString);
                //autIntNo = tmsData.GetAutIntFromAuthorityFromAutNo(autNo);

                string contraventionFile = CONTRAVENTION_FILE_PREFIX + fileName.Substring(5, 10) + Path.GetExtension(fileName);
                string camUnitID = fileName.Substring(5, 3);

                if (processor.Equals("Cip_Aarto"))
                {
                    contraventionFile = INFRINGEMENT_FILE_PREFIX + fileName.Substring(5, 10) + Path.GetExtension(fileName);
                    autNo = fileName.Substring(5, 3);
                    camUnitID = fileName.Substring(8, 3);
                }
                else if (processor.Equals("Cip_CofCT"))
                {
                    contraventionFile = INFRINGEMENT_FILE_PREFIX_CC + fileName.Substring(5, 10) + Path.GetExtension(fileName);
                    autNo = fileName.Substring(5, 3);
                    camUnitID = fileName.Substring(8, 3);
                }

                if (fileType != "configuration")
                {
                    SqlDataReader reader = batch.CameraUnitBatchDetails(autNo, camUnitID, contraventionFile);
                    while (reader.Read())
                    {
                        if (reader["CUBFileName"].ToString() != "")
                        {
                            ContraLineCount = Convert.ToInt32(reader["CUBNoOfRecords"]);
                        }
                        else
                        {
                            //there is no contravention file in the db
                            service.FileToLocalFolder(file, moveDest, fileType, "move");
                            service.Logger.Info("FileCheck: There is no record of the contravention file in the Camera Unit Batch table. File name : " + file + " moved to folder " + moveDest + " - ");
                            //add record to civitas load errors table
                            cp.InsertCivitasLoadError(fileName, DateTime.Now, "FileCheck: There is no record of the contravention file in the Camera Unit Batch table");
                            validFile = false;
                        }
                    }

                    reader.Close();
                }
            }
            if (trailerLine == 0 && validFile == true)
            {
                //there was no trailer line for the file
                //move file to error folder and write error to log
                service.FileToLocalFolder(file, moveDest, fileType, "move");
                service.Logger.Info("FileCheck: Failed due to missing trailer line. File name : " + file + " moved to folder " + moveDest + " - ");
                //add record to civitas load errors table
                cp.InsertCivitasLoadError(fileName, DateTime.Now, "FileCheck: Failed due to missing trailer line");
                validFile = false;

            }
            else if (lineCount != trailerLineCount && trailerLineCount != 0 && validFile == true)
            {
                //the actual line count of the file did not equal the trailer line count number
                //move file to error folder and write error to log
                service.FileToLocalFolder(file, moveDest, fileType, "move");
                service.Logger.Info("FileCheck: Failed due to difference in actual line count with reported line count. File name : " + file + " moved to folder " + moveDest + " - ");
                //add record to civitas load errors table
                cp.InsertCivitasLoadError(fileName, DateTime.Now, "FileCheck: Failed due to difference in actual line count with reported line count");
                validFile = false;
            }
            else if (lineCount != ContraLineCount && ContraLineCount != 0 && trailerLineCount != 0 && validFile == true)
            {
                //the line count for the file that is being check does not match the line count for the parent contravention file.
                //move file to error folder and write error to log
                service.FileToLocalFolder(file, moveDest, fileType, "move");
                service.Logger.Info("FileCheck: Failed due to difference in actual line count and line count recored for contravetion file. File name : " + file + " moved to folder " + moveDest + " - ");
                //add record to civitas load errors table
                cp.InsertCivitasLoadError(fileName, DateTime.Now, "FileCheck: Failed due to difference in actual line count and line count recored for contravetion file");
                validFile = false;
            }

            return validFile;
        }
    }
}
