using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Stalberg.TMS
{
    /// <summary>
    /// Defines an object that can translate or map character data according to a predefined set of rules
    /// </summary>
    public class CharacterTranslator
    {
        // Fields
        private StringBuilder sb = null;

        #region Static

        // Fields
        private static Regex nameRegex;
        private static Regex addressRegex;
        private static Dictionary<char, char> addressMappings;
        private static Dictionary<char, char> nameMappings;
        private static bool isInitialised = false;

        /// <summary>
        /// Initializes the <see cref="CharacterTranslator"/> class.
        /// </summary>
        static CharacterTranslator()
        {
            CharacterTranslator.addressMappings = new Dictionary<char, char>();
            CharacterTranslator.nameMappings = new Dictionary<char, char>();
        }

        /// <summary>
        /// Initialises this instance.
        /// </summary>
        public static void Initialise(string connectionString)
        {
            if (connectionString.Trim().Length == 0)
                throw new ApplicationException("The database connection string for retrieving the mapping data has not been set.");

            // Create the regular expressions
            string pattern = @"[^ a-zA-Z'\(\)-]"; // Includes a space!
            CharacterTranslator.nameRegex = new Regex(pattern, RegexOptions.Compiled | RegexOptions.Singleline);
            pattern = @"[^ a-zA-Z0-9&'\(\),-\./]";
            CharacterTranslator.addressRegex = new Regex(pattern, RegexOptions.Compiled | RegexOptions.Singleline);

            // Load the mapping data
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("SELECT [Character], Mapping, IsName FROM CharacterTranslatorMapping", con);
            com.CommandType = System.Data.CommandType.Text;

            try
            {
                string mapper;
                char mapTo;
                bool isName;

                con.Open();
                SqlDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    mapTo = reader.GetString(1)[0];
                    mapper = reader.GetString(0);
                    isName = reader.GetBoolean(2);

                    foreach (char c in mapper)
                    {
                        if (!CharacterTranslator.addressMappings.ContainsKey(c))
                        {
                            if (isName)
                                CharacterTranslator.nameMappings.Add(c, mapTo);
                            CharacterTranslator.addressMappings.Add(c, mapTo);
                        }
                        else
                            Trace.WriteLine(string.Format("{0} is has been duplicated int the mapping dictionary and the entry {0} - {1} has not been added,", c, mapTo));
                    }
                }
                reader.Close();
            }
            catch (SqlException se)
            {
                throw new ApplicationException("There was a problem accessing the database to retrieve mapping information for the CharacterTranslator.", se);
            }
            finally
            {
                con.Dispose();
            }

            CharacterTranslator.isInitialised = true;
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="CharacterTranslator"/> class.
        /// </summary>
        public CharacterTranslator()
        {
            if (!CharacterTranslator.isInitialised)
                throw new ApplicationException("The CharacterTranslator class has not yet been initialised!");

            this.sb = new StringBuilder();
        }

        /// <summary>
        /// Checks whether the supplied name contains any illegal characters and translates them if there are.
        /// </summary>
        /// <param name="name">The name to check.</param>
        /// <returns>The translated name</returns>
        public bool TryParseName(string value, out string name)
        {
            if (!CharacterTranslator.nameRegex.IsMatch(value))
            {
                name = value.ToUpper();
                return true;
            }

            this.sb.Length = 0;

            bool response = this.Translate(value, true);

            name = this.sb.ToString();
            return response;
        }

        /// <summary>
        /// Checks whether the supplied address contains any illegal address characters and translates them if there are.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns>A translated address</returns>
        public bool TryParseAddress(string value, out string address)
        {
            if (!CharacterTranslator.addressRegex.IsMatch(value))
            {
                address = value.ToUpper();
                return true;
            }

            this.sb.Length = 0;

            bool response = this.Translate(value, false);

            address = this.sb.ToString();
            return response;
        }

        private bool Translate(string value, bool isName)
        {
            bool response = true;

            // Loop the string
            foreach (char c in value)
            {
                if (isName)
                {
                    if (CharacterTranslator.nameMappings.ContainsKey(c))
                    {
                        this.sb.Append(CharacterTranslator.nameMappings[c]);
                        continue;
                    }
                }
                else
                {
                    if (CharacterTranslator.addressMappings.ContainsKey(c))
                    {
                        this.sb.Append(CharacterTranslator.addressMappings[c]);
                        continue;
                    }
                }

                response = false;
            }

            return response;
        }

    }
}
