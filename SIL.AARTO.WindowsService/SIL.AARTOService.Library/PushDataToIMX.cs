﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Collections;
using SIL.IMX.DAL.Data;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;

namespace SIL.AARTOService.Library
{
    public class PushDataToIMX : ClientService
    {
        #region ****************Structure Assembly****************
        private Assembly assemblyEntity;
        private Assembly assemblyService;
        private Assembly assemblyQuery;
        private string IMXconnectStr=string.Empty;
        AARTOServiceBase AARTOBase = null;
        public PushDataToIMX(ref string errorMsg)
            : base("", "",new Guid("A713DD86-7051-469C-BC6A-309B6CDF1684"))
        {
            try
            {
                AARTOBase = new AARTOServiceBase(this);
                //init imx connect 
                IMXconnectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.SIL_IMX, ServiceConnectionTypeList.DB);
                ServiceUtility.InitializeNetTier(IMXconnectStr, "SIL.IMX");
                assemblyEntity = Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SIL.IMX.DAL.Entities.dll"));
                assemblyService = Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SIL.IMX.DAL.Services.dll"));
                assemblyQuery = Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SIL.IMX.DAL.Data.dll"));
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "IMXMaintenanceData", ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        #endregion

        public void ARRTOMappingIMX(List<Maintenance> maintenanceList, ref List<string> errorMsgList)
        {
            //Remote,local
            string currentPath = AppDomain.CurrentDomain.BaseDirectory + "AARTOMappingIMX.xml";
            XElement xTree = XElement.Load(currentPath);

            IEnumerable<XElement> tables = xTree.Elements();
            //
            List<DeleteTable> deleteTableList = new List<DeleteTable>();
            foreach (XElement table in tables)
            {
                string tableName = table.FirstAttribute.Value.ToString();
                string tableColumn = table.LastAttribute.Value.ToString();
                //if database not exsits 
                bool flag = CheckDatabaseHasTheTable(tableName, tableColumn);
                
                if (flag)
                {
                    List<Maintenance> InsertList = GetTableData(maintenanceList, tableName, "I");
                    List<Maintenance> UpdateList = GetTableData(maintenanceList, tableName, "U");
                    List<Maintenance> DeleteList = GetTableData(maintenanceList, tableName, "D");
                    IEnumerable<XElement> columns = table.Elements();
                    if (InsertList.Count > 0)
                        xmlToMaintenanceInsert(tableName, tableColumn, InsertList, columns, ref errorMsgList);
                    if (UpdateList.Count > 0)
                        xmlToMaintenanceUpdate(tableName, tableColumn, UpdateList, columns, ref errorMsgList);
                    if (DeleteList.Count > 0)
                    {
                        DeleteTable delTable = new DeleteTable();
                        delTable.TableName = tableName;
                        delTable.TableColumn = tableColumn;
                        delTable.DeleteList = DeleteList;
                        delTable.Colums = columns;
                        deleteTableList.Add(delTable);
                    }
                }
            }
            DoTheOppositeDelete(deleteTableList, ref errorMsgList);
        }
        public void DoTheOppositeDelete(List<DeleteTable> list, ref List<string> errorMsgList)
        {
            List<DeleteTable> oppositeList = OppositeOrder(list);
            foreach (var item in oppositeList)
            {
                xmlToMaintenanceDelete(item.TableName, item.TableColumn, item.DeleteList, item.Colums, ref errorMsgList);
            }
        }
        // get the table data
        public List<Maintenance> GetTableData(List<Maintenance> maintenanceList, string tableName, string active)
        {
            List<Maintenance> mainData = new List<Maintenance>();
            var query = from maintenance in maintenanceList
                        where maintenance.MdmdTableName.ToLower() == tableName.ToLower() & maintenance.MdmdAction == active
                        select maintenance;
            if (query != null)
            {
                mainData = query.ToList();
            }
            return mainData;
        }
        #region ****************insert table****************
        public void xmlToMaintenanceInsert(string tableName, string tableColumn, List<Maintenance> list, IEnumerable<XElement> elements, ref List<string> errorMsgList)
        {
            string errorMsg = string.Empty;
            try
            {
                string[] EntityInfoArray = assemblyEntity.ToString().Split(',');
                string[] ServiceInfoArray = assemblyService.ToString().Split(',');
                string EntityInfoName = EntityInfoArray[0] + "." + tableName;
                string ServiceInfoName = ServiceInfoArray[0] + "." + tableName + "Service";
                Type tableType = assemblyEntity.GetType(EntityInfoName, true, false);
                Type serviceType = assemblyService.GetType(ServiceInfoName, true, false);
                string[] columnArray = tableColumn.Split(',');
                Hashtable nodeNameArray = GetNodeName(elements, columnArray, ref errorMsg);
                foreach (Maintenance maintenance in list)
                {   //Checks for the presence of this data
                    try
                    {
                        Object IsExsitObj = GetTableObj(tableName, nodeNameArray, maintenance, ref errorMsg);
                        if (IsExsitObj == null)
                        {
                            Object TableObj = assemblyEntity.CreateInstance(EntityInfoName);
                            foreach (XElement node in elements)
                            {
                                //create mapping imx value
                                MappingIMX(maintenance, node, tableType, ref TableObj, tableName, ref errorMsg);
                            }
                            Object ServiceObj = assemblyService.CreateInstance(ServiceInfoName);
                            MethodInfo methodInfo = serviceType.GetMethod("Save", new Type[] { TableObj.GetType() });
                            object flag = methodInfo.Invoke(ServiceObj, new object[] { TableObj });
                            if (flag == null)
                                errorMsgList.Add(tableName + " method: Save affected rows 0 .");
                            //Logger.Info(tableName + " method: Save affected rows 0 .");
                        }
                        else
                        {
                            errorMsgList.Add(tableName + " method: save, this data has existed .");
                        }
                    }
                    catch (Exception ex)
                    {
                        errorMsgList.Add(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMsg))
                    errorMsg = string.Format("{0}:{1}", "xmlToMaintenanceInsert", ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        #endregion
        #region ****************update table****************
        public void xmlToMaintenanceUpdate(string tableName, string tableColumn, List<Maintenance> list, IEnumerable<XElement> elements, ref List<string> errorMsgList)
        {
            string errorMsg = string.Empty;
            try
            {
                string[] EntityInfoArray = assemblyEntity.ToString().Split(',');
                string[] ServiceInfoArray = assemblyService.ToString().Split(',');
                string EntityInfoName = EntityInfoArray[0] + "." + tableName;
                string ServiceInfoName = ServiceInfoArray[0] + "." + tableName + "Service";
                Type tableType = assemblyEntity.GetType(EntityInfoName, true, false);
                Type serviceType = assemblyService.GetType(ServiceInfoName, true, false);
                string[] columnArray = tableColumn.Split(',');
                Hashtable nodeNameArray = GetNodeName(elements, columnArray, ref errorMsg);
                foreach (Maintenance maintenance in list)
                {
                    try
                    {
                        Object TableObj = GetTableObj(tableName, nodeNameArray, maintenance, ref errorMsg);
                        if (TableObj != null)
                        {
                            IList iList = TableObj as IList;
                            for (int i = 0; i < iList.Count; i++)
                            {
                                object upateObj = iList[i];
                                foreach (XElement node in elements)
                                {
                                    //create mapping imx value
                                    MappingIMX(maintenance, node, tableType, ref upateObj, tableName, ref errorMsg);
                                }
                                Object ServiceObj = assemblyService.CreateInstance(ServiceInfoName);
                                MethodInfo methodInfo = serviceType.GetMethod("Update", new Type[] { upateObj.GetType() });
                                bool flag = Convert.ToBoolean(methodInfo.Invoke(ServiceObj, new object[] { upateObj }));
                                if (!flag)
                                    errorMsgList.Add(tableName + " method: Update affected rows 0 .");
                                //Logger.Info(tableName + " method: Update affected rows 0 .");
                            }
                        }
                        else
                        {
                            errorMsgList.Add(tableName + " method: Update affected rows 0 .");
                        }
                    }
                    catch (Exception ex)
                    {
                        errorMsgList.Add(ex.Message);
                        //Logger.Error(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMsg))
                    errorMsg = string.Format("{0}:{1}", "xmlToMaintenanceUpdate", ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        #endregion
        #region ****************delete data****************
        public void xmlToMaintenanceDelete(string tableName, string tableColumn, List<Maintenance> list, IEnumerable<XElement> elements, ref List<string> errorMsgList)
        {
            string errorMsg = string.Empty;
            string[] EntityInfoArray = assemblyEntity.ToString().Split(',');
            string[] ServiceInfoArray = assemblyService.ToString().Split(',');
            string EntityInfoName = EntityInfoArray[0] + "." + tableName;
            string ServiceInfoName = ServiceInfoArray[0] + "." + tableName + "Service";
            Type tableType = assemblyEntity.GetType(EntityInfoName, true, false);
            Type serviceType = assemblyService.GetType(ServiceInfoName, true, false);
            string[] columnArray = tableColumn.Split(',');
            Hashtable nodeNameArray = GetNodeName(elements, columnArray, ref errorMsg);
            foreach (Maintenance maintenance in list)
            {
                try
                {
                    Object TableObj = GetTableObj(tableName, nodeNameArray, maintenance, ref errorMsg);
                    if (TableObj != null)
                    {
                        IList iList = TableObj as IList;
                        for (int i = 0; i < iList.Count; i++)
                        {
                            object deleteObj = iList[i];
                            Object ServiceObj = assemblyService.CreateInstance(ServiceInfoName);
                            MethodInfo methodInfo = serviceType.GetMethod("Delete", new Type[] { deleteObj.GetType() });
                            methodInfo.Invoke(ServiceObj, new object[] { deleteObj });
                        }
                    }
                    else
                    {
                        errorMsgList.Add(tableName + " method: Delete affected rows 0 .");
                    }
                }
                catch (Exception ex)
                {
                    errorMsgList.Add(ex.Message);
                    //Logger.Error(ex);
                }
            }
        }
        #endregion

        #region ****************action public data****************
        public Object GetTableObj(string tableName, Hashtable nodeNameArray, Maintenance maintenance, ref string errorMsg)
        {
            try
            {
                string[] EntityInfoArray = assemblyEntity.ToString().Split(',');
                string[] ServiceInfoArray = assemblyService.ToString().Split(',');

                string EntityInfoName = EntityInfoArray[0] + "." + tableName;
                string ServiceInfoName = ServiceInfoArray[0] + "." + tableName + "Service";

                Type tableType = assemblyEntity.GetType(EntityInfoName, true, false);
                Type serviceType = assemblyService.GetType(ServiceInfoName, true, false);

                Object ServiceObj = assemblyService.CreateInstance(ServiceInfoName);
                MethodInfo find = ServiceObj.GetType().GetMethod("Find", new Type[] { typeof(string) });
                Hashtable columnValue = GetTableColumnValue(nodeNameArray, maintenance, ref errorMsg);
                StringBuilder sb = new StringBuilder();
                foreach (string str in columnValue.Keys)
                {
                    sb.Append(string.Format("{0}='{1}' AND ", str, columnValue[str]));
                }
                string whereStr = sb.ToString();
                whereStr = whereStr.Substring(0, whereStr.Length - 4);
                Object tableObj = find.Invoke(ServiceObj, new object[] { whereStr });
                IList list = tableObj as IList;
                Object obj = null;
                if (list.Count > 0)
                    obj = list;
                return obj;
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "GetTableObj", ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        public Hashtable GetTableColumnValue(Hashtable nodeNameArray, Maintenance maintenance, ref string erroMsg)
        {
            try
            {
                Hashtable ht = new Hashtable();
                foreach (string str in nodeNameArray.Keys)
                {
                    object[] objs = nodeNameArray[str] as object[];
                    if (objs[1] != null)
                    {
                        string[] strs = objs[1] as string[];
                        string foreignKeyValue = GetForeignKey(maintenance, strs[0].Split(','), strs[1], strs[2], ref erroMsg);
                        ht.Add((string)objs[0], foreignKeyValue);
                    }
                    else
                    {
                        ht.Add((string)objs[0], GetMappingValue(maintenance, str, ref erroMsg));
                    }
                }
                return ht;
            }
            catch (Exception ex)
            {
                erroMsg = string.Format("{0}:{1}", "GetTableColumnValue", ex.ToString());
                throw new Exception(erroMsg);
            }
        }
        public void MappingIMX(Maintenance maintenance, XElement node, Type tableType, ref Object obj, string tableName, ref string errorMsg)
        {
            try
            {
                PropertyInfo propertyIMX;
                string getValue = "";
                propertyIMX = tableType.GetProperty(node.Value, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                getValue = GetMappingValue(maintenance, node.Name.ToString(), ref errorMsg);
                if (node.HasAttributes)
                    getValue = getProperInfo(maintenance, node, ref errorMsg);
                if (propertyIMX.PropertyType.Name == "Int32")
                    propertyIMX.SetValue(obj, Convert.ToInt32(getValue), null);
                else if (propertyIMX.PropertyType.Name == "Boolean")
                    propertyIMX.SetValue(obj, ConvertStringToBool(getValue), null);
                else
                    propertyIMX.SetValue(obj, getValue, null);
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "MappingIMX", "Entity name " + tableType.FullName + ", NodeName:" + node.Name + ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        public string getProperInfo(Maintenance maintenance, XElement node, ref string errorMsg)
        {
            try
            {
                List<XAttribute> attributes = node.Attributes().ToList();
                string[] columns = attributes[0].Value.ToString().Split(',');
                string foreignTable = attributes[1].Value.ToString();
                string intNo = attributes[2].Value.ToString();
                return GetForeignKey(maintenance, columns, intNo, foreignTable, ref errorMsg);
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMsg))
                    errorMsg = string.Format("{0}:{1}", "getProperInfo", ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        public string GetForeignKey(Maintenance maintenance, string[] columns, string intNo, string tableName, ref string errorMsg)
        {
            string flag = "", strColumn = "", strValue = "", strWhere = "";
            try
            {
                string[] EntityInfoArray = assemblyEntity.ToString().Split(',');
                string[] ServiceInfoArray = assemblyService.ToString().Split(',');
                string[] QueryInfoArray = assemblyQuery.ToString().Split(',');

                string EntityInfoName = EntityInfoArray[0] + "." + tableName;
                string ServiceInfoName = ServiceInfoArray[0] + "." + tableName + "Service";
                string QueryInfoName = QueryInfoArray[0] + "." + tableName + "Query";

                Type tableType = assemblyEntity.GetType(EntityInfoName, true, false);
                Type serviceType = assemblyService.GetType(ServiceInfoName, true, false);
                Type queryType = assemblyQuery.GetType(QueryInfoName, true, false);

                Object ServiceObj = assemblyService.CreateInstance(ServiceInfoName);
                Object QueryObj = assemblyQuery.CreateInstance(QueryInfoName);
                MethodInfo find = serviceType.GetMethod("Find", new Type[] { typeof(IFilterParameterCollection) });

                MethodInfo query = queryType.GetMethod("AppendEquals", new Type[] { typeof(string), typeof(string) });
                foreach (string str in columns)
                {
                    strColumn = str;
                    strValue = GetMappingValue(maintenance, str, ref errorMsg);
                    query.Invoke(QueryObj, new object[] { str, strValue });
                }
                IFilterParameterCollection iFilterParamter = QueryObj as IFilterParameterCollection;
                strWhere = iFilterParamter.ToString();
                for (int i = 0; i < iFilterParamter.GetParameters().Count; i++)
                {
                    strWhere = strWhere.Replace("@Param" + i, iFilterParamter.GetParameters()[i].Value).Replace("\r\n", "");
                }
                Object tableObj = find.Invoke(ServiceObj, new object[] { QueryObj as IFilterParameterCollection });
                IList list = tableObj as IList;
                Object obj = list[0];
                PropertyInfo property = obj.GetType().GetProperty(intNo, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                if (property != null)
                    flag = property.GetValue(obj, null).ToString();
                return flag;
            }
            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(flag))
                    errorMsg = string.Format("{0}:{1}", "GetForeignKey", "Can't find the Entity " + tableName + " with the condition: " + strWhere);
                else
                    errorMsg = string.Format("{0}:{1}", "GetForeignKey", "Can't find the Entity " + tableName + " with the condition: " + strWhere + "*****" + ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        //get the mapping data value
        public string GetMappingValue(Maintenance maintenance, string column, ref string errorMsg)
        {
            try
            {
                PropertyInfo columnNameProperty, columnDataProperty;
                Type mainType = maintenance.GetType();
                string flag = "", columnNameValue = "";
                for (int i = 0; i < 15; i++)
                {
                    string columnName = "MdmdColumn" + i.ToString() + "Name";
                    string columnData = "MdmdColumn" + i.ToString() + "DataValue";
                    columnNameProperty = mainType.GetProperty(columnName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                    columnDataProperty = mainType.GetProperty(columnData, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                    object objProperty = columnNameProperty.GetValue(maintenance, null);
                    columnNameValue = (objProperty == null ? null : objProperty.ToString());
                    if (column == columnNameValue)
                    {
                        object valueObj = columnDataProperty.GetValue(maintenance, null);
                        flag = valueObj == null ? null : valueObj.ToString();
                        break;
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "GetMappingValue", ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        //get the mapping node:key-nodename,value-nodeValue
        public Hashtable GetNodeName(IEnumerable<XElement> elements, string[] NodeValue, ref string errorMsg)
        {
            string elementName = "";
            try
            {
                Hashtable ht = new Hashtable();
                for (int i = 0; i < NodeValue.Length; i++)
                {
                    foreach (XElement element in elements)
                    {
                        elementName = element.Name.ToString();
                        if (element.Value == NodeValue[i])
                        {
                            object[] objs = new object[2];
                            if (element.HasAttributes)
                            {
                                List<XAttribute> attributes = element.Attributes().ToList();
                                string column = attributes[0].Value.ToString();
                                string foreignTable = attributes[1].Value.ToString();
                                string intNo = attributes[2].Value.ToString();
                                objs[0] = NodeValue[i];
                                objs[1] = new string[] { column, intNo, foreignTable };
                            }
                            else
                            {
                                objs[0] = NodeValue[i];
                                objs[1] = null;
                            }
                            ht.Add(element.Name.ToString(), objs);
                            break;
                        }
                    }
                }
                return ht;
            }
            catch (Exception ex)
            {
                errorMsg = string.Format("{0}:{1}", "GetNodeName", "Element--" + elementName + " " + ex.ToString());
                throw new Exception(errorMsg);
            }
        }
        public bool ConvertStringToBool(string str)
        {
            bool flag = false;
            str = str.ToLower();
            switch (str)
            {
                case "1":
                case "y":
                    flag = true;
                    break;
            }
            return flag;
        }
        public override void MainWork(ref List<QueueLibrary.QueueItem> queueList)
        {
            //throw new NotImplementedException();
        }
        //check the database has the table?
        public bool CheckDatabaseHasTheTable(string tableName,string coulumn)
        {
            bool flag = false;
            string errMsg = string.Empty;
            try
            {
                string[] EntityInfoArray = assemblyEntity.ToString().Split(',');
                string[] ServiceInfoArray = assemblyService.ToString().Split(',');

                string EntityInfoName = EntityInfoArray[0] + "." + tableName;
                string ServiceInfoName = ServiceInfoArray[0] + "." + tableName + "Service";

                Type tableType = assemblyEntity.GetType(EntityInfoName, true, false);
                Type serviceType = assemblyService.GetType(ServiceInfoName, true, false);

                Object ServiceObj = assemblyService.CreateInstance(ServiceInfoName);
                MethodInfo find = ServiceObj.GetType().GetMethod("Find", new Type[] { typeof(string) });

                string whereStr = string.Format("{0}='-1'", coulumn.Split(',')[0]);
                Object tableObj = find.Invoke(ServiceObj, new object[] { whereStr });
                flag = true;
            }
            catch
            {
                //AARTOBase.InitializeAartoConnectionStringForNetTier(IMXconnectStr,
                //    "SIL.IMX.DAL.Data.SqlClient",
                //    "SIL.IMX.DAL.Data",
                //    "SIL.IMX.DAL.Entities.EntityFactory",
                //    "SIL.IMX.DAL.Data.ConnectionString");
            }
            return flag;
        }
        //set delete list opposite order
        public List<DeleteTable> OppositeOrder(List<DeleteTable> list)
        {
            List<DeleteTable> reList = new List<DeleteTable>();
            int count = list.Count;
            for (int i = count-1; i >= 0; i--)
            {
                reList.Add(list[i]);
            }
            return reList;
        }
        #endregion
    }
    public class Maintenance
    {
        public int MdMaintenanceDataId { get; set; }
        public string AuthorityId { get; set; }
        public string MdmdAction { get; set; }
        public DateTime MdmddbDate { get; set; }
        public string MdmdTableName { get; set; }
        public string MdmdColumn0Name { get; set; }
        public string MdmdColumn0DataValue { get; set; }
        public string MdmdColumn1Name { get; set; }
        public string MdmdColumn1DataValue { get; set; }
        public string MdmdColumn2Name { get; set; }
        public string MdmdColumn2DataValue { get; set; }
        public string MdmdColumn3Name { get; set; }
        public string MdmdColumn3DataValue { get; set; }
        public string MdmdColumn4Name { get; set; }
        public string MdmdColumn4DataValue { get; set; }
        public string MdmdColumn5Name { get; set; }
        public string MdmdColumn5DataValue { get; set; }
        public string MdmdColumn6Name { get; set; }
        public string MdmdColumn6DataValue { get; set; }
        public string MdmdColumn7Name { get; set; }
        public string MdmdColumn7DataValue { get; set; }
        public string MdmdColumn8Name { get; set; }
        public string MdmdColumn8DataValue { get; set; }
        public string MdmdColumn9Name { get; set; }
        public string MdmdColumn9DataValue { get; set; }
        public string MdmdColumn10Name { get; set; }
        public string MdmdColumn10DataValue { get; set; }
        public string MdmdColumn11Name { get; set; }
        public string MdmdColumn11DataValue { get; set; }
        public string MdmdColumn12Name { get; set; }
        public string MdmdColumn12DataValue { get; set; }
        public string MdmdColumn13Name { get; set; }
        public string MdmdColumn13DataValue { get; set; }
        public string MdmdColumn14Name { get; set; }
        public string MdmdColumn14DataValue { get; set; }
    }

    public class DeleteTable
    {
        public string TableName
        { get; set; }
        public string TableColumn
        { get; set; }
        public List<Maintenance> DeleteList
        { get; set; }
        public IEnumerable<XElement> Colums
        { get; set; }
    }
}
