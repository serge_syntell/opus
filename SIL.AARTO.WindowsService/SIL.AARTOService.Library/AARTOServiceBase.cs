﻿using System;
using System.Collections.Generic;
using SIL.AARTOService.Resource;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.QueueLibrary;
using Stalberg.TMS;
using System.Data;
using System.Reflection;
using System.IO;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Data.SqlClient;
using SIL.AARTOService.DAL;
using IsolationLevel = System.Transactions.IsolationLevel;

namespace SIL.AARTOService.Library
{
    public enum PrintFileNameType
    {
        Notice,
        Summons,
        WOA
    }

    public enum PrintServiceType
    {
        FirstNotice,
        SecondNotice,
        Summons,
        WOA,
        NewOffender,
        ChangeRegNo,
        CourtRoll,
        NoticeOfWOA,
        RepresentationResult,
        RepresentationInsufficientDetails
    }

    public class AARTOServiceBase : ServiceProcessor<QueueProcess>
    {
        public AARTOServiceBase(ClientService clientService, bool needTransaction = true, int deadlockRetry = 5)
            : base(clientService, needTransaction, deadlockRetry)
        {
            ServiceUtility.InitializeNetTier(GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));
        }

        List<AuthorityDetails> authorityList;
        public List<AuthorityDetails> AuthorityList
        {
            get { return GetAuthorities(); }
        }
        public string currentMethod = "";

        // Constants
        //natis first
        public const int STATUS_LOADED_FIRST = 5;
        public const int STATUS_START_SEND_NATIS_FRAME_FIRST = 11;
        public const int STATUS_SENT_TO_NATIS_FIRST = 100;
        public const int STATUS_NOT_YET_RECEIVED_FROM_NATIS_FIRST = 150;
        public const string CTYPE = "CAM";

        //natis last
        public const int STATUS_LOADED_LAST = 710; //post adjudication
        public const int STATUS_START_SEND_NATIS_FRAME_LAST = 711; //started send to natis
        public const int STATUS_SENT_TO_NATIS_LAST = 730;
        public const int STATUS_NOT_YET_RECEIVED_FROM_NATIS_LAST = 750;

        //natis for notices
        public const int STATUS_START_SENT_TO_NATIS_NOTICE = 29;
        //private const int STATUS_341_LOADED = 22;

        public const int NO_OF_NATIS_RECORDS = 600; //default no of records to place in Natis file

        public int statusProsecute;
        public const int STATUS_PROSECUTE_FIRST = 600;
        public const int STATUS_PROSECUTE_LAST = 800;

        #region Error Processing

        /// <summary>
        /// set queue status, process error, and stop service
        /// </summary>
        /// <param name="item"></param>
        /// <param name="ex"></param>
        /// <param name="errorStatus"></param>
        [Obsolete("please use LogProcessing")]
        public void ErrorProcessing(ref QueueItem item, Exception ex, QueueItemStatus errorStatus = QueueItemStatus.UnKnown)
        {
            //item.IsSuccessful = false;
            //item.Status = errorStatus;
            //ErrorProcessing(ex, item.Body);

            // Oscar 2013-04-10 changed
            LogProcessing(ex, LogType.Error, ServiceOption.BreakAndStop, item, errorStatus);
        }

        /// <summary>
        /// set queue status, process error, and optional stop service
        /// </summary>
        /// <param name="item"></param>
        /// <param name="errorMsg"></param>
        /// <param name="needStopService"></param>
        /// <param name="errorStatus"></param>
        [Obsolete("please use LogProcessing")]
        public void ErrorProcessing(ref QueueItem item, string errorMsg = null, bool needStopService = false, QueueItemStatus errorStatus = QueueItemStatus.UnKnown)
        {
            //item.IsSuccessful = false;
            //item.Status = errorStatus;
            //if (!string.IsNullOrWhiteSpace(errorMsg))
            //    errorMsg = string.Format("Queue Key: {0} | {1}", item.Body, errorMsg);
            //ErrorProcessing(errorMsg, needStopService);

            // Oscar 2013-04-10 changed
            LogProcessing(errorMsg, LogType.Error, needStopService ? ServiceOption.BreakAndStop : ServiceOption.ContinueButQueueFail, item, errorStatus);
        }

        /// <summary>
        /// process error, and stop service
        /// </summary>
        /// <param name="ex"></param>
        [Obsolete("please use LogProcessing")]
        public void ErrorProcessing(Exception ex, object queKey = null)
        {
            //if (ex != null)
            //    this._clientService.Logger.Error(ex, queKey);
            //if (!this._clientService.MustStop)
            //    StopService();

            // Oscar 2013-04-10 changed
            QueueItem item = null;
            if (queKey != null)
            {
                if (_clientService is ServiceDataProcessViaQueue)
                    item = new QueueItem { Body = queKey };
                else if (_clientService is ServiceDataProcessViaFileSystem)
                    item = new QueueItem { Body = new FileInfo(queKey.ToString()) };
            }
            LogProcessing(ex, LogType.Error, ServiceOption.BreakAndStop, item);
        }

        /// <summary>
        /// process error, and optional stop service
        /// </summary>
        /// <param name="errorMsg"></param>
        /// <param name="needStopService"></param>
        [Obsolete("please use LogProcessing")]
        public void ErrorProcessing(string errorMsg = null, bool needStopService = false)
        {
            //if (!string.IsNullOrWhiteSpace(errorMsg))
            //    this._clientService.Logger.Error(errorMsg);
            //if (needStopService && !this._clientService.MustStop)
            //    StopService();

            // Oscar 2013-04-10 changed
            LogProcessing(errorMsg, LogType.Error, needStopService ? ServiceOption.BreakAndStop : ServiceOption.ContinueButQueueFail);
        }

        #endregion

        #region Particular Functions

        public DataSet GetNatisAuthorities(string connString, int startStatus, int endStatus, string type, int autIntNo, out string message)
        {
            DBHelper db = new DBHelper(connString);
            SqlParameter[] paras = new SqlParameter[4];
            paras[0] = new SqlParameter("@StartStatus", startStatus);
            paras[1] = new SqlParameter("@EndStatus", endStatus);
            paras[2] = new SqlParameter("@Type", type);
            paras[3] = new SqlParameter("@AutIntNo", autIntNo);
            return db.ExecuteDataSet("GetNatisAuthData", paras, out message);
        }

        List<AuthorityDetails> GetAuthorities()
        {
            if (this.authorityList == null || this.authorityList.Count <= 0)
            {
                AuthorityDB auth = new AuthorityDB(GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));
                this.authorityList = GetAuthorityDetailsList(auth.GetAuthorityListDS("", 0, ""));
            }
            return this.authorityList;
        }

        List<AuthorityDetails> GetAuthorityDetailsList(DataSet authDS)
        {
            List<AuthorityDetails> authList = new List<AuthorityDetails>();
            AuthorityDetails auth = new AuthorityDetails();
            MemberInfo[] mis = auth.GetType().GetMembers();
            if (authDS != null && authDS.Tables.Count > 0 && authDS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in authDS.Tables[0].Rows)
                {
                    auth = new AuthorityDetails();
                    foreach (MemberInfo mi in mis)
                    {
                        if (mi.MemberType == MemberTypes.Field && authDS.Tables[0].Columns.Contains(mi.Name) && row[mi.Name] != DBNull.Value)
                            ((FieldInfo)mi).SetValue(auth, row[mi.Name]);
                    }
                    authList.Add(auth);
                }
            }
            return authList;
        }

        public int SavePrintFileName(PrintFileNameType pfType, string printFileName, int autIntNo, int IntNo, out string errMsg)
        {
            string spName = string.Empty;
            switch (pfType)
            {
                case PrintFileNameType.Notice:
                    spName = "AddPrintFileNameNotice_WS";
                    break;
                case PrintFileNameType.Summons:
                    spName = "AddPrintFileNameSummons_WS";
                    break;
                case PrintFileNameType.WOA:
                    spName = "AddPrintFileNameWOA_WS";
                    break;
                default:
                    ErrorProcessing(string.Format(ResourceHelper.GetResource("ErrorGetPrintFileNameSP"), LastUser), true);
                    break;
            }
            DBHelper db = new DBHelper(GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));

            SqlParameter[] paras = new SqlParameter[4];
            paras[0] = new SqlParameter("@PrintFileName", printFileName);
            paras[1] = new SqlParameter("@AutIntNo", autIntNo);
            paras[2] = new SqlParameter("@IntNo", IntNo);
            paras[3] = new SqlParameter("@LastUser", LastUser);

            object obj = db.ExecuteScalar(spName, paras, out errMsg);
            int result;
            if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(errMsg))
                result = Convert.ToInt32(obj);
            else
                result = -1;

            return result;
        }

        public string GetPrintFileName(int pfnIntNo, out string msg)
        {
            string pfName = null;
            SqlParameter[] paras = new SqlParameter[1];
            paras[0] = new SqlParameter("@PFNIntNo", pfnIntNo);
            DBHelper db = new DBHelper(GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));
            object result = db.ExecuteScalar("GetPrintFileName_WS", paras, out msg);
            if (result != null)
                pfName = result.ToString();
            return pfName;
        }

        //2013-05-21 added by Nancy 
        public int GetPrintFileCount(int pfnIntNo, string printfilename, string ConjoinType, out string msg)
        {
            int result = 0;
            SqlParameter[] paras = new SqlParameter[3];
            paras[0] = new SqlParameter("@PFNIntNo", pfnIntNo);
            paras[1] = new SqlParameter("@PrintFileName", "");
            paras[2] = new SqlParameter("@ConjoinType", ConjoinType);
            DBHelper db = new DBHelper(GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));
            DataSet ds = db.ExecuteDataSet("GetPrintFileNameConjoinList_WS", paras, out msg);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                result = ds.Tables[0].Rows.Count;
            }
            else
            {
                if (ConjoinType == "S")
                {
                    msg = string.Format("The print file {0} doesn't have valid summons.", pfnIntNo);
                }
                else
                {
                    msg = string.Format("The print file name {0} doesn't have valid notice.", "'" + printfilename + "' : " + pfnIntNo);
                }
            }
            return result;
        }

        public string GetPrintFileDirectory(string parentFolder, string autCode, PrintServiceType psType, string crtName = "", string crtDate = "")
        {
            string path;
            path = parentFolder.Trim();
            autCode = autCode.Trim();

            if (path.EndsWith("\\"))
                path = path.Remove(path.Length - 1, 1);

            path += "\\" + autCode.ToUpper();

            if (psType == PrintServiceType.FirstNotice)
                path += "\\1stNotice";
            else if (psType == PrintServiceType.SecondNotice)
                path += "\\2ndNotice";
            else
                path += "\\" + psType.ToString();

            if (psType == PrintServiceType.CourtRoll)
                path += "\\" + crtName + "\\" + crtDate.Trim().Substring(0, 10);
            else
                path += "\\" + DateTime.Now.ToString("yyyy-MM-dd");

            return path;
        }

        #endregion

    }

    #region Jerry 2012-04-17 move in here from CIP_CofCT_ResponseFileRead

    public class PrintFileNameSetting
    {
        public string SPDDate { get; set; }
        public string RLVDate { get; set; }
        public string NAGDate { get; set; }
        public string ASDDate { get; set; }
        public string ASD_NAGDate { get; set; }

        public PrintFileNameSetting()
        {
            Reset();
        }

        public void Reset()
        {
            string date = string.Format("-{0}-1", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
            this.SPDDate = date;
            this.RLVDate = date;
            this.NAGDate = date;
            this.ASDDate = date;
            this.ASD_NAGDate = date;
        }

        public void GetNew(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                name = string.Empty;
            name = name.Trim().ToUpper();
            string date = string.Empty;
            switch (name)
            {
                case "SPD":
                    date = this.SPDDate;
                    break;
                case "RLV":
                    date = this.RLVDate;
                    break;
                case "NAG":
                    date = this.NAGDate;
                    break;
                case "ASD":
                    date = this.ASDDate;
                    break;
                case "ASD_NAG":
                    date = this.ASD_NAGDate;
                    break;
            }
            date = date.Trim();

            int index = date.LastIndexOf("-");
            if (index >= 0)
            {
                int count = Convert.ToInt32(date.Substring(index + 1, date.Length - index - 1));
                date.Remove(index + 1, date.Length - index - 1);
                count++;
                date += count;
                switch (name)
                {
                    case "SPD":
                        this.SPDDate = date;
                        break;
                    case "RLV":
                        this.RLVDate = date;
                        break;
                    case "NAG":
                        this.NAGDate = date;
                        break;
                    case "ASD":
                        this.ASDDate = date;
                        break;
                    case "ASD_NAG":
                        this.ASD_NAGDate = date;
                        break;
                }
            }
        }
    }

    #endregion

}
