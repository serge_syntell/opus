﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SIL.AARTOService.Library
{
    public static class Extensions
    {
        //public static bool IsNullOrEmpty<T>(this ICollection<T> list)
        //{
        //    return list == null || (list.Count <= 0);
        //}

        //public static bool Equals2(this string s, string value)
        //{
        //    var a = s != null ? s.Trim() : null;
        //    var b = value != null ? value.Trim() : null;
        //    return string.Equals(a, b, StringComparison.OrdinalIgnoreCase);
        //}

        //public static bool In<T>(this T element, params T[] args)
        //{
        //    return !args.IsNullOrEmpty()
        //        && args.Any(arg => typeof(T) == typeof(string)
        //            ? Convert.ToString(arg).Equals2(Convert.ToString(element))
        //            : arg.Equals(element));
        //}

        public static string Trim2(this string source)
        {
            return string.IsNullOrEmpty(source) ? source : source.Trim();
        }
    }
}
