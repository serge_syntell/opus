﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTOService.Resource;
using SIL.QueueLibrary;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;

namespace SIL.AARTOService.Library
{
    public class NoticeWithNoCourtDateManager
    {
        const int Status_RepresentationLoggedNoResult = 410;
        const int Status_SummonsGenerated = 610;
        readonly AuthorityService authorityService = new AuthorityService();
        readonly string connStr, lastUser;
        readonly NoticeService noticeService = new NoticeService();
        readonly NoticeWithNoCourtDateService noticeWithNoCourtDateService = new NoticeWithNoCourtDateService();
        readonly QueueItemProcessor processor = new QueueItemProcessor();
        readonly AARTORules rule = AARTORules.GetSingleTon();
        readonly List<AuthorityAndRule> rules = new List<AuthorityAndRule>();
        readonly ServiceDB serviceDB;
        AuthorityAndRule current;
        DateTime now;

        public NoticeWithNoCourtDateManager(string connStr, string lastUser)
        {
            this.connStr = connStr;
            this.lastUser = lastUser;
            this.serviceDB = new ServiceDB(connStr);
        }

        public Action<string, LogType> OnLog { get; set; }
        public int ProcessedCount { get; private set; }
        public int PushedCount { get; private set; }

        public bool InsertNoticeWithNoCourtDate(int autIntNo, int notIntNo, int crtIntNo, DateTime? actionDate, int priority)
        {
            var paras = new[]
            {
                new SqlParameter("@AutIntNo", autIntNo),
                new SqlParameter("@NotIntNo", notIntNo),
                new SqlParameter("@CrtIntNo", crtIntNo),
                new SqlParameter("@ActionDate", SqlDbType.SmallDateTime) { Value = actionDate.HasValue ? (object)actionDate.Value : DBNull.Value },
                new SqlParameter("@Priority", priority),
                new SqlParameter("@LastUser", SqlDbType.NVarChar, 50) { Value = this.lastUser }
            };

            try
            {
                var obj = this.serviceDB.ExecuteScalar("NoticeWithNoCourtDateAdd_WS", paras);
                var result = Convert.ToInt32(obj);
                if (result < 0)
                {
                    switch (result)
                    {
                        case -1:
                            WriteLog(ResourceHelper.GetResource("UnableToFindOffenceDate", notIntNo));
                            break;
                        case -2:
                            WriteLog(ResourceHelper.GetResource("FailToInsertIntoNoticeWithNoCourtDate", notIntNo, actionDate.HasValue ? actionDate.Value.ToString("yyyy-MM-dd HH:mm:ss") : ""));
                            break;
                    }
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
            return false;
        }

        public bool Process(string autCode, int crtIntNo, DateTime courtDate)
        {
            return Initialize(autCode)
                && GetNoticeWithNoCourtDateList(crtIntNo, courtDate).All(ProcessNoticeWithNoCourtDate);
        }

        #region

        void WriteLog(string msg, LogType type = LogType.Error)
        {
            if (OnLog != null && !string.IsNullOrWhiteSpace(msg))
                OnLog(msg, type);
        }

        // initialize authority and rule
        bool Initialize(string autCode, bool getRule = true)
        {
            this.now = DateTime.Now;
            this.current = null;
            ProcessedCount = PushedCount = 0;

            AuthorityAndRule aar;

            if (string.IsNullOrWhiteSpace(autCode))
            {
                WriteLog(ResourceHelper.GetResource("InvalidAuthority", autCode));
                return false;
            }

            autCode = autCode.Trim();
            if ((aar = this.rules.FirstOrDefault(a => a.AutCode.Equals(autCode))) == null)
            {
                Authority authority;
                if ((authority = this.authorityService.GetByAutCode(autCode)) == null)
                {
                    WriteLog(ResourceHelper.GetResource("InvalidAuthority", autCode));
                    return false;
                }

                aar = new AuthorityAndRule
                {
                    AutIntNo = authority.AutIntNo,
                    AutCode = autCode
                };

                this.rules.Add(aar);
            }

            if (getRule && aar.DaysBeforeCourtDate <= 0)
            {
                try
                {
                    this.rule.InitParameter(this.connStr, autCode);
                    aar.DaysBeforeCourtDate = this.rule.Rule("NotIssueSummonsDate", "CDate").DtRNoOfDays;
                }
                catch (Exception ex)
                {
                    WriteLog(ex.ToString());
                    return false;
                }
            }

            this.current = aar;

            return true;
        }

        #endregion

        #region process

        // get list of NoticeWithNoCourtDate
        IEnumerable<NoticeWithNoCourtDate> GetNoticeWithNoCourtDateList(int crtIntNo, DateTime courtDate)
        {
            //if (this.now >= courtDate.AddDays(1 - this.current.DaysBeforeCourtDate))
            //    return new List<NoticeWithNoCourtDate>();

            //var where = string.Format(" AutIntNo = {0} AND CrtIntNo = {1} ", this.current.AutIntNo, crtIntNo);
            //return this.noticeWithNoCourtDateService.Find(where);

            return this.noticeWithNoCourtDateService.GetNoticeWithNoCourtDateList(this.current.AutIntNo, crtIntNo, courtDate, this.current.DaysBeforeCourtDate);
        }

        bool ProcessNoticeWithNoCourtDate(NoticeWithNoCourtDate nwncd)
        {
            try
            {
                var notice = this.noticeService.GetByNotIntNo(nwncd.NotIntNo);

                using (var scope = ServiceUtility.CreateTransactionScope())
                {
                    if (!PushGenerateSummons(nwncd, notice)
                        || !RemoveNoticeWithNoCourtDate(nwncd))
                        return false;

                    if (!ServiceUtility.TransactionCanCommit())
                    {
                        WriteLog(new TransactionAbortedException().Message);
                        return false;
                    }

                    scope.Complete();
                }

                return true;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
            return false;
        }

        bool PushGenerateSummons(NoticeWithNoCourtDate nwncd, Notice notice)
        {
            if (notice.NoticeStatus == Status_RepresentationLoggedNoResult
                || notice.NoticeStatus >= Status_SummonsGenerated)
            {
                WriteLog(ResourceHelper.GetResource("NoticeIsNotEligibleForSummons", notice.NotTicketNo, notice.NoticeStatus), LogType.Info);
                return true;
            }

            try
            {
                var group = new[]
                {
                    this.current.AutCode,
                    notice.NotFilmType.Equals2("H") && !notice.IsVideo ? "H" : "",
                    notice.NotCourtName
                };

                this.processor.Send(new QueueItem
                {
                    Body = nwncd.NotIntNo,
                    Group = string.Join("|", group),
                    ActDate = nwncd.SeQuActionDate,
                    Priority = nwncd.SeQuPriority,
                    QueueType = ServiceQueueTypeList.GenerateSummons,
                    LastUser = this.lastUser
                });

                PushedCount++;
                return true;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
            return false;
        }

        bool RemoveNoticeWithNoCourtDate(NoticeWithNoCourtDate nwncd)
        {
            try
            {
                if (this.noticeWithNoCourtDateService.Delete(nwncd))
                {
                    ProcessedCount++;
                    return true;
                }
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
            return false;
        }

        #endregion

        class AuthorityAndRule
        {
            public int AutIntNo { get; set; }
            public string AutCode { get; set; }
            public int DaysBeforeCourtDate { get; set; }
        }
    }
}
