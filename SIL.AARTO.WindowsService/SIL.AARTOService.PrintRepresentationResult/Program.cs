﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PrintRepresentationResult
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.PrintRepresentationResult"
                ,
                DisplayName = "SIL.AARTOService.PrintRepresentationResult"
                ,
                Description = "SIL AARTOService PrintRepresentationResult"
            };

            ProgramRun.InitializeService(new PrintRepresentationResult(), serviceDescriptor, args);
        }
    }
}
