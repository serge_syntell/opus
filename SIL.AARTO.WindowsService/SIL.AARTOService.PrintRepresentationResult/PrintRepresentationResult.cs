﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PrintRepresentationResult
{
    partial class PrintRepresentationResult : ServiceHost
    {
        public PrintRepresentationResult()
            : base("", new Guid("32A79378-61C2-45D2-85C3-5BB4F18628EA"), new PrintRepresentationResultService())
        {
            InitializeComponent();
        }
    }
}
