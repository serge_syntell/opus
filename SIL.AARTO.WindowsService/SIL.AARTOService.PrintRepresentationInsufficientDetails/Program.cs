﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PrintRepresentationInsufficientDetails
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.PrintRepresentationInsufficientDetails"
                ,
                DisplayName = "SIL.AARTOService.PrintRepresentationInsufficientDetails"
                ,
                Description = "SIL AARTOService PrintRepresentationInsufficientDetails"
            };

            ProgramRun.InitializeService(new PrintRepresentationInsufficientDetails(), serviceDescriptor, args);
        }
    }
}
