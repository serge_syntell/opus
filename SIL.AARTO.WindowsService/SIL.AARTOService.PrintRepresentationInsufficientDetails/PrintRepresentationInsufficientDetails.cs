﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PrintRepresentationInsufficientDetails
{
    partial class PrintRepresentationInsufficientDetails : ServiceHost
    {
        public PrintRepresentationInsufficientDetails()
            : base("", new Guid("18325D58-4C4E-4375-AE4F-BBBC08277370"), new PrintRepresentationInsufficientDetailsService())
        {
            InitializeComponent();
        }        
    }
}
