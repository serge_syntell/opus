﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CIP_CofCT_ResponseFileRead
{
    partial class CIP_CofCT_ResponseFileRead : ServiceHost
    {
        public CIP_CofCT_ResponseFileRead()
            : base("", new Guid("C37E761F-E67B-4C96-83CC-5F57BBFBE57D"), new CIP_CofCT_ResponseFileReadService())
        {
            InitializeComponent();
        }
    }
}
