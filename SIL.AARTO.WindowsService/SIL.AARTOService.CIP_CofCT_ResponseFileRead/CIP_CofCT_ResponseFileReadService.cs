﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using System.IO;
using Stalberg.TMS;
using SIL.AARTOService.Resource;
using System.Data;
using System.Transactions;
using System.Configuration;
using System.Data.SqlClient;
using Stalberg.TMS_TPExInt.Components;
using Stalberg.TMS.Data;
using System.Text.RegularExpressions;
using SIL.AARTOService.DAL;

namespace SIL.AARTOService.CIP_CofCT_ResponseFileRead
{
    #region Jerry 2012-04-17 move it in AARTOServiceBase
    //public class PrintFileNameSetting
    //{
    //    public string SPDDate { get; set; }
    //    public string RLVDate { get; set; }
    //    public string NAGDate { get; set; }
    //    public string ASDDate { get; set; }
    //    public string ASD_NAGDate { get; set; }

    //    public PrintFileNameSetting()
    //    {
    //        Reset();
    //    }

    //    public void Reset()
    //    {
    //        string date = string.Format("-{0}-1", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
    //        this.SPDDate = date;
    //        this.RLVDate = date;
    //        this.NAGDate = date;
    //        this.ASDDate = date;
    //        this.ASD_NAGDate = date;
    //    }

    //    public void GetNew(string name)
    //    {
    //        if (string.IsNullOrWhiteSpace(name))
    //            name = string.Empty;
    //        name = name.Trim().ToUpper();
    //        string date = string.Empty;
    //        switch (name)
    //        {
    //            case "SPD":
    //                date = this.SPDDate;
    //                break;
    //            case "RLV":
    //                date = this.RLVDate;
    //                break;
    //            case "NAG":
    //                date = this.NAGDate;
    //                break;
    //            case "ASD":
    //                date = this.ASDDate;
    //                break;
    //            case "ASD_NAG":
    //                date = this.ASD_NAGDate;
    //                break;
    //        }
    //        date = date.Trim();

    //        int index = date.LastIndexOf("-");
    //        if (index >= 0)
    //        {
    //            int count = Convert.ToInt32(date.Substring(index + 1, date.Length - index - 1));
    //            date.Remove(index + 1, date.Length - index - 1);
    //            count++;
    //            date += count;
    //            switch (name)
    //            {
    //                case "SPD":
    //                    this.SPDDate = date;
    //                    break;
    //                case "RLV":
    //                    this.RLVDate = date;
    //                    break;
    //                case "NAG":
    //                    this.NAGDate = date;
    //                    break;
    //                case "ASD":
    //                    this.ASDDate = date;
    //                    break;
    //                case "ASD_NAG":
    //                    this.ASD_NAGDate = date;
    //                    break;
    //            }
    //        }
    //    }
    //}
    #endregion

    public class CIP_CofCT_ResponseFileReadService : ServiceDataProcessViaFileSystem
    {
        private const string RESPONSE_FILE_PREFIX_CC = "cores";
        private const string INFRINGEMENT_FILE_PREFIX_CC = "cocon";
        private const int STATUS_RESPONSE_NON_SUMMONS = 170;
        private const int STATUS_RESPONSE_NS_FAILED = 961;
        private const int STATUS_RESPONSE_LOADED = 200;
        private const int STATUS_EXPIRED = 921;
        private const int STATUS_ERRORS = 110;
        private const int STATUS_NO_AOG_CC = 235;
        private const string SUMMONS_IDENTIFIER = "999";
        private const string FILE_EXT = ".txt";
        private const string NAME = "Aarto";

        FileInfo fileInfo;
        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        string connAARTODB, connCIP_CofCTUNC;

        int batchSize;
        Dictionary<int, int> pfnList = new Dictionary<int, int>();
        string autCode;
        PrintFileNameSetting pf1stNotice = new PrintFileNameSetting();

        public CIP_CofCT_ResponseFileReadService()
            : base("", "", new Guid("E55693AA-6DB8-4B1D-8425-25FA205A336B"))
        {
            this._serviceHelper = new AARTOServiceBase(this, false);
            this.Recursion = false;
            // 2012-02-21 jerry change
            this.connCIP_CofCTUNC = AARTOBase.GetConnectionString(ServiceConnectionNameList.CiprusCofCT_LoadFiles, ServiceConnectionTypeList.UNC);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);

            AARTOBase.OnServiceStarting = () =>
            {
                AARTOBase.CheckFolder(this.connCIP_CofCTUNC);
                SetServiceParameters();
            };
        }

        public override void PrepareWork()
        {
            //this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            //this.connCIP_CofCTUNC = AARTOBase.GetConnectionString(ServiceConnectionNameList.CiprusCofCT_LoadFiles, ServiceConnectionTypeList.UNC);
            //Jerry 2012-04-17 change
            //connResponseFolder = Path.Combine(ServiceParameters["CiprusCofCTExportFolder"], "ResponseFiles");
            this.DestinationPath = connCIP_CofCTUNC;

            this.GetDataFilesFromTP_CofCT(connCIP_CofCTUNC);
        }

        public override void InitialWork(ref QueueItem item)
        {
            if (item.Body != null && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    //Jerry 2012-04-18 change
                    //string file = fileInfo.FullName;
                    fileInfo = (FileInfo)item.Body;
                    string file = fileInfo.FullName;

                    if (Path.GetFileName(file).ToLower().StartsWith(RESPONSE_FILE_PREFIX_CC) && Path.GetExtension(file).ToLower() == FILE_EXT)
                    {
                        item.IsSuccessful = true;
                        item.Status = QueueItemStatus.Discard;
                        //this.fileInfo = (FileInfo)item.Body;

                        InitialBatch();
                    }
                    else
                        AARTOBase.ErrorProcessing(ref item);
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                return;
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                if (!item.IsSuccessful)
                    continue;

                try
                {
                    bool failed;
                    fileInfo = (FileInfo)item.Body;
                    string file = fileInfo.FullName;
                    item.Status = QueueItemStatus.Discard;


                    //BD need to check for 999 in file name to ensure we are dealing with a summons response file
                    if (Path.GetFileName(file).ToLower().StartsWith(RESPONSE_FILE_PREFIX_CC) && Path.GetExtension(file).ToLower() == FILE_EXT && Path.GetFileName(file).ToLower().IndexOf(SUMMONS_IDENTIFIER, 0) == 8)
                    {
                        Logger.Info(ResourceHelper.GetResource("ResponseFile_StartingLoadFilesNSD") + DateTime.Now.ToString());
                        failed = GetResponseNSDataFromTP();

                        if (failed)
                            Logger.Warning(ResourceHelper.GetResource("ResponseFile_LoadFileFailedNSD") + DateTime.Now.ToString());
                        else
                            Logger.Info(ResourceHelper.GetResource("ResponseFile_LoadFileCompletedNSD") + DateTime.Now.ToString());
                    }
                    else if (Path.GetFileName(file).ToLower().StartsWith(RESPONSE_FILE_PREFIX_CC) && Path.GetExtension(file).ToLower() == FILE_EXT)
                    {
                        // load the  latest response files
                        Logger.Info(ResourceHelper.GetResource("ResponseFile_StartingLoadFiles", NAME) + DateTime.Now.ToString());
                        failed = GetResponseDataFromTP(fileInfo);

                        if (failed)
                            Logger.Warning(ResourceHelper.GetResource("ResponseFile_LoadFileFailed", NAME) + DateTime.Now.ToString());
                        else
                            Logger.Info(ResourceHelper.GetResource("ResponseFile_LoadFileCompleted", NAME) + DateTime.Now.ToString());
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        public override void FinaliseData(List<QueueItem> queueList) { }

        private ResponseProcessCodeList responseCodeList = new ResponseProcessCodeList();

        private bool GetResponseNSDataFromTP()
        {
            bool failed = false;
            int countFailed = 0;
            int countOK = 0;
            int countExpired = 0;

            CameraUnitBatchDB batch = new CameraUnitBatchDB(connAARTODB);
            NoticeDB notice = new NoticeDB(connAARTODB);
            CameraUnitPanelDB panel = new CameraUnitPanelDB(connAARTODB);
            ChargeDB charge = new ChargeDB(connAARTODB);

            int autIntNo = 0;
            string autNo = "";
            string srLine = "";
            string file = fileInfo.FullName;
            string fileName = fileInfo.Name;


            autIntNo = 0;
            bool fileFailed = false;

            if (!failed)
            {
                // open the file and update the each database table separately
                FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(fs, System.Text.Encoding.Default);
                while (!sr.EndOfStream && !fileFailed)
                {
                    srLine = sr.ReadLine();

                    if (srLine.Length > 11)
                    {
                        //This needs to check the response code not the record type
                        //string recordType = srLine.Substring(0, 2);
                        string ResponseCode = srLine.Substring(5, 2);

                        switch (ResponseCode)
                        {
                            case "14": //Contravention accepted, notice number generated
                            case "67": //rejected natis conflict
                            case "68": //expired, too late to process
                            case "69": //non-summons registration - other reason
                                // process the response records
                                failed = ProcessResponseNSRecord(file, srLine, ref countFailed, ref countOK, ref autNo, ref autIntNo, ref countExpired, ref fileFailed);
                                break;
                            case "99":
                                // 99=file traier record
                                // 99|C|01|000193
                                // -2n|interface-1a|version-2n|count-6n
                                break;
                        }
                    }
                } // end of while sr

                sr.Close();
                fs.Close();

                // move the file to the response files subfolder - new sub folder for the summons files
                if (!Directory.Exists(connCIP_CofCTUNC + @"\ResponseFiles\SummonsCompleted\" + autNo))
                    Directory.CreateDirectory(connCIP_CofCTUNC + @"\ResponseFiles\SummonsCompleted\" + autNo);

                // move the file to the response files subfolder - new sub folder for the summons files
                if (!Directory.Exists(connCIP_CofCTUNC + @"\ResponseFiles\SummonsError\"))
                    Directory.CreateDirectory(connCIP_CofCTUNC + @"\ResponseFiles\SummonsError\");

                if (fileFailed)
                {
                    Logger.Warning(ResourceHelper.GetResource("ResponseFile_ProcessedFailedNSD") + file);
                    this.FileToLocalFolder(file, connCIP_CofCTUNC + @"\ResponseFiles\SummonsError\", "response", "move");
                }
                else
                {
                    Logger.Info(ResourceHelper.GetResource("ResponseFile_ProcessedOKNSD") + file);
                    this.FileToLocalFolder(file, connCIP_CofCTUNC + @"\ResponseFiles\SummonsCompleted\" + autNo + @"\", "response", "move");
                }
            }
            return failed;
        }

        private bool GetResponseDataFromTP(FileInfo fileInfo)
        {
            bool failed = false;
            int countFailed = 0;
            int countOK = 0;
            int cubIntNo = 0;
            int countExpired = 0;
            string errMessage = "";
            DateTime fileDate = DateTime.Now;

            CameraUnitBatchDB batch = new CameraUnitBatchDB(connAARTODB);
            NoticeDB notice = new NoticeDB(connAARTODB);
            CameraUnitPanelDB panel = new CameraUnitPanelDB(connAARTODB);
            ChargeDB charge = new ChargeDB(connAARTODB);

            int autIntNo = 0;
            string autNo = "";
            string srLine = "";
            string file = fileInfo.FullName;
            string fileName = fileInfo.Name;

            autIntNo = 0;

            string infringementFile = INFRINGEMENT_FILE_PREFIX_CC + fileName.Substring(5, 10) + FILE_EXT;
            string camUnitID = fileName.Substring(8, 3);

            bool fileFailed = false;

            if (!failed)
            {
                // open the file and update the each database table separately
                FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(fs, System.Text.Encoding.Default);
                while (!sr.EndOfStream && !fileFailed)
                {
                    srLine = sr.ReadLine();

                    if (srLine.Length > 2)
                    {
                        string recordType = srLine.Substring(0, 2);

                        switch (recordType)
                        {
                            case "61":
                                // process the response records
                                failed = ProcessResponseRecord(file, srLine, ref countFailed, ref countOK, ref autNo, ref autIntNo, ref countExpired, ref fileFailed);
                                break;
                            case "99":
                                //failed = true;
                                // 99=file traier record
                                // 99|C|01|000193
                                // -2n|interface-1a|version-2n|count-6n
                                break;
                        }
                    }
                } // end of while sr

                sr.Close();
                fs.Close();

                //dls 071207 - need to get the batch details regardless of whether there are failures or not
                SqlDataReader reader = batch.CameraUnitBatchDetails(autNo, camUnitID, infringementFile);

                if (countFailed > 0)
                {
                    failed = false;
                    while (reader.Read() && !failed)
                    {
                        if (reader["CUBFileName"].ToString().Equals(""))
                        {
                            failed = true;
                            Logger.Info(ResourceHelper.GetResource("ResponseFile_UnableFindBatch") + infringementFile);
                        }
                        //else
                        //{
                        //    cubIntNo = Convert.ToInt32(reader["CUBIntNo"]);

                        //     //cancel the batch
                        //    int updCUBIntNo = batch.UpdateCameraUnitBatchColumn(autNo, camUnitID, infringementFile, AARTOBase.lastUser, cubIntNo, "Response file failed - " + countFailed.ToString() + " rows", "CUBReasonFailed", ref errMessage);

                        //    if (updCUBIntNo < 1)
                        //    {
                        //        Logger.Info("GetResponseDataFromTP: unable to cancel infringement date for file " + infringementFile + " - " + errMessage);
                        //        failed = true;
                        //    }

                        //}
                        if (countExpired > 0)
                        {
                            //dls 071220 - - new CiprusPI code = 53 Offence date too old (30-day rule)
                            int noOfRows = notice.UpdateExpiredNotices(autIntNo, camUnitID, AARTOBase.LastUser, infringementFile, STATUS_EXPIRED, ref errMessage, STATUS_ERRORS);

                            if (noOfRows < 0)
                            {
                                Logger.Warning(ResourceHelper.GetResource("ResponseFile_UableUpdateExpiredNotice", NAME, infringementFile, errMessage));
                                failed = true;
                            }
                            else if (noOfRows > 0)
                            {
                                Logger.Warning(ResourceHelper.GetResource("ResponseFile_NoticesExpired", noOfRows.ToString(), infringementFile));
                            }
                        }
                    }

                    reader.Dispose();
                }

                if (countOK > 0 || countFailed > 0)
                {
                    cubIntNo = batch.UpdateCameraUnitBatch(autNo, camUnitID, infringementFile, AARTOBase.LastUser, fileDate, countFailed, countOK, ref errMessage);

                    if (cubIntNo < 1)
                    {
                        Logger.Warning(ResourceHelper.GetResource("ResponseFile_UnableUpdateBatch") + file);
                    }
                }

                // move the file to the response files completed subfolder
                if (!Directory.Exists(connCIP_CofCTUNC + @"\ResponseFiles\Completed\" + autNo))
                    Directory.CreateDirectory(connCIP_CofCTUNC + @"\ResponseFiles\Completed\" + autNo);

                // move the file to the response files error subfolder
                if (!Directory.Exists(connCIP_CofCTUNC + @"\ResponseFiles\Error\"))
                    Directory.CreateDirectory(connCIP_CofCTUNC + @"\ResponseFiles\Error\");

                if (fileFailed)
                {
                    Logger.Info(ResourceHelper.GetResource("ResponseFile_ProcessedFailed") + file);
                    this.FileToLocalFolder(file, connCIP_CofCTUNC + @"\ResponseFiles\Error\", "response", "move");
                }
                else
                {
                    Logger.Info(ResourceHelper.GetResource("ResponseFile_ProcessedOK") + file);
                    this.FileToLocalFolder(file, connCIP_CofCTUNC + @"\ResponseFiles\Completed\" + autNo + @"\", "response", "move");
                }
            }
            return failed;
        }


        //BD need to process data for the non summons file recived.
        private bool ProcessResponseNSRecord(string file, string srLine, ref int countFailed, ref int countOK, ref string autNo, ref int autIntNo, ref int countExpired, ref bool fileFailed)
        {
            bool failed = false;
            int processingCode = 0;
            string processDescr = "";
            int notIntNo = 0;
            string noticeNumber = "";

            if (autIntNo == 0)
            {
                //for the first record, we need to find the authority
                autNo = Path.GetFileName(file).Substring(5, 3);

                // if the local authority is missing bail out immediately swearing
                TMSData tmsData = new TMSData(connAARTODB);
                autIntNo = tmsData.GetAutIntFromAuthorityFromAutNo(autNo);
                if (autIntNo <= 0)
                {
                    Logger.Info(ResourceHelper.GetResource("ResponseFile_ErrorAuthName", file, autNo));
                    //failed = true;
                    //return failed;
                    fileFailed = true;
                    return fileFailed;
                }

            }

            // Oscar 20120323 add, get autCode
            int AutIntNo = autIntNo;
            this.autCode = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutIntNo == AutIntNo).AutCode.Trim();

            try
            {
                processingCode = int.Parse(srLine.Substring(5, 2));
                //need to update charge/notice with correct status
                // get the description
                processDescr = responseCodeList.GetDescrByCode(processingCode);

                // update frame
                string errMessage = string.Empty;
                NoticeDB notice = new NoticeDB(connAARTODB);

                string recordType = srLine.Substring(0, 2);
                int updNotIntNo = 0;
                notIntNo = Convert.ToInt32(srLine.Substring(164, 15));
                noticeNumber = srLine.Substring(16, 16);
                noticeNumber = noticeNumber.Substring(0, 2) + "/" + noticeNumber.Substring(2, 5) + "/" + noticeNumber.Substring(7, 3) + "/" + noticeNumber.Substring(10, 6);

                //must check processing code
                switch (processingCode.ToString())
                {
                    case "14": //Contravention accepted, notice number generated
                        updNotIntNo = notice.UpdateCiprusCofCTNSResponse(noticeNumber, processDescr, AARTOBase.LastUser, notIntNo, STATUS_RESPONSE_LOADED, ref errMessage);

                        // Oscar 20120323 add, process print file
                        if (updNotIntNo > 0)
                            ProcessPrintFile(autIntNo, updNotIntNo);

                        break;
                    case "67": //rejected natis conflict
                    case "68": //expired, too late to process
                    case "69": //non-summons registration - other reason
                        // process the response records
                        updNotIntNo = notice.UpdateCiprusCofCTNSResponse("", processDescr, AARTOBase.LastUser, notIntNo, STATUS_RESPONSE_NS_FAILED, ref errMessage);
                        break;
                    case "99":
                        // 99=file traier record
                        // 99|C|01|000193
                        // -2n|interface-1a|version-2n|count-6n
                        break;
                }

                if (updNotIntNo <= 0)
                {
                    failed = true;

                    switch (updNotIntNo)
                    {
                        case -1:
                            errMessage = ResourceHelper.GetResource("Result_NoticeNotExist");
                            break;

                        case -2:
                            errMessage = ResourceHelper.GetResource("Result_UnableUpdateNotice");
                            break;

                        case -3:
                            errMessage = ResourceHelper.GetResource("Result_UnableUpdateCharge");
                            break;

                        case -4:
                            errMessage = ResourceHelper.GetResource("Result_ErrorChargeStatus");
                            break;

                    }

                    Logger.Error(ResourceHelper.GetResource("ResponseFile_FaildedUpdateNotice", file, notIntNo, errMessage));
                }
                else
                {
                    //QueueItemProcessor queueProcessor = new QueueItemProcessor();
                    //QueueItem pushItem = new QueueItem();
                    //pushItem = new QueueItem();
                    //pushItem.Body = updNotIntNo.ToString();
                    //pushItem.Group = autNo;
                    //pushItem.QueueType = ServiceQueueTypeList.Print1stNotice;
                    //queueProcessor.Send(pushItem);

                    Logger.Info(ResourceHelper.GetResource("ResponseFile_UpdateNoticeSuccessfully", srLine.Substring(8, 6)));
                }
                return failed;
            }
            catch
            {
                Logger.Info(ResourceHelper.GetResource("ResponseFile_ProcessCodeInvalid", file));
                failed = true;
                return failed;
            }
        }


        private bool ProcessResponseRecord(string file, string srLine, ref int countFailed, ref int countOK, ref string autNo, ref int autIntNo, ref int countExpired, ref bool fileFailed)
        {
            bool failed = false;

            int processingCode = 0;
            int processingType = 0;
            int severityCode = 0;

            int notIntNo = 0;
            string processDescr = "";

            CiprusCofCT61Record rec61 = new CiprusCofCT61Record();

            // read a row get the response and the tms key from the original row
            // processing code = 01-99 position 5 length 2
            // severity code 0 = OK; 2 = rejected in position 7 length 1

            // original record 66 starts in position 149 + 27 = 176 start of reference no length 15 notintno
            // 

            if (autIntNo == 0)
            {
                //for the first record, we need to find the authority
                autNo = Path.GetFileName(file).Substring(5, 3);

                // if the local authority is missing bail out immediately swearing
                TMSData tmsData = new TMSData(connAARTODB);
                autIntNo = tmsData.GetAutIntFromAuthorityFromAutNo(autNo);
                if (autIntNo <= 0)
                {
                    Logger.Error(ResourceHelper.GetResource("ResponseFile_ErrorAuthName", file, autNo));
                    //failed = true;
                    //return failed;
                    fileFailed = true;
                    return fileFailed;
                }

                // Oscar 20120323 add, get autCode
                int AutIntNo = autIntNo;
                this.autCode = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutIntNo == AutIntNo).AutCode.Trim();

                //// need to check whether this file is the correct one in the sequence to be loaded
                ////*******************************************************************************

                //check that this is the response file that we are expecting for this camera/autno
                CameraUnitBatchDB batch = new CameraUnitBatchDB(connAARTODB);

                string fileName = Path.GetFileName(file);
                int batchNo = Convert.ToInt32(fileName.Substring(11, 4));
                char pad0Char = Convert.ToChar("0");

                string camUnitID = fileName.Substring(8, 3);

                string infringementFile = INFRINGEMENT_FILE_PREFIX_CC + autNo + camUnitID + batchNo.ToString().PadLeft(4, pad0Char) + FILE_EXT;

                SqlDataReader batchReader = batch.CameraUnitBatchCheckResponse(autNo, camUnitID, infringementFile);

                while (batchReader.Read())
                {
                    if (batchReader["CUBFileName"].ToString().Equals(""))
                    {
                        //there is no infringement file for this response file
                        Logger.Error(ResourceHelper.GetResource("ResponseFile_NoInfrigementFileCreated") + file);
                        //failed = true;
                        fileFailed = true;
                    }
                    else if (batchReader["CUBReasonFailed"].ToString().Equals("Cancelled"))
                    {
                        //there is no infringement file for this response file
                        Logger.Error(ResourceHelper.GetResource("ResponseFile_InfrigementFileCancelled") + file);
                        //failed = true;
                        fileFailed = true;
                    }
                    //dls 091022 - they seem to be incapable of loading the files in the correct order, so don't bother to check!
                    else if (batchReader["CUBFileName"].ToString().Equals(infringementFile) && !batchReader["CUBResponseDate"].Equals(""))
                    {
                        Logger.Error(ResourceHelper.GetResource("ResponseFile_AlreadyLoaded", file));
                        //failed = true;
                        fileFailed = true;
                    }
                    break;
                }

                batchReader.Close();

                //if (failed) return failed;
                if (fileFailed) return fileFailed;
            }

            //BD changes to handle process reponse code 66. [Actually record type!!!]
            try
            {
                processingType = int.Parse(srLine.Substring(0, 2));
                processingCode = int.Parse(srLine.Substring(5, 2));
                if (processingType == 61)
                {
                    rec61.processCode = srLine.Substring(5, 2); // process code returned by ciprus indicates action required if any

                    try
                    {
                        rec61.severityCode = srLine.Substring(7, 1); // 0=processed OK; 2=record rejected

                        severityCode = int.Parse(rec61.severityCode);
                    }
                    catch
                    {
                        Logger.Error(ResourceHelper.GetResource("ResponseFile_SeverityCodeInvalid", file));
                        failed = true;
                        return failed;
                    }

                }
                else if (processingType == 53)
                    countExpired++;
            }
            catch
            {
                Logger.Error(ResourceHelper.GetResource("ResponseFile_ProcessCodeInvalid", file));
                failed = true;
                return failed;
            }

            try
            {
                if (srLine.Length > 15 && processingCode != 4)
                {
                    if (srLine.Substring(0, 2) == "61")
                    {
                        rec61.recType = srLine.Substring(0, 2);

                        //mrs 20090206 the transaction data field is 50 length, we should add code to process the notintno out of whatever is there
                        notIntNo = Convert.ToInt32(srLine.Substring(164, 15)); // notIntNo is the unique reference sent to ciprus

                        if (severityCode == 0)
                        {
                            rec61.recordNumber = srLine.Substring(8, 6);
                            rec61.legislation = srLine.Substring(14, 1); //1= print aarto-03; should be 2 for Ciprus
                            rec61.noticeNoType = srLine.Substring(15, 1); //1=aarto notice no; 2=ciprus notice no
                            rec61.noticeNumber = srLine.Substring(16, 16);
                            rec61.issuingAuthority = srLine.Substring(32, 40).Trim();
                            rec61.officeNatisNo = srLine.Substring(72, 9).Trim(); //infrastructure no
                            rec61.officerName = srLine.Substring(81, 40).Trim();
                            rec61.officerInit = srLine.Substring(121, 5).Trim();
                            rec61.penaltyAmount = srLine.Substring(126, 8).Trim();
                            rec61.discountAmount = srLine.Substring(134, 8).Trim();
                            rec61.discountedAmount = srLine.Substring(142, 8).Trim();
                            rec61.chargeType = srLine.Substring(150, 10).Trim(); //minor or major
                            rec61.demeritPoints = srLine.Substring(160, 4).Trim();
                            rec61.tranData = srLine.Substring(164, 50).Trim();
                            //rec61.uniqueDocID = srLine.Substring(208, 12);
                            //mrs 20080205 1.07 changed length of trandata
                            rec61.uniqueDocID = srLine.Substring(214, 12).Trim();

                            //rec61.infringementRecord = srLine.Substring(221, 900);
                            //rec61.infringementRecord = srLine.Substring(226, 939);
                            rec61.infringementRecord = srLine.Substring(226, 1086);

                            if (rec61.noticeNoType.Equals("1"))
                            {
                                rec61.noticeNumber = string.Format("{0}-{1}-{2}-{3}",
                                    rec61.noticeNumber.Substring(0, 2), rec61.noticeNumber.Substring(2, 4),
                                    rec61.noticeNumber.Substring(6, 9), rec61.noticeNumber.Substring(15, 1));
                            }
                            else if (rec61.noticeNoType.Equals("2"))
                            {
                                rec61.noticeNumber = string.Format("{0}/{1}/{2}/{3}",
                                    rec61.noticeNumber.Substring(0, 2), rec61.noticeNumber.Substring(2, 5),
                                    rec61.noticeNumber.Substring(7, 3), rec61.noticeNumber.Substring(10, 6));
                            }

                            rec61.ciprusLoadDate = string.Empty;
                            rec61.paymentDate = string.Empty;

                            //dls 090622 - this does need to go in, but need to check the position in the string
                            if (srLine.Length > 1312)
                            {
                                //dls 090803 - need to laod payment date - filler changes to length 92
                                //rec61.Filler100 = srLine.Substring(1312, 100);

                                rec61.Filler100 = srLine.Substring(1312, 92);

                                //dls 090622 - add possibility of there being a Ciprus load date
                                if (srLine.Length > 1400)
                                {
                                    DateTime d;

                                    string temp = string.Format("{0}-{1}-{2}", srLine.Substring(1404, 4), srLine.Substring(1408, 2), srLine.Substring(1410, 2));

                                    if (DateTime.TryParse(temp, out d))
                                        rec61.paymentDate = temp;
                                    else
                                    {
                                        Logger.Error("ProcessResponseRecord: Ciprus payment date - " + srLine.Substring(1404, 8) + " invalid in file " + file);
                                        failed = true;
                                        return failed;
                                    }

                                    temp = string.Format("{0}-{1}-{2}", srLine.Substring(1412, 4), srLine.Substring(1416, 2), srLine.Substring(1418, 2));

                                    if (DateTime.TryParse(temp, out d))
                                        rec61.ciprusLoadDate = temp;
                                    else
                                    {
                                        Logger.Error("ProcessResponseRecord: Ciprus load date - " + srLine.Substring(1412, 8) + " invalid in file " + file);
                                        failed = true;
                                        return failed;
                                    }
                                }
                            }
                        }
                        else
                        {
                            //the notice has been rejected
                            rec61.recordNumber = srLine.Substring(8, 6);
                            rec61.legislation = string.Empty;
                            rec61.noticeNoType = string.Empty;
                            rec61.noticeNumber = string.Empty;
                            rec61.issuingAuthority = string.Empty;
                            rec61.officeNatisNo = string.Empty;
                            rec61.officerName = string.Empty;
                            rec61.officerInit = string.Empty;
                            rec61.penaltyAmount = "0";
                            rec61.discountAmount = "0";
                            rec61.discountedAmount = "0";
                            rec61.chargeType = string.Empty;
                            rec61.demeritPoints = "0";
                            //mrs 20090205 added
                            rec61.uniqueDocID = string.Empty;

                            rec61.tranData = srLine.Substring(164, 50).Trim();
                            //mrs 20090205 1.07 length change
                            //rec61.infringementRecord = srLine.Substring(208, 900);
                            //rec61.infringementRecord = srLine.Substring(226, 973); //973);
                            rec61.infringementRecord = srLine.Substring(226, 1086);

                            rec61.ciprusLoadDate = string.Empty;
                            rec61.paymentDate = string.Empty;
                        }
                    }
                    else
                        notIntNo = 0;
                }
                else
                    notIntNo = 0;
            }
            catch
            {
                Logger.Error(ResourceHelper.GetResource("ResponseFile_ProcessError", file, " - Record no: " + srLine.Substring(8, 6)));
                failed = true;
                return failed;
            }

            if (notIntNo > 0)
            {
                // update the database
                if (severityCode > 1) // ciprus found an error
                    countFailed++;
                else if (notIntNo > 0)      // Don't want to add the 'File balances OK count!
                    countOK++;

                // get the description
                processDescr = responseCodeList.GetDescrByCode(processingCode); // crashed here null reference on 14 list is null

                // update frame
                string errMessage = string.Empty;

                //mrs 20080205 need to add the uniqueDocumentID to the notice table here
                NoticeDB notice = new NoticeDB(connAARTODB);
                //BD need to handle both 61 and 66
                int updNotIntNo = 0;
                if (processingType == 61)
                {
                    if (processingCode == 66)
                    {
                        updNotIntNo = notice.UpdateCiprusCofCTResponse(rec61, processDescr, AARTOBase.LastUser, notIntNo, STATUS_ERRORS, STATUS_RESPONSE_NON_SUMMONS, STATUS_NO_AOG_CC,
                            null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, ref errMessage);
                    }
                    else
                    {

                        if (processingCode == 71)
                        {
                            CiprusCofCT66Record record66 = ReadInfringementRecord(rec61);
                            if (record66 != null && rec61.processCode == "71")
                            {
                                updNotIntNo = notice.UpdateCiprusCofCTResponse(rec61, processDescr, AARTOBase.LastUser, notIntNo, STATUS_ERRORS, STATUS_RESPONSE_LOADED, STATUS_NO_AOG_CC,
                                  record66.typeVehOwner, record66.streetAddrLine1, record66.streetAddrLine2, record66.streetAddrLine3, record66.streetAddrLine4, record66.streetAddrLine5, record66.streetAddrCode,
                                  record66.postAddrLine1, record66.postAddrLine2, record66.postAddrLine3, record66.postAddrLine4, record66.postAddrLine5, record66.postAddrCode,
                                    ref errMessage);
                            }   
                        }
                        else
                        {
                            updNotIntNo = notice.UpdateCiprusCofCTResponse(rec61, processDescr, AARTOBase.LastUser, notIntNo, STATUS_ERRORS, STATUS_RESPONSE_LOADED, STATUS_NO_AOG_CC,
                               null, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, ref errMessage);

                        }
                        // Oscar 20120323 add, process print file
                        if (updNotIntNo > 0 && (processingCode == 71 || processingCode == 14))
                            ProcessPrintFile(autIntNo, updNotIntNo);
                    }
                }
                if (updNotIntNo <= 0)
                {
                    failed = true;

                    switch (updNotIntNo)
                    {
                        case -1:
                            errMessage = ResourceHelper.GetResource("Result_NoticeNotExist");
                            break;

                        case -2:
                            errMessage = ResourceHelper.GetResource("Result_UnableUpdateNotice");
                            break;

                        case -3:
                            errMessage = ResourceHelper.GetResource("Result_UnableUpdateCharge");
                            break;

                        case -4:
                            errMessage = ResourceHelper.GetResource("Result_ErrorChargeStatus");
                            break;
                        case -5:
                            errMessage = ResourceHelper.GetResource("Result_UnableUpdateDriver");
                            break;
                        case -6:
                            errMessage = ResourceHelper.GetResource("Result_UnableUpdateOwner");
                            break;
                        case -7:
                            errMessage = ResourceHelper.GetResource("Result_UnableUpdateProxy");
                            break;

                    }
                    Logger.Error(ResourceHelper.GetResource("ResponseFile_FaildedUpdateNotice", file, notIntNo, errMessage));
                }
                else
                {
                    //QueueItemProcessor queueProcessor = new QueueItemProcessor();
                    //QueueItem pushItem = new QueueItem();
                    //pushItem = new QueueItem();
                    //pushItem.Body = updNotIntNo.ToString();
                    //pushItem.Group = autNo;
                    //pushItem.QueueType = ServiceQueueTypeList.Print1stNotice;
                    //queueProcessor.Send(pushItem);

                    Logger.Info(ResourceHelper.GetResource("ResponseFile_UpdateNoticeSuccessfully", rec61.recordNumber));
                }
            }

            return failed;
        }


        private CiprusCofCT66Record ReadInfringementRecord(CiprusCofCT61Record record)
        {
            CiprusCofCT66Record rec66 = null;
            try
            {
                if (!String.IsNullOrEmpty(record.infringementRecord) && record.infringementRecord.Length == 1086)
                {
                    rec66 = new CiprusCofCT66Record();
                    string strLine = record.infringementRecord;

                    rec66.recType = strLine.Substring(0, 2);
                    //rec66.interfaceType = strLine.Substring(2, 1);
                    //rec66.versionNo = strLine.Substring(3, 2);
                    //rec66.supplierCode = strLine.Substring(5, 1);
                    //rec66.cameraID = strLine.Substring(6, 3);
                    //rec66.autNo = strLine.Substring(9, 3);
                    //rec66.Filler1 = strLine.Substring(12, 7);
                    //rec66.filmNo = strLine.Substring(19, 8);
                    //rec66.refNo = strLine.Substring(27, 9);
                    //rec66.firstSpeed = strLine.Substring(36, 4);
                    //rec66.secondSpeed = strLine.Substring(40, 4);
                    //rec66.officerNo = strLine.Substring(44, 9);
                    //rec66.cameraLoc = strLine.Substring(53, 6);
                    //rec66.courtNo = strLine.Substring(59, 6);
                    //rec66.offenceDate = strLine.Substring(65, 8);
                    //rec66.Filler2 = strLine.Substring(73, 4);
                    //rec66.Filler3 = strLine.Substring(77, 2);
                    //rec66.Filler4 = strLine.Substring(79, 2);
                    //rec66.regNo = strLine.Substring(81, 10);
                    //rec66.travelDirection = strLine.Substring(91, 1);
                    //rec66.offenceCode = strLine.Substring(92, 6);


                    rec66.typeVehOwner = strLine.Substring(98, 1);

                    //rec66.idNo = strLine.Substring(99, 13);
                    //rec66.surname = strLine.Substring(112, 30);
                    //rec66.forenames = strLine.Substring(142, 40);
                    //rec66.initials = strLine.Substring(182, 5);
                    //rec66.businessIDNo = strLine.Substring(187, 13);
                    //rec66.businessName = strLine.Substring(200, 40);
                    //rec66.Filler5 = strLine.Substring(240, 30);
                    //rec66.gender = strLine.Substring(270, 2);
                    //rec66.organisationType = strLine.Substring(272, 30);
                    //rec66.dateOfBirth = strLine.Substring(302, 8);
                    rec66.streetAddrLine1 = strLine.Substring(310, 40);
                    rec66.streetAddrLine2 = strLine.Substring(350, 40);
                    rec66.streetAddrLine3 = strLine.Substring(390, 40);
                    rec66.streetAddrLine4 = strLine.Substring(430, 40);
                    rec66.streetAddrLine5 = strLine.Substring(470, 40);
                    rec66.streetAddrCode = strLine.Substring(510, 4);
                    rec66.postAddrLine1 = strLine.Substring(514, 40);
                    rec66.postAddrLine2 = strLine.Substring(554, 40);
                    rec66.postAddrLine3 = strLine.Substring(594, 40);
                    rec66.postAddrLine4 = strLine.Substring(634, 40);
                    rec66.postAddrLine5 = strLine.Substring(674, 40);
                    rec66.postAddrCode = strLine.Substring(714, 4);
                    //rec66.telHome = strLine.Substring(718, 12);
                    //rec66.telWork = strLine.Substring(730, 12);
                    //rec66.fax = strLine.Substring(742, 12);
                    //rec66.cell = strLine.Substring(754, 12);
                    //rec66.email = strLine.Substring(766, 30);
                    //rec66.natisVehMCode = strLine.Substring(796, 3);
                    //rec66.natisVehDescrCode = strLine.Substring(799, 2);
                    //rec66.idType = strLine.Substring(801, 2);
                    //rec66.countryOfIssue = strLine.Substring(803, 3);
                    //rec66.licenceDiscNo = strLine.Substring(806, 20);
                    //rec66.prDPCode = strLine.Substring(826, 20);
                    //rec66.driverLicenceCode = strLine.Substring(846, 3);
                    //rec66.operatorCardNo = strLine.Substring(849, 20);
                    //rec66.vehicleGVM = strLine.Substring(869, 8);
                    //rec66.offenceTime = strLine.Substring(877, 6);
                    //rec66.amberTime = strLine.Substring(883, 6);
                    //rec66.redTime = strLine.Substring(889, 6);
                    //rec66.dateCaptured = strLine.Substring(895, 8);
                    //rec66.dateVerified = strLine.Substring(903, 8);
                    //rec66.dateNatisReceived = strLine.Substring(911, 8);
                    //rec66.dateAdjudicated = strLine.Substring(919, 8);
                    //rec66.adjOfficerNo = strLine.Substring(927, 9);
                    //rec66.tranData = strLine.Substring(936, 50);
                    //rec66.branchCode = strLine.Substring(986, 2);
                    //rec66.taxiIndicatorFlag = strLine.Substring(988, 1);
                    //rec66.Filler6 = strLine.Substring(989, 97);

                }
                else
                {
                    Logger.Error(ResourceHelper.GetResource("ProcessInfringement_FailedRecordIsNull") + (record.infringementRecord == null ? "" : record.infringementRecord).Length);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ResourceHelper.GetResource("ProcessInfringement_Failed") + ex.Message);
            }
            return rec66;
        }


        private void InitialBatch()
        {
            this.pfnList.Clear();
            this.pf1stNotice.Reset();
        }

        private void ProcessPrintFile(int autIntNo, int notIntNo)
        {
            string msg;
            string printFileName;

            if (!NoticeChargeIssue_CC_WS(autIntNo, notIntNo, this.pf1stNotice, out printFileName, out msg))
            {
                AARTOBase.ErrorProcessing(msg, true);
                return;
            }

            int pfnIntNo = AARTOBase.SavePrintFileName(PrintFileNameType.Notice, printFileName, autIntNo, notIntNo, out msg);

            if (pfnIntNo <= 0)
            {
                AARTOBase.ErrorProcessing(msg, true);
                return;
            }

            if (!this.pfnList.ContainsKey(pfnIntNo))
            {
                this.pfnList.Add(pfnIntNo, 1);

                //Oscar 20120412 changed group
                int frameIntNo = new NoticeDB(this.connAARTODB).GetFrameForNotice(notIntNo);
                string violationType = new FrameDB(this.connAARTODB).GetViolationType(frameIntNo);

                QueueItemProcessor processor = new QueueItemProcessor();
                QueueItem item = new QueueItem()
                {
                    Body = pfnIntNo,
                    Group = string.Format("{0}|{1}", this.autCode.Trim(), violationType),
                    ActDate = this.printActionDate,
                    QueueType = ServiceQueueTypeList.Print1stNotice
                };
                processor.Send(item);
            }
            else if (this.pfnList[pfnIntNo]++ >= this.batchSize)
            {
                this.pfnList.Remove(pfnIntNo);

                int index = printFileName.IndexOf("-");
                if (index >= 0)
                {
                    string type = printFileName.Substring(0, index);
                    this.pf1stNotice.GetNew(type);
                }
            }
        }

        private bool NoticeChargeIssue_CC_WS(int autIntNo, int notIntNo, PrintFileNameSetting pfModel, out string printFileName, out string msg)
        {
            printFileName = string.Empty;
            msg = string.Empty;
            SqlParameter[] paras = new SqlParameter[9];
            paras[0] = new SqlParameter("@AutIntNo", autIntNo);
            paras[1] = new SqlParameter("@NotIntNo", notIntNo);
            paras[2] = new SqlParameter("@PrintFileName", printFileName) { Direction = ParameterDirection.InputOutput, Size = 100 };
            paras[3] = new SqlParameter("@LastUser", AARTOBase.LastUser);
            paras[4] = new SqlParameter("@SPDDate", pfModel.SPDDate);
            paras[5] = new SqlParameter("@RLVDate", pfModel.RLVDate);
            paras[6] = new SqlParameter("@NAGDate", pfModel.NAGDate);
            paras[7] = new SqlParameter("@ASDDate", pfModel.ASDDate);
            paras[8] = new SqlParameter("@ASD_NAGDate", pfModel.ASD_NAGDate);
            object obj = new DBHelper(this.connAARTODB).ExecuteScalar("NoticeChargeIssue_CC_WS", paras, out msg);
            if (obj != null && ServiceUtility.IsNumeric(obj) && Convert.ToInt32(obj) > 0)
            {
                if (paras[2].Value != null)
                {
                    printFileName = paras[2].Value.ToString();
                    return true;
                }
            }
            return false;
        }

        DateTime printActionDate = DateTime.Now;
        private void SetServiceParameters()
        {
            int delayHours = 0;
            if (ServiceParameters != null
                && ServiceParameters.ContainsKey("DelayActionDate_InHours")
                && !string.IsNullOrEmpty(ServiceParameters["DelayActionDate_InHours"]))
                delayHours = Convert.ToInt32(ServiceParameters["DelayActionDate_InHours"]);
            this.printActionDate = DateTime.Now.AddHours(delayHours);

            if (ServiceParameters != null
                && ServiceParameters.ContainsKey("InternalBatchSize")
                && !string.IsNullOrEmpty(ServiceParameters["InternalBatchSize"]))
                this.batchSize = Convert.ToInt32(ServiceParameters["InternalBatchSize"]);
        }

    }
}
