﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.CIP_CofCT_ResponseFileRead
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.CIP_CofCT_ResponseFileRead"
                ,
                DisplayName = "SIL.AARTOService.CIP_CofCT_ResponseFileRead"
                ,
                Description = "SIL AARTOService CIP_CofCT_ResponseFileRead"
            };

            ProgramRun.InitializeService(new CIP_CofCT_ResponseFileRead(), serviceDescriptor, args);
        }
    }
}
