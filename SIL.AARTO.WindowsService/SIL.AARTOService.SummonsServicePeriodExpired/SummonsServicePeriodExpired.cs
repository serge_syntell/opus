﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.SummonsServicePeriodExpired
{
    partial class SummonsServicePeriodExpired : ServiceHost
    {
        public SummonsServicePeriodExpired()
            : base("", new Guid("5020082C-F18C-4AC9-A23A-97010F23FFE2"), new SummonsServicePeriodExpiredService())
        {
            InitializeComponent();
        }
    }
}
