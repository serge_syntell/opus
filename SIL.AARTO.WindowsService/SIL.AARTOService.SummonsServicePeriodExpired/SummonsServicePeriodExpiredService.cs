﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using SIL.AARTOService.Resource;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using Stalberg.TMS;

namespace SIL.AARTOService.SummonsServicePeriodExpired
{
    public class SummonsServicePeriodExpiredService : ServiceDataProcessViaQueue
    {
        public SummonsServicePeriodExpiredService()
            : base("", "", new Guid("5D2ED844-3F0A-4807-8A94-4C80CEFD7A32"), ServiceQueueTypeList.SummonsServicePeriodExpired)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
        }

        private const string TICKET_PROCESSOR_AARTO = "AARTO";
        private const string TICKET_PROCESSOR_JMPD_AARTO = "JMPD_AARTO";

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        AuthorityDetails authDetails = null;
        string connectStr = string.Empty;
        string strAutCode = string.Empty;
        string autName = string.Empty;
        int autIntNo;

        int noDaysForNoticeExpiry;
        int noDaysForNoticeExpiryBeforeSummons;

        public sealed override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && ServiceUtility.IsNumeric(body))
            {
                item.IsSuccessful = true;
            }
            else
            {
                // jerry 2012-02-21 change
                //item.IsSuccessful = false;
                //item.Status = QueueItemStatus.UnKnown;
                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.lastUser, item.Body));
                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrorBodyOfQueueIsNotNum"), AARTOBase.LastUser, item.Body), true);
            }

            if (strAutCode != item.Group)
            {
                // Oscar 20121008 Move this to line 69
                //strAutCode = item.Group.Trim();
                //strAutCode = item.Group;

                // Oscar 20121122 changed for group checking
                var group = item.Group.Trim();

                //jerry 2012-04-05 check group value
                //if (string.IsNullOrWhiteSpace(strAutCode))

                // Oscar 20121122 changed for group checking
                if (string.IsNullOrWhiteSpace(group))
                {
                    //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", group));
                    return;
                }

                // Oscar 20121122 changed for group checking
                //strAutCode = item.Group.Trim();
                group = item.Group.Trim();

                //this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode);
                this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == group);

                // Oscar 20121008 add
                if (this.authDetails == null)
                {
                    //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", group));
                    return;
                }

                autIntNo = authDetails.AutIntNo;
                autName = authDetails.AutName;

                //Jerry 2012-12-28 changed
                int notIntNo = Convert.ToInt32(item.Body);
                SIL.AARTO.DAL.Entities.Notice noticeEntity = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(notIntNo);
                //if (this.authDetails.AutTicketProcessor == TICKET_PROCESSOR_AARTO ||
                //    this.authDetails.AutTicketProcessor == TICKET_PROCESSOR_JMPD_AARTO)
                if (noticeEntity != null && noticeEntity.NotTicketProcessor != "TMS")
                {
                    // jake 2013-07-12 add message when discard queue 
                    item.Status = QueueItemStatus.Discard;
                    item.IsSuccessful = false;
                    this.Logger.Info(String.Format("Invalid notice ticket processor {0}", noticeEntity == null ? "" : noticeEntity.NotTicketProcessor), item.Body.ToString());
                }

                // Oscar 20121122 changed for group checking
                this.strAutCode = group;

                rules.InitParameter(connectStr, strAutCode);
            }
        }

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            int notIntNo, success;
            string msg = string.Empty;

            // jerry 2012-02-21 change
            //foreach (QueueItem item in queueList)
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                try
                {
                    if (item.IsSuccessful)
                    {
                        notIntNo = Convert.ToInt32(item.Body);

                        //get notice expiry date
                        noDaysForNoticeExpiry = rules.Rule("NotOffenceDate", "NotExpireDate").DtRNoOfDays;

                        //get no longer able to summons
                        noDaysForNoticeExpiryBeforeSummons = rules.Rule("NotOffenceDate", "NotSummonsBeforeDate").DtRNoOfDays;

                        success = UpdateExpiredNoticesBeforeSummons(notIntNo, ref msg);
                        switch (success)
                        {
                            case -1:
                                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrMsgUpdateChargeInSummonsServicePeriodExpired"), notIntNo));
                                //item.Status = QueueItemStatus.UnKnown;
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrMsgUpdateChargeInSummonsServicePeriodExpired"), notIntNo));
                                break;
                            case -2:
                                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrMsgUpdateNoticeInSummonsServicePeriodExpired"), notIntNo));
                                //item.Status = QueueItemStatus.UnKnown;
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrMsgUpdateNoticeInSummonsServicePeriodExpired"), notIntNo));
                                break;
                            case -3:
                                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrMsgUpdateEvidenceInSummonsServicePeriodExpired"), notIntNo));
                                //item.Status = QueueItemStatus.UnKnown;
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrMsgUpdateEvidenceInSummonsServicePeriodExpired"), notIntNo));
                                break;
                            case -4:
                                //this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrMsgExeSPInSummonsServicePeriodExpired"), msg, notIntNo));
                                //item.Status = QueueItemStatus.UnKnown;
                                AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ErrMsgExeSPInSummonsServicePeriodExpired"), msg, notIntNo), true);
                                break;
                            case 0:
                                item.Status = QueueItemStatus.Discard;
                                this.Logger.Info("Update Expired Notices Before Summons falid and function returned 0; Queue key:" + item.Body.ToString());
                                break;
                            case 1:
                                this.Logger.Info(string.Format(ResourceHelper.GetResource("SuccessfulMsgInSummonsServicePeriodExpired"), notIntNo));
                                item.Status = QueueItemStatus.Discard;

                                //push queue about CancelExpiredNotice
                                QueueItemProcessor queueProcessor = new QueueItemProcessor();
                                QueueItem pushItem;
                                NoticeDB noticeDB = new NoticeDB(connectStr);
                                NoticeDetails notice = noticeDB.GetNoticeDetails(notIntNo);
                                pushItem = new QueueItem();
                                pushItem.Body = notIntNo.ToString();
                                pushItem.Group = strAutCode;
                                pushItem.ActDate = notice.NotOffenceDate.AddDays(noDaysForNoticeExpiry);
                                pushItem.QueueType = ServiceQueueTypeList.CancelExpiredNotice;
                                queueProcessor.Send(pushItem);
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //item.Status = QueueItemStatus.UnKnown;
                    //this.Logger.Error(ex);
                    //throw ex;
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        private int UpdateExpiredNoticesBeforeSummons(int notIntNo, ref string errMsg)
        {
            DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));

            SqlParameter[] paras = new SqlParameter[3];
            paras[0] = new SqlParameter("@NotIntNo", notIntNo);
            paras[1] = new SqlParameter("@NoDaysForNoticeExpiryBeforeSummons", noDaysForNoticeExpiryBeforeSummons);
            paras[2] = new SqlParameter("@LastUser", AARTOBase.LastUser);

            object obj = db.ExecuteScalar("NoticeExpiredBeforeSummons_WS", paras, out errMsg);
            int result;
            if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(errMsg))
                result = Convert.ToInt32(obj);
            else
                result = -4;

            return result;
        }
    }
}
