﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.AARTOService.DAL;
using SIL.QueueLibrary;
using SIL.AARTOService.Library;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Collections;
using Stalberg.TMS;
using SIL.AARTOService.Resource;

namespace SIL.AARTOService.GenerateWOA
{
    public class GenerateWOAService : ServiceDataProcessViaQueue
    {
        public GenerateWOAService()
            : base("", "", new Guid("1B7054E9-BEA2-487E-8E54-BAA173461A46"), ServiceQueueTypeList.GenerateWOA)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            //this.AARTOBase.OnServiceSleeping = OnServiceSleeping;

            // Oscar 2013-04-09 added
            AARTOBase.OnServiceStarting = () =>
            {
                this.lastStamp = DateTime.Now;
                //2013-12-26 Heidi added for WOA Direct Printing (5101)
                SetServiceParameters();
                this.pfnIntNoList.Clear();

            };
        }

        int sumIntNo;
        int autIntNo;
        string connectStr;
        string strAutCode = null;
        string strCourtNo = null;
        string batchFilename;
        DataSet dsSummonsDetails = new DataSet();
        private const string TICKET_PROCESSOR_TMS = "TMS";
        private const string TICKET_PROCESSOR_AARTO = "AARTO";
        private const string TICKET_PROCESSOR_JMPD_AARTO = "JMPD_AARTO";
        //const string DATE_AND_TIME_SHORT_FORMAT = "yyyy-MM-dd_HH-mm";
        // Oscar 2013-04-09 modified
        const string DATE_AND_TIME_SHORT_FORMAT = "yyyy-MM-dd_HH-mm-ss";
        DateTime stamp, lastStamp;

        const string DATE_FORMAT = "yyyy-MM-dd";
        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        //2013-12-26 Heidi added for WOA Direct Printing (5101)
        bool isIbmPrinter;

        // jerry 2012-03-13 change, in order to push Batch queue
        List<Int32> pfnIntNoList = new List<Int32>();//2013-12-26 Heidi changed for WOA Direct Printing (5101)
        int delayActionDate_InHours;//2013-12-26 Heidi changed for WOA Direct Printing (5101)

        public override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            string strGroup = null;
            string[] strArrGroup = null;
            //string autTicketProcessor = null;

            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && ServiceUtility.IsNumeric(body) && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    strGroup = item.Group.Trim();
                    strArrGroup = strGroup.Split('~');
                    if (strArrGroup.Length != 3 || string.IsNullOrEmpty(strArrGroup[0]) || string.IsNullOrEmpty(strArrGroup[1]) || string.IsNullOrEmpty(strArrGroup[2]))
                    {
                        AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("CreatingWOA") + ": " + ResourceHelper.GetResource("WOAQueueGroupInvalid", body));
                        return;
                    }
                    strAutCode = strArrGroup[0].Trim();
                    strCourtNo = strArrGroup[1].Trim();
                    rules.InitParameter(connectStr, strAutCode);

                    // WOA numbers can be generated automatically(Y) or manually(N)
                    CourtDB crt = new CourtDB(connectStr);
                    int crtIntNo = crt.GetCourtIntNoByCourtNo(strCourtNo);
                    string isAuto = rules.Rule(crtIntNo, "5010").CRString;

                    if (isAuto == "N")
                    {
                        Logger.Warning("Queue Key: " + body + " | " + ResourceHelper.GetResource("CreatingWOA") + ": " + ResourceHelper.GetResource("ManualWOAForCourt"));
                        item.Status = QueueItemStatus.Discard;
                        return;
                    }

                    //autTicketProcessor = (AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode).AutTicketProcessor + "").Trim();

                    //modified by linda 2012-12-21(replace AutTicketProcessor by NotTicketProcessor)
                    int sumIntNo = Convert.ToInt32(item.Body);
                    SIL.AARTO.DAL.Entities.NoticeSummons noticeSummons = new SIL.AARTO.DAL.Services.NoticeSummonsService().GetBySumIntNo(sumIntNo).FirstOrDefault();
                    SIL.AARTO.DAL.Entities.Notice notice = new SIL.AARTO.DAL.Entities.Notice();
                    if (noticeSummons!=null)
                    {
                        notice=new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(noticeSummons.NotIntNo);
                    }                    

                    if (notice.NotTicketProcessor != TICKET_PROCESSOR_TMS)
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        return;
                    }

                    item.IsSuccessful = true;
                    AARTOBase.BatchCount--;
                    //jerry 2012-03-13 add in order to get delay hour
                    //GetServiceParameters();
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
            }
        }

        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            string strGroup = null;
            string[] strArrGroup = null;
            string errMsg1 = "";
            string errMsg2 = "";

            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                try
                {
                    if (item.IsSuccessful)
                    {
                        sumIntNo = Convert.ToInt32(item.Body);
                        int returnValue = GetSummonsDetailsForWOA(out errMsg1);
                        // dls 2012-08-21 - an error message here should not discard the Q ==> need to separate result & error from an empty recordset
                        //if (!string.IsNullOrEmpty(errMsg1) || dsSummonsDetails == null || dsSummonsDetails.Tables.Count == 0 || dsSummonsDetails.Tables[0].Rows.Count == 0)
                        if (!string.IsNullOrEmpty(errMsg1))
                        {
                            AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("CreatingWOA") + ": " + errMsg1 , false, QueueItemStatus.UnKnown);
                            continue;
                        }
                        else if (dsSummonsDetails == null || dsSummonsDetails.Tables.Count == 0 || dsSummonsDetails.Tables[0].Rows.Count == 0)
                        {
                            if (returnValue == -1)
                            {
                                errMsg1 = ResourceHelper.GetResource("GetSummonsDetailError1ForGenerateWOA");

                            }
                            else if (returnValue == -2)
                            {
                                errMsg1 = ResourceHelper.GetResource("GetSummonsDetailError2ForGenerateWOA");
                            }
                            else if (returnValue == -3)
                            {
                                errMsg1 = ResourceHelper.GetResource("GetSummonsDetailError3ForGenerateWOA");
                            }
                            else if (returnValue == -4)
                            {
                                errMsg1 = ResourceHelper.GetResource("GetSummonsDetailError4ForGenerateWOA");
                            }
                            //else
                            //{
                            //    errMsg1 = ResourceHelper.GetResource("ErrToGetSumDetForWOA");
                            //}

                            errMsg1 = ResourceHelper.GetResource("CreatingWOA") + ": " + ResourceHelper.GetResource("ErrToGetSumDetForWOA") + ", " + errMsg1;
                            //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("CreatingWOA") + ": " + ResourceHelper.GetResource("ErrToGetSumDetForWOA"));
                            //remove the item from the Q if the status is wrong
                            AARTOBase.LogProcessing(errMsg1, LogType.Info, ServiceOption.Continue, item, QueueItemStatus.Discard);
                            continue;
                        }

                        DataRow dr = dsSummonsDetails.Tables[0].Rows[0];
                        string crtPrefix = DBHelper.GetDataRowValue<string>(dr, "CrtPrefix").Trim();
                        DateTime crtDate = DBHelper.GetDataRowValue<DateTime>(dr, "SumCourtDate");

                        //moved out of SP into code
                        int noOfDays = rules.Rule("SumCourtDate", "WOALoadDate").DtRNoOfDays;

                        if (crtDate.AddDays(noOfDays) > DateTime.Today)
                        {
                            //this will leave the item on the Q for later processing
                            AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("CreatingWOA") + ": " + ResourceHelper.GetResource("ErrToGetSumDetForWOAYet"));
                            continue;
                        }

                        if (strGroup != item.Group.Trim())
                        {
                            strGroup = item.Group.Trim();
                            strArrGroup = strGroup.Split('~');
                            strAutCode = strArrGroup[0].Trim();
                            strCourtNo = strArrGroup[1].Trim();
                            //DataRow dr = dsSummonsDetails.Tables[0].Rows[0];
                            //string crtPrefix = DBHelper.GetDataRowValue<string>(dr, "CrtPrefix").Trim();
                            //DateTime crtDate = DBHelper.GetDataRowValue<DateTime>(dr, "SumCourtDate");

                            ////moved out of SP into code
                            //int noOfDays = rules.Rule("SumCourtDate", "WOALoadDate").DtRNoOfDays;

                            //if (crtDate.AddDays(noOfDays) > DateTime.Today)
                            //{
                            //    //this will leave the item on the Q for later processing
                            //    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("CreatingWOA") + ": " + ResourceHelper.GetResource("ErrToGetSumDetForWOAYet"));
                            //}

                            // 2012-03-05 nick changed naming rule for batch of WOA
                            //batchFilename = string.Format("WOA-{0}-{1}-{2}-CourtDate-{3}", strAutCode, DateTime.Now.ToString(DATE_AND_TIME_SHORT_FORMAT), crtPrefix, crtDate.ToString(DATE_FORMAT));

                            // Oscar 2013-04-09 changed
                            batchFilename = CreatePrintFileName(crtPrefix, crtDate);
                        }

                        autIntNo = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode).AutIntNo;

                        int woaIntNo = AddWOA(out errMsg2);
                        if (woaIntNo <= 0)
                        {
                            //Jerry 2013-11-07 add errMsg
                            string errMsg = string.Empty;

                            switch (woaIntNo)
                            {
                                case 0:
                                    errMsg = "UnableToCreateWOA";
                                    break;
                                case -1:
                                    errMsg = "FailedToGenerateNextWOANumForCrtRoom";
                                    break;
                                case -2:
                                    errMsg = "FailedToInsertWOARecord";
                                    break;
                                case -3:
                                    errMsg = "UnableToUpdateSumChargeRow";
                                    break;
                                case -4:
                                    errMsg = "UnableToUpdateSumWithPrevStat";
                                    break;
                                case -5:
                                    errMsg = "UnableToUpdateSumRow";
                                    break;
                                //Jerry 2013-11-07 add -51
                                case -51:
                                    errMsg = "UnableToUpdateNoticeStatus";
                                    break;
                                case -6:
                                    errMsg = "UnableToUpdateChargeRow";
                                    break;
                                //Jerry 2013-11-07 add -61
                                case -61:
                                    errMsg = "UnableToUpdateSumChargeStatus";
                                    break;
                                case -7:
                                    errMsg = "UnableToInsertEvidencePackRow";
                                    break;
                                default:
                                    errMsg = "UnableToCreateWOA";
                                    break;
                            }

                            //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("CreatingWOA") + ": " + ResourceHelper.GetResource(errMsg2));
                            //continue;
                            AARTOBase.LogProcessing(ResourceHelper.GetResource("CreatingWOA") + ": " + string.Format(ResourceHelper.GetResource(errMsg), sumIntNo, woaIntNo, errMsg2), 
                                LogType.Error, ServiceOption.Break, item);
                            break;
                        }
                        else
                        {
                            item.Status = QueueItemStatus.Discard;

                            //push queue about CancelExpiredWOA
                            QueueItemProcessor queueProcessor = new QueueItemProcessor();
                            QueueItem pushItem;
                            pushItem = new QueueItem();
                            pushItem.Body = woaIntNo.ToString();
                            pushItem.Group = strAutCode;

                            //Jake 2015-02-28 modified ,use Court date add days (Data rule)
                            // Jake 2013-10-18 modified , use new data rule DR_SumCourtDate_WOAExpireDate
                            //pushItem.ActDate = DateTime.Now.AddDays(rules.Rule("SumCourtDate", "WOAExpireDate").DtRNoOfDays);
                            pushItem.ActDate = crtDate.AddDays(rules.Rule("SumCourtDate", "WOAExpireDate").DtRNoOfDays);
                            
                            pushItem.QueueType = ServiceQueueTypeList.CancelExpiredWOA;
                            queueProcessor.Send(pushItem);

                            // jerry 2012-03-02 push Print WOA service
                            string errMsg;
                            int pfnIntNo = AARTOBase.SavePrintFileName(PrintFileNameType.WOA, batchFilename, autIntNo, woaIntNo, out errMsg);
                            if (pfnIntNo <= 0)
                            {
                                AARTOBase.ErrorProcessing(errMsg, true);
                            }
                            else
                            {
                                //2014-01-10 Heidi added for According to the requirements (5101)
                                isIbmPrinter = rules.Rule("6209").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase);
                                if (!isIbmPrinter)
                                {
                                    if (!this.pfnIntNoList.Contains(pfnIntNo))
                                    {
                                        this.pfnIntNoList.Add(pfnIntNo);

                                        pushItem = new QueueItem();
                                        pushItem.Body = pfnIntNo.ToString();
                                        pushItem.Group = strAutCode;
                                        pushItem.ActDate = DateTime.Now.AddHours(this.delayActionDate_InHours);
                                        pushItem.QueueType = ServiceQueueTypeList.PrintWOA;
                                        pushItem.LastUser = AARTOBase.LastUser;
                                        queueProcessor.Send(pushItem);
                                    }

                                }
                                else
                                {
                                    //2013-12-26 Heidi added for WOA Direct Printing (5101)
                                    if (!this.pfnIntNoList.Contains(pfnIntNo))
                                    {
                                        this.pfnIntNoList.Add(pfnIntNo);

                                        QueueItem printToFileItem = new QueueItem()
                                        {
                                            Body = pfnIntNo,
                                            Group = strAutCode + "|" + (int)SIL.AARTO.DAL.Entities.ReportConfigCodeList.WOADirectPrinting,
                                            ActDate = DateTime.Now.AddHours(this.delayActionDate_InHours),
                                            QueueType = ServiceQueueTypeList.PrintToFile,
                                            LastUser = AARTOBase.LastUser
                                        };
                                        queueProcessor.Send(printToFileItem);
                                    }
                                }
                                
                            }
                            //jerry 2012-03-14 change that, can't push print woa service in here, 
                            //because the WOAAuthorised = 'N', push in PrintWOA page when you click
                            //authorize button
                            //else
                            //{
                            //    pushItem = new QueueItem();
                            //    pushItem.Body = pfnIntNo.ToString();
                            //    pushItem.Group = strAutCode;
                            //    pushItem.ActDate = DateTime.Now.AddHours(this.delayActionDate_InHours);
                            //    pushItem.QueueType = ServiceQueueTypeList.PrintWOA;

                            //    // jerry 2012-03-08 change
                            //    if (!this.pfnIntNoList.Contains(pfnIntNo))
                            //    {
                            //        this.pfnIntNoList.Add(pfnIntNo);
                            //        queueProcessor.Send(pushItem);
                            //    }
                            //}
                        }
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }

            }
        }

        //private void OnServiceSleeping()
        //{
        //    if (this.pfnIntNoList.Count > 0)
        //    {
        //        this.pfnIntNoList.Clear();
        //    }
        //}

        private int GetSummonsDetailsForWOA(out string errMsg)
        {
            //dls 2012-07-05 - moved date check out of SP into code
            // Get the date rule for the Authority that no. days from Court date to WOA load date
            //int noOfDays = rules.Rule("SumCourtDate", "WOALoadDate").DtRNoOfDays;

            DBHelper db = new DBHelper(connectStr);
            SqlParameter[] paras = new SqlParameter[2];
            paras[0] = new SqlParameter("@SumIntNo", sumIntNo);
            paras[1] = new SqlParameter("@ReturnValue", SqlDbType.Int);
            paras[1].Direction = ParameterDirection.Output;
            //paras[1] = new SqlParameter("@NoOfDays", noOfDays);

            dsSummonsDetails = db.ExecuteDataSet("GetSummonsDetailsForWOA_WS", paras, out errMsg);

            if (paras[1].Value != null)
            {
                return Convert.ToInt32(paras[1].Value);
            }

            return 0;
        }

        private int AddWOA(out string errMsg)
        {
            //Rule that indicates if Notice of WOA (letter) should be generated
            string autIssueWOANotice = rules.Rule("5801").ARString;

            //Jake 2013-04-24 add authoriry for WOA number format rule
            string autWoaNumberFormat = rules.Rule("9510").ARString;

            DataRow dr = dsSummonsDetails.Tables[0].Rows[0];
            string crtPrefix = DBHelper.GetDataRowValue<string>(dr, "CrtPrefix");
            if (!String.IsNullOrEmpty(crtPrefix)) crtPrefix = crtPrefix.Trim();
            string crtRPrefix = DBHelper.GetDataRowValue<string>(dr, "CrtRPrefix");
            string crtRoomNo = DBHelper.GetDataRowValue<string>(dr, "CrtRoomNo");
            string woaNumberPrefix = crtRPrefix + "/V/" + crtRoomNo + "/";
            if (autWoaNumberFormat == "Y")
            {
                woaNumberPrefix = crtPrefix + "/W/" + crtRoomNo + "/";
            }
            //string woaNumberSuffix = "/" + DateTime.Now.Year.ToString().Substring(2, 2);
            // 2014-06-16, Oscar changed
            var courtDateObj = dr["SumCourtDate"];
            var courtDate = courtDateObj != DBNull.Value && courtDateObj != null ? Convert.ToDateTime(courtDateObj).Year : DateTime.Now.Year;
            string woaNumberSuffix = "/" + courtDate.ToString().Substring(2, 2);

            decimal sChSentenceAmount = DBHelper.GetDataRowValue<decimal>(dr, "SChSentenceAmount");
            decimal sChTotalAddAmount = DBHelper.GetDataRowValue<decimal>(dr, "SChTotalAddAmount");

            DBHelper db = new DBHelper(connectStr);
            SqlParameter[] paras = new SqlParameter[11];
            paras[0] = new SqlParameter("@SumIntNo", sumIntNo);
            paras[1] = new SqlParameter("@AutIntNo", autIntNo);
            paras[2] = new SqlParameter("@WOANumberPrefix", woaNumberPrefix);
            paras[3] = new SqlParameter("@WOANumberSuffix", woaNumberSuffix);
            paras[4] = new SqlParameter("@WOAPrintFileName", batchFilename);
            paras[5] = new SqlParameter("@WOAFineAmount", sChSentenceAmount);
            paras[6] = new SqlParameter("@WOAAddAmount", sChTotalAddAmount);
            paras[7] = new SqlParameter("@LastUser", AARTOBase.LastUser);
            paras[8] = new SqlParameter("@IssueWOANotice", autIssueWOANotice);
            paras[9] = new SqlParameter("@WOAIntNo", 0);
            paras[10] = new SqlParameter("@DisplayRepresentative", rules.Rule("5065").ARString);
            //paras[9].Direction = ParameterDirection.Output;
            //Oscar 20121010 changed
            paras[9].Direction = ParameterDirection.InputOutput;

            db.ExecuteNonQuery("WOAAdd_WS", paras, out errMsg);
            int result = Convert.ToInt32(paras[9].Value);
            return result;
        }

        //void StopService()
        //{
        //    this.MustStop = true;
        //    ((SIL.ServiceLibrary.ServiceHost)this.ServiceHost).Stop();
        //}

        // Oscar 2013-04-09 added
        string CreatePrintFileName(string crtPrefix, DateTime crtDate)
        {
            this.stamp = DateTime.Now;
            if (this.stamp < this.lastStamp.AddSeconds(1))
                this.stamp = this.lastStamp.AddSeconds(1);
            this.lastStamp = this.stamp;
            crtPrefix = Helper.ReplaceSpecialCharacters(crtPrefix);//2014-01-16 Heidi added for use "/" not create file(5101)
            return string.Format("WOA-{0}-{1}-{2}-CourtDate-{3}", strAutCode, this.stamp.ToString(DATE_AND_TIME_SHORT_FORMAT), crtPrefix, crtDate.ToString(DATE_FORMAT));
        }

        /// <summary>
        /// jerry 2012-03-13 add this method in order to delay the print service action date time
        /// </summary>
        //private void GetServiceParameters()
        //{
        //    if (ServiceParameters != null)
        //    {
        //        if (this.delayActionDate_InHours == 0 && ServiceParameters.ContainsKey("DelayActionDate_InHours") && !string.IsNullOrEmpty(ServiceParameters["DelayActionDate_InHours"]))
        //        {
        //            this.delayActionDate_InHours = Convert.ToInt32(ServiceParameters["DelayActionDate_InHours"]);
        //        }
        //    }
        //}
        //2013-12-26 Heidi added for WOA Direct Printing (5101)
        private void SetServiceParameters()
        {
            int delayHours = 0;
            if (ServiceParameters != null
                && ServiceParameters.ContainsKey("DelayActionDate_InHours")
                && !string.IsNullOrEmpty(ServiceParameters["DelayActionDate_InHours"]))
            {
                delayHours = Convert.ToInt32(ServiceParameters["DelayActionDate_InHours"]);
                this.delayActionDate_InHours = delayHours;
            }

        }
    }
}
