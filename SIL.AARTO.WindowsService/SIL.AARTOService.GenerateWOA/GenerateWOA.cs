﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.GenerateWOA
{
    partial class GenerateWOA : ServiceHost
    {
        public GenerateWOA()
            : base("", new Guid("00C0120B-5DA3-48D7-A168-767BED68EAD5"), new GenerateWOAService())
        {
            InitializeComponent();
        }
    }
}
