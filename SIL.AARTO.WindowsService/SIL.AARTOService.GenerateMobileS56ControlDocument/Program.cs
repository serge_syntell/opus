﻿using SIL.ServiceLibrary;

namespace SIL.AARTOService.GenerateMobileS56ControlDocument
{
    static class Program
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ProgramRun.InitializeService(new GenerateMobileS56ControlDocument(),
                new ServiceDescriptor
                {
                    ServiceName = "SIL.AARTOService.GenerateMobileS56ControlDocument",
                    DisplayName = "SIL.AARTOService.GenerateMobileS56ControlDocument",
                    Description = "SIL AARTOService GenerateMobileS56ControlDocument"
                }, args);
        }
    }
}
