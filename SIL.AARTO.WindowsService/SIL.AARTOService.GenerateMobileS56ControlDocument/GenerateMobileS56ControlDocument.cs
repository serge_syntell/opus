﻿using System;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.GenerateMobileS56ControlDocument
{
    partial class GenerateMobileS56ControlDocument : ServiceHost
    {
        public GenerateMobileS56ControlDocument()
            : base("", new Guid("4BFF9A72-1512-4152-9BA9-FD25CA42F017"), new GenerateMobileS56ControlDocumentService())
        {
            InitializeComponent();
        }
    }
}
