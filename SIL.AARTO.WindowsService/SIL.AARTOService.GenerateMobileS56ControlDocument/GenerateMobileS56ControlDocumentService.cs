﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using SIL.AARTO.BLL.Utility.PrintFile;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.QueueLibrary;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;

namespace SIL.AARTOService.GenerateMobileS56ControlDocument
{
    class GenerateMobileS56ControlDocumentService : ServiceDataProcessViaQueue
    {
        readonly AuthorityService authorityService = new AuthorityService();

        readonly string connStr;
        readonly ServiceDB db;
        readonly PrintFileProcess process;
        string autCode;
        Authority authority;
        MobileControlDocument doc;
        bool isOfficerError;
        int pfnIntNo;
        QueueItem queueItem;

        public GenerateMobileS56ControlDocumentService()
            : base("", "", new Guid("88BB4011-1E39-46F2-8DCF-E8B7FF70E687"), ServiceQueueTypeList.GenerateMobileS56ControlDocument)
        {
            _serviceHelper = new AARTOServiceBase(this, false);
            this.connStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.db = new ServiceDB(this.connStr);
            this.process = new PrintFileProcess(this.connStr, AARTOBase.LastUser)
            {
                UseTransaction = false
            };
            AARTOBase.OnServiceStarting = () =>
            {
                this.process.LogProcessing = (m, t) => AARTOBase.LogProcessing(m, t, ServiceOption.Continue, this.queueItem, this.queueItem != null ? this.queueItem.Status : QueueItemStatus.UnKnown);
            };
        }

        AARTOServiceBase AARTOBase
        {
            get { return (AARTOServiceBase)_serviceHelper; }
        }

        public override void InitialWork(ref QueueItem item)
        {
            this.queueItem = item;

            Initialization();

            item.IsSuccessful = true;
            item.Status = QueueItemStatus.UnKnown;

            if (item.Body == null
                || !int.TryParse(item.Body.ToString(), out this.pfnIntNo))
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("InvalidQueueBody", item.Body), LogType.Error, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                return;
            }

            string[] group;

            if (string.IsNullOrWhiteSpace(item.Group)
                || !(group = item.Group.Split('|')).Length.In(2, 3)
                || string.IsNullOrWhiteSpace(group[0]))
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("InvalidQueueGroup", item.Group), LogType.Error, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                return;
            }

            if (this.authority == null
                || !this.authority.AutCode.Equals2(group[0]))
            {
                this.authority = this.authorityService.GetByAutCode(group[0].Trim2());
                if (this.authority == null)
                {
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("InvalidAuthority", group[0]), LogType.Error, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                }
            }

            if (group.Length == 3
                && group[2].Equals2("Error"))
                this.isOfficerError = true;

            this.doc = null;
            this.doc = GetMobileControlDocument(this.pfnIntNo);
            if (this.doc == null)
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("TheDocumentDoesNotExistOrHasBeenGenerated", this.pfnIntNo), LogType.Error, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            foreach (var item in queueList.Where(q => q.IsSuccessful))
            {
                this.queueItem = item;

                var module = new PrintFileModuleMobileS56ControlDocumentBatch(this.isOfficerError);
                module.onLoad = () =>
                {
                    module.PFNIntNo = this.pfnIntNo;
                    this.process.ExportPrintFilePath = this.doc.MobileControlDocumentFullPath;
                };

                this.process.BuildPrintFile(module, this.authority.AutIntNo, null, null, null, true);

                if (this.process.IsSuccessful)
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("TheDocumentHasBeenCreatedSuccessfully", this.process.ExportPrintFilePath), LogType.Info, ServiceOption.Continue, item, QueueItemStatus.Discard);
                else
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("FailedToCreateDocument", this.process.ExportPrintFilePath), LogType.Error, ServiceOption.ContinueButQueueFail, item);
            }
        }

        void Initialization()
        {
            this.pfnIntNo = 0;
            //this.autCode = null;
            this.isOfficerError = false;
        }

        MobileControlDocument GetMobileControlDocument(int pfnIntNo)
        {
            MobileControlDocument doc = null;
            var paras = new[]
            {
                new SqlParameter("@PFNIntNo", pfnIntNo)
            };
            this.db.ExecuteReader("VerifyMobileControlDocument", paras, dr =>
            {
                if (dr.Read())
                {
                    doc = new MobileControlDocument
                    {
                        PFNIntNo = ServiceDB.GetDataReaderValue<int>(dr, "PFNIntNo"),
                        PrintFileName = ServiceDB.GetDataReaderValue<string>(dr, "PrintFileName"),
                        ImageMachineName = ServiceDB.GetDataReaderValue<string>(dr, "ImageMachineName"),
                        ImageShareName = ServiceDB.GetDataReaderValue<string>(dr, "ImageShareName"),
                        MobileControlDocumentPath = ServiceDB.GetDataReaderValue<string>(dr, "MobileControlDocumentPath"),
                    };
                }
            });
            return doc;
        }
    }

    class MobileControlDocument
    {
        public int PFNIntNo { get; set; }
        public string PrintFileName { get; set; }
        public string ImageMachineName { get; set; }
        public string ImageShareName { get; set; }
        public string MobileControlDocumentPath { get; set; }

        public string MobileControlDocumentFullPath
        {
            get
            {
                return !string.IsNullOrWhiteSpace(ImageMachineName)
                    && !string.IsNullOrWhiteSpace(ImageShareName)
                    && !string.IsNullOrWhiteSpace(MobileControlDocumentPath)
                    ? string.Format(@"\\{0}\{1}\{2}", ImageMachineName, ImageShareName, MobileControlDocumentPath) : string.Empty;
            }
        }
    }
}
