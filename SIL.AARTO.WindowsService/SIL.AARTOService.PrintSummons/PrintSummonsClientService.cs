﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.AARTOService.Library;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using SIL.AARTO.BLL.Utility.PrintFile;
using Stalberg.TMS;
using SIL.AARTOService.Resource;
using System.IO;

namespace SIL.AARTOService.PrintSummons
{
    public class PrintSummonsClientService : ServiceDataProcessViaQueue
    {
        public PrintSummonsClientService()
            : base("", "", new Guid("0153B109-AC44-4AF6-AAD5-AFB71AA5A330"), ServiceQueueTypeList.PrintSummons)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            this.connUNC_Export = AARTOBase.GetConnectionString(ServiceConnectionNameList.PrintFilesFolder, ServiceConnectionTypeList.UNC);
            AARTOBase.OnServiceStarting = OnServiceStarting;
            AARTOBase.OnServiceSleeping = OnServiceSleeping;
            process = new PrintFileProcess(this.connAARTODB, AARTOBase.LastUser);
            process.ErrorProcessing = (s) => { this.Logger.Error(s); };
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        PrintFileProcess process;
        AuthorityDetails authDetails = null;
        string connAARTODB;
        string connUNC_Export;
        string currentAutCode, lastAutCode;
        string exportPath;
        int autIntNo;
        int pfnIntNo;
        string emailToAdministrator = string.Empty;
        List<string> fileList = new List<string>();

        public override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;

                    SetServiceParameters();

                    if (!ServiceUtility.IsNumeric(body))
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        return;
                    }

                    this.pfnIntNo = Convert.ToInt32(body.Trim());
                    this.currentAutCode = item.Group.Trim();

                    if (this.lastAutCode != this.currentAutCode)
                    {
                        QueueItemValidation(ref item);
                    }

                    //2013-05-21 added by Nancy start
                    string msg;
                    if (AARTOBase.GetPrintFileCount(this.pfnIntNo, "", "S", out msg) <= 0)
                    {
                        AARTOBase.LogProcessing(msg, LogType.Warning, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);//2013-05-23 added by Nancy
                        //AARTOBase.ErrorProcessing(ref item, msg, false, QueueItemStatus.Discard); 2013-05-21 removed by Nancy
                        return;
                    }
                    //2013-05-21 added by Nancy end
                    
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex.Message, true);
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                return;
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                try
                {
                    if (item.IsSuccessful)
                    {//2013-05-21 Nancy add 'if (item.IsSuccessful)' for checking whether the queue is successful
                        process.BuildPrintFile(new PrintFileModuleSummons(), this.autIntNo, this.pfnIntNo, this.exportPath);
                        if (process.IsSuccessful)
                        {
                            this.fileList.Add(process.ExportPrintFileName);
                            this.Logger.Info(ResourceHelper.GetResource("PrintFileSavedTo", process.PrintFileName, process.ExportPrintFilePath));
                        }
                        else
                        {
                            AARTOBase.ErrorProcessing(ref item);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ServiceHasError"), AARTOBase.LastUser, item.Body, ex.Message), true);
                }
                queueList[i] = item;
            }
        }

        private void QueueItemValidation(ref QueueItem item)
        {
            if (this.lastAutCode != this.currentAutCode)
            {
                this.exportPath = AARTOBase.GetPrintFileDirectory(this.connUNC_Export, this.currentAutCode, PrintServiceType.Summons);
                if (!AARTOBase.CheckFolder(this.exportPath)) return;

                this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim().Equals(this.currentAutCode, StringComparison.OrdinalIgnoreCase));
                if (this.authDetails != null && this.authDetails.AutIntNo > 0)
                {
                    this.autIntNo = this.authDetails.AutIntNo;
                    this.lastAutCode = this.currentAutCode;
                }
                else
                {
                    //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.currentAutCode), false, QueueItemStatus.Discard);
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.currentAutCode));
                    return;
                }
            }

        }

        private void OnServiceStarting()
        {
            if (this.fileList.Count > 0)
                this.fileList.Clear();
        }

        private void OnServiceSleeping()
        {
            if (this.fileList.Count > 0)
            {
                string title = ResourceHelper.GetResource("Summons");
                SendEmailManager.SendEmail(
                    this.emailToAdministrator,
                    ResourceHelper.GetResource("EmailSubjectOfPrintService", title),
                    SendEmailManager.GetFileListContent(title, this.fileList));
            }
        }


        private void SetServiceParameters()
        {
            if (ServiceParameters != null
                && string.IsNullOrEmpty(this.emailToAdministrator)
                && ServiceParameters.ContainsKey("EmailToAdministrator")
                && !string.IsNullOrEmpty(ServiceParameters["EmailToAdministrator"]))
                this.emailToAdministrator = ServiceParameters["EmailToAdministrator"];
            if (!SendEmailManager.CheckEmailAddress(this.emailToAdministrator))
                AARTOBase.ErrorProcessing(ResourceHelper.GetResource("EmailAddressError"), true);
        }

    }
}
