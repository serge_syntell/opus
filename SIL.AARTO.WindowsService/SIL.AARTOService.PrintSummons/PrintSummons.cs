﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PrintSummons
{
    partial class PrintSummons : ServiceHost
    {
        public PrintSummons()
            : base("", new Guid("AF38C12C-F82A-40A5-B0E8-C2BC4955CF40"), new PrintSummonsClientService())
        {
            InitializeComponent();
        }
    }
}
