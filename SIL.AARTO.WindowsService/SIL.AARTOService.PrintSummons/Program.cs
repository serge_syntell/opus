﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.PrintSummons
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.PrintSummons"
                ,
                DisplayName = "SIL.AARTOService.PrintSummons"
                ,
                Description = "SIL AARTOService PrintSummons"
            };

            ProgramRun.InitializeService(new PrintSummons(), serviceDescriptor, args);
        }
    }
}
