﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;
using SIL.AARTOService.Library;
using SIL.eNaTIS.DAL.Services;
using SIL.eNaTIS.DAL.Data;
using SIL.eNaTIS.DAL.Entities;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Transactions;
using SIL.ServiceBase;
using SIL.QueueLibrary;
using Stalberg.TMS;
using System.IO;
using System.Data;
using SIL.AARTOService.Resource;
using System.Threading;



namespace SIL.AARTOService.Notice_JMPD_ImportFromNTI
{
    public class Notice_JMPD_ImportFromNTIService : ClientService//ServiceDataProcessViaQueue
    {
        public Notice_JMPD_ImportFromNTIService()
            : base("", "", new Guid("B2C4D58C-7BED-4DCD-9EF1-4B6A8CD24F02"))
        {
            this._serviceHelper = new AARTOServiceBase(this, false);//The outer layer does not need Transaction
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            //2014-01-26 Heidi add for Extract For SMS data(5155)
            extractForSMSFolderPath = AARTOBase.GetConnectionString(ServiceConnectionNameList.ExtractForSMSFolder, ServiceConnectionTypeList.UNC);

            AARTOBase.OnServiceStarting = () =>
            {
                ServiceUtility.InitializeNetTier(connectStr);
                string eNaTUSStr = AARTOBase.GetConnectionString(SIL.ServiceQueueLibrary.DAL.Entities.ServiceConnectionNameList.SIL_NTI,
                     SIL.ServiceQueueLibrary.DAL.Entities.ServiceConnectionTypeList.DB);
                ServiceUtility.InitializeNetTier(eNaTUSStr, "SIL.eNaTIS");
            };
            AARTOBase.OnServiceSleeping = () =>
            {
            };

        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        string connectStr;
        //2014-01-26 Heidi add for Extract For SMS data(5155)
        string extractForSMSFolderPath = string.Empty;
        DateTime lastExecutionTime = DateTime.MinValue;

        NoticeService noticeService = new NoticeService();
        SIL.AARTO.DAL.Services.EnatisErrorLogService errorLogService = new AARTO.DAL.Services.EnatisErrorLogService();
        SIL.AARTO.DAL.Services.ChargeService aARTOChargeService = new AARTO.DAL.Services.ChargeService();
        SIL.AARTO.DAL.Services.ReportConfigService reportConfigService = new AARTO.DAL.Services.ReportConfigService();
        SIL.eNaTIS.DAL.Services.NoticeService eNaTISNoticeServie = new NoticeService();
        SIL.AARTO.DAL.Services.NoticeService aartoNoticeServie = new SIL.AARTO.DAL.Services.NoticeService();
        SIL.AARTO.DAL.Data.ReportConfigQuery query = new SIL.AARTO.DAL.Data.ReportConfigQuery();
        SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.ReportConfig> rcs;
        SIL.AARTO.DAL.Entities.ReportConfig reportConfigEntity = new SIL.AARTO.DAL.Entities.ReportConfig();
        public override void MainWork(ref List<QueueLibrary.QueueItem> queueList)
        {
            var noticeList = GetNoticeListByLastNoticeID(0);
            HandleNoticeData(noticeList);

            //2014-01-26 Heidi add for Extract For SMS data(5155)
            GetLastExecutionTime();

            if (lastExecutionTime!=DateTime.MinValue)
            {
               
                if (Convert.ToDateTime(lastExecutionTime.ToShortDateString()) < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
                {
                    ExtractDataToFile(reportConfigEntity);
                }
            }
            else
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("CanNotFindSMSReport"),LogType.Info);
            }
           
        }

        private void GetLastExecutionTime()
        {
            query.AppendEquals(SIL.AARTO.DAL.Entities.ReportConfigColumn.ReportCode, ((int)SIL.AARTO.DAL.Entities.ReportConfigCodeList.SMSReport).ToString());
            rcs = reportConfigService.Find(query);
            if (rcs != null && rcs.Count > 0)
            {
                reportConfigEntity = new SIL.AARTO.DAL.Entities.ReportConfig();
                reportConfigEntity = rcs[0];
                if (reportConfigEntity != null)
                {
                    //if AutomaticGenerateLastDatetime is null need set lastExecutionTime=DateTime.Now.AddDays(-2) for the first can be executed
                    lastExecutionTime = reportConfigEntity.AutomaticGenerateLastDatetime.HasValue ? Convert.ToDateTime(reportConfigEntity.AutomaticGenerateLastDatetime) : DateTime.Now.AddDays(-2);
                }
                else
                {
                    lastExecutionTime = DateTime.MinValue;
                }
            }
            else
            {
                lastExecutionTime = DateTime.MinValue;
            }
        }

        private void ExtractDataToFile(SIL.AARTO.DAL.Entities.ReportConfig reportConfigEntity)
        {
            StringBuilder sb = new StringBuilder();
            List<SIL.eNaTIS.DAL.Entities.Notice> noticelist = GetNoticeListForExtractToSMSData();
            SIL.eNaTIS.DAL.Entities.TList<SIL.eNaTIS.DAL.Entities.Notice> needUpdateNoticeList = new SIL.eNaTIS.DAL.Entities.TList<SIL.eNaTIS.DAL.Entities.Notice>();
            NoticeDB noticedb = new NoticeDB(connectStr);
            using (TransactionScope scope = ServiceUtility.CreateTransactionScope())
            {
                try
                {
                    if (noticelist != null && noticelist.Count > 0)
                    {
                        //sb.Append("LicN" + "," + "DrvIDNumber" + "," + "NotOffenceDate" + "\r\n");

                        foreach (var noticeitem in noticelist)
                        {
                            string strDrvIDNumberNotOffenceDate = noticedb.GetNoticeDataByENatisGuid(noticeitem.ENaTisGuid);
                            if (strDrvIDNumberNotOffenceDate != "")
                            {
                                sb.Append(noticeitem.Licn +","+ strDrvIDNumberNotOffenceDate + "\r\n");
                                noticeitem.ExtractForSms = true;
                                eNaTISNoticeServie.Update(noticeitem);
                                //needUpdateNoticeList.Add(noticeitem);
                            }
                        }

                        if (!Directory.Exists(extractForSMSFolderPath))
                        {
                            Directory.CreateDirectory(extractForSMSFolderPath);
                        }
                        string filename = "sms_data"+DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss").Replace(":", "") + ".csv";
                        //write the exported files
                        using (FileStream fs = new FileStream(extractForSMSFolderPath + "\\" + filename, FileMode.Create, FileAccess.Write))
                        {
                            using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.ASCII))
                            {
                                sw.Write(sb.ToString());
                            }
                        }

                        AARTOBase.LogProcessing(ResourceHelper.GetResource("CSVFileHasBeenCreatedSuccessfully", filename), LogType.Info);
                        //if (needUpdateNoticeList != null && needUpdateNoticeList.Count > 0)
                        //{
                        //    eNaTISNoticeServie.Update(needUpdateNoticeList);
                        //}

                        //update last Execution Time
                        if (reportConfigEntity != null)
                        {
                            reportConfigEntity.AutomaticGenerateLastDatetime = DateTime.Now;
                            reportConfigService.Update(reportConfigEntity);
                        }
                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.LogProcessing(ex, LogType.Error, ServiceOption.Break);
                }
            }
        }
       

        public void HandleNoticeData(SIL.eNaTIS.DAL.Entities.TList<Notice> noticeList)
        {
            int NoticeIDTemp = 0;
            if (noticeList != null && noticeList.Count > 0)
            {
                for (int i = 0; i < noticeList.Count; i++)
                {
                    //Operating
                    HandleNoticeImportFromNTI(noticeList[i]);

                    if (i == noticeList.Count - 1)
                    {
                        NoticeIDTemp = noticeList[i].NoticeId;
                        var noticeNextList = GetNoticeListByLastNoticeID(NoticeIDTemp);
                        if (noticeNextList != null && noticeNextList.Count > 0)
                        {
                            HandleNoticeData(noticeNextList);
                        }
                    }

                }
                //get next a 50 data
                
            }

        }

        private SIL.eNaTIS.DAL.Entities.TList<Notice> GetNoticeListByLastNoticeID(int NoticeID)
        {
            string where = "NoticeID>" + NoticeID + " and ( eNatisQueueStatusID=" + (int)ENatisQueueStatusList.Success
                + " OR eNatisQueueStatusID=" + (int)ENatisQueueStatusList.Failed
                + " )  ";//+ " and (AARTOImported=0 OR AARTOImported IS NULL)  ";//ErrorCode或者此notice是否已经下载
            int totalCount = 0;
            var noticeList = noticeService.GetPaged(where, "NoticeID", 0, 50, out totalCount);
            return noticeList;
        }

        //2014-01-26 Heidi add for Extract For SMS data(5155)
        private List<SIL.eNaTIS.DAL.Entities.Notice> GetNoticeListForExtractToSMSData()
        {
            NoticeService noticeService = new NoticeService();
            NoticeQuery noticeQuery = new NoticeQuery();

            noticeQuery.Append(NoticeColumn.ENatisQueueStatusId, Convert.ToInt32(ENatisQueueStatusList.Imported).ToString());
            noticeQuery.Append(NoticeColumn.ExtractForSms,"false");
            return noticeService.Find(noticeQuery as SIL.eNaTIS.DAL.Data.IFilterParameterCollection).ToList();
        }

       
        private void HandleNoticeImportFromNTI(SIL.eNaTIS.DAL.Entities.Notice nTINotice)
        {

            // nTINotice.MagisterialCd
            using (TransactionScope scope = ServiceUtility.CreateTransactionScope())
            {
                try
                {
                    if (nTINotice != null)
                    {
                        if (nTINotice.ENatisQueueStatusId == (int)ENatisQueueStatusList.Success)
                        {
                            //using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,TimeSpan.MaxValue))
                            //{
                            //try
                            //{
                            SIL.AARTO.DAL.Services.NoticeService aARTONoticeService = new AARTO.DAL.Services.NoticeService();
                            //update notice table of arrto database, notice.NotTicketNo=InfringeNoticen
                            SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.Notice> aARTONoticeList
                                = aARTONoticeService.GetByENaTisGuid(nTINotice.ENaTisGuid);
                            if (aARTONoticeList != null && aARTONoticeList.Count > 0)
                            {

                                SIL.AARTO.DAL.Entities.Notice noticeitem = aARTONoticeList[0];
                                noticeitem.NotTicketNo = nTINotice.InfringeNoticen;
                                //2014-01-23 Heidi added NotTicketNo display format for JMPD (5155)
                                try
                                {
                                    noticeitem.NotTicketNo = nTINotice.InfringeNoticen.Substring(0, 2) + "-" + nTINotice.InfringeNoticen.Substring(2, 4) + "-" + nTINotice.InfringeNoticen.Substring(6, 9) + "-" + nTINotice.InfringeNoticen.Substring(15, 1);
                                }
                                catch (Exception ex)
                                {
                                    AARTOBase.LogProcessing(ResourceHelper.GetResource("InvalidNoticeTicketNoFromNTIDataBase", nTINotice.InfringeNoticen), LogType.Error,ServiceOption.Break);
                                    return;
                                }
                                noticeitem.NoticeStatus = 255;
                                aARTONoticeService.Update(noticeitem);


                                // update charge table ChargeStatus=255
                                SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.Charge> chargeModelList
                                    = aARTOChargeService.GetByNotIntNo(noticeitem.NotIntNo);
                                if (chargeModelList != null && chargeModelList.Count > 0)
                                {
                                    foreach (SIL.AARTO.DAL.Entities.Charge item in chargeModelList)
                                    {
                                        item.ChargeStatus = 255;
                                        aARTOChargeService.Update(item);
                                    }
                                }

                                //handel Success，then update NTL database notice table IsImport=true。
                                nTINotice.ENatisQueueStatusId = (int)ENatisQueueStatusList.Imported;
                                noticeService.Update(nTINotice);

                                // Jake 2013-09-16 added push SubmitInfringementRecord queue here 
                                new QueueItemProcessor().Send(new QueueItem()
                                {
                                    Body = noticeitem.NotIntNo,
                                    QueueType = ServiceQueueTypeList.SubmitInfringementRecord
                                });
                            }
                            // scope.Complete();
                            //}
                            //catch (Exception ex)
                            //{
                            //    AARTOBase.ErrorProcessing(ex, null);
                            //}
                            //}
                        }
                        else if (nTINotice.ENatisQueueStatusId == (int)ENatisQueueStatusList.Failed)
                        {

                            //try
                            //{
                            SIL.AARTO.DAL.Services.NoticeService aARTONoticeService = new AARTO.DAL.Services.NoticeService();
                            //insert a Record into eNatisErrorLog table
                            SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.Notice> aARTONoticeListTemp
                                = aARTONoticeService.GetByENaTisGuid(nTINotice.ENaTisGuid);
                            if (aARTONoticeListTemp != null && aARTONoticeListTemp.Count > 0)
                            {

                                SIL.AARTO.DAL.Entities.Notice noticeitem = aARTONoticeListTemp[0];


                                SIL.AARTO.DAL.Data.EnatisErrorLogQuery query = new SIL.AARTO.DAL.Data.EnatisErrorLogQuery();
                                query.Append(SIL.AARTO.DAL.Entities.EnatisErrorLogColumn.NotIntNo, noticeitem.NotIntNo.ToString());
                                query.Append(SIL.AARTO.DAL.Entities.EnatisErrorLogColumn.EnelHandleStatus, "1");

                                SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.EnatisErrorLog> eNatisErrorLogSource
                                    = errorLogService.Find(query);
                                if (eNatisErrorLogSource != null && eNatisErrorLogSource.Count > 0)
                                { }
                                else
                                {
                                    //using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,TimeSpan.MaxValue))
                                    //{
                                    noticeitem.NoticeStatus = 105;
                                    aARTONoticeService.Update(noticeitem);


                                    // update charge table ChargeStatus=105
                                    SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.Charge> chargeModelList
                                        = aARTOChargeService.GetByNotIntNo(noticeitem.NotIntNo);
                                    if (chargeModelList != null && chargeModelList.Count > 0)
                                    {
                                        foreach (SIL.AARTO.DAL.Entities.Charge item in chargeModelList)
                                        {
                                            item.ChargeStatus = 105;
                                            aARTOChargeService.Update(item);
                                        }
                                    }

                                    SIL.AARTO.DAL.Entities.EnatisErrorLog eNatisErrorLog = new AARTO.DAL.Entities.EnatisErrorLog();
                                    eNatisErrorLog.NotIntNo = noticeitem.NotIntNo;
                                    eNatisErrorLog.ErrorCode = nTINotice.ErrorCode;
                                    eNatisErrorLog.ErrorDesc = nTINotice.ErrorDesc;
                                    eNatisErrorLog.EnelDate = DateTime.Now;
                                    eNatisErrorLog.EnelHandleStatus = 1;//1:unprocessed 2：rejected 3:resend
                                    errorLogService.Insert(eNatisErrorLog);

                                    nTINotice.ENatisQueueStatusId = (int)ENatisQueueStatusList.Imported;
                                    noticeService.Update(nTINotice);

                                    // scope.Complete();
                                    //}

                                }
                            }
                            //}
                            //catch (Exception ex)
                            //{
                            //    AARTOBase.ErrorProcessing(ex, null);
                            //}
                        }
                    }

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    AARTOBase.LogProcessing(ex, LogType.Error, ServiceOption.Break);
                }
            }//

        }

    }

}

