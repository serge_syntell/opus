﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Notice_JMPD_ImportFromNTI
{
    partial class Notice_JMPD_ImportFromNTI : ServiceHost
    {
        public Notice_JMPD_ImportFromNTI()
            :base("",new Guid("9D352DB2-DD23-4BD0-A8AB-00ACD6F75002"),new Notice_JMPD_ImportFromNTIService())
        {
            InitializeComponent();
            
        }
    }
}
