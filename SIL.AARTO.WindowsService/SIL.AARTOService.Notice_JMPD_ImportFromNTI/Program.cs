﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Notice_JMPD_ImportFromNTI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.Notice_JMPD_ImportFromNTI"
                ,
                DisplayName = "SIL.AARTOService.Notice_JMPD_ImportFromNTI"
                ,
                Description = "SIL.AARTOService.Notice_JMPD_ImportFromNTI"
            };

            ProgramRun.InitializeService(new Notice_JMPD_ImportFromNTI(), serviceDescriptor, args);
        }
    }
}
