﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using SIL.AARTOService.Resource;
using Stalberg.TMS;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using System.Data;
using Stalberg.TMS_TPExInt.Components;

namespace SIL.AARTOService.AllocateCaseNumber
{
    // Oscar 20120309 add for print service Q
    //public class PrintCourtRollQueue
    //{
    //    public int PFNIntNo { get; set; }
    //    public string AutCode { get; set; }
    //    public DateTime ActDate { get; set; }
    //}
    //public class PrintCourtRollQueueList : List<PrintCourtRollQueue>
    //{
    //    public void Add(int pfnIntNo, string autCode, DateTime actDate)
    //    {
    //        PrintCourtRollQueue item = new PrintCourtRollQueue()
    //        {
    //            PFNIntNo = pfnIntNo,
    //            AutCode = autCode,
    //            ActDate = actDate
    //        };
    //        this.Add(item);
    //    }
    //    public bool ContainsKey(int pfnIntNo)
    //    {
    //        if (this == null || this.Count <= 0) return false;
    //        return this.FirstOrDefault(p => p.PFNIntNo == pfnIntNo) != null;
    //    }
    //}

    public class AllocateCaseNumberClientService : ServiceDataProcessViaQueue
    {
        public AllocateCaseNumberClientService()
            : base("", "", new Guid("4D214FBB-0E7E-4656-A369-E62F70CCEFA3"), ServiceQueueTypeList.SummonsIsServed)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            AARTOBase.OnServiceStarting = () =>
            {
                this.pfnList.Clear();
                SetServiceParameters();
                this.lastGroup = null;
                this.currentGroup = null;
            };
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        AuthorityDetails authDetails = null;

        string connAARTODB;
        string currentAutCode, lastAutCode;
        string crtName, crtRoomNo;
        DateTime cDate;
        int sumIntNo, autIntNo;
        string autName;
        string caseNumberGenerateRule;
        int noOfDays;
        DateTime processDate = DateTime.Now;
        DateTime? courtDate = null;
        List<int> pfnList = new List<int>();

        DateTime createDate = DateTime.Now;
        string currentGroup, lastGroup;

        public override void InitialWork(ref QueueItem item)
        {
            if (item.Body != null && !string.IsNullOrWhiteSpace(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;

                    if (ServiceUtility.IsNumeric(item.Body))
                        this.sumIntNo = Convert.ToInt32(item.Body);

                    this.currentGroup = item.Group.Trim();
                    string[] group = this.currentGroup.Split('|');
                    if (group == null || group.Length != 4)
                    {
                        AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("QueueGroupValidationFailed"), true);
                        return;
                    }

                    this.currentAutCode = group[0].Trim();
                    this.crtName = group[1].Trim();
                    this.crtRoomNo = group[2].Trim();
                    this.cDate = Convert.ToDateTime(group[3].Trim());

                    // Oscar 2013-03-11 added for passed date checking.
                    if (DateTime.Now.Date >= this.cDate.Date)
                    {
                        Logger.Warning(ResourceHelper.GetResource("CourtDatePassedNoCaseNumber", this.sumIntNo));
                        item.IsSuccessful = false;
                        item.Status = QueueItemStatus.Discard;
                        return;
                    }

                    this.courtDate = null;

                    rules.InitParameter(this.connAARTODB, this.currentAutCode);

                    if (this.lastAutCode != this.currentAutCode)
                    {
                        if (QueueItemValidation(ref item))
                        {
                            this.lastAutCode = this.currentAutCode;
                            this.processDate = DateTime.Now;
                        }
                    }

                    if (this.lastGroup != this.currentGroup)
                    {
                        this.createDate = DateTime.Now;
                        this.lastGroup = this.currentGroup;
                    }

                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                    return;
                }
            }
            else
            {
                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.currentAutCode));
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                return;
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            string msg;
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                if (item.IsSuccessful)
                {
                    try
                    {
                        if (!ProcessingAllocateCaseNumber(ref item, out msg))
                        {
                            AARTOBase.ErrorProcessing(ref item, msg);
                            return;
                        }
                        else
                        {
                            if (this.courtDate != null)
                            {
                                QueueItemProcessor queProcessor = new QueueItemProcessor();
                                queProcessor.Send(
                                    new QueueItem()
                                    {
                                        Body = this.sumIntNo,
                                        Group = this.currentAutCode,
                                        ActDate = this.courtDate ?? DateTime.Now,
                                        QueueType = ServiceQueueTypeList.SummonsLock
                                    },
                                    new QueueItem()
                                    {
                                        Body = this.sumIntNo,
                                        Group = this.currentAutCode,
                                        ActDate = (this.courtDate ?? DateTime.Now).AddDays(rules.Rule("6100").ARNumeric),
                                        QueueType = ServiceQueueTypeList.SummonsLock
                                    }
                                );
                            }
                        }


                    }
                    catch (Exception ex)
                    {
                        AARTOBase.ErrorProcessing(ref item, ex);
                        return;
                    }
                }
                queueList[i] = item;
            }
        }


        private bool QueueItemValidation(ref QueueItem item)
        {
            if (this.lastAutCode != this.currentAutCode)
            {
                this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim().Equals(this.currentAutCode, StringComparison.OrdinalIgnoreCase));
                if (this.authDetails != null && this.authDetails.AutIntNo > 0)
                {
                    this.autIntNo = this.authDetails.AutIntNo;
                    this.autName = this.authDetails.AutName;

                    //Jerry 2012-12-20 change
                    SIL.AARTO.DAL.Entities.NoticeSummons noticeSummonsEntity = new SIL.AARTO.DAL.Services.NoticeSummonsService().GetBySumIntNo(this.sumIntNo).FirstOrDefault();
                    SIL.AARTO.DAL.Entities.Notice noticeEntity = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(noticeSummonsEntity.NotIntNo);
                    //if (this.authDetails.AutTicketProcessor == "AARTO" || this.authDetails.AutTicketProcessor == "TMS" || this.authDetails.AutTicketProcessor == "JMPD_AARTO")
                    if (noticeEntity != null && noticeEntity.NotTicketProcessor == "TMS")
                    {
                        this.Logger.Info(string.Format(ResourceHelper.GetResource("StartingCaseNoAlloactionForSummons"), noticeEntity.NotTicketProcessor, this.sumIntNo, this.processDate));

                        if (rules.Rule("6010").ARString.ToUpper().Equals("Y"))
                        {
                            this.Logger.Info(string.Format(ResourceHelper.GetResource("ProcessingCaseNoAllocationForAuthority"), this.autName, this.sumIntNo));

                            this.noOfDays = rules.Rule("CDate", "FinalCourtRoll").DtRNoOfDays;
                            this.caseNumberGenerateRule = rules.Rule("6030").ARString;

                            return true;
                        }
                        else
                        {
                            item.IsSuccessful = false;
                            item.Status = QueueItemStatus.Discard;
                            this.Logger.Info(string.Format(ResourceHelper.GetResource("CaseNoAllocatedManually"), this.autName, this.sumIntNo));
                            return false;
                        }
                    }
                    else
                    {

                        //Jake 2013-07-12 added message when discard queue
                        item.IsSuccessful = false;
                        item.Status = QueueItemStatus.Discard;
                        this.Logger.Info(String.Format("Invalid notice ticket processor  {0}", (noticeEntity == null ? "" : noticeEntity.NotTicketProcessor)), item.Body.ToString());
                        return false;
                    }
                }
                else
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.currentAutCode));
                    return false;
                }
            }
            else
                return true;
        }

        private bool ProcessingAllocateCaseNumber(ref QueueItem item, out string errorMsg)
        {
            string msg;
            errorMsg = string.Empty;
            if (!item.IsSuccessful) return false;

            //Jerry 2012-12-20 change
            //if (GetAuthorityForServedSummons_WS(this.authDetails.AutTicketProcessor, out msg))
            if (GetAuthorityForServedSummons_WS("TMS", out msg))
            {
                int crtIntNo, crtRIntNo;
                DateTime? courtDate = this.cDate;
                DataRow dr;
                DataSet ds = CheckForAllocateCaseNo_WS(out msg);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    dr = ds.Tables[0].Rows[0];
                    crtIntNo = DBHelper.GetDataRowValue<int>(dr, "CrtIntNo");
                    crtRIntNo = DBHelper.GetDataRowValue<int>(dr, "CrtRIntNo");

                    if (crtIntNo > 0 && crtRIntNo > 0)
                    {
                        if (dr.IsNull("CDCourtRollCreatedDate"))
                        {
                            int nRes = CaseNoAllocate_WS(crtRIntNo, ref courtDate, rules.Rule(crtIntNo, "1020").CRString, out msg);

                            string error = string.Format(ResourceHelper.GetResource("NoCaseNoAllocatedForCourtRoom"), this.crtRoomNo);
                            switch (nRes)
                            {
                                case -1:
                                    errorMsg = string.Format(ResourceHelper.GetResource("UnableToCreateTempSummonsTable"), error);
                                    return false;
                                case -2:
                                    errorMsg = string.Format(ResourceHelper.GetResource("UnableToUpdateCaseNoForSummons"), error);
                                    return false;
                                case -3:
                                    errorMsg = string.Format(ResourceHelper.GetResource("UnableToUpdateChargeStatus"), error);
                                    return false;
                                case -4:
                                    errorMsg = string.Format(ResourceHelper.GetResource("UnableToUpdateSumChargeTable"), error);
                                    return false;
                                case -5:
                                    errorMsg = string.Format(ResourceHelper.GetResource("UnableToUpdateCourtRoomTable"), error);
                                    return false;
                                case -6:
                                    errorMsg = string.Format(ResourceHelper.GetResource("UnableToUpdateCourtDatesTable"), error);
                                    return false;
                                case -7:
                                    errorMsg = string.Format(ResourceHelper.GetResource("UnableToUpdateCourtRoomTableForNewYear"), error);
                                    return false;
                                case -8:
                                    errorMsg = string.Format(ResourceHelper.GetResource("UnableToInsertEvidencePack"), error);
                                    return false;
                                case 0:
                                    if (string.IsNullOrEmpty(msg))
                                        this.Logger.Info(string.Format(ResourceHelper.GetResource("NoCaseNoAllocatedForCourtRoom"), this.crtRoomNo));
                                    else
                                    {
                                        AARTOBase.ErrorProcessing(ref item, null, true);
                                        return false;
                                    }
                                    break;
                                case 1:
                                    this.Logger.Info(string.Format(ResourceHelper.GetResource("SuccessfullyAllocatedCaseNoForCourtRoom"), this.crtRoomNo));
                                    break;
                                default:
                                    errorMsg = string.Format(ResourceHelper.GetResource("ErrorInAllocatingCaseNoForCourtRoom"), this.crtRoomNo);
                                    return false;
                            }
                            if (nRes > 0 && courtDate != null)
                            {
                                this.courtDate = courtDate;

                                int pfnIntNo = -1;
                                DateTime actionDate;
                                SetPrintFileName(this.sumIntNo, courtDate.Value, out pfnIntNo, out actionDate);

                                if (!this.pfnList.Contains(pfnIntNo))
                                {
                                    this.pfnList.Add(pfnIntNo);
                                    new QueueItemProcessor().Send(
                                        new QueueItem()
                                        {
                                            Body = pfnIntNo,
                                            Group = this.currentAutCode,
                                            // 2014-08-14 Jerry fixed action date
                                            //ActDate = actionDate > this.printActionDate ? actionDate : this.printActionDate,
                                            ActDate = actionDate > DateTime.Now.AddHours(this.delayHours) ? actionDate : DateTime.Now.AddHours(this.delayHours),
                                            QueueType = ServiceQueueTypeList.PrintCourtRoll
                                        }
                                    );
                                }
                            }
                            return true;
                        }
                    }
                    else
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        //this.Logger.Warning(string.Format(ResourceHelper.GetResource("NoCourtRoomsAllowableForAuthority"), this.autName));
                        // Oscar 20120504 changed
                        this.Logger.Warning(string.Format(ResourceHelper.GetResource("NoCourtRoomsAllowableForAuthority"), this.crtName, this.crtRoomNo, courtDate, this.autName));
                        return false;
                    }
                }

                #region
                //DataSet dsRooms = GetCourtRoomsForAut(out msg);
                //DataSet dsDates;
                //TMSData tmsDB = new TMSData(this.connAARTODB);
                //if (dsRooms != null && dsRooms.Tables.Count > 0 && dsRooms.Tables[0].Rows.Count > 0)
                //{
                //    int crtRIntNo;
                //    string courtRoomNo;
                //    DataRow drRoom, drDate;
                //    DateTime? courtDate;
                //    for (int i = 0; i < dsRooms.Tables[0].Rows.Count; i++)
                //    {
                //        drRoom = dsRooms.Tables[0].Rows[i];
                //        crtRIntNo = DBHelper.GetDataRowValue<int>(drRoom, "CrtRIntNo");
                //        courtRoomNo = DBHelper.GetDataRowValue<string>(drRoom, "CrtRoomNo");

                //        dsDates = GetCourtDatesListByRule(crtRIntNo, out msg);
                //        if (dsDates != null && dsDates.Tables.Count > 0 && dsDates.Tables[0].Rows.Count > 0)
                //        {
                //            for (int j = 0; j < dsDates.Tables[0].Rows.Count; j++)
                //            {
                //                drDate = dsDates.Tables[0].Rows[j];
                //                if (drDate["CDCourtRollCreatedDate"] == null || drDate["CDCourtRollCreatedDate"] == DBNull.Value)
                //                {
                //                    //CourtRulesDetails courtRulesDetails = new CourtRulesDetails();
                //                    //courtRulesDetails.CrtIntNo = DBHelper.GetDataRowValue<int>(drRoom, "CrtIntNo");
                //                    //courtRulesDetails.CRCode = "1020";
                //                    //courtRulesDetails.LastUser = AARTOBase.lastUser;
                //                    //DefaultCourtRules defaultCourtRule = new DefaultCourtRules(courtRulesDetails, this.connAARTODB);
                //                    //KeyValuePair<int, string> courtRule = defaultCourtRule.SetDefaultCourtRule();

                //                    courtDate = DBHelper.GetDataRowValue<DateTime>(drDate, "CDate");
                //                    int nRes = CaseNoAllocate_WS(crtRIntNo, ref courtDate, rules.Rule(DBHelper.GetDataRowValue<int>(drRoom, "CrtIntNo"), "1020").CRString, out msg);

                //                    string error = string.Format(ResourceHelper.GetResource("NoCaseNoAllocatedForCourtRoom"), courtRoomNo);
                //                    switch (nRes)
                //                    {
                //                        case -1:
                //                            this.Logger.Error(string.Format(ResourceHelper.GetResource("UnableToCreateTempSummonsTable"), error));
                //                            return false;
                //                            break;
                //                        case -2:
                //                            this.Logger.Error(string.Format(ResourceHelper.GetResource("UnableToUpdateCaseNoForSummons"), error));
                //                            return false;
                //                            break;
                //                        case -3:
                //                            this.Logger.Error(string.Format(ResourceHelper.GetResource("UnableToUpdateChargeStatus"), error));
                //                            return false;
                //                            break;
                //                        case -4:
                //                            this.Logger.Error(string.Format(ResourceHelper.GetResource("UnableToUpdateSumChargeTable"), error));
                //                            return false;
                //                            break;
                //                        case -5:
                //                            this.Logger.Error(string.Format(ResourceHelper.GetResource("UnableToUpdateCourtRoomTable"), error));
                //                            return false;
                //                            break;
                //                        case -6:
                //                            this.Logger.Error(string.Format(ResourceHelper.GetResource("UnableToUpdateCourtDatesTable"), error));
                //                            return false;
                //                            break;
                //                        case -7:
                //                            this.Logger.Error(string.Format(ResourceHelper.GetResource("UnableToUpdateCourtRoomTableForNewYear"), error));
                //                            return false;
                //                            break;
                //                        case -8:
                //                            this.Logger.Error(string.Format(ResourceHelper.GetResource("UnableToInsertEvidencePack"), error));
                //                            return false;
                //                            break;
                //                        case 0:
                //                            if (string.IsNullOrEmpty(msg))
                //                                this.Logger.Info(string.Format(ResourceHelper.GetResource("NoCaseNoAllocatedForCourtRoom"), courtRoomNo));
                //                            else
                //                            {
                //                                item.IsSuccessful = false;
                //                                item.Status = QueueItemStatus.UnKnown;
                //                                StopService();
                //                                return false;
                //                            }
                //                            break;
                //                        case 1:
                //                            this.Logger.Info(string.Format(ResourceHelper.GetResource("SuccessfullyAllocatedCaseNoForCourtRoom"), courtRoomNo));
                //                            break;
                //                        default:
                //                            this.Logger.Error(string.Format(ResourceHelper.GetResource("ErrorInAllocatingCaseNoForCourtRoom"), courtRoomNo));
                //                            return false;
                //                            break;
                //                    }
                //                    if (nRes > 0 && courtDate != null)
                //                        this.courtDate = courtDate;
                //                }
                //            }
                //        }
                //        else
                //        {
                //            this.Logger.Info(string.Format(ResourceHelper.GetResource("NoCourtDatesForCourtRoom"), courtRoomNo));
                //        }
                //    }

                //    return true;
                //}
                //else
                //{
                //    item.IsSuccessful = false;
                //    item.Status = QueueItemStatus.UnKnown;
                //    this.Logger.Warning(string.Format(ResourceHelper.GetResource("NoCourtRoomsAllowableForAuthority"), this.autName));
                //    return false;
                //}
                #endregion

            }
            else
            {
                //AARTOBase.ErrorProcessing(ref item);
                AARTOBase.ErrorProcessing(ref item, null, false, QueueItemStatus.Discard);
                this.Logger.Warning(string.Format(ResourceHelper.GetResource("CaseNoAndCourtRoomAllocationNoSummonsEligible"), this.autName, this.sumIntNo));
                return false;
            }

            return false;
        }

        private void SetPrintFileName(int sumIntNo, DateTime courtDate, out int pfnIntNo, out DateTime actionDate)
        {
            string msg;
            actionDate = courtDate.AddDays(this.noOfDays);
            string printFileName = string.Format("CR_{0}_{1}_{2}_{3}_{4}", this.currentAutCode, this.crtName, this.crtRoomNo.Trim(), courtDate.ToString("yyyy-MM-dd"), actionDate.ToString("yyyy-MM-dd HH-mm"));
            pfnIntNo = AARTOBase.SavePrintFileName(PrintFileNameType.Summons, printFileName, this.autIntNo, sumIntNo, out msg);
            if (pfnIntNo <= 0 || !string.IsNullOrWhiteSpace(msg))
                AARTOBase.ErrorProcessing(msg, true);
        }

        private bool GetAuthorityForServedSummons_WS(string ticketProcessor, out string message)
        {
            DBHelper db = new DBHelper(this.connAARTODB);
            SqlParameter[] paras = new SqlParameter[3];
            paras[0] = new SqlParameter("@AutIntNo", this.autIntNo);
            paras[1] = new SqlParameter("@SumIntNo", this.sumIntNo);
            paras[2] = new SqlParameter("@Processor", ticketProcessor ?? "");
            DataSet ds = db.ExecuteDataSet("AuthListForServedSummons_WS", paras, out message);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                return true;
            else
                return false;
        }

        //private DataSet GetCourtRoomsForAut(out string message)
        //{
        //    DBHelper db = new DBHelper(this.connAARTODB);
        //    SqlParameter[] paras = new SqlParameter[1];
        //    paras[0] = new SqlParameter("@AutIntNo", this.autIntNo);
        //    return db.ExecuteDataSet("CourtRoomsByAuthority", paras, out message);
        //}

        //private DataSet GetCourtDatesListByRule(int crtRIntNo, out string message)
        //{
        //    DBHelper db = new DBHelper(this.connAARTODB);
        //    SqlParameter[] paras = new SqlParameter[3];
        //    paras[0] = new SqlParameter("@CrtRIntNo", crtRIntNo);
        //    paras[1] = new SqlParameter("@AutIntNo", this.autIntNo);
        //    paras[2] = new SqlParameter("@NoOfDays", this.noOfDays);
        //    return db.ExecuteDataSet("CourtDatesListWithRule", paras, out message);
        //}

        private int CaseNoAllocate_WS(int crtRIntNo, ref DateTime? dtCourtDate, string courtRule, out string message)
        {
            DBHelper db = new DBHelper(this.connAARTODB);
            SqlParameter[] paras = new SqlParameter[9];
            paras[0] = new SqlParameter("@AutIntNo", this.autIntNo);
            paras[1] = new SqlParameter("@SumIntNo", this.sumIntNo);
            paras[2] = new SqlParameter("@CaseNumberGenerateRule", this.caseNumberGenerateRule);
            paras[3] = new SqlParameter("@CrtRIntNo", crtRIntNo);
            paras[4] = new SqlParameter("@CourtDate", dtCourtDate) { Direction = ParameterDirection.InputOutput };
            paras[5] = new SqlParameter("@CourtRule", courtRule);
            paras[6] = new SqlParameter("@CreateDate", this.createDate);
            paras[7] = new SqlParameter("@LastUser", AARTOBase.LastUser);
            //Jerry 2013-01-18 add
            paras[8] = new SqlParameter("@CaseNoFormatRule", rules.Rule("6005").ARString);
            object obj = db.ExecuteScalar("CaseNoAllocate_WS", paras, out message);
            dtCourtDate = null;
            if (ServiceUtility.IsNumeric(obj))
            {
                if (paras[4].Value != null && paras[4].Value != DBNull.Value)
                    dtCourtDate = Convert.ToDateTime(paras[4].Value);
                return Convert.ToInt32(obj);
            }
            else
                return 0;
        }

        private DataSet CheckForAllocateCaseNo_WS(out string message)
        {
            DBHelper db = new DBHelper(this.connAARTODB);
            SqlParameter[] paras = new SqlParameter[5];
            paras[0] = new SqlParameter("@AutIntNo", this.autIntNo);
            paras[1] = new SqlParameter("@CrtName", this.crtName);
            paras[2] = new SqlParameter("@CrtRoomNo", this.crtRoomNo);
            paras[3] = new SqlParameter("@CDate", this.cDate);
            paras[4] = new SqlParameter("@NoOfDays", this.noOfDays);
            return db.ExecuteDataSet("CheckForAllocateCaseNo_WS", paras, out message);
        }

        // 2014-08-14 Jerry fixed action date
        //DateTime printActionDate = DateTime.Now;
        int delayHours = 2;
        private void SetServiceParameters()
        {
            //int delayHours = 0;
            if (ServiceParameters != null
                && ServiceParameters.ContainsKey("DelayActionDate_InHours")
                && !string.IsNullOrEmpty(ServiceParameters["DelayActionDate_InHours"]))
                delayHours = Convert.ToInt32(ServiceParameters["DelayActionDate_InHours"]);
            //this.printActionDate = DateTime.Now.AddHours(delayHours);
        }
    }
}
