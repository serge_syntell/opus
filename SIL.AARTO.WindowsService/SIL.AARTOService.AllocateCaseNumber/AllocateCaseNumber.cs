﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.AllocateCaseNumber
{
    partial class AllocateCaseNumber : ServiceHost
    {
        public AllocateCaseNumber()
            : base("", new Guid("6962C291-374B-4051-B16D-A62D0413AFAC"), new AllocateCaseNumberClientService())
        {
            InitializeComponent();
        }
    }
}
