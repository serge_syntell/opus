﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.AllocateCaseNumber
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.AllocateCaseNumber"
                ,
                DisplayName = "SIL.AARTOService.AllocateCaseNumber"
                ,
                Description = "SIL AARTOService AllocateCaseNumber"
            };

            ProgramRun.InitializeService(new AllocateCaseNumber(), serviceDescriptor, args);
        }
    }
}
