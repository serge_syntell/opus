﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Frame_eNatis_NTI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.Frame_eNatis_NTI"
                ,
                DisplayName = "SIL.AARTOService.Frame_eNatis_NTI"
                ,
                Description = "SIL AARTOService Frame_eNatis_NTI"
            };

            ProgramRun.InitializeService(new Frame_eNatis_NTI(), serviceDescriptor, args);
        }
    }
}
