﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Frame_eNatis_NTI
{
    partial class Frame_eNatis_NTI : ServiceHost
    {
        public Frame_eNatis_NTI()
            : base("", new Guid("03099B20-1F2C-4C9D-9C08-445F5BC1036B"), new Frame_eNatis_NTIClientService())
        {
            InitializeComponent();
        }
    }
}
