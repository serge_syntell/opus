﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using Stalberg.TMS;
using SIL.AARTOService.Resource;
using SIL.eNaTIS.DAL.Entities;
using SIL.eNaTIS.Client;
using SIL.eNaTIS.DAL.Services;
using SIL.eNaTIS.DAL.Data;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using System.Data;
using Stalberg.TMS.Data;
using System.Reflection;
using SIL.AARTO.BLL.CameraManagement;
using SIL.AARTO.DAL.Services;
using SIL.DMS.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Model;

namespace SIL.AARTOService.Frame_eNatis_NTI
{
    public enum ENatisQueueType
    {
        DefaultNull,
        ValidENatisData,
        ValidPersonData,
        OutstandingPersonData
    }

    public class Frame_eNatis_NTIClientService : ServiceDataProcessViaQueue
    {
        public Frame_eNatis_NTIClientService()
            : base("", "", new Guid("42B2D66E-68D1-4D92-9F19-F15C1B31E4D7"), ServiceQueueTypeList.Frame_eNatis_OUT)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            var ntiConnString = AARTOBase.GetConnectionString(ServiceConnectionNameList.SIL_NTI, ServiceConnectionTypeList.DB);
            ServiceUtility.InitializeNetTier(ntiConnString, "SIL.eNaTIS");
            AARTOBase.OnServiceStarting = () =>
            {
                if (noAogDB == null) noAogDB = new NoAOGDB(connAARTODB);
            };
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        AuthorityDetails authDetails = null;
        string connAARTODB;

        ENatisQueue eNatisQueue;
        ENatisQueueType eNatisQueueType = ENatisQueueType.DefaultNull;
        ENatisQueueService eNatisQueueService = new ENatisQueueService();
        eNaTISQueue eNatisService = new eNaTISQueue();
        Guid eNatisGuid;
        int autIntNo;
        string currentAutNo, lastAutNo;
        bool natisLast = true;
        string allowLookups, showDifferencesOnly;

        int expiryNoOfDays;
        string autCode;
        NoAOGDB noAogDB;

        //Heidi 2013-12-30 added for send back to DMS 
        readonly SIL.AARTO.DAL.Services.NoticeService noticeService = new SIL.AARTO.DAL.Services.NoticeService();
        readonly ChargeService chargeService = new ChargeService();
        readonly SIL.AARTO.DAL.Services.FrameService frameService = new SIL.AARTO.DAL.Services.FrameService();
        readonly DocumentErrorMessageService documentErrorMessageService = new DocumentErrorMessageService();
        readonly BatchDocumentService batchDocumentService = new BatchDocumentService();
        NoticeDB updateNotice = null;

        public override void InitialWork(ref QueueItem item)
        {
            if (item.Body != null && !string.IsNullOrWhiteSpace(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;

                    this.eNatisGuid = Guid.Parse(item.Body.ToString());
                    //this.currentAutCode = item.Group.Trim();
                    // eNatis only have AutNo, didn't have AutCode
                    this.currentAutNo = item.Group.Trim();
                    //rules.InitParameter(this.connAARTODB, this.currentAutCode);

                    if (this.lastAutNo != this.currentAutNo)
                    {
                        if (QueueItemValidation(ref item))
                        {
                            this.lastAutNo = this.currentAutNo;
                        }
                    }

                    FrameValidation(ref item);
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
            else
            {
                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.currentAutNo));
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                return;
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                try
                {
                    UpdateFrame(ref item);
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
                queueList[i] = item;
            }
            this.eNatisGuid = default(Guid);
            this.eNatisQueue = null;
            this.eNatisQueueType = ENatisQueueType.DefaultNull;
        }


        private bool QueueItemValidation(ref QueueItem item)
        {
            string msg;
            if (this.lastAutNo != this.currentAutNo)
            {
                this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutNo.Trim().Equals(this.currentAutNo, StringComparison.OrdinalIgnoreCase));
                if (this.authDetails == null || this.authDetails.AutIntNo <= 0)
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.currentAutNo));
                    return false;
                }

                rules.InitParameter(this.connAARTODB, this.authDetails.AutCode);

                this.autIntNo = this.authDetails.AutIntNo;
                string autName = this.authDetails.AutName;
                this.autCode = this.authDetails.AutCode.Trim();

                if (rules.Rule("0400").ARString == "Y")
                {
                    this.natisLast = true;
                    msg = ResourceHelper.GetResource("NatisSentAfterAdjudication");
                }
                else
                {
                    this.natisLast = false;
                    msg = ResourceHelper.GetResource("NatisSentBeforeVerification");
                }

                allowLookups = rules.Rule("0530").ARString;
                showDifferencesOnly = rules.Rule("0550").ARString;

                return true;
            }
            else
                return true;
        }

        private bool FrameValidation(ref QueueItem item)
        {
            if (item.IsSuccessful)
            {
                ENatisQueueQuery query = new ENatisQueueQuery();
                query.AppendEquals(ENatisQueueColumn.ENaTisGuid, this.eNatisGuid.ToString());
                this.eNatisQueue = eNatisQueueService.Find(query).FirstOrDefault();

                if (this.eNatisQueue == null)
                {
                    item.IsSuccessful = false;
                    return false;
                }

                eNatisQueueService.DeepLoad(this.eNatisQueue, false, DeepLoadType.IncludeChildren, typeof(RespTranState));

                int eNatisStatusID = 0;
                if (this.eNatisQueue.ENatisQueueStatusId.HasValue)
                    eNatisStatusID = this.eNatisQueue.ENatisQueueStatusId.Value;

                if (eNatisStatusID == (int)ENatisQueueStatusList.Failed || eNatisStatusID == (int)ENatisQueueStatusList.Success)
                    this.eNatisQueueType = ENatisQueueType.ValidENatisData;
                else if (eNatisStatusID == (int)ENatisQueueStatusList.PSuccess)
                    this.eNatisQueueType = ENatisQueueType.ValidPersonData;
                else
                {
                    // Jake 2013-07-12 added message when discard queue 
                    item.IsSuccessful = false;
                    item.Status = QueueItemStatus.Discard;
                    this.Logger.Info("Invalid eNatisQueueType " + this.eNatisQueueType, item.Body.ToString());
                    return false;
                }

                return true;
            }
            else
                return false;
        }

        private bool UpdateFrame(ref QueueItem item)
        {
            string msg = string.Empty;
            if (item.IsSuccessful)
            {
                FrameDB frameDB = new FrameDB(this.connAARTODB);
                updateNotice = new NoticeDB(this.connAARTODB); //Heidi 2013-12-30 added 
                int frameIntNo = 0;
                int natisErrorCount = 0; //Heidi 2013-12-30 added 
                int returnValue = 0;//Heidi 2013-12-30 added 
                if (this.eNatisQueueType == ENatisQueueType.ValidENatisData)
                {
                    // 2013-12-30, Heidi added natisErrorCount and returnValue params for error handling
                    frameIntNo = frameDB.UpdateFrameFromENATIS(this.eNatisQueue.RespDataRaw, AARTOBase.LastUser, this.eNatisQueue.ENaTisGuid, this.showDifferencesOnly, this.natisLast ? "Y" : "N", this.allowLookups, ref msg, "", this.eNatisQueue.RespTranStateIdSource.RespTranState, out natisErrorCount, out returnValue);

                    // 2013-12-30, Heidi added for error handling
                    if (returnValue < 0 || !string.IsNullOrWhiteSpace(msg))
                    {
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("UpdateFrameFailed", frameIntNo, returnValue) + (string.IsNullOrWhiteSpace(msg) ? "" : " " + msg), LogType.Error, ServiceOption.ContinueButQueueFail, item);
                        return false;
                    }
                    //Oscar 20120322 copy this from Natis_Out
                    FrameDetails frame = frameDB.GetFrameDetails(frameIntNo);
                    if (frame != null && ServiceUtility.IsNumeric(frame.FrameStatus))
                    {
                        int frameStatus = Convert.ToInt32(frame.FrameStatus);
                        QueueItemProcessor queProcessor = new QueueItemProcessor();

                        //Heidi 2013-12-30 added for send back to DMS need to check DMS imported S341 Document
                        NoticeDetails noticeDetails = updateNotice.GetNoticeDataByFrameIntNo(frameIntNo);
                        if (natisErrorCount > 0 && noticeDetails != null
                            && noticeDetails.NotStatisticsCode.Equals("H341", StringComparison.OrdinalIgnoreCase))
                        {
                            if (natisErrorCount < 5)
                            {
                                SendBackToDMS(noticeDetails.NotIntNo, frameIntNo);
                            }
                        }
                        else
                        {

                            FilmDetails film = new FilmDB(this.connAARTODB).GetFilmDetails(frame.FilmIntNo);
                            if (film != null && film.FilmType.Equals("H", StringComparison.OrdinalIgnoreCase)
                                && (frameStatus == 600 || frameStatus == 800)
                            )
                            {
                                string errMessage;
                                DateTime? not1stPostDate = null;
                                int notIntNo = UpdateNoticeForHandWritten_WS(frameIntNo, out not1stPostDate, out errMessage);
                                if (notIntNo > 0 && string.IsNullOrEmpty(errMessage))
                                {
                                    NoticeDB noticeDB = new NoticeDB(this.connAARTODB);
                                    NoticeDetails notice = noticeDB.GetNoticeDetails(notIntNo);
                                    string[] group = new string[3];
                                    group[0] = this.autCode.Trim();
                                    //Jerry 2013-03-07 change
                                    //group[1] = notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) ? "H" : "";
                                    group[1] = (notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) && !notice.IsVideo) ? "H" : "";
                                    group[2] = Convert.ToString(notice.NotCourtName);

                                    int noOfDaysToSummons = rules.Rule("NotPosted1stNoticeDate", "NotIssueSummonsDate").DtRNoOfDays;
                                    //Jerry 2013-03-06 add
                                    int daysPosted1stTo2ndNoticeDate = rules.Rule("NotPosted1stNoticeDate", "NotIssue2ndNoticeDate").DtRNoOfDays;
                                    int daysIssue2ndNoticeTo2ndPaymentDate = rules.Rule("NotIssue2ndNoticeDate", "Not2ndPaymentDate").DtRNoOfDays;
                                    int daysNotOffenceDateToNotPaymentDate = rules.Rule("NotOffenceDate", "NotPaymentDate").DtRNoOfDays;
                                   //2014-11-4 Tommi add
                                    DateRuleInfo dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotIssue2ndNoticeDate", "SMSExtractOn2ndNoticeDate");
                                    int noOfDaysToSMSExtractOn2ndNoticeDate = dateRule.ADRNoOfDays;
                                    //dls 20120424 - handwritten offences must be HWO
                                    //Oscar 20120412 changed group
                                    //string violationType = new FrameDB(this.connAARTODB).GetViolationType(frameIntNo);

                                    // Oscar 20130110 copy IBM printer from Natis_Out for HWO
                                    //var isIbmPrinter = rules.Rule("6209").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase);
                                    var violationType = new FrameDB(this.connAARTODB).GetViolationType(frameIntNo);

                                    //Jerry 2013-02-06 change
                                    //if (isIbmPrinter)
                                    //Jerry 2013-03-06 change it from && to ||
                                    if (notice.IsVideo || rules.Rule("2500").ARString == "Y")
                                    {
                                        queProcessor.Send(new QueueItem()
                                        {
                                            Body = notIntNo,
                                            Group = string.Format("{0}|{1}|{2}", this.autCode.Trim(), violationType, notice.IsVideo ? "VID" : "HWO"),
                                            QueueType = ServiceQueueTypeList.Frame_Generate1stNotice,
                                            LastUser = AARTOBase.LastUser
                                        });
                                    }
                                    // 2014-05-23, Oscar added checking
                                    else if (!notice.NotFilmType.Equals2("H")
                                        || !notice.NotStatisticsCode.Equals2("H341")
                                        || !notice.NotNewOffender.Equals2("Y"))
                                    {
                                        queProcessor.Send(new QueueItem()
                                        {
                                            Body = notIntNo,
                                            //Group = string.Format("{0}|{1}", this.autCode.Trim(), violationType),
                                            Group = string.Format("{0}|{1}", this.autCode.Trim(), "HWO"),
                                            //Jerry 2013-03-06 add action date
                                            ActDate = notice.NotOffenceDate.AddDays(daysNotOffenceDateToNotPaymentDate + 2),
                                            QueueType = ServiceQueueTypeList.Generate2ndNotice
                                        });

                                        //2014-11-4 Tommi add for SMS Extract notice on2ndNotice
                                        queProcessor.Send(new QueueItem()
                                        {
                                            Body = notIntNo,
                                            Group = this.autCode.Trim(),
                                           LastUser=AARTOBase.LastUser,
                                            ActDate = notice.NotOffenceDate.AddDays(daysNotOffenceDateToNotPaymentDate + 2 + noOfDaysToSMSExtractOn2ndNoticeDate),
                                            QueueType = ServiceQueueTypeList.SMSExtractOn2ndNotice
                                        });
                                    }

                                    //Jake 2014-07-22 added push SearchNameIDUpdate queue item for H notice
                                    //after natis data updated
                                    if (notice.NotFilmType.Equals2("H") || notice.NotStatisticsCode.Equals2("H341"))
                                    {
                                        queProcessor.Send(new QueueItem()
                                        {
                                            Body = notIntNo,
                                            Group = autCode,
                                            LastUser = AARTOBase.LastUser,
                                            QueueType = ServiceQueueTypeList.SearchNameIDUpdate
                                        }
                                            );
                                    }

                                    //Jerry 2013-02-06 change
                                    //Jerry 2013-03-06 delete if
                                    //if (!notice.IsVideo && rules.Rule("2500").ARString == "N")
                                    //{

                                    // Oscar 2013-06-17 added
                                    var isNoAog = noAogDB.IsNoAOG(notIntNo, NoAOGQueryType.Notice);

                                    queProcessor.Send(
                                        new QueueItem()
                                        {
                                            Body = notIntNo,
                                            //Group = this.autCode,
                                            // Oscar 20120315 changed the Group
                                            Group = string.Join("|", group),
                                            //Jerry 2013-03-06 change action date
                                            //ActDate = (not1stPostDate.HasValue ? not1stPostDate.Value : DateTime.Now).AddDays(noOfDaysToSummons),
                                            ActDate = isNoAog ? noAogDB.GetNoAOGGenerateSummonsActionDate(autIntNo, notice.NotOffenceDate) : (rules.Rule("2500").ARString == "Y" ? notice.NotOffenceDate.AddDays(noOfDaysToSummons) :
                                            notice.NotOffenceDate.AddDays(daysNotOffenceDateToNotPaymentDate + daysIssue2ndNoticeTo2ndPaymentDate + 4)),
                                            Priority = notice.NotOffenceDate.Subtract(DateTime.Now).Days,   // 2013-09-06, Oscar added
                                            //Jerry 2012-05-14 add LastUser
                                            LastUser = AARTOBase.LastUser,
                                            QueueType = ServiceQueueTypeList.GenerateSummons
                                        }
                                    );
                                    // 2013-03-27 Henry add
                                    if (rules.Rule("5050").ARString.Trim().ToUpper() == "Y")
                                    {
                                        queProcessor.Send(
                                            new QueueItem()
                                            {
                                                Body = notIntNo,
                                                Group = string.Join("|", group),
                                                ActDate = notice.NotOffenceDate.AddMonths(rules.Rule("5050").ARNumeric).AddDays(-5),
                                                Priority = notice.NotOffenceDate.Subtract(DateTime.Now).Days,   // 2013-09-06, Oscar added
                                                LastUser = AARTOBase.LastUser,
                                                QueueType = ServiceQueueTypeList.GenerateSummons
                                            }
                                        );
                                    }
                                    //}

                                    //Nick 20120329 add to push data washing queue
                                    string dwEnabled = rules.Rule("9060").ARString.Trim();
                                    if (dwEnabled == "Y")
                                    {
                                        string IDNum = null;
                                        bool needDataWash = false;
                                        int dwRecycle = rules.Rule("9080").ARNumeric;
                                        string notSendTo = notice.NotSendTo.Trim();
                                        switch (notSendTo)
                                        {
                                            case "D":
                                                DriverDB driverDB = new DriverDB(connAARTODB);
                                                DriverDetails drvDet = driverDB.GetDriverDetailsByNotice(notIntNo);
                                                if (drvDet != null) IDNum = (drvDet.DrvIDNumber + "").Trim();
                                                break;
                                            case "O":
                                                OwnerDB ownerDB = new OwnerDB(connAARTODB);
                                                OwnerDetails ownDet = ownerDB.GetOwnerDetailsByNotice(notIntNo);
                                                if (ownDet != null) IDNum = (ownDet.OwnIDNumber + "").Trim();
                                                break;
                                            case "P":
                                                ProxyDB proxyDB = new ProxyDB(connAARTODB);
                                                ProxyDetails pxyDet = proxyDB.GetProxyDetailsByNotice(notIntNo);
                                                if (pxyDet != null) IDNum = (pxyDet.PrxIDNumber + "").Trim();
                                                break;
                                            default:
                                                break;
                                        }
                                        if (!String.IsNullOrEmpty(IDNum))
                                        {
                                            MI5DB mi5 = new MI5DB(connAARTODB);
                                            DataSet personDet = mi5.GetPersonaDetailsByID(IDNum, AARTOBase.LastUser);
                                            if (personDet != null && personDet.Tables.Count > 0 && personDet.Tables[0].Rows.Count > 0)
                                            {
                                                string strLastSentDWDate = personDet.Tables[0].Rows[0]["MIPSentToDataWasherDate"] + "";
                                                string strLastDWDate = personDet.Tables[0].Rows[0]["MIPLastWashedDate"] + "";
                                                DateTime? lastSentDWDate = strLastSentDWDate == "" ? null : (DateTime?)Convert.ToDateTime(strLastSentDWDate);
                                                DateTime? lastDWDate = strLastDWDate == "" ? null : (DateTime?)Convert.ToDateTime(strLastDWDate);

                                                if ((!lastSentDWDate.HasValue && !lastDWDate.HasValue) ||
                                                    (lastSentDWDate.HasValue && !lastDWDate.HasValue && Convert.ToDateTime(lastSentDWDate).AddMonths(dwRecycle) < DateTime.Now))
                                                {
                                                    needDataWash = true;
                                                }
                                                else
                                                {
                                                    lastSentDWDate = lastSentDWDate.HasValue ? lastSentDWDate : DateTime.Parse("1900-01-01");
                                                    if (lastSentDWDate < lastDWDate && Convert.ToDateTime(lastDWDate).AddMonths(dwRecycle) < DateTime.Now)
                                                    {
                                                        needDataWash = true;
                                                    }
                                                }
                                            }

                                            if (needDataWash)
                                            {
                                                int daysDWToSummon = rules.Rule("DataWashDate", "NotIssueSummonDate").DtRNoOfDays;
                                                queProcessor.Send(
                                                    new QueueItem()
                                                    {
                                                        Body = notIntNo,
                                                        Group = this.autCode.Trim(),
                                                        ActDate = (not1stPostDate.HasValue ? not1stPostDate.Value : DateTime.Now).AddDays(noOfDaysToSummons - daysDWToSummon),
                                                        QueueType = ServiceQueueTypeList.DataWashingExtractor
                                                    }
                                                );
                                            }
                                        }
                                    }

                                }
                            }
                            else if (frameStatus == 800)
                            {
                                if (this.expiryNoOfDays == 0)
                                {
                                    this.expiryNoOfDays = rules.Rule("NotOffenceDate", "NotIssue1stNoticeDate").DtRNoOfDays;
                                }

                                string violationType = new FrameDB(this.connAARTODB).GetViolationType(frame.FrameIntNo);
                                queProcessor.Send(
                                    new QueueItem()
                                    {
                                        Body = frameIntNo,
                                        Group = string.Format("{0}|{1}", this.autCode.Trim(), violationType),
                                        Priority = this.expiryNoOfDays - ((TimeSpan)(DateTime.Now - frame.OffenceDate)).Days,
                                        QueueType = ServiceQueueTypeList.Frame_Generate1stNotice
                                    }
                                );
                            }

                            //Jerry 2013-04-10 add for secondary offence for Vehicle license expired
                            if (!film.FilmType.Equals("H", StringComparison.OrdinalIgnoreCase))
                            {
                                if (frame.ParentFrameIntNo == null && rules.Rule("0590").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase)
                                    && frame.FrameVehicleLicenceExpiry != null && frame.FrameVehicleLicenceExpiry < frame.OffenceDate)
                                {
                                    SIL.AARTO.DAL.Entities.SecondaryOffence secondaryOffenceEntity = new SIL.AARTO.DAL.Services.SecondaryOffenceService().GetBySeOfCode("78011");//78011: Vehicle license expired code
                                    if (secondaryOffenceEntity != null)
                                    {
                                        FrameManager.CreateFrameAboutSecondaryOffence(film.FilmNo, frame.FrameNo, secondaryOffenceEntity.SeOfCode, AARTOBase.LastUser);
                                    }
                                }
                            }
                        }//sendBackToDms

                    }//frame != null

                }
                else if (this.eNatisQueueType == ENatisQueueType.ValidPersonData)
                    frameIntNo = frameDB.UpdateFrameFromENATISPerson(this.eNatisQueue.RespDataRaw, AARTOBase.LastUser, this.eNatisQueue.ENaTisGuid, ref msg);
                else
                {
                    // Jake 2013-07-12 add message when discard queue 
                    item.IsSuccessful = false;
                    item.Status = QueueItemStatus.Discard;
                    this.Logger.Info("Invalid eNatisQueueType " + this.eNatisQueueType, item.Body.ToString());
                    return false;
                }

                if (frameIntNo <= 0)
                {
                    AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("ImportNatisDetailErrorOnFrameGuid"), this.eNatisGuid, msg), true);
                    return false;
                }
                else
                {
                    eNatisService.DeleteNaTISQueue(this.eNatisQueue);
                    return true;
                }
            }
            else
                return false;
        }

        private void SendBackToDMS(int noticeIntNo, int frameIntNo)
        {
            string frameNaTISErrorFieldList = "";
            string frameNaTISIndicator = "";
            //Heidi 2013-10-21 changed for send back to DMS need to check DMS imported S341 Document
            //NoticeDetails noticeDetails = updateNotice.GetNoticeDataByFrameIntNo(frameIntNo);
            //int noticeIntNo = 0;
            //if (noticeDetails != null)
            //{
            //    noticeIntNo = noticeDetails.NotIntNo;
            //}
            //SIL.AARTO.DAL.Entities.Notice noticeEntity = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(noticeIntNo);
            // 2013-12-24, Oscar promoted dbService to top
            var noticeEntity = this.noticeService.GetByNotIntNo(noticeIntNo);
            if (noticeEntity != null)
            {
                if (noticeEntity.NoticeStatus < 900 && noticeEntity.NoticeStatus != 410)
                {
                    noticeEntity.NoticeStatus = (int)ChargeStatusList.DMSDocumentCorrection;
                    noticeEntity.LastUser = AARTOBase.LastUser;
                    noticeEntity.IsHwoCorrection = true;
                    //new SIL.AARTO.DAL.Services.NoticeService().Update(noticeEntity);
                    // 2013-12-24, Oscar promoted dbService to top
                    this.noticeService.Update(noticeEntity);
                }
                //SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.Charge> chargeEntityList = new SIL.AARTO.DAL.Services.ChargeService().GetByNotIntNo(noticeEntity.NotIntNo);
                // 2013-12-24, Oscar promoted dbService to top
                var chargeEntityList = this.chargeService.GetByNotIntNo(noticeEntity.NotIntNo);
                foreach (var charge in chargeEntityList)
                {
                    if (charge.ChargeStatus < 900 && charge.ChargeStatus != 410)
                    {
                        charge.ChargeStatus = (int)ChargeStatusList.DMSDocumentCorrection;
                        charge.LastUser = AARTOBase.LastUser;
                        //new SIL.AARTO.DAL.Services.ChargeService().Update(charge);
                        // 2013-12-24, Oscar promoted dbService to top
                        this.chargeService.Update(charge);
                    }
                }

                //SIL.AARTO.DAL.Entities.Frame frameEntity = new SIL.AARTO.DAL.Services.FrameService().GetByFrameIntNo(frameIntNo);
                // 2013-12-24, Oscar promoted dbService to top
                var frameEntity = this.frameService.GetByFrameIntNo(frameIntNo);
                if (frameEntity != null)
                {
                    frameNaTISErrorFieldList = string.IsNullOrEmpty(frameEntity.FrameNaTisErrorFieldList) ? "" : frameEntity.FrameNaTisErrorFieldList;
                    frameNaTISIndicator = string.IsNullOrEmpty(frameEntity.FrameNatisIndicator) ? "" : frameEntity.FrameNatisIndicator;
                    frameEntity.FrameStatus = (int)FrameStatusList.NoNatisDataS341HW;
                    //new SIL.AARTO.DAL.Services.FrameService().Update(frameEntity);
                    // 2013-12-24, Oscar promoted dbService to top
                    this.frameService.Update(frameEntity);
                }
                if (!string.IsNullOrEmpty(frameNaTISIndicator) && frameNaTISIndicator != "Y")
                {
                    if (frameNaTISErrorFieldList.Trim() == "003")
                    {
                        frameNaTISErrorFieldList = frameNaTISErrorFieldList + ResourceHelper.GetResource("NatisErrorMsg003");
                    }
                    else if (frameNaTISErrorFieldList.Trim() == "060")
                    {
                        frameNaTISErrorFieldList = frameNaTISErrorFieldList + ResourceHelper.GetResource("NatisErrorMsg060");
                    }
                    else if (frameNaTISIndicator.Trim() == "!")
                    {
                        frameNaTISIndicator = " ";
                    }
                    else
                    {
                        frameNaTISIndicator = " ";
                        frameNaTISErrorFieldList = frameNaTISErrorFieldList + ResourceHelper.GetResource("NatisErrorMsgUnknown");
                    }
                }
                string errorMsg = string.Format(ResourceHelper.GetResource("NatisErrorMsg"), frameNaTISIndicator, frameNaTISErrorFieldList);
                //update DMS 
                UpdateDMS(noticeEntity.NotFilmNo.Trim(), noticeEntity.NotFrameNo.Trim(), errorMsg);
            }
            else
            {
                this.Logger.Info(string.Format(ResourceHelper.GetResource("NoNoticeByFrameIntNo"), AARTOBase.LastUser, frameIntNo));
            }
        }

        private void UpdateDMS(string batchID, string batchDocID, string reasonMsg)
        {
            //Insert err msg data into DMS
            SIL.DMS.DAL.Entities.DocumentErrorMessage docErrMsg = new DMS.DAL.Entities.DocumentErrorMessage();
            docErrMsg.BatchId = batchID;
            docErrMsg.BaDoId = batchDocID;
            // Paole 20121031  for display the category of error message on DMS QC page, temporary proposal， not checked by DEV
            docErrMsg.DoErMeCode = "HW Correction reason";
            docErrMsg.DoErMessage = reasonMsg;
            docErrMsg.DoErMeDateCreated = DateTime.Now;
            docErrMsg.LastUser = AARTOBase.LastUser;
            //new SIL.DMS.DAL.Services.DocumentErrorMessageService().Save(docErrMsg);
            // 2013-12-24, Oscar promoted dbService to top
            this.documentErrorMessageService.Save(docErrMsg);

            //update document table
            //SIL.DMS.DAL.Entities.BatchDocument batchDoc = new SIL.DMS.DAL.Services.BatchDocumentService().GetByBaDoId(batchDocID);
            // 2013-12-24, Oscar promoted dbService to top
            var batchDoc = this.batchDocumentService.GetByBaDoId(batchDocID);
            batchDoc.BaDoStId = (int)SIL.DMS.DAL.Entities.BatchDocumentStatusList.DocumentReturnedToQC;
            batchDoc.IsBackFromAarto = true;
            batchDoc.LastUser = AARTOBase.LastUser; //Heidi 2013-10-21 added
            //new SIL.DMS.DAL.Services.BatchDocumentService().Update(batchDoc);
            // 2013-12-24, Oscar promoted dbService to top
            this.batchDocumentService.Update(batchDoc);
        }

        private int UpdateNoticeForHandWritten_WS(int frameIntNo, out DateTime? not1stPostDate, out string message)
        {
            not1stPostDate = DateTime.Now;
            DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));
            SqlParameter[] paras = new SqlParameter[6];
            paras[0] = new SqlParameter("@AutIntNo", this.autIntNo);
            paras[1] = new SqlParameter("@FrameIntNo", frameIntNo);
            paras[2] = new SqlParameter("@StatusNotice", 900);
            paras[3] = new SqlParameter("@NatisLast", this.natisLast ? 'Y' : 'N');
            paras[4] = new SqlParameter("@LastUser", AARTOBase.LastUser);
            paras[5] = new SqlParameter("@NotPosted1stNoticeDate", not1stPostDate) { Direction = ParameterDirection.InputOutput };
            object obj = db.ExecuteScalar("UpdateNoticeForHandWritten_WS", paras, out message);
            int result;
            if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(message))
                result = Convert.ToInt32(obj);
            else
                result = 0;
            if (paras[5].Value != null)
                not1stPostDate = Convert.ToDateTime(paras[5].Value);
            else
                not1stPostDate = null;
            return result;
        }

    }
}