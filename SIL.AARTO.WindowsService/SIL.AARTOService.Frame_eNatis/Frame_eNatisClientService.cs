﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTOService.Library;
using SIL.QueueLibrary;
using Stalberg.TMS;
using System.Data;
using SIL.AARTOService.Resource;
using SIL.eNaTIS.DAL.Entities;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using SIL.eNaTIS.Client;

namespace SIL.AARTOService.Frame_eNatis
{
    public class Frame_eNatisClientService : ServiceDataProcessViaQueue
    {
        public Frame_eNatisClientService()
            //2014-01-08 Heidi changed for Should not be the ServiceQueueTypeList.Frame_eNatis instead of ServiceQueueTypeList.Frame_Natis(bontq841)
            //: base("", "", new Guid("9963269F-B576-4365-8014-4991511F9C8C"), ServiceQueueTypeList.Frame_eNatis)
            : base("", "", new Guid("9963269F-B576-4365-8014-4991511F9C8C"), ServiceQueueTypeList.Frame_Natis)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            this.connAARTODB = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            var ntiConnString = AARTOBase.GetConnectionString(ServiceConnectionNameList.SIL_NTI, ServiceConnectionTypeList.DB);
            ServiceUtility.InitializeNetTier(ntiConnString, "SIL.eNaTIS");
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        AuthorityDetails authDetails = null;
        SIL.eNaTIS.DAL.Entities.TList<Frame> sendFrameList = new eNaTIS.DAL.Entities.TList<Frame>();
        SIL.eNaTIS.DAL.Entities.TList<Frame> validFrameList = new eNaTIS.DAL.Entities.TList<Frame>();

        string connAARTODB;

        string currentAutCode, lastAutCode;
        int frameIntNo, autIntNo;
        bool batchIsFinished = true, authorityIsFinished = true;
        bool natisLast = true;
        //int noOfRecords = 600;
        //string showDifferencesOnly = "Y";

        public override void InitialWork(ref QueueItem item)
        {
            if (item.Body != null && !string.IsNullOrWhiteSpace(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.PostPone;

                    if (ServiceUtility.IsNumeric(item.Body))
                        this.frameIntNo = Convert.ToInt32(item.Body);

                    this.currentAutCode = item.Group.Trim();

                    rules.InitParameter(this.connAARTODB, this.currentAutCode);

                    if (this.lastAutCode != this.currentAutCode)
                    {
                        if (this.batchIsFinished)
                        {
                            if (QueueItemValidation(ref item))
                            {
                                this.lastAutCode = this.currentAutCode;

                                this.sendFrameList.Clear();
                                this.validFrameList.Clear();

                                AddSendFrame(ref item);

                                this.batchIsFinished = false;
                                this.authorityIsFinished = false;
                            }
                        }
                        else
                        {
                            this.authorityIsFinished = true;
                            //item.Status = QueueItemStatus.Retry;
                            //AARTOBase.SkipReceivingData = true;
                            AARTOBase.SkipReceivingQueueData(ref item);
                        }
                    }
                    else
                    {
                        if (this.batchIsFinished)
                        {
                            this.sendFrameList.Clear();
                            this.validFrameList.Clear();
                        }

                        AddSendFrame(ref item);

                        this.batchIsFinished = false;
                        this.authorityIsFinished = false;
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
            else
            {
                //AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.currentAutCode));
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                return;
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            if (UpdateSendFrames())
            {
                this.batchIsFinished = true;
            }
        }


        private bool QueueItemValidation(ref QueueItem item)
        {
            string msg;
            int statusLoaded = 0;
            int statusSent = 0;
            if (this.lastAutCode != this.currentAutCode)
            {
                this.authDetails = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim().Equals(this.currentAutCode, StringComparison.OrdinalIgnoreCase));
                if (this.authDetails == null || this.authDetails.AutIntNo <= 0)
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.currentAutCode));
                    return false;
                }

                this.autIntNo = this.authDetails.AutIntNo;
                string autName = this.authDetails.AutName;

                // check Realtime Mode
                if (rules.Rule("0570").ARString != "R")
                {
                    AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("AuthorityRuleStringNotEqualsR", autName, this.frameIntNo), true);
                    return false;
                }

                this.Logger.Info(string.Format(ResourceHelper.GetResource("eNatisRealTimeMode"), autName));

                if (rules.Rule("0400").ARString == "Y")
                {
                    this.natisLast = true;
                    msg = ResourceHelper.GetResource("NatisSentAfterAdjudication");
                    statusLoaded = 710 - 1;
                    statusSent = 730;
                }
                else
                {
                    this.natisLast = false;
                    msg = ResourceHelper.GetResource("NatisSentBeforeVerification");
                    statusLoaded = 5;
                    statusSent = 100;
                }

                this.Logger.Info(string.Format(ResourceHelper.GetResource("GetNatisFilesForAuthority"), msg, autName));

                //this.noOfRecords = rules.Rule("1510").ARNumeric;
                //this.showDifferencesOnly = rules.Rule("0550").ARString;

                //DataSet natisAuthDS = AARTOBase.GetNatisAuthorities(this.connAARTODB, statusLoaded, statusSent, "Frame", this.autIntNo, out msg);
                //if (natisAuthDS != null && natisAuthDS.Tables.Count > 0 && natisAuthDS.Tables[0].Rows.Count > 0)
                //{
                return true;
                //}
                //else
                //{
                //    item.IsSuccessful = false;
                //    item.Status = QueueItemStatus.UnKnown;
                //    this.Logger.Warning(string.Format(ResourceHelper.GetResource("FailedGetNatisAuthorities"), autName, this.frameIntNo, msg));
                //    return false;
                //}
            }
            else
                return true;
        }

        private void AddSendFrame(ref QueueItem item)
        {
            if (!item.IsSuccessful) return;

            string msg;
            int frameStatus = 100;
            int statusSend = 0;

            if (this.natisLast)
            {
                frameStatus = 730;
                statusSend = 735;
            }
            else
            {
                frameStatus = 100;
                statusSend = 110;
            }

            Frame frame = GetNaTISSendData_WS(this.autIntNo, this.frameIntNo, AARTOBase.LastUser, frameStatus, statusSend, out msg);
            if (frame != null)
            {
                //jake 2013-07-12 added message when discard queue 
                int flag = (int)((object[])frame.Tag)[0];
                if (flag == 3)
                    AARTOBase.ErrorProcessing(ref item, "Frame has been sent to eNatis or dead", false, QueueItemStatus.Discard);
                else
                {
                    this.sendFrameList.Add(frame);
                    if (flag == 1)
                        this.validFrameList.Add(frame);
                    else if (flag == 0)
                    {
                        //AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("IneligibleDataForBeingSentOutToNaits"), this.authDetails.AutName, this.frameIntNo));
                        //Oscar 20120405 change to Info
                        item.IsSuccessful = false;
                        item.Status = QueueItemStatus.Discard;
                        this.Logger.Info(ResourceHelper.GetResource("IneligibleDataForBeingSentOutToNaits", this.authDetails.AutName, this.frameIntNo));
                    }
                    else if (flag == 4)
                    {
                        //dls 2012-09-01-need to keep rows where FilmLockUser is not NULL and FilmLoadStatus != 1000 on the Q to try again later
                        item.IsSuccessful = false;
                        item.Status = QueueItemStatus.UnKnown;
                        this.Logger.Info(ResourceHelper.GetResource("IneligibleDataForBeingSentOutToNaits_TryAgain", this.authDetails.AutName, this.frameIntNo));
                    }
                    else
                    {
                        //jake 2013-07-12 added message when discard queue 
                        //item.Status = QueueItemStatus.Discard;
                        //this.Logger.Info("Invalid frame data and this frame will throw into the basket frameIntNo:" + this.frameIntNo);

                        // 2014-02-07, Oscar has moved message into resource file.
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("InvalidFrameDataAndWillBeThrownIntoBasket", this.frameIntNo),
                            LogType.Info, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(msg))
                    item.Status = QueueItemStatus.Discard;
                else
                {
                    AARTOBase.ErrorProcessing(ref item, string.Format(ResourceHelper.GetResource("GetNatisSendDataHasError"), this.authDetails.AutName, this.frameIntNo, msg));
                }
            }

        }

        private bool UpdateSendFrames()
        {
            if (this.sendFrameList.Count <= 0) return true;

            DateTime? offenceDate = null;
            int? noOfFrames = null;
            int frameStatus = 100;
            int statusSend = 0;
            string msg;

            if (this.natisLast)
            {
                frameStatus = 730;
                statusSend = 735;
            }
            else
            {
                frameStatus = 100;
                statusSend = 110;
            }

            if (this.validFrameList.Count > 0)
            {
                offenceDate = this.validFrameList.Min(p => p.OffenceDate);
                noOfFrames = this.validFrameList.Count;
            }

            int flag;
            foreach (Frame frame in this.sendFrameList)
            {
                flag = (int)((object[])frame.Tag)[0];
                FrameUpdateAfterNatisSend_WS(this.autIntNo, frame.FrameId, frameStatus, flag, AARTOBase.LastUser, (byte[])((object[])frame.Tag)[1], out msg);
                if (!string.IsNullOrEmpty(msg))
                {
                    //AARTOBase.StopService();
                    // Oscar 2013-04-10 changed
                    AARTOBase.LogProcessing(msg, LogType.Error, ServiceOption.BreakAndShutdown);

                    return false;
                }
            }

            FrameDB frameDB = new FrameDB(this.connAARTODB);
            eNaTISFrame eNatisDB = new eNaTISFrame();
            if (this.validFrameList.Count > 0)
            {
                foreach (Frame frame in this.validFrameList)
                {
                    if (frameDB.UpdateFrameGuid(frame.FrameId, statusSend, AARTOBase.LastUser, frame.ENaTisGuid) < 1)
                    {
                        this.Logger.Error(string.Format(ResourceHelper.GetResource("GetFrameNatisDataUnableToUpdateFrameStatusAndGuid"), frame.FrameId));
                        AARTOBase.StopService();
                        return false;
                    }
                    else
                    {
                        frame.FrameId = 0;
                        frame.Tag = null;
                        frame.EntityState = SIL.eNaTIS.DAL.Entities.EntityState.Added;
                    }
                }
                eNatisDB.SaveeNaTISFrame(this.validFrameList);
            }

            this.sendFrameList.Clear();
            this.validFrameList.Clear();

            return true;
        }

        private Frame GetNaTISSendData_WS(int autIntNo, int frameIntNo, string lastUser, int status, int statusSend, out string message)
        {
            DBHelper db = new DBHelper(this.connAARTODB);
            SqlParameter[] paras = new SqlParameter[4];
            paras[0] = new SqlParameter("@AutIntNo", autIntNo);
            paras[1] = new SqlParameter("@FrameIntNo", frameIntNo);
            paras[2] = new SqlParameter("@LastUser", lastUser);
            paras[3] = new SqlParameter("@Status", status);
            //paras[5] = new SqlParameter("@NoOFRecords", noOfRecords);
            //paras[4] = new SqlParameter("@DtLastRun", dtLastRun);
            DataSet result = db.ExecuteDataSet("FrameSendNaTIS_WS", paras, out message);
            Frame frame = null;
            if (result != null && result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0)
            {
                DataRow dr = result.Tables[0].Rows[0];
                frame = new Frame();
                frame.AutNo = this.authDetails.AutNo;
                frame.IdNo = DBHelper.GetDataRowValue<string>(dr, "IdNo");
                frame.OffenceDate = DBHelper.GetDataRowValue<DateTime>(dr, "OffenceDateDT");
                frame.RegNo = DBHelper.GetDataRowValue<string>(dr, "RegNo");
                frame.ENaTisGuid = Guid.NewGuid();
                frame.TmsFrameStatus = statusSend.ToString();
                frame.FrameId = DBHelper.GetDataRowValue<int>(dr, "FrameIntNo");    //should set 0 later for eNatis insert.
                object[] extendInfo = new object[2];
                extendInfo[0] = DBHelper.GetDataRowValue<int>(dr, "Flag");
                extendInfo[1] = (byte[])dr["RowVersion"];
                frame.Tag = extendInfo;   // save Flag, RowVersion, should set NULL later.
                //frame.EntityState = SIL.eNaTIS.DAL.Entities.EntityState.Added;
            }
            return frame;
        }

        private int FrameUpdateAfterNatisSend_WS(int autIntNo, int frameIntNo, int status, int flag, string lastUser, byte[] rowVersion, out string message)
        {
            DBHelper db = new DBHelper(this.connAARTODB);
            SqlParameter[] paras = new SqlParameter[7];
            paras[0] = new SqlParameter("@AutIntNo", autIntNo);
            paras[1] = new SqlParameter("@FrameIntNo", frameIntNo);
            paras[2] = new SqlParameter("@Status", status);
            paras[3] = new SqlParameter("@FrameSendNatisFileName", "");
            paras[4] = new SqlParameter("@Flag", flag);
            paras[5] = new SqlParameter("@LastUser", lastUser);
            paras[6] = new SqlParameter("@RowVersion", rowVersion);
            object obj = db.ExecuteScalar("FrameUpdateAfterNatisSend_WS", paras, out message);
            int result;
            if (ServiceUtility.IsNumeric(obj) && string.IsNullOrEmpty(message))
                result = Convert.ToInt32(obj);
            else
                result = 0;
            switch (result)
            {
                case -1:
                    message = ResourceHelper.GetResource("UpdatingNatisSendFiles");
                    break;
                case -2:
                    message = ResourceHelper.GetResource("UpdatingNatisBasket");
                    break;
            }
            return result;
        }

    }
}
