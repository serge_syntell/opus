﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Frame_eNatis
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.Frame_eNatis"
                ,
                DisplayName = "SIL.AARTOService.Frame_eNatis"
                ,
                Description = "SIL AARTOService Frame_eNatis"
            };

            ProgramRun.InitializeService(new Frame_eNatis(), serviceDescriptor, args);
        }
    }
}
