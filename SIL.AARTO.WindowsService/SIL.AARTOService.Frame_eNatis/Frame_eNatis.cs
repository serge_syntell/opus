﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.Frame_eNatis
{
    partial class Frame_eNatis : ServiceHost
    {
        public Frame_eNatis()
            : base("", new Guid("8FFAE01F-DCF2-4FEB-9EDF-9B6BC84DB9F0"), new Frame_eNatisClientService())
        {
            InitializeComponent();
        }
    }
}
