﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;
using SIL.AARTOService.DAL;
using System.Data.SqlClient;
using System.Configuration;
using SIL.QueueLibrary;
using SIL.AARTOService.Library;
using System.Data;
using System.Diagnostics;
using Stalberg.TMS;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Collections;
using SIL.AARTOService.Resource;
using Stalberg.TMS.Data;
using Stalberg.TMS_TPExInt.Components;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.DAL.Entities;


namespace SIL.AARTOService.ProcessNewOffenderNotices
{
    public class ProcessNewOffenderNoticesService : ServiceDataProcessViaQueue
    {
        public ProcessNewOffenderNoticesService()
            : base("", "", new Guid("2428885B-4A90-483C-8F8D-0C953BA2FF0D"), ServiceQueueTypeList.ProcessNewOffenderNotices)
        {
            this._serviceHelper = new AARTOServiceBase(this);
            AARTOBase.OnServiceStarting = () =>
            {
                this.pfnList.Clear();
                SetServiceParameters();
            };
        }

        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        AARTORules rules = AARTORules.GetSingleTon();
        NoticeDB noticeDB;
        string strAutCode = null;
        string connectStr;
        AuthorityDetails auth;

        List<int> pfnList = new List<int>();

        public override void PrepareWork()
        {
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
            noticeDB = new NoticeDB(connectStr);
        }

        public override void InitialWork(ref QueueItem item)
        {
            string body = string.Empty;
            if (item.Body != null)
                body = item.Body.ToString();
            if (!string.IsNullOrEmpty(body) && !string.IsNullOrEmpty(item.Group))
            {
                try
                {
                    item.IsSuccessful = true;
                    item.Status = QueueItemStatus.Discard;

                    int notIntNo;
                    if (!int.TryParse(body, out notIntNo))
                    {
                        AARTOBase.ErrorProcessing(ref item);
                        return;
                    }

                    NoticeDetails notice = noticeDB.GetNoticeDetails(notIntNo);
                    //if (notice.NoticeStatus >= 600 || notice.NoticeStatus < 255)
                    // 2014-02-21, Oscar changed 600->610 and 255->250 for COO under Dawn's permission.
                    if (notice.NoticeStatus >= 610 || notice.NoticeStatus < 250)
                    {
                        //Jerry 2012-05-18 change
                        //AARTOBase.ErrorProcessing(ref item);
                        Logger.Info(string.Format(ResourceHelper.GetResource("InvalidStatusOfNoticeInProcessNewOffenderNotice"), AARTOBase.LastUser, notice.NoticeStatus, notIntNo));
                        item.IsSuccessful = false;
                        item.Status = QueueItemStatus.Discard;
                        return;
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
            else
            {
                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", ""));
                return;
            }
        }

        string batchFilename;
        int x = 0;
        public sealed override void MainWork(ref List<QueueItem> queueList)
        {
            string msg = string.Empty;
            QueueItemProcessor queueProcessor = new QueueItemProcessor();

            for (int i = 0; i < queueList.Count; i++)
            {
                QueueItem item = queueList[i];
                if (!item.IsSuccessful)
                    continue;

                try
                {
                    //jerry 2012-03-05 add if(item.IsSuccessful)
                    if (item.IsSuccessful)
                    {
                        if (strAutCode != item.Group.Trim())
                        {
                            strAutCode = item.Group.Trim();
                            //jerry 2012-04-05 check group value
                            if (string.IsNullOrWhiteSpace(strAutCode))
                            {
                                AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("InvalidAuthority", this.strAutCode));
                                return;
                            }

                            auth = AARTOBase.AuthorityList.FirstOrDefault(p => p.AutCode.Trim() == strAutCode.Trim());
                            AARTORules.GetSingleTon().InitParameter(connectStr, strAutCode);
                        }

                        int notIntNo;
                        if (!int.TryParse(item.Body.ToString(), out notIntNo))
                        {
                            continue;
                        }

                        //bool failed = ProcessNewOffenderNotices(notIntNo, msg);
                        string printFileName = ProcessNewOffenderNotices(notIntNo, msg);

                        if (printFileName == null)
                        {
                            AARTOBase.ErrorProcessing(ref item, ResourceHelper.GetResource("NewOffenderNotices_ERROR", msg), false, QueueItemStatus.PostPone);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(printFileName))
                            {
                                // 2014-03-13, Oscar added
                                NoticeDB.SetPendingNewOffender(notIntNo, AARTOBase.LastUser, true);

                                QueueItem pushItem = new QueueItem();
                                pushItem.Body = notIntNo.ToString();
                                pushItem.Group = strAutCode;
                                pushItem.QueueType = ServiceQueueTypeList.SearchNameIDUpdate;
                                //Heidi 2014-10-29 add LastUser(bontq1649)
                                pushItem.LastUser = AARTOBase.LastUser;
                                queueProcessor.Send(pushItem);

                                // Oscar 20120427 changed the Group
                                NoticeDetails notice = noticeDB.GetNoticeDetails(notIntNo);
                                string[] group = new string[3];
                                group[0] = this.strAutCode.Trim();
                                //Jerry 2013-03-07 change
                                //group[1] = notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) ? "H" : "";
                                group[1] = (notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) && !notice.IsVideo) ? "H" : "";
                                group[2] = Convert.ToString(notice.NotCourtName);

                                // 2013-09-06, Oscar added
                                var noAogDB = new NoAOGDB(connectStr);
                                var isNoAog = noAogDB.IsNoAOG(notIntNo, NoAOGQueryType.Notice);

                                pushItem = new QueueItem();
                                pushItem.Body = notIntNo.ToString();
                                //pushItem.Group = strAutCode;
                                pushItem.ActDate = isNoAog ? noAogDB.GetNoAOGGenerateSummonsActionDate(auth.AutIntNo, notice.NotPosted1stNoticeDate) : notice.NotPosted1stNoticeDate.AddDays(rules.Rule("NotPosted1stNoticeDate", "NotIssueSummonsDate").DtRNoOfDays);  // 2013-09-06, Oscar added
                                pushItem.Group = string.Join("|", group);
                                pushItem.QueueType = ServiceQueueTypeList.GenerateSummons;
                                pushItem.Priority = notice.NotOffenceDate.Subtract(DateTime.Now).Days;   // 2013-09-06, Oscar added
                                //Jerry 2012-05-14 add LastUser
                                pushItem.LastUser = AARTOBase.LastUser;
                                queueProcessor.Send(pushItem);

                                //Henry 2013-03-27 add
                                AuthorityRuleInfo aR_5050 = new AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(auth.AutIntNo, "5050");
                                if (aR_5050.ARString.Trim().ToUpper() == "Y")
                                {
                                    pushItem = new QueueItem();
                                    pushItem.Body = notIntNo.ToString();
                                    pushItem.ActDate = notice.NotOffenceDate.AddMonths(aR_5050.ARNumeric).AddDays(-5);
                                    pushItem.Group = string.Join("|", group);
                                    pushItem.QueueType = ServiceQueueTypeList.GenerateSummons;
                                    pushItem.Priority = notice.NotOffenceDate.Subtract(DateTime.Now).Days;   // 2013-09-06, Oscar added
                                    pushItem.LastUser = AARTOBase.LastUser;
                                    queueProcessor.Send(pushItem);
                                }

                                // jerry 2012-03-02 push Print 2ndNotice service
                                string errMsg;
                                int pfnIntNo = AARTOBase.SavePrintFileName(PrintFileNameType.Notice, printFileName, auth.AutIntNo, notIntNo, out errMsg);
                                if (pfnIntNo <= 0)
                                {
                                    AARTOBase.ErrorProcessing(errMsg, true);
                                }
                                else if (!this.pfnList.Contains(pfnIntNo))
                                {
                                    this.pfnList.Add(pfnIntNo);

                                    //Oscar 20120412 changed group
                                    int frameIntNo = new NoticeDB(this.connectStr).GetFrameForNotice(notIntNo);
                                    string violationType = new FrameDB(this.connectStr).GetViolationType(frameIntNo);

                                    pushItem = new QueueItem();
                                    pushItem.Body = pfnIntNo.ToString();
                                    pushItem.Group = string.Format("{0}|{1}", this.strAutCode.Trim(), violationType);
                                    // 2014-08-14 Jerry fixed action date
                                    //pushItem.ActDate = this.printActionDate;
                                    pushItem.ActDate = DateTime.Now.AddHours(this.delayHours);
                                    //Heidi 2014-10-29 add LastUser(bontq1649)
                                    pushItem.LastUser = AARTOBase.LastUser;
                                    //Jerry 2012-05-14 add
                                    //2014-10-28 Heidi added "Contains("NAG")" condition for A NoAOG/S35 is ALWAYS a first notice (bontq1649)
                                    if (printFileName.ToUpper().Contains("1ST") || printFileName.ToUpper().Contains("NAG"))
                                    {
                                        pushItem.QueueType = ServiceQueueTypeList.Print1stNotice;
                                    }
                                    else
                                    {  //added by jacob 20120726 start
                                        //new authority rule(print using IBM info printer), 
                                        //prevent pushing Print2ndNotice queue.
                                        //not tested yet.
                                        //2014-01-08 Heidi added for 2nd Notice Direct Printing (5101)
                                        bool isIbmPrinter = rules.Rule("6209").ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase);
                                        if (isIbmPrinter)
                                        {
                                            //return;
                                            pushItem = new QueueItem();
                                            pushItem.Body = pfnIntNo.ToString();
                                            pushItem.Group = this.strAutCode.Trim() + "|" + (int)ReportConfigCodeList.SecondNoticeDirectPrinting;
                                            // 2014-08-14 Jerry fixed action date
                                            //pushItem.ActDate = this.printActionDate;
                                            pushItem.ActDate = DateTime.Now.AddHours(this.delayHours);
                                            pushItem.QueueType = ServiceQueueTypeList.PrintToFile;
                                            pushItem.LastUser = AARTOBase.LastUser;
                                        }
                                        else
                                        {
                                            pushItem.QueueType = ServiceQueueTypeList.Print2ndNotice;
                                        }
                                        //added by jacob 20120726 end 
                                    }
                                    queueProcessor.Send(pushItem);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.ErrorProcessing(ref item, ex);
                }
            }
        }

        /// <summary>
        /// Processes the new offender notices.
        /// jerry 2012=03-02 change this method, it's not correct
        /// Jake 2013-12-09 add new parameter Separate section 35 according to authority rule 6600
        /// </summary>
        public string ProcessNewOffenderNotices(int notIntNo, string errMessage)
        {
            //int failed = noticeDB.CreateNewOffenderNoticeBatch(auth.AutIntNo, ref errMessage,
            //    rules.Rule("4250").ARString,
            //    rules.Rule("NotIssue2ndNoticeDate", "Not2ndPaymentDate").DtRNoOfDays
            //    );
            DBHelper db = new DBHelper(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB));

			//Jake 2014-01-17 fixed  the sql parameter length from 5 to 6
            SqlParameter[] paras = new SqlParameter[6];
            paras[0] = new SqlParameter("@NotIntNo", notIntNo);
            paras[1] = new SqlParameter("@AutIntNo", auth.AutIntNo);
            paras[2] = new SqlParameter("@Print2ndNotices", rules.Rule("4250").ARString);
            paras[3] = new SqlParameter("@NumberDaysFor2ndPayment", rules.Rule("NotIssue2ndNoticeDate", "Not2ndPaymentDate").DtRNoOfDays);
            paras[4] = new SqlParameter("@FileName", string.Format("RNO_{0}", DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-ff")));
            // Jake 2013-12-09 added new parameter
            paras[5] = new SqlParameter("@SeparateS35", rules.Rule("6600").ARString);
            object obj = db.ExecuteScalar("NewOffenderCreateBatch_WS", paras, out errMessage);
            string printFileName = null;
            if (obj != null && string.IsNullOrEmpty(errMessage))
            {
                printFileName = obj.ToString();
                if (!string.IsNullOrEmpty(printFileName))
                {
                    Logger.Info(ResourceHelper.GetResource("CreatedBatchNotices", printFileName, auth.AutName));
                }

                return printFileName;
            }
            else if (obj == null || !string.IsNullOrEmpty(errMessage))
            {
                //Jerry 2012-05-18 change
                //Logger.Error(errMessage);
                AARTOBase.ErrorProcessing(errMessage);
                return null;
            }

            return printFileName;

        }

        // 2014-08-14 Jerry fixed action date
        //DateTime printActionDate = DateTime.Now;
        int delayHours = 2;
        private void SetServiceParameters()
        {
            //int delayHours = 0;
            if (ServiceParameters != null
                && ServiceParameters.ContainsKey("DelayActionDate_InHours")
                && !string.IsNullOrEmpty(ServiceParameters["DelayActionDate_InHours"]))
                delayHours = Convert.ToInt32(ServiceParameters["DelayActionDate_InHours"]);
            //this.printActionDate = DateTime.Now.AddHours(delayHours);
        }

    }
}

