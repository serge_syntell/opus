﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.ProcessNewOffenderNotices
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescriptor = new ServiceDescriptor
            {
                ServiceName = "SIL.AARTOService.ProcessNewOffenderNotices"
                ,
                DisplayName = "SIL.AARTOService.ProcessNewOffenderNotices"
                ,
                Description = "SIL AARTOService ProcessNewOffenderNotices"
            };

            ProgramRun.InitializeService(new ProcessNewOffenderNotices(), serviceDescriptor, args);
        }
    }
}
