﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.ProcessNewOffenderNotices
{
    partial class ProcessNewOffenderNotices : ServiceHost
    {
        public ProcessNewOffenderNotices()
            : base("", new Guid("902B336D-A119-45D5-BA5F-10F519C04A41"), new ProcessNewOffenderNoticesService())
        {
            InitializeComponent();
        }
    }
}
