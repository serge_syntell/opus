﻿using SIL.ServiceLibrary;

namespace SIL.AARTOService.WithdrawSec56WithOfficerError
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ProgramRun.InitializeService(new WithdrawSec56WithOfficerError(),
                new ServiceDescriptor
                {
                    ServiceName = "SIL.AARTOService.WithdrawSec56WithOfficerError",
                    DisplayName = "SIL.AARTOService.WithdrawSec56WithOfficerError",
                    Description = "SIL.AARTOService.WithdrawSec56WithOfficerError"
                }, args);
        }
    }
}
