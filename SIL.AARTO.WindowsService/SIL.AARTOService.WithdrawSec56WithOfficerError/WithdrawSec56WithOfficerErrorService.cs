﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTOService.Library;
using SIL.AARTOService.Resource;
using SIL.QueueLibrary;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Globalization;

namespace SIL.AARTOService.WithdrawSec56WithOfficerError
{
    class WithdrawSec56WithOfficerErrorService : ServiceDataProcessViaQueue
    {
        public WithdrawSec56WithOfficerErrorService()
            : base("", "", new Guid("9A28F158-FEF3-4611-BB3C-29DBB17F9CEF"), ServiceQueueTypeList.WithdrawSec56WithOfficerError)
        {
            _serviceHelper = new AARTOServiceBase(this,false);
           AARTOBase.OnServiceStarting = OnServiceStarting;

            this.lastUser = AARTOBase.LastUser;
            ServiceUtility.InitializeNetTier(AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB), "SIL.AARTO");
            connectStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);
        }

        AARTOServiceBase AARTOBase
        {
            get { return (AARTOServiceBase)_serviceHelper; }
        }

        #region variables
        readonly string lastUser;
        string DropOffDirectory = string.Empty;
        string connectStr;
        int notIntNo;
        QueueItem item;
        Notice notice;
        int origiNoticeStatus;
        NoticeService noticeService = new NoticeService();
        NoticeSummonsService noticeSummonsService = new NoticeSummonsService();
        ChargeService chargeService = new ChargeService();
        SumChargeService sumChargeService = new SumChargeService();
        SummonsService summmonsService = new SummonsService();
        string fileName;
        Stalberg.TMS.NoticeDB noticedb;
        string exportFullPath = "";
        bool isCreatedNewFile = false;
        string AR_5610 = "";
        int noDaysForWithDrawnDate;
        AuthorityRuleInfo authorityRuleInfo = new AuthorityRuleInfo();
        DateRuleInfo DR_sumCourtDate_WithDrawnDate = new DateRuleInfo();
        #endregion

        #region Initial & main work

        public override void InitialWork(ref QueueItem item)
        {
            item.IsSuccessful = true;
            this.item = item;
            try
            {
                if (!int.TryParse(Convert.ToString(item.Body), out this.notIntNo)
                   || this.notIntNo <= 0)
                {
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("InvalidQueueBody", item.Body),
                        LogType.Error, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                    return;
                }
                QueueItemValidation();
               
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex, LogType.Error, ServiceOption.ContinueButQueueFail, item);
            }
        }

        public override void MainWork(ref List<QueueItem> queueList)
        {
            noticedb = new Stalberg.TMS.NoticeDB(connectStr);
            //foreach (var _item in queueList.Where(q => q.IsSuccessful))
            //{
                //using (TransactionScope scope = ServiceUtility.CreateTransactionScope())
                //{
                // implement deadlock handling
            AARTOBase.InternalTransaction(queueList, null,
             (item, index) =>
                {
                    try
                    {
                        if (this.notice != null)
                        {
                            if (this.notice.NoticeStatus == 581)
                            {
                                //UpdateNoticeAndChargeStatus();
                               int returnValue = noticedb.UpdateStatusForAutoWithdrawS56WithOfficerErrors(this.notice.NotIntNo, this.lastUser);
                               if (returnValue <=0)
                               {
                                   AARTOBase.LogProcessing(ResourceHelper.GetResource("UpdateStatusForAutoWithdrawS56WithOfficerErrorsFailed", notice.NotIntNo), LogType.Error, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.UnKnown);
                                   return false;
                               }

                                DataTable dt = noticedb.GetS56WithOfficerErrorWithdrawnData(notice.NotIntNo);
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    //update by Rachel 20140812 for 5337
                                  //string officerErrorDescr = "\"" + dt.Rows[0]["NotTicketNo"].ToString() + "\"" + "," +
                                  //                      "\"" + dt.Rows[0]["NotOffenceDate"].ToString() + "\"" + "," +
                                  //                      "\"" + dt.Rows[0]["SumCourtDate"].ToString() + "\"" + "," +
                                  //                      "\"" + dt.Rows[0]["SChFineAmount"].ToString() + "\"" + "," +
                                  //                      "\"" + dt.Rows[0]["SChRevAmount"].ToString() + "\"" + "," +
                                  //                      "\"" + dt.Rows[0]["NotOfficerSName"].ToString().Replace("\"","\"\"") + "\"" + "," +
                                  //                      "\"" + dt.Rows[0]["NotOfficerNo"].ToString().Replace("\"", "\"\"") + "\"" + "," +
                                  //                      "\"" + dt.Rows[0]["OfficerErrorDescr"].ToString().Replace("\"", "\"\"") + "\"";
                                  string officerErrorDescr = "\"" + dt.Rows[0]["NotTicketNo"].ToString() + "\"" + "," +
                                                     "\"" + dt.Rows[0]["NotOffenceDate"].ToString() + "\"" + "," +
                                                     "\"" + dt.Rows[0]["SumCourtDate"].ToString() + "\"" + "," +
                                                     "\"" + dt.Rows[0]["SChFineAmount"].ToString().ToString(CultureInfo.InvariantCulture) + "\"" + "," +
                                                     "\"" + dt.Rows[0]["SChRevAmount"].ToString().ToString(CultureInfo.InvariantCulture) + "\"" + "," +
                                                     "\"" + dt.Rows[0]["NotOfficerSName"].ToString().Replace("\"", "\"\"") + "\"" + "," +
                                                     "\"" + dt.Rows[0]["NotOfficerNo"].ToString().Replace("\"", "\"\"") + "\"" + "," +
                                                     "\"" + dt.Rows[0]["OfficerErrorDescr"].ToString().Replace("\"", "\"\"") + "\"";
                                    //end update by Rachel 20140812 for 5337
                                    exportFullPath = this.DropOffDirectory +"\\"+ this.fileName;
                                    if (File.Exists(exportFullPath))
                                    {
                                        using (FileStream fs = new FileStream(exportFullPath, FileMode.Append, FileAccess.Write))
                                        {
                                            using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.ASCII))
                                            {
                                                sw.WriteLine(officerErrorDescr);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string title = ResourceHelper.GetResource("OfficerErrorsWithdrawnAfterCourtDateTitle");
                                        using (FileStream fs = new FileStream(exportFullPath, FileMode.Create, FileAccess.Write))
                                        {
                                            using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.ASCII))
                                            {
                                                sw.WriteLine(title);
                                                sw.WriteLine(officerErrorDescr);
                                            }
                                        }
                                        isCreatedNewFile = true;
                                    }

                                    if (isCreatedNewFile)
                                    {
                                        AARTOBase.LogProcessing(ResourceHelper.GetResource("SuccessToCreateAOfficerErrorDescrCSVFile", this.fileName), LogType.Info, ServiceOption.Continue);
                                        isCreatedNewFile = false;
                                    }
                                }
                                else
                                {
                                    AARTOBase.LogProcessing(ResourceHelper.GetResource("NotFoundByNotIntNo", notice.NotIntNo), LogType.Error, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.UnKnown);
                                    return false;
                                }

                                //ServiceUtility.TransactionCanCommit();
                                //scope.Complete();

                                AARTOBase.LogProcessing(ResourceHelper.GetResource("UpdateStatusForAutoWithdrawS56Successful"),
                               LogType.Info, ServiceOption.Continue, item, QueueItemStatus.Discard);
                               return true;

                            }
                        }

                        return false;
                    }
                    catch (Exception ex)
                    {
                        AARTOBase.LogProcessing(ex, LogType.Error, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.UnKnown);
                        return false;
                    }

                    
                //}
                //end TransactionScope
                });
            //}
        }

        #endregion

        #region Service Starting & Sleeping

        private void OnServiceStarting()
        {
            SetServiceParameters();
            this.fileName = "OfficerErrorsWithdrawnAfterCourtDate_" + DateTime.Now.ToString("yyyy-MM-dd HH:mm").Replace(":", "") + ".csv";
            
        }


        #endregion

        private void SetServiceParameters()
        {
            if (ServiceParameters != null)
            {
                if (!ServiceParameters.ContainsKey("DropOffDirectory"))
                {
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("NotSetServicePara","DropOffDirectory"), LogType.Error, ServiceOption.BreakAndStop);
                    return;
                }
                else
                {
                    if (string.IsNullOrEmpty(ServiceParameters["DropOffDirectory"]))
                    {
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("ParameterExistsButHasNoAssignedValue","DropOffDirectory"), LogType.Error, ServiceOption.BreakAndStop);
                        return;
                    }
                    else
                    {
                        this.DropOffDirectory = ServiceParameters["DropOffDirectory"];
                    }
                }
            }
            else
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("NotFoundServiceParameters"), LogType.Error, ServiceOption.BreakAndStop);
                return;
            }

            //check Directory
            try
            {
                if (!Directory.Exists(this.DropOffDirectory))
                {
                    Directory.CreateDirectory(this.DropOffDirectory);
                }
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ResourceHelper.GetResource("FailedToCreateDirectory",ex.Message), LogType.Error, ServiceOption.BreakAndStop);
                return;
            }
           
        }

        bool QueueItemValidation()
        {
            try
            {
                this.notice = noticeService.GetByNotIntNo(this.notIntNo);

                if (this.notice != null)
                {
                    //check AuthorityRule
                    AuthorityRuleInfo _authorityRuleInfo = authorityRuleInfo.GetAuthorityRulesInfoByWFRFANameAutoIntNo(this.notice.AutIntNo, "5610");
                    if (_authorityRuleInfo != null)
                    {
                        AR_5610 = _authorityRuleInfo.ARString.Trim();
                    }
                    else
                    {
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("AR5610CanNotBeFound", "5610"), LogType.Error, ServiceOption.BreakAndStop, item);
                        this.item.IsSuccessful = false;
                        return false;
                    }

                    //check DateRule
                    DateRuleInfo dr_sumCourtDate_WithDrawnDate = DR_sumCourtDate_WithDrawnDate.GetDateRuleInfoByDRNameAutIntNo(this.notice.AutIntNo, "SumCourtDate", "WithDrawnDate");
                    if (dr_sumCourtDate_WithDrawnDate != null)
                    {
                        noDaysForWithDrawnDate = dr_sumCourtDate_WithDrawnDate.ADRNoOfDays;
                    }
                    else
                    {
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("DR_sumCourtDate_WithDrawnDateCanNotBeFound", "SumCourtDate", "WithDrawnDate"), LogType.Error, ServiceOption.BreakAndStop, item);
                        this.item.IsSuccessful = false;
                        return false;
                    }

                    if (this.notice.NoticeStatus != 581)
                    {
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("NoticeStatusCanNotMeet581Condition"),
                        LogType.Error, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                        return false;
                    }
                    if (AR_5610.Equals("Y", StringComparison.OrdinalIgnoreCase))
                    {
                        var noticeSummons = noticeSummonsService.GetByNotIntNo(this.notice.NotIntNo).FirstOrDefault();
                        var summons = summmonsService.GetBySumIntNo(noticeSummons.SumIntNo);
                        if (summons != null && summons.SumCourtDate.HasValue)
                        {
                            if ((Convert.ToDateTime(summons.SumCourtDate).AddDays(noDaysForWithDrawnDate).Date > DateTime.Now.Date))
                            {
                                AARTOBase.LogProcessing(ResourceHelper.GetResource("sumCourtDateCanNotMeetCondition", summons.SumIntNo),
                               LogType.Error, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                                this.item.IsSuccessful = false;
                                return false;
                            }
                         
                        }
                        else
                        {
                            AARTOBase.LogProcessing(ResourceHelper.GetResource("NotFoundSummonsOrSummonsCourtDate", summons.SumIntNo),
                              LogType.Error, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                            this.item.IsSuccessful = false;
                            return false;
                        }

                    }
                    else
                    {
                        AARTOBase.LogProcessing(ResourceHelper.GetResource("AR5610CanNotMeetCondition"),
                          LogType.Error, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                        this.item.IsSuccessful = false;
                        return false;
                    }

                }
                else
                {
                    AARTOBase.LogProcessing(ResourceHelper.GetResource("FoundNoValidNotice"),
                        LogType.Error, ServiceOption.ContinueButQueueFail, item, QueueItemStatus.Discard);
                    this.item.IsSuccessful = false;
                    return false;

                }
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex, LogType.Error, ServiceOption.ContinueButQueueFail, item);
                this.item.IsSuccessful = false;
                return false;

            }

            return true;
            
        }

        void UpdateNoticeAndChargeStatus()
        {
            //this.origiNoticeStatus = this.notice.NoticeStatus;
          
            var noticeSummons = noticeSummonsService.GetByNotIntNo(notIntNo).FirstOrDefault();

            #region update notice

            if (notice != null && notice.NoticeStatus == 581)
            {
                notice.NoticeStatus = 941;
                notice.NotPrevNoticeStatus = 581;
                notice.LastUser = this.lastUser;
                noticeService.Update(notice);
            }

            #endregion

            #region update charge

            var chargeList = chargeService.GetByNotIntNo(notice.NotIntNo);
            if (chargeList != null && chargeList.Count > 0)
            {
                foreach (var chargeitem in chargeList)
                {
                    if (chargeitem.ChargeStatus == 581)
                    {
                        chargeitem.ChargeStatus = 941;
                        chargeitem.PreviousStatus = 581;
                        chargeitem.LastUser = this.lastUser;
                        chargeService.Update(chargeitem);
                    }
                }
            }

            #endregion

            #region update summons & sumCharge

            if (noticeSummons != null)
            {
                var sumChargeList = sumChargeService.GetBySumIntNo(noticeSummons.SumIntNo);
                var summons = summmonsService.GetBySumIntNo(noticeSummons.SumIntNo);
               
                if (summons != null && summons.SummonsStatus == 581)
                {
                    summons.SummonsStatus = 941;
                    summons.SumPrevSummonsStatus = 581;
                    summons.LastUser = this.lastUser;
                    summmonsService.Update(summons);
                }

                if (sumChargeList != null && sumChargeList.Count > 0)
                {
                    foreach (var sumChargeitem in sumChargeList)
                    {
                        if (sumChargeitem.SumChargeStatus == 581)
                        {
                            sumChargeitem.SumChargeStatus = 941;
                            sumChargeitem.SchPrevSumChargeStatus = 581;
                            sumChargeitem.LastUser = this.lastUser;
                            sumChargeService.Update(sumChargeitem);
                        }
                    }
                }

            }

            #endregion

            #region insert EvidencePack
            #endregion

        }
    }
}
