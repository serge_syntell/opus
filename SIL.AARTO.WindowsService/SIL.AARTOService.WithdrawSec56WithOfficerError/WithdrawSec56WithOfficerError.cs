﻿using System;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.WithdrawSec56WithOfficerError
{
    public partial class WithdrawSec56WithOfficerError : ServiceHost
    {
        public WithdrawSec56WithOfficerError()
            : base("", new Guid("CF0F1150-8666-49B3-B0E6-33D757B71755"), new WithdrawSec56WithOfficerErrorService())
        {
            InitializeComponent();
        }
       
    }
}
