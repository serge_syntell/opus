﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.SubmitInfringementRecord
{
    partial class SubmitInfringementRecord : ServiceHost
    {
        public SubmitInfringementRecord()
            : base("Submit infringement record to CIS interface", new Guid("D7C3363A-9752-4043-A337-A7F112C53191"), new SubmitInfringementRecordClientService())
        {
            InitializeComponent();
        }

    } 
 
}
