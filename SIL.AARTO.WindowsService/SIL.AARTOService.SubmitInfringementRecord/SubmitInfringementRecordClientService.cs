﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SIL.AARTOService.Library;
using SIL.ServiceLibrary;
using System.ServiceModel;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

using Gendac.Syntell.Curator.Web.Services;
using Gendac.Syntell.Curator.Web.Services.Data;
using System.Transactions;
using System.Threading;
using SIL.AARTO.BLL;
using SIL.AARTO.BLL.Utility;
using Stalberg.TMS;
using System.Data;
using SIL.ServiceBase;
using SIL.QueueLibrary;
using System.Globalization;

namespace SIL.AARTOService.SubmitInfringementRecord
{
    public class SubmitInfringementRecordClientService : ServiceDataProcessViaQueue
    {
        CuratorClient client = null;
        List<InfringementRecord> infringementRecordList = null;
        CisInfringementHistoryFileService hisFileService = null;
        CisInfringementResubmissionService resubmissionService = null;
        CisInfringementResubmissionErrorsService resubmissionErrorService = null;
        ImageProcesses imgProcess = null;
        AARTOServiceBase AARTOBase { get { return (AARTOServiceBase)_serviceHelper; } }
        static string userName = string.Empty;
        static string password = string.Empty;
        static int reTryCount = 3;
        static string aartoConnectionStr = string.Empty;
        ScanImageDB scanImageDB = null;
        QueueItemProcessor queueProcessor = new QueueItemProcessor();

        List<CisResponseCode> responseCodeList = new List<CisResponseCode>();

        public SubmitInfringementRecordClientService() :
            base("", "", new Guid("42A281C4-53C3-4D0D-B4E6-3C6C84E68934"), ServiceQueueTypeList.SubmitInfringementRecord)
        {
            this._serviceHelper = new AARTOServiceBase(this, false);

            hisFileService = new CisInfringementHistoryFileService();
            resubmissionService = new CisInfringementResubmissionService();
            resubmissionErrorService = new CisInfringementResubmissionErrorsService();

            AARTOBase.OnServiceStarting = () =>
            {
                string url = AARTOBase.GetConnectionString(ServiceConnectionNameList.CISInterfaceURL, ServiceConnectionTypeList.WSU);
                aartoConnectionStr = AARTOBase.GetConnectionString(ServiceConnectionNameList.AARTO, ServiceConnectionTypeList.DB);

                if (string.IsNullOrEmpty(url))
                {
                    AARTOBase.LogProcessing("CIS interface URL is null or empty", LogType.Fatal, ServiceBase.ServiceOption.BreakAndShutdown);
                    return;
                }
                if (string.IsNullOrEmpty(aartoConnectionStr))
                {
                    AARTOBase.LogProcessing("Database connection string is null or empty", LogType.Fatal, ServiceBase.ServiceOption.BreakAndShutdown);
                    return;
                }

                responseCodeList = new CisResponseCodeService().GetAll().ToList();
                if (responseCodeList == null || responseCodeList.Count == 0)
                {
                    AARTOBase.LogProcessing("CIS_ResponseCode table is empty", LogType.Fatal, ServiceBase.ServiceOption.BreakAndShutdown);
                    return;
                }

                userName = AARTOBase.GetServiceParameter("UserName");
                password = AARTOBase.GetServiceParameter("Password");

                imgProcess = new ImageProcesses(aartoConnectionStr);
                scanImageDB = new ScanImageDB(aartoConnectionStr);

                client = InitialCuratorClient(url);


                infringementRecordList = new List<InfringementRecord>();

            };

            AARTOBase.OnServiceSleeping = () =>
            {
                if (client != null && client.State != CommunicationState.Closed)
                    client.Close();

                if (infringementRecordList != null && infringementRecordList.Count > 0)
                    infringementRecordList.Clear();

                if (responseCodeList != null && responseCodeList.Count > 0)
                    responseCodeList.Clear();
            };
        }


        public override void InitialWork(ref QueueLibrary.QueueItem item)
        {
            if (item != null)
            {
                if (!ServiceUtility.IsNumeric(item.Body))
                {
                    AARTOBase.LogProcessing(String.Format("Invalid queue item, queue key: {0}", item.Body), LogType.Error);
                    return;
                }

                InfringementRecord record = GetInfringementRecord(Convert.ToInt32(item.Body));
                if (record != null)
                    infringementRecordList.Add(record);
                else
                {
                    AARTOBase.LogProcessing(String.Format(
                        "Function:GetNoticeDataListWS in eNatisDB, Could not find Notice info in Database, Infringement Number: {0}",
                        record.InfringementNoticeNumber),
                        LogType.Error);
                }
            }
        }

        public override void MainWork(ref List<QueueLibrary.QueueItem> queueList)
        {
            if (queueList == null || queueList.Count == 0) return;

            QueueLibrary.QueueItem queue = null;
            for (int index = 0; index < queueList.Count; index++)
            {
                queue = queueList[index];
                try
                {
                    InfringementRecord record = infringementRecordList[index];
                    if (record != null)
                    {
                        ProcessInfringementRecord(record, ref queue);
                    }
                }
                catch (Exception ex)
                {
                    AARTOBase.LogProcessing(ex.ToString(), LogType.Error);
                }
                finally
                {
                    if (infringementRecordList != null)
                        infringementRecordList.Clear();
                }
            }
        }

        #region Private functions

        void ProcessInfringementRecord(InfringementRecord record, ref QueueLibrary.QueueItem queue)
        {

            // create record in historyfile table
            // set requestxml and request date
            int notIntNo = Convert.ToInt32(queue.Body);
            //string notTicketNo = string.Empty;
            int retryIndex = 1;
            try
            {
                // create request history file
                int cihIntNo = CreateRequestHistoryFile(notIntNo, record);

                SubmitReply response = null;
                while (retryIndex <= reTryCount)
                {
                    try
                    {
                        // call wcf interface to submit infringement record
                        response = client.SubmitInfringementRecord(record);
                    }
                    catch (Exception ex)
                    {
                        AARTOBase.LogProcessing(ex, LogType.Error, ServiceBase.ServiceOption.Continue, queue, QueueLibrary.QueueItemStatus.UnKnown);
                    }
                    if (response != null)
                    {
                        break;
                    }
                    else
                    {
                        if (retryIndex == reTryCount)
                        {
                            AARTOBase.LogProcessing(String.Format("We have tried {0} times and get no response from CIS interface, Infringement Number: {1}", reTryCount, record.InfringementNoticeNumber), LogType.Error);
                        }
                        retryIndex++;
                        Thread.Sleep(new TimeSpan(0, 0, 3));
                    }
                }

                if (response != null)
                {
                    // create response history file
                    CreateResponseHistoryFile(cihIntNo, response);

                    using (TransactionScope tran = new TransactionScope())
                    {
                        // if response is success, discard queue
                        if (response.IsSuccess)
                        {
                            //queue.Status = QueueLibrary.QueueItemStatus.Discard;
                            //this.Logger.Info(String.Format("Submit infringement record successful, Infringement Number: {0}", record.InfringementNoticeNumber), queue.Body.ToString(),true);

                            AARTOBase.LogProcessing(String.Format("Submit infringement record successful, Infringement Number: {0}", record.InfringementNoticeNumber),
                               LogType.Info, ServiceOption.Continue, queue, QueueLibrary.QueueItemStatus.Discard);

                            queueProcessor.Send(new QueueItem()
                            {
                                Body = notIntNo,
                                ActDate=DateTime.Now,
                                QueueType = ServiceQueueTypeList.SubmitSupportingEvidence
                            });
                        }
                        else
                        {
                            // request failed
                            AARTOBase.LogProcessing(String.Format("Request CIS interface failed, Please check the error details in Opus, Infringement Number: {0} ", record.InfringementNoticeNumber),
                                LogType.Error, ServiceOption.Continue, queue, QueueLibrary.QueueItemStatus.Discard);
                            // create resubmit infringement record row here
                            CreateResubmission(notIntNo, record.InfringementNoticeNumber, AARTOBase.LastUser, response.Messages);
                        }

                        tran.Complete();
                    }
                }
                else
                {
                    AARTOBase.LogProcessing(String.Format("No response return, Infringement Number: {0}", record.InfringementNoticeNumber),
                        LogType.Error,
                        ServiceBase.ServiceOption.Continue, queue);
                }
            }
            catch (Exception ex)
            {
                AARTOBase.LogProcessing(ex.ToString(), LogType.Error, ServiceBase.ServiceOption.Continue, queue);
            }
        }

        CuratorClient InitialCuratorClient(string url)
        {

            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential);
            //binding.Name = "BasicHttpBinding_ICurator";
            //BasicHttpBinding binding = new BasicHttpBinding();

            binding.MessageEncoding = WSMessageEncoding.Text;
            binding.TextEncoding = Encoding.UTF8;
            //binding.SendTimeout = new TimeSpan(0, 1, 0);
            binding.ReceiveTimeout = new TimeSpan(0, 10, 0);

            client = new CuratorClient(binding, new System.ServiceModel.EndpointAddress(url));
            client.ClientCredentials.UserName.UserName = userName;
            client.ClientCredentials.UserName.Password = password;

            //if (client.State == CommunicationState.Closed)
            client.Open();

            return client;
        }

        InfringementRecord GetInfringementRecord(int notIntNo)
        {

            SubmitInfringementRecord s = new SubmitInfringementRecord();

            eNatisDB noticeDB = new eNatisDB(aartoConnectionStr);
            DataSet ds = noticeDB.GetNoticeDataListWS(notIntNo);
            if (ds == null || ds.Tables.Count == 0) return null;

            InfringementRecord r = new InfringementRecord();
            r.CameraSerialNumber = ds.Tables[0].Rows[0]["CamSerialNo"].ToString();
            r.ExtensionData = null;
            r.FieldSheetRefNumber = ds.Tables[0].Rows[0]["FilmFieldSheetReference"] is DBNull ? "" : ds.Tables[0].Rows[0]["FilmFieldSheetReference"].ToString();
            r.InfringementNoticeNumber = ds.Tables[0].Rows[0]["NotTicketNo"].ToString();
            r.LocationCode = ds.Tables[0].Rows[0]["NotLocCode"].ToString();

            // SubmissionTo eNatis date need to confirm, i think it's not now
            // r.SubmissionToEnatisDate = DateTime.Now;
            r.SubmissionToEnatisDate = ds.Tables[0].Rows[0]["RequestSentToNTIDate"] is DBNull ? DateTime.Now : DateTime.Parse(ds.Tables[0].Rows[0]["RequestSentToNTIDate"].ToString());

            r.EvidenceFile = new EvidenceFile();

            r.EvidenceFile.AmberTime = "";
            r.EvidenceFile.RedTime = "";
            r.EvidenceFile.RouteN = "";
            r.EvidenceFile.ExtensionData = null;
            r.EvidenceFile.GpsxCoord = "";
            r.EvidenceFile.GpsyCoord = "";
            r.EvidenceFile.Province = "";

            r.EvidenceFile.CityTown = ds.Tables[0].Rows[0]["City"].ToString();
            r.EvidenceFile.Code = ds.Tables[0].Rows[0]["ChgOffenceCode"].ToString();
            r.EvidenceFile.Date = DateTime.Parse(ds.Tables[0].Rows[0]["NotOffenceDate"].ToString());
            r.EvidenceFile.Time = Convert.ToDateTime(DateTime.Parse(ds.Tables[0].Rows[0]["NotOffenceDate"].ToString()).ToString("HH:mm:ss"));
            r.EvidenceFile.Description = ds.Tables[0].Rows[0]["ChgOffenceDescr"] is DBNull ? "" : ds.Tables[0].Rows[0]["ChgOffenceDescr"].ToString() + " " +
                    ds.Tables[0].Rows[0]["ChgOffenceDescr2"] is DBNull ? "" : ds.Tables[0].Rows[0]["ChgOffenceDescr2"].ToString() + " " +
                    ds.Tables[0].Rows[0]["ChgOffenceDescr3"] is DBNull ? "" : ds.Tables[0].Rows[0]["ChgOffenceDescr3"].ToString();

            string direction = ds.Tables[0].Rows[0]["TravelDirection"] is DBNull ? "" : ds.Tables[0].Rows[0]["TravelDirection"].ToString();
            switch (direction.ToUpper().Trim())
            {
                case "E":
                    r.EvidenceFile.DirectionFrom = "E";
                    r.EvidenceFile.DirectionTo = "W";
                    break;
                case "W":
                    r.EvidenceFile.DirectionFrom = "W";
                    r.EvidenceFile.DirectionTo = "E";
                    break;
                case "S":
                    r.EvidenceFile.DirectionFrom = "S";
                    r.EvidenceFile.DirectionTo = "N";
                    break;
                case "N":
                    r.EvidenceFile.DirectionFrom = "N";
                    r.EvidenceFile.DirectionTo = "S";
                    break;
            }

            r.EvidenceFile.IssAuthority = ds.Tables[0].Rows[0]["ChgIssueAuthority"] is DBNull ? "" : ds.Tables[0].Rows[0]["ChgIssueAuthority"].ToString();
            r.EvidenceFile.MvTypeCd = ds.Tables[0].Rows[0]["VTCode"].ToString();


            r.EvidenceFile.FilmN = ds.Tables[0].Rows[0]["FilmNo"].ToString();
            r.EvidenceFile.InfrastructureN = ds.Tables[0].Rows[0]["OfficerNatisNo"].ToString();

            r.EvidenceFile.LicN = ds.Tables[0].Rows[0]["NotRegNo"].ToString();
            r.EvidenceFile.MagisterialCd = ds.Tables[0].Rows[0]["MagisterialCD"] == DBNull.Value ? null : ds.Tables[0].Rows[0]["MagisterialCD"].ToString();
            r.EvidenceFile.OtherLocInfo = ds.Tables[0].Rows[0]["LocDescr"].ToString(); ;
            r.EvidenceFile.RefN = ds.Tables[0].Rows[0]["FrameNo"].ToString();
            r.EvidenceFile.RegAuthority = ds.Tables[0].Rows[0]["NotRegisterAuth"].ToString();
            //update by Rachel 20140812 for 5337
            //r.EvidenceFile.SpeedRead1 = Convert.ToDecimal(ds.Tables[0].Rows[0]["NotSpeed1"]).ToString("f2").PadLeft(6, '0');
            //r.EvidenceFile.SpeedRead2 = Convert.ToDecimal(ds.Tables[0].Rows[0]["NotSpeed2"]).ToString("f2").PadLeft(6, '0');
            r.EvidenceFile.SpeedRead1 = Convert.ToDecimal(ds.Tables[0].Rows[0]["NotSpeed1"]).ToString("f2", CultureInfo.InvariantCulture).PadLeft(6, '0');
            r.EvidenceFile.SpeedRead2 = Convert.ToDecimal(ds.Tables[0].Rows[0]["NotSpeed2"]).ToString("f2", CultureInfo.InvariantCulture).PadLeft(6, '0');
            //end update by Rachel 20140812 for 5337
            r.EvidenceFile.StreetNameA = ds.Tables[0].Rows[0]["StreetA"].ToString();
            r.EvidenceFile.StreetNameB = ds.Tables[0].Rows[0]["StreetB"].ToString().Length > 0 ? ds.Tables[0].Rows[0]["StreetB"].ToString() : "";
            r.EvidenceFile.Suburb = ds.Tables[0].Rows[0]["Suburb"].ToString();

            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["VehImageIntNo"].ToString()))
            {
                int VehImageIntNo = Convert.ToInt32(ds.Tables[0].Rows[0]["VehImageIntNo"].ToString());

                ScanImageDetails imgDetail = null;
                r.EvidenceFile.VehImage = imgProcess.CompressImage(scanImageDB.GetScanImageDataFromRemoteServer(VehImageIntNo, out imgDetail), 0);

                if (imgDetail != null && !String.IsNullOrEmpty(imgDetail.JPegName))
                {
                    string filmType = ds.Tables[0].Rows[0]["FilmType"] is DBNull ? "" : ds.Tables[0].Rows[0]["FilmType"].ToString();
                    // according to email CIS - Images, if FilmType=P, no need MainSupportingEvidence
                    //if (filmType != "P")
                    //{
                    r.MainSupportingEvidence = r.EvidenceFile.VehImage;
                    r.MainSupportingEvidenceFileName = imgDetail.JPegName;
                    //}
                }

            }

            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["LicImageIntNo"].ToString()))
            {
                int LicImageIntNo = Convert.ToInt32(ds.Tables[0].Rows[0]["LicImageIntNo"].ToString());
                r.EvidenceFile.LicImage = scanImageDB.GetScanImageDataFromRemoteServer(LicImageIntNo);
            }

            return r;

        }

        int CreateRequestHistoryFile(int notIntNo, InfringementRecord record)
        {
            CisInfringementHistoryFile his = new CisInfringementHistoryFile();

            string requestXml = ObjectSerializer.Serialize(record);
            InfringementRecord r = ObjectSerializer.DeSerializerString2Object<InfringementRecord>(requestXml);

            r.MainSupportingEvidence = new byte[] { };
            r.EvidenceFile.VehImage = new byte[] { };
            r.EvidenceFile.LicImage = new byte[] { };

            requestXml = ObjectSerializer.Serialize(r);

            his.NotIntNo = notIntNo;
            his.NotTicketNo = record.InfringementNoticeNumber;
            his.CihRequestDate = DateTime.Now;
            his.CihRequestXml = requestXml;
            his.LastUser = AARTOBase.LastUser;

            his = hisFileService.Save(his);
            return his.CihIntNo;
        }

        bool CreateResponseHistoryFile(int cihIntNo, SubmitReply response)
        {
            CisInfringementHistoryFile his = hisFileService.GetByCihIntNo(cihIntNo);

            string responseXml = ObjectSerializer.Serialize(response);
            his.CihResponseDate = DateTime.Now;
            his.CihResponseXml = responseXml;
            his.LastUser = AARTOBase.LastUser;

            return hisFileService.Save(his) != null;

        }

        CisInfringementResubmission CreateResubmission(int notIntNo, string notTicketNo, string lastUser, ReplyMessage[] messages)
        {
            CisInfringementResubmission reSubmission = resubmissionService.GetByNotIntNo(notIntNo)
                .Where(r => r.CirSubmitDate.HasValue == false).FirstOrDefault();

            if (reSubmission == null)
            {
                reSubmission = new CisInfringementResubmission();
                reSubmission.NotIntNo = notIntNo;
                reSubmission.NotTicketNo = notTicketNo;
                reSubmission.CirSubmitDate = null;
                reSubmission.LastUser = lastUser;

                reSubmission = resubmissionService.Save(reSubmission);
            }

            if (messages != null && messages.Length > 0)
            {
                SIL.AARTO.DAL.Entities.TList<CisInfringementResubmissionErrors> errors = new SIL.AARTO.DAL.Entities.TList<CisInfringementResubmissionErrors>();
                for (int index = 0; index < messages.Length; index++)
                {
                    CisResponseCode responseCode = this.responseCodeList.Where(r => r.CrcCode == messages[index].Code.ToString()).FirstOrDefault();
                    if (responseCode == null)
                    {
                        AARTOBase.LogProcessing(String.Format("Could not find response code in our DataBase, ResponseCode: {0} and Description: {1}, Infringement Number: {2}",
                            messages[index].Code, messages[index].Description, notTicketNo),
                            LogType.Error);
                        continue;
                    }
                    errors.Add(new CisInfringementResubmissionErrors()
                    {
                        CirIntNo = reSubmission.CirIntNo,
                        CireSubmitDate = null,
                        CrcIntNo = responseCode.CrcIntNo,
                        LastUser = lastUser,
                        CrcDescription = messages[index].Description
                    });

                }
                resubmissionErrorService.Save(errors);
            }

            return reSubmission;
        }

        #endregion
    }
}
