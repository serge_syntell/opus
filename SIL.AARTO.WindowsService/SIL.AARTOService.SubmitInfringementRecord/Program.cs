﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using SIL.ServiceLibrary;

namespace SIL.AARTOService.SubmitInfringementRecord
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            ServiceDescriptor serviceDescr = new ServiceDescriptor()
            {
                Description = "SIL.AARTOService.SubmitInfringementRecord",
                DisplayName = "SIL.AARTOService.SubmitInfringementRecord",
                ServiceName = "SIL.AARTOService.SubmitInfringementRecord"
            };

            ProgramRun.InitializeService(new SubmitInfringementRecord(), serviceDescr, args);
        }
    }
}
