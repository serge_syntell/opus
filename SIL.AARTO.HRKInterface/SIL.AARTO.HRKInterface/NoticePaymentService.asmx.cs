﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using SIL.AARTO.HRKInterface.BLL;
using System.Xml;

namespace SIL.AARTO.HRKInterface
{
    /// <summary>
    /// Summary description for NoticePaymentService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class NoticePaymentService : IPaymentRequestBinding
    {
        static string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

        void IPaymentRequestBinding.PaymentRequest(ref string XMLMessage)
        {
            // 2014-10-16, Oscar added for diagnostic
            Common.StartDebug();
            Common.WriteDebug("NoticePayment", "--- Start Request at NoticePaymentService.PaymentRequest. ---");

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XMLMessage);

            NoticePayment noticePayment = new NoticePayment(connectionStr);

            XMLMessage = noticePayment.DoNoticePayment(xmlDoc).OuterXml;

            // 2014-10-16, Oscar added for diagnostic
            Common.WriteDebug("NoticePayment", "--- End Request at NoticePaymentService.PaymentRequest. ---");
            Common.EndDebug();
        }
    }
}
