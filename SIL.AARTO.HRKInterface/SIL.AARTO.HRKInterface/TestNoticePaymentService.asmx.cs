﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using SIL.AARTO.HRKInterface.BLL;
using System.Xml;

namespace SIL.AARTO.HRKInterface
{
    /// <summary>
    /// Summary description for TestNoticePaymentService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class TestNoticePaymentService : System.Web.Services.WebService
    {
        static string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

        [WebMethod]
        public void TestPaymentRequest(ref string XMLMessage, bool isCommit)
        {
            // 2014-10-16, Oscar added for diagnostic
            Common.StartDebug();
            Common.WriteDebug("NoticePayment", "--- Start Request at TestNoticePaymentService.TestPaymentRequest. ---");

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XMLMessage);

            NoticePayment noticePayment = new NoticePayment(connectionStr);

            XMLMessage = noticePayment.DoTestNoticePayment(xmlDoc, isCommit).OuterXml;

            // 2014-10-16, Oscar added for diagnostic
            Common.WriteDebug("NoticePayment", "--- End Request at TestNoticePaymentService.TestPaymentRequest. ---");
            Common.EndDebug();

        }
    }
}
