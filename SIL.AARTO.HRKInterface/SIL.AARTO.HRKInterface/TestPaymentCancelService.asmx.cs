﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Xml.Schema;

using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.HRKInterface.BLL;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.HRKInterface
{
    /// <summary>
    /// Summary description for TestPaymentCancelService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class TestPaymentCancelService : System.Web.Services.WebService
    {
        static string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

        [WebMethod]
        public void TestPaymentCancellation(ref string XMLMessage, bool isCommit)
        {
            XmlDocument xmlDoc = new XmlDocument();
            PaymentCancellation payment = new PaymentCancellation(connectionStr);

            xmlDoc.LoadXml(XMLMessage);

            XMLMessage = payment.DoTestPaymentCancellation(xmlDoc, isCommit).OuterXml;
        }
    }
}
