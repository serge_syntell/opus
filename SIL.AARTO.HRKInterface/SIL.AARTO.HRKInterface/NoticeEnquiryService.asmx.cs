﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Xml.Schema;

using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.HRKInterface.BLL;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.HRKInterface
{
    /// <summary>
    /// Summary description for NoticeEnquiryService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class NoticeEnquiryService :INoticeEnquiryBinding
    {
        static string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

        void INoticeEnquiryBinding.NoticeEnquiry(ref string XMLMessage)
        {
            // 2014-10-16, Oscar added for diagnostic
            Common.StartDebug();
            Common.WriteDebug("NoticeEnquiry", "--- Start Request at NoticeEnquiryService.NoticeEnquiry. ---");

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XMLMessage);

            NoticeEnquiry noticeEnquiry = new NoticeEnquiry(connectionStr);

            XMLMessage = noticeEnquiry.DoNoticeEnquiry(xmlDoc).OuterXml;

            // 2014-10-16, Oscar added for diagnostic
            Common.WriteDebug("NoticeEnquiry", "--- End Request at NoticeEnquiryService.NoticeEnquiry. ---");
            Common.EndDebug();
        }
    }
}
