﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Xml;
using System.Xml.Schema;
using System.IO;

namespace TestApp
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Add("Select service ...");
            comboBox1.Items.Add("NoticeEnquiryService");
            comboBox1.Items.Add("NoticePaymentService");
            comboBox1.Items.Add("PaymentCancelService");

            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != 1)
            {
                MessageBox.Show("Select NoticeEnquiryService");
                return;
            }
            string url = ConfigurationManager.AppSettings["NoticeEnquiryService"];
            //NoticeEnquiryBinding client = new NoticeEnquiryBinding();
            //client.Url = url;
            ServiceReference1.NoticeEnquiryBindingClient client = new ServiceReference1.NoticeEnquiryBindingClient("NoticeEnquiryBinding");

            string XMLMessage = tbValue.Text;
            client.NoticeEnquiry(ref XMLMessage);

            XMLMessage = XMLMessage.Replace("><", ">\r\n<");

            ValidateXML(XMLMessage);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != 2)
            {
                MessageBox.Show("Select NoticePaymentService");
                return;
            }

            //ServiceReference2.PaymentRequestBindingClient client = new ServiceReference2.PaymentRequestBindingClient("PaymentRequestBinding");
            ServiceReference4.TestNoticePaymentServiceSoapClient client = new ServiceReference4.TestNoticePaymentServiceSoapClient();

            string XMLMessage = tbValue.Text;

            if (radioButton1.Checked)
            {
                client.TestPaymentRequest(ref XMLMessage, false);

                XMLMessage = XMLMessage.Replace("><", ">\r\n<");

                ValidateXML(XMLMessage);
            }
            if (radioButton2.Checked)
            {
                MessageBoxForm from = new MessageBoxForm();

                DialogResult result = from.ShowDialog();
                if (result == DialogResult.Yes)
                {
                    client.TestPaymentRequest(ref XMLMessage, true);

                    XMLMessage = XMLMessage.Replace("><", ">\r\n<");

                    ValidateXML(XMLMessage);
                }
                else if (result == DialogResult.No)
                {
                    client.TestPaymentRequest(ref XMLMessage, false);

                    XMLMessage = XMLMessage.Replace("><", ">\r\n<");

                    ValidateXML(XMLMessage);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != 3)
            {
                MessageBox.Show("Select PaymentCancelService");
                return;
            }
            //ServiceReference3.PaymentCancellationBindingClient client = new ServiceReference3.PaymentCancellationBindingClient("PaymentCancellationBinding");

            ServiceReference5.TestPaymentCancelServiceSoapClient client = new ServiceReference5.TestPaymentCancelServiceSoapClient();

            string XMLMessage = tbValue.Text;

            if (radioButton1.Checked)
            {
                client.TestPaymentCancellation(ref XMLMessage, false);

                XMLMessage = XMLMessage.Replace("><", ">\r\n<");

                ValidateXML(XMLMessage);
            }
            if (radioButton2.Checked)
            {
                MessageBoxForm from = new MessageBoxForm();

                DialogResult result = from.ShowDialog();
                if (result == DialogResult.Yes)
                {
                    client.TestPaymentCancellation(ref XMLMessage, true);

                    XMLMessage = XMLMessage.Replace("><", ">\r\n<");

                    ValidateXML(XMLMessage);
                }
                else if (result == DialogResult.No)
                {
                    client.TestPaymentCancellation(ref XMLMessage, false);

                    XMLMessage = XMLMessage.Replace("><", ">\r\n<");

                    ValidateXML(XMLMessage);
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            radioButton1.Checked = true;
            string strMsg = "";
            switch (comboBox1.SelectedIndex)
            {
                case 1:
                    strMsg += "<?xml version=\"1.0\"?> " + Environment.NewLine;
                    strMsg += "<NoticeEnquiryRequest xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" + Environment.NewLine;
                    strMsg += "<HEADER>" + Environment.NewLine;
                    strMsg += "<ClientData/>" + Environment.NewLine;
                    strMsg += "<LocalAuthorityCode>805</LocalAuthorityCode>" + Environment.NewLine;
                    strMsg += "<ResultCode/>" + Environment.NewLine;
                    strMsg += "<ServerData/>" + Environment.NewLine;
                    strMsg += "<ServiceProvider>1001</ServiceProvider>" + Environment.NewLine;
                    strMsg += "<VersionControl>1</VersionControl>" + Environment.NewLine;
                    strMsg += "</HEADER>" + Environment.NewLine;
                    strMsg += "<NoticeEnquiry>" + Environment.NewLine;
                    strMsg += "<PaymentOriginator>2000</PaymentOriginator>" + Environment.NewLine;
                    strMsg += "<RequestData>90/000047/213/397</RequestData>" + Environment.NewLine;
                    strMsg += "<RequestType>N</RequestType>" + Environment.NewLine;
                    strMsg += "</NoticeEnquiry>" + Environment.NewLine;
                    strMsg += "</NoticeEnquiryRequest>";
                    break;
                case 2:
                    strMsg += "<?xml version=\"1.0\"?> " + Environment.NewLine;
                    strMsg += "<NoticePaymentRequest xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" + Environment.NewLine;
                     strMsg += "<HEADER>" + Environment.NewLine;
                    strMsg += "<ClientData/>" + Environment.NewLine;
                    strMsg += "<LocalAuthorityCode>805</LocalAuthorityCode>" + Environment.NewLine;
                    strMsg += "<ResultCode/>" + Environment.NewLine;
                    strMsg += "<ServerData/>" + Environment.NewLine;
                    strMsg += "<ServiceProvider>1001</ServiceProvider>" + Environment.NewLine;
                    strMsg += "<VersionControl>1</VersionControl>" + Environment.NewLine;
                    strMsg += "</HEADER>" + Environment.NewLine;
                    strMsg += "<NoticePayment>" + Environment.NewLine;
                    strMsg += "<PaymentOriginator>2000</PaymentOriginator>" + Environment.NewLine;
                    strMsg += "<PaymentType>4001</PaymentType>" + Environment.NewLine;
                    strMsg += "<PayPointDescription>Jake test</PayPointDescription>" + Environment.NewLine;
                    strMsg += "<PayPointIdentification>Pay point identification</PayPointIdentification>" + Environment.NewLine;
                    strMsg += "<PayPointType>3003</PayPointType>" + Environment.NewLine;
                    strMsg += "<ReceiptDate>2012-05-08T13:00:00</ReceiptDate>" + Environment.NewLine;
                    strMsg += "<ReceiptNumber>HEK00001</ReceiptNumber>" + Environment.NewLine;
                    strMsg += "<ServiceProviderReference>1004</ServiceProviderReference>" + Environment.NewLine;
                    strMsg += "<TotalAmount>180</TotalAmount>" + Environment.NewLine;
                    strMsg += "<NoticeNumber>20/00005/561/000591</NoticeNumber>" + Environment.NewLine;
                    strMsg += "</NoticePayment>" + Environment.NewLine;
                    strMsg += "</NoticePaymentRequest>";
                    break;
                case 3:
                    strMsg += "<?xml version=\"1.0\"?> " + Environment.NewLine;
                    strMsg += "<NoticePaymentCancellationRequest xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" + Environment.NewLine;
                    strMsg += "<HEADER>" + Environment.NewLine;
                    strMsg += "<ClientData/>" + Environment.NewLine;
                    strMsg += "<LocalAuthorityCode>805</LocalAuthorityCode>" + Environment.NewLine;
                    strMsg += "<ResultCode/>" + Environment.NewLine;
                    strMsg += "<ServerData/>" + Environment.NewLine;
                    strMsg += "<ServiceProvider>1001</ServiceProvider>" + Environment.NewLine;
                    strMsg += "<VersionControl>1</VersionControl>" + Environment.NewLine;
                    strMsg += "</HEADER>" + Environment.NewLine;
                    strMsg += "<NoticePaymentCancellation>" + Environment.NewLine;
                    strMsg += "<CancellationReason>AA</CancellationReason>" + Environment.NewLine;
                    strMsg += "<CancelledDate>2012-05-08T13:00:00</CancelledDate>" + Environment.NewLine;
                    strMsg += "<NoticeNumber>20/000001/561/000583</NoticeNumber>" + Environment.NewLine;
                    strMsg += "<ReceiptNumber>N</ReceiptNumber>" + Environment.NewLine;
                    strMsg += "</NoticePaymentCancellation>" + Environment.NewLine;
                    strMsg += "</NoticePaymentCancellationRequest>";
                    break;
            }
            tbValue.Text = strMsg;

        }
        string strMsg = "";
        private void ValidateXML(string xmlMessage)
        {
            string xsdPath = AppDomain.CurrentDomain.BaseDirectory + "XSD";
            string responsePath = "";
            switch (comboBox1.SelectedIndex)
            {
                case 1:
                    responsePath = "NoticeEnquiryResponse.xsd";
                    break;
                case 2:
                    responsePath = "NoticePaymentResponse.xsd";
                    break;
                case 3:
                    responsePath = "PaymentCancellationResponse.xsd";
                    break;
            }

            try
            {
                MessageForm mess = new MessageForm();
                mess.strMsg = xmlMessage;
                mess.ShowDialog();

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlMessage);

                string Version = "";
                if (xmlDoc.ChildNodes.Count > 1 && xmlDoc.ChildNodes[1].SelectNodes("HEADER/VersionControl") != null)
                {
                    Version = xmlDoc.ChildNodes[1].SelectNodes("HEADER/VersionControl")[0].InnerText.Trim();
                }

                xsdPath += "\\" + Version + "\\";

                if (File.Exists(xsdPath + responsePath)
                    && File.Exists(xsdPath + "Types.xsd")
                    && File.Exists(xsdPath + "Enumerations.xsd"))
                {
                    using (XmlTextReader schemaReader = new XmlTextReader(xsdPath + responsePath))
                    {
                        XmlSchema schema = XmlSchema.Read(schemaReader, ValidationEventHandler);
                        xmlDoc.Schemas.Add(schema);
                    }

                    xmlDoc.Validate(ValidationEventHandler);

                    if (!string.IsNullOrEmpty(strMsg))
                    {
                        MessageBox.Show(strMsg);
                    }
                }
                else
                {
                    MessageBox.Show("XSD files is not exists,please check XSDVersion Number");
                }

                strMsg = "";
            }
            catch (Exception ex)
            {
                strMsg = "";
                MessageBox.Show(ex.ToString());
            }
        }

        private void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Message != null)
            {
                strMsg += e.Message;
            }
        }
    }
}
