﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.296
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TestApp.ServiceReference5 {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference5.TestPaymentCancelServiceSoap")]
    public interface TestPaymentCancelServiceSoap {
        
        // CODEGEN: Generating message contract since element name XMLMessage from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/TestPaymentCancellation", ReplyAction="*")]
        TestApp.ServiceReference5.TestPaymentCancellationResponse TestPaymentCancellation(TestApp.ServiceReference5.TestPaymentCancellationRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class TestPaymentCancellationRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="TestPaymentCancellation", Namespace="http://tempuri.org/", Order=0)]
        public TestApp.ServiceReference5.TestPaymentCancellationRequestBody Body;
        
        public TestPaymentCancellationRequest() {
        }
        
        public TestPaymentCancellationRequest(TestApp.ServiceReference5.TestPaymentCancellationRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class TestPaymentCancellationRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string XMLMessage;
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=1)]
        public bool isCommit;
        
        public TestPaymentCancellationRequestBody() {
        }
        
        public TestPaymentCancellationRequestBody(string XMLMessage, bool isCommit) {
            this.XMLMessage = XMLMessage;
            this.isCommit = isCommit;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class TestPaymentCancellationResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="TestPaymentCancellationResponse", Namespace="http://tempuri.org/", Order=0)]
        public TestApp.ServiceReference5.TestPaymentCancellationResponseBody Body;
        
        public TestPaymentCancellationResponse() {
        }
        
        public TestPaymentCancellationResponse(TestApp.ServiceReference5.TestPaymentCancellationResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class TestPaymentCancellationResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string XMLMessage;
        
        public TestPaymentCancellationResponseBody() {
        }
        
        public TestPaymentCancellationResponseBody(string XMLMessage) {
            this.XMLMessage = XMLMessage;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface TestPaymentCancelServiceSoapChannel : TestApp.ServiceReference5.TestPaymentCancelServiceSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class TestPaymentCancelServiceSoapClient : System.ServiceModel.ClientBase<TestApp.ServiceReference5.TestPaymentCancelServiceSoap>, TestApp.ServiceReference5.TestPaymentCancelServiceSoap {
        
        public TestPaymentCancelServiceSoapClient() {
        }
        
        public TestPaymentCancelServiceSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public TestPaymentCancelServiceSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public TestPaymentCancelServiceSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public TestPaymentCancelServiceSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        TestApp.ServiceReference5.TestPaymentCancellationResponse TestApp.ServiceReference5.TestPaymentCancelServiceSoap.TestPaymentCancellation(TestApp.ServiceReference5.TestPaymentCancellationRequest request) {
            return base.Channel.TestPaymentCancellation(request);
        }
        
        public void TestPaymentCancellation(ref string XMLMessage, bool isCommit) {
            TestApp.ServiceReference5.TestPaymentCancellationRequest inValue = new TestApp.ServiceReference5.TestPaymentCancellationRequest();
            inValue.Body = new TestApp.ServiceReference5.TestPaymentCancellationRequestBody();
            inValue.Body.XMLMessage = XMLMessage;
            inValue.Body.isCommit = isCommit;
            TestApp.ServiceReference5.TestPaymentCancellationResponse retVal = ((TestApp.ServiceReference5.TestPaymentCancelServiceSoap)(this)).TestPaymentCancellation(inValue);
            XMLMessage = retVal.Body.XMLMessage;
        }
    }
}
