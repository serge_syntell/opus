﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Reflection;
using System.Collections;

namespace SIL.AARTO.HRKInterface.BLL
{
    public class XmlUtility
    {
        public static T DeserializeXml<T>(XmlDocument xmlDoc)
        {
            try
            {
                XmlSerializer lizer = new XmlSerializer(typeof(T));
                StringReader reader = new StringReader(xmlDoc.OuterXml);
                T t = (T)lizer.Deserialize(reader);
                return t;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static XmlDocument Serialize<T>(T t)
        {
            try
            {
                XmlSerializer lizer = new XmlSerializer(t.GetType());
                MemoryStream ms = new MemoryStream();
                lizer.Serialize(ms, t);
                XmlDocument xmlDoc = new XmlDocument();
                ms.Position = 0;
                xmlDoc.Load(ms);

                return xmlDoc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static IList<string> GetEnumValues<T>()
        {
            Type enumType = typeof(T);

            if (!enumType.IsEnum)
            {
                return null;
            }

            IList<string> list = new List<string>();

            var fields = from field in enumType.GetFields()
                         where field.IsLiteral
                         select field;

            foreach (FieldInfo field in fields)
            {
                object value = field.GetValue(enumType);
                list.Add(((int)value).ToString());
            }

            return list;
        }

        public static List<Attribute> GetEnumText<T>(object enumValue)
        {
            //Attribute[] eds = (Attribute[])enumType.GetCustomAttributes(typeof(T), false);
            Type enumType = enumValue.GetType();

            FieldInfo[] fields = enumType.GetFields();

            List<Attribute> returnList = new List<Attribute>();

            foreach (FieldInfo fi in fields)
            {
                if (fi.Name == enumValue.ToString())
                {
                    Attribute[] eds = (Attribute[])fi.GetCustomAttributes(typeof(T), false);

                    returnList = eds.ToList();

                    break;
                }
            }

            return returnList;

        }
 
    }
}
