﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SIL.AARTO.HRKInterface.BLL
{
    [Serializable()]
    public class PaymentHeader
    {
        [XmlElement(ElementName = "ClientData")]
        public string ClientData { get; set; }
        [XmlElement(ElementName = "LocalAuthorityCode")]
        public string LocalAuthorityCode { get; set; }

        [XmlElement(ElementName = "ResultCode")]
        public string ResultCode { get; set; }

        [XmlElement(ElementName = "ServerData")]
        public string ServerData { get; set; }

        [XmlElement(ElementName = "ServiceProvider")]
        public string ServiceProvider { get; set; }

        [XmlElement(ElementName = "VersionControl")]
        public string VersionControl { get; set; }

    }
}
