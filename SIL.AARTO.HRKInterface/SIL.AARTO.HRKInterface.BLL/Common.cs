﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Transactions;

namespace SIL.AARTO.HRKInterface.BLL
{
    public class Common
    {
        static readonly AartoErrorLogService aartoErrorLogService = new AartoErrorLogService();

        public static void WriteLog(string logName,string category,string description)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                AartoErrorLog log = new AartoErrorLog();
                log.AaProjectId = (int)AartoProjectList.HRKInterface;
                if (category == "Error")
                {
                    log.AaErLogPriority = 5;
                }
                else
                {
                    log.AaErLogPriority = 0;
                }
                log.AaErLogDate = DateTime.Now;
                log.AaErLogName = logName;
                log.AaErLogDescription = description;
                log.LastUser = "HRKInterface";
                //log = new AartoErrorLogService().Save(log);
                // 2014-10-15, Oscar changed
                aartoErrorLogService.Save(log);
                scope.Complete();
            }
        }

        #region 2014-10-16, Oscar added for diagnostic

        static readonly object syncObj = new object();
        static long count;

        //[ThreadStatic] static AartoErrorLogService aartoErrorLogService;
        [ThreadStatic] static long uid;
        [ThreadStatic] public static string Originator;
        [ThreadStatic] public static string Type;
        [ThreadStatic] public static string Data;

        public static void StartDebug()
        {
            lock (syncObj)
            {
                count++;
                uid = count;
            }
            //if (aartoErrorLogService == null) aartoErrorLogService = new AartoErrorLogService();
        }

        public static void EndDebug()
        {
            Originator = null;
            Type = null;
            Data = null;

            //aartoErrorLogService = null;
        }

        public static void WriteDebug(string name, string message, params object[] args)
        {
            var now = DateTime.Now;

            var info = string.Format("Uid: {0}, Originator: {1}, Type: {2}, Data: {3}", uid, Originator, Type, Data);

            var process = Process.GetCurrentProcess();
            var diagnostics = string.Format(CultureInfo.InvariantCulture, "ProcessName: {0}; Id: {1}; PrivateMemory: {2}MB; WorkingSet: {3}MB",
                process.ProcessName, process.Id, Math.Round((double)process.PrivateMemorySize64/1048576, 3), Math.Round((double)process.WorkingSet64/1048576, 3));

            var msg = string.Format(message, args);
            var content = string.Format("Debug [{0} ({1})] - [{2}]  {3}", now.ToString("yyyy-MM-dd HH:mm:ss.fff"), diagnostics, info, msg);

            var log = new AartoErrorLog
            {
                AaProjectId = (int)AartoProjectList.HRKInterface,
                AaErLogPriority = 3,
                AaErLogDate = DateTime.Now,
                AaErLogName = name,
                AaErLogDescription = content,
                LastUser = "HRKInterface"
            };

            using (var scope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                aartoErrorLogService.Save(log);
                scope.Complete();
            }
        }

        #endregion
    }
}
