﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;
using System.Data;
using System.Transactions;

using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.NoticeProcess;

namespace SIL.AARTO.HRKInterface.BLL
{
    [Serializable()]
    [XmlRoot("NoticePaymentCancellationRequest")]
    public class NoticePaymentCancellationRequest
    {
        [XmlElement(ElementName = "HEADER")]
        public PaymentHeader header { get; set; }

        [XmlElement(ElementName = "NoticePaymentCancellation")]
        public PaymentCancelReqBody paymentCancelReqBody { get; set; }
    }

    [Serializable()]
    public class PaymentCancelReqBody
    {
        [XmlElement(ElementName = "CancellationReason")]
        public string CancellationReason { get; set; }

        [XmlElement(ElementName = "CancelledDate")]
        public string CancelledDate { get; set; }

        [XmlElement(ElementName = "NoticeNumber")]
        public string NoticeNumber { get; set; }

        [XmlElement(ElementName = "ReceiptNumber")]
        public string ReceiptNumber { get; set; }
    }

    [Serializable()]
    public class PaymentCancelResBody
    {
        [XmlElement(ElementName = "CancellationInd")]
        public string CancellationInd { get; set; }

        [XmlElement(ElementName = "NoticeNumber")]
        public string NoticeNumber { get; set; }
    }

    [Serializable()]
    [XmlRoot("PaymentCancellationResponse")]
    public class PaymentCancellationResponse
    {
        [XmlElement(ElementName = "HEADER")]
        public PaymentHeader header { get; set; }

        [XmlElement(ElementName = "NoticePaymentCancellation")]
        public PaymentCancelResBody paymentCancelResBody { get; set; }

    }

    public class PaymentCancellation
    {
        private readonly string connectionSttring = string.Empty;
        private bool isCommit = false;

        public PaymentCancellation(string connStr)
        {
            this.connectionSttring = connStr;
        }

        public XmlDocument DoTestPaymentCancellation(XmlDocument xmlDoc, bool isCommit)
        {
            return DoPaymentCancellation(xmlDoc, isCommit);
        }

        public XmlDocument DoPaymentCancellation(XmlDocument xmlDoc, bool isCommit = true)
        {
            this.isCommit = isCommit;
            if (this.isCommit)
            {
                AddHRKHistoryFile(xmlDoc, "Request");
            }

            PaymentCancellationResponse response = null;
            XmlDocument returnXml = null;
            try
            {
                NoticePaymentCancellationRequest request = XmlUtility.DeserializeXml<NoticePaymentCancellationRequest>(xmlDoc);
                if (request.header == null)
                    request.header = new PaymentHeader();

                if (request.header.ClientData == null) request.header.ClientData = "";
                if (request.header.LocalAuthorityCode == null) request.header.LocalAuthorityCode = "";
                if (request.header.ResultCode == null) request.header.ResultCode = "";
                if (request.header.ServerData == null) request.header.ServerData = "";
                if (request.header.ServiceProvider == null) request.header.ServiceProvider = "";
                if (request.header.VersionControl == null) request.header.VersionControl = "";

                if (request.paymentCancelReqBody == null)
                    request.paymentCancelReqBody = new PaymentCancelReqBody();

                if (request.paymentCancelReqBody.CancellationReason == null) request.paymentCancelReqBody.CancellationReason = "";
                if (request.paymentCancelReqBody.CancelledDate == null) request.paymentCancelReqBody.CancelledDate = "";
                if (request.paymentCancelReqBody.NoticeNumber == null) request.paymentCancelReqBody.NoticeNumber = "";
                if (request.paymentCancelReqBody.ReceiptNumber == null) request.paymentCancelReqBody.ReceiptNumber = "";

                response = new PaymentCancellationResponse();
                response.header = new PaymentHeader();
                response.header.ClientData = request.header.ClientData;
                response.header.LocalAuthorityCode = request.header.LocalAuthorityCode;
                response.header.ServiceProvider = request.header.ServiceProvider;
                response.header.VersionControl = request.header.VersionControl;
                response.header.ServerData = "";
                response.header.ResultCode = ((int)ResultCodeList.UnrecoverableSystemError).ToString();

                response.paymentCancelResBody = new PaymentCancelResBody();
                response.paymentCancelResBody.NoticeNumber = request.paymentCancelReqBody.NoticeNumber;
                response.paymentCancelResBody.CancellationInd = "N";

                string errorMsg = string.Empty;
                if (XMLValidate.getInstance(this.connectionSttring).Validate<NoticePaymentCancellationRequest>(request, out errorMsg))
                {
                    Woa woa = null;
                    List<SumCharge> sumCharges = new List<SumCharge>();
                    Notice notice = new NoticeService().GetByNotTicketNo(request.paymentCancelReqBody.NoticeNumber).FirstOrDefault();
                    if (notice != null)
                    {
                        NoticeSummons ns = new NoticeSummonsService().GetByNotIntNo(notice.NotIntNo).FirstOrDefault();
                        if (ns != null)
                        {
                            Summons summons = new SummonsService().GetBySumIntNo(ns.SumIntNo);
                            CourtJudgementType cjt = new CourtJudgementTypeService().GetByCrtIntNoCjtCode(summons.CrtIntNo, 49);  // Paid at court

                            sumCharges = new SumChargeService().GetBySumIntNo(ns.SumIntNo).Where(s => s.CjtIntNo == cjt.CjtIntNo).ToList();
 
                            woa = new WoaService().GetBySumIntNo(ns.SumIntNo).Where(m => m.WoaChargeStatus == 955).FirstOrDefault();
                            if (woa != null || (sumCharges != null && sumCharges.Count > 0))
                            {
                                response.header.ResultCode = ((int)ResultCodeList.DataValidationError).ToString();
                                response.header.ServerData = ((int)ErrorCodeList.PaymentCanNoLongerBeCancelled).ToString() + "~" + XMLValidate.getInstance(this.connectionSttring).GetEnumDescription(ErrorCodeList.PaymentCanNoLongerBeCancelled);

                                errorMsg = "PaymentCancellation failed " + response.header.ServerData + " for NoticeNumber " + request.paymentCancelReqBody.NoticeNumber + ", There is a judgement: Paid at court for this WOA";
                                if (this.isCommit)
                                {
                                    Common.WriteLog("PaymentCancellation", "General", errorMsg);
                                }
                                EntLibLogger.WriteLog("General", "HRKInterface", errorMsg);
                            }
                        }
                    }

                    if (woa == null)
                    {
                        if (sumCharges != null && sumCharges.Count > 0)
                        {
                            response.header.ResultCode = ((int)ResultCodeList.DataValidationError).ToString();
                            response.header.ServerData = ((int)ErrorCodeList.PaymentCanNoLongerBeCancelled).ToString() + "~" + XMLValidate.getInstance(this.connectionSttring).GetEnumDescription(ErrorCodeList.PaymentCanNoLongerBeCancelled);

                            errorMsg = "PaymentCancellation failed " + response.header.ServerData + " for NoticeNumber " + request.paymentCancelReqBody.NoticeNumber + ", There is a judgement: Paid at court exists";
                            if (this.isCommit)
                            {
                                Common.WriteLog("PaymentCancellation", "General", errorMsg);
                            }
                            EntLibLogger.WriteLog("General", "HRKInterface", errorMsg);
                        }
                        else
                        {
                            DataRow[] dr = this.GetReceipt(request.paymentCancelReqBody.NoticeNumber, request.paymentCancelReqBody.ReceiptNumber);
                            if (dr != null && dr.Length > 0)
                            {
                                if (dr[dr.Length - 1]["Reversed"].ToString() == "R")
                                {
                                    response.header.ResultCode = ((int)ResultCodeList.DataValidationError).ToString();
                                    response.header.ServerData = ((int)ErrorCodeList.TheLastTransactionWasNotApayment).ToString() + "~" + XMLValidate.getInstance(this.connectionSttring).GetEnumDescription(ErrorCodeList.TheLastTransactionWasNotApayment);

                                    errorMsg = "PaymentCancellation failed " + response.header.ServerData + " for NoticeNumber " + request.paymentCancelReqBody.NoticeNumber;
                                    if (this.isCommit)
                                    {
                                        Common.WriteLog("PaymentCancellation", "General", errorMsg);
                                    }
                                    EntLibLogger.WriteLog("General", "HRKInterface", errorMsg);
                                }
                                else
                                {
                                    DateRulesDetails details = new DateRulesDetails(int.Parse(dr[dr.Length - 1]["AutIntNo"].ToString()), "", "ChgPaidDate", "ReversePaymentDate");
                                    DefaultDateRules dateRule = new DefaultDateRules(details);
                                    dateRule.SetDefaultDateRule();

                                    DateTime cancelledDate = DateTime.Parse(DateTime.Parse(request.paymentCancelReqBody.CancelledDate).ToString("yyyy-MM-dd"));
                                    DateTime rctDate = DateTime.Parse(DateTime.Parse(dr[dr.Length - 1]["RctDate"].ToString()).ToString("yyyy-MM-dd"));
                                    if (cancelledDate <= rctDate.AddDays(details.DtRNoOfDays))
                                    {
                                        if (dr[dr.Length - 1]["Reversed"].ToString() == "N" && dr[dr.Length - 1]["ParentRctIntNo"].ToString() == "0")
                                        {
                                            string receiptID = dr[dr.Length - 1]["RctIntNo"].ToString();
                                            int autIntNo = int.Parse(dr[dr.Length - 1]["AutIntNo"].ToString());
                                            string errMessage = string.Empty;
                                            DateTime date = DateTime.Now;
                                            date = DateTime.Parse(request.paymentCancelReqBody.CancelledDate);
                                            try
                                            {
                                                using (TransactionScope scope = new TransactionScope())
                                                {
                                                    if (this.ProcessReversal(receiptID, autIntNo, request.paymentCancelReqBody.CancellationReason, date, ref errMessage))
                                                    {
                                                        response.header.ResultCode = ((int)ResultCodeList.TransactionCompletedOK).ToString();
                                                        response.paymentCancelResBody.CancellationInd = "Y";

                                                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionSttring);
                                                        punchStatistics.PunchStatisticsTransactionAdd(autIntNo, "HRKInterface", PunchStatisticsTranTypeList.PaymentTrafficCancel);

                                                        errorMsg = "PaymentCancellation complete for NoticeNumber " + request.paymentCancelReqBody.NoticeNumber;
                                                        if (this.isCommit)
                                                        {
                                                            Common.WriteLog("PaymentCancellation", "General", errorMsg);
                                                            scope.Complete();
                                                        }
                                                        EntLibLogger.WriteLog("General", "HRKInterface", errorMsg);
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                errorMsg = "PaymentCancellation failed for NoticeNumber " + request.paymentCancelReqBody.NoticeNumber + ex.ToString();
                                                if (this.isCommit)
                                                {
                                                    Common.WriteLog("PaymentCancellation", "Error", errorMsg);
                                                }
                                                EntLibLogger.WriteLog("Error", "HRKInterface", errorMsg);
                                                response.header.ResultCode = ((int)ResultCodeList.UnrecoverableSystemError).ToString();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        response.header.ResultCode = ((int)ResultCodeList.DataValidationError).ToString();
                                        response.header.ServerData = ((int)ErrorCodeList.PaymentCanNoLongerBeCancelled).ToString() + "~" + XMLValidate.getInstance(this.connectionSttring).GetEnumDescription(ErrorCodeList.PaymentCanNoLongerBeCancelled);

                                        errorMsg = "PaymentCancellation failed " + response.header.ServerData + " for NoticeNumber " + request.paymentCancelReqBody.NoticeNumber;
                                        if (this.isCommit)
                                        {
                                            Common.WriteLog("PaymentCancellation", "General", errorMsg);
                                        }
                                        EntLibLogger.WriteLog("General", "HRKInterface", errorMsg);
                                    }
                                }
                            }
                            else
                            {
                                response.header.ResultCode = ((int)ResultCodeList.RequestedDataNotFound).ToString();

                                errorMsg = "PaymentCancellation failed RequestedDataNotFound for NoticeNumber " + request.paymentCancelReqBody.NoticeNumber;
                                if (this.isCommit)
                                {
                                    Common.WriteLog("PaymentCancellation", "General", errorMsg);
                                }
                                EntLibLogger.WriteLog("General", "HRKInterface", errorMsg);
                            }
                        }
                    }
                }
                else
                {
                    response.header.ResultCode = ((int)ResultCodeList.DataValidationError).ToString();
                    response.header.ServerData = errorMsg;

                    errorMsg = "PaymentCancellation failed " + response.header.ServerData + " for NoticeNumber " + request.paymentCancelReqBody.NoticeNumber;
                    if (this.isCommit)
                    {
                        Common.WriteLog("PaymentCancellation", "General", errorMsg);
                    }
                    EntLibLogger.WriteLog("General", "HRKInterface", errorMsg);
                }
            }
            catch (Exception ex)
            {
                if (this.isCommit)
                {
                    Common.WriteLog("PaymentCancellation", "Error", ex.ToString());
                }
                EntLibLogger.WriteErrorLog(ex, "Error", "HRKInterface");
            }                        
           
            returnXml = XmlUtility.Serialize<PaymentCancellationResponse>(response);

            if (this.isCommit)
            {
                AddHRKHistoryFile(returnXml, "Response");
            }

            return returnXml;
        }

        void AddHRKHistoryFile(XmlDocument xmlDoc, string type)
        {
            HrkHistoryFile hrkFile = new HrkHistoryFile();
            hrkFile.HhfType = type;
            hrkFile.HhfxmlBody = xmlDoc.ChildNodes.Count > 1 ? xmlDoc.ChildNodes[1].OuterXml : xmlDoc.ChildNodes[0].OuterXml;
            hrkFile.HhfCreateDate = DateTime.Now;
            hrkFile.LastUser = "HRKInterface";

            hrkFile = new HrkHistoryFileService().Save(hrkFile);
        }

        private bool IsExistNotice(string ticketNo)
        {
            bool ret = false;

            TList<Notice> list = new NoticeService().GetByNotTicketNo(ticketNo);
            if (list != null && list.Count > 0)
            {
                ret = true;
            }

            return ret;
        }

        private DataRow[] GetReceipt(string ticketNo, string referenceNumber)
        {
            Stalberg.TMS.CashReceiptDB db = new Stalberg.TMS.CashReceiptDB(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
            DataSet ds = db.ReceiptEnquiryOnTicketNumber(ticketNo);

            DataRow[] dr = ds.Tables[0].Select("ReferenceNumber = '" + referenceNumber + "'", "RctIntNo Asc");
            return dr;
        }

        private bool ProcessReversal(string receiptID, int autIntNo, string cancellationReason, DateTime reversalDate, ref string errMessage)
        {
            bool ret = false;

            StringBuilder sb = new StringBuilder(string.Empty);

            sb = new StringBuilder("<root>");
            sb.Append("<receipt rctintno=\"");
            sb.Append(receiptID);
            sb.Append("\"");
            sb.Append(" />");
            sb.Append("</root>");

            string receiptList = sb.ToString();

            Stalberg.TMS.CashReceiptDB db = new Stalberg.TMS.CashReceiptDB(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);

            List<Int32> receipts = new List<Int32>();
            receipts = db.ProcessReceiptReversal(receiptList, 0, "HRKInterface", autIntNo, reversalDate, ref errMessage, cancellationReason);

            if (receipts[0] > 0)
            {
                ret = true;
            }

            return ret;
        }
    }
}
