﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Schema;
using System.Configuration;
using System.IO;

using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using Stalberg.TMS.Data.Util;


namespace SIL.AARTO.HRKInterface.BLL
{
    public enum SERVICE_TYPE
    {
        NoticeEnquiry,
        NoticePayment,
        PaymentCancellation
    }

    public class XMLValidate
    {
        // 2014-10-15, Oscar added
        readonly AuthorityService authorityService = new AuthorityService();
        readonly RequestTypeService requestTypeService = new RequestTypeService();

        static XMLValidate instance = null;
        static string connectionString = string.Empty;
        string XSDFolder = AppDomain.CurrentDomain.BaseDirectory + "XSD";

        public static XMLValidate getInstance(string connStr)
        {
            if (instance == null)
            {
                instance = new XMLValidate();
                connectionString = connStr;
            }

            return instance;
        }


        public bool CheckXSDVersion(string version, SERVICE_TYPE serviceType)
        {
            bool ret = false;
            string xsdFile = serviceType.ToString() + ".xsd";

            if (File.Exists(Path.Combine(XSDFolder, version, xsdFile))
                && File.Exists(Path.Combine(XSDFolder, version, "Types.xsd"))
                && File.Exists(Path.Combine(XSDFolder, version, "Enumerations.xsd")))
            {
                ret = true;
            }

            return ret;
        }


        public bool Validate<T>(T obj, out string errorMsg)
        {
            bool ret = true;
            errorMsg = string.Empty;
            Dictionary<string, string> dictionary = new Dictionary<string, string>(); ;

            if (obj is NoticeEnquiryRequest)
            {
                NoticeEnquiryRequest noticeEnquiryRequest = obj as NoticeEnquiryRequest;
                dictionary = ValidateNoticeEnquiry(noticeEnquiryRequest);
            }
            else if (obj is NoticePaymentRequest)
            {
                NoticePaymentRequest noticePaymentRequest = obj as NoticePaymentRequest;
                dictionary = ValidateNoticePayment(noticePaymentRequest);
            }
            else if (obj is NoticePaymentCancellationRequest)
            {
                NoticePaymentCancellationRequest noticePaymentCancellationRequest = obj as NoticePaymentCancellationRequest;
                dictionary = ValidatePaymentCancellation(noticePaymentCancellationRequest);
            }

            if (dictionary.Count > 0)
            {
                ret = false;
                foreach (var dic in dictionary)
                {
                    errorMsg = dic.Key + "~" + dic.Value;
                    break;
                }
            }

            return ret;

        }

        private Dictionary<string, string> ValidateNoticeEnquiry(NoticeEnquiryRequest noticeEnquiryRequest)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            if (!CheckXSDVersion(noticeEnquiryRequest.header.VersionControl, SERVICE_TYPE.NoticeEnquiry))
            {
                dictionary.Add(((int)ErrorCodeList.InvalidVersionControl).ToString(), GetEnumDescription(ErrorCodeList.InvalidVersionControl));
                return dictionary;
            }

            //Authority authority = new AuthorityService().GetByAutNo(noticeEnquiryRequest.header.LocalAuthorityCode);
            // 2014-10-15, Oscar changed
            var authority = this.authorityService.GetByAutNo(noticeEnquiryRequest.header.LocalAuthorityCode);
            if (authority == null)
            {
                dictionary.Add(((int)ErrorCodeList.InvalidLocalAuthorityCode).ToString(), GetEnumDescription(ErrorCodeList.InvalidLocalAuthorityCode));
                return dictionary;
            }

            if (!XmlUtility.GetEnumValues<ServiceProviderList>().Contains(noticeEnquiryRequest.header.ServiceProvider))
            {
                dictionary.Add(((int)ErrorCodeList.InvalidServiceProvider).ToString(), GetEnumDescription(ErrorCodeList.InvalidServiceProvider));
                return dictionary;
            }

            if (!XmlUtility.GetEnumValues<PaymentOriginatorList>().Contains(noticeEnquiryRequest.noticeEnquiryBody.PaymentOriginator))
            {
                dictionary.Add(((int)ErrorCodeList.InvalidPaymentOriginator).ToString(), GetEnumDescription(ErrorCodeList.InvalidPaymentOriginator));
                return dictionary;
            }

            //RequestType reqType = new RequestTypeService().GetByRtCode(noticeEnquiryRequest.noticeEnquiryBody.RequestType);
            // 2014-10-15, Oscar changed
            var reqType = this.requestTypeService.GetByRtCode(noticeEnquiryRequest.noticeEnquiryBody.RequestType);
            if (reqType == null)
            {
                dictionary.Add(((int)ErrorCodeList.InvalidRequestType).ToString(), GetEnumDescription(ErrorCodeList.InvalidRequestType));
                return dictionary;
            }

            RequestTypeList requestType = (RequestTypeList)Enum.Parse(typeof(RequestTypeList), noticeEnquiryRequest.noticeEnquiryBody.RequestType, true);
            if (requestType == RequestTypeList.N)
            {
                string noticeNumber = noticeEnquiryRequest.noticeEnquiryBody.RequestData.Trim();
                string validateType = "";
                if (noticeNumber.IndexOf('/') > 0)
                {
                    validateType = "cdv";
                }
                if (Validation.GetInstance(connectionString).ValidateTicketNumber(noticeNumber, validateType))
                {
                    dictionary.Add(((int)ErrorCodeList.InvalidNoticeNumber).ToString(), GetEnumDescription(ErrorCodeList.InvalidNoticeNumber));
                    return dictionary;
                }
            }
            if (requestType == RequestTypeList.I)
            {
                string identityNumber = noticeEnquiryRequest.noticeEnquiryBody.RequestData.Trim();
                long number = 0;
                if (!long.TryParse(identityNumber, out number))
                {
                    dictionary.Add(((int)ErrorCodeList.InvalidIdentityNumber).ToString(), GetEnumDescription(ErrorCodeList.InvalidIdentityNumber));
                    return dictionary;
                }
            }
            if (requestType == RequestTypeList.R)
            {
                string vehicleRegNo = noticeEnquiryRequest.noticeEnquiryBody.RequestData.Trim();
                if (!System.Text.RegularExpressions.Regex.IsMatch(vehicleRegNo, @"^[A-Za-z0-9]+$") || vehicleRegNo.Length >= 11)
                {
                    dictionary.Add(((int)ErrorCodeList.InvalidVehicleRegistrationNumber).ToString(), GetEnumDescription(ErrorCodeList.InvalidVehicleRegistrationNumber));
                    return dictionary;
                }
            }

            return dictionary;
        }

        private Dictionary<string, string> ValidateNoticePayment(NoticePaymentRequest noticePaymentRequest)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            if (noticePaymentRequest != null && noticePaymentRequest.NoticePaymentHeader != null && noticePaymentRequest.NoticePaymentRequestBody != null)
            {
                if (XmlUtility.GetEnumValues<PaymentTypeList>().Contains(noticePaymentRequest.NoticePaymentRequestBody.PaymentType) == false)
                {
                    dictionary.Add(((int)ErrorCodeList.InvalidTenderType).ToString(), GetEnumDescription(ErrorCodeList.InvalidTenderType));
                    return dictionary;
                }
                if (XmlUtility.GetEnumValues<PaymentOriginatorList>().Contains(noticePaymentRequest.NoticePaymentRequestBody.PaymentOriginator) == false)
                {
                    dictionary.Add(((int)ErrorCodeList.InvalidTransactionOriginator).ToString(), GetEnumDescription(ErrorCodeList.InvalidTransactionOriginator));
                    return dictionary;
                }
                if (XmlUtility.GetEnumValues<PayPointTypeList>().Contains(noticePaymentRequest.NoticePaymentRequestBody.PayPointType) == false)
                {
                    dictionary.Add(((int)ErrorCodeList.InvalidTypeOfPayPoint).ToString(), GetEnumDescription(ErrorCodeList.InvalidTypeOfPayPoint));
                    return dictionary;
                }
                if (XmlUtility.GetEnumValues<ServiceProviderList>().Contains(noticePaymentRequest.NoticePaymentHeader.ServiceProvider) == false)
                {
                    dictionary.Add(((int)ErrorCodeList.InvalidServiceProvider).ToString(), GetEnumDescription(ErrorCodeList.InvalidServiceProvider));
                    return dictionary;
                }
                if (String.IsNullOrEmpty(noticePaymentRequest.NoticePaymentRequestBody.TotalAmount))
                {
                    dictionary.Add(((int)ErrorCodeList.InvalidAmount).ToString(), GetEnumDescription(ErrorCodeList.InvalidAmount));
                    return dictionary;
                }
                else
                {
                    decimal totalAmount = 0;
                    Decimal.TryParse(noticePaymentRequest.NoticePaymentRequestBody.TotalAmount, out totalAmount);
                    if (totalAmount <= 0)
                    {
                        dictionary.Add(((int)ErrorCodeList.InvalidAmount).ToString(), GetEnumDescription(ErrorCodeList.InvalidAmount));
                        return dictionary;
                    }
                }
                if (String.IsNullOrEmpty(noticePaymentRequest.NoticePaymentHeader.LocalAuthorityCode))
                {
                    dictionary.Add(((int)ErrorCodeList.InvalidLocalAuthorityCode).ToString(), GetEnumDescription(ErrorCodeList.InvalidLocalAuthorityCode));
                    return dictionary;
                }
                if (String.IsNullOrEmpty(noticePaymentRequest.NoticePaymentRequestBody.NoticeNumber))
                {
                    dictionary.Add(((int)ErrorCodeList.InvalidNoticeNumber).ToString(), GetEnumDescription(ErrorCodeList.InvalidNoticeNumber));
                    return dictionary;
                }
                else
                {
                    string validateType = "cdv";
                    string noticeNumber = noticePaymentRequest.NoticePaymentRequestBody.NoticeNumber;
                    if (noticeNumber.IndexOf("/") > 0) { validateType = "cdv"; }
                    else { validateType = "verhoeff"; }
                    if (Validation.GetInstance(connectionString).ValidateTicketNumber(noticeNumber, validateType))
                    {
                        dictionary.Add(((int)ErrorCodeList.InvalidNoticeNumber).ToString(), GetEnumDescription(ErrorCodeList.InvalidNoticeNumber));
                        return dictionary;
                    }
                    else
                    {
                        if (noticeNumber.IndexOf("/") > 0)
                        {
                            if (noticeNumber.Split('/').Length != 4)
                            {
                                dictionary.Add(((int)ErrorCodeList.InvalidNoticeNumber).ToString(), GetEnumDescription(ErrorCodeList.InvalidNoticeNumber));
                                return dictionary;
                            }
                        }
                        else
                        {
                            if (noticeNumber.Split('-').Length != 4)
                            {
                                dictionary.Add(((int)ErrorCodeList.InvalidNoticeNumber).ToString(), GetEnumDescription(ErrorCodeList.InvalidNoticeNumber));
                                return dictionary;
                            }
                        }
                    }
                }

                DateTime receiptDate = DateTime.Now;
                if (!DateTime.TryParse(noticePaymentRequest.NoticePaymentRequestBody.ReceiptDate, out receiptDate))
                {
                    dictionary.Add(((int)ErrorCodeList.InvalidDateOfPayment).ToString(), GetEnumDescription(ErrorCodeList.InvalidDateOfPayment));
                    return dictionary;
                }
                else
                {
                    //if (noticePaymentRequest.NoticePaymentRequestBody.PaymentOriginator != ((int)SIL.AARTO.DAL.Entities.PaymentOriginatorList.HRK).ToString())
                    //{
                        if (Convert.ToDateTime(noticePaymentRequest.NoticePaymentRequestBody.ReceiptDate) > DateTime.Now.AddHours(5))
                        {
                            dictionary.Add(((int)ErrorCodeList.InvalidDateOfPayment).ToString(), GetEnumDescription(ErrorCodeList.InvalidDateOfPayment));
                            return dictionary;
                        }
                   // }
                }

            }

            return dictionary;
        }


        private Dictionary<string, string> ValidatePaymentCancellation(NoticePaymentCancellationRequest noticePaymentCancellationRequest)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            if (!CheckXSDVersion(noticePaymentCancellationRequest.header.VersionControl, SERVICE_TYPE.NoticeEnquiry))
            {
                dictionary.Add(((int)ErrorCodeList.InvalidVersionControl).ToString(), GetEnumDescription(ErrorCodeList.InvalidVersionControl));
                return dictionary;
            }

            //Authority authority = new AuthorityService().GetByAutNo(noticePaymentCancellationRequest.header.LocalAuthorityCode);
            // 2014-10-15, Oscar changed
            var authority = this.authorityService.GetByAutNo(noticePaymentCancellationRequest.header.LocalAuthorityCode);
            if (authority == null)
            {
                dictionary.Add(((int)ErrorCodeList.InvalidLocalAuthorityCode).ToString(), GetEnumDescription(ErrorCodeList.InvalidLocalAuthorityCode));
                return dictionary;
            }

            if (!XmlUtility.GetEnumValues<ServiceProviderList>().Contains(noticePaymentCancellationRequest.header.ServiceProvider))
            {
                dictionary.Add(((int)ErrorCodeList.InvalidServiceProvider).ToString(), GetEnumDescription(ErrorCodeList.InvalidServiceProvider));
                return dictionary;
            }

            string noticeNumber = noticePaymentCancellationRequest.paymentCancelReqBody.NoticeNumber.Trim();
            string validateType = "";
            if (noticeNumber.IndexOf('/') > 0)
            {
                validateType = "cdv";
            }
            if (Validation.GetInstance(connectionString).ValidateTicketNumber(noticeNumber, validateType))
            {
                dictionary.Add(((int)ErrorCodeList.InvalidNoticeNumber).ToString(), GetEnumDescription(ErrorCodeList.InvalidNoticeNumber));
                return dictionary;
            }

            DateTime date = DateTime.Now;
            if (!DateTime.TryParse(noticePaymentCancellationRequest.paymentCancelReqBody.CancelledDate, out date))
            {
                dictionary.Add(((int)ErrorCodeList.InvalidDateCancelled).ToString(), GetEnumDescription(ErrorCodeList.InvalidDateCancelled));
                return dictionary;
            }
            if (date > DateTime.Now.AddHours(5))
            {
                dictionary.Add(((int)ErrorCodeList.InvalidDateCancelled).ToString(), GetEnumDescription(ErrorCodeList.InvalidDateCancelled));
                return dictionary;
            }

            if (noticePaymentCancellationRequest.paymentCancelReqBody.ReceiptNumber.Trim() == "")
            {
                dictionary.Add(((int)ErrorCodeList.InvalidReceiptNumber).ToString(), GetEnumDescription(ErrorCodeList.InvalidReceiptNumber));
                return dictionary;
            }

            return dictionary;
        }


        private void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Message != null)
            {
                throw new Exception("XSD error");
            }
        }

        public string GetEnumDescription(ErrorCodeList errorCode)
        {
            List<Attribute> attributes = XmlUtility.GetEnumText<EnumTextValueAttribute>(errorCode);
            if (attributes != null && attributes.Count > 0)
                return ((EnumTextValueAttribute)attributes[0]).Text;

            return string.Empty;
        }
    }
    public class ValidateClass
    {
        public ValidateClass(XmlDocument xmlDoc, ref string Msg)
        {
            xmlDoc.Validate(ValidationEventHandler);
            Msg = strMsg;
        }

        string strMsg = null;
        private void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Message != null)
            {
                strMsg += e.Message;
            }
        }
    }
}