﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;

using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.BLL.NoticeProcess;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using Stalberg.TMS.Data.Util;

namespace SIL.AARTO.HRKInterface.BLL
{
    [Serializable()]
    public class NoticeEnquiryBody
    {
        [XmlElement(ElementName = "PaymentOriginator")]
        public string PaymentOriginator { get; set; }

        [XmlElement(ElementName = "RequestData")]
        public string RequestData { get; set; }

        [XmlElement(ElementName = "RequestType")]
        public string RequestType { get; set; }
    }

    [Serializable()]
    [XmlRoot("NoticeEnquiryRequest")]
    public class NoticeEnquiryRequest
    {
        [XmlElement(ElementName = "HEADER")]
        public PaymentHeader header { get; set; }

        [XmlElement(ElementName = "NoticeEnquiry")]
        public NoticeEnquiryBody noticeEnquiryBody { get; set; }
    }

    [Serializable()]
    [XmlRoot("NoticeEnquiryResponse")]
    public class NoticeEnquiryResponse
    {
        [XmlElement(ElementName = "HEADER")]
        public PaymentHeader header { get; set; }

        [XmlElement(ElementName = "NoticeList")]
        public List<NoticeList> NoticeList { get; set; }
    }

    public class NoticeList
    {
        [XmlElement(ElementName = "CameraSerial")]
        public string CameraSerial { get; set; }

        [XmlElement(ElementName = "CaseNumber")]
        public string CaseNumber { get; set; }

        [XmlElement(ElementName = "ContemptAmount")]
        public decimal ContemptAmount { get; set; }

        [XmlElement(ElementName = "Contractor")]
        public string Contractor { get; set; }

        [XmlElement(ElementName = "CourtCode")]
        public string CourtCode { get; set; }

        [XmlElement(ElementName = "CourtDate")]
        public DateTime? CourtDate { get; set; }

        [XmlElement(ElementName = "FilmNumber")]
        public string FilmNumber { get; set; }

        [XmlElement(ElementName = "LocationDescription")]
        public string LocationDescription { get; set; }

        [XmlElement(ElementName = "NoticeNumber")]
        public string NoticeNumber { get; set; }

        [XmlElement(ElementName = "OffenceDate")]
        public DateTime OffenceDate { get; set; }

        [XmlElement(ElementName = "OffenderIDNumber")]
        public string OffenderIDNumber { get; set; }

        [XmlElement(ElementName = "PaymentInd")]
        public string PaymentInd { get; set; }

        [XmlElement(ElementName = "RDChequeNoticeNumber")]
        public string RDChequeNoticeNumber { get; set; }

        [XmlElement(ElementName = "SummonsNumber")]
        public string SummonsNumber { get; set; }

        [XmlElement(ElementName = "TotalAmount")]
        public decimal TotalAmount { get; set; }

        [XmlElement(ElementName = "VehicleRegNo")]
        public string VehicleRegNo { get; set; }

        [XmlElement(ElementName = "WarrantPending")]
        public string WarrantPending { get; set; }

        [XmlElement(ElementName = "WOANumber")]
        public string WOANumber { get; set; }
    }

    public class NoticeEnquiry
    {
        // 2014-10-15, Oscar added
        readonly NoticeService noticeService = new NoticeService();
        readonly NoticeSummonsService noticeSummonsService = new NoticeSummonsService();
        readonly SummonsService summonsService = new SummonsService();
        readonly WoaService woaService = new WoaService();
        readonly NoticeFrameService noticeFrameService = new NoticeFrameService();
        readonly FrameService frameService = new FrameService();
        readonly FilmService filmService = new FilmService();
        readonly ChargeService chargeService = new ChargeService();
        readonly ManualCaptureTypeService manualCaptureTypeService = new ManualCaptureTypeService();
        readonly OwnerService ownerService = new OwnerService();
        readonly DriverService driverService = new DriverService();
        readonly ProxyService proxyService = new ProxyService();
        readonly AccusedService accusedService = new AccusedService();
        readonly SearchIdService searchIdService = new SearchIdService();
        readonly SearchRegnoService searchRegnoService = new SearchRegnoService();
        readonly SumChargeService sumChargeService = new SumChargeService();
        readonly ScDeferredPaymentsService scDeferredPaymentsService = new ScDeferredPaymentsService();
        readonly HrkHistoryFileService hrkHistoryFileService = new HrkHistoryFileService();

        private readonly string connectionSttring = string.Empty;

        public NoticeEnquiry(string connStr)
        {
            this.connectionSttring = connStr;
        }

        public XmlDocument DoNoticeEnquiry(XmlDocument xmlDoc)
        {
            NoticeEnquiryResponse response = null;
            XmlDocument returnXml = null;
            string[] noticeIDList = null;

            // 2014-10-16, Oscar added for diagnostic
            Common.WriteDebug("NoticeEnquiry", "Start AddHRKHistoryFile for Request at NoticeEnquiry.DoNoticeEnquiry, xml length: {0}.", xmlDoc.ChildNodes[xmlDoc.ChildNodes.Count > 1 ? 1 : 0].OuterXml.Length);

            AddHRKHistoryFile(xmlDoc, "Request");

            // 2014-10-16, Oscar added for diagnostic
            Common.WriteDebug("NoticeEnquiry", "End AddHRKHistoryFile for Request at NoticeEnquiry.DoNoticeEnquiry.");

            try
            {
                NoticeEnquiryRequest request = XmlUtility.DeserializeXml<NoticeEnquiryRequest>(xmlDoc);
                if (request.header == null)
                    request.header = new PaymentHeader();

                if (request.header.ClientData == null) request.header.ClientData = "";
                if (request.header.LocalAuthorityCode == null) request.header.LocalAuthorityCode = "";
                if (request.header.ResultCode == null) request.header.ResultCode = "";
                if (request.header.ServerData == null) request.header.ServerData = "";
                if (request.header.ServiceProvider == null) request.header.ServiceProvider = "";
                if (request.header.VersionControl == null) request.header.VersionControl = "";

                if (request.noticeEnquiryBody == null)
                    request.noticeEnquiryBody = new NoticeEnquiryBody();

                if (request.noticeEnquiryBody.PaymentOriginator == null) request.noticeEnquiryBody.PaymentOriginator = "";
                if (request.noticeEnquiryBody.RequestData == null) request.noticeEnquiryBody.RequestData = "";
                if (request.noticeEnquiryBody.RequestType == null) request.noticeEnquiryBody.RequestType = "";

                // 2014-10-16, Oscar added for diagnostic
                Common.Originator = request.noticeEnquiryBody.PaymentOriginator;
                Common.Type = request.noticeEnquiryBody.RequestType;
                Common.Data = request.noticeEnquiryBody.RequestData;

                response = new NoticeEnquiryResponse();
                response.header = new PaymentHeader();
                response.header.ClientData = request.header.ClientData;
                response.header.LocalAuthorityCode = request.header.LocalAuthorityCode;
                response.header.ServiceProvider = request.header.ServiceProvider;
                response.header.VersionControl = request.header.VersionControl;
                response.header.ServerData = "";
                response.header.ResultCode = ((int)ResultCodeList.UnrecoverableSystemError).ToString();

                string errorMsg = string.Empty;

                // 2014-10-16, Oscar added for diagnostic
                Common.WriteDebug("NoticeEnquiry", "Start Validate at NoticeEnquiry.DoNoticeEnquiry.");

                if (XMLValidate.getInstance(this.connectionSttring).Validate<NoticeEnquiryRequest>(request, out errorMsg))
                {
                    // 2014-10-16, Oscar added for diagnostic
                    Common.WriteDebug("NoticeEnquiry", "End Validate at NoticeEnquiry.DoNoticeEnquiry.");

                    RequestTypeList requestType = (RequestTypeList)Enum.Parse(typeof(RequestTypeList), request.noticeEnquiryBody.RequestType, true);
                    string num = request.noticeEnquiryBody.RequestData.Trim();
                    if (requestType == RequestTypeList.N)
                    {
                        // 2014-10-16, Oscar added for diagnostic
                        Common.WriteDebug("NoticeEnquiry", "Start GetNoticeByTicket at NoticeEnquiry.DoNoticeEnquiry.");

                        TList<Notice> noticeList = this.GetNoticeByTicket(num);
                        if (noticeList != null && noticeList.Count > 0)
                        {
                            noticeIDList = new string[1];
                            noticeIDList[0] = noticeList[0].NotIntNo.ToString();
                        }

                        // 2014-10-16, Oscar added for diagnostic
                        Common.WriteDebug("NoticeEnquiry", "End GetNoticeByTicket at NoticeEnquiry.DoNoticeEnquiry.");
                    }
                    else if (requestType == RequestTypeList.I)
                    {
                        // 2014-10-16, Oscar added for diagnostic
                        Common.WriteDebug("NoticeEnquiry", "Start GetNoticeByIdentityNo at NoticeEnquiry.DoNoticeEnquiry.");

                        TList<SearchId> searchList = this.GetNoticeByIdentityNo(num);
                        if (searchList != null && searchList.Count > 0)
                        {
                            noticeIDList = new string[searchList.Count];
                            for (int i = 0; i < searchList.Count; i++)
                            {
                                noticeIDList[i] = searchList[i].NotIntNo.ToString();
                            }
                        }

                        // 2014-10-16, Oscar added for diagnostic
                        Common.WriteDebug("NoticeEnquiry", "End GetNoticeByIdentityNo at NoticeEnquiry.DoNoticeEnquiry.");
                    }
                    else if (requestType == RequestTypeList.R)
                    {
                        // 2014-10-16, Oscar added for diagnostic
                        Common.WriteDebug("NoticeEnquiry", "Start GetNoticeByRegno at NoticeEnquiry.DoNoticeEnquiry.");

                        TList<SearchRegno> searchList = this.GetNoticeByRegno(num);
                        if (searchList != null && searchList.Count > 0)
                        {
                            noticeIDList = new string[searchList.Count];
                            for (int i = 0; i < searchList.Count; i++)
                            {
                                noticeIDList[i] = searchList[i].NotIntNo.ToString();
                            }
                        }

                        // 2014-10-16, Oscar added for diagnostic
                        Common.WriteDebug("NoticeEnquiry", "End GetNoticeByRegno at NoticeEnquiry.DoNoticeEnquiry.");
                    }

                    // 2014-10-16, Oscar added for diagnostic
                    Common.WriteDebug("NoticeEnquiry", "Start GetNoticeList at NoticeEnquiry.DoNoticeEnquiry.");

                    response.NoticeList = this.GetNoticeList(noticeIDList, request.noticeEnquiryBody.PaymentOriginator);

                    // 2014-10-16, Oscar added for diagnostic
                    Common.WriteDebug("NoticeEnquiry", "End GetNoticeList at NoticeEnquiry.DoNoticeEnquiry.");

                    if (response.NoticeList == null || response.NoticeList.Count == 0)
                    {
                        response.header.ResultCode = ((int)ResultCodeList.RequestedDataNotFound).ToString();
                    }
                    else
                    {
                        //validate deferred judgement (add by Rachel 2014-09-17 bontq1512)
                        for (int i = 0; i < response.NoticeList.Count; i++)
                        {
                            // 2014-10-16, Oscar added for diagnostic
                            Common.WriteDebug("NoticeEnquiry", "Start GetScDeferredPaymentsByNoticeNo at NoticeEnquiry.DoNoticeEnquiry.");

                            TList<ScDeferredPayments> scdeferredpaymentList = GetScDeferredPaymentsByNoticeNo(response.NoticeList[i].NoticeNumber);
                            if (scdeferredpaymentList != null && scdeferredpaymentList.Count > 0)
                            {
                                response.header.ResultCode = ((int)ResultCodeList.DataValidationError).ToString();
                                response.header.ServerData = String.Format("{0}~{1}", (int)ErrorCodeList.TransactionNotAllowedInCurrentsystemState, ErrorCodeList.TransactionNotAllowedInCurrentsystemState.ToString());
                                Common.WriteLog("DeferredPayment", "Error", "Notice Payment Interface:Payment not permitted - deferred judgment settlement required for NoticeNumber " + response.NoticeList[i].NoticeNumber);
                                break;
                            }
                            else
                            {
                                response.header.ResultCode = ((int)ResultCodeList.TransactionCompletedOK).ToString();
                            }

                            // 2014-10-16, Oscar added for diagnostic
                            Common.WriteDebug("NoticeEnquiry", "End GetScDeferredPaymentsByNoticeNo at NoticeEnquiry.DoNoticeEnquiry.");
                        }
                        //end validate deferred judgement (add by Rachel 2014-09-17 bontq1512)

                        //response.header.ResultCode = ((int)ResultCodeList.TransactionCompletedOK).ToString();
                    }
                }
                else
                {
                    // 2014-10-16, Oscar added for diagnostic
                    Common.WriteDebug("NoticeEnquiry", "End Validate at NoticeEnquiry.DoNoticeEnquiry.");

                    response.header.ResultCode = ((int)ResultCodeList.DataValidationError).ToString();
                    response.header.ServerData = errorMsg;
                }

                returnXml = XmlUtility.Serialize<NoticeEnquiryResponse>(response);
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteErrorLog(ex, "Error", "HRKInterface");
            }

            // 2014-10-16, Oscar added for diagnostic
            Common.WriteDebug("NoticeEnquiry", "Start AddHRKHistoryFile for Response at NoticeEnquiry.DoNoticeEnquiry, xml length: {0}.", returnXml.ChildNodes[returnXml.ChildNodes.Count > 1 ? 1 : 0].OuterXml.Length);

            AddHRKHistoryFile(returnXml, "Response");

            // 2014-10-16, Oscar added for diagnostic
            Common.WriteDebug("NoticeEnquiry", "End AddHRKHistoryFile for Response at NoticeEnquiry.DoNoticeEnquiry.");

            return returnXml;
        }

        public List<NoticeList> GetNoticeList(string[] noticeIDList, string paymentOriginator)
        {
            List<NoticeList> list = null;
            //Jake 2014-11-06 modified added noticeIDList.Length>0
            if (noticeIDList != null && noticeIDList.Length > 0)
            {
                NoticeQuery query = new NoticeQuery();
                query.AppendIn(NoticeColumn.NotIntNo, noticeIDList);
                //Jake 2014-11-10 added NoticeStatus>=10
                query.AppendGreaterThanOrEqual(NoticeColumn.NoticeStatus, ((int)ChargeStatusList.CameraViolationLoaded).ToString());
                //TList<Notice> nlist = new NoticeService().Find(query as IFilterParameterCollection);
                // 2014-10-15, Oscar changed
                var nlist = this.noticeService.Find(query);
                if (nlist != null && nlist.Count > 0)
                {
                    list = new List<NoticeList>();
                    for (int i = 0; i < nlist.Count; i++)
                    {

                        NoticeList noticeList = new NoticeList();
                        Notice notice = nlist[i];
                        //Jake 2014-11-10 added , check notice number
                        if (String.IsNullOrEmpty(notice.NotTicketNo))
                            continue;

                        Summons summons = GetSummons(notice);
                        Woa woa = GetWoa(summons);
                        Frame frame = GetFrame(notice);
                        Film film = GetFilm(notice, frame);

                        string paymentInd = "";
                        decimal totalAmount = 0;
                        decimal contemptAmoumnt = 0;

                        GetAmountDue(notice, summons, paymentOriginator, out paymentInd, out totalAmount, out contemptAmoumnt);

                        string identityNumber = "";
                        GetIdentity(notice, summons, out identityNumber);

                        if (notice.NotFilmType != "M" && notice.NotFilmType != "H")
                        {
                            noticeList.CameraSerial = notice.NotCamSerialNo;
                        }
                        else
                        {
                            noticeList.CameraSerial = "";
                        }

                        if (summons != null)
                        {
                            //noticeList.CaseNumber = summons.SumCaseNo != null ? summons.SumCaseNo : "";
                            noticeList.CaseNumber = summons.SumCaseNo != null ? "100" : "0";
                        }
                        else
                        {
                            noticeList.CaseNumber = "0";
                        }

                        noticeList.ContemptAmount = contemptAmoumnt;
                        noticeList.Contractor = SupplierCodeList.Q.ToString();
                        noticeList.CourtCode = notice.NotCourtNo != null ? notice.NotCourtNo : "";

                        if (summons != null && summons.SumCourtDate != null)
                        {
                            noticeList.CourtDate = summons.SumCourtDate.Value;
                        }

                        if (film != null)
                        {
                            noticeList.FilmNumber = film.FilmNo;
                        }
                        else
                        {
                            noticeList.FilmNumber = "";
                        }

                        noticeList.OffenderIDNumber = identityNumber;
                        noticeList.LocationDescription = notice.NotLocDescr != null ? notice.NotLocDescr : "";
                        noticeList.NoticeNumber = notice.NotTicketNo != null ? notice.NotTicketNo : "";
                        noticeList.OffenceDate = notice.NotOffenceDate;
                        noticeList.PaymentInd = paymentInd;
                        noticeList.RDChequeNoticeNumber = "";

                        if (summons != null)
                        {
                            noticeList.SummonsNumber = summons.SummonsNo;
                        }
                        else
                        {
                            noticeList.SummonsNumber = "";
                        }

                        noticeList.TotalAmount = totalAmount;
                        noticeList.VehicleRegNo = notice.NotRegNo != null ? notice.NotRegNo : "";

                        //Jake 2013-10-22 removed ,we need to allow payment even if there is a WOA exists
                        //if (notice.NoticeStatus >= (int)ChargeStatusList.WarrantNoticeGenerated && notice.NoticeStatus < (int)ChargeStatusList.Completion)
                        //{
                        //    noticeList.WarrantPending = "Y";
                        //}
                        //else
                        //{
                        noticeList.WarrantPending = "N";
                        //}

                        if (woa != null)
                        {
                            noticeList.WOANumber = woa.WoaNumber;
                        }
                        else
                        {
                            noticeList.WOANumber = "";
                        }


                        list.Add(noticeList);
                    }
                }
            }

            return list;
        }

        private TList<Notice> GetNoticeByTicket(string ticket)
        {
            //TList<Notice> list = new NoticeService().GetByNotTicketNo(ticket);
            // 2014-10-15, Oscar changed
            var list = this.noticeService.GetByNotTicketNo(ticket);
            return list;
        }

        private TList<SearchId> GetNoticeByIdentityNo(string idNumber)
        {
            //SearchIdQuery query = new SearchIdQuery();
            //query.Append(SearchIdColumn.IdNumber, idNumber);
            //TList<SearchId> list = new SearchIdService().Find(query as IFilterParameterCollection);
            // 2014-10-15, Oscar changed
            var list = this.searchIdService.GetByIdNumber(idNumber);
            return list;
        }

        private TList<SearchRegno> GetNoticeByRegno(string Regno)
        {
            //SearchRegnoQuery query = new SearchRegnoQuery();
            //query.Append(SearchRegnoColumn.RegNo, Regno);
            //TList<SearchRegno> list = new SearchRegnoService().Find(query as IFilterParameterCollection);
            // 2014-10-15, Oscar changed
            var list = this.searchRegnoService.GetByRegNo(Regno);
            return list;
        }

        //Jake 2014-11-05 modified Find function
        //validate deferred judgement (add by Rachel 2014-09-17 bontq1512)
        private TList<ScDeferredPayments> GetScDeferredPaymentsByNoticeNo(string noticeNo)
        {
            TList<ScDeferredPayments> scdeferredpaymentsList = null;
            //Jake 2014-11-07 added ,if noticeNo is empty ,return
            if (String.IsNullOrEmpty(noticeNo)) return scdeferredpaymentsList;
            SummonsQuery summonsQuery = new SummonsQuery();
            summonsQuery.AppendEquals(SummonsColumn.SumNoticeNo, noticeNo);
            //TList<Summons> summonsList = new SummonsService().Find(summonsQuery as IFilterParameterCollection);
            // 2014-10-15, Oscar changed
            var summonsList = this.summonsService.Find(summonsQuery);
            //var summonsList = this.summonsService.Find(String.Format("SumNoticeNo='{0}'", noticeNo));
            if (summonsList != null && summonsList.Count > 0)
            {
                //SumChargeQuery sumchargeQuery = new SumChargeQuery();
                //sumchargeQuery.Append(SumChargeColumn.SumIntNo, summonsList[0].SumIntNo.ToString());
                //TList<SumCharge> sumchargeList = new SumChargeService().Find(sumchargeQuery as IFilterParameterCollection);
                // 2014-10-15, Oscar changed
                var sumchargeList = this.sumChargeService.GetBySumIntNo(summonsList[0].SumIntNo);
                if (sumchargeList != null && sumchargeList.Count > 0)
                {
                    //ScDeferredPaymentsQuery scdeferredpaymentQuery = new ScDeferredPaymentsQuery();
                    //scdeferredpaymentQuery.Append(ScDeferredPaymentsColumn.SchIntNo, sumchargeList[0].SchIntNo.ToString());
                    //scdeferredpaymentsList = new ScDeferredPaymentsService().Find(scdeferredpaymentQuery as IFilterParameterCollection);
                    // 2014-10-15, Oscar changed
                    scdeferredpaymentsList = this.scDeferredPaymentsService.GetBySchIntNo(sumchargeList[0].SchIntNo);
                }
            }
            return scdeferredpaymentsList;
        }
        //end validate deferred judgement (add by Rachel 2014-09-17 bontq1512)

        private Summons GetSummons(Notice notice)
        {
            Summons summons = null;

            //NoticeSummonsQuery noticesummonsQuery = new NoticeSummonsQuery();
            //noticesummonsQuery.Append(NoticeSummonsColumn.NotIntNo, notice.NotIntNo.ToString());
            //TList<NoticeSummons> noticesummonsList = new NoticeSummonsService().Find(noticesummonsQuery as IFilterParameterCollection);
            // 2014-10-15, Oscar changed
            var noticesummonsList = this.noticeSummonsService.GetByNotIntNo(notice.NotIntNo);

            if (noticesummonsList != null && noticesummonsList.Count > 0)
            {
                //summons = new SummonsService().GetBySumIntNo(noticesummonsList[0].SumIntNo);
                // 2014-10-15, Oscar changed
                summons = this.summonsService.GetBySumIntNo(noticesummonsList[0].SumIntNo);
            }
            return summons;
        }

        // Jake 2013-10-18 modified this function to apply bench woa or double woa
        private Woa GetWoa(Summons summons)
        {
            Woa woa = null;
            if (summons != null)
            {
                WoaQuery woaQuery = new WoaQuery();
                woaQuery.Append(WoaColumn.SumIntNo, summons.SumIntNo.ToString());
                // jake 2013-10-18 added new parematers 
                woaQuery.Append(WoaColumn.IsCurrent, "true");
                woaQuery.AppendNotEquals(WoaColumn.WoaCancel, "true");
                //TList<Woa> woaList = new WoaService().Find(woaQuery as IFilterParameterCollection);
                // 2014-10-15, Oscar changed
                var woaList = this.woaService.Find(woaQuery);
                if (woaList != null && woaList.Count > 0)
                {
                    woa = woaList.OrderByDescending(w => w.WoaEdition).ToList()[0];
                }
            }
            return woa;
        }

        private Frame GetFrame(Notice notice)
        {
            Frame frame = null;

            if (notice.NotFilmType != "M" && notice.NotFilmType != "H")
            {
                //NoticeFrameQuery nfQuery = new NoticeFrameQuery();
                //nfQuery.Append(NoticeFrameColumn.NotIntNo, notice.NotIntNo.ToString());
                //TList<NoticeFrame> nfList = new NoticeFrameService().Find(nfQuery as IFilterParameterCollection);
                // 2014-10-15, Oscar changed
                var nfList = this.noticeFrameService.GetByNotIntNo(notice.NotIntNo);
                if (nfList != null && nfList.Count > 0)
                {
                    //FrameQuery frameQuery = new FrameQuery();
                    //frameQuery.Append(FrameColumn.FrameIntNo, nfList[0].FrameIntNo.ToString());
                    //TList<Frame> frameList = new FrameService().Find(frameQuery as IFilterParameterCollection);
                    // 2014-10-15, Oscar changed
                    frame = this.frameService.GetByFrameIntNo(nfList[0].FrameIntNo);
                    //if (frameList != null && frameList.Count > 0)
                    //{
                    //    frame = frameList[0];
                    //}
                }
            }
            return frame;
        }

        private Film GetFilm(Notice notice, Frame frame)
        {
            Film film = null;

            if (notice.NotFilmType != "M" && notice.NotFilmType != "H")
            {
                if (frame != null)
                {
                    //FilmQuery filmQuery = new FilmQuery();
                    //filmQuery.Append(FilmColumn.FilmIntNo, frame.FilmIntNo.ToString());
                    //TList<Film> filmList = new FilmService().Find(filmQuery as IFilterParameterCollection);
                    // 2014-10-15, Oscar changed
                    film = this.filmService.GetByFilmIntNo(frame.FilmIntNo);
                    //if (filmList != null && filmList.Count > 0)
                    //{
                    //    film = filmList[0];
                    //}
                }
            }
            return film;
        }

        private void GetAmountDue(Notice notice, Summons summons, string paymentOriginator, out string paymentInd, out decimal totalAmount, out decimal contemptAmount)
        {
            paymentInd = PaymentIndList.F.ToString();
            totalAmount = 0;
            contemptAmount = 0;

            //Adam 20140728 We have been asked by the client to return  total due amount and contempt due amount for the paid notices!!
            //if (notice.NoticeStatus >= (int)ChargeStatusList.Completion)
            //{
            //    paymentInd = PaymentIndList.F.ToString();
            //    totalAmount = 0;
            //    contemptAmount = 0;
            //}
            //else
            //{

            //ChargeQuery query = new ChargeQuery();
            //query.Append(ChargeColumn.NotIntNo, notice.NotIntNo.ToString());

            //TList<Charge> chargeList = new ChargeService().Find(query as IFilterParameterCollection);
            // 2014-10-15, Oscar changed
            var chargeList = this.chargeService.GetByNotIntNo(notice.NotIntNo);

            if (chargeList != null && chargeList.Count > 0)
            {
                bool noAog = false;
                for (int i = 0; i < chargeList.Count; i++)
                {
                    if (chargeList[i].ChgNoAog == "Y" && chargeList[i].ChgFineAmount == 999999 && chargeList[i].ChgRevFineAmount == 0
                        && chargeList[i].ChargeStatus != 733 && chargeList[i].ChargeStatus != 741
                        // 2015-01-08, Oscar added NoAogAmountUpdated (5385)
                        && !notice.NoAogAmountUpdated.GetValueOrDefault())
                    {
                        noAog = true;
                        break;
                    }
                }

                if (noAog)
                {
                    paymentInd = PaymentIndList.N.ToString();
                    totalAmount = 0;
                    contemptAmount = GetContemptAmountDue(notice, chargeList);
                }
                else
                {   //Adam 20140728 We have been asked by the client to return  total due amount and contempt due amount for the paid notices!!
                    //We will leave the indicator as 'F' alone but return these 2 amounts for paid notices as rquested by the client.
                    if (notice.NoticeStatus >= (int)ChargeStatusList.Completion)
                    {
                        paymentInd = PaymentIndList.F.ToString();
                    }
                    else
                    {
                        if (summons != null)
                        {
                            //DateRulesDetails details = new DateRulesDetails(notice.AutIntNo, "", "CDate", "LastAOGDate");
                            //DefaultDateRules noticePaymentDateRule = new DefaultDateRules(details);
                            //noticePaymentDateRule.SetDefaultDateRule();

                            //if (summons.SumCourtDate.Value.AddDays(-1 * details.DtRNoOfDays) < DateTime.Now)
                            //if (summons.SumLastAogDate < DateTime.Now)
                            //{
                            //    if (paymentOriginator != ((int)SIL.AARTO.DAL.Entities.PaymentOriginatorList.HRK).ToString())
                            //    {
                            //        paymentInd = PaymentIndList.D.ToString();
                            //    }
                            //    else
                            //    {
                            //        paymentInd = PaymentIndList.Y.ToString();
                            //    }
                            //}
                            //else
                            //{
                            //    paymentInd = PaymentIndList.Y.ToString();
                            //}

                            //string notManualCapture = new ManualCaptureTypeService().GetByMctIntNo(notice.NotManualCapture).MctName.Trim();
                            // 2014-10-15, Oscar changed
                            var notManualCapture = this.manualCaptureTypeService.GetByMctIntNo(notice.NotManualCapture).MctName.Trim();

                            if (notManualCapture == "Y" || notManualCapture == "R")
                            {
                                paymentInd = PaymentIndList.Y.ToString();
                            }
                            else
                            {
                                if (DateTime.Now > summons.SumCourtDate.Value.AddHours(-48)
                                    && !string.IsNullOrWhiteSpace(summons.SumCaseNo)
                                    && (summons.SumServedStatus == 1 || summons.SumServedStatus == 2))
                                {
                                    paymentInd = PaymentIndList.D.ToString();
                                }
                                else
                                {
                                    paymentInd = PaymentIndList.Y.ToString();
                                }
                            }

                            //Jerry 2014-05-06 just 2001 and 2018 can be paid if court data expired.
                            if (summons.SumCourtDate.Value < DateTime.Now && paymentOriginator != ((int)SIL.AARTO.DAL.Entities.PaymentOriginatorList.HRK).ToString()
                                && paymentOriginator != ((int)SIL.AARTO.DAL.Entities.PaymentOriginatorList.PayfineRoadsidePayments).ToString())
                            {
                                paymentInd = PaymentIndList.D.ToString();
                            }
                        }
                        else
                        {
                            paymentInd = PaymentIndList.Y.ToString();
                        }
                    }
                    totalAmount = GetTotalAmountDue(notice, chargeList);
                    contemptAmount = GetContemptAmountDue(notice, chargeList);
                }
            }
            //}
        }

        private decimal GetTotalAmountDue(Notice notice, TList<Charge> chargeList)
        {
            decimal totalAmount = 0;
            for (int i = 0; i < chargeList.Count; i++)
            {
                //Adam 20140728 We have been asked by the client to return  total due amount and contempt due amount for the paid notices!!
                // if (chargeList[i].ChgIsMain == true && chargeList[i].ChargeStatus < (int)ChargeStatusList.Completion && (decimal)chargeList[i].ChgRevFineAmount + chargeList[i].ChgContemptCourt > 0)
                if (chargeList[i].ChgIsMain == true
                    && (decimal)chargeList[i].ChgRevFineAmount + chargeList[i].ChgContemptCourt > 0
                    && (notice.NoticeStatus < (int)ChargeStatusList.Completion || notice.NoticeStatus == (int)ChargeStatusList.FullyPaid || notice.NoticeStatus == (int)ChargeStatusList.FullyPaidAtCourt)
                    && (chargeList[i].ChargeStatus < (int)ChargeStatusList.Completion || chargeList[i].ChargeStatus == (int)ChargeStatusList.FullyPaid || chargeList[i].ChargeStatus == (int)ChargeStatusList.FullyPaidAtCourt))
                {
                    totalAmount += (decimal)chargeList[i].ChgRevFineAmount + chargeList[i].ChgContemptCourt;
                }
            }
            return totalAmount;
        }

        private decimal GetContemptAmountDue(Notice notice, TList<Charge> chargeList)
        {
            decimal contemptAmount = 0;
            for (int i = 0; i < chargeList.Count; i++)
            {
                //Adam 20140728 We have been asked by the client to return  total due amount and contempt due amount for the paid notices!!
                if (chargeList[i].ChgIsMain == true
                    && (notice.NoticeStatus < (int)ChargeStatusList.Completion || notice.NoticeStatus == (int)ChargeStatusList.FullyPaid || notice.NoticeStatus == (int)ChargeStatusList.FullyPaidAtCourt)
                    && (chargeList[i].ChargeStatus < (int)ChargeStatusList.Completion || chargeList[i].ChargeStatus == (int)ChargeStatusList.FullyPaid || chargeList[i].ChargeStatus == (int)ChargeStatusList.FullyPaidAtCourt))
                // if (chargeList[i].ChgIsMain == true && chargeList[i].ChargeStatus < (int)ChargeStatusList.Completion)
                {
                    contemptAmount += chargeList[i].ChgContemptCourt;
                }
            }
            return contemptAmount;
        }

        public void GetIdentity(Notice notice, Summons summons, out string identityNumber)
        {
            identityNumber = "";

            if (summons == null)
            {
                switch (notice.NotSendTo)
                {
                    case "O":
                        //OwnerQuery ownerQuery = new OwnerQuery();
                        //ownerQuery.Append(OwnerColumn.NotIntNo, notice.NotIntNo.ToString());
                        //TList<Owner> ownerList = new OwnerService().Find(ownerQuery as IFilterParameterCollection);
                        // 2014-10-15, Oscar changed
                        var ownerList = this.ownerService.GetByNotIntNo(notice.NotIntNo);
                        if (ownerList != null && ownerList.Count > 0)
                        {
                            identityNumber = ownerList[0].OwnIdNumber;
                        }
                        break;
                    case "D":
                        //DriverQuery driverQuery = new DriverQuery();
                        //driverQuery.Append(DriverColumn.NotIntNo, notice.NotIntNo.ToString());
                        //TList<Driver> driverList = new DriverService().Find(driverQuery as IFilterParameterCollection);
                        // 2014-10-15, Oscar changed
                        var driverList = this.driverService.GetByNotIntNo(notice.NotIntNo);
                        if (driverList != null && driverList.Count > 0)
                        {
                            identityNumber = driverList[0].DrvIdNumber;
                        }
                        break;
                    case "P":
                        //ProxyQuery proxyQuery = new ProxyQuery();
                        //proxyQuery.Append(ProxyColumn.NotIntNo, notice.NotIntNo.ToString());
                        //TList<Proxy> proxyList = new ProxyService().Find(proxyQuery as IFilterParameterCollection);
                        // 2014-10-15, Oscar changed
                        var proxyList = this.proxyService.GetByNotIntNo(notice.NotIntNo);
                        if (proxyList != null && proxyList.Count > 0)
                        {
                            identityNumber = proxyList[0].PrxIdNumber;
                        }
                        break;
                }
            }
            else
            {
                //AccusedQuery accusedQeury = new AccusedQuery();
                //accusedQeury.Append(AccusedColumn.SumIntNo, summons.SumIntNo.ToString());
                //TList<Accused> accusedList = new AccusedService().Find(accusedQeury as IFilterParameterCollection);
                // 2014-10-15, Oscar changed
                var accusedList = this.accusedService.GetBySumIntNo(summons.SumIntNo);
                if (accusedList != null && accusedList.Count > 0)
                {
                    identityNumber = accusedList[0].AccIdNumber;
                }
            }

            if (identityNumber == null)
                identityNumber = "";
        }

        // 2014-10-16, Oscar added for diagnostic
        void AddHRKHistoryFile(XmlDocument xmlDoc, string type)
        {
            HrkHistoryFile hrkFile = new HrkHistoryFile();
            hrkFile.HhfType = type;
            hrkFile.HhfxmlBody = xmlDoc.ChildNodes.Count > 1 ? xmlDoc.ChildNodes[1].OuterXml : xmlDoc.ChildNodes[0].OuterXml;
            hrkFile.HhfCreateDate = DateTime.Now;
            hrkFile.LastUser = "HRKInterface";

            //hrkFile = new HrkHistoryFileService().Save(hrkFile);
            // 2014-10-15, Oscar changed
            this.hrkHistoryFileService.Save(hrkFile);
        }

    }
}
