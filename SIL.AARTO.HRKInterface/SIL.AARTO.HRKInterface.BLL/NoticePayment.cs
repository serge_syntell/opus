﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;

using Stalberg.TMS.Data;
using Stalberg.TMS;
using System.Data;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.EntLib;
using Stalberg.TMS.Data.Util;
using System.Transactions;
using SIL.AARTO.DAL.Data;
using System.Globalization;

namespace SIL.AARTO.HRKInterface.BLL
{
    [Serializable()]
    [XmlRoot("NoticePaymentRequest")]
    public class NoticePaymentRequest
    {
        [XmlElement(ElementName = "HEADER")]
        public PaymentHeader NoticePaymentHeader { get; set; }

        [XmlElement(ElementName = "NoticePayment")]
        public NoticePaymentRequestBody NoticePaymentRequestBody { get; set; }

        //[XmlElement(ElementName = "PaymentNoticeList")]
        //public List<NoticePaymentNoticeListRequest> NoticePaymentNoticeListRequest { get; set; }
    }

    [Serializable()]
    [XmlRoot]
    public class NoticePaymentResponse
    {
        [XmlElement(ElementName = "HEADER")]
        public PaymentHeader NoticePaymentHeader { get; set; }

        [XmlElement(ElementName = "NoticePayment")]
        public NoticePaymentResponseBody NoticePaymentResponseBody { get; set; }

        public NoticePaymentResponse()
        {
            this.NoticePaymentHeader = new PaymentHeader();
            this.NoticePaymentResponseBody = new NoticePaymentResponseBody();
        }

    }

    public class NoticePaymentRequestBody
    {
        [XmlElement(ElementName = "NoticeNumber")]
        public string NoticeNumber { get; set; }

        [XmlElement(ElementName = "PaymentOriginator")]
        public string PaymentOriginator { get; set; }//2001:HRK

        [XmlElement(ElementName = "PaymentType")]
        public string PaymentType { get; set; }//4001:Credit card

        [XmlElement(ElementName = "PayPointDescription")]
        public string PayPointDescription { get; set; }

        [XmlElement(ElementName = "PayPointIdentification")]
        public string PayPointIdentification { get; set; }

        [XmlElement(ElementName = "PayPointType")]
        public string PayPointType { get; set; }//3002:ATM

        [XmlElement(ElementName = "ReceiptDate")]
        public string ReceiptDate { get; set; }

        [XmlElement(ElementName = "ReceiptNumber")]
        public string ReceiptNumber { get; set; }
        //[XmlElement(ElementName = "ReceiptTime")]
        //public string ReceiptTime { get; set; }

        //This field has been moved to HEADER 
        [XmlElement(ElementName = "ServiceProviderReference")]
        public string ServiceProviderReference { get; set; }//1001:Civitas Data Services

        [XmlElement(ElementName = "TotalAmount")]
        public string TotalAmount { get; set; }
        //[XmlElement(ElementName = "TransactionDate")]
        //public DateTime TransactionDate { get; set; }
        //[XmlElement(ElementName = "XSDVersion")]
        //public string XSDVersion { get; set; }

    }



    public class NoticePaymentResponseBody
    {

        [XmlElement(ElementName = "NoticeNumber")]
        public string NoticeNumber { get; set; }

        //[XmlElement(ElementName = "Description")]
        //public string Description { get; set; }

        [XmlElement(ElementName = "PaidAmount")]
        public decimal PaidAmount { get; set; }

        [XmlElement(ElementName = "PaidContemptAmount")]
        public decimal PaidContemptAmount { get; set; }

        //[XmlElement(ElementName = "Result")]
        //public string ResultCode { get; set; }

        [XmlElement(ElementName = "ServiceProviderReference")]
        public string ServiceProviderReference { get; set; }

        //[XmlElement(ElementName = "XSDVersion")]
        //public string XSDVersion { get; set; }

        //[XmlElement(ElementName = "NoticeList")]
        //public List<PaymentNoticeListResponse> PaymentNoticeListResponse { get; set; }
    }

    public class NoticePayment
    {
        // 2014-10-15, Oscar added
        readonly HrkHistoryFileService hrkHistoryFileService = new HrkHistoryFileService();
        readonly AuthorityService authorityService = new AuthorityService();
        readonly NoticeService noticeService = new NoticeService();
        readonly ChargeService chargeService = new ChargeService();
        readonly ReceiptService receiptService = new ReceiptService();
        readonly NoticeSummonsService noticeSummonsService = new NoticeSummonsService();
        readonly SummonsService summonsService = new SummonsService();
        readonly NoticePrefixHistoryService noticePrefixHistoryService = new NoticePrefixHistoryService();
        readonly SumChargeService sumChargeService = new SumChargeService();
        readonly ScDeferredPaymentsService scDeferredPaymentsService = new ScDeferredPaymentsService();
        readonly NoticeStatisticalTypeService noticeStatisticalTypeService = new NoticeStatisticalTypeService();

        private readonly string _connectionString = string.Empty;
        private static CashReceiptDB cashReceiptDB = null;
        static XMLValidate _xmlValidate = null;

        private int _minStatus = 0;
        private int _maxStatus = 900;
        private bool _usePostalReceipt;
        private int _noDaysForLastPaymentDate;
        private bool _isSelectAllNotices;
        private int _noOfDaysForPayment;
        private string _lastUser = "";
        private bool _isSuspenseNotice;
        private List<NoticeForPayment> _notices = new List<NoticeForPayment>();

        //private static List<ResultCode> ResultCodes = new List<ResultCode>();
        private bool _isDisplayByID;
        private bool _cashAfterCourtDate = true;
        private bool _cashAfterSummons = true;
        private bool _cashAfterWarrant = true;
        private bool _cashAfterGracePeriod = true;
        private bool _cashAfterCaseNumber = true;
        private int _sequenceNumberLength = 6;
        private string _needPadding = "N";

        private int isDeferredPayment;

        private bool isCommit = false;

        public NoticePayment(string connectionStr)
        {
            this._connectionString = connectionStr;
            cashReceiptDB = new CashReceiptDB(_connectionString);
            //GetAllResultCode();
            _xmlValidate = XMLValidate.getInstance(_connectionString);
        }

        public XmlDocument DoTestNoticePayment(XmlDocument xmlDoc, bool isCommit)
        {
            return DoNoticePayment(xmlDoc, isCommit);
        }

        public XmlDocument DoNoticePayment(XmlDocument xmlDoc, bool isCommit = true)
        {
            this.isCommit = isCommit;
            if (this.isCommit)
            {
                // 2014-10-16, Oscar added for diagnostic
                Common.WriteDebug("NoticePayment", "Start AddHRKHistoryFile for Request at NoticePayment.DoNoticePayment, xml length: {0}.", xmlDoc.ChildNodes[xmlDoc.ChildNodes.Count > 1 ? 1 : 0].OuterXml.Length);

                AddHRKHistoryFile(xmlDoc, "Request");

                // 2014-10-16, Oscar added for diagnostic
                Common.WriteDebug("NoticePayment", "End AddHRKHistoryFile for Request at NoticePayment.DoNoticePayment.");
            }

            NoticePaymentResponse noticePaymentRespone = null;
            XmlDocument returnXml = null;
            string errorMessage = string.Empty;
            bool dataValidated = true;
            try
            {
                NoticePaymentRequest noticePaymentRequest = XmlUtility.DeserializeXml<NoticePaymentRequest>(xmlDoc);

                InitialNoticePyamentRequest(noticePaymentRequest);

                // 2014-10-16, Oscar added for diagnostic
                Common.Originator = noticePaymentRequest.NoticePaymentRequestBody.PaymentOriginator;
                Common.Type = noticePaymentRequest.NoticePaymentRequestBody.PaymentType;
                Common.Data = noticePaymentRequest.NoticePaymentRequestBody.NoticeNumber;

                noticePaymentRespone = InitialNoticePaymentRespone(noticePaymentRequest);

                if (_xmlValidate.CheckXSDVersion(noticePaymentRequest.NoticePaymentHeader.VersionControl, SERVICE_TYPE.NoticePayment) == false)
                {
                    // version not match
                    SetResultCode(noticePaymentRespone, ResultCodeList.DataValidationError, ErrorCodeList.InvalidVersionControl);
                    dataValidated = false;
                }

                // 2014-10-16, Oscar added for diagnostic
                Common.WriteDebug("NoticePayment", "Start Validate at NoticePayment.DoNoticePayment.");

                if (dataValidated && _xmlValidate.Validate<NoticePaymentRequest>(noticePaymentRequest, out errorMessage) == false)
                {
                    //xml data validation failed
                    SetResultCode(noticePaymentRespone, ResultCodeList.DataValidationError, errorMessage);
                    noticePaymentRespone.NoticePaymentHeader.ServerData = errorMessage;
                    dataValidated = false;
                }

                // 2014-10-16, Oscar added for diagnostic
                Common.WriteDebug("NoticePayment", "End Validate at NoticePayment.DoNoticePayment.");

                // 2014-10-16, Oscar added for diagnostic
                Common.WriteDebug("NoticePayment", "Start GetByAutNo at NoticePayment.DoNoticePayment.");

                //Validate Authority No
                //SIL.AARTO.DAL.Entities.Authority authority = new AuthorityService().GetByAutNo(noticePaymentRequest.NoticePaymentHeader.LocalAuthorityCode);
                // 2014-10-15, Oscar changed
                var authority = this.authorityService.GetByAutNo(noticePaymentRequest.NoticePaymentHeader.LocalAuthorityCode);

                // 2014-10-16, Oscar added for diagnostic
                Common.WriteDebug("NoticePayment", "End GetByAutNo at NoticePayment.DoNoticePayment.");

                if (dataValidated && authority == null)
                {
                    SetResultCode(noticePaymentRespone, ResultCodeList.DataValidationError, ErrorCodeList.InvalidLocalAuthorityCode);
                    dataValidated = false;
                }

                string noticeNumber = noticePaymentRequest.NoticePaymentRequestBody.NoticeNumber;
                if (dataValidated && noticeNumber.IndexOf("/") > 0)
                {
                    // 2014-10-16, Oscar added for diagnostic
                    Common.WriteDebug("NoticePayment", "Start GetByAutNo at NoticePayment.DoNoticePayment.");

                    //authority = new AuthorityService().GetByAutNo(noticeNumber.Split('/')[2]);
                    // 2014-10-15, Oscar changed
                    authority = this.authorityService.GetByAutNo(noticeNumber.Split('/')[2]);
                    if (authority == null)
                    {
                        SetResultCode(noticePaymentRespone, ResultCodeList.DataValidationError, ErrorCodeList.InvalidLocalAuthorityCode);
                        dataValidated = false;
                    }

                    // 2014-10-16, Oscar added for diagnostic
                    Common.WriteDebug("NoticePayment", "End GetByAutNo at NoticePayment.DoNoticePayment.");
                }

                if (dataValidated && noticeNumber.IndexOf("/") <= 0)
                {
                    // 2014-10-16, Oscar added for diagnostic
                    Common.WriteDebug("NoticePayment", "Start GetByAutNo / GetByNotTicketNo at NoticePayment.DoNoticePayment.");

                    //authority = new AuthorityService().GetByAutNo(noticeNumber.Split('-')[1]);
                    // 2014-10-15, Oscar changed
                    authority = this.authorityService.GetByAutNo(noticeNumber.Split('-')[1]);
                    if (authority == null)
                    {
                        SetResultCode(noticePaymentRespone, ResultCodeList.DataValidationError, ErrorCodeList.InvalidLocalAuthorityCode);
                        dataValidated = false;
                    }
                    else
                    {
                        //SIL.AARTO.DAL.Entities.Notice notice = new NoticeService().GetByNotTicketNo(noticeNumber).FirstOrDefault();
                        // 2014-10-15, Oscar changed
                        var notice = this.noticeService.GetByNotTicketNo(noticeNumber).FirstOrDefault();
                        if (notice == null)
                        {
                            SetResultCode(noticePaymentRespone, ResultCodeList.DataValidationError, ErrorCodeList.InvalidNoticeNumber);
                            dataValidated = false;
                        }
                    }

                    // 2014-10-16, Oscar added for diagnostic
                    Common.WriteDebug("NoticePayment", "End GetByAutNo / GetByNotTicketNo at NoticePayment.DoNoticePayment.");
                }

                if (authority != null && dataValidated)
                {
                    // 2014-10-16, Oscar added for diagnostic
                    Common.WriteDebug("NoticePayment", "Start GetAuthRuleSettings at NoticePayment.DoNoticePayment.");

                    GetAuthRuleSettings(authority.AutIntNo);

                    // 2014-10-16, Oscar added for diagnostic
                    Common.WriteDebug("NoticePayment", "End GetAuthRuleSettings at NoticePayment.DoNoticePayment.");
                }

                if (dataValidated && !Validation.GetInstance(_connectionString).CheckNoticeSequenceNumber(noticePaymentRequest.NoticePaymentRequestBody.NoticeNumber, this._sequenceNumberLength, this._needPadding))
                {
                    dataValidated = false;
                    SetResultCode(noticePaymentRespone, ResultCodeList.DataValidationError, ErrorCodeList.InvalidNoticeNumber);
                }

                if (dataValidated)
                {
                    decimal totalAmount = 0;
                    decimal contemptCourtAmount = 0;
                    bool noticeCreateError = false;
                    DataSet dsNotice = null;

                    // 2014-10-15, Oscar removed
                    //NoticeService noticeService = new NoticeService();
                    //ChargeService chargeService = new ChargeService();

                    List<SIL.AARTO.DAL.Entities.Notice> notices = new List<DAL.Entities.Notice>();
                    List<SIL.AARTO.DAL.Entities.Charge> charges = new List<DAL.Entities.Charge>();

                    // 2014-10-16, Oscar added for diagnostic
                    Common.WriteDebug("NoticePayment", "Start TransactionScope at NoticePayment.DoNoticePayment.");

                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        this._isSuspenseNotice = false;

                        // 2014-10-16, Oscar added for diagnostic
                        Common.WriteDebug("NoticePayment", "Start GetByNotTicketNo at NoticePayment.DoNoticePayment.");

                        SIL.AARTO.DAL.Entities.Notice notice = noticeService.GetByNotTicketNo(noticePaymentRequest.NoticePaymentRequestBody.NoticeNumber).FirstOrDefault();

                        // 2014-10-16, Oscar added for diagnostic
                        Common.WriteDebug("NoticePayment", "End GetByNotTicketNo at NoticePayment.DoNoticePayment.");

                        if (notice == null)
                        {
                            // 2014-10-16, Oscar added for diagnostic
                            Common.WriteDebug("NoticePayment", "Start ImportNotice at NoticePayment.DoNoticePayment.");

                            notice = ImportNotice(noticePaymentRequest.NoticePaymentRequestBody, authority, ref errorMessage, ref dataValidated);
                            if (!dataValidated)
                            {
                                SetResultCode(noticePaymentRespone, ResultCodeList.DataValidationError, ErrorCodeList.InvalidNoticeNumber);
                                if (this.isCommit)
                                {
                                    Common.WriteLog("NoticePayment", "Error", errorMessage + " for NoticeNumber " + noticePaymentRequest.NoticePaymentRequestBody.NoticeNumber);
                                }
                                EntLibLogger.WriteLog("Error", "HRKInterface", errorMessage + " for NoticeNumber " + noticePaymentRequest.NoticePaymentRequestBody.NoticeNumber);
                            }
                            else if (notice == null)
                            {
                                dataValidated = false;
                                SetResultCode(noticePaymentRespone, ResultCodeList.UnrecoverableSystemError, errorMessage);
                            }

                            // 2014-10-16, Oscar added for diagnostic
                            Common.WriteDebug("NoticePayment", "End ImportNotice at NoticePayment.DoNoticePayment.");
                        }
                        else
                        {
                            // 2014-10-16, Oscar added for diagnostic
                            Common.WriteDebug("NoticePayment", "Start LockNoticeChargeSummonsSumCharge at NoticePayment.DoNoticePayment.");

                            // 2014-09-03, Oscar added
                            Locking.LockNoticeChargeSummonsSumCharge(this._connectionString, notice.NotIntNo, true);

                            // 2014-10-16, Oscar added for diagnostic
                            Common.WriteDebug("NoticePayment", "End LockNoticeChargeSummonsSumCharge at NoticePayment.DoNoticePayment.");

                            // 2014-10-16, Oscar added for diagnostic
                            Common.WriteDebug("NoticePayment", "Start ValidateNotice at NoticePayment.DoNoticePayment.");

                            if (ValidateNotice(noticePaymentRespone, noticePaymentRequest.NoticePaymentRequestBody) == false)
                            {
                                dataValidated = false;                                
                            }

                            // 2014-10-16, Oscar added for diagnostic
                            Common.WriteDebug("NoticePayment", "End ValidateNotice at NoticePayment.DoNoticePayment.");
                        }
                        if (dataValidated)
                        {
                            if (notice != null)
                            {
                                //notice = noticeService.GetByNotTicketNo(n.NoticeNumber).FirstOrDefault();
                                notices.Add(notice);

                                // 2014-10-16, Oscar added for diagnostic
                                Common.WriteDebug("NoticePayment", "Start GetByNotIntNo at NoticePayment.DoNoticePayment.");

                                if (_isSuspenseNotice)
                                {
                                    charges.AddRange(chargeService.GetByNotIntNo(notice.NotIntNo).Where(c => c.ChargeStatus == (int)ChargeStatusList.SuspensePaymentNoValidNotice && c.ChgIsMain).ToList());

                                    // 2014-10-16, Oscar added for diagnostic
                                    Common.WriteDebug("NoticePayment", "End GetByNotIntNo at NoticePayment.DoNoticePayment.");
                                }
                                else
                                {
                                    charges.AddRange(chargeService.GetByNotIntNo(notice.NotIntNo).Where(c => c.ChargeStatus < (int)ChargeStatusList.Completion && c.ChgIsMain).ToList());

                                    // 2014-10-16, Oscar added for diagnostic
                                    Common.WriteDebug("NoticePayment", "End GetByNotIntNo at NoticePayment.DoNoticePayment.");

                                    bool isHRK = false;
                                    //#5166 give payment originator 2018 the identical capability to HRK 2001 -- Edit by Teresa 2013/12/19
                                    if (noticePaymentRequest.NoticePaymentRequestBody.PaymentOriginator == ((int)SIL.AARTO.DAL.Entities.PaymentOriginatorList.HRK).ToString() || noticePaymentRequest.NoticePaymentRequestBody.PaymentOriginator == ((int)SIL.AARTO.DAL.Entities.PaymentOriginatorList.PayfineRoadsidePayments).ToString())
                                    {
                                        isHRK = true;
                                    }

                                    // 2014-10-16, Oscar added for diagnostic
                                    Common.WriteDebug("NoticePayment", "Start NoticeSearchResults at NoticePayment.DoNoticePayment.");

                                    dsNotice = NoticeSearchResults(notice.NotTicketNo, isHRK, ref errorMessage);

                                    // 2014-10-16, Oscar added for diagnostic
                                    Common.WriteDebug("NoticePayment", "End NoticeSearchResults at NoticePayment.DoNoticePayment.");
                                }
                                foreach (SIL.AARTO.DAL.Entities.Charge chg in charges.OrderBy(c => c.ChgIntNo).ToList())
                                {
                                    if (chg.ChgIsMain && (decimal)chg.ChgRevFineAmount + chg.ChgContemptCourt > 0)
                                    {
                                        contemptCourtAmount += chg.ChgContemptCourt;
                                        totalAmount = totalAmount + (decimal)chg.ChgRevFineAmount + chg.ChgContemptCourt;
                                    }
                                }
                            }

                            if (!String.IsNullOrEmpty(errorMessage))
                            {
                                //Create notice failed
                                //errorMessage = "Unable to create notice";

                                SetResultCode(noticePaymentRespone, ResultCodeList.UnrecoverableSystemError, errorMessage);
                            }

                            else if (Convert.ToDecimal(noticePaymentRequest.NoticePaymentRequestBody.TotalAmount) != totalAmount)
                            {
                                //DifferentPaymentAmount
                                SetResultCode(noticePaymentRespone, ResultCodeList.DataValidationError, ErrorCodeList.TransactionRejectedIncorrectamountPaid);
                            }
                            else
                            {

                                ReceiptTransaction receipt = GenerateReceipt(noticePaymentRequest, contemptCourtAmount, authority.AutIntNo);

                                bool header = false;
                                string lastUser = "HRKInterface";


                                int trhIntNo = 0;
                                int userIntNo = 0;

                                // 2014-10-16, Oscar added for diagnostic
                                Common.WriteDebug("NoticePayment", "Start AddReceiptToPaymentList at NoticePayment.DoNoticePayment.");

                                trhIntNo = cashReceiptDB.AddReceiptToPaymentList(receipt, DateTime.Now, trhIntNo, ref errorMessage, header, lastUser, userIntNo);

                                // 2014-10-16, Oscar added for diagnostic
                                Common.WriteDebug("NoticePayment", "End AddReceiptToPaymentList at NoticePayment.DoNoticePayment.");

                                if (trhIntNo > 0)
                                {
                                    // 2014-10-16, Oscar added for diagnostic
                                    Common.WriteDebug("NoticePayment", "Start GetReceiptPaymentListDS at NoticePayment.DoNoticePayment.");

                                    DataSet dsReceiptPayments = cashReceiptDB.GetReceiptPaymentListDS(trhIntNo);

                                    // 2014-10-16, Oscar added for diagnostic
                                    Common.WriteDebug("NoticePayment", "End GetReceiptPaymentListDS at NoticePayment.DoNoticePayment.");

                                    // 2014-10-16, Oscar added for diagnostic
                                    Common.WriteDebug("NoticePayment", "Start AddNoticesForPayment at NoticePayment.DoNoticePayment.");

                                    bool isValid = AddNoticesForPayment(notices, charges, dsNotice, trhIntNo, this.isDeferredPayment, ref errorMessage);

                                    // 2014-10-16, Oscar added for diagnostic
                                    Common.WriteDebug("NoticePayment", "End AddNoticesForPayment at NoticePayment.DoNoticePayment.");

                                    if (isValid)
                                    {
                                        header = true;

                                        // 2014-10-16, Oscar added for diagnostic
                                        Common.WriteDebug("NoticePayment", "Start AddReceiptToPaymentList at NoticePayment.DoNoticePayment.");

                                        int updTRHIntNo = cashReceiptDB.AddReceiptToPaymentList(receipt,
                                            Convert.ToDateTime(noticePaymentRequest.NoticePaymentRequestBody.ReceiptDate),
                                            trhIntNo, ref errorMessage, header, lastUser, userIntNo);

                                        // 2014-10-16, Oscar added for diagnostic
                                        Common.WriteDebug("NoticePayment", "End AddReceiptToPaymentList at NoticePayment.DoNoticePayment.");

                                        if (updTRHIntNo < 0)
                                        {
                                            errorMessage = string.Format("Unable to add payment " + errorMessage);

                                            //noticePaymentRespone.Description = ResultCodes.Where(c => c.RcIntId == (int)ResultCodeList.OperationError).FirstOrDefault().RcDescription;
                                            //noticePaymentRespone.ResultCode = ((int)ResultCodeList.OperationError).ToString();
                                            SetResultCode(noticePaymentRespone, ResultCodeList.UnrecoverableSystemError, errorMessage);
                                        }
                                        else
                                        {
                                            // 3 Payment
                                            List<ReceiptList> receipts = new List<ReceiptList>();

                                            // 2014-10-16, Oscar added for diagnostic
                                            Common.WriteDebug("NoticePayment", "Start CreateReceiptFromMultiplePayments at NoticePayment.DoNoticePayment.");

                                            //2014-02-25 Heidi changed for fixed a problem about HRK payments - Payment Originator(5214)
                                            receipts = cashReceiptDB.CreateReceiptFromMultiplePayments(trhIntNo, ref errorMessage, this._noOfDaysForPayment,
                                                noticePaymentRequest.NoticePaymentRequestBody.ReceiptNumber, true, noticePaymentRequest.NoticePaymentRequestBody.PaymentOriginator);

                                            // 2014-10-16, Oscar added for diagnostic
                                            Common.WriteDebug("NoticePayment", "End CreateReceiptFromMultiplePayments at NoticePayment.DoNoticePayment.");

                                            if (receipts == null || receipts[0].rctIntNo <= 0)
                                            {
                                                errorMessage = String.Format("Unable to add the receipt transaction! " + errorMessage);

                                                SetResultCode(noticePaymentRespone, ResultCodeList.UnrecoverableSystemError, errorMessage);
                                            }
                                            else
                                            {
                                                // 2014-10-16, Oscar added for diagnostic
                                                Common.WriteDebug("NoticePayment", "Start GetByRctIntNo / Save at NoticePayment.DoNoticePayment.");

                                                //Receipt rec = new ReceiptService().GetByRctIntNo(receipts[0].rctIntNo);
                                                // 2014-10-15, Oscar changed
                                                var rec = this.receiptService.GetByRctIntNo(receipts[0].rctIntNo);
                                                rec.ServiceProvider = noticePaymentRequest.NoticePaymentHeader.ServiceProvider;
                                                rec.PaymentOriginator = noticePaymentRequest.NoticePaymentRequestBody.PaymentOriginator;
                                                rec.PayPointDescription = noticePaymentRequest.NoticePaymentRequestBody.PayPointDescription;
                                                rec.PayPointIdentification = noticePaymentRequest.NoticePaymentRequestBody.PayPointIdentification;
                                                rec.PayPointType = noticePaymentRequest.NoticePaymentRequestBody.PayPointType;
                                                rec.ServiceProviderReference = noticePaymentRequest.NoticePaymentRequestBody.ServiceProviderReference;
                                                rec.RctDate = Convert.ToDateTime(noticePaymentRequest.NoticePaymentRequestBody.ReceiptDate);
                                                //rec = new ReceiptService().Save(rec);
                                                // 2014-10-15, Oscar changed
                                                this.receiptService.Save(rec);

                                                // 2014-10-16, Oscar added for diagnostic
                                                Common.WriteDebug("NoticePayment", "End GetByRctIntNo / Save at NoticePayment.DoNoticePayment.");

                                                // 2014-10-16, Oscar added for diagnostic
                                                Common.WriteDebug("NoticePayment", "Start PunchStatisticsTransactionAdd at NoticePayment.DoNoticePayment.");

                                                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this._connectionString);
                                                punchStatistics.PunchStatisticsTransactionAdd(authority.AutIntNo, "HRKInterface", PunchStatisticsTranTypeList.PaymentTraffic);

                                                // 2014-10-16, Oscar added for diagnostic
                                                Common.WriteDebug("NoticePayment", "End PunchStatisticsTransactionAdd at NoticePayment.DoNoticePayment.");

                                                errorMessage = "Notice Payment complete";
                                                SetResultCode(noticePaymentRespone, ResultCodeList.TransactionCompletedOK, errorMessage);

                                                if (this.isCommit)
                                                {
                                                    scope.Complete();
                                                }

                                                noticePaymentRespone.NoticePaymentResponseBody.NoticeNumber = noticePaymentRespone.NoticePaymentResponseBody.NoticeNumber;
                                                noticePaymentRespone.NoticePaymentResponseBody.PaidAmount = totalAmount;
                                                noticePaymentRespone.NoticePaymentResponseBody.PaidContemptAmount = contemptCourtAmount;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //noticePaymentRespone.Description = ResultCodes.Where(c => c.RcIntId == (int)ResultCodeList.OperationError).FirstOrDefault().RcDescription;
                                        //noticePaymentRespone.ResultCode = ((int)ResultCodeList.OperationError).ToString();

                                        SetResultCode(noticePaymentRespone, ResultCodeList.UnrecoverableSystemError, errorMessage);
                                    }
                                }
                                else
                                {
                                    errorMessage = string.Format("Unable to add payment " + errorMessage);

                                    //noticePaymentRespone.Description = ResultCodes.Where(c => c.RcIntId == (int)ResultCodeList.OperationError).FirstOrDefault().RcDescription;
                                    //noticePaymentRespone.ResultCode = ((int)ResultCodeList.OperationError).ToString();

                                    SetResultCode(noticePaymentRespone, ResultCodeList.UnrecoverableSystemError, errorMessage);
                                }
                            }
                        }
                    }

                    // 2014-10-16, Oscar added for diagnostic
                    Common.WriteDebug("NoticePayment", "End TransactionScope at NoticePayment.DoNoticePayment.");
                }

            }
            catch (Exception ex)
            {
                SetResultCode(noticePaymentRespone, ResultCodeList.UnrecoverableSystemError, ex.ToString());
            }
            finally
            {
                returnXml = XmlUtility.Serialize<NoticePaymentResponse>(noticePaymentRespone);
            }

            if (this.isCommit)
            {
                // 2014-10-16, Oscar added for diagnostic
                Common.WriteDebug("NoticePayment", "Start AddHRKHistoryFile for Response at NoticeEnquiry.DoNoticeEnquiry, xml length: {0}.", returnXml.ChildNodes[returnXml.ChildNodes.Count > 1 ? 1 : 0].OuterXml.Length);

                AddHRKHistoryFile(returnXml, "Response");

                // 2014-10-16, Oscar added for diagnostic
                Common.WriteDebug("NoticePayment", "End AddHRKHistoryFile for Response at NoticeEnquiry.DoNoticeEnquiry.");
            }

            return returnXml;
        }

        //void GetAllResultCode()
        //{
        //    if (ResultCodes.Count() == 0)
        //        ResultCodes = new ResultCodeService().GetAll().ToList();
        //}

        void AddHRKHistoryFile(XmlDocument xmlDoc, string type)
        {
            HrkHistoryFile hrkFile = new HrkHistoryFile();
            hrkFile.HhfType = type;
            hrkFile.HhfxmlBody = xmlDoc.ChildNodes.Count > 1 ? xmlDoc.ChildNodes[1].OuterXml : xmlDoc.ChildNodes[0].OuterXml;
            hrkFile.HhfCreateDate = DateTime.Now;
            hrkFile.LastUser = "HRKInterface";

            //hrkFile = new HrkHistoryFileService().Save(hrkFile);
            // 2014-10-15, Oscar changed
            this.hrkHistoryFileService.Save(hrkFile);
        }

        void InitialNoticePyamentRequest(NoticePaymentRequest request)
        {
            if (request.NoticePaymentHeader == null)
                request.NoticePaymentHeader = new PaymentHeader();

            if (request.NoticePaymentHeader.ClientData == null) request.NoticePaymentHeader.ClientData = "";
            if (request.NoticePaymentHeader.LocalAuthorityCode == null) request.NoticePaymentHeader.LocalAuthorityCode = "";
            if (request.NoticePaymentHeader.ResultCode == null) request.NoticePaymentHeader.ResultCode = "";
            if (request.NoticePaymentHeader.ServerData == null) request.NoticePaymentHeader.ServerData = "";
            if (request.NoticePaymentHeader.ServiceProvider == null) request.NoticePaymentHeader.ServiceProvider = "";
            if (request.NoticePaymentHeader.VersionControl == null) request.NoticePaymentHeader.VersionControl = "";

            if (request.NoticePaymentRequestBody == null)
                request.NoticePaymentRequestBody = new NoticePaymentRequestBody();

            if (request.NoticePaymentRequestBody.NoticeNumber == null)
            {
                request.NoticePaymentRequestBody.NoticeNumber = "";
            }
            else
            {
                request.NoticePaymentRequestBody.NoticeNumber = request.NoticePaymentRequestBody.NoticeNumber.Trim();
            }

            if (request.NoticePaymentRequestBody.PaymentOriginator == null) request.NoticePaymentRequestBody.PaymentOriginator = "";
            if (request.NoticePaymentRequestBody.PaymentType == null) request.NoticePaymentRequestBody.PaymentType = "";
            if (request.NoticePaymentRequestBody.PayPointDescription == null) request.NoticePaymentRequestBody.PayPointDescription = "";
            if (request.NoticePaymentRequestBody.PayPointIdentification == null) request.NoticePaymentRequestBody.PayPointIdentification = "";
            if (request.NoticePaymentRequestBody.PayPointType == null) request.NoticePaymentRequestBody.PayPointType = "";
            if (request.NoticePaymentRequestBody.ReceiptDate == null) request.NoticePaymentRequestBody.ReceiptDate = "";
            if (request.NoticePaymentRequestBody.ReceiptNumber == null) request.NoticePaymentRequestBody.ReceiptNumber = "";
            if (request.NoticePaymentRequestBody.ServiceProviderReference == null) request.NoticePaymentRequestBody.ServiceProviderReference = "";
            if (request.NoticePaymentRequestBody.TotalAmount == null) request.NoticePaymentRequestBody.TotalAmount = "";
        }

        NoticePaymentResponse InitialNoticePaymentRespone(NoticePaymentRequest request)
        {
            NoticePaymentResponse response = new NoticePaymentResponse();
            if (request != null || request.NoticePaymentHeader != null)
            {
                decimal amount = 0;
                response.NoticePaymentHeader.ClientData = String.IsNullOrEmpty(request.NoticePaymentHeader.ClientData) == true ? "" : request.NoticePaymentHeader.ClientData;
                response.NoticePaymentHeader.ServerData = String.IsNullOrEmpty(request.NoticePaymentHeader.ServerData) == true ? "" : request.NoticePaymentHeader.ServerData;
                response.NoticePaymentHeader.LocalAuthorityCode = String.IsNullOrEmpty(request.NoticePaymentHeader.LocalAuthorityCode) == true ? "" : request.NoticePaymentHeader.LocalAuthorityCode;
                response.NoticePaymentHeader.ServiceProvider = String.IsNullOrEmpty(request.NoticePaymentHeader.ServiceProvider) == true ? "" : request.NoticePaymentHeader.ServiceProvider;
                response.NoticePaymentHeader.VersionControl = String.IsNullOrEmpty(request.NoticePaymentHeader.VersionControl) == true ? "" : request.NoticePaymentHeader.VersionControl;
                response.NoticePaymentResponseBody.NoticeNumber = String.IsNullOrEmpty(request.NoticePaymentRequestBody.NoticeNumber) == true ? "" : request.NoticePaymentRequestBody.NoticeNumber;
                response.NoticePaymentResponseBody.ServiceProviderReference = String.IsNullOrEmpty(request.NoticePaymentRequestBody.ServiceProviderReference) == true ? "" : request.NoticePaymentRequestBody.ServiceProviderReference;
                response.NoticePaymentResponseBody.PaidAmount = decimal.TryParse(request.NoticePaymentRequestBody.TotalAmount, out amount) ? amount : 0;
                response.NoticePaymentResponseBody.PaidContemptAmount = 0;
            }

            return response;
        }

        bool ValidateNotice(NoticePaymentResponse respone, NoticePaymentRequestBody noticePayment)
        {
            //SIL.AARTO.DAL.Entities.Notice notice = new NoticeService().GetByNotTicketNo(noticePayment.NoticeNumber).FirstOrDefault();
            // 2014-10-15, Oscar changed
            var notice = this.noticeService.GetByNotTicketNo(noticePayment.NoticeNumber).FirstOrDefault();
            if (notice == null)
            {
                SetResultCode(respone, ResultCodeList.DataValidationError, ErrorCodeList.InvalidNoticeNumber);
                return false;
            }
            NoticeList noticeList = new NoticeEnquiry(this._connectionString).GetNoticeList(new string[] { notice.NotIntNo.ToString() }, noticePayment.PaymentOriginator).FirstOrDefault();
            //#5166 give payment originator 2018 the identical capability to HRK 2001 -- Edit by Teresa 2013/12/19
            if (noticePayment.PaymentOriginator != ((int)SIL.AARTO.DAL.Entities.PaymentOriginatorList.HRK).ToString() && noticePayment.PaymentOriginator != ((int)SIL.AARTO.DAL.Entities.PaymentOriginatorList.PayfineRoadsidePayments).ToString())
            {
                if (noticeList.PaymentInd == "D")
                {
                    SetResultCode(respone, ResultCodeList.DataValidationError, ErrorCodeList.PaymentRejectedDueDatePassed);
                    return false;
                }
            }
            if (noticeList.PaymentInd == "N")
            {
                SetResultCode(respone, ResultCodeList.DataValidationError, ErrorCodeList.NoPaymentCanBeAccepted);
                return false;
            }
            if (noticeList.PaymentInd == "F")
            {
                SetResultCode(respone, ResultCodeList.DataValidationError, ErrorCodeList.TransactionRejectedCaseAlreadyfinalized);
                return false;
            }
            //#5166 give payment originator 2018 the identical capability to HRK 2001 -- Edit by Teresa 2013/12/19
            if (noticePayment.PaymentOriginator != ((int)SIL.AARTO.DAL.Entities.PaymentOriginatorList.HRK).ToString() && noticePayment.PaymentOriginator != ((int)SIL.AARTO.DAL.Entities.PaymentOriginatorList.PayfineRoadsidePayments).ToString())
            {
                if (noticeList.WarrantPending == "Y")
                {
                    SetResultCode(respone, ResultCodeList.DataValidationError, ErrorCodeList.TransactionRejectedWarrantOfArrestPending);
                    return false;
                }
            }
            SIL.AARTO.DAL.Entities.Summons summons = GetSummons(noticePayment.NoticeNumber);
            if (summons != null)
            {
                //#5166 give payment originator 2018 the identical capability to HRK 2001 -- Edit by Teresa 2013/12/19
                //Adam 20140624 Removed this identical line of code.
                if (noticePayment.PaymentOriginator != ((int)SIL.AARTO.DAL.Entities.PaymentOriginatorList.HRK).ToString() && noticePayment.PaymentOriginator != ((int)SIL.AARTO.DAL.Entities.PaymentOriginatorList.PayfineRoadsidePayments).ToString())
                {
                    if (summons.SumLocked.HasValue && summons.SumLocked.Value)
                    {
                        SetResultCode(respone, ResultCodeList.DataValidationError, ErrorCodeList.TransactionNotAllowedInCurrentsystemState);
                        return false;
                    }
                }

                //validate deferred judgement (add by Rachel 2014-09-17 bontq1512)
                SIL.AARTO.DAL.Entities.ScDeferredPayments scdeferredpayments = GetScDeferredPayments(summons.SumIntNo);
                if (scdeferredpayments!=null)
                {
                    SetResultCode(respone, ResultCodeList.DataValidationError, ErrorCodeList.TransactionNotAllowedInCurrentsystemState);
                    Common.WriteLog("DeferredPayment", "Error", "Payment not permitted - deferred judgment settlement required for NoticeNumber " + noticePayment.NoticeNumber);
                    return false;
                }
                //end validate deferred judgement (add by Rachel 2014-09-17 bontq1512)
            }
            
            return true;
        }

        private SIL.AARTO.DAL.Entities.Summons GetSummons(string notTicketNo)
        {
            SIL.AARTO.DAL.Entities.Summons summons = null;

            //SIL.AARTO.DAL.Entities.Notice notice = new NoticeService().GetByNotTicketNo(notTicketNo).FirstOrDefault();
            // 2014-10-15, Oscar changed
            var notice = this.noticeService.GetByNotTicketNo(notTicketNo).FirstOrDefault();
            if (notice != null)
            {
                //NoticeSummonsQuery noticesummonsQuery = new NoticeSummonsQuery();
                //noticesummonsQuery.Append(NoticeSummonsColumn.NotIntNo, notice.NotIntNo.ToString());
                //TList<NoticeSummons> noticesummonsList = new NoticeSummonsService().Find(noticesummonsQuery as IFilterParameterCollection);
                // 2014-10-15, Oscar changed
                var noticesummonsList = this.noticeSummonsService.GetByNotIntNo(notice.NotIntNo);

                if (noticesummonsList != null && noticesummonsList.Count > 0)
                {

                    //Adam 20140624 Fixed the bug to use SumIntNo to retrieve the summons instead of NotSumIntNo.
                    //summons = new SummonsService().GetBySumIntNo(noticesummonsList[0].SumIntNo);
                    // 2014-10-15, Oscar changed
                    summons = this.summonsService.GetBySumIntNo(noticesummonsList[0].SumIntNo);
                }
            }
            return summons;
        }

        //validate deferred judgement (add by Rachel 2014-09-17 bontq1512)
        private SIL.AARTO.DAL.Entities.ScDeferredPayments GetScDeferredPayments(int sumIntNo)
        {
            SIL.AARTO.DAL.Entities.ScDeferredPayments scdeferredpayments = null;
            //SIL.AARTO.DAL.Entities.SumCharge sumcharge= new SumChargeService().GetBySumIntNo(sumIntNo).FirstOrDefault();
            // 2014-10-15, Oscar changed
            var sumcharge = this.sumChargeService.GetBySumIntNo(sumIntNo).FirstOrDefault();
            if (sumcharge!=null)
            {
                //scdeferredpayments = new ScDeferredPaymentsService().GetBySchIntNo(sumcharge.SchIntNo).FirstOrDefault();
                // 2014-10-15, Oscar changed
                scdeferredpayments = this.scDeferredPaymentsService.GetBySchIntNo(sumcharge.SchIntNo).FirstOrDefault();
            }
            return scdeferredpayments;
        }
        //end validate deferred judgement (add by Rachel 2014-09-17 bontq1512)

        string GetEnumDescription(ErrorCodeList errorCode)
        {
            List<Attribute> attributes = XmlUtility.GetEnumText<EnumTextValueAttribute>(errorCode);
            if (attributes != null && attributes.Count > 0)
                return ((EnumTextValueAttribute)attributes[0]).Text;

            return string.Empty;
        }

        void SetResultCode(NoticePaymentResponse respone, ResultCodeList resultCode, ErrorCodeList erroCode)
        {
            respone.NoticePaymentHeader.ResultCode = ((int)resultCode).ToString();
            respone.NoticePaymentHeader.ServerData = String.Format("{0}~{1}", (int)erroCode, GetEnumDescription(erroCode));

            string message = respone.NoticePaymentHeader.ServerData + " for NoticeNumber " + respone.NoticePaymentResponseBody.NoticeNumber;
            if (resultCode == ResultCodeList.TransactionCompletedOK)
            {
                Common.WriteLog("NoticePayment", "General", message);
                EntLibLogger.WriteLog("General", "HRKInterface", message);
            }
            else
            {
                Common.WriteLog("NoticePayment", "Error", message);
                EntLibLogger.WriteLog("Error", "HRKInterface", message);
            }
        }

        void SetResultCode(NoticePaymentResponse respone, ResultCodeList resultCode, string errMessage)
        {
            respone.NoticePaymentHeader.ResultCode = ((int)resultCode).ToString();

            string message = errMessage + " for NoticeNumber " + respone.NoticePaymentResponseBody.NoticeNumber;
            if (resultCode == ResultCodeList.TransactionCompletedOK)
            {
                if (this.isCommit)
                {
                    Common.WriteLog("NoticePayment", "General", message);
                }
                EntLibLogger.WriteLog("General", "HRKInterface", message);
            }
            else
            {
                if (this.isCommit)
                {
                    Common.WriteLog("NoticePayment", "Error", message);
                }
                EntLibLogger.WriteLog("Error", "HRKInterface", message);
            }
        }


        ReceiptTransaction GenerateReceipt(NoticePaymentRequest noticePaymentRequest, decimal contempAmount, int autIntNo)
        {
            ReceiptTransaction receipt = new ReceiptTransaction();

            DateTime expiryDate = new DateTime(3000, 01, 01);

            if (!String.IsNullOrEmpty(noticePaymentRequest.NoticePaymentRequestBody.PaymentType))
            {
                PaymentTypeList payType = (PaymentTypeList)Enum.Parse(typeof(PaymentTypeList), noticePaymentRequest.NoticePaymentRequestBody.PaymentType, true);
                switch (payType)
                {
                    case PaymentTypeList.Cash:
                        receipt.CashType = CashType.HRK_Cash;
                        break;
                    case PaymentTypeList.Cheque:
                        receipt = new ChequeTransaction();
                        receipt.CashType = CashType.HRK_Cheque;
                        receipt.ReceivedFrom = string.Empty;
                        ((ChequeTransaction)receipt).ChequeBank = "";
                        ((ChequeTransaction)receipt).ChequeBranch = "";
                        ((ChequeTransaction)receipt).ChequeDate = Convert.ToDateTime(noticePaymentRequest.NoticePaymentRequestBody.ReceiptDate);
                        ((ChequeTransaction)receipt).ChequeDrawer = "";
                        ((ChequeTransaction)receipt).ChequeNo = 0;
                        break;
                    case PaymentTypeList.BankAccount:
                        receipt = new BankPaymentReceipt();
                        receipt.CashType = CashType.HRK_BankAccount;
                        ((BankPaymentReceipt)receipt).Reference = "";
                        break;
                    case PaymentTypeList.CreditCard:
                        receipt = new CardReceipt();
                        receipt.CashType = CashType.HRK_CreditCard;
                        ((CardReceipt)receipt).CardIssuer = payType.ToString();
                        ((CardReceipt)receipt).NameOnCard = "";
                        ((CardReceipt)receipt).ExpiryDate = expiryDate;//DateTime.Now.Date.AddYears(1);
                        break;
                    case PaymentTypeList.DebitCard:
                        receipt = new CardReceipt();
                        receipt.CashType = CashType.HRK_DebitCard;
                        ((CardReceipt)receipt).CardIssuer = payType.ToString();
                        ((CardReceipt)receipt).NameOnCard = "";
                        ((CardReceipt)receipt).ExpiryDate = expiryDate;//DateTime.Now.Date.AddYears(1);
                        break;
                    case PaymentTypeList.Other:
                        receipt.CashType = CashType.HRK_Other;
                        break;
                }

                receipt.Details = "Payment";
                receipt.ReceivedDate = Convert.ToDateTime(noticePaymentRequest.NoticePaymentRequestBody.ReceiptDate);
                receipt.Amount = Convert.ToDecimal(noticePaymentRequest.NoticePaymentRequestBody.TotalAmount);
                receipt.ContemptAmount = contempAmount;
                receipt.AutIntNo = autIntNo;
                //int authIntNo = 0;

                receipt.MaxStatus = this._maxStatus;

                //if (noticePaymentRequest.NoticePaymentNoticeListRequest != null)
                //    noticePaymentRequest.NoticePaymentNoticeListRequest.ForEach(delegate(NoticePaymentNoticeListRequest notice) { 

                //    });
            }

            return receipt;

        }

        bool AddNoticesForPayment(List<SIL.AARTO.DAL.Entities.Notice> notices, List<SIL.AARTO.DAL.Entities.Charge> charges, DataSet dsNotices, int trhIntNo, int isDeferredPayment, ref string errMessage)
        {
            bool isValid = true;
            errMessage = string.Empty;

            // 2014-10-15, Oscar removed
            //NoticeService noticeService = new NoticeService();
            //ChargeService chargeService = new ChargeService();

            AccountDB accountCharge = new AccountDB(_connectionString);

            List<SIL.AARTO.DAL.Entities.Charge> chgList = new List<DAL.Entities.Charge>();
            int accIntNo = 0;
            //StringBuilder sb = new StringBuilder("<root>");

            // 2014-09-09, Oscar changed
            var noticeList = new List<TempReceiptNotice>();

            if (this._isSuspenseNotice)
            {
                foreach (SIL.AARTO.DAL.Entities.Notice notice in notices)
                //foreach (NoticePaymentNoticeListRequest noticePaymentRequest in noticePayment.NoticePaymentNoticeListRequest)
                {
                    // If create a suspense notice,NoticeNumber will create as a dummary notice,so using the follow code can not find the notice;
                    //notice = noticeService.GetByNotTicketNo(noticePaymentRequest.NoticeNumber).FirstOrDefault();
                    if (notice != null)
                    {
                        chgList = charges.Where(c => c.NotIntNo == notice.NotIntNo && c.ChargeStatus < (int)ChargeStatusList.Completion && c.ChgIsMain).ToList();
                        //charges = chargeService.GetByNotIntNo(notice.NotIntNo).Where(c => c.ChargeStatus != 936).ToList();
                        foreach (SIL.AARTO.DAL.Entities.Charge chg in chgList)
                        {
                            accIntNo = accountCharge.GetAccount(chg.CtIntNo);
                            if (accIntNo <= 0)
                            {
                                isValid = false;
                                errMessage = string.Format("Unable to find a valid ledger account for this charge. Receipt not processed");
                                return isValid;
                            }

                            //sb.Append("<notice notintno=\"");
                            //sb.Append(notice.NotIntNo);
                            //sb.Append("\"");
                            //sb.Append(" chgintno=\"");
                            //sb.Append(chg.ChgIntNo);
                            //sb.Append("\"");
                            //sb.Append(" ctintno=\"");
                            //sb.Append(chg.CtIntNo);
                            //sb.Append("\"");
                            //sb.Append(" accintno=\"");
                            //sb.Append(accIntNo);
                            //sb.Append("\"");
                            //sb.Append(" chgrevfineamount=\"");
                            //sb.Append(chg.ChgRevFineAmount);
                            //sb.Append("\"");
                            //sb.Append(" contemptamount=\"");
                            //sb.Append(chg.ChgContemptCourt);
                            //sb.Append("\"");
                            //sb.Append(" amountpaid=\"");
                            //sb.Append((decimal)chg.ChgRevFineAmount + chg.ChgContemptCourt);
                            //sb.Append("\"");
                            //sb.Append(" seniorofficer=\"");
                            //sb.Append("");
                            //sb.Append("\"");
                            //sb.Append(" />");

                            // 2014-09-09, Oscar changed
                            noticeList.Add(new TempReceiptNotice
                            {
                                NotIntNo = notice.NotIntNo,
                                ChgIntNo = chg.ChgIntNo,
                                CTIntNo = chg.CtIntNo,
                                AccIntNo = accIntNo,
                                ChgRevFineAmount = Convert.ToDecimal(chg.ChgRevFineAmount, CultureInfo.InvariantCulture),
                                ContemptAmount = chg.ChgContemptCourt,
                                AmountToPay = Convert.ToDecimal(chg.ChgRevFineAmount, CultureInfo.InvariantCulture) + chg.ChgContemptCourt,
                                SeniorOfficer = ""
                            });
                        };
                    }
                }
            }
            else
            {
                if (isDeferredPayment == 0)
                {
                    foreach (SIL.AARTO.DAL.Entities.Notice notice in notices)
                    //foreach (NoticePaymentNoticeListRequest noticePaymentRequest in noticePayment.NoticePaymentNoticeListRequest)
                    {
                        // If create a suspense notice,NoticeNumber will create as a dummary notice,so using the follow code can not find the notice;
                        //notice = noticeService.GetByNotTicketNo(noticePaymentRequest.NoticeNumber).FirstOrDefault();
                        if (notice != null)
                        {
                            chgList = charges.Where(c => c.NotIntNo == notice.NotIntNo && c.ChargeStatus < (int)ChargeStatusList.Completion && c.ChgIsMain).ToList();
                            //charges = chargeService.GetByNotIntNo(notice.NotIntNo).Where(c => c.ChargeStatus != 936).ToList();
                            foreach (SIL.AARTO.DAL.Entities.Charge chg in chgList)
                            {
                                accIntNo = accountCharge.GetAccount(chg.CtIntNo);
                                if (accIntNo <= 0)
                                {
                                    isValid = false;
                                    errMessage = string.Format("Unable to find a valid ledger account for this charge. Receipt not processed");
                                    return isValid;
                                }

                                //sb.Append("<notice notintno=\"");
                                //sb.Append(notice.NotIntNo);
                                //sb.Append("\"");
                                //sb.Append(" chgintno=\"");
                                //sb.Append(chg.ChgIntNo);
                                //sb.Append("\"");
                                //sb.Append(" ctintno=\"");
                                //sb.Append(chg.CtIntNo);
                                //sb.Append("\"");
                                //sb.Append(" accintno=\"");
                                //sb.Append(accIntNo);
                                //sb.Append("\"");
                                //sb.Append(" chgrevfineamount=\"");
                                //sb.Append(chg.ChgRevFineAmount);
                                //sb.Append("\"");
                                //sb.Append(" contemptamount=\"");
                                //sb.Append(chg.ChgContemptCourt);
                                //sb.Append("\"");
                                //sb.Append(" amountpaid=\"");
                                //sb.Append((decimal)chg.ChgRevFineAmount + chg.ChgContemptCourt);
                                //sb.Append("\"");
                                //sb.Append(" seniorofficer=\"");
                                //sb.Append("");
                                //sb.Append("\"");
                                //sb.Append(" />");

                                // 2014-09-09, Oscar changed
                                noticeList.Add(new TempReceiptNotice
                                {
                                    NotIntNo = notice.NotIntNo,
                                    ChgIntNo = chg.ChgIntNo,
                                    CTIntNo = chg.CtIntNo,
                                    AccIntNo = accIntNo,
                                    ChgRevFineAmount = Convert.ToDecimal(chg.ChgRevFineAmount, CultureInfo.InvariantCulture),
                                    ContemptAmount = chg.ChgContemptCourt,
                                    AmountToPay = Convert.ToDecimal(chg.ChgRevFineAmount, CultureInfo.InvariantCulture) + chg.ChgContemptCourt,
                                    SeniorOfficer = ""
                                });
                            };
                        }
                    }
                }
                else
                {
                    if (dsNotices == null || dsNotices.Tables[0].Rows.Count == 0)
                    {
                        isValid = false;
                        return isValid;
                    }
                    //Deferred Payment information
                    foreach (DataRow dr in dsNotices.Tables[0].Rows)
                    {
                        isDeferredPayment = 1;

                        int notIntNo = Convert.ToInt32(dr["NoticeIntNo"]);
                        int chgIntNo = Convert.ToInt32(dr["ChgIntNo"]);
                        int SCDefPayIntNo = Convert.ToInt32(dr["SCDefPayIntNo"]);

                        //Barry Dickson - added 3 fields to grid 9 now = 12
                        //dls 080416 - this may be blank - seems like it ended up being column 13 not 12????
                        //int ctIntNo = int.Parse(item.Cells[12].Text);
                        int ctIntNo = Convert.ToInt32(dr["ChargeTypeIntNo"]);


                        //Barry Dickson - add in contempt court amt to total - amount to pay must be the addition of both
                        //dls - 080417 - no the rev fine amount is the fine amount before the rep
                        //decimal chgRevFineAmount = this.ParseValue(item.Cells[7].Text); //+ this.ParseValue(item.Cells[9].Text);
                        //decimal contemptAmount = this.ParseValue(item.Cells[9].Text);
                        //decimal amountPaid = chgRevFineAmount + contemptAmount;
                        //string seniorOfficer = string.Empty;
                        decimal deferredAmount = Convert.ToDecimal(dr["DPAmount"]);

                        //if (this.allowOnTheFlyRepresentations && this.hasRepresentation)
                        //{
                        //dls 080417 ============= - amountPaid = this.ParseValue((string)((TextBox)item.FindControl("txtAmountToPay")).Text) + contemptAmount;
                        //Barry Dickson putting contempt amount back in here...
                        //amountPaid = this.ParseValue((string)((TextBox)item.FindControl("txtAmountToPay")).Text);
                        //amountPaid = this.ParseValue((string)((TextBox)item.FindControl("txtAmountToPay")).Text) + contemptAmount;
                        //seniorOfficer = ((DropDownList)item.FindControl("ddlSeniorOfficer")).Text;
                        //}

                        accIntNo = accountCharge.GetAccount(ctIntNo);
                        if (accIntNo <= 0)
                        {
                            isValid = false;
                            //this.SetErrorState("Unable to find a valid ledger account for this charge. Receipt not processed");
                            errMessage = "Unable to find a valid ledger account for this charge. Receipt not processed";
                            return isValid;
                        }
                        ////Barry Dickson 20080409 - add in contempt amount to xml
                        //sb.Append("<notice notintno=\"");
                        //sb.Append(notIntNo);
                        //sb.Append("\"");
                        //sb.Append(" chgintno=\"");
                        //sb.Append(chgIntNo);
                        //sb.Append("\"");
                        //sb.Append(" ctintno=\"");
                        //sb.Append(ctIntNo);
                        //sb.Append("\"");
                        //sb.Append(" accintno=\"");
                        //sb.Append(accIntNo);
                        //sb.Append("\"");
                        //sb.Append(" deferredAmount=\"");
                        //sb.Append(deferredAmount);
                        //sb.Append("\"");
                        //sb.Append(" SCDefPayIntNo=\"");
                        //sb.Append(SCDefPayIntNo);
                        //sb.Append("\"");

                        //sb.Append(" />");

                        // 2014-09-09, Oscar changed
                        noticeList.Add(new TempReceiptNotice
                        {
                            NotIntNo = notIntNo,
                            ChgIntNo = chgIntNo,
                            CTIntNo = ctIntNo,
                            AccIntNo = accIntNo,
                            AmountToPay = deferredAmount,
                            DeferredAmount = deferredAmount,
                            SCDefPayIntNo = SCDefPayIntNo
                        });
                    }
                }
            }

            //sb.Append("</root>");

            int updTRHIntNo = cashReceiptDB.AddNoticesToReceiptList(trhIntNo, "", this.isDeferredPayment, ref errMessage, noticeList);

            if (updTRHIntNo < 0)
            {
                isValid = false;
                errMessage = String.Format("Unable to add notice details to receipt. Receipt not processed" + errMessage);

                return isValid;
            }

            return isValid;
        }

        private void GetAuthRuleSettings(int authIntNo)
        {
            //use this for all authority rules
            //authRules = new AuthorityRulesDB(_connectionString);

            // Check minimum payable status
            //this.CheckMinimumStatusRule(authIntNo);

            // Check maximum payable status
            this.CheckMaximumStatusRule(authIntNo);

            // Check the date rule for the last payment date
            this.CheckLastPaymentDate(authIntNo);

            this.CheckSelectAllRule(authIntNo);

            this.GetNoOfDaysForPayment(authIntNo);

            this.CheckIDRule(authIntNo);

            // Jake 2012-07-25 added to check the length of sequence number for notice number
            this.CheckNoticeSequenceRule(authIntNo);

            //this.CheckOneReceiptPerNoticeRule(authIntNo);

            ////Barry Dickson 20080408 new rules for cashier
            //this.CheckCashAfterCourtDateRule(authIntNo);
            //this.CheckCashAfterSummonsServedRule(authIntNo);
            //this.CheckCashAfterWarrantRule(authIntNo);

            ////BD 20080624 cash after grace period
            //this.CheckCashAfterGracePeriodRule(authIntNo);

            //// SD 20081202 cash after case number
            //this.CheckCashAfterCaseNumberRule(authIntNo);
        }

        private void GetNoOfDaysForPayment(int autIntNo)
        {
            AuthorityRulesDetails arule = new AuthorityRulesDetails();
            arule.AutIntNo = autIntNo;
            arule.ARCode = "4660";
            arule.LastUser = this._lastUser;

            DefaultAuthRules paymentRule = new DefaultAuthRules(arule, this._connectionString);
            KeyValuePair<int, string> payment = paymentRule.SetDefaultAuthRule();

            _noOfDaysForPayment = payment.Key;
        }

        private void CheckMinimumStatusRule(int authIntNo)
        {

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = authIntNo;
            rule.ARCode = "4605";
            rule.LastUser = this._lastUser;

            DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            this._minStatus = value.Key; //rule.ARNumeric;
            //this.ViewState.Add("MinStatus", this._minStatus);

        }

        private void CheckNoticeSequenceRule(int authIntNo)
        {
            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = authIntNo;
            rule.ARCode = "9300";
            rule.LastUser = this._lastUser;

            DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            this._sequenceNumberLength = value.Key; //rule.ARNumeric;
            this._needPadding = value.Value;
        }

        private void CheckMaximumStatusRule(int authIntNo)
        {


            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = authIntNo;
            rule.ARCode = "4570";
            rule.LastUser = this._lastUser;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();


            this._maxStatus = value.Key; //rule.ARNumeric;
            //this.ViewState.Add("MaxStatus", this._maxStatus);

        }

        private void CheckUsePostalReceipt(int authIntNo)
        {
            // LMZ Added (2007-03-02): To check for Authority Rule - print all receipts to Postal Receipt

            //ard = authRules.GetAuthorityRulesDetailsByCode(this._autIntNo, "4510", "Rule to indicate using of postal receipt for all receipts", 0, "N", "Y = Yes; N= No(default)", this.login);

            //20090113 SD	
            //AutIntNo, ARCode and LastUser need to be set from here
            AuthorityRulesDetails ard = new AuthorityRulesDetails();
            ard.AutIntNo = authIntNo;
            ard.ARCode = "4510";
            ard.LastUser = this._lastUser;

            DefaultAuthRules authRule = new DefaultAuthRules(ard, this._connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            this._usePostalReceipt = ard.ARString.Equals("Y", StringComparison.InvariantCultureIgnoreCase) ? true : false;

        }

        private void CheckIDRule(int authIntNo)
        {

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = authIntNo;
            rule.ARCode = "3300";
            rule.LastUser = this._lastUser;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            this._isDisplayByID = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);


        }

        private void CheckCashAfterCourtDateRule(int authIntNo)
        {

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = authIntNo;
            rule.ARCode = "4730";
            rule.LastUser = this._lastUser;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            this._cashAfterCourtDate = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
            //rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);


        }

        private void CheckCashAfterSummonsServedRule(int authIntNo)
        {
            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = authIntNo;
            rule.ARCode = "4710";
            rule.LastUser = this._lastUser;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            this._cashAfterSummons = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);

        }

        private void CheckCashAfterWarrantRule(int authIntNo)
        {

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = authIntNo;
            rule.ARCode = "4720";
            rule.LastUser = this._lastUser;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            this._cashAfterWarrant = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);

        }

        private void CheckCashAfterGracePeriodRule(int authIntNo)
        {

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = authIntNo;
            rule.ARCode = "4740";
            rule.LastUser = this._lastUser;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            this._cashAfterGracePeriod = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);

        }

        private void CheckCashAfterCaseNumberRule(int authIntNo)
        {

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.ARCode = "6011";
            rule.AutIntNo = authIntNo;
            rule.LastUser = this._lastUser;

            DefaultAuthRules ar = new DefaultAuthRules(rule, this._connectionString);

            KeyValuePair<int, string> cashAfterCase = ar.SetDefaultAuthRule();

            //this.CashAfterGracePeriod = rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
            this._cashAfterCaseNumber = cashAfterCase.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);

        }

        private void CheckLastPaymentDate(int authIntNo)
        {

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = authIntNo;
            rule.ARCode = "4505";
            rule.LastUser = this._lastUser;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            this._noDaysForLastPaymentDate = value.Key; //rule.ARNumeric;


        }

        private void CheckSelectAllRule(int authIntNo)
        {


            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = authIntNo;
            rule.ARCode = "4400";
            rule.LastUser = this._lastUser;

            DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            this._isSelectAllNotices = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
            //rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);

        }

        private string GetCaptureHWOOffenceRule(int authIntNo)
        {
            AuthorityRulesDetails arDetails9050 = new AuthorityRulesDetails();
            arDetails9050.AutIntNo = authIntNo;
            arDetails9050.ARCode = "9050";
            arDetails9050.LastUser = "";

            DefaultAuthRules authRule9050 = new DefaultAuthRules(arDetails9050, this._connectionString);
            KeyValuePair<int, string> value9050 = authRule9050.SetDefaultAuthRule();
            return value9050.Value;
        }

        //Jerry 2013-02-07
        private string Get1stNoticeForS341HWO(int authIntNo)
        {
            AuthorityRulesDetails arDetails2500 = new AuthorityRulesDetails();
            arDetails2500.AutIntNo = authIntNo;
            arDetails2500.ARCode = "2500";
            arDetails2500.LastUser = this._lastUser;

            DefaultAuthRules authRule2500 = new DefaultAuthRules(arDetails2500, this._connectionString);
            KeyValuePair<int, string> value2500 = authRule2500.SetDefaultAuthRule();
            return value2500.Value;
        }

        //Jerry 2013-02-07 add
        private int GetNotOffenceDateNotPaymentDate(int authIntNo)
        {
            DateRulesDetails dateRule = new DateRulesDetails();
            dateRule.AutIntNo = authIntNo;
            dateRule.DtRStartDate = "NotOffenceDate";
            dateRule.DtREndDate = "NotPaymentDate";
            dateRule.LastUser = this._lastUser;
            DefaultDateRules rule = new DefaultDateRules(dateRule, this._connectionString);
            return rule.SetDefaultDateRule();
        }

        SIL.AARTO.DAL.Entities.Notice ImportNotice(NoticePaymentRequestBody paymentNotice, SIL.AARTO.DAL.Entities.Authority authority, ref string errorMessage, ref bool dataValidated)
        {
            string notPrefix = string.Empty;
            string autNo = string.Empty;
            string checkValue = string.Empty;
            int notIntNo = 0;
            errorMessage = string.Empty;

            SuspenseAccount suspenseAccount = new SuspenseAccount(this._connectionString);
            SIL.AARTO.DAL.Entities.Notice notice = null;

            //string validateType = string.Empty;
            //if (paymentNotice.NoticeNumber.IndexOf("/") > 0) { validateType = "cdv"; }
            //else { validateType = "verhoeff"; }

            if (paymentNotice.NoticeNumber.IndexOf("/") > 0)
            {
                notPrefix = paymentNotice.NoticeNumber.Split('/')[0];
                autNo = paymentNotice.NoticeNumber.Split('/')[2];
                checkValue = paymentNotice.NoticeNumber.Split('/')[3];

                //SIL.AARTO.DAL.Entities.Authority authority = new AuthorityService().GetByAutNo(autNo);
                string ruleCode = GetCaptureHWOOffenceRule(authority.AutIntNo);

                DummyNotice dummyNotice = null;

                if (ruleCode.Equals("Y", StringComparison.OrdinalIgnoreCase))
                {
                    //NoticePrefixHistory noticePrefx = new NoticePrefixHistoryService().GetByNprefixAndAuthIntNo(notPrefix, authority.AutIntNo).FirstOrDefault();
                    // 2014-10-15, Oscar changed
                    var noticePrefx = this.noticePrefixHistoryService.GetByNprefixAndAuthIntNo(notPrefix, authority.AutIntNo).FirstOrDefault();
                    //notType = new NoticeTypeService().GetByNtIntNo(noticePrefix.NtIntNo);

                    if (noticePrefx != null)
                    {
                        //Jake 2014-05-29 added to get NoticeStatisticalType per NotPreFix and AutIntNo
                        //So HRK will include Moblie offences
                        string nstCode = string.Empty;
                        //NoticeStatisticalType noticeStatisticalType = new NoticeStatisticalTypeService().GetByNstIntNo(noticePrefx.NstIntNo ?? 0);
                        // 2014-10-15, Oscar changed
                        var noticeStatisticalType = this.noticeStatisticalTypeService.GetByNstIntNo(noticePrefx.NstIntNo ?? 0);
                        if (noticeStatisticalType != null)
                        {
                            if (Validation.GetInstance(_connectionString).ValidateTicketNumber(paymentNotice.NoticeNumber, "cdv"))
                            {
                                noticePrefx.NphType = "O";
                            }

                            switch (noticePrefx.NphType.Trim())
                            {
                                case "H":
                                    nstCode = noticeStatisticalType == null ? "H341" : noticeStatisticalType.NstCode;
                                    //if (!Validation.ValidateTicketNumber(paymentNotice.NoticeNumber, "cdv"))
                                    //{
                                    //Jerry 2013-02-07 add
                                    DateTime notPaymentDate = DateTime.Now;
                                    string aR_2500 = Get1stNoticeForS341HWO(authority.AutIntNo);
                                    if (aR_2500.Equals("Y", StringComparison.OrdinalIgnoreCase))
                                    {
                                        notPaymentDate = notPaymentDate.AddDays(GetNotOffenceDateNotPaymentDate(authority.AutIntNo));
                                    }

                                    cashReceiptDB.MinimalCaptureForSection341(authority.AutIntNo, authority.AutCode + "_SUS_" + checkValue, "0001", paymentNotice.NoticeNumber, "", "", "", "000000", "", "", "", "", "", "", (float)Convert.ToDecimal(paymentNotice.TotalAmount), 0, 0, authority.AutCode, "HRKInterface", out notIntNo, nstCode, true, notPaymentDate, DateTime.Parse(paymentNotice.ReceiptDate));
                                    if (notIntNo > 0)
                                    {
	                                    //notice = new NoticeService().GetByNotIntNo(notIntNo);
	                                    // 2014-10-15, Oscar changed
	                                    notice = this.noticeService.GetByNotIntNo(notIntNo);
                                    }
                                    else
                                    {
                                        errorMessage = CaptureErrorMessageForSection341(notIntNo, paymentNotice.NoticeNumber);
                                    }
                                    //}
                                    //else
                                    //{
                                    //    dummyNotice = suspenseAccount.GetDummyNotice(authority.AutIntNo, Convert.ToDecimal(paymentNotice.TotalAmount), paymentNotice.NoticeNumber);
                                    //    if (dummyNotice != null && dummyNotice.NotIntNo > 0)
                                    //    {
                                    //        notice = new NoticeService().GetByNotIntNo(dummyNotice.NotIntNo);
                                    //        _isSuspenseNotice = true;
                                    //    }
                                    //}
                                    break;
                                case "M":
                                    nstCode = noticeStatisticalType == null ? "H56" : noticeStatisticalType.NstCode;
                                    //if (!Validation.ValidateTicketNumber(paymentNotice.NoticeNumber, "cdv"))
                                    //{
                                    cashReceiptDB.MinimalCaptureForSection56(authority.AutIntNo, authority.AutCode + "_SUS_" + checkValue, "0001", paymentNotice.NoticeNumber, "", "", "", "000000", "", "", "", "", "", "", (float)Convert.ToDecimal(paymentNotice.TotalAmount), 0, 0, authority.AutCode, "HRKInterface", out notIntNo, nstCode, true, DateTime.Parse(paymentNotice.ReceiptDate));
                                    if (notIntNo > 0)
                                    {
	                                    //notice = new NoticeService().GetByNotIntNo(notIntNo);
	                                    // 2014-10-15, Oscar changed
	                                    notice = this.noticeService.GetByNotIntNo(notIntNo);
                                    }
                                    else
                                    {
                                        errorMessage = CaptureErrorMessageForSection56(notIntNo, paymentNotice.NoticeNumber);
                                    }
                                    //}
                                    //else
                                    //{
                                    //    dummyNotice = suspenseAccount.GetDummyNotice(authority.AutIntNo, Convert.ToDecimal(paymentNotice.TotalAmount), paymentNotice.NoticeNumber);
                                    //    if (dummyNotice != null && dummyNotice.NotIntNo > 0)
                                    //    {
                                    //        notice = new NoticeService().GetByNotIntNo(dummyNotice.NotIntNo);
                                    //        _isSuspenseNotice = true;
                                    //    }
                                    //}
                                    break;
                                case "O":
                                default:
                                    //dummyNotice = suspenseAccount.GetDummyNotice(authority.AutIntNo, Convert.ToDecimal(paymentNotice.TotalAmount), paymentNotice.NoticeNumber);
                                    //if (dummyNotice != null && dummyNotice.NotIntNo > 0)
                                    //{
                                    //    notice = new NoticeService().GetByNotIntNo(dummyNotice.NotIntNo);
                                    //    _isSuspenseNotice = true;
                                    //}
                                    dataValidated = false;
                                    errorMessage = "NoticePrefix type is error";
                                    break;
                            }
                        }
                        else {
                            dataValidated = false;
                            errorMessage = "NoticeStatisticalType is null";
                        }
                    }
                    else
                    {
                        dataValidated = false;
                        errorMessage = "NoticePrefixHistory is null";
                    }
                }
                else
                {
                    //dummyNotice = suspenseAccount.GetDummyNotice(authority.AutIntNo, Convert.ToDecimal(paymentNotice.TotalAmount), paymentNotice.NoticeNumber);
                    //if (dummyNotice != null && dummyNotice.NotIntNo > 0)
                    //{
                    //    notice = new NoticeService().GetByNotIntNo(dummyNotice.NotIntNo);
                    //    _isSuspenseNotice = true;
                    //}
                    dataValidated = false;
                    errorMessage = "Authority rule don't allow MinimalCapture";
                }
            }
            else
            {
                dataValidated = false;
                errorMessage = "That don't allow MinimalCapture";
            }
            //else //if (paymentNotice.NoticeNumber.IndexOf("-") > 0)
            //{
            //    //notPrefix = paymentNotice.NoticeNumber.Split('-')[0];
            //    //autNo = paymentNotice.NoticeNumber.Split('-')[1];
            //    //checkValue = paymentNotice.NoticeNumber.Split('-')[3];

            //    //SIL.AARTO.DAL.Entities.Authority authority = new AuthorityService().GetByAutNo(autNo);

            //    DummyNotice dummyNotice = suspenseAccount.GetDummyNotice(authority.AutIntNo, Convert.ToDecimal(paymentNotice.TotalAmount), paymentNotice.NoticeNumber);

            //    if (dummyNotice != null && dummyNotice.NotIntNo > 0)
            //    {
            //        notice = new NoticeService().GetByNotIntNo(dummyNotice.NotIntNo);
            //        _isSuspenseNotice = true;
            //    }
            //}

            return notice;
        }


        string CaptureErrorMessageForSection56(int notIntNo, string notTicketNo)
        {
            string errorMessage = string.Empty;
            if (notIntNo == -1)
            {

                errorMessage = String.Format("Import S56 An error occurred while" + "creating Notice");

            }
            else if (notIntNo == -2)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "creating Charge");

            }
            else if (notIntNo == -3)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "creating Summons");

            }
            else if (notIntNo == -4)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "creating Summons_Charge");

            }
            else if (notIntNo == -5)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "creating Accused");

            }
            else if (notIntNo == -6)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "creating Film");

            }
            else if (notIntNo == -7)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "creating Frame");

            }
            else if (notIntNo == -8)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "creating Notice_Frame");

            }
            else if (notIntNo == -9)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "creating AartoDocumentImage");

            }
            else if (notIntNo == -10)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "creating SumCharge");

            }
            else if (notIntNo == -11)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "creating Driver");

            }
            else if (notIntNo == -12)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "updating Notice");

            }
            else if (notIntNo == -13)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "creating Evidence Pack");

            }
            else if (notIntNo == -14)
            {
                errorMessage = String.Format("Import S56 An error occurred " + "Court Date S56 Allocation");

            }
            else if (notIntNo == -15)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "updating Film ");

            }
            else if (notIntNo == -16)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "creating AARTONoticeDocument");

            }
            else if (notIntNo == -17)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "updating Frame");

            }
            else if (notIntNo == -18)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "getting offence code");

            }
            else if (notIntNo == -19)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "updating Charge");

            }
            else if (notIntNo == -20)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "updating Summons");

            }
            else if (notIntNo == -21)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "updating SumCharge");

            }
            else if (notIntNo == -22)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "updating Accused");

            }
            else if (notIntNo == -103 || notIntNo == -104)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "updating Search tables");

            }
            else if (notIntNo == -30)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "handling offier errors");

            }
            //Added 2010-10-13  ,update status = ImportedFromDMS in AartobmDocument
            else if (notIntNo == -36)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "updating AaBmStatusIs in AartoBMDocument");

            }
            else if (notIntNo == -27)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "creating CourtDate");

            }
            else if (notIntNo == -100)
            {
                errorMessage = String.Format("Import S56 Notice Ticket No already exists in Notice table,TicketNo:{0}", notTicketNo);
            }
            else if (notIntNo == -101)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "creating Charge_SumCharge");

            }
            else if (notIntNo == -102)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "creating Notice_Summons");

            }
            else if (notIntNo == -110)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "officer error message:Incorrect CourtDate does not in AARTOOfficerError table");

            }
            else if (notIntNo == -130)
            {
                errorMessage = String.Format("Import S56 An error occurred while" + "creating CourtRoom for CourtRoomNo");

            }
            else if (notIntNo == -150)
            {
                errorMessage = String.Format("Import S56 An error occurred " + "Invalid ticketNo,TicketNo:{0}", notTicketNo);

            }
            else if (notIntNo == -151)
            {
                errorMessage = String.Format("Import S56 An error occurred " + "Not allowed to do minimul capture for this notice,TicketNo:{0}", notTicketNo);
            }
            else
            {
                if (notIntNo < 0)
                {
                    errorMessage = String.Format("Import S56 Unknown Error Importing S56, return code: " + notIntNo.ToString());

                }
            }

            return errorMessage;
        }

        string CaptureErrorMessageForSection341(int notIntNo, string notTicketNo)
        {
            string errorMessage = string.Empty;
            if (notIntNo == -1)
            {

                errorMessage = String.Format("Import S341 An error occurred while" + "creating Notice");

            }
            else if (notIntNo == -2)
            {
                errorMessage = String.Format("Import S341 An error occurred while" + "creating Charge");

            }
            else if (notIntNo == -3)
            {
                errorMessage = String.Format("Import S341 An error occurred while" + "creating Film");

            }
            else if (notIntNo == -4)
            {
                errorMessage = String.Format("Import S341 An error occurred while" + "handling Frame");

            }
            else if (notIntNo == -5)
            {
                errorMessage = String.Format("Import S341 An error occurred while" + "handling Notice_Frame");

            }
            else if (notIntNo == -6)
            {
                errorMessage = String.Format("Import S341 An error occurred while" + "creating AartoDocument and AartoDocumentImage");

            }
            else if (notIntNo == -30)
            {
                errorMessage = String.Format("Import S341 An error occurred while" + "handling offier errors");

            }
            else if (notIntNo == -50)
            {
                errorMessage = String.Format("Import S341 An error occurred while" + "creating Driver");

            }
            else if (notIntNo == -51)
            {
                errorMessage = String.Format("Import S341 An error occurred while" + "creating FrameDriver");

            }
            //Added 2010-10-13  ,update status = ImportedFromDMS in AartobmDocument
            else if (notIntNo == -16)
            {
                errorMessage = String.Format("Import S341 An error occurred while" + "updating AaBmStatusIs in AartoBMDocument");

            }
            else if (notIntNo == -100)
            {
                errorMessage = String.Format("Import S341 Notice Ticket No already exists in Notice table,TicketNo:{0}", notTicketNo);
            }
            else if (notIntNo == -103 || notIntNo == -104)
            {
                errorMessage = String.Format("Import S341 An error occurred while" + "handling Search tables");

            }
            else if (notIntNo == -150)
            {
                errorMessage = String.Format("Import S341 An error occurred " + "Invalid ticketNo,TicketNo:{0}", notTicketNo);

            }
            else if (notIntNo == -151)
            {
                errorMessage = String.Format("Import S341 An error occurred " + "Not allowed to do minimul capture for this notice,TicketNo:{0}", notTicketNo);
            }
            else
            {
                if (notIntNo < 0)
                {
                    errorMessage = String.Format("Import S341 Unexpected error code:  " + notTicketNo);

                }
            }

            return errorMessage;
        }


        private DataSet NoticeSearchResults(string ticket, bool isHRK, ref string errorMessage)
        {

            StringBuilder sb = new StringBuilder();

            //Barry Dickson 20080408 -  1. need to add in checks for new rules which are passed into the stored proc
            //                          2. need to add new fields to grid (status, contempt of court amount, show court date for notices that are passed summons stage)
            //                          3. to complete point 1 and 2 must change GetNoticesById

            //dls 070712 - added autIntNo to proc so that Auth-rule can be checked and only notices for single authority can be processed
            //dls 081223 - add rule for allowing cash after case no has been generated

            NoticeForPaymentDB notice = new NoticeForPaymentDB(_connectionString);
            DataSet ds = new DataSet();
            string filmType = string.Empty;

            //check if this is a S341
            if (!ticket.Equals(string.Empty))
            {
                filmType = notice.GetFilmType(ticket.Replace("-", string.Empty).Replace("/", string.Empty));
            }

            string id = string.Empty;

            if (isHRK)
            {
                ds = notice.GetNoticesByIdForHRK(id, ticket, this._notices, _isDisplayByID, _noDaysForLastPaymentDate, this._minStatus, this._cashAfterCourtDate, filmType.Equals("M") ? true : this._cashAfterSummons, this._cashAfterWarrant, this._cashAfterGracePeriod, this._cashAfterCaseNumber, this._maxStatus, filmType);
            }
            else
            {
                ds = notice.GetNoticesByIdForHRKGeneral(id, ticket, this._notices, _isDisplayByID, _noDaysForLastPaymentDate, this._minStatus, this._cashAfterCourtDate, filmType.Equals("M") ? true : this._cashAfterSummons, this._cashAfterWarrant, this._cashAfterGracePeriod, this._cashAfterCaseNumber, this._maxStatus, filmType);
            }

            //dls 100202 - Authrule removed from SP
            //dls 070713 - we now check the AuthRule inside the proc, so there may be two tables. We want to use the 2nd one in this case
            int tableID = 0;

            if (ds.Tables.Count > 1)
                tableID = 1;

            if (ds.Tables[tableID].Rows.Count > 0)
            {
                //The dataset could have deferred or normal payment information. If deferred information then need to handle it seperately to the current structure 
                //for display purposes and capturing of information.
                //If the user had already selected a normal payment and then tried to select a deferred payment the column DPComments will have text in, explaining
                //that you have to pay deferred payments seperately, and there will be no deferred payment information.
                int chargeStatus = 0;
                foreach (DataRow row in ds.Tables[tableID].Rows)
                {
                    chargeStatus = (int)row["DPChargeStatus"];
                    if (chargeStatus == 734)
                        break;
                }

                if (chargeStatus == 734)
                {

                    //Deferred Payments
                    // Add the results to the notice list
                    int notIntNo = 0;

                    bool isChecked = false;
                    string noticeNo = string.Empty;
                    bool isThisNoticePresent = ticket.Length == 0 ? true : false;
                    int scDefPayIntNo = 0;
                    decimal amountToPay = 0;
                    //foreach (DataRow row in ds.Tables[0].Rows)
                    foreach (DataRow row in ds.Tables[tableID].Rows)
                    {
                        notIntNo = (int)row["NoticeIntNo"];
                        isChecked = (bool)row["IsChecked"];
                        noticeNo = (string)row["TicketNo"];
                        scDefPayIntNo = (int)row["SCDefPayIntNo"];
                        amountToPay = (decimal)row["DPAmount"];
                        NoticeForPayment nfp = new NoticeForPayment(notIntNo, isChecked);

                        if (noticeNo.Equals(ticket, StringComparison.InvariantCultureIgnoreCase))
                        {
                            isThisNoticePresent = true;
                            nfp.IsSelected = true;
                            nfp.HasJudgement = (bool)row["HasJudgement"].ToString().Equals("Y") ? true : false;
                            nfp.DPIntNo = scDefPayIntNo;
                            nfp.AmountToPay = amountToPay;
                        }

                        //if (!this.notices.Contains(nfp))
                        this._notices.Add(nfp);
                    }

                    // There are tickets to pay
                    if (isThisNoticePresent)
                    {
                        if (_isSelectAllNotices)
                        {
                            foreach (NoticeForPayment n in this._notices)
                                n.IsSelected = true;
                        }

                        isDeferredPayment = 1;
                        //display deferred payment details

                        //this.panelDetails.Visible = true;
                        //this.lblError.Visible = false;
                        ////New grid to show deferred payment breakdown
                        //this.dgDeferredPayments.DataSource = ds.Tables[tableID];
                        //this.dgDeferredPayments.DataKeyField = "NoticeIntNo";
                        //this.dgDeferredPayments.DataBind();
                        ////dont show ticket details grid
                        //this.dgTicketDetails.Visible = false;
                        //this.dgTicketDetails.DataSource = ds.Tables[tableID];
                        //this.dgTicketDetails.DataKeyField = "NoticeIntNo";
                        //this.dgTicketDetails.DataBind();

                        //If defered must not allow to link back to add another notice
                        //this.linkBack.Visible = false;

                        //ShowHideColumns(false);

                        //string name = ds.Tables[tableID].Rows[0]["Name"].ToString();
                        //this.txtReceivedFrom.Text = name;
                        //this.txtChequeDrawer.Text = name;
                        //this.txtNameOnCard.Text = name;
                        ////BD name on card for debot card as well
                        //this.txtNameOnDCard.Text = name;

                        // dls 08012 - they want to be able to toggle between owner, driver & proxy addresses
                        int selectedNotIntNo = 0;

                        //find the first notice that is selected
                        foreach (NoticeForPayment n in this._notices)
                        {
                            if (n.IsSelected)
                            {
                                selectedNotIntNo = n.NotIntNo;
                                break;
                            }
                        }

                        //if (selectedNotIntNo > 0)
                        //    GetAddressDetails(selectedNotIntNo, true);

                        ////dls 080122 - show the address all the time
                        //this.pnlAddress.Visible = true;

                        //this.CalculateTotal(true, "deferred");

                        //need to reset the session object here so that it remembers which ones were selected
                        //this.Session["NoticesToPay"] = this._notices;

                        return ds;
                    }
                }
                else
                {
                    //btnAddNotices.Visible = true;
                    // Add the results to the notice list
                    int notIntNo = 0;

                    bool isChecked = false;
                    string noticeNo = string.Empty;
                    bool isThisNoticePresent = ticket.Length == 0 ? true : false;
                    string deferredErrorMsg = string.Empty;

                    //foreach (DataRow row in ds.Tables[0].Rows)
                    foreach (DataRow row in ds.Tables[tableID].Rows)
                    {
                        notIntNo = (int)row["NoticeIntNo"];

                        isChecked = (bool)row["IsChecked"];
                        noticeNo = (string)row["TicketNo"];
                        deferredErrorMsg = (string)row["DPComments"];

                        NoticeForPayment nfp = new NoticeForPayment(notIntNo, isChecked);

                        if (noticeNo.Equals(ticket, StringComparison.InvariantCultureIgnoreCase))
                        {
                            isThisNoticePresent = true;
                            nfp.IsSelected = true;
                            nfp.HasJudgement = (bool)row["HasJudgement"].ToString().Equals("Y") ? true : false;
                        }

                        if (!this._notices.Contains(nfp))
                            this._notices.Add(nfp);
                    }

                    // There are tickets to pay
                    if (isThisNoticePresent || !deferredErrorMsg.Equals(string.Empty))
                    {
                        if (_isSelectAllNotices)
                        {
                            foreach (NoticeForPayment n in this._notices)
                                n.IsSelected = true;
                        }

                        //this.panelDetails.Visible = true;
                        //this.lblError.Visible = false;
                        //this.dgTicketDetails.DataSource = ds.Tables[tableID];
                        //this.dgTicketDetails.DataKeyField = "NoticeIntNo";
                        //this.dgTicketDetails.DataBind();

                        // jerry 2010-10-29 if NotFilmType = "M" to disable checkbox
                        // dls 2011-06-30 - disabling 'H' as well - the grids do not handle multiple charges under a single notice

                        //TO DO: handle each charge as an individual entity
                        //

                        //foreach (DataGridItem item in this.dgTicketDetails.Items)
                        //{
                        //    Label lblTicketNo = (Label)item.FindControl("lblTicketNo");

                        //    if (!lblTicketNo.Text.Equals(string.Empty))
                        //    {
                        //        filmType = notice.GetFilmType(lblTicketNo.Text.Replace("-", string.Empty).Replace("/", string.Empty));
                        //    }

                        //    if (filmType.Equals("M") || filmType.Equals("H"))
                        //    {
                        //        ImageButton check = (ImageButton)item.FindControl("igPayNow");
                        //        check.Enabled = false;

                        //        TextBox txtAmountToPay = (TextBox)item.FindControl("txtAmountToPay");
                        //        txtAmountToPay.Enabled = false;

                        //        DropDownList ddlSeniorOfficer = (DropDownList)item.FindControl("ddlSeniorOfficer");
                        //        ddlSeniorOfficer.Enabled = false;
                        //    }
                        //}

                        //dont show deferred payments grid
                        //this.dgDeferredPayments.Visible = false;
                        //ShowHideColumns(false);

                        ////BD Set the comment from sql, incase they have selected to pay a deferred notice after they selected a normal notice.
                        //this.lblDeferredError.Text = deferredErrorMsg; //"You are not allowed to receipt a deferred transaction and a normal transaction together. First receipt your currently selected transaction and then receipt the deferred transaction.";

                        //string name = ds.Tables[tableID].Rows[0]["Name"].ToString();
                        //this.txtReceivedFrom.Text = name;
                        //this.txtChequeDrawer.Text = name;
                        //this.txtNameOnCard.Text = name;
                        ////BD name on card for debot card as well
                        //this.txtNameOnDCard.Text = name;

                        // dls 08012 - they want to be able to toggle between owner, driver & proxy addresses
                        int selectedNotIntNo = 0;

                        //find the first notice that is selected
                        foreach (NoticeForPayment n in this._notices)
                        {
                            if (n.IsSelected)
                            {
                                selectedNotIntNo = n.NotIntNo;
                                break;
                            }
                        }

                        //if (selectedNotIntNo > 0)
                        //    GetAddressDetails(selectedNotIntNo, true);

                        ////dls 080122 - show the address all the time
                        //this.pnlAddress.Visible = true;

                        //this.CalculateTotal(true, "");

                        ////need to reset the session object here so that it remembers which ones were selected
                        //this.Session["NoticesToPay"] = this._notices;

                        return ds;
                    }
                }
            }

            // There's nothing outstanding
            //this.lblError.Visible = true;
            //this.panelDetails.Visible = false;
            sb.Length = 0;
            string sReason = string.Empty;

            try
            {
                //LMZ 09-03-2007 - added this to give back Charge status description 
                ChargeStatusDB cdb = new ChargeStatusDB(this._connectionString);

                //dls 080417 ============= - add meaningful error if ticket is not returned
                //sReason = cdb.GetChargeStatusById(ticket, id);
                //BD 20080624 changed to include the error for grace period expired disallowedDPComments
                sReason = cdb.GetChargeStatusById(ticket, id, this._cashAfterCourtDate, this._cashAfterSummons, this._cashAfterWarrant, this._cashAfterGracePeriod, this._cashAfterCaseNumber, isHRK);

                if (sReason != null && sReason != "")
                {
                    //Session["CacheReceiptTraffic_Details_Error"] = "Y";
                }
                else
                {
                    //Session["CacheReceiptTraffic_Details_Error"] = "N";
                }
            }
            catch (Exception cdbEx)
            {
                // mrs 20080719 added message
                sb.Append(string.Format("There was a problem returning an error message from the charge status table " + cdbEx.Message));
            }

            if (id.Length > 0)
                sb.Append(string.Format("No notices were found that matched ID Number '{0}'.<br/>Reason :- {1}", id, sReason));
            if (ticket.Length > 0)
                sb.Append(string.Format("No notices were found that matched Ticket Number '{0}'.<br/>Reason :- {1}.", ticket, sReason));

            errorMessage = sb.ToString();

            return null;
        }
    }
}
