
@echo off

cd StalbergCommonDLL
set "dll=%cd%"

cd..

for /f "delims=" %%j in ('dir *SIL* /ad /s /b') do (
	cd %%j
	
	%SystemRoot%\system32\xcopy.exe "%dll%\*.*" "%%j" /o /s /h /d /c /y
)

pause