﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Services;

using SIL.AARTO.BLL.AARTONoticeClock;
using SIL.AARTO.BLL.NoticeProcess;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.ClockManagerConsole
{
    public class NoticeProcess
    {
        private bool _natisLast = false;
        //private int _crossHairStyle = 0;
        //private bool _showCrossHairs = true;
        private string lastUser = string.Empty;

        private NoticeProcessManager noticeProccess = new NoticeProcessManager();
        public NoticeProcess()
        { }
        public NoticeProcess(string lastUser)
        {
            this.lastUser = lastUser;
        }

        public bool CreateFirstNotices()
        {
            bool failed = false;
            int statusProsecute = 0;
            int noOfRecords = 0;

            EntLibLogger.WriteLine(String.Format("Starting creation of 1st notices for AARTO authorities at: {0}", DateTime.Now));

            foreach (Authority aut in noticeProccess.GetAllAuthority())
            {
                AuthorityRulesDetails ard;

                ard = noticeProccess.GetAutRuleDetailsByAutIntNoAndCode(aut.AutIntNo, "0400", lastUser);
                if (ard.ARString.Equals("Y"))
                {
                    _natisLast = true;
                    statusProsecute = ConstNotice.STATUS_NOTICE_LAST;
                    EntLibLogger.WriteLine("Natis files will be sent/loaded after Adjudication");
                }
                else
                {
                    _natisLast = false;
                    statusProsecute = ConstNotice.STATUS_NOTICE_FIRST;
                    EntLibLogger.WriteLine("Natis files will be sent/loaded before Verification");
                }
                bool boolHasAuthotiry = noticeProccess.GetAuthListForCreatingNotices(aut.AutIntNo, statusProsecute);
                if (boolHasAuthotiry)
                {

                    EntLibLogger.WriteLine(String.Format("CreateNoticeProcess (1st notice) for authority: {0}", aut.AutName));

                    //Create notice
                    failed = CreatingNotices(aut.AutIntNo, ref noOfRecords);
                    if (!failed)
                    {
                        failed = this.CreateTicketsNo(aut.AutIntNo, aut.AutNo);
                    }

                    EntLibLogger.WriteLine(String.Format("Completed creation of 1st notices for {0} at {1}", aut.AutName, DateTime.Now.ToString()));
                }
                else
                {
                    EntLibLogger.WriteLine("CreateNoticeProcess - no frame data for prosecuting - no first notices created");
                }


            }

            return failed;
        }

        private bool CreatingNotices(int autIntNo, ref int noOfRecords)
        {
            bool failed = false;
            string errorMsg = "";
            bool noticesToCreate = true;

            //Components.TMSData create = new Components.TMSData(this.parameters.ConnectionString);

            //set temp location for pdf export
            string filename = "";

            while (noticesToCreate)
            {
                string dateStr = DateTime.Now.ToString();

                dateStr = dateStr.Replace("/", "-");
                dateStr = dateStr.Replace(":", "-");

                filename = "NBS-" + dateStr;

                int noOfFrames = 0;
                string natisLast = _natisLast ? "Y" : "N";
                List<DuplicateNotice> noticeAlready = new List<DuplicateNotice>();

                //noticeProccess.CheckAndUpdateNoticeChargeFromFramForS341(autIntNo, ConstNotice.STATUS_1ST_NOTICE_POSTED, ConstNotice.STATUS_NO_FINE,
                //    ConstNotice.STATUS_EXPIRED, ConstNotice.STATUS_NOTICE,"341", lastUser, filename, ref noOfRecords, ref errorMsg, ConstNotice.STATUS_AMOUNT_ADDED, ref noOfFrames, natisLast);

                noticeProccess.CheckAndUpdateNoticeChargeFromFrame(autIntNo,
                    (int)ChargeStatusList.FirstNoticePosted,
                    (int)ChargeStatusList.NoFineAmount,
                    (int)ChargeStatusList.CancelledNoticeExpiredNotIssued,
                    (int)ChargeStatusList.Completion,
                    lastUser,
                    ref noOfRecords,
                    ref errorMsg,
                    (int)ChargeStatusList.FineAmountAdded,
                    ref noOfFrames,
                    natisLast);

                //int? success = noticeProccess.AddNoticeChargeFromFrame(autIntNo, ConstNotice.STATUS_LOADED, ConstNotice.STATUS_NO_FINE, ConstNotice.STATUS_EXPIRED
                //   , ConstNotice.STATUS_NOTICE, ConstNotice.CTYPE, lastUser, filename, ref noOfRecords
                //   , ref errorMsg, ConstNotice.STATUS_AMOUNT_ADDED, ref noOfFrames, natisLast, noticeAlready);

                int? success = noticeProccess.AddNoticeChargeFromFrame(autIntNo,
                    (int)ChargeStatusList.LoadedCameraViolations,
                    (int)ChargeStatusList.NoFineAmount,
                    (int)ChargeStatusList.CancelledNoticeExpiredNotIssued,
                    (int)ChargeStatusList.Completion,
                    ConstNotice.CTYPE,
                    lastUser,
                    filename,
                    ref noOfRecords,
                    ref errorMsg,
                    (int)ChargeStatusList.FineAmountAdded,
                    ref noOfFrames,
                    natisLast,
                    noticeAlready);

                // See if there are any frames that already have notices
                if (noticeAlready.Count > 0)
                {
                    EntLibLogger.WriteLine("Notices were found for authority {0} that already had notices created for them.", autIntNo);

                    foreach (DuplicateNotice notice in noticeAlready)
                        EntLibLogger.WriteLine("\t{0}", notice.ToString());
                }

                // Process Errors
                //			return Success values
                //			0  - All notices are valid, all fine amounts available
                //			1  - no date rules for authority
                //			2  - all notices have first notice issue date expired
                //			3  - some notices have first notice issue date expired
                //			4  - not all notices have offence codes - unable to load
                //			-1 - something went wrong with the stored proc
                if (success == 1)
                {
                    errorMsg = "There are no date rules for this authority - please set them up before trying create notices.";
                    EntLibLogger.WriteLine(String.Format("CreatingNotices: {0} {1}", errorMsg, DateTime.Now.ToString()));
                    failed = true;
                    return failed;
                }
                else if (success == 4)
                {
                    errorMsg = "Some notices have expired!";
                    EntLibLogger.WriteLine("CreatingNotices: {0} {1}", errorMsg, DateTime.Now.ToString());
                }
                else if (success == 8)
                {
                    errorMsg = "Some notices have no charge records: frames have speeds with no matching offence codes:" + errorMsg + "\nnotice creation cancelled!";
                    EntLibLogger.WriteLine("CreatingNotices: " + errorMsg + " " + DateTime.Now.ToString());
                }
                else if (success == -1)
                {
                    EntLibLogger.WriteLine("CreatingNotices: {0} {1}", errorMsg, DateTime.Now.ToString());
                    noticesToCreate = false;
                }
                else if (success == -2)
                {
                    EntLibLogger.WriteLine("There were duplicate number plates in the film that had not been verified.");
                }
                else if (success == -3)
                {
                    EntLibLogger.WriteLine("Notices already exists for frames in the film.");
                }
                else if (noOfFrames > 0)
                {
                    EntLibLogger.WriteLine("CreatingNotices: {0}  first notices created", noOfRecords.ToString());
                }

                if (noOfFrames == 0) noticesToCreate = false;
            }
            return failed;

        }

        private bool CreateTicketsNo(int autIntNo, string autNo)
        {
            bool failed = false;
            int invalidCount = 0;
            int validCount = 0;
            string errorMessage = string.Empty;
            int? upIntNo = 0;

            List<int> noticeList = noticeProccess.GetNoticeList(autIntNo, ConstNotice.STATUS_AMOUNT_ADDED);
            foreach (int notIntNo in noticeList)
            {
                //string ntCode = "6";
                TicketNo ticket = noticeProccess.GenerateNoticeNumber(autIntNo, lastUser);

                if (ticket.NotTicketNo.Equals("0"))
                {
                    errorMessage = "There was a problem generating the notice numbers - unable to continue";
                    EntLibLogger.WriteLine(String.Format("CreateTickets: {0} {1}", errorMessage, DateTime.Now.ToString()));
                    failed = true;
                    return failed;
                }

                try
                {
                    upIntNo = noticeProccess.UpdateNoticeOfTicketNo(ticket.NotTicketNo, autNo, notIntNo, lastUser);

                    if (upIntNo == null)
                    {
                        invalidCount += 1;
                    }
                    else if (upIntNo == 0 || upIntNo == -1)
                    {
                        invalidCount += 1;
                    }
                    else
                    {
                        Notice notice = noticeProccess.GetNotice(notIntNo);
                        //create notice to be posted a03 clock
                        NoticeToBePostedA03Clock newNoticeClock = new NoticeToBePostedA03Clock(notIntNo);
                        newNoticeClock.LastUser = lastUser;
                        newNoticeClock.Create();
                        newNoticeClock.Start(notice.NotOffenceDate);

                        // Set Charge status = CameraViolationLoaded ;
                        Charge chg = NoticeManager.GetChargeByNotIntNo(notice.NotIntNo);

                        if (chg != null)
                        {
                            chg.ChargeStatus = (int)ChargeStatusList.CameraViolationLoaded;
                            chg = new ChargeService().Save(chg);
                        }

                        validCount += 1;
                    }
                }
                catch
                {
                    invalidCount += 1;
                    EntLibLogger.WriteLine(String.Format("CreateTickets: Create TicketNo for NotIntNo {0} failure  {1}", notIntNo.ToString(), DateTime.Now.ToString()));
                }

            }

            if (invalidCount > 0)
                EntLibLogger.WriteLine(String.Format("CreateTickets: Invalid notice count = {0} {1}", invalidCount.ToString(), DateTime.Now.ToString()));

            EntLibLogger.WriteLine(String.Format("CreateTickets: Valid notice count = {0} {1}", validCount.ToString(), DateTime.Now.ToString()));

            return failed;
        }



    }
}
