﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration;
using System.Threading;
using SIL.AARTO.BLL.AARTONoticeClock;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.EntLib;
using System.Reflection;
namespace SIL.AARTO.ClockManagerConsole
{
    public class Program
    {
        private static int ImportNumOnce = 0;
        private static int SleepMinutes = 0;
        private const string _lastUser = "ClockManager";

        public static string APP_TITLE = string.Empty;
        public static string APP_NAME = AartoProjectList.AARTOClockManagerConsole.ToString();

          
        static void Main(string[] args)
        {
            string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
            Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);

            // Check Last updated Version            
            string errorMessage;
            if (!CheckVersionManager.CheckVersion(AartoProjectList.AARTOClockManagerConsole, out errorMessage))
            {
                Console.WriteLine(errorMessage);
                EntLibLogger.WriteLog(LogCategory.General, null, errorMessage);
                return;
            }

            // Write the started Log
            APP_TITLE = string.Format("{0} - Version {1}\n Last Updated {2}\n Started at: {3}",
                APP_NAME,
                Assembly.GetExecutingAssembly().GetName().Version.ToString(2),
                ProjectLastUpdated.AARTOClockManagerConsole, DateTime.Now);

            string strDatabaseInfo = EntLibLogger.GetServerAndDatabaseName(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());

            EntLibLogger.WriteLog(LogCategory.General, null, string.Format("{0} \n\r {1}", APP_TITLE, strDatabaseInfo));
            Console.Title = APP_TITLE; 

            //need to check this 
            /*AartoClock ac=new AartoClock();
            AartoClockHistory preAC=new AartoClockHistoryService().GetByAaClockHisId((int)ac.AaPreviousClockId);
            AartoRepDocument ard=new AartoRepDocument();
            if (
                (ac.AaClockTypeId == (int)AartoClockTypeList.RepFailedFullPayment
                || ac.AaClockTypeId == (int)AartoClockTypeList.NominateDriverFailedFullPayment)
                && preAC.AaClockTypeId == (int)AartoClockTypeList.DiscountPeriod
                && (ard.postDate.addDays(20) < DateTime.Now))
            {
                FineManager.NoDiscount(notIntNo, lastUser);
            }            
            */
            NoticeProcess noticeProcess = new NoticeProcess(_lastUser);
            noticeProcess.CreateFirstNotices();

            bool continueDo = true;
            ImportNumOnce = Convert.ToInt32(ConfigurationManager.AppSettings["RecordNumberOnceReadFromDB"]);
            SleepMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["SleepMinutes"]);// *1000 * 60;
            TList<AartoClock> clockList = null;
            while (continueDo)
            {
                    clockList = ClockManager.GetAARTOClockListByClockTypeID(ImportNumOnce);
                    if (clockList.Count > 0)
                    {
                        foreach (AartoClock entity in clockList)
                        {
                            try
                            {
                                using (ConnectionScope.CreateTransaction())
                                {
                                    Clock clock = ClockFactory.CreateClock(entity) as Clock;
                                    if (clock != null)
                                    {
                                        clock.LastUser = _lastUser;
                                        //may also change the chargeStatus.
                                        clock.Expire();
                                    }
                                    ConnectionScope.Complete();
                                }
                            }
                            catch (Exception ex)
                            {
                                //ErrorLog.AddErrorLog(ex, _lastUser);
                                EntLibLogger.WriteErrorLog(ex, LogCategory.Error, AartoProjectList.AARTOClockManagerConsole.ToString());
                            }
                        }
                        Thread.Sleep(SleepMinutes);
                    }
                    else
                    {
                        //Set all the clock data IsProcessed=false
                        Clock.ResetIsProcessedForAll();
                        continueDo = false;
                    }
            }
            // Write the ended Log for end of console app.
            EntLibLogger.WriteLog(LogCategory.General, null, string.Format("{0} – Ended at {1}", APP_NAME, DateTime.Now));  
        }
    } 
}
