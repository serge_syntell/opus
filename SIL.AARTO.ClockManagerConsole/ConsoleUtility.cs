﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.ClockManagerConsole
{
    public static class ConsoleUtil
    {
        public static void HandleException(Exception ex)
        {
            EntLibExceptionHandler.HandleException(ex, ExceptionHandlingPolicy.UILayerPolicy);
        }
    }
}
