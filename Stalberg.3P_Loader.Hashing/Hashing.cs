using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace Stalberg.TMS_3P_Loader
{
    /// <summary>
    /// Represents a class that can read and write file lists with their hashes, as well as create and verify the actual file hashes
    /// </summary>
    public class Hashing
    {
        // Fields
        private string fileName = string.Empty;
        private readonly StringBuilder sbErrors;
        private readonly StringBuilder sbFileList;
        private string basePath = string.Empty;
        private bool hasError;
        private readonly List<string> inclusions;
        private string contents;

        // Constants
        //private const string FILE_FILTER = "*.jpg|*.jp2|*.ts1";
        private const char DELIMITER = '|';

        /// <summary>
        /// Gets the File extension used for the hash file creation process
        /// </summary>
        public static readonly string HASH_EXTENSION = ".hashes";

        /// <summary>
        /// Initializes a new instance of the <see cref="Hashing"/> class.
        /// </summary>
        public Hashing()
        {
            this.sbErrors = new StringBuilder();
            this.sbFileList = new StringBuilder();
            this.inclusions = new List<string>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Hashing"/> class.
        /// </summary>
        /// <param name="contents">The contents.</param>
        /// <param name="fileName">Name of the file.</param>
        public Hashing(string contents, string fileName)
            : this()
        {
            this.fileName = fileName;
            this.contents = contents;
        }

        /// <summary>
        /// Gets the name of the file.
        /// </summary>
        /// <value>The name of the file.</value>
        public string FileName
        {
            get { return this.fileName; }
        }

        /// <summary>
        /// Gets the list of excluded file types.
        /// This is a list of file extensions (i.e. '.txt') that will be skipped by the CreateHashFile algorithm.
        /// </summary>
        /// <param name="inclusion">The inclusion.</param>
        public void AddInclusion(string inclusion)
        {
            this.inclusions.Add(inclusion.ToUpper());
        }

        /// <summary>
        /// Removes the exclusion from the list.
        /// </summary>
        /// <param name="inclusion">The inclusion.</param>
        public void RemoveInclusion(string inclusion)
        {
            this.inclusions.Remove(inclusion.ToUpper());
        }

        /// <summary>
        /// Creates a file containing file names and Base64 encoded hashes of all the files in the supplied path.
        /// </summary>
        /// <param name="fileName">The file name of the file to create, containing the file list with their hashes.</param>
        /// <param name="pathToContent">Path to the content files.</param>
        /// <param name="inclusions">The inclusions.</param>
        /// <returns>
        /// 	<c>true</c> if there were no errors, otherwise <c>false</c>, and the Errors property should be read for a detailed list.
        /// </returns>
        public bool CreateHashFile(string fileName, string pathToContent, string[] inclusions)
        {
            // Check that the root directory exists
            DirectoryInfo directory = new DirectoryInfo(pathToContent);
            if (!directory.Exists)
                throw new DirectoryNotFoundException(string.Format("The path to the content ({0}) does not exist.", pathToContent));

            if (inclusions == null)
                inclusions = new string[] { };

            // Add the inclusions to the list
            foreach (string inclusion in inclusions)
                this.AddInclusion(inclusion);

            // Initialise variables
            this.sbErrors.Length = 0;
            this.hasError = false;

            // Create hash file stream
            FileInfo file = new FileInfo(Path.Combine(pathToContent, fileName));

            this.basePath = file.DirectoryName;
            StreamWriter sw = file.CreateText();
            this.fileName = file.Name;
            file = null;

            // Recurse the directory to record and hash the
            try
            {
                this.TraverseDirectory(directory, sw);
            }
            catch (Exception ex)
            {
                this.hasError = true;
                this.sbErrors.Append("Error creating hash file.\n" + ex.Message);
            }
            finally
            {
                sw.Close();
            }

            return !this.hasError;
        }

        private void TraverseDirectory(DirectoryInfo directory, StreamWriter sw)
        {
            bool isFound = false;
            // Record the file details
            foreach (FileInfo file in directory.GetFiles())
            {
                if (file.Name == this.fileName)
                    continue;

                // Check if the file type should be included in the hash file
                isFound = false;
                foreach (string inclusion in this.inclusions)
                {
                    if (inclusion.Equals(file.Extension.ToUpper()))
                    {
                        isFound = true;
                        break;
                    }
                }
                if (!isFound)
                    continue;

                // Write the hash entry
                try
                {
                    sw.Write(file.FullName.Replace(this.basePath, string.Empty));
                    sw.Write(DELIMITER);
                    sw.WriteLine(Hasher.GetFileHash(file.FullName));
                }
                catch (Exception ex)
                {
                    this.SetError(string.Format("There was an error process in '{0}' - {1}", file.FullName, ex.Message), file.FullName);
                }
            }

            // Check any sub-directories
            foreach (DirectoryInfo subDirectory in directory.GetDirectories())
                this.TraverseDirectory(subDirectory, sw);
        }

        /// <summary>
        /// BD : creates a hash for a passed in file name
        /// </summary>
        public string CreateFileHash(string fileName)
        {
            string newHash = "";
            // Write the hash entry 
            try
            {
                newHash = Hasher.getMd5Hash(fileName);
            }
            catch (Exception ex)
            {
                newHash = "";
            }

            return newHash;
        }

        /// <summary>
        /// Verifies that all the content files listed in the hash file are present and correct.
        /// </summary>
        /// <param name="fileName">The full path and file name of the hash file.
        /// This is used as the base path for all content files listed in the has file.</param>
        /// <returns>
        /// 	<c>true</c> if there were NO errors verifying the content. <c>false</c> if there were errors; 
        ///    these error will be listed in the Errors property.
        /// </returns>
        public bool VerifyHashFile(string fileName)
        {
            FileInfo file = new FileInfo(fileName);
            if (!file.Exists)
            {
                this.sbErrors.Append(string.Format("The hash file '{0}' does not exist.", file.FullName));
                return false;
            }

            // Initialise variables
            hasError = false;
            this.sbErrors.Length = 0;
            this.basePath = file.DirectoryName;

            this.ReadHashFile(file);

            return !hasError;
        }

        private void ReadHashFile(FileInfo hashFile)
        {
            // Read out the file contents
            StreamReader sr = null;

            try
            {
                sr = hashFile.OpenText();
                this.contents = sr.ReadToEnd();
            }
            finally
            {
                sr.Close();
                hashFile = null;
            }

            Dictionary<string, string> files = this.ParseContents();

            FileInfo file = null;
            foreach (KeyValuePair<string, string> value in files)
            {
                file = new FileInfo(this.basePath + value.Key);
                if (!file.Exists)
                    this.SetError(string.Format("The file, '{0}', does no exist.", file.FullName), file.FullName);
                else
                {
                    string hash = Hasher.GetFileHash(file.FullName);
                    if (!hash.Equals(value.Value))
                        this.SetError(string.Format("The file hash of '{0}' does not match the one recorded in the hash file '{1}'.", file.FullName, hash), file.FullName);
                }
            }
            file = null;
        }

        private Dictionary<string, string> ParseContents()
        {
            Dictionary<string, string> files = new Dictionary<string, string>();
            if (this.contents.Length == 0)
                return files;

            // Split the blob of text into lines
            string[] lines = this.contents.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string line in lines)
            {
                // Split the lines into 'file name | hash' pairs
                string[] split = line.Split(DELIMITER);
                if (split.Length != 2)
                    this.SetError(string.Format("The line, '{0}', was incorrectly formatted.", line), string.Empty);
                else
                    files.Add(split[0].Trim(), split[1].Trim());
            }

            return files;
        }

        private void SetError(string errorMessage, string fileName)
        {
            this.hasError = true;
            this.sbErrors.Append(errorMessage);

            if (fileName.Length > 0)
                this.sbFileList.Append(fileName + "\n");
        }

        /// <summary>
        /// Gets the list of errors generated during creating and verifying has files.
        /// </summary>
        /// <value>The errors.</value>
        public string Errors
        {
            get { return sbErrors.ToString(); }
        }

        /// <summary>
        /// Gets the list of files with problems.
        /// </summary>
        /// <value>The file list.</value>
        public string FileList
        {
            get { return this.sbFileList.ToString(); }
        }

        /// <summary>
        /// Parses the file contents
        /// </summary>
        public Dictionary<string, string> Parse()
        {
            return this.ParseContents();
        }

        /// <summary>
        /// Saves the Hash file to the specified output directory.
        /// </summary>
        /// <param name="outputDirectory">The output directory.</param>
        public void Save(string outputDirectory)
        {
            string fullName = Path.Combine(outputDirectory, this.fileName);
            StreamWriter writer = new StreamWriter(fullName, false, Encoding.Default);
            writer.Write(this.contents);
            writer.Close();
        }

    }
}
