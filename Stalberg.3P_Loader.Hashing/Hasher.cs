using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Stalberg.TMS_3P_Loader
{
	internal static class Hasher
	{
		// Fields
		private static readonly SHA256Managed sha;
        private static readonly MD5 md5;
		private static byte[] buffer;
		private static FileInfo file;
		private static FileStream fs;

		/// <summary>
		/// Static initialization of the <see cref="Hasher"/> class.
		/// </summary>
		static Hasher ()
		{
			sha = new SHA256Managed();
			sha.Initialize();
            md5 = new MD5CryptoServiceProvider();
            md5.Initialize();
		}

		/// <summary>
		/// Returns a Base64 encoded hash of the file.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <returns>A Base64 encoded string</returns>
		public static string GetFileHash (string fileName)
		{
			try
			{
				file = new FileInfo(fileName);
				if (!file.Exists)
					throw new FileNotFoundException(string.Format("The file '{0}' does not exist.", file.FullName));

				fs = file.OpenRead();
				long length = file.Length;
				buffer = new byte[length];
				fs.Read(buffer, 0, (int)length);
			}
			finally
			{
				fs.Close();
				file = null;
			}

			return Convert.ToBase64String(sha.ComputeHash(buffer));
		}

        public static string getMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();
            string strHashData = "";

            using (FileStream oFileStream = GetFileStream(input))
            {
                // Convert the input string to a byte array and compute the hash.
                //byte[] data = md5Hasher.ComputeHash(Encoding.UTF8.GetBytes(input));
                byte[] data = md5Hasher.ComputeHash(oFileStream);

                strHashData = System.BitConverter.ToString(data);
                strHashData = strHashData.Replace("-", "");
            }
            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            //StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            //for (int i = 0; i < data.Length; i++)
            //{
            //    sBuilder.Append(data[i].ToString("x2"));
            //}

            // Return the hexadecimal string.
            return strHashData; //sBuilder.ToString();
        }

        private static FileStream GetFileStream(string pathName)
        {
            return (new FileStream(pathName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
        }
	}
}
