﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SIL.AARTO.ImporterConsole.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.EntLib;
namespace SIL.AARTO.ImporterConsole.AARTO27
{
    public class AARTO27Data:AARTOData
    {
        private AartoInfringerDetail _infDetail;
        private AartoPerToAccDemPoiHistory _perToAccDemPoiHistory;
        public AARTO27Data()
        {
            _aartoEntity = new AARTOEntity();
        }
        private void LoadRequestInformationFromXML()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(_importData.BaDoResult);
            XmlElement rootElement = xmlDoc.DocumentElement;
            XmlNodeList dataList = rootElement.SelectNodes(@"//Data");

            bool geted = false;
            foreach (XmlNode xn in dataList)
            {
                #region get the other values.
                geted = false;
                switch (xn.Attributes["FieldName"].Value.ToLower())
                {
                    case "vtintno":
                        geted = true;
                        if (xn.Attributes["Value"] == null || xn.Attributes["Value"].ToString() == "")
                            _infDetail.VtIntNo = null;
                        else
                            _infDetail.VtIntNo = AARTODataManager.GetVehicleTypeIDByCode(xn.Attributes["Value"].Value);
                        break;
                    case "vmintno":
                        geted = true;
                        if (xn.Attributes["Value"] == null || xn.Attributes["Value"].ToString() == "")
                            _infDetail.VmIntNo = null;
                        else
                            _infDetail.VmIntNo = AARTODataManager.GetVehicleMakeIDByCode(xn.Attributes["Value"].Value);
                        break;
                    case "vcintno":
                        geted = true;
                        if (xn.Attributes["Value"] == null || xn.Attributes["Value"].ToString() == "")
                            _infDetail.VcIntNo = null;
                        else
                            _infDetail.VcIntNo = AARTODataManager.GetVehicleColourIDByCode(xn.Attributes["Value"].Value);
                        break;
                    case "aainforgtypeid":
                        if (xn.Attributes["Value"].Value.Equals("-1"))
                        {
                            geted = true;
                            _infDetail.AaInfOrgTypeId = -1;
                            _infDetail.AaInfOtherOrgType = xn.Attributes["Other"].Value;
                        }
                        break;
                    case "aaapporgtypeid":
                        if (xn.Attributes["Value"].Value.Equals("-1"))
                        {
                            geted = true;
                            _perToAccDemPoiHistory.AaAppOrgTypeId = -1;
                            _perToAccDemPoiHistory.AaAppOtherOrgType = xn.Attributes["Other"].Value;
                        }
                        break;
                    case "aainflicodeid":
                        if (xn.Attributes["Value"].Value.Equals("-1"))
                        {
                            geted = true;
                            _infDetail.AaInfLiCodeId = -1;
                            _infDetail.AaInfForeignCode = xn.Attributes["Other"].Value;
                        }
                        break;
                    default:
                        break;
                }
                if (geted) continue;
                #endregion
                if (!Common.InsertValueIntoObjectByKey(xn.Attributes["FieldName"].Value, xn.Attributes["Value"].Value, _infDetail))
                {
                    Common.InsertValueIntoObjectByKey(xn.Attributes["FieldName"].Value, xn.Attributes["Value"].Value, _perToAccDemPoiHistory);
                }
            }
        }
        public override void DoImport(ImportDataEntity importData)
        {
            _importSuccess = true;
            _importData = importData;
            _infDetail = new AartoInfringerDetail();
            _perToAccDemPoiHistory = new AartoPerToAccDemPoiHistory();
            SIL.AARTO.DAL.Data.TransactionManager AARTOTM = SIL.AARTO.DAL.Services.ConnectionScope.CreateTransaction();

            SIL.DMS.DAL.Data.TransactionManager DMSTM = SIL.DMS.DAL.Services.ConnectionScope.CreateTransaction();
            bool UpdateAARTOSuccessful = false;
            bool UpdateDMSSuccessful = false;
            try
            {
                LoadRequestInformationFromXML();
                if (!AARTOTM.IsOpen)
                {
                    AARTOTM.BeginTransaction();
                }
                if (!DMSTM.IsOpen)
                {
                    DMSTM.BeginTransaction();
                }
                AartoRequestDocument doc = AARTODataManager.CreateAARTORequestDocumentAndImages(importData.BaDoID, (int)AartoDocumentTypeList.AARTO27, importData.IFSIntNo);
                new AartoInfringerDetailService().Insert(_infDetail);
                _perToAccDemPoiHistory.AaInfDetailId = _infDetail.AaInfDetailId;
                _perToAccDemPoiHistory.AaReqDocId = doc.AaReqDocId;
                new AartoPerToAccDemPoiHistoryService().Insert(_perToAccDemPoiHistory);
                UpdateAARTOSuccessful = true;

                //update status into DMS
                DMSDataManager.UpdateImportResultToDMS(_importData.BaDoID, true, "");
                UpdateDMSSuccessful = true;
                AARTOTM.Commit();
                DMSTM.Commit();
            }
            catch (Exception ex)
            {
                _importSuccess = false;
                if (!UpdateAARTOSuccessful || !UpdateDMSSuccessful)
                {
                    if (AARTOTM.IsOpen) AARTOTM.Rollback();
                    if (DMSTM.IsOpen) DMSTM.Rollback();
                }
                DMSDataManager.UpdateImportResultToDMS(_importData.BaDoID, false, AARTOMessage.UNKNOWN_ERROR);

                //Jake 2010-10-15 Write log using Enterprise Library log
                //ErrorLog.AddErrorLog(ex, AARTODataManager.LastUser);
                EntLibLogger.WriteErrorLog(ex, LogCategory.Error, AartoProjectList.AARTOImporterConsole.ToString());
            }
            finally
            {
                AARTOTM.Dispose();
                DMSTM.Dispose();

            }
        }
    }
}

