﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SIL.AARTO.ImporterConsole.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.InfringerOption.Model;
using SIL.AARTO.BLL.InfringerOption;
namespace SIL.AARTO.ImporterConsole.AARTO10
{
    public class AARTO10Data : AARTOData
    {
        public AARTO10Data()
        {
            _aartoEntity = new AARTOEntity();
        }
        protected override void LoadOtherData(XmlNodeList dataList)
        {
        }
        protected override void UpdateChargeStatus()
        {
            AARTODataManager.UpdateChargeStatus(ChargeStatusList.AARTO_ElectCourt_10, _charge.ChgIntNo);
        }
        protected override void CreateReceiptForRepresentationDocument(int repID)
        {
        }
        protected override int DoValidation(out int needCancelledrepID, out int notIntNo)
        {
            return base.DoValidation(out needCancelledrepID, out notIntNo);
        }
        public override void DoImport(SIL.AARTO.ImporterConsole.Utility.ImportDataEntity importData)
        {
            base.DoImport(importData);
            if (_importSuccess)
            {//should be move to program.cs.
                DecisionModel model = DecisionManager.GetDecideModelByRepID(_rep.AaRepId, (int)AartoDocumentTypeList.AARTO10);
                model.DecideResult = (int)RepresentationCodeList.AARTO_RepresentationSuccessful;
                model.LastUser = AARTODataManager.LastUser;
                DecisionManager.DecideRepresentation(model);
            }
        }
    }
}
