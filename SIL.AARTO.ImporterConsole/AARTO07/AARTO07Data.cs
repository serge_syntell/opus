﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SIL.AARTO.ImporterConsole.Utility;
using SIL.AARTO.DAL.Entities;
namespace SIL.AARTO.ImporterConsole.AARTO07
{
    public class AARTO07Data : AARTOData
    {
        public AARTO07Data()
        {
            _aartoEntity = new Entity07();
        }
        protected override void LoadOtherData(XmlNodeList dataList)
        {
            foreach (XmlNode xn in dataList)
            {
                if (xn.Attributes["Value"].Value.Equals("-1") && xn.Attributes["FieldName"].Value.ToLower().Equals("aarepnewinflicodeid"))
                {
                    _rep.AaRepNewInfLiCodeId = -1;
                    _rep.AaRepNewInfForeignCode = xn.Attributes["Other"].Value;
                    break;
                }
            }
        }
        protected override void UpdateChargeStatus()
        {
            AARTODataManager.UpdateChargeStatus(ChargeStatusList.AARTO_RegisterRepresentationNominatedDriver_07, _charge.ChgIntNo);
        }
        protected override void CreateReceiptForRepresentationDocument(int repID)
        {
        }
        protected override int DoValidation(out int needCancelledrepID, out int notIntNo)
        {
            return base.DoValidation(out needCancelledrepID, out notIntNo);
        }
    }
}
