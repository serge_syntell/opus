﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SIL.AARTO.ImporterConsole.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.InfringerOption;
namespace SIL.AARTO.ImporterConsole.AARTO08
{
    public class AARTO08Data : AARTOData
    {
        public AARTO08Data()
        {
            _aartoEntity = new Entity08();
        }
        protected override void LoadOtherData(XmlNodeList dataList)
        {
            _rep.AaRepDetails = _rep.AaRepDetails + ((Entity08)_aartoEntity).AaRepDetails2;
        }
        protected override void UpdateChargeStatus()
        {
            AARTODataManager.UpdateChargeStatus(ChargeStatusList.AARTO_RegisterRepresentation_08, _charge.ChgIntNo);
        }
        protected override void CreateReceiptForRepresentationDocument(int repID)
        {
            AARTODocumentManager.CreateAARTONoticeDocument(repID, (int)AartoDocumentTypeList.AARTO05c, AARTODataManager.LastUser);  
        }
    }
}
