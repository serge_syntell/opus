﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SIL.AARTO.ImporterConsole.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.EntLib;
namespace SIL.AARTO.ImporterConsole.AARTO25
{
    public class AARTO25Data:AARTOData
    {
        private AartoInfringerDetail _infDetail;
        private AartoRefundOfMoney _refundOfMoney;
        public AARTO25Data()
        {
            _aartoEntity = new AARTOEntity();
        }
        private void LoadRequestInformationFromXML()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(_importData.BaDoResult);
            XmlElement rootElement = xmlDoc.DocumentElement;
            XmlNodeList dataList = rootElement.SelectNodes(@"//Data");

            bool geted = false;
            foreach (XmlNode xn in dataList)
            {
                #region get the other values.
                geted = false;
                switch (xn.Attributes["FieldName"].Value.ToLower())
                {
                    case "vtintno":
                        geted = true;
                        if (xn.Attributes["Value"] == null || xn.Attributes["Value"].ToString() == "")
                            _infDetail.VtIntNo = null;
                        else
                        _infDetail.VtIntNo = AARTODataManager.GetVehicleTypeIDByCode(xn.Attributes["Value"].Value);
                        break;
                    case "vmintno":
                        geted = true;
                        if (xn.Attributes["Value"] == null || xn.Attributes["Value"].ToString() == "")
                            _infDetail.VmIntNo = null;
                        else
                        _infDetail.VmIntNo = AARTODataManager.GetVehicleMakeIDByCode(xn.Attributes["Value"].Value);
                        break;
                    case "vcintno":
                        geted = true;
                        if (xn.Attributes["Value"] == null || xn.Attributes["Value"].ToString() == "")
                            _infDetail.VcIntNo = null;
                        else
                        _infDetail.VcIntNo = AARTODataManager.GetVehicleColourIDByCode(xn.Attributes["Value"].Value);
                        break;
                    case "aainforgtypeid":
                        if (xn.Attributes["Value"].Value.Equals("-1"))
                        {
                            geted = true;
                            _infDetail.AaInfOrgTypeId = -1;
                            _infDetail.AaInfOtherOrgType = xn.Attributes["Other"].Value;
                        }
                        break;
                    case "aainflicodeid":
                        if (xn.Attributes["Value"].Value.Equals("-1"))
                        {
                            geted = true;
                            _infDetail.AaInfLiCodeId = -1;
                            _infDetail.AaInfForeignCode = xn.Attributes["Other"].Value;
                        }
                        break;
                    default:
                        break;
                }
                if (geted) continue;
                #endregion
                if (!Common.InsertValueIntoObjectByKey(xn.Attributes["FieldName"].Value, xn.Attributes["Value"].Value, _infDetail))
                {
                    Common.InsertValueIntoObjectByKey(xn.Attributes["FieldName"].Value, xn.Attributes["Value"].Value, _refundOfMoney);
                }
            }

        }
        protected override int DoValidation(out int needCancelledrepID, out int notIntNo)
        {
            needCancelledrepID = -1;
            notIntNo = -1;
            Notice notice = NoticeManager.GetNoticeByNotTicketNo(_aartoEntity.NotTicketNo);
            if (notice == null)
            {
                return (int)ValidationStatus.FalseForNoticeNumberNotExists;
            }
            else
            {
                notIntNo = notice.NotIntNo;
                return (int)ValidationStatus.TrueForAddNewOne;
            }
        }
        public override void DoImport(ImportDataEntity importData)
        {
            _importSuccess = true;
            _importData = importData;
            _infDetail = new AartoInfringerDetail();
            _refundOfMoney = new AartoRefundOfMoney();
            SIL.AARTO.DAL.Data.TransactionManager AARTOTM = SIL.AARTO.DAL.Services.ConnectionScope.CreateTransaction();

            SIL.DMS.DAL.Data.TransactionManager DMSTM = SIL.DMS.DAL.Services.ConnectionScope.CreateTransaction();
            bool UpdateAARTOSuccessful = false;
            bool UpdateDMSSuccessful = false;
            try
            {
                LoadRequestInformationFromXML();
                if (!AARTOTM.IsOpen)
                {
                    AARTOTM.BeginTransaction();
                }
                if (!DMSTM.IsOpen)
                {
                    DMSTM.BeginTransaction();
                }
                int needCancelledrepID;
                int validationResult = -1;
                int notIntNo = -1;
                validationResult = DoValidation(out needCancelledrepID, out notIntNo);
                switch (validationResult)
                {
                    case (int)ValidationStatus.FalseForNoticeNumberNotExists:
                        //notice number not exists.
                        DMSDataManager.UpdateImportResultToDMS(_importData.BaDoID, false, AARTOMessage.NOTICE_NUMBER_NOT_EXISTS);
                        _importSuccess = false;
                        break;
                    case (int)ValidationStatus.TrueForAddNewOne:
                        AartoRequestDocument doc = AARTODataManager.CreateAARTORequestDocumentAndImages(importData.BaDoID, (int)AartoDocumentTypeList.AARTO25, importData.IFSIntNo);
                        new AartoInfringerDetailService().Insert(_infDetail);
                        _refundOfMoney.AaInfDetailId = _infDetail.AaInfDetailId;
                        _refundOfMoney.AaReqDocId = doc.AaReqDocId;
                        _refundOfMoney.NotIntNo = notIntNo;
                        new AartoRefundOfMoneyService().Insert(_refundOfMoney);
                        NoticeManager.UpdateChargeStatus(ChargeStatusList.AARTO_RequestRefund_25, notIntNo);
                        DMSDataManager.UpdateImportResultToDMS(_importData.BaDoID, true, "");
                        break;
                    default: break;
                }
                UpdateAARTOSuccessful = true;
                UpdateDMSSuccessful = true;
                AARTOTM.Commit();
                DMSTM.Commit();
            }
            catch (Exception ex)
            {
                _importSuccess = false;
                if (!UpdateAARTOSuccessful || !UpdateDMSSuccessful)
                {
                    if (AARTOTM.IsOpen) AARTOTM.Rollback();
                    if (DMSTM.IsOpen) DMSTM.Rollback();
                }
                DMSDataManager.UpdateImportResultToDMS(_importData.BaDoID, false, AARTOMessage.UNKNOWN_ERROR);

                //Jake 2010-10-15 Write log using Enterprise Library log
                //ErrorLog.AddErrorLog(ex, AARTODataManager.LastUser);
                EntLibLogger.WriteErrorLog(ex, LogCategory.Error, AartoProjectList.AARTOImporterConsole.ToString());
            }
            finally
            {
                AARTOTM.Dispose();
                DMSTM.Dispose();

            }
        }
    }
}
