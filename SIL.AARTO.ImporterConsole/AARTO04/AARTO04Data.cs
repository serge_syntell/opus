﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SIL.AARTO.ImporterConsole.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.InfringerOption;
namespace SIL.AARTO.ImporterConsole.AARTO04
{
    public class AARTO04Data : AARTOData
    {
        public AARTO04Data()
        {
            _aartoEntity = new AARTOEntity();
        }
        protected override void LoadOtherData(XmlNodeList dataList)
        {
        }
        protected override void UpdateChargeStatus()
        {
            AARTODataManager.UpdateChargeStatus(ChargeStatusList.AARTO_ApplyToPayInstalments_04, _charge.ChgIntNo);
        }
        protected override void CreateReceiptForRepresentationDocument(int repID)
        {
            AARTODocumentManager.CreateAARTONoticeDocument(repID,(int)AartoDocumentTypeList.AARTO05a,AARTODataManager.LastUser);            
        }
        protected override int DoValidation(out int needCancelledrepID, out int notIntNo)
        {
            return base.DoValidation(out needCancelledrepID, out notIntNo);
        }
    }
}
