﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.ImporterConsole.Utility;
using System.Xml;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections;
using SIL.DMS.DAL.Entities;
using SIL.DMS.DAL.Services;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.AARTONoticeClock;
using System.Data;
using System.Configuration;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.ImporterConsole.AARTO31
{
    public class AARTO31Data : AARTOData
    {
        //Notice
        private const string NOTSOURCE = "AARTO31";
        private int NOTRDTYPECODE = 0;

        //Charge
        private int CTINTNO = 0;
        private const int CHARGESTATUS = (int)ChargeStatusList.AARTO_Loaded_31;
        private const int OfficerErrorCHARGESTATUS = (int)ChargeStatusList.OfficerError;

        //Film
        private int CONINTNO = 0;
        private const string CDLABEL = "AARTO31";
        private const string FILMDESCR = "AARTO31";

        //Frame
        private int LOCINTNO = 0;
        private int VMINTNO = 0;
        private int VTINTNO = 0;
        private int CAMINTNO = 0;
        private int TOINTNO = 0;
        private int RDTINTNO = 0;
        private int SZINTNO = 0;
        private int CRTINTNO = 0;
        private int REJINTNO = 0;
        private string AUTNO = "";
        private string CAMUNITID = "";
        private int FIRSTSPEED = 0;
        private int SECONDSPEED = 0;
        private string SEQUENCE = "";
        private string REGNO = "";
        private string ELAPSEDTIME = "";
        private string TRAVELDIRECTION = "";
        private string MANUALVIEW = "";
        private string MULTFRAMES = "";

        //private int vehicleMakeIntNo = 0;
        //private int vehicleTypeIntNo = 0;
        private int officerIntNo = 0;
        //private string courtName;
        private string offenceType;
        //private string chgOffenceCode;
        private int chgTypeIntNo = 0;
        private string locCode = string.Empty;
        private string locDescr = string.Empty;

        //Notice_Frame
        private string NFSOURCECODE = "";


        public AARTO31Data()
        {
            _aartoEntity = new AARTOEntity();
        }

        public override void DoImport(ImportDataEntity importDataEntity)
        {
            string OfficerErrors = "";
            string filmNo = "";
            string frameNo = "";
            #region loadXML
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(importDataEntity.BaDoResult);
            XmlElement rootElement = xmlDoc.DocumentElement;
            XmlNode datas = rootElement.SelectSingleNode(@"//Datas");
            OfficerErrors = datas.Attributes["OfficerErrorIds"] == null ? "" : datas.Attributes["OfficerErrorIds"].Value.Trim();
            filmNo = datas.Attributes["BatchId"].Value.Trim();
            frameNo = datas.Attributes["DocId"].Value.Trim();
            XmlNodeList dataList = rootElement.SelectNodes(@"//Data");

            Hashtable hsXML = new Hashtable();
            Hashtable hsXMLText = new Hashtable();
            foreach (XmlNode xn in dataList)
            {
                if (!hsXML.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["Value"] != null)
                {
                    hsXML.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["Value"].Value.Trim());
                }

                if (!hsXMLText.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["Text"] != null)
                {
                    hsXMLText.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["Text"].Value.Trim());
                }

            }
            #endregion
            bool noticeIsUpdate = false;
            string imgFilePath = DMSDataManager.GetBatchDocumentImageListByBatchDocumentID(importDataEntity.BaDoID)[0].BaDoImFilePath;

            SIL.AARTO.DAL.Data.TransactionManager AARTOTM = SIL.AARTO.DAL.Services.ConnectionScope.CreateTransaction();

            SIL.DMS.DAL.Data.TransactionManager DMSTM = SIL.DMS.DAL.Services.ConnectionScope.CreateTransaction();
            bool UpdateAARTOSuccessful = false;
            bool UpdateDMSSuccessful = false;
            try
            {
                
                if (!AARTOTM.IsOpen)
                {
                    AARTOTM.BeginTransaction();
                }
                if (!DMSTM.IsOpen)
                {
                    DMSTM.BeginTransaction();
                }
                #region Notice

                  int autIntNo = 0;
                SIL.AARTO.DAL.Entities.Authority aut = NoticeManager.GetAuthorityByAutCode(importDataEntity.ClientID);
                if (aut != null) autIntNo = aut.AutIntNo;

                string offenceCode = String.IsNullOrEmpty(hsXML["ChgOffenceCode"].ToString().Trim()) ? "" : hsXML["ChgOffenceCode"].ToString().Trim().Split('~')[0];
               
                Initialization("", "", "", "", "", offenceCode, autIntNo, _lastUser, "31");
                
                //if (!string.IsNullOrEmpty(offenceCode))
                //{
                    // Not complete

                    //SIL.AARTO.DAL.Entities.TList<Offence> offenceList = new OffenceService().Find(String.Format("OffCode={0}", offenceCode));
                    //if (offenceList.Count > 0)
                    //{
                    //    OffenceGroup offGroup = new SIL.AARTO.DAL.Services.OffenceGroupService().GetByOgIntNo(offenceList[0].OgIntNo);
                    //    notOffenceType = new OffenceTypeService().GetByOcTintNo(offGroup.OcTintNo).OcTcode;
                    //}

                //}

                Notice notice = null;
                string noticeTicketNo = hsXML["NotTicketNo"].ToString().Trim().Insert(2, "-").Insert(7, "-").Insert(17, "-");
                SIL.AARTO.DAL.Entities.TList<Notice> listNotice = new NoticeService().GetByNotTicketNo(noticeTicketNo);
                if (listNotice.Count > 0)
                {
                    noticeIsUpdate = true;
                    notice = listNotice[0];
                }
                else
                {
                    notice = new Notice();
                }

                notice.AutIntNo = autIntNo;

                notice.NotTicketNo = noticeTicketNo;

                DateTime offenceDate;
                DateTime.TryParse(hsXML["OffenceDate"].ToString().Trim() + " " + hsXML["OffenceTime"].ToString().Trim(), out offenceDate);
                notice.NotOffenceDate = offenceDate;

                //notice.NotLocCode = hsXML["OffencePlaceCode"].ToString().Trim();
                notice.NotLocDescr = hsXML["NotLocDescr"].ToString().Trim();

                notice.NotRegNo = hsXML["NotRegNo"].ToString().Trim();
                notice.OrigRegNo = notice.NotRegNo;

                notice.NotClearanceCert = hsXML["NotClearanceCert"].ToString().Trim();

                //notice.NotRegisterAuth = hsXML["NotRegisterAuth"].ToString().Trim();
                notice.AutName = hsXML["NotRegisterAuth"].ToString().Trim();
                notice.NotOffenceType = offenceType;


                notice.NotSource = "DMS";
                //notice1.NotIsOfficerError = !String.IsNullOrEmpty(OfficerErrors);
                
                SIL.AARTO.DAL.Entities.TList<TrafficOfficer> officerList = new TrafficOfficerService().GetByToNo(notice.NotOfficerNo);
                if (officerList.Count > 0)
                    notice.NotOfficerInit = officerList[0].ToInit;

                notice.NotLocCode = locCode;
                notice.NotLocDescr = locDescr;
                //notice1.NotSpeedLimit = 0;
                notice.NotCameraId = "000";
                notice.NotFilmType = "H";

                notice.NotLoadDate = DateTime.Now.Date;
                //if (aut != null)
                //{
                //    notice.AutName = aut.AutName;
                //}
                notice.NotPaymentDate = offenceDate.AddDays(32);
                notice.NotIssue1stNoticeDate = offenceDate;
                notice.NotIssue2ndNoticeDate = offenceDate;
                notice.NotSendTo = "O";
                notice.NotTicketProcessor = _TicketProcessor;
                notice.LastUser = _lastUser;

                notice = new NoticeService().Save(notice);

                #endregion

                #region Charge

                Charge charge = null;

                charge = NoticeManager.GetChargeByNotIntNo(notice.NotIntNo);
                if (charge == null)
                {
                    charge = new Charge();
                    
                }

                charge.ChgIsMain = true;
                charge.NotIntNo = notice.NotIntNo;
                charge.CtIntNo = chgTypeIntNo;//
                charge.ChgType = "Minor";
                charge.ChgLegislation = "1";
                charge.ChgOffenceCode = hsXML["ChgOffenceCode"].ToString().Trim();
                charge.ChgStatutoryRef = hsXML["ChgStatutoryRef"].ToString().Trim();
                charge.ChgOffenceDescr = hsXML["ChgOffenceDescr"].ToString().Trim();
                if (hsXML["ChgFineAmount"] == null || hsXML["ChgFineAmount"].ToString().Trim() == "")
                    charge.ChgFineAmount = 0;
                else
                {
                    charge.ChgFineAmount = Convert.ToSingle(hsXML["ChgFineAmount"]);
                    charge.ChgRevFineAmount =Convert.ToSingle(hsXML["ChgFineAmount"]);
                }
                charge.ChargeStatus = OfficerErrors.Trim().Length > 0 ? OfficerErrorCHARGESTATUS : CHARGESTATUS; ;//
                charge.LastUser = _lastUser;
                charge.ChgOffenceType = offenceType;

                new ChargeService().Save(charge);

                #endregion

                if (!noticeIsUpdate)
                {

                    #region Film
                    Film film = new Film();
                    film.AutIntNo = autIntNo;
                    film.ConIntNo = CONINTNO;//
                    film.FilmNo = filmNo;
                    film.CdLabel = CDLABEL;//
                    film.FilmDescr = FILMDESCR;//
                    //film.NoOfFrames 
                    film.FilmType = "H";
                    film.FilmLoadDateTime = DateTime.Now;
                    film.LastUser = _lastUser;

                    new FilmService().Save(film);

                    #endregion

                    #region Frame
                    Frame frame = new Frame();
                    frame.FilmIntNo = film.FilmIntNo;
                    frame.FrameNo = "";
                    frame.OffenceDate = offenceDate;
                    frame.FrameImagePath = imgFilePath;
                    frame.IfsIntNo = importDataEntity.IFSIntNo;
                    frame.LocIntNo = LOCINTNO;//
                    frame.VmIntNo = VMINTNO;//
                    frame.VtIntNo = VTINTNO;//
                    frame.CamIntNo = CAMINTNO;
                    frame.ToIntNo = TOINTNO;//
                    frame.RdTintNo = RDTINTNO;//
                    frame.SzIntNo = SZINTNO;//
                    frame.CrtIntNo = CRTINTNO;//
                    frame.RejIntNo = REJINTNO;//
                    frame.AutNo = AUTNO;//
                    frame.CamUnitId = CAMUNITID;//
                    frame.FirstSpeed = (byte)FIRSTSPEED;//
                    frame.SecondSpeed = (byte)SECONDSPEED;//
                    frame.Sequence = SEQUENCE;//
                    frame.RegNo = notice.NotRegNo;//
                    frame.ElapsedTime = ELAPSEDTIME;
                    frame.TravelDirection = TRAVELDIRECTION;//
                    frame.ManualView = MANUALVIEW;//
                    frame.MultFrames = MULTFRAMES;//
                    frame.FrameStatus = 10;
                    frame.LastUser = _lastUser;
                    frame.DmsDocumentNo = frameNo;

                    new FrameService().Save(frame);


                    #endregion

                    #region Notice_Frame
                    NoticeFrame noticeFrame = new NoticeFrame();
                    noticeFrame.NotIntNo = notice.NotIntNo;
                    noticeFrame.FrameIntNo = frame.FrameIntNo;
                    noticeFrame.NfSourceCode = NFSOURCECODE;//
                    noticeFrame.LastUser = _lastUser;

                    new NoticeFrameService().Save(noticeFrame);
                    #endregion

                    DiscountPeriodClock clock = new DiscountPeriodClock(notice.NotIntNo);
                    clock.LastUser = _lastUser;
                    clock.Create();
                    clock.Start(ConvertDateTime.GetShortDateTime(offenceDate));

                    AARTODataManager.CreateAARTONoticeDocumentAndImages(notice.NotIntNo, importDataEntity.BaDoID, (int)AartoDocumentTypeList.AARTO31, (int)AartoDocumentStatusList.Parking, importDataEntity.IFSIntNo);
                }

                DMSDataManager.UpdateImportResultToDMS(importDataEntity.BaDoID, true, "");
                AARTODataManager.SaveOfficerErrorsWithNotice(notice.NotIntNo, OfficerErrors, AARTODataManager.LastUser);
                

                UpdateAARTOSuccessful = true;
                //update status into DMS
                UpdateDMSSuccessful = true;
                AARTOTM.Commit();
                DMSTM.Commit();
            }
            catch (Exception ex)
            {
                _importSuccess = false;
                if (!UpdateAARTOSuccessful || !UpdateDMSSuccessful)
                {
                    if (AARTOTM.IsOpen) AARTOTM.Rollback();
                    if (DMSTM.IsOpen) DMSTM.Rollback();
                }
                DMSDataManager.UpdateImportResultToDMS(importDataEntity.BaDoID, false, AARTOMessage.UNKNOWN_ERROR);

                //Jake 2010-10-15 Write log using Enterprise Library log
                //ErrorLog.AddErrorLog(ex, AARTODataManager.LastUser);
                EntLibLogger.WriteErrorLog(ex, LogCategory.Error, AartoProjectList.AARTOImporterConsole.ToString());
            }
            finally
            {
                AARTOTM.Dispose();
                DMSTM.Dispose();

            }
        }

        private bool Initialization(string crtNo, string notVehicleMakeCode,
            string notVehicleTypeCode, string notOfficerNo,
            string crtRoomNo, string chgOffenceCode,
            int autIntNo, string lastUser, string cType)
        {
            bool success = false;
            string conCode = ConfigurationManager.AppSettings["ContractorCode"].ToString();

            using (IDataReader reader = new NoticeService().InitialForImporter(
                crtNo,
                notVehicleMakeCode,
                notVehicleTypeCode,
                notOfficerNo,
                chgOffenceCode,
                crtRoomNo,
                conCode,
                autIntNo,
                lastUser,
                cType))
            {
                while (reader.Read())
                {
                    CRTINTNO = reader["CrtIntNo"] == null ? 0 : Convert.ToInt32(reader["CrtIntNo"]);
                    //CRTRINTNo = reader["CrtRIntNo"] == null ? 0 : Convert.ToInt32(reader["CrtRIntNo"]);
                    CONINTNO = reader["ConIntNo"] == null ? 0 : Convert.ToInt32(reader["ConIntNo"]);
                    LOCINTNO = reader["LocIntNo"] == null ? 0 : Convert.ToInt32(reader["LocIntNo"]);
                    locCode = reader["LocCode"] == null ? "" : reader["LocCode"].ToString();
                    locDescr = reader["LocDesc"] == null ? "" : reader["LocDesc"].ToString();
                    VMINTNO = reader["VehicleMakeIntNo"] == null ? 0 : Convert.ToInt32(reader["VehicleMakeIntNo"]);
                    VTINTNO = reader["VehicleTypeIntNo"] == null ? 0 : Convert.ToInt32(reader["VehicleTypeIntNo"]);
                    CAMINTNO = reader["CameraIntNo"] == null ? 0 : Convert.ToInt32(reader["CameraIntNo"]);
                    TOINTNO = reader["OfficerIntNo"] == null ? 0 : Convert.ToInt32(reader["OfficerIntNo"]);
                    RDTINTNO = reader["RdTIntNo"] == null ? 0 : Convert.ToInt32(reader["RdTIntNo"]);
                    NOTRDTYPECODE = reader["RdTypeCode"] == null ? 0 : Convert.ToInt32(reader["RdTypeCode"]);
                    SZINTNO = reader["SzIntNo"] == null ? 0 : Convert.ToInt32(reader["SzIntNo"]);
                    REJINTNO = reader["RejIntNo"] == null ? 0 : Convert.ToInt32(reader["RejIntNo"]);
                    AUTNO = reader["AutNo"] == null ? "" : reader["AutNo"].ToString();
                    CAMUNITID = reader["CamerUnitIntNo"] == null ? "" : reader["CamerUnitIntNo"].ToString();
                    //courtName = reader["CourtName"] == null ? "" : reader["CourtName"].ToString();
                    offenceType = reader["OffenceType"] == null ? "" : reader["OffenceType"].ToString();
                    chgTypeIntNo = reader["ChgTypeIntNo"] == null ? 0 : Convert.ToInt32(reader["ChgTypeIntNo"]);
                    success = true;
                    break;
                }
            }
            return success;
        }
    }
}
