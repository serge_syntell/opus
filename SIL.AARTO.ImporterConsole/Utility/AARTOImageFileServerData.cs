﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
namespace SIL.AARTO.ImporterConsole.Utility
{
    public class AARTOImageFileServerData
    {
        private AARTOImageFileServerData()
        {
            _data = new Hashtable();
        }
        private Hashtable _data;
        public void Add(int dmsIFSID, int aartoIFSID)
        {
            _data.Add(dmsIFSID, aartoIFSID);
        }
        public int GetByDMSIFSID(int id)
        {
            if (!_data.ContainsKey(id))
            {
                _data.Add(id, AARTODataManager.GetAARTOIFSIDByDMSIFS(id));
            }
            return (int)_data[id];
        }

        private static AARTOImageFileServerData _entity;
        public static AARTOImageFileServerData Get()
        {
            if (_entity == null) _entity = new AARTOImageFileServerData();
            return _entity;
        }
        public void Clear()
        {
            _data.Clear();
        }
    }
}
