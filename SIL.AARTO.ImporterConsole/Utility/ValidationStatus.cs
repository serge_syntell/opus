﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.ImporterConsole.Utility
{
    public enum ValidationStatus
    {
        FalseForNoticeNumberNotExists = 0,
        FalseForSameTypeAlreadyAdjudicated = 1,
        TrueForSameTypeNotAdjudicated = 2,
        TrueForAddNewOne = 3,
        FalseForDifferentTypeAlreadySuccessful = 4,
        FalseForNoticeHasBeenPaidOrFinalised = 5,
        FalseForWarrantOfAttachmentStagePassed = 6,
        FalseForNotEnforcementOrderStage = 7
    }
}
