﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.ImporterConsole.Utility
{
    public class ImportDataEntity
    {
        public string ClientID { get; set; }
        public string BaDoID { get; set; }
        public string BaDoResult { get; set; }
        public DateTime CaptureDate { get; set; }
        public string DoTeID { get; set; }
        public int DoTeTypeID { get; set; }
        public int IFSIntNo { get; set; }
    }
}
