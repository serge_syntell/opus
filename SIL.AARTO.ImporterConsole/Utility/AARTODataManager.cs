﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using SIL.DMS.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.ImporterConsole.AARTO07;
using SIL.AARTO.BLL.AARTONoticeClock;
using SIL.AARTO.BLL.EntLib;
namespace SIL.AARTO.ImporterConsole.Utility
{
    class AARTODataManager
    {
        public const string LastUser = "Importer";

        #region representation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="noticeNumber"></param>
        /// <param name="docTypeID"></param>
        /// <param name="repID"></param>
        /// <returns></returns>
        public static int IsRepresentationValid(string noticeNumber, int docTypeID, out int repID, out int notIntNo)
        {
            int validationResult = -1;
            
            //-- 0 NoticeNumber dose not exist
            //-- 1 same type doc, have rep(adjudicated). (load canceled rep,reason with Already Adjudicated)	
            //-- 2 same type doc, have rep(registered);canceling the existing Rep (@RepID),and load canceled Rep.
            //-- 3 same type doc, have no rep(registered).(can add new)
            //-- 4 diffrent type doc,not unsuccessful.(load canceled rep,reason with Already Adjudicated)	
            //-- 5 notice has been paid or finalised.
            //-- 6 the rep. can't be submitted anymore after the main flow passed Warrant of attachment stage.
            //-- 7 must be in the enforcement order stage.
            repID = -1;
            notIntNo = -1;
            IDataReader idr = new AartoRepresentationService().IsRepresentationValid(noticeNumber, docTypeID
                , (int)RepresentationCodeList.AARTO_RepresentationRegistered
                , (int)RepresentationCodeList.AARTO_RepresentationUnsuccessful
                , (int)RepresentationCodeList.AARTO_RepresentationSuccessful
                , (int)RepresentationCodeList.AARTO_RepresentationAdviseElectCourt
                , (int)ChargeStatusList.Completion
                , (int)ChargeStatusList.AARTO_PrintWarrant_24);
            try
            {
                while (idr.Read())
                {
                    validationResult = idr["RetValue"] == DBNull.Value ? (int)ValidationStatus.FalseForNoticeNumberNotExists : Convert.ToInt32(idr["RetValue"]);
                    repID = idr["RepID"] == DBNull.Value ? -1 : Convert.ToInt32(idr["RepID"]);
                    notIntNo = idr["NotIntNo"] == DBNull.Value ? -1 : Convert.ToInt32(idr["NotIntNo"]);
                    break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                idr.Close();
            }
            return validationResult;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="noticeNumber"></param>
        /// <param name="docTypeID"></param>
        /// <param name="repID"></param>
        /// <returns></returns>
        public static int IsRepresentationValidForAARTO14(string noticeNumber, int docTypeID, out int repID, out int notIntNo)
        {
            int validationResult = -1;
            //-- 0 NoticeNumber dose not exist
            //-- 1 same type doc, have rep(adjudicated). (load canceled rep,reason with Already Adjudicated)	
            //-- 2 same type doc, have rep(registered);canceling the existing Rep (@RepID),and load canceled Rep.
            //-- 3 same type doc, have no rep(registered).(can add new)
            //-- 4 diffrent type doc,not unsuccessful.(load canceled rep,reason with Already Adjudicated)	
            //-- 5 notice has been paid or finalised.
            //-- 6 the rep. can't be submitted anymore after the main flow passed enforement order stage.
            //-- 7 must be in the enforcement order stage.
            repID = -1;
            notIntNo = -1;
            IDataReader idr = new AartoRepresentationService().IsRepresentationValidForAARTO14(noticeNumber, docTypeID
                , (int)RepresentationCodeList.AARTO_RepresentationRegistered
                , (int)RepresentationCodeList.AARTO_RepresentationUnsuccessful
                , (int)RepresentationCodeList.AARTO_RepresentationSuccessful
                , (int)ChargeStatusList.Completion
                , (int)AartoClockTypeList.EnforcementOrder);
            try
            {
                while (idr.Read())
                {
                    validationResult = idr["RetValue"] == DBNull.Value ? (int)ValidationStatus.FalseForNoticeNumberNotExists : Convert.ToInt32(idr["RetValue"]);
                    repID = idr["RepID"] == DBNull.Value ? -1 : Convert.ToInt32(idr["RepID"]);
                    notIntNo = idr["NotIntNo"] == DBNull.Value ? -1 : Convert.ToInt32(idr["NotIntNo"]);
                    break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                idr.Close();
            }
            return validationResult;
        }
        public static AartoRepresentation InsertAARTORepresentation(AartoRepresentation rep)
        {
            rep.LastUser = LastUser;
            return new AartoRepresentationService().Save(rep);
        }
        public static bool CancelRepresentation(int repID, Charge cg)
        {
            AartoRepresentation rep = new AartoRepresentationService().GetByAaRepId(repID);            
            if (rep != null)
            {
                cg.ChargeStatus = rep.CsCode;
                new ChargeService().Save(cg);
                rep.RcCode = (int)RepresentationCodeList.RepresentationCancelled;//canceled  
                rep.AaRepCanceledDate = DateTime.Now;
                rep.LastUser = LastUser;
                return new AartoRepresentationService().Update(rep);
            }
            else return true;
        }
        #endregion
        public static void UpdateChargeStatus(ChargeStatusList csCode, int chgIntNo)
        {
            new ChargeService().UpdateChargeStatusWhenRegisterRepresentation(chgIntNo, (int)csCode, LastUser);
        }
        public static Charge GetChargeByNoticeNumber(string noticeNumber)
        {
            Charge charge = null;
            SIL.AARTO.DAL.Entities.TList<Charge> cgList = null;
            Notice notice = GetNoticeByNoticeNumber(noticeNumber);
            if (notice != null)
            {
                cgList = new ChargeService().GetByNotIntNo(notice.NotIntNo);
                if (cgList.Count > 0)
                    charge = cgList[0];
            }
            return charge;
        }
        public static Notice GetNoticeByNoticeNumber(string noticeNumber)
        {
            Notice notice = null;
            SIL.AARTO.DAL.Entities.TList<Notice> nList = new NoticeService().GetByNotTicketNo(noticeNumber);
            if (nList.Count > 0)
            {
                notice = nList[0];
            }
            return notice;
        }
        public static Charge GetChargeByNotIntNo(int notIntNo)
        {
            Charge charge = null;
            SIL.AARTO.DAL.Entities.TList<Charge> cgList = null;
            Notice notice = GetNoticeByNotIntNo(notIntNo);
            if (notice != null)
            {
                cgList = new ChargeService().GetByNotIntNo(notice.NotIntNo);
                if (cgList.Count > 0)
                    charge = cgList[0];
            }
            return charge;
        }
        public static Notice GetNoticeByNotIntNo(int notIntNo)
        {
            Notice notice = new NoticeService().GetByNotIntNo(notIntNo);
            return notice;
        }
        /// <summary>
        /// add aarto document and iamges
        /// </summary>
        /// <param name="notIntNo"></param>
        /// <param name="baDoID"></param>
        public static AartoRepDocument CreateAARTORepDocumentAndImages(int repID, string baDoID, int docTypeID, int ifsIntNo)
        {
            AartoRepDocument doc = new AartoRepDocument();
            doc.DmsBaDoId = baDoID;
            doc.IfsIntNo = ifsIntNo;
            doc.AaDocTypeId = docTypeID;
            doc.AaDocStatusId = (int)AartoDocumentStatusList.Received;
            doc.AaRepDocReceivedDate = DateTime.Now;
            doc.AaRepId = repID;
            doc.LastUser = LastUser;
            doc = new AartoRepDocumentService().Save(doc);

            //get doc image info
            SIL.DMS.DAL.Entities.TList<BatchDocumentImage> DMSImageList = DMSDataManager.GetBatchDocumentImageListByBatchDocumentID(baDoID);

            if (DMSImageList.Count > 0)
            {
                SIL.AARTO.DAL.Entities.TList<AartoDocumentImage> AARTOImageList = new SIL.AARTO.DAL.Entities.TList<AartoDocumentImage>();
                AartoDocumentImage adImage = null;
                foreach (BatchDocumentImage bdi in DMSImageList)
                {
                    adImage = new AartoDocumentImage();
                    adImage.AaDocId = doc.AaRepDocId;
                    adImage.AaDocSourceTableId = (int)AartoDocumentSourceTableList.AARTORepDocument;
                    adImage.AaDocImgDateLoaded = DateTime.Now;
                    adImage.AaDocImgOrder = bdi.BaDoImOrder;
                    adImage.AaDocImgPath = bdi.BaDoImFilePath;
                    adImage.LastUser = LastUser;
                    AARTOImageList.Add(adImage);
                }
                new AartoDocumentImageService().Save(AARTOImageList);
            }
            return doc;
        }
        public static AartoNoticeDocument CreateAARTONoticeDocumentAndImages(int noticeID, string baDoID, int docTypeID,int docStatusID, int ifsIntNo)
        {
            AartoNoticeDocument doc = new AartoNoticeDocument();
            //doc.DmsBaDoId = baDoID;
            doc.IfsIntNo = ifsIntNo;
            doc.AaDocTypeId = docTypeID;
            doc.AaDocStatusId = docStatusID;// (int)AartoDocumentStatusList.Received;
            doc.AaDocCreatedDate = DateTime.Now;
            doc.NotIntNo = noticeID;
            doc.LastUser = LastUser;
            doc.AaNoticeDocNo = baDoID;

            doc = new AartoNoticeDocumentService().Save(doc);

            //get doc image info
            SIL.DMS.DAL.Entities.TList<BatchDocumentImage> DMSImageList = DMSDataManager.GetBatchDocumentImageListByBatchDocumentID(baDoID);

            if (DMSImageList.Count > 0)
            {
                SIL.AARTO.DAL.Entities.TList<AartoDocumentImage> AARTOImageList = new SIL.AARTO.DAL.Entities.TList<AartoDocumentImage>();
                AartoDocumentImage adImage = null;
                foreach (BatchDocumentImage bdi in DMSImageList)
                {
                    adImage = new AartoDocumentImage();
                    adImage.AaDocId = doc.AaNotDocId;
                    adImage.AaDocSourceTableId = (int)AartoDocumentSourceTableList.AARTONoticeDocument;
                    adImage.AaDocImgDateLoaded = DateTime.Now;
                    adImage.AaDocImgOrder = bdi.BaDoImOrder;
                    adImage.AaDocImgPath = bdi.BaDoImFilePath;
                    adImage.LastUser = LastUser;
                    AARTOImageList.Add(adImage);
                }
                new AartoDocumentImageService().Save(AARTOImageList);
            }
            return doc;
        }
        public static AartoRequestDocument CreateAARTORequestDocumentAndImages(string baDoID, int docTypeID, int ifsIntNo)
        {
            AartoRequestDocument doc = new AartoRequestDocument();
            //doc.DmsBaDoId = baDoID;
            doc.IfsIntNo = ifsIntNo;
            doc.AaDocTypeId = docTypeID;
            doc.AaDocStatusId = (int)AartoDocumentStatusList.Received;
            doc.AaDocCreatedDate = DateTime.Now;
            doc.LastUser = LastUser;
            doc = new AartoRequestDocumentService().Save(doc);

            //get doc image info
            SIL.DMS.DAL.Entities.TList<BatchDocumentImage> DMSImageList = DMSDataManager.GetBatchDocumentImageListByBatchDocumentID(baDoID);

            if (DMSImageList.Count > 0)
            {
                SIL.AARTO.DAL.Entities.TList<AartoDocumentImage> AARTOImageList = new SIL.AARTO.DAL.Entities.TList<AartoDocumentImage>();
                AartoDocumentImage adImage = null;
                foreach (BatchDocumentImage bdi in DMSImageList)
                {
                    adImage = new AartoDocumentImage();
                    adImage.AaDocId = doc.AaReqDocId;
                    adImage.AaDocSourceTableId = (int)AartoDocumentSourceTableList.AARTORequestDocument;
                    adImage.AaDocImgDateLoaded = DateTime.Now;
                    adImage.AaDocImgOrder = bdi.BaDoImOrder;
                    adImage.AaDocImgPath = bdi.BaDoImFilePath;
                    adImage.LastUser = LastUser;
                    AARTOImageList.Add(adImage);
                }
                new AartoDocumentImageService().Save(AARTOImageList);
            }
            return doc;
        }
        public static int GetAARTOIFSIDByDMSIFS(int ifsID)
        {
            //should get data from a program cash secondly.
            SIL.DMS.DAL.Entities.ImageFileServer ifs = new SIL.DMS.DAL.Services.ImageFileServerService().GetByIfsIntNo(ifsID);
            SIL.AARTO.DAL.Entities.ImageFileServer aartoIfs= new SIL.AARTO.DAL.Services.ImageFileServerService().GetByAaSysFileTypeIdImageMachineNameImageShareName(
                (int)AartoSystemFileTypeList.DMSScanImage, ifs.ImageMachineName, ifs.ImageShareName);
            if (aartoIfs == null)
            {
                aartoIfs = AddAARTOIFSByDMSIFS(ifs);
            }
            return aartoIfs.IfsIntNo;
        }
        public static AartoRepDocument InsertAartoRepDocument(AartoRepDocument repDoc)
        {
            repDoc.LastUser = LastUser;
            return new AartoRepDocumentService().Save(repDoc);
        }
        public static bool AddEvidencePack(string ePItemDescr, int chgIntNo, string ePSourceTable, int ePSourceID)
        {
            EvidencePack ep = new EvidencePack();
            ep.ChgIntNo = chgIntNo;
            ep.EpItemDate = DateTime.Now;
            ep.EpItemDescr = ePItemDescr;
            ep.EpSourceTable = ePSourceTable;
            ep.EpSourceId = ePSourceID;
            ep.LastUser = LastUser;
            return new EvidencePackService().Insert(ep);
        }

        public static void PauseTheNoticeClock(int notIntNo, int repID)
        {
            Clock clock = new Clock(notIntNo);
            clock.LastUser = LastUser;
            clock.Pause(repID);
        }
        private static SIL.AARTO.DAL.Entities.ImageFileServer AddAARTOIFSByDMSIFS(SIL.DMS.DAL.Entities.ImageFileServer dmsIFS)
        {
            SIL.AARTO.DAL.Entities.ImageFileServer aartoIFSNew = new SIL.AARTO.DAL.Entities.ImageFileServer();
            aartoIFSNew.AaSysFileTypeId = (int)AartoSystemFileTypeList.DMSScanImage;
            aartoIFSNew.ImageMachineName = dmsIFS.ImageMachineName;
            aartoIFSNew.ImageShareName = dmsIFS.ImageShareName;
            aartoIFSNew.IfServerName = dmsIFS.IfServerName;
            aartoIFSNew.ImageDrive = dmsIFS.ImageDrive;
            aartoIFSNew.ImageRootFolder = dmsIFS.ImageRootFolder;
            aartoIFSNew.ImageDrive = dmsIFS.ImageDrive;
            return new SIL.AARTO.DAL.Services.ImageFileServerService().Save(aartoIFSNew);
        }
        public static bool IsTheImageFileServerSame()
        {
            try
            {
                SIL.DMS.DAL.Entities.TList<SIL.DMS.DAL.Entities.ImageFileServer> dmsIFSList = DMSDataManager.GetImageFileServerListFromDMS();
                SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.ImageFileServer> aartoIFSList = new ImageFileServerService().GetAll();                
                SIL.AARTO.DAL.Entities.ImageFileServer aartoIFSNew = null;
                List<SIL.AARTO.DAL.Entities.ImageFileServer> tempIFSList = null;
                foreach (SIL.DMS.DAL.Entities.ImageFileServer ifs in dmsIFSList)
                {
                    tempIFSList = aartoIFSList.Where(tmp =>
                        (tmp.AaSysFileTypeId == (int)AartoSystemFileTypeList.DMSScanImage
                        && tmp.ImageMachineName == ifs.ImageMachineName
                        && tmp.ImageShareName == ifs.ImageShareName)).ToList();
                    if (tempIFSList.Count == 0)
                    {
                        aartoIFSNew = AddAARTOIFSByDMSIFS(ifs);
                        AARTOImageFileServerData.Get().Add(ifs.IfsIntNo, aartoIFSNew.IfsIntNo);
                    }
                    else
                        AARTOImageFileServerData.Get().Add(ifs.IfsIntNo, tempIFSList[0].IfsIntNo);
                }
            }
            catch (Exception ex)
            {
                //ErrorLog.AddErrorLog(ex, LastUser);
                EntLibLogger.WriteErrorLog(ex, LogCategory.Error, AartoProjectList.AARTOImporterConsole.ToString());
            }
            return true;
        }
        public static string GetDocumentDescriptionByDocTypeID(int docTypeID)
        {
            return ((AartoDocumentTypeList)docTypeID).ToString();
        }
        #region officer error
        public static bool SaveOfficerErrorsWithNotice(int notIntNo, string officerErrors,string lastUser)
        {
            SIL.AARTO.DAL.Entities.TList<AartoOfficerError> aartoErrors = new SIL.AARTO.DAL.Services.AartoOfficerErrorService().GetAll();
            SIL.DMS.DAL.Entities.TList<SIL.DMS.DAL.Entities.OfficerError> dmsErrors = new SIL.DMS.DAL.Services.OfficerErrorService().GetAll();
            SIL.AARTO.DAL.Entities.AartoNoticeOfficerError aartoErr = null;
            SIL.AARTO.DAL.Entities.AartoOfficerError aartoOffErr = null;
            SIL.DMS.DAL.Entities.OfficerError dmsOffErr = null;

            string[] offArr = officerErrors.Split('~');
            try
            {
                SIL.AARTO.DAL.Entities.TList<AartoNoticeOfficerError> list = new SIL.AARTO.DAL.Services.AartoNoticeOfficerErrorService().GetByNotIntNo(notIntNo);
                new SIL.AARTO.DAL.Services.AartoNoticeOfficerErrorService().Delete(list);

                for (int i = 0; i < offArr.Length; i++)
                {
                    if (aartoErrors.Find(err => err.AaOfErId.ToString() == offArr[i]) == null)
                    {
                        dmsOffErr = dmsErrors.Find(derr => derr.OfErId.ToString() == offArr[i]);
                        aartoOffErr = new AartoOfficerError();
                        aartoOffErr.AaOfErName = dmsOffErr.OfErName;
                        aartoOffErr.AaOfErDescription = dmsOffErr.OfErDescription;
                        aartoOffErr.AaOfErId = dmsOffErr.OfErId;
                        aartoOffErr.LastUser = lastUser;
                        new AartoOfficerErrorService().Insert(aartoOffErr);
                    }
                    aartoErr = new SIL.AARTO.DAL.Entities.AartoNoticeOfficerError();
                    aartoErr.AaOfErId = Convert.ToInt32(offArr[i]);
                    aartoErr.NotIntNo = notIntNo;
                    aartoErr.LastUser = lastUser;
                    new SIL.AARTO.DAL.Services.AartoNoticeOfficerErrorService().Insert(aartoErr);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region vehicle

        public static int? GetVehicleTypeIDByCode(string code)
        {
            try
            {
                return new VehicleTypeService().GetByVtCode(code)[0].VtIntNo;
            }
            catch
            {
                return null;
            }
        }
        public static int? GetVehicleColourIDByCode(string code)
        {
            try
            {
                return new VehicleColourService().GetByVcCode(code)[0].VcIntNo;
            }
            catch
            {
                return null;
            }
        }
        public static int? GetVehicleMakeIDByCode(string code)
        {
            try
            {
                return new VehicleMakeService().GetByVmCode(code)[0].VmIntNo;
            }
            catch
            {
                return null;
            }
        }
        public static int? GetCourtIDByCode(string code)
        {
            try
            {
                //return null;
                return new CourtService().GetByCrtNo(code).CrtIntNo;
            }
            catch
            {
                return null;
            }
        }
        #endregion
    }
}
