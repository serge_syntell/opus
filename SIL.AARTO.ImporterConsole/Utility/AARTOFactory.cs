﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.ImporterConsole.AARTO07;
using SIL.AARTO.ImporterConsole.AARTO08;
using SIL.AARTO.ImporterConsole.AARTO04;
using SIL.AARTO.ImporterConsole.AARTO10;
using System.Reflection;
using SIL.AARTO.ImporterConsole.AARTO01;
using SIL.AARTO.ImporterConsole.AARTO31;
namespace SIL.AARTO.ImporterConsole.Utility
{
    public class AARTOFactory
    {
        public static AARTOData CreateAARTODataInstance(int doTeTypeID)
        {
            string assmName = "SIL.AARTO.ImporterConsole";
            string className = ((AartoDocumentTypeList)doTeTypeID).ToString() + "Data";
            Assembly assm = Assembly.Load(assmName);
            return (AARTOData)assm.CreateInstance(assmName + "." + ((AartoDocumentTypeList)doTeTypeID).ToString() + "." + className);
            //switch (doTeTypeID)
            //{
            //    case (int)AartoDocumentTypeList.AARTO07:
            //        return new AARTO07Data();
            //    case (int)AartoDocumentTypeList.AARTO08:
            //        return new AARTO08Data();
            //    case (int)AartoDocumentTypeList.AARTO04:
            //        return new AARTO04Data();
            //    case (int)AartoDocumentTypeList.AARTO10:
            //        return new AARTO10Data();
            //    default: return null;                     
            //}
        }
    }
}
