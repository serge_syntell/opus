﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using SIL.DMS.DAL.Data;
using SIL.DMS.DAL.Entities;
using SIL.DMS.DAL.Services;
namespace SIL.AARTO.ImporterConsole.Utility
{
    class DMSDataManager
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientID"></param>
        /// <param name="importNumOnce"></param>
        /// <returns></returns>
        public static List<ImportDataEntity> GetBatchDocumentListForImport(string clientID, int importNumOnce)
        {
            List<ImportDataEntity> list = new List<ImportDataEntity>();
            IDataReader idr = new BatchDocumentService().GetBatchDocumentListByClientIDAndImportStatus(clientID, importNumOnce, (int)BatchStatusList.QAFinished, (int)BatchDocumentStatusList.ImportFailed, (int)BatchDocumentStatusList.ImportSucceeded,true);
            try
            {
                ImportDataEntity bde = null;
                while (idr.Read())
                {
                    bde = new ImportDataEntity();
                    bde.IFSIntNo=idr["IFSIntNo"] is DBNull ? -1 : Convert.ToInt32(idr["IFSIntNo"]);
                    bde.IFSIntNo = AARTOImageFileServerData.Get().GetByDMSIFSID(bde.IFSIntNo);
                    bde.BaDoID = idr["BaDoID"].ToString();
                    bde.BaDoResult = idr["BaDoResult"].ToString();
                    bde.CaptureDate = idr["CaptureDate"] is DBNull ? DateTime.MinValue : Convert.ToDateTime(idr["CaptureDate"]);
                    bde.ClientID = idr["ClientID"].ToString();
                    bde.DoTeID = idr["DoTeID"].ToString();
                    bde.DoTeTypeID = idr["DoTeTypeID"] is DBNull ? -1 : Convert.ToInt32(idr["DoTeTypeID"]);
                    list.Add(bde);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                idr.Close();
            }
            return list;
        }
        public static TList<BatchDocumentImage> GetBatchDocumentImageListByBatchDocumentID(string baDoID)
        {
            return new SIL.DMS.DAL.Services.BatchDocumentImageService().GetByBaDoId(baDoID);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="baDoID"></param>
        /// <param name="importSuccess"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static bool UpdateImportResultToDMS(string baDoID, bool importSuccess, string msg)
        {
            BatchDocument bd = new BatchDocumentService().GetByBaDoId(baDoID);
            bd.BaDoStId = importSuccess ? (int)BatchDocumentStatusList.ImportSucceeded : (int)BatchDocumentStatusList.ImportFailed;
            bd.ImportMessage = msg;
            return new BatchDocumentService().Update(bd);
        }

        public static TList<ImageFileServer> GetImageFileServerListFromDMS()
        {
            return new ImageFileServerService().GetAll();
        }
    }
}
