﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.ImporterConsole.Utility
{
    public class AARTOMessage
    {
        public const string ALREADY_ADJUDICATED = "Already adjudicated.";
        public const string ALREADY_PAID_OR_FINALISED = "Already finalised.";
        public const string NOTICE_NUMBER_NOT_EXISTS = "The notice number does not exist.";
        public const string Warrant_STAGE_PASSED = "Warrant Of attachment stage passed.";
        public const string NOT_ENFORCEMENT_ORDER_STAGE = "Not enforement order stage.";
        public const string UNKNOWN_ERROR = "Unknown error.";
    }
}
