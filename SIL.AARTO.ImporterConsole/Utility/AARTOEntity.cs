﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.ImporterConsole.Utility
{
    public class AARTOEntity
    {
        public string NotTicketNo { get; set; }
        public string NotOffenceDate { get; set; }
        public string IssuingAuthority { get; set; }
    }
}
