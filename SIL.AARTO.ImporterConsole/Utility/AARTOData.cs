﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.EntLib;
namespace SIL.AARTO.ImporterConsole.Utility
{
    public class AARTOData
    {
        protected ImportDataEntity _importData;
        protected AartoRepresentation _rep;
        protected AARTOEntity _aartoEntity;
        protected AartoRepDocument _document;
        protected Charge _charge;
        protected Notice _notice;
        protected bool _importSuccess;
        protected const string _lastUser = "AARTOImporterConsole";
        protected const string _TicketProcessor = "AARTO"; 
        public AARTOData()
        {
        }
        private void LoadDataFromXML()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(_importData.BaDoResult);
            XmlElement rootElement = xmlDoc.DocumentElement;
            XmlNodeList dataList = rootElement.SelectNodes(@"//Data");
            
            bool geted = false;
            foreach (XmlNode xn in dataList)
            {
                #region get the other values.
                geted = false;
                switch (xn.Attributes["FieldName"].Value.ToLower())
                {
                    case "vtintno":
                        geted = true;
                        _rep.VtIntNo = AARTODataManager.GetVehicleTypeIDByCode(xn.Attributes["Value"].Value);
                        break;
                    case "vmintno":
                        geted = true;
                        _rep.VmIntNo = AARTODataManager.GetVehicleMakeIDByCode(xn.Attributes["Value"].Value);
                        break;
                    case "vcintno":
                        geted = true;
                        _rep.VcIntNo = AARTODataManager.GetVehicleColourIDByCode(xn.Attributes["Value"].Value);
                        break;
                    case "aareporgtypeid":
                        if (xn.Attributes["Value"].Value.Equals("-1"))
                        {
                            geted = true;
                            _rep.AaRepOrgTypeId = -1;
                            _rep.AaRepOtherOrgType = xn.Attributes["Other"].Value;
                        }
                        break;
                    case "aarepinflicodeid":
                        if (xn.Attributes["Value"].Value.Equals("-1"))
                        {
                            geted = true;
                            _rep.AaRepInfLiCodeId = -1;
                            _rep.AaRepInfForeignCode = xn.Attributes["Other"].Value;
                        }
                        break;
                    case "crtintno":
                        //these code should be test.
                        geted = true;
                        _rep.CrtIntNo = AARTODataManager.GetCourtIDByCode(xn.Attributes["Value"].Value);
                        break;
                    default:
                        break;
                }
                if (geted) continue;
                #endregion
                if (!Common.InsertValueIntoObjectByKey(xn.Attributes["FieldName"].Value, xn.Attributes["Value"].Value, _rep))
                {
                    Common.InsertValueIntoObjectByKey(xn.Attributes["FieldName"].Value, xn.Attributes["Value"].Value, _aartoEntity);
                }
            }
            LoadOtherData(dataList);
        }
        protected virtual void LoadOtherData(XmlNodeList dataList)
        {

        }
        private AartoRepresentation CreateAARTORepresentation(AartoRepresentation rep, RepresentationCodeList rcCode, int csCode)
        {
            rep.CsCode = csCode;
            rep.ChgIntNo = _charge.ChgIntNo;
            rep.RcCode = (int)rcCode;
            rep.AaRepCaptureDate = _importData.CaptureDate;
            return AARTODataManager.InsertAARTORepresentation(rep);
        }
        protected virtual void CreateReceiptForRepresentationDocument(int repID)
        {
        }
        protected virtual void UpdateChargeStatus()
        {
        }
        protected virtual int DoValidation(out int needCancelledrepID, out int notIntNo)
        {
            //make validation of representation
            return AARTODataManager.IsRepresentationValid(_aartoEntity.NotTicketNo, _importData.DoTeTypeID, out needCancelledrepID, out notIntNo);
        }
        public virtual void DoImport(ImportDataEntity importData)
        {
            _importSuccess = true;
            _rep = new AartoRepresentation();
            _importData = importData;
            SIL.AARTO.DAL.Data.TransactionManager AARTOTM = SIL.AARTO.DAL.Services.ConnectionScope.CreateTransaction();

            SIL.DMS.DAL.Data.TransactionManager DMSTM = SIL.DMS.DAL.Services.ConnectionScope.CreateTransaction();
            bool UpdateAARTOSuccessful = false;
            bool UpdateDMSSuccessful = false;
            try
            {
                LoadDataFromXML();
                if (!AARTOTM.IsOpen)
                {
                    AARTOTM.BeginTransaction();
                }
                if (!DMSTM.IsOpen)
                {
                    DMSTM.BeginTransaction();
                }
                int needCancelledrepID;
                int validationResult = -1;
                int notIntNo = -1;
                //make validation of representation
                //validationResult = AARTODataManager.IsRepresentationValid(_aartoEntity.NotTicketNo, _importData.DoTeTypeID, out needCancelledrepID, out notIntNo);
                validationResult = DoValidation(out needCancelledrepID, out notIntNo);
                #region add rep
                if (validationResult != (int)ValidationStatus.FalseForNoticeNumberNotExists)
                {
                    _notice = AARTODataManager.GetNoticeByNotIntNo(notIntNo);
                    _charge = AARTODataManager.GetChargeByNotIntNo(notIntNo);
                }
                switch (validationResult)
                {
                    case (int)ValidationStatus.FalseForNoticeNumberNotExists:
                        //notice number not exists.
                        DMSDataManager.UpdateImportResultToDMS(_importData.BaDoID, false, AARTOMessage.NOTICE_NUMBER_NOT_EXISTS);
                        _importSuccess = false;
                        break;
                    case (int)ValidationStatus.FalseForSameTypeAlreadyAdjudicated:
                        //load canceled rep,reason with Already Adjudicated.
                        _rep.AaRepCanceledDate = DateTime.Now;
                        _rep.AaRepUnsuccessfulReason = AARTOMessage.ALREADY_ADJUDICATED;
                        _rep = CreateAARTORepresentation(_rep, RepresentationCodeList.RepresentationCancelled, _charge.ChargeStatus);//canceled
                        _document = AARTODataManager.CreateAARTORepDocumentAndImages(_rep.AaRepId, _importData.BaDoID, _importData.DoTeTypeID,_importData.IFSIntNo);

                        DMSDataManager.UpdateImportResultToDMS(_importData.BaDoID, true, "");
                        _importSuccess = false;
                        break;
                    case (int)ValidationStatus.TrueForSameTypeNotAdjudicated:
                        //canceling the existing Rep (@RepID),and load new Rep.
                        AARTODataManager.CancelRepresentation(needCancelledrepID,_charge);
                        AARTODataManager.AddEvidencePack(AARTODataManager.GetDocumentDescriptionByDocTypeID(importData.DoTeTypeID) + " canceled", _charge.ChgIntNo, Constant.TABLE_NAME_OF_AARTOREPRESENTATION, needCancelledrepID);

                        _rep = CreateAARTORepresentation(_rep, RepresentationCodeList.AARTO_RepresentationRegistered, _charge.ChargeStatus);//registered 
                        _document = AARTODataManager.CreateAARTORepDocumentAndImages(_rep.AaRepId, _importData.BaDoID, _importData.DoTeTypeID, _importData.IFSIntNo);
                        CreateReceiptForRepresentationDocument(_rep.AaRepId);
                        UpdateChargeStatus();
                        AARTODataManager.PauseTheNoticeClock(_notice.NotIntNo, _rep.AaRepId);
                        AARTODataManager.AddEvidencePack(AARTODataManager.GetDocumentDescriptionByDocTypeID(importData.DoTeTypeID) + " registered", _charge.ChgIntNo, Constant.TABLE_NAME_OF_AARTOREPRESENTATION, _rep.AaRepId);

                        DMSDataManager.UpdateImportResultToDMS(_importData.BaDoID, true, "");
                        break;
                    case (int)ValidationStatus.TrueForAddNewOne:
                        //add new rep.
                        _rep = CreateAARTORepresentation(_rep, RepresentationCodeList.AARTO_RepresentationRegistered, _charge.ChargeStatus);//registered
                        _document = AARTODataManager.CreateAARTORepDocumentAndImages(_rep.AaRepId, _importData.BaDoID, _importData.DoTeTypeID, _importData.IFSIntNo);
                        CreateReceiptForRepresentationDocument(_rep.AaRepId);
                        UpdateChargeStatus();
                        AARTODataManager.PauseTheNoticeClock(_notice.NotIntNo, _rep.AaRepId);
                        AARTODataManager.AddEvidencePack(AARTODataManager.GetDocumentDescriptionByDocTypeID(importData.DoTeTypeID) + " registered", _charge.ChgIntNo, Constant.TABLE_NAME_OF_AARTOREPRESENTATION, _rep.AaRepId);
                        DMSDataManager.UpdateImportResultToDMS(_importData.BaDoID, true, "");
                        break;
                    case (int)ValidationStatus.FalseForDifferentTypeAlreadySuccessful:
                        //load canceled rep,reason with diffrent type rep successful.
                        _rep.AaRepCanceledDate = DateTime.Now;
                        _rep.AaRepUnsuccessfulReason = AARTOMessage.ALREADY_ADJUDICATED;
                        _rep = CreateAARTORepresentation(_rep, RepresentationCodeList.RepresentationCancelled, _charge.ChargeStatus);//canceled
                        _document = AARTODataManager.CreateAARTORepDocumentAndImages(_rep.AaRepId, _importData.BaDoID, _importData.DoTeTypeID, _importData.IFSIntNo);
                        DMSDataManager.UpdateImportResultToDMS(_importData.BaDoID, true, "");
                        _importSuccess = false;
                        break;
                    case (int)ValidationStatus.FalseForNoticeHasBeenPaidOrFinalised:
                        //Register the representation, but cancel immediately and set the RCCode = 99 (Reason: Already finalised)
                        //	No Evidence Pack
                        //	No change the ChargeStatus

                        _rep.AaRepCanceledDate = DateTime.Now;
                        _rep.AaRepUnsuccessfulReason = AARTOMessage.ALREADY_PAID_OR_FINALISED;
                        _rep = CreateAARTORepresentation(_rep, RepresentationCodeList.RepresentationCancelled, _charge.ChargeStatus);//canceled
                        _document = AARTODataManager.CreateAARTORepDocumentAndImages(_rep.AaRepId, _importData.BaDoID, _importData.DoTeTypeID, _importData.IFSIntNo);
                        DMSDataManager.UpdateImportResultToDMS(_importData.BaDoID, true, "");
                        _importSuccess = false;
                        break;
                    case (int)ValidationStatus.FalseForWarrantOfAttachmentStagePassed:
                        DMSDataManager.UpdateImportResultToDMS(_importData.BaDoID, false, AARTOMessage.Warrant_STAGE_PASSED);
                        _importSuccess = false;
                        break;
                    case (int)ValidationStatus.FalseForNotEnforcementOrderStage:
                        DMSDataManager.UpdateImportResultToDMS(_importData.BaDoID, false, AARTOMessage.NOT_ENFORCEMENT_ORDER_STAGE);
                        _importSuccess = false;
                        break;
                    default: break;

                }
                #endregion
                UpdateAARTOSuccessful = true;
                //update status into DMS
                UpdateDMSSuccessful = true;
                AARTOTM.Commit();
                DMSTM.Commit();
            }
            catch (Exception ex)
            {
                _importSuccess = false;
                if (!UpdateAARTOSuccessful || !UpdateDMSSuccessful)
                {
                    if (AARTOTM.IsOpen) AARTOTM.Rollback();
                    if (DMSTM.IsOpen) DMSTM.Rollback();
                }
                DMSDataManager.UpdateImportResultToDMS(_importData.BaDoID, false, AARTOMessage.UNKNOWN_ERROR);

                //Jake 2010-10-15 Write log using Enterprise Library log
                //ErrorLog.AddErrorLog(ex, AARTODataManager.LastUser);
                EntLibLogger.WriteErrorLog(ex, LogCategory.Error, AartoProjectList.AARTOImporterConsole.ToString());
            }
            finally
            {
                AARTOTM.Dispose();
                DMSTM.Dispose();

            }
        }

        protected string CheckOffenceCode(string offenceCode)
        {
            SIL.AARTO.DAL.Entities.TList<Offence> listOffence = null;

            if (!String.IsNullOrEmpty(offenceCode))
            {
                OffenceQuery query = new OffenceQuery();
                query.Append(OffenceColumn.OffCode, offenceCode);

                listOffence = new OffenceService().Find(query as IFilterParameterCollection);

                if (listOffence != null && listOffence.Count > 0)
                {
                    return offenceCode;
                }
                else
                {
                    return string.Empty;

                }
            }
            return string.Empty;
        }
    }
}
