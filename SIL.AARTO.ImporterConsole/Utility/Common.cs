﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Collections;
namespace SIL.AARTO.ImporterConsole.Utility
{
    public class Common
    {
        public static Object GetValueFromHashTable(string key,Hashtable ht)
        {
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="container"></param>
        /// <param name="obj"></param>
        /// <param name="prefixs"></param>
        /// <returns></returns>
        public static bool InsertValueIntoObjectByKey(string key, string value,Object obj)
        {
            bool yes = false;
            PropertyInfo[] pis = obj.GetType().GetProperties();
            foreach (PropertyInfo pi in pis)
            {
                if (key.ToLower() == pi.Name.ToLower())
                {
                    pi.SetValue(obj, getValueByType(value, pi.PropertyType), null);
                    yes = true;
                    break;
                }
            }
            return yes;
        }
        private static object getValueByType(object value, Type type)
        {
            if (type.Name.Equals("String"))
            {
                return value.ToString();
            }
            else
            {
                if (value.ToString().Trim().Length == 0) return null;
                if (type.Name.Equals("Int32"))
                {
                    return Convert.ToInt32(value);
                }
                else if (type.Name.Equals("Decimal"))
                {
                    return Convert.ToDecimal(value);
                }
                else if (type.Name.Equals("Boolean"))
                {
                    return Convert.ToBoolean(value);
                }
                else if (type.FullName.ToLower().IndexOf("System.DateTime".ToLower()) != -1)
                {
                    return Convert.ToDateTime(value);
                }
                else if (type.FullName.ToLower().IndexOf("System.Int32".ToLower()) != -1)
                {
                    return Convert.ToInt32(value);
                }
                else if (type.FullName.ToLower().IndexOf("System.Byte".ToLower()) != -1)
                {
                    return Convert.ToByte(value);
                }
                else if (type.FullName.ToLower().IndexOf("System.Single".ToLower()) != -1)
                {
                    return Convert.ToSingle(value);
                }
                else
                {
                    return null;
                }
            }
        }
        public static DateTime GetShortDateTime(DateTime date)
        {
            return Convert.ToDateTime(date.ToShortDateString());
        }
        

    }
}
