﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

using SIL.AARTO.ImporterConsole.Utility;
using System.Xml;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections;
using SIL.DMS.DAL.Entities;
using SIL.DMS.DAL.Services;
using SIL.AARTO.BLL.AARTONoticeClock;
using SIL.AARTO.BLL.Utility;
using System.Data;
using SIL.AARTO.BLL.EntLib;
namespace SIL.AARTO.ImporterConsole.AARTO01
{
    public class AARTO01Data : AARTOData
    {
        //Notice
        private int RDTYPECODE = 0;

        //Charge
        private const int CHARGESTATUS = (int)ChargeStatusList.AARTO_Loaded_01;
        private const int OfficerErrorCHARGESTATUS = (int)ChargeStatusList.OfficerError;
        private int CTINTNO = 0;

        //Film
        private int CONINTNO = 0;
        private const string CDLABEL = "AARTO01";
        private const string FILMDESCR = "AARTO01";

        //Frame
        private int LOCINTNO = 0;
        private int VMINTNO = 0;
        private int VTINTNO = 0;
        private int CAMINTNO = 0;
        private int TOINTNO = 0;
        private int RDTINTNO = 0;
        private int SZINTNO = 0;
        private int CRTINTNO = 0;
        private int CRTRINTNo = 0;
        private int REJINTNO = 0;
        private string AUTNO = "000";
        private string CAMUNITID = "";
        private byte FIRSTSPEED = 0;
        private byte SECONDSPEED = 0;
        private string SEQUENCE = "";
        private string REGNO = "";
        private string ELAPSEDTIME = "";
        private string TRAVELDIRECTION = "";
        private string MANUALVIEW = "";
        private string MULTFRAMES = "";

        private int vehicleMakeIntNo = 0;
        private int vehicleTypeIntNo = 0;
        private int officerIntNo = 0;
        private string courtName;
        private string offenceType;
        //private string chgOffenceCode;
        //private int chgTypeIntNo = 0;
        private string locCode = string.Empty;
        private string locDescr = string.Empty;

        //Notice_Frame
        private const string NFSOURCECODE = "";

        public AARTO01Data()
        {
            _aartoEntity = new AARTOEntity();
        }

        private bool Initialization(string crtNo, string notVehicleMakeCode,
            string notVehicleTypeCode, string notOfficerNo,
            string crtRoomNo, string chgOffenceCode,
            int autIntNo, string lastUser, string cType)
        {
            bool success = false;
            string conCode = ConfigurationManager.AppSettings["ContractorCode"].ToString();

            using (IDataReader reader = new NoticeService().InitialForImporter(
                crtNo,
                notVehicleMakeCode,
                notVehicleTypeCode,
                notOfficerNo,
                chgOffenceCode,
                crtRoomNo,
                conCode,
                autIntNo,
                lastUser,
                cType))
            {
                while (reader.Read())
                {
                    CRTINTNO = reader["CrtIntNo"] == null ? 0 : Convert.ToInt32(reader["CrtIntNo"]);
                    CRTRINTNo = reader["CrtRIntNo"] == null ? 0 : Convert.ToInt32(reader["CrtRIntNo"]);
                    CONINTNO = reader["ConIntNo"] == null ? 0 : Convert.ToInt32(reader["ConIntNo"]);
                    LOCINTNO = reader["LocIntNo"] == null ? 0 : Convert.ToInt32(reader["LocIntNo"]);
                    locCode = reader["LocCode"] == null ? "" : reader["LocCode"].ToString();
                    locDescr = reader["LocDesc"] == null ? "" : reader["LocDesc"].ToString();
                    VMINTNO = reader["VehicleMakeIntNo"] == null ? 0 : Convert.ToInt32(reader["VehicleMakeIntNo"]);
                    VTINTNO = reader["VehicleTypeIntNo"] == null ? 0 : Convert.ToInt32(reader["VehicleTypeIntNo"]);
                    CAMINTNO = reader["CameraIntNo"] == null ? 0 : Convert.ToInt32(reader["CameraIntNo"]);
                    TOINTNO = reader["OfficerIntNo"] == null ? 0 : Convert.ToInt32(reader["OfficerIntNo"]);
                    RDTINTNO = reader["RdTIntNo"] == null ? 0 : Convert.ToInt32(reader["RdTIntNo"]);
                    //RDTYPECODE = reader["RdTypeCode"] == null ? 0 : Convert.ToInt32(reader["RdTypeCode"]); 
                    SZINTNO = reader["SzIntNo"] == null ? 0 : Convert.ToInt32(reader["SzIntNo"]);
                    REJINTNO = reader["RejIntNo"] == null ? 0 : Convert.ToInt32(reader["RejIntNo"]);
                    AUTNO = reader["AutNo"] == null ? "" : reader["AutNo"].ToString();
                    CAMUNITID = reader["CamerUnitIntNo"] == null ? "" : reader["CamerUnitIntNo"].ToString();
                    courtName = reader["CourtName"] == null ? "" : reader["CourtName"].ToString();
                    offenceType = reader["OffenceType"] == null ? "" : reader["OffenceType"].ToString();
                    CTINTNO = reader["ChgTypeIntNo"] == null ? 0 : Convert.ToInt32(reader["ChgTypeIntNo"]);
                    success = true;
                    break;
                }
            }
            return success;
        }

       

        public override void DoImport(ImportDataEntity importDataEntity)
        {
            #region loadXML
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(importDataEntity.BaDoResult);

            XmlElement rootElement = xmlDoc.DocumentElement;
            string OfficerErrors = "";
            string filmNo = "";
            string frameNo = "";
            XmlNode datas = rootElement.SelectSingleNode(@"//Datas");
            OfficerErrors = datas.Attributes["OfficerErrorIds"] == null ? "" : datas.Attributes["OfficerErrorIds"].Value.Trim();
            filmNo = datas.Attributes["BatchId"].Value.Trim();
            frameNo = datas.Attributes["DocId"].Value.Trim();
            XmlNodeList dataList = rootElement.SelectNodes(@"//Data");

            Hashtable hsXML = new Hashtable();
            Hashtable hsXMLText = new Hashtable();
            foreach (XmlNode xn in dataList)
            {
                if (!hsXML.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["Value"] != null)
                {
                    hsXML.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["Value"].Value.Trim());
                }

                if (!hsXMLText.ContainsKey(xn.Attributes["FieldName"].Value.Trim()) && xn.Attributes["Text"] != null)
                {
                    hsXMLText.Add(xn.Attributes["FieldName"].Value.Trim(), xn.Attributes["Text"].Value.Trim());
                }

            }
            #endregion

            SIL.AARTO.DAL.Data.TransactionManager AARTOTM = SIL.AARTO.DAL.Services.ConnectionScope.CreateTransaction();

            SIL.DMS.DAL.Data.TransactionManager DMSTM = SIL.DMS.DAL.Services.ConnectionScope.CreateTransaction();
            bool UpdateAARTOSuccessful = false;
            bool UpdateDMSSuccessful = false;
             
            try
            {
                if (!AARTOTM.IsOpen)
                {
                    AARTOTM.BeginTransaction();
                }
                if (!DMSTM.IsOpen)
                {
                    DMSTM.BeginTransaction();
                }
                string imgFilePath = DMSDataManager.GetBatchDocumentImageListByBatchDocumentID(importDataEntity.BaDoID)[0].BaDoImFilePath;

                Notice notice1 = null;
                Notice notice2 = null;
                Driver driver = null;
                Driver driver2 = null;
                int autIntNo = 0;
                DateTime offenceDate = new DateTime();
                SIL.AARTO.DAL.Entities.Authority authority = NoticeManager.GetAuthorityByAutCode(hsXML["AutCode"].ToString().Trim());
                if (authority != null) autIntNo = authority.AutIntNo;
                #region Notice1 Chareg1 Drive


                string crtNo = "";
                string notVehicleMakeCode = hsXML["VehicleMake"].ToString().Trim();
                string notVehicleTypeCode = hsXML["VehicleType"].ToString().Trim();
                string notOfficerNo = hsXML["NotOfficerNo"].ToString().Trim();
                string crtRoomNo = "";

                string offenceCode = String.IsNullOrEmpty(hsXML["ChargeOffenceCode1"].ToString().Trim()) ? "" : hsXML["ChargeOffenceCode1"].ToString().Trim();
                
                bool initalStatus = false;

                string chgOffenceCode1 = string.Empty;

                if (hsXML["ChargeOffenceCode1"] != null)
                {
                    chgOffenceCode1 = CheckOffenceCode(hsXML["ChargeOffenceCode1"].ToString().Trim());
                    if (String.IsNullOrEmpty(chgOffenceCode1))
                    {
                        Console.WriteLine(String.Format("Charge Offence Code: {0} does not exists!", chgOffenceCode1));
                    }
                }
                if (!String.IsNullOrEmpty(hsXML["NoticeNo1"].ToString().Trim()) && !String.IsNullOrEmpty(chgOffenceCode1))
                {

                    initalStatus = Initialization(crtNo, notVehicleMakeCode, notVehicleTypeCode, notOfficerNo, crtRoomNo, chgOffenceCode1, autIntNo, _lastUser, "01");

                    if (initalStatus == false)
                    {
                        Console.WriteLine("Initial Data failure!");
                        return;
                    }

                    bool noticeIsUpdate = false;
                    string noticeTicketNo = hsXML["NoticeNo1"].ToString().Trim().Insert(2, "-").Insert(7, "-").Insert(17, "-");
                    #region Notice

                    SIL.AARTO.DAL.Entities.TList<Notice> listNotice = new NoticeService().GetByNotTicketNo(noticeTicketNo);
                    if (listNotice.Count > 0)
                    {
                        notice1 = listNotice[0];
                        noticeIsUpdate = true;
                    }
                    else
                    {
                        notice1 = new Notice();
                        notice1.NotTicketNo = noticeTicketNo;
                    }

                    notice1.AutIntNo = autIntNo;
                    DateTime.TryParse(hsXML["OffenceDate"].ToString().Trim() + " " + hsXML["OffenceTime"].ToString().Trim(), out offenceDate);
                    notice1.NotOffenceDate = offenceDate;

                    notice1.NotRegNo = hsXML["NotRegNo"].ToString().Trim();
                    notice1.NotClearanceCert = hsXML["NotClearanceCert"].ToString().Trim();
                    notice1.NotVehicleTypeCode = hsXML["VehicleType"].ToString().Trim();
                    notice1.NotVehicleType = hsXMLText["VehicleType"].ToString().Trim();
                    notice1.NotVehicleMakeCode = hsXML["VehicleMake"].ToString().Trim();
                    notice1.NotVehicleMake = hsXMLText["VehicleMake"].ToString().Trim();
                    notice1.NotVehicleColour = hsXML["VehicleColour"].ToString().Trim();

                    StringBuilder loctionInfringement = new StringBuilder();
                    loctionInfringement.Append("<LoctionInfringement>");
                    loctionInfringement.Append("<Province>" + hsXML["Province"].ToString().Trim() + "</Province>");
                    loctionInfringement.Append("<CityTown>" + hsXML["CityTown"].ToString().Trim() + "</CityTown>");
                    loctionInfringement.Append("<Suburb>" + (hsXML["Suburb"] == null ? "" : hsXML["Suburb"].ToString().Trim()) + "</Suburb>");
                    loctionInfringement.Append("<Date>" + hsXML["OffenceDate"].ToString().Trim() + "</Date>");
                    loctionInfringement.Append("<Time>" + hsXML["OffenceTime"].ToString().Trim() + "</Time>");
                    loctionInfringement.Append("<StreetNameA>" + hsXML["StreetNameA"].ToString().Trim() + "</StreetNameA>");
                    loctionInfringement.Append("<StreetNameB>" + hsXML["StreetNameB"].ToString().Trim() + "</StreetNameB>");
                    loctionInfringement.Append("<RouteNo>" + hsXML["RouteNo"].ToString().Trim() + "</RouteNo>");
                    loctionInfringement.Append("<FromPlace>" + hsXML["FromPlace"].ToString().Trim() + "</FromPlace>");
                    loctionInfringement.Append("<ToPlace>" + hsXML["ToPlace"].ToString().Trim() + "</ToPlace>");
                    loctionInfringement.Append("<GPSX>" + hsXML["GPSX"].ToString().Trim() + "</GPSX>");
                    loctionInfringement.Append("<GPSY>" + hsXML["GPSY"].ToString().Trim() + "</GPSY>");
                    loctionInfringement.Append("</LoctionInfringement>");

                    notice1.NotLoctionInfringement = loctionInfringement.ToString();

                    notice1.NotOfficerNo = hsXML["NotOfficerNo"].ToString().Trim();
                    notice1.NotOfficerSname = hsXML["NotOfficerSname"].ToString().Trim();
                    notice1.NotSource = "DMS";
                    //notice1.NotRdTypeCode = (short)RDTYPECODE;
                    notice1.OperatorCardNo = hsXML["OperatorCardNo"].ToString().Trim();
                    notice1.NotIsOfficerError = !String.IsNullOrEmpty(OfficerErrors);
                    notice1.LastUser = _lastUser;
                    notice1.NotOffenceType = offenceType;

                    SIL.AARTO.DAL.Entities.TList<TrafficOfficer> officerList = new TrafficOfficerService().GetByToNo(notice1.NotOfficerNo);
                    if (officerList.Count > 0)
                        notice1.NotOfficerInit = officerList[0].ToInit;

                    notice1.NotLocCode = locCode;
                    notice1.NotLocDescr = locDescr;
                    //notice1.NotSpeedLimit = 0;
                    notice1.NotCameraId = "000";

                    notice1.OrigRegNo = notice1.NotRegNo;
                    notice1.NotLoadDate = DateTime.Now.Date;
                    if (authority != null)
                    {
                        notice1.AutName = authority.AutName;
                    }
                    notice1.NotPaymentDate = offenceDate.AddDays(32);
                    notice1.NotIssue1stNoticeDate = offenceDate;
                    notice1.NotIssue2ndNoticeDate = offenceDate;
                    notice1.NotSendTo = "D";
                    notice1.NotFilmType = "M";
                    int notVechileGVM = 0;
                    int.TryParse(hsXML["NotVehicleGVM"].ToString(), out notVechileGVM);
                    notice1.NotVehicleGvm = notVechileGVM;
                    notice1.NotTicketProcessor = _TicketProcessor;
                    notice1.LastUser = _lastUser;

                    notice1 = new NoticeService().Save(notice1);
                    AARTODataManager.SaveOfficerErrorsWithNotice(notice1.NotIntNo, OfficerErrors, AARTODataManager.LastUser);

                    #endregion
                    if (!noticeIsUpdate)
                    {
                        AARTODataManager.CreateAARTONoticeDocumentAndImages(notice1.NotIntNo, importDataEntity.BaDoID, (int)AartoDocumentTypeList.AARTO01, (int)AartoDocumentStatusList.HandDelivered, importDataEntity.IFSIntNo);
                    }
                    #region Charge

                    Charge charge1 = null;
                    if (noticeIsUpdate)
                    {
                        foreach (Charge chg in new ChargeService().GetByNotIntNo(notice1.NotIntNo))
                        {
                            if (chg.ChgIsMain)
                            {
                                charge1 = chg;
                                break;
                            }
                        }
                    }
                    if (charge1 == null)
                        charge1 = new Charge();

                    charge1.ChgIsMain = true;
                    charge1.NotIntNo = notice1.NotIntNo;
                    //charge1.CtIntNo = CTINTNO.Value;

                    charge1.ChgOffenceCode = hsXML["ChargeOffenceCode1"].ToString().Trim();
                    charge1.ChgOffenceDescr = hsXML["ChargeOffenceDescr1"].ToString().Trim();

                    float fineAmount1 = 0;
                    float.TryParse(hsXML["ChargeFineAmount1"].ToString().Trim(), out fineAmount1);
                    charge1.ChgFineAmount = fineAmount1;
                    charge1.ChgRevFineAmount = fineAmount1;

                    decimal discountAmount1 = 0;
                    decimal.TryParse(hsXML["ChargeDiscountAmount1"].ToString().Trim(), out discountAmount1);
                    charge1.ChgDiscountAmount = discountAmount1;
                    charge1.ChgRevDiscountAmount = (float)discountAmount1;

                    decimal discountedAmount1 = 0;
                    decimal.TryParse(hsXML["ChargeDiscountedAmount1"].ToString().Trim(), out discountedAmount1);
                    charge1.ChgDiscountedAmount = discountedAmount1;
                    charge1.ChgRevDiscountedAmount = (float)discountedAmount1;

                    int CTIntNo1 = 0;
                    int.TryParse(hsXML["ChargeType1"].ToString().Trim(), out CTIntNo1);
                    //charge1.CtIntNo = CTIntNo1;
                    charge1.CtIntNo = CTINTNO;// CTIntNo1;
                    charge1.ChgType = hsXMLText["ChargeType1"].ToString();

                    if (charge1.ChgType.Equals("Minor"))
                        charge1.ChgLegislation = "1";
                    else if (charge1.ChgType.Equals("Major"))
                    {
                        charge1.ChgLegislation = "2";
                    }
                    int demeritPoints1 = 0;
                    int.TryParse(hsXML["ChargeDemeritPoints1"].ToString().Trim(), out demeritPoints1);
                    charge1.ChgDemeritPoints = demeritPoints1;

                    charge1.ChargeStatus = OfficerErrors.Trim().Length > 0 ? OfficerErrorCHARGESTATUS : CHARGESTATUS;

                    charge1.ChgOffenceType = offenceType; 
                    //string ffff = charge1.ToString();
                    //charge1.ChgIsMain = true;

                    charge1.LastUser = _lastUser;

                    new ChargeService().Save(charge1);

                    if (!String.IsNullOrEmpty(hsXML["AlternativeChargeOffenceCode1"].ToString().Trim()))
                    {
                        Charge alternativeCharge1 = null;
                        if (noticeIsUpdate)
                        {
                            foreach (Charge chg in new ChargeService().GetByNotIntNo(notice1.NotIntNo))
                            {
                                if (chg.ChgIsMain == false)
                                {
                                    alternativeCharge1 = chg;
                                    break;
                                }
                            }
                        }
                        if (alternativeCharge1 == null)
                        {
                            alternativeCharge1 = new Charge();

                            alternativeCharge1.NotIntNo = notice1.NotIntNo;
                            alternativeCharge1.ChgIsMain = false;
                            //alternativeCharge1.CtIntNo = CTINTNO;
                        }

                        alternativeCharge1.ChgOffenceCode = hsXML["AlternativeChargeOffenceCode1"].ToString().Trim();
                        alternativeCharge1.ChgOffenceDescr = hsXML["AlternativeChargeOffenceDescr1"].ToString().Trim();

                        float alternativeFineAmount1 = 0;
                        float.TryParse(hsXML["AlternativeChargeFineAmount1"].ToString().Trim(), out alternativeFineAmount1);
                        alternativeCharge1.ChgFineAmount = alternativeFineAmount1;
                        alternativeCharge1.ChgRevFineAmount = alternativeFineAmount1;

                        decimal alternativeDiscountAmount1 = 0;
                        decimal.TryParse(hsXML["AlternativeChargeDiscountAmount1"].ToString().Trim(), out alternativeDiscountAmount1);
                        alternativeCharge1.ChgDiscountAmount = alternativeDiscountAmount1;
                        alternativeCharge1.ChgRevDiscountAmount = (float)alternativeDiscountAmount1;

                        decimal alternativeDiscountedAmount1 = 0;
                        decimal.TryParse(hsXML["AlternativeChargeDiscountedAmount1"].ToString().Trim(), out alternativeDiscountedAmount1);
                        alternativeCharge1.ChgDiscountedAmount = alternativeDiscountedAmount1;
                        alternativeCharge1.ChgRevDiscountedAmount = (float)alternativeDiscountedAmount1;

                        int AlternativeCTIntNo1 = 0;
                        int.TryParse(hsXML["AlternativeChargeType1"].ToString().Trim(), out AlternativeCTIntNo1);
                        alternativeCharge1.CtIntNo = CTINTNO;// AlternativeCTIntNo1;
                        alternativeCharge1.ChgType = hsXMLText["AlternativeChargeType1"].ToString();

                        if (@String.IsNullOrEmpty(alternativeCharge1.ChgType))
                            alternativeCharge1.ChgLegislation = alternativeCharge1.ChgType.Substring(0, 1);

                        int alternativeDemeritPoints1 = 0;
                        int.TryParse(hsXML["AlternativeChargeDemeritPoints1"].ToString().Trim(), out alternativeDemeritPoints1);
                        alternativeCharge1.ChgDemeritPoints = alternativeDemeritPoints1;


                        alternativeCharge1.ChargeStatus = OfficerErrors.Trim().Length > 0 ? OfficerErrorCHARGESTATUS : CHARGESTATUS;
                        //alternativeCharge1.ChgIsMain = false;
                        alternativeCharge1.LastUser = _lastUser;

                        alternativeCharge1 = new ChargeService().Save(alternativeCharge1);
                    }

                    #endregion

                    #region Driver
                    if (noticeIsUpdate)
                    {
                        driver = new DriverService().GetByNotIntNo(notice1.NotIntNo)[0];
                    }
                    else
                    {
                        driver = new Driver();
                    }

                    driver.NotIntNo = notice1.NotIntNo;

                    driver.DrvSurname = hsXML["DrvSurname"].ToString().Trim();
                    driver.DrvForenames = hsXML["DrvForenames"].ToString().Trim();
                    driver.DrvInitials = hsXML["DrvInitials"].ToString().Trim();

                    DateTime birthDate = DateTime.Now;
                    DateTime.TryParse(hsXML["BirthDate"].ToString().Trim(), out birthDate);
                    driver.DrvAge = Convert.ToString(DateTime.Now.Year - birthDate.Year + 1);
                    if (hsXML["AaIdTypeId"] != null)
                        driver.AaIdTypeId = Convert.ToInt32(hsXML["AaIdTypeId"]);
                    driver.DrvIdNumber = hsXML["DrvIdNumber"].ToString().Trim();
                    driver.DrvLicencePlace = hsXML["DrvLicencePlace"].ToString().Trim();
                    driver.DrvLicenceCode = hsXML["DrvLicenceCode"].ToString().Trim();
                    if (hsXML["AaLiCodeId"] != null && hsXML["AaLiCodeId"].ToString()!="")
                        driver.AaLiCodeId = Convert.ToInt32(hsXML["AaLiCodeId"]);
                    if (hsXML["AaLeCodeId"] != null && hsXML["AaLeCodeId"].ToString() != "")
                        driver.AaLeCodeId = Convert.ToInt32(hsXML["AaLeCodeId"]);
                    if (hsXML["AaPrDpCodeId"] != null && hsXML["AaPrDpCodeId"].ToString() != "")
                        driver.AaPrDpCodeId = Convert.ToInt32(hsXML["AaPrDpCodeId"]);
                    driver.DrvStAdd1 = hsXML["DrvStAdd1"].ToString().Trim();
                    driver.DrvStAdd2 = hsXML["DrvStAdd2"].ToString().Trim();
                    driver.DrvPoAdd1 = hsXML["DrvPoAdd1"].ToString().Trim();
                    driver.DrvPoAdd2 = hsXML["DrvPoAdd2"].ToString().Trim();
                    driver.DrvPoCode = hsXML["DrvPoCode"].ToString().Trim();
                    driver.DrvEmployerName = hsXML["DrvEmployerName"].ToString().Trim();
                    driver.DrvEmployerAddress = hsXML["DrvEmployerAddress"].ToString().Trim();
                    driver.DrvHomeNo = hsXML["DrvHomeNo"].ToString().Trim();
                    driver.DrvWorkNo = hsXML["DrvWorkNo"].ToString().Trim();
                    driver.DrvFax = hsXML["DrvFax"].ToString().Trim();
                    driver.DrvCellNo = hsXML["DrvCellNo"].ToString().Trim();
                    driver.DrvEmail = hsXML["DrvEmail"].ToString().Trim();
                    driver.LastUser = _lastUser;

                    driver = new DriverService().Save(driver);
                    #endregion

                    NoticeManager.AddSearchIDAndName(notice1.NotIntNo, driver.DrvIdNumber, driver.DrvSurname, driver.DrvInitials);

                    if (!noticeIsUpdate)
                    {
                        #region Film
                        Film film = new Film();
                        film.AutIntNo = autIntNo;
                        film.ConIntNo = CONINTNO;//
                        film.FilmNo = filmNo+"_1";
                        film.CdLabel = CDLABEL;//
                        film.FilmDescr = FILMDESCR;//
                        film.FilmType = "M";
                        film.FilmLoadDateTime = DateTime.Now;
                        film.LastUser = _lastUser;

                        new FilmService().Save(film);

                        #endregion

                        #region Frame


                        Frame frame = new Frame();
                        frame.FilmIntNo = film.FilmIntNo;
                        frame.FrameNo = "";

                        frame.OffenceDate = offenceDate;
                        frame.FrameImagePath = imgFilePath;
                        frame.IfsIntNo = importDataEntity.IFSIntNo;
                        frame.LocIntNo = LOCINTNO;//
                        frame.VmIntNo = VMINTNO;//
                        frame.VtIntNo = VTINTNO;//
                        frame.CamIntNo = CAMINTNO;
                        frame.ToIntNo = TOINTNO;//
                        frame.RdTintNo = RDTINTNO;//
                        frame.SzIntNo = SZINTNO;//
                        frame.CrtIntNo = CRTINTNO;//
                        frame.RejIntNo = REJINTNO;//
                        frame.AutNo = AUTNO;//
                        frame.CamUnitId = CAMUNITID;//
                        frame.FirstSpeed = FIRSTSPEED;//
                        frame.SecondSpeed = SECONDSPEED;//
                        frame.Sequence = SEQUENCE;//
                        frame.RegNo = REGNO;//
                        frame.ElapsedTime = ELAPSEDTIME;
                        frame.TravelDirection = TRAVELDIRECTION;//
                        frame.ManualView = MANUALVIEW;//
                        frame.MultFrames = MULTFRAMES;//
                        frame.OperatorCardNo = hsXML["OperatorCardNo"].ToString().Trim();
                        frame.FrameStatus = 901;
                        frame.DmsDocumentNo = frameNo+"_1";
                        frame.LastUser = _lastUser;
                        new FrameService().Save(frame);

                        #endregion

                        #region Notice_Frame
                        NoticeFrame noticeFrame = new NoticeFrame();
                        noticeFrame.NotIntNo = notice1.NotIntNo;
                        noticeFrame.FrameIntNo = frame.FrameIntNo;
                        noticeFrame.NfSourceCode = NFSOURCECODE;//
                        noticeFrame.LastUser = _lastUser;
                        new NoticeFrameService().Save(noticeFrame);
                        #endregion


                        // Add Clock 
                        DiscountPeriodClock clock = new DiscountPeriodClock(notice1.NotIntNo);
                        clock.LastUser = AARTODataManager.LastUser;
                        clock.Create();
                        clock.Start(ConvertDateTime.GetShortDateTime(offenceDate));
                    }
                }

                #endregion

                #region Notice2 Chareg2 Drive

                string chgOffenceCode2 = string.Empty ;
                if (hsXML["ChargeOffenceCode2"] != null)
                {
                    chgOffenceCode2 = CheckOffenceCode(hsXML["ChargeOffenceCode2"].ToString().Trim());
                    if (String.IsNullOrEmpty(chgOffenceCode2))
                    {
                        Console.WriteLine(String.Format("Charge Offence Code: {0} does not exists!",chgOffenceCode2));
                    }
                }

                if (!String.IsNullOrEmpty(hsXML["NoticeNo2"].ToString().Trim()) && !String.IsNullOrEmpty(chgOffenceCode2))
                {
                    initalStatus = Initialization(crtNo, notVehicleMakeCode, notVehicleTypeCode, notOfficerNo, crtRoomNo, chgOffenceCode2, autIntNo, _lastUser, "01");

                    if (initalStatus == false)
                    {
                        Console.WriteLine("Initial Data failure!");
                        return;
                    }

                    #region Notice
                    bool notice2IsUpdate = false;
                    string notice2TicketNo = hsXML["NoticeNo2"].ToString().Trim().Insert(2, "-").Insert(7, "-").Insert(17, "-");
                    SIL.AARTO.DAL.Entities.TList<Notice> listNotice2 = new NoticeService().GetByNotTicketNo(notice2TicketNo);
                    if (listNotice2.Count > 0)
                    {
                        notice2IsUpdate = true;
                        notice2 = listNotice2[0];
                    }
                    else
                    {
                        notice2 = new Notice();
                    }

                    notice2.NotTicketNo = hsXML["NoticeNo2"].ToString().Trim().Insert(2, "-").Insert(7, "-").Insert(17, "-");

                    #region not clone
                    notice2.AutIntNo = autIntNo;

                    DateTime.TryParse(hsXML["OffenceDate"].ToString().Trim() + " " + hsXML["OffenceTime"].ToString().Trim(), out offenceDate);
                    notice2.NotOffenceDate = offenceDate;

                    notice2.NotRegNo = hsXML["NotRegNo"].ToString().Trim();
                    notice2.NotClearanceCert = hsXML["NotClearanceCert"].ToString().Trim();
                    notice2.NotVehicleTypeCode = hsXML["VehicleType"].ToString().Trim();
                    notice2.NotVehicleType = hsXMLText["VehicleType"].ToString().Trim();
                    notice2.NotVehicleMakeCode = hsXML["VehicleMake"].ToString().Trim();
                    notice2.NotVehicleMake = hsXMLText["VehicleMake"].ToString().Trim();
                    notice2.NotVehicleColour = hsXML["VehicleColour"].ToString().Trim();


                    StringBuilder loctionInfringement = new StringBuilder();
                    loctionInfringement.Append("<LoctionInfringement>");
                    loctionInfringement.Append("<Province>" + hsXML["Province"].ToString().Trim() + "</Province>");
                    loctionInfringement.Append("<CityTown>" + hsXML["CityTown"].ToString().Trim() + "</CityTown>");
                    loctionInfringement.Append("<Suburb>" + hsXML["Suburb"].ToString().Trim() + "</Suburb>");
                    loctionInfringement.Append("<Date>" + hsXML["OffenceDate"].ToString().Trim() + "</Date>");
                    loctionInfringement.Append("<Time>" + hsXML["OffenceTime"].ToString().Trim() + "</Time>");
                    loctionInfringement.Append("<StreetNameA>" + hsXML["StreetNameA"].ToString().Trim() + "</StreetNameA>");
                    loctionInfringement.Append("<StreetNameB>" + hsXML["StreetNameB"].ToString().Trim() + "</StreetNameB>");
                    loctionInfringement.Append("<RouteNo>" + hsXML["RouteNo"].ToString().Trim() + "</RouteNo>");
                    loctionInfringement.Append("<FromPlace>" + hsXML["FromPlace"].ToString().Trim() + "</FromPlace>");
                    loctionInfringement.Append("<ToPlace>" + hsXML["ToPlace"].ToString().Trim() + "</ToPlace>");
                    loctionInfringement.Append("<GPSX>" + hsXML["GPSX"].ToString().Trim() + "</GPSX>");
                    loctionInfringement.Append("<GPSY>" + hsXML["GPSY"].ToString().Trim() + "</GPSY>");
                    loctionInfringement.Append("</LoctionInfringement>");

                    notice2.NotLoctionInfringement = loctionInfringement.ToString();

                    notice2.NotOfficerNo = hsXML["NotOfficerNo"].ToString().Trim();
                    notice2.NotOfficerSname = hsXML["NotOfficerSname"].ToString().Trim();
                    notice2.NotSource = "DMS";
                    //notice2.NotRdTypeCode = (short)RDTYPECODE;
                    notice2.OperatorCardNo = hsXML["OperatorCardNo"].ToString().Trim();
                    notice2.NotIsOfficerError = !String.IsNullOrEmpty(OfficerErrors);
                    notice2.LastUser = _lastUser;
                    notice2.NotOffenceType = offenceType;

                    SIL.AARTO.DAL.Entities.TList<TrafficOfficer> officerList = new TrafficOfficerService().GetByToNo(notice2.NotOfficerNo);
                    if (officerList.Count > 0)
                        notice2.NotOfficerInit = officerList[0].ToInit;

                    notice2.NotLocCode = locCode;
                    notice2.NotLocDescr = locDescr;
                    //notice1.NotSpeedLimit = 0;
                    notice2.NotCameraId = "000";

                    notice2.OrigRegNo = notice1.NotRegNo;
                    notice2.NotLoadDate = DateTime.Now.Date;
                    if (authority != null)
                    {
                        notice2.AutName = authority.AutName;
                    }
                    notice2.NotPaymentDate = offenceDate.AddDays(32);
                    notice2.NotIssue1stNoticeDate = offenceDate;
                    notice2.NotIssue2ndNoticeDate = offenceDate;
                    notice2.NotSendTo = "D";
                    notice2.NotFilmType = "M";
                    int notVechileGVM = 0;
                    int.TryParse(hsXML["NotVehicleGVM"].ToString(), out notVechileGVM);
                    notice2.NotVehicleGvm = notVechileGVM;
                    notice2.NotTicketProcessor = _TicketProcessor;
                    notice2.LastUser = _lastUser;
                    #endregion
                    //}
                    notice2 = new NoticeService().Save(notice2);

                    AARTODataManager.SaveOfficerErrorsWithNotice(notice2.NotIntNo, OfficerErrors, AARTODataManager.LastUser);
                    #endregion

                    #region Charge
                    Charge charge2 = null;
                    if (notice2IsUpdate)
                    {
                        foreach (Charge chg in new ChargeService().GetByNotIntNo(notice2.NotIntNo))
                        {
                            if (chg.ChgIsMain)
                            {
                                charge2 = chg;
                                break;
                            }
                        }
                    }
                    else
                    {
                        AARTODataManager.CreateAARTONoticeDocumentAndImages(notice2.NotIntNo, importDataEntity.BaDoID, (int)AartoDocumentTypeList.AARTO01, (int)AartoDocumentStatusList.HandDelivered, importDataEntity.IFSIntNo);
                    }
                    if (charge2 == null)
                    {
                        charge2 = new Charge();
                        charge2.NotIntNo = notice2.NotIntNo;
                        charge2.ChgIsMain = true;
                        //charge2.CtIntNo = CTINTNO;
                    }
                    charge2.ChgOffenceCode = hsXML["ChargeOffenceCode2"].ToString().Trim();
                    charge2.ChgOffenceDescr = hsXML["ChargeOffenceDescr2"].ToString().Trim();

                    float fineAmount2 = 0;
                    float.TryParse(hsXML["ChargeFineAmount2"].ToString().Trim(), out fineAmount2);
                    charge2.ChgFineAmount = fineAmount2;
                    charge2.ChgRevFineAmount = fineAmount2;

                    decimal discountAmount2 = 0;
                    decimal.TryParse(hsXML["ChargeDiscountAmount2"].ToString().Trim(), out discountAmount2);
                    charge2.ChgDiscountAmount = discountAmount2;
                    charge2.ChgRevDiscountAmount = (float)discountAmount2;

                    decimal discountedAmount2 = 0;
                    decimal.TryParse(hsXML["ChargeDiscountedAmount2"].ToString().Trim(), out discountedAmount2);
                    charge2.ChgDiscountedAmount = discountedAmount2;
                    charge2.ChgRevDiscountedAmount = (float)discountedAmount2;

                    int CTIntNo2 = 0;
                    int.TryParse(hsXML["ChargeType2"].ToString().Trim(), out CTIntNo2);
                    
                    charge2.CtIntNo = CTINTNO;//CTIntNo2;
                    charge2.ChgType = hsXMLText["ChargeType2"].ToString();

                    if (!String.IsNullOrEmpty(charge2.ChgType))
                        charge2.ChgLegislation = charge2.ChgType.Substring(0, 1);

                    int demeritPoints2 = 0;
                    int.TryParse(hsXML["ChargeDemeritPoints2"].ToString().Trim(), out demeritPoints2);
                    charge2.ChgDemeritPoints = demeritPoints2;

                    charge2.ChargeStatus = OfficerErrors.Trim().Length > 0 ? OfficerErrorCHARGESTATUS : CHARGESTATUS; ;
                    charge2.ChgOffenceType = offenceType; 

                    //charge2.ChgIsMain = true;
                    charge2.LastUser = _lastUser;

                    charge2 = new ChargeService().Save(charge2);

                    if (!String.IsNullOrEmpty(hsXML["AlternativeChargeOffenceCode2"].ToString().Trim()))
                    {
                        Charge alternativeCharge2 = null;
                        if (notice2IsUpdate)
                        {
                            foreach (Charge chg in new ChargeService().GetByNotIntNo(notice2.NotIntNo))
                            {
                                if (chg.ChgIsMain == false)
                                {
                                    alternativeCharge2 = chg;
                                    break;
                                }
                            }
                        }
                        if (alternativeCharge2 == null)
                        {
                            alternativeCharge2 = new Charge();

                            alternativeCharge2.NotIntNo = notice2.NotIntNo;
                            alternativeCharge2.ChgIsMain = false;
                            //alternativeCharge2.CtIntNo = CTINTNO;
                        }

                        alternativeCharge2.ChgOffenceCode = hsXML["AlternativeChargeOffenceCode2"].ToString().Trim();
                        alternativeCharge2.ChgOffenceDescr = hsXML["AlternativeChargeOffenceDescr2"].ToString().Trim();

                        float alternativeFineAmount2 = 0;
                        float.TryParse(hsXML["AlternativeChargeFineAmount2"].ToString().Trim(), out alternativeFineAmount2);
                        alternativeCharge2.ChgFineAmount = alternativeFineAmount2;
                        alternativeCharge2.ChgRevFineAmount = alternativeFineAmount2;

                        decimal alternativeDiscountAmount2 = 0;
                        decimal.TryParse(hsXML["AlternativeChargeDiscountAmount2"].ToString().Trim(), out alternativeDiscountAmount2);
                        alternativeCharge2.ChgDiscountAmount = alternativeDiscountAmount2;
                        alternativeCharge2.ChgRevDiscountAmount = (float)alternativeDiscountAmount2;

                        decimal alternativeDiscountedAmount2 = 0;
                        decimal.TryParse(hsXML["AlternativeChargeDiscountedAmount2"].ToString().Trim(), out alternativeDiscountedAmount2);
                        alternativeCharge2.ChgDiscountedAmount = alternativeDiscountedAmount2;
                        alternativeCharge2.ChgRevDiscountedAmount = (float)alternativeDiscountedAmount2;

                        int AlternativeCTIntNo2 = 0;
                        int.TryParse(hsXML["AlternativeChargeType2"].ToString().Trim(), out AlternativeCTIntNo2);
                        alternativeCharge2.CtIntNo = CTINTNO;// AlternativeCTIntNo2;
                        alternativeCharge2.ChgType = hsXMLText["AlternativeChargeType2"].ToString();

                        int alternativeDemeritPoints2 = 0;
                        int.TryParse(hsXML["AlternativeChargeDemeritPoints2"].ToString().Trim(), out alternativeDemeritPoints2);
                        alternativeCharge2.ChgDemeritPoints = alternativeDemeritPoints2;


                        alternativeCharge2.ChargeStatus = OfficerErrors.Trim().Length > 0 ? OfficerErrorCHARGESTATUS : CHARGESTATUS; ;
                        //alternativeCharge2.ChgIsMain = false;
                        alternativeCharge2.LastUser = _lastUser;

                        alternativeCharge2 = new ChargeService().Save(alternativeCharge2);
                    }

                    #endregion

                    #region Driver

                    driver2 = new Driver();
                    if (notice2IsUpdate)
                    {
                        SIL.AARTO.DAL.Entities.TList<Driver> tempDriverList = new DriverService().GetByNotIntNo(notice2.NotIntNo);
                        if (tempDriverList.Count > 0)
                        {
                            driver2 = tempDriverList[0];
                        }
                    }

                    #region no clone
                    //driver2 = new Driver();

                    driver2.NotIntNo = notice2.NotIntNo;

                    driver2.DrvSurname = hsXML["DrvSurname"].ToString().Trim();
                    driver2.DrvForenames = hsXML["DrvForenames"].ToString().Trim();
                    driver2.DrvInitials = hsXML["DrvInitials"].ToString().Trim();

                    DateTime birthDate = DateTime.Now;
                    DateTime.TryParse(hsXML["BirthDate"].ToString().Trim(), out birthDate);
                    driver2.DrvAge = Convert.ToString(DateTime.Now.Year - birthDate.Year + 1);
                    if (hsXML["AaIdTypeId"] != null)
                        driver2.AaIdTypeId = Convert.ToInt32(hsXML["AaIdTypeId"]);
                    driver2.DrvIdNumber = hsXML["DrvIdNumber"].ToString().Trim();
                    driver2.DrvLicencePlace = hsXML["DrvLicencePlace"].ToString().Trim();
                    driver2.DrvLicenceCode = hsXML["DrvLicenceCode"].ToString().Trim();
                    if (hsXML["AaLiCodeId"] != null)
                        driver2.AaLiCodeId = Convert.ToInt32(hsXML["AaLiCodeId"]);
                    if (hsXML["AaLeCodeId"] != null)
                        driver2.AaLeCodeId = Convert.ToInt32(hsXML["AaLeCodeId"]);
                    if (hsXML["AaPrDpCodeId"] != null)
                        driver2.AaPrDpCodeId = Convert.ToInt32(hsXML["AaPrDpCodeId"]);
                    driver2.DrvStAdd1 = hsXML["DrvStAdd1"].ToString().Trim();
                    driver2.DrvStAdd2 = hsXML["DrvStAdd2"].ToString().Trim();
                    driver2.DrvPoAdd1 = hsXML["DrvPoAdd1"].ToString().Trim();
                    driver2.DrvPoAdd2 = hsXML["DrvPoAdd2"].ToString().Trim();
                    driver2.DrvPoCode = hsXML["DrvPoCode"].ToString().Trim();
                    driver2.DrvEmployerName = hsXML["DrvEmployerName"].ToString().Trim();
                    driver2.DrvEmployerAddress = hsXML["DrvEmployerAddress"].ToString().Trim();
                    driver2.DrvHomeNo = hsXML["DrvHomeNo"].ToString().Trim();
                    driver2.DrvWorkNo = hsXML["DrvWorkNo"].ToString().Trim();
                    driver2.DrvFax = hsXML["DrvFax"].ToString().Trim();
                    driver2.DrvCellNo = hsXML["DrvCellNo"].ToString().Trim();
                    driver2.DrvEmail = hsXML["DrvEmail"].ToString().Trim();
                    driver2.LastUser = _lastUser;
                    #endregion

                    driver2 = new DriverService().Save(driver2);
                    #endregion

                    NoticeManager.AddSearchIDAndName(notice2.NotIntNo, driver2.DrvIdNumber, driver2.DrvSurname, driver2.DrvInitials);

                    if (notice2IsUpdate == false)
                    {
                        #region Film
                        Film film = new Film();
                        film.AutIntNo = autIntNo;
                        film.ConIntNo = CONINTNO;//
                        film.FilmNo = filmNo+"_2";
                        film.CdLabel = CDLABEL;//
                        film.FilmDescr = FILMDESCR;//
                        film.FilmLoadDateTime = DateTime.Now;
                        film.LastUser = _lastUser;

                        new FilmService().Save(film);

                        #endregion

                        #region Frame
                        Frame frame = new Frame();
                        frame.FilmIntNo = film.FilmIntNo;
                        frame.FrameNo = "";
                        frame.OffenceDate = offenceDate;
                        frame.FrameImagePath = imgFilePath;
                        frame.IfsIntNo = importDataEntity.IFSIntNo;
                        frame.LocIntNo = LOCINTNO;//
                        frame.VmIntNo = VMINTNO;//
                        frame.VtIntNo = VTINTNO;//
                        frame.CamIntNo = CAMINTNO;
                        frame.ToIntNo = TOINTNO;//
                        frame.RdTintNo = RDTINTNO;//
                        frame.SzIntNo = SZINTNO;//
                        frame.CrtIntNo = CRTINTNO;//
                        frame.RejIntNo = REJINTNO;//
                        frame.AutNo = AUTNO;//
                        frame.CamUnitId = CAMUNITID;//
                        frame.FirstSpeed = FIRSTSPEED;//
                        frame.SecondSpeed = SECONDSPEED;//
                        frame.Sequence = SEQUENCE;//
                        frame.RegNo = REGNO;//
                        frame.ElapsedTime = ELAPSEDTIME;
                        frame.TravelDirection = TRAVELDIRECTION;//
                        frame.ManualView = MANUALVIEW;//
                        frame.MultFrames = MULTFRAMES;//
                        frame.OperatorCardNo = hsXML["OperatorCardNo"].ToString().Trim();
                        frame.LastUser = _lastUser;
                        frame.FrameStatus = 901;
                        frame.DmsDocumentNo = frameNo + "_2";

                        new FrameService().Save(frame);


                        #endregion

                        #region Notice_Frame
                        NoticeFrame noticeFrame = new NoticeFrame();
                        noticeFrame.NotIntNo = notice2.NotIntNo;
                        noticeFrame.FrameIntNo = frame.FrameIntNo;
                        noticeFrame.NfSourceCode = NFSOURCECODE;//
                        noticeFrame.LastUser = _lastUser;
                        new NoticeFrameService().Save(noticeFrame);
                        #endregion

                        // Add Clock 
                        DiscountPeriodClock clock = new DiscountPeriodClock(notice2.NotIntNo);
                        clock.LastUser = AARTODataManager.LastUser;
                        clock.Create();
                        clock.Start(ConvertDateTime.GetShortDateTime(offenceDate));
                    }
                }

                #endregion

                #region Notice3 Chareg3 Drive

                string chgOffenceCode3 = string.Empty;
                if (hsXML["ChargeOffenceCode3"] != null)
                {
                    chgOffenceCode3 = CheckOffenceCode(hsXML["ChargeOffenceCode3"].ToString().Trim());
                    if (String.IsNullOrEmpty(chgOffenceCode2))
                    {
                        Console.WriteLine(String.Format("Charge Offence Code: {0} does not exists!", chgOffenceCode3));
                    }
                }

                if (!String.IsNullOrEmpty(hsXML["NoticeNo3"].ToString().Trim()) && !String.IsNullOrEmpty(chgOffenceCode3))
                {
                    #region Notice

                    initalStatus = Initialization(crtNo, notVehicleMakeCode, notVehicleTypeCode, notOfficerNo, crtRoomNo, chgOffenceCode3, autIntNo, _lastUser, "01");

                    if (initalStatus == false)
                    {
                        Console.WriteLine("Initial Data failure!");
                        return;
                    }

                    Notice notice3 = null;

                    bool notice3IsUpdate = false;
                    string notice3TicketNo = hsXML["NoticeNo3"].ToString().Trim().Insert(2, "-").Insert(7, "-").Insert(17, "-");
                    SIL.AARTO.DAL.Entities.TList<Notice> listNotice3 = new NoticeService().GetByNotTicketNo(notice3TicketNo);
                    if (listNotice3.Count > 0)
                    {
                        notice3IsUpdate = true;
                        notice3 = listNotice3[0];
                    }
                    else
                    {
                        notice3 = new Notice();
                    }

                    #region not clone
                    //notice3 = new Notice();
                    notice3.NotTicketNo = hsXML["NoticeNo3"].ToString().Trim().Insert(2, "-").Insert(7, "-").Insert(17, "-");

                    //int.TryParse(hsXML["AutIntNo"].ToString().Trim(), out autIntNo);
                    notice3.AutIntNo = autIntNo;

                    DateTime.TryParse(hsXML["OffenceDate"].ToString().Trim() + " " + hsXML["OffenceTime"].ToString().Trim(), out offenceDate);
                    notice3.NotOffenceDate = offenceDate;

                    notice3.NotRegNo = hsXML["NotRegNo"].ToString().Trim();
                    notice3.NotClearanceCert = hsXML["NotClearanceCert"].ToString().Trim();
                    notice3.NotVehicleTypeCode = hsXML["VehicleType"].ToString().Trim();
                    notice3.NotVehicleType = hsXMLText["VehicleType"].ToString().Trim();
                    notice3.NotVehicleMakeCode = hsXML["VehicleMake"].ToString().Trim();
                    notice3.NotVehicleMake = hsXMLText["VehicleMake"].ToString().Trim();
                    notice3.NotVehicleColour = hsXML["VehicleColour"].ToString().Trim();

                    StringBuilder loctionInfringement = new StringBuilder();
                    loctionInfringement.Append("<LoctionInfringement>");
                    loctionInfringement.Append("<Province>" + hsXML["Province"].ToString().Trim() + "</Province>");
                    loctionInfringement.Append("<CityTown>" + hsXML["CityTown"].ToString().Trim() + "</CityTown>");
                    loctionInfringement.Append("<Suburb>" + hsXML["Suburb"].ToString().Trim() + "</Suburb>");
                    loctionInfringement.Append("<Date>" + hsXML["OffenceDate"].ToString().Trim() + "</Date>");
                    loctionInfringement.Append("<Time>" + hsXML["OffenceTime"].ToString().Trim() + "</Time>");
                    loctionInfringement.Append("<StreetNameA>" + hsXML["StreetNameA"].ToString().Trim() + "</StreetNameA>");
                    loctionInfringement.Append("<StreetNameB>" + hsXML["StreetNameB"].ToString().Trim() + "</StreetNameB>");
                    loctionInfringement.Append("<RouteNo>" + hsXML["RouteNo"].ToString().Trim() + "</RouteNo>");
                    loctionInfringement.Append("<FromPlace>" + hsXML["FromPlace"].ToString().Trim() + "</FromPlace>");
                    loctionInfringement.Append("<ToPlace>" + hsXML["ToPlace"].ToString().Trim() + "</ToPlace>");
                    loctionInfringement.Append("<GPSX>" + hsXML["GPSX"].ToString().Trim() + "</GPSX>");
                    loctionInfringement.Append("<GPSY>" + hsXML["GPSY"].ToString().Trim() + "</GPSY>");
                    loctionInfringement.Append("</LoctionInfringement>");

                    notice3.NotLoctionInfringement = loctionInfringement.ToString();

                    notice3.NotOfficerNo = hsXML["NotOfficerNo"].ToString().Trim();
                    notice3.NotOfficerSname = hsXML["NotOfficerSname"].ToString().Trim();
                    notice3.NotSource = "DMS";
                    //notice3.NotRdTypeCode = (short)RDTYPECODE;
                    notice3.OperatorCardNo = hsXML["OperatorCardNo"].ToString().Trim();

                    notice3.NotIsOfficerError = !String.IsNullOrEmpty(OfficerErrors);
                    notice3.NotOffenceType = offenceType;
                    notice3.LastUser = _lastUser;

                    SIL.AARTO.DAL.Entities.TList<TrafficOfficer> officerList = new TrafficOfficerService().GetByToNo(notice3.NotOfficerNo);
                    if (officerList.Count > 0)
                        notice2.NotOfficerInit = officerList[0].ToInit;

                    notice3.NotLocCode = locCode;
                    notice3.NotLocDescr = locDescr;
                    //notice1.NotSpeedLimit = 0;
                    notice3.NotCameraId = "000";

                    notice3.OrigRegNo = notice1.NotRegNo;
                    notice3.NotLoadDate = DateTime.Now.Date;
                    if (authority != null)
                    {
                        notice3.AutName = authority.AutName;
                    }
                    notice3.NotPaymentDate = offenceDate.AddDays(32);
                    notice3.NotIssue1stNoticeDate = offenceDate;
                    notice3.NotIssue2ndNoticeDate = offenceDate;
                    notice3.NotSendTo = "D";
                    notice3.NotFilmType = "M";
                    int notVechileGVM = 0;
                    int.TryParse(hsXML["NotVehicleGVM"].ToString(), out notVechileGVM);
                    notice3.NotVehicleGvm = notVechileGVM;
                    notice3.NotTicketProcessor = _TicketProcessor;
                    notice3.LastUser = _lastUser;
                    #endregion

                    notice3 = new NoticeService().Save(notice3);

                    AARTODataManager.SaveOfficerErrorsWithNotice(notice3.NotIntNo, OfficerErrors, AARTODataManager.LastUser);
                    #endregion

                    #region Charge
                    Charge charge3 = null;
                    if (notice3IsUpdate)
                    {
                        foreach (Charge chg in new ChargeService().GetByNotIntNo(notice2.NotIntNo))
                        {
                            if (chg.ChgIsMain == false)
                            {
                                charge3 = chg;
                                break;
                            }
                        }
                    }
                    else
                    {
                        AARTODataManager.CreateAARTONoticeDocumentAndImages(notice3.NotIntNo, importDataEntity.BaDoID, (int)AartoDocumentTypeList.AARTO01, (int)AartoDocumentStatusList.HandDelivered, importDataEntity.IFSIntNo);
                    }
                    if (charge3 == null)
                    {
                        charge3 = new Charge();

                        charge3.NotIntNo = notice2.NotIntNo;
                        charge3.ChgIsMain = true;
                        //charge3.CtIntNo = CTINTNO;
                    }

                    charge3.ChgOffenceCode = hsXML["ChargeOffenceCode3"].ToString().Trim();
                    charge3.ChgOffenceDescr = hsXML["ChargeOffenceDescr3"].ToString().Trim();

                    float fineAmount3 = 0;
                    float.TryParse(hsXML["ChargeFineAmount3"].ToString().Trim(), out fineAmount3);
                    charge3.ChgFineAmount = fineAmount3;
                    charge3.ChgRevFineAmount = fineAmount3;

                    decimal discountAmount3 = 0;
                    decimal.TryParse(hsXML["ChargeDiscountAmount3"].ToString().Trim(), out discountAmount3);
                    charge3.ChgDiscountAmount = discountAmount3;
                    charge3.ChgRevDiscountAmount = (float)discountAmount3;

                    decimal discountedAmount3 = 0;
                    decimal.TryParse(hsXML["ChargeDiscountedAmount3"].ToString().Trim(), out discountedAmount3);
                    charge3.ChgDiscountedAmount = discountedAmount3;
                    charge3.ChgRevDiscountedAmount = (float)discountedAmount3;

                    int CTIntNo3 = 0;
                    int.TryParse(hsXML["ChargeType3"].ToString().Trim(), out CTIntNo3);
                    charge3.CtIntNo = CTINTNO;// CTIntNo3;
                    charge3.ChgType = hsXMLText["ChargeType3"].ToString();

                    int demeritPoints3 = 0;
                    int.TryParse(hsXML["ChargeDemeritPoints3"].ToString().Trim(), out demeritPoints3);
                    charge3.ChgDemeritPoints = demeritPoints3;

                    charge3.ChargeStatus = OfficerErrors.Trim().Length > 0 ? OfficerErrorCHARGESTATUS : CHARGESTATUS; ;
                    charge3.ChgOffenceType = offenceType; 

                    //charge3.ChgIsMain = true;
                    charge3.LastUser = _lastUser;

                    charge3 = new ChargeService().Save(charge3);

                    if (!String.IsNullOrEmpty(hsXML["AlternativeChargeOffenceCode3"].ToString().Trim()))
                    {
                        Charge alternativeCharge3 = null;
                        if (notice3IsUpdate)
                        {
                            foreach (Charge chg in new ChargeService().GetByNotIntNo(notice2.NotIntNo))
                            {
                                if (chg.ChgIsMain == false)
                                {
                                    alternativeCharge3 = chg;
                                    break;
                                }
                            }
                        }
                        if (alternativeCharge3 == null)
                        {
                            alternativeCharge3 = new Charge();

                            alternativeCharge3.NotIntNo = notice2.NotIntNo;
                            alternativeCharge3.ChgIsMain = false;
                            //alternativeCharge3.CtIntNo = CTINTNO;
                        }

                        alternativeCharge3.ChgOffenceCode = hsXML["AlternativeChargeOffenceCode3"].ToString().Trim();
                        alternativeCharge3.ChgOffenceDescr = hsXML["AlternativeChargeOffenceDescr3"].ToString().Trim();

                        float alternativeFineAmount3 = 0;
                        float.TryParse(hsXML["AlternativeChargeFineAmount3"].ToString().Trim(), out alternativeFineAmount3);
                        alternativeCharge3.ChgFineAmount = alternativeFineAmount3;
                        alternativeCharge3.ChgRevFineAmount = alternativeFineAmount3;

                        decimal alternativeDiscountAmount3 = 0;
                        decimal.TryParse(hsXML["AlternativeChargeDiscountAmount3"].ToString().Trim(), out alternativeDiscountAmount3);
                        alternativeCharge3.ChgDiscountAmount = alternativeDiscountAmount3;
                        alternativeCharge3.ChgRevDiscountAmount = (float)alternativeDiscountAmount3;

                        decimal alternativeDiscountedAmount3 = 0;
                        decimal.TryParse(hsXML["AlternativeChargeDiscountedAmount3"].ToString().Trim(), out alternativeDiscountedAmount3);
                        alternativeCharge3.ChgDiscountedAmount = alternativeDiscountedAmount3;
                        alternativeCharge3.ChgRevDiscountedAmount = (float)alternativeDiscountedAmount3;
                        int AlternativeCTIntNo3 = 0;
                        int.TryParse(hsXML["AlternativeChargeType3"].ToString().Trim(), out AlternativeCTIntNo3);
                        alternativeCharge3.CtIntNo = CTINTNO;// AlternativeCTIntNo3;
                        alternativeCharge3.ChgType = hsXMLText["AlternativeChargeType3"].ToString();

                        int alternativeDemeritPoints3 = 0;
                        int.TryParse(hsXML["AlternativeChargeDemeritPoints3"].ToString().Trim(), out alternativeDemeritPoints3);
                        alternativeCharge3.ChgDemeritPoints = alternativeDemeritPoints3;


                        alternativeCharge3.ChargeStatus = OfficerErrors.Trim().Length > 0 ? OfficerErrorCHARGESTATUS : CHARGESTATUS; ;
                        alternativeCharge3.LastUser = _lastUser;

                        alternativeCharge3 = new ChargeService().Save(alternativeCharge3);
                    }

                    #endregion

                    #region Driver
                    Driver driver3 = new Driver();

                    if (notice3IsUpdate)
                    {
                        SIL.AARTO.DAL.Entities.TList<Driver> tempDriverList = new DriverService().GetByNotIntNo(notice3.NotIntNo);
                        if (tempDriverList.Count > 0)
                        {
                            driver3 = tempDriverList[0];
                        }
                    }

                    #region no clone
                    //driver3 = new Driver();

                    driver3.NotIntNo = notice3.NotIntNo;

                    driver3.DrvSurname = hsXML["DrvSurname"].ToString().Trim();
                    driver3.DrvForenames = hsXML["DrvForenames"].ToString().Trim();
                    driver3.DrvInitials = hsXML["DrvInitials"].ToString().Trim();

                    DateTime birthDate = DateTime.Now;
                    DateTime.TryParse(hsXML["BirthDate"].ToString().Trim(), out birthDate);
                    driver3.DrvAge = Convert.ToString(DateTime.Now.Year - birthDate.Year + 1);
                    if (hsXML["AaIdTypeId"] != null)
                        driver3.AaIdTypeId = Convert.ToInt32(hsXML["AaIdTypeId"]);
                    driver3.DrvIdNumber = hsXML["DrvIdNumber"].ToString().Trim();
                    driver3.DrvLicencePlace = hsXML["DrvLicencePlace"].ToString().Trim();
                    driver3.DrvLicenceCode = hsXML["DrvLicenceCode"].ToString().Trim();
                    if (hsXML["AaLiCodeId"] != null)
                        driver3.AaLiCodeId = Convert.ToInt32(hsXML["AaLiCodeId"]);
                    if (hsXML["AaLeCodeId"] != null)
                        driver3.AaLeCodeId = Convert.ToInt32(hsXML["AaLeCodeId"]);
                    if (hsXML["AaPrDpCodeId"] != null)
                        driver3.AaPrDpCodeId = Convert.ToInt32(hsXML["AaPrDpCodeId"]);
                    driver3.DrvStAdd1 = hsXML["DrvStAdd1"].ToString().Trim();
                    driver3.DrvStAdd2 = hsXML["DrvStAdd2"].ToString().Trim();
                    driver3.DrvPoAdd1 = hsXML["DrvPoAdd1"].ToString().Trim();
                    driver3.DrvPoAdd2 = hsXML["DrvPoAdd2"].ToString().Trim();
                    driver3.DrvPoCode = hsXML["DrvPoCode"].ToString().Trim();
                    driver3.DrvEmployerName = hsXML["DrvEmployerName"].ToString().Trim();
                    driver3.DrvEmployerAddress = hsXML["DrvEmployerAddress"].ToString().Trim();
                    driver3.DrvHomeNo = hsXML["DrvHomeNo"].ToString().Trim();
                    driver3.DrvWorkNo = hsXML["DrvWorkNo"].ToString().Trim();
                    driver3.DrvFax = hsXML["DrvFax"].ToString().Trim();
                    driver3.DrvCellNo = hsXML["DrvCellNo"].ToString().Trim();
                    driver3.DrvEmail = hsXML["DrvEmail"].ToString().Trim();
                    driver3.LastUser = _lastUser;
                    #endregion

                    driver3 = new DriverService().Save(driver3);

                    #endregion

                    NoticeManager.AddSearchIDAndName(notice3.NotIntNo, driver3.DrvIdNumber, driver3.DrvSurname, driver3.DrvInitials);

                    if (!notice3IsUpdate)
                    {
                        # region Insert Film and Frame
                        #region Film
                        Film film = new Film();
                        film.AutIntNo = autIntNo;
                        film.ConIntNo = CONINTNO;//
                        film.FilmNo = filmNo+"_3";
                        film.CdLabel = CDLABEL;//
                        film.FilmDescr = FILMDESCR;//
                        film.FilmLoadDateTime = DateTime.Now;
                        film.LastUser = _lastUser;

                        new FilmService().Save(film);

                        #endregion

                        #region Frame
                        Frame frame = new Frame();
                        frame.FilmIntNo = film.FilmIntNo;
                        frame.FrameNo = "";
                        frame.OffenceDate = offenceDate;
                        frame.FrameImagePath = imgFilePath;
                        frame.IfsIntNo = importDataEntity.IFSIntNo;
                        frame.LocIntNo = LOCINTNO;//
                        frame.VmIntNo = VMINTNO;//
                        frame.VtIntNo = VTINTNO;//
                        frame.CamIntNo = CAMINTNO;
                        frame.ToIntNo = TOINTNO;//
                        frame.RdTintNo = RDTINTNO;//
                        frame.SzIntNo = SZINTNO;//
                        frame.CrtIntNo = CRTINTNO;//
                        frame.RejIntNo = REJINTNO;//
                        frame.AutNo = AUTNO;//
                        frame.CamUnitId = CAMUNITID;//
                        frame.FirstSpeed = FIRSTSPEED;//
                        frame.SecondSpeed = SECONDSPEED;//
                        frame.Sequence = SEQUENCE;//
                        frame.RegNo = REGNO;//
                        frame.ElapsedTime = ELAPSEDTIME;
                        frame.TravelDirection = TRAVELDIRECTION;//
                        frame.ManualView = MANUALVIEW;//
                        frame.MultFrames = MULTFRAMES;//
                        frame.OperatorCardNo = hsXML["OperatorCardNo"].ToString().Trim();
                        frame.FrameStatus = 901;
                        frame.LastUser = _lastUser;
                         
                        frame.DmsDocumentNo = frameNo + "_3";

                        new FrameService().Save(frame);


                        #endregion

                        #region Notice_Frame
                        NoticeFrame noticeFrame = new NoticeFrame();
                        noticeFrame.NotIntNo = notice3.NotIntNo;
                        noticeFrame.FrameIntNo = frame.FrameIntNo;
                        noticeFrame.NfSourceCode = NFSOURCECODE;//

                        new NoticeFrameService().Save(noticeFrame);
                        #endregion

                        #endregion

                        // Add Clock 
                        DiscountPeriodClock clock = new DiscountPeriodClock(notice3.NotIntNo);
                        clock.LastUser = AARTODataManager.LastUser;
                        clock.Create();
                        clock.Start(ConvertDateTime.GetShortDateTime(offenceDate));
                    }
                }

                #endregion

                DMSDataManager.UpdateImportResultToDMS(importDataEntity.BaDoID, true, "");
                UpdateAARTOSuccessful = true;
                //update status into DMS
                UpdateDMSSuccessful = true;
                AARTOTM.Commit();
                DMSTM.Commit();
            }
            catch (Exception ex)
            {
                _importSuccess = false;
                if (!UpdateAARTOSuccessful || !UpdateDMSSuccessful)
                {
                    if (AARTOTM.IsOpen) AARTOTM.Rollback();
                    if (DMSTM.IsOpen) DMSTM.Rollback();
                }
                DMSDataManager.UpdateImportResultToDMS(importDataEntity.BaDoID, false, AARTOMessage.UNKNOWN_ERROR);

                //Jake 2010-10-15 Write log using Enterprise Library log
                //ErrorLog.AddErrorLog(ex, AARTODataManager.LastUser);
                EntLibLogger.WriteErrorLog(ex, LogCategory.Error, AartoProjectList.AARTOImporterConsole.ToString());
            }
            finally
            {
                AARTOTM.Dispose();
                DMSTM.Dispose();

            }
        }
    }
}
