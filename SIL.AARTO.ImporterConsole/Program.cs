﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration;
using System.Threading;
using System.Xml;

using SIL.AARTO.ImporterConsole.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.EntLib;
using System.Reflection;
namespace SIL.AARTO.ImporterConsole
{
    public class Program
    {
        private static int ImportNumOnce = 0;
        private static AARTOData aartoData;

        public static string APP_TITLE = string.Empty;
        public static string APP_NAME = AartoProjectList.AARTOImporterConsole.ToString();

        static void Main(string[] args)
        {
            string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
            Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);

            // Check Last updated Version            
            string errorMessage;
            if (!CheckVersionManager.CheckVersion(AartoProjectList.AARTOImporterConsole, out errorMessage))
            {
                Console.WriteLine(errorMessage);
                EntLibLogger.WriteLog(LogCategory.General, null, errorMessage);
                return;
            }

            // Write the started Log
            APP_TITLE = string.Format("{0} - Version {1}\n Last Updated {2}\n Started at: {3}",
                APP_NAME,
                Assembly.GetExecutingAssembly().GetName().Version.ToString(2),
                ProjectLastUpdated.AARTOImporterConsole, DateTime.Now);

            string strDatabaseInfo = EntLibLogger.GetServerAndDatabaseName(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());
                        
            EntLibLogger.WriteLog(LogCategory.General, null, string.Format("{0} \n\r {1}", APP_TITLE, strDatabaseInfo));
            Console.Title = APP_TITLE; 

            AartoRepresentation rep = new AartoRepresentation();

            //string ttt = TicketNumber.GenerateNotNo(22, 90, 3, "Lucas");
            //bool ss = Verhoeff.Check(ttt.Replace("-", ""));
            if (AARTODataManager.IsTheImageFileServerSame())
            {
                ImportNumOnce = Convert.ToInt32(ConfigurationManager.AppSettings["RecordNumberOnceReadFromDB"]);
                List<ImportDataEntity> list = null;
                list = DMSDataManager.GetBatchDocumentListForImport("", ImportNumOnce);
                if (list.Count > 0)
                {
                    foreach (ImportDataEntity idEntity in list)
                    {
                        aartoData = AARTOFactory.CreateAARTODataInstance(idEntity.DoTeTypeID);
                        aartoData.DoImport(idEntity);
                    }
                    AARTOImageFileServerData.Get().Clear();
                }
            }
            else
            {
                EntLibLogger.WriteLog(LogCategory.General, null, string.Format("Image file server data need to update."));
                //ErrorLog.AddErrorLog("Image file server data need to update.", 10, AARTODataManager.LastUser);
            }

            // Write the ended Log for end of console app.
            EntLibLogger.WriteLog(LogCategory.General, null, string.Format("{0} – Ended at {1}", APP_NAME, DateTime.Now));  
        }
    }
}
