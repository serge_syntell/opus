﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SIL.AARTO.ImporterConsole.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
namespace SIL.AARTO.ImporterConsole.AARTO14
{
    public class AARTO14Data : AARTOData
    {
        public AARTO14Data()
        {
            _aartoEntity = new AARTOEntity();
        }
        protected override void LoadOtherData(XmlNodeList dataList)
        {
            //CrtIntNo,AaEnfOrderRevAppDate,AaEnfOrderRevReason
        }
        protected override void UpdateChargeStatus()
        {
            AARTODataManager.UpdateChargeStatus(ChargeStatusList.AARTO_RequestRevocationEnforcementOrder_14, _charge.ChgIntNo);
        }
        protected override void CreateReceiptForRepresentationDocument(int repID)
        {
        }
        protected override int DoValidation(out int needCancelledrepID, out int notIntNo)
        {
            return AARTODataManager.IsRepresentationValidForAARTO14(_aartoEntity.NotTicketNo, _importData.DoTeTypeID, out needCancelledrepID, out notIntNo);
        }
    }
}
