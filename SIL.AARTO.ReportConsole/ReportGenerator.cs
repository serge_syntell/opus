﻿using System;
using System.Collections.Generic;
using System.Text;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.EntLib;
using System.Xml;
using System.IO;
using System.Data;
using System.Runtime.Serialization.Formatters.Binary;
using SIL.AARTO.BLL.Report;
using System.Globalization;

namespace SIL.AARTO.ReportConsole
{
    public class ReportGenerator
    {
        public ReportGenerator()
        {
        }

        private TimeSpan ProgramRunTime = new TimeSpan(0, 30, 0);

        public void GenerateReport()
        {
            TList<ReportConfig> reports = SIL.AARTO.BLL.Report.ReportManager.GetAllReportConfig();

            foreach (ReportConfig rc in reports)
            {
                if (rc.AutomaticGenerate)
                {
                    if (rc.AutomaticGenerateLastDatetime == null)
                    {
                        ConsoleWriteAndLogInfo("[" + DateTime.Now.ToString() + "]Report " + rc.ReportName + " generating (first time) ... ");
                        Generate(rc);
                    }
                    else
                    {
                        DateTime? dt = rc.AutomaticGenerateLastDatetime;
                        DateTime dtCompare = DateTime.MinValue;
                        switch(rc.AutomaticGenerateIntervalType.Trim().ToLower())
                        {
                            case "hours":
                                dtCompare = dt.Value.AddHours(rc.AutomaticGenerateInterval.Value);
                                break;
                            case "days":
                                dtCompare = dt.Value.AddDays(rc.AutomaticGenerateInterval.Value);
                                break;
                            case "months":
                                dtCompare = dt.Value.AddMonths(rc.AutomaticGenerateInterval.Value);
                                break;
                            case "daily":
                                if (dt.Value.Date != DateTime.Now.Date)
                                {
                                    ConsoleWriteAndLogInfo("[" + DateTime.Now.ToString() + "] Report " + rc.ReportName + " generating... ");
                                    Generate(rc);
                                }
                                break;
                            case "weekly":
                                int iCurrentWeek = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFullWeek, DayOfWeek.Saturday);
                                int iCompareWeek = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(dt.Value.Date, CalendarWeekRule.FirstFullWeek, DayOfWeek.Saturday);

                                if (iCurrentWeek != iCompareWeek)
                                {
                                    ConsoleWriteAndLogInfo("[" + DateTime.Now.ToString() + "] Report " + rc.ReportName + " generating... ");
                                    Generate(rc);
                                }
                                break;
                            case "monthly":
                                if (dt.Value.Date.Month != DateTime.Now.Date.Month || dt.Value.Date.Year != DateTime.Now.Date.Year)
                                {
                                    ConsoleWriteAndLogInfo("[" + DateTime.Now.ToString() + "] Report " + rc.ReportName + " generating... ");
                                    Generate(rc);
                                }
                                break;

                        }
                    }
                }
            }
        }

        private void GenerateWithDateTime(DateTime dtCompare, ReportConfig rc)
        {
            if (DateTime.Now.CompareTo(dtCompare.Subtract(ProgramRunTime)) > 0)
            {
                ConsoleWriteAndLogInfo("[" + DateTime.Now.ToString() + "] Report " + rc.ReportName + " generating... ");
                Generate(rc);
            }
        }

        private void Generate(ReportConfig rc)
        {
            ReportModel model = new ReportModel();
            SIL.AARTO.BLL.Report.ReportManager.DeepLoadReport(rc);

            if (rc.AutomaticGenerateQuery != null && rc.AutomaticGenerateQuery.Length > 0)
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    memoryStream.Write(rc.AutomaticGenerateQuery, 0, rc.AutomaticGenerateQuery.Length);
                    memoryStream.Position = 0;
                    model = formatter.Deserialize(memoryStream) as ReportModel;
                }
            }

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(rc.Template);
            model.xmlDoc = xmlDoc;
            model.AutIntNo = rc.AutIntNo.ToString();
            model.ReportName = rc.ReportName;
            model.ReportCode = rc.ReportCode;
            model.StoredProcName = rc.RspIntNoSource.ReportStoredProcName;

            switch (rc.AutomaticGenerateIntervalType.Trim().ToLower())
            {
                case "daily":
                    model.DateFrom = DateTime.Now.AddDays(-1).Date;
                    model.DateTo = DateTime.Now.Date.AddMilliseconds(-1);
                    break;
                case "weekly":
                   int iDays = (int)DateTime.Now.DayOfWeek + 2;
                   if (iDays > 7)
                       iDays -= 7;

                   model.DateFrom = DateTime.Now.AddDays(-1 * iDays - 7).Date;
                   model.DateTo = DateTime.Now.AddDays(-1 * iDays).Date.AddMilliseconds(-1);
                    break;
                case "monthly":
                    int iMonth = DateTime.Now.Month - 1;

                    model.DateFrom = new DateTime(DateTime.Now.Year, iMonth, 1);
                    model.DateTo = new DateTime(DateTime.Now.Year, iMonth + 1, 1).AddMilliseconds(-1);
                    break;
                default:
                    break;
            }

            SetupModel(model);

            string RootPath = System.Configuration.ConfigurationManager.AppSettings["RootPath"].ToString();
            ReportExporter reportExporter = new ReportExporter(RootPath);
            switch (rc.AutomaticGenerateFormat.Trim().ToLower())
            {
                case "ascii":
                    reportExporter.ExportToTxt(model, 365);
                    ConsoleWriteAndLogInfo("[" + DateTime.Now.ToString() + "] Report (ascii)" + rc.ReportName + " generated.");
                    break;
                case "word":
                    reportExporter.ExportToWord(model, 365);
                    ConsoleWriteAndLogInfo("[" + DateTime.Now.ToString() + "] Report (word)" + rc.ReportName + " generated.");
                    break;
                case "excel":
                    reportExporter.ExportToExcel(model, 365);
                    ConsoleWriteAndLogInfo("[" + DateTime.Now.ToString() + "] Report (excel)" + rc.ReportName + " generated.");
                    break;
                case "pdf":
                    reportExporter.ExportToPDF(model, true, 365);
                    ConsoleWriteAndLogInfo("[" + DateTime.Now.ToString() + "] Report (pdf)" + rc.ReportName + " generated.");
                    break;
            }

            rc.AutomaticGenerateLastDatetime = DateTime.Now;
            SIL.AARTO.BLL.Report.ReportManager.UpdateReport(rc);
        }


        private void SetupModel(ReportModel model)
        {
            model.Parameters = model.ReportName;
            if (model.DateFrom > new DateTime(1900, 1, 1))
                model.Parameters += "<BR/>" + model.DateFrom.ToString("yyyy/MM/dd") + " - " + model.DateTo.ToString("yyyy/MM/dd");

            if (model.SelectedCourtsList != null && model.SelectedCourtsList.Count > 0)
            {
                model.Parameters += "<BR/>";
                foreach (Court c in model.SelectedCourtsList)
                {
                    model.Parameters += " " + c.CrtName;
                }
            }

            if (model.LANameAndCode == null && model.AutIntNo != null)
            {
                Authority aut = SIL.AARTO.BLL.Report.ReportManager.GetAuthorityById(Convert.ToInt32(model.AutIntNo));
                model.LANameAndCode = aut.AutNo + " " + aut.AutName;
            }

            DataSet ds = SIL.AARTO.BLL.Report.ReportManager.GetData(model);
            DataTable dt = ds.Tables[0];
            if (ds.Tables.Count == 3 && ds.Tables[1].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[1].Rows[0];
                model.ItemCount = Convert.ToInt32(dr[1]);
            }
        }



        private void ConsoleWriteAndLogInfo(string strInfo)
        {
            Console.WriteLine(strInfo);
            EntLibLogger.WriteLog(LogCategory.General, null, strInfo);
        }
    }
}
