﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.EntLib;
using System.Reflection;
using System.Configuration;

namespace SIL.AARTO.ReportConsole
{
    class Program
    {
        public static string APP_TITLE = string.Empty;
        public static string APP_NAME = AartoProjectList.AARTOReportConsole.ToString();

        static void Main(string[] args)
        {
            string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
            Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);

            // Check Last updated Version            
            string errorMessage;
            if (!CheckVersionManager.CheckVersion(AartoProjectList.AARTOReportConsole, out errorMessage))
            {
                Console.WriteLine(errorMessage);
                EntLibLogger.WriteLog(LogCategory.General, null, errorMessage);
                return;
            }

            // Write the started Log
            APP_TITLE = string.Format("{0} - Version {1}\n Last Updated {2}\n Started at: {3}",
                APP_NAME,
                Assembly.GetExecutingAssembly().GetName().Version.ToString(2),
                ProjectLastUpdated.AARTOBatchConsole, DateTime.Now);

            string strDatabaseInfo = EntLibLogger.GetServerAndDatabaseName(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());

            //Logger.Write(string.Format("{0} \n\r {1}", APP_TITLE, strDatabaseInfo), "General", (int)TraceEventType.Information);
            EntLibLogger.WriteLog(LogCategory.General, null, string.Format("{0} \n\r {1}", APP_TITLE, strDatabaseInfo));
            Console.Title = APP_TITLE;

            try
            {
                ReportGenerator reportGenerator = new ReportGenerator();
                reportGenerator.GenerateReport();
              
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EntLibLogger.WriteLog(LogCategory.Error, null, errorMessage);
            }

            // Write the ended Log for end of console app.
            EntLibLogger.WriteLog(LogCategory.General, null, string.Format("{0} – Ended at {1}", APP_NAME, DateTime.Now));
            //Console.WriteLine("Press enter to exit...");
            //Console.ReadLine();
        }   
    }
}
