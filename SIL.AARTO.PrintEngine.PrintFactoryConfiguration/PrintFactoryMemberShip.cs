﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Reflection;

using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.PrintEngine.PrintFactoryConfiguration
{
    public enum UserRole
    {
        User = 0,
        Admin = 1
    }

    [XmlRoot("MemberShip")]
    public class PrintFactoryMemberShip
    {
        public static string UserRegistrationsPath = "Configuration/UserRegistrations.xml";

        public List<PrintFactoryUser> Users;  

        public PrintFactoryMemberShip()
        {
            this.Users = new List<PrintFactoryUser>();
        }              

        #region [Public Function]

        public PrintFactoryUser FindByUserLoginName(string userName)
        {
            PrintFactoryUser userT = new PrintFactoryUser();
            userT.UserName = userName;
            return this.Users.Find(userT.FindByName);
        }

        public bool ValidateUser(string userName, string pwd)
        {
            PrintFactoryUser user = this.FindByUserLoginName(userName);
            if (user == null)
            {
                return false;
            }
            string storedPwd = user.Password;
            string inputPwd = Encrypt.HashPassword(pwd);
            if (storedPwd == inputPwd)
                return true;
            return false;
        }

        #endregion

        #region [Static Function]

        public static PrintFactoryMemberShip Instance()
        {
            return ObjectSerializer.ReadXmlFileToObject<PrintFactoryMemberShip>(UserRegistrationsPath);
        }

        public static PrintFactoryUser GetByUserLoginName(string userName)
        {
            PrintFactoryMemberShip memberShip = Instance();
            return memberShip.FindByUserLoginName(userName);
        }         

        public static bool Save(PrintFactoryMemberShip membership)
        {
            bool result = false;
            try
            {
                ObjectSerializer.SaveObjectToXmlFile(membership, UserRegistrationsPath);
                result = true;
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        #endregion


    }
    
    public class PrintFactoryUser
    {
        public string UserName
        {
            get;
            set;
        }
        public string Password
        {
            get;set;
        }
        public UserRole Role
        {
            get;
            set;
        }                                                                                                                                                                                                                                                                                                                                                                                    

        public bool FindByName(PrintFactoryUser user)
        {
            return this.UserName.ToLower().Trim() == user.UserName.ToLower().Trim();
        }
    }
}
