﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using SIL.AARTO.DAL.Entities;
using System.Reflection;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.PrintEngine.PrintFactoryConfiguration
{
    static class Program
    {
        public static string APP_TITLE = string.Empty;
        public static string APP_NAME = AartoProjectList.AARTOPrintEnginePrintFactoryConfiguration.ToString();
        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
            Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);

            //There is no database connected on PrintFactory, so don't need to Check Last updated Version 
            //string errorMessage;
            //if (!CheckVersionManager.CheckVersion(AartoProjectList.AARTOPrintEnginePrintFactoryConfiguration, out errorMessage))
            //{
            //    EntLibLogger.WriteLog(LogCategory.General, null, errorMessage);
            //    return;
            //}

            // Write the started Log
            APP_TITLE = string.Format("{0} - Version {1} Last Updated {2} Started at: {3}",
                APP_NAME,
                Assembly.GetExecutingAssembly().GetName().Version.ToString(2),
                ProjectLastUpdated.AARTOPrintEnginePrintFactoryConfiguration, DateTime.Now);

            //string strDatabaseInfo = EntLibLogger.GetServerAndDatabaseName(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());

            EntLibLogger.WriteLog(LogCategory.General, null, string.Format("{0}", APP_TITLE));


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            // Update batches.xml file from QueueA


            Application.Run(new UserLogin());
        }
    }
}
