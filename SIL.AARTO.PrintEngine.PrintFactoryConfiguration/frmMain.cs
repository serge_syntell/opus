﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using SIL.AARTO.PrintEngine.PrintFactoryConfiguration.Admin;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.PrintEngine.PrintFactoryConfiguration
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            tslLastUpdated.Text = Program.APP_TITLE;
        }

        private void userRegistrationsToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            UserRegistrations frmUserReg = new UserRegistrations();
            frmUserReg.MdiParent = this;
            frmUserReg.Show();
        }

        private void remoteSiteLARegistrationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemoteSiteRegistrations frmRemoteSiteReg = new RemoteSiteRegistrations();
            frmRemoteSiteReg.MdiParent = this;
            frmRemoteSiteReg.Show();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.MdiClient mc = GetMdiClient(this);
            if (mc != null)
            {
                mc.BackColor = Color.FromArgb(213, 221, 253);
                mc.Invalidate();
            }

            if (GlobalVariates<PrintFactoryUser>.CurrentUser.Role != UserRole.Admin)
            {
                adminToolStripMenuItem.Visible = false;
                remoteSiteLARegistrationsToolStripMenuItem.Enabled = false;
                userRegistrationsToolStripMenuItem.Enabled = false;
            }
        }

        public static System.Windows.Forms.MdiClient GetMdiClient(System.Windows.Forms.Form f)
        {
            foreach (System.Windows.Forms.Control c in f.Controls)
                if (c is System.Windows.Forms.MdiClient)
                    return (System.Windows.Forms.MdiClient)c;
            return null;
        }

        private void printBatchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrintBatches frmPrintBatch = new PrintBatches();
            frmPrintBatch.MdiParent = this;
            frmPrintBatch.Show();
        }
    }
}
