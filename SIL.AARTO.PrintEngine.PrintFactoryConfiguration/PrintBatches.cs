﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;

using SIL.AARTO.Util;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Model;
using System.Linq.Expressions;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.BLL.InfringerOption;
using SIL.AARTO.DAL.Entities;



namespace SIL.AARTO.PrintEngine.PrintFactoryConfiguration
{
    public partial class PrintBatches : Form
    {
        public PrintBatches()
        {
            InitializeComponent();
        }

        PrintFactoryBatches batches;

        private void PrintBatches_Load(object sender, EventArgs e)
        {
            string strBatchXmlPath = ConfigurationManager.AppSettings["BatchesXmlPath"];
            batches = PrintFactoryBatches.GetInstance(strBatchXmlPath);            

            // Dequeque and append batches.xml
            string qName = ConfigurationManager.AppSettings["ReceivedQueue"];
            int receiveTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["ReceiveQueueTimeout"]);            
            List<string> msgList = MessageQueueUtil.GetMsgListFromQueue<string>(qName, receiveTimeout);

            if (msgList != null && msgList.Count > 0)
            {
                foreach (string pebsPath in msgList)
                {                    
                    // Append pebs detail info to the batches.xml file for enquiry list.
                    batches.AppendPebsFile(pebsPath);
                }
                batches.SaveChange();
            }

            if (batches != null)
	        {                
                var las = from data in batches.Batches orderby data.AutCode ascending select data.AutCode;               
                cbxLA.DataSource = las.Distinct().ToList();

                var docTypes = from data in batches.Batches orderby data.DocumentType ascending select data.DocumentType;
                cbxDocType.DataSource = docTypes.Distinct().ToList();

                cbxStatus.SelectedIndex = 0;

                AdjustUI();
            }        
        }

        private bool GetCondition(PrintFactoryBatch batch)
        {
            bool boolResult = true;

            if (txtBatchNo.Text.Trim() != string.Empty)
            {
                boolResult &= batch.BatchCode == txtBatchNo.Text.Trim();
            }

            if (cbxLA.SelectedIndex > -1 && cbxLA.SelectedValue.ToString() != string.Empty)
            {
                boolResult &= batch.AutCode == cbxLA.SelectedValue.ToString();
            }
            
            if (cbxDocType.SelectedIndex > -1 && cbxDocType.SelectedValue.ToString() != string.Empty)
            {
                boolResult &= batch.DocumentType == cbxDocType.SelectedValue.ToString();
            }

            if (cbxStatus.SelectedIndex > -1 && cbxStatus.SelectedItem.ToString() != string.Empty)
            {
                boolResult &= batch.Status == cbxStatus.SelectedItem.ToString();
            }
            return boolResult;
        }


        private void AdjustUI()
        {
            if (dgvPebsBatches.Rows.Count > 0 && cbxStatus.SelectedItem != null &&
                cbxStatus.SelectedItem.ToString() == "Printed")
            {
                dateTimePicker1.Visible = true;
                btnSetPosted.Visible = true;
            }
            else
            {
                dateTimePicker1.Visible = false;
                btnSetPosted.Visible = false;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.RetrieveData();
        }

        private void btnSetPosted_Click(object sender, EventArgs e)
        {
            string CurrentUserName = GlobalVariates<PrintFactoryUser>.CurrentUser.UserName;
            
            DateTime dt = dateTimePicker1.Value;
            int k = 0;
            for (int i = 0; i < dgvPebsBatches.Rows.Count; i++)
            {
                bool selection = (bool)dgvPebsBatches.Rows[i].Cells["Selection"].FormattedValue;
                if (selection)
                {
                    k++;
                    
                    int batchId = (int)dgvPebsBatches.Rows[i].Cells["BatchId"].Value;
                    string path = dgvPebsBatches.Rows[i].Cells[2].Value.ToString();
                    
                    //AARTODocumentManager.UpdateAARTODocumentBatchAfterPosted(
                    //    batchId, dt, GlobalVariates<User>.CurrentUser.UserLoginName);
                    
                    // update pebs file
                    PebsBatch pebs = ObjectSerializer.ReadXmlFileToObject<PebsBatch>(path);
                    pebs.Post = new PebsPost();
                    pebs.Post.PostedTimeValue = DateTime.Now;
                    pebs.Post.IsPosted = true;
                    pebs.Post.PostedBy = CurrentUserName;
                    pebs.NeedToSynchronize = true;
                    pebs.Status = "Posted";
                    ObjectSerializer.SaveObjectToXmlFile(pebs, path);

                    // move pebs and pdf file to archieve folder
                    FileInfo file = new FileInfo(path);
                    //Directory.Move(file.DirectoryName, file.DirectoryName.Replace("Printed", "Posted"));

                    string newFolder = file.DirectoryName.Replace("Printed", "Posted");
                    if (!Directory.Exists(newFolder))
                    {
                        Directory.CreateDirectory(newFolder);
                    }
                    foreach (FileInfo f in file.Directory.GetFiles())
                    {
                        f.MoveTo(newFolder + "\\" + f.Name);
                    }
                    file.Directory.Delete();

                    // remove pebs from batch.xml
                    PrintFactoryBatch pfBatch = batches.Batches.First(x => x.BatchId == pebs.BatchId);
                    batches.Batches.Remove(pfBatch);
                    batches.SaveChange();

                    // enqueue to update webservice
                    string qName = ConfigurationManager.AppSettings["UpdateBatchStatusQueue"];
                    UpdateMessageInfo umi = new UpdateMessageInfo();
                    umi.BatchId = pebs.BatchId;
                    umi.LaCode = pebs.AutCode;
                    umi.ToStatus = AartoDocumentStatusList.Posted;
                    umi.UserName = CurrentUserName;

                    MessageQueueUtil.SendMsgToQueue(umi, qName);

                }
            }
            if (k > 0)
            {
                string msg = string.Format("{0} batch(es) are updated.", k);
                MessageBox.Show(msg);
                RetrieveData();
            }
            else
            {
                MessageBox.Show("Please select item(s).");
            }
        }

        public static bool PrintPDF(object filePath)
        {
            try
            {
                PdfPrintOperation operation = new PdfPrintOperation(filePath.ToString());
                Retryer retryer = new Retryer(operation);
                retryer.Perform(10, 5000);
                // Print the file.
                operation.Print();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool PrintConfirm()
        {
            string message = "Are you sure you wish to print?";
            string caption = "Are you sure you wish to print?";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            result = MessageBox.Show(message, caption, buttons);

            return (result == System.Windows.Forms.DialogResult.Yes);
        }
        
        private void dgvPebsBatches_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string CurrentUserName = GlobalVariates<PrintFactoryUser>.CurrentUser.UserName;
            
            int colIndex = e.ColumnIndex;
            if (colIndex >= 0)
            {
                int batchId = (int)dgvPebsBatches.Rows[e.RowIndex].Cells[1].Value;
                switch (colIndex)
                {
                    case 8:
                        if (PrintConfirm())
                        {
                            string path = dgvPebsBatches.Rows[e.RowIndex].Cells[2].Value.ToString();

                            bool b = PrintPDF(path.Replace(".pebs", ".pdf"));//File.Exists(path.Replace(".pebs", ".pdf"));
                            if (b)
                            {
                                PebsBatch pebs = ObjectSerializer.ReadXmlFileToObject<PebsBatch>(path);
                                if (cbxStatus.SelectedItem.ToString() == "Issued")
                                {
                                    // first print
                                    // update pebs file
                                    pebs.Print = new PebsPrint();
                                    pebs.Print.PrintedTimeValue = DateTime.Now;
                                    pebs.Print.IsPrinted = true;
                                    pebs.Print.PrintedBy = CurrentUserName;
                                    pebs.NeedToSynchronize = true;
                                    pebs.Status = "Printed";
                                    ObjectSerializer.SaveObjectToXmlFile(pebs, path);

                                    string strNewPath = path.Replace("Issued", "Printed");

                                    // update batch.xml file
                                    PrintFactoryBatch pfBatch = batches.Batches.First(x => x.BatchId == pebs.BatchId);
                                    pfBatch.Print = pebs.Print;
                                    pfBatch.NeedToSynchronize = pebs.NeedToSynchronize;
                                    pfBatch.Status = pebs.Status;
                                    pfBatch.Path = strNewPath;
                                    batches.SaveChange();

                                    //move pebs, pdf to printed folder
                                    FileInfo file = new FileInfo(path);
                                    string newFolder = file.DirectoryName.Replace("Issued", "Printed");
                                    if (!Directory.Exists(newFolder))
                                    {
                                        Directory.CreateDirectory(newFolder);
                                    }
                                    foreach (FileInfo f in file.Directory.GetFiles())
                                    {
                                        f.MoveTo(newFolder + "\\" + f.Name);
                                    }
                                    file.Directory.Delete();
                                    //Directory.Move(file.DirectoryName, newFolder);

                                    string qName = ConfigurationManager.AppSettings["UpdateBatchStatusQueue"];

                                    // send to update queue
                                    UpdateMessageInfo umi = new UpdateMessageInfo();
                                    umi.BatchId = pebs.BatchId;
                                    umi.LaCode = pebs.AutCode;
                                    umi.ToStatus = AartoDocumentStatusList.Printed;
                                    umi.UserName = CurrentUserName;

                                    MessageQueueUtil.SendMsgToQueue(umi, qName);

                                    RetrieveData();
                                }
                                else
                                {
                                    // reprint batch
                                    // update pebs file                                    
                                    PebsReprint reprint = new PebsReprint();
                                    reprint.ReprintDateValue = DateTime.Now;
                                    reprint.ReprintedBy = CurrentUserName;

                                    if (pebs.Reprints == null)
                                    {
                                        pebs.Reprints = new List<PebsReprint>();
                                    }                                    
                                    pebs.Reprints.Add(reprint);
                                    ObjectSerializer.SaveObjectToXmlFile(pebs, path);                                   
                                }
                                StringBuilder sbResult = new StringBuilder();
                                sbResult.AppendFormat("Batch {0} was printed.", pebs.BatchCode);
                                MessageBox.Show(sbResult.ToString());
                            }
                            //this.Enabled = true;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void RetrieveData()
        {
            Expression<Func<PrintFactoryBatch, bool>> expr = n => GetCondition(n);
            var xQuery = batches.Batches.Where<PrintFactoryBatch>(expr.Compile());
            //List<PrintFactoryBatch> listBatches = xQuery.ToList();
            //var pebses = from pebs in listBatches select pebs.Pebs;
            dgvPebsBatches.AutoGenerateColumns = false;
            dgvPebsBatches.DataSource = xQuery.ToList();//pebses.ToList();//xQuery.ToList<TestTable>();
           
            AdjustUI();
        }
    }
}
