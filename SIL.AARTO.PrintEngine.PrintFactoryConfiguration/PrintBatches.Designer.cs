﻿namespace SIL.AARTO.PrintEngine.PrintFactoryConfiguration
{
    partial class PrintBatches
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labLA = new System.Windows.Forms.Label();
            this.labDocType = new System.Windows.Forms.Label();
            this.labStatus = new System.Windows.Forms.Label();
            this.dgvPebsBatches = new System.Windows.Forms.DataGridView();
            this.cbxLA = new System.Windows.Forms.ComboBox();
            this.cbxDocType = new System.Windows.Forms.ComboBox();
            this.cbxStatus = new System.Windows.Forms.ComboBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.labBatchNo = new System.Windows.Forms.Label();
            this.txtBatchNo = new System.Windows.Forms.TextBox();
            this.btnSetPosted = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.Selection = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BatchId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Path = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocumentType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoOfDocuments = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateIssued = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DatePrinted = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Print = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPebsBatches)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 9;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 123F));
            this.tableLayoutPanel1.Controls.Add(this.labLA, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labDocType, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.labStatus, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.dgvPebsBatches, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.cbxLA, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbxDocType, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbxStatus, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnSearch, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.labBatchNo, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtBatchNo, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnSetPosted, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePicker1, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(784, 464);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // labLA
            // 
            this.labLA.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labLA.AutoSize = true;
            this.labLA.Location = new System.Drawing.Point(4, 18);
            this.labLA.Name = "labLA";
            this.labLA.Size = new System.Drawing.Size(23, 13);
            this.labLA.TabIndex = 4;
            this.labLA.Text = "LA:";
            // 
            // labDocType
            // 
            this.labDocType.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labDocType.AutoSize = true;
            this.labDocType.Location = new System.Drawing.Point(99, 18);
            this.labDocType.Name = "labDocType";
            this.labDocType.Size = new System.Drawing.Size(119, 13);
            this.labDocType.TabIndex = 4;
            this.labDocType.Text = "Select Document Type:";
            // 
            // labStatus
            // 
            this.labStatus.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labStatus.AutoSize = true;
            this.labStatus.Location = new System.Drawing.Point(317, 18);
            this.labStatus.Name = "labStatus";
            this.labStatus.Size = new System.Drawing.Size(73, 13);
            this.labStatus.TabIndex = 4;
            this.labStatus.Text = "Select Status:";
            // 
            // dgvPebsBatches
            // 
            this.dgvPebsBatches.AllowUserToAddRows = false;
            this.dgvPebsBatches.AllowUserToDeleteRows = false;
            this.dgvPebsBatches.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPebsBatches.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Selection,
            this.BatchId,
            this.Path,
            this.BatchNo,
            this.DocumentType,
            this.NoOfDocuments,
            this.DateIssued,
            this.DatePrinted,
            this.Print});
            this.tableLayoutPanel1.SetColumnSpan(this.dgvPebsBatches, 9);
            this.dgvPebsBatches.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPebsBatches.Location = new System.Drawing.Point(3, 53);
            this.dgvPebsBatches.Name = "dgvPebsBatches";
            this.dgvPebsBatches.RowHeadersVisible = false;
            this.dgvPebsBatches.RowHeadersWidth = 4;
            this.dgvPebsBatches.Size = new System.Drawing.Size(778, 360);
            this.dgvPebsBatches.TabIndex = 7;
            this.dgvPebsBatches.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPebsBatches_CellContentClick);
            // 
            // cbxLA
            // 
            this.cbxLA.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbxLA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxLA.FormattingEnabled = true;
            this.cbxLA.Location = new System.Drawing.Point(33, 14);
            this.cbxLA.Name = "cbxLA";
            this.cbxLA.Size = new System.Drawing.Size(55, 21);
            this.cbxLA.TabIndex = 8;
            // 
            // cbxDocType
            // 
            this.cbxDocType.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbxDocType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxDocType.FormattingEnabled = true;
            this.cbxDocType.Location = new System.Drawing.Point(224, 14);
            this.cbxDocType.Name = "cbxDocType";
            this.cbxDocType.Size = new System.Drawing.Size(86, 21);
            this.cbxDocType.TabIndex = 9;
            // 
            // cbxStatus
            // 
            this.cbxStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cbxStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxStatus.FormattingEnabled = true;
            this.cbxStatus.Items.AddRange(new object[] {
            "Issued",
            "Printed"});
            this.cbxStatus.Location = new System.Drawing.Point(396, 14);
            this.cbxStatus.Name = "cbxStatus";
            this.cbxStatus.Size = new System.Drawing.Size(71, 21);
            this.cbxStatus.TabIndex = 11;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSearch.Location = new System.Drawing.Point(662, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(110, 44);
            this.btnSearch.TabIndex = 10;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // labBatchNo
            // 
            this.labBatchNo.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labBatchNo.AutoSize = true;
            this.labBatchNo.Location = new System.Drawing.Point(485, 12);
            this.labBatchNo.Name = "labBatchNo";
            this.labBatchNo.Size = new System.Drawing.Size(59, 26);
            this.labBatchNo.TabIndex = 12;
            this.labBatchNo.Text = "Search for Batch:";
            // 
            // txtBatchNo
            // 
            this.txtBatchNo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtBatchNo.Location = new System.Drawing.Point(550, 15);
            this.txtBatchNo.Name = "txtBatchNo";
            this.txtBatchNo.Size = new System.Drawing.Size(104, 20);
            this.txtBatchNo.TabIndex = 13;
            // 
            // btnSetPosted
            // 
            this.btnSetPosted.Location = new System.Drawing.Point(224, 419);
            this.btnSetPosted.Name = "btnSetPosted";
            this.btnSetPosted.Size = new System.Drawing.Size(86, 42);
            this.btnSetPosted.TabIndex = 15;
            this.btnSetPosted.Text = "Set as Posted";
            this.btnSetPosted.UseVisualStyleBackColor = true;
            this.btnSetPosted.Click += new System.EventHandler(this.btnSetPosted_Click);
            // 
            // dateTimePicker1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.dateTimePicker1, 3);
            this.dateTimePicker1.CustomFormat = "";
            this.dateTimePicker1.Location = new System.Drawing.Point(3, 419);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(215, 20);
            this.dateTimePicker1.TabIndex = 14;
            // 
            // Selection
            // 
            this.Selection.FillWeight = 25.38071F;
            this.Selection.HeaderText = "";
            this.Selection.Name = "Selection";
            // 
            // BatchId
            // 
            this.BatchId.DataPropertyName = "BatchId";
            this.BatchId.HeaderText = "BatchId";
            this.BatchId.Name = "BatchId";
            this.BatchId.Visible = false;
            // 
            // Path
            // 
            this.Path.DataPropertyName = "Path";
            this.Path.HeaderText = "Path";
            this.Path.Name = "Path";
            this.Path.ReadOnly = true;
            this.Path.Visible = false;
            // 
            // BatchNo
            // 
            this.BatchNo.DataPropertyName = "BatchCode";
            this.BatchNo.HeaderText = "Batch No";
            this.BatchNo.Name = "BatchNo";
            // 
            // DocumentType
            // 
            this.DocumentType.DataPropertyName = "DocumentType";
            this.DocumentType.HeaderText = "Document Type";
            this.DocumentType.Name = "DocumentType";
            // 
            // NoOfDocuments
            // 
            this.NoOfDocuments.DataPropertyName = "DocumentCount";
            this.NoOfDocuments.HeaderText = "No. Of Documents";
            this.NoOfDocuments.Name = "NoOfDocuments";
            // 
            // DateIssued
            // 
            this.DateIssued.DataPropertyName = "DateIssued";
            this.DateIssued.HeaderText = "Date Issued";
            this.DateIssued.Name = "DateIssued";
            // 
            // DatePrinted
            // 
            this.DatePrinted.DataPropertyName = "PrintedTime";
            this.DatePrinted.HeaderText = "Date Printed";
            this.DatePrinted.Name = "DatePrinted";
            // 
            // Print
            // 
            this.Print.HeaderText = "Print";
            this.Print.Name = "Print";
            this.Print.Text = "Print";
            this.Print.ToolTipText = "Print";
            this.Print.UseColumnTextForButtonValue = true;
            // 
            // PrintBatches
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 464);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "PrintBatches";
            this.Text = "Print Batches";
            this.Load += new System.EventHandler(this.PrintBatches_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPebsBatches)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labLA;
        private System.Windows.Forms.DataGridView dgvPebsBatches;
        private System.Windows.Forms.Label labStatus;
        private System.Windows.Forms.Label labDocType;
        private System.Windows.Forms.ComboBox cbxLA;
        private System.Windows.Forms.ComboBox cbxDocType;
        private System.Windows.Forms.ComboBox cbxStatus;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label labBatchNo;
        private System.Windows.Forms.TextBox txtBatchNo;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button btnSetPosted;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Selection;
        private System.Windows.Forms.DataGridViewTextBoxColumn BatchId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Path;
        private System.Windows.Forms.DataGridViewTextBoxColumn BatchNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocumentType;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOfDocuments;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateIssued;
        private System.Windows.Forms.DataGridViewTextBoxColumn DatePrinted;
        private System.Windows.Forms.DataGridViewButtonColumn Print;
    }
}