﻿namespace SIL.AARTO.PrintEngine.PrintFactoryConfiguration.Admin
{
    partial class RemoteSiteRegistrations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvSiteRegistrations = new System.Windows.Forms.DataGridView();
            this.labAutCode = new System.Windows.Forms.Label();
            this.labWebServiceUrl = new System.Windows.Forms.Label();
            this.txtAutCode = new System.Windows.Forms.TextBox();
            this.txtWebServiceUrl = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.AutCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WebServiceUrl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Delete = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSiteRegistrations)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvSiteRegistrations
            // 
            this.dgvSiteRegistrations.AllowUserToAddRows = false;
            this.dgvSiteRegistrations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSiteRegistrations.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AutCode,
            this.WebServiceUrl,
            this.Edit,
            this.Delete});
            this.dgvSiteRegistrations.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvSiteRegistrations.Location = new System.Drawing.Point(0, 0);
            this.dgvSiteRegistrations.Name = "dgvSiteRegistrations";
            this.dgvSiteRegistrations.ReadOnly = true;
            this.dgvSiteRegistrations.Size = new System.Drawing.Size(684, 286);
            this.dgvSiteRegistrations.TabIndex = 0;
            this.dgvSiteRegistrations.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSiteRegistrations_CellContentClick);
            // 
            // labAutCode
            // 
            this.labAutCode.AutoSize = true;
            this.labAutCode.Location = new System.Drawing.Point(77, 333);
            this.labAutCode.Name = "labAutCode";
            this.labAutCode.Size = new System.Drawing.Size(54, 13);
            this.labAutCode.TabIndex = 1;
            this.labAutCode.Text = "Aut Code:";
            // 
            // labWebServiceUrl
            // 
            this.labWebServiceUrl.AutoSize = true;
            this.labWebServiceUrl.Location = new System.Drawing.Point(43, 359);
            this.labWebServiceUrl.Name = "labWebServiceUrl";
            this.labWebServiceUrl.Size = new System.Drawing.Size(88, 13);
            this.labWebServiceUrl.TabIndex = 2;
            this.labWebServiceUrl.Text = "Web Service Url:";
            // 
            // txtAutCode
            // 
            this.txtAutCode.Location = new System.Drawing.Point(138, 325);
            this.txtAutCode.Name = "txtAutCode";
            this.txtAutCode.Size = new System.Drawing.Size(100, 20);
            this.txtAutCode.TabIndex = 2;
            // 
            // txtWebServiceUrl
            // 
            this.txtWebServiceUrl.Location = new System.Drawing.Point(138, 359);
            this.txtWebServiceUrl.Name = "txtWebServiceUrl";
            this.txtWebServiceUrl.Size = new System.Drawing.Size(198, 20);
            this.txtWebServiceUrl.TabIndex = 3;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(138, 405);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(259, 405);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save Change";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // AutCode
            // 
            this.AutCode.DataPropertyName = "AutCode";
            this.AutCode.HeaderText = "AutCode";
            this.AutCode.Name = "AutCode";
            this.AutCode.ReadOnly = true;
            // 
            // WebServiceUrl
            // 
            this.WebServiceUrl.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.WebServiceUrl.DataPropertyName = "WebServiceUrl";
            this.WebServiceUrl.HeaderText = "WebServiceUrl";
            this.WebServiceUrl.Name = "WebServiceUrl";
            this.WebServiceUrl.ReadOnly = true;
            // 
            // Edit
            // 
            this.Edit.HeaderText = "Edit";
            this.Edit.Name = "Edit";
            this.Edit.ReadOnly = true;
            this.Edit.Text = "Edit";
            this.Edit.ToolTipText = "Edit";
            this.Edit.UseColumnTextForButtonValue = true;
            // 
            // Delete
            // 
            this.Delete.HeaderText = "Delete";
            this.Delete.Name = "Delete";
            this.Delete.ReadOnly = true;
            this.Delete.Text = "Delete";
            this.Delete.UseColumnTextForButtonValue = true;
            // 
            // RemoteSiteRegistrations
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 464);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtWebServiceUrl);
            this.Controls.Add(this.txtAutCode);
            this.Controls.Add(this.labWebServiceUrl);
            this.Controls.Add(this.labAutCode);
            this.Controls.Add(this.dgvSiteRegistrations);
            this.Name = "RemoteSiteRegistrations";
            this.Text = "Admin - Remote Site Registrations";
            this.Load += new System.EventHandler(this.RemoteSiteRegistrations_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSiteRegistrations)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvSiteRegistrations;
        private System.Windows.Forms.Label labAutCode;
        private System.Windows.Forms.Label labWebServiceUrl;
        private System.Windows.Forms.TextBox txtAutCode;
        private System.Windows.Forms.TextBox txtWebServiceUrl;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn AutCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn WebServiceUrl;
        private System.Windows.Forms.DataGridViewButtonColumn Edit;
        private System.Windows.Forms.DataGridViewButtonColumn Delete;
    }
}