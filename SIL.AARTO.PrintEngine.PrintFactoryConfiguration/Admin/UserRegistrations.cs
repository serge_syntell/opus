﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.PrintEngine.PrintFactoryConfiguration.Admin
{
    public partial class UserRegistrations : Form
    {
        public UserRegistrations()
        {
            InitializeComponent();
        }

        private PrintFactoryMemberShip Membership;
        private PrintFactoryUser CurrentUser;

        private void UserRegistrations_Load(object sender, EventArgs e)
        {            
            RetrieveData();

            cbxRole.DataSource = System.Enum.GetNames(typeof(UserRole));
            cbxRole.SelectedIndex = cbxRole.FindString(UserRole.User.ToString());
        }

        private void RetrieveData()
        {
            Membership = PrintFactoryMemberShip.Instance();
            dgvUserlist.AutoGenerateColumns = false;
            dgvUserlist.DataSource = Membership.Users;  
        }

        private void dgvUserlist_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 2:
                    // edit     
                    CurrentUser = Membership.Users[e.RowIndex];
                    txtUserName.Text = CurrentUser.UserName;
                    //cbxRole.SelectedIndex = cbxRole.Items.IndexOf(AllUsers[e.RowIndex].Role.ToString());
                    cbxRole.SelectedIndex = cbxRole.FindString(CurrentUser.Role.ToString());
                    txtPwd.Enabled = false;
                    btnSave.Enabled = true;
                    break;
                case 3:
                    // delete
                    CurrentUser = Membership.Users[e.RowIndex];
                    Membership.Users.Remove(CurrentUser);                    
                    if (PrintFactoryMemberShip.Save(Membership))
                    {
                        dgvUserlist.DataSource = null;
                        dgvUserlist.DataSource = Membership.Users;
                        //dgvUserlist.Refresh();
                    }
                    else
                    {
                        MessageBox.Show("Save UserRegistrations failed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;
                default:
                    break;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (ValidateInput() == false)
            {                
                return;
            }

            if (string.IsNullOrEmpty(txtPwd.Text.Trim()))
            {
                MessageBox.Show("Password is required.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return; 
            }

            // validate duplicate user name
            PrintFactoryUser user = Membership.FindByUserLoginName(txtUserName.Text.Trim());
            if (user != null)
            {
                MessageBox.Show("User Name exist! Do not allow duplicate user name!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return; 
            }

            CurrentUser = new PrintFactoryUser();
            CurrentUser.UserName = txtUserName.Text.Trim();
            CurrentUser.Password = Encrypt.HashPassword(txtPwd.Text.Trim());
            CurrentUser.Role = (UserRole)Enum.Parse(typeof(UserRole), cbxRole.SelectedItem.ToString(), true);

            Membership.Users.Add(CurrentUser);
            if (PrintFactoryMemberShip.Save(Membership))
            {
                dgvUserlist.DataSource = null;
                dgvUserlist.DataSource = Membership.Users;
                //dgvUserlist.Refresh();
                ResetInput();
            }
            else
            {
                MessageBox.Show("Save UserRegistrations failed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool ValidateInput()
        {
            if (string.IsNullOrEmpty(txtUserName.Text.Trim()))
            {
                MessageBox.Show("User name is required.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false; 
            }

            if (string.IsNullOrEmpty(cbxRole.SelectedItem.ToString()))
            {
                MessageBox.Show("User Role is required.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void ResetInput()
        {
            txtUserName.Text = string.Empty;
            txtPwd.Text = string.Empty;
            cbxRole.SelectedIndex = cbxRole.FindString(UserRole.User.ToString());

            txtPwd.Enabled = true;
            btnSave.Enabled = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateInput())
            {
                CurrentUser.UserName = txtUserName.Text.Trim();
                CurrentUser.Role = (UserRole)Enum.Parse(typeof(UserRole), cbxRole.SelectedItem.ToString(), true);
                if (PrintFactoryMemberShip.Save(Membership))
                {
                    dgvUserlist.DataSource = Membership.Users;
                    dgvUserlist.Refresh();

                    ResetInput();
                }
                else
                {
                    MessageBox.Show("Save UserRegistrations failed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }            
        }       
    }
}
