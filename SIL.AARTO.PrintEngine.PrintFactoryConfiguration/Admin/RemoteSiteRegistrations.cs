﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using SIL.AARTO.BLL.Model;
using System.Configuration;

namespace SIL.AARTO.PrintEngine.PrintFactoryConfiguration.Admin
{
    public partial class RemoteSiteRegistrations : Form
    {
        public RemoteSiteRegistrations()
        {
            InitializeComponent();
        }

        private RemoteSiteWebServices Services;
        private RemoteSiteWebService CurrentService;

        public static string RemoteSiteWebServicesPath = "Configuration/RemoteSiteRegistrations.xml";


        private void RemoteSiteRegistrations_Load(object sender, EventArgs e)
        {
            Services = RemoteSiteWebServices.Instance(RemoteSiteWebServicesPath);            
            dgvSiteRegistrations.AutoGenerateColumns = false;
            dgvSiteRegistrations.DataSource = Services.WebServices;
        }

        private bool ValidateInput()
        {
            if (string.IsNullOrEmpty(txtAutCode.Text.Trim()))
            {
                MessageBox.Show("Aut Code is required.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtWebServiceUrl.Text.Trim()))
            {
                MessageBox.Show("Web Service Url is required.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (ValidateInput() == false)
            {
                return;
            }

            CurrentService = new RemoteSiteWebService();
            CurrentService.AutCode = txtAutCode.Text.Trim();
            CurrentService.WebServiceUrl = txtWebServiceUrl.Text.Trim();
            Services.WebServices.Add(CurrentService);

            if (RemoteSiteWebServices.Save(Services, RemoteSiteWebServicesPath))
            {
                dgvSiteRegistrations.DataSource = null;
                dgvSiteRegistrations.DataSource = Services.WebServices;

                ResetInput();
            }
            else
            {
                MessageBox.Show("Save RemoteSiteWebServices failed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private void dgvSiteRegistrations_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 2:
                    // edit     
                    CurrentService = Services.WebServices[e.RowIndex];
                    txtAutCode.Text = CurrentService.AutCode;
                    txtWebServiceUrl.Text = CurrentService.WebServiceUrl;
                    btnSave.Enabled = true;
                    break;
                case 3:
                    // delete
                    CurrentService = Services.WebServices[e.RowIndex];
                    Services.WebServices.Remove(CurrentService);
                    if (RemoteSiteWebServices.Save(Services, RemoteSiteWebServicesPath))
                    {
                        dgvSiteRegistrations.DataSource = null;
                        dgvSiteRegistrations.DataSource = Services.WebServices;
                    }
                    else
                    {
                        MessageBox.Show("Save RemoteSiteWebServices failed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    } 
                    break;
                default:
                    break;
            }
        }

        private void ResetInput()
        {
            txtAutCode.Text = string.Empty;
            txtWebServiceUrl.Text = string.Empty;
            btnSave.Enabled = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateInput())
            {
                CurrentService.AutCode = txtAutCode.Text.Trim();
                CurrentService.WebServiceUrl = txtWebServiceUrl.Text.Trim();

                if (RemoteSiteWebServices.Save(Services, RemoteSiteWebServicesPath))
                {                    
                    dgvSiteRegistrations.DataSource = Services.WebServices;
                    dgvSiteRegistrations.Refresh();

                    ResetInput();
                }
                else
                {
                    MessageBox.Show("Save RemoteSiteWebServices failed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                } 
            }        
        }
    }
}
