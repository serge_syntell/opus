﻿namespace SIL.AARTO.PrintEngine.PrintFactoryConfiguration.Admin
{
    partial class UserRegistrations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvUserlist = new System.Windows.Forms.DataGridView();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.labUserName = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtPwd = new System.Windows.Forms.TextBox();
            this.labPwd = new System.Windows.Forms.Label();
            this.labRole = new System.Windows.Forms.Label();
            this.cbxRole = new System.Windows.Forms.ComboBox();
            this.labNotice = new System.Windows.Forms.Label();
            this.printFactoryMemberShipBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.UserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Role = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Delete = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserlist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printFactoryMemberShipBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvUserlist
            // 
            this.dgvUserlist.AllowUserToAddRows = false;
            this.dgvUserlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUserlist.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UserName,
            this.Role,
            this.Edit,
            this.Delete});
            this.dgvUserlist.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvUserlist.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvUserlist.Location = new System.Drawing.Point(0, 0);
            this.dgvUserlist.Name = "dgvUserlist";
            this.dgvUserlist.ReadOnly = true;
            this.dgvUserlist.Size = new System.Drawing.Size(580, 325);
            this.dgvUserlist.TabIndex = 0;
            this.dgvUserlist.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUserlist_CellContentClick);
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(178, 460);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(84, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save Change";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(51, 460);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(93, 23);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Add New User";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // labUserName
            // 
            this.labUserName.AutoSize = true;
            this.labUserName.Location = new System.Drawing.Point(48, 368);
            this.labUserName.Name = "labUserName";
            this.labUserName.Size = new System.Drawing.Size(63, 13);
            this.labUserName.TabIndex = 3;
            this.labUserName.Text = "User Name:";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(117, 365);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(100, 20);
            this.txtUserName.TabIndex = 4;
            // 
            // txtPwd
            // 
            this.txtPwd.Location = new System.Drawing.Point(117, 391);
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.PasswordChar = '*';
            this.txtPwd.Size = new System.Drawing.Size(100, 20);
            this.txtPwd.TabIndex = 5;
            this.txtPwd.UseSystemPasswordChar = true;
            // 
            // labPwd
            // 
            this.labPwd.AutoSize = true;
            this.labPwd.Location = new System.Drawing.Point(48, 394);
            this.labPwd.Name = "labPwd";
            this.labPwd.Size = new System.Drawing.Size(56, 13);
            this.labPwd.TabIndex = 3;
            this.labPwd.Text = "Password:";
            // 
            // labRole
            // 
            this.labRole.AutoSize = true;
            this.labRole.Location = new System.Drawing.Point(48, 422);
            this.labRole.Name = "labRole";
            this.labRole.Size = new System.Drawing.Size(57, 13);
            this.labRole.TabIndex = 5;
            this.labRole.Text = "User Role:";
            // 
            // cbxRole
            // 
            this.cbxRole.FormattingEnabled = true;
            this.cbxRole.Location = new System.Drawing.Point(117, 419);
            this.cbxRole.Name = "cbxRole";
            this.cbxRole.Size = new System.Drawing.Size(121, 21);
            this.cbxRole.TabIndex = 6;
            // 
            // labNotice
            // 
            this.labNotice.AutoSize = true;
            this.labNotice.Location = new System.Drawing.Point(223, 394);
            this.labNotice.Name = "labNotice";
            this.labNotice.Size = new System.Drawing.Size(317, 13);
            this.labNotice.TabIndex = 7;
            this.labNotice.Text = "Notice: Do not change password here, only use for add new user.";
            // 
            // printFactoryMemberShipBindingSource
            // 
            this.printFactoryMemberShipBindingSource.DataSource = typeof(SIL.AARTO.PrintEngine.PrintFactoryConfiguration.PrintFactoryMemberShip);
            // 
            // UserName
            // 
            this.UserName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.UserName.DataPropertyName = "UserName";
            this.UserName.HeaderText = "UserName";
            this.UserName.Name = "UserName";
            this.UserName.ReadOnly = true;
            // 
            // Role
            // 
            this.Role.DataPropertyName = "Role";
            this.Role.HeaderText = "Role";
            this.Role.Name = "Role";
            this.Role.ReadOnly = true;
            // 
            // Edit
            // 
            this.Edit.HeaderText = "Edit";
            this.Edit.Name = "Edit";
            this.Edit.ReadOnly = true;
            this.Edit.Text = "Edit";
            this.Edit.ToolTipText = "Edit";
            this.Edit.UseColumnTextForButtonValue = true;
            // 
            // Delete
            // 
            this.Delete.HeaderText = "Delete";
            this.Delete.Name = "Delete";
            this.Delete.ReadOnly = true;
            this.Delete.Text = "Delete";
            this.Delete.ToolTipText = "Delete";
            this.Delete.UseColumnTextForButtonValue = true;
            // 
            // UserRegistrations
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 576);
            this.Controls.Add(this.labNotice);
            this.Controls.Add(this.cbxRole);
            this.Controls.Add(this.labRole);
            this.Controls.Add(this.txtPwd);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.labPwd);
            this.Controls.Add(this.labUserName);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dgvUserlist);
            this.Name = "UserRegistrations";
            this.Text = "Admin - User Registrations";
            this.Load += new System.EventHandler(this.UserRegistrations_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserlist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printFactoryMemberShipBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvUserlist;
        private System.Windows.Forms.BindingSource printFactoryMemberShipBindingSource;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label labUserName;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtPwd;
        private System.Windows.Forms.Label labPwd;
        private System.Windows.Forms.Label labRole;
        private System.Windows.Forms.ComboBox cbxRole;
        private System.Windows.Forms.Label labNotice;
        private System.Windows.Forms.DataGridViewTextBoxColumn UserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Role;
        private System.Windows.Forms.DataGridViewButtonColumn Edit;
        private System.Windows.Forms.DataGridViewButtonColumn Delete;
    }
}