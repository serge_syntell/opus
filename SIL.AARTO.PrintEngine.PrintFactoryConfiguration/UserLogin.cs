﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.PrintEngine.PrintFactoryConfiguration
{
    public partial class UserLogin : Form
    {
        public UserLogin()
        {
            InitializeComponent();
        }

        public enum LoginResult
        {
            Succeeded = 0, IsLocked = 1, ErrorUserName = 2, ErrorPassword = 3, Exception = 4
        }

        public PrintFactoryUser Login(string userName, string pwd, out LoginResult loginResult)
        {            
            if (PrintFactoryMemberShip.Instance().ValidateUser(userName, pwd))
            {
                loginResult = LoginResult.Succeeded;
                return PrintFactoryMemberShip.GetByUserLoginName(userName);
            }
            loginResult = LoginResult.ErrorPassword;
            return null;
        }       

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUserName.Text.Trim()))
            {
                MessageBox.Show("User name is required.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (string.IsNullOrEmpty(txtPwd.Text.Trim()))
            {
                MessageBox.Show("Password is required.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            LoginResult loginResult;
            GlobalVariates<PrintFactoryUser>.CurrentUser = Login(txtUserName.Text.Trim(), txtPwd.Text.Trim(), out loginResult);
            switch (loginResult)
            {
                case LoginResult.ErrorUserName:
                    MessageBox.Show("Sorry, your user name does not exist.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                case LoginResult.ErrorPassword:
                    MessageBox.Show("Sorry, your password is invalid.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                case LoginResult.IsLocked:
                    MessageBox.Show("Sorry, your user name is locked. Please contact administrator. ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                case LoginResult.Exception:
                    MessageBox.Show("Sorry, cannot connect to the database server.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                case LoginResult.Succeeded:
                    break;
            }
            this.Hide();
            frmMain mainForm = new frmMain();
            mainForm.Show();
        }

       

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
