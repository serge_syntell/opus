﻿namespace SIL.AARTO.PrintEngine.PrintFactoryConfiguration
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printBatchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remoteSiteLARegistrationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userRegistrationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslLastUpdated = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.searchToolStripMenuItem,
            this.adminToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.printBatchToolStripMenuItem});
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.searchToolStripMenuItem.Text = "Search";
            // 
            // printBatchToolStripMenuItem
            // 
            this.printBatchToolStripMenuItem.Name = "printBatchToolStripMenuItem";
            this.printBatchToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F5)));
            this.printBatchToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.printBatchToolStripMenuItem.Text = "Print Batch";
            this.printBatchToolStripMenuItem.Click += new System.EventHandler(this.printBatchToolStripMenuItem_Click);
            // 
            // adminToolStripMenuItem
            // 
            this.adminToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.remoteSiteLARegistrationsToolStripMenuItem,
            this.userRegistrationsToolStripMenuItem});
            this.adminToolStripMenuItem.Name = "adminToolStripMenuItem";
            this.adminToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.adminToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.adminToolStripMenuItem.Text = "Admin";
            // 
            // remoteSiteLARegistrationsToolStripMenuItem
            // 
            this.remoteSiteLARegistrationsToolStripMenuItem.Name = "remoteSiteLARegistrationsToolStripMenuItem";
            this.remoteSiteLARegistrationsToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.remoteSiteLARegistrationsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F11)));
            this.remoteSiteLARegistrationsToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.remoteSiteLARegistrationsToolStripMenuItem.Text = "Remote Site (LA) Registrations";
            this.remoteSiteLARegistrationsToolStripMenuItem.Click += new System.EventHandler(this.remoteSiteLARegistrationsToolStripMenuItem_Click);
            // 
            // userRegistrationsToolStripMenuItem
            // 
            this.userRegistrationsToolStripMenuItem.Name = "userRegistrationsToolStripMenuItem";
            this.userRegistrationsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F12)));
            this.userRegistrationsToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.userRegistrationsToolStripMenuItem.Text = "User Registrations";
            this.userRegistrationsToolStripMenuItem.Click += new System.EventHandler(this.userRegistrationsToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslLastUpdated});
            this.statusStrip1.Location = new System.Drawing.Point(0, 542);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(784, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslLastUpdated
            // 
            this.tslLastUpdated.Name = "tslLastUpdated";
            this.tslLastUpdated.Size = new System.Drawing.Size(118, 17);
            this.tslLastUpdated.Text = "toolStripStatusLabel1";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 564);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.Text = "Print Factory Configuration";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adminToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem remoteSiteLARegistrationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userRegistrationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printBatchToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslLastUpdated;
    }
}

