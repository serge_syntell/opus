﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Xml.Serialization;


namespace SIL.AARTO.AARTOAuthorize
{

    /// <summary>
    /// Controller Authorization Info Collection
    /// </summary>
    [XmlRoot("controllers")]
    public class ControllerAuthorizationInfoCollection : List<ControllerAuthorizationInfo> {

        /// <summary>
        /// Determines whether user can access specific controller.
        /// </summary>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="user">The user.</param>
        /// <returns>
        /// 	<c>true</c> if this instance [can access controller] the specified controller name; otherwise, <c>false</c>.
        /// </returns>
        public bool CanAccessController(string controllerName, System.Security.Principal.IPrincipal user) {
            ControllerAuthorizationInfo controllerInfo = findController(controllerName);

            if (controllerInfo != null && controllerInfo.IsInAnyRoles(user) == false) {
                return false;
            }

            return true;
        }

         


        /// <summary>
        /// Determines whether user can access specific controller and action.
        /// </summary>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="actionName">Name of the action.</param>
        /// <param name="user">The user.</param>
        /// <returns>
        /// 	<c>true</c> if this instance [can access action] the specified controller name; otherwise, <c>false</c>.
        /// </returns>
        public bool CanAccessAction(string controllerName, string actionName, System.Security.Principal.IPrincipal user) {
            ControllerAuthorizationInfo controllerInfo = findController(controllerName);
            ActionAuthorizationInfo actionInfo = null;

            if (controllerInfo != null) {

                // if user can not access controller then he should not have access to action.
                if (controllerInfo.IsInAnyRoles(user) == false) {
                    return false;
                }
                
                
                actionInfo = controllerInfo.FindAction(actionName);

                if (actionInfo != null && actionInfo.IsInAnyRoles(user) == false) {
                    return false;
                }
            }

            return true;
        }


        public ControllerAuthorizationInfo findController(string controllerName) {
            
            foreach (ControllerAuthorizationInfo info in this) {
                if (string.Compare(info.Name, controllerName,true) == 0) {
                    return info;
                }
            }

            return null;
        }
    }

}
