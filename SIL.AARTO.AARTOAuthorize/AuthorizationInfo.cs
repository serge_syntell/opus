﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SIL.AARTO.AARTOAuthorize
{


    /// <summary>
    /// Authorization Info
    /// </summary>
    public abstract class AuthorizationInfo {
        private string name;

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [XmlAttribute("name")]
        public string Name {
            get { return name; }
            set { name = value; }
        }


        private string[] roles;

        /// <summary>
        /// Gets or sets the roles.
        /// </summary>
        /// <value>The roles.</value>
        [XmlArray(ElementName = "roles")]
        [XmlArrayItem(ElementName = "role")]
        public string[] Roles {
            get { return roles; }
            set { roles = value; }
        }


        /// <summary>
        /// Determines whether [is in any roles] [the specified user].
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>
        /// 	<c>true</c> if [is in any roles] [the specified user]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsInAnyRoles(System.Security.Principal.IPrincipal user) {
            if (user == null)
                return false;

            // no-roles is same as free free everyone..
            if (roles == null || roles.Length == 0)
            {
                return true;
            }
            
            foreach (string role in roles) {
                if (user.IsInRole(role)) {
                    return true;
                }
            }

            return false;
        }

    }
}
