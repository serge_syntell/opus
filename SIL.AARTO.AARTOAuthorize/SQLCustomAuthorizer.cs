﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.AARTOAuthorize
{
    public class MVCSQLCustomAuthorizer : ICustomAuthorizer
    {
        static AartoControllerService controllerService = new AartoControllerService();
        static AartoActionService actionService = new AartoActionService();
        ControllerAuthorizationInfoCollection controllers = null;

        #region ICustomAuthorizer Members

        public bool IsAuthorized(string controllerName, string actionName, System.Security.Principal.IPrincipal user)
        {
            return controllers.CanAccessAction(controllerName, actionName, user);
        }

        public void Initilize(string connectionString)
        {
            string key = "MCVXMLAuthorizerCacheKey";

            if (HttpContext.Current.Cache[key] != null)
            {
                controllers = (ControllerAuthorizationInfoCollection)HttpContext.Current.Cache[key];

            }
            else
            {
                string path = HttpContext.Current.Server.MapPath(connectionString);

                controllers = getControllerAuthorizationInfoCollection();
                //HttpContext.Current.Cache.Insert(key, controllers, new System.Web.Caching.CacheDependency(path));

            }
        }

        #endregion

        private ControllerAuthorizationInfoCollection getControllerAuthorizationInfoCollection()
        {
            ControllerAuthorizationInfoCollection rVal = new ControllerAuthorizationInfoCollection();

            ControllerAuthorizationInfo controllerAuthorizationInfo = null;

            DataTable dtController = new DataTable();
            dtController.Columns.AddRange(
                    new DataColumn[]{
                        new DataColumn("AAConID",typeof(Int32)),
                        new DataColumn("AaActID",typeof(Int32)),
                        new DataColumn("AaUserRoleID"),
                        new DataColumn("AaConName"),
                        new DataColumn("AaActName"),
                        new DataColumn("AaUserRoleName"),
                    }
                );

            DataRow dRow = null;
            //AartoController controller 
            try
            {
                using (IDataReader reader = controllerService.GetControllerJoinActionAndRoles())
                {

                    while (reader.Read())
                    {
                        dRow = dtController.NewRow();
                        dRow["AAConID"] = reader["AAConID"];
                        dRow["AaActID"] = reader["AaActID"];
                        dRow["AaUserRoleID"] = reader["AaUserRoleID"];
                        dRow["AaConName"] = reader["AaConName"];
                        dRow["AaActName"] = reader["AaActName"];
                        dRow["AaUserRoleName"] = reader["AaUserRoleName"];

                        dtController.Rows.Add(dRow);
                    }
                }


                List<string> tempRolesList = new List<string>();
                List<ActionAuthorizationInfo> tempAuthList = new List<ActionAuthorizationInfo>();

                foreach (AartoController controller in controllerService.GetAll())
                {
                    controllerAuthorizationInfo = new ControllerAuthorizationInfo();
                    controllerAuthorizationInfo.Name = controller.AaConName;

                    foreach (AartoAction action in actionService.GetByAaConId(controller.AaConId))
                    {
                        ActionAuthorizationInfo actionAuthorizationInfo = new ActionAuthorizationInfo();
                        actionAuthorizationInfo.Name = action.AaActName;
                        //foreach (DataRow ddr in dtController.Select(String.Format("AAConID={0} And AaActID={1}",
                        //foreach (DataRow ddr in dtController.Select(String.Format("AaActID={0}",
                        //     action.AaActId)))
                        foreach (DataRow ddr in dtController.Rows)
                        {
                            if (Convert.ToInt32(ddr["AaActID"]) == action.AaActId && Convert.ToInt32(ddr["AAConID"]) == action.AaConId)
                                tempRolesList.Add(ddr["AaUserRoleName"].ToString());

                        }

                        actionAuthorizationInfo.Roles = tempRolesList.ToArray();
                        tempRolesList.Clear();
                        tempAuthList.Add(actionAuthorizationInfo);

                    }

                    controllerAuthorizationInfo.Actions = tempAuthList.ToArray();
                    tempAuthList.Clear();
                    rVal.Add(controllerAuthorizationInfo);
                }
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteErrorLog(ex, LogCategory.Error, AartoProjectList.AARTOWebApplication.ToString());
            }

            return rVal;
        }



    }
}
