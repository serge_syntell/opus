﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Xml.Serialization;
using System.Xml;

namespace SIL.AARTO.AARTOAuthorize
{


    /// <summary>
    /// Authorization Mapping Section
    /// </summary>
    public class AuthorizationMappingSection : IConfigurationSectionHandler {


        #region IConfigurationSectionHandler Members

        /// <summary>
        /// Creates a configuration section handler.
        /// </summary>
        /// <param name="parent">Parent object.</param>
        /// <param name="configContext">Configuration context object.</param>
        /// <param name="section">Section XML node.</param>
        /// <returns>The created section handler object.</returns>
        public object Create(object parent, object configContext, System.Xml.XmlNode section) {
            // Create an instance of XmlSerializer based on the RewriterConfiguration type...
            XmlSerializer ser = new XmlSerializer(typeof(AuthorizationMappingSection));

            // Return the Deserialized object from the Web.config XML
            return ser.Deserialize(new XmlNodeReader(section));
        }

        #endregion

        /// <summary>
        /// Gets the settings.
        /// </summary>
        /// <returns></returns>
        public static AuthorizationMappingSection GetSettings() {
            if (HttpContext.Current.Cache["AuthorizationMappingSection"] == null) {

                AuthorizationMappingSection settings 
                    = (AuthorizationMappingSection)ConfigurationManager.GetSection("AuthorizationMappingSection");

                HttpContext.Current.Cache["AuthorizationMappingSection"] = settings;
            }

            return ((AuthorizationMappingSection)HttpContext.Current.Cache["AuthorizationMappingSection"]);

        }

        /// <summary>
        /// Gets or sets the type of authorization mapping.
        /// </summary>
        /// <value>The type.</value>
        [XmlAttribute("type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>The connection string.</value>
        [XmlAttribute("connectionStringName")]
        public string connectionStringName { get; set; }
    }
}
