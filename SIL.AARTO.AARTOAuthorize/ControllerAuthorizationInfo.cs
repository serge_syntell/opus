﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SIL.AARTO.AARTOAuthorize
{
    
    /// <summary>
    /// Controller Authorization Info
    /// </summary>
    [XmlType(TypeName = "controller")]
    public class ControllerAuthorizationInfo : AuthorizationInfo {

        private ActionAuthorizationInfo[] actions;

        /// <summary>
        /// Gets or sets the actions.
        /// </summary>
        /// <value>The actions.</value>
        [XmlArray(ElementName = "actions")]
        [XmlArrayItem(ElementName = "action")]
        public ActionAuthorizationInfo[] Actions {
            get { return actions; }
            set { actions = value; }
        }

        internal ActionAuthorizationInfo FindAction(string actionName) {
            if (actions != null)
            {
                foreach (ActionAuthorizationInfo info in actions)
                {
                    if (string.Compare(info.Name, actionName, true) == 0)
                    {
                        return info;
                    }
                }
            }

            return null;
        }
    }
}
