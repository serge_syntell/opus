﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System.Web;

namespace SIL.AARTO.AARTOAuthorize
{
    public class XMLCustomAuthorizer : ICustomAuthorizer
    {

        ControllerAuthorizationInfoCollection controllers = null;


        #region IMVCAuthorizer Members

        /// <summary>
        /// Determines whether the user is authorized to access specified controller .
        /// </summary>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="actionName">Name of the action.</param>
        /// <param name="user">The user.</param>
        /// <returns>
        /// 	<c>true</c> if the specified controller name is authorized; otherwise, <c>false</c>.
        /// </returns>
        public bool IsAuthorized(string controllerName, string actionName, System.Security.Principal.IPrincipal user)
        {
            return controllers.CanAccessAction(controllerName, actionName, user);
        }

        /// <summary>
        /// Initilizes the Authorizer using connection string.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public void Initilize(string connectionString)
        {
            string key = "MCVXMLAuthorizerCacheKey";

            if (HttpContext.Current.Cache[key] != null)
            {
                controllers = (ControllerAuthorizationInfoCollection)HttpContext.Current.Cache[key];

            }
            else
            {
                string path = HttpContext.Current.Server.MapPath(connectionString);

                controllers = getControllerAuthorizationInfoCollection(path);
                HttpContext.Current.Cache.Insert(key, controllers, new System.Web.Caching.CacheDependency(path));

            }
        }



        #endregion

        private ControllerAuthorizationInfoCollection getControllerAuthorizationInfoCollection(string path)
        {
            ControllerAuthorizationInfoCollection rVal = null;

            XmlSerializer ser = new XmlSerializer(typeof(ControllerAuthorizationInfoCollection));
            using (FileStream fs = File.OpenRead(path))
            {
                rVal = (ControllerAuthorizationInfoCollection)ser.Deserialize(fs);
            }

            return rVal;
        }
    }
}
