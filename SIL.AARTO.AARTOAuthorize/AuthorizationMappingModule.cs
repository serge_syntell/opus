﻿using System;
using System.Collections.Generic;
using System.Web.Routing;
using System.Web;
using System.Security;

namespace SIL.AARTO.AARTOAuthorize
{
    /// <summary>
    /// Authorization Mapping Modele
    /// </summary>
    public class AuthorizationMappingModule : IHttpModule
    {

        #region IHttpModule Members

        /// <summary>
        /// Disposes of the resources (other than memory) used by the module that implements <see cref="T:System.Web.IHttpModule"/>.
        /// </summary>
        public void Dispose()
        {

        }

        /// <summary>
        /// Inits the specified app.
        /// </summary>
        /// <param name="app">The app.</param>
        public void Init(HttpApplication app)
        {
            app.AuthorizeRequest += new EventHandler(OnAuthorizeRequest);
        }

        void OnAuthorizeRequest(object sender, EventArgs e)
        {
            HttpContext context = ((HttpApplication)sender).Context;

            RouteData routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(context));

            try
            {
                if (routeData != null)
                {
                    if (routeData.Values.ContainsKey("controller"))
                    {
                        string controller = routeData.GetRequiredString("controller");
                        string action = routeData.GetRequiredString("action");

                        ICustomAuthorizer authorizer = GetMVCAuthorizer();

                        if (!authorizer.IsAuthorized(controller, action, context.User))
                        {
                            string message = string.Format("User {0} does not have permission to access {1} on {2}"
                                , context.User.Identity.Name, action, controller);

                            //System.Diagnostics.Trace.TraceInformation(message);

                            //context.Response.Redirect(String.Format("~/Error/ApplicationError?ErrorMessage={0}", message));
                            throw new Exception(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        ICustomAuthorizer GetMVCAuthorizer()
        {
            string key = "IMCVAuthorizerCacheKey";
            ICustomAuthorizer rVal = null;

            if (HttpContext.Current.Cache[key] != null)
            {
                rVal = (ICustomAuthorizer)HttpContext.Current.Cache[key];

            }
            else
            {
                AuthorizationMappingSection settings = AuthorizationMappingSection.GetSettings();

                rVal = Activator.CreateInstance(Type.GetType(settings.Type)) as ICustomAuthorizer;
                rVal.Initilize(settings.connectionStringName);

                HttpContext.Current.Cache.Insert(key, rVal);

            }

            return rVal;

        }

        #endregion
    }
}
