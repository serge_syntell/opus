﻿using System;
using System.Collections.Generic;

namespace SIL.AARTO.AARTOAuthorize
{

    /// <summary>
    /// MVC Authorizer interface
    /// </summary>
    public interface ICustomAuthorizer
    {

        /// <summary>
        /// Determines whether the user is authorized to access specified controller .
        /// </summary>
        /// <param name="controllerName">Name of the controller.</param>
        /// <param name="actionName">Name of the action.</param>
        /// <param name="user">The user.</param>
        /// <returns>
        /// 	<c>true</c> if the specified controller name is authorized; otherwise, <c>false</c>.
        /// </returns>
        bool IsAuthorized(string controllerName, string actionName, System.Security.Principal.IPrincipal user);

        /// <summary>
        /// Initilizes the Authorizer using connection string.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        void Initilize(string connectionString);
    }
}
