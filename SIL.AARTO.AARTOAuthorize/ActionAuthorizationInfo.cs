﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SIL.AARTO.AARTOAuthorize
{
    /// <summary>
    /// Action Authorization Info
    /// </summary>

    public class ActionAuthorizationInfo : AuthorizationInfo {


    }

    public class AuthorizationInfoComparer : IEqualityComparer<AuthorizationInfo>
    {
        public bool Equals(AuthorizationInfo x, AuthorizationInfo y)
        {

            // Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            // Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            // Check whether the products' properties are equal.
            return x.Name == y.Name;
        }

        // If Equals() returns true for a pair of objects,
        // GetHashCode must return the same value for these objects.

        public int GetHashCode(AuthorizationInfo authorizationInfo)
        {
            // Check whether the object is null.
            if (Object.ReferenceEquals(authorizationInfo, null)) return 0;

            // Get the hash code for the Name field if it is not null.
            int hashProductName = authorizationInfo.Name == null ? 0 : authorizationInfo.Name.GetHashCode();


            // Calculate the hash code for the product.
            return hashProductName;
        }
    }

}
