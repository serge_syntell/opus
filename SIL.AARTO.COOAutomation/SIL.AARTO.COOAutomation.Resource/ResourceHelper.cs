﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace SIL.AARTO.COOAutomation.Resource
{
    public class ResourceHelper
    {
        static string language;
        static readonly Dictionary<string, string> resources = new Dictionary<string, string>();
        static readonly object syncObj = new object();

        public static string GetResource(string key, params object[] args)
        {
            return string.Format(GetResource(key), args);
        }

        public static string GetResource(string key)
        {
            lock (syncObj)
            {
                string result = string.Empty;
                try
                {
                    string fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, GetLanguage() + ".xml");
                    var xmldoc = new XmlDocument();
                    xmldoc.Load(fileName);
                    var resList = xmldoc.DocumentElement.ChildNodes;
                    if (resources.Count <= 0)
                    {
                        foreach (XmlNode element in resList)
                        {
                            if (element.NodeType != XmlNodeType.Element) continue;
                            string resKey = element.Attributes["key"].Value;
                            string resValue = element.InnerText.Replace(@"\r", "\r").Replace(@"\n", "\n");

                            if (!string.IsNullOrEmpty(resKey) && !string.IsNullOrEmpty(resValue))
                            {
                                if (resKey.Equals(key))
                                    result = resValue;

                                if (!resources.ContainsKey(resKey))
                                    resources.Add(resKey, resValue);
                            }
                        }
                    }
                }
                catch
                {
                    resources.Clear();
                }
                if (string.IsNullOrEmpty(result) && resources.Count > 0 && resources.Keys.Contains(key))
                    result = resources[key];

                return string.IsNullOrEmpty(result) ? key : result;
            }
        }

        static string GetLanguage()
        {
            if (string.IsNullOrEmpty(language))
            {
                //try
                //{
                //    if (ConfigurationManager.ConnectionStrings["SIL.ServiceQueueLibrary.DAL.Data.ConnectionString"] != null)
                //    {
                //        string connstr = ConfigurationManager.ConnectionStrings["SIL.ServiceQueueLibrary.DAL.Data.ConnectionString"].ConnectionString;
                //        if (!string.IsNullOrWhiteSpace(connstr))
                //        {
                //            var db = new ServiceDB(connstr);
                //            object obj = db.ExecuteScalar("SILCustom_ServiceHost_GetLanguage");
                //            if (obj != null)
                //                language = Convert.ToString(obj);
                //        }
                //    }
                //}
                //catch { }

                if (string.IsNullOrEmpty(language))
                    //language = System.Globalization.CultureInfo.CurrentCulture.ToString();
                    language = "en-us";
            }
            return language;
        }
    }
}
