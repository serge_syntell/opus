﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.COOAutomation.Utility
{
    public class Message
    {
        public string Text;
        public bool Status;

        public Message()
        {
            Status = true;
        }
    }
}
