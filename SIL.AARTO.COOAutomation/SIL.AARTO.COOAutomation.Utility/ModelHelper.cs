﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SIL.AARTO.COOAutomation.Utility
{
    public class ModelHelper
    {
        public static T InitializeValues<T>(T entity)
            where T : class, new()
        {
            if (entity == null)
                entity = new T();

            var mis = entity.GetType().GetMembers();
            foreach (var mi in mis)
            {
                if (mi.MemberType == MemberTypes.Property)
                {
                    var pi = (PropertyInfo)mi;

                    if (pi.PropertyType == typeof(string))
                        pi.SetValue(entity, string.Empty, null);
                }
                else if (mi.MemberType == MemberTypes.Field)
                {
                    var fi = (FieldInfo)mi;

                    if (fi.FieldType == typeof(string))
                        fi.SetValue(entity, string.Empty);
                }
            }

            return entity;
        }
    }
}
