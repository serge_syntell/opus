﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace SIL.AARTO.COOAutomation.Utility
{
    public class Formatter
    {
        public static string Serialize<T>(T obj, Encoding encoding = null)
            where T : class
        {
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            using (var ms = new MemoryStream())
            {
                new XmlSerializer(typeof(T)).Serialize(ms, obj, ns);
                return (encoding ?? Encoding.UTF8).GetString(ms.ToArray());
            }
        }

        public static T Deserialize<T>(string xml, Encoding encoding = null)
            where T : class
        {
            var bytes = (encoding ?? Encoding.UTF8).GetBytes(xml);
            using (var ms = new MemoryStream(bytes))
            {
                return new XmlSerializer(typeof(T)).Deserialize(ms) as T;
            }
        }

        public static XmlDocument SerializeXML<T>(T obj, Encoding encoding = null)
            where T : class
        {
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            using (var ms = new MemoryStream())
            {
                new XmlSerializer(typeof(T)).Serialize(ms, obj, ns);
                var xml= (encoding ?? Encoding.UTF8).GetString(ms.ToArray());
                var doc = new XmlDocument();
                doc.LoadXml(xml);
                return doc;
            }
        }
    }
}
