﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;

namespace SIL.AARTO.COOAutomation.Utility
{
    public class WCFSessionManager
    {
        WCFSessionManager() {}
        static WCFSessionManager instance;

        public static WCFSessionManager GetSingleton()
        {
            if (instance == null)
            {
                instance = new WCFSessionManager();
                if (!long.TryParse(ConfigurationManager.AppSettings["sessionTimeout"], out instance.timeout))
                    instance.timeout = 1200;
            }
            return instance;
        }

        readonly object lockObj = new object();
        readonly List<WCFSession> sessionList = new List<WCFSession>();
        long timeout;
        DateTime maxExpiredDate, minExpiredDate;
        string minExpiredID;
        string pattern = @"^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$";

        public WCFSession Current(string guid)
        {
            if (!IsGuid(guid)) return null;
            lock (this.lockObj)
            {
                Regain();
                var current = Find(guid);
                if (current != null)
                {
                    var now = DateTime.Now;
                    current.LastUpdatedDate = now;
                    SetExpiredDate(guid, now);
                }
                return current;
            }
        }

        public bool IsValid(string guid)
        {
            return Current(guid) != null;
        }

        public WCFSession New(string guid)
        {
            var current = new WCFSession(guid);
            if (!IsGuid(guid)) return current;
            var now = DateTime.Now;
            lock (this.lockObj)
            {
                current = Find(guid);
                if (current == null)
                    this.sessionList.Add(current = new WCFSession(guid));
                else
                    current.LastUpdatedDate = now;

                SetExpiredDate(guid, now);
            }
            return current;
        }

        //public WCFSession Update(string guid)
        //{
        //    var current = new WCFSession(guid);
        //    if (!IsGuid(guid)) return current;
        //    var now = DateTime.Now;
        //    lock (this.lockObj)
        //    {
        //        current = Find(guid);
        //        if (current != null)
        //        {
        //            current.LastUpdatedDate = now;

        //            SetExpiredDate(guid, now);
        //        }
        //    }
        //    return current;
        //}


        bool IsGuid(string guid)
        {
            return new Regex(pattern).IsMatch(guid);
        }

        WCFSession Find(string guid)
        {
            return this.sessionList.FirstOrDefault(p => p.ID.Equals(guid, StringComparison.OrdinalIgnoreCase));
        }

        void Regain()
        {
            var now = DateTime.Now;

            if (now <= this.minExpiredDate)
                return;

            this.minExpiredID = null;
            this.minExpiredDate = DateTime.MinValue;

            if (now >= this.maxExpiredDate)
            {
                this.sessionList.Clear();
                this.maxExpiredDate = DateTime.MinValue;
            }
            else
            {
                DateTime? newExpiredDate = null;
                for (var i = 0; i < this.sessionList.Count; i++)
                {
                    var s = this.sessionList[i];
                    var expiredDate = s.LastUpdatedDate.AddSeconds(this.timeout);
                    if (expiredDate <= now)
                    {
                        this.sessionList.Remove(s);
                        i--;
                    }
                    else if (!newExpiredDate.HasValue || expiredDate < newExpiredDate.Value)
                    {
                        newExpiredDate = expiredDate;
                        this.minExpiredID = s.ID;
                    }
                }
                if (newExpiredDate.HasValue)
                    this.minExpiredDate = newExpiredDate.Value;
            }
        }

        void SetExpiredDate(string guid, DateTime now)
        {
            var newExpiredDate = now.AddSeconds(this.timeout);
            if (string.IsNullOrWhiteSpace(this.minExpiredID) || newExpiredDate < this.minExpiredDate)
            {
                this.minExpiredID = guid;
                this.minExpiredDate = newExpiredDate;
            }
            else if (this.minExpiredID.Equals(guid, StringComparison.OrdinalIgnoreCase))
                this.minExpiredDate = newExpiredDate;

            if (newExpiredDate > this.maxExpiredDate)
                this.maxExpiredDate = newExpiredDate;
        }

    }

    public class WCFSession
    {
        public WCFSession(string id)
        {
            ID = id;
            LastUpdatedDate = DateTime.Now;
        }

        readonly Dictionary<string, object> content = new Dictionary<string, object>();

        public string ID { get; private set; }
        public DateTime LastUpdatedDate { get; set; }

        public object this[string key]
        {
            get { return this.content[key]; }
            set { this.content[key] = value; }
        }

        public void Add(string key, object value)
        {
            if (this.content.ContainsKey(key))
            {
                this.content[key] = value;               
            }
            else
            {
                this.content.Add(key, value);
            }
        }

        public void Clear()
        {
            this.content.Clear();
        }

        public bool Remove(string key)
        {
            return this.content.Remove(key);
        }

        public object GetValue(string key)
        {
            return this.content[key];
        }
    }

    #region

    //public class WCFSessionManager
    //{
    //    WCFSessionManager() {}
    //    static WCFSessionManager instance;

    //    readonly object lockObj = new object();
    //    readonly List<WCFSession> sessionList = new List<WCFSession>();
    //    long timeout;
    //    string prefix = "urn:uuid:";
    //    //string pattern = @"^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$";

    //    public OperationContext Current
    //    {
    //        get { return OperationContext.Current; }
    //    }

    //    public static WCFSessionManager GetSingleton()
    //    {
    //        if (instance == null)
    //        {
    //            instance = new WCFSessionManager();
    //            var timeout = ConfigurationManager.AppSettings["sessionTimeout"];
    //            if (!long.TryParse(timeout, out instance.timeout))
    //                instance.timeout = 60;
    //        }
    //        return instance;
    //    }

    //    public void Clear()
    //    {
    //        this.sessionList.Clear();
    //    }

    //    public bool IsValid()
    //    {
    //        Regain();
    //        return GetCurrent() != null;
    //    }

    //    public void Regain()
    //    {
    //        lock (this.lockObj)
    //        {
    //            var now = DateTime.Now;
    //            for (var i = 0; i < this.sessionList.Count; i++)
    //            {
    //                var s = this.sessionList[i];
    //                if (s.LastUpdatedDate.AddSeconds(s.ExpiredSeconds) > now)
    //                    this.sessionList.Remove(s);
    //            }
    //        }
    //    }

    //    public void Add()
    //    {
    //        var id = GetID();
    //        if (!string.IsNullOrWhiteSpace(id))
    //        {
    //            lock (this.lockObj)
    //            {
    //                var s = GetCurrent();
    //                if (s == null)
    //                    this.sessionList.Add(new WCFSession { ID = id, LastUpdatedDate = DateTime.Now, ExpiredSeconds = this.timeout });
    //                else
    //                    s.LastUpdatedDate = DateTime.Now;
    //            }
    //        }
    //    }

    //    public void Update()
    //    {
    //        Regain();
    //        lock (this.lockObj)
    //        {
    //            var s = GetCurrent();
    //            if (s != null)
    //                s.LastUpdatedDate = DateTime.Now;
    //        }
    //    }

    //    public string GetID()
    //    {
    //        var id = Current.SessionId;
    //        if (string.IsNullOrWhiteSpace(id))
    //            return id;
    //        if (id.StartsWith(this.prefix, StringComparison.OrdinalIgnoreCase))
    //            id = id.Remove(0, this.prefix.Length);
    //        //return new Regex(this.pattern).IsMatch(id) ? id : string.Empty;
    //        return id;
    //    }

    //    WCFSession GetCurrent()
    //    {
    //        return this.sessionList.FirstOrDefault(p => p.ID.Equals(GetID(), StringComparison.OrdinalIgnoreCase));
    //    }
    //}

    #endregion
}