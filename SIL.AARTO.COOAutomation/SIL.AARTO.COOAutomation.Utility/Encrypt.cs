﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SIL.AARTO.COOAutomation.Utility
{
    public class Encrypt
    {
        /// <summary>
        /// Encrypts a string using the SHA256 algorithm.
        /// </summary>
        public static string HashPassword(string plainMessage)
        {
            var data = Encoding.UTF8.GetBytes(plainMessage);
            using (var sha = new SHA256Managed())
            {
                sha.TransformFinalBlock(data, 0, data.Length);
                return Convert.ToBase64String(sha.Hash);
            }
        }
    }
}
