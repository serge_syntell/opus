﻿$(document).ready(function () {
    $("#CompanyCode").val("");
    $("#Number").val("");
    $("#Password").val("");
    $("#btnLogin").click(function () {
        $("#lblError").html('');
        if ($("#CompanyCode").val() == '') {
            //alert("User login name is required!");
            $("#lblError").html('The Company needs to be entered!');
            return false;
        }
        if ($("#Number").val() == '') {
            //alert("Password is required!");
            $("#lblError").html('The Proxy ID number needs to be entered!');
            return false;
        }
        if ($("#Password").val() == '') {
            //alert("Password is required!");
            $("#lblError").html('The Password needs to be entered!');
            return false;
        }

        $.ajax({
            url: _ROOTPATH + "/Account/Login",
            type: "POST",
            dataType: "json",
            data: { CompanyCode: $("#CompanyCode").val(), ProxyID: $("#Number").val(), Password: $("#Password").val() },
            success: function (data) {
                if (data.Status == true) {
                    parent.location.href = _ROOTPATH + "/Home/Index";
                }
                else {
                    $("#lblError").html(data.Text);
                }
            }
        });
    });
})