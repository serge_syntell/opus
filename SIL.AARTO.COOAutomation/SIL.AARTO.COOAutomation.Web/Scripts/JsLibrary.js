﻿function IsNullOrEmpty(dataStr, nullValue) {
    if (typeof (dataStr) == "undefined"
        || dataStr == null
            || dataStr.length == 0
                || dataStr == nullValue)
        return true;
    else
        return false;
}

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return null;
}