﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.COOAutomation.Model;
using SIL.AARTO.COOAutomation.Utility;
using SIL.AARTO.COOAutomation.Web.COOAccountSvc;
using SIL.AARTO.COOAutomation.Web.COOBasicDataSvc;
using SIL.AARTO.COOAutomation.Web.Models;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.COOAutomation.Web.Controllers
{
    public class AccountController : BaseController
    {
        //
        // GET: /Account/

        readonly COOAccountClient accountClient = new COOAccountClient();

        public ActionResult LogOn()
        {
            ProxyLogOnModel model = new ProxyLogOnModel();
            try
            {
                model.CompanyList.Add(new SelectListItem
                {
                    Text = "Please select your company",
                    Value = "",
                    Selected = true
                });
                model.CompanyList.AddRange(GetCompanyList());
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                EntLibLogger.WriteLog("Error", "LogOn", ex.Message);
            }

            return View(model);
        }

        [HttpPost]
        public JsonResult Login(string CompanyCode, string ProxyID, string Password)
        {
            Message message = new Message();
            try
            {
                var userInfo = new ProxyInfo
                {
                    CompanyCode = CompanyCode,
                    ProxyID = ProxyID,
                    Password = Password,
                    PasswordNew = "",
                    EmailAddress = "",
                    ResponseCode = "",
                    ResponseDescription = "",
                    XSDVersion = 1
                };
                var request = Formatter.Serialize(userInfo);

                userInfo = Formatter.Deserialize<ProxyInfo>(accountClient.LogOn(request, session.Current.SessionID));
                if (userInfo.ResponseCode == "NO00")
                {
                    session.Current.Proxy = userInfo;
                    session.Current.IsLogOn = true;
                }
                else
                {
                    session.Current.Proxy = null;
                    session.Current.IsLogOn = false;

                    message.Status = false;
                    message.Text = userInfo.ResponseDescription;
                }
            }
            catch (Exception ex)
            {
                message.Status = false;
                message.Text = ex.ToString();

                EntLibLogger.WriteLog("Error", "LogOn", ex.Message);
            }
            return Json(message);
        }


        public ActionResult ChangePassword()
        {
            if (!session.Current.IsLogOn) return LogOff();

            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (!session.Current.IsLogOn) return LogOff();

            if (ModelState.IsValid)
            {
                try
                {
                    var userInfo = new ProxyInfo
                    {
                        CompanyCode = session.Current.Proxy.CompanyCode,
                        ProxyID = session.Current.Proxy.ProxyID,
                        Password = model.OldPassword,
                        PasswordNew = model.NewPassword,
                        EmailAddress = session.Current.Proxy.EmailAddress,
                        ResponseCode = "",
                        ResponseDescription = "",
                        XSDVersion = 1
                    };
                    var request = Formatter.Serialize(userInfo);

                    userInfo = Formatter.Deserialize<ProxyInfo>(accountClient.ChangePassword(request, session.Current.SessionID));
                    if (userInfo.ResponseCode == "NO00")
                    {
                        session.Current.Proxy = userInfo;
                        return View("ChangePasswordSuccess");
                    }
                    else
                    {
                        ModelState.AddModelError("", userInfo.ResponseDescription);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);

                    EntLibLogger.WriteLog("Error", "ChangePassword", ex.Message);
                }
            }
            return View(model);
        }

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }


        public ActionResult ChangeInfo()
        {
            if (!session.Current.IsLogOn) return LogOff();

            return View();
        }

        [HttpPost]
        public ActionResult ChangeInfo(ChangeInfoModel model)
        {
            if (!session.Current.IsLogOn) return LogOff();

            if (ModelState.IsValid)
            {
                try
                {
                    var userInfo = new ProxyInfo
                    {
                        CompanyCode = session.Current.Proxy.CompanyCode,
                        ProxyID = session.Current.Proxy.ProxyID,
                        Password = "",
                        PasswordNew = "",
                        EmailAddress = model.EmailAddress.Trim(),
                        ResponseCode = "",
                        ResponseDescription = "",
                        XSDVersion = 1
                    };
                    var request = Formatter.Serialize(userInfo);

                    userInfo = Formatter.Deserialize<ProxyInfo>(accountClient.ChangeInfo(request, session.Current.SessionID));
                    if (userInfo.ResponseCode == "NO00")
                    {
                        session.Current.Proxy = userInfo;
                        return View("ChangeInfoSuccess");
                    }
                    else
                    {
                        ModelState.AddModelError("", userInfo.ResponseDescription);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);

                    EntLibLogger.WriteLog("Error", "ChangeInfo", ex.Message);
                }
            }
            return View(model);
        }

        public ActionResult ChangeInfoSuccess()
        {
            return View();
        }


        public ActionResult LogOff()
        {
            session.Clear();

            return RedirectToAction("LogOn", "Account");
        }


        IEnumerable<SelectListItem> GetCompanyList(string number = null)
        {
            List<SelectListItem> comList;
            var cacheKey = "CompanyList";
            if ((comList = globalCache.GetCache<List<SelectListItem>>(cacheKey)) == null)
            {
                var client = new COOBasicDataClient();
                client.Open();
                var comCollection = Formatter.Deserialize<CompanyCollection>(client.GetCompanyList());
                client.Close();

                comList = new List<SelectListItem>();
                comList.AddRange(
                    comCollection.Companies.Select(
                        com => new SelectListItem
                        {
                            Text = com.CompanyName,
                            Value = com.CompanyCode.ToString(CultureInfo.InvariantCulture)
                        }));
                comList.Sort(new CompanyListComparer());

                globalCache.Add(cacheKey, comList);
            }
            return comList;
        }

        class CompanyListComparer : IComparer<SelectListItem>
        {
            public int Compare(SelectListItem x, SelectListItem y)
            {
                if (x == null && y == null)
                    return 0;
                else if (x == null)
                    return -1;
                else if (y == null)
                    return 1;

                return string.Compare(x.Text, y.Text, StringComparison.OrdinalIgnoreCase);
            }
        }

    }
}
