﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web.Mvc;
using SIL.AARTO.COOAutomation.BLL;
using SIL.AARTO.COOAutomation.Model;
using SIL.AARTO.COOAutomation.Utility;

namespace SIL.AARTO.COOAutomation.Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            if (!session.Current.IsLogOn) return LogOff();

            ViewBag.Message = "Welcome to COO!";
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult LogOff()
        {
            session.Clear();

            return RedirectToAction("LogOn", "Account");
        }
    }
}
