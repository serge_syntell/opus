﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.COOAutomation.Web.Models;

namespace SIL.AARTO.COOAutomation.Web.Controllers
{
    public class BaseController : Controller
    {
        protected SiteSessionManager session = new SiteSessionManager();
        protected SiteGlobalCacheManager globalCache = SiteGlobalCacheManager.GetSingleton();

        protected bool CheckReturnUrl(string returnUrl)
        {
            return !string.IsNullOrWhiteSpace(returnUrl)
                   && Url.IsLocalUrl(returnUrl)
                   && returnUrl.Length > 1
                   && returnUrl.StartsWith("/")
                   && !returnUrl.StartsWith("//")
                   && !returnUrl.StartsWith("/\\");
        }
    }
}