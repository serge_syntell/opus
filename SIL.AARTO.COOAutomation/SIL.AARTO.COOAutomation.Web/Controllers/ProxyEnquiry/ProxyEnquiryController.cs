﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Web.Mvc;
using SIL.AARTO.COOAutomation.Model;
using SIL.AARTO.COOAutomation.Web.Models;
using SIL.AARTO.COOAutomation.Utility;
using SIL.AARTO.BLL.EntLib;


namespace SIL.AARTO.COOAutomation.Web.Controllers
{
    public class ProxyEnquiryController : BaseController
    {
        //
        // GET: /ProxyEnquiry/

        public ActionResult Index(ProxyEnquiryModel model)
        {
            if (!session.Current.IsLogOn) return LogOff();

            try
            {
                ProxyEnquiry proxyEnquiry = new ProxyEnquiry
                {
                    RequestType = "I",
                    CompanyCode = session.Current.Proxy.CompanyCode,
                    ProxyID = session.Current.Proxy.ProxyID,
                    PageIndex = model.Page,
                    XSDVersion = 1
                };

                var request = Formatter.Serialize(proxyEnquiry);

                COOProxyEnquirySvc.COOProxyEnquiryClient client = new COOProxyEnquirySvc.COOProxyEnquiryClient();
                ProxyEnquiryResponse response = Formatter.Deserialize<ProxyEnquiryResponse>(client.COOProxyEnquiry(request, session.Current.SessionID));

                if (response.ResponseCode == "NO00")
                {
                    model.ProxyNoticeList = response.ProxyNotices.ProxyNoticeList;
                    if (model.ProxyNoticeList == null)
                    {
                        model.ProxyNoticeList = new List<ProxyNotice>();
                    }
                    model.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
                    model.TotalCount = response.TotalCount;

                    if (model.ProxyNoticeList.Count == 0)
                    {
                        model.ErrorMsg = "There are no data for Proxy Enquiry";
                    }
                }
                else
                {
                    model.ErrorMsg = response.ResponseDescription;
                }
            }
            catch (Exception ex)
            {
                model.ErrorMsg = ex.Message;
                EntLibLogger.WriteLog("Error", "ProxyEnquiry", ex.Message);
            }
            return View(model);
        }

        public ActionResult Download(ProxyEnquiryModel model)
        {
            if (!session.Current.IsLogOn) return LogOff();

            try
            {
                ProxyEnquiry proxyEnquiry = new ProxyEnquiry
                {
                    RequestType = "I",
                    CompanyCode = session.Current.Proxy.CompanyCode,
                    ProxyID = session.Current.Proxy.ProxyID,
                    XSDVersion = 1
                };

                var request = Formatter.Serialize(proxyEnquiry);

                COOProxyEnquirySvc.COOProxyEnquiryClient client = new COOProxyEnquirySvc.COOProxyEnquiryClient();
                ProxyEnquiryDownloadResponse response = Formatter.Deserialize<ProxyEnquiryDownloadResponse>(client.COOProxyEnquiryDownload(request, session.Current.SessionID));

                if (response.ResponseCode == "NO00")
                {
                    return File(response.Excel, "application/ms-excel", "Proxy Enquiry response.xls");
                }
                else
                {
                    model.ErrorMsg = response.ResponseDescription;
                }
            }
            catch (Exception ex)
            {
                model.ErrorMsg = ex.Message;
                EntLibLogger.WriteLog("Error", "Download", ex.Message);
            }
            return View(model);
        }

        public ActionResult LogOff()
        {
            session.Clear();

            return RedirectToAction("LogOn", "Account");
        }
    }
}
