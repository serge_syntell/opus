﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Web.Mvc;
using SIL.AARTO.COOAutomation.Model;
using SIL.AARTO.COOAutomation.Web.Models;
using SIL.AARTO.COOAutomation.Utility;
using SIL.AARTO.BLL.EntLib;


namespace SIL.AARTO.COOAutomation.Web.Controllers
{
    public class ResultEnquiryController : BaseController
    {
        //
        // GET: /ProxyEnquiry/
        public ActionResult Index(string OffenceDate, int? Page)
        {
            if (!session.Current.IsLogOn) return LogOff();

            //ResultEnquiryModel model = new ResultEnquiryModel();
            //DateTime OffenceDate;
            //OffenceDate = DateTime.Today.AddDays(-60);
            //model.OffenceDate = OffenceDate.ToString("yyyy-MM-dd");

            // 2014-04-17, Oscar changed
            DateTime offenceDate;
            var dateValidated = DateTime.TryParse(OffenceDate, out offenceDate);
            if (!dateValidated) offenceDate = DateTime.Now.AddDays(-60);

            var model = new ResultEnquiryModel
            {
                OffenceDate = offenceDate.ToString("yyyy-MM-dd"),
                Page = Page.GetValueOrDefault()
            };

            return !dateValidated ? View(model) : Index(model);
        }

        [HttpPost]
        public ActionResult Index(ResultEnquiryModel model)
        {
            if (!session.Current.IsLogOn) return LogOff();

            try
            {
                DateTime OffenceDate;
                if (!string.IsNullOrWhiteSpace(model.OffenceDate))
                {
                    if (!DateTime.TryParse(model.OffenceDate, out OffenceDate))
                    {
                        model.ErrorMsg = "Please enter a correct offence date";
                        return View(model);
                    }
                }
                else
                {
                    OffenceDate = DateTime.Today.AddDays(-60);
                    model.OffenceDate = OffenceDate.ToString("yyyy-MM-dd");
                }

                ResultEnquiry resultEnquiry = new ResultEnquiry
                {
                    CompanyCode = session.Current.Proxy.CompanyCode,
                    ProxyID = session.Current.Proxy.ProxyID,
                    Dateofoffence = OffenceDate,
                    PageIndex = model.Page,
                    XSDVersion = 1
                };

                var request = Formatter.Serialize(resultEnquiry);

                COOResultEnquirySvc.COOResultEnquiryClient client = new COOResultEnquirySvc.COOResultEnquiryClient();
                ResultEnquiryResponse response = Formatter.Deserialize<ResultEnquiryResponse>(client.COOResultEnquiry(request, session.Current.SessionID));

                if (response.ResponseCode == "NO00")
                {
                    model.ResultNoticeList = response.ResultNotices.ResultNoticeList;
                    if (model.ResultNoticeList == null)
                    {
                        model.ResultNoticeList = new List<ResultNotice>();
                    }
                    model.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
                    model.TotalCount = response.TotalCount;

                    if (model.ResultNoticeList.Count == 0)
                    {
                        model.ErrorMsg = "There are no data for Result Enquiry";
                    }
                }
                else
                {
                    model.ErrorMsg = response.ResponseDescription;
                }
            }
            catch (Exception ex)
            {
                model.ErrorMsg = ex.Message;
                EntLibLogger.WriteLog("Error", "ResultEnquiry", ex.Message);
            }
            return View(model);
        }

        public ActionResult LogOff()
        {
            session.Clear();

            return RedirectToAction("LogOn", "Account");
        }
    }
}
