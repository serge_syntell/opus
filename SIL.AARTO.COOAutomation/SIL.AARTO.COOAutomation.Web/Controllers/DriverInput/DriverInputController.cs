﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web.Mvc;
using SIL.AARTO.COOAutomation.BLL;
using SIL.AARTO.COOAutomation.Model;
using SIL.AARTO.COOAutomation.Utility;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.COOAutomation.Web.Controllers
{
    public class DriverInputController : BaseController
    {
        //
        // GET: /DriverInput/

        public ActionResult Index()
        {
            if (!session.Current.IsLogOn) return LogOff();
            return View();
        }

        [HttpPost]
        public ActionResult Upload(FormCollection form)
        {
            ViewBag.Error = "";

            try
            {
                var validation = new DriversUploadManager(Request.Files);
                if (!validation.Validate())
                {
                    ViewBag.Error = validation.ErrorMessage;
                    return View("Index");
                }

                var request = validation.RequestXml;

                COODriverInputSvc.COODriverInputClient client = new COODriverInputSvc.COODriverInputClient();
                string response = client.COODriverInput(request, session.Current.SessionID);

                return File(Encoding.Default.GetBytes(response), "text/xml", "CandidateDriversResponse.xml");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;

                EntLibLogger.WriteLog("Error", "Upload", ex.Message);

                return View("Index");
            }
        }

        public ActionResult LogOff()
        {
            session.Clear();

            return RedirectToAction("LogOn", "Account");
        }
    }
}