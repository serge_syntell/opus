﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.Specialized;


namespace SIL.AARTO.COOAutomation.Web.Helpers.Paginator
{
    public static class GridPagerExtension
    {

        public static string GridPager(this HtmlHelper htmlHelper)
        {
            return GridPager(htmlHelper, new GridPagerProperties());
        }

        public static string GridPager(this HtmlHelper htmlHelper, GridPagerProperties properties)
        {
            System.Web.HttpContext.Current.Session["parameters"] = properties.parameters;
            return GenerateLinks(properties);
        }

        private static string GeneratePostBackUrl(int pageNumber, string key)
        {
            string url = HttpContext.Current.Request.Path;
            NameValueCollection page = new NameValueCollection();
            page.Add(key, pageNumber.ToString());

            string qString = QueryString.SetValues(page, HttpContext.Current.Request.RawUrl);

            if (qString.Length > 0)
            {
                if (System.Web.HttpContext.Current.Session["parameters"] != null)
                {
                    string para = "";
                    for (int i = 0; i < qString.Split('&').Length; i++)
                    {

                        string QSPara = qString.Split('&')[i].Substring(0, qString.Split('&')[i].IndexOf("="));
                        // Oscar 20121108 added key match "QSPara == key"
                        if ((QSPara == "Page") || (QSPara == "SortExpr") || (QSPara == "SortDir") || QSPara == key)
                        {
                            para += qString.Split('&')[i] + "&";
                        }
                    }
                    qString = para + System.Web.HttpContext.Current.Session["parameters"].ToString();
                }
                url += "?" + qString;

            }
            return url;
        }

        private static string GenerateLinks(GridPagerProperties properties)
        {
            //StringBuilder pagerString = new StringBuilder();

            TagBuilder pagerDiv = new TagBuilder("div") { };
            pagerDiv.MergeAttributes(new Dictionary<string, string> { { "class", "pager" } });

            if (properties.PageCount > 1)
            {
                //create the previous tag
                if (properties.CurrentPageIndex > properties.PageStart)
                {
                    string prevUrl = GeneratePostBackUrl(properties.CurrentPageIndex - 1, properties.PageKey);
                    TagBuilder prevTag = new TagBuilder("a")
                    {
                        InnerHtml = HttpUtility.HtmlEncode("<")
                    };
                    prevTag.MergeAttributes(new Dictionary<string, string> { { "href", prevUrl } });

                    pagerDiv.InnerHtml += prevTag.ToString(); ;
                    //pagerString.Append(prevTag.ToString());
                }

                int pageStartSub = properties.PageStart - 1;
                for (int page = properties.PageStart; page < properties.PageCount + properties.PageStart; page++)
                {

                    string url = GeneratePostBackUrl(page, properties.PageKey);
                    TagBuilder tag = new TagBuilder("a")
                    {
                        InnerHtml = HttpUtility.HtmlEncode((page - pageStartSub).ToString())
                    };
                    tag.MergeAttributes(new Dictionary<string, string> { { "href", url } });


                    if (HttpContext.Current.Request.Url.PathAndQuery == url)
                        tag.Attributes.Add("class", "selected");
                    else if ((HttpContext.Current.Request.QueryString.Count == 0 || QueryString.GetInt(properties.PageKey) == 0) && page == properties.PageStart)
                        tag.Attributes.Add("class", "selected");

                    TagBuilder label = new TagBuilder("label")
                    {
                        InnerHtml = HttpUtility.HtmlEncode("...")
                    };
                    label.MergeAttributes(new Dictionary<string, string> { { "text", "..." } });


                    if (properties.PageCount > 11)
                    {
                        if (properties.CurrentPageIndex < 8 + pageStartSub)
                        {
                            //beginning
                            if (page < 9 || ((properties.PageCount - page) < 3))
                                pagerDiv.InnerHtml += tag.ToString();
                            else if (page == 10)
                                pagerDiv.InnerHtml += label.ToString();
                        }
                        else if (properties.CurrentPageIndex >= (properties.PageCount - 8 + pageStartSub))
                        {
                            //end
                            if (page < 2 || (page >= properties.PageCount - 9))
                                pagerDiv.InnerHtml += tag.ToString();
                            else if (page == 2)
                                pagerDiv.InnerHtml += label.ToString();
                        }
                        else
                        {
                            //middle
                            if (page < 2)
                                pagerDiv.InnerHtml += tag.ToString();
                            else if (page == 2)
                                pagerDiv.InnerHtml += label.ToString();
                            else if ((page >= (properties.CurrentPageIndex - 5 + pageStartSub)) && (page <= (properties.CurrentPageIndex + 5 + pageStartSub)))
                                pagerDiv.InnerHtml += tag.ToString();
                            else if (page == properties.CurrentPageIndex + 6 + pageStartSub)
                                pagerDiv.InnerHtml += label.ToString();
                            else if (page >= properties.PageCount - 2)
                                pagerDiv.InnerHtml += tag.ToString();
                        }
                    }
                    else
                        pagerDiv.InnerHtml += tag.ToString();
                }


                //create the next tag
                if (properties.CurrentPageIndex < properties.PageCount + pageStartSub)
                {
                    string nextUrl = GeneratePostBackUrl(properties.CurrentPageIndex + 1, properties.PageKey);
                    TagBuilder nextTag = new TagBuilder("a")
                    {
                        InnerHtml = HttpUtility.HtmlEncode(">")
                    };
                    nextTag.MergeAttributes(new Dictionary<string, string> { { "href", nextUrl } });

                    pagerDiv.InnerHtml += nextTag.ToString();


                }
                pagerDiv.InnerHtml += "<span>" + String.Format("{0} items", properties.RecordCount) + "</span>";
            }

            // create the total items tag
            //TagBuilder countDiv = new TagBuilder("div")
            //{
            //    InnerHtml = String.Format("{0} items", properties.RecordCount)
            //};
            //countDiv.MergeAttributes(new Dictionary<string, string> { { "class", "totalrecords" } });

            //pagerDiv.InnerHtml += countDiv.ToString();

            TagBuilder clearDiv = new TagBuilder("div") { };
            clearDiv.MergeAttributes(new Dictionary<string, string> { { "class", "clear" } });

            pagerDiv.InnerHtml += clearDiv.ToString();

            return pagerDiv.ToString();
        }
    }

    public class GridPagerProperties
    {
        public GridPagerProperties()
        {
            PageStart = 0;
        }
        public string PageKey { get; set; }
        public int PageStart { get; set; }
        public int PageSize { get; set; }
        public int RecordCount { get; set; }
        public string parameters { get; set; }//add by Tod
        public int PageCount
        {
            get
            {
                if (PageSize > 0)
                    return (RecordCount / PageSize) + ((RecordCount % PageSize == 0) ? 0 : 1);
                else
                    return 0;
            }
        }

        public int CurrentPageIndex
        {
            get
            {
                if (QueryString.GetInt(PageKey) > 0)
                    return QueryString.GetInt(PageKey);
                else
                    return PageStart;
            }
        }
    }
}
