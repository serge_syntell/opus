﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.Specialized;
using System.Web.UI.WebControls;


namespace SIL.AARTO.COOAutomation.Web.Helpers.Paginator
{
    public static class GridSortingLink
    {
        public static string SortLink(this HtmlHelper htmlHelper)
        {
            return SortLink(htmlHelper, new GridSortingLinkProperties());
        }

        public static string SortLink(this HtmlHelper htmlHelper, GridSortingLinkProperties properties)
        {
            System.Web.HttpContext.Current.Session["parameters1"] = properties.parameters;

            string url = GenerateSortUrl(properties);

            string currentSortExpression = QueryString.GetString(properties.SortExpressionKey);
            string currentSortDirection = QueryString.GetString(properties.SortDirectionKey);
            string arrow = "link5";

            if (properties.SortExpression == currentSortExpression)
            {
                if (currentSortDirection == "ASC")
                    arrow = "link5 asc";
                else
                    arrow = "link5 desc";
            }

            TagBuilder tag = new TagBuilder("a")
            {
                InnerHtml = HttpUtility.HtmlEncode(properties.Text)
            };
            tag.MergeAttributes(new Dictionary<string, string> { { "href", url }, { "class", arrow } });

            return tag.ToString();
        }

        private static string GenerateSortUrl(GridSortingLinkProperties p)
        {
            string url, qString = string.Empty;
            string newSortDirection = "ASC";
            NameValueCollection sort = new NameValueCollection();

            string currentSortExpression = QueryString.GetString(p.SortExpressionKey);
            string currentSortDirection = QueryString.GetString(p.SortDirectionKey);

            if (string.IsNullOrEmpty(currentSortDirection) && string.IsNullOrEmpty(currentSortExpression))
            {
                if (p.IsDefaultSortColumn)
                    newSortDirection = ToggleSortDirection(newSortDirection);
                else
                    newSortDirection = p.LinkSortDirection == SortDirection.Ascending ? "ASC" : "DESC";
            }
            else
            {
                if (p.SortExpression == currentSortExpression)
                    newSortDirection = ToggleSortDirection(currentSortDirection);
                else
                    newSortDirection = p.LinkSortDirection == SortDirection.Ascending ? "ASC" : "DESC";
            }

            sort.Add(p.SortExpressionKey, p.SortExpression);
            sort.Add(p.SortDirectionKey, newSortDirection);

            url = HttpContext.Current.Request.Path;
            qString = QueryString.SetValues(sort, HttpContext.Current.Request.RawUrl);

            if (qString.Length > 0)
            {
                if (System.Web.HttpContext.Current.Session["parameters1"] != null)
                {
                    string para = "";

                    for (int i = 0; i < qString.Split('&').Length; i++)
                    {

                        string QSPara = qString.Split('&')[i].Substring(0, qString.Split('&')[i].IndexOf("="));
                        if ((QSPara == "Page") || (QSPara == "SortExpr") || (QSPara == "SortDir"))
                        {
                            para += qString.Split('&')[i] + "&";
                        }
                    }


                    qString = para + System.Web.HttpContext.Current.Session["parameters1"].ToString();
                }
                url += "?" + qString;
            }

            return url;
        }

        private static string ToggleSortDirection(string currentSortDirection)
        {
            if (currentSortDirection == "ASC")
                return "DESC";
            else
                return "ASC";
        }
    }

    public class GridSortingLinkProperties
    {
        private SortDirection _sortDirection = SortDirection.Ascending;
        public SortDirection LinkSortDirection
        {
            get { return _sortDirection; }
            set { _sortDirection = value; }
        }

        private bool _isDefaultSortColumn = false;
        public bool IsDefaultSortColumn
        {
            get { return _isDefaultSortColumn; }
            set { _isDefaultSortColumn = value; }
        }

        public string Text { get; set; }
        public string SortExpression { get; set; }
        public string SortDirectionKey { get; set; }
        public string SortExpressionKey { get; set; }
        public string parameters { get; set; }//
    }
}
