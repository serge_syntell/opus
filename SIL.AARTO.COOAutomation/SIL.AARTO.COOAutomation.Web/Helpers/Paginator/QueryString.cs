﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;
using System.Text;

namespace SIL.AARTO.COOAutomation.Web.Helpers.Paginator
{
    public class QueryString
    {

        public static int GetInt(string qs)
        {
            int id;
            if (int.TryParse(HttpUtility.UrlDecode(HttpContext.Current.Request.QueryString[qs]), out id))
            {
                return id;
            }

            // if an error, return 0
            return 0;
        }

        public static string GetString(string qs)
        {
            return HttpUtility.UrlDecode(HttpContext.Current.Request.QueryString[qs]);
        }

        public static string FormatQueryString(NameValueCollection qs)
        {

            StringBuilder result = new StringBuilder();

            foreach (string strKey in qs.AllKeys)
            {

                if (result.Length > 0)
                    result.Append("&");

                // use encoding
                result.Append(HttpUtility.UrlEncode(strKey));
                result.Append("=");
                result.Append(HttpUtility.UrlEncode(qs.Get(strKey)));
            }

            return result.ToString();
        }

        public static string FormatQueryStringDecoded(NameValueCollection qs)
        {

            StringBuilder result = new StringBuilder();

            foreach (string strKey in qs.AllKeys)
            {

                if (result.Length > 0)
                    result.Append("&");

                // use encoding
                result.Append(strKey);
                result.Append("=");
                result.Append(qs.Get(strKey));
            }

            return result.ToString();
        }


        public static string SetStringValue(string key, string value)
        {
            bool valueSet = false;
            NameValueCollection queryParameters = new NameValueCollection();
            NameValueCollection currentParameters = HttpContext.Current.Request.QueryString;
            for (int index = 0; index < currentParameters.Count; index++)
            {
                if (currentParameters.GetKey(index) == key)
                {

                    queryParameters.Add(key, value);
                    valueSet = true;
                }
                else
                    queryParameters.Add(currentParameters.GetKey(index), GetString(currentParameters.GetKey(index)));
            }

            if (!valueSet)
                queryParameters.Add(key, value);

            return QueryString.FormatQueryString(queryParameters);
        }

        public static string SetIntValue(string key, int value)
        {
            return SetStringValue(key, value.ToString());
        }

        public static string SetValues(NameValueCollection newValues)
        {
            NameValueCollection queryParameters = new NameValueCollection();
            NameValueCollection currentParameters = HttpContext.Current.Request.QueryString;

            for (int index = 0; index < currentParameters.Count; index++)
            {
                string key = string.Empty;
                string value = string.Empty;

                if (newValues.Count > 0)
                {
                    key = newValues.GetKey(0);
                    value = newValues[0];
                }

                if (currentParameters.GetKey(index) == key)
                {
                    queryParameters.Add(key, value);
                    newValues.Remove(key);
                }
                else
                {
                    key = currentParameters.GetKey(index);
                    value = currentParameters[index];

                    if (string.IsNullOrEmpty(queryParameters[key]))
                        queryParameters.Add(key, value);
                    else if (queryParameters[key] != currentParameters[key])
                        queryParameters[key] = value;
                }
            }

            while (newValues.Count > 0)
            {
                string key = newValues.GetKey(0);
                string value = newValues[0];

                queryParameters.Add(key, value);
                newValues.Remove(key);
            }
            return QueryString.FormatQueryStringDecoded(queryParameters);
        }

        public static string SetValues(NameValueCollection newValues, string prettyUrl)
        {
            NameValueCollection queryParameters = new NameValueCollection();
            NameValueCollection currentParameters = CreateNameValueCollection(prettyUrl);

            for (int index = 0; index < currentParameters.Count; index++)
            {
                string key = string.Empty;
                string value = string.Empty;

                if (newValues.Count > 0)
                {
                    key = newValues.GetKey(0);
                    value = newValues[0];
                }

                if (currentParameters.GetKey(index) == key)
                {
                    queryParameters.Add(key, value);
                    newValues.Remove(key);
                }
                else
                {
                    key = currentParameters.GetKey(index);
                    value = currentParameters[index];

                    if (string.IsNullOrEmpty(queryParameters[key]))
                        queryParameters.Add(key, value);
                    else if (queryParameters[key] != currentParameters[key])
                        queryParameters[key] = value;
                }
            }

            while (newValues.Count > 0)
            {
                string key = newValues.GetKey(0);
                string value = newValues[0];

                queryParameters.Add(key, value);
                newValues.Remove(key);
            }
            return QueryString.FormatQueryStringDecoded(queryParameters);
        }

        public static NameValueCollection CreateNameValueCollection(string prettyUrl)
        {
            NameValueCollection values = new NameValueCollection();


            if (prettyUrl.IndexOf('?') > 0)
            {
                //peel the visible querystring from the url
                string qString = prettyUrl.Substring(prettyUrl.IndexOf('?') + 1);

                string[] queryParams = qString.Split('&');

                for (int index = 0; index < queryParams.Length; index++)
                {
                    string[] nameValue = queryParams[index].Split('=');
                    if (nameValue.Length > 1)
                        values.Add(nameValue[0], nameValue[1]);
                    else
                        values.Add(nameValue[0], "");
                }
            }

            return values;
        }

        public static NameValueCollection CreateRedirectNameValueCollection(string prettyUrl)
        {
            NameValueCollection values = new NameValueCollection();


            if (prettyUrl.IndexOf('?') > 0)
            {
                //peel the visible querystring from the url
                string qString = prettyUrl.Substring(prettyUrl.IndexOf('?') + 1);

                string[] queryParams = qString.Split('&');

                for (int index = 0; index < queryParams.Length; index++)
                {
                    string[] nameValue = queryParams[index].Split('=');

                    values.Add(nameValue[0], HttpUtility.UrlDecode(nameValue[1]));
                }
            }

            return values;
        }
    }
}
