﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIL.AARTO.COOAutomation.Web
{
    /// <summary>
    /// Summary description for RootPathHandler
    /// </summary>
    public class RootPathHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            string clientCode = string.Empty;
            if ("/RootPathHandler.ashx".ToUpper() == context.Request.ServerVariables["SCRIPT_NAME"].ToUpper())
            {
                //clientCode = "var _ROOTPATH='" + "http://" + context.Request.ServerVariables["HTTP_HOST"] + "'";
                //context.Response.Write("var _ROOTPATH='" + "http://" + context.Request.ServerVariables["HTTP_HOST"] + "'");
                context.Response.Write("var _ROOTPATH=''");
            }
            else
            {
                string virtualPath = context.Request.ServerVariables["SCRIPT_NAME"];
                int tmpEnd = virtualPath.IndexOf('/', 1);
                //context.Response.Write("var _ROOTPATH='" + "http://" + context.Request.ServerVariables["HTTP_HOST"] + virtualPath.Substring(0, tmpEnd + 1) + "'");

                context.Response.Write("var _ROOTPATH='" + virtualPath.Substring(0, tmpEnd) + "'");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}