﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using SIL.AARTO.COOAutomation.BLL;

namespace SIL.AARTO.COOAutomation.Web.Models
{
    public class ChangePasswordModel
    {
        [Required(ErrorMessage = "*")]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "*")]
        [RegularExpression(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,50}$", ErrorMessage = "Password must be at least 6 characters, no more than 50 characters and must include at least one upper case letter, one lower case letter, and one numeric digit.")]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        //[Compare("OldPassword", ErrorMessage = "The new password and old password should be not equal.")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "*")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangeInfoModel
    {
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "*")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        [RegularExpression(ConstVars.EmailPattern, ErrorMessage = "Email format is invalid")]
        public string EmailAddress { get; set; }
    }

    public class ProxyLogOnModel
    {
        public ProxyLogOnModel()
        {
            CompanyList = new List<SelectListItem>();
        }

        [Required(ErrorMessage = "*")]
        public int CompanyID { get; set; }

        [Required(ErrorMessage = "*")]
        public string Number { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "Password must be at least 6 characters long", MinimumLength = 6)]
        [RegularExpression(ConstVars.PasswordPattern, ErrorMessage = "Password format is invalid")]
        public string Password { get; set; }

        public List<SelectListItem> CompanyList { get; set; }
    }
}
