﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using SIL.AARTO.COOAutomation.Model;

namespace SIL.AARTO.COOAutomation.Web.Models
{
    public class SiteSession
    {
        string id;
        public string SessionID
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.id))
                    this.id = Guid.NewGuid().ToString();
                return this.id;
            }
        }

        // add your session elements here!

        bool isLogOn;
        public bool IsLogOn
        {
            get { return isLogOn && Proxy != null; }
            set { isLogOn = value; }
        }

        public ProxyInfo Proxy { get; set; }
    }

    public class SiteSessionManager
    {
        public SiteSessionManager()
        {
            if (!IsAvailable)
                HttpContext.Current.Session["SiteSession"] = new SiteSession();
        }

        string sessionName = "SiteSession";

        public SiteSession Current
        {
            get
            {
                if (IsAvailable)
                    return HttpContext.Current.Session[this.sessionName] as SiteSession;
                else
                {
                    var session = new SiteSession();
                    HttpContext.Current.Session[this.sessionName] = session;
                    return session;
                }
            }
        }
        public bool IsAvailable
        {
            get
            {
                return HttpContext.Current.Session[this.sessionName] != null
                       && HttpContext.Current.Session[this.sessionName] is SiteSession;
            }
        }

        public void Clear(bool clearAll = true)
        {
            if (clearAll)
                HttpContext.Current.Session.Clear();
            else
                HttpContext.Current.Session.Remove(this.sessionName);
        }
    }
}