﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIL.AARTO.COOAutomation.Model;

namespace SIL.AARTO.COOAutomation.Web.Models
{
    public class ProxyEnquiryModel
    {
        public string ErrorMsg { get; set; }

        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public string URLPara { get; set; }
        public string LastUser { get; set; }
        public int Page { get; set; }

        public List<ProxyNotice> ProxyNoticeList { get; set; }

        public ProxyEnquiryModel()
        {
            ProxyNoticeList = new List<ProxyNotice>();
        }
    }
}