﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace SIL.AARTO.COOAutomation.Web.Models
{
    public class SiteGlobalCache
    {
        public string Key { get; set; }
        public object Value { get; set; }

        public DateTime LastUpdatedDate { get; set; }
        public long TimeoutMinutes { get; set; }
    }

    public class SiteGlobalCacheManager
    {
        SiteGlobalCacheManager() {}
        static SiteGlobalCacheManager instance;

        public static SiteGlobalCacheManager GetSingleton()
        {
            if (instance == null)
            {
                instance = new SiteGlobalCacheManager();
                var timeout = ConfigurationManager.AppSettings["globalCacheTimeoutMinutes"];
                instance.isEnable = long.TryParse(timeout, out instance.timeout) && instance.timeout != 0;
            }
            return instance;
        }

        readonly object lockObj = new object();
        readonly List<SiteGlobalCache> cacheList = new List<SiteGlobalCache>();
        DateTime maxExpiredDate, minExpiredDate;
        long timeout;
        bool isEnable;

        public void Add(string key, object value, long? timeoutMinutes = null)
        {
            if (!this.isEnable) return;

            var now = DateTime.Now;
            lock (this.lockObj)
            {
                //Regain(now);
                var cache = Find(key);

                DateTime expiredDate;
                if (cache == null)
                {
                    this.cacheList.Add(new SiteGlobalCache
                    {
                        Key = key,
                        Value = value,
                        LastUpdatedDate = now,
                        TimeoutMinutes = timeoutMinutes ?? this.timeout
                    });

                    if ((expiredDate = now.AddMinutes(timeoutMinutes ?? this.timeout)) < this.minExpiredDate)
                        this.minExpiredDate = expiredDate;
                }
                else
                {
                    cache.Value = value;
                    cache.LastUpdatedDate = now;
                    expiredDate = now.AddMinutes(cache.TimeoutMinutes);
                }

                if (expiredDate > this.maxExpiredDate)
                    this.maxExpiredDate = expiredDate;
            }
        }

        public T GetCache<T>(string key)
        {
            var cache = GetCache(key);
            return cache != null ? (T)cache : default(T);
        }

        public object GetCache(string key)
        {
            if (!this.isEnable) return null;

            lock (this.lockObj)
            {
                Regain();
                var cache = Find(key);
                return cache != null ? cache.Value : null;
            }
        }


        SiteGlobalCache Find(string key)
        {
            return this.cacheList.FirstOrDefault(p => p.Key == key);
        }

        void Regain()
        {
            var now = DateTime.Now;

            if (now <= this.minExpiredDate) return;

            this.minExpiredDate = DateTime.MinValue;

            if (now >= this.maxExpiredDate)
            {
                this.cacheList.Clear();
                this.maxExpiredDate = DateTime.MinValue;
            }
            else
            {
                DateTime? newExpiredDate = null;
                for (var i = 0; i < this.cacheList.Count; i++)
                {
                    var s = this.cacheList[i];
                    var expiredDate = s.LastUpdatedDate.AddMinutes(s.TimeoutMinutes);
                    if (expiredDate <= now)
                    {
                        this.cacheList.Remove(s);
                        i--;
                    }
                    else if (!newExpiredDate.HasValue || expiredDate < newExpiredDate.Value)
                        newExpiredDate = expiredDate;
                }
                if (newExpiredDate.HasValue)
                    this.minExpiredDate = newExpiredDate.Value;
            }
        }

    }
}