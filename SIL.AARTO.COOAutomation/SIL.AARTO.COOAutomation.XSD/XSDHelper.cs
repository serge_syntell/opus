﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using SIL.AARTO.COOAutomation.Resource;

namespace SIL.AARTO.COOAutomation.XSD
{
    public class XSDHelper
    {
        static readonly object syncObj = new object();

        static FileInfo GetXSDFile(string version, XSDType xsdType)
        {
            string xsdFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\XSDFiles");
            if (!Directory.Exists(xsdFolder))
                throw new IOException(ResourceHelper.GetResource("DirectoryDoesNotExist", xsdFolder));

            if (!string.IsNullOrWhiteSpace(version))
                xsdFolder = Path.Combine(xsdFolder, version);

            string xsdFilePath = Path.Combine(xsdFolder, xsdType.ToString() + ".xsd");

            return new FileInfo(xsdFilePath);
        }

        static string GetXSDVersion(XmlDocument xmlDoc)
        {
            return GetFirstNodeValue(xmlDoc, "XSDVersion");
        }

        public static bool CheckXSDVersion(XmlDocument xmlDoc, XSDType xsdType, out string xsdVersion)
        {
            xsdVersion = GetXSDVersion(xmlDoc);
            if (string.IsNullOrWhiteSpace(xsdVersion)) return false;

            var xsdFile = GetXSDFile(xsdVersion, xsdType);
            return xsdFile != null && xsdFile.Exists;
        }

        /// <summary>
        /// will return empty message or error message.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xsdType"></param>
        /// <returns></returns>
        public static string XSDValidation(XmlDocument xmlDoc, XSDType xsdType)
        {
            string errorMsg = string.Empty;
            try
            {
                lock (syncObj)
                {
                    var xsdFile = GetXSDFile(GetXSDVersion(xmlDoc), xsdType);
                    //if (xsdFile != null && xsdFile.Exists)
                    //{
                    using (var xmlReader = new XmlTextReader(xsdFile.FullName))
                    {
                        var schema = XmlSchema.Read(xmlReader, (s, e) => { errorMsg = e.Message; });
                        if (!string.IsNullOrWhiteSpace(errorMsg)) return errorMsg;

                        xmlDoc.Schemas.Add(schema);

                        xmlDoc.Validate((s, e) => { errorMsg = e.Message; });
                    }
                    //}
                }
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
            }
            return errorMsg;
        }

        public static string GetFirstNodeValue(XmlDocument xmlDoc, string nodeName, XmlNode parentNode = null)
        {
            if (string.IsNullOrWhiteSpace(nodeName)) return null;

            string value = null;
            nodeName = nodeName.Trim();

            XmlNodeList nodeList = null;
            if (parentNode != null)
            {
                if (parentNode.HasChildNodes)
                    nodeList = parentNode.ChildNodes;
            }
            else if (xmlDoc != null && xmlDoc.HasChildNodes)
                nodeList = xmlDoc.ChildNodes;
            if (nodeList != null && nodeList.Count > 0)
            {
                foreach (XmlNode node in nodeList)
                {
                    if (node.Name.Equals(nodeName))
                    {
                        value = node.HasChildNodes ? node.InnerXml.Trim() : node.InnerText.Trim();
                        break;
                    }
                    if (node.HasChildNodes
                        && (node.ChildNodes.Count > 1
                            || (node.ChildNodes.Count == 1 && node.FirstChild.NodeType == XmlNodeType.Element))
                        )
                    {
                        value = GetFirstNodeValue(xmlDoc, nodeName, node);
                        if (value != null)
                            break;
                    }
                }
            }
            return value;
        }
    }

    public enum XSDType
    {
        None,
        CandidateDrivers,
        EvidenceStatus,
        ProxyInfo,
        ProxyEnquiry,
        ResultEnquiry
    }
}
