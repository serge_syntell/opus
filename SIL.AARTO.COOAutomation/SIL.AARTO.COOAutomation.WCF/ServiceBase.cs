﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using SIL.AARTO.COOAutomation.Utility;

namespace SIL.AARTO.COOAutomation.WCF
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ServiceBase : IServiceBase
    {
        protected WCFSessionManager session = WCFSessionManager.GetSingleton();
    }
}