﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using SIL.AARTO.COOAutomation.BLL;
using SIL.AARTO.COOAutomation.Model;
using SIL.AARTO.COOAutomation.Utility;

using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.COOAutomation.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "COOResultEnquiry" in code, svc and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class COOResultEnquiryService :ServiceBase, ICOOResultEnquiry
    {
        public string COOResultEnquiry(string xmlRequest, string guid)
        {
            ResultEnquiryResponse response = new ResultEnquiryResponse();
            if (!session.IsValid(guid))
            {
                response.ResponseCode = CooResponseCodeList.NO07.ToString();
                response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
            }
            else
            {
                var manager = new ResultEnquiryManager(xmlRequest);
                response = manager.Enquiry();
            }
            return Formatter.Serialize(response);
        }
    }
}
