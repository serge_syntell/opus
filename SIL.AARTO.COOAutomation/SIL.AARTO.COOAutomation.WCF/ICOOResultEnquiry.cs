﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SIL.AARTO.COOAutomation.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICOOResultEnquiry" in both code and config file together.
    [ServiceContract]
    public interface ICOOResultEnquiry
    {
        [OperationContract]
        string COOResultEnquiry(string xmlRequest, string guid);
    }
}
