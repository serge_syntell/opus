﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using SIL.AARTO.COOAutomation.BLL;
using SIL.AARTO.COOAutomation.Model;
using SIL.AARTO.COOAutomation.Utility;

using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.COOAutomation.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "COOProxyEnquiry" in code, svc and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class COOProxyEnquiryService : ServiceBase, ICOOProxyEnquiry
    {
        public string COOProxyEnquiry(string xmlRequest, string guid)
        {
            ProxyEnquiryResponse response = new ProxyEnquiryResponse();
            if (!session.IsValid(guid))
            {
                response.ResponseCode = CooResponseCodeList.NO07.ToString();
                response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
            }
            else
            {
                var manager = new ProxyEnquiryManager(xmlRequest);
                response = manager.Enquiry();
            }
            return Formatter.Serialize(response);
        }

        public string COOProxyEnquiryDownload(string xmlRequest, string guid)
        {
            ProxyEnquiryDownloadResponse response = new ProxyEnquiryDownloadResponse();
            if (!session.IsValid(guid))
            {
                response.ResponseCode = CooResponseCodeList.NO07.ToString();
                response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
            }
            else
            {
                var manager = new ProxyEnquiryManager(xmlRequest);
                response = manager.EnquiryDownload();
            }
            return Formatter.Serialize(response);
        }
    }
}
