﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using SIL.AARTO.COOAutomation.Model;
using SIL.AARTO.COOAutomation.Utility;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.COOAutomation.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "COOBasicData" in code, svc and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class COOBasicDataService : ServiceBase, ICOOBasicData
    {
        public string GetCompanyList()
        {
            var comCollection = new CompanyCollection();
            try
            {
                var comList = new CooCompanyInformationService().GetAll();
                foreach (var com in comList)
                {
                    comCollection.Add(com.CciCompanyCode, com.CciCompanyName);
                }
            }
            catch {}
            return Formatter.Serialize(comCollection);
        }
    }
}
