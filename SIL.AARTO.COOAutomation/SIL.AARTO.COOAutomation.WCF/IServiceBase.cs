﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace SIL.AARTO.COOAutomation.WCF
{
    [ServiceContract(SessionMode = SessionMode.NotAllowed)]
    public interface IServiceBase {}
}
