﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using SIL.AARTO.COOAutomation.BLL;
using SIL.AARTO.COOAutomation.Model;
using SIL.AARTO.COOAutomation.Utility;

using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.COOAutomation.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "COOAccount" in code, svc and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class COOAccountService : ServiceBase, ICOOAccount
    {
        public string LogOn(string xmlRequest, string guid)
        {
            var userInfo = new ProxyInfo();
            var manager = new ProxyInfoManager(xmlRequest);
            if (manager.LogOn())
            {
                session.New(guid).Add(guid, manager.Entity);
            }
            userInfo = manager.Entity;
            return Formatter.Serialize(userInfo);
        }

        public string ChangePassword(string xmlRequest, string guid)
        {
            var proxyInfo = new ProxyInfo();
            if (!session.IsValid(guid))
            {
                proxyInfo.ResponseCode = CooResponseCodeList.NO07.ToString();
                proxyInfo.ResponseDescription = new CooResponseCodeService().GetByCrcCode(proxyInfo.ResponseCode).CrcDescription;
                return Formatter.Serialize(proxyInfo);
            }
            else
            {
                var manager = new ProxyInfoManager(xmlRequest);
                manager.ChangePassword();
                    
                return Formatter.Serialize(manager.Entity);
            }
        }

        public string ChangeInfo(string xmlRequest, string guid)
        {
            var proxyInfo = new ProxyInfo();
            if (!session.IsValid(guid))
            {
                proxyInfo.ResponseCode = CooResponseCodeList.NO07.ToString();
                proxyInfo.ResponseDescription = new CooResponseCodeService().GetByCrcCode(proxyInfo.ResponseCode).CrcDescription;
                return Formatter.Serialize(proxyInfo);
            }
            else
            {
                var manager = new ProxyInfoManager(xmlRequest);
                manager.ChangeInfo();

                return Formatter.Serialize(manager.Entity);
            }
        }
    }
}
