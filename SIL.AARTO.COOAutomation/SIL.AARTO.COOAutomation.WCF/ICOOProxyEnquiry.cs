﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SIL.AARTO.COOAutomation.WCF
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICOOProxyEnquiry" in both code and config file together.
	[ServiceContract]
    public interface ICOOProxyEnquiry : IServiceBase
	{
        [OperationContract]
        string COOProxyEnquiry(string xmlRequest, string guid);

        [OperationContract]
        string COOProxyEnquiryDownload(string xmlRequest, string guid);
	}
}
