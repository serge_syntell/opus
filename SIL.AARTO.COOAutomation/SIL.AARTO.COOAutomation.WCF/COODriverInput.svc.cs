﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using SIL.AARTO.COOAutomation.BLL;
using SIL.AARTO.COOAutomation.Model;
using SIL.AARTO.COOAutomation.Utility;

using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.COOAutomation.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "COODriverInput" in code, svc and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class COODriverInputService : ServiceBase, ICOODriverInput
    {
        public string COODriverInput(string xmlRequest, string guid)
        {
            var response = new CandidateDriversResponse();
            if (!session.IsValid(guid))
            {
                response.ResponseCode = CooResponseCodeList.NO07.ToString();
                response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
            }
            else
            {
                ProxyInfo proxy = (ProxyInfo)session.Current(guid).GetValue(guid);
                var manager = new CandidateDriverManager(xmlRequest, proxy);
                response = manager.DriverInput();
            }
            return Formatter.Serialize(response);
        }
    }
}
