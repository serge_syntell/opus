﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Configuration;
using System.Text.RegularExpressions;
using SIL.AARTO.COOAutomation.Model;
using SIL.AARTO.COOAutomation.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.COOAutomation.XSD;
using System.Data.SqlClient;

using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace SIL.AARTO.COOAutomation.BLL
{
    public class ResultEnquiryManager
    {
        public string XmlRequest { get; set; }
        public ResultEnquiryManager(string xmlRequest)
        {
            XmlRequest = xmlRequest;
        }

        CooCompanyInformationService companyService = new CooCompanyInformationService();
        CooCompanyProxyService proxyService = new CooCompanyProxyService();

        public ResultEnquiryResponse Enquiry()
        {
            ResultEnquiryResponse response = new ResultEnquiryResponse();

            try
            {
                var validation = new XmlRequestValidation();
                try
                {
                    validation.XmlDoc.LoadXml(XmlRequest);
                }
                catch(Exception ex)
                {
                    response.ResponseCode = CooResponseCodeList.NO02.ToString();
                    response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                    COOHistory.WriteCOOLog("ResultEnquiry", "Error", XmlRequest + ex.ToString());
                    return response;
                }

                COOHistory.WriteCOOHistoryFile(validation.XmlDoc, "Request");

                if (!validation.ValidateXSDVersion(XSDType.ResultEnquiry))
                {
                    response.ResponseCode = CooResponseCodeList.NO01.ToString();
                    response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                    return response;
                }

                response.XSDVersion = validation.XSDVersion;

                if (!validation.Validate(XSDType.ResultEnquiry))
                {
                    response.ResponseCode = CooResponseCodeList.NO02.ToString();
                    response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                    return response;
                }
                else
                {
                    ResultEnquiry request = Formatter.Deserialize<ResultEnquiry>(XmlRequest);

                    CooCompanyInformation company = companyService.GetByCciCompanyCode(request.CompanyCode.Trim()).FirstOrDefault();
                    if (company == null)
                    {
                        response.ResponseCode = CooResponseCodeList.NO03.ToString();
                        response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                        return response;
                    }
                    else
                    {
                        CooCompanyProxy proxy = proxyService.GetByCciIntNoCcpNumber(company.CciIntNo, request.ProxyID);
                        if (proxy == null)
                        {
                            response.ResponseCode = CooResponseCodeList.NO04.ToString();
                            response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                            return response;
                        }

                        if (proxy.CcpRevokedDate.HasValue)
                        {
                            response.ResponseCode = CooResponseCodeList.NO05.ToString();
                            response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                            return response;
                        }

                        int PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
                        DataSet ds = new DataSet();

                        if (!request.PageIndex.HasValue) request.PageIndex = 0;
                        ds = GetResultEnquiryByPage(proxy.CcpIntNo, request.Dateofoffence, request.PageIndex.Value, PageSize);
                        
                        ResultNotices resultNotices = new ResultNotices();
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            ResultNotice resultNotice = this.GetResultNotice(ds.Tables[0].Rows[i]);
                            resultNotices.ResultNoticeList.Add(resultNotice);
                        }
                        response.TotalCount = int.Parse(ds.Tables[1].Rows[0]["TotalRowCount"].ToString());
                        response.ResultNotices = resultNotices;

                        response.ResponseCode = CooResponseCodeList.NO00.ToString();
                        response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;

                        COOHistory.WriteCOOHistoryFile(Formatter.SerializeXML<ResultEnquiryResponse>(response), "Response");
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = CooResponseCodeList.NO08.ToString();
                response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;

                COOHistory.WriteCOOHistoryFile(Formatter.SerializeXML<ResultEnquiryResponse>(response), "Response");
                COOHistory.WriteCOOLog("ResultEnquiry", "Error", ex.ToString());
                return response;
            }
        }

        public DataSet GetResultEnquiryByPage(int CCPIntNo, DateTime OffenceDate, int PageIndex, int PageSize)
        {
            string mConstr = ConfigurationManager.ConnectionStrings["SIL.AARTO.DAL.Data.ConnectionString"].ToString();

            SqlDataAdapter sqlDa = new SqlDataAdapter();
            DataSet ds = new DataSet();

            sqlDa.SelectCommand = new SqlCommand();
            sqlDa.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDa.SelectCommand.CommandText = "COOGetResultEnquiryByPage";

            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterCCPIntNo = new SqlParameter("@CCPIntNo", SqlDbType.Int, 4);
            parameterCCPIntNo.Value = CCPIntNo;
            sqlDa.SelectCommand.Parameters.Add(parameterCCPIntNo);

            if (OffenceDate != DateTime.MinValue)
            {
                SqlParameter parameterOffenceDate = new SqlParameter("@OffenceDate", SqlDbType.DateTime);
                parameterOffenceDate.Value = OffenceDate;
                sqlDa.SelectCommand.Parameters.Add(parameterOffenceDate);
            }

            SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int, 4);
            parameterPageIndex.Value = PageIndex;
            sqlDa.SelectCommand.Parameters.Add(parameterPageIndex);

            SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int, 4);
            parameterPageSize.Value = PageSize;
            sqlDa.SelectCommand.Parameters.Add(parameterPageSize);

            sqlDa.Fill(ds);
            sqlDa.SelectCommand.Connection.Dispose();

            return ds;
        }

        private ResultNotice GetResultNotice(DataRow dr)
        {
            int NotIntNo = (int)dr["NotIntNo"];

            ResultNotice resultNotice = new ResultNotice();
            resultNotice.RegNumber = dr["NotRegNo"].ToString();
            resultNotice.NoticeNumber = dr["NotTicketNo"].ToString();
            resultNotice.Dateofoffence = DateTime.Parse(dr["NotOffenceDate"].ToString());
            resultNotice.OffAmt = GetTotalAmountDue(NotIntNo);
            resultNotice.IDNumber = dr["DrvIDNumber"].ToString().Trim();
            resultNotice.DriverSurname = dr["DrvSurname"].ToString().Trim();
            resultNotice.Telephone = dr["Telephone"].ToString();
            resultNotice.Email = dr["EmailAddress"].ToString();
            if (dr["ActionCode"].ToString() == "")
            {
                resultNotice.Result = "";
            }
            else
            {
                resultNotice.Result = (int)dr["ActionCode"] == (int)CooActionCodeList.AcceptedByOfficer ? "Accept" : "Reject";
            }
            resultNotice.DateLoaded = DateTime.Parse(dr["DateLoaded"].ToString());
            if (dr["DateActionOfficer"].ToString() != "")
            {
                resultNotice.DateActionByOfficer = DateTime.Parse(dr["DateActionOfficer"].ToString());
            }
            if (dr["DateCOOAutomationProcessed"].ToString() != "")
            {
                resultNotice.DateCooAutomaticProcessed = DateTime.Parse(dr["DateCOOAutomationProcessed"].ToString());
            }

            return resultNotice;
        }

        private string GetTotalAmountDue(int NotIntNo)
        {
            ChargeQuery query = new ChargeQuery();
            query.Append(ChargeColumn.NotIntNo, NotIntNo.ToString());
            TList<Charge> chargeList = new ChargeService().Find(query as IFilterParameterCollection);

            bool noAog = false;
            for (int i = 0; i < chargeList.Count; i++)
            {
                if (chargeList[i].ChgNoAog == "Y" && chargeList[i].ChgFineAmount == 999999 && chargeList[i].ChgRevFineAmount == 0)
                {
                    noAog = true;
                    break;
                }
            }

            if (noAog)
            {
                return "NAG";
            }
            else
            {
                decimal totalAmount = 0;
                for (int i = 0; i < chargeList.Count; i++)
                {
                    if (chargeList[i].ChgIsMain == true && chargeList[i].ChargeStatus < (int)ChargeStatusList.Completion && (decimal)chargeList[i].ChgRevFineAmount + chargeList[i].ChgContemptCourt > 0)
                    {
                        totalAmount += (decimal)chargeList[i].ChgRevFineAmount + chargeList[i].ChgContemptCourt;
                    }
                }
                return totalAmount.ToString();
            }
        }
    }
}
