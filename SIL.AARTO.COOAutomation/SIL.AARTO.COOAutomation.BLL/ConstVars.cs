﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.COOAutomation.BLL
{
    public class ConstVars
    {
        public const string PasswordPattern = @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,50}$";
        public const string EmailPattern = @"^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$";
    }
}