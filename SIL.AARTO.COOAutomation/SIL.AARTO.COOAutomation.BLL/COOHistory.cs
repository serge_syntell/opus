﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.COOAutomation.BLL
{
    public class COOHistory
    {
        public static void WriteCOOHistoryFile(XmlDocument xmlDoc, string type)
        {
            CooHistoryFile cooFile = new CooHistoryFile();
            cooFile.ChfType = type;
            cooFile.ChfxmlBody = xmlDoc.ChildNodes.Count > 1 ? xmlDoc.ChildNodes[1].OuterXml : xmlDoc.ChildNodes[0].OuterXml;
            cooFile.ChfCreateDate = DateTime.Now;
            cooFile.LastUser = "COOAutomation";

            cooFile = new CooHistoryFileService().Save(cooFile);
        }

        public static void WriteCOOLog(string logName, string category, string description)
        {
            AartoErrorLog log = new AartoErrorLog();
            log.AaProjectId = (int)AartoProjectList.COOAutomation;
            if (category == "Error")
            {
                log.AaErLogPriority = 5;
            }
            else
            {
                log.AaErLogPriority = 0;
            }
            log.AaErLogDate = DateTime.Now;
            log.AaErLogName = logName;
            log.AaErLogDescription = description;
            log.LastUser = "COOAutomation";
            log = new AartoErrorLogService().Save(log);
        }
    }
}
