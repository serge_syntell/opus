﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using SIL.AARTO.COOAutomation.Model;
using SIL.AARTO.COOAutomation.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.COOAutomation.XSD;
using System.Data.SqlClient;

namespace SIL.AARTO.COOAutomation.BLL
{
    public class CandidateDriverManager
    {
        public CandidateDriverManager(string xmlRequest, ProxyInfo proxy)
        {
            XmlRequest = xmlRequest;
            Proxy = proxy;
        }

        public ProxyInfo Proxy;
        public string XmlRequest { get; set; }

        public CandidateDriversResponse DriverInput()
        {
            CandidateDriversResponse response = new CandidateDriversResponse();

            try
            {
                var validation = new XmlRequestValidation();
                try
                {
                    validation.XmlDoc.LoadXml(XmlRequest);
                }
                catch (Exception ex)
                {
                    response.ResponseCode = CooResponseCodeList.NO02.ToString();
                    response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                    COOHistory.WriteCOOLog("DriverInput", "Error", XmlRequest + ex.ToString());
                    return response;
                }

                COOHistory.WriteCOOHistoryFile(validation.XmlDoc, "Request");

                if (!validation.ValidateXSDVersion(XSDType.CandidateDrivers))
                {
                    response.ResponseCode = CooResponseCodeList.NO01.ToString();
                    response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                    return response;
                }

                response.XSDVersion = validation.XSDVersion;

                if (!validation.Validate(XSDType.CandidateDrivers))
                {
                    response.ResponseCode = CooResponseCodeList.NO02.ToString();
                    response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                    return response;
                }
                else
                {
                    CandidateDrivers request = Formatter.Deserialize<CandidateDrivers>(XmlRequest);

                    if (request.CompanyCode != Proxy.CompanyCode)
                    {
                        response.ResponseCode = CooResponseCodeList.NO03.ToString();
                        response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                        return response;
                    }
                    if (request.ProxyID != Proxy.ProxyID)
                    {
                        response.ResponseCode = CooResponseCodeList.NO04.ToString();
                        response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                        return response;
                    }

                    int CCIIntNo = new CooCompanyInformationService().GetByCciCompanyCode(request.CompanyCode.Trim()).FirstOrDefault().CciIntNo;
                    //CooCompanyProxy proxy = new CooCompanyProxyService().GetByCcpNumber(request.ProxyID.Trim()).FirstOrDefault();
                    // 2014-09-26, Oscar changed
                    var proxy = new CooCompanyProxyService().GetByCciIntNoCcpNumber(CCIIntNo, request.ProxyID.Trim());

                    int CCPIntNo = proxy.CcpIntNo;

                    if (proxy.CcpRevokedDate.HasValue)
                    {
                        response.ResponseCode = CooResponseCodeList.NO05.ToString();
                        response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                        return response;
                    }

                    response.ResponseCode = CooResponseCodeList.NO00.ToString();
                    response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;

                    using (TransactionScope scope = new TransactionScope())
                    {
                        DateTime date = DateTime.Now;
                        CooCompanyAffidavit affidavit = SaveAffidavit(CCPIntNo, request.Affidavit, date);

                        for (int i = 0; i < request.Drivers.Count; i++)
                        {
                            CandidateDriverResponse driverResponse = Validate(request.Drivers[i], CCIIntNo, CCPIntNo);
                            if (driverResponse.ResponseCode == CooResponseCodeList.NO00.ToString())
                            {
                                SaveDriver(CCIIntNo, CCPIntNo, affidavit.CcaIntNo, request.Drivers[i], date);
                                COOHistory.WriteCOOLog("DriverInput", "General", "Driver upload success for NoticeNumber" + request.Drivers[i].NoticeNumber);
                            }
                            else
                            {
                                COOHistory.WriteCOOLog("DriverInput", "General", "Driver upload fail for NoticeNumber" + request.Drivers[i].NoticeNumber + driverResponse.ResponseDescription);
                            }
                            response.DriversResponse.Add(driverResponse);
                        }

                        scope.Complete();
                    }
                }

                COOHistory.WriteCOOHistoryFile(Formatter.SerializeXML<CandidateDriversResponse>(response), "Response");
                return response;
            }
            catch(Exception ex)
            {
                response.ResponseCode = CooResponseCodeList.NO08.ToString();
                response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;

                COOHistory.WriteCOOHistoryFile(Formatter.SerializeXML<CandidateDriversResponse>(response), "Response");
                COOHistory.WriteCOOLog("DriverInput", "Error", ex.ToString());
                return response;
            }
        }

        readonly CooCompanyProxyService ccpService = new CooCompanyProxyService();
        public CandidateDriverResponse Validate(CandidateDriver driver, int CCIIntNo, int CCPIntNo)
        {
            CandidateDriverResponse driverResponse = new CandidateDriverResponse();
            driverResponse.NoticeNumber = driver.NoticeNumber;
            driverResponse.ResponseCode = CooResponseCodeList.NO00.ToString();
            driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;

            if (string.IsNullOrWhiteSpace(driver.NoticeNumber))
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO09.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }

            Notice notice = new NoticeService().GetByNotTicketNo(driver.NoticeNumber.Trim()).FirstOrDefault();
            if (notice == null)
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO09.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }
            if (notice.NoticeStatus < 255 || notice.NoticeStatus >= 600 || notice.NoticeStatus == 410 || notice.NoticeStatus == 420 || notice.NoticeStatus == 450)
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO26.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }

            CooIdNotice idNotice = new CooIdNoticeService().GetByNotIntNo(notice.NotIntNo).FirstOrDefault();
            if(idNotice == null)
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO09.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }

            // 2014-09-29, Oscar added
            if (idNotice.CcpIntNo != CCPIntNo)
            {
                var ccpNumberMatched = string.Equals(this.ccpService.GetByCcpIntNo(idNotice.CcpIntNo).CcpNumber.Trim(),
                    this.ccpService.GetByCcpIntNo(CCPIntNo).CcpNumber.Trim(),
                    StringComparison.OrdinalIgnoreCase);

                driverResponse.ResponseCode = (ccpNumberMatched ? CooResponseCodeList.NO03 : CooResponseCodeList.NO04).ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }

            CooForCityApprovalQuery query = new CooForCityApprovalQuery();
            query.AppendEquals(CooForCityApprovalColumn.NotIntNo,notice.NotIntNo.ToString());
            query.AppendEquals(CooForCityApprovalColumn.CciIntNo,CCIIntNo.ToString());
            query.AppendEquals(CooForCityApprovalColumn.CcpIntNo,CCPIntNo.ToString());
            query.AppendEquals(CooForCityApprovalColumn.ActionCode, "4");

            TList<CooForCityApproval> list = new CooForCityApprovalService().Find(query as IFilterParameterCollection);
            if(list.Count > 0)
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO23.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }

            if (string.IsNullOrWhiteSpace(driver.RegNo))
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO10.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }
            if (notice.NotRegNo.Trim() != driver.RegNo.Trim())
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO10.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }

            long idNumber;
            if (driver.DriverIDNumber.Trim().Length != 13 || !long.TryParse(driver.DriverIDNumber.Trim(), out idNumber))
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO11.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }

            if (driver.DriverSurname.Trim().Length == 0)
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO12.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }

            if (driver.DriverForenames.Trim().Length == 0)
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO13.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }

            if (driver.DriverInitials.Trim().Length == 0)
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO14.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }

            if (driver.AddressPhysical1.Trim().Length == 0)
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO15.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }

            if (driver.PhysicalCode.Trim().Length == 0)
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO17.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }

            if (driver.AddressPostal1.Trim().Length == 0)
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO18.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }

            if (driver.PostalCode.Trim().Length == 0)
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO20.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }

            if (driver.Telephone.Trim().Length == 0)
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO21.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }

            if (driver.EmailAddress.Trim().Length == 0)
            {
                driverResponse.ResponseCode = CooResponseCodeList.NO22.ToString();
                driverResponse.ResponseDescription = new CooResponseCodeService().GetByCrcCode(driverResponse.ResponseCode).CrcDescription;
                return driverResponse;
            }
            
            return driverResponse;
        }

        private CooCompanyAffidavit SaveAffidavit(int CCPIntNo, byte[] affidavit, DateTime dateLoaded)
        {
            CooCompanyAffidavit aff = new CooCompanyAffidavitService().GetByCcpIntNo(CCPIntNo).FirstOrDefault();
            if (aff == null)
            {
                aff = new CooCompanyAffidavit();
                aff.CcpIntNo = CCPIntNo;
                aff.DateLoaded = dateLoaded;
                aff.Affidavit = affidavit;
                aff.IsAccepted = "N";
                aff.LastUser = new CooCompanyProxyService().GetByCcpIntNo(CCPIntNo).CcpNumber.Trim();
                aff = new CooCompanyAffidavitService().Save(aff);
            }
            else
            {
                aff.DateLoaded = dateLoaded;
                aff.Affidavit = affidavit;
                aff.IsAccepted = "N";
                aff.LastUser = new CooCompanyProxyService().GetByCcpIntNo(CCPIntNo).CcpNumber.Trim();
                aff = new CooCompanyAffidavitService().Save(aff);
            }

            return aff;
        }

        private void SaveDriver(int CCIIntNo, int CCPIntNo, int CCAIntNo, CandidateDriver driver, DateTime dateLoaded)
        {
            Notice notice = new NoticeService().GetByNotTicketNo(driver.NoticeNumber.Trim()).FirstOrDefault();

            CooForCityApprovalQuery query = new CooForCityApprovalQuery();
            query.AppendEquals(CooForCityApprovalColumn.NotIntNo, notice.NotIntNo.ToString());
            query.AppendEquals(CooForCityApprovalColumn.CciIntNo, CCIIntNo.ToString());
            query.AppendEquals(CooForCityApprovalColumn.CcpIntNo, CCPIntNo.ToString());
            TList<CooForCityApproval> list = new CooForCityApprovalService().Find(query as IFilterParameterCollection);
            if (list.Count == 0)
            {
                CooForCityApproval approval = new CooForCityApproval();
                approval.NotIntNo = notice.NotIntNo;
                approval.CciIntNo = CCIIntNo;
                approval.CcpIntNo = CCPIntNo;
                approval.CcaIntNo = CCAIntNo;
                approval.DateLoaded = dateLoaded;
                approval.DrvSurname = driver.DriverSurname.Trim();
                approval.DrvInitials = driver.DriverInitials.Trim();
                approval.DrvForenames = driver.DriverForenames.Trim();
                approval.DrvIdNumber = driver.DriverIDNumber.Trim();
                approval.RegNo = driver.RegNo.Trim();
                approval.PostalAddress1 = driver.AddressPostal1.Trim();
                approval.PostalAddress2 = driver.AddressPostal2.Trim();
                approval.PostalAddress3 = driver.AddressPostal3.Trim();
                approval.PostalAddress4 = driver.AddressPostal4.Trim();
                approval.PostalAddress5 = driver.AddressPostal5.Trim();
                approval.PostalCode = driver.PostalCode.Trim();
                approval.PhysicalAddress1 = driver.AddressPhysical1.Trim();
                approval.PhysicalAddress2 = driver.AddressPhysical2.Trim();
                approval.PhysicalAddress3 = driver.AddressPhysical3.Trim();
                approval.PhysicalAddress4 = driver.AddressPhysical4.Trim();
                approval.PhysicalCode = driver.PhysicalCode.Trim();
                approval.EmailAddress = driver.EmailAddress.Trim();
                approval.Telephone = driver.Telephone.Trim();
                approval.LastUser = new CooCompanyProxyService().GetByCcpIntNo(CCPIntNo).CcpNumber.Trim();

                approval = new CooForCityApprovalService().Save(approval);
            }
            if (list.Count == 1)
            {
                CooForCityApproval approval = list[0];
                approval.DateLoaded = dateLoaded;
                approval.DrvSurname = driver.DriverSurname.Trim();
                approval.DrvInitials = driver.DriverInitials.Trim();
                approval.DrvForenames = driver.DriverForenames.Trim();
                approval.DrvIdNumber = driver.DriverIDNumber.Trim();
                approval.RegNo = driver.RegNo.Trim();
                approval.PostalAddress1 = driver.AddressPostal1.Trim();
                approval.PostalAddress2 = driver.AddressPostal2.Trim();
                approval.PostalAddress3 = driver.AddressPostal3.Trim();
                approval.PostalAddress4 = driver.AddressPostal4.Trim();
                approval.PostalAddress5 = driver.AddressPostal5.Trim();
                approval.PostalCode = driver.PostalCode.Trim();
                approval.PhysicalAddress1 = driver.AddressPhysical1.Trim();
                approval.PhysicalAddress2 = driver.AddressPhysical2.Trim();
                approval.PhysicalAddress3 = driver.AddressPhysical3.Trim();
                approval.PhysicalAddress4 = driver.AddressPhysical4.Trim();
                approval.PhysicalCode = driver.PhysicalCode.Trim();
                approval.EmailAddress = driver.EmailAddress.Trim();
                approval.Telephone = driver.Telephone.Trim();
                approval.LastUser = new CooCompanyProxyService().GetByCcpIntNo(CCPIntNo).CcpNumber.Trim();

                approval.DateActionOfficer = null;
                approval.OfficerName = null;
                approval.ActionCode = null;

                approval = new CooForCityApprovalService().Save(approval);
            }
        }
    }
}
