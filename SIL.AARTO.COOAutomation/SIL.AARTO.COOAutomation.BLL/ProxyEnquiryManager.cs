﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Configuration;
using System.Text.RegularExpressions;
using SIL.AARTO.COOAutomation.Model;
using SIL.AARTO.COOAutomation.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.COOAutomation.XSD;
using System.Data.SqlClient;

using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace SIL.AARTO.COOAutomation.BLL
{
    public class ProxyEnquiryManager
    {
        public string XmlRequest { get; set; }
        public ProxyEnquiryManager(string xmlRequest)
        {
            XmlRequest = xmlRequest;
        }

        CooCompanyInformationService companyService = new CooCompanyInformationService();
        CooCompanyProxyService proxyService = new CooCompanyProxyService();

        public ProxyEnquiryResponse Enquiry()
        {
            ProxyEnquiryResponse response = new ProxyEnquiryResponse();

            try
            {
                var validation = new XmlRequestValidation();
                try
                {
                    validation.XmlDoc.LoadXml(XmlRequest);
                }
                catch(Exception ex)
                {
                    response.ResponseCode = CooResponseCodeList.NO02.ToString();
                    response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                    COOHistory.WriteCOOLog("ProxyEnquiry", "Error", XmlRequest + ex.ToString());
                    return response;
                }

                COOHistory.WriteCOOHistoryFile(validation.XmlDoc, "Request");

                if (!validation.ValidateXSDVersion(XSDType.ProxyEnquiry))
                {
                    response.ResponseCode = CooResponseCodeList.NO01.ToString();
                    response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                    return response;
                }

                response.XSDVersion = validation.XSDVersion;

                if (!validation.Validate(XSDType.ProxyEnquiry))
                {
                    response.ResponseCode = CooResponseCodeList.NO02.ToString();
                    response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                    return response;
                }
                else
                {
                    ProxyEnquiry request = Formatter.Deserialize<ProxyEnquiry>(XmlRequest);

                    CooCompanyInformation company = companyService.GetByCciCompanyCode(request.CompanyCode.Trim()).FirstOrDefault();
                    if (company == null)
                    {
                        response.ResponseCode = CooResponseCodeList.NO03.ToString();
                        response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                        return response;
                    }
                    else
                    {
                        CooCompanyProxy proxy = proxyService.GetByCciIntNoCcpNumber(company.CciIntNo, request.ProxyID);
                        if (proxy == null)
                        {
                            response.ResponseCode = CooResponseCodeList.NO04.ToString();
                            response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                            return response;
                        }

                        if (proxy.CcpRevokedDate.HasValue)
                        {
                            response.ResponseCode = CooResponseCodeList.NO05.ToString();
                            response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                            return response;
                        }

                        int PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
                        DataSet ds = new DataSet();

                        if (!request.PageIndex.HasValue) request.PageIndex = 0;
                        ds = GetProxyEnquiryByPage(proxy.CcpIntNo, request.PageIndex.Value, PageSize);

                        ProxyNotices notices = new ProxyNotices();
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            ProxyNotice notice = GetProxyNotice(ds.Tables[0].Rows[i], request.ProxyID);

                            notices.ProxyNoticeList.Add(notice);
                        }
                        response.TotalCount = int.Parse(ds.Tables[1].Rows[0]["TotalRowCount"].ToString());
                        response.ProxyNotices = notices;

                        response.ResponseCode = CooResponseCodeList.NO00.ToString();
                        response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;

                        COOHistory.WriteCOOHistoryFile(Formatter.SerializeXML<ProxyEnquiryResponse>(response), "Response");
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = CooResponseCodeList.NO08.ToString();
                response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;

                COOHistory.WriteCOOHistoryFile(Formatter.SerializeXML<ProxyEnquiryResponse>(response), "Response");
                COOHistory.WriteCOOLog("ProxyEnquiry", "Error", ex.ToString());
                return response;
            }
        }

        public ProxyEnquiryDownloadResponse EnquiryDownload()
        {
            ProxyEnquiryDownloadResponse response = new ProxyEnquiryDownloadResponse();

            try
            {
                var validation = new XmlRequestValidation();
                try
                {
                    validation.XmlDoc.LoadXml(XmlRequest);
                }
                catch(Exception ex)
                {
                    response.ResponseCode = CooResponseCodeList.NO02.ToString();
                    response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                    COOHistory.WriteCOOLog("EnquiryDownload", "Error", XmlRequest + ex.ToString());
                    return response;
                }

                COOHistory.WriteCOOHistoryFile(validation.XmlDoc, "Request");

                if (!validation.ValidateXSDVersion(XSDType.ProxyEnquiry))
                {
                    response.ResponseCode = CooResponseCodeList.NO01.ToString();
                    response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                    return response;
                }

                response.XSDVersion = validation.XSDVersion;

                if (!validation.Validate(XSDType.ProxyEnquiry))
                {
                    response.ResponseCode = CooResponseCodeList.NO02.ToString();
                    response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                    return response;
                }
                else
                {
                    ProxyEnquiry request = Formatter.Deserialize<ProxyEnquiry>(XmlRequest);

                    CooCompanyInformation company = companyService.GetByCciCompanyCode(request.CompanyCode.Trim()).FirstOrDefault();
                    if (company == null)
                    {
                        response.ResponseCode = CooResponseCodeList.NO03.ToString();
                        response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                        return response;
                    }
                    else
                    {
                        CooCompanyProxy proxy = proxyService.GetByCciIntNoCcpNumber(company.CciIntNo, request.ProxyID);
                        if (proxy == null)
                        {
                            response.ResponseCode = CooResponseCodeList.NO04.ToString();
                            response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                            return response;
                        }

                        if (proxy.CcpRevokedDate.HasValue)
                        {
                            response.ResponseCode = CooResponseCodeList.NO05.ToString();
                            response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;
                            return response;
                        }
                        DataSet ds = new DataSet();

                        ds = GetProxyEnquiry(proxy.CcpIntNo);

                        response.Excel = ExportToExcel(ds).GetBuffer();

                        response.ResponseCode = CooResponseCodeList.NO00.ToString();
                        response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;

                        COOHistory.WriteCOOHistoryFile(Formatter.SerializeXML<ProxyEnquiryDownloadResponse>(response), "Response");
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = CooResponseCodeList.NO08.ToString();
                response.ResponseDescription = new CooResponseCodeService().GetByCrcCode(response.ResponseCode).CrcDescription;

                COOHistory.WriteCOOHistoryFile(Formatter.SerializeXML<ProxyEnquiryDownloadResponse>(response), "Response");
                COOHistory.WriteCOOLog("EnquiryDownload", "Error", ex.ToString());
                return response;
            }
        }

        public DataSet GetProxyEnquiry(int CCPIntNo)
        {
            string mConstr = ConfigurationManager.ConnectionStrings["SIL.AARTO.DAL.Data.ConnectionString"].ToString();

            SqlDataAdapter sqlDa = new SqlDataAdapter();
            DataSet ds = new DataSet();

            sqlDa.SelectCommand = new SqlCommand();
            sqlDa.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDa.SelectCommand.CommandText = "COOGetProxyEnquiry";

            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterCCPIntNo = new SqlParameter("@CCPIntNo", SqlDbType.Int, 4);
            parameterCCPIntNo.Value = CCPIntNo;
            sqlDa.SelectCommand.Parameters.Add(parameterCCPIntNo);

            sqlDa.Fill(ds);
            sqlDa.SelectCommand.Connection.Dispose();

            return ds;
        }

        public DataSet GetProxyEnquiryByPage(int CCPIntNo, int PageIndex, int PageSize)
        {
            string mConstr = ConfigurationManager.ConnectionStrings["SIL.AARTO.DAL.Data.ConnectionString"].ToString();

            SqlDataAdapter sqlDa = new SqlDataAdapter();
            DataSet ds = new DataSet();

            sqlDa.SelectCommand = new SqlCommand();
            sqlDa.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDa.SelectCommand.CommandText = "COOGetProxyEnquiryByPage";

            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterCCPIntNo = new SqlParameter("@CCPIntNo", SqlDbType.Int, 4);
            parameterCCPIntNo.Value = CCPIntNo;
            sqlDa.SelectCommand.Parameters.Add(parameterCCPIntNo);

            SqlParameter parameterPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int, 4);
            parameterPageIndex.Value = PageIndex;
            sqlDa.SelectCommand.Parameters.Add(parameterPageIndex);

            SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int, 4);
            parameterPageSize.Value = PageSize;
            sqlDa.SelectCommand.Parameters.Add(parameterPageSize);

            sqlDa.Fill(ds);
            sqlDa.SelectCommand.Connection.Dispose();

            return ds;	
        }

        private ProxyNotice GetProxyNotice(DataRow dr, string ProxyID)
        {
            ProxyNotice proxyNotice = new ProxyNotice();
            int notIntNo = int.Parse(dr["NotIntNo"].ToString());

            Summons summons = GetSummons(notIntNo);
            Woa woa = GetWoa(summons);

            ChargeQuery query = new ChargeQuery();
            query.Append(ChargeColumn.NotIntNo, notIntNo.ToString());
            TList<Charge> chargeList = new ChargeService().Find(query as IFilterParameterCollection);

            bool noAog = false;
            for (int i = 0; i < chargeList.Count; i++)
            {
                if (chargeList[i].ChgNoAog == "Y" && chargeList[i].ChgFineAmount == 999999 && chargeList[i].ChgRevFineAmount == 0
                    && chargeList[i].ChargeStatus != 733 && chargeList[i].ChargeStatus != 741)
                {
                    noAog = true;
                    break;
                }
            }

            if (woa != null)
            {
                proxyNotice.NoticeType = "W";
            }
            else
            {
                if (summons != null)
                {
                    proxyNotice.NoticeType = "S";
                }
                else
                {
                    proxyNotice.NoticeType = "N";
                }
            }
            proxyNotice.IDNumber = ProxyID;
            proxyNotice.RegNumber = dr["NotRegNo"].ToString().Trim();
            proxyNotice.NoticeNumber = dr["NotTicketNo"].ToString();
            proxyNotice.Dateofoffence = DateTime.Parse(dr["NotOffenceDate"].ToString());

            if (noAog)
            {
                proxyNotice.OffAmt = "NAG";
            }
            else
            {
                proxyNotice.OffAmt = GetTotalAmountDue(chargeList).ToString();
            }

            if (summons != null)
            {
                proxyNotice.Court = new CourtService().GetByCrtIntNo(summons.CrtIntNo).CrtName.Trim();
                if (summons.SumCourtDate.HasValue)
                {
                    proxyNotice.CourtDate = summons.SumCourtDate.Value.ToString("yyyy-MM-dd");
                }
            }
            else
            {
                proxyNotice.Court = dr["NotCourtName"].ToString();
            }
            proxyNotice.StatusDescr = dr["CSDescr"].ToString();

            return proxyNotice;
        }

        private Summons GetSummons(int NotIntNo)
        {
            Summons summons = null;

            NoticeSummonsQuery noticesummonsQuery = new NoticeSummonsQuery();
            noticesummonsQuery.Append(NoticeSummonsColumn.NotIntNo, NotIntNo.ToString());
            TList<NoticeSummons> noticesummonsList = new NoticeSummonsService().Find(noticesummonsQuery as IFilterParameterCollection);

            if (noticesummonsList != null && noticesummonsList.Count > 0)
            {
                summons = new SummonsService().GetBySumIntNo(noticesummonsList[0].SumIntNo);
            }
            return summons;
        }

        private Woa GetWoa(Summons summons)
        {
            Woa woa = null;
            if (summons != null)
            {
                WoaQuery woaQuery = new WoaQuery();
                woaQuery.Append(WoaColumn.SumIntNo, summons.SumIntNo.ToString());
                TList<Woa> woaList = new WoaService().Find(woaQuery as IFilterParameterCollection);
                if (woaList != null && woaList.Count > 0)
                {
                    woa = woaList[0];
                }
            }
            return woa;
        }

        private decimal GetTotalAmountDue(TList<Charge> chargeList)
        {
            decimal totalAmount = 0;
            for (int i = 0; i < chargeList.Count; i++)
            {
                if (chargeList[i].ChgIsMain == true && chargeList[i].ChargeStatus < (int)ChargeStatusList.Completion && (decimal)chargeList[i].ChgRevFineAmount + chargeList[i].ChgContemptCourt > 0)
                {
                    totalAmount += (decimal)chargeList[i].ChgRevFineAmount + chargeList[i].ChgContemptCourt;
                }
            }
            return totalAmount;
        }

        HSSFWorkbook wb;
        ISheet sheet;
        public MemoryStream ExportToExcel(DataSet ds)
        {
            MemoryStream stream = new MemoryStream();

            wb = new HSSFWorkbook();
            sheet = wb.CreateSheet();
            wb.SetSheetName(0, "Proxy Enquiry");

            createHeadingsRow();

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                createDataRows(1 + i, 
                    ds.Tables[0].Rows[i]["NotRegNo"].ToString(), 
                    ds.Tables[0].Rows[i]["NotTicketNo"].ToString(), 
                    DateTime.Parse(ds.Tables[0].Rows[i]["NotOffenceDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss"),
                    ds.Tables[0].Rows[i]["CCPNumber"].ToString(),
                    ds.Tables[0].Rows[i]["ChgFineAmount"].ToString(),
                    ds.Tables[0].Rows[i]["NotCourtName"].ToString(),
                    "",
                    ds.Tables[0].Rows[i]["CSDescr"].ToString()
                );
            }

            for (int i = 0; i < 3; i++)
            {
                sheet.AutoSizeColumn((short)i);
            }

            wb.Write(stream);
            return stream;
        }

        private void createHeadingsRow()
        {
            sheet.CreateRow(0);
            IFont fontHeading = wb.CreateFont();
            fontHeading.Boldweight = (short)FontBoldWeight.BOLD;
            ICellStyle styleHeading = wb.CreateCellStyle();
            styleHeading.Alignment = HorizontalAlignment.CENTER;
            styleHeading.VerticalAlignment = VerticalAlignment.CENTER;

            ICell cell0 = sheet.GetRow(0).CreateCell(0);
            cell0.CellStyle = styleHeading;
            HSSFRichTextString rtf0 = new HSSFRichTextString("ID Number");
            rtf0.ApplyFont(fontHeading);
            cell0.SetCellValue(rtf0);

            ICell cell1 = sheet.GetRow(0).CreateCell(1);
            cell1.CellStyle = styleHeading;
            HSSFRichTextString rtf1 = new HSSFRichTextString("Registration");
            rtf1.ApplyFont(fontHeading);
            cell1.SetCellValue(rtf1);

            ICell cell2 = sheet.GetRow(0).CreateCell(2);
            cell2.CellStyle = styleHeading;
            HSSFRichTextString rtf2 = new HSSFRichTextString("Ticket No.");
            rtf2.ApplyFont(fontHeading);
            cell2.SetCellValue(rtf2);

            ICell cell3 = sheet.GetRow(0).CreateCell(3);
            cell3.CellStyle = styleHeading;
            HSSFRichTextString rtf3 = new HSSFRichTextString("Offence Date");
            rtf3.ApplyFont(fontHeading);
            cell3.SetCellValue(rtf3);

            ICell cell4 = sheet.GetRow(0).CreateCell(4);
            cell4.CellStyle = styleHeading;
            HSSFRichTextString rtf4 = new HSSFRichTextString("Amount");
            rtf4.ApplyFont(fontHeading);
            cell4.SetCellValue(rtf4);

            ICell cell5 = sheet.GetRow(0).CreateCell(5);
            cell5.CellStyle = styleHeading;
            HSSFRichTextString rtf5 = new HSSFRichTextString("Court");
            rtf5.ApplyFont(fontHeading);
            cell5.SetCellValue(rtf5);

            ICell cell6 = sheet.GetRow(0).CreateCell(6);
            cell6.CellStyle = styleHeading;
            HSSFRichTextString rtf6 = new HSSFRichTextString("Court Date");
            rtf6.ApplyFont(fontHeading);
            cell6.SetCellValue(rtf6);

            ICell cell7 = sheet.GetRow(0).CreateCell(7);
            cell7.CellStyle = styleHeading;
            HSSFRichTextString rtf7 = new HSSFRichTextString("Status Description");
            rtf7.ApplyFont(fontHeading);
            cell7.SetCellValue(rtf7);
        }

        private void createDataRows(int rowIndex, string RegNo, string TicketNo, string OffenceDate, string IDNumber, string Amount, string court, string CourtDate, string StatusDesc)
        {
            sheet.CreateRow(rowIndex);

            ICell cell0 = sheet.GetRow(rowIndex).CreateCell(0);
            cell0.SetCellValue(IDNumber);

            ICell cell1 = sheet.GetRow(rowIndex).CreateCell(1);
            cell1.SetCellValue(RegNo);

            ICell cell2 = sheet.GetRow(rowIndex).CreateCell(2);
            cell2.SetCellValue(TicketNo);

            ICell cell3 = sheet.GetRow(rowIndex).CreateCell(3);
            cell3.SetCellValue(OffenceDate);

            ICell cell4 = sheet.GetRow(rowIndex).CreateCell(4);
            cell4.SetCellValue(Amount);

            ICell cell5 = sheet.GetRow(rowIndex).CreateCell(5);
            cell5.SetCellValue(court);

            ICell cell6 = sheet.GetRow(rowIndex).CreateCell(6);
            cell6.SetCellValue(CourtDate);

            ICell cell7 = sheet.GetRow(rowIndex).CreateCell(7);
            cell7.SetCellValue(StatusDesc);

        }
    }
}
