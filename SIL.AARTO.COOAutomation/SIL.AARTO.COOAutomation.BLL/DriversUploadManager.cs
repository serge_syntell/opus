﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using SIL.AARTO.COOAutomation.Model;
using SIL.AARTO.COOAutomation.Utility;
using SIL.AARTO.COOAutomation.XSD;

using SIL.AARTO.DAL.Data;

namespace SIL.AARTO.COOAutomation.BLL
{
    public class DriversUploadManager
    {
        public DriversUploadManager(HttpFileCollectionBase files)
        {
            this.files = files;
        }

        HttpFileCollectionBase files;

        public string ErrorMessage { get; private set; }
        public string RequestXml { get; private set; }
        public CandidateDrivers Drivers { get; private set; }

        public bool Validate()
        {
            try
            {
                if (this.files == null
                    || this.files.Count != 2
                    || this.files[0].ContentLength <= 0
                    || this.files[1].ContentLength <= 0
                    )
                {
                    ErrorMessage = "You have to select 2 files, xml and image.";
                    return false;
                }

                if (!ValidateImage()) return false;

                var xml = string.Empty;
                using (var sr = new StreamReader(this.files[1].InputStream))
                {
                    xml = sr.ReadToEnd();
                }
                this.files[1].InputStream.Close();

                Drivers = Formatter.Deserialize<CandidateDrivers>(xml);

                var image = new byte[this.files[0].InputStream.Length];
                this.files[0].InputStream.Position = 0;
                this.files[0].InputStream.Read(image, 0, image.Length);
                this.files[0].InputStream.Close();

                Drivers.Affidavit = image;
                RequestXml = Formatter.Serialize(Drivers);

                if (!ValidateXml()) return false;

                return true;
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return false;
            }
        }

        bool ValidateXml()
        {
            var xmlValidation = new XmlRequestValidation();
            try
            {
                xmlValidation.XmlDoc.LoadXml(RequestXml);
            }
            catch
            {
                ErrorMessage = "Invalid XML Document";
                return false;
            }
            if (!xmlValidation.ValidateXSDVersion(XSDType.CandidateDrivers))
            {
                ErrorMessage = "Invalid XSDVersion";
                return false;
            }
            if (!xmlValidation.Validate(XSDType.CandidateDrivers))
            {
                ErrorMessage = "Invalid XML Document";
                return false;
            }
            return true;
        }

        bool ValidateImage()
        {
            try
            {
                Image.FromStream(this.files[0].InputStream);
            }
            catch
            {
                ErrorMessage = "Please select a valid image";
                return false;
            }
            return true;
        }
    }
}
