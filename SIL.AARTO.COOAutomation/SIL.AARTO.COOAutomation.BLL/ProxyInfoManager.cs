﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using SIL.AARTO.COOAutomation.Model;
using SIL.AARTO.COOAutomation.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.COOAutomation.XSD;

namespace SIL.AARTO.COOAutomation.BLL
{
    public class ProxyInfoManager
    {
        public ProxyInfoManager(string xmlRequest)
        {
            XmlRequest = xmlRequest;
        }

        public string XmlRequest { get; set; }
        public ProxyInfo Entity { get; private set; }

        CooCompanyInformationService companyService = new CooCompanyInformationService();
        CooCompanyProxyService proxyService = new CooCompanyProxyService();

        public bool LogOn()
        {
            try
            {
                var validation = new XmlRequestValidation();
                Entity = new ProxyInfo();
                try
                {
                    validation.XmlDoc.LoadXml(XmlRequest);
                }
                catch
                {
                    Entity.ResponseCode = CooResponseCodeList.NO02.ToString();
                    Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                    return false;
                }

                COOHistory.WriteCOOHistoryFile(validation.XmlDoc, "Request");

                if (!validation.ValidateXSDVersion(XSDType.ProxyInfo))
                {
                    Entity.ResponseCode = CooResponseCodeList.NO01.ToString();
                    Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                    return false;
                }

                Entity.XSDVersion = validation.XSDVersion;

                if (!validation.Validate(XSDType.ProxyInfo))
                {
                    Entity.ResponseCode = CooResponseCodeList.NO02.ToString();
                    Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                    return false;
                }
                else
                {
                    Entity = Formatter.Deserialize<ProxyInfo>(XmlRequest);

                    CooCompanyInformation company = companyService.GetByCciCompanyCode(Entity.CompanyCode.Trim()).FirstOrDefault();
                    if (company == null)
                    {
                        Entity.ResponseCode = CooResponseCodeList.NO03.ToString();
                        Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                        return false;
                    }
                    else
                    {
                        CooCompanyProxy proxy = proxyService.GetByCciIntNoCcpNumber(company.CciIntNo, Entity.ProxyID);
                        if (proxy == null)
                        {
                            Entity.ResponseCode = CooResponseCodeList.NO04.ToString();
                            Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                            return false;
                        }

                        TList<CooCompanyProxy> proxyList = proxyService.GetByCciIntNoCcpNumberCcpPassword(company.CciIntNo, Entity.ProxyID, Encrypt.HashPassword(Entity.Password));
                        if (proxyList.Count <= 0)
                        {
                            Entity.ResponseCode = CooResponseCodeList.NO24.ToString();
                            Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                            return false;
                        }

                        if (proxyList[0].CcpRevokedDate.HasValue)
                        {
                            Entity.ResponseCode = CooResponseCodeList.NO05.ToString();
                            Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                            return false;
                        }

                        Entity.EmailAddress = proxyList[0].CcpEmailAddress.Trim();

                        Entity.ResponseCode = CooResponseCodeList.NO00.ToString();
                        Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;

                        COOHistory.WriteCOOHistoryFile(Formatter.SerializeXML<ProxyInfo>(Entity), "Response");
                        return true;
                    }
                }
            }
            catch(Exception ex)
            {
                Entity.ResponseCode = CooResponseCodeList.NO08.ToString();
                Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;

                COOHistory.WriteCOOHistoryFile(Formatter.SerializeXML<ProxyInfo>(Entity), "Response");
                COOHistory.WriteCOOLog("LogOn", "Error", ex.ToString());
                return false;
            }
        }

        public bool ChangePassword()
        {
            try
            {
                var validation = new XmlRequestValidation();
                Entity = new ProxyInfo();
                try
                {
                    validation.XmlDoc.LoadXml(XmlRequest);
                }
                catch
                {
                    Entity.ResponseCode = CooResponseCodeList.NO02.ToString();
                    Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                    return false;
                }

                COOHistory.WriteCOOHistoryFile(validation.XmlDoc, "Request");

                if (!validation.ValidateXSDVersion(XSDType.ProxyInfo))
                {
                    Entity.ResponseCode = CooResponseCodeList.NO01.ToString();
                    Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                    return false;
                }

                Entity.XSDVersion = validation.XSDVersion;

                if (!validation.Validate(XSDType.ProxyInfo))
                {
                    Entity.ResponseCode = CooResponseCodeList.NO02.ToString();
                    Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                    return false;
                }
                else
                {
                    Entity = Formatter.Deserialize<ProxyInfo>(XmlRequest);

                    CooCompanyInformation company = companyService.GetByCciCompanyCode(Entity.CompanyCode.Trim()).FirstOrDefault();
                    if (company == null)
                    {
                        Entity.ResponseCode = CooResponseCodeList.NO03.ToString();
                        Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                        return false;
                    }
                    else
                    {
                        if (Entity.Password == Entity.PasswordNew)
                        {
                            Entity.ResponseCode = CooResponseCodeList.NO25.ToString();
                            Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                            return false;
                        }

                        TList<CooCompanyProxy> proxyList = proxyService.GetByCciIntNoCcpNumberCcpPassword(company.CciIntNo, Entity.ProxyID, Encrypt.HashPassword(Entity.Password));
                        if (proxyList.Count <= 0)
                        {
                            Entity.ResponseCode = CooResponseCodeList.NO24.ToString();
                            Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                            return false;
                        }

                        if (proxyList[0].CcpRevokedDate.HasValue)
                        {
                            Entity.ResponseCode = CooResponseCodeList.NO05.ToString();
                            Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                            return false;
                        }

                        var proxy = proxyList[0];
                        proxyList = proxyService.GetByCcpNumber(Entity.ProxyID);
                        for (int i = 0; i < proxyList.Count; i++)
                        {
                            proxy = proxyList[i];
                            proxy.CcpPassword = Encrypt.HashPassword(Entity.PasswordNew);
                            proxy = proxyService.Save(proxy);
                        }

                        Entity.ResponseCode = CooResponseCodeList.NO00.ToString();
                        Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;

                        COOHistory.WriteCOOHistoryFile(Formatter.SerializeXML<ProxyInfo>(Entity), "Response");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Entity.ResponseCode = CooResponseCodeList.NO08.ToString();
                Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;

                COOHistory.WriteCOOHistoryFile(Formatter.SerializeXML<ProxyInfo>(Entity), "Response");
                COOHistory.WriteCOOLog("ChangePassword", "Error", ex.ToString());
                return false;
            }
        }

        public bool ChangeInfo()
        {
            try
            {
                var validation = new XmlRequestValidation();
                Entity = new ProxyInfo();
                try
                {
                    validation.XmlDoc.LoadXml(XmlRequest);
                }
                catch
                {
                    Entity.ResponseCode = CooResponseCodeList.NO02.ToString();
                    Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                    return false;
                }

                COOHistory.WriteCOOHistoryFile(validation.XmlDoc, "Request");

                if (!validation.ValidateXSDVersion(XSDType.ProxyInfo))
                {
                    Entity.ResponseCode = CooResponseCodeList.NO01.ToString();
                    Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                    return false;
                }

                Entity.XSDVersion = validation.XSDVersion;

                if (!validation.Validate(XSDType.ProxyInfo))
                {
                    Entity.ResponseCode = CooResponseCodeList.NO02.ToString();
                    Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                    return false;
                }
                else
                {
                    Entity = Formatter.Deserialize<ProxyInfo>(XmlRequest);

                    CooCompanyInformation company = companyService.GetByCciCompanyCode(Entity.CompanyCode.Trim()).FirstOrDefault();
                    if (company == null)
                    {
                        Entity.ResponseCode = CooResponseCodeList.NO03.ToString();
                        Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                        return false;
                    }
                    else
                    {
                        CooCompanyProxy proxy = proxyService.GetByCciIntNoCcpNumber(company.CciIntNo, Entity.ProxyID);
                        if (proxy == null)
                        {
                            Entity.ResponseCode = CooResponseCodeList.NO04.ToString();
                            Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                            return false;
                        }

                        if (proxy.CcpRevokedDate.HasValue)
                        {
                            Entity.ResponseCode = CooResponseCodeList.NO05.ToString();
                            Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;
                            return false;
                        }
                        TList<CooCompanyProxy> proxyList = proxyService.GetByCcpNumber(Entity.ProxyID);
                        for (int i = 0; i < proxyList.Count; i++)
                        {
                            proxy = proxyList[i];
                            proxy.CcpEmailAddress = Entity.EmailAddress.Trim();
                            proxy = proxyService.Save(proxy);
                        }

                        Entity.ResponseCode = CooResponseCodeList.NO00.ToString();
                        Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;

                        COOHistory.WriteCOOHistoryFile(Formatter.SerializeXML<ProxyInfo>(Entity), "Response");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Entity.ResponseCode = CooResponseCodeList.NO08.ToString();
                Entity.ResponseDescription = new CooResponseCodeService().GetByCrcCode(Entity.ResponseCode).CrcDescription;

                COOHistory.WriteCOOHistoryFile(Formatter.SerializeXML<ProxyInfo>(Entity), "Response");
                COOHistory.WriteCOOLog("ChangeInfo", "Error", ex.ToString());
                return false;
            }
        }
    }
}
