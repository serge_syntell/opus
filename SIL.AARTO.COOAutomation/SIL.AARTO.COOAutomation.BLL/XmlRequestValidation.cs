﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using SIL.AARTO.COOAutomation.Model;
using SIL.AARTO.COOAutomation.Utility;
using SIL.AARTO.COOAutomation.XSD;

namespace SIL.AARTO.COOAutomation.BLL
{
    public class XmlRequestValidation
    {
        public XmlRequestValidation()
        {
            XmlDoc = new XmlDocument();
        }
        int xsdVersion;

        public XmlDocument XmlDoc { get; protected set; }
        public int XSDVersion
        {
            get { return this.xsdVersion; }
        }

        public bool ValidateXSDVersion(XSDType xsdType)
        {
            string xsdVersion;
            if (!XSDHelper.CheckXSDVersion(XmlDoc, xsdType, out xsdVersion)
                || !int.TryParse(xsdVersion, out this.xsdVersion)
                || this.xsdVersion <= 0
                )
            {
                return false;
            }

            return true;
        }

        public bool Validate(XSDType xsdType)
        {
            try
            {   
                var errorMsg = XSDHelper.XSDValidation(XmlDoc, xsdType);
                if (!string.IsNullOrWhiteSpace(errorMsg))
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
