﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.COOAutomation.Model
{
    [Serializable]
    public class ProxyEnquiry
    {
        public string RequestType { get; set; }
        public string CompanyCode { get; set; }
        public string ProxyID { get; set; }
        public int? PageIndex { get; set; }
        public int XSDVersion { get; set; }
    }

    [Serializable]
    public class ProxyEnquiryResponse
    {
        public ProxyNotices ProxyNotices { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseDescription { get; set; }
        public int TotalCount { get; set; }
        public int XSDVersion { get; set; } 

        public ProxyEnquiryResponse()
        {
            ProxyNotices = new ProxyNotices();
        }
    }

    [Serializable]
    public class ProxyNotices
    {
        public List<ProxyNotice> ProxyNoticeList { get; set; }

        public ProxyNotices()
        {
            ProxyNoticeList = new List<ProxyNotice>();
        }
    }

    [Serializable]
    public class ProxyNotice
    {
        public string NoticeType { get; set; }
        public string IDNumber { get; set; }
        public string RegNumber { get; set; }
        public string NoticeNumber { get; set; }
        public DateTime Dateofoffence { get; set; }
        public string OffAmt { get; set; }
        public string Court { get; set; }
        public string CourtDate { get; set; }
        public string StatusDescr { get; set; }
    }

    [Serializable]
    public class ProxyEnquiryDownloadResponse
    {
        public byte[] Excel { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseDescription { get; set; }
        public int XSDVersion { get; set; }
    }
}
