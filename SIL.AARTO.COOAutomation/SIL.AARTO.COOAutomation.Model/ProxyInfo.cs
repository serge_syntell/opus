﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.COOAutomation.Model
{
    [Serializable]
    public class ProxyInfo
    {
        public string CompanyCode { get; set; }
        public string ProxyID { get; set; }
        public string Password { get; set; }
        public string PasswordNew { get; set; }
        public string EmailAddress { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseDescription { get; set; }
        public int XSDVersion { get; set; }
    }
}
