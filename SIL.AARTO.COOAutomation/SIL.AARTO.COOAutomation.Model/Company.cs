﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.COOAutomation.Model
{
    [Serializable]
    public class Company
    {
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
    }

    [Serializable]
    public class CompanyCollection
    {
        public CompanyCollection()
        {
            Companies = new List<Company>();
        }

        public List<Company> Companies { get; set; }

        public void Add(string companyCode, string companyName)
        {
            Companies.Add(new Company { CompanyCode = companyCode, CompanyName = companyName });
        }
    }
}
