﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SIL.AARTO.COOAutomation.Model
{
    [Serializable]
    public class CandidateDriver
    {
        public string NoticeNumber { get; set; }
        public string RegNo { get; set; }
        public string DriverIDNumber { get; set; }
        public string DriverSurname { get; set; }
        public string DriverInitials { get; set; }
        public string DriverForenames { get; set; }
        public string AddressPhysical1 { get; set; }
        public string AddressPhysical2 { get; set; }
        public string AddressPhysical3 { get; set; }
        public string AddressPhysical4 { get; set; }
        public string PhysicalCode { get; set; }
        public string AddressPostal1 { get; set; }
        public string AddressPostal2 { get; set; }
        public string AddressPostal3 { get; set; }
        public string AddressPostal4 { get; set; }
        public string AddressPostal5 { get; set; }
        public string PostalCode { get; set; }
        public string EmailAddress { get; set; }
        public string Telephone { get; set; }
    }

    [Serializable]
    public class CandidateDrivers
    {
        public CandidateDrivers()
        {
            Drivers = new List<CandidateDriver>();
        }

        public string CompanyCode { get; set; }
        public string ProxyID { get; set; }
        public byte[] Affidavit { get; set; }
        public int XSDVersion { get; set; }
        public List<CandidateDriver> Drivers { get; set; }
    }

    [Serializable]
    public class CandidateDriversResponse
    {
        public CandidateDriversResponse()
        {
            DriversResponse = new List<CandidateDriverResponse>();
        }

        public string ResponseCode { get; set; }
        public string ResponseDescription { get; set; }
        public int XSDVersion { get; set; }
        public List<CandidateDriverResponse> DriversResponse { get; set; }
    }

    [Serializable]
    public class CandidateDriverResponse
    {
        public string NoticeNumber { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseDescription { get; set; }
    }
}
