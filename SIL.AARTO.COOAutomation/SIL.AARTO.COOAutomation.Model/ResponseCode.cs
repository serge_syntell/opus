﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.COOAutomation.Model
{
    public enum ResponseCode
    {
        Failed = 100,
        Successful = 101,

        SessionTimeout = 102,
        InvalidCompany = 103,
        InvalidProxyIDNumber = 104,
        InvalidProxyPassword = 105,
        InvalidNewProxyPassword = 106,
        LogOnFailed = 107,
        RevokedProxy = 108,

        XmlInvalidDocument = 200,
        XmlInvalidXSDVersion = 201,
        XmlValidationFailed = 202,

        InvalidRegNo = 300,
        InvalidDriverIDNumber = 301,
        InvalidDriverSurname = 302,
        InvalidDriverInitials = 303,
        InvalidDriverForenames = 304,
        InvalidAddressPhysical1 = 305,
        InvalidAddressPhysical2 = 306,
        InvalidAddressPhysical3 = 307,
        InvalidAddressPhysical4 = 308,
        InvalidAddressPostal1 = 309,
        InvalidAddressPostal2 = 310,
        InvalidAddressPostal3 = 311,
        InvalidAddressPostal4 = 312,
        InvalidAddressPostal5 = 313,
        InvalidEmailAddress = 314,
        InvalidTelephone = 315,
        InvalidPhysicalCode = 316,
        InvalidPostalCode = 317
    }
}