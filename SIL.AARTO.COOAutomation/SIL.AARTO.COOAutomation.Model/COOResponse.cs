﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.COOAutomation.Utility;

namespace SIL.AARTO.COOAutomation.Model
{
    [Serializable]
    public class COOResponse
    {
        public COOResponse()
        {
            ResponseCodes = new List<COOResponseCode>();
        }

        public bool Status
        {
            get
            {
                return ResponseCodes.Count == 0;
            }
            set { }
        }
        public List<COOResponseCode> ResponseCodes { get; set; }


        public virtual void Add(ResponseCode? code = null, string msg = null)
        {
            var responseCode = new COOResponseCode();
            if (!string.IsNullOrWhiteSpace(msg))
            {
                responseCode.ResponseCode = code.HasValue ? (int)code.Value : (int)ResponseCode.Failed;
                responseCode.ResponseMessage = msg;
            }
            else if (code.HasValue)
            {
                responseCode.ResponseCode = (int)code.Value;
                responseCode.ResponseMessage = code.Value.ToString();
            }
            else
            {
                responseCode.ResponseCode = (int)ResponseCode.Successful;
                responseCode.ResponseMessage = ResponseCode.Successful.ToString();
            }

            ResponseCodes.Add(responseCode);
        }

        public virtual string GetResponseXml()
        {
            return Formatter.Serialize(this);
        }

        public virtual string GetResponseMessage(string newLine = null)
        {
            if (string.IsNullOrWhiteSpace(newLine))
                newLine = Environment.NewLine;

            var sb = new StringBuilder();
            for (int i = 0; i < ResponseCodes.Count; i++)
            {
                var s = ResponseCodes[i];
                sb.Append(s.ResponseMessage);
                if (i != ResponseCodes.Count - 1)
                    sb.Append(newLine);
            }
            return sb.ToString();
        }
    }

    public class COOResponseCode
    {
        public int ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
    }
}
