﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.COOAutomation.Model
{
    [Serializable]
    public class ResultEnquiry
    {
        public string CompanyCode { get; set; }
        public string ProxyID { get; set; }
        public DateTime Dateofoffence { get; set; }
        public int? PageIndex { get; set; }
        public int XSDVersion { get; set; }
    }

    [Serializable]
    public class ResultEnquiryResponse
    {
        public ResultNotices ResultNotices { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseDescription { get; set; }
        public int TotalCount { get; set; }
        public int XSDVersion { get; set; }

        public ResultEnquiryResponse()
        {
            ResultNotices = new ResultNotices();
        }
    }

    [Serializable]
    public class ResultNotices
    {
        public List<ResultNotice> ResultNoticeList { get; set; }

        public ResultNotices()
        {
            ResultNoticeList = new List<ResultNotice>();
        }
    }

    [Serializable]
    public class ResultNotice
    {
        public string RegNumber { get; set; }
        public string NoticeNumber { get; set; }
        public DateTime Dateofoffence { get; set; }
        public string OffAmt { get; set; }
        public string IDNumber { get; set; }
        public string DriverSurname { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Result { get; set; }
        public DateTime DateLoaded { get; set; }
        public DateTime? DateActionByOfficer { get; set; }
        public DateTime? DateCooAutomaticProcessed { get; set; }
    }
}
