﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using SIL.QueueLibrary;
using System.Transactions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.ServiceQueueLibrary.DAL.Entities;

namespace SIL.AARTO.BLL.Infringement
{
    public class InfringementManager
    {
        CisInfringementResubmissionService resubmissionService = new CisInfringementResubmissionService();
        CisInfringementResubmissionErrorsService resubmissionErrorService = new CisInfringementResubmissionErrorsService();
        public SIL.AARTO.DAL.Entities.TList<CisInfringementResubmission> GetResubmission(string notTicketNo, int pageIndex, int pageSize, out int totalCount)
        {
            totalCount = 0;
            SIL.AARTO.DAL.Entities.TList<CisInfringementResubmission> returnList = new SIL.AARTO.DAL.Entities.TList<CisInfringementResubmission>();
            string whereClause = " CIRSubmitDate IS NULL ";
            if (!String.IsNullOrEmpty(notTicketNo))
            {
                whereClause += String.Format(" AND NotTicketNo ='{0}'", notTicketNo);
            }

            returnList = resubmissionService.GetPaged(whereClause, "NotTicketNo", pageIndex, pageSize, out totalCount);

            resubmissionService.DeepLoad(returnList,
                true,
                DAL.Data.DeepLoadType.IncludeChildren,
                new Type[] { typeof(SIL.AARTO.DAL.Entities.TList<CisInfringementResubmissionErrors>),
                        typeof(CisResponseCode)});

            return returnList;

        }

        public bool ResubmitInfringement(int notIntNo, int creIntNo, string lastUser)
        {
            CisInfringementResubmission resubmission = null;
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    //SIL.AARTO.DAL.Entities.TList<CisInfringementResubmission> resubmissionList = resubmissionService.GetByNotIntNo(notIntNo);

                    resubmission = resubmissionService.GetByNotIntNo(notIntNo).FirstOrDefault();

                    CisInfringementResubmissionErrors resubmissionError = resubmissionErrorService.GetByCireIntNo(creIntNo);

                    if (resubmissionError != null)
                    {
                        resubmissionError.CireSubmitDate = DateTime.Now;
                        resubmissionError.LastUser = lastUser;

                        resubmissionError = resubmissionErrorService.Save(resubmissionError);
                    }

                    int count = 0;
                    //foreach (CisInfringementResubmission r in resubmissionList)
                    //{
                    SIL.AARTO.DAL.Entities.TList<CisInfringementResubmissionErrors> errors = resubmissionErrorService.GetByCirIntNo(resubmission.CirIntNo);
                    count = errors.Count;

                    if (errors.Where(e => e.CireSubmitDate.HasValue.Equals(true)).ToList().Count == count)
                    {
                        resubmission.CirSubmitDate = DateTime.Now;
                        resubmission.LastUser = lastUser;
                        resubmission = resubmissionService.Save(resubmission);

                        QueueItemProcessor processor = new QueueItemProcessor();

                        processor.Send(new QueueItem[] { 
                        new QueueItem(){
                         Body=notIntNo,
                          QueueType=ServiceQueueTypeList.SubmitInfringementRecord
                        }
                    });
                    }
                    //}

                    tran.Complete();

                    return true;
                }
            }
            catch (SqlException ex)
            {
                throw new Exception(String.Format(
                    "There is en error occured when processing Infringement Resubmission, Infringement Number: {0}, Exception Info: {1} ",
                    resubmission == null ? "" : resubmission.NotTicketNo, ex.Message));
            }

        }

    }
}
