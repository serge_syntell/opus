﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.BLL.OfficerError.Model
{
    [Serializable]
    public class ViewOfficerErrorsModel
    {
        public int AaDocTypeID { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public string OfficerNo { get; set; }

        public SelectList DocTypeList { get; set; }
        public SelectList OfficerList { get; set; }
        public bool IsInSession { get; set; }

        public IEnumerable<RuleViolation> GetRuleViolations()
        {
            if (string.IsNullOrEmpty(DateStart))
            {
                yield return new RuleViolation("DateStart", "DateStart.Empty");
            }
            if (string.IsNullOrEmpty(DateEnd))
            {
                yield return new RuleViolation("DateEnd", "DateEnd.Empty");
            }
            if (!ConvertDateTime.IsValidBrowserFormat(DateStart))
            {
                yield return new RuleViolation("DateStart", "DateStart.Error");
            }
            if (!ConvertDateTime.IsValidBrowserFormat(DateEnd))
            {
                yield return new RuleViolation("DateEnd", "DateEnd.Error");
            }
            if (ConvertDateTime.IsValidBrowserFormat(DateStart) &&
                ConvertDateTime.IsValidBrowserFormat(DateEnd) &&
                ConvertDateTime.BrowserStr2Date(DateEnd) < ConvertDateTime.BrowserStr2Date(DateStart))
            {
                yield return new RuleViolation("DateEnd", "DateEnd.IsLessThanFromDate");
            }

            yield break;
        }

        public bool IsValid
        {
            get { return (GetRuleViolations().Count() == 0); }
        }
    }
}
