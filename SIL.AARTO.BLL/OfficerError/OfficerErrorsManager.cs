﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.Common;


namespace SIL.AARTO.BLL.OfficerError
{
    public class OfficerErrorsManager
    {
        private static AartoOfficerErrorService aartoOfficerErrorService = new AartoOfficerErrorService();

        public static DataSet GetOfficerErrorsForReport(int autIntNo, int aaDocTypeID,
            string officerNo, string dateStart, string dateEnd, List<int> userRoles, string userName)
        {
            DataSet officerErrorReportDS = new DataSet("DSOfficerErrorReport");
            DataTable officerErrorDT = new DataTable("OfficerErrorsTable");
            officerErrorDT.Columns.AddRange(new DataColumn[] { 
                new DataColumn("InfringementNO", Type.GetType("System.String")),
                new DataColumn("OffenceDate", Type.GetType("System.String")),
                new DataColumn("InfringerName", Type.GetType("System.String")),
                new DataColumn("InfringerID", Type.GetType("System.String")),
                new DataColumn("Officer", Type.GetType("System.String")),
                new DataColumn("Location", Type.GetType("System.String")),
                new DataColumn("Court", Type.GetType("System.String")),
                new DataColumn("PaymentDate", Type.GetType("System.String")),
                new DataColumn("Amount", Type.GetType("System.String")),
                new DataColumn("AutName", Type.GetType("System.String")),
                new DataColumn("AaDocTypeName", Type.GetType("System.String")),
                new DataColumn("DateStart", Type.GetType("System.String")),
                new DataColumn("DateEnd", Type.GetType("System.String")),
                new DataColumn("OfficerErrorsDesc", Type.GetType("System.String")),
                new DataColumn("Fix", Type.GetType("System.String")),
                new DataColumn("HttpUrl", Type.GetType("System.String"))
            });

            DataRow dataRow = null;

            using (IDataReader reader = aartoOfficerErrorService.GetInfoForViewOfficerErrorReport(autIntNo,
                aaDocTypeID, officerNo, ConvertDateTime.BrowserStr2Date(dateStart), ConvertDateTime.BrowserStr2Date(dateEnd)))
            {
                while (reader.Read())
                {
                    dataRow = officerErrorDT.NewRow();
                    dataRow["InfringementNO"] = reader["NotTicketNo"];
                    dataRow["OffenceDate"] = reader["NotOffenceDate"];
                    dataRow["InfringerName"] = reader["InfringerName"];
                    dataRow["InfringerID"] = reader["DrvIDNumber"];
                    dataRow["Officer"] = reader["Officer"];
                    dataRow["Location"] = reader["NotLocDescr"];
                    dataRow["Court"] = reader["NotCourtName"];
                    dataRow["PaymentDate"] = reader["NotPaymentDate"];
                    dataRow["Amount"] = reader["ChgRevFineAmount"];
                    dataRow["AutName"] = reader["AutName"];
                    dataRow["AaDocTypeName"] = reader["AaDocTypeName"];
                    dataRow["DateStart"] = dateStart as object;
                    dataRow["DateEnd"] = dateEnd as object;

                    //string tempOfficerErrorsDesc = "";

                    //TList<AartoNoticeOfficerError> aaNoticeOfficerErrorList =
                    //    new AartoNoticeOfficerErrorService().GetByNotIntNo(Convert.ToInt32(reader["NotIntNo"]));
                    //foreach (AartoNoticeOfficerError aaNoticeOfficerError in aaNoticeOfficerErrorList)
                    //{
                    //    AartoOfficerError aartoOfficerErrot = new AartoOfficerErrorService().GetByAaOfErId(aaNoticeOfficerError.AaOfErId);
                    //    tempOfficerErrorsDesc += aartoOfficerErrot.AaOfErDescription + "\r\n";
                    //}

                    dataRow["OfficerErrorsDesc"] = reader["AaOfErDescription"];

                    //AartoPageList aartoPageList = new AartoPageListService().GetByAaPageUrl("/EditDocInputs.aspx");

                    int tempFrameIntNo = 0;

                    TList<NoticeFrame> noticeFrameList = new NoticeFrameService().GetByNotIntNo(Convert.ToInt32(reader["NotIntNo"]));
                    if (noticeFrameList.Count > 0)
                    {
                        tempFrameIntNo = noticeFrameList[0].FrameIntNo;
                        Frame frame = new FrameService().GetByFrameIntNo(tempFrameIntNo);

                        string tempBatchDocID = frame.DmsDocumentNo;

                        if(tempBatchDocID.Contains("_"))
                        {
                            tempBatchDocID = tempBatchDocID.Split(new Char[] { '_' })[0];
                        }

                        string baseUrl = String.Format(System.Configuration.ConfigurationManager.AppSettings["DMSDomainURL"]
                        + "/Management/EditDocInputs.aspx?BatchDocumentId={0}&UserName={1}", tempBatchDocID, userName);

                        dataRow["HttpUrl"] = UrlVerify.GetSecretUrl(baseUrl, tempBatchDocID + userName);
                    }
                    else
                    {
                        dataRow["HttpUrl"] = string.Empty as object;
                    }
                                        

                    if (userRoles.Contains((int)AartoUserRoleList.Supervisor))
                    {
                        dataRow["Fix"] = "Fix Officer Errors";
                    }
                    else
                    {
                        dataRow["Fix"] = string.Empty as object;
                    }
                    officerErrorDT.Rows.Add(dataRow);
                }
            }

            officerErrorReportDS.Tables.Add(officerErrorDT);
            return officerErrorReportDS;
        }
    }
}
