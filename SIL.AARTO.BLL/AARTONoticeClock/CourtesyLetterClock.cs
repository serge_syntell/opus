﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.BLL.InfringerOption;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
namespace SIL.AARTO.BLL.AARTONoticeClock
{
    public class CourtesyLetterClock : Clock
    {
        public CourtesyLetterClock(AartoClock clock):base(clock){}
        public CourtesyLetterClock(int noticeID) : base(noticeID) { }
        public override void Create()
        {
            this.ClockTypeID = (int)AartoClockTypeList.CourtesyLetter;
            base.Create();
            //update fine
            //set ChgRevDiscountAmount=0
            AARTODocumentManager.CreateAARTONoticeDocument(_clock.NotIntNo, (int)AartoDocumentTypeList.AARTO12, LastUser);
            FineManager.NoDiscount(this.NoticeID, LastUser);
            FineManager.AddDocFees(this.NoticeID, (int)AartoDocumentTypeList.AARTO12, LastUser);
        }
        public override void Start()
        {
            //Precondition: Courtesy Letter Posted
            base.Start();

            // Check Notice if officer error. if officer error ,set clock end date =null
            if (NoticeID != 0)
            {
                NoticeProcessManager noticeManager = new NoticeProcessManager();
                Notice notice = noticeManager.GetNotice(NoticeID);
                if (notice != null)
                {
                    if (notice.NotIsOfficerError)
                    {
                       AartoClock clock= ClockManager.GetAartoClock(ClockID);
                       clock.AaExpectedEndDate = null;
                       clock = ClockManager.UpdateAartoClock(clock);
                    }
                }
            }
        }
        public override void Pause(int? repID)
        {
            //Preconditions: Receive A04, A07, A08, A10, Payment Received
            base.Pause(repID);
        }
        public override void Stop()
        {
            base.Stop();
        }
        public override bool Expire()
        {
            if (base.Expire())
            {
                EnforcementOrderClock clock = new EnforcementOrderClock(this.NoticeID);
                clock.LastUser = this.LastUser;
                clock.Create();
                return true;
            }
            return false;

        }
    }
}
