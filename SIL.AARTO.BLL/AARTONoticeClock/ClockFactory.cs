﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.DAL.Entities;
using System.Reflection;
namespace SIL.AARTO.BLL.AARTONoticeClock
{
    public class ClockFactory
    {
        public static Clock CreateClock(AartoClock clock)
        {
            //string assmName = "SIL.AARTO.BLL.AARTONoticeClock";
            //string className = ((AartoClockTypeList)clock.AaClockTypeId).ToString() + "Clock";
            //Assembly assm = Assembly.Load(assmName);
            //return (Clock)assm.CreateInstance(assmName + "." + ((AartoClockTypeList)clock.AaClockTypeId).ToString() + "." + className);

            switch (clock.AaClockTypeId)
            {
                case (int)AartoClockTypeList.NoticeToBePostedA03:
                    return new NoticeToBePostedA03Clock(clock);
                case (int)AartoClockTypeList.DiscountPeriod:
                    return new DiscountPeriodClock(clock);
                case (int)AartoClockTypeList.CourtesyLetter:
                    return new CourtesyLetterClock(clock);
                case (int)AartoClockTypeList.EnforcementOrder:
                    return new EnforcementOrderClock(clock);
                //case (int)AartoClockTypeList.WarrantOfAttachment:
                //    return new WarrantOfExecutionClock(clock);
                case (int)AartoClockTypeList.RepFailedFullPayment:
                    return new RepFailedFullPaymentClock(clock);
                case (int)AartoClockTypeList.NominateDriverFailedFullPayment:
                    return new NominateDriverFailedFullPaymentClock(clock);
                case (int)AartoClockTypeList.RequestIntallmentsRejectedFullPayment:
                    return new RequestIntallmentsRejectedFullPaymentClock(clock);
                case (int)AartoClockTypeList.InstallmentPayment:
                    return new InstallmentPaymentClock(clock);
                case (int)AartoClockTypeList.DishonouredInstallment:
                    return new DishonouredInstallmentClock(clock);
                case (int)AartoClockTypeList.RevocationOfEnforcementOrder:
                    return new RevocationOfEnforcementOrderClock(clock);
                case (int)AartoClockTypeList.PartialPayment:
                    return new PartialPaymentClock(clock);
                case (int)AartoClockTypeList.DishonouredPayment:
                    return new DishonouredPaymentClock(clock);
                //case (int)AartoClockTypeList.ElectCourt:
                //    return new ElectCourtClock(clock);
                default: return null;
            }
        }
    }
}
