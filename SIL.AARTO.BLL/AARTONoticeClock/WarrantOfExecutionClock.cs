﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.InfringerOption;
namespace SIL.AARTO.BLL.AARTONoticeClock
{
    public class WarrantOfExecutionClock:Clock
    {
        public WarrantOfExecutionClock(AartoClock clock) : base(clock) { }
        public WarrantOfExecutionClock(int noticeID) : base(noticeID) { }
        public override void Create()
        {
            this.ClockTypeID = (int)AartoClockTypeList.WarrantOfAttachment;
            AARTODocumentManager.CreateAARTONoticeDocument(this.NoticeID, (int)AartoDocumentTypeList.AARTO24, LastUser);
            FineManager.AddDocFees(this.NoticeID, (int)AartoDocumentTypeList.AARTO24, LastUser);
            base.Create();
        }
        public override void Start()
        {
            //A24 Served
        }
        public override void Pause(int? repID)
        {
            //Payment Received
        }
        public override void Continue()
        {

        }
        public override void Stop()
        {
            //Payment Processed or Warrant Executed.
        }
        public override bool Expire()
        {
            return false;
        }
    }
}
