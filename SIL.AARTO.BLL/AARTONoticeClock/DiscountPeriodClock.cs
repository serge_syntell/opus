﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
namespace SIL.AARTO.BLL.AARTONoticeClock
{
    public class DiscountPeriodClock:Clock
    {
        public DiscountPeriodClock(AartoClock clock) : base(clock) { }
        public DiscountPeriodClock(int noticeID) : base(noticeID) { }
        public override void Create()
        {
            //Precondition: Notice Posted (A03) Precondition: Notice Served (A01, A02, A31)
            this.ClockTypeID = (int)AartoClockTypeList.DiscountPeriod;
            base.Create();
        }
        public override void Start()
        {
            base.Start();
        }
        public void Start(DateTime startDate)
        {
            _clock.AaStartDate = ConvertDateTime.GetShortDateTime(startDate);
            _clock.AaExpectedEndDate = ((DateTime)_clock.AaStartDate).AddDays(_clock.AaAllowedDays);
            base.Start();
        }
        public override void Pause(int? repID)
        {
            base.Pause(repID);
        }
        public override void Continue(){}
        public override void Stop()
        {
            base.Stop();
        }
        public override bool Expire()
        {
            if (base.Expire())
            {
                CourtesyLetterClock clock = new CourtesyLetterClock(this.NoticeID);
                clock.LastUser = this.LastUser;
                clock.Create();
                return true;
            }
            return false;
        }
    }
}
