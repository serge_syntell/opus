﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.BLL.InfringerOption;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
namespace SIL.AARTO.BLL.AARTONoticeClock
{
    public class RevocationOfEnforcementOrderClock:Clock
    {
        public RevocationOfEnforcementOrderClock(AartoClock clock):base(clock){}
        public RevocationOfEnforcementOrderClock(int noticeID) : base(noticeID) { }
        public override void Create()
        {
            this.ClockTypeID = (int)AartoClockTypeList.RevocationOfEnforcementOrder;
            base.Create();
            AARTODocumentManager.CreateAARTONoticeDocument(this.RepID, (int)AartoDocumentTypeList.AARTO15, LastUser);
            this.Start();
            //this.Pause(null);
        }
        public override void Start()
        {
            //Precondition: A15.Posted
            base.Start();
        }
        public override void Pause(int? repID)
        {
            //Preconditions: Payment Received
            //AARTO 04
            //AARTO 07
            //AARTO 08
            //AARTO 10
            base.Pause(repID);
        }
        public override void Stop()
        {
            //Preconditions: Payment Processed.
            //1. Final Payment?
            //    AARTO.BLL.TerminateNotice(9xx);
            //2. Not Final Payment:
            //     AARTO.Clocks.InstallmentPayment.Create() 
            base.Stop();
        }
        public override bool Expire()
        {
            if (base.Expire())
            {
                bool A14Successful=true;//false;
                if (A14Successful)
                {
                    EnforcementOrderClock clock = new EnforcementOrderClock(this.NoticeID);
                    clock.LastUser = LastUser;
                    clock.Create();
                }
                else
                {
                    WarrantOfExecutionClock clock = new WarrantOfExecutionClock(this.NoticeID);
                    clock.LastUser = LastUser;
                    clock.Create();
                }
                return true;
            }
            return false;

        }
    }
}
