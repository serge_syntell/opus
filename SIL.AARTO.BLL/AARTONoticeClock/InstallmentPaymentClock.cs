﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.BLL.InfringerOption;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
namespace SIL.AARTO.BLL.AARTONoticeClock
{
    public class InstallmentPaymentClock:Clock
    {
        public InstallmentPaymentClock(AartoClock clock):base(clock){}
        public InstallmentPaymentClock(int noticeID) : base(noticeID) { }
        public override void Create()
        {
            this.ClockTypeID = (int)AartoClockTypeList.InstallmentPayment;
            base.Create();
            //successful
            AARTODocumentManager.CreateAARTONoticeDocument(this.RepID, (int)AartoDocumentTypeList.AARTO06, LastUser);            
        }
        public override void Start()
        {
            //Precondition: A06.Posted
            base.Start();
        }
        public override void Pause(int? repID)
        {
            //Preconditions: Payment Received

            base.Pause(repID);
        }
        public override void Stop()
        {
            //Preconditions: Payment Processed.
            //1. Final Payment?
            //    AARTO.BLL.TerminateNotice(9xx);
            //2. Not Final Payment:
            //     AARTO.Clocks.InstallmentPayment.Create()
            base.Stop();
        }
        public override bool Expire()
        {
            if (base.Expire())
            {
                DishonouredInstallmentClock clock = new DishonouredInstallmentClock(this.NoticeID);
                clock.LastUser = this.LastUser;
                clock.Create();
                return true;
            }
            return false;

        }
    }
}
