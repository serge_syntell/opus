﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;

namespace SIL.AARTO.BLL.AARTONoticeClock
{
    public class Clock:IClock
    {
        /*
         * the code in CourtesyLetterClock,ExecutionOrder,WarrantOfAttachment will not be updated.
         */
        #region Clock property
        protected int ClockTypeID
        {
            get;
            set;
        }
        public int AaClockTypeID { get { return _clock.AaClockTypeId; } }
        protected int ClockID { get; set; }
        protected int NoticeID { get; set; }
        public int RepID { get; set; }
        public int ClockStatusID
        {
            get
            {
                return _clock.AaClockStatusId;
            }
        }
        public string LastUser {get;set;}
        public bool IsProcessed
        {
            get
            {
                return (bool)_clock.IsProcessed;
            }
            set
            {
                if (_clock.IsProcessed != value)
                {
                    _clock.IsProcessed = value;
                    _clock.LastUser = LastUser;
                    new AartoClockService().Save(_clock);
                }
            }
        }
        protected static AartoClock _clock;
        #endregion

        #region Clock constructors
        public Clock() { }
        public Clock(int noticeID)
        {
            NoticeID = noticeID;
            GetAARTOClockByNoticeID();
        }
        public Clock(string clockID)
        {
            ClockID = Convert.ToInt32(clockID);
            GetAARTOClockByClockID();
        }
        public Clock(AartoClock clock)
        {
            _clock = clock;
            ClockTypeID = _clock.AaClockTypeId;
            NoticeID = _clock.NotIntNo;
            ClockID = _clock.AaClockId;
        }
        #endregion

        #region IClock Members

        public virtual void Create()
        {
            AartoClock newAaClock = new AartoClock();
            newAaClock.AaClockTypeId = this.ClockTypeID;
            newAaClock.AaClockStatusId = (int)AartoClockStatusList.Created;
            //AartoClockHistory preHis = ClockManager.GetAartoClockHistoryByClockID(this.NoticeID);
            //if (preHis != null) newAaClock.AaPreviousClockId = preHis.AaClockTypeId;
            newAaClock.AaAllowedDays = new AartoClockTypeService().GetByAaClockTypeId(this.ClockTypeID).AaDefaultAllowedDays;
            newAaClock.NotIntNo = this.NoticeID;
            newAaClock.LastUser = LastUser;
            newAaClock.IsProcessed = false;
            newAaClock.AaClockCreatedDate = DateTime.Now;
            new AartoClockService().Insert(newAaClock);
            _clock = newAaClock;
            ClockTypeID = _clock.AaClockTypeId;
            NoticeID = _clock.NotIntNo;
            ClockID = _clock.AaClockId;
        }

        public virtual void Start()
        {
            _clock.AaClockStatusId = (int)AartoClockStatusList.Running;
            _clock.AaStartDate = ConvertDateTime.GetShortDateTime(DateTime.Now.AddDays(SystemParameter.Get().PostDeliveryDays));
            _clock.AaExpectedEndDate = ((DateTime)_clock.AaStartDate).AddDays(_clock.AaAllowedDays);
            _clock.LastUser = LastUser;
            _clock = new AartoClockService().Save(_clock);
        }

        public virtual void Pause(int? repID)
        {
            MoveNoticeClockRowToHistory();

            _clock.AaClockStatusId = (int)AartoClockStatusList.Paused;
            _clock.AaPauseDate =ConvertDateTime.GetShortDateTime(DateTime.Now);
            _clock.AaRepId = repID;
            _clock.AaExpectedEndDate = null;
            _clock.LastUser = LastUser;
            _clock = new AartoClockService().Save(_clock);
        }

        public virtual void Continue()
        {//maybe the notice process will go to Enforcement order after representation decision.
            MoveNoticeClockRowToHistory();
            _clock.AaClockStatusId = (int)AartoClockStatusList.Running;
            _clock.AaRestartDate = ConvertDateTime.GetShortDateTime(DateTime.Now);
            _clock.AaExpectedEndDate = ((DateTime)_clock.AaStartDate).AddDays(_clock.AaAllowedDays + (((DateTime)_clock.AaRestartDate) - ((DateTime)_clock.AaPauseDate)).Days);
            _clock.AaPauseDate = null;
            _clock.LastUser = LastUser;
            _clock = new AartoClockService().Save(_clock);
        }
        public virtual void Reset()
        {
            MoveNoticeClockRowToHistory();
            _clock.AaClockStatusId = (int)AartoClockStatusList.Running;
            _clock.AaStartDate = ConvertDateTime.GetShortDateTime(DateTime.Now);
            _clock.AaExpectedEndDate = ((DateTime)_clock.AaStartDate).AddDays(_clock.AaAllowedDays);
            _clock.AaPauseDate = null;
            _clock.LastUser = LastUser;
            _clock = new AartoClockService().Save(_clock);
        }
        public virtual bool Expire()
        {
            if (_clock.AaExpectedEndDate <= ConvertDateTime.GetNowDate())
            {
                //int? nextClockTypeID = new AartoClockTypeService().GetByAaClockTypeId(_clock.AaClockTypeId).AaNextClockTypeId;
                //if (nextClockTypeID != null)
                //{
                //    this.MoveNoticeClockRowToHistory();
                //    AartoClockType nextClockType = new AartoClockTypeService().GetByAaClockTypeId((int)nextClockTypeID);
                //    _clock.AaAllowedDays = nextClockType.AaDefaultAllowedDays;
                //    _clock.IsProcessed = true;
                //    _clock.AaClockTypeId = (int)nextClockTypeID;
                //    _clock.AaExpectedEndDate = ConvertDateTime.GetShortDateTime(DateTime.Now).AddDays(_clock.AaAllowedDays + SystemParameter.PostDeliveryDays);
                //    _clock.LastUser = LastUser;
                //    _clock = new AartoClockService().Save(_clock);
                //}
                //else
                //{
                //    this.Stop();
                //}
                this.Stop();
                return true;
            }
            else
            {
                _clock.IsProcessed = true;
                _clock.LastUser = LastUser;
                _clock = new AartoClockService().Save(_clock);
                return false;
            }
        }

        public virtual void Stop()
        {
            _clock.LastUser = LastUser;
            _clock.AaClockStatusId = (int)AartoClockStatusList.Stopped;
            MoveNoticeClockRowToHistory();
            new AartoClockService().Delete(_clock);
        }

        public static void ResetIsProcessedForAll()
        {
            new AartoClockService().SetAllIsProcessedWithFalse();
        }

        private void MoveNoticeClockRowToHistory()
        {
            
            AartoClockHistory clockHistroy = new AartoClockHistory();
            clockHistroy.NotIntNo = _clock.NotIntNo;
            clockHistroy.AaRepId = _clock.AaRepId;
            clockHistroy.AaClockTypeId = _clock.AaClockTypeId;
            clockHistroy.AaStartDate = _clock.AaStartDate;
            clockHistroy.AaPauseDate = _clock.AaPauseDate;
            clockHistroy.AaClockStatusId = _clock.AaClockStatusId;
            clockHistroy.AaPreviousClockId = _clock.AaPreviousClockId;
            clockHistroy.AaRestartDate = _clock.AaRestartDate;
            clockHistroy.AaExpectedEndDate = _clock.AaExpectedEndDate;
            clockHistroy.AaAllowedDays = _clock.AaAllowedDays;
            clockHistroy.AaClockCreatedDate = _clock.AaClockCreatedDate;
            clockHistroy.LastUser = _clock.LastUser;
            new AartoClockHistoryService().Insert(clockHistroy);
        }
        private void GetAARTOClockByNoticeID()
        {
            TList<AartoClock> clockList = new AartoClockService().GetByNotIntNo(NoticeID);
            if (clockList.Count > 0)
            {
                _clock = clockList[0];
                ClockTypeID = _clock.AaClockTypeId;
                ClockID = _clock.AaClockId;
            }
        }
        private void GetAARTOClockByClockID()
        {
            if (_clock == null)
            {
                _clock = new AartoClockService().GetByAaClockId(ClockID);
                ClockTypeID = _clock.AaClockTypeId;
                NoticeID = _clock.NotIntNo;
            }
        }
        #endregion
    }
}

