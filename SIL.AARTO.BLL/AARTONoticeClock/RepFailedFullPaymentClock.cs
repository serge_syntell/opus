﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using SIL.AARTO.BLL.InfringerOption;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
namespace SIL.AARTO.BLL.AARTONoticeClock
{
    public class RepFailedFullPaymentClock:Clock
    {
        public RepFailedFullPaymentClock(AartoClock clock):base(clock){}
        public RepFailedFullPaymentClock(int noticeID) : base(noticeID) { }
        public override void Create()
        {
            this.ClockTypeID = (int)AartoClockTypeList.RepFailedFullPayment;
            base.Create();
            AARTODocumentManager.CreateAARTONoticeDocument(this.RepID, (int)AartoDocumentTypeList.AARTO09, LastUser);            
            //add fee
            FineManager.AddDocFees(this.NoticeID, (int)AartoDocumentTypeList.AARTO09, LastUser);
        }
        public override void Start()
        {
            //Precondition: A09 Posted
            base.Start();
        }
        public override void Pause(int? repID)
        {
            //Preconditions: Payment Received

            //If AARTO 08 Received within 32 + 10 days of Notice.PostDate
            //And Payment Received within 10  + 10 days of AARTO09.PostDate
            //AARTO.BLL.Fine.AllowDiscount()
            base.Pause(repID);
        }
        public override void Continue()
        {

        }
        public override void Stop()
        {
            //Preconditions: Payment Processed.
            //Payment Processed:
            base.Stop();
        }
        public override bool Expire()
        {
            if (base.Expire())
            {
                EnforcementOrderClock clock = new EnforcementOrderClock(this.NoticeID);
                clock.LastUser = this.LastUser;
                clock.Create();
                return true;
            }
            return false;
        }
    }
}
