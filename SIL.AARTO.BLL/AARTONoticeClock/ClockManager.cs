﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.BLL.AARTONoticeClock;
using SIL.AARTO.BLL.Utility;
namespace SIL.AARTO.BLL.AARTONoticeClock
{
    public class ClockManager
    {
        static AartoClockService clockService = new AartoClockService();

        public static TList<AartoClock> GetAARTOClockListByClockTypeID( int rowNumber)
        {
            int rowCount = -1;
            AartoClockQuery query = new AartoClockQuery();
            query.Append(AartoClockColumn.IsProcessed, "false");
            query.AppendIsNotNull(AartoClockColumn.AaExpectedEndDate);
            return clockService.Find(query as IFilterParameterCollection, "", 0, rowNumber, out rowCount);

        }
        public static AartoClockHistory GetAartoClockHistoryByClockID(int notIntNo)
        {
            int rowCount = -1;
            AartoClockHistoryQuery query = new AartoClockHistoryQuery();
            query.Append(AartoClockHistoryColumn.NotIntNo, notIntNo.ToString());
            TList<AartoClockHistory> hisList = new AartoClockHistoryService().Find(query as IFilterParameterCollection, "AaClockCreatedDate Desc", 0, 1, out rowCount);
            if (hisList.Count > 0) return hisList[0];
            else return null;
        }
        //public static List<int> GetAARTOClockListByClockTypeID(int clockTypeID, int rowNumber)
        //{
        //    List<int> clockIDList = new List<int>();
        //    IDataReader idr = new AartoClockService().GetAARTOClockListByClockTypeID(clockTypeID, rowNumber);
        //    while (idr.Read())
        //    {
        //        clockIDList.Add(idr["DoTeTypeID"] is DBNull ? -1 : Convert.ToInt32(idr["DoTeTypeID"]));
        //    }
        //}
        public static bool TerminateNotice(int notIntNo,int chgSatusID,string lastUser)
        {
            Charge cg = NoticeManager.GetChargeByNotIntNo(notIntNo);
            if (cg != null)
            {
                cg.ChargeStatus = chgSatusID;
                cg.LastUser = lastUser;
                try
                {
                    new ChargeService().Save(cg);
                    return true;
                }
                catch
                {
                    return false;
                }
            } return false;
        }
        public static bool GetAdjudicateResultByClockID(int clockID)
        {
            AartoClock clock = clockService.GetByAaClockId(clockID);
            if (clock.AaRepId != null)
            {
                AartoRepresentation rep = new AartoRepresentationService().GetByAaRepId((int)clock.AaRepId);
                return rep.RcCode == (int)RepresentationCodeList.AARTO_RepresentationSuccessful;
            }
            else return false;
        }

        public static AartoClock GetAartoClock(int clockID)
        {
            return clockService.GetByAaClockId(clockID);
        }

        public static AartoClock UpdateAartoClock(AartoClock clock)
        {
            return clockService.Save(clock);
        }

        public static Clock GetClockByRepID(int repID)
        {
            int? notIntNo = NoticeManager.GetNotIntNoByRepID(repID);
            if (notIntNo == null) return null;
            return new Clock((int)notIntNo);
        }
        public static Clock GetClockByNoticeID(int noticeID)
        {
            return new Clock(noticeID);
        }
    }
    public class ClockEntity
    {
        public int ClockID { get; set; }
        public int NoticeID { get; set; }
        public DateTime ExceptedEndDate { get; set; }
    }
}
