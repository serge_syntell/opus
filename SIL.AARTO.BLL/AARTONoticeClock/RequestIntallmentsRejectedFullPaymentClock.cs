﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.BLL.InfringerOption;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
namespace SIL.AARTO.BLL.AARTONoticeClock
{
    public class RequestIntallmentsRejectedFullPaymentClock:Clock
    {
        public RequestIntallmentsRejectedFullPaymentClock(AartoClock clock):base(clock){}
        public RequestIntallmentsRejectedFullPaymentClock(int noticeID) : base(noticeID) { }
        public override void Create()
        {
            this.ClockTypeID = (int)AartoClockTypeList.RequestIntallmentsRejectedFullPayment;
            base.Create();
            //create document unsuccessful
            AARTODocumentManager.CreateAARTONoticeDocument(this.RepID, (int)AartoDocumentTypeList.AARTO06, LastUser);            
        }
        public override void Start()
        {
            //Precondition: A06.Posted
            base.Start();
        }
        public override void Pause(int? repID)
        {
            //Preconditions: Payment Received

            //If AARTO 08 Received within 32 + 10 days of Notice.PostDate
            //And Payment Received within 10  + 10 days of AARTO09.PostDate
            //AARTO.BLL.Fine.AllowDiscount()

            base.Pause(repID);
        }
        public override void Continue()
        {

        }
        public override void Stop()
        {
            //Preconditions: Payment Processed.
            base.Stop();
        }
        public override bool Expire()
        {
            if (base.Expire())
            {
                EnforcementOrderClock clock = new EnforcementOrderClock(this.NoticeID);
                clock.LastUser = this.LastUser;
                clock.Create();
                return true;
            }
            return false;

        }
    }
}
