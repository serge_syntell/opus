﻿namespace SIL.AARTO.BLL.AARTONoticeClock
{
    public interface IClock
    {
        void Create();
        void Start();
        void Pause(int? repID);
        void Continue();
        bool Expire();
        void Stop();
    }
}
