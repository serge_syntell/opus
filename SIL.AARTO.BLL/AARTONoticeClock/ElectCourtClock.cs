﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.BLL.InfringerOption;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
namespace SIL.AARTO.BLL.AARTONoticeClock
{
    public class ElectCourtClock : Clock
    {
        public ElectCourtClock(AartoClock clock):base(clock){}
        public ElectCourtClock(int noticeID) : base(noticeID) { }
        public override void Create()
        {
            this.ClockTypeID = (int)AartoClockTypeList.ElectCourt;
            base.Create();
            AARTODocumentManager.CreateAARTONoticeDocument(this.RepID, (int)AartoDocumentTypeList.AARTO05d, LastUser);
        }
        public override void Start()
        {
            //Precondition:  A05d.Created / Posted
            base.Start();
        }
        public override void Pause(int? repID)
        {
        }
        public override void Stop()
        {            
            //1.Attended Court:
            //AARTO.BLL.TerminateNotice(9xx);
            //2. Did not attend court:
            //AARTO.Clocks.EnforcementOrder.Create();
            //3. A11
            //AARTO.BLL.TerminateNotice(9xx);

            base.Stop();
        }
        public override bool Expire()
        {
            if (base.Expire())
            {
                ////maybe wrong here
                //ElectCourtClock clock = new ElectCourtClock(this.NoticeID);
                //clock.LastUser = this.LastUser;
                //clock.Create();
                return true;
            }
            return false;

        }
    }
}
