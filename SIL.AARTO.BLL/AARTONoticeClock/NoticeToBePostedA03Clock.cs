﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.InfringerOption;
namespace SIL.AARTO.BLL.AARTONoticeClock
{
    public class NoticeToBePostedA03Clock : Clock
    {
        public NoticeToBePostedA03Clock(AartoClock clock) : base(clock) { }
        public NoticeToBePostedA03Clock(int noticeID) : base(noticeID) { }
        public override void Create()
        {
            //Precondition: Notice created on system (ClockManager must create Notices from Frames and start this Clock – similar to NoticeProcessing in TpExInt (standard version)
            this.ClockTypeID = (int)AartoClockTypeList.NoticeToBePostedA03;
            base.Create();
            AARTODocumentManager.CreateAARTONoticeDocument(_clock.NotIntNo, (int)AartoDocumentTypeList.AARTO03, LastUser);            
        }
        public override void Start()
        {
            base.Start();
        }
        public void Start(DateTime startDate)
        {
            _clock.AaStartDate = ConvertDateTime.GetShortDateTime(startDate);
            _clock.AaExpectedEndDate = ((DateTime)_clock.AaStartDate).AddDays(_clock.AaAllowedDays);
            base.Start();
        }
        public override void Pause(int? repID)
        {
         
        }
        public override void Continue()
        {
            
        }
        public override bool Expire()
        {
            if (base.Expire())
            {
                ClockManager.TerminateNotice(this.NoticeID, 990, LastUser);
                return true;
            } return false;
        }
        public override void Stop()
        {
            //Precondition: A03 Posted (AaDocStatusId = 50, AaDocPostedDate not null)
            base.Stop();
            DiscountPeriodClock clock = new DiscountPeriodClock(this.NoticeID);
            clock.LastUser = this.LastUser;
            clock.Create();
            clock.Start();
        }
    }
}
