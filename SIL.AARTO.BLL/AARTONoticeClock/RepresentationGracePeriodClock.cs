﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.BLL.InfringerOption;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
namespace SIL.AARTO.BLL.AARTONoticeClock
{
    public class RepresentationGracePeriodClock : Clock
    {
        public RepresentationGracePeriodClock(AartoClock clock):base(clock){}
        public RepresentationGracePeriodClock(int noticeID) : base(noticeID) { }
        public override void Create()
        {
            this.ClockTypeID = (int)AartoClockTypeList.RepresentationGracePeriod;
            base.Create();
            //update fine
            //set ChgRevDiscountAmount=0
            //AARTODocumentManager.CreateAARTONoticeDocument(_clock.NotIntNo, (int)AartoDocumentTypeList.AARTO12, LastUser);
            //FineManager.NoDiscount(this.NoticeID, LastUser);
            //FineManager.AddDocFees(this.NoticeID, (int)AartoDocumentTypeList.AARTO12, LastUser);
        }
        public override void Start()
        {
            //Precondition: Courtesy Letter Posted
            base.Start();
        }
    }
}
