﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.BLL.InfringerOption;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
namespace SIL.AARTO.BLL.AARTONoticeClock
{
    class EnforcementOrderClock: Clock
    {
        public EnforcementOrderClock(AartoClock clock):base(clock){}
        public EnforcementOrderClock(int noticeID) : base(noticeID) { }
        public override void Create()
        {
            this.ClockTypeID = (int)AartoClockTypeList.EnforcementOrder;
            base.Create();
            AARTODocumentManager.CreateAARTONoticeDocument(this.NoticeID, (int)AartoDocumentTypeList.AARTO13, LastUser);
            FineManager.AddDocFees(this.NoticeID, (int)AartoDocumentTypeList.AARTO13, LastUser);
        }
        public override void Start()
        {
            //Precondition: A13 Posted
            base.Start();
        }
        public override void Pause(int? repID)
        {
            //AARTO 14, Payment Received
            base.Pause(repID);
        }
        public override void Continue()
        {
            base.Continue();
        }
        public override void Reset()
        {
            base.Reset();
        }
        public override void Stop()
        {
            //Precondition: A15 Posted, Payment Processed
            //Payment Processed:
            base.Stop();
        }
        public override bool Expire()
        {
            if (base.Expire())
            {
                WarrantOfExecutionClock clock = new WarrantOfExecutionClock(this.NoticeID);
                clock.LastUser = this.LastUser;
                clock.Create();
                return true;
            }
            return false;

        }
    }
}
