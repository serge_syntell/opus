﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility;
namespace SIL.AARTO.BLL.AARTONoticeClock
{
    public class FineManager
    {
        public static bool AddDocFees(int notIntNo,int docTypeID,string lastUser)
        {
            int mtrIntNo = NoticeManager.GetAuthorityByNotIntNo(notIntNo).MtrIntNo;
            decimal fee = GetFeeByMetroIDAndDocTypeID(mtrIntNo, docTypeID);
            if (fee > 0)
            {
                Charge cg = NoticeManager.GetChargeByNotIntNo(notIntNo);
                if (cg != null)
                {
                    AartoChargeTransactionFee cgFee = new AartoChargeTransactionFee();
                    cgFee.AaChargeFee = fee;
                    cgFee.AaDocTypeId = docTypeID;
                    cgFee.ChgIntNo = cg.ChgIntNo;
                    cgFee.LastUser = lastUser;
                    return new AartoChargeTransactionFeeService().Insert(cgFee);
                }
                else return false;
            }
            else
                return false;
        }
        private static decimal GetFeeByMetroIDAndDocTypeID(int metroID, int docTypeID)
        {
            AartoMetroRateCard mrc = new AartoMetroRateCardService().GetByAaDocTypeId(docTypeID).Find(mf => mf.MtrIntNo == metroID);
            if (mrc != null) return mrc.AaMeRcAmount;
            else return 0;
        }
        public static bool NoDiscount(int notIntNo, string lastUser)
        {
            Charge cg = NoticeManager.GetChargeByNotIntNo(notIntNo);
            if (cg != null)
            {
                cg.ChgRevDiscountAmount = 0;
                new ChargeService().Save(cg);
                return true;
            }
            else
                return false;
        }
    }
}
