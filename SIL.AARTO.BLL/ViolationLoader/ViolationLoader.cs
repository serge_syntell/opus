﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SIL.AARTO.BLL.NoticeProcess;
using Stalberg.Indaba;
using Stalberg.TMS_3P_Loader;
using System.Xml;
using System.Configuration;
using SIL.AARTO.DataTransfer;
using System.Data;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections;
using SIL.AARTO.BLL.Utility;
using System.Data.SqlClient;
using System.Net.Mail;
using SIL.AARTO.BLL.Export;
using System.Transactions;

namespace SIL.AARTO.IMX.XMLLoader
{
    public class ViolationLoader
    {
        private string strTempFilePath;
        private const string ZIPFILE_SUFFIX = ".IMX";
        private const string FILE_SUFFIX = ".xml";
        private string _connStr;

        protected int _filmIntNo = 0;
        protected bool _natisLast = true;
        protected int _noOfImagesToPrint = 1;
        protected int _whichImageTraffiPax = 1;
        protected int _whichImageDigital = 1;
        protected string _excluListActive = "N";
        protected int _excluSpeedAllowance = 30;
        protected string _fullAdjudicated = "N";
        protected string _procName = string.Empty;

        protected const int STATUS_NATIS_FIRST = 6;     //new status for setting frames that must go straight to adjudication
        protected const int STATUS_NATIS_LAST = 699;//700;     //new status for setting frames that must go straight to adjudication
        protected const int STATUS_CANCELLED = 999;		//cancelled
        protected const int STATUS_EXPIRED = 921;		//offence > delay time period days old
        protected const int STATUS_AMOUNT_ADDED = 7;
        protected const int STATUS_NO_AOG = 500;
        protected const string WET_FILM_FIRST_IMAGE = "_17.j";
        protected const string DIG_FILM_FIRST_IMAGE = "001.j";
        protected const string DIG_FILM_SPD_2ND_IMAGE = "002.j";
        protected const int STATUS_FILM_DATA_ERROR = 500;
        protected const int STATUS_FILM_SUCCESSFUL = 900;
        protected const int STATUS_OFFICIAL_VEHICLE_EXCLUSION = 991;

        protected string lowSpeedRejReason = string.Empty;
        protected string invalidSpeed = string.Empty;
        protected int lowSpeedRejIntNo = 0;
        protected string asdInvalidSetup = string.Empty;
        protected int asdInvalidRejIntNo = 0;
        protected string expiredRejReason = string.Empty;
        protected int expiredRejIntNo = 0;

        string EmailFrom = "";
        string SupportEmail = "";
        string SupportFName = "";
        string SupportSName = "";
        string HostServer = "";
        string Smtp = "";
        string APPNAME = "";


        public ViolationLoader(string strAppName)
        {
            _connStr = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
            strTempFilePath = ConfigurationManager.AppSettings["TempFilePath"];

            EmailFrom = ConfigurationManager.AppSettings["EmailFrom"];
            SupportEmail = ConfigurationManager.AppSettings["SupportEmail"];
            SupportFName = ConfigurationManager.AppSettings["SupportFName"];
            SupportSName = ConfigurationManager.AppSettings["SupportSName"];
            HostServer = ConfigurationManager.AppSettings["HostServer"];
            Smtp = ConfigurationManager.AppSettings["Smtp"];

            APPNAME = strAppName;
        }

        public void LoadViolationsByIndaba()
        {
            InitRejection();
            CommonFunction.ConsoleWriteLine("Start importing films...");

            //CommonFunction.indaba.Destination = System.Configuration.ConfigurationManager.AppSettings["IndabaDestination"];
            IndabaFileInfo[] fileinfos = CommonFunction.indaba.GetFileInformation(ZIPFILE_SUFFIX);

            foreach (IndabaFileInfo fileInfo in fileinfos)
            {
                try
                {
                    //get file stream from indaba
                    using (MemoryStream stream = (MemoryStream)CommonFunction.indaba.Peek(fileInfo))
                    {
                        string zipFileName = Path.Combine(strTempFilePath, fileInfo.Name);
                        string strFileName = Path.GetFileNameWithoutExtension(fileInfo.Name);
                        string DirectoryName = Path.GetDirectoryName(zipFileName)+ "\\" + strFileName;
                        string FileName = Path.Combine(DirectoryName, strFileName + FILE_SUFFIX);

                        //Write to zip file
                        FileStream file = File.Create(zipFileName);
                        stream.WriteTo(file);
                        file.Flush();
                        file.Close();

                        //unzip
                        List<string> fileList = ZipUtility.Unzip(DirectoryName, zipFileName, "");
                        if (fileList.Count == 0)
                        {
                            throw new Exception(string.Format("Uncompress file failed. File name:{0}", zipFileName));
                        }

                        //check hash file
                        if (CheckHashFile(FileName))
                        {
                            CommonFunction.ConsoleWriteLine("Incorrect hash file or source file: " + fileInfo.Name);
                            continue;
                        }

                        //Import file
                        CommonFunction.ConsoleWriteLine("Importing: " + fileInfo.Name);
  
                        //Import data to DB
                        try
                        {
                            FilmsToLoad = new List<string>();
                            if (!ProcessViolations(FileName))
                                CommonFunction.ConsoleWriteLine("IMXImporter, import failed.", LogCategory.Error);

                            CommonFunction.indaba.Dequeue(fileInfo);
                            Directory.Delete(DirectoryName, true);
                        }
                        catch (Exception ex)
                        {
                            CommonFunction.ConsoleWriteLine("Insert into database failed! " + ex.Message, LogCategory.Error, ex);
                            CreateErrorFile(0, 0, FileName, "Insert into database failed! " + ex.Message, "header_only", false);
                            CommonFunction.indaba.Dequeue(fileInfo);
                            Directory.Delete(DirectoryName, true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    CommonFunction.ConsoleWriteLine(string.Format("Get file from indaba failed. File name:{0}", fileInfo.Name), LogCategory.Error, ex);
                    continue;
                }
            }
        }


        public int LoadViolationsByDVD(string strPath, ref int failCount)
        {
            int filmCount = 0;
            InitRejection();
            LoadViolationsInFolders(strPath, ref filmCount, ref failCount);
            return filmCount;
        }

        private void LoadViolationsInFolders(string strPath, ref int filmCount, ref int failCount)
        {
            string[] Files = Directory.GetFiles(strPath, "*" + ZIPFILE_SUFFIX);
            if (Files.Length > 0)
            {
                foreach (string strFilePath in Files)
                {
                    bool success = LoadViolationsByFile(strFilePath);
                    if (success)
                        filmCount++;
                    else
                        failCount++;
                }
            }
            else
            {
                foreach (string dirName in Directory.GetDirectories(strPath))
                {
                    LoadViolationsInFolders(dirName, ref filmCount, ref failCount);
                }
            }
        }

        ExportDataManager exportDataManager = new ExportDataManager();
        List<String> FilmsToLoad;
        public bool LoadViolationsByFile(string strPath)
        {
            bool success = false;

            CommonFunction.ConsoleWriteLine("Start importing films...");
            string strFileName = Path.GetFileNameWithoutExtension(strPath);
            string strImagFolder = Path.GetDirectoryName(strPath);
            string DirectoryName = Path.Combine(strTempFilePath, strFileName);

            string FileName = Path.Combine(DirectoryName, strFileName + FILE_SUFFIX);

            try
            {
                //unzip
                List<string> fileList = ZipUtility.Unzip(DirectoryName, strPath, "");
                if (fileList.Count == 0)
                {
                    throw new Exception(string.Format("Uncompress file failed. File name: {0}", strPath));
                }

                //check hash file
                if (CheckHashFile(FileName))
                {
                    CommonFunction.ConsoleWriteLine("Incorrect hash file or source file: " + FileName);
                    return false;
                }

                //Import file
                CommonFunction.ConsoleWriteLine("Importing: " + FileName);

                //Import data to DB
                try
                {
                    FilmsToLoad = new List<string>();

                    if (!ProcessViolations(FileName))
                        CommonFunction.ConsoleWriteLine("Import failed.", LogCategory.Error);
                    else
                    {
                        foreach (String strFilmNo in FilmsToLoad)
                        {
                            string errorMessage;
                            // 2013-07-17 add LastUser by Henry
                            if (exportDataManager.DoImport(strImagFolder, strFilmNo, out errorMessage, 
                                string.Format("{0}_{1}", APPNAME, GlobalVariates<User>.CurrentUser.UserLoginName)) == false)
                            {
                                //MessageBox.Show(SetLanguage.GetString(typeof(LoadImage), "LoadImagesFailure"));
                                if (!String.IsNullOrEmpty(errorMessage))
                                    CreateErrorFile(0, 0, FileName, "Import images failed! " + errorMessage, "header_only", false);
                            }
                            else
                            {
                                success = true;
                            }
                        }
                    }

                    if (Directory.Exists(DirectoryName))
                        Directory.Delete(DirectoryName, true);
                }
                catch (Exception ex)
                {
                    CommonFunction.ConsoleWriteLine("Insert into database failed! " + ex.Message, LogCategory.Error, ex);
                    CreateErrorFile(0, 0, FileName, "Insert into database failed! " + ex.Message, "header_only", false);
                }

            }
            catch (Exception ex)
            {
                CommonFunction.ConsoleWriteLine(string.Format("Get file from DVD failed. File name:{0}", FileName), LogCategory.Error, ex);
            }

            return success;
        }
        
        public void InitRejection()
        {
            //add camera set up rejection reason - this one actually uses the string RejResaon in the SP, not the integer key
            ViolationLoaderDB rejDB = new ViolationLoaderDB(this._connStr);
            this.lowSpeedRejReason = "Camera Setup - incorrect speed limit";
            this.lowSpeedRejIntNo = rejDB.UpdateRejection(0, lowSpeedRejReason, "N", APPNAME, 0, "Y");

            //add low speed rejection reason
            this.invalidSpeed = "Zero or low speed - unable to prosecute";
            this.lowSpeedRejIntNo = rejDB.UpdateRejection(0, invalidSpeed, "N", APPNAME, 0, "Y");

            //add low speed rejection reason
            this.asdInvalidSetup = "ASD Camera Setup Invalid";
            this.asdInvalidRejIntNo = rejDB.UpdateRejection(0, asdInvalidSetup, "N", APPNAME, 0, "Y");

            //expired
            this.expiredRejReason = "Expired. The frame offence date is too old to continue";
            this.expiredRejIntNo = rejDB.UpdateRejection(0, expiredRejReason, "N", APPNAME, 0, "Y");
        }

        private bool CheckHashFile(string FileName)
        {
            bool noErrors = false;
            bool failed = false;

            string path = Path.GetDirectoryName(FileName);
            string hashFile = path + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".hashes";
            CommonFunction.ConsoleWriteLine("CheckHashFile: processing hashFile " + hashFile + " at " + DateTime.Now.ToString());

            Stalberg.TMS_3P_Loader.Hashing hasher = new Hashing();
            CommonFunction.ConsoleWriteLine("CheckHashFile: hashFile " + hashFile + " initialised");

            noErrors = hasher.VerifyHashFile(hashFile);
            CommonFunction.ConsoleWriteLine("CheckHashFile: hashFile " + hashFile + " verified " + noErrors.ToString());

            //read and check hashFile file
            if (!noErrors)
            {
                failed = true;

                //create error file to return to Traffic/3rd party via ftp
                CreateErrorFile(0, 0, FileName, "Error in hash file", "header_only", false);

                //create the error file
                string errors = path + "\\" + Path.GetFileName(hashFile) + ".errors";

                FileStream fsWriter = new FileStream(errors, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fsWriter, System.Text.Encoding.Default);

                sw.WriteLine(hasher.Errors);
                sw.Close();
                fsWriter.Close();

                //create the file list
                string fileList = path + "\\" + Path.GetFileName(hashFile) + ".filelist";

                fsWriter = new FileStream(fileList, FileMode.OpenOrCreate, FileAccess.Write);
                sw = new StreamWriter(fsWriter, System.Text.Encoding.Default);

                sw.WriteLine(hasher.FileList);
                sw.Close();
                fsWriter.Close();
            }
            return failed;
        }

        private void CreateErrorFile(int autIntNo, int filmIntNo, string FileName, string errMessge, string type, bool allOK)
        {
            try
            {
                LoadViolationsDB loadviol = new LoadViolationsDB(_connStr);

                DataSet dsData = new DataSet();
                dsData.ReadXmlSchema("./DataSetSchema.xsd");
                DataTable dtHeader = dsData.Tables[0];
                DataTable dtData = dsData.Tables[1];
                dsData.Tables.RemoveAt(2);

                for (int i = dtData.Columns.Count - 1; i >= 6; i--)
                {
                    dtData.Columns.RemoveAt(i);
                }

                dtHeader.Columns.Add("errMessge", typeof(string));
                dtData.Columns.Add("errMessge", typeof(string));


                dsData.ReadXml(FileName);

                if (!allOK)
                    errMessge += Environment.NewLine + "File loaded failed, please recreate the file and load again.";

                dtHeader.Rows[0]["errMessge"] = errMessge;

                if (!type.Equals("header_only"))
                {
                    foreach (DataRow drFrame in dtData.Rows)
                    {
                        try
                        {
                            //get errors from database by frame
                            SqlDataReader reader = loadviol.GetLoadViolationErrorsByFrame(autIntNo, drFrame["FilmNo"].ToString(), drFrame["FrameNo"].ToString(), "0");
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    drFrame["errMessge"] += reader["ErrorDescription"].ToString() + Environment.NewLine;
                                }
                            }
                            reader.Close();
                        }
                        catch (Exception e)
                        {
                            CommonFunction.ConsoleWriteLine("ReadDataRow " + e.Message);
                        }
                    }
                }
                else
                    dsData.Tables.RemoveAt(1);

                //create the error file
                string errorFile = "";

                if (allOK)
                    errorFile = Path.Combine(strTempFilePath, Path.GetFileName(FileName)) + ".ok";
                else
                    errorFile = Path.Combine(strTempFilePath, Path.GetFileName(FileName)) + ".err";

                dsData.WriteXml(errorFile);

                SendEmail(errorFile);

                if (type.Equals("header_only"))
                {
                    if (allOK)
                    {
                        UpdateFilmLoadStatus(filmIntNo, STATUS_FILM_SUCCESSFUL);
                    }
                    else
                    {
                        if (filmIntNo > 0)
                            UpdateFilmLoadStatus(filmIntNo, STATUS_FILM_DATA_ERROR);
                    }
                }
                else
                {
                     UpdateFilmLoadStatus(filmIntNo, STATUS_FILM_DATA_ERROR);
                }
                return;
            }
            catch (Exception a)
            {
                CommonFunction.ConsoleWriteLine("Error: " + a.Message, LogCategory.Error, a);
                return;
            }
        }

        private void SendEmail(string strFile)
        {
            //send error file by email to contractor email address
            if (SupportEmail.Equals(""))
            {
                CommonFunction.ConsoleWriteLine("CreateErrorFile: unable to email error details to contractor: no email address at: " + DateTime.Now.ToString(), LogCategory.Error);
                return;
            }

            //email error details and filelist to contractor             
            string message = "Attention: " + SupportFName + " " +
                                SupportSName + "\n\n"
                             + "On the " + DateTime.Now.ToString() +
                             " the following output file was created by the " + APPNAME +
                             " process:\n\n"
                             + "File name: " + strFile + "\n\n"
                             + "The file is attached to this email.\n\n"
                             + "Thank you very much.\n\n"
                             + "Regards\n"
                             + "AARTO System Administrator";

            try
            {
                MailAddress from = new MailAddress(EmailFrom);
                MailAddress to = new MailAddress(SupportEmail);

                MailMessage mail = new MailMessage(from, to);
                mail.Subject = HostServer.ToUpper() + " " + APPNAME +  " output file";
                mail.Body = message;
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                Attachment myAttachment = new Attachment(strFile);
                mail.Attachments.Add(myAttachment);
                try
                {
                    SmtpClient mailClient = new SmtpClient();
                    mailClient.Host = Smtp;
                    mailClient.UseDefaultCredentials = true;
                    mailClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;

                    //Send delivers the message to the mail server
                    mailClient.Send(mail);

                    CommonFunction.ConsoleWriteLine("CreateErrorFile: output file " + strFile + " sent to " + SupportEmail + " at: " + DateTime.Now.ToString(), LogCategory.General);
                }
                catch (Exception smtpEx)
                {
                    CommonFunction.ConsoleWriteLine("CreateErrorFile: Failed to send output file to " + SupportEmail + " - " + smtpEx.Message, LogCategory.Error);
                }

            }
            catch (Exception emailEx)
            {
                CommonFunction.ConsoleWriteLine("Failed to send output file " + strFile + " - " + emailEx.Message, LogCategory.Error);
            }
        }

        private void UpdateFilmLoadStatus(int filmIntNo, int Status)
        {
            FilmService filmService = new FilmService();
            Film film = filmService.GetByFilmIntNo(filmIntNo);

            film.FilmLoadStatus = Status;
            // 2013-07-17 add LastUser by Henry
            film.LastUser = string.Format("{0}_{1}", APPNAME, GlobalVariates<User>.CurrentUser.UserLoginName);
            filmService.Save(film);
        }

        public bool ProcessViolations(string strFileName)
        {
            DataSet dsData = new DataSet();

            dsData.ReadXmlSchema("./DataSetSchema.xsd");
            dsData.ReadXml(strFileName);

            if (dsData.Tables.Count < 3)
            {
                CommonFunction.ConsoleWriteLine("Load XML file filed! " + strFileName, LogCategory.Error);
                return false;
            }

            //test 
            //foreach(DataRow dr in dsData.Tables[1].Rows)
            //{
            //    dr["OffenceLetter"] = "R";
            //    dr["OffenceCode"] = "2053";
            //}

            string autCode = dsData.Tables[0].Rows[0]["AuthCode"].ToString();
            Authority authority = new AuthorityService().GetByAutCode(autCode);
            if (authority == null)
            {
                CommonFunction.ConsoleWriteLine("GetAuthorityDetailsByAutCode failed!", LogCategory.Error);
                return false;
            }

            int autIntNo = authority.AutIntNo;

            try
            {
                //get the rule for working out whether Natis must be completed 1st/last
                // Check if this authority processes NaTIS data last

                //  20090113 SD:
                AuthorityRulesDetails arDetails0400 = new AuthorityRulesDetails();
                arDetails0400.AutIntNo = autIntNo;
                arDetails0400.ARCode = "0400";
                arDetails0400.LastUser = APPNAME;
                DefaultAuthRules authRule0400 = new DefaultAuthRules(arDetails0400);
                KeyValuePair<int, string> value0400 = authRule0400.SetDefaultAuthRule();

                string msg = "";
                if (value0400.Value.Equals("Y"))
                {
                    _natisLast = true;
                    msg = "Natis files will be sent after Adjudication";
                }
                else
                {
                    _natisLast = false;
                    msg = "Natis files will be sent before Verification";
                }
                CommonFunction.ConsoleWriteLine("ProcessViolations: " + msg + " for Authority " + autCode);

                AuthorityRulesDetails arDetails2510 = new AuthorityRulesDetails();
                arDetails2510.AutIntNo = autIntNo;
                arDetails2510.ARCode = "2510";
                arDetails2510.LastUser = APPNAME;
                DefaultAuthRules authRule2510 = new DefaultAuthRules(arDetails2510);
                KeyValuePair<int, string> value2510 = authRule2510.SetDefaultAuthRule();

                // 1: print only the one image (A)
                // 2: print two images (A) and (B)
                // 3: print two images (A) and (R)
                // 4: print three images (A) and (B) - if it exists - and (R)
                // 5: print three images (A) and (R) and (D) - speed only

                _noOfImagesToPrint = value2510.Key; //arDetails.ARNumeric;

                CommonFunction.ConsoleWriteLine("ProcessViolations: No of Images to print for Authority " + autCode + ": " + _noOfImagesToPrint.ToString());

                // SD:20090113
                AuthorityRulesDetails arule2520 = new AuthorityRulesDetails();
                arule2520.AutIntNo = autIntNo;
                arule2520.ARCode = "2520";
                arule2520.LastUser = APPNAME;
                DefaultAuthRules authRule2520 = new DefaultAuthRules(arule2520);
                KeyValuePair<int, string> value2520 = authRule2520.SetDefaultAuthRule();

                _whichImageTraffiPax = value2520.Key; //arDetails.ARNumeric;

                CommonFunction.ConsoleWriteLine("ProcessViolations: TrafficPax Image to print for Authority " + autCode + ": " + _whichImageTraffiPax.ToString());

                AuthorityRulesDetails arule2530 = new AuthorityRulesDetails();
                arule2530.AutIntNo = autIntNo;
                arule2530.ARCode = "2530";
                arule2530.LastUser = APPNAME;
                DefaultAuthRules authRule2530 = new DefaultAuthRules(arule2530);
                KeyValuePair<int, string> value2530 = authRule2530.SetDefaultAuthRule();

                _whichImageDigital = value2530.Key; //arDetails.ARNumeric;

                CommonFunction.ConsoleWriteLine("ProcessViolations: Digital Image to print for Authority " + autCode + ": " + _whichImageDigital.ToString());
                AuthorityRulesDetails adj100pc = new AuthorityRulesDetails();

                adj100pc.AutIntNo = autIntNo;
                adj100pc.ARCode = "0600";
                adj100pc.LastUser = APPNAME;

                DefaultAuthRules defAuthRule = new DefaultAuthRules(adj100pc);
                KeyValuePair<int, string> adj100pcRule = defAuthRule.SetDefaultAuthRule();
                _fullAdjudicated = adj100pcRule.Value;

                //dls 090617 - they are not allowed to adjudicate at 100% if they are using Natis Last = true
                if (_natisLast && _fullAdjudicated.Equals("Y"))
                {
                    CommonFunction.ConsoleWriteLine("Authority " + autCode + " may not have Natis last = True and Adjudication @ 100% = true!", LogCategory.Error);
                    return false;
                }

                //vehicle exclusions rule
                AuthorityRulesDetails arDetails8000 = new AuthorityRulesDetails();
                arDetails8000.AutIntNo = autIntNo;
                arDetails8000.ARCode = "8000";
                arDetails8000.LastUser = APPNAME;
                DefaultAuthRules authRule8000 = new DefaultAuthRules(arDetails8000);
                KeyValuePair<int, string> value8000 = authRule8000.SetDefaultAuthRule();

                _excluListActive = value8000.Value;

                //max speed over speed limit rule
                AuthorityRulesDetails arDetails8010 = new AuthorityRulesDetails();
                arDetails8010.AutIntNo = autIntNo;
                arDetails8010.ARCode = "8010";
                arDetails8010.LastUser = APPNAME;
                DefaultAuthRules authRule8010 = new DefaultAuthRules(arDetails8010);
                KeyValuePair<int, string> value8010 = authRule8010.SetDefaultAuthRule();

                _excluSpeedAllowance = value8010.Key;

            }
            catch (Exception e)
            {
                CommonFunction.ConsoleWriteLine(e.Message, LogCategory.Error, e);
                return false;
            }

            // check that all frames have at least one image
            if(!CheckAllImages(dsData))
                return false;

            bool bResult = false;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromSeconds(300)))
            {
                if (ProcessViolationsTextFile(dsData, autIntNo, strFileName))
                    bResult = true;

                DeleteTempViolations(autIntNo);

                if(bResult)
                    scope.Complete();
            }
            return bResult;
        }

        private bool CheckAllImages(DataSet dsData)
        {
            try
            {
                string strFilmType = dsData.Tables[0].Rows[0]["FilmType"].ToString();
                foreach(DataRow drFrame in dsData.Tables[1].Rows)
                {
                    bool foundImage = false;
                    string strFrameNo = drFrame["FrameNo"].ToString();
                    string strFilmNo = drFrame["FilmNo"].ToString();
                    string strAuthCode = drFrame["AuthCode"].ToString();

                    foreach (DataRow drImg in dsData.Tables[2].Rows)
                    {
                        if(strFrameNo == drImg["FrameNo"].ToString()
                            && strFilmNo == drImg["FilmNo"].ToString()
                            && strAuthCode == drImg["AuthCode"].ToString()
                            && GetPrintValue(strFilmType, drImg["JPegName"].ToString(), drImg["ImageType"].ToString()) == 1)
                        {
                            foundImage = true;
                            break;
                        }
                    }

                    if(!foundImage)
                    {
                        CommonFunction.ConsoleWriteLine("Image not found for Frame: " + strFrameNo, LogCategory.Error);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonFunction.ConsoleWriteLine("CheckAllImages: Error " + ex.Message + " " + DateTime.Now.ToString(), LogCategory.Error, ex);
                return false;
            }
            return true;
        }

        public bool ProcessViolationsTextFile(DataSet dsData, int autIntNo, string strFileName)
        {
            int noOfHeaderRecords = 0;
            int noOfDataRecords = 0;
            int noOfImageRecords = 0;
            int noOfFailedImageRecords = 0;

            string filePath = System.IO.Path.GetDirectoryName(strFileName);

            ArrayList filmsLoaded = new ArrayList();
            LoadViolationsDB load = new LoadViolationsDB(_connStr);

            //use an arraylist to add all the data so that we don't keep 
            ArrayList violations = new ArrayList();

            try
            {
                //delete all temp violations
                if (!DeleteTempViolations(autIntNo))
                    return false;

                if (!ProcessHeaderRow(dsData.Tables[0].Rows[0], autIntNo, ref filmsLoaded, strFileName, dsData.Tables[1].Rows.Count))
                    return false;
                else
                    noOfHeaderRecords += 1;

                foreach (DataRow drFrame in dsData.Tables[1].Rows)
                {
                    try
                    {
                        LoadInputDataFromTextFile(ref violations, drFrame, autIntNo, strFileName);
                        noOfDataRecords += 1;
                    }
                    catch (Exception e)
                    {
                        CommonFunction.ConsoleWriteLine("ReadDataRow " + e.Message);
                        CreateErrorFile(0, 0, strFileName, "Unable to read data from file " + e.Message, "header_only", false);
                        return false;
                    }
                }

                if (!ValidateAndLoadInputData(ref violations, autIntNo, strFileName))
                {
                    return false;
                }
                violations.Clear();

                List<int> processedFrames = new List<int>();
                foreach (DataRow drImg in dsData.Tables[2].Rows)
                {
                    string jpegName = drImg["JpegName"].ToString();
                    string strFrameNo = drImg["FrameNo"].ToString();
                    string strFilmNo = drImg["FilmNo"].ToString();

                    try
                    {
                        if (!ProcessImageRow(drImg, autIntNo, jpegName, strFileName, ref processedFrames))
                            noOfImageRecords++;
                        else
                            noOfFailedImageRecords++;
                    }
                    catch (Exception e)
                    {
                        //failed = true; - no point setting this to true - it gets over written ==> checking the no. of failer records instead
                        noOfFailedImageRecords++;
                        CommonFunction.ConsoleWriteLine("Processing image file " + jpegName + " " + e.Message);

                        Int32 tlvIntNo = load.AddTempLoadViolationsErrors(autIntNo, strFilmNo, "Processing image file failed " + " " + e.Message, "JPeg name", jpegName, "", strFrameNo);
                    }
                }
            }
            catch (Exception e)
            {
                CommonFunction.ConsoleWriteLine("Processing filestream " + strFileName + " " + e.Message);
                CreateErrorFile(autIntNo, _filmIntNo, strFileName, "ProcessViolationsTextFile failed:" + e.Message, "header_only", false);
                return false;
            }

            CommonFunction.ConsoleWriteLine("Header rows processed: " + noOfHeaderRecords.ToString());
            CommonFunction.ConsoleWriteLine("Data rows processed: " + noOfDataRecords.ToString());
            CommonFunction.ConsoleWriteLine("Image rows processed: " + noOfImageRecords.ToString());

            ViolationLoaderDB film = new ViolationLoaderDB(this._connStr);
            for (int i = 0; i < filmsLoaded.Count; i++)
            {
                int filmintNo = film.DeleteFilm(Convert.ToInt32(filmsLoaded[i]));
            }

            if (noOfDataRecords > 0 && noOfImageRecords == 0)
            {
                CommonFunction.ConsoleWriteLine("Input file contains 0 image rows - unable to process film");
                CreateErrorFile(autIntNo, _filmIntNo, strFileName, "Input file contains 0 image rows - unable to process film", "header_only", false);
            }
            else if (noOfFailedImageRecords > 0)
            {
                CommonFunction.ConsoleWriteLine( "Input file contains " + noOfFailedImageRecords.ToString() + " failed image rows - unable to process film");
                CreateErrorFile(autIntNo, _filmIntNo, strFileName, "Error processing image files", "data", false);
                return false;
            }
            else
                CreateErrorFile(autIntNo, _filmIntNo, strFileName, "All frames processed correctly", "header_only", true);

            return true;
        }

        public bool ProcessHeaderRow(DataRow dr, int autIntNo, ref ArrayList filmsLoaded, string strFileName, int NoOfFrames)
        {
            FilmService filmService = new FilmService();
            Film film = filmService.GetByFilmNo(dr["FilmNo"].ToString());

            if (film == null || film.AutIntNo != autIntNo)
            {
                //all systems go, we can continue
                film = new Film();
            }
            else if (film.FilmLoadStatus >= STATUS_FILM_SUCCESSFUL && film.ValidateDataDateTime == null)
            {
                CommonFunction.ConsoleWriteLine("Film " + film.FilmNo + " has already been successfully loaded, re-import!");
                //return false;
            }
            else if (film.FilmLoadStatus >= STATUS_FILM_SUCCESSFUL && film.ValidateDataDateTime != null)
            {
                CommonFunction.ConsoleWriteLine("Violations on film " + film.FilmNo + " have already been validated");
                return false;
            }

            string offenceDateStr = dr["FirstOffenceDateYear"].ToString() + "/" + dr["FirstOffenceDateMonth"].ToString()
                + "/" + dr["FirstOffenceDateDay"].ToString() + " " + dr["FirstOffenceDateHour"].ToString() + ":"
                + dr["FirstOffenceDateMinute"].ToString() + ":00";

            DateTime offenceDate = Convert.ToDateTime(offenceDateStr);

            film.AutIntNo = autIntNo;
            film.FilmNo = dr["FilmNo"].ToString();
            film.CdLabel = dr["CDLabel"].ToString();
            film.NoOfFrames = NoOfFrames;
            if (dr["NoOfScans"] != DBNull.Value)film.NoOfScans = Convert.ToInt32(dr["NoOfScans"]);
            film.MultipleFrames = dr["MultipleFrames"].ToString();
            film.MultipleViolations = dr["MultipleViolations"].ToString();
            film.FilmDescr = dr["FilmDescr"].ToString();
            film.LastRefNo = Convert.ToInt32(dr["LastRefNo"]);
            film.FilmType = dr["FilmType"].ToString();
            film.FilmLoadDateTime = DateTime.Now;
            film.FilmLoadType = "V";
            film.FirstOffenceDate = offenceDate;
            film.Film1stOffenceDateYear = offenceDate.Year.ToString();
            film.Film1stOffenceDateMonth = offenceDate.Month.ToString();
            film.LastUser = APPNAME;
            film.FilmLoadFileName = Path.GetFileName(strFileName);
            if (dr["NoOfFrames"] != DBNull.Value) film.NoOfFrames = (int)dr["NoOfFrames"];

            film = filmService.Save(film);
            if (film.FilmIntNo == 0)
            {
                CommonFunction.ConsoleWriteLine("Unable to add film: " + film.FilmNo, LogCategory.Error);
                CreateErrorFile(autIntNo, 0, strFileName, "Unable to add film " + film.FilmNo , "header_only", false);
                return false;
            }
            else
            {
                filmsLoaded.Add(film.FilmIntNo);
                FilmsToLoad.Add(film.FilmNo);
                _filmIntNo = film.FilmIntNo;
                CommonFunction.ConsoleWriteLine("ProcessHeaderRow: header row for Film " + film.FilmNo + " successfully added " + DateTime.Now.ToString());
            }
            return true;
        }

        private bool ValidateAndLoadInputData(ref ArrayList violations, int autIntNo, string fileName)
        {
            LoadViolationsDB loadViolations = new LoadViolationsDB(_connStr);

            //pass whole arraylist with all rows to class
            string errMessage = string.Empty;
            int noOfRecords = loadViolations.AddTempLoadViolations(violations, 0, ref errMessage);
            if (noOfRecords <= 0)
            {
                CommonFunction.ConsoleWriteLine("Unable to load data. " + errMessage, LogCategory.Error);
                CreateErrorFile(autIntNo, _filmIntNo, fileName, "Unable to load data for film", "header_only", false);
                return false;
            }

            //validate input data
            errMessage = string.Empty;
            int invalidRows = loadViolations.ValidateTempLoadViolations(autIntNo, 0,
                string.Format("{0}_{1}", APPNAME, GlobalVariates<User>.CurrentUser.UserLoginName),
                ref errMessage);

            if (!errMessage.Equals(string.Empty))
            {
                CommonFunction.ConsoleWriteLine("ValidateTempLoadViolations error - " + errMessage, LogCategory.Error);
                CreateErrorFile(autIntNo, _filmIntNo, fileName, "Data errors", "data", false);
                return false;
            }

            string error = "";
            if (invalidRows > 0)
            {
                error = "There";
                if (invalidRows != 1)
                    error += " are " + invalidRows + " invalid rows";
                else
                    error += " is " + invalidRows + " invalid row";

                string file = System.IO.Path.GetFileName(fileName);

                error += " - unable to load data. Please view error report and fix the errors before reloading file " + file;

                CommonFunction.ConsoleWriteLine(error, LogCategory.Error);
                CreateErrorFile(autIntNo, _filmIntNo, fileName, "Data errors", "data", false);
                return false;
            }

            if (invalidRows == -1)
            {
                CommonFunction.ConsoleWriteLine(error, LogCategory.Error);
                CreateErrorFile(autIntNo, _filmIntNo, fileName, "Data errors", "data", false);
                return false;
            }

            string errorMsg = "";
            int statusLoaded;

            if (_natisLast)
                statusLoaded = STATUS_NATIS_LAST;
            else
                statusLoaded = STATUS_NATIS_FIRST;

            //Add Authority Coding system
            Authority authority = new AuthorityService().GetByAutIntNo(autIntNo);
            string aut3PCodingSystem = authority.Aut3PcodingSystem;

            if ((!aut3PCodingSystem.Equals("S") && !aut3PCodingSystem.Equals("T") && !aut3PCodingSystem.Equals("C")))
                aut3PCodingSystem = "S";

            ViolationLoaderDB rejDB = new ViolationLoaderDB(_connStr);
            //add camera set up rejection reason - this one actually uses the string RejResaon in the SP, not the integer key
            string lowSpeedRejReason = "Camera Setup - incorrect speed limit";

            int lowSpeedRejIntNo = rejDB.UpdateRejection(0, lowSpeedRejReason, "N", APPNAME, 0, "Y");

            if (lowSpeedRejIntNo <= 0)
            {
                CommonFunction.ConsoleWriteLine("Unable to create reason for rejection: " + lowSpeedRejReason, LogCategory.Error);
                return false;
            }

            //add low speed rejection reason
            string invalidSpeed = "Zero or low speed - unable to prosecute";

            lowSpeedRejIntNo = rejDB.UpdateRejection(0, invalidSpeed, "N", APPNAME, 0, "Y");

            if (lowSpeedRejIntNo <= 0)
            {
                CommonFunction.ConsoleWriteLine("Unable to create reason for rejection: " + invalidSpeed, LogCategory.Error);
                return false;
            }

            //add vehicle exclusion rejection reason
            string exclRejReason = "Official vehicle (under assigned limit)";

            int exclRejIntNo = rejDB.UpdateRejection(0, exclRejReason, "N", APPNAME, 0, "Y");

            if (exclRejIntNo <= 0)
            {
                CommonFunction.ConsoleWriteLine("Unable to create reason for rejection: " + exclRejReason, LogCategory.Error);
                return false;
            }

            string expiredFrame = "Expired. The frame offence date is too old to continue";

            int rejIntNoExpired = rejDB.UpdateRejection(0, expiredFrame, "N", APPNAME, 0, "Y");

            if (rejIntNoExpired <= 0)
            {
                CommonFunction.ConsoleWriteLine("Unable to create reason for rejection: " + expiredFrame, LogCategory.Error);
                return false;
            }

            //violation cutoff
            SysParam param = new SysParamService().GetBySpColumnName("ViolationCutOff");
            if (param == null)
            {
                param = new SysParam();
                param.SpColumnName = "ViolationCutOff";
                param.SpDescr = "Rule to generate compressed images for notices";
                param.SpIntegerValue = 0;
                param.SpStringValue = "Y";
                param.LastUser = APPNAME;
            }
            string cutOff = param.SpStringValue.Equals("-") ? "Y" : param.SpStringValue;

            //authority rule for bypassing verification for specific reasons for rejection
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = autIntNo;
            arDetails.ARCode = "0510";
            arDetails.LastUser = APPNAME;

            DefaultAuthRules defAR = new DefaultAuthRules(arDetails);
            KeyValuePair<int, string> aRule = defAR.SetDefaultAuthRule();
            string skipRejReasons = aRule.Value;

            //authority rule for bypassing verification for RegNo = 00000000
            arDetails.ARCode = "0520";
            arDetails.LastUser = APPNAME;

            defAR = new DefaultAuthRules(arDetails);
            aRule = defAR.SetDefaultAuthRule();
            string skipZeroes = aRule.Value;

            //no of days from Offence Date to Issue Date
            DateRulesDetails drDetails = new DateRulesDetails();
            drDetails.AutIntNo = autIntNo;
            drDetails.DtRStartDate = "NotOffenceDate";
            drDetails.DtREndDate = "NotIssue1stNoticeDate";
            drDetails.LastUser = APPNAME;

            DefaultDateRules dr = new DefaultDateRules(drDetails);
            int noOfDays = dr.SetDefaultDateRule();

            int success = loadViolations.AddFrameFromTempLoadViolations(autIntNo, 0, APPNAME,
                ref errorMsg, statusLoaded, aut3PCodingSystem, lowSpeedRejReason, cutOff, skipRejReasons, skipZeroes,
                lowSpeedRejIntNo, noOfDays, _fullAdjudicated, _excluListActive, _excluSpeedAllowance, exclRejIntNo,
                STATUS_OFFICIAL_VEHICLE_EXCLUSION, this.asdInvalidSetup, this.asdInvalidRejIntNo, rejIntNoExpired);

            if (success == 0)
            {
                CommonFunction.ConsoleWriteLine("Data rows processed successfully");
            }
            else if (success == 1)
            {
                CommonFunction.ConsoleWriteLine("No date rules for authority", LogCategory.Error); ;

                CreateErrorFile(autIntNo, _filmIntNo, fileName, "No date rules for authority", "header_only", false);
                return false;
            }
            else if (success == 3)
            {
                CommonFunction.ConsoleWriteLine("Some frames have offence codes that do not exist in TMS - Frame_Offence records not added ");
            }
            else if (success == -1)
            {
                CommonFunction.ConsoleWriteLine(errorMsg, LogCategory.Error); ;

                CreateErrorFile(autIntNo, _filmIntNo, fileName, "Unable to process data rows " + errMessage, "header_only", false);
                return false;
            }
            return true;
        }

        private void LoadInputDataFromTextFile(ref ArrayList violations, DataRow data, int autIntNo,  string fileName)
        {
            LoadViolationsDetail lvDetail = new LoadViolationsDetail();

            lvDetail.CameraID = data["CamUnNO"].ToString();
            lvDetail.OffenceType = data["OffenceLetter"].ToString();
            lvDetail.FilmNo = data["FilmNo"].ToString();
            lvDetail.FineAlloc = data["FineAllocation"].ToString();
            lvDetail.FrameNo = data["FrameNo"].ToString();
            lvDetail.LaneNo = "0"; //data["LaneNo;		//missing from file - not yet in ARS
            lvDetail.LocDescr = data["LocDescr"].ToString();
            lvDetail.LocCode = data["CameraLocCode"].ToString();
            lvDetail.OffenceCode = data["OffenceCode"].ToString();

            string offenceDateStr = data["OffenceDateYear"].ToString() + "/" + data["OffenceDateMonth"].ToString()
               + "/" + data["OffenceDateDay"].ToString() + " " + data["OffenceDateHour"].ToString() + ":"
               + data["OffenceDateMinute"].ToString() + ":00";

            lvDetail.OffenceDate = Convert.ToDateTime(offenceDateStr);
            lvDetail.OffenderType = data["OffenderType"].ToString();
            lvDetail.OfficerSName = data["OfficerSName"].ToString();
            lvDetail.OfficerInit = data["OfficerInit"].ToString();
            lvDetail.OfficerGroup = data["OfficerGroup"].ToString();
            lvDetail.OfficerNo = data["OfficerNo"].ToString();
            lvDetail.RefNo = data["ReferenceNo"].ToString();
            lvDetail.RegNo = data["RegNo"].ToString();
            lvDetail.Speed1 = Convert.ToInt32(data["FirstSpeed"]);
            lvDetail.Speed2 = Convert.ToInt32(data["SecondSpeed"]);
            lvDetail.SeqNo = data["Sequence"].ToString();
            lvDetail.Source = "LV";			//data["Source"].ToString();
            lvDetail.SpeedLimit = data["SpeedZone"].ToString();
            lvDetail.VehicleMake = data["VehMDescr"].ToString();
            lvDetail.VehicleType = data["VehTDescr"].ToString();
            lvDetail.VehicleMakeCode = data["VehMCode"].ToString();
            lvDetail.VehicleTypeCode = data["VehTCode"].ToString();
            lvDetail.CDLabel = data["CDLabel"].ToString();
            lvDetail.JPegNameA = "";
            lvDetail.JPegNameB = "";
            lvDetail.RegNoImage = "";
            lvDetail.RdTypeCode = data["RdTypeCode"].ToString();
            lvDetail.RdTypeDescr = data["RdTypeDescr"].ToString();
            lvDetail.ElapsedTime = data["ElapsedTime"].ToString();
            lvDetail.TravelDirection = data["TravelDirection"].ToString();
            lvDetail.CourtNo = data["CourtNo"].ToString();
            lvDetail.FileDate = DateTime.Now;
            lvDetail.CourtName = data["CourtName"].ToString();
            lvDetail.AutIntNo = autIntNo;
            lvDetail.RejReason = data["RejectReason"].ToString();
            lvDetail.ManualView = data["ManualView"].ToString();
            lvDetail.Violation = data["Violation"].ToString();
            lvDetail.ConfirmViolation = data["ConfirmViolation"].ToString();
            lvDetail.MultFrames = data["MultFrames"].ToString();
            lvDetail.Interface = "";
            lvDetail.CamSerialNo = data["CamSerialNo"].ToString();

            if (data["ASDGPSDateTime1"] != DBNull.Value)
                lvDetail.ASDGPSDateTime1 = Convert.ToDateTime(data["ASDGPSDateTime1"]);
            if (data["ASDGPSDateTime2"] != DBNull.Value)
                lvDetail.ASDGPSDateTime2 = Convert.ToDateTime(data["ASDGPSDateTime2"]);

            lvDetail.ASDTimeDifference = Convert.ToInt32(data["ASDTimeDifference"]);
            lvDetail.ASDSectionStartLane = Convert.ToInt32(data["ASDSectionStartLane"]);
            lvDetail.ASDSectionEndLane = Convert.ToInt32(data["ASDSectionEndLane"]);
            lvDetail.ASDSectionDistance = Convert.ToInt32(data["ASDSectionDistance"]);

            lvDetail.ASD1stCamUnitID = data["ASD1stCamUnitID"].ToString();
            lvDetail.ASD2ndCamUnitID = data["ASD2ndCamUnitID"].ToString();
            lvDetail.ASDCameraSerialNo1 = data["ASDCameraSerialNo1"].ToString();
            lvDetail.ASDCameraSerialNo2 = data["ASDCameraSerialNo2"].ToString();

            //Jerry 2013-04-24 add
            lvDetail.ParentFrameNo = data["ParentFrameNo"].ToString();

            violations.Add(lvDetail);
        }

        public bool ProcessImageRow(DataRow imageData, int autIntNo, string jpegName, string fileName, ref List<int> processedFrames)
        {
            bool failed = false;
            LoadViolationsDB load = new LoadViolationsDB(_connStr);

            string frameNo = imageData["FrameNo"].ToString();
            string filmNo = imageData["FilmNo"].ToString();

            FilmService filmService = new FilmService();
            Film film = filmService.GetByFilmNo(filmNo);

            if (film == null || film.AutIntNo != autIntNo)
            {
                CommonFunction.ConsoleWriteLine("Film " + filmNo + " does not exist", LogCategory.Error);

                Int32 tlvIntNo = load.AddTempLoadViolationsErrors(autIntNo, filmNo, "Cannot load image as film does not exist", "Image", "", "", filmNo);
                return true;
            }
            else if (film.FilmLoadStatus >= STATUS_FILM_SUCCESSFUL && film.ValidateDataDateTime == null)
            {
                //CommonFunction.ConsoleWriteLine("Film " + film.FilmNo + " has already been successfully loaded");
                //re - import
            }
            else if (film.FilmLoadStatus >= STATUS_FILM_SUCCESSFUL && film.ValidateDataDateTime != null)
            {
                CommonFunction.ConsoleWriteLine("Violations on film " + film.FilmNo + " have already been validated");
                return true;
            }

            FrameService frameService = new FrameService();
            Frame frame = frameService.GetByFilmIntNoFrameNo(film.FilmIntNo, frameNo);

            if (frame == null || frame.FrameIntNo == 0)
            {
                CommonFunction.ConsoleWriteLine("Frame " + frameNo + " does not exist", LogCategory.Error);

                Int32 tlvIntNo = load.AddTempLoadViolationsErrors(autIntNo, filmNo, "Cannot load image as frame does not exist", "Image", "", "", frameNo);
                failed = true;
                return failed;
            }

            ScanImageService scanImageService = new ScanImageService();

            //Fred: Check processed frames, delete old data
            if(!processedFrames.Contains(frame.FrameIntNo))
            {
                TList<ScanImage> images = scanImageService.GetByFrameIntNo(frame.FrameIntNo);
                scanImageService.Delete(images);

                processedFrames.Add(frame.FrameIntNo);
            }

            int scImPrintVal = this.GetPrintValue(film.FilmType, imageData["JPegName"].ToString(), imageData["ImageType"].ToString());

            //add ScanImage row
            ScanImage scaImage = new ScanImage();
            scaImage.FrameIntNo = frame.FrameIntNo;
            scaImage.ScImType = imageData["ImageType"].ToString();
            scaImage.JpegName = imageData["JpegName"].ToString();
            scaImage.ScImPrintVal = scImPrintVal;
            scaImage.Xvalue = Convert.ToInt32(imageData["XValue"]);
            scaImage.Yvalue = Convert.ToInt32(imageData["YValue"]);
            scaImage.LastUser = APPNAME;

            scaImage = scanImageService.Save(scaImage);

            if (scaImage.ScImIntNo == 0)
            {
                CommonFunction.ConsoleWriteLine(string.Format("Unable to load ScanImage for frame {0} and jpeg {1}", frameNo, scaImage.JpegName), LogCategory.Error);

                Int32 tlvIntNo = load.AddTempLoadViolationsErrors(autIntNo, filmNo, "Save scaImage failed! ", "Jpeg name", scaImage.JpegName, "", frameNo);
                failed = true;
                return failed;
            }
            return failed;
        }

        private int GetPrintValue(string filmType, string jpegName, string imageType)
        {
            int scImPrintVal = 0;
            string whichImage_1st = DIG_FILM_FIRST_IMAGE;
            string whichImage_2nd = DIG_FILM_SPD_2ND_IMAGE;

            switch (filmType)
            {
                case "O":
                    if (jpegName.ToLower().Contains("_001.j") && imageType.Equals("A"))
                        scImPrintVal = 1;
                    else if (jpegName.ToLower().Contains("002.j") && imageType.Equals("A"))
                    //else if (jpegName.ToLower().Contains("002.j") && imageType.Equals("B"))
                        scImPrintVal = 2;
                    else if (imageType.Equals("R"))
                        scImPrintVal = 3;
                    else if (imageType.Equals("D"))
                        scImPrintVal = 4;
                    break;

                case "N":               //normal wet film             
                    switch (_noOfImagesToPrint)
                    {
                        case 0:
                            scImPrintVal = 0;
                            break;

                        case 1:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            break;

                        case 2:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            break;

                        case 3:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            break;

                        case 4:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 3;
                            else if (imageType.Equals("D"))
                                scImPrintVal = 4;
                            break;

                        case 5:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("D"))
                                scImPrintVal = 3;
                            break;
                    }

                    break;

                case "Y":               //truvella wet film
                    switch (_noOfImagesToPrint)
                    {
                        case 0:
                            scImPrintVal = 0;
                            break;

                        case 1:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            break;

                        case 2:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            break;

                        case 3:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            break;

                        case 4:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 3;
                            else if (imageType.Equals("D"))
                                scImPrintVal = 4;
                            break;

                        case 5:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("D"))
                                scImPrintVal = 3;
                            break;

                    }
                    break;

                case "P":               //TraffiPax digital speed film
                case "S":               //SafeTCam digital speed film
                case "T":               //TruCam digital speed film
                    if (_whichImageTraffiPax != 1)
                    {
                        whichImage_1st = DIG_FILM_SPD_2ND_IMAGE;
                        whichImage_2nd = DIG_FILM_FIRST_IMAGE;
                    }

                    switch (_noOfImagesToPrint)
                    {
                        case 0:
                            scImPrintVal = 0;
                            break;

                        case 1:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(whichImage_2nd) && imageType.Equals("A"))
                                scImPrintVal = 2;
                            break;

                        case 2:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            break;

                        case 3:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            break;

                        case 4:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 3;
                            else if (imageType.Equals("D"))
                                scImPrintVal = 4;
                            break;

                        case 5:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("D"))
                                scImPrintVal = 3;
                            break;

                    }
                    break;

                case "D":               //red light digital film
                    //DLS 070608 - need to introduce AuthRule to determine which image is the for printing

                    if (_whichImageDigital != 1)
                    {
                        whichImage_1st = DIG_FILM_SPD_2ND_IMAGE;
                        whichImage_2nd = DIG_FILM_FIRST_IMAGE;
                    }

                    switch (_noOfImagesToPrint)
                    {
                        case 0:
                            scImPrintVal = 0;
                            break;

                        case 1:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(whichImage_2nd) && imageType.Equals("A"))
                                scImPrintVal = 2;
                            break;

                        case 2:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            break;

                        case 3:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            break;

                        case 4:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 3;
                            else if (imageType.Equals("D"))
                                scImPrintVal = 4;
                            break;

                        case 5:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 3;
                            break;

                    }
                    break;

                case "A": //Anpri camera - single images for bus lane violations - dls 090410
                case "C":               //DigiCam film - single images
                case "B":               //Belstow camera - single images
                default:
                    switch (_noOfImagesToPrint)
                    {
                        case 0:
                            scImPrintVal = 0;
                            break;

                        case 1:
                            if (imageType.Equals("A"))
                                scImPrintVal = 1;
                            else
                                scImPrintVal = 0;
                            break;

                        case 2:
                            if (imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("B"))
                                scImPrintVal = 2;
                            else
                                scImPrintVal = 0;
                            break;

                        case 3:
                            if (imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            else
                                scImPrintVal = 0;
                            break;

                        case 4:
                            if (imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("B"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 3;
                            else
                                scImPrintVal = 4;
                            break;

                        case 5:
                            if (imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            else
                                scImPrintVal = 3;
                            break;
                    }
                    break;

                //default:
                //    break;
            }

            return scImPrintVal;
        }

        private bool DeleteTempViolations(ref StreamWriter writer, int autIntNo, int tlvType)
        {
            bool failed = false;

            //delete current rows for user in temp table
            LoadViolationsDB loadViolations = new LoadViolationsDB(_connStr);
            try
            {
                loadViolations.DeleteTempLoadViolations(autIntNo, tlvType);
            }
            catch (Exception e)
            {
                CommonFunction.ConsoleWriteLine("Unable to delete temporary table " + e.Message, LogCategory.Error, e);
                failed = true;
            }
            return failed;

        }

        private bool DeleteTempViolations(int autIntNo)
        {
            //clear temp file
            LoadViolationsDB load = new LoadViolationsDB(_connStr);
            try
            {
                load.DeleteTempLoadViolations(autIntNo, 0);
            }
            catch(Exception ex)
            {
                CommonFunction.ConsoleWriteLine("Problem deleting temporary loading tables!", LogCategory.Error, ex);
                return false;
            }
            return true;
        }
    }
}
