﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace SIL.AARTO.IMX.XMLLoader
{
    /// <summary>
    /// Run general procedures and functions
    /// </summary>

    public class LoadViolationsDetail
    {
        public string OffenceType;
        public string LocDescr;
        public string LocCode;
        public string SpeedLimit;
        public string LaneNo;
        public string CameraID;
        public string OfficerSName;
        public string OfficerInit;
        public string OfficerNo;
        public string OfficerGroup;
        public string RegNo;
        public int Speed1;
        public int Speed2;
        public string VehicleMake;
        public string VehicleType;
        public string VehicleMakeCode;
        public string VehicleTypeCode;
        public string OffenderType;
        public string FilmNo;
        public string FrameNo;
        public string RefNo;
        public string SeqNo;
        public DateTime OffenceDate;
        public string OffenceCode;
        public string FineAlloc;
        public string Source;
        public string Valid;
        public string CDLabel;
        public string JPegNameA;
        public string JPegNameB;
        public string RegNoImage;
        public DateTime FileDate;
        public string RdTypeCode;
        public string RdTypeDescr;
        public string CourtNo;
        public string CourtName;
        public string TravelDirection;
        public string ElapsedTime;
        public Int32 AutIntNo;
        public string RejReason;
        public string Violation;
        public string ConfirmViolation;
        public string ManualView;
        public string MultFrames;
        //dls 060801 - add Interface field to export
        public string Interface;
        //dls 061020 - add CamSerialNo field to export
        public string CamSerialNo;

        //FT 100505 For Average speed over distance 
        //public int ASD2ndCameraIntNo;
        public DateTime ASDGPSDateTime1;
        public DateTime ASDGPSDateTime2;

        public int ASDTimeDifference;
        public int ASDSectionStartLane;
        public int ASDSectionEndLane;
        public int ASDSectionDistance;

        public string ASD1stCamUnitID;
        public string ASD2ndCamUnitID;
        public int LCSIntNo;
        public string ASDCameraSerialNo1;
        public string ASDCameraSerialNo2;
        //Jerry 2013-04-24 add
        public string ParentFrameNo; 
    }

    public class LoadViolationsDB
    {
        // fields
        private readonly string _connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadViolationsDB"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public LoadViolationsDB(string connectionString)
        {
            this._connectionString = connectionString;
        }

        //this arraylist consists of many instances of 
        //the LoadViolations struct
        /// <summary>
        /// Adds the temp load violations.
        /// </summary>
        /// <param name="violations">The violations.</param>
        /// <param name="tlvType">Type of the TLV.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <returns>The number of violations loaded</returns>
        public int AddTempLoadViolations(ArrayList violations, int tlvType, ref string errorMessage)
        {
            // Create Instance of Connection and Command Object
            SqlConnection con = new SqlConnection(_connectionString);
            SqlCommand cmd = new SqlCommand("TempLoadViolationsAdd", con);

            // Mark the Command as a SPROC
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();// change by Paole 2013-04-24
            for (int i = 0; i < violations.Count; i++)
            {
                //get next instance of detail
                LoadViolationsDetail lvDetail = (LoadViolationsDetail)violations[i];

                cmd.Parameters.Clear();

                // Add Parameters to SPROC
                SqlParameter parameterOffenceType = new SqlParameter("@OffenceType", SqlDbType.Char, 1);
                parameterOffenceType.Value = lvDetail.OffenceType;
                cmd.Parameters.Add(parameterOffenceType);

                SqlParameter parameterTLVType = new SqlParameter("@TLVType", SqlDbType.Int, 4);
                parameterTLVType.Value = tlvType;
                cmd.Parameters.Add(parameterTLVType);

                SqlParameter parameterLocDescr = new SqlParameter("@LocDescr", SqlDbType.VarChar, 150);
                parameterLocDescr.Value = lvDetail.LocDescr;
                cmd.Parameters.Add(parameterLocDescr);

                SqlParameter parameterLocCode = new SqlParameter("@LocCode", SqlDbType.VarChar, 15);
                parameterLocCode.Value = lvDetail.LocCode;
                cmd.Parameters.Add(parameterLocCode);

                SqlParameter parameterSpeedLimit = new SqlParameter("@SpeedLimit", SqlDbType.Int);
                parameterSpeedLimit.Value = lvDetail.SpeedLimit;
                cmd.Parameters.Add(parameterSpeedLimit);

                SqlParameter parameterLaneNo = new SqlParameter("@LaneNo", SqlDbType.Char, 1);
                parameterLaneNo.Value = lvDetail.LaneNo;
                cmd.Parameters.Add(parameterLaneNo);

                SqlParameter parameterCameraID = new SqlParameter("@CameraID", SqlDbType.VarChar, 15);
                parameterCameraID.Value = lvDetail.CameraID;
                cmd.Parameters.Add(parameterCameraID);

                SqlParameter parameterOfficer = new SqlParameter("@OfficerSName", SqlDbType.VarChar, 35);
                parameterOfficer.Value = lvDetail.OfficerSName;
                cmd.Parameters.Add(parameterOfficer);

                SqlParameter parameterOfficerInit = new SqlParameter("@OfficerInit", SqlDbType.VarChar, 5);
                parameterOfficerInit.Value = lvDetail.OfficerInit;
                cmd.Parameters.Add(parameterOfficerInit);

                SqlParameter parameterOfficerNo = new SqlParameter("@OfficerNo", SqlDbType.VarChar, 10);
                parameterOfficerNo.Value = lvDetail.OfficerNo;
                cmd.Parameters.Add(parameterOfficerNo);

                SqlParameter parameterOfficerGroup = new SqlParameter("@OfficerGroup", SqlDbType.Char, 3);
                parameterOfficerGroup.Value = lvDetail.OfficerGroup;
                cmd.Parameters.Add(parameterOfficerGroup);

                SqlParameter parameterRegNo = new SqlParameter("@RegNo", SqlDbType.VarChar, 10);
                parameterRegNo.Value = lvDetail.RegNo;
                cmd.Parameters.Add(parameterRegNo);

                SqlParameter parameterSpeed1 = new SqlParameter("@Speed1", SqlDbType.Int);
                parameterSpeed1.Value = lvDetail.Speed1;
                cmd.Parameters.Add(parameterSpeed1);

                SqlParameter parameterSpeed2 = new SqlParameter("@Speed2", SqlDbType.Int);
                parameterSpeed2.Value = lvDetail.Speed2;
                cmd.Parameters.Add(parameterSpeed2);

                SqlParameter parameterVehicleMake = new SqlParameter("@VehicleMake", SqlDbType.VarChar, 30);
                parameterVehicleMake.Value = lvDetail.VehicleMake;
                cmd.Parameters.Add(parameterVehicleMake);

                SqlParameter parameterVehicleType = new SqlParameter("@VehicleType", SqlDbType.VarChar, 30);
                parameterVehicleType.Value = lvDetail.VehicleType;
                cmd.Parameters.Add(parameterVehicleType);

                SqlParameter parameterVehicleMakeCode = new SqlParameter("@VehicleMakeCode", SqlDbType.VarChar, 3);
                parameterVehicleMakeCode.Value = lvDetail.VehicleMakeCode;
                cmd.Parameters.Add(parameterVehicleMakeCode);

                SqlParameter parameterVehicleTypeCode = new SqlParameter("@VehicleTypeCode", SqlDbType.VarChar, 3);
                parameterVehicleTypeCode.Value = lvDetail.VehicleTypeCode;
                cmd.Parameters.Add(parameterVehicleTypeCode);

                SqlParameter parameterOffenderType = new SqlParameter("@OffenderType", SqlDbType.Char, 1);
                parameterOffenderType.Value = lvDetail.OffenderType;
                cmd.Parameters.Add(parameterOffenderType);

                SqlParameter parameterFilmNo = new SqlParameter("@FilmNo", SqlDbType.VarChar, 20);
                parameterFilmNo.Value = lvDetail.FilmNo;
                cmd.Parameters.Add(parameterFilmNo);

                SqlParameter parameterFrameNo = new SqlParameter("@FrameNo", SqlDbType.VarChar, 20);
                parameterFrameNo.Value = lvDetail.FrameNo;
                cmd.Parameters.Add(parameterFrameNo);

                SqlParameter parameterRefNo = new SqlParameter("@RefNo", SqlDbType.VarChar, 20);
                parameterRefNo.Value = lvDetail.RefNo;
                cmd.Parameters.Add(parameterRefNo);

                SqlParameter parameterSeqNo = new SqlParameter("@SeqNo", SqlDbType.Char, 1);
                parameterSeqNo.Value = lvDetail.SeqNo;
                cmd.Parameters.Add(parameterSeqNo);

                SqlParameter parameterOffenceDate = new SqlParameter("@OffenceDate", SqlDbType.DateTime);
                parameterOffenceDate.Value = lvDetail.OffenceDate;
                cmd.Parameters.Add(parameterOffenceDate);

                SqlParameter parameterOffenceCode = new SqlParameter("@OffenceCode", SqlDbType.VarChar, 15);
                parameterOffenceCode.Value = lvDetail.OffenceCode;
                cmd.Parameters.Add(parameterOffenceCode);

                SqlParameter parameterFineAlloc = new SqlParameter("@FineAlloc", SqlDbType.VarChar, 15);
                parameterFineAlloc.Value = lvDetail.FineAlloc;
                cmd.Parameters.Add(parameterFineAlloc);

                SqlParameter parameterSource = new SqlParameter("@Source", SqlDbType.Char, 3);
                parameterSource.Value = lvDetail.Source;
                cmd.Parameters.Add(parameterSource);

                SqlParameter parameterCDLabel = new SqlParameter("@CDLabel", SqlDbType.VarChar, 25);
                parameterCDLabel.Value = lvDetail.CDLabel;
                cmd.Parameters.Add(parameterCDLabel);

                SqlParameter parameterFileDate = new SqlParameter("@FileDate", SqlDbType.DateTime);
                parameterFileDate.Value = lvDetail.FileDate;
                cmd.Parameters.Add(parameterFileDate);

                SqlParameter parameterRdTypeCode = new SqlParameter("@RdTypeCode", SqlDbType.SmallInt);
                parameterRdTypeCode.Value = lvDetail.RdTypeCode;
                cmd.Parameters.Add(parameterRdTypeCode);

                SqlParameter parameterRdTypeDescr = new SqlParameter("@RdTypeDescr", SqlDbType.VarChar, 30);
                parameterRdTypeDescr.Value = lvDetail.RdTypeDescr;
                cmd.Parameters.Add(parameterRdTypeDescr);

                SqlParameter parameterCourtNo = new SqlParameter("@CourtNo", SqlDbType.VarChar, 6);
                parameterCourtNo.Value = lvDetail.CourtNo;
                cmd.Parameters.Add(parameterCourtNo);

                SqlParameter parameterCourtName = new SqlParameter("@CourtName", SqlDbType.VarChar, 30);
                parameterCourtName.Value = lvDetail.CourtName;
                cmd.Parameters.Add(parameterCourtName);

                SqlParameter parameterTravelDirection = new SqlParameter("@TravelDirection", SqlDbType.Char, 1);
                parameterTravelDirection.Value = lvDetail.TravelDirection;
                cmd.Parameters.Add(parameterTravelDirection);

                SqlParameter parameterElapsedTime = new SqlParameter("@ElapsedTime", SqlDbType.VarChar, 5);
                parameterElapsedTime.Value = lvDetail.ElapsedTime;
                cmd.Parameters.Add(parameterElapsedTime);

                SqlParameter parameterRejReason = new SqlParameter("@RejReason", SqlDbType.VarChar, 50);
                parameterRejReason.Value = lvDetail.RejReason;
                cmd.Parameters.Add(parameterRejReason);

                SqlParameter parameterManualView = new SqlParameter("@ManualView", SqlDbType.Char, 1);
                parameterManualView.Value = lvDetail.ManualView;
                cmd.Parameters.Add(parameterManualView);

                SqlParameter parameterMultFrames = new SqlParameter("@MultFrames", SqlDbType.Char, 1);
                parameterMultFrames.Value = lvDetail.MultFrames;
                cmd.Parameters.Add(parameterMultFrames);

                SqlParameter parameterViolation = new SqlParameter("@Violation", SqlDbType.Char, 1);
                parameterViolation.Value = lvDetail.Violation;
                cmd.Parameters.Add(parameterViolation);

                SqlParameter parameterConfirmViolation = new SqlParameter("@ConfirmViolation", SqlDbType.Char, 1);
                parameterConfirmViolation.Value = lvDetail.ConfirmViolation;
                cmd.Parameters.Add(parameterConfirmViolation);

                SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
                parameterAutIntNo.Value = lvDetail.AutIntNo;
                cmd.Parameters.Add(parameterAutIntNo);

                //dls 060801 - add Interface field to export
                SqlParameter parameterInterface = new SqlParameter("@Interface", SqlDbType.Char, 1);
                parameterInterface.Value = lvDetail.Interface;
                cmd.Parameters.Add(parameterInterface);

                //dls 061020 - add CamSerialNo field to export
                SqlParameter parameterCamSerialNo = new SqlParameter("@CamSerialNo", SqlDbType.VarChar, 18);
                parameterCamSerialNo.Value = lvDetail.CamSerialNo;
                cmd.Parameters.Add(parameterCamSerialNo);


                //FT 100505 For Average speed over distance 
                //SqlParameter parameterASD2ndCameraID = new SqlParameter("@ASD2ndCameraIntNo", SqlDbType.Int, 4);
                //parameterASD2ndCameraID.Value = lvDetail.ASD2ndCameraIntNo;
                //myCommand.Parameters.Add(parameterASD2ndCameraID);

                if (lvDetail.ASDGPSDateTime1 != null && DateTime.Compare(lvDetail.ASDGPSDateTime1, Convert.ToDateTime("2010-04-01")) > 0)
                {
                    SqlParameter parameterASDGPSDateTime1 = new SqlParameter("@ASDGPSDateTime1", SqlDbType.SmallDateTime);
                    parameterASDGPSDateTime1.Value = lvDetail.ASDGPSDateTime1;
                    cmd.Parameters.Add(parameterASDGPSDateTime1);
                }

                if (lvDetail.ASDGPSDateTime2 != null && DateTime.Compare(lvDetail.ASDGPSDateTime2, Convert.ToDateTime("2010-04-01")) > 0)
                {
                    SqlParameter parameterASDGPSDateTime2 = new SqlParameter("@ASDGPSDateTime2", SqlDbType.SmallDateTime);
                    parameterASDGPSDateTime2.Value = lvDetail.ASDGPSDateTime2;
                    cmd.Parameters.Add(parameterASDGPSDateTime2);
                }

                SqlParameter parameterASDTimeDifference = new SqlParameter("@ASDTimeDifference", SqlDbType.Int, 4);
                parameterASDTimeDifference.Value = lvDetail.ASDTimeDifference;
                cmd.Parameters.Add(parameterASDTimeDifference);

                SqlParameter parameterASDSectionStartLane = new SqlParameter("@ASDSectionStartLane", SqlDbType.Int, 4);
                parameterASDSectionStartLane.Value = lvDetail.ASDSectionStartLane;
                cmd.Parameters.Add(parameterASDSectionStartLane);

                SqlParameter parameterASDSectionEndLane = new SqlParameter("@ASDSectionEndLane", SqlDbType.Int, 4);
                parameterASDSectionEndLane.Value = lvDetail.ASDSectionEndLane;
                cmd.Parameters.Add(parameterASDSectionEndLane);

                SqlParameter parameterASDSectionDistance = new SqlParameter("@ASDSectionDistance", SqlDbType.Int, 4);
                parameterASDSectionDistance.Value = lvDetail.ASDSectionDistance;
                cmd.Parameters.Add(parameterASDSectionDistance);

                SqlParameter parameterASD1stCamUnitID = new SqlParameter("@ASD1stCamUnitID", SqlDbType.VarChar, 5);
                parameterASD1stCamUnitID.Value = lvDetail.ASD1stCamUnitID;
                cmd.Parameters.Add(parameterASD1stCamUnitID);

                SqlParameter parameterASD2ndCamUnitID = new SqlParameter("@ASD2ndCamUnitID", SqlDbType.VarChar, 5);
                parameterASD2ndCamUnitID.Value = lvDetail.ASD2ndCamUnitID;
                cmd.Parameters.Add(parameterASD2ndCamUnitID);

                SqlParameter parameterASDCameraSerialNo1 = new SqlParameter("@ASDCameraSerialNo1", SqlDbType.VarChar, 18);
                parameterASDCameraSerialNo1.Value = lvDetail.ASDCameraSerialNo1;
                cmd.Parameters.Add(parameterASDCameraSerialNo1);

                SqlParameter parameterASDCameraSerialNo2 = new SqlParameter("@ASDCameraSerialNo2", SqlDbType.VarChar, 18);
                parameterASDCameraSerialNo2.Value = lvDetail.ASDCameraSerialNo2;
                cmd.Parameters.Add(parameterASDCameraSerialNo2);

                //Jerry 2013-04-24 add
                if (lvDetail.ParentFrameNo != null)
                {
                    SqlParameter parameterParentFrameNo = new SqlParameter("@ParentFrameNo", SqlDbType.VarChar, 4);
                    parameterParentFrameNo.Value = lvDetail.ParentFrameNo;
                    cmd.Parameters.Add(parameterParentFrameNo);
                }

                SqlParameter parameterTLVIntNo = new SqlParameter("@TLVIntNo", SqlDbType.Int, 4);
                parameterTLVIntNo.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(parameterTLVIntNo);

                try
                {
                    //open connection at the beginning
                    //con.Open(); //Modified by Henry --2012-9-4 change by Paole 2013-04-24
                    cmd.ExecuteNonQuery();

                    // Calculate the CustomerID using Output Param from SPROC
                    //int tlvIntNo = Convert.ToInt32(parameterTLVIntNo.Value);
                }
                catch (Exception e)
                {
                    errorMessage = e.Message;
                    cmd.Dispose();
                    con.Dispose();
                    return 0;
                }
                 //change by Paole 2013-04-24
                //finally
                //{
                //    con.Close();
                //    con.Dispose();
                //}
            }

            cmd.Dispose();
            con.Dispose();

            return violations.Count;
        }

        public int AddTempLoadViolationsErrors(int autIntNo, string filmNo, string tErDescr, string tErColumn,
            string tErValue, string tErCorrectVal, string tErRefNo)
        //this arraylist consists of many instances of 
        //the LoadViolations struct
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(_connectionString);
            SqlCommand myCommand = new SqlCommand("TempLoadViolErrorsAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFilmNo = new SqlParameter("@FilmNo", SqlDbType.VarChar, 10);
            parameterFilmNo.Value = filmNo;
            myCommand.Parameters.Add(parameterFilmNo);

            SqlParameter parameterTErDescr = new SqlParameter("@TErDescr", SqlDbType.VarChar, 255);
            parameterTErDescr.Value = tErDescr;
            myCommand.Parameters.Add(parameterTErDescr);

            SqlParameter parameterTErColumn = new SqlParameter("@TErColumn", SqlDbType.VarChar, 30);
            parameterTErColumn.Value = tErColumn;
            myCommand.Parameters.Add(parameterTErColumn);

            SqlParameter parameterTErValue = new SqlParameter("@TErValue", SqlDbType.VarChar, 60);
            parameterTErValue.Value = tErValue;
            myCommand.Parameters.Add(parameterTErValue);

            SqlParameter parameterTErCorrectVal = new SqlParameter("@TErCorrectVal", SqlDbType.VarChar, 60);
            parameterTErCorrectVal.Value = tErCorrectVal;
            myCommand.Parameters.Add(parameterTErCorrectVal);

            SqlParameter parameterTErRefNo = new SqlParameter("@TErRefNo", SqlDbType.VarChar, 50);
            parameterTErRefNo.Value = tErRefNo;
            myCommand.Parameters.Add(parameterTErRefNo);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterTLVIntNo = new SqlParameter("@TLVIntNo", SqlDbType.Int, 4);
            parameterTLVIntNo.Value = 0;
            parameterTLVIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterTLVIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int tlvIntNo = Convert.ToInt32(parameterTLVIntNo.Value);
                return tlvIntNo;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return 0;
            }
            finally
            {
                myConnection.Dispose();
            }
        }

        public void DeleteTempLoadViolations(int autIntNo, int tlvType)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(_connectionString);
            SqlCommand myCommand = new SqlCommand("TempLoadViolationsDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterTLVType = new SqlParameter("@TLVType", SqlDbType.Int, 4);
            parameterTLVType.Value = tlvType;
            myCommand.Parameters.Add(parameterTLVType);

            ////SqlParameter parameterTLVIntNo = new SqlParameter("@TLVIntNo", SqlDbType.Int, 4);
            ////parameterTLVIntNo.Direction = ParameterDirection.Output;
            ////myCommand.Parameters.Add(parameterTLVIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                //// Calculate the CustomerID using Output Param from SPROC
                //int tlvIntNo = Convert.ToInt32(parameterTLVIntNo.Value);

                //return tlvIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return;
            }
        }
        // 2013-07-29 add parameter lastUser by Henry
        public int ValidateTempLoadViolations(int autIntNo, int tlvType, string lastUser, ref string errMessage)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(_connectionString);
            SqlCommand myCommand = new SqlCommand("TempLoadViolationsValidate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterTLVType = new SqlParameter("@TLVType", SqlDbType.Int, 4);
            parameterTLVType.Value = tlvType;
            myCommand.Parameters.Add(parameterTLVType);

            //SqlParameter parameterFilmNo = new SqlParameter("@FilmNo", SqlDbType.VarChar, 20);
            //parameterFilmNo.Value = filmNo;
            //myCommand.Parameters.Add(parameterFilmNo);

            SqlParameter parameterInvalidRows = new SqlParameter("@InvalidCount", SqlDbType.Int, 4);
            parameterInvalidRows.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterInvalidRows);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int invalidRows = Convert.ToInt32(parameterInvalidRows.Value);

                return invalidRows;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                errMessage = e.Message;
                return -1;
            }
        }

        // 2013-07-19 comment by Henry for useless
        //public int AddNoticeChargeFromTempLoadViolations(int autIntNo, int tlvType, int csCode, int csCode2, int csCode3,
        //    string cType, string lastUser, ref int noOfRecords, ref string errorMsg)
        //{

        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(_connectionString);
        //    SqlCommand myCommand = new SqlCommand("NoticeChargeAddFromTempLoadViolations", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
        //    parameterAutIntNo.Value = autIntNo;
        //    myCommand.Parameters.Add(parameterAutIntNo);

        //    SqlParameter parameterTLVType = new SqlParameter("@TLVType", SqlDbType.Int, 4);
        //    parameterTLVType.Value = tlvType;
        //    myCommand.Parameters.Add(parameterTLVType);

        //    SqlParameter parameterCSCode = new SqlParameter("@CSCode", SqlDbType.Int, 4);
        //    parameterCSCode.Value = csCode;
        //    myCommand.Parameters.Add(parameterCSCode);

        //    SqlParameter parameterCSCode2 = new SqlParameter("@CSCode2", SqlDbType.Int, 4);
        //    parameterCSCode2.Value = csCode2;
        //    myCommand.Parameters.Add(parameterCSCode2);

        //    SqlParameter parameterCSCode3 = new SqlParameter("@CSCode3", SqlDbType.Int, 4);
        //    parameterCSCode3.Value = csCode3;
        //    myCommand.Parameters.Add(parameterCSCode3);

        //    SqlParameter parameterCType = new SqlParameter("@CType", SqlDbType.VarChar, 15);
        //    parameterCType.Value = cType;
        //    myCommand.Parameters.Add(parameterCType);

        //    SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
        //    parameterLastUser.Value = lastUser;
        //    myCommand.Parameters.Add(parameterLastUser);

        //    SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int);
        //    parameterSuccess.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterSuccess);

        //    SqlParameter parameterNoOfRecords = new SqlParameter("@NoOfRecords", SqlDbType.Int);
        //    parameterNoOfRecords.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterNoOfRecords);

        //    SqlParameter parameterErrorMsg = new SqlParameter("@ErrorMsg", SqlDbType.VarChar, 255);
        //    parameterErrorMsg.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterErrorMsg);

        //    try
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        int success = Convert.ToInt32(parameterSuccess.Value);

        //        errorMsg = parameterErrorMsg.Value.ToString();

        //        noOfRecords = Convert.ToInt32(parameterNoOfRecords.Value);
        //        return success;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return -1;
        //    }
        //}

        public int AddFrameFromTempLoadViolations(int autIntNo, int tlvType, string lastUser, ref string errorMsg, int statusLoaded,
            string aut3PCodingSystem, string lowSpeedRejReason, string cutOff, string skipRejReasons, string skipZeroes,
            int lowSpeedRejIntNo, int noOfDays, string strFullAdjudicated,
            string excluListActive, int excluSpeedAllowance, int offVehExcluRejIntNo, int offVehExcluFrameStatus,
            string asdInvalidSetup, int asdInvalidRejIntNo, int rejIntNoExpired)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(_connectionString);
            SqlCommand myCommand = new SqlCommand("FrameAddFromTempLoadViolations", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;
            myCommand.CommandTimeout = 0;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterStatusLoaded = new SqlParameter("@StatusLoaded", SqlDbType.Int, 4);
            parameterStatusLoaded.Value = statusLoaded;
            myCommand.Parameters.Add(parameterStatusLoaded);

            //SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            //parameterFilmIntNo.Value = filmIntNo;
            //myCommand.Parameters.Add(parameterFilmIntNo);

            SqlParameter parameterTLVType = new SqlParameter("@TLVType", SqlDbType.Int, 4);
            parameterTLVType.Value = tlvType;
            myCommand.Parameters.Add(parameterTLVType);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterSuccess = new SqlParameter("@Success", SqlDbType.Int);
            parameterSuccess.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterSuccess);

            myCommand.Parameters.Add("@Aut3PCodingSystem", SqlDbType.Char, 1).Value = aut3PCodingSystem;
            myCommand.Parameters.Add("@LowSpeedRejReason", SqlDbType.VarChar, 50).Value = lowSpeedRejReason;
            myCommand.Parameters.Add("@ASDSpeedRejReason", SqlDbType.VarChar, 50).Value = asdInvalidSetup;
            myCommand.Parameters.Add("@ViolationCutOff", SqlDbType.Char, 1).Value = cutOff;
            myCommand.Parameters.Add("@SkipRejReasons", SqlDbType.Char, 1).Value = skipRejReasons;
            myCommand.Parameters.Add("@SkipZeroes", SqlDbType.Char, 1).Value = skipZeroes;
            myCommand.Parameters.Add("@LowSpdRejIntNo", SqlDbType.Int, 1).Value = lowSpeedRejIntNo;
            myCommand.Parameters.Add("@ASDInvalidRejIntNo", SqlDbType.Int, 1).Value = asdInvalidRejIntNo;
            myCommand.Parameters.Add("@NoOfDays", SqlDbType.Int, 1).Value = noOfDays;
            myCommand.Parameters.Add("@FullAdjudicated", SqlDbType.Char, 1).Value = strFullAdjudicated;
            myCommand.Parameters.Add("@ExcluListActive", SqlDbType.Char, 1).Value = excluListActive;
            myCommand.Parameters.Add("@ExcluSpeedAllowance", SqlDbType.Int, 4).Value = excluSpeedAllowance;
            myCommand.Parameters.Add("@OffVehExcluRejIntNo", SqlDbType.Int, 4).Value = offVehExcluRejIntNo;
            myCommand.Parameters.Add("@OffVehExcluFrameStatus", SqlDbType.Int, 4).Value = offVehExcluFrameStatus;
            myCommand.Parameters.Add("@RejIntNoExpired", SqlDbType.Int, 4).Value = rejIntNoExpired;
            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                int success = Convert.ToInt32(parameterSuccess.Value);
                return success;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                errorMsg = e.Message;
                return -1;
            }
        }

        public DataSet GetLoadViolationErrorsDS(int autIntNo, int tlvType)
        {

            SqlDataAdapter sqlDAErrors = new SqlDataAdapter();
            DataSet dsErrors = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAErrors.SelectCommand = new SqlCommand();
            sqlDAErrors.SelectCommand.Connection = new SqlConnection(_connectionString);
            sqlDAErrors.SelectCommand.CommandText = "TempLoadViolErrorList";

            // Mark the Command as a SPROC
            sqlDAErrors.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            sqlDAErrors.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterTLVType = new SqlParameter("@TLVType", SqlDbType.Int, 4);
            parameterTLVType.Value = tlvType;
            sqlDAErrors.SelectCommand.Parameters.Add(parameterTLVType);

            // Execute the command and close the connection
            sqlDAErrors.Fill(dsErrors);
            sqlDAErrors.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsErrors;

        }

        public SqlDataReader GetLoadViolationErrorsByFrame(int autIntNo, string filmNo, string frameNo, string tlvType)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(_connectionString);
            SqlCommand myCommand = new SqlCommand("TempLoadViolErrorListByFrame", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterFilmNo = new SqlParameter("@FilmNo", SqlDbType.VarChar, 20);
            parameterFilmNo.Value = filmNo;
            myCommand.Parameters.Add(parameterFilmNo);

            SqlParameter parameterFrameNo = new SqlParameter("@FrameNo", SqlDbType.VarChar, 20);
            parameterFrameNo.Value = frameNo;
            myCommand.Parameters.Add(parameterFrameNo);

            SqlParameter parameterTLVType = new SqlParameter("@TLVType", SqlDbType.Int, 4);
            parameterTLVType.Value = tlvType;
            myCommand.Parameters.Add(parameterTLVType);

            // Execute the command
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

            // Return the datareader result
            return result;

        }

    }
}
