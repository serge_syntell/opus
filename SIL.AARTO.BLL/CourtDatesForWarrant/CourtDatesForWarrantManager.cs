﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.CourtDatesForWarrant
{
    public class CourtDatesForWarrantManager
    {
        private string connectionStr;
        public CourtDatesForWarrantManager(string connectionStr)
        {
            this.connectionStr = connectionStr;
        }

        public int CreateCourtDates(int crtRIntNo, int autIntNo, DateTime cDate, Int16 cdNoOfCases, string lastUser, int ogIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionStr);
            SqlCommand myCommand = new SqlCommand("CourtDatesForWarrantAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCrtRIntNo = new SqlParameter("@CrtRIntNo", SqlDbType.Int, 4);
            parameterCrtRIntNo.Value = crtRIntNo;
            myCommand.Parameters.Add(parameterCrtRIntNo);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = autIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterCDate = new SqlParameter("@CDate", SqlDbType.SmallDateTime);
            parameterCDate.Value = cDate;
            myCommand.Parameters.Add(parameterCDate);

            SqlParameter parameterCDNoOfCases = new SqlParameter("@CDNoOfCases", SqlDbType.SmallInt);
            parameterCDNoOfCases.Value = cdNoOfCases;
            myCommand.Parameters.Add(parameterCDNoOfCases);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int, 4);
            parameterOGIntNo.Value = ogIntNo;
            myCommand.Parameters.Add(parameterOGIntNo);


            SqlParameter parameterCDIntNo = new SqlParameter("@CDIntNo", SqlDbType.Int, 4);
            parameterCDIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterCDIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();
                int cdIntNo = Convert.ToInt32(parameterCDIntNo.Value);
                return cdIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }


        public int UpdateCourtDates(int cdIntNo, DateTime cDate, Int16 cdNoOfCases, string lastUser, int ogIntNo)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(this.connectionStr);
            SqlCommand myCommand = new SqlCommand("CourtDatesForWarrantUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCDate = new SqlParameter("@CDate", SqlDbType.SmallDateTime);
            parameterCDate.Value = cDate;
            myCommand.Parameters.Add(parameterCDate);

            SqlParameter parameterCDNoOfCases = new SqlParameter("@CDNoOfCases", SqlDbType.SmallInt);
            parameterCDNoOfCases.Value = cdNoOfCases;
            myCommand.Parameters.Add(parameterCDNoOfCases);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterOGIntNo = new SqlParameter("@OGIntNo", SqlDbType.Int);
            parameterOGIntNo.Value = ogIntNo;
            myCommand.Parameters.Add(parameterOGIntNo);


            SqlParameter parameterCDIntNo = new SqlParameter("@CDIntNo", SqlDbType.Int);
            parameterCDIntNo.Direction = ParameterDirection.InputOutput;
            parameterCDIntNo.Value = cdIntNo;
            myCommand.Parameters.Add(parameterCDIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                cdIntNo = (int)myCommand.Parameters["@CDIntNo"].Value;
                return cdIntNo;
            }
            catch (Exception e)
            {

                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }
    }
}
