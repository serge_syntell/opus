﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;


namespace SIL.AARTO.BLL
{
    class MvcResourceDisplayNameExtension : DisplayNameAttribute
    {
        private string _defaultName = "";

        public Type ResourceType
        {
            get;
            set;
        }

        public string ResourceName
        {
            get;
            set;
        }

        public MvcResourceDisplayNameExtension(string defaultName)
        {
            _defaultName = defaultName;
        }

        public override string DisplayName
        {
            get
            {
                PropertyInfo p = ResourceType.GetProperty(ResourceName);
                if (p != null)
                { return p.GetValue(null, null).ToString(); }
                else
                { return _defaultName; }
            }
        }
    }
}
