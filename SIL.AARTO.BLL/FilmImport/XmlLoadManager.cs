﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;
using SIL.IMX.DAL.Entities;
using System.ComponentModel;

namespace SIL.AARTO.BLL.DataImport
{
    public class XmlLoadManager
    {
        public static XmlFilmEntity GetFilmEntityFromXml(XmlDocument document)
        {
            XmlFilmEntity filmEntity = new XmlFilmEntity();
            XmlNodeList dataList = document.DocumentElement.SelectNodes(@"//Film");

            foreach (XmlNode node in dataList)
            {

                FilmEntity film = new FilmEntity();
                GetDataInfoFromXml(node, film);
                filmEntity.Film = film;

                foreach (XmlNode frameNode in node.SelectNodes(@"//Film"))
                {
                    FrameEntity frame = new FrameEntity();
                    GetDataInfoFromXml(frameNode, frame);

                    if (frame != null)
                    {
                       
                        foreach (XmlNode imageNode in frameNode.SelectNodes(@"//ScanImage"))
                        {
                            ScanImageEntity image = new ScanImageEntity(frame);
                            GetDataInfoFromXml(imageNode, image);
                            frame.ScanImages.Add(image);

                        }

                        if (filmEntity.Frames == null)
                            filmEntity.Frames = new List<FrameEntity>();

                        filmEntity.Frames.Add(frame);
                    }
                }

            }
            return filmEntity;
        }

        private static void GetDataInfoFromXml(XmlNode node, object obj)
        {
            foreach (PropertyInfo framePro in obj.GetType().GetProperties())
            {
                foreach (XmlAttribute attr in node.Attributes)
                {
                    if (framePro.Name.Trim().ToUpper().Equals(attr.Name.Trim().ToUpper()))
                    {
                        framePro.SetValue(obj, Convert.ChangeType(attr.Value, framePro.PropertyType), null);
                    }
                }
            }
        }


        public static object ChangeType(object value, Type conversionType)
        {
            if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value != null)
                {
                    NullableConverter nullableConverter = new NullableConverter(conversionType);
                    conversionType = nullableConverter.UnderlyingType;
                }

                return null;
            }
            return Convert.ChangeType(value, conversionType);
        }



    }
}
