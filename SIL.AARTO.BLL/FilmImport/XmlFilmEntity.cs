﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.IMX.DAL.Entities;
 

namespace SIL.AARTO.BLL.DataImport
{
    
    public class XmlFilmEntity
    {
        public FilmEntity Film { get; set; }

        public List<FrameEntity> Frames { get; set; }

    }

}
