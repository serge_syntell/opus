﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.IMX.DAL.Entities;

namespace SIL.AARTO.BLL.DataImport
{
    public class FrameEntity
    {
        // Fields for export
        private int frameIntNo;
        private string frameNo;
        private char sequence;
        private int referenceNo;
        private string cameraID = string.Empty;
        private string cameraSerial = string.Empty;
        private string officerNo = string.Empty;
        private string officerGroup = string.Empty;
        private string officerSurname = string.Empty;
        private string officerInit = string.Empty;
        private string cameraLocationCode = string.Empty;
        private string locationDescription = string.Empty;
        private char travelDirection;
        private string courtNo = string.Empty;
        private string courtName = string.Empty;
        private int roadTypeCode;
        private string roadTypeDescription = string.Empty;
        private string elapsedTime;
        private int speed;
        private string offenceCode = string.Empty;
        private string offenceDescription = string.Empty;
        private decimal fineAmount;
        private char fineAllocation;
        private char offenceLetter;
        private int offenderType;
        private int firstSpeed;
        private int secondSpeed;
        private DateTime offenceDate;
        private string rejectionReason = string.Empty;
        private string registrationNo = string.Empty;
        private string vehicleMakeCode = string.Empty;
        private string vehicleMakeDescription = string.Empty;
        private string vehicleTypeCode = string.Empty;
        private string vehicleTypeDescription = string.Empty;
        private char violation;
        private char confirmViolation;
        private char manualView;
        private char multipleFrames;
        private char truvellaFlag;
        private List<ScanImageEntity> scanImages;
        private Film parent = null;
        // Fields for Feedback 
        List<string> errors = null;
        private int version;
        private string contractor = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:FrameData"/> class.
        /// </summary>
        public FrameEntity()
        {
            this.scanImages = new List<ScanImageEntity>();
            this.errors = new List<string>();
        }

        /// <summary>
        /// Gets or sets the contractor.
        /// </summary>
        /// <value>The contractor.</value>
        public string Contractor
        {
            get { return contractor; }
            set { contractor = value; }
        }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>The version.</value>
        public int Version
        {
            get { return version; }
            set { version = value; }
        }

        /// <summary>
        /// Gets the errors.
        /// </summary>
        /// <value>The errors.</value>
        public List<string> Errors
        {
            get { return errors; }
        }

        /// <summary>
        /// Gets or sets the truvella flag.
        /// </summary>
        /// <value>The truvella flag.</value>
        public char TruvellaFlag
        {
            get { return truvellaFlag; }
            set { truvellaFlag = value; }
        }

        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        /// <value>The parent.</value>
        internal Film Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        /// <summary>
        /// Gets the list of scanned images for this frame.
        /// </summary>
        /// <value>The images.</value>
        internal List<ScanImageEntity> ScanImages
        {
            get { return scanImages; }
        }

        /// <summary>
        /// Gets or sets the frame int no.
        /// </summary>
        /// <value>The frame int no.</value>
        public int FrameIntNo
        {
            get { return frameIntNo; }
            set { frameIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the type of the offender.
        /// </summary>
        /// <value>The type of the offender.</value>
        public int OffenderType
        {
            get { return offenderType; }
            set { offenderType = value; }
        }

        /// <summary>
        /// Gets or sets whether there are multiple frames.
        /// </summary>
        /// <value>The multiple frames.</value>
        public char MultipleFrames
        {
            get { return multipleFrames; }
            set { multipleFrames = value; }
        }

        /// <summary>
        /// Gets or sets the manual view flag.
        /// </summary>
        /// <value>The manual view.</value>
        public char ManualView
        {
            get { return manualView; }
            set { manualView = value; }
        }

        /// <summary>
        /// Gets or sets the confirm violation flag.
        /// </summary>
        /// <value>The confirm violation.</value>
        public char ConfirmViolation
        {
            get { return confirmViolation; }
            set { confirmViolation = value; }
        }

        /// <summary>
        /// Gets or sets the violation flag.
        /// </summary>
        /// <value>The violation.</value>
        public char Violation
        {
            get { return violation; }
            set { violation = value; }
        }

        /// <summary>
        /// Gets or sets the vehicle type description.
        /// </summary>
        /// <value>The vehicle type description.</value>
        public string VehicleTypeDescription
        {
            get { return vehicleTypeDescription; }
            set { vehicleTypeDescription = value; }
        }

        /// <summary>
        /// Gets or sets the vehicle type code.
        /// </summary>
        /// <value>The vehicle type code.</value>
        public string VehicleTypeCode
        {
            get { return vehicleTypeCode; }
            set { vehicleTypeCode = value; }
        }

        /// <summary>
        /// Gets or sets the vehicle make description.
        /// </summary>
        /// <value>The vehicle make description.</value>
        public string VehicleMakeDescription
        {
            get { return vehicleMakeDescription; }
            set { vehicleMakeDescription = value; }
        }

        /// <summary>
        /// Gets or sets the vehicle make code.
        /// </summary>
        /// <value>The vehicle make code.</value>
        public string VehicleMakeCode
        {
            get { return vehicleMakeCode; }
            set { vehicleMakeCode = value; }
        }

        /// <summary>
        /// Gets or sets the vehicle registration number.
        /// </summary>
        /// <value>The registration number.</value>
        public string RegistrationNumber
        {
            get { return registrationNo; }
            set { registrationNo = value; }
        }

        /// <summary>
        /// Gets or sets the rejection reason.
        /// </summary>
        /// <value>The rejection reason.</value>
        public string RejectionReason
        {
            get { return rejectionReason; }
            set { rejectionReason = value; }
        }

        /// <summary>
        /// Gets or sets the offence date.
        /// </summary>
        /// <value>The offence date.</value>
        public DateTime OffenceDate
        {
            get { return offenceDate; }
            set { offenceDate = value; }
        }

        /// <summary>
        /// Gets or sets the second speed.
        /// </summary>
        /// <value>The second speed.</value>
        public int SecondSpeed
        {
            get { return secondSpeed; }
            set { secondSpeed = value; }
        }

        /// <summary>
        /// Gets or sets the first speed.
        /// </summary>
        /// <value>The first speed.</value>
        public int FirstSpeed
        {
            get { return firstSpeed; }
            set { firstSpeed = value; }
        }

        /// <summary>
        /// Gets or sets the offence letter code.
        /// </summary>
        /// <value>The offence letter.</value>
        public char OffenceLetter
        {
            get { return offenceLetter; }
            set { offenceLetter = value; }
        }

        /// <summary>
        /// Gets or sets the fine allocation cod.
        /// </summary>
        /// <value>The fine allocation.</value>
        public char FineAllocation
        {
            get { return fineAllocation; }
            set { fineAllocation = value; }
        }

        /// <summary>
        /// Gets or sets the fine amount.
        /// </summary>
        /// <value>The fine amount.</value>
        public decimal FineAmount
        {
            get { return fineAmount; }
            set { fineAmount = value; }
        }

        /// <summary>
        /// Gets or sets the offence description.
        /// </summary>
        /// <value>The offence description.</value>
        public string OffenceDescription
        {
            get { return offenceDescription; }
            set { offenceDescription = value; }
        }

        /// <summary>
        /// Gets or sets the offence code.
        /// </summary>
        /// <value>The offence code.</value>
        public string OffenceCode
        {
            get { return offenceCode; }
            set { offenceCode = value; }
        }

        /// <summary>
        /// Gets or sets the speed.
        /// </summary>
        /// <value>The speed.</value>
        public int Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        /// <summary>
        /// Gets or sets the elapsed time.
        /// </summary>
        /// <value>The elapsed time.</value>
        public string ElapsedTime
        {
            get { return elapsedTime; }
            set { elapsedTime = value; }
        }

        /// <summary>
        /// Gets or sets the road type description.
        /// </summary>
        /// <value>The road type description.</value>
        public string RoadTypeDescription
        {
            get { return roadTypeDescription; }
            set { roadTypeDescription = value; }
        }

        /// <summary>
        /// Gets or sets the road type code.
        /// </summary>
        /// <value>The road type code.</value>
        public int RoadTypeCode
        {
            get { return roadTypeCode; }
            set { roadTypeCode = value; }
        }

        /// <summary>
        /// Gets or sets the name of the court.
        /// </summary>
        /// <value>The name of the court.</value>
        public string CourtName
        {
            get { return courtName; }
            set { courtName = value; }
        }

        /// <summary>
        /// Gets or sets the court no.
        /// </summary>
        /// <value>The court no.</value>
        public string CourtNo
        {
            get { return courtNo; }
            set { courtNo = value; }
        }

        /// <summary>
        /// Gets or sets the travel direction.
        /// </summary>
        /// <value>The travel direction.</value>
        public char TravelDirection
        {
            get { return travelDirection; }
            set { travelDirection = value; }
        }

        /// <summary>
        /// Gets or sets the location description.
        /// </summary>
        /// <value>The location description.</value>
        public string LocationDescription
        {
            get { return locationDescription; }
            set { locationDescription = value; }
        }

        /// <summary>
        /// Gets or sets the camera location code.
        /// </summary>
        /// <value>The camera location code.</value>
        public string CameraLocationCode
        {
            get { return cameraLocationCode; }
            set { cameraLocationCode = value; }
        }

        /// <summary>
        /// Gets or sets the officer init.
        /// </summary>
        /// <value>The officer init.</value>
        public string OfficerInit
        {
            get { return officerInit; }
            set { officerInit = value; }
        }

        /// <summary>
        /// Gets or sets the officer surname.
        /// </summary>
        /// <value>The officer surname.</value>
        public string OfficerSurname
        {
            get { return officerSurname; }
            set { officerSurname = value; }
        }

        /// <summary>
        /// Gets or sets the officer group.
        /// </summary>
        /// <value>The officer group.</value>
        public string OfficerGroup
        {
            get { return officerGroup; }
            set { officerGroup = value; }
        }

        /// <summary>
        /// Gets or sets the officer no.
        /// </summary>
        /// <value>The officer no.</value>
        public string OfficerNo
        {
            get { return officerNo; }
            set { officerNo = value; }
        }

        /// <summary>
        /// Gets or sets the camera serial number.
        /// </summary>
        /// <value>The camera serial number.</value>
        public string CameraID
        {
            get { return cameraID; }
            set { cameraID = value; }
        }

        /// <summary>
        /// Gets or sets the camera serial number.
        /// </summary>
        /// <value>The camera serial.</value>
        public string CameraSerial
        {
            get { return this.cameraSerial; }
            set { this.cameraSerial = value; }
        }

        /// <summary>
        /// Gets or sets the reference no.
        /// </summary>
        /// <value>The reference no.</value>
        public int ReferenceNo
        {
            get { return referenceNo; }
            set { referenceNo = value; }
        }

        /// <summary>
        /// Gets or sets the sequence.
        /// </summary>
        /// <value>The sequence.</value>
        public char Sequence
        {
            get { return sequence; }
            set
            {
                this.sequence = value;
            }
        }

        /// <summary>
        /// Gets or sets the frame no.
        /// </summary>
        /// <value>The frame no.</value>
        public string FrameNo
        {
            get { return frameNo; }
            set { frameNo = value; }
        }

        /// <summary>
        /// For Average speed over distance FT 100430
        /// </summary>
        #region Average speed over distance
        private DateTime _ASDGPSDateTime1;
        private DateTime _ASDGPSDateTime2;
        private int _ASDTimeDifference;
        private int _ASD2ndCameraIntNo;

        private int _ASDSectionStartLane;
        private int _ASDSectionEndLane;
        private int _ASDSectionDistance;

        private int _ASD1stCamUnitID;
        private int _ASD2ndCamUnitID;
        private int _LCSIntNo;
        private string _ASDCameraSerialNo1;
        private string _ASDCameraSerialNo2;

        public int ASDSectionDistance
        {
            get { return _ASDSectionDistance; }
            set { _ASDSectionDistance = value; }
        }

        public int ASD2ndCameraIntNo
        {
            get { return _ASD2ndCameraIntNo; }
            set { _ASD2ndCameraIntNo = value; }
        }

        public DateTime ASDGPSDateTime1
        {
            get { return _ASDGPSDateTime1; }
            set { _ASDGPSDateTime1 = value; }
        }

        public DateTime ASDGPSDateTime2
        {
            get { return _ASDGPSDateTime2; }
            set { _ASDGPSDateTime2 = value; }
        }

        public int ASDTimeDifference
        {
            get { return _ASDTimeDifference; }
            set { _ASDTimeDifference = value; }
        }

        public int ASDSectionStartLane
        {
            get { return _ASDSectionStartLane; }
            set { _ASDSectionStartLane = value; }
        }

        public int ASDSectionEndLane
        {
            get { return _ASDSectionEndLane; }
            set { _ASDSectionEndLane = value; }
        }

        public int ASD1stCamUnitID
        {
            get { return _ASD1stCamUnitID; }
            set { _ASD1stCamUnitID = value; }
        }

        public int ASD2ndCamUnitID
        {
            get { return _ASD2ndCamUnitID; }
            set { _ASD2ndCamUnitID = value; }
        }

        public int LCSIntNo
        {
            get { return _LCSIntNo; }
            set { _LCSIntNo = value; }
        }

        public string ASDCameraSerialNo1
        {
            get { return _ASDCameraSerialNo1; }
            set { _ASDCameraSerialNo1 = value; }
        }

        public string ASDCameraSerialNo2
        {
            get { return _ASDCameraSerialNo2; }
            set { _ASDCameraSerialNo2 = value; }
        }
        #endregion




    }
}
