﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.IMX.DAL.Entities;

namespace SIL.AARTO.BLL.DataImport
{
    internal class ScanImageEntity
    {
        // Fields
        private string name = string.Empty;
        private int x;
        private int y;
        private char type;
        private FrameEntity parent;

        // Static
        //private static AtalaImage img = null;
        //private static Jp2Encoder jp2CodecSmall = new Jp2Encoder(2.5);
        //private static Jp2Encoder jp2CodecLarge = new Jp2Encoder(5.0);
        //private static JpegEncoder jpgCodec = new JpegEncoder();

        private static ScanImage img = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="Image"/> class.
        /// </summary>
        public ScanImageEntity(FrameEntity parent)
        {
            this.parent = parent;
        }

        /// <summary>
        /// Gets the parent frame.
        /// </summary>
        /// <value>The parent.</value>
        public FrameEntity Parent
        {
            get { return this.parent; }
        }

        /// <summary>
        /// Gets or sets the type of the image.
        /// </summary>
        /// <value>The type of the image.</value>
        public char ImageType
        {
            get { return this.type; }
            set
            {
                if (char.ToUpper(value).Equals('X'))
                    this.type = 'A';
                else
                    this.type = char.ToUpper(value);
            }
        }

        /// <summary>
        /// Gets or sets the Y value.
        /// </summary>
        /// <value>The Y.</value>
        public int Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        /// <summary>
        /// Gets or sets the X value.
        /// </summary>
        /// <value>The X.</value>
        public int X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }



      
    }
}
