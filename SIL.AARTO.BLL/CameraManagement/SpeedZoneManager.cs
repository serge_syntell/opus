﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;
using System.Data;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class SpeedZoneManager
    {
        public static SelectList GetSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            SpeedZoneDB speedZoneList = new SpeedZoneDB(Config.ConnectionString);
            foreach (DataRow row in speedZoneList.GetSpeedZoneListDS().Tables[0].Rows)
            {
                list.Add(new SelectListItem() { Value = (row["SZIntNo"]).ToString(), Text = (row["SZSpeed"]).ToString() });
            }
            return new SelectList(list, "Value", "Text");
        }
    }
}
