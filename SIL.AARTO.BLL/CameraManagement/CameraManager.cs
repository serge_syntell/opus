﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIL.AARTO.BLL.Culture;
using System.Web.Mvc;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class CameraManager
    {
        public static SelectList GetSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            CameraDB camList = new CameraDB(Config.ConnectionString);
            foreach (DataRow row in camList.GetCameraListDS().Tables[0].Rows)
            {
                list.Add(new SelectListItem { Value = (row["CamIntNo"]).ToString(), Text = (string)row["CamSerialNo"] });
            }
            return new SelectList(list, "Value", "Text");
        }
    }
}
