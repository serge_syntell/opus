﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.CameraManagement.Model
{
    public class FilmEntity
    {
        public int FilmIntNo { get; set; }
        public string FilmNo { get; set; }
        public int ToVerify { get; set; }
        public DateTime FirstOffence { get; set; }
        public DateTime LastOffence { get; set; }
        public int SinceFirstOffence { get; set; }
        public string FilmLockUser { get; set; }
        public int NoFrames { get; set; }
        public long RowVersion { get; set; }
        public DateTime ValidateDataDateTime { get; set; }
    }
}
