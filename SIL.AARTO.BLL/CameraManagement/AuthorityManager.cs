﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class AuthorityManager
    {
        private static AuthorityDB authorityDB = new AuthorityDB(Config.ConnectionString);
        public static string GetAutCode(int authIntNo)
        {
            return authorityDB.GetAuthorityDetails(authIntNo).AutCode;
        }
    }
}
