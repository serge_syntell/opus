﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class ImageManager
    {
        private static ImageProcesses img = new ImageProcesses(Config.ConnectionString);
        public static int UpdateImageSetting(Int64 scImIntNo, decimal contrast, decimal brightness, string userName)
        {
            return img.UpdateImageSetting(scImIntNo, contrast, brightness, userName, 0);
        }
    }
}
