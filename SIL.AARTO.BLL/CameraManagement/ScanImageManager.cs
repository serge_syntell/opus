﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;
using System.Data;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class ScanImageManager
    {
        private static ScanImageDB scanImage = new ScanImageDB(Config.ConnectionString);
        public static List<ScanImageDetails> GetScanImageList(int filmIntNo, int frameIntNo, string scImType)
        {
            List<ScanImageDetails> scanImageList = new List<ScanImageDetails>();
            foreach (DataRow dr in scanImage.GetScanImageListDS(filmIntNo, frameIntNo, scImType).Tables[0].Rows)
            {
                scanImageList.Add(new ScanImageDetails() { 
                    ScImIntNo = (int)dr["ScImIntNo"],
                    FrameIntNo = Convert.ToInt32(dr["FrameIntNo"]),
                    ScImType = dr["ScImType"].ToString(),
                    JPegName = dr["JPegName"].ToString(),
                    ScImPrintVal = Convert.ToInt32(dr["ScImPrintVal"]),
                    XValue = Convert.ToInt32(dr["XValue"]),
                    YValue  = Convert.ToInt32(dr["YValue"]) ,
                });
            }
            return scanImageList;
        }

        public static ScanImageDetails GetScanImageDetails(int scanImageIntNo)
        {
            return scanImage.GetScanImageDetails(scanImageIntNo);
        }
    }
}
