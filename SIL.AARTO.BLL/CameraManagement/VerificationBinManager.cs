﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Data;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class VerificationBinManager
    {
        private static readonly VerificationBinService service = new VerificationBinService();

        public static TList<VerificationBin> GetAll()
        {
            return service.GetAll();
        }

        public static TList<VerificationBin> GetPage(int start, int pageLength, out int totalCount)
        {
            return service.GetPaged(start, pageLength, out totalCount);
        }

        public static TList<VerificationBin> Search(string searchStr, int start, int pageLength, out int count)
        {
            VerificationBinQuery query = new VerificationBinQuery();
            query.AppendContains("or", VerificationBinColumn.VeBiComment, searchStr);
            string sql = string.Format("(VeBiComment Like '%{0}%')", searchStr);
            return service.GetPaged(sql, "VeBiComment", start, pageLength, out count);
        }

        public static VerificationBin GetByInReIntNo(int veBiIntNo)
        {
            return service.GetByVeBiIntNo(veBiIntNo);
        }

        public static VerificationBin GetByRegNo(string regNo)
        {
            TList<VerificationBin> verBins = service.GetByVeBiRegNo(regNo);
            if (verBins.Count > 0)
            {
                return verBins[0];
            }
            return null;
        }

        public static int Add(VerificationBin verBin)
        {
            VerificationBinQuery query = new VerificationBinQuery();
            query.Append(VerificationBinColumn.VeBiComment, verBin.VeBiComment);
            if (service.Find(query).Count > 0)
            {
                return -1;
            }
            service.Save(verBin);
            return 1;
        }

        public static int Update(VerificationBin verBin)
        {
            VerificationBin veBiOld = service.GetByVeBiIntNo(verBin.VeBiIntNo);
            VerificationBinQuery query = new VerificationBinQuery();
            query.Append(VerificationBinColumn.VeBiComment, verBin.VeBiComment);

            if (veBiOld.VeBiComment != verBin.VeBiComment && service.Find(query).Count > 0)
            {
                return -1;
            }
            verBin.RowVersion = veBiOld.RowVersion;
            verBin.VeBiDateInserted = veBiOld.VeBiDateInserted;
            service.Update(verBin);
            return 1;
        }

        public static int Delete(int veBiIntNo)
        {
            VerificationBin veBin = service.GetByVeBiIntNo(veBiIntNo);
            service.Delete(veBin);
            return 1;
        }
        public static bool CheckIsInCameraLocation(int filmIntNo,int locIntNo)
        {
            bool flag = true;
            FrameService frameService = new FrameService();
            ViewCameraLocationService service = new ViewCameraLocationService();
            List<Frame> frameList = frameService.GetByFilmIntNo(filmIntNo).ToList();
            List<ViewCameraLocation> list = new List<ViewCameraLocation>();
            ViewCameraLocationQuery query = new ViewCameraLocationQuery();
            query.Append(ViewCameraLocationColumn.LocIntNo, locIntNo.ToString());
            list = service.Find(query as IFilterParameterCollection).ToList();
            if (frameList.Count > 0)
            {
                foreach (var item in frameList)
                {
                    int count1 = list.Where(m => m.CamSerialNo == item.AsdCameraSerialNo1).Count();
                    int count2 = list.Where(m => m.CamSerialNo == item.AsdCameraSerialNo2).Count();
                    if (count1 == 0 || count2 == 0)
                    {
                        flag = false;
                        break;
                    }
                }
            }
            else
            {
                flag = false;
            }
            return flag;
        }
        public static bool CheckIsInLocationSite(int filmIntNo, int locIntNo)
        {
            bool flag = true;
            FrameService frameService = new FrameService();
            LocationSiteService siteService=new LocationSiteService();
            List<Frame> frameList = frameService.GetByFilmIntNo(filmIntNo).ToList();
            List<LocationSite> siteList = siteService.GetByLocIntNo(locIntNo).ToList();
            if (frameList.Count > 0)
            {
                foreach (var item in frameList)
                {
                    int count1 = siteList.Where(m => m.LsSiteCode == item.LsSiteCode1).Count();
                    int count2 = siteList.Where(m => m.LsSiteCode == item.LsSiteCode2).Count();
                    if (count1 == 0 || count2 == 0)
                    {
                        flag = false;
                        break;
                    }
                }
            }
            else
            {
                flag = false;
            }
            return flag;
        }
    }
}
