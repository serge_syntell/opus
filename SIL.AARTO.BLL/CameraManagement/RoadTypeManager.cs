﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Stalberg.TMS;
using System.Data;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class RoadTypeManager
    {
        public static RoadTypeDB roadTypeList = new RoadTypeDB(Config.ConnectionString);
        public static SelectList GetSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            Dictionary<int,string> allRoadTypes = RoadTypeLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            int rdTIntNo;
            foreach (DataRow row in roadTypeList.GetRoadTypeListDS().Tables[0].Rows)
            {
                rdTIntNo = (int)row["RdTIntNo"];
                if (allRoadTypes.Keys.Contains(rdTIntNo))
                {
                    list.Add(new SelectListItem() { Value = (rdTIntNo).ToString(), Text = allRoadTypes[rdTIntNo] });
                }
                else {
                    list.Add(new SelectListItem() { Value = (rdTIntNo).ToString(), Text = row["RdTypeDescr"].ToString() });
                }
            }
            return new SelectList(list, "Value", "Text");
        }
    }
}
