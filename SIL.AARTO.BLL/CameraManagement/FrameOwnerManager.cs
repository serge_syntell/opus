﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class FrameOwnerManager
    {
        private static FrameOwnerDB frameOwner = new FrameOwnerDB(Config.ConnectionString);

        public static FrameOwnerDetails GetFrameOwnerDetailsByFrame(int frameIntNo)
        {
            return frameOwner.GetFrameOwnerDetailsByFrame(frameIntNo);
        }

        public static int UpdateFrameOwner(int frOwnIntNo, int frameIntNo, string frOwnSurname, string frOwnInitials,
                        string frOwnIDNumber, string frOwnPoAdd1, string frOwnPoAdd2, string frOwnPoAdd3,
                        string frOwnPoAdd4, string frOwnPoAdd5, string frOwnPoCode, string frOwnStAdd1,
                        string frOwnStAdd2, string frOwnStAdd3, string frOwnStAdd4,
                        string lastUser)
        { 
            return frameOwner.UpdateFrameOwner(frOwnIntNo, frameIntNo,  frOwnSurname,  frOwnInitials,
                         frOwnIDNumber,  frOwnPoAdd1,  frOwnPoAdd2,  frOwnPoAdd3,
                         frOwnPoAdd4,  frOwnPoAdd5,  frOwnPoCode,  frOwnStAdd1,
                         frOwnStAdd2,  frOwnStAdd3,  frOwnStAdd4,
                         lastUser);
        }
    }
}
