﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.BLL.Culture;
using Stalberg.TMS;
using System.Data;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class CourtManager
    {
        private static readonly CourtService service = new CourtService();
        public static SelectList GetSelectList(int authIntNo)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            CourtDB courtList = new CourtDB(Config.ConnectionString);
            foreach (DataRow row in courtList.GetAuth_CourtListByAuthDS(authIntNo).Tables[0].Rows)
            {
                list.Add(new SelectListItem() { Value = (row["CrtIntNo"]).ToString(), Text = (string)row["CrtDetails"] });
            }
            return new SelectList(list, "Value", "Text");
        }

        public static Court GetByCrtIntNo(int crtIntNo)
        {
            return service.GetByCrtIntNo(crtIntNo);
        }
    }
}
