﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.CameraManagement.Model;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class FilmManager
    {
        private static FilmDB filmDB = new FilmDB(Config.ConnectionString);
        public static FilmDetails GetFilmDetailsByFilmIntNo(Int64 filmIntNo)
        {
            return filmDB.GetFilmDetails(filmIntNo);
        }

        //20130111 (isFishpond)Added by Nancy for distinguish frame of verification bin from those of fishpond
        public static List<FilmEntity> GetListFilms(int authIntNo, ValidationStatus status, bool isFishpond = false)
        {
            List<FilmEntity> filmList = new List<FilmEntity>();
            SqlDataReader reader = filmDB.GetAuthorityFilmsForVerification(authIntNo, status, isFishpond);
            while (reader.Read())
            {
                filmList.Add(new FilmEntity()
                {
                    FilmIntNo = (int)reader["FilmIntNo"],
                    FilmNo = (string)reader["FilmNo"],
                    FilmLockUser = reader["FilmLockUser"] as string ?? string.Empty,
                    FirstOffence = (DateTime)reader["FirstOffence"],
                    LastOffence = (DateTime)reader["LastOffence"],
                    SinceFirstOffence = (int)reader["SinceFirstOffence"],
                    NoFrames = (int)reader["NoFrames"],
                    ToVerify = (int)reader["ToVerify"]
                });
            }
            return filmList;
        }

        public static FilmEntity GetNextFilmToValidate(string userName, ValidationStatus status, int authIntNo)
        {
            using (SqlDataReader reader = filmDB.GetNextFilmForVerificationByUser(authIntNo, status, userName))
            {
                if (reader.Read())
                {
                    return new FilmEntity()
                    {
                        FilmIntNo = (int)reader["FilmIntNo"],
                        RowVersion = (long)reader["rowversion"],
                        ValidateDataDateTime =
                            reader["ValidateDataDateTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)reader["ValidateDataDateTime"],
                        FilmNo = (string)reader["FilmNo"]
                    };
                }
                return null;
            }
        }

        public static FilmEntity GetNextFilmToAdjudicate(string userName, ValidationStatus status, int authIntNo)
        {
            using (SqlDataReader reader = filmDB.GetNextFilmForAdjudicationByUser(authIntNo, status, userName))
            {
                if (reader.Read())
                {
                    return new FilmEntity()
                    {
                        FilmIntNo = (int)reader["FilmIntNo"],
                        RowVersion = (long)reader["rowversion"],
                        ValidateDataDateTime =
                            reader["ValidateDataDateTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)reader["ValidateDataDateTime"],
                        FilmNo = (string)reader["FilmNo"]
                    };
                }
                return null;
            }
        }

        public static FilmEntity GetNextFilmToInvestigate(string userName, ValidationStatus status, int authIntNo)
        {
            using (SqlDataReader reader = filmDB.GetNextFilmForInvestigationByUser(authIntNo, status, userName))
            {
                if (reader.Read())
                {
                    return new FilmEntity()
                    {
                        FilmIntNo = (int)reader["FilmIntNo"],
                        RowVersion = (long)reader["rowversion"],
                        ValidateDataDateTime =
                            reader["ValidateDataDateTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)reader["ValidateDataDateTime"],
                        FilmNo = (string)reader["FilmNo"]
                    };
                }
                return null;
            }
        }

        public static int RejectFilm(int filmIntNo, int regIntNo, int frameStatus, string userName, ref string errMessage)
        {
            return filmDB.RejectFilm(filmIntNo, regIntNo, frameStatus, userName, ref errMessage);
        }

        public static int LockUser(int filmIntNo, string userName)
        {
            return filmDB.LockUser(filmIntNo, userName);
        }

        public static int LockUser(int filmIntNo, string userName, long rowversion)
        {
            return filmDB.LockUser(filmIntNo, userName, rowversion);
        }

        public static int FinaliseFilmVerification(int filmIntNo, int cvPhase, string lastUser, ref string errMessage, string natisLast)
        {
            return filmDB.FinaliseFilmVerification(filmIntNo, cvPhase, lastUser, ref errMessage, natisLast);
        }

        public static void FinaliseFilmAdjudication(int filmIntNo, string natisLast, string lastUser, ref string errMessage)
        {
            filmDB.FinaliseFilmAdjudication(filmIntNo, natisLast, lastUser, ref errMessage);
        }

		public static SIL.AARTO.DAL.Entities.Film GetFilmByFilmNo(string filmNo)
        {
            SIL.AARTO.DAL.Entities.Film filmEntity = null;
            filmEntity = new SIL.AARTO.DAL.Services.FilmService().GetByFilmNo(filmNo);
            return filmEntity;
        }

    }
}
