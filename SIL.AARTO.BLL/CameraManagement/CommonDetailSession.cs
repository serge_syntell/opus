﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;
using System.Collections;
using SIL.AARTO.BLL.Utility.Cache;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class CommonDetailSession
    {
        public static List<SysParamDetails> GetSystemParameters()
        {
            return SysParamCache.GetAllSysParamDetails();
        }

        public static DomainRuleDetails GetDomainSettingsByUrl(string webUrl)
        {
            
            //System.IO.File.WriteAllText(System.AppDomain.CurrentDomain.BaseDirectory + "/WebUrl.txt", webUrl);
            Dictionary<string, DomainRuleDetails> domains = DomainRuleCache.GetAllDomainRulesDic();
            if (domains.ContainsKey(webUrl))
            {
                return domains[webUrl];
            }
            else
            {
                return domains[webUrl.TrimEnd('/')];
            }
        }
    }
}
