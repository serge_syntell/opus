﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;
using System.Data;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class VehicleTypeManager
    {
        private static VehicleTypeDB vehicleTypeDB = new VehicleTypeDB(Config.ConnectionString);
        public static SelectList GetSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            Dictionary<int, string> vechicleTypes = VehicleTypeLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            int vtIntNo;
            foreach (DataRow row in vehicleTypeDB.GetVehicleTypeListDS(0,string.Empty).Tables[0].Rows)
            {
                vtIntNo = (int)row["VTIntNo"];
                if (vechicleTypes.Keys.Contains(vtIntNo))
                {
                    list.Add(new SelectListItem() { Value = vtIntNo.ToString(), Text = vechicleTypes[vtIntNo] });
                }
                else
                {
                    list.Add(new SelectListItem() { Value = vtIntNo.ToString(), Text = row["VTDescr"].ToString() });
                }
            }
            return new SelectList(list, "Value", "Text");
        }

        public static VehicleTypeDetails GetVehicleTypeDetailsByCode(string vtCode, string system)
        {
            return vehicleTypeDB.GetVehicleTypeDetailsByCode(vtCode, system);
        }

        public static VehicleTypeDetails GetByVtIntNo(int vtIntNo)
        {
            return vehicleTypeDB.GetVehicleTypeDetails(vtIntNo);
        }
    }
}
