﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class AuthorityRulesManager
    {
        public static bool GetIsOrNotAdjAt100(string userName, int authIntNo)
        {
            return GetAuthorityRulesBool(userName, "0600", authIntNo);
        }

        public static bool GetIsOrNotShowOfficer(string userName, int authIntNo)
        {
            return GetAuthorityRulesBool(userName, "0560", authIntNo);
        }

        public static bool GetIsOrNotShowDiscrepanciesOnly(string userName, int authIntNo)
        {
            return GetAuthorityRulesBool(userName, "0550", authIntNo);
        }

        public static bool GetIsOrNotDisplayCrossHair(string userName, int authIntNo)
        {
            return GetAuthorityRulesBool(userName, "0650", authIntNo);
        }

        public static bool GetIsOrNotShowCrossHairs(string userName, int authIntNo)
        {
            return GetAuthorityRulesBool(userName, "3100", authIntNo);
        }

        public static bool GetIsOrNotNaTISLast(string userName, int authIntNo)
        {
            return GetAuthorityRulesBool(userName, "0400", authIntNo);
        }

        public static bool GetIsOrNotShowImageSettingButton(string userName, int authIntNo)
        {
            return GetAuthorityRulesBool(userName, "2560", authIntNo);
        }

        public static bool GetIsOrNotShowInvestigation(string userName, int authIntNo)
        {
            return GetAuthorityRulesBool(userName, "2310", authIntNo);
        }

        public static bool GetIsOrNotShowVerificationBin(string userName, int authIntNo)
        {
            return GetAuthorityRulesBool(userName, "2310", authIntNo);
        }

        public static bool GetIsOrNotEnableFishpond(string userName, int authIntNo)
        {//20130108 added by Nancy for fishpond
            return GetAuthorityRulesBool(userName, "2313", authIntNo);
        }

        public static bool GetIsOrNotShowStatusCodeOfViewOffence_Search(string userName, int authIntNo)
        {
            return GetAuthorityRulesBool(userName, "4930", authIntNo);
        }        

        //Jerry 2013-06-05 add for secondary offence
        public static bool GetIsOrNotShowSecondaryOffence(string userName, int authIntNo)
        {
            return GetAuthorityRulesBool(userName, "0590", authIntNo);
        }

        public static KeyValuePair<int,bool> GetIsOrNotAllowBach(string userName, int authIntNo)
        {
            KeyValuePair<int, string> rule = GetAuthorityRulesKeyValue(userName, "0590", authIntNo);
            return new KeyValuePair<int,bool>(rule.Key,rule.Value == "Y");
        }



        private static bool GetAuthorityRulesBool(string userName, string arCode, int authIntNo)
        {
            return GetAuthorityRulesKeyValue(userName,arCode,authIntNo).Value.Equals("Y");
        }

        private static KeyValuePair<int, string> GetAuthorityRulesKeyValue(string userName, string arCode, int authIntNo)
        {
            return (new DefaultAuthRules(GetAuthorityRules(userName, arCode, authIntNo),
                Config.ConnectionString)).SetDefaultAuthRule();
        }

        private static AuthorityRulesDetails GetAuthorityRules(string userName, string arCode, int authIntNo)
        { 
            return new AuthorityRulesDetails()
            {
                AutIntNo = authIntNo,
                ARCode = arCode,
                LastUser = userName
            };
        }
    }
}
