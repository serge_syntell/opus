﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Cache;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class LanguageSelectorManager
    {
        public static LanguageSelectorService lanSelService = new LanguageSelectorService();
        public static TList<LanguageSelector> GetAllLanSelector()
        {
            return lanSelService.GetAll();
        }

        public static LanguageSelector GetDefaultLanSelector()
        {
            return LanguageSelectorCache.GetDefaultLanguageSelector();
        }

        public static LanguageSelector GetLanguageSelectorByLSCode(string lsCode)
        {
            return LanguageSelectorCache.GetLanguageSelectorByLSCode(lsCode);
        }
    }
}
