﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.CameraManagement.EnumLinkes
{
    public class FrameNaTISErrorFieldListConst
    {
        public const string PROXY_ID_NO_BLACK = "proxy id no is blank";
        public const string SURNAME_BLACK = "surname is blank";
        public const string PO_ADDR1_BLACK = "po addr1 is blank";
        public const string PO_CODE_BLACK = "po code is blank";
        public const string ID_NO_BLACK = "id no is blank";
    }
}
