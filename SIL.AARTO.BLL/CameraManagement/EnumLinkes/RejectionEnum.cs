﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.CameraManagement.EnumLinkes
{
    public class RejectionEnum
    {
        private static readonly RejectionService rejectionServices = new RejectionService();
        private static RejectionDB rejDB = new RejectionDB(Config.ConnectionString);

        public readonly KeyValuePair<string, int> NONE =
            GetKeyValueEntity("None");
        public readonly KeyValuePair<string, int> ASD_INVALID_SETUP =
            GetKeyValueEntity("ASD Camera Setup Invalid");
        public readonly KeyValuePair<string, int> EXPIRED =
            GetKeyValueEntity("Expired. The frame offence date is too old to continue");

        private static string LSCode = string.Empty;
        private static string UserName = string.Empty;
        public static RejectionEnum Init(string lsCode, string userName)
        {
            LSCode = lsCode;
            UserName = userName;
            return new RejectionEnum();
        }

        static KeyValuePair<string, int> GetKeyValueEntity(string key)
        {
            Rejection rejection = rejectionServices.GetByRejReason(key);
            int value = 0;
            if (rejection == null)
            {
                value = rejDB.UpdateRejection(0, key, "N", UserName, 0, "Y");
            }
            else
            {
                value = rejection.RejIntNo;
            }
            return new KeyValuePair<string, int>(key, value);
        }

    }
}
