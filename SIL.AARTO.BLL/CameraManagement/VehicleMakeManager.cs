﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Stalberg.TMS;
using System.Data;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class VehicleMakeManager
    {
        private static VehicleMakeDB vehicleMakeDB = new VehicleMakeDB(Config.ConnectionString);
        public static SelectList GetSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            Dictionary<int, string> allVehicleMaker = VehicleMakeLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            int vmIntNo;
            foreach (DataRow row in vehicleMakeDB.GetVehicleMakeListDS("").Tables[0].Rows)
            {
                vmIntNo = (int)row["VMIntNo"];
                if (allVehicleMaker.Keys.Contains(vmIntNo))
                {
                    list.Add(new SelectListItem() { Value = vmIntNo.ToString(), Text = allVehicleMaker[vmIntNo] });
                }
                else
                {
                    list.Add(new SelectListItem() { Value = vmIntNo.ToString(), Text = row["VMDescr"].ToString() });
                }
            }
            return new SelectList(list, "Value", "Text");
        }

        public static VehicleMakeDetails GetVehicleMakeDetailsByCode(string vmCode, string system)
        {
            return vehicleMakeDB.GetVehicleMakeDetailsByCode(vmCode, system);
        }

        public static VehicleMakeDetails GetByVmIntNo(int vmIntNo)
        {
            return vehicleMakeDB.GetVehicleMakeDetails(vmIntNo);
        }
    }
}
