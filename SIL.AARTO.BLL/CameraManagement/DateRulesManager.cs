﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Culture;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class DateRulesManager
    {
        public static int GetNoOfDays(UserLoginInfo userInfo, int authIntNo)
        {
            DateRulesDetails dateRule = new DateRulesDetails()
            {
                AutIntNo = authIntNo,
                DtRStartDate = "NotOffenceDate",
                DtREndDate = "NotIssue1stNoticeDate",
                LastUser = userInfo.UserName
            };
            return (new DefaultDateRules(dateRule, Config.ConnectionString)).SetDefaultDateRule();
        }
    }
}
