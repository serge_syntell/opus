﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class FrameDriverManager
    {
        private static FrameDriverDB frameDriverDB = new FrameDriverDB(Config.ConnectionString);
        public static FrameDriverDetails GetFrameDriverDetailsByFrame(int frameIntNo)
        {
            return frameDriverDB.GetFrameDriverDetailsByFrame(frameIntNo);
        }
        public static int UpdateFrameDriver(int frDrvIntNo, int frameIntNo, string frDrvSurname, string frDrvInitials,
                        string frDrvIDNumber, string frDrvPoAdd1, string frDrvPoAdd2, string frDrvPoAdd3,
                        string frDrvPoAdd4, string frDrvPoAdd5, string frDrvPoCode, string frDrvStAdd1,
                        string frDrvStAdd2, string frDrvStAdd3, string frDrvStAdd4,
                        string lastUser)
        {
            return frameDriverDB.UpdateFrameDriver(frDrvIntNo, frameIntNo,  frDrvSurname,  frDrvInitials,
                         frDrvIDNumber,  frDrvPoAdd1,  frDrvPoAdd2,  frDrvPoAdd3,
                         frDrvPoAdd4,  frDrvPoAdd5,  frDrvPoCode,  frDrvStAdd1,
                         frDrvStAdd2,  frDrvStAdd3,  frDrvStAdd4,
                         lastUser);
        }

    }
}
