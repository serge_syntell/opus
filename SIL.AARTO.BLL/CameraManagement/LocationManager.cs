﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;
using System.Data;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class LocationManager
    {
        public static SelectList GetSelectList(int authIntNo)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            LocationDB locationList = new LocationDB(Config.ConnectionString);
            foreach (DataRow row in locationList.GetLocationListDS(authIntNo, string.Empty, "LocDescr").Tables[0].Rows)
            {
                list.Add(new SelectListItem() { Value = (row["LocIntNo"]).ToString(), Text = (string)row["LocDescr"] });
            }
            return new SelectList(list, "Value", "Text");
        }
    }
}
