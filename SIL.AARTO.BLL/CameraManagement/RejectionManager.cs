﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Utility;
using System.Web.Mvc;
using System.Data;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class RejectionManager
    {
        private static RejectionDB rejDB = new RejectionDB(Config.ConnectionString);
        public static int GetASDInvalidRejIntNo(UserLoginInfo userInfo)
        {
            return rejDB.UpdateRejection(0, "ASD Camera Setup Invalid", "N", userInfo.UserName, 0, "Y");
        }

        public static RejectionDetails GetRejectionDetailsByRejIntNo(int rejIntNo)
        {
            return rejDB.GetRejectionDetails(rejIntNo);
        }

        public static SelectList GetSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            Dictionary<int, string> rejReasons = RejectionLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            int regIntNo;
            foreach (DataRow row in rejDB.GetRejectionListDS().Tables[0].Rows)
            {
                regIntNo = (int)row["RejIntNo"];
                if (rejReasons.Keys.Contains(regIntNo))
                {
                    list.Add(new SelectListItem() { Value = regIntNo.ToString(), Text = rejReasons[regIntNo] });
                }
                else {
                    list.Add(new SelectListItem() { Value = regIntNo.ToString(), Text = row["RejReason"].ToString() });
                }
            }
            return new SelectList(list, "Value", "Text");
        }

        public static SelectList GetSelectListValueWithText()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            Dictionary<int, string> rejReasons = RejectionLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            int regIntNo;
            foreach (DataRow row in rejDB.GetRejectionListDS().Tables[0].Rows)
            {
                regIntNo = (int)row["RejIntNo"];
                string value = string.Empty;
                string text = string.Empty;
                if (rejReasons.Keys.Contains(regIntNo))
                {
                    text = rejReasons[regIntNo];
                    value = regIntNo.ToString() + '|' + row["RejReason"].ToString();
                    list.Add(new SelectListItem() { Value = value, Text = text });
                }
                else
                {
                    text = row["RejReason"].ToString();
                    value = regIntNo.ToString() + '|' + text;
                    list.Add(new SelectListItem() { Value = value, Text =  text});
                }
            }
            return new SelectList(list, "Value", "Text");
        }
    }
}
