﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Threading;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.BLL.CameraManagement.Model;
//using SIL.AES.BLL.CameraManagement.DAL;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
using System.Transactions;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class InvestigateReasonManager
    {
        public static SelectList GetSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            Dictionary<int, string> allLangReasons = InvestigateReasonLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);

            TList<InvestigateReason> allReasons = InvestigateReasonManager.GetAll();
            int inReIntNo;
            foreach (var reason in allReasons)
            {
                inReIntNo = reason.InReIntNo;
                if (allLangReasons.Keys.Contains(inReIntNo))
                {
                    list.Add(new SelectListItem() { Value = (inReIntNo).ToString(), Text = allLangReasons[inReIntNo] });
                }
                else
                {
                    list.Add(new SelectListItem() { Value = (inReIntNo).ToString(), Text = reason.InReason });
                }
            }
            return new SelectList(list, "Value", "Text");
        }

        private static readonly InvestigateReasonService service = new InvestigateReasonService();
        private static readonly InvestigateReasonLookupService lookupService = new InvestigateReasonLookupService();

        public static TList<InvestigateReason> GetAll()
        {
            return service.GetAll();
        }

        public static TList<InvestigateReason> GetPage(int start, int pageLength, out int totalCount)
        {
            return service.GetPaged(start, pageLength, out totalCount);
        }

        public static TList<InvestigateReason> Search(string searchStr, int start, int pageLength, out int count)
        {
            InvestigateReasonQuery query = new InvestigateReasonQuery();
            query.AppendContains("or", InvestigateReasonColumn.InReason, searchStr);
            string sql = string.Format("(InReason Like '%{0}%')", searchStr);
            //TList<InvestigateReason> citys = service.Find(query, "CitName", start, pageLength, out count);

            return service.GetPaged(sql, "InReason", start, pageLength, out count);
        }

        public static InvestigateReason GetByInReIntNo(int inReIntNo)
        {
            InvestigateReason reason = service.GetByInReIntNo(inReIntNo);
            reason.InvestigateReasonLookupCollection = lookupService.GetByInReIntNo(inReIntNo);
            return reason;
        }


        public static int Add(InvestigateReason reason, TList<InvestigateReasonLookup> languages)
        {
            InvestigateReasonQuery query = new InvestigateReasonQuery();
            query.Append(InvestigateReasonColumn.InReason, reason.InReason);
            if (service.Find(query).Count > 0)
            {
                return -1;
            }
            using (TransactionScope scope = new TransactionScope())
            {
                SetLanguageFK(languages, service.Save(reason).InReIntNo);
                lookupService.Save(languages);
                scope.Complete();
                return 1;
            }
        }

        public static int Update(InvestigateReason reason, TList<InvestigateReasonLookup> languages, int inReIntNo)
        {
            InvestigateReason reasonOld = service.GetByInReIntNo(inReIntNo);
            InvestigateReasonQuery query = new InvestigateReasonQuery();
            query.Append(InvestigateReasonColumn.InReason, reason.InReason);

            if (reasonOld.InReason != reason.InReason && service.Find(query).Count > 0)
            {
                return -1;
            }

            using (TransactionScope scope = new TransactionScope())
            {
                reason.RowVersion = reasonOld.RowVersion;
                service.Update(reason);
                lookupService.Save(UpdateLookupBeforProcess(languages, inReIntNo));
                scope.Complete();
                return 1;
            }
        }

        public static int Delete(int inReIntNo)
        {
            InvestigateReason investigateReason = service.GetByInReIntNo(inReIntNo);
            using (TransactionScope scope = new TransactionScope())
            {
                lookupService.Delete(lookupService.GetByInReIntNo(inReIntNo));
                service.Delete(investigateReason);
                scope.Complete();
                return 1;
            }
        }


        private static void SetLanguageFK(TList<InvestigateReasonLookup> langusges, int inReIntNo)
        {
            foreach (var lookup in langusges)
            {
                lookup.InReIntNo = inReIntNo;
            }
        }

        private static TList<InvestigateReasonLookup> UpdateLookupBeforProcess(TList<InvestigateReasonLookup> languages, int inReIntNo)
        {
            TList<InvestigateReasonLookup> lookups = lookupService.GetByInReIntNo(inReIntNo);
            foreach (var lookup in languages)
            {
                if (lookups.Exists(item => item.LsCode == lookup.LsCode))
                {
                    lookups.Find(item => item.LsCode == lookup.LsCode).InReason = lookup.InReason;
                }
                else if (!string.IsNullOrEmpty(lookup.InReason))
                {
                    lookups.Add(lookup);
                }
            }
            return lookups;
        }

    }
}
