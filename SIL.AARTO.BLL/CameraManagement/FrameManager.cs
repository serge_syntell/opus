﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Reflection;
using System.Transactions;
using SIL.QueueLibrary;
using SIL.DMS.DAL.Services;


namespace SIL.AARTO.BLL.CameraManagement
{
    public static class FrameManager
    {
        private static FrameDB frameDB = new FrameDB(Config.ConnectionString);
        //private static readonly FrameService service = new FrameService();
        // 2013-12-25, Jerry promoted dbService to top
        private static readonly FilmService filmService = new FilmService();
        private static readonly SecondaryOffenceService secondaryOffenceService = new SecondaryOffenceService();
        private static readonly NoticeService noticeService = new NoticeService();
        private static readonly ChargeService chargeService = new ChargeService();
        private static readonly FrameService frameService = new FrameService();
        private static readonly FrameOffenceService frameOffenceService = new FrameOffenceService();
        private static readonly RejectionService rejectionService = new RejectionService();
        private static readonly ScanImageService scanImageService = new ScanImageService();
        private static readonly FrameDriverService frameDriverService = new FrameDriverService();
        private static readonly FrameOwnerService frameOwnerService = new FrameOwnerService();
        private static readonly FrameProxyService frameProxyService = new FrameProxyService();
        private static readonly AuthorityService authorityService = new AuthorityService();
        private static readonly DocumentErrorMessageService documentErrorMessageService = new DocumentErrorMessageService();
        private static readonly BatchDocumentService batchDocumentService = new BatchDocumentService();

        public static FrameDetails GetFrameDetailsOfFilmByFilmNo(int filmIntNo)
        {
            return frameDB.GetFrameDetailsForFilm(filmIntNo);
        }

        public static FrameDetails GetFrameDetails(int frameIntNo)
        {
            return frameDB.GetFrameDetails(frameIntNo);
        }

        public static string GetViolationType(int frameIntNo)
        {
            return frameDB.GetViolationType(frameIntNo);
        }

        public static int UpdateAllFramesForFilm(int filmIntNo, int locIntNo, int camIntNo, string proceed, string userName)
        {
            return frameDB.UpdateAllFramesForFilm(filmIntNo, locIntNo, camIntNo, proceed, userName);
        }

        public static FrameList GetFrameList(
            int filmIntNo,
            string filmNo,
            int authIntNo,
            UserLoginInfo userInfo,
            ValidationStatus status,
            ValidationStatus validation,
            ValidationStatus action
            )
        {
            if (validation == ValidationStatus.Verification)
            {

                return new FrameList(
                    action,
                    filmNo,
                    frameDB.FrameToVerify(
                        filmIntNo,
                        authIntNo,
                        userInfo.UserName,
                        status,
                        AuthorityRulesManager.GetIsOrNotShowDiscrepanciesOnly(userInfo.UserName, authIntNo))
                    );
            }
            else if (validation == ValidationStatus.Adjudication)
            {
                return new FrameList(action, filmNo, frameDB.FrameToAdjudicate(filmIntNo, status));
            }
            else
            {
                return new FrameList(action, filmNo, frameDB.FrameToInvestigate(filmIntNo, status));
            }
        }


        public static bool VerficationToInvestigation(int frameIntNo, int status, string userName, DateTime time, int inReIntNo)
        {
            Frame frame = frameService.GetByFrameIntNo(frameIntNo);
            frame.FrameStatus = status;
            frame.FrameVerifyUser = userName;
            frame.LastUser = userName;
            frame.FrameVerifyDateTime = time;
            frame.InReIntNo = inReIntNo;
            return frameService.Update(frame);
        }
        //20120103 added by Nancy for investigate
        public static bool AdjudicationToInvestigation(int frameIntNo, int status, string userName, DateTime time, int inReIntNo)
        {
            Frame frame = frameService.GetByFrameIntNo(frameIntNo);
            frame.FrameStatus = status;
            frame.FrameAdjudicateUser = userName;
            frame.LastUser = userName;
            frame.FrameAdjudicateDateTime = time;
            frame.InReIntNo = inReIntNo;
            return frameService.Update(frame);

        }

        /// <summary>
        /// Get By FilmIntNo FrameNo
        /// 2013-12-25, Jerry promoted dbService to top
        /// </summary>
        /// <param name="filmIntNo"></param>
        /// <param name="frameNo"></param>
        /// <returns></returns>
        public static SIL.AARTO.DAL.Entities.Frame GetByFilmIntNoFrameNo(int filmIntNo, string frameNo)
        {
            SIL.AARTO.DAL.Entities.Frame frameEntity = null;
            //frameEntity = new SIL.AARTO.DAL.Services.FrameService().GetByFilmIntNoFrameNo(filmIntNo, frameNo.Trim());
            frameEntity = frameService.GetByFilmIntNoFrameNo(filmIntNo, frameNo.Trim());
            return frameEntity;
        }

        #region about secondary offence

        /// <summary>
        /// Get All Child Frames Secondary Offence Code
        /// 2013-12-25, Jerry promoted dbService to top
        /// </summary>
        /// <param name="filmNo"></param>
        /// <param name="frameNo"></param>
        /// <returns></returns>
        public static string GetAllChildFramesSecondaryOffenceCode(string filmNo, string frameNo)
        {
            string secondaryOffenceCode = string.Empty;
            //SIL.AARTO.DAL.Entities.Film filmEntity = FilmManager.GetFilmByFilmNo(filmNo.Trim());
            SIL.AARTO.DAL.Entities.Film filmEntity = filmService.GetByFilmNo(filmNo.Trim());
            if (filmEntity != null)
            {
                Frame parentFrameEntity = FrameManager.GetByFilmIntNoFrameNo(filmEntity.FilmIntNo, frameNo.Trim());
                //TList<Frame> childFrames = new SIL.AARTO.DAL.Services.FrameService().GetByParentFrameIntNo(parentFrameEntity.FrameIntNo);
                TList<Frame> childFrames = frameService.GetByParentFrameIntNo(parentFrameEntity.FrameIntNo);
                foreach (Frame childFrame in childFrames)
                {
                    if (childFrame.RejIntNo != 1)
                        continue;
                    //FrameOffence frameOffence = new SIL.AARTO.DAL.Services.FrameOffenceService().GetByFrameIntNo(childFrame.FrameIntNo).FirstOrDefault();
                    FrameOffence frameOffence = frameOffenceService.GetByFrameIntNo(childFrame.FrameIntNo).FirstOrDefault();
                    if (frameOffence != null)
                    {
                        //SecondaryOffence secondaryOffence = new SIL.AARTO.DAL.Services.SecondaryOffenceService().GetByOffIntNo(frameOffence.OffIntNo).FirstOrDefault();
                        SecondaryOffence secondaryOffence = secondaryOffenceService.GetByOffIntNo(frameOffence.OffIntNo).FirstOrDefault();
                        if (secondaryOffence != null)
                        {
                            secondaryOffenceCode = secondaryOffenceCode + secondaryOffence.SeOfCode.Trim() + "|";
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(secondaryOffenceCode))
            {
                secondaryOffenceCode = secondaryOffenceCode.Substring(0, secondaryOffenceCode.Length - 1);
            }

            return secondaryOffenceCode;
        }

        /// <summary>
        /// Create Frame About Secondary Offence
        /// </summary>
        /// <param name="filmNo"></param>
        /// <param name="parentFrameNo"></param>
        /// <param name="secondaryOffenceCode"></param>
        /// <param name="lastUser"></param>
        public static void CreateFrameAboutSecondaryOffence(string filmNo, string parentFrameNo, string secondaryOffenceCode, string lastUser)
        {
            //Check child frame whether is exists
            bool isExists = false;
            int childFrameIntNo = 0;
            isExists = CheckChildFrameIsExists(filmNo, parentFrameNo, secondaryOffenceCode, ref childFrameIntNo);
            if (isExists)
            {
                RejectChildFrame(childFrameIntNo, false, lastUser);
                return;
            }

            CreateChildFrame(filmNo, parentFrameNo, secondaryOffenceCode, lastUser);
        }

        /// <summary>
        /// Reject Frame About Secondary Offence
        /// </summary>
        /// <param name="filmNo"></param>
        /// <param name="parentFrameNo"></param>
        /// <param name="secondaryOffenceCode"></param>
        /// <param name="lastUser"></param>
        public static void RejectFrameAboutSecondaryOffence(string filmNo, string parentFrameNo, string secondaryOffenceCode, string lastUser)
        {
            //Check child frame whether is exists
            bool isExists = false;
            int childFrameIntNo = 0;
            isExists = CheckChildFrameIsExists(filmNo, parentFrameNo, secondaryOffenceCode, ref childFrameIntNo);
            if (!isExists)
                return;

            RejectChildFrame(childFrameIntNo, true, lastUser);
        }

        /// <summary>
        /// Create Child Frame
        /// </summary>
        /// <param name="filmNo"></param>
        /// <param name="parentFrameNo"></param>
        /// <param name="secondaryOffenceCode"></param>
        /// <param name="lastUser"></param>
        private static void CreateChildFrame(string filmNo, string parentFrameNo, string secondaryOffenceCode, string lastUser)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //insert frame
                    Frame newFrame = InsertFrame(filmNo, parentFrameNo, lastUser);
                    if (newFrame != null)
                    {
                        //Insert scan Image about sub frame
                        InsertImages(newFrame.FrameIntNo, filmNo, parentFrameNo, lastUser);

                        //Insert frame_offence about sub frame
                        InsertFrame_Offence(filmNo, parentFrameNo, newFrame.FrameIntNo, secondaryOffenceCode, lastUser);

                        //Insert frame driver about sub frame
                        InsertFrameDriver(newFrame.FrameIntNo, filmNo, parentFrameNo, lastUser);

                        //Insert frame owner about sub frame
                        InsertFrameOwner(newFrame.FrameIntNo, filmNo, parentFrameNo, lastUser);

                        //Insert frame proxy about sub frame
                        InsertFrameProxy(newFrame.FrameIntNo, filmNo, parentFrameNo, lastUser);

                        //push CancelExpiredViolations_Frame queue about this new frame
                        PushCancelExpiredViolationsQueue(filmNo, newFrame.FrameIntNo, lastUser);
                    }

                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Check Child Frame Is Exists
        /// 2013-12-25, Jerry promoted dbService to top
        /// </summary>
        /// <param name="filmNo"></param>
        /// <param name="parentFrameNo"></param>
        /// <param name="secondaryOffenceCode"></param>
        /// <param name="childFrameIntNo"></param>
        /// <returns></returns>
        public static bool CheckChildFrameIsExists(string filmNo, string parentFrameNo, string secondaryOffenceCode, ref int childFrameIntNo)
        {
            bool isExists = false;
            //SIL.AARTO.DAL.Entities.Film filmEntity = FilmManager.GetFilmByFilmNo(filmNo.Trim());
            SIL.AARTO.DAL.Entities.Film filmEntity = filmService.GetByFilmNo(filmNo);
            if (filmEntity != null)
            {
                Frame parentFrameEntity = FrameManager.GetByFilmIntNoFrameNo(filmEntity.FilmIntNo, parentFrameNo.Trim());
                //TList<Frame> childFrames = new SIL.AARTO.DAL.Services.FrameService().GetByParentFrameIntNo(parentFrameEntity.FrameIntNo);
                TList<Frame> childFrames = frameService.GetByParentFrameIntNo(parentFrameEntity.FrameIntNo);
                foreach (Frame childFrame in childFrames)
                {
                    //FrameOffence frameOffence = new SIL.AARTO.DAL.Services.FrameOffenceService().GetByFrameIntNo(childFrame.FrameIntNo).FirstOrDefault();
                    FrameOffence frameOffence = frameOffenceService.GetByFrameIntNo(childFrame.FrameIntNo).FirstOrDefault();
                    if (frameOffence != null)
                    {
                        //SecondaryOffence secondaryOffence = new SIL.AARTO.DAL.Services.SecondaryOffenceService().GetByOffIntNo(frameOffence.OffIntNo).FirstOrDefault();
                        SecondaryOffence secondaryOffence = secondaryOffenceService.GetByOffIntNo(frameOffence.OffIntNo).FirstOrDefault();
                        if (secondaryOffence != null)
                        {
                            if (secondaryOffence.SeOfCode.Trim() == secondaryOffenceCode.Trim())
                            {
                                isExists = true;
                                childFrameIntNo = childFrame.FrameIntNo;
                                break;
                            }
                        }
                    }
                }
            }

            return isExists;
        }

        /// <summary>
        /// Reject Child Frame
        /// 2013-12-25, Jerry promoted dbService to top
        /// </summary>
        /// <param name="childFrameIntNo"></param>
        /// <param name="isCancel"></param>
        /// <param name="lastUser"></param>
        private static void RejectChildFrame(int childFrameIntNo, bool isCancel, string lastUser)
        {
            SIL.AARTO.DAL.Entities.Rejection rejectEntity;

            if (isCancel)
                //rejectEntity = new SIL.AARTO.DAL.Services.RejectionService().GetByRejReason("Secondary Offence Cancel");
                rejectEntity = rejectionService.GetByRejReason("Secondary Offence Cancel");
            else
                //rejectEntity = new SIL.AARTO.DAL.Services.RejectionService().GetByRejReason("None");
                rejectEntity = rejectionService.GetByRejReason("None");

            //Frame childFrame = new SIL.AARTO.DAL.Services.FrameService().GetByFrameIntNo(childFrameIntNo);
            Frame childFrame = frameService.GetByFrameIntNo(childFrameIntNo);
            if (childFrame != null && rejectEntity != null)
            {
                childFrame.RejIntNo = rejectEntity.RejIntNo;
                childFrame.LastUser = lastUser;
                //new SIL.AARTO.DAL.Services.FrameService().Save(childFrame);
                frameService.Save(childFrame);
            }
        }

        /// <summary>
        /// Insert Frame
        /// 2013-12-25, Jerry promoted dbService to top
        /// </summary>
        /// <param name="filmNo"></param>
        /// <param name="parentFrameNo"></param>
        /// <param name="lastUser"></param>
        /// <returns></returns>
        private static Frame InsertFrame(string filmNo, string parentFrameNo, string lastUser)
        {
            //SIL.AARTO.DAL.Services.FilmService filmService = new SIL.AARTO.DAL.Services.FilmService();
            //SIL.AARTO.DAL.Services.FrameService frameService = new SIL.AARTO.DAL.Services.FrameService();

            SIL.AARTO.DAL.Entities.Frame newFrame = null;
            try
            {
                SIL.AARTO.DAL.Entities.Film film = filmService.GetByFilmNo(filmNo.Trim());
                if (film != null)
                {
                    List<SIL.AARTO.DAL.Entities.Frame> frames = frameService.GetByFilmIntNo(film.FilmIntNo).ToList();
                    int maxFrameNo = 0; int tempFrameNo = 0;
                    foreach (SIL.AARTO.DAL.Entities.Frame f in frames)
                    {
                        tempFrameNo = Convert.ToInt32(f.FrameNo);
                        if (tempFrameNo > maxFrameNo)
                            maxFrameNo = tempFrameNo;
                    }

                    SIL.AARTO.DAL.Entities.Frame frame = frameService.GetByFilmIntNoFrameNo(film.FilmIntNo, parentFrameNo.Trim());
                    if (frame != null)
                    {
                        // insert a copy of the current frame - FrameNo is incremented by 1
                        newFrame = new SIL.AARTO.DAL.Entities.Frame();

                        foreach (PropertyInfo properties in frame.GetType().GetProperties())
                        {

                            if (properties.Name.ToLower().Equals("rowversion"))
                                continue;
                            if (properties.Name.ToLower().Equals("frameintno"))
                                continue;
                            PropertyInfo pInfo = newFrame.GetType().GetProperties().SingleOrDefault(t => t.Name.Equals(properties.Name));
                            if (pInfo != null)
                            {
                                if (pInfo.CanWrite && properties.CanRead)
                                    pInfo.SetValue(newFrame, properties.GetValue(frame, null), null);
                            }
                        }

                        newFrame.FrameNo = (maxFrameNo + 1).ToString().PadLeft(frame.FrameNo.Length, '0');
                        newFrame.ParentFrameIntNo = frame.FrameIntNo;
                        newFrame.LastUser = lastUser;
                        newFrame = frameService.Save(newFrame);

                        film.NoOfFrames = film.NoOfFrames + 1;
                        film = filmService.Save(film);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return newFrame;
        }

        /// <summary>
        /// Insert Images
        /// 2013-12-25, Jerry promoted dbService to top
        /// </summary>
        /// <param name="subFrameIntNo"></param>
        /// <param name="filmNo"></param>
        /// <param name="parentFrameNo"></param>
        /// <param name="lastUser"></param>
        private static void InsertImages(int subFrameIntNo, string filmNo, string parentFrameNo, string lastUser)
        {
            //SIL.AARTO.DAL.Services.FilmService filmService = new SIL.AARTO.DAL.Services.FilmService();
            //SIL.AARTO.DAL.Services.FrameService frameService = new SIL.AARTO.DAL.Services.FrameService();
            //SIL.AARTO.DAL.Services.ScanImageService scanImageService = new SIL.AARTO.DAL.Services.ScanImageService();

            Film film = filmService.GetByFilmNo(filmNo.Trim());
            if (film != null)
            {
                Frame parentFrame = frameService.GetByFilmIntNoFrameNo(film.FilmIntNo, parentFrameNo.Trim());
                if (parentFrame != null)
                {
                    TList<ScanImage> returnList = new TList<ScanImage>();
                    TList<ScanImage> scanImageList = new TList<ScanImage>();
                    ScanImage newScanImage;
                    try
                    {
                        scanImageList = scanImageService.GetByFrameIntNo(parentFrame.FrameIntNo);

                        foreach (SIL.AARTO.DAL.Entities.ScanImage scanImage in scanImageList)
                        {
                            newScanImage = new SIL.AARTO.DAL.Entities.ScanImage();
                            newScanImage.FrameIntNo = subFrameIntNo;
                            newScanImage.ScImType = scanImage.ScImType;
                            newScanImage.JpegName = scanImage.JpegName;
                            newScanImage.Xvalue = scanImage.Xvalue;
                            newScanImage.Yvalue = scanImage.Yvalue;
                            newScanImage.LastUser = lastUser;
                            newScanImage.ScImPrintVal = scanImage.ScImPrintVal;
                            newScanImage.Contrast = scanImage.Contrast;
                            newScanImage.Brightness = scanImage.Brightness;
                            newScanImage.ScanImage = scanImage.ScanImage;

                            returnList.Add(newScanImage);
                        }
                        scanImageService.Save(returnList);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    if (returnList.Count > 0)
                    {
                        film.NoOfScans += returnList.Count;
                        filmService.Update(film);
                    }
                }
            }
        }

        /// <summary>
        /// Insert Frame_Offence
        /// 2013-12-25, Jerry promoted dbService to top
        /// </summary>
        /// <param name="filmNo"></param>
        /// <param name="parentFrameNo"></param>
        /// <param name="subFrameIntNo"></param>
        /// <param name="secondaryOffenceCode"></param>
        /// <param name="lastUser"></param>
        private static void InsertFrame_Offence(string filmNo, string parentFrameNo, int subFrameIntNo, string secondaryOffenceCode, string lastUser)
        {
            //SIL.AARTO.DAL.Services.FilmService filmService = new SIL.AARTO.DAL.Services.FilmService();
            //SIL.AARTO.DAL.Services.FrameService frameService = new SIL.AARTO.DAL.Services.FrameService();

            Film film = filmService.GetByFilmNo(filmNo.Trim());
            if (film != null)
            {
                Frame parentFrame = frameService.GetByFilmIntNoFrameNo(film.FilmIntNo, parentFrameNo.Trim());
                if (parentFrame != null)
                {
                    FrameOffence newFrameOffence = null;
                    //FrameOffence frameOffence = new SIL.AARTO.DAL.Services.FrameOffenceService().GetByFrameIntNo(parentFrame.FrameIntNo).FirstOrDefault();
                    FrameOffence frameOffence = frameOffenceService.GetByFrameIntNo(parentFrame.FrameIntNo).FirstOrDefault();
                    if (frameOffence != null)
                    {
                        newFrameOffence = new AARTO.DAL.Entities.FrameOffence();
                        foreach (PropertyInfo properties in frameOffence.GetType().GetProperties())
                        {

                            if (properties.Name.ToLower().Equals("rowversion"))
                                continue;
                            if (properties.Name.ToLower().Equals("frofintno"))
                                continue;
                            PropertyInfo pInfo = newFrameOffence.GetType().GetProperties().SingleOrDefault(t => t.Name.Equals(properties.Name));
                            if (pInfo != null)
                            {
                                if (pInfo.CanWrite && properties.CanRead)
                                    pInfo.SetValue(newFrameOffence, properties.GetValue(frameOffence, null), null);
                            }
                        }

                        newFrameOffence.FrameIntNo = subFrameIntNo;
                        //newFrameOffence.OffIntNo = new SIL.AARTO.DAL.Services.SecondaryOffenceService().GetBySeOfCode(secondaryOffenceCode.Trim()).OffIntNo; //Vehicle license expired
                        newFrameOffence.OffIntNo = secondaryOffenceService.GetBySeOfCode(secondaryOffenceCode.Trim()).OffIntNo; 
                        newFrameOffence.LastUser = lastUser;
                        //new SIL.AARTO.DAL.Services.FrameOffenceService().Save(newFrameOffence);
                        frameOffenceService.Save(newFrameOffence);
                    }
                }
            }
        }

        /// <summary>
        /// Get All Secondary Frames
        /// 2013-12-25, Jerry promoted dbService to top
        /// </summary>
        /// <param name="parentFrameIntNo"></param>
        /// <returns></returns>
        public static List<Frame> GetAllSecondaryFrames(int parentFrameIntNo)
        {
            //TList<Frame> frameEntityList = new SIL.AARTO.DAL.Services.FrameService().GetByParentFrameIntNo(parentFrameIntNo);
            TList<Frame> frameEntityList = frameService.GetByParentFrameIntNo(parentFrameIntNo);
            return frameEntityList.ToList();
        }

        /// <summary>
        /// Insert Frame Driver
        /// 2013-12-25, Jerry promoted dbService to top
        /// </summary>
        /// <param name="subFrameIntNo"></param>
        /// <param name="filmNo"></param>
        /// <param name="parentFrameNo"></param>
        /// <param name="lastUser"></param>
        private static void InsertFrameDriver(int subFrameIntNo, string filmNo, string parentFrameNo, string lastUser)
        {
            //SIL.AARTO.DAL.Services.FilmService filmService = new SIL.AARTO.DAL.Services.FilmService();
            //SIL.AARTO.DAL.Services.FrameService frameService = new SIL.AARTO.DAL.Services.FrameService();
            //SIL.AARTO.DAL.Services.FrameDriverService frameDriverService = new SIL.AARTO.DAL.Services.FrameDriverService();

            SIL.AARTO.DAL.Entities.FrameDriver newFrameDriver = null;
            try
            {
                SIL.AARTO.DAL.Entities.Film film = filmService.GetByFilmNo(filmNo.Trim());
                SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.FrameDriver> newFrameDriverList = frameDriverService.GetByFrameIntNo(subFrameIntNo);
                if (film != null && newFrameDriverList.Count <= 0)
                {
                    SIL.AARTO.DAL.Entities.Frame parentFrame = frameService.GetByFilmIntNoFrameNo(film.FilmIntNo, parentFrameNo.Trim());
                    if (parentFrame != null)
                    {
                        SIL.AARTO.DAL.Entities.FrameDriver parentFrameDriver = frameDriverService.GetByFrameIntNo(parentFrame.FrameIntNo).FirstOrDefault();
                        if (parentFrameDriver != null)
                        {
                            newFrameDriver = new SIL.AARTO.DAL.Entities.FrameDriver();

                            foreach (PropertyInfo properties in parentFrameDriver.GetType().GetProperties())
                            {

                                if (properties.Name.ToLower().Equals("rowversion"))
                                    continue;
                                if (properties.Name.ToLower().Equals("frdrvintno"))
                                    continue;
                                PropertyInfo pInfo = newFrameDriver.GetType().GetProperties().SingleOrDefault(t => t.Name.Equals(properties.Name));
                                if (pInfo != null)
                                {
                                    if (pInfo.CanWrite && properties.CanRead)
                                        pInfo.SetValue(newFrameDriver, properties.GetValue(parentFrameDriver, null), null);
                                }
                            }
                            newFrameDriver.FrameIntNo = subFrameIntNo;
                            newFrameDriver.LastUser = lastUser;

                            frameDriverService.Save(newFrameDriver);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Insert Frame Owner
        /// 2013-12-25, Jerry promoted dbService to top
        /// </summary>
        /// <param name="subFrameIntNo"></param>
        /// <param name="filmNo"></param>
        /// <param name="parentFrameNo"></param>
        /// <param name="lastUser"></param>
        private static void InsertFrameOwner(int subFrameIntNo, string filmNo, string parentFrameNo, string lastUser)
        {
            //SIL.AARTO.DAL.Services.FilmService filmService = new SIL.AARTO.DAL.Services.FilmService();
            //SIL.AARTO.DAL.Services.FrameService frameService = new SIL.AARTO.DAL.Services.FrameService();
            //SIL.AARTO.DAL.Services.FrameOwnerService frameOwnerService = new SIL.AARTO.DAL.Services.FrameOwnerService();

            SIL.AARTO.DAL.Entities.FrameOwner newFrameOwner = null;
            try
            {
                SIL.AARTO.DAL.Entities.Film film = filmService.GetByFilmNo(filmNo.Trim());
                SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.FrameOwner> newFrameOwnerList = frameOwnerService.GetByFrameIntNo(subFrameIntNo);
                if (film != null && newFrameOwnerList.Count <= 0)
                {
                    SIL.AARTO.DAL.Entities.Frame parentFrame = frameService.GetByFilmIntNoFrameNo(film.FilmIntNo, parentFrameNo.Trim());
                    if (parentFrame != null)
                    {
                        SIL.AARTO.DAL.Entities.FrameOwner parentFrameOwner = frameOwnerService.GetByFrameIntNo(parentFrame.FrameIntNo).FirstOrDefault();
                        if (parentFrameOwner != null)
                        {
                            newFrameOwner = new SIL.AARTO.DAL.Entities.FrameOwner();

                            foreach (PropertyInfo properties in parentFrameOwner.GetType().GetProperties())
                            {

                                if (properties.Name.ToLower().Equals("rowversion"))
                                    continue;
                                if (properties.Name.ToLower().Equals("frownintno"))
                                    continue;
                                PropertyInfo pInfo = newFrameOwner.GetType().GetProperties().SingleOrDefault(t => t.Name.Equals(properties.Name));
                                if (pInfo != null)
                                {
                                    if (pInfo.CanWrite && properties.CanRead)
                                        pInfo.SetValue(newFrameOwner, properties.GetValue(parentFrameOwner, null), null);
                                }
                            }
                            newFrameOwner.FrameIntNo = subFrameIntNo;
                            newFrameOwner.LastUser = lastUser;

                            frameOwnerService.Save(newFrameOwner);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Insert Frame Proxy
        /// 2013-12-25, Jerry promoted dbService to top
        /// </summary>
        /// <param name="subFrameIntNo"></param>
        /// <param name="filmNo"></param>
        /// <param name="parentFrameNo"></param>
        /// <param name="lastUser"></param>
        private static void InsertFrameProxy(int subFrameIntNo, string filmNo, string parentFrameNo, string lastUser)
        {
            //SIL.AARTO.DAL.Services.FilmService filmService = new SIL.AARTO.DAL.Services.FilmService();
            //SIL.AARTO.DAL.Services.FrameService frameService = new SIL.AARTO.DAL.Services.FrameService();
            //SIL.AARTO.DAL.Services.FrameProxyService frameProxyService = new SIL.AARTO.DAL.Services.FrameProxyService();

            SIL.AARTO.DAL.Entities.FrameProxy newFrameProxy = null;
            try
            {
                SIL.AARTO.DAL.Entities.Film film = filmService.GetByFilmNo(filmNo.Trim());
                SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.FrameProxy> newFrameProxyList = frameProxyService.GetByFrameIntNo(subFrameIntNo);
                if (film != null && newFrameProxyList.Count <= 0)
                {
                    SIL.AARTO.DAL.Entities.Frame parentFrame = frameService.GetByFilmIntNoFrameNo(film.FilmIntNo, parentFrameNo.Trim());
                    if (parentFrame != null)
                    {
                        SIL.AARTO.DAL.Entities.FrameProxy parentFrameProxy = frameProxyService.GetByFrameIntNo(parentFrame.FrameIntNo).FirstOrDefault();
                        if (parentFrameProxy != null)
                        {
                            newFrameProxy = new SIL.AARTO.DAL.Entities.FrameProxy();

                            foreach (PropertyInfo properties in parentFrameProxy.GetType().GetProperties())
                            {

                                if (properties.Name.ToLower().Equals("rowversion"))
                                    continue;
                                if (properties.Name.ToLower().Equals("frprxintno"))
                                    continue;
                                PropertyInfo pInfo = newFrameProxy.GetType().GetProperties().SingleOrDefault(t => t.Name.Equals(properties.Name));
                                if (pInfo != null)
                                {
                                    if (pInfo.CanWrite && properties.CanRead)
                                        pInfo.SetValue(newFrameProxy, properties.GetValue(parentFrameProxy, null), null);
                                }
                            }
                            newFrameProxy.FrameIntNo = subFrameIntNo;
                            newFrameProxy.LastUser = lastUser;

                            frameProxyService.Save(newFrameProxy);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Push Cancel Expired Violations Queue
        /// 2013-12-25, Jerry promoted dbService to top
        /// </summary>
        /// <param name="filmNo"></param>
        /// <param name="subFrameIntNo"></param>
        /// <param name="lastUser"></param>
        private static void PushCancelExpiredViolationsQueue(string filmNo, int subFrameIntNo, string lastUser)
        {
            //SIL.AARTO.DAL.Services.FilmService filmService = new SIL.AARTO.DAL.Services.FilmService();
            //SIL.AARTO.DAL.Services.FrameService frameService = new SIL.AARTO.DAL.Services.FrameService();

            Film film = filmService.GetByFilmNo(filmNo.Trim());
            if (film != null)
            {
                Frame subFrame = frameService.GetByFrameIntNo(subFrameIntNo);
                if (subFrame != null)
                {
                    QueueItemProcessor queProcessor = new QueueItemProcessor();
                    //SIL.AARTO.DAL.Entities.Film filmEntity = new SIL.AARTO.DAL.Services.FilmService().GetByFilmNo(filmNo.Trim());
                    SIL.AARTO.DAL.Entities.Film filmEntity = filmService.GetByFilmNo(filmNo.Trim());
                    //SIL.AARTO.DAL.Entities.Authority authorityEntity = new SIL.AARTO.DAL.Services.AuthorityService().GetByAutIntNo(filmEntity.AutIntNo);
                    SIL.AARTO.DAL.Entities.Authority authorityEntity = authorityService.GetByAutIntNo(filmEntity.AutIntNo);

                    SIL.AARTO.BLL.NoticeProcess.DateRulesDetails drDetails = new SIL.AARTO.BLL.NoticeProcess.DateRulesDetails();
                    drDetails.AutIntNo = authorityEntity.AutIntNo;
                    drDetails.DtRStartDate = "NotOffenceDate";
                    drDetails.DtREndDate = "NotIssue1stNoticeDate";
                    drDetails.LastUser = lastUser;
                    SIL.AARTO.BLL.NoticeProcess.DefaultDateRules dr = new SIL.AARTO.BLL.NoticeProcess.DefaultDateRules(drDetails);
                    int expiryNoOfDays = dr.SetDefaultDateRule();

                    queProcessor.Send(
                        new QueueItem()
                        {
                            Body = subFrame.FrameIntNo,
                            Group = authorityEntity.AutCode.Trim(),
                            ActDate = subFrame.OffenceDate.AddDays(expiryNoOfDays),
                            LastUser = lastUser,
                            QueueType = SIL.ServiceQueueLibrary.DAL.Entities.ServiceQueueTypeList.CancelExpiredViolations_Frame
                        }
                    );
                }
            }
        }

        #endregion
    }
}
