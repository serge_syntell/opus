﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Culture;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class UserManager
    {
        private static UserDB userDB = new UserDB(Config.ConnectionString);
        public static int GetUSIntNo(UserLoginInfo userInfo)
        {
            return userDB.UserShiftUpdate(userInfo.UserIntNo, userInfo.UserName);
        }

        public static string GetOfficerNo(UserLoginInfo userInfo)
        {
            return userDB.GetOfficerNo(userInfo.UserIntNo);
        }

        public static int UserShiftEdit(int nUSIntNo, string sOfficerNo, string sMode, string lastUser)
        { 
            return userDB.UserShiftEdit(nUSIntNo, sOfficerNo, sMode, lastUser);
        }
    }
}
