﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class QueueManager
    {
        private static readonly QueueItemProcessor queProcessor = new QueueItemProcessor();
        public static void PushQueueByFrameStatus(string frameStatus, object body, string group, int priority, string violationType)
        {
            // 2014-03-18, Oscar added 710 for adjudication
            if (frameStatus == "740" || frameStatus == "50" || frameStatus == "710")
            {
                PushQueue(body,group,priority,ServiceQueueTypeList.Frame_Natis);
            }
            if (frameStatus == "800" || frameStatus == "600" || frameStatus == "860")
            {
                // Oscar 2013-04-18 added delay hours
                var delayHours = new DAL.Services.SysParamService().GetBySpColumnName(DAL.Entities.SysParamList.DelayActionDate_InHours.ToString()).SpIntegerValue;

                PushQueue(body, string.Format("{0}|{1}", group, violationType), priority, ServiceQueueTypeList.Frame_Generate1stNotice, DateTime.Now.AddHours(delayHours));
            }
        }

        public static void PushQueue(object body, string group, int priority, ServiceQueueTypeList queueType, DateTime? actDate = null)
        {
            QueueItem item = new QueueItem
            {
                Body = body,
                Group = group,
                Priority = priority,
                QueueType = queueType,
                ActDate = actDate
            };
            queProcessor.Send(item);
        }
    }
}
