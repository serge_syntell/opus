using System;
using System.Data.SqlClient;
using Stalberg.TMS;
using SIL.AARTO.Web.Resource.CameraManagement;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Transactions;
using SIL.AARTO.BLL.CameraManagement.EnumLinkes;

/// <summary>
/// Summary description for FrameUpdates
/// </summary>
namespace SIL.AARTO.BLL.CameraManagement
{
    public class FrameUpdate
    {
        private string connectionString = "";
        private FrameDB frame = null;
        private FrameList frames = null;
        private int _statusProsecute = 0;
        private int _statusCancel = 0;
        private int _statusVerified = 0;
        private int _statusRejected = 0;
        private int _statusNatis = 0;
        private string _PreUpdatedFrameRegNo = "";
        private string _PreUpdatedRejReason = "";
        private string _PreUpdatedAllowContinue = "Y";
        private int _FrameIntNo = 0;
        private string _AllowContinue = "Y";
        private string Session_AdjOfficerNo = "";
        private int Session_USIntNo = 0;
        private bool Session_IsNatisLast = false;

        private int InvestigatePushToQueue = 860;

        public FrameUpdate(string conStr, ref FrameList frameList)
        {
            connectionString = conStr;
            this.frames = frameList;
            frame = new FrameDB(connectionString);
        }

        public string Adjudicate(string processName,string rejReason, int rejIntNo, string registration, bool changedMake, string vehType,
            string vehMake, string userName, string processType, int statusProsecute, int statusCancel, int statusNatis,
            string frameNo, string preUpdatedRegNo, string preUpdatedRejReason, string sessionAdjOfficerNo, int sessionUSIntNo,
            int frameIntNo, bool allowContinue, string preUpdatedAllowContinue, Int64 nRowVersion, bool isNaTISLast, int autIntNo, bool bNatisData,
            bool adjAt100, bool reject, bool sendToNatis, bool saveImageSettings, decimal contrast, decimal brightness)
        {
            string returnMessage = "";

            _statusProsecute = statusProsecute;
            _statusCancel = statusCancel;
            _statusNatis = statusNatis;

            _PreUpdatedFrameRegNo = preUpdatedRegNo;
            _PreUpdatedRejReason = preUpdatedRejReason;
            _PreUpdatedAllowContinue = preUpdatedAllowContinue;

            _FrameIntNo = frameIntNo;

            if (!allowContinue)
                _AllowContinue = "N";

            Session_AdjOfficerNo = sessionAdjOfficerNo;
            Session_USIntNo = sessionUSIntNo;
            Session_IsNatisLast = isNaTISLast;

            // Update Vehicle type, make, and Reg No
            if (!Convert.ToBoolean(changedMake))
            {
                //dls 070830 - there is no longer a check for blank registration numbers.
                if (registration.Trim().Equals(""))
                {
                    return FrameUpdateBLL.Adjudicate_Return_Str1;
                }

                //frame.UpdateFrameDetailsAtAdjudication(this.frames.Current, Convert.ToInt32(vehType), Convert.ToInt32(vehMake), registration, userName);
                SqlDataReader reader = frame.UpdateFrameDetailsAtAdjudication(_FrameIntNo, Convert.ToInt32(vehType), Convert.ToInt32(vehMake), 
                    registration, userName, rejIntNo, ref returnMessage);

                reader.Read();
                if (Convert.ToInt32( reader[0].ToString()) < 1)
                {
                    return string.Format(FrameUpdateBLL.Unable_Update_Str, returnMessage);
                }
                nRowVersion = Convert.ToInt64(reader[1]);
                reader.Dispose();
            }   

            int frameStatus = 0;

            if (sendToNatis)
                frameStatus = statusNatis;
            else if (processType.Equals("Prosecute"))
                if (processName.Equals("InvestigateFrame"))
                {
                    frameStatus = InvestigatePushToQueue;
                }
                else
                {
                    frameStatus = statusProsecute;
                }
            else
                frameStatus = statusCancel;

            returnMessage = this.AdjudicateFrame(frameStatus, rejReason, rejIntNo, registration, frameNo, userName, nRowVersion, autIntNo, isNaTISLast, bNatisData,
                adjAt100, reject, saveImageSettings, contrast, brightness);

            return returnMessage;
        }

        public string AdjudicateFrame(int status, string rejReason, int rejIntNo, string registration, string frameNo, string userName,
            Int64 nRowVersion, int autIntNo, bool isNaTISLast, bool bNatisData, bool adjAt100, bool reject, bool saveImageSettings, 
            decimal contrast, decimal brightness)
        {
            RejectionEnum rejectionEnum = RejectionEnum.Init("en-US", userName);

            string returnMessage = "";

            string confirmViolation = "N";
            if (status == _statusProsecute)
                confirmViolation = "Y";

            if (reject)
            {
                status = _statusCancel;
            }

            if (status == _statusProsecute)
            {
                if (_PreUpdatedFrameRegNo != null)
                {
                    //dls 060606 - don't resend zeroes to natis
                    if (!_PreUpdatedFrameRegNo.Equals(registration) && !registration.Equals("0000000000"))
                        status = _statusNatis;
                }

                if (_PreUpdatedRejReason != null)
                {
                    //if the reason for rejection has changed to None, we need to resend the frame to NAtis
                    if (!_PreUpdatedRejReason.Equals(rejReason) && rejReason.Equals(rejectionEnum.NONE.Key))
                        status = _statusNatis;

                    if (_PreUpdatedRejReason.Equals(rejectionEnum.ASD_INVALID_SETUP.Key))
                    {
                        returnMessage = FrameUpdateBLL.AdjudicateFrame_Return_Str1;
                        return returnMessage;
                    }
                }
            }

            string strResetRejReason = "N";
            if (rejReason.Equals(rejectionEnum.NONE.Key) || status == _statusNatis)
            {
                int regNo = 0;

                try
                {
                    regNo = Convert.ToInt32(registration);
                    if (regNo == 0 || registration.Trim().Length < 3)
                    {
                        returnMessage = FrameUpdateBLL.AdjudicateFrame_Return_Str2;
                        return returnMessage;
                    }
                }
                catch { }
            }
            //else if (GetRuleFullAdjudicated(autIntNo, userName) == "Y")
            else if (adjAt100 && status == _statusProsecute)
            {
                if (status == _statusProsecute)
                {
                    if (!bNatisData)
                    {
                        if (!isNaTISLast)
                        {
                            status = 50;
                        }
                        else
                        {
                            status = 710;
                        }
                    }

                    if (!rejReason.Equals(rejectionEnum.NONE.Key))
                    {
                        confirmViolation = "Y";
                        strResetRejReason = "Y";
                    }
                }
            }

            string errMessage = "";

            int updFrameIntNo = frame.AdjudicateFrame(_FrameIntNo, confirmViolation, userName, status, Session_AdjOfficerNo, ref errMessage,
                    _AllowContinue, _PreUpdatedAllowContinue, nRowVersion, strResetRejReason, _PreUpdatedFrameRegNo);

            switch (updFrameIntNo)
            {
                case 0:
                    returnMessage = string.Format(FrameUpdateBLL.Unable_Update_Str, errMessage);
                    break;
                case -1:
                    returnMessage = FrameUpdateBLL.AdjudicateFrame_Return_Msg2;
                    break;
                case -2:
                    returnMessage = FrameUpdateBLL.AdjudicateFrame_Return_Msg3;
                    break;
                case -3:
                    returnMessage = FrameUpdateBLL.AdjudicateFrame_Return_Msg4;
                    break;
                case -4:
                    returnMessage = FrameUpdateBLL.AdjudicateFrame_Return_Msg5;
                    break;
                case -5:
                    returnMessage = FrameUpdateBLL.AdjudicateFrame_Return_Msg6;
                    break;
                case -12:
                    returnMessage = FrameUpdateBLL.AdjudicateFrame_Return_Msg7;
                    break;
                case -13:
                    returnMessage = FrameUpdateBLL.AdjudicateFrame_Return_Msg8;
                    break;
                case -14:
                    returnMessage = FrameUpdateBLL.AdjudicateFrame_Return_Msg9;
                    break;
                default:                   

                    // dls 070517 - this will get done as a scheduled job at end of day
                    //LMZ 15-03-2007 - check to see if this frame has already been adjudicated
                    /*if (!this.frames.CurrentProcessed)
                    {
                        try
                        {
                            UserDB db = new UserDB(connectionString);
                            int nUSIntNo = db.UserShiftEdit(Session_USIntNo, "", "Adjudicate");
                        }
                        catch { }
                    }*/
                    //returnMessage = "Frame " + frameNo + " has been updated";
                    // FBJ Added (2007-03-12): Moved this to a page reload to avoid accidental reposting of data
                    //this.frames.GetNextFrame(false);
                    //this.GetNextFrameToAdjudicate();
                    //this.frames.MarkProcessed();
                    //this.SetIFrame(0, false);
                    this.frames.MarkProcessed(_FrameIntNo);

                    //dls 070604 - remove from the framelist so that they don't try to update it again
                    if (!Session_IsNatisLast && status == _statusNatis)
                        this.frames.HideFrame(_FrameIntNo);

                    //dls 100119 - need to update the ScImPrintVal = 1 ScanImage for this frame if brightness and contrast have been set
                    if (saveImageSettings && contrast != 99 && brightness != 99)
                    {
                        ImageProcesses imageProcess = new ImageProcesses(this.connectionString);

                        int scImIntNo = imageProcess.UpdateImageSetting(0, contrast, brightness * 0.1m, userName, _FrameIntNo);

                        if (scImIntNo < 1)
                        {
                            if (returnMessage.Length > 0)
                                returnMessage += "; ";

                            returnMessage += FrameUpdateBLL.AdjudicateFrame_Return_Msg10;
                            break;
                        }
                    }
                    break;
            }

            return returnMessage;
        }

        public string Verify (string process, string rejReason, int rejIntNo, string registration, string vehType,
            string vehMake, string userName, string processType, int statusVerified, int statusRejected, int statusNatis, int statusInvestigate,
            int speed1, int speed2, DateTime offenceDate, string frameNo, string preUpdatedRegNo, string preUpdatedRejReason,
            bool isNaTISLast, int frameIntNo, bool allowContinue, string preUpdatedAllowContinue, Int64 nRowVersion, int autIntNo, bool adjAt100)
        {
            string returnMessage = "";

            int vmIntNo = Convert.ToInt32(vehMake);
            int vtIntNo = Convert.ToInt32(vehType);

            _statusVerified = statusVerified;
            _statusRejected = statusRejected;

            string violation = "Y";

            //dls 090618 - moved this to the frame verification page so that it doesn't ahve to call the database on on every frame
            //if (GetRuleFullAdjudicated(autIntNo, userName) == "Y")
            if (adjAt100)
            {
                if (!isNaTISLast)
                {
                    _statusRejected = 500;
                    //violation = "N";
                }
                else
                    return FrameUpdateBLL.Verify_Return_Str1;
            }
            //dls 090624 - added this so that when the stuff that has no natis data is rejected, it will move on to Adjudication (will get resent to Natis there if they decide to prosecute)
            // otherwise the rejected stuff with no natis data keeps getting sent back to natis (_StatusVerified = 50!)
            //dls 2010-10-12 - need to stop everything from ending up in Adjudication
            else if (processType.Equals("Reject") && !isNaTISLast)
            {
                _statusVerified = 500;
                _statusRejected = 999;
            }
                
            _statusNatis = statusNatis;

            _PreUpdatedFrameRegNo = preUpdatedRegNo;
            _PreUpdatedRejReason = preUpdatedRejReason;
            _PreUpdatedAllowContinue = preUpdatedAllowContinue;

            _FrameIntNo = frameIntNo;

            if (!allowContinue)
                _AllowContinue = "N";

            int frameStatus = 0;

            if (process.Equals("ReturnToNatis"))
                frameStatus = _statusNatis;
            else if (processType.Equals("Reject"))
                frameStatus = _statusRejected;
            else if (processType.Equals("Investigate"))
            {
                frameStatus = statusInvestigate;
            }
            else
                frameStatus = _statusVerified;

            returnMessage = VerifyFrame(frameStatus, processType, rejReason, rejIntNo, vmIntNo, vtIntNo, registration, speed1, speed2, offenceDate, frameNo, userName, nRowVersion, violation);

            return returnMessage;
        }

        public string VerifyFrame(int status, string processType, string rejReason, int rejIntNo, int vmIntNo, int vtIntNo,
            string registration, int speed1, int speed2, DateTime offenceDate, string frameNo, string userName, Int64 nRowVersion, string violation)
        {
            RejectionEnum rejectionEnum = RejectionEnum.Init("en-US", userName);

            string returnMessage = "";

            if (processType.Equals("Reject")) 
                violation = "N";

            if (status == _statusVerified)
            {
                //dls 060606 - don't resend zeroes to natis
                if (!_PreUpdatedFrameRegNo.Equals(registration) && !registration.Equals("0000000000"))
                {
                    status = _statusNatis;
                }
            }

            if (rejReason.Equals(rejectionEnum.NONE.Key))
            {
                int regNo = 0;

                try
                {
                    regNo = Convert.ToInt32(registration);
                    if (regNo == 0 || registration.Trim().Length < 3)
                    {
                        returnMessage = FrameUpdateBLL.VerifyFrame_Return_Msg1;
                        return returnMessage;
                    }
                }
                catch { }
            }

            string errMessage = "";

            int updFrameIntNo = frame.VerifyFrame(_FrameIntNo, vmIntNo, vtIntNo, speed1, speed2, offenceDate,
                rejIntNo, registration, violation, _AllowContinue, _PreUpdatedAllowContinue, status, userName, 
                ref errMessage, nRowVersion);

            switch (updFrameIntNo)
            {
                case 0:
                    returnMessage = string.Format(FrameUpdateBLL.Unable_Update_Str, errMessage);
                    break;
                case -1:
                    returnMessage = FrameUpdateBLL.VerifyFrame_Return_Msg3;
                    break;
                case -2:
                    returnMessage = FrameUpdateBLL.VerifyFrame_Return_Msg4;
                    break;
                case -3:
                    returnMessage = FrameUpdateBLL.VerifyFrame_Return_Msg5;
                    break;
                case -4:
                    returnMessage = FrameUpdateBLL.VerifyFrame_Return_Msg6;
                    break;
                case -5:
                    returnMessage = FrameUpdateBLL.VerifyFrame_Return_Msg7;
                    break;
                case -6:
                    returnMessage = FrameUpdateBLL.VerifyFrame_Return_Msg8;
                    break;
                case -7:
                    returnMessage = string.Format(FrameUpdateBLL.VerifyFrame_Return_Msg9, speed1);
                    break;
                case -8:
                    returnMessage = FrameUpdateBLL.VerifyFrame_Return_Msg10;
                    break;
                case -9:
                    returnMessage = FrameUpdateBLL.VerifyFrame_Return_Msg11;
                    break;
                case -10:
                    returnMessage = FrameUpdateBLL.VerifyFrame_Return_Msg12;
                    break;
                case -11:
                    returnMessage = FrameUpdateBLL.VerifyFrame_Return_Msg13;
                    break;
                case -12:
                    returnMessage = FrameUpdateBLL.VerifyFrame_Return_Msg14;
                    break;
                case -13:
                    returnMessage = FrameUpdateBLL.VerifyFrame_Return_Msg15;
                    break;
                case -14:
                    returnMessage = FrameUpdateBLL.VerifyFrame_Return_Msg16;
                    break;
                default:
                    //returnMessage = "Frame " + frameNo + " has been updated";
                    this.frames.MarkProcessed(_FrameIntNo);

                    //dls 070604 - remove from the framelist so that they don't try to update it again
                    if (status == _statusNatis)
                        this.frames.HideFrame(_FrameIntNo);

                    break;
            }

            return returnMessage;
        }
    }
}