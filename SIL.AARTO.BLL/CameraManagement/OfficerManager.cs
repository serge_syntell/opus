﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Stalberg.TMS;
using System.Data;
using SIL.AARTO.BLL.Culture;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class OfficerManager
    {
        public static SelectList GetSelectList(int mtrIntNo)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            TrafficOfficerDB officerList = new TrafficOfficerDB(Config.ConnectionString);
            foreach (DataRow row in officerList.GetTrafficOfficerListDS(mtrIntNo).Tables[0].Rows)
            {
                list.Add(new SelectListItem() { Value = (row["TOIntNo"]).ToString(), Text = (string)row["TODescr"] });
            }
            return new SelectList(list, "Value", "Text");
        }
    }
}
