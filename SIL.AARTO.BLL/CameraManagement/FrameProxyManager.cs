﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;

namespace SIL.AARTO.BLL.CameraManagement
{
    public static class FrameProxyManager
    {
        private static FrameProxyDB frameProxyDB = new FrameProxyDB(Config.ConnectionString);
        public static FrameProxyDetails GetFrameProxyDetailsByFrame(int frameIntNo)
        {
            return frameProxyDB.GetFrameProxyDetailsByFrame(frameIntNo);
        }

        public static int UpdateFrameProxy(int frPrxIntNo, int frameIntNo, string frPrxSurname, string frPrxInitials,
                        string frPrxIDNumber, string frPrxPoAdd1, string frPrxPoAdd2, string frPrxPoAdd3,
                        string frPrxPoAdd4, string frPrxPoAdd5, string frPrxPoCode, string frPrxStAdd1,
                        string frPrxStAdd2, string frPrxStAdd3, string frPrxStAdd4,
                        string lastUser)
        {
            return frameProxyDB.UpdateFrameProxy(frPrxIntNo, frameIntNo, frPrxSurname, frPrxInitials,
                        frPrxIDNumber, frPrxPoAdd1, frPrxPoAdd2, frPrxPoAdd3,
                        frPrxPoAdd4, frPrxPoAdd5, frPrxPoCode, frPrxStAdd1,
                        frPrxStAdd2, frPrxStAdd3, frPrxStAdd4,
                        lastUser);
        }
    }
}
