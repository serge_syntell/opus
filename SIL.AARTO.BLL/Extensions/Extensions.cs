﻿using System;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using SIL.AARTO.DAL.Entities;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Extensions
{
    public static class Extensions
    {
        public static string Trim2(this string str, bool trimAll = false)
        {
            if (string.IsNullOrEmpty(str)) return str;
            if (!trimAll) return str.Trim();

            var result = new StringBuilder(str.Length);
            foreach (var c in str)
                if (!char.IsWhiteSpace(c))
                    result.Append(c);
            return result.ToString();
        }

        public static int Length2(this string str, int nullLength = -1)
        {
            return str == null ? nullLength : str.Length;
        }

        public static int Length2(this Array array, int nullLength = -1)
        {
            return array == null ? nullLength : array.Length;
        }

        public static bool AllEquals<T>(this T sample, params T[] args)
        {
            return sample == null
                ? args == null || args.All(arg => arg == null)
                : args != null && args.All(arg => sample.Equals2(arg));
        }

        public static void Absorbs<TSource, TNewSource>(this TSource source, TNewSource newSource,
            Func<PropertyInfo, object, PropertyInfo, object, bool> matching = null,
            bool onlyPublic = true)
            where TSource : class, TNewSource
            where TNewSource : class
        {
            if (source == null || newSource == null) return;
            foreach (var p2 in newSource.GetType().GetProperties(onlyPublic
                ? (BindingFlags.Public | BindingFlags.Instance)
                : (BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)))
            {
                try
                {
                    var p1 = source.GetType().GetProperty(p2.Name);
                    if (p1 == null || !p1.CanWrite) continue;
                    var v1 = p1.GetValue(source, null);
                    var v2 = p2.GetValue(newSource, null);
                    if (matching != null && !matching(p1, v1, p2, v2)) continue;
                    p1.SetValue(source, v2, null);
                }
                catch {}
            }
        }

        public static bool IsNullOrDBNull(this IDataRecord dr, string columnName)
        {
            return dr == null
                || string.IsNullOrWhiteSpace(columnName)
                || dr[columnName].IsNullOrDBNull();
        }

        public static bool IsNullOrDBNull(this DataRow dr, string columnName)
        {
            return dr == null
                || string.IsNullOrWhiteSpace(columnName)
                || dr[columnName].IsNullOrDBNull();
        }

        public static bool IsNullOrDBNull(this object obj)
        {
            return obj.In(DBNull.Value, null);
        }

        public static IHtmlString Raw(this string s)
        {
            return new HtmlString(s ?? "");
        }

        public static long RowVersionLong(this EntityBase source)
        {
            PropertyInfo rowVersion;
            if (source == null 
                || (rowVersion = source.GetType().GetProperty("RowVersion", BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase)) == null)
                return -1;
            return Convert.ToInt64(BitConverter.ToString((byte[])rowVersion.GetValue(source, null)).Replace("-", string.Empty), 16);
        }

        public static StringBuilder AppendLine2(this StringBuilder source, string format, params object[] args)
        {
            if (source == null) return source;
            source.AppendFormat(format, args);
            return source.Append(Environment.NewLine);
        }
    }
}
