﻿using System;

namespace SIL.AARTO.BLL.Extensions
{
    public class Lazy2<T>
    {
        readonly Func<T> create;
        readonly object syncObj = new object();
        T instance;

        public Lazy2(Func<T> valueFactory)
        {
            this.create = valueFactory;
        }

        public bool IsValueCreated { get; private set; }

        public T Value
        {
            get
            {
                if (!IsValueCreated)
                    lock (this.syncObj)
                        if (!IsValueCreated)
                        {
                            this.instance = this.create();
                            IsValueCreated = true;
                        }
                return this.instance;
            }
        }
    }
}
