﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace SIL.AARTO.BLL.Extensions
{
    public abstract class ReadyForUse : IDisposable
    {
        #region Const

        const string MustSelectProperty = "Must select a property.";
        const string SetValueFailed = "Unable to assign value to the constant property \"{0}\".";

        #endregion

        protected static readonly object SyncObj = new object();
        readonly List<KeyValue<string, int>> changedMembers = new List<KeyValue<string, int>>();
        bool isSettingConstant;

        public bool SaveNullValue { get; set; }

        public void Reset<T>(Expression<Func<T>> propertySelector)
        {
            CheckKeySelector(propertySelector);
            var name = ((MemberExpression)propertySelector.Body).Member.Name;
            lock (SyncObj)
                for (var i = 0; i < this.changedMembers.Count; i++)
                {
                    if (this.changedMembers[i].Key != name) continue;
                    this.changedMembers.RemoveAt(i);
                    return;
                }
        }

        public void Reset(Func<KeyValue<string, int>, bool> match)
        {
            lock (SyncObj)
                this.changedMembers.RemoveAll(m => match != null && match(m));
        }

        public void ResetAll()
        {
            lock (SyncObj)
                this.changedMembers.Clear();
        }

        public void ResetConstants()
        {
            lock (SyncObj)
                this.changedMembers.RemoveAll(m => m.Flag);
        }

        public void ResetInconstants()
        {
            lock (SyncObj)
                this.changedMembers.RemoveAll(m => !m.Flag);
        }

        public bool SetConstantValue<T>(Expression<Func<T>> propertySelector, T value)
        {
            CheckKeySelector(propertySelector);
            lock (SyncObj)
            {
                this.isSettingConstant = true;
                var result = SetPropertyValue(propertySelector, value);
                this.isSettingConstant = false;
                return result;
            }
        }

        public bool IsReady<T>(Expression<Func<T>> propertySelector)
        {
            CheckKeySelector(propertySelector);
            var name = ((MemberExpression)propertySelector.Body).Member.Name;
            lock (SyncObj)
                return this.changedMembers.Exists(m => m.Key == name);
        }

        public bool IsConstant<T>(Expression<Func<T>> propertySelector)
        {
            CheckKeySelector(propertySelector);
            var name = ((MemberExpression)propertySelector.Body).Member.Name;
            lock (SyncObj)
                return this.changedMembers.Exists(m => m.Flag && m.Key == name);
        }

        #region MUST BE IMPLEMENTED !!!

        protected abstract object LoadValue<T>(PropertyInfo pi, int type);

        #region On Trigger

        protected void OnTriggerChanged(int type)
        {
            lock (SyncObj)
                this.changedMembers.RemoveAll(m => !m.Flag && m.Value == type);
        }

        #endregion

        #region On Property

        protected T OnPropertyGetting<T>(Expression<Func<T>> propertySelector, int type)
        {
            CheckKeySelector(propertySelector);
            lock (SyncObj)
            {
                var pi = (PropertyInfo)((MemberExpression)propertySelector.Body).Member;
                return GetValue<T>(pi.Name, type, () => LoadValue<T>(pi, type));
            }
        }

        protected void OnPropertySetting<T>(Expression<Func<T>> propertySelector, int type, T value)
        {
            CheckKeySelector(propertySelector);
            lock (SyncObj)
                SetValue(((MemberExpression)propertySelector.Body).Member.Name, type, value);
        }

        #endregion

        #endregion

        #region Set / Get Value

        void SetValue<T>(string name, int type, T value)
        {
            var member = this.changedMembers.FirstOrDefault(m => m.Key == name);
            if (member == null)
                this.changedMembers.Add(new KeyValue<string, int>(name, type, this.isSettingConstant));
            else if (this.isSettingConstant || !member.Flag)
            {
                member.Value = type;
                member.Flag = this.isSettingConstant;
                member.Body = value;
            }
            else
                throw new ArgumentException(string.Format(SetValueFailed, name));
        }

        T GetValue<T>(string name, int type, Func<object> initValue)
        {
            var member = this.changedMembers.FirstOrDefault(m => m.Key == name);
            if (member == null)
            {
                member = new KeyValue<string, int>(name, type, false, initValue());
                if (member.Body != null || SaveNullValue)
                    this.changedMembers.Add(member);
            }
            return (T)member.Body;
        }

        bool SetPropertyValue<T>(Expression<Func<T>> propertySelector, T value)
        {
            var pi = (PropertyInfo)((MemberExpression)propertySelector.Body).Member;
            if (!pi.CanWrite) return false;
            pi.SetValue(this, value, null);
            return true;
        }

        #endregion

        #region Checkings

        static void CheckKeySelector<T>(Expression<Func<T>> propertySelector)
        {
            if (propertySelector == null)
                throw new ArgumentNullException("propertySelector");

            if (((MemberExpression)propertySelector.Body).Member.MemberType != MemberTypes.Property)
                throw new ArgumentException(MustSelectProperty, "propertySelector");
        }

        #endregion

        #region IDisposable

        bool isDisposed;

        public void Dispose()
        {
            Dispose(true);
            //GC.SuppressFinalize(this);
        }

        ~ReadyForUse()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (this.isDisposed) return;

            lock (SyncObj)
            {
                this.changedMembers.ForEach(m =>
                {
                    m.Key = null;
                    m.Body = null;
                });
                this.changedMembers.Clear();
                this.changedMembers.TrimExcess();
            }

            this.isDisposed = true;
        }

        #endregion
    }

    public class KeyValue<TKey, TValue>
    {
        public KeyValue() {}

        public KeyValue(TKey key, TValue value, bool flag = false, object body = null)
        {
            Key = key;
            Value = value;
            Flag = flag;
            Body = body;
        }

        public TKey Key { get; set; }
        public TValue Value { get; set; }
        public bool Flag { get; set; }
        public object Body { get; set; }
    }
}
