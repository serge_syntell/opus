﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml;
using System.IO;
using System.Data.SqlClient;
using System.Reflection;
using System.Xml.Serialization;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility;
using System.Globalization;

namespace SIL.AARTO.BLL.JudgementSnapshot
{
    public class JudgementSnapshotDB
    {
        // Fields
        private SqlConnection con;

        public JudgementSnapshotDB(string connectionString)
        {
            this.con = new SqlConnection(connectionString);
        }

        public bool SetSnapshot(int sumIntNo, bool lockSnapshot, string lastUser)
        {

            DataSet dsJudgementSnapshot = GetSnapshotDataFromDB(sumIntNo);

            if (dsJudgementSnapshot != null)
            {
                SortSnapshotDataSet(dsJudgementSnapshot);

                XmlDocument xmlDoc = ReadSnapshotToXML(dsJudgementSnapshot);

                if (xmlDoc != null && !String.IsNullOrEmpty(xmlDoc.InnerXml))
                {
                    if (CheckSnapshotIsCreated(sumIntNo))
                    {
                        return UpdateSummonsBeforeJudgementSnapshot(sumIntNo, false, xmlDoc.InnerXml, lastUser);
                    }
                    else
                    {
                        //Save xml to DB
                        return AddSummonsBeforeJudgementSnapshot(sumIntNo, lockSnapshot, xmlDoc.InnerXml, lastUser);
                    }
                }
            }
            return false;
        }

        public bool RollBackFromSnapshot(int sumIntNo, string lastUser)
        {

            XmlDocument xmlDoc = GetSnapshotFromTable(sumIntNo);

            if (xmlDoc != null && !String.IsNullOrEmpty(xmlDoc.InnerXml))
            {
                JudgementSnapshot snapShot = ReadSnapshotToEntity(xmlDoc);

                if (snapShot != null)
                {
                    //roll back operation here
                    try
                    {
                        RollBackFromSnapShot(snapShot, lastUser);

                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }


            }
            return false;
        }

        public bool CheckSnapshotIsCreated(int sumIntNo)
        {
            SummonsBeforeJudgementSnapshot snapshot = new SummonsBeforeJudgementSnapshotService().GetBySumIntNo(sumIntNo);
            if (snapshot != null && !String.IsNullOrEmpty(snapshot.SbjsSummonsDataXml))
            {
                return true;
            }
            return false;
        }

        // 2013-07-17 add parameter lastUser by Henry
        public bool SaveCourtDatesToSnapshot(int sumIntNo, DateTime newCourtDate, string lastUser)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                SummonsBeforeJudgementSnapshotService sbjsService = new SummonsBeforeJudgementSnapshotService();
                SummonsBeforeJudgementSnapshot snapshot = sbjsService.GetBySumIntNo(sumIntNo);

                if (snapshot != null)
                {
                    //doc = AddCourtDateToXml(sumIntNo, newCourtDate);

                    //if (!String.IsNullOrEmpty(doc.InnerXml))
                    //{
                    //snapshot.SbjsSummonsDataXml = doc.InnerXml;
                    snapshot.SbjsLocked = false;
                    snapshot.SumNewCourtDate = newCourtDate;

                    snapshot.LastUser = lastUser;
                    return sbjsService.Save(snapshot) != null;
                    //}
                }

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // 2013-07-17 add parameter lastUser by Henry
        public bool UnLockSnapshot(int sumIntNo, string lastUser)
        {
            try
            {
                SummonsBeforeJudgementSnapshotService sbjsService = new SummonsBeforeJudgementSnapshotService();
                SummonsBeforeJudgementSnapshot snapshot = sbjsService.GetBySumIntNo(sumIntNo);
                if (snapshot != null)
                {
                    snapshot.SbjsLocked = false;
                    snapshot.SumNewCourtDate = null;
                    snapshot.LastUser = lastUser;
                    sbjsService.Save(snapshot);

                    return snapshot != null;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool TestRollBack(int sumIntNo, out string errorMessage)
        {
            bool flag = true;
            errorMessage = string.Empty;

            SIL.AARTO.DAL.Entities.Summons summons = new SIL.AARTO.DAL.Services.SummonsService().GetBySumIntNo(sumIntNo);

            if (summons == null)
            {
                flag = false;
                errorMessage += "Summons does not exist" + "\r\n";
            }

            //if ((summons.SumRemandedSummonsFlag && !String.IsNullOrEmpty(summons.SumRemandedFromCaseNumber)) || String.IsNullOrEmpty(summons.SumCaseNo))       //did not generate case no
            //{
            //    flag = false;
            //    errorMessage += "Missing case number" + "</br>";
            //}

            //Jerry 2014-03-06 add summons.SummonsStatus == (int)ChargeStatusList.CaseNumberGenerated
            if (summons.SummonsStatus != (int)ChargeStatusList.SummonsNotServedWithdrawnOrPaid
                && (summons.SummonsStatus < (int)ChargeStatusList.CourtSystem
                    || summons.SummonsStatus > (int)ChargeStatusList.WarrantWithdrawalForSummonsReIssue
                    || summons.SummonsStatus == (int)ChargeStatusList.CaseNumberGenerated))
            {
                //Jake 2014-11-13 added , allow to reverse if status =710 and remanded from court date has value on summons table 
                if (!(summons.SummonsStatus == (int)ChargeStatusList.CaseNumberGenerated
                    && summons.SumRemandedFromCourtDate.HasValue))
                {
                    flag = false;
                    errorMessage += "This summons status [" + ((ChargeStatusList)summons.SummonsStatus).ToString() + "] is not eligible for reversal" + "\r\n";
                }
            }

            //Jerry 2014-03-05 check WOA whether already exists
            List<Woa> woaList = new WoaService().GetBySumIntNo(sumIntNo).Where(w => w.WoaCancel != true).ToList();
            if (woaList.Count > 0)
            {
                flag = false;
                errorMessage += "Unable to reverse, WOA[" + woaList[0].WoaNumber + "] has already been generated" + "\r\n";
            }

            //check rollback data
            SummonsBeforeJudgementSnapshot snapshot = new SummonsBeforeJudgementSnapshotService().GetBySumIntNo(sumIntNo);

            if (snapshot == null)
            {
                flag = false;
                errorMessage += "Missing rollback snapshot" + "\r\n";
            }
            else
            {
                if (snapshot.SbjsLocked)
                {
                    flag = false;
                    errorMessage += "Unable to reverse a judgement that has already been reversed" + "\r\n";
                }
            }
            // Jake 2014-03-04 add woa check, if there is a active woa exists , we can't reverse the judgement
            Woa woa = new WoaService().GetBySumIntNo(sumIntNo).Where(w => w.WoaCancel != true).OrderByDescending(w => w.WoaEdition).FirstOrDefault();
            if (woa != null)
            {
                flag = false;
                errorMessage += "Unable to reverse a judgement that there is a WOA exists" + "\r\n";
            }
            //if (snapshot != null && snapshot.SumIntNoNew.HasValue && snapshot.SumIntNoNew.Value > 0)
            //{
            //    SIL.AARTO.DAL.Entities.Summons summons_New = new SummonsService().GetBySumIntNo(snapshot.SumIntNoNew.Value);

            //    if (!String.IsNullOrEmpty(summons_New.SumCaseNo))
            //    {
            //        flag = false;
            //        errorMessage += "This summons has been Remanded(CJTCode 19) and new case number has been assigned" + "</br>";
            //    }
            //}


            // 2013-05-31 Jake comment it out 
            // In the way we have changed the handling of remanded cases – the original case number will still be on the summons, 
            // which means the rollback will never be able to proceed.
            //if (snapshot != null && summons.SumRemandedSummonsFlag && summons.SumCaseNo != null)
            //{
            //    flag = false;
            //    errorMessage += "This summons has been remanded to a new court date and a new case number has been assigned" + "</br>";
            //}
            if (flag == false)
            {
                return flag;
            }

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(snapshot.SbjsSummonsDataXml);

            JudgementSnapshot js = ReadSnapshotToEntity(doc);

            if (js.Notices != null && js.Notices.NoticeStatus == (int)ChargeStatusList.FullyPaid)
            {
            }
            else
            {
                SIL.AARTO.DAL.Data.ReceiptTranQuery rtQuery = new DAL.Data.ReceiptTranQuery();
                rtQuery.AppendInQuery(ReceiptTranColumn.ChgIntNo, String.Format(" SELECT ChgIntNo FROM dbo.Charge_SumCharge WHERE SChIntNo IN ( "
                    + " SELECT SChIntNo FROM dbo.SumCharge WHERE SumIntNo={0})", +summons.SumIntNo));

                TList<SIL.AARTO.DAL.Entities.ReceiptTran> receipTranList = new ReceiptTranService().Find(rtQuery as SIL.AARTO.DAL.Data.IFilterParameterCollection);

                if (receipTranList != null && receipTranList.Count > 0)
                {
                    if (receipTranList.Sum(r => r.RtAmount) > 0)
                    {
                        flag = false;
                        errorMessage += "A payment has already been made";
                    }
                }
            }

            //TList<SIL.AARTO.DAL.Entities.SumCharge> sumCharges = new SumChargeService().GetBySumIntNo(summons.SumIntNo);
            //if (sumCharges.Where(s => s.SumChargeStatus.Equals((int)ChargeStatusList.JudgementPaidAtCourt)
            //    //|| s.SumChargeStatus.Equals((int)ChargeStatusList.JudgementDeferredPayment)
            //    || s.SumChargeStatus.Equals((int)ChargeStatusList.JudgementPayment)).Count() > 0)
            //{
            //    flag = false;
            //    errorMessage += "A payment has already been made";
            //}
            //else
            //{
            //    List<SIL.AARTO.DAL.Entities.SumCharge> scList = sumCharges.Where(s => s.SumChargeStatus.Equals((int)ChargeStatusList.JudgementDeferredPayment)).ToList();
            //    if (scList != null && scList.Count > 0)
            //    {
            //        flag = false;
            //        errorMessage += "A deferred payment has already been made";
            //        //foreach(SIL.AARTO.DAL.Entities.Charge chg in new ChargeService().getby)
            //    }
            //}

            //any full or deferred payments


            return flag;
        }

        Charge GetFirstMainCharge(List<SIL.AARTO.BLL.JudgementSnapshot.Charge> chargeList)
        {
            Charge firstMainCharge = chargeList.Where(c => (c.ChargeStatus == 955 || c.ChargeStatus < 900) && c.ChgIsMain == true).OrderBy(c => c.ChgSequence).FirstOrDefault();

            return firstMainCharge;
        }

        #region DB Access

        public DataSet GetSummonsToRollback(int crtIntNo, int crtrIntNo, string summonsNo,
            int pageSize, int pageIndex, out int totalCount)    //2013-04-10 add by Henry for pagination
        {
            SqlCommand command = con.CreateCommand();
            command.CommandText = "GetSummonsAfterJudgement";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@CrtIntNo", SqlDbType.Int)).Value = crtIntNo;
            command.Parameters.Add(new SqlParameter("@CrtRIntNo", SqlDbType.Int)).Value = crtrIntNo;
            command.Parameters.Add(new SqlParameter("@SummonsNo", SqlDbType.VarChar, 50)).Value = summonsNo;

            command.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            command.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            command.Parameters.Add(paraTotalCount);

            DataSet dsReturn = new DataSet();

            SqlDataAdapter da = new SqlDataAdapter(command);

            try
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                da.Fill(dsReturn);

                //return command.ExecuteReader();
                totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

                return dsReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        bool AddSummonsBeforeJudgementSnapshot(int sumIntNo, bool locked, string snapShotXML, string lastUser)
        {

            SqlCommand command = con.CreateCommand();
            command.CommandText = "SummonsBeforeJudgementSnapshotAdd";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@SumIntNo", SqlDbType.Int)).Value = sumIntNo;
            command.Parameters.Add(new SqlParameter("@SBJSSummonsDataXML", SqlDbType.Xml)).Value = snapShotXML;
            command.Parameters.Add(new SqlParameter("@SBJSLocked", SqlDbType.Bit)).Value = locked;
            command.Parameters.Add(new SqlParameter("@LastUser", SqlDbType.NVarChar, 50)).Value = lastUser;

            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();

                int SBJSIntNo = Convert.ToInt32(command.ExecuteScalar());

                return SBJSIntNo > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        //2013-07-19 comment by Henry for useless function 
        bool UpdateSummonsBeforeJudgementSnapshot(int sumIntNo, bool lockSnapShot, string snapShotXML, string lastUser)
        {

            SqlCommand command = con.CreateCommand();
            command.CommandText = "SummonsBeforeJudgementSnapshotUpdate";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@SumIntNo", SqlDbType.Int)).Value = sumIntNo;
            command.Parameters.Add(new SqlParameter("@SBJSSummonsDataXML", SqlDbType.Xml)).Value = snapShotXML;
            command.Parameters.Add(new SqlParameter("@SBJSLocked", SqlDbType.Bit)).Value = lockSnapShot;
            command.Parameters.Add(new SqlParameter("@LastUser", SqlDbType.NVarChar, 50)).Value = lastUser;

            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();

                return Convert.ToInt32(command.ExecuteNonQuery()) > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        DataSet GetSnapshotDataFromDB(int sumIntNo)
        {
            DataSet dsReturn = new DataSet();

            SqlCommand command = con.CreateCommand();
            command.CommandText = "CreateJudgementSnapshot";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@SumIntNo", SqlDbType.Int)).Value = sumIntNo;

            SqlDataAdapter da = new SqlDataAdapter(command);

            try
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                da.Fill(dsReturn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }

            return dsReturn;
        }

        XmlDocument GetSnapshotFromTable(int sumIntNo)
        {
            XmlDocument document = new XmlDocument();
            //MemoryStream memoryStream = new MemoryStream();
            try
            {
                SummonsBeforeJudgementSnapshot snapshot = new SummonsBeforeJudgementSnapshotService().GetBySumIntNo(sumIntNo);
                if (snapshot != null && !String.IsNullOrEmpty(snapshot.SbjsSummonsDataXml))
                {
                    document.LoadXml(snapshot.SbjsSummonsDataXml);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return document;
        }

        XmlDocument AddCourtDateToXml(int sumIntNo, DateTime newCourtDate)
        {
            try
            {
                XmlDocument document = GetSnapshotFromTable(sumIntNo);

                SIL.AARTO.DAL.Entities.Summons summons = new SIL.AARTO.DAL.Services.SummonsService().GetBySumIntNo(sumIntNo);

                CourtDates courtDate = new CourtDates();
                courtDate.CDate = newCourtDate.Date;
                courtDate.CrtRIntNo = summons.CrtRintNo;
                courtDate.AutIntNo = summons.AutIntNo;

                ObjectSerializer.Serialize(document, courtDate);

                return document;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        bool RollBackFromSnapShot(JudgementSnapshot snapShot, string lastUser)
        {
            try
            {

                using (ConnectionScope.CreateTransaction())
                {
                    SummonsBeforeJudgementSnapshot snapShotDB = new SummonsBeforeJudgementSnapshotService().GetBySumIntNo(snapShot.Summons.SumIntNo);

                    //Roall back Summons
                    ProcessSummons(snapShot.Summons, lastUser);

                    //roll back notice
                    ProcessNotice(snapShot.Notices, lastUser);

                    //roll back notice_summons
                    ProcessNoticeSummons(snapShotDB, snapShot.NoticeSummons, lastUser);

                    //roll back sumcharge
                    ProcessSumCharge(snapShot.SumCharges);

                    //roll back charge_sumcharge
                    ProcessChargeSumCharge(snapShotDB, snapShot.ChargeSumCharges);

                    //roll back charge
                    ProcessCharge(snapShot.Charges, snapShot.Summons.SumIntNo, lastUser);

                    //roll back summons_charge
                    // ProcessSummonsCharge(snapShotDB, snapShot.SummonsCharges);

                    //Process CourtDates
                    //if (snapShot.CourtDates != null && snapShotDB.SumIntNoNew.HasValue)
                    if (snapShotDB.SumNewCourtDate.HasValue)
                        ProcessCourtDate(snapShotDB, snapShotDB, lastUser);

                    //Jerry 2014-03-05 don't rollback woa
                    //Process WOA
                    //ProcessWoa(snapShot.WOAs, snapShot.Charges, snapShot.Summons.SumIntNo, lastUser);

                    snapShotDB.SbjsLocked = true;

                    snapShotDB.LastUser = lastUser;
                    new SummonsBeforeJudgementSnapshotService().Save(snapShotDB);

                    ConnectionScope.Complete();

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessSummons(SIL.AARTO.BLL.JudgementSnapshot.Summons s, string supervisorName)
        {
            try
            {
                SummonsService sService = new SummonsService();
                SIL.AARTO.DAL.Entities.Summons summons = sService.GetBySumIntNo(s.SumIntNo);
                summons.SumStatus = s.SumStatus;
                summons.SumChargeStatus = s.SumChargeStatus;
                summons.SumLocked = s.SumLocked;
                summons.SumCancelled = s.SumCancelled;
                summons.SumDateCancelled = s.SumDateCancelled;
                summons.SumReasonCancelled = String.IsNullOrEmpty(s.SumReasonCancelled) ? "" : s.SumReasonCancelled;
                summons.SummonsStatus = s.SummonsStatus;
                summons.SumPaidDate = s.SumPaidDate;
                summons.SumCaseNo = s.SumCaseNo;
                summons.SumCourtDate = s.SumCourtDate;
                summons.SumPrevSummonsStatus = s.SumPrevSummonsStatus;
                summons.SupervisorActionDate = DateTime.Now;
                summons.SupervisorName = supervisorName;

                summons.SumRemandedFromCaseNumber = null;
                summons.SumRemandedFromCourtDate = null;
                summons.SumRemandedSummonsFlag = false;

                // 2013-07-17 add LastUser by Henry
                summons.LastUser = supervisorName;
                sService.Save(summons);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // 2013-07-17 add parameter lastUser by Henry
        void ProcessNotice(SIL.AARTO.BLL.JudgementSnapshot.Notice n, string lastUser)
        {
            try
            {
                NoticeService nService = new NoticeService();
                SIL.AARTO.DAL.Entities.Notice notice = nService.GetByNotIntNo(n.NotIntNo);
                notice.NoticeStatus = n.NoticeStatus;
                notice.NotPrevNoticeStatus = n.NotPrevNoticeStatus;
                notice.LastUser = lastUser;
                nService.Save(notice);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // 2013-07-17 add parameter lastUser by Henry
        void ProcessNoticeSummons(SummonsBeforeJudgementSnapshot snapShot, SIL.AARTO.BLL.JudgementSnapshot.Notice_Summons notice_summons, string lastUser)
        {
            try
            {
                SIL.AARTO.DAL.Services.NoticeSummonsService nsService = new NoticeSummonsService();
                SIL.AARTO.DAL.Services.NoticeSummonsCancelledService nscService = new NoticeSummonsCancelledService();
                // Delete Notice_Summons row , this summons created by judgement
                if (snapShot.SumIntNoNew.HasValue && snapShot.SumIntNoNew.Value > 0)
                {
                    nsService.Delete(nsService.GetBySumIntNo(notice_summons.SumIntNo));

                    NoticeSummonsCancelled nsc = new NoticeSummonsCancelled();
                    nsc.NotIntNo = notice_summons.NotIntNo;
                    nsc.SumIntNo = snapShot.SumIntNoNew.Value;
                    nsc.LastUser = lastUser;
                    nscService.Save(nsc);
                }

                TList<SIL.AARTO.DAL.Entities.NoticeSummons> nsList = nsService.GetByNotIntNo(notice_summons.NotIntNo);
                nsService.Delete(nsList);

                //SIL.AARTO.DAL.Entities.NoticeSummons noticeSummons = nsService.GetByNotSumIntNo(notice_summons.NotSumIntNo);
                //if (noticeSummons == null)
                //{
                NoticeSummons noticeSummons = new NoticeSummons();
                //}
                noticeSummons.NotIntNo = notice_summons.NotIntNo;
                noticeSummons.SumIntNo = notice_summons.SumIntNo;
                noticeSummons.LastUser = notice_summons.LastUser;

                nsService.Save(noticeSummons);

                TList<NoticeSummonsCancelled> nscList = nscService.GetBySumIntNo(noticeSummons.SumIntNo);
                if (nscList != null && nscList.Count > 0)
                {
                    nscService.Delete(nscList);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessAccused(SIL.AARTO.BLL.JudgementSnapshot.Accused accused)
        {
            try { }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessSumCharge(List<SIL.AARTO.BLL.JudgementSnapshot.SumCharge> scList)
        {
            try
            {
                SumChargeService scService = new SumChargeService();

                foreach (SIL.AARTO.BLL.JudgementSnapshot.SumCharge sc in scList)
                {
                    SIL.AARTO.DAL.Entities.SumCharge sumCharge = scService.GetBySchIntNo(sc.SChIntNo);

                    if (sumCharge == null)
                    {
                        sumCharge = new DAL.Entities.SumCharge();
                    }

                    sumCharge.SchIntNo = sc.SChIntNo;
                    sumCharge.SumIntNo = sc.SumIntNo;
                    sumCharge.SchStatRef = sc.SChStatRef;
                    sumCharge.SchNumber = sc.SChNumber;
                    sumCharge.SchCode = sc.SChCode;
                    sumCharge.SchDescr = sc.SChDescr;
                    sumCharge.SchOcTdescr = sc.SChOcTDescr;
                    sumCharge.SchOctDescr1 = sc.SChOctDescr1;
                    sumCharge.SchFineAmount = sc.SChFineAmount;
                    sumCharge.SchRevAmount = sc.SChRevAmount;
                    sumCharge.SchCourtJudgementLoaded = sc.SChCourtJudgementLoaded;
                    sumCharge.SchSentenceAmount = sc.SChSentenceAmount;
                    sumCharge.SchContemptAmount = sc.SChContemptAmount;
                    sumCharge.SchOtherAmount = sc.SChOtherAmount;
                    sumCharge.SchPaidAmount = sc.SChPaidAmount;
                    sumCharge.CjtIntNo = sc.CJTIntNo;
                    sumCharge.SchVerdict = sc.SChVerdict;
                    sumCharge.SchSentence = sc.SChSentence;
                    sumCharge.SchNoAog = sc.SChNoAOG;
                    sumCharge.SchWoaLoaded = sc.SChWOALoaded;
                    sumCharge.LastUser = sc.LastUser;
                    sumCharge.GraceExpiryDate = sc.GraceExpiryDate;
                    sumCharge.SchDescrAfr = sc.SChDescrAfr;
                    sumCharge.SchIsMain = sc.SchIsMain;
                    sumCharge.SchSequence = (byte)sc.SChSequence;
                    sumCharge.SumChargeStatus = sc.SumChargeStatus;
                    sumCharge.SchPrevSumChargeStatus = sc.SChPrevSumChargeStatus;
                    sumCharge.MainSumChargeId = sc.MainSumChargeID;
                    sumCharge.SchType = sc.SChType;


                    scService.Save(sumCharge);

                    TList<ScDeferredPayments> payments = new ScDeferredPaymentsService().GetBySchIntNo(sumCharge.SchIntNo);

                    if (payments != null && payments.Count > 0)
                    {
                        new ScDeferredPaymentsService().Delete(payments);
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessChargeSumCharge(SummonsBeforeJudgementSnapshot snapShot, List<SIL.AARTO.BLL.JudgementSnapshot.Charge_SumCharge> cscList)
        {
            try
            {
                ChargeSumChargeService cscService = new ChargeSumChargeService();
                SumChargeService scService = new SumChargeService();
                if (snapShot.SumIntNoNew.HasValue && snapShot.SumIntNoNew.Value > 0)
                {
                    TList<SIL.AARTO.DAL.Entities.SumCharge> sumCharges = scService.GetBySumIntNo(snapShot.SumIntNoNew.Value);

                    foreach (SIL.AARTO.DAL.Entities.SumCharge sc in sumCharges)
                    {
                        // Delete Charge_SumCharge row , this Charge_SumCharge created by judgement
                        cscService.Delete(cscService.GetBySchIntNo(sc.SchIntNo));
                    }
                }

                //roll back Charge_SumChage row
                SIL.AARTO.DAL.Entities.ChargeSumCharge csCharge = null;
                foreach (SIL.AARTO.BLL.JudgementSnapshot.Charge_SumCharge csc in cscList)
                {
                    //csCharge = cscService.GetByChgSchIntNo(csc.ChgSChIntNo);
                    csCharge = cscService.GetByChgIntNo(csc.ChgIntNo).FirstOrDefault(s => s.SchIntNo.Equals(csc.SChIntNo));
                    if (csCharge == null)
                    {
                        csCharge = new ChargeSumCharge();
                    }
                    csCharge.ChgIntNo = csc.ChgIntNo;
                    //csCharge.ChgSchIntNo = csc.ChgSChIntNo;
                    csCharge.SchIntNo = csc.SChIntNo;
                    csCharge.LastUser = csc.LastUser;

                    cscService.Save(csCharge);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessCharge(List<SIL.AARTO.BLL.JudgementSnapshot.Charge> chargeList, int sumIntNo, string lastUser)
        {
            try
            {
                ChargeService cService = new ChargeService();
                foreach (SIL.AARTO.BLL.JudgementSnapshot.Charge c in chargeList)
                {
                    SIL.AARTO.DAL.Entities.Charge charge = cService.GetByChgIntNo(c.ChgIntNo);
                    if (charge == null)
                    {
                        charge = new DAL.Entities.Charge();
                    }

                    charge.NotIntNo = c.NotIntNo;
                    charge.CtIntNo = c.CTIntNo;
                    charge.ChgOffenceType = c.ChgOffenceType;
                    charge.ChgOffenceCode = c.ChgOffenceCode;
                    charge.ChgFineAmount = c.ChgFineAmount;
                    charge.ChgFineAlloc = c.ChgFineAlloc;
                    charge.LastUser = c.LastUser;
                    charge.ChgStatutoryRef = c.ChgStatutoryRef ?? "";
                    charge.ChgStatRefLine1 = c.ChgStatRefLine1 ?? "";
                    charge.ChgStatRefLine2 = c.ChgStatRefLine2 ?? "";
                    charge.ChgStatRefLine3 = c.ChgStatRefLine3 ?? "";
                    charge.ChgStatRefLine4 = c.ChgStatRefLine4 ?? "";
                    charge.ChgOffenceDescr = c.ChgOffenceDescr ?? "";
                    charge.ChgOffenceDescr2 = c.ChgOffenceDescr2 ?? "";
                    charge.ChgOffenceDescr3 = c.ChgOffenceDescr3 ?? "";
                    charge.ChargeStatus = c.ChargeStatus;
                    charge.ChgPaidDate = c.ChgPaidDate;
                    charge.ChgNoAog = c.ChgNoAOG;
                    charge.ChgRevFineAmount = (float)c.ChgRevFineAmount;
                    charge.ChgRevDiscountAmount = (float)c.ChgRevDiscountAmount;
                    charge.ChgRevDiscountedAmount = (float)c.ChgRevDiscountedAmount;
                    charge.PreviousStatus = c.PreviousStatus;
                    charge.ChgContemptCourt = c.ChgContemptCourt;
                    charge.ChgIssueAuthority = c.ChgIssueAuthority;
                    charge.ChgOfficerNatisNo = c.ChgOfficerNatisNo;
                    charge.ChgOfficerName = c.ChgOfficerName;
                    charge.ChgDiscountAmount = c.ChgDiscountAmount;
                    charge.ChgDiscountedAmount = c.ChgDiscountedAmount;
                    charge.ChgType = c.ChgType;
                    charge.ChgDemeritPoints = c.ChgDemeritPoints;
                    charge.ChgLegislation = c.ChgLegislation;
                    charge.ChgOffenceTypeOld = c.ChgOffenceTypeOld;
                    charge.ChgIsMain = c.ChgIsMain;
                    charge.ChgSequence = (byte)c.ChgSequence;
                    charge.MainChargeId = c.MainChargeID;

                    cService.Save(charge);

                    /* Jake 2013-11-26 comment out, because we have added WOA in snapshot
                    if (charge.ChgIsMain && charge.ChgSequence == 1)
                    {
                        //“Judgement Reversed by “ + SupervisorName + “ on “ + SupervisorActionDate.
                        InsertEvidencePack(charge.ChgIntNo, DateTime.Now, String.Format("Judgement reversed by {0} on {1}", lastUser, DateTime.Now.ToString("yyyy/MM/dd")), "Summons", sumIntNo, lastUser, (int)EpActionTypeList.Others);

                        //Jake 2013-10-24 modified 
                        // added double WOA process here 
                        WoaService woaService = new WoaService();
                        // Jake 2013-03-04 add IsCurrent column for section 72 document
                        TList<Woa> woas = woaService.GetBySumIntNo(sumIntNo);
                        Woa woa = woas.Where(w => w.IsCurrent == true && w.WoaCancel != true).OrderByDescending(w => w.WoaEdition).FirstOrDefault();

                        //if WOA has already been cancelled for some or other reason - do not cancel it again!
                        if (woa != null && woa.WoaCancel != true)
                        {
                            if (woa.WoaType == "D")
                            {
                                Woa previousWoa = woas.Where(w => w.WoaIntNo != woa.WoaIntNo && w.WoaCancel != true).OrderByDescending(w => w.WoaEdition).FirstOrDefault();
                                if (previousWoa.WoaIntNo != woa.WoaIntNo)
                                {
                                    if (previousWoa.WoaCancel != null && previousWoa.WoaCancel == true)
                                    {
                                        previousWoa.WoaCancel = false;
                                        previousWoa.WoaCancelDate = null;

                                        woaService.Save(previousWoa);
                                    }
                                }

                            }

                            woa.WoaCancel = true;
                            woa.WoaCancelDate = DateTime.Now;

                            InsertEvidencePack(charge.ChgIntNo, DateTime.Now, String.Format("WOA judgement reversed by {0} on {1}", lastUser, DateTime.Now.ToString("yyyy/MM/dd")), "WOA", woa.WoaIntNo, lastUser, (int)EpActionTypeList.Others);

                            // 2013-07-17 add LastUser by Henry
                            woa.LastUser = lastUser;
                            woaService.Save(woa);
                        }
                    }
                     * */

                }

                Charge firstMainCharge = GetFirstMainCharge(chargeList);// chargeList.Where(c => c.ChargeStatus < 900 && c.ChgIsMain == true).OrderBy(c => c.ChgSequence).FirstOrDefault();
                if (firstMainCharge != null)
                {
                    //“Judgement Reversed by “ + SupervisorName + “ on “ + SupervisorActionDate.
                    InsertEvidencePack(firstMainCharge.ChgIntNo, DateTime.Now, String.Format("Judgement reversed by {0} on {1}", lastUser, DateTime.Now.ToString("yyyy/MM/dd")), "Summons", sumIntNo, lastUser);

                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Jake 2014-02-25 modified
        // If when we create snapshot, there is no woa in snapshot and then do a judgement and create a WOA ,then goto reverse judgement page.
        // We need to cancel the woa in DB if no WOA in snapshot.
        void ProcessWoa(List<WOA> woas, List<SIL.AARTO.BLL.JudgementSnapshot.Charge> chargeList, int sumIntNo, string lastUser)
        {

            WoaService woaService = new WoaService();

            //Jerry 2014-04-01 check WOACancel != true
            //TList<Woa> woasInDB = woaService.GetBySumIntNo(sumIntNo);
            List<Woa> woasInDB = woaService.GetBySumIntNo(sumIntNo).Where(w => w.WoaCancel != true).ToList();

            if (woas != null)
            {
                if (woas.Count < woasInDB.Count)
                {
                    var woaq = from d in woas
                               select new { WoaIntNo = d.WOAIntNo };

                    foreach (Woa w in woasInDB)
                    {
                        Charge chg = GetFirstMainCharge(chargeList);

                        if (woaq.Where(q => q.WoaIntNo.Equals(w.WoaIntNo)).Count() == 0)
                        {
                            w.WoaCancel = true;
                            w.WoaCancelDate = DateTime.Now;
                            w.LastUser = lastUser;
                            woaService.Save(w);

                            if (chg != null)
                                InsertEvidencePack(chg.ChgIntNo, DateTime.Now, String.Format("WOA judgement reversed by {0} on {1}", lastUser, DateTime.Now.ToString("yyyy/MM/dd")), "WOA", w.WoaIntNo, lastUser);
                        }
                    }
                }
                foreach (WOA woa in woas)
                {
                    Woa woaDB = woaService.GetByWoaIntNo(woa.WOAIntNo);
                    if (woaDB != null)
                    {
                        woaDB.WoaIntNo = woa.WOAIntNo;
                        woaDB.SumIntNo = woa.SumIntNo;
                        woaDB.WoaNumber = woa.WOANumber;
                        woaDB.WoaLoadDate = woa.WOALoadDate;
                        woaDB.WoaIssueDate = woa.WOAIssueDate;
                        woaDB.WoaPrintDate = woa.WOAPrintDate;
                        woaDB.WoaPrintFileName = woa.WOAPrintFileName;
                        woaDB.WoaFineAmount = woa.WOAFineAmount;
                        woaDB.WoaAddAmount = woa.WOAAddAmount;
                        woaDB.WoaReceiptNo = woa.WOAReceiptNo;
                        woaDB.WoaPaidAmount = woa.WOAPaidAmount;
                        woaDB.LastUser = woa.LastUser;
                        woaDB.WoaGeneratedDate = woa.WOAGeneratedDate;
                        woaDB.WoaPaidDate = woa.WOAPaidDate;
                        woaDB.WoaExpireDate = woa.WOAExpireDate;
                        woaDB.WoaChargeStatus = woa.WOAChargeStatus;
                        woaDB.WoaAuthorised = woa.WOAAuthorised;
                        woaDB.WoaCancel = woa.WOACancel;
                        woaDB.WoaCancelDate = woa.WOACancelDate;
                        woaDB.WoaBookOutToOfficerDate = woa.WOABookOutToOfficerDate;
                        woaDB.WoaServedStatus = woa.WOAServedStatus;
                        woaDB.WoaOfficerNumber = woa.WOAOfficerNumber;
                        woaDB.WoaNewCourtDate = woa.WOANewCourtDate;
                        woaDB.NotIntNo = woa.NotIntNo;
                        woaDB.Woas72ReturnDate = woa.WOAS72ReturnDate;
                        woaDB.WoaChargePrevStatus = woa.WOAChargePrevStatus;
                        woaDB.IsCurrent = woa.IsCurrent;
                        woaDB.WoaSentToCourtDate = woa.WOASentToCourtDate;
                        woaDB.WoaReturnedFromCourtDate = woa.WOAReturnedFromCourtDate;
                        woaDB.WoaRejectedReason = woa.WOARejectedReason;
                        woaDB.WoaEdition = woa.WOAEdition;
                        woaDB.IsArrested = woa.IsArrested;
                        woaDB.WoaType = woa.WOAType;

                        woaService.Save(woaDB);
                    }
                }
            }
            else
            {
                if (woasInDB != null && woasInDB.Count > 0)
                {
                    // we need to cancel all woas in db
                    foreach (SIL.AARTO.DAL.Entities.Woa w in woasInDB)
                    {
                        w.WoaCancel = true;
                        w.WoaCancelDate = DateTime.Now;
                        w.LastUser = lastUser;

                        woaService.Save(w);
                    }
                    //woaService.Save(woasInDB);
                }
            }
        }


        void ProcessSummonsCharge(SummonsBeforeJudgementSnapshot snapShot, List<SIL.AARTO.BLL.JudgementSnapshot.Summons_Charge> scList)
        {
            try
            {
                SummonsChargeService scService = new SummonsChargeService();
                SIL.AARTO.DAL.Services.NoticeSummonsService nsService = new NoticeSummonsService();

                // Delete Notice_Summons row , this summons created by judgement
                if (snapShot.SumIntNoNew.HasValue && snapShot.SumIntNoNew.Value > 0)
                {
                    scService.Delete(scService.GetBySumIntNo(snapShot.SumIntNoNew.Value));
                }

                foreach (SIL.AARTO.BLL.JudgementSnapshot.Summons_Charge summonsCharge in scList)
                {
                    SIL.AARTO.DAL.Entities.SummonsCharge sCharge = scService.GetByScIntNo(summonsCharge.SCIntNo);
                    if (sCharge == null)
                    {
                        sCharge = new SummonsCharge();
                    }

                    sCharge.SumIntNo = summonsCharge.SumIntNo;
                    sCharge.ChgIntNo = summonsCharge.ChgIntNo;
                    sCharge.LastUser = summonsCharge.LastUser;

                    scService.Save(sCharge);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // 2013-07-17 add parameter lastUser by Henry
        void ProcessCourtDate(SummonsBeforeJudgementSnapshot snapShot, SIL.AARTO.DAL.Entities.SummonsBeforeJudgementSnapshot snapshot, string lastUser)
        {
            try
            {
                //if (snapShot.SumIntNoNew.HasValue && snapShot.SumIntNoNew.Value > 0)
                //{
                CourtDatesService cdService = new CourtDatesService();
                SIL.AARTO.DAL.Entities.Summons summons = new SummonsService().GetBySumIntNo(snapShot.SumIntNo);

                SIL.AARTO.DAL.Entities.CourtDates courtDates = cdService.GetByCdateCrtRintNoAutIntNo(snapshot.SumNewCourtDate.Value, summons.CrtRintNo.Value, summons.AutIntNo);
                if (courtDates != null)
                {
                    switch (summons.SumType.ToUpper().Trim())
                    {
                        case "S54":
                            courtDates.Cds54Allocated = (short)((courtDates.Cds54Allocated - 1) > 0 ? (courtDates.Cds54Allocated - 1) : 0);
                            break;
                        case "S56":
                            courtDates.Cds56Allocated = (short)((courtDates.Cds56Allocated - 1) > 0 ? (courtDates.Cds56Allocated - 1) : 0);
                            break;
                    }
                    courtDates.LastUser = lastUser;
                    cdService.Save(courtDates);
                }
                // }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void InsertEvidencePack(int chgIntNo, DateTime date, string descr, string sourceTable, int sourceId, string lastUser, int epatIntNo = (int)EpActionTypeList.Others)
        {
            try
            {
                EvidencePack evidencePack = new EvidencePack();
                evidencePack.ChgIntNo = chgIntNo;
                evidencePack.EpItemDate = date;
                evidencePack.EpItemDescr = descr;
                evidencePack.EpSourceId = sourceId;
                evidencePack.EpSourceTable = sourceTable;
                evidencePack.EpItemDateUpdated = true;
                evidencePack.LastUser = lastUser;
                // Jake 2013-07-02 added
                evidencePack.EpTransactionDate = DateTime.Now;
                evidencePack.EpatIntNo = epatIntNo;//Jerry 2013-10-15 add
                new EvidencePackService().Save(evidencePack);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessSummonsBail(List<Summons_Bail> summonsBails, Summons summons)
        {
            SummonsBailService summonsBailService = new SummonsBailService();

            TList<SummonsBail> summonsBailsDB = summonsBailService.GetBySumIntNo(summons.SumIntNo);

            summonsBailService.Delete(summonsBailsDB);

            if (summonsBails != null && summonsBails.Count > 0)
            {
                foreach (Summons_Bail sb in summonsBails)
                {
                    TList<SummonsBail> bails = new TList<SummonsBail>();
                    bails.Add(new SummonsBail()
                    {
                        SumIntNo = sb.SumIntNo,
                        CrtIntNoTrFrom = sb.CrtIntNoTrFrom,
                        CrtIntNoTrTo = sb.CrtIntNoTrTo,
                        SuBaAmount = (float?)sb.SuBaAmount,
                        SuBaReceiptNumber = sb.SuBaReceiptNumber,
                        WoaArrestedIntNo = sb.WOAArrestedIntNo,
                        SuBaDatePaid = sb.SuBaDatePaid,
                        LastUser = sb.LastUser
                    });

                    summonsBailService.Save(bails);
                }
            }

        }
        #endregion

        void SortSnapshotDataSet(DataSet ds)
        {
            try
            {
                ds.DataSetName = "JudgementSnapshot";

                for (int index = 0; index < ds.Tables.Count; index++)
                {
                    switch (index)
                    {
                        case 0:
                            ds.Tables[index].TableName = "Summons"; break;
                        case 1:
                            ds.Tables[index].TableName = "Notice"; break;
                        case 2:
                            ds.Tables[index].TableName = "Notice_Summons"; break;
                        case 3:
                            ds.Tables[index].TableName = "SumCharge"; break;
                        case 4:
                            ds.Tables[index].TableName = "Charge_SumCharge"; break;
                        case 5:
                            ds.Tables[index].TableName = "Charge"; break;
                        case 6:
                            ds.Tables[index].TableName = "Summons_Charge"; break;
                        case 7:
                            ds.Tables[index].TableName = "WOA"; break;
                        case 8:
                            ds.Tables[index].TableName = "Summons_Bail"; break;

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        XmlDocument ReadSnapshotToXML(DataSet ds)
        {
            XmlDocument document = new XmlDocument();
            MemoryStream memoryStream = new MemoryStream();
            try
            {
                ds.WriteXml(memoryStream, XmlWriteMode.IgnoreSchema);
                memoryStream.Position = 0;

                document.Load(memoryStream);

                memoryStream.Close();
                memoryStream.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return document;
        }

        JudgementSnapshot ReadSnapshotToEntity(XmlDocument document)
        {
            JudgementSnapshot snapShot = new JudgementSnapshot();

            XmlNode root = document.FirstChild;

            foreach (XmlNode node in root.ChildNodes)
            {
                object obj = null;
                switch (node.Name)
                {
                    case "Summons": obj = new Summons(); break;
                    case "Notice": obj = new Notice(); break;
                    case "Notice_Summons": obj = new Notice_Summons(); break;
                    case "Charge": obj = new Charge(); break;
                    case "Charge_SumCharge": obj = new Charge_SumCharge(); break;
                    case "SumCharge": obj = new SumCharge(); break;
                    case "Summons_Charge": obj = new Summons_Charge(); break;
                    case "CourtDates": obj = new CourtDates(); break;
                    case "WOA": obj = new WOA(); break;
                    case "Summons_Bail": obj = new Summons_Bail(); break;
                }

                if (obj != null)
                {
                    PropertyInfo proInfo = null; object value = null; Type type = null;
                    foreach (XmlNode valueNode in node.ChildNodes)
                    {
                        proInfo = obj.GetType().GetProperty(valueNode.Name);
                        if (proInfo != null)
                        {

                            type = proInfo.PropertyType;

                            if (proInfo.PropertyType.IsGenericType && proInfo.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                            {
                                type = proInfo.PropertyType.GetGenericArguments()[0];
                                switch (type.FullName)
                                {
                                    case "System.DateTime":
                                        if (!String.IsNullOrEmpty(valueNode.InnerText)) value = DateTime.Parse(valueNode.InnerText);
                                        break;
                                    default:
                                        value = valueNode.InnerText.Trim();
                                        break;
                                }

                            }
                            else
                            {
                                value = String.IsNullOrEmpty(valueNode.InnerText) == true ? null : valueNode.InnerText.Trim();
                            }

                            proInfo.SetValue(obj, System.Convert.ChangeType(value, type, CultureInfo.InvariantCulture), null);
                        }
                    }


                    switch (obj.GetType().Name)
                    {
                        case "Summons": snapShot.Summons = obj as Summons; break;
                        case "Notice": snapShot.Notices = obj as Notice; break;
                        case "Notice_Summons": snapShot.NoticeSummons = obj as Notice_Summons; break;
                        case "Charge": snapShot.AddCharge(obj as Charge); break;
                        case "Charge_SumCharge": snapShot.AddChargeSumCharge(obj as Charge_SumCharge); break;
                        case "SumCharge": snapShot.AddSumCharge(obj as SumCharge); break;
                        case "Summons_Charge": snapShot.AddSummmonsCharge(obj as Summons_Charge); break;
                        case "CourtDates": snapShot.CourtDates = obj as CourtDates; break;
                        case "WOA": snapShot.AddWOA(obj as WOA); break;
                        case "Summons_Bail": snapShot.AddSummonsBail(obj as Summons_Bail); break;
                    }
                }

            }

            return snapShot;
        }

        XmlDocument SerialJudgementSnapshot(JudgementSnapshot snapShot)
        {
            XmlDocument doc = new XmlDocument();
            XmlElement element = doc.CreateElement("JudgementSnapshot");

            XmlNode root = doc.AppendChild(element);

            //XmlSerializer xs = new XmlSerializer(snapShot.GetType());

            foreach (PropertyInfo pInfo in snapShot.GetType().GetProperties())
            {
                switch (pInfo.Name)
                {
                    case "Summons":
                        ObjectSerializer.Serialize(doc, snapShot.Summons);
                        break;
                    case "Notices":
                        ObjectSerializer.Serialize(doc, snapShot.Notices);
                        break;
                    case "Charges":
                        ObjectSerializer.Serialize<Charge>(doc, snapShot.Charges);
                        break;
                    case "ChargeSumCharges":
                        ObjectSerializer.Serialize<Charge_SumCharge>(doc, snapShot.ChargeSumCharges);
                        break;
                    case "NoticeSummons":
                        ObjectSerializer.Serialize(doc, snapShot.NoticeSummons);
                        //Serialize<Notice_Summons>(doc, snapShot.NoticeSummons);
                        break;
                    case "SumCharges":
                        ObjectSerializer.Serialize<SumCharge>(doc, snapShot.SumCharges);
                        break;
                    case "CourtDates":
                        ObjectSerializer.Serialize(doc, snapShot.CourtDates);
                        break;
                    case "SummonsCharges":
                        ObjectSerializer.Serialize<Summons_Charge>(doc, snapShot.SummonsCharges);
                        break;
                    case "WOA":
                        ObjectSerializer.Serialize<WOA>(doc, snapShot.WOAs);
                        break;
                    case "Summons_Bail":
                        ObjectSerializer.Serialize<Summons_Bail>(doc, snapShot.SummonsBails);
                        break;
                }
            }

            return doc;
        }


    }
}
