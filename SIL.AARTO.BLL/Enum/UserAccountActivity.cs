﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Enum
{
    public enum UserAccountActivity
    {
        PasswordReset = 1,
        PasswordChanged = 2
    }
}
