﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.BookManagement
{
    public class BookS341 : Book
    {
        public override int BookType
        {
            get { return (int)AartobmBookTypeList.Section341; }
        }

        public override int BookSize
        {
            get
            {
                AartobmBookTypeService db = new AartobmBookTypeService();
                return (int)db.GetByAaBmBookTypeId(BookType).AaBmBookSize;
            }
        }
    }
}
