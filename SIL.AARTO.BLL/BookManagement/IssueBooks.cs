﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.PageElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using System.Configuration;
using ceTe.DynamicPDF;
using System.IO;
using System.Data;

namespace SIL.AARTO.BLL.BookManagement
{
    public class IssueBooks : BookProcessBase
    {
        

        public override List<AartobmBook> SearchBooks(int mtrIntNo, int depotIntNo, int? officerIntNo, int startNo, int endNo)
        {
            List<AartobmBook> returnList = new List<AartobmBook>();
            using (IDataReader reader = new AartobmBookService().SearchBooksForIssue(mtrIntNo, depotIntNo, startNo, endNo))
            {
                while (reader.Read())
                {
                    AartobmBook book = new AartobmBook();
                    book.AaBmBookId = (decimal)reader["AaBmBookId"];
                    book.AaBmBookNo = reader["AaBmBookNo"].ToString();
                    book.AaBmBookStartingNo = (int)reader["AaBmBookStartingNo"];
                    book.AaBmBookEndingNo = (int)reader["AaBmBookEndingNo"];
                    book.AaBmBookStatusId = (int)reader["AaBmBookStatusId"];
                    book.AaBmBookTypeId = (int)reader["AaBmBookTypeId"];
                    if (reader["AaBmDepotId"] != DBNull.Value)
                        book.AaBmDepotId = (int)reader["AaBmDepotId"];
                    if (reader["MtrIntNo"] != DBNull.Value)
                        book.MtrIntNo = (int)reader["MtrIntNo"];
                    if (reader["ToIntNo"] != DBNull.Value)
                        book.ToIntNo = (int)reader["ToIntNo"];
                    book.LastUser = reader["LastUser"].ToString();
                    if (reader["AaBMBookTypeID"] != DBNull.Value)
                    {
                        book.AaBmBookTypeId = Convert.ToInt32(reader["AaBMBookTypeID"]);
                        book.AaBmBookTypeIdSource = new AartobmBookType();
                        book.AaBmBookTypeIdSource.AaBmBookTypeName = reader["AaBmBookTypeName"].ToString();
                    }
                    if (reader["NPHIntNo"] != DBNull.Value)
                    {
                        book.NphIntNo = Convert.ToInt32(reader["NPHIntNo"]);
                        book.NphIntNoSource = new NoticePrefixHistory();
                        book.NphIntNoSource.Nprefix = reader["Nprefix"].ToString();
                    }
                    returnList.Add(book);
                }
            }

            return returnList;
        }

        public override TList<AartobmBook> ProcessBooks(List<Book> books, int autIntNo)
        {
            AartobmBookService bookService = new AartobmBookService();
            AartobmDocumentService documentService = new AartobmDocumentService();
            TList<AartobmBook> returnList = new TList<AartobmBook>();

            foreach (Book book in books)
            {
                using (ConnectionScope.CreateTransaction())
                {
                    AartobmBook bmBook = bookService.GetByAaBmBookId(book.BookId);
                    if (bmBook != null)
                    {
                        bmBook.AaBmBookStatusId = (int)AartobmStatusList.BookIssuedToOfficer;
                        bmBook.LastUser = book.LastUser;
                        bmBook.ToIntNo = book.OfficerId;
                        bmBook.AabmBookIssuedDate=DateTime.Now;

                        TList<AartobmDocument> listDocument = documentService.GetByAaBmBookId(book.BookId);
                        foreach (AartobmDocument document in listDocument)
                        {
                            document.AaBmDocStatusId = (int)AartobmStatusList.DocumentIssuedToOfficer;
                            document.ToIntNo = book.OfficerId;
                            document.LastUser = book.LastUser;
                            document.AabmDocIssuedDate=DateTime.Now;
                        }

                        bmBook = bookService.Save(bmBook);
                        documentService.Update(listDocument);


                        returnList.Add(bmBook);

                        ConnectionScope.Complete();
                    }
                }
            }
            return returnList;

        }

        public  byte[] PrintNote(List<Book> books, int noteNo, string tempFileName)
        {
            byte[] documentByte = SetPDFBody(books, noteNo,tempFileName);
            return documentByte;
        }

        protected override byte[] GetPrintNoteDocument(List<AartobmBook> list, int mtrIntNo, string lastUser, int transactionType)
        {
            throw new NotImplementedException();
        }

        private void SetPDFHeader(DocumentLayout docLayout, int noteNo)
        {
            //Label lbl;

            //lbl = (Label)docLayout.GetElementById("lblNoteNo");
            //lbl.Text = noteNo.ToString();
        }

        private byte[] SetPDFBody(List<Book> bookList,int noteNo, string tempFilename)
        {
            //string reportPage =  @"Reports\ReceiveBooks.dplx";
            //string s = System.IO.Path.GetPathRoot(@"Reports\ReceiveBooks.dplx");
            //string fullPath = Path.Combine(ConfigurationManager.AppSettings["ReportFilesDir"], reportPage);
            byte[] buffer = new byte[] { };
            if (bookList.Count > 0)
            {
                //int status = bookList[0].BookStatusId;
                int mtrIntNo = bookList[0].MetrIntNo;
                int depotIntNo = bookList[0].DepotIntNo;
                int officerIntNo = bookList[0].OfficerId;

                DocumentLayout docLayout = new DocumentLayout(tempFilename);

                //Setting PDF Header

                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblDate");
                lbl.Text = String.Format("{0:yyyy/MM/dd}", DateTime.Now.Date);

                lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblUser");
                lbl.Text = bookList[0].LastUser;

                lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblMetro");
                lbl.Text = new MetroService().GetByMtrIntNo(bookList[0].MetrIntNo).MtrName;

                lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblDepot");
                lbl.Text = new AartobmDepotService().GetByAaBmDepotId(depotIntNo).AaBmDepotDescription;

                lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblOfficer");
                lbl.Text = new TrafficOfficerService().GetByToIntNo(officerIntNo).TosName;
                //2013-02-18 added by Nancy for traffic officer initials 
                if (!string.IsNullOrEmpty(new TrafficOfficerService().GetByToIntNo(officerIntNo).ToInit))
                {
                    lbl.Text += ",  " + new TrafficOfficerService().GetByToIntNo(officerIntNo).ToInit;
                }

                lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblNoteNo");
                lbl.Text = noteNo.ToString().PadLeft(10, '0');

                //2012-12-11 Nancy(add TONo)
                lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblOfficeNo");
                lbl.Text = new TrafficOfficerService().GetByToIntNo(officerIntNo).ToNo;

                //Srtting PDF Body

                StoredProcedureQuery query = (StoredProcedureQuery)docLayout.GetQueryById("Query");
                query.ConnectionString
                    = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
                ParameterDictionary parameters = new ParameterDictionary();
                string bookId = String.Empty;
                foreach (BookS56 book in bookList)
                {
                    bookId += book.BookId + ";";
                }
                bookId = bookId.Remove(bookId.LastIndexOf(';'));
                parameters.Add("BookId", bookId);

                Document doc = docLayout.Run(parameters);
                buffer = doc.Draw();
            }

            return buffer;
        }
    }
}
