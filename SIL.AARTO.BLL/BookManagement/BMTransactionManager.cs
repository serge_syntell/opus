﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.BookManagement
{
    public class BMTransactionManager
    {
        public static int GetTransactionNote(TList<AartobmBook> bookList, int mtrIntNo, int transactionType, string noteUser, string lastUser)
        {
            int number = 0;
            using (ConnectionScope.CreateTransaction())
            {
                MetroTransaction transaction = new MetroTransactionService().GetByMetroIntNoTtIntNo(mtrIntNo, transactionType);
                if (transaction != null)
                {
                    number = transaction.MtNumber;
                    transaction.MtNumber += 1;
                    transaction.LastUser = lastUser;
                    transaction = new MetroTransactionService().Save(transaction);
                }
                AartobmBookTransaction bookTransacion = new AartobmBookTransaction();

                bookTransacion.AaBmNoteDate = DateTime.Now.Date;
                bookTransacion.AaBmNoteNo = number.ToString();
                bookTransacion.AaBmNoteUserName = noteUser;
                bookTransacion.LastUser = lastUser;

                bookTransacion =new AartobmBookTransactionService().Save(bookTransacion);
                TList<AartobmBookTranConjoin> listBookTranConjoin = new TList<AartobmBookTranConjoin>();
                foreach (AartobmBook book in bookList)
                {
                    AartobmBookTranConjoin tranConjoin = new AartobmBookTranConjoin();
                    tranConjoin.AaBmBookId = book.AaBmBookId;
                    tranConjoin.AaBmTranId = bookTransacion.AaBmTranId;
                    tranConjoin.LastUser = lastUser;
                    listBookTranConjoin.Add(tranConjoin);

                }
                listBookTranConjoin = new AartobmBookTranConjoinService().Save(listBookTranConjoin);

                ConnectionScope.Complete();
            }
            return number;
        }

        public static void ResetTransactionNote(int mtrIntNo, int transactionType, string lastUser)
        {  
            MetroTransaction transaction = new MetroTransactionService().GetByMetroIntNoTtIntNo(mtrIntNo, transactionType);
            if (transaction != null)
            {
                transaction.MtNumber -= 1;
                transaction.LastUser = lastUser;
                transaction = new MetroTransactionService().Save(transaction);
            }   
        }
    }
}
