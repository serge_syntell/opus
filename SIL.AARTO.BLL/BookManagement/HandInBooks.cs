﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Data;
using SIL.AARTO.DAL.Data;

namespace SIL.AARTO.BLL.BookManagement
{
    public class HandInBooks:BookProcessBase
    {
        
        public HandInBooks()
        {
        }


        public override List<AartobmBook> SearchBooks(int mtrIntNo, int depotIntNo, int? officerIntNo, int startNo, int endNo)
        {
            List<AartobmBook> returnList = new List<AartobmBook>();
            using (IDataReader reader = new AartobmBookService().SearchBooksForHandIn(mtrIntNo, depotIntNo,officerIntNo, startNo, endNo))
            {
                while (reader.Read())
                {
                    AartobmBook book = new AartobmBook();
                    book.AaBmBookId = (decimal)reader["AaBmBookId"];
                    book.AaBmBookNo = reader["AaBmBookNo"].ToString();
                    book.AaBmBookStartingNo = (int)reader["AaBmBookStartingNo"];
                    book.AaBmBookEndingNo = (int)reader["AaBmBookEndingNo"];
                    book.AaBmBookStatusId = (int)reader["AaBmBookStatusId"];
                    book.AaBmBookTypeId = (int)reader["AaBmBookTypeId"];
                    if (reader["AaBmDepotId"] != DBNull.Value)
                        book.AaBmDepotId = (int)reader["AaBmDepotId"];
                    if (reader["MtrIntNo"] != DBNull.Value)
                        book.MtrIntNo = (int)reader["MtrIntNo"];
                    if (reader["ToIntNo"] != DBNull.Value)
                        book.ToIntNo = (int)reader["ToIntNo"];
                    book.LastUser = reader["LastUser"].ToString();
                    if (reader["AaBMBookTypeID"] != DBNull.Value)
                    {
                        book.AaBmBookTypeId = Convert.ToInt32(reader["AaBMBookTypeID"]);
                        book.AaBmBookTypeIdSource = new AartobmBookType();
                        book.AaBmBookTypeIdSource.AaBmBookTypeName = reader["AaBmBookTypeName"].ToString();
                    }
                    if (reader["NPHIntNo"] != DBNull.Value)
                    {
                        book.NphIntNo = Convert.ToInt32(reader["NPHIntNo"]);
                        book.NphIntNoSource = new NoticePrefixHistory();
                        book.NphIntNoSource.Nprefix = reader["Nprefix"].ToString();
                    }
                    returnList.Add(book);
                }
            }

            return returnList;
              
        }

        public override TList<AartobmBook> ProcessBooks(List<Book> books, int autIntNo)
        {
            AartobmBookService bookService = new AartobmBookService();
            AartobmDocumentService documentService = new AartobmDocumentService();
            TList<AartobmBook> returnList = new TList<AartobmBook>();

            foreach (Book book in books)
            {
                using (ConnectionScope.CreateTransaction())
                {
                    AartobmBook bmBook = bookService.GetByAaBmBookId(book.BookId);
                    if (bmBook != null)
                    {
                        bmBook.AaBmBookStatusId = book.BookStatusId;
                        bmBook.LastUser = book.LastUser;
                        bmBook.AaBmDepotId = book.DepotIntNo;
                        bmBook.ToIntNo = book.OfficerId;

                        //TList<AartobmDocument> listDocument = documentService.GetByAaBmBookId(book.BookId);
                        //foreach (AartobmDocument document in listDocument)
                        //{
                        //    document.AaBmDocStatusId = (int)AartobmStatusList.DocumentHandedInToDepot;
                        //    document.ToIntNo = book.OfficerId;
                        //    document.LastUser = book.LastUser;
                        //}

                        bmBook = bookService.Save(bmBook);
                        //documentService.Update(listDocument);


                        returnList.Add(bmBook);

                        ConnectionScope.Complete();
                    }
                }
            }
            return returnList;
        }

        

        public override byte[] PrintNote(List<Book> books, string tempFileName)
        {
            throw new NotImplementedException();
        }

        protected override byte[] GetPrintNoteDocument(List<AartobmBook> list, int mtrIntNo, string lastUser, int transactionType)
        {
            throw new NotImplementedException();
        }

        public TList<AartobmBook> GetBooks(List<Book> books)
        {
            TList<AartobmBook> returnList = new TList<AartobmBook>();
            try
            {
                if (books != null && books.Count > 0)
                {
                    string[] booksStrArr = new string[books.Count];
                    int index = 0;
                    foreach (Book book in books)
                    {
                        booksStrArr[index++] = book.BookId.ToString();
                    }
                    AartobmBookQuery query = new AartobmBookQuery();
                    query.AppendIn(AartobmBookColumn.AaBmBookId, booksStrArr);

                    returnList = new AartobmBookService().Find(query as IFilterParameterCollection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return returnList;
        }
    }
}
