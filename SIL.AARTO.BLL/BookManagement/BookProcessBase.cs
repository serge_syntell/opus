﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Services;
using System.Data;
using SIL.AARTO.BLL.Model;

namespace SIL.AARTO.BLL.BookManagement
{
    public abstract class BookProcessBase
    {

        public List<MetroListItem> GetAllMetroList()
        {
            return MetroManager.GetMetroListItem();
        }

        public List<DepotListItem> GetDepotListItem(int metrIntNo)
        {
            return DepotManager.GetDepots(metrIntNo);
        }

        public List<TrafficOfficerListItem> GetTrafficOfficers(int metrIntNo)
        {
            return TrafficOfficerManager.GetTrafficOfficerListItem(metrIntNo);
        }

        public TList<AartobmBook> GetBooksFromDB(int? bookStatus, int depotId)
        {
            TList<AartobmBook> tListBookList = new TList<AartobmBook>();
            if (bookStatus.HasValue)
            {
                foreach (AartobmBook book in
                    new AartobmBookService().GetByAaBmBookStatusId(bookStatus.Value).Where(m => m.AaBmDepotId.Equals(depotId)).ToList())
                {
                    tListBookList.Add(book);
                }
            }
            else
            {
                tListBookList = new AartobmBookService().GetByAaBmDepotId(depotId);
            }
            if (tListBookList == null)
                tListBookList = new TList<AartobmBook>();
            return tListBookList;

        }

        public List<AartobmBook> GetBooks(int? bookStatus, int depotId)
        {
            List<AartobmBook> bookList = new List<AartobmBook>();
            TList<AartobmBook> TListBookList = GetBooksFromDB(bookStatus, depotId);

            foreach (AartobmBook book in TListBookList)
            {
                bookList.Add(book);
            }
            return bookList;
        }

        public List<AartobmBook> GetBooks(int? bookStatus, int depotId, int officerId)
        {
            string where = String.Empty;
            List<AartobmBook> bookList = new List<AartobmBook>();
            if (bookStatus.HasValue)
            {
                where = String.Format(" AaBMBookStatusID={0} And AaBMDepotID={1} And TOIntNo={2} ", bookStatus.Value, depotId, officerId);
            }
            else
            {
                where = String.Format(" AaBMDepotID={0} And TOIntNo={1} ", depotId, officerId);
            }
            int totalCount = 0;
            foreach (AartobmBook book in new AartobmBookService().GetPaged(where, "AaBMBookNo", 0, 0, out totalCount))
            {
                bookList.Add(book);
            }
            return bookList;
        }

        public int GetNoteNo(int mertIntNo, int transactionType)
        {
            int number = 0;
            MetroTransaction transaction = new MetroTransactionService().GetByMetroIntNoTtIntNo(mertIntNo, transactionType);
            if (transaction != null)
            {
                number = transaction.MtNumber;
            }
            return number;
        }

        // 2013-07-17 comment by Henry for no use
        //public int SetNoteNo(int metroIntNo, int transactionType, int number)
        //{
        //    MetroTransactionService service = new MetroTransactionService();

        //    MetroTransaction transaction = service.GetByMetroIntNoTtIntNo(metroIntNo, transactionType);
        //    if (transaction != null)
        //    {
        //        transaction.MtNumber = number;

        //        service.Save(transaction);
        //    }
        //    else
        //    {
        //        number = 0;
        //    }
        //    return number;
        //}

        public AuthorityRuleInfo GetAuthorityRules(int autIntNo, string code)
        {
            AuthorityRuleInfo autRule =new AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(autIntNo, code);
            return autRule;
        }


        public bool CheckMaxBooks(List<Book> bookList, int autIntNo, int toIntNo, ref int maxNum)
        {
            if (toIntNo == 0)
                return false;

            AuthorityRuleInfo autRule = new AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(autIntNo, "4820");
            //int maxNum = 0;
            if (autRule != null)
            {
                maxNum = autRule.ARNumeric!=null ? Convert.ToInt32(autRule.ARNumeric) : 0;
            }
            if (bookList.Count > maxNum)
            {
                return true;
            }
            else
            {
                TList<AartobmBook> list = null; //new AartobmBookService().GetByToIntNo(toIntNo);
                string where = String.Format(" ToIntNo ={0} And ( AaBMBookStatusID={1} Or AaBMBookStatusID={2})", toIntNo, (int)AartobmStatusList.BookIssuedToOfficer, (int)AartobmStatusList.BookTransferedByDepot);
                int totalCount = 0;
                list = new AartobmBookService().GetPaged(where, "AaBMBookNo", 0, 0, out totalCount);
                int recoreds = list == null ? 0 : list.Count;

                return ((recoreds + bookList.Count) > maxNum);
            }
        }

        protected abstract byte[] GetPrintNoteDocument(List<AartobmBook> list, int mtrIntNo, string lastUser, int transactionType);

        public abstract List<AartobmBook> SearchBooks(int mtrIntNo, int depotIntNo, int? officerIntNo, int startNo, int endNo);

        public abstract TList<AartobmBook> ProcessBooks(List<Book> books, int autIntNo);

        //public abstract void PrintNote(byte[] documentByte);
        public virtual byte[] PrintNote(List<Book> books, string tempFileName)
        {
            return new byte[] { };
        }

    }
}
