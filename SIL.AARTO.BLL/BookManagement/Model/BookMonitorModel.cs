﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.BookManagement.Model
{
    public class BookMonitorModel
    {
        public int DepotIntNo { get; set; }
        public int MetrIntNo { get; set; }
        public int OfficerIntNo { get; set; }
        public int? StartNo { get; set; }
        public int? EndNo { get; set; }
        public int Status { get; set; }
        public int BookTypeId { get; set; }
        public SelectList DepotList { get; set; }
        public SelectList MetroList { get; set; }
        public SelectList OfficerList { get; set; }
        public SelectList BookStatusList { get; set; }
        public SelectList BookTypeList { get; set; }

        public List<AartobmBook> BookList { get; set; }

        public List<AartobmDocument> DocumentList { get; set; }

        public BookMonitorModel()
        {
            BookList = new List<AartobmBook>();
            DocumentList = new List<AartobmDocument>();
        }
    }
}
