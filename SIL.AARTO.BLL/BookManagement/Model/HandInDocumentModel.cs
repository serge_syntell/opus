﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.BookManagement.Model
{
    public class HandInDocumentModel
    {
        public int DepotIntNo { get; set; }
        public int TOIntNo { get; set; }
        public int MetroIntNo { get; set; }
        public string DocumentNo { get; set; }

        public AartobmDocument Document { get; set; }
        public string AuthNo { get; set; }
        public int Rule_9300 { get; set; }
        //public SelectList DepotList { get; set; }
        //public SelectList OfficerList { get; set; }
        //public SelectList MetroList { get; set; }
        //public List<AartobmBook> BookList { get; set; }

    }
}
