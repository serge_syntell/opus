﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.BLL.BookManagement.Model
{
    public class ReceiveBooksModel
    {
        public int MetrIntNo { get; set; }
        public int DepotIntNo { get; set; }

        public int BookTypeId { get; set; }

        public string PrintingCompany { get; set; }
        public int BooksNum { get; set; }
        public int? BookStartNo { get; set; }
        public int? BookEndNo { get; set; }
        public int? NPHIntNo { get; set; }

        public SelectList MetroList { get; set; }
        public SelectList DepotList { get; set; }

        public SelectList BookTypeList { get; set; }
        public SelectList NotPrefixList { get; set; }

        public bool IsValid
        {
            get { return (GetRuleViolations().Count() == 0); }
        }

        public IEnumerable<RuleViolation> GetRuleViolations()
        {
            if (String.IsNullOrEmpty(PrintingCompany.Trim()))
            {
                yield return new RuleViolation("PrintingCompany", "Printing company is required!");
            }
            if (BooksNum.Equals(0))
            {
                yield return new RuleViolation("BooksNum", "Number of books is required!");
            }
            if (!BookStartNo.HasValue)
            {
                yield return new RuleViolation("BookStartNo", "Book start no is required!");
            }
            if (!BookEndNo.HasValue)
            {
                yield return new RuleViolation("BookEndNo", "Book end no is required!");
            }
            //if (BookStartNo.HasValue && BookEndNo.HasValue)
            //{
            //    int booksNum = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["BooksNum"]);

            //    float result = (BookEndNo.Value - BookStartNo.Value + 1) / booksNum;

            //    if (!result.Equals(0f))
            //    {
            //        yield return new RuleViolation("BookEndNo", "Book start no or book end no is incorrect!");
            //    }

            //}


        }
    }
}
