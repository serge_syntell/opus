﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.BookManagement.Model
{
    public class HandInBookModel
    {
        public int DepotIntNo { get; set; }
        public int TOIntNo { get; set; }
        public int MetroIntNo { get; set; }
        public int? StartNo { get; set; }
        public int? EndNo { get; set; }
        public SelectList DepotList { get; set; }
        public SelectList OfficerList { get; set; }
        public SelectList MetroList { get; set; }
        public List<AartobmBook> BookList { get; set; }

        public HandInBookModel()
        {
            BookList = new List<AartobmBook>();
        }

    }
}
