﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.BookManagement.Model
{
    public class DocumentMonitorModel
    {
        public string BookNo { get; set; }

        public List<AartobmDocument> DocumentList { get; set; }
    }
}
