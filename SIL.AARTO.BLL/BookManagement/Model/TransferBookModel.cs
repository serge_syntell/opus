﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.BookManagement.Model
{
    public class TransferBookModel
    {
        public int DepotFromIntNo { get; set; }
        public int TOFromIntNo { get; set; }
        public int DepotToIntNo { get; set; }
        public int TOToIntNo { get; set; }
        public int MetroIntNo { get; set; }
        public int? StartNo { get; set; }
        public int? EndNo { get; set; }
        public SelectList DepotFromList { get; set; }
        public SelectList OfficerFromList { get; set; }
        public SelectList DepotToList { get; set; }
        public SelectList OfficerToList { get; set; }
        public SelectList MetroList { get; set; }
        public List<AartobmBook> BookList { get; set; }

        public TransferBookModel()
        {
            BookList = new List<AartobmBook>();
        }
    }
}
