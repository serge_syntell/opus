﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web.Mvc;

namespace SIL.AARTO.BLL.BookManagement.Model
{
    public class ReversalBookModel
    {
        public int DepotIntNo { get; set; }
        public int MetroIntNo { get; set; }
       
        public SelectList DepotList { get; set; }
        public SelectList MetroList { get; set; }

        public List<AartobmBook> BookList { get; set; }
        public ReversalBookModel()
        {
            BookList = new List<AartobmBook>();
        }
    }
}
