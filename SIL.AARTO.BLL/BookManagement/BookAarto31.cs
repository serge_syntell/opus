﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.BookManagement
{
    public class BookAarto31 : Book  // Book
    {

        public override int BookType
        {
            get { return (int)AartobmBookTypeList.AARTO31; }
        }

        public override int BookSize
        {
            get
            {
                AartobmBookTypeService db = new AartobmBookTypeService();
                return (int)db.GetByAaBmBookTypeId(BookType).AaBmBookSize;
            }
        }

    }
}
