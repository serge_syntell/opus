﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility;

using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using System.Configuration;
using ceTe.DynamicPDF.PageElements;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.BLL.Model;


namespace SIL.AARTO.BLL.BookManagement
{
    public class ReceiveBooks : BookProcessBase
    {

        public override List<AartobmBook> SearchBooks(int mtrIntNo, int depotIntNo, int? officerIntNo, int startNo, int endNo)
        {
            throw new NotImplementedException();
        }

        public bool CheckBooks(List<Book> books, int depotIntNo)
        {
            foreach (AartobmBook book in new AartobmBookService().GetByAaBmDepotId(depotIntNo))
            {

            }

            return false;
        }

        protected override byte[] GetPrintNoteDocument(List<AartobmBook> list, int mtrIntNo, string lastUser, int transactionType)
        {
            //Get Note Seq No
            try
            {
                int noteNo = 0;
                MetroTransaction metroTransaction = new MetroTransactionService().GetByMetroIntNoTtIntNo(mtrIntNo, transactionType);
                if (metroTransaction != null)
                {
                    noteNo = metroTransaction.MtNumber;
                }
                byte[] returnByte = new byte[] { };

                return returnByte;

                // Dynamic PDF 

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Reveice books from printer
        /// </summary>
        /// <param name="book"></param>
        public override TList<AartobmBook> ProcessBooks(List<Book> books, int autIntNo)
        {
            try
            {
                AartobmBookService bookService = new AartobmBookService();
                AartobmDocumentService documentService = new AartobmDocumentService();

                int startBookNo = 0;
                TList<AartobmDocument> listDocument = new TList<AartobmDocument>();
                TList<AartobmBook> listBook = new TList<AartobmBook>();
                //ARC Code 4810:   BM book numbering method
                AuthorityRuleInfo autRules = new AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(autIntNo, "4810");
                if (autRules == null || !autRules.ARString.Equals("N"))
                {
                    return listBook;
                }
                foreach (Book book in books)
                {
                    using (ConnectionScope.CreateTransaction())
                    {
                        for (int index = 1; index <= book.BookNum; index++)
                        {
                            startBookNo = book.BookStartNo + book.BookSize * (index - 1);

                            // Create Book
                            AartobmBook aartoBook = new AartobmBook();

                            aartoBook.AaBmBookNo = String.Format("{0}-{1}", startBookNo, startBookNo + book.BookSize - 1);

                            aartoBook.AaBmBookStartingNo = startBookNo;
                            aartoBook.AaBmBookEndingNo = startBookNo + book.BookSize - 1;
                            aartoBook.AaBmPrintingCompany = book.PrintingCompany;
                            aartoBook.AaBmBookTypeId = book.BookType;
                            aartoBook.AaBmBookStatusId = book.BookStatusId;
                            aartoBook.AaBmBookCreatedDate = DateTime.Now.Date;
                            aartoBook.MtrIntNo = book.MetrIntNo;
                            aartoBook.AaBmDepotId = book.DepotIntNo;
                            aartoBook.LastUser = book.LastUser;
                            aartoBook.NphIntNo = book.NotPrefixIntNo;
                            listBook.Add(aartoBook);

                            listBook = bookService.Save(listBook);

                        }
                        foreach (AartobmBook aartobmBook in listBook)
                        {
                            for (int index = aartobmBook.AaBmBookStartingNo; index <= aartobmBook.AaBmBookEndingNo; index++)
                            {
                                AartobmDocument document = new AartobmDocument();
                                document.AaBmBookId = aartobmBook.AaBmBookId;
                                document.AaBmDocCreatedDate = DateTime.Now.Date;
                                document.AaBmDocNo = index.ToString();
                                document.AaBmDocStatusId = (int)AartobmStatusList.DocumentReceivedByDepot;
                                document.LastUser = book.LastUser;
                                document.AaBmDepotId = book.DepotIntNo;
                                document.MtrIntNo = book.MetrIntNo;
                                document.AaBmIsCancelled = false;
                                listDocument.Add(document);
                            }
                            listDocument = documentService.Save(listDocument);
                        }

                        ConnectionScope.Complete();
                    }
                }
                return listBook;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                //ErrorLog.AddErrorLog(ex, "");
                //return null;
                throw ex;
            }
        }

        public byte[] PrintNote(List<Book> books, int noteNo, string tempFileName)
        {
            //throw new NotImplementedException();

            return SetPDFBody(books, noteNo, tempFileName);
        }


        private byte[] SetPDFBody(List<Book> bookList, int noteNo, string tempFilename)
        {

            byte[] buffer = new byte[] { };
            if (bookList.Count > 0)
            {
                int status = bookList[0].BookStatusId;
                int mtrIntNo = bookList[0].MetrIntNo;
                int depotIntNo = bookList[0].DepotIntNo;
                string printingCompany = bookList[0].PrintingCompany;

                DocumentLayout docLayout = new DocumentLayout(tempFilename);

                //Setting PDF Header

                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblDate");
                lbl.Text = String.Format("{0:yyyy/MM/dd}", DateTime.Now.Date);

                lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblUser");
                lbl.Text = bookList[0].LastUser;

                lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblPrintingCompany");
                lbl.Text = printingCompany;

                lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblNoteNo");
                lbl.Text = noteNo.ToString().PadLeft(10, '0');


                //Setting PDF Body
                StoredProcedureQuery query = (StoredProcedureQuery)docLayout.GetQueryById("Query");
                query.ConnectionString
                    = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
                ParameterDictionary parameters = new ParameterDictionary();
                parameters.Add("Status", status);
                parameters.Add("MtrIntNo", mtrIntNo);
                parameters.Add("DepotIntNo", depotIntNo);
                parameters.Add("NoteNo", noteNo);

                Document doc = docLayout.Run(parameters);
                buffer = doc.Draw();
            }

            return buffer;
        }

        public bool CheckDocumentsExists(int stratNo, int endNo, int bookTypeId, int prefixId, int metrIntNo)
        {
            int totcount = 0;
            try
            {
                //string sql = String.Format("MtrIntNo={0} ANd Convert(Int,AaBMDocNo) Between {1} And {2} And AaBMDocStatusID<>{3}", metrIntNo, stratNo, endNo, (int)AartobmStatusList.DocumentReversed);
                using (IDataReader reader = new AartobmBookService().CheckBook(metrIntNo, bookTypeId, prefixId, stratNo, endNo))
                {
                    while (reader.Read())
                    {
                        totcount = Convert.ToInt32(reader["DocumentCount"]);
                        break;
                    }
                }
                //new AartobmDocumentService().GetPaged(sql, "AaBMDocID", 0, 0, out totcount);
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return totcount > 0;
        }

        public int GetBookSize(int bookType)
        {
            AartobmBookTypeService db = new AartobmBookTypeService();
            return (int)db.GetByAaBmBookTypeId(bookType).AaBmBookSize;
        }
    }
}
