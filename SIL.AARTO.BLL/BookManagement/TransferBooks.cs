﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using System.Configuration;
using ceTe.DynamicPDF;
using System.Data;

namespace SIL.AARTO.BLL.BookManagement
{
    public class TransferBooks : BookProcessBase
    {

        public override TList<AartobmBook> ProcessBooks(List<Book> books, int autIntNo)
        {
            AartobmBookService bookService = new AartobmBookService();
            AartobmDocumentService documentService = new AartobmDocumentService();
            TList<AartobmBook> returnList = new TList<AartobmBook>();

            foreach (Book book in books)
            {
                using (ConnectionScope.CreateTransaction())
                {
                    AartobmBook bmBook = bookService.GetByAaBmBookId(book.BookId);
                    if (bmBook != null)
                    {
                        bmBook.AaBmBookStatusId = (int)AartobmStatusList.BookTransferedByDepot;
                        bmBook.LastUser = book.LastUser;
                        //bmBook.AabmBookIssuedDate = DateTime.Now;
                        if (book.DepotIntNo != 0)
                            bmBook.AaBmDepotId = book.DepotIntNo;
                        if (book.OfficerId != 0)
                            bmBook.ToIntNo = book.OfficerId;

                        TList<AartobmDocument> listDocument = new TList<AartobmDocument>();
                        foreach (AartobmDocument document in documentService.GetByAaBmBookId(book.BookId))
                        {

                            if (document.AaBmDocStatusId.Equals((int)AartobmStatusList.DocumentIssuedToOfficer)
                                || document.AaBmDocStatusId.Equals((int)AartobmStatusList.DocumentReceivedByDepot))
                            {
                                if (book.OfficerId != 0 && !document.AaBmIsCancelled)
                                {
                                    document.ToIntNo = book.OfficerId;
                                }
                                if (book.DepotIntNo != 0)
                                {
                                    document.AaBmDepotId = book.DepotIntNo;
                                }
                                document.LastUser = book.LastUser;
                                document.AabmDocIssuedDate = DateTime.Now;

                                listDocument.Add(document);
                            }
                        }

                        bmBook = bookService.Save(bmBook);
                        documentService.Update(listDocument);


                        returnList.Add(bmBook);

                        ConnectionScope.Complete();
                    }
                }
            }
            return returnList;

        }

        public override List<AartobmBook> SearchBooks(int mtrIntNo, int depotIntNo, int? officerIntNo, int startNo, int endNo)
        {
            List<AartobmBook> returnList = new List<AartobmBook>();
            using (IDataReader reader = new AartobmBookService().SearchBooksForTransfer(mtrIntNo, depotIntNo, officerIntNo, startNo, endNo))
            {
                while (reader.Read())
                {
                    AartobmBook book = new AartobmBook();
                    book.AaBmBookId = (decimal)reader["AaBmBookId"];
                    book.AaBmBookNo = reader["AaBmBookNo"].ToString();
                    book.AaBmBookStartingNo = (int)reader["AaBmBookStartingNo"];
                    book.AaBmBookEndingNo = (int)reader["AaBmBookEndingNo"];
                    book.AaBmBookStatusId = (int)reader["AaBmBookStatusId"];
                    book.AaBmBookTypeId = (int)reader["AaBmBookTypeId"];
                    if (reader["AaBmDepotId"] != DBNull.Value)
                        book.AaBmDepotId = (int)reader["AaBmDepotId"];
                    if (reader["MtrIntNo"] != DBNull.Value)
                        book.MtrIntNo = (int)reader["MtrIntNo"];
                    if (reader["ToIntNo"] != DBNull.Value)
                        book.ToIntNo = (int)reader["ToIntNo"];
                    book.LastUser = reader["LastUser"].ToString();
                    if (reader["AaBMBookTypeID"] != DBNull.Value)
                    {
                        book.AaBmBookTypeId = Convert.ToInt32(reader["AaBMBookTypeID"]);
                        book.AaBmBookTypeIdSource = new AartobmBookType();
                        book.AaBmBookTypeIdSource.AaBmBookTypeName = reader["AaBmBookTypeName"].ToString();
                    }
                    if (reader["NPHIntNo"] != DBNull.Value)
                    {
                        book.NphIntNo = Convert.ToInt32(reader["NPHIntNo"]);
                        book.NphIntNoSource = new NoticePrefixHistory();
                        book.NphIntNoSource.Nprefix = reader["Nprefix"].ToString();
                    }
                    returnList.Add(book);
                }
            }

            return returnList;
        }

        public new byte[] PrintNote(List<Book> books, int noteNo, int fromepotId, int fromOfficerId, string tempFileName)
        {
            return SetPDFBody(books, noteNo, fromepotId, fromOfficerId, tempFileName);
        }

        protected override byte[] GetPrintNoteDocument(List<AartobmBook> list, int mtrIntNo, string lastUser, int transactionType)
        {
            throw new NotImplementedException();
        }

        private byte[] SetPDFBody(List<Book> bookList, int noteNo, int fromDepotId, int fromOfficerId, string tempFilename)
        {
            byte[] buffer = new byte[] { };
            if (bookList.Count > 0)
            {

                DocumentLayout docLayout = new DocumentLayout(tempFilename);

                //Setting PDF Header

                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblDate");
                lbl.Text = String.Format("{0:yyyy/MM/dd}", DateTime.Now.Date);

                lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblUser");
                lbl.Text = bookList[0].LastUser;

                lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblMetro");
                lbl.Text = new MetroService().GetByMtrIntNo(bookList[0].MetrIntNo).MtrName;

                lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblNoteNo");
                lbl.Text = noteNo.ToString().PadLeft(10, '0');

                if (bookList[0].OfficerId == 0)
                {
                    lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblFromDepot");
                    lbl.Text = new AartobmDepotService().GetByAaBmDepotId(fromDepotId).AaBmDepotDescription;

                    lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblToDepot");
                    lbl.Text = new AartobmDepotService().GetByAaBmDepotId(bookList[0].DepotIntNo).AaBmDepotDescription;
                }
                else
                {
                    lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblFromOfficer");
                    lbl.Text = new TrafficOfficerService().GetByToIntNo(fromOfficerId).TosName;

                    lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblToOfficer");
                    lbl.Text = new TrafficOfficerService().GetByToIntNo(bookList[0].OfficerId).TosName; ;
                }


                //Setting PDF Body
                StoredProcedureQuery query = (StoredProcedureQuery)docLayout.GetQueryById("Query");
                query.ConnectionString
                    = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
                ParameterDictionary parameters = new ParameterDictionary();
                //parameters.Add("Status", status);
                //parameters.Add("MtrIntNo", mtrIntNo);
                //parameters.Add("DepotIntNo", depotIntNo);
                string bookId = String.Empty;
                foreach (BookS56 book in bookList)
                {
                    bookId += book.BookId + ";";
                }
                bookId = bookId.Remove(bookId.LastIndexOf(';'));
                parameters.Add("BookId", bookId);

                Document doc = docLayout.Run(parameters);
                buffer = doc.Draw();
            }

            return buffer;
        }
    }
}
