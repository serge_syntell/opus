﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.BookManagement
{
    public class ReversalBooks : BookProcessBase
    {

        protected override byte[] GetPrintNoteDocument(List<AartobmBook> list, int mtrIntNo, string lastUser, int transactionType)
        {
            throw new NotImplementedException();
        }

        public override List<AartobmBook> SearchBooks(int mtrIntNo, int depotIntNo, int? officerIntNo, int startNo, int endNo)
        {
            List<AartobmBook> returnList = new List<AartobmBook>();
            returnList = new BookMonitors().SearchBooks(mtrIntNo, depotIntNo, 0, (int)AartobmStatusList.BookReceivedByDepot, startNo, endNo);
            //AartobmBookService DB = new AartobmBookService();
            //List<AartobmBook> returnList = new List<AartobmBook>();
            //int totalCount = 0;
            //string sql = String.Format(" AaBmBookStatusId ={0} And AaBMDepotID={1} ", (int)AartobmStatusList.BookReceivedByDepot, depotIntNo);
            //foreach (AartobmBook book in DB.GetPaged(sql, "AaBMBookID", 0, 0, out totalCount))
            //{
            //    returnList.Add(book);
            //}
            return returnList;
        }

        public override TList<AartobmBook> ProcessBooks(List<Book> books, int autIntNo)
        {
            AartobmBookService bookService = new AartobmBookService();
            AartobmDocumentService documentService = new AartobmDocumentService();
            TList<AartobmBook> returnList = new TList<AartobmBook>();

            foreach (Book book in books)
            {
                using (ConnectionScope.CreateTransaction())
                {
                    AartobmBook bmBook = bookService.GetByAaBmBookId(book.BookId);
                    if (bmBook != null)
                    {
                        bmBook.AaBmBookNo = bmBook.AaBmBookNo + "_REV";
                        bmBook.AaBmBookStatusId = (int)AartobmStatusList.BookReversed;
                        bmBook.LastUser = book.LastUser;

                        TList<AartobmDocument> listDocument = documentService.GetByAaBmBookId(book.BookId);
                        foreach (AartobmDocument document in listDocument)
                        {
                            document.AaBmDocNo = document.AaBmDocNo + "_REV";
                            document.AaBmDocStatusId = (int)AartobmStatusList.DocumentReversed;
                            document.LastUser = book.LastUser;
                        }

                        bmBook = bookService.Save(bmBook);
                        documentService.Update(listDocument);

                        returnList.Add(bmBook);

                        ConnectionScope.Complete();
                    }
                }
            }
            return returnList;
        }
    }
}
