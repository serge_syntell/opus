﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.BookManagement
{
    public class BookS56 : Book
    {
        public override int BookType
        {
            get { return (int)AartobmBookTypeList.Section56; }
        }

        public override int BookSize
        {
            get
            {
                AartobmBookTypeService db = new AartobmBookTypeService();
                return (int)db.GetByAaBmBookTypeId(BookType).AaBmBookSize;
            }
        }
    }
}