﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;

namespace SIL.AARTO.BLL.BookManagement
{
    public class BookTypeItem
    {
        public int BookTypeId { get; set; }
        public string BookTypeName { get; set; }
    }
    public class BookTypeManager
    {
        public List<BookTypeItem> GetAllBookType()
        {
            List<BookTypeItem> returnList = new List<BookTypeItem>();
            AartobmBookTypeService bookTypeService = new AartobmBookTypeService();
            Dictionary<int,string> lookups = 
                AARTOBMBookTypeLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            foreach (AartobmBookType type in bookTypeService.GetAll())
            {
                BookTypeItem item = new BookTypeItem();
                item.BookTypeId = type.AaBmBookTypeId;
                item.BookTypeName = 
                    lookups.ContainsKey(type.AaBmBookTypeId) ? lookups[type.AaBmBookTypeId] : type.AaBmBookTypeDescription;
                returnList.Add(item);
            }
            return returnList;
        }
    }
}
