﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Data;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.BookManagement
{
    public class BookMonitors
    {
        public List<AartobmBook> SearchBooks(int metrIntNo, int depotIntNo, int officerIntNo, int status, int startNo, int endNo)
        {
            List<AartobmBook> returnList = new List<AartobmBook>();
            using (IDataReader reader = new AartobmBookService().SearchBooks(metrIntNo, depotIntNo, officerIntNo, status, startNo, endNo))
            {
                while (reader.Read())
                {
                    AartobmBook book = new AartobmBook();
                    book.AaBmBookId = (decimal)reader["AaBmBookId"];
                    book.AaBmBookNo = reader["AaBmBookNo"].ToString();
                    book.AaBmBookStartingNo = (int)reader["AaBmBookStartingNo"];
                    book.AaBmBookEndingNo = (int)reader["AaBmBookEndingNo"];
                    if (reader["AaBmBookStatusId"] != DBNull.Value)
                    {
                        book.AaBmBookStatusIdSource = new AartobmStatus();
                        book.AaBmBookStatusIdSource.AaBmStatusDescription = reader["AaBMStatusName"].ToString();
                    }
                    book.AaBmBookStatusId = (int)reader["AaBmBookStatusId"];
                    book.AaBmBookTypeId = (int)reader["AaBmBookTypeId"];
                    if (reader["AaBmDepotId"] != DBNull.Value)
                    {
                        book.AaBmDepotId = (int)reader["AaBmDepotId"];
                        book.AaBmDepotIdSource = new AartobmDepot();
                        book.AaBmDepotIdSource.AaBmDepotDescription = reader["AaBmDepotDescription"].ToString();
                    }
                    if (reader["MtrIntNo"] != DBNull.Value)
                    {
                        book.MtrIntNo = (int)reader["MtrIntNo"];
                        book.MtrIntNoSource = new Metro();
                        book.MtrIntNoSource.MtrName = reader["MtrName"].ToString();
                    }
                    if (reader["ToIntNo"] != DBNull.Value)
                    {
                        book.ToIntNo = (int)reader["ToIntNo"];
                        book.ToIntNoSource = new TrafficOfficer();
                        book.ToIntNoSource.TosName = reader["ToName"].ToString();
                    }
                    if (reader["AaBMBookTypeID"] != DBNull.Value)
                    {
                        book.AaBmBookTypeId = Convert.ToInt32(reader["AaBMBookTypeID"]);
                        book.AaBmBookTypeIdSource = new AartobmBookType();
                        book.AaBmBookTypeIdSource.AaBmBookTypeName = reader["AaBmBookTypeName"].ToString();
                    }
                    if (reader["NPHIntNo"] != DBNull.Value)
                    {
                        book.NphIntNo = Convert.ToInt32(reader["NPHIntNo"]);
                        book.NphIntNoSource = new NoticePrefixHistory();
                        book.NphIntNoSource.Nprefix = reader["Nprefix"].ToString();
                    }
                    book.LastUser = reader["LastUser"].ToString();
                    returnList.Add(book);
                }
            }
            return returnList;
        }

        public List<AartobmDocument> GetDocumentsByBookId(decimal bookId)
        {
            List<AartobmDocument> returnList = new List<AartobmDocument>();
            using (IDataReader reader = new AartobmDocumentService().GetDocumentByBookId(bookId))
            {
                while (reader.Read())
                {
                    AartobmDocument document = new AartobmDocument();
                    if (reader["AaBmDepotId"] != DBNull.Value)
                    {
                        document.AaBmDepotId = (int)reader["AaBmDepotId"];
                        document.AaBmDepotIdSource = new AartobmDepot();
                        document.AaBmDepotIdSource.AaBmDepotDescription = reader["AaBmDepotDescription"].ToString();
                    }
                    if (reader["MtrIntNo"] != DBNull.Value)
                    {
                        document.MtrIntNo = (int)reader["MtrIntNo"];
                        document.MtrIntNoSource = new Metro();
                        document.MtrIntNoSource.MtrName = reader["MtrName"].ToString();
                    }
                    if (reader["ToName"] != DBNull.Value)
                    {
                        document.ToIntNo = (int)reader["ToIntNo"];
                        document.ToIntNoSource = new TrafficOfficer();
                        document.ToIntNoSource.TosName = reader["ToName"].ToString();
                    }
                    
                    document.AaBmDocId = (decimal)reader["AaBmDocId"];
                    document.AaBmDocNo = reader["AaBmDocNo"].ToString();
                    if (reader["AaBmDocStatusId"] != DBNull.Value)
                    {
                        document.AaBmDocStatusId = (int)reader["AaBmDocStatusId"];
                        document.AaBmDocStatusIdSource = new AartobmStatus();
                        document.AaBmDocStatusIdSource.AaBmStatusDescription = reader["AaBMStatusName"].ToString();
                    }
                    document.LastUser = reader["LastUser"].ToString();
                    document.AaBmDocCreatedDate = DateTime.Parse(reader["AaBmDocCreatedDate"].ToString());

                    returnList.Add(document);
                }

            }
            return returnList;
        }
        public List<AartobmDocument> GetDocumentsByBookId(int bookId)
        {
            List<AartobmDocument> documentList = new List<AartobmDocument>();
            foreach (AartobmDocument document in new AartobmDocumentService().GetByAaBmBookId(bookId))
            {
                documentList.Add(document);
            }
            return documentList;
        }

        public  AartobmBook GetBookByBookId(decimal bookId)
        {
            return new AartobmBookService().GetByAaBmBookId(bookId);
        }
    }
}
