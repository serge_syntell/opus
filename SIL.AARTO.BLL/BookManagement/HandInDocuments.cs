﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Model;
using System.Data;
using SIL.AARTO.DAL.Data;

namespace SIL.AARTO.BLL.BookManagement
{
    public class HandInDocuments
    {
        public List<AartobmBook> DeepLoadBooks(int status, int toIntNo, int depotIntNo)
        {
            string where = String.Empty;
            TList<AartobmBook> bookList = new TList<AartobmBook>();
            List<AartobmBook> returnList = new List<AartobmBook>();
            where = String.Format(" AaBMBookStatusID={0} And AaBMDepotID={1} And TOIntNo={2} ", status, depotIntNo, toIntNo);

            int totalCount = 0;
            bookList = new AartobmBookService().GetPaged(where, "AaBMBookNo", 0, 0, out totalCount);

            new AartobmBookService().DeepLoad(bookList, true, SIL.AARTO.DAL.Data.DeepLoadType.IncludeChildren, new Type[] { new AartobmDocument().GetType() });
            foreach (AartobmBook book in bookList)
            {
                returnList.Add(book);
            }

            return returnList;
        }

        //public AartobmDocument ProcessHandInDocument(string docNo, string autNo, string lastUser)
        public int ProcessHandInDocument(string notPrefix, string docNo, string autNo, string lastUser)
        {
            AartobmDocumentService documentService = new AartobmDocumentService();
            int returnValue = -1;
            Authority authority = AuthorityManager.GetAuthority(autNo);
            if (authority == null)
            {
                // Authority No does not exists
                returnValue = -1;
                return returnValue;
            }
            AartobmDocument document = null;

            Metro metro = MetroManager.GetMetro(authority.MtrIntNo);

            decimal docId = 0;

            using (IDataReader reader = documentService.GetDocumentForHandIn(notPrefix, docNo, autNo))
            {
                while (reader.Read())
                {
                    docId = Convert.ToDecimal(reader["AaBMDocID"]);
                    break;
                }
            }

            document = new AartobmDocumentService().GetByAaBmDocId(docId);
            if (document == null)
            {
                //return document;
                // Document No does not exists
                returnValue = -2;
                return returnValue;
            }


            if (document.AaBmDocStatusId == (int)AartobmStatusList.DocumentIssuedToOfficer)
            {
                using (ConnectionScope.CreateTransaction())
                {//2012-12-10 Nancy(check the doucument is the last in the book, and update the status of book to 'hand in')
                    document.AaBmDocStatusId = (int)AartobmStatusList.DocumentHandedInToDepot;
                    document.LastUser = lastUser;
                    //document.AaBmIsNoAffAllowed = false;
                    document = documentService.Save(document);

                    // Jake 2013-04-26 modified existing code to set book status to handed in if all documents are at right status

                    //TList<AartobmDocument> documentlist = documentService.GetByAaBmBookId(document.AaBmBookId);
                    AartobmDocumentQuery q = new AartobmDocumentQuery();
                    q.Append(AartobmDocumentColumn.AaBmBookId, document.AaBmBookId.ToString());
                    q.AppendNotEquals(AartobmDocumentColumn.AaBmDocId, document.AaBmDocId.ToString());
                    q.AppendNotIn(AartobmDocumentColumn.AaBmDocStatusId,
                        new string[] { ((int)AartobmStatusList.DocumentHandedInToDepot).ToString(), 
                            ((int)AartobmStatusList.DocumentHandedInAsCancelled).ToString(), 
                            ((int)AartobmStatusList.DocumentProcessedByDMS).ToString() });
                    int totalCount = 0;
                    TList<AartobmDocument> documentlist = documentService.Find(q as IFilterParameterCollection, "", 0, 1, out totalCount);

                    //bool IsHandedIn = true;
                    bool IsHandedIn = totalCount == 0;
                    //foreach (AartobmDocument ADcument in documentlist)
                    //{
                    //    if (ADcument.AaBmDocStatusId != (int)AartobmStatusList.DocumentHandedInToDepot && ADcument.AaBmDocStatusId != (int)AartobmStatusList.DocumentHandedInAsCancelled)
                    //    {
                    //        IsHandedIn = false;
                    //        break;
                    //    }
                    //}

                    if (IsHandedIn)
                    {
                        AartobmBookService bookService = new AartobmBookService();
                        AartobmBook ABook = bookService.GetByAaBmBookId(document.AaBmBookId);
                        if (ABook != null)
                        {
                            ABook.AaBmBookStatusId = (int)AartobmStatusList.BookHandedInToDepot;
                            // 2013-07-17 add LastUser by Henry
                            ABook.LastUser = lastUser;
                            ABook = bookService.Save(ABook);
                        }
                    }

                    ConnectionScope.Complete();
                }
                returnValue = 0;
                return returnValue;
            }
            else
            {
                returnValue = -3;
                return returnValue;
            }

        }

        public int ReverseDocument(string notPrefix, string docNo, string autNo, string lastUser)
        {
            AartobmDocumentService documentService = new AartobmDocumentService();
            int returnValue = -1;
            Authority authority = AuthorityManager.GetAuthority(autNo);
            if (authority == null)
            {
                // Authority No does not exists
                returnValue = -1;
                return returnValue;
            }
            AartobmDocument document = null;

            Metro metro = MetroManager.GetMetro(authority.MtrIntNo);

            decimal docId = 0;

            using (IDataReader reader = documentService.GetDocumentForHandIn(notPrefix, docNo, autNo))
            {
                while (reader.Read())
                {
                    docId = Convert.ToDecimal(reader["AaBMDocID"]);
                    break;
                }
            }

            document = new AartobmDocumentService().GetByAaBmDocId(docId);
            if (document == null)
            {
                //return document;
                // Document No does not exists
                returnValue = -2;
                return returnValue;
            }

            if (document.AaBmDocStatusId == (int)AartobmStatusList.DocumentHandedInToDepot
                || document.AaBmDocStatusId == (int)AartobmStatusList.DocumentHandedInAsCancelled)
            {
                using (ConnectionScope.CreateTransaction())
                {//2012-12-17 Nancy(When document is Reverse, update the status of book to 'Issued To Officer')
                    AartobmBookService bookService = new AartobmBookService();
                    AartobmBook ABook = bookService.GetByAaBmBookId(document.AaBmBookId);
                    if (ABook != null && ABook.AaBmBookStatusId == (int)AartobmStatusList.BookHandedInToDepot)
                    {
                        ABook.AaBmBookStatusId = (int)AartobmStatusList.BookIssuedToOfficer;
                        // 2013-07-17 add LastUser by Henry
                        ABook.LastUser = lastUser;
                        ABook = bookService.Save(ABook);
                    }

                    document.AaBmDocStatusId = (int)AartobmStatusList.DocumentIssuedToOfficer;
                    document.LastUser = lastUser;
                    document.AaBmIsCancelled = false;
                    document.AaBmIsNoAffAllowed = false;
                    //document.AaBmIsNoAffAllowed = false;
                    document = documentService.Save(document);

                    ConnectionScope.Complete();
                }

                returnValue = 0;
                return returnValue;
            }
            else
            {
                returnValue = -3;
                return returnValue;
            }


        }

        public AuthorityRuleInfo GetAutRuleByCode(int autIntNo, string autCode)
        {
            AuthorityRuleInfo autRules = new AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(autIntNo, autCode);

            return autRules;
        }

        // public AartobmDocument ProcessHandInCancelledDocument(string docNo, string autNo,bool noAffAllowed, string lastUser)
        public int ProcessHandInCancelledDocument(string notPrefix, string docNo, string autNo, bool noAffAllowed, string lastUser)
        {
            AartobmDocumentService documentService = new AartobmDocumentService();
            int returnValue = -1;
            Authority authority = AuthorityManager.GetAuthority(autNo);
            if (authority == null)
            {
                // Authority No does not exists
                returnValue = -1;
                return returnValue;
            }

            AartobmDocument document = null;

            decimal docId = 0;

            using (IDataReader reader = documentService.GetDocumentForHandIn(notPrefix, docNo, autNo))
            {
                while (reader.Read())
                {
                    docId = Convert.ToDecimal(reader["AaBMDocID"]);
                    break;
                }
            }

            Metro metro = MetroManager.GetMetro(authority.MtrIntNo);
            document = documentService.GetByAaBmDocId(docId);
            if (document == null)
            {
                // Document No does not exists
                returnValue = -2;
                return returnValue;
            }

            if (document.AaBmDocStatusId == (int)AartobmStatusList.DocumentIssuedToOfficer)
            {
                using (ConnectionScope.CreateTransaction())
                {//2012-12-17 Nancy(check the doucument is the last in the book, and update the status of book to 'hand in') 
                    document.AaBmDocStatusId = (int)AartobmStatusList.DocumentHandedInAsCancelled;
                    document.AaBmIsCancelled = true;
                    document.AaBmIsNoAffAllowed = noAffAllowed;
                    document.LastUser = lastUser;
                    document = documentService.Save(document);
                    // Hand cancelled document successful

                    TList<AartobmDocument> documentlist = documentService.GetByAaBmBookId(document.AaBmBookId);
                    bool IsHandInCancelled = true;
                    foreach (AartobmDocument ADocument in documentlist)
                    {
                        if (ADocument.AaBmDocStatusId != (int)AartobmStatusList.DocumentHandedInAsCancelled && ADocument.AaBmDocStatusId != (int)AartobmStatusList.DocumentHandedInToDepot)
                        {
                            IsHandInCancelled = false;
                            break;
                        }
                    }
                    if (IsHandInCancelled)
                    {
                        AartobmBookService bookService = new AartobmBookService();
                        AartobmBook ABook = bookService.GetByAaBmBookId(document.AaBmBookId);
                        if (ABook != null)
                        {
                            ABook.AaBmBookStatusId = (int)AartobmStatusList.BookHandedInToDepot;
                            // 2013-07-17 add LastUser by Henry
                            ABook.LastUser = lastUser;
                            ABook = bookService.Save(ABook);
                        }
                    }

                    ConnectionScope.Complete();
                }
                returnValue = 0;
                return returnValue;
            }
            else
            {
                // document status is not incorrect,can not be set to canclled
                returnValue = -3;
                return returnValue;
            }

        }
    }
}
