﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.BookManagement
{
    public class Book
    {

        //private int aaBmBookTypeId;
        private int metrIntNo;
        private int depotIntno;
        private string lastUser;
        private int bookStatusId;
        private int bookType;
        private int notPrefixIntNo;

        #region Book Members

        

        public virtual int BookType
        {
            get { return bookType; }

        }

        public int NotPrefixIntNo
        {
            get { return notPrefixIntNo; }
            set { notPrefixIntNo = value; }
        }

        public int MetrIntNo
        {
            get
            {
                return metrIntNo;
            }
            set
            {
                metrIntNo = value;
            }
        }

        public int DepotIntNo
        {
            get
            {
                return depotIntno;
            }
            set
            {
                depotIntno = value;
            }
        }

        public int BookStatusId
        {
            get
            {
                return bookStatusId;
            }
            set
            {
                bookStatusId = value;
            }
        }

        public string LastUser
        {
            get
            {
                return lastUser;
            }
            set
            {
                lastUser = value;
            }
        }

        private int bookSize;
        public virtual int BookSize
        {
            get
            {
                return bookSize;
            }

        }
        private int bookNum;
        public int BookNum
        {
            get { return bookNum; }
            set { bookNum = value; }
        }

        private int bookStartNo;
        public int BookStartNo
        {
            get { return bookStartNo; }
            set { bookStartNo = value; }
        }
        private int bookEndNo;

        public int BookEndNo
        {
            get { return bookEndNo; }
            set { bookEndNo = value; }
        }

        private int bookId;
        public int BookId
        {
            get { return bookId; }
            set { bookId = value; }
        }
        private int officerId;
        public int OfficerId
        {
            get { return officerId; }
            set { officerId = value; }
        }

        public string PrintingCompany { get; set; }

        #endregion


    }


}
