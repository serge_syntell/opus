﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace SIL.AARTO.BLL.PostalManagement
{
    public class PostalConfigManager
    {
        public static SelectList GetSelectList(string type)
        {
            Dictionary<string, string> listOr = new Dictionary<string, string>();
            listOr.Add("SPD","SPD");
            listOr.Add("RLV", "RLV");
            listOr.Add("HWO", "HWO");
            listOr.Add("VID", "VID");
            listOr.Add("ASD", "ASD");
            if (type == "1ST")
            {
                listOr.Add("NAG", "NAG");
            }
            listOr.Add("RNO","RNO");
            listOr.Add("REG", "REG");
            listOr.Add("BUS", "BUS");
            SelectList list = new SelectList(listOr, "Key", "Value");
            return list;
        }
    }
}
