﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Stalberg.TMS;

using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.Web.Resource.PostalManagement;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using Stalberg.TMS.Data;


namespace SIL.AARTO.BLL.PostalManagement
{
    
    public class PostDateNotice
    {
        private readonly string connectionString;

        readonly NoAOGDB noAogDB;

        public PostDateNotice(string connectionString)
        {
            this.connectionString = connectionString;
            this.noAogDB = new NoAOGDB(connectionString);
        }

        public int SetNoticePostedDate(string printFileName, int autIntNo, DateTime dtActualDate, string lastUser, ref string errMessage)
        {
            NoticeStatus status = NoticeStatus.First;

            string firstPostOrder = "1st";

            if (printFileName.IndexOf("2ND") < 0)
            {
                firstPostOrder = "1st";
                switch (printFileName.Substring(0, 3).ToUpper())
                {
                    case "SPD":
                    case "RLV":
                    case "HWO":
                    case "VID":
                    case "ASD":
                        firstPostOrder = "1st";
                        break;
                    case "NAG":
                        firstPostOrder = "NAG";
                        break;
                    case "RNO":
                        firstPostOrder = "1stNewOffender";
                        break;
                    case "REG":
                        firstPostOrder = "1stReg";
                        break;
                }
                //2014-12-02 Heidi added the checked for it can deal with such as "ASD_NAG_.."etc printfileName(bontq1747)
                if (!string.IsNullOrEmpty(printFileName) && printFileName.Contains("NAG"))
                {
                    firstPostOrder = "NAG";
                }
            }
            else
            {
                firstPostOrder = "2nd";
                switch (printFileName.Substring(0, 3).ToUpper())
                {
                    case "SPD":
                    case "RLV":
                    case "HWO":
                    case "VID":
                    case "ASD":
                        firstPostOrder = "2nd";
                        break;
                    case "RNO":
                        firstPostOrder = "2ndNewOffender";
                        break;
                    case "REG":
                        firstPostOrder = "2ndNewReg";
                        break;
                }
            }

            if (printFileName.IndexOf("EPR") > 0 || (printFileName.Length > 5 && printFileName.Substring(4, 3).ToUpper() == "2ND"))
            {
                status = NoticeStatus.Second;
            }
            else
            {
                switch (firstPostOrder)
                {
                    case "1st":
                        status = NoticeStatus.First;
                        break;
                    case "2nd":
                        status = NoticeStatus.Second;
                        break;
                    case "NAG":
                        status = NoticeStatus.NoAOG;
                        break;
                    case "1stNewOffender":
                        status = NoticeStatus.NewOffender;
                        break;
                    case "2ndNewOffender":
                        status = NoticeStatus.NewOffender;
                        break;
                    case "New":
                        status = NoticeStatus.NewOffender;
                        break;
                    case "1stReg":
                        status = NoticeStatus.NewRegNo;
                        break;
                    case "2ndNewReg":
                        status = NoticeStatus.NewRegNo;
                        break;
                    case "Reg":
                        status = NoticeStatus.NewRegNo;
                        break;
                }
            }

            NoticeDB db = new NoticeDB(connectionString);
            errMessage = string.Empty;
            int iRe = -1;

            iRe = db.SetPostedDate(printFileName, autIntNo, dtActualDate, lastUser, status, ref errMessage);

            switch (iRe)
            {
                //case 1:
                //    errMessage = string.Format(NoticePostRes.lblErrorText3, printFileName, firstPostOrder);
                //    break;
                case 0:
                    errMessage = string.Format(NoticePostRes.lblErrorText4, printFileName, firstPostOrder, errMessage);
                    break;
                case -1:
                    errMessage = string.Format(NoticePostRes.lblErrorText5, printFileName, firstPostOrder);
                    break;
                case -2:
                    errMessage = string.Format(NoticePostRes.lblErrorText6, printFileName, firstPostOrder);
                    break;
                case -3:
                    errMessage = string.Format(NoticePostRes.lblErrorText7, printFileName, firstPostOrder);
                    break;
                case -4:
                    errMessage = string.Format(NoticePostRes.lblErrorText8, printFileName, firstPostOrder);
                    break;
            }

            // Nick 20120917 added for report Non-summons registrations
            DateRulesDetails dateRuleIssueSum = new DateRulesDetails();
            dateRuleIssueSum.AutIntNo = autIntNo;
            dateRuleIssueSum.DtRStartDate = "NotPosted1stNoticeDate";
            dateRuleIssueSum.DtREndDate = "NotIssueSummonsDate";
            dateRuleIssueSum.LastUser = lastUser;
            DefaultDateRules ruleIssueSum = new DefaultDateRules(dateRuleIssueSum, connectionString);
            int noOfDaysToSummons = ruleIssueSum.SetDefaultDateRule();

            DataTable dt = db.GetPostedNotices(printFileName, autIntNo, ref errMessage).Tables[0];
            if (iRe == 1)
            {
                if (dt != null)
                {
                    NoticeMobileDetailService notMobDetServ = new NoticeMobileDetailService();
                    NoticeMobileDetail notMobDet;
                    int notIntNo;

                    AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = autIntNo;
                    arDetails.ARCode = "2312";
                    arDetails.LastUser = lastUser;
                    DefaultAuthRules ar = new DefaultAuthRules(arDetails, connectionString);
                    int noOfDays_ReportNonVerificate_IssueSum = ar.SetDefaultAuthRule().Key;

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (!int.TryParse(dr["NotIntNo"] == null ? "0" : dr["NotIntNo"].ToString(), out notIntNo))
                            continue;
                        notMobDet = notMobDetServ.GetByNotIntNo(notIntNo).FirstOrDefault();
                        if (notMobDet != null)
                        {
                            if (notMobDet.IsNonSummonsRegNo)
                            {
                                notMobDet.ReportBeforeSummonsDate = DateTime.Now.AddDays(noOfDaysToSummons - noOfDays_ReportNonVerificate_IssueSum);
                                notMobDet.LastUser = lastUser;
                                notMobDetServ.Update(notMobDet);
                            }
                        }
                    }
                }

                //we need to push the Summons for the NoAOG as well!!! and check data washing
                if (status == NoticeStatus.First || status == NoticeStatus.NoAOG)
                {
                    #region push the Summons
                    AuthorityDB authDB = new AuthorityDB(connectionString);
                    AuthorityDetails authDetails = authDB.GetAuthorityDetails(autIntNo);
                    string autTicketProcessor = authDetails.AutTicketProcessor;

                    DateRulesDetails dateRule = new DateRulesDetails();
                    dateRule.AutIntNo = autIntNo;
                    dateRule.DtRStartDate = "NotPosted1stNoticeDate";
                    dateRule.DtREndDate = "NotIssue2ndNoticeDate";
                    dateRule.LastUser = lastUser;
                    DefaultDateRules rule = new DefaultDateRules(dateRule, connectionString);
                    int noOfDaysTo2ndNotice = rule.SetDefaultDateRule();

                    //2014-11-13 Tommi added
                    dateRule = new DateRulesDetails();
                    dateRule.AutIntNo = autIntNo;
                    dateRule.DtRStartDate = "NotIssue2ndNoticeDate";
                    dateRule.DtREndDate = "SMSExtractOn2ndNoticeDate";
                    dateRule.LastUser = lastUser;
                    rule = new DefaultDateRules(dateRule, connectionString);
                    int noOfDaysToSMSExtractOn2ndNoticeDate = rule.SetDefaultDateRule();

                    /*
                    dateRule = new DateRulesDetails();
                    dateRule.AutIntNo = autIntNo;
                    dateRule.DtRStartDate = "NotPosted1stNoticeDate";
                    dateRule.DtREndDate = "NotIssueSummonsDate";
                    dateRule.LastUser = this.login;
                    rule = new DefaultDateRules(dateRule, connectionString);
                    int noOfDaysToSummons = rule.SetDefaultDateRule();
                    */

                    AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = autIntNo;
                    arDetails.ARCode = "4250";
                    arDetails.LastUser = lastUser;
                    DefaultAuthRules ar = new DefaultAuthRules(arDetails, connectionString);
                    bool SecNotice = ar.SetDefaultAuthRule().Value.Equals("Y");

                    //  2012-01-21 Nick add, check the data washing is enabled.
                    arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = autIntNo;
                    arDetails.ARCode = "9060";
                    arDetails.LastUser = lastUser;
                    ar = new DefaultAuthRules(arDetails, connectionString);
                    bool DWEnabled = ar.SetDefaultAuthRule().Value.Equals("Y");

                    //  Get the data washing recycle period.
                    arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = autIntNo;
                    arDetails.ARCode = "9080";
                    arDetails.LastUser = lastUser;
                    ar = new DefaultAuthRules(arDetails, connectionString);
                    int DWRecycle = ar.SetDefaultAuthRule().Key;

                    // 2013-03-27 Henry add
                    arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = autIntNo;
                    arDetails.ARCode = "5050";
                    arDetails.LastUser = lastUser;
                    KeyValuePair<int, string> aR_5050 = (new DefaultAuthRules(arDetails, connectionString)).SetDefaultAuthRule();

                    //DataSet ds = db.GetPostedNotices(printFileName, autIntNo, ref errMessage);   // Nick 20120918 moved upword.
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        //DataTable dt = ds.Tables[0];
                        QueueItemProcessor queueProcessor = new QueueItemProcessor();
                        foreach (DataRow dr in dt.Rows)
                        {
                            QueueItem pushItem = new QueueItem();

                            string NotIntNo = dr["NotIntNo"].ToString();
                            string AutCode = dr["AutCode"].ToString();
                            DateTime NotPosted1stNoticeDate = Convert.ToDateTime(dr["NotPosted1stNoticeDate"]);

                            // Get persona last data wash date
                            bool needDataWash = false;
                            string notSendTo = null;
                            string IDNum = null;

                            if (DWEnabled)
                            {
                                NoticeDB notDB = new NoticeDB(connectionString);
                                int notIntNo = Convert.ToInt32(NotIntNo);
                                NoticeDetails notDet = notDB.GetNoticeDetails(notIntNo);
                                if (notDet != null) notSendTo = (notDet.NotSendTo + "").Trim();

                                switch (notSendTo)
                                {
                                    case "D":
                                        DriverDB driverDB = new DriverDB(connectionString);
                                        DriverDetails drvDet = driverDB.GetDriverDetailsByNotice(notIntNo);
                                        if (drvDet != null) IDNum = (drvDet.DrvIDNumber + "").Trim();
                                        break;
                                    case "O":
                                        OwnerDB ownerDB = new OwnerDB(connectionString);
                                        OwnerDetails ownDet = ownerDB.GetOwnerDetailsByNotice(notIntNo);
                                        if (ownDet != null) IDNum = (ownDet.OwnIDNumber + "").Trim();
                                        break;
                                    case "P":
                                        ProxyDB proxyDB = new ProxyDB(connectionString);
                                        ProxyDetails pxyDet = proxyDB.GetProxyDetailsByNotice(notIntNo);
                                        if (pxyDet != null) IDNum = (pxyDet.PrxIDNumber + "").Trim();
                                        break;
                                    default:
                                        break;
                                }
                                //Jerry 2014-12-12 comment out it
                                //if (String.IsNullOrEmpty(IDNum))
                                //{
                                //    EntLibLogger.WriteLog("Error", "NoticePostDate", string.Format(NoticePostRes.lblErrorText10, notDet.NotTicketNo));
                                //}
                                //else
                                if (!string.IsNullOrEmpty(IDNum))
                                {

                                    MI5DB mi5 = new MI5DB(connectionString);
                                    DataSet personDet = mi5.GetPersonaDetailsByID(IDNum, lastUser);
                                    //Jerry 2014-12-12 comment out it
                                    //if (personDet == null || personDet.Tables.Count == 0 || personDet.Tables[0].Rows.Count == 0)
                                    //{
                                    //    EntLibLogger.WriteLog("Error", "NoticePostDate", string.Format(NoticePostRes.lblErrorText11, notDet.NotTicketNo));
                                    //}
                                    //else
                                    if (personDet != null && personDet.Tables.Count > 0 && personDet.Tables[0].Rows.Count > 0)
                                    {
                                        string strLastSentDWDate = personDet.Tables[0].Rows[0]["MIPSentToDataWasherDate"] + "";
                                        string strLastDWDate = personDet.Tables[0].Rows[0]["MIPLastWashedDate"] + "";
                                        DateTime? lastSentDWDate = strLastSentDWDate == "" ? null : (DateTime?)Convert.ToDateTime(strLastSentDWDate);
                                        DateTime? lastDWDate = strLastDWDate == "" ? null : (DateTime?)Convert.ToDateTime(strLastDWDate);

                                        if ((!lastSentDWDate.HasValue && !lastDWDate.HasValue) ||
                                            (lastSentDWDate.HasValue && !lastDWDate.HasValue && Convert.ToDateTime(lastSentDWDate).AddMonths(DWRecycle) < DateTime.Now))
                                        {
                                            needDataWash = true;
                                        }
                                        else
                                        {
                                            lastSentDWDate = lastSentDWDate.HasValue ? lastSentDWDate : DateTime.Parse("1900-01-01");
                                            if (lastSentDWDate < lastDWDate && Convert.ToDateTime(lastDWDate).AddMonths(DWRecycle) < DateTime.Now)
                                            {
                                                needDataWash = true;
                                            }
                                        }
                                    }
                                }
                            }

                            //no second notice for a NoAOG
                            if (status == NoticeStatus.First && SecNotice)
                            {
                                //Oscar 20120412 changed group
                                int frameIntNo = new NoticeDB(connectionString).GetFrameForNotice(Convert.ToInt32(NotIntNo));
                                string violationType = new FrameDB(connectionString).GetViolationType(frameIntNo);

                                pushItem = new QueueItem();
                                pushItem.Body = NotIntNo;
                                pushItem.Group = string.Format("{0}|{1}", AutCode.Trim(), violationType);
                                pushItem.ActDate = NotPosted1stNoticeDate.AddDays(noOfDaysTo2ndNotice);
                                pushItem.QueueType = ServiceQueueTypeList.Generate2ndNotice;
                                queueProcessor.Send(pushItem);

                                queueProcessor.Send(new QueueItem() { 
                                 Body=NotIntNo,
                                 Group=AutCode,
                                 ActDate = NotPosted1stNoticeDate.AddDays(noOfDaysTo2ndNotice + noOfDaysToSMSExtractOn2ndNoticeDate),
                                   QueueType = ServiceQueueTypeList.SMSExtractOn2ndNotice
                                });

                                // 20120222 Nick add, push the data wash queue
                                if (needDataWash)
                                {
                                    dateRule = new DateRulesDetails();
                                    dateRule.AutIntNo = autIntNo;
                                    dateRule.DtRStartDate = "DataWashDate";
                                    dateRule.DtREndDate = "NotIssue2ndNoticeDate";
                                    dateRule.LastUser = lastUser;
                                    rule = new DefaultDateRules(dateRule, connectionString);
                                    int daysDWTo2ndNotice = rule.SetDefaultDateRule();

                                    pushItem = new QueueItem();
                                    pushItem.Body = NotIntNo;
                                    pushItem.Group = AutCode;
                                    pushItem.ActDate = NotPosted1stNoticeDate.AddDays(noOfDaysTo2ndNotice - daysDWTo2ndNotice);
                                    pushItem.QueueType = ServiceQueueTypeList.DataWashingExtractor;
                                    queueProcessor.Send(pushItem);
                                }
                            }

                            //dls 2012-03-22 made a decision to push the Summons DW Queue here as well in case the Export from 2ndNotice does not return in time. Extractor rules will stop the ID from being sent more than once within a recycle period
                            //else
                            //{

                            //only need to datawash for summons is we are using these ticket processors1
                            if (needDataWash && (autTicketProcessor == "TMS" || autTicketProcessor == "AARTO"))
                            {
                                dateRule = new DateRulesDetails();
                                dateRule.AutIntNo = autIntNo;
                                dateRule.DtRStartDate = "DataWashDate";
                                dateRule.DtREndDate = "NotIssueSummonDate";
                                dateRule.LastUser = lastUser;
                                rule = new DefaultDateRules(dateRule, connectionString);
                                int daysDWToSummon = rule.SetDefaultDateRule();

                                pushItem = new QueueItem();
                                pushItem.Body = NotIntNo;
                                pushItem.Group = AutCode;
                                pushItem.ActDate = NotPosted1stNoticeDate.AddDays(noOfDaysToSummons - daysDWToSummon);
                                pushItem.QueueType = ServiceQueueTypeList.DataWashingExtractor;
                                queueProcessor.Send(pushItem);
                            }
                            //}

                            // Oscar 20120315 changed the Group
                            NoticeDetails notice = db.GetNoticeDetails(int.Parse(NotIntNo));
                            string[] group = new string[3];
                            group[0] = AutCode.Trim();
                            //Jerry 2013-03-07 change
                            //group[1] = notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) ? "H" : "";
                            group[1] = (notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) && !notice.IsVideo) ? "H" : "";
                            group[2] = Convert.ToString(notice.NotCourtName);

                            // Oscar 2013-06-17 added
                            var isNoAog = noAogDB.IsNoAOG(Convert.ToInt32(NotIntNo), NoAOGQueryType.Notice);

                            pushItem = new QueueItem();
                            pushItem.Body = NotIntNo;
                            //pushItem.Group = AutCode;
                            // Oscar 20120315 changed the Group
                            pushItem.Group = string.Join("|", group);
                            pushItem.ActDate = isNoAog ? noAogDB.GetNoAOGGenerateSummonsActionDate(autIntNo, NotPosted1stNoticeDate) : NotPosted1stNoticeDate.AddDays(noOfDaysToSummons);
                            pushItem.QueueType = ServiceQueueTypeList.GenerateSummons;
                            //jerry 2012-03-31 add Priority
                            pushItem.Priority = (notice.NotOffenceDate.Date - DateTime.Now.Date).Days;
                            //Jerry 2012-05-14 add last user
                            pushItem.LastUser = lastUser;
                            queueProcessor.Send(pushItem);

                            //Henry 2013-03-27 add
                            if (aR_5050.Value.Trim().ToUpper() == "Y")
                            {
                                pushItem = new QueueItem();
                                pushItem.Body = NotIntNo;
                                pushItem.Group = string.Join("|", group);
                                pushItem.ActDate = notice.NotOffenceDate.AddMonths(aR_5050.Key).AddDays(-5);
                                pushItem.QueueType = ServiceQueueTypeList.GenerateSummons;
                                pushItem.Priority = (notice.NotOffenceDate.Date - DateTime.Now.Date).Days;
                                pushItem.LastUser = lastUser;
                                queueProcessor.Send(pushItem);
                            }
                        }
                    }
                    #endregion
                }
            }

            return iRe;
        }
    }
}
