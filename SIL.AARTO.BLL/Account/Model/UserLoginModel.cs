﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.BLL.Account.Model
{
    public class UserLoginModel
    {
        public string UserLoginName { get; set; }
        public string UserPassword { get; set; }
        public SelectList LocalAuthoryList { get; set; }
        public int LAuthorityIntNo { get; set; }

        public bool IsValid
        {
            get { return (GetRuleViolations().Count() == 0); }
        }

        public IEnumerable<RuleViolation> GetRuleViolations()
        {
            if (String.IsNullOrEmpty(UserLoginName.Trim()))
            {
                yield return new RuleViolation("UserLoginName", "User login name is required!");
            }
            if (String.IsNullOrEmpty(UserPassword.Trim()))
            {
                yield return new RuleViolation("UserPassword", "Password is required!");
            }
            //if (LAuthorityIntNo == 0)
            //{
            //    yield return new RuleViolation("LocalAuthority", "Local auathority is required!");
            //}
        }
    }
}
