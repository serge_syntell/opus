﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Enum;
using System.Security.Cryptography;
using System.Text.RegularExpressions;


namespace SIL.AARTO.BLL.Account
{
    

     
    public class UserLoginManager
    {

        public static User GetUserByUserLoginName(string userLoginName)
        {
            return new UserService().GetByUserLoginName(userLoginName);
        }

        public static User GetUserByUserID(int userID)
        {
            return new UserService().GetByUserIntNo(userID) ;
        }

        public static List<AartoUserRoleConjoin> GetUserRolesByUserIntNo(int userIntNo)
        {
            List<AartoUserRoleConjoin> returnList = new List<AartoUserRoleConjoin>();
            foreach (AartoUserRoleConjoin role in
                new AartoUserRoleConjoinService().GetByAaUserId(userIntNo))
            {
                returnList.Add(role);
            }

            return returnList;
        }

        public static AartoLoginSynchronization GetLoginSynchronizationByUserIDAndAutID(int userID, int authID)
        {
            AartoLoginSynchronization aartoLoginSynchronization = null;
            aartoLoginSynchronization= new AartoLoginSynchronizationService().GetByAaLogSynAutIntNoAaLogSynUserId(authID, userID);

            if (aartoLoginSynchronization == null)
            {
                aartoLoginSynchronization = new AartoLoginSynchronization();
            }
            return aartoLoginSynchronization;
        }

        public static AartoLoginSynchronization GetLoginSynchronizationByKey(string key)
        {
            AartoLoginSynchronization aartoLoginSynchronization = null;
            aartoLoginSynchronization = new AartoLoginSynchronizationService().GetByAaLogSynSecretKey(key);

            if (aartoLoginSynchronization == null)
            {
                aartoLoginSynchronization = new AartoLoginSynchronization();
            }
            return aartoLoginSynchronization;
        }

        public static  AartoPageList GetPageByPageID(int pageID)
        {
            try {
                return new AartoPageListService().GetByAaPageId(pageID);
            }
            catch {
                return null;
            }
        }

        public static AartoPageList GetPageByUrl(string url)
        {
            int totalCount = 0;
            TList<AartoPageList> list = new AartoPageListService().GetPaged(" AaPageUrl ='" + url + "' ", "AaPageUrl", 0, 0, out totalCount);
            if (list.Count > 0)
                return list[0];
            else
                return new AartoPageList();
        }

        public static AartoLoginSynchronization RemoveSecrectKeyByUserIDAndAuthID(int userID, int authID)
        {
            AartoLoginSynchronizationService service = new AartoLoginSynchronizationService();
            AartoLoginSynchronization aartoLoginSynchronization = null;
            aartoLoginSynchronization = service.GetByAaLogSynAutIntNoAaLogSynUserId(authID, userID);
            if (aartoLoginSynchronization != null)
            {
                aartoLoginSynchronization.AaLogSynSecretKey = aartoLoginSynchronization.AaLogSynId.ToString();
                aartoLoginSynchronization.LastUser = new UserService().GetByUserIntNo(userID).UserLoginName;
            }
            return service.Save(aartoLoginSynchronization);

        }

        public static AartoLoginSynchronization CreateUserKeyByUserIDAndAutID(int userID, int authID)
        {
            AartoLoginSynchronization aartoLoginSynchronization = null;
            AartoLoginSynchronizationService service = new AartoLoginSynchronizationService();
            aartoLoginSynchronization = service.GetByAaLogSynAutIntNoAaLogSynUserId(authID, userID);
            
            if (aartoLoginSynchronization == null)
            {
                aartoLoginSynchronization = new AartoLoginSynchronization();
                aartoLoginSynchronization.AaLogSynAutIntNo = authID;
                aartoLoginSynchronization.AaLogSynUserId = userID;
                //aartoLoginSynchronization.LastUser = new UserService().GetByUserIntNo(userID).UserLoginName;
                //aartoLoginSynchronization.AaLogSynSecretKey = Guid.NewGuid().ToString(); 
            }
            aartoLoginSynchronization.AaLogSynSecretKey = Encrypt.MD5(Guid.NewGuid().ToString());
            aartoLoginSynchronization.LastUser = new UserService().GetByUserIntNo(userID).UserLoginName;
            aartoLoginSynchronization = service.Save(aartoLoginSynchronization);
            
            return aartoLoginSynchronization;
        }


        /// <summary>
        /// Log User Account Activity.
        /// </summary>
        /// <param name="userIntNo"></param>
        /// <param name="activity"></param>
        /// <returns></returns>
        public static bool CreateAccountActivity(int userIntNo, UserAccountActivity activity, Guid? guid = null)
        {
            var context = new SIL.AARTO.Web.DAL.OpusDB();

            // Expire old links.
            var linksToExpire = context.UserAccountActivities.Where(x => x.UserID == userIntNo && x.Active == true);

            foreach (var link in linksToExpire)
            {
                link.Active = false;
            }

            // Create and Add new link
            var accountActivity = new Web.DAL.UserAccountActivity();

            accountActivity.Activity = activity.ToString();
            accountActivity.ActivityDateTime = DateTime.Now;
            accountActivity.RestLinkGuid = guid.ToString();
            accountActivity.UserID = userIntNo;
            accountActivity.Active = true;

            context.UserAccountActivities.Add(accountActivity);
            context.SaveChanges();

            return true;
        }


        public static int? CheckResetLinkId(string RestLinkGuid)
        {
            var context = new SIL.AARTO.Web.DAL.OpusDB();
            var accountActivity = context.UserAccountActivities.Where(x => x.RestLinkGuid == RestLinkGuid).FirstOrDefault();

            if (accountActivity.Active)
            {
                accountActivity.Active = false;
                context.SaveChanges();

                if ( (DateTime.Now - accountActivity.ActivityDateTime).TotalDays <= 1)
                    return accountActivity.UserID;
            }

            return null;
        }


        public static bool ChangePassword(string loginName, string newPassword)
        {
            if (ValidatePassword(newPassword).Count() > 0)
                return false;

            var DB = new SIL.AARTO.Web.DAL.OpusDB();
            var user = DB.Users.Where(x => x.UserLoginName == loginName).FirstOrDefault();

            int expiryDays = new SIL.AARTO.DAL.Services.SysParamService().GetBySpColumnName(SIL.AARTO.DAL.Entities.SysParamList.UserPasswordExpiryDays.ToString()).SpIntegerValue;

            if (user == null)
                return false;

            user.UserPassword = HashPassword(newPassword);
            user.UserPasswordReset = false;
            user.UserPasswordGraceLoginCount = 1;
            user.UserPasswordExpiryDate = DateTime.Now.AddDays(expiryDays);
            user.LastUser = loginName;

            DB.SaveChanges();

            return true;
        }

        public static IEnumerable<string> ValidatePassword(string password)
        {
            var containsLowerCase = new Regex("[a-z]{1}");
            var containsUpperCase = new Regex("[A-Z]{1}");
            var containsDigitCase = new Regex("\\d{1}");

            if (!containsDigitCase.IsMatch(password))
                yield return "Password needs to contain at least one Number.";

            if (!containsLowerCase.IsMatch(password))
                yield return "Password needs to contain at least one Lower Case letter.";

            if (!containsUpperCase.IsMatch(password))
                yield return "Password needs to contain at least one Upper Case letter.";

            if (password.Length < 6 || password.Length  > 10)
                yield return "Password should be between 6 and 10 characters.";
        }

        protected static string HashPassword(string plainMessage)
        {
            byte[] data = Encoding.UTF8.GetBytes(plainMessage);
            using (HashAlgorithm sha = new SHA256Managed())
            {
                byte[] encryptedBytes = sha.TransformFinalBlock(data, 0, data.Length);
                return Convert.ToBase64String(sha.Hash);
            }
        }

        
    }
}
