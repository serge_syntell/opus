﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.BLL.TraceAndTrack.Model;
using System.Web.Mvc;
using System.Collections;
using System.Reflection;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;


namespace SIL.AARTO.BLL.TraceAndTrack
{
    public class TraceAndTrackInfoManager
    {
        public IEnumerable<SelectListItem> ToListItem<T>()
        {
            List<SelectListItem> listItem = new List<SelectListItem>();
            Type enumType = typeof(T);
            foreach (int s in System.Enum.GetValues(typeof(T)))
            {
                listItem.Add(new SelectListItem()
                {
                    Value = s.ToString(),
                    Text = System.Enum.GetName(typeof(T), s)
                });
            }
            return listItem;
        }

        public List<SelectListItem> GetOwnerTypeList()
        {
            List<SelectListItem> listItem = new List<SelectListItem>();
            OwnerTypeService service=new OwnerTypeService();
            List<OwnerType> list = service.GetAll().ToList();
            foreach(var item in list)
            {
                listItem.Add(new SelectListItem() { 
                    Value=item.TooIntNo.ToString(),
                    Text=item.TooDescr
                });
            }
            return listItem;
        }

        public void GetNameAndAddressList(ref List<TtAlternateNameAndAddress> nameAndAddressList,Hashtable ht,int pageIndex,int PageSize,ref int totalCount)
        {
            TtAlternateNameAndAddressService service = new TtAlternateNameAndAddressService();
            if (ht.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("1=1 AND ");
                foreach (string str in ht.Keys)
                {
                    object valueData = ht[str];
                    if (valueData != null)
                    {
                        valueData = valueData.ToString().Replace("'", "''");
                        sb.Append(string.Format("{0}='{1}' AND ", str, valueData));
                    }
                }
                string whereStr = sb.ToString();
                whereStr = whereStr.Substring(0, whereStr.Length - 4);
                nameAndAddressList = service.GetPaged(whereStr, "TTANAIntNo", pageIndex, PageSize,out totalCount).ToList();
            }
        }
        public string GetOneEditModel(string regNo)
        {
            TtAlternateNameAndAddressService service = new TtAlternateNameAndAddressService();
            TtAlternateNameAndAddress nameAndAddress = service.GetByRegistrationNumber(regNo);
            TraceAndTrackInfoModel model = new TraceAndTrackInfoModel();
            PropertyInfo[] properties = model.GetType().GetProperties();
            StringBuilder sb = new StringBuilder();
            string flag ="";
            if (nameAndAddress != null)
            {
                foreach (PropertyInfo property in properties)
                {
                    string name = property.Name;
                    PropertyInfo namePro = nameAndAddress.GetType().GetProperty(name,
                        BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase);
                    if (namePro != null)
                    {
                        object obj = namePro.GetValue(nameAndAddress, null);
                        if (obj != null)
                            sb.Append("{"+string.Format("name:'{0}',value:'{1}'", name, obj)+"},");
                    }
                }
                flag = sb.ToString();
            }
            if(flag!="")
                return ("["+flag.Substring(0,flag.Length-1)+"]");
            else
                return flag;
        }
        /// <summary>
        /// save and update TtAlternateNameAndAddress
        /// </summary>
        /// <param name="model"></param>
        // 2013-07-17 add parameter lastUser by Henry
        public void InvokeSaveAndUpdateNameAndAddress(TraceAndTrackInfoModel  model, string lastUser)
        {
            TtAlternateNameAndAddressService service = new TtAlternateNameAndAddressService();
            TtAlternateNameAndAddress nameAndAddress=service.GetByRegistrationNumber(model.RegistrationNumber);
            if (nameAndAddress == null)
                nameAndAddress = new TtAlternateNameAndAddress();
            PropertyInfo[] properties = model.GetType().GetProperties();
            foreach (PropertyInfo property in properties)
            {
                string name = property.Name;
                object proValue = property.GetValue(model, null);
                PropertyInfo aartoPro = nameAndAddress.GetType().GetProperty(name,
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase);
                if (aartoPro != null)
                    aartoPro.SetValue(nameAndAddress, proValue, null);
            }
            nameAndAddress.LastUser = lastUser;
            service.Save(nameAndAddress);
        }
        //check data
        public bool IsExitsTheData(string regNo)
        {
            bool flag = false;
            TtAlternateNameAndAddressService service = new TtAlternateNameAndAddressService();
            TtAlternateNameAndAddress nameAndAddress = service.GetByRegistrationNumber(regNo);
            if (nameAndAddress != null)
                flag = true;
            return flag;
        }
        //delete the data
        public void DeleInfo(string regNo)
        {
            TtAlternateNameAndAddressService service = new TtAlternateNameAndAddressService();
            TtAlternateNameAndAddress nameAndAddress = service.GetByRegistrationNumber(regNo);
            service.Delete(nameAndAddress);
        }
    }
}
