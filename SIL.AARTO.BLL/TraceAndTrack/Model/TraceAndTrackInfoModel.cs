﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using SIL.AARTO.DAL.Entities;
using System.Configuration;
using SIL.AARTO.Web.Resource.TraceAndTrack;

namespace SIL.AARTO.BLL.TraceAndTrack.Model
{
  public  class TraceAndTrackInfoModel
    {
        public int PageSize = int.Parse(ConfigurationManager.AppSettings["PageSize"]);
        public int TotalCount { get; set;}
        public string URLPara { get; set; }

        [Required(ErrorMessageResourceName = "RegistrationNumber_Is_Required",ErrorMessageResourceType=typeof(TraceAndTrackInfo_cshtml))]
        [StringLength(10,ErrorMessageResourceName = "RegistrationNumber_Is_Length",ErrorMessageResourceType=typeof(TraceAndTrackInfo_cshtml))]
        public string RegistrationNumber { get; set; }

        public string SearchRegistrationNumber { get; set; }

        [Required(ErrorMessageResourceName = "TypeofOwner_Is_Required", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string TypeofOwner { get; set; }

        [Required(ErrorMessageResourceName = "OwnerResponsiblePerson_Is_Required", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        [StringLength(50, ErrorMessageResourceName = "OwnerResponsiblePerson_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string OwnerResponsiblePerson { get; set; }

        [Required(ErrorMessageResourceName = "IdentityNumber_Is_Required", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        [StringLength(25, ErrorMessageResourceName = "IdentityNumber_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string IdentityNumber { get; set; }

        [StringLength(100, ErrorMessageResourceName = "ForeNames_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string ForeNames { get; set; }

        [Required(ErrorMessageResourceName = "Initials_Is_Required", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
      [StringLength(10, ErrorMessageResourceName = "Initials_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string Initials { get; set; }

      [StringLength(100, ErrorMessageResourceName = "CompanyName_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string CompanyName { get; set; }

        [Required(ErrorMessageResourceName = "TTStAdd1_Is_Required", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
      [StringLength(100, ErrorMessageResourceName = "TTStAdd1_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string TTStAdd1 { get; set; }

        [Required(ErrorMessageResourceName = "TTStAdd2_Is_Required", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
      [StringLength(100, ErrorMessageResourceName = "TTStAdd2_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string TTStAdd2 { get; set; }

      [StringLength(100, ErrorMessageResourceName = "TTStAdd3_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string TTStAdd3 { get; set; }
      [StringLength(100, ErrorMessageResourceName = "TTStAdd4_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string TTStAdd4 { get; set; }

        [Required(ErrorMessageResourceName = "TTStCode_Is_Required", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
      [StringLength(10, ErrorMessageResourceName = "TTStCode_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string TTStCode { get; set; }

        [Required(ErrorMessageResourceName = "TTPoAdd1_Is_Required", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
      [StringLength(100, ErrorMessageResourceName = "TTPoAdd1_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string TTPoAdd1 { get; set; }

        [Required(ErrorMessageResourceName = "TTPoAdd2_Is_Required", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
      [StringLength(100, ErrorMessageResourceName = "TTPoAdd2_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string TTPoAdd2 { get; set; }

      [StringLength(100, ErrorMessageResourceName = "TTPoAdd3_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string TTPoAdd3 { get; set; }
      [StringLength(100, ErrorMessageResourceName = "TTPoAdd4_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string TTPoAdd4 { get; set; }

        [Required(ErrorMessageResourceName = "TTPoCode_Is_Required", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
      [StringLength(10, ErrorMessageResourceName = "TTPoCode_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string TTPoCode { get; set; }

        [DataType(DataType.EmailAddress)]
        [StringLength(100, ErrorMessageResourceName = "EmailAddress_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string EmailAddress { get; set; }
      [StringLength(20, ErrorMessageResourceName = "TelephoneNumber_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string TelephoneNumber { get; set; }
      [StringLength(20, ErrorMessageResourceName = "MobileNumber_Is_Length", ErrorMessageResourceType = typeof(TraceAndTrackInfo_cshtml))]
        public string MobileNumber { get; set; }

        public string LastUser { get; set; }
        public string actionMode { get; set; }

        public List<TtAlternateNameAndAddress> TraceAndTrackInfos { get; set; }
    }
}
