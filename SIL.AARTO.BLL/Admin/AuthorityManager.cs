﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Resource;

namespace SIL.AARTO.BLL.Admin
{
    public class AuthorityManager
    {
        public static Authority GetAuthority(string autNo)
        {
            return new AuthorityService().GetByAutNo(autNo);
        }

        public static Authority GetAuthorityByAuthIntNo(int authIntNo)
        {
            return new AuthorityService().GetByAutIntNo(authIntNo);
        }

        public List<Authority> GetAllAuthorityForDropDown()
        {
            List<Authority> autList = new AuthorityService().GetAll().OrderBy(r => r.AutName).ToList();
            foreach (Authority a in autList)
                a.AutName = a.AutName + " (" + a.AutCode.Trim() + ")";

            Authority auth = new Authority();
            auth.AutIntNo = 0;
            auth.AutName = Global.SelectAut;
            autList.Insert(0, auth);
            return autList;
        }
    }
}
