﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Admin
{
    public class OffenceTypeLookupLangManager
    {
        private static OffenceTypeLookupService lookupService = new OffenceTypeLookupService();
        public List<LanguageLookupEntity> GetListBySourceTableID(string ocTIntNo)
        {
            List<LanguageLookupEntity> languageList = new List<LanguageLookupEntity>();
            
            IDataReader reader = lookupService.GetColumnOcTDescr(int.Parse(ocTIntNo));
            while (reader.Read())
            {
                languageList.Add(new LanguageLookupEntity()
                {
                    LookUpId = ocTIntNo,
                    LsCode = (string)reader["LSCode"],
                    LsDescription = (string)reader["LsDescription"],
                    LookupValue = reader["OcTDescr"] as string ?? string.Empty,
                    IsDefault = (bool)reader["LSIsDefault"] == true ? 1 : 0
                });
            }
            return languageList;
        }
        
    }
}

