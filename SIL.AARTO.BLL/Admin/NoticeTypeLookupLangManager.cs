﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Admin
{
    public class NoticeTypeLookupLangManager
    {
        private static NoticeTypeLookupService lookupService = new NoticeTypeLookupService();
        public List<LanguageLookupEntity> GetListBySourceTableID(string nTIntNo)
        {
            List<LanguageLookupEntity> languageList = new List<LanguageLookupEntity>();
            
            IDataReader reader = lookupService.GetColumnNTDescr(int.Parse(nTIntNo));
            while (reader.Read())
            {
                languageList.Add(new LanguageLookupEntity()
                {
                    LookUpId = nTIntNo,
                    LsCode = (string)reader["LSCode"],
                    LsDescription = (string)reader["LsDescription"],
                    LookupValue = reader["NTDescr"] as string ?? string.Empty,
                    IsDefault = (bool)reader["LSIsDefault"] == true ? 1 : 0
                });
            }
            return languageList;
        }
        
    }
}

