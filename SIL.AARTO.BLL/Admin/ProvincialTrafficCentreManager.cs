﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;

namespace SIL.AARTO.BLL.Admin
{
    public class ProvincialTrafficCentreManager
    {
        private static readonly ProvincialTrafficCentreService service = new ProvincialTrafficCentreService();

        public static TList<ProvincialTrafficCentre> GetAll()
        {
            return service.GetAll();
        }

        public static TList<ProvincialTrafficCentre> GetPage(int start, int pageLength, out int totalCount)
        {
            return service.GetPaged(start, pageLength, out totalCount);
        }

        public static TList<ProvincialTrafficCentre> Search(string searchStr, int start, int pageLength, out int count)
        {
            ProvincialTrafficCentreQuery query = new ProvincialTrafficCentreQuery();
            query.AppendContains("or", ProvincialTrafficCentreColumn.PtcCode, searchStr);
            query.AppendContains("or", ProvincialTrafficCentreColumn.PtcDescription, searchStr);
            return service.Find(query.GetParameters(), "PaBrCode", start, pageLength, out count);
        }

        // 2013-07-16 commented by Henry for no use task 4969 LastUser
        //public static void Add(ProvincialTrafficCentre provincialTrafficCentre)
        //{
        //    service.Insert(provincialTrafficCentre);
        //}

        public static void Update(ProvincialTrafficCentre provincialTrafficCentre)
        {
            service.Update(provincialTrafficCentre);
        }

        public static void Save(ProvincialTrafficCentre provincialTrafficCentre)
        {
            service.Save(provincialTrafficCentre);
        }

        public static ProvincialTrafficCentre GetByPTCIntNo(int pTCIntNo)
        {
            return service.GetByPtcIntNo(pTCIntNo);
        }

        public static void Delete(int pTCIntNo)
        {
            ProvincialTrafficCentre provincialTrafficCentre = service.GetByPtcIntNo(pTCIntNo);
            service.Delete(provincialTrafficCentre);
        }

    }
}
