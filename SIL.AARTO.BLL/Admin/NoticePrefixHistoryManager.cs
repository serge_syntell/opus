﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Data;
using SIL.AARTO.DAL.Data;

namespace SIL.AARTO.BLL.Admin
{
    public class NotPrefixItem
    {
        public NotPrefixItem(int nphIntNo, string notPrefix)
        {
            this.NPHIntNo = nphIntNo;
            this.NotPrefix = notPrefix;
        }
        public int NPHIntNo { get; set; }
        public string NotPrefix { get; set; }
    }

    public class NoticePrefixHistoryManager
    {
        private static NoticePrefixHistoryService notPrefixService = new NoticePrefixHistoryService();

        public List<NotPrefixItem> GetPrefixByType(string notType, int mtrIntNo)
        {
            List<NotPrefixItem> items = new List<NotPrefixItem>();




            using (IDataReader reader = new NoticePrefixHistoryService().GetNoticePrefixListForTheMetroWithAutCode(mtrIntNo, notType,"H"))
            {
                while (reader.Read())
                {
                   
                    NotPrefixItem nPrefixItem = new NotPrefixItem(0,"");
                    nPrefixItem.NPHIntNo = Convert.ToInt32(reader["NPHIntNo"]);
                    nPrefixItem.NotPrefix = reader["DLLText"].ToString();
                    items.Add(nPrefixItem);
                }
                return items.OrderBy(i => i.NotPrefix).ToList();

            }

            //TList<Authority> authories = new AuthorityService().GetByMtrIntNo(mtrIntNo);
            //Authority aut = null;
            //Dictionary<string, NoticePrefixHistory> dicts = new Dictionary<string, NoticePrefixHistory>();

            //foreach (NoticePrefixHistory prefix in notPrefixService.GetAll().Where(n => n.NphType == notType).ToList())
            ////notPrefixService.GetAll().Where(n => n.NphType == notType).ToList().ForEach(delegate(NoticePrefixHistory prefix)
            //{
            //    aut = authories.Where(a => a.AutIntNo == prefix.AuthIntNo).FirstOrDefault();
            //    if (dicts.Keys.Contains(aut.AutCode.Trim()))
            //    {
            //        NoticePrefixHistory his = dicts[aut.AutCode.Trim()];

            //        if (his.Nprefix.Trim() != prefix.Nprefix.Trim())
            //        {
            //            dicts.Add(aut.AutCode.Trim(), his);
            //        }
            //    }
            //    else
            //    {
            //        dicts.Add(aut.AutCode.Trim(), prefix);
            //    }

            //    //if (authories.Where(a => a.AutIntNo == prefix.AuthIntNo).Count() > 0
            //    //    && items.Where(t => t.NotPrefix == prefix.Nprefix).Count() == 0)
            //    //{
            //    //    items.Add(new NotPrefixItem(prefix.NphIntNo, String.Format("{0}~{1}",prefix.Nprefix));
            //    //}
            //}



            //foreach (string autCode in dicts.Keys)
            //{
            //    items.Add(new NotPrefixItem(dicts[autCode].NphIntNo, String.Format("{0}~{1}", autCode, dicts[autCode].Nprefix)));
            //}

            //  return items;
        }
    }
}
