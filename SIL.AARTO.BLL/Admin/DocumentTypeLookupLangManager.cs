﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Admin
{
    public class DocumentTypeLookupLangManager
    {
        private static DocumentTypeLookupService lookupService = new DocumentTypeLookupService();
        public List<LanguageLookupEntity> GetListBySourceTableID(string docTypeID)
        {
            List<LanguageLookupEntity> languageList = new List<LanguageLookupEntity>();
            
            IDataReader reader = lookupService.GetColumnDocDescr(int.Parse(docTypeID));
            while (reader.Read())
            {
                languageList.Add(new LanguageLookupEntity()
                {
                    LookUpId = docTypeID,
                    LsCode = (string)reader["LSCode"],
                    LsDescription = (string)reader["LsDescription"],
                    LookupValue = reader["DocDescr"] as string ?? string.Empty,
                    IsDefault = (bool)reader["LSIsDefault"] == true ? 1 : 0
                });
            }
            return languageList;
        }
        
    }
}

