﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Admin;
using System.Data;

namespace SIL.AARTO.BLL.Admin
{
    public class WorkFlowRuleForCourtCommentLookupLangManager
    {
        private static WorkFlowRuleForCourtCommentLookupService lookupService = new WorkFlowRuleForCourtCommentLookupService();
        public List<LanguageLookupEntity> GetListBySourceTableID(string dRID)
        {
            List<LanguageLookupEntity> languageList = new List<LanguageLookupEntity>();

            IDataReader reader = lookupService.GetColumnWFRFCComment(int.Parse(dRID));
            while (reader.Read())
            {
                languageList.Add(new LanguageLookupEntity()
                {
                    LookUpId = dRID,
                    LsCode = (string)reader["LSCode"],
                    LsDescription = (string)reader["LsDescription"],
                    LookupValue = reader["WFRFCComment"] as string ?? string.Empty,
                    IsDefault = (bool)reader["LSIsDefault"] == true ? 1 : 0
                });
            }
            return languageList;
        }
    }
}
