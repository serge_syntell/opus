﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.BLL.Admin.Model
{
    public class RoleModel
    {
        public TList<AartoUserRole> UserRoleList { get; set; }
        public string RoleID { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }

        public bool IsValid
        {
            get { return (GetRuleViolations().Count() == 0); }
        }

        public IEnumerable<RuleViolation> GetRuleViolations()
        {
            if (String.IsNullOrEmpty(RoleName.Trim()))
            {
                yield return new RuleViolation("RoleName", "Role name is required!");
            }
            //if (String.IsNullOrEmpty(UserPassword.Trim()))
            //{
            //    yield return new RuleViolation("UserPassword", "UserPassword is required!");
            //}
            if (String.IsNullOrEmpty(RoleDescription.Trim()))
            {
                yield return new RuleViolation("RoleDescription", "Role description is required!");
            }
             

        }
    }
}
