﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

using SIL.AARTO.BLL.Utility.UserMenu;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Admin.Model
{
    public class MenuModel:UserMenuEntity
    {
        public SelectList PageList { get; set; }
        public string PageUrl { get; set; }
    }
}
