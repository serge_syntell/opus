﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Admin.Model
{
    public class DepotModel
    {
        public string DepotCode { get; set; }
        public string DepotDescr { get; set; }
        public int MetroIntNo { get; set; }
        public int MetroSearchIntNo { get; set; }
        public string LastUser { get; set; }

        public SelectList MetroSearchList { get; set; }

        public SelectList MetroList { get; set; }

        public TList<AartobmDepot> DeoptList { get; set; }
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
    }
}
