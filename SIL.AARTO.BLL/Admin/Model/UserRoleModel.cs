﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility.UserMenu;

namespace SIL.AARTO.BLL.Admin.Model
{
    public class UserRoleModel
    {
        //public int RoleID { get; set; }
        public int UserIntNo { get; set; }
        public string RoleName { get; set; }
        public SelectList NotUseRoleList { get; set; }
        public string UserName{get;set;}
        public SelectList UserRoleList { get; set; }
        public UserMenuEntityCollection UserMenuList { get; set; }
    }
}
