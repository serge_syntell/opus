﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.DAL.Entities;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.BLL.Admin.Model
{
    public class UserModel
    {
        public string SearchKey { get; set; }
        public string UserLoginName { get; set; }
        public string UserPassword { get; set; }
        public string UserEmail { get; set; }
        public string UserSName { get; set; }
        public string UserInit { get; set; }
        public string UserFName { get; set; }
        public string UserAccessLevel { get; set; }
        public int TrafficOfficerIntNo { get; set; }
        public int UserDefaultAuthIntNo { get; set; }
        public string UserIntNo { get; set; }
        public string LanguageSlectorListIntNo { get; set; }
        public bool HasErrors { get; set; }
        public int TotalCount { get; set; }
        public string URLPara { get; set; }

        public SelectList TrafficOfficerList { get; set; }
        public SelectList AuthorityList { get; set; }
        public TList<User> Users { get; set; }
        public SelectList LanguageSlectorList { get; set; }

        public bool IsValid
        {
            get { return (GetRuleViolations().Count() == 0); }
        }

        public IEnumerable<RuleViolation> GetRuleViolations()
        {
            if (String.IsNullOrEmpty(UserLoginName))
            {
                yield return new RuleViolation("UserLoginName", "User login name is required!");
            }
            //jerry 2011-11-14 add
            else
            {
                if (UserLoginName.Length > 15)
                {
                    yield return new RuleViolation("UserLoginName", "User login name must be less that 15 characters!");
                }
                //j
                UserManager userManager = new UserManager();
                if (String.IsNullOrEmpty(UserIntNo))
                {
                    if (userManager.GetUserByUserLoginName(UserLoginName) != null)
                    {
                        yield return new RuleViolation("UserLoginName", "User login name already exists!");
                    }
                }
                else
                {
                    User userEntity = userManager.GetUserByUserID(Convert.ToInt32(UserIntNo));
                    if (userEntity.UserLoginName != this.UserLoginName)
                    {
                        if (userManager.GetUserByUserLoginName(UserLoginName) != null)
                        {
                            yield return new RuleViolation("UserLoginName", "User login name already exists!");
                        }
                    }
                }
            }
            //if (String.IsNullOrEmpty(UserPassword.Trim()))
            //{
            //    yield return new RuleViolation("UserPassword", "UserPassword is required!");
            //}
            if (String.IsNullOrEmpty(UserEmail))
            {
                yield return new RuleViolation("UserEmail", "UserEmail is required!");
            }
            else
            {
                if (UserEmail.Length > 50) { yield return new RuleViolation("UserEmail", "User Email must be less than 50 characters!"); }
                if (!Validate.CheckEmail(UserEmail))
                {
                    yield return new RuleViolation("UserEmail", "Incorrent email format!");
                }
            }
            if (String.IsNullOrEmpty(UserSName))
            {
                yield return new RuleViolation("UserSName", "UserSName is required!");
            }
            if (!String.IsNullOrEmpty(UserSName) && UserSName.Length > 20)
            {
                yield return new RuleViolation("UserSName", "UserSName must be less than 20 characters!");
            }
            if (String.IsNullOrEmpty(UserInit))
            {
                yield return new RuleViolation("UserInit", "UserInit is required!");
            }
            else
            {
                if (UserInit.Length > 5) { yield return new RuleViolation("UserInit", "User Init must be less than 5 characters!"); }
            }
            if (String.IsNullOrEmpty(UserFName))
            {
                yield return new RuleViolation("UserFName", "UserFName is required!");
            }
            if (!String.IsNullOrEmpty(UserFName) && UserFName.Length > 15)
            {
                yield return new RuleViolation("UserFName", "UserFName must be less than 15 characters!");
            }
            if (UserDefaultAuthIntNo == 0)
            {
                yield return new RuleViolation("UserDefaultAuthIntNo", "UserDefaultAuthIntNo is required!");
            }
            //if (String.IsNullOrEmpty(UserAccessLevel))
            //{
            //    yield return new RuleViolation("UserAccessLevel", "UserAccessLevel is required!");
            //}
            //else
            //{
            //    if (!Validate.IsInt(UserAccessLevel))
            //    {
            //        yield return new RuleViolation("UserAccessLevel", "UserAccessLevel must be a Integer!");
            //    }
            //}

        }
    }
}
