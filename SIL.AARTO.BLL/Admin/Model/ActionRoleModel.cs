﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility.UserMenu;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Admin.Model
{
    public class ActionRoleModel
    {
        //public List<UserMenuEntity> MenuList { get; set; }
        //public List<AartoUserRole> RoleList { get; set; }
        //public List<AartoUserRole> ActionRoleList { get; set; }
        public int UserIntNo { get; set; }
        public string UserLoginName { get; set; }
        public SelectList MenuList { get; set; }
        public SelectList RoleList { get; set; }
        public SelectList ActionRoleList { get; set; }
    }
}
