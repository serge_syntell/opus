﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Admin
{
    public class LanguageLookupEntity
    {
        public string LookUpId { get; set; }
        public string LsCode { get; set; }
        public string LsDescription { get; set; }
        public string LookupValue { get; set; }
        public int IsDefault { get; set; }
        public string LastUser { get; set; }
      }
}
