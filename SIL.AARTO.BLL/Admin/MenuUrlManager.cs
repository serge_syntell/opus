﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Admin.Model;
using System.Collections;

namespace SIL.AARTO.BLL.Admin
{
    public class MenuUrlEntity
    {
        public string MenuPageUrl { get; set; }
        public string MenuPageDesc { get; set; }
        public int? MenuRoleID { get; set; }
        public int MenuPageID { get; set; }
        public string PageName { get; set; }
    }
    public class MenuUrlManager
    {
        private AartoPageListService pageService = new AartoPageListService();
        private AartoMenuService menuService = new AartoMenuService();
        public void InitMenuUrlModel(MenuModel model)
        {
            model.PageList = new SelectList(GetPageListNotUse() as IEnumerable<MenuUrlEntity>, "MenuPageID", "MenuPageUrl", model.PageUrl);
        }

        private List<MenuUrlEntity> GetAllPageList()
        {
            MenuUrlEntity entity = null;
            List<MenuUrlEntity> returnList = new List<MenuUrlEntity>();
            foreach (AartoPageList page in pageService.GetAll())
            {
                entity = new MenuUrlEntity();
                entity.MenuPageID = page.AaPageId;
                entity.MenuPageUrl = page.AaPageUrl;
                entity.MenuRoleID = page.AaUserRoleId;

                returnList.Add(entity);
            }

            return returnList;
        }

        public MenuUrlEntity GetMenuPageUrlByID(decimal menuID)
        {
            MenuUrlEntity menuUrl = new MenuUrlEntity();
            AartoMenu menu = menuService.GetByAaMeId(menuID);
            if (menu != null && menu.AaPageId != null)
            {
                AartoPageList page = pageService.GetByAaPageId(menu.AaPageId.Value);
                menuUrl.MenuPageID = page.AaPageId;
                menuUrl.MenuPageUrl = page.AaPageUrl;
                menuUrl.MenuRoleID = page.AaUserRoleId;
                menuUrl.PageName = page.AaDefaultPageName;
                menuUrl.MenuPageDesc = page.AaDefaultPageDesc;
            }
            return menuUrl;
        }

        public TList<AartoPageList> GetPageListByRoleID(int roleID)
        {
            return pageService.GetByAaUserRoleId(roleID);
        }

        public List<MenuUrlEntity> GetPageListNotUse()
        {
            List<MenuUrlEntity> returnList = new List<MenuUrlEntity>();
            MenuUrlEntity entity = null;
            int totalCount = 0;
            string where = " AaPageID NOT IN (SELECT AaPageID FROM dbo.AARTOMenu WHERE AaPageID IS NOT NULL )";
            foreach (AartoPageList page in pageService.GetPaged(where, "AaPageUrl", 0, 0, out totalCount))
            {
                entity = new MenuUrlEntity();
                entity.MenuPageID = page.AaPageId;
                entity.MenuPageUrl = page.AaPageUrl;
                entity.MenuRoleID = page.AaUserRoleId;
                returnList.Add(entity);
            }
            return returnList;
        }

        public bool CheckMenuByMenuID(decimal menuID)
        {
            bool success = true;

            try
            {
                TList<AartoMenu> menuList = menuService.GetByAaParentMeId(menuID);
                if (menuList != null && menuList.Count > 0)
                {
                    success = false;
                }
                return success;
            }
            catch
            {
                success = false;
            }
            return success;

        }

        // 2013-07-16 add parameter lastUser by Henry
        public bool AssignMenuUrl(decimal menuID, int pageID, string lastUser)
        {
            bool success = false;
            try
            {
                AartoMenu menu = menuService.GetByAaMeId(menuID);
                if (menu != null && menu.AaParentMeId!=null)
                {
                    menu.AaPageId = pageID;
                    menu.LastUser = lastUser;
                    menu = menuService.Save(menu);
                    if (menu != null)
                        success = true;
                }
            }
            catch { success = false; }
            return success;

        }

        // 2013-07-17 add parameter lastUser by Henry
        public bool RemoveMenuUrlByMenuID(decimal menuID, string lastUser)
        {
            bool success = false;
            try {
                AartoMenu menu = menuService.GetByAaMeId(menuID);
                if (menu != null)
                {
                    menu.AaPageId = null;
                    menu.LastUser = lastUser;
                    menu = menuService.Save(menu);
                    if (menu != null)
                        success = true;
                }
            }
            catch { success = false; }
            return success;
        }

        public AartoPageList GetPageDetailByPageID(int pageID)
        {
            AartoPageList page = null;
            page = pageService.GetByAaPageId(pageID);
            return page;
        }

    }
}
