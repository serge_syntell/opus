﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Admin
{
    //Jake 2013-08-28 added new json entity
    public class CourtRoomEntity {
        public int CrtRIntNo { get; set; }
        public string CrtRoomName { get; set; }
    }
    public class CourtRoomLookupLangManager
    {
        private static CourtRoomLookupService lookupService = new CourtRoomLookupService();
        public List<LanguageLookupEntity> GetListBySourceTableID(string crtRIntNo)
        {
            List<LanguageLookupEntity> languageList = new List<LanguageLookupEntity>();
            
            IDataReader reader = lookupService.GetColumnCrtRoomName(int.Parse(crtRIntNo));
            while (reader.Read())
            {
                languageList.Add(new LanguageLookupEntity()
                {
                    LookUpId = crtRIntNo,
                    LsCode = (string)reader["LSCode"],
                    LsDescription = (string)reader["LsDescription"],
                    LookupValue = reader["CrtRoomName"] as string ?? string.Empty,
                    IsDefault = (bool)reader["LSIsDefault"] == true ? 1 : 0
                });
            }
            return languageList;
        }
        
    }
}

