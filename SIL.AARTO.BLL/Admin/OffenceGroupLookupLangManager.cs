﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Admin
{
    public class OffenceGroupLookupLangManager
    {
        private static OffenceGroupLookupService lookupService = new OffenceGroupLookupService();
        public List<LanguageLookupEntity> GetListBySourceTableID(string oGIntNo)
        {
            List<LanguageLookupEntity> languageList = new List<LanguageLookupEntity>();
            
            IDataReader reader = lookupService.GetColumnOGDescr(int.Parse(oGIntNo));
            while (reader.Read())
            {
                languageList.Add(new LanguageLookupEntity()
                {
                    LookUpId = oGIntNo,
                    LsCode = (string)reader["LSCode"],
                    LsDescription = (string)reader["LsDescription"],
                    LookupValue = reader["OGDescr"] as string ?? string.Empty,
                    IsDefault = (bool)reader["LSIsDefault"] == true ? 1 : 0
                });
            }
            return languageList;
        }
        
    }
}

