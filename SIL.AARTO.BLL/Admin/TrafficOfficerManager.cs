﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;

namespace SIL.AARTO.BLL.Admin
{
    public class TrafficOfficerListItem
    {
        public int TOIntNo { get; set; }
        public string TOName { get; set; }
    }
    public class TrafficOfficerManager
    {
        private static TrafficOfficerService trafficOfficerService = new TrafficOfficerService();
        public static List<TrafficOfficerListItem> GetTrafficOfficerListItem(int metrIntNo)
        {
            // Jake 2013-05-15 added IsActive=true
            List<TrafficOfficerListItem> listItem = new List<TrafficOfficerListItem>();
            List<TrafficOfficer> list = trafficOfficerService.GetByMtrIntNo(metrIntNo).Where(t => t.IsActive).ToList();
            if (list == null)
            {
                return new List<TrafficOfficerListItem>();
            }

            foreach (TrafficOfficer officer in list)
            {
                TrafficOfficerListItem item = new TrafficOfficerListItem();

                item.TOIntNo = officer.ToIntNo;
                item.TOName = String.Format("{0}~{1}~{2}", officer.TosName, officer.ToInit, officer.ToNo);
                listItem.Add(item);
            }
            foreach (TrafficOfficer officer in list)
            {
                TrafficOfficerListItem item = new TrafficOfficerListItem();

                item.TOIntNo = officer.ToIntNo;
                item.TOName = String.Format("{0}~{1}~{2}", officer.ToNo, officer.TosName, officer.ToInit);
                listItem.Add(item);
            }

            return listItem.OrderBy(r => r.TOName).ToList();
        }

        public static List<TrafficOfficer> GetTrafficOfficersPage(int mtrIntNo, string toIdNumber, string surname, int start, int pageSize, out int totalCount)
        {
            TrafficOfficerQuery query = new TrafficOfficerQuery();
            query.AppendContains("or", TrafficOfficerColumn.ToIdNumber, toIdNumber);
            string sql = string.Format("(MtrIntNo = {0}) AND (ToIdNumber Like '%{1}%') AND (TOSName Like '%{2}%')", mtrIntNo, toIdNumber, surname);
            return trafficOfficerService.GetPaged(sql, "IsActive DESC,TosName,ToInit", start, pageSize, out totalCount).ToList();
        }

        public static TrafficOfficer GetTrafficOfficerByToIntNo(int toIntNo)
        {
            return trafficOfficerService.GetByToIntNo(toIntNo);
        }

        public static bool UpdateTrafficOfficer(TrafficOfficer officer)
        {
            TrafficOfficer dubOfficer = trafficOfficerService.GetByToIdNumber(officer.ToIdNumber);

            if (dubOfficer == null || dubOfficer.ToIntNo == officer.ToIntNo)
            {
                officer.RowVersion = trafficOfficerService.GetByToIntNo(officer.ToIntNo).RowVersion;
                return trafficOfficerService.Update(officer);
            }
            return false;
        }

        public static bool AddTrafficOfficer(TrafficOfficer officer)
        {
            TrafficOfficer oldOfficer = trafficOfficerService.GetByToIdNumber(officer.ToIdNumber);
            if (oldOfficer == null)
            {
                return trafficOfficerService.Insert(officer);
            }
            return false;
        }

        // Jake 2013-05-15 renamed function from DeleteTrafficOfficer to ArchiveTrafficOfficer
        // Added new column IsActive on TrafficOfficer table
        // 2013-07-17 add parameter lastUser by Henry
        public static bool ArchiveTrafficOfficer(int toIntNo, string lastUser)
        {

            //deleted = trafficOfficerService.Delete(trafficOfficerService.GetByToIntNo(toIntNo));
            int totalCount = 0;
            new AartobmBookService().GetPaged(String.Format("AaBMBookStatusID={0} AND TOIntNo={1}", (int)AartobmStatusList.BookIssuedToOfficer, toIntNo),
                "", 0, 1, out totalCount);
            if (totalCount > 0) return false;
            new AartobmDocumentService().GetPaged(String.Format("AaBMDocStatusID={0} AND TOIntNo={1}", (int)AartobmStatusList.DocumentIssuedToOfficer, toIntNo),
                "", 0, 1, out totalCount);
            if (totalCount > 0) return false;

            TrafficOfficer officer = trafficOfficerService.GetByToIntNo(toIntNo);
            officer.IsActive = false;
            officer.LastUser = lastUser;

            return trafficOfficerService.Save(officer) != null;

        }

        // 2013-07-17 add parameter lastUser by Henry
        public static bool ActiveTrafficOfficer(int toIntNo, string lastUser)
        {
            TrafficOfficer officer = trafficOfficerService.GetByToIntNo(toIntNo);
            if (officer != null)
            {
                if (officer.IsActive == false)
                {
                    officer.IsActive = true;
                    officer.LastUser = lastUser;
                    trafficOfficerService.Save(officer);
                    return true;
                }
            }
            return false;
        }

        public static List<TrafficOfficer> GetByOfGrIntNo(int ofGrIntNo)
        {
            return trafficOfficerService.GetByOfGrIntNo(ofGrIntNo).Where(t => t.IsActive).ToList();
        }
    }
}
