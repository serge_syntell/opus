﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Admin
{

    public class UserRoleEntity {
        public int AaUserRoleID { get; set; }
        public string AaUserRoleName { get; set; }
    }

    public class UserRoleComparer : IEqualityComparer<UserRoleEntity>
    {
        public bool Equals(UserRoleEntity x, UserRoleEntity y)
        {

            // Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            // Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            // Check whether the products' properties are equal.
            return x.AaUserRoleID == y.AaUserRoleID && x.AaUserRoleName == y.AaUserRoleName;
        }

        // If Equals() returns true for a pair of objects,
        // GetHashCode must return the same value for these objects.

        public int GetHashCode(UserRoleEntity userRole)
        {
            // Check whether the object is null.
            if (Object.ReferenceEquals(userRole, null)) return 0;

            // Get the hash code for the Name field if it is not null.
            int hashProductName = userRole.AaUserRoleName == null ? 0 : userRole.AaUserRoleName.GetHashCode();

            // Get the hash code for the Code field.
            int hashProductCode = userRole.AaUserRoleID.GetHashCode();

            // Calculate the hash code for the product.
            return hashProductName ^ hashProductCode;
        }
    }

    public class UserRoleManager
    {
        AartoUserRoleService userRoleService = new AartoUserRoleService();
        AartoUserRoleConjoinService userRoleConjoinService = new AartoUserRoleConjoinService();

        public TList<AartoUserRole> GetAllUserRole()
        {
            return userRoleService.GetAll();
        }

        public int GetMaxRoleID()
        {
            int maxRoleID = 0;
            foreach (AartoUserRole role in GetAllUserRole())
            {
                if (role.AaUserRoleId > maxRoleID)
                    maxRoleID = role.AaUserRoleId;
            }
            return maxRoleID;
        }

        public AartoUserRole SaveUserRole(AartoUserRole role)
        {
            return userRoleService.Save(role);
        }

        public AartoUserRole GetUserRoleByRoleID(int roleID)
        {
            return userRoleService.GetByAaUserRoleId(roleID);
        }

        public List<UserRoleEntity> GetAllUserRoleEntity()
        {
            List<UserRoleEntity> returnList = new List<UserRoleEntity>();
            UserRoleEntity userRoleEntity = null;
            foreach (AartoUserRole entity in GetAllUserRole())
            {
                userRoleEntity = new UserRoleEntity();
                userRoleEntity.AaUserRoleID = entity.AaUserRoleId;
                userRoleEntity.AaUserRoleName = entity.AaUserRoleName;
                returnList.Add(userRoleEntity);
            }

            //Jerry 2014-06-09 set the order by AaUserRoleName
            //return returnList;
            return returnList.OrderBy(r => r.AaUserRoleName).ToList();
        }

        public TList<AartoUserRole> GetUserRolesByUserIntNo(int userIntNo)
        {
            int totalCount = 0;
            string where = string.Format("AaUserRoleID IN (SELECT AaUserRoleID "
                + " FROM dbo.AARTOUserRoleConjoin WHERE AaUserID={0})", userIntNo);

            //Jerry 2014-06-09 change the order by
            //return userRoleService.GetPaged(where, "AaUserRoleID", 0, 0, out totalCount);
            return userRoleService.GetPaged(where, "AaUserRoleName", 0, 0, out totalCount);
        }

        public List<UserRoleEntity> GetUserRoleEntityByUserIntNo(int userIntNo)
        {
            List<UserRoleEntity> returnList = new List<UserRoleEntity>();
            UserRoleEntity userRoleEntity = null;
            foreach (AartoUserRole entity in GetUserRolesByUserIntNo(userIntNo))
            {
                userRoleEntity = new UserRoleEntity();
                userRoleEntity.AaUserRoleID = entity.AaUserRoleId;
                userRoleEntity.AaUserRoleName = entity.AaUserRoleName;
                returnList.Add(userRoleEntity);
            }

            return returnList;
        }

        public List<UserRoleEntity> GetUserRoleEntityNotUse(int userIntNo)
        {
            List<UserRoleEntity> returnList = new List<UserRoleEntity>();
             
            List<UserRoleEntity> temp = GetUserRoleEntityByUserIntNo(userIntNo);
            if (temp == null)
            {
                return GetAllUserRoleEntity();
            }
           UserRoleComparer comparer = new UserRoleComparer();
            foreach(UserRoleEntity userRole in GetAllUserRoleEntity())
            {
                if (!temp.Contains(userRole, comparer))
                 {
                     returnList.Add(userRole);
                 }
            }

            return returnList;
        }

        public TList<AartoUserRoleConjoin> AddRolesForUser(int userIntNo, List<int> roles, string lastUser)
        {
            TList<AartoUserRoleConjoin> userRoleConjoinList = new TList<AartoUserRoleConjoin>();
            AartoUserRoleConjoin userRoleConjoin = null;
            foreach (int role in roles)
            {
                userRoleConjoin = new AartoUserRoleConjoin();
                userRoleConjoin.AaUserId = userIntNo;
                userRoleConjoin.AaUserRoleId = role;
                userRoleConjoin.LastUser = lastUser;
                userRoleConjoinList.Add(userRoleConjoin);
            }
            if (userRoleConjoinList != null)
                userRoleConjoinList = userRoleConjoinService.Save(userRoleConjoinList);

            return userRoleConjoinList;
        }

        // 2013-07-17 add parameter lastUser by Henry
        public TList<AartoUserRoleConjoin> RemoveRolesFromUser(int userIntNo, List<int> roles, string lastUser)
        {
            TList<AartoUserRoleConjoin> userRoleConjoinList = new TList<AartoUserRoleConjoin>();
            foreach (AartoUserRoleConjoin userRoleConjoin in userRoleConjoinService.GetByAaUserId(userIntNo))
            {
                if (roles.Contains(userRoleConjoin.AaUserRoleId))
                {
                    userRoleConjoin.MarkToDelete();
                    userRoleConjoin.LastUser = lastUser;
                    userRoleConjoinList.Add(userRoleConjoin);
                }
            }

            return userRoleConjoinService.Save(userRoleConjoinList);

        }

        
         
    }
}
