﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;

namespace SIL.AARTO.BLL.Admin
{
    public class OfficerGroupListItem
    {
        public int OfGrIntNo{get;set;}
        public string OfGrDescr { get; set; }
    }
    public class OfficerGroupManager
    {
        private static OfficerGroupService OfficerGroupService = new OfficerGroupService();
        public static List<OfficerGroupListItem> GetOfficerGroupListItem()
        {
            List<OfficerGroupListItem> listItem = new List<OfficerGroupListItem>();
            TList<OfficerGroup> list =OfficerGroupService.GetAll();
            if (list == null)
            {
                return new List<OfficerGroupListItem>();
            }

            foreach (OfficerGroup officerGroup in list)
            {
                OfficerGroupListItem item = new OfficerGroupListItem();

                item.OfGrIntNo = officerGroup.OfGrIntNo;
                item.OfGrDescr = String.Format("{0}~{1}", officerGroup.OfGrCode.Trim(), officerGroup.OfGrDescr.Trim());
                listItem.Add(item);
            }
            //foreach (OfficerGroup officerGroup in list)
            //{
            //    OfficerGroupListItem item = new OfficerGroupListItem();

            //    item.OfGrIntNo = officerGroup.OfGrIntNo;
            //    item.OfGrDescr = String.Format("{0}~{1}", officerGroup.OfGrCode, officerGroup.OfGrDescr);
            //    listItem.Add(item);
            //}

            return listItem.OrderBy(r => r.OfGrDescr).ToList();
        }

        public static Dictionary<int, OfficerGroup> GetAllOfficerGroupKeyValues()
        {
            TList<OfficerGroup> list = OfficerGroupService.GetAll();
            return list.ToDictionary<OfficerGroup, int>(itemKey => itemKey.OfGrIntNo);
        }

        public static List<OfficerGroup> GetOfficerGroupsPage(string ofGrDescr, int start, int pageSize, out int totalCount)
        {
            //OfficerGroupQuery query = new OfficerGroupQuery();
            //query.AppendContains("or", OfficerGroupColumn.OfGrDescr, ofGrDescr);
            string sql = string.Format("OfGrDescr Like '%{0}%'", ofGrDescr);
            return OfficerGroupService.GetPaged(sql, "OfGrDescr", start, pageSize, out totalCount).ToList();
        }

        public static OfficerGroup GetOfficerGroupByOfGrIntNo(int ofGrIntNo)
        {
            return OfficerGroupService.GetByOfGrIntNo(ofGrIntNo);
        }

        public static bool UpdateOfficerGroup(OfficerGroup officerGroup)
        {
            OfficerGroup dubOfficerGroup = OfficerGroupService.GetByOfGrCode(officerGroup.OfGrCode);

            if (dubOfficerGroup == null || dubOfficerGroup.OfGrIntNo == officerGroup.OfGrIntNo)
            {
                officerGroup.RowVersion = OfficerGroupService.GetByOfGrIntNo(officerGroup.OfGrIntNo).RowVersion;
                return OfficerGroupService.Update(officerGroup);
            }
            return false;
        }

        public static bool AddOfficerGroup(OfficerGroup officerGroup)
        {
            OfficerGroup oldOfficerGroup = OfficerGroupService.GetByOfGrCode(officerGroup.OfGrCode);
            if (oldOfficerGroup == null)
            {
                return OfficerGroupService.Insert(officerGroup);
            }
            return false;
        }

        public static bool DeleteOfficerGroup(int ofGrIntNo)
        {
            bool deleted = false;
            List<TrafficOfficer> officers = TrafficOfficerManager.GetByOfGrIntNo(ofGrIntNo);
            if (officers == null || officers.Count == 0)
            {
                deleted = OfficerGroupService.Delete(OfficerGroupService.GetByOfGrIntNo(ofGrIntNo));
            }
            return deleted;
        }
    }
}
