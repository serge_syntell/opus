﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Admin
{
    public class VehicleTypeLookupLangManager
    {
        private static VehicleTypeLookupService lookupService = new VehicleTypeLookupService();
        public List<LanguageLookupEntity> GetListBySourceTableID(string vTIntNo)
        {
            List<LanguageLookupEntity> languageList = new List<LanguageLookupEntity>();
            
            IDataReader reader = lookupService.GetColumnVTDescr(int.Parse(vTIntNo));
            while (reader.Read())
            {
                languageList.Add(new LanguageLookupEntity()
                {
                    LookUpId = vTIntNo,
                    LsCode = (string)reader["LSCode"],
                    LsDescription = (string)reader["LsDescription"],
                    LookupValue = reader["VTDescr"] as string ?? string.Empty,
                    IsDefault = (bool)reader["LSIsDefault"] == true ? 1 : 0
                });
            }
            return languageList;
        }
        
    }
}

