﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;

namespace SIL.AARTO.BLL.Admin
{
    public class BookStatusListItem
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }
    public class BookStatusManager
    {
        public static List<BookStatusListItem> GetList()
        {
            List<BookStatusListItem> list = new List<BookStatusListItem>();
            int key;
            Dictionary<int, string> lookups = 
                AARTOBMStatusLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            foreach(AartobmStatus  status in new AartobmStatusService().GetAll())
            {
                if (status.AaBmIsBookStatus)
                {
                    key = status.AaBmStatusId;
                    BookStatusListItem listItem = new BookStatusListItem();
                    listItem.StatusId = status.AaBmStatusId;
                    listItem.StatusName = lookups.ContainsKey(key) ? lookups[key] : status.AaBmStatusDescription;
                    list.Add(listItem);
                }
                
            }
            return list;
        }
    }
}
