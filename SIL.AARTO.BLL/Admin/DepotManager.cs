﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Threading;
using SIL.AARTO.BLL.Utility.Cache;

namespace SIL.AARTO.BLL.Admin
{
    public class DepotListItem
    {
        public int DepotIntNo { get; set; }
        public string DepotDescription { get; set; }
    }
    
    public class DepotManager
    {

        private static AartobmDepotService depotService = new AartobmDepotService();

        public static List<DepotListItem> GetDepots(int metroIntNo)
        {
            Dictionary<int,string> lookups = AARTOBMDepotLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            List<DepotListItem> depotList = new List<DepotListItem>();
            int key;
            foreach (AartobmDepot depot in depotService.GetByMtrIntNo(metroIntNo))
            {
                key = depot.AaBmDepotId;
                DepotListItem item = new DepotListItem();
                item.DepotIntNo = key;
                item.DepotDescription = lookups.ContainsKey(key) ? lookups[key] : depot.AaBmDepotDescription;
                depotList.Add(item);
            }
            return depotList;
        }


        public TList<AartobmDepot> GetAllDeopt()
        {
            return depotService.GetAll();
        }

        public TList<AartobmDepot> GetAllDeoptPage(int pageIndex, int pageSize, out int totalCount)
        {
            int start = pageSize * pageIndex;
            return depotService.GetAll(start, pageSize, out totalCount);
        } 

        public AartobmDepot GetDepotByDepotId(int depotId)
        {
            return depotService.GetByAaBmDepotId(depotId);
        }

        public TList<AartobmDepot> GetListByMetroIntNo(int mtrIntNo)
        {
            return depotService.GetByMtrIntNo(mtrIntNo);
        }

        public TList<AartobmDepot> GetListByMetroIntNoPage(int mtrIntNo, int pageIndex, int pageSize, out int totalCount)
        {
            int start = pageSize * pageIndex;
            return depotService.GetByMtrIntNo(mtrIntNo,start, pageSize, out totalCount);
        }

        public AartobmDepot Save(AartobmDepot depot)
        {
            return depotService.Save(depot);
        }
    }
}
