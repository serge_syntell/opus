﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Admin
{
    public class DateRuleLookupLangManager
    {
        private static DateRuleLookupService lookupService = new DateRuleLookupService();
        public List<LanguageLookupEntity> GetListBySourceTableID(string dRID)
        {
            List<LanguageLookupEntity> languageList = new List<LanguageLookupEntity>();

            IDataReader reader = lookupService.GetColumnDtRDescr(int.Parse(dRID));
            while (reader.Read())
            {
                languageList.Add(new LanguageLookupEntity()
                {
                    LookUpId = dRID,
                    LsCode = (string)reader["LSCode"],
                    LsDescription = (string)reader["LsDescription"],
                    LookupValue = reader["DtRDescr"] as string ?? string.Empty,
                    IsDefault = (bool)reader["LSIsDefault"] == true ? 1 : 0
                });
            }
            return languageList;
        }
        
    }
}

