﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.BLL.Admin
{
    public class ActionMenuEntity
    {
        public decimal MenuID { get; set; }
        public int AaPageID { get; set; }
        public int ActionID { get; set; }
        public string MenuName { get; set; }
        public string MenuDescription { get; set; }
        public int ActionRoleID { get; set; }
    }


    public class ActionRoleManager
    {
        AARTOPageListCache cachePage = new AARTOPageListCache();
        AARTOMenuCache cacheMenu = new AARTOMenuCache();
        AARTOMenuLookUpCache cacheMenuLookup = new AARTOMenuLookUpCache();
        AARTOActionCache cacheAction = new AARTOActionCache();
        AARTOActionRoleConjoinCache cacheActionRole = new AARTOActionRoleConjoinCache();
        AartoUserRoleService userRoleService = new AartoUserRoleService();

        AartoActionRoleConjoinService actionService = new AartoActionRoleConjoinService();

        public List<ActionMenuEntity> GetAllActionMenu()
        {
            List<ActionMenuEntity> returnList = new List<ActionMenuEntity>();
            ActionMenuEntity actionMenu = new ActionMenuEntity() ;
            actionMenu.ActionID = 0;
            actionMenu.MenuName = GetResourcesInClassLibraries.GetResourcesValueByCustomPath(@"Views\Admin\App_LocalResources\ActionRoleManage.aspx", "selectMenu");
            returnList.Add(actionMenu);

            foreach (AartoAction action in cacheAction.GetAll())
            {
                foreach (AartoPageList page in cachePage.GetAll())
                {
                    if (page.AaPageId != 0 && page.AaPageId == action.AaPageId)
                    {
                        actionMenu = new ActionMenuEntity();
                        actionMenu.ActionID = action.AaActId;
                        //actionMenu.MenuID = menuLookup.AaMeId;
                        actionMenu.AaPageID = page.AaPageId;
                        actionMenu.MenuName = page.AaDefaultPageName;
                        actionMenu.MenuDescription = page.AaDefaultPageDesc;
                        returnList.Add(actionMenu);
                        break;
                    }
                }
            }

            return returnList;
        }

        public List<ActionMenuEntity> GetAllMenuHasAssignedPage()
        {
            List<ActionMenuEntity> returnList = new List<ActionMenuEntity>();
            ActionMenuEntity actionMenu = null;
            string where =" AaPageID IN (SELECT AaPageID FROM dbo.AARTOMenu WHERE AaPageID IS NOT NULL)";
            int totalCount = 0;
            actionMenu = new ActionMenuEntity();
            actionMenu.MenuID = 0;
            actionMenu.MenuName = "";
            returnList.Add(actionMenu);

            //foreach (AartoMenuLookup menuLookup in new AartoMenuLookupService().GetPaged(where, "AaMeID", 0, 0, out totalCount))
            //{
            //    actionMenu = new ActionMenuEntity();
            //    //actionMenu.ActionID = action.AaActId;
            //    actionMenu.MenuID = menuLookup.AaMeId;
            //    actionMenu.MenuName = menuLookup.AaMlMenuItemName;
            //    actionMenu.MenuDescription = menuLookup.AaMlMenuItemDesc;
            //    returnList.Add(actionMenu);
            //}
            foreach (AartoPageList page in new AartoPageListService().GetPaged(where, "AaPageID", 0, 0, out totalCount))
            {
                actionMenu = new ActionMenuEntity();
                //actionMenu.ActionID = action.AaActId;
                //actionMenu.MenuID = menuLookup.AaMeId;
                actionMenu.AaPageID = page.AaPageId;
                actionMenu.MenuName = page.AaDefaultPageName;//.AaMlMenuItemName;
                actionMenu.MenuDescription = page.AaDefaultPageDesc;//.AaMlMenuItemDesc;
                returnList.Add(actionMenu);
            }

            return returnList;
        }

        public ActionMenuEntity GetActionMenuEntityByMenuID(int pageID)
        {
            ActionMenuEntity actionMenu = null;
            foreach (AartoAction action in cacheAction.GetAll())
            {
                if (action.AaPageId!=null  && action.AaPageId ==pageID)
                {
                    //foreach (AartoMenuLookup menuLookup in cacheMenuLookup.GetAll())
                    //{
                    //    if (menuLookup.AaMeId == menuID && menuLookup.AaMlLanguage == language)
                    //    {
                    //        actionMenu = new ActionMenuEntity();
                    //        actionMenu.ActionID = action.AaActId;
                    //        actionMenu.MenuID = menuLookup.AaMeId;
                    //        actionMenu.MenuName = menuLookup.AaMlMenuItemName;
                    //        actionMenu.MenuDescription = menuLookup.AaMlMenuItemDesc;
                    //        break;
                    //    }
                    //}
                    foreach(AartoPageList page in cachePage.GetAll())
                    {
                        if(page.AaPageId == pageID)
                        {
                            actionMenu = new ActionMenuEntity();
                            actionMenu.ActionID = action.AaActId;
                            //actionMenu.MenuID = menuLookup.AaMeId;
                            actionMenu.AaPageID = page.AaPageId;
                            actionMenu.MenuName = page.AaDefaultPageName;//.AaMlMenuItemName;
                            actionMenu.MenuDescription = page.AaDefaultPageDesc;//.AaMlMenuItemDesc;
                            break;
                        }
                    }
                }
            }

            return actionMenu;
        }

        public ActionMenuEntity GetActionMenuEntityByActionID(int actionID)
        {
            ActionMenuEntity actionMenu = null;
            foreach (AartoAction action in cacheAction.GetAll())
            {
                if (action.AaActId == actionID)
                {
                    //foreach (AartoMenuLookup menuLookup in cacheMenuLookup.GetAll())
                    //{
                    //    if (menuLookup.AaMeId == action.AaMeId && menuLookup.AaMlLanguage == language)
                    //    {
                    //        actionMenu = new ActionMenuEntity();
                    //        actionMenu.ActionID = action.AaActId;
                    //        actionMenu.MenuID = menuLookup.AaMeId;
                    //        actionMenu.MenuName = menuLookup.AaMlMenuItemName;
                    //        actionMenu.MenuDescription = menuLookup.AaMlMenuItemDesc;
                    //        break;
                    //    }
                    //}
                    foreach(AartoPageList page in cachePage.GetAll())
                    {
                        if(page.AaPageId == action.AaPageId)
                        {
                            actionMenu = new ActionMenuEntity();
                            actionMenu.ActionID = action.AaActId;
                            //actionMenu.MenuID = menuLookup.AaMeId;
                            actionMenu.AaPageID = page.AaPageId;
                            actionMenu.MenuName = page.AaDefaultPageName;//.AaMlMenuItemName;
                            actionMenu.MenuDescription = page.AaDefaultPageDesc;//.AaMlMenuItemDesc;
                            break;
                        }
                    }
                }
            }

            return actionMenu;
        }

        public TList<AartoActionRoleConjoin> AddRolesToAction(int actionID,List<int> roles,string lastUser)
        {
            TList<AartoActionRoleConjoin> actionRoleConjoinList = new TList<AartoActionRoleConjoin>();
            AartoActionRoleConjoin actionRole = null;
            foreach (int role in roles)
            {
                actionRole = new AartoActionRoleConjoin();
                actionRole.AaActId = actionID;
                actionRole.AaUserRoleId = role;
                actionRole.LastUser = lastUser;
                actionRole.IsNew = true;
                actionRoleConjoinList.Add(actionRole);
            }
            if (actionRoleConjoinList != null)
               actionRoleConjoinList= actionService.Save(actionRoleConjoinList);

            return actionRoleConjoinList;
        }

        // 2013-07-16 add parameter lastUser by Henry
        public TList<AartoActionRoleConjoin> RemoveRolesFromAction(int actionID, List<int> roles, string lastUser)
        {
            TList<AartoActionRoleConjoin> actionRoleConjoinList =  new TList<AartoActionRoleConjoin>();

            foreach (AartoActionRoleConjoin actionRoleConjoin in actionService.GetByAaActId(actionID))
            {
                if (roles.Contains(actionRoleConjoin.AaUserRoleId))
                {
                    actionRoleConjoin.MarkToDelete();
                    actionRoleConjoin.LastUser = lastUser;
                    actionRoleConjoinList.Add(actionRoleConjoin);
                }
            }

            return actionService.Save(actionRoleConjoinList);
        }

        public List<UserRoleEntity> GetRolesByActID(int actID)
        {
            List<UserRoleEntity> returnList = new List<UserRoleEntity>();
            UserRoleEntity userRoleEntity = null;
            TList<AartoUserRole> userRoles = userRoleService.GetAll();
            foreach (AartoActionRoleConjoin actionRole in cacheActionRole.GetAll())
            {
                if (actionRole.AaActId == actID)
                {
                    foreach (AartoUserRole userRole in userRoles)
                    {
                        if (userRole.AaUserRoleId == actionRole.AaUserRoleId)
                        {
                            userRoleEntity = new UserRoleEntity();
                            userRoleEntity.AaUserRoleID = userRole.AaUserRoleId;
                            userRoleEntity.AaUserRoleName = userRole.AaUserRoleName;
                            returnList.Add(userRoleEntity);
                            break;
                        }
                    }
                }
            }

            return returnList;
        }

        public List<UserRoleEntity> GetActionRolesNotUseByActID(int actID)
        {
            List<UserRoleEntity> returnList = new List<UserRoleEntity>();

            List<UserRoleEntity> temp = GetRolesByActID(actID);
            if (temp == null)
            {
                return new UserRoleManager().GetAllUserRoleEntity();
            }
            UserRoleComparer comparer = new UserRoleComparer();
            foreach (UserRoleEntity userRole in new UserRoleManager().GetAllUserRoleEntity())
            {
                if (!temp.Contains(userRole, comparer))
                {
                    returnList.Add(userRole);
                }
            }

            return returnList;
        }

        
    }
}
