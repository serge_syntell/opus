﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Admin
{
   public class SysParamLookupLangManager
   {
       private static SysParamLookupService lookupService = new SysParamLookupService();
       public List<LanguageLookupEntity> GetListBySourceTableID(string spIntNo)
       {
           //Heidi 2014-03-25 added for maintaining multiple languages(5141)
           List<LanguageLookupEntity> languageList = new List<LanguageLookupEntity>();
           IDataReader reader = lookupService.GetColumnSPDescr(int.Parse(spIntNo));
           while (reader.Read())
           {
               languageList.Add(new LanguageLookupEntity()
               {
                   LookUpId = spIntNo,
                   LsCode = (string)reader["LSCode"],
                   LsDescription = (string)reader["LsDescription"],
                   LookupValue = reader["SPDescr"] as string ?? string.Empty,
                   IsDefault = (bool)reader["LSIsDefault"] == true ? 1 : 0
               });
           }
           return languageList;
       }
   }
}