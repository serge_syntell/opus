﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Threading;
using SIL.AARTO.BLL.Utility.Cache;
using Microsoft.Office.Interop.Word;

namespace SIL.AARTO.BLL.Admin
{
    public class MetroListItem
    {
        public int MtrIntNo { get; set; }
        public string MtrName { get; set; }
    }
    public class MetroManager
    {
        static MetroService metroService = new MetroService();
        public TList<Metro> GetList()
        {
            return metroService.GetAll();
        }

        public static List<MetroListItem> GetMetroListItem()
        {
            List<MetroListItem> list = new List<MetroListItem>();
            //MetroListItem item = new MetroListItem();
            //item.MtrIntNo = 0;
            //item.MtrName = "Please select a Metro";

            Dictionary<int, string> metroLookups = 
                MetroLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);

            foreach (Metro metro in metroService.GetAll())
            {
                MetroListItem listItem = new MetroListItem();
                listItem.MtrIntNo = metro.MtrIntNo;
                listItem.MtrName = metroLookups[metro.MtrIntNo];
                    //metroLookups.ContainsKey(metro.MtrIntNo); ? metroLookups[metro.MtrIntNo] : metro.MtrName;
                list.Add(listItem);
            }

            return list;
        }

        public static Metro GetMetro(int mtrIntNo)
        {
            return metroService.GetByMtrIntNo(mtrIntNo);
        }
    }

    
}
