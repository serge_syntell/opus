﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Admin.Model;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.BLL.Admin
{
    public class AuthorityEntity
    {
        public int AutIntNo { get; set; }
        public string AutName { get; set; }
        public string AutCode { get; set; }
        public string AutDesc { get; set; }
    }

    public class TrafficOfficerEntity
    {
        public string ToNo { get; set; }
        public int ToIntNo { get; set; }
        public string TOSName { get; set; }
        public string TOInit { get; set; }
        public string ToDesc { get; set; }

    }
    public class UserManager
    {
        UserService userService = new UserService();

        public TList<User> GetAllUser()
        {
            return userService.GetAll();
        }

        public TList<User> GetAllUser(string surName, int pageIndex, int pageSize, out int totalCount)
        {
            string where = String.Empty;
            if (!String.IsNullOrEmpty(surName))
            {
                where = String.Format(" UserSname like '%{0}%' Or UserFName like '%{0}%'", surName);

                //return userService.GetPaged(where,"UserSname", pageIndex, pageSize, out totalCount);
            }
            else
            {
                where = String.Format("1=1");
            }
            return userService.GetPaged(where, "UserSname", pageIndex, pageSize, out totalCount);
        }


        public User GetUserByUserID(int userIntNo)
        {
            return userService.GetByUserIntNo(userIntNo);
        }

        public User GetUserByUserLoginName(string userName)
        {
            return userService.GetByUserLoginName(userName);
        }

        //2013-07-17 comment by Henry for no use
        //public User UpdateUserDetail(User user)
        //{
        //    return userService.Save(user);
        //}

        /// 2013-07-17 add parameter lastUser by Henry
        public User DeleteUser(int userIntNo, string lastUser)
        {
            User user = GetUserByUserID(userIntNo);
            if (user != null)
                user.MarkToDelete();
            user.LastUser = lastUser;
            return userService.Save(user);
        }

        public List<AuthorityEntity> GetAllAuthority()
        {
            AuthorityEntity authority = null;
            List<AuthorityEntity> authorityList = new List<AuthorityEntity>();
            authority = new AuthorityEntity();
            authority.AutIntNo = 0;
            authority.AutDesc = GetResourcesInClassLibraries.GetResourcesValueByCustomPath(@"Views\Admin\App_LocalResources\UserManage.aspx", "selectAuthority");
            authorityList.Add(authority);

            foreach (Authority aut in new AuthorityService().GetAll())
            {
                authority = new AuthorityEntity();
                authority.AutIntNo = aut.AutIntNo;
                authority.AutName = aut.AutName;
                authority.AutCode = aut.AutCode;
                authority.AutDesc = aut.AutName + " (" + aut.AutCode.Trim() + ")";
                authorityList.Add(authority);
            }

            return authorityList;

        }

        public AuthorityEntity GetAuthorityByAutIntNo(int autIntNo)
        {
            AuthorityEntity autEntity = new AuthorityEntity();
            Authority aut = new AuthorityService().GetByAutIntNo(autIntNo);
            if (aut != null)
            {
                autEntity.AutName = aut.AutName;
                autEntity.AutCode = aut.AutCode;
                autEntity.AutIntNo = aut.AutIntNo;

            }
            return autEntity;
        }

        public User SaveUser(User user)
        {
            return userService.Save(user);
        }

        // 2013-07-17 add parameter lastUser by Henry
        public void SaveUser(UserModel model, bool isNew, string lastUser)
        {
            User user = null;
            if (isNew)
            {
                user = new User();
            }
            else
            {
                user = GetUserByUserID(Convert.ToInt32(model.UserIntNo));
            }

            user.UserDefaultAutIntNo = model.UserDefaultAuthIntNo;
            user.UserSname = model.UserSName;
            user.UserInit = model.UserInit;
            user.UserFname = model.UserFName;
            user.UserLoginName = model.UserLoginName;
            user.UserEmail = model.UserEmail;
            //user.UserPassword = Encrypt.RC4Encrypt(model.UserPassword);
            //user.UserPassword = Encrypt.HashPassword("888888");
            user.UserAccessLevel = 0;
            user.DefaultLanguage = model.LanguageSlectorListIntNo;
            //Jerry 2012-06-12 add
            int expiryDays = new SIL.AARTO.DAL.Services.SysParamService().GetBySpColumnName(SIL.AARTO.DAL.Entities.SysParamList.UserPasswordExpiryDays.ToString()).SpIntegerValue;
            user.UserPasswordReset = false;
            user.UserPasswordGraceLoginCount = 1;
            user.UserPasswordExpiryDate = DateTime.Now.AddDays(expiryDays);

            user.LastUser = lastUser;

            using (ConnectionScope.CreateTransaction())
            {
                user = SaveUser(user);
                TList<UserOfficer> list = new TList<UserOfficer>();

                if (model.TrafficOfficerIntNo != 0)
                {
                    TrafficOfficer officer = new TrafficOfficerService().GetByToIntNo(model.TrafficOfficerIntNo);

                    if (officer != null)
                    {
                        UserOfficer userOfficer = null;

                        list = new UserOfficerService().GetByUoUserIntNo(user.UserIntNo);
                        if (list.Count > 0)
                        {
                            userOfficer = list[0];
                        }
                        else
                        {
                            userOfficer = new UserOfficer();
                        }

                        userOfficer.UotoIntNo = officer.ToIntNo;
                        userOfficer.UoUserIntNo = Convert.ToInt32(user.UserIntNo);
                        userOfficer.LastUser = lastUser;
                        // removed by Henry for currect LastUser
                        //userOfficer.LastUser = model.UserLoginName; 

                        userOfficer = new UserOfficerService().Save(userOfficer);
                    }
                }
                else
                {
                    list = new UserOfficerService().GetByUoUserIntNo(user.UserIntNo);
                    new UserOfficerService().Delete(list);
                }

                ConnectionScope.Complete();
            }
        }


        // Jake 2013-05-15 added IsActive=true
        public TList<TrafficOfficer> GetTrafficOfficerByAutIntNo(int autID)
        {
            int totalCount = 0;
            return new TrafficOfficerService().GetPaged(String.Format("AutIntNo={0} AND IsActive=1", autID), "AutIntNo", 0, 0, out totalCount);
        }

        public TList<UserOfficer> GetUserOfficerByUserIntNo(int userIntNo)
        {
            return new UserOfficerService().GetByUoUserIntNo(userIntNo);
        }

        // Jake 2013-05-15 added IsActive=true
        public List<TrafficOfficerEntity> GetAllTrafficOffericer()
        {
            TrafficOfficerEntity trafficOfficer = null;
            List<TrafficOfficerEntity> trafficOfficerList = new List<TrafficOfficerEntity>();
            trafficOfficer = new TrafficOfficerEntity();
            trafficOfficer.ToNo = "0";
            trafficOfficer.ToDesc = GetResourcesInClassLibraries.GetResourcesValueByCustomPath(@"Views\Admin\App_LocalResources\UserManage.aspx", "NotApplicable");
            trafficOfficerList.Add(trafficOfficer);

            Dictionary<int, OfficerGroup> officerGroups = OfficerGroupManager.GetAllOfficerGroupKeyValues();
            foreach (TrafficOfficer officer in new TrafficOfficerService().GetAll().Where(t=>t.IsActive).ToList())
            {
                trafficOfficer = new TrafficOfficerEntity();
                trafficOfficer.ToNo = officer.ToNo;
                trafficOfficer.TOSName = officer.TosName;
                trafficOfficer.TOInit = officer.ToInit;
                trafficOfficer.ToIntNo = officer.ToIntNo;
                trafficOfficer.ToDesc = trafficOfficer.TOSName + ", " + officer.ToInit + " (" + officerGroups[officer.OfGrIntNo].OfGrCode + "~" + officer.ToNo + ")";

                trafficOfficerList.Add(trafficOfficer);
            }
            return trafficOfficerList;
        }


    }
}
