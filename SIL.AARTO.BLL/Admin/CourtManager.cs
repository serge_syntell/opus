﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Resource;
using System.Data;

namespace SIL.AARTO.BLL.Admin
{
    public class CourtManager
    {
        public List<Court> GetCourtsByAutIntNoForDropDown(int autIntNo)
        {
            List<Court> crtList = new List<Court>();
            Court crt = new Court();

            if (autIntNo > 0)
            {
                IDataReader reader = new CourtService().GetByAutIntNo(autIntNo);
                DataTable dt = new DataTable();
                dt.Load(reader);
                if (!reader.IsClosed) reader.Close();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    crt = new Court();
                    crt.CrtIntNo = Convert.ToInt32(dt.Rows[i]["CrtIntNo"]);
                    crt.CrtName = dt.Rows[i]["CrtName"].ToString();
                    crtList.Add(crt);
                }
            }

            crtList = crtList.OrderBy(r => r.CrtName).ToList();
            crt = new Court();
            crt.CrtIntNo = 0;
            crt.CrtName = Global.SelectCrt;
            crtList.Insert(0, crt);
            return crtList;
        }

        public List<Court> GetCourtsByAutIntNo(int autIntNo)
        {
            List<Court> crtList = new List<Court>();
            Court crt = new Court();

            if (autIntNo > 0)
            {
                IDataReader reader = new CourtService().GetByAutIntNo(autIntNo);
                DataTable dt = new DataTable();
                dt.Load(reader);
                if (!reader.IsClosed) reader.Close();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    crt = new Court();
                    crt.CrtIntNo = Convert.ToInt32(dt.Rows[i]["CrtIntNo"]);
                    crt.CrtName = dt.Rows[i]["CrtName"].ToString();
                    crtList.Add(crt);
                }
            }

            return crtList;
        }

        public List<Court> GetAllCourts()
        {
            return new CourtService().GetAll().ToList();
        }

    }
}
