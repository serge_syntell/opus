﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.WOAExecutionSnapshot
{
    [Serializable]
    public class WoaExecutionSnapshot
    {
        public CourtDates CourtDates { get; set; }
        public Notice Notices { get; set; }
        public Notice_Summons NoticeSummons { get; set; }
        public Summons Summons { get; set; }
        public List<Charge> Charges { get; set; }
        public List<Summons_Charge> SummonsCharges { get; set; }
        public List<Charge_SumCharge> ChargeSumCharges { get; set; }
        public List<SumCharge> SumCharges { get; set; }
        public List<WOA> WOAs { get; set; }
        public List<Summons_Bail> SummonsBails { get; set; }
        //public void AddNoticeSummons(Notice_Summons noticeSummons)
        //{
        //    if (NoticeSummons == null)
        //        NoticeSummons = new List<Notice_Summons>();

        //    if (NoticeSummons.Contains(noticeSummons) == false)
        //        NoticeSummons.Add(noticeSummons);
        //}

        //public void RemoveNoticeSummons(Notice_Summons noticeSummons)
        //{
        //    if (NoticeSummons == null)
        //        NoticeSummons = new List<Notice_Summons>();

        //    if (NoticeSummons.Contains(noticeSummons))
        //        NoticeSummons.Remove(noticeSummons);
        //}

        public void AddSummonsBail(Summons_Bail bail)
        {
            if (SummonsBails == null)
                SummonsBails = new List<Summons_Bail>();

            if (SummonsBails.Contains(bail) == false)
                SummonsBails.Add(bail);
        }

        public void RemoveSummonsBail(Summons_Bail bail)
        {
            if (SummonsBails == null)
                SummonsBails = new List<Summons_Bail>();

            if (SummonsBails.Contains(bail))
                SummonsBails.Remove(bail);
        }

        public void AddCharge(Charge charge)
        {
            if (Charges == null)
                Charges = new List<Charge>();

            if (Charges.Contains(charge) == false)
                Charges.Add(charge);
        }

        public void RemoveCharge(Charge charge)
        {
            if (Charges == null)
                Charges = new List<Charge>();

            if (Charges.Contains(charge))
                Charges.Remove(charge);
        }

        public void AddChargeSumCharge(Charge_SumCharge chargeSumCharge)
        {
            if (ChargeSumCharges == null)
                ChargeSumCharges = new List<Charge_SumCharge>();

            if (ChargeSumCharges.Contains(chargeSumCharge) == false)
                ChargeSumCharges.Add(chargeSumCharge);
        }

        public void RemoveChargeSumCharge(Charge_SumCharge chargeSumCharge)
        {
            if (ChargeSumCharges == null)
                ChargeSumCharges = new List<Charge_SumCharge>();

            if (ChargeSumCharges.Contains(chargeSumCharge))
                ChargeSumCharges.Remove(chargeSumCharge);
        }

        public void AddSumCharge(SumCharge sumCharge)
        {
            if (SumCharges == null)
                SumCharges = new List<SumCharge>();

            if (SumCharges.Contains(sumCharge) == false)
                SumCharges.Add(sumCharge);
        }

        public void RemoveSumCharge(SumCharge sumCharge)
        {
            if (SumCharges == null)
                SumCharges = new List<SumCharge>();

            if (SumCharges.Contains(sumCharge))
                SumCharges.Remove(sumCharge);
        }

        public void AddSummmonsCharge(Summons_Charge summonsCharge)
        {
            if (SummonsCharges == null)
                SummonsCharges = new List<Summons_Charge>();

            if (SummonsCharges.Contains(summonsCharge) == false)
                SummonsCharges.Add(summonsCharge);
        }

        public void RemoveSummmonsCharge(Summons_Charge summonsCharge)
        {
            if (SummonsCharges == null)
                SummonsCharges = new List<Summons_Charge>();

            if (SummonsCharges.Contains(summonsCharge))
                SummonsCharges.Remove(summonsCharge);
        }

        public void AddWOA(WOA woa)
        {
            if (WOAs == null)
                WOAs = new List<WOA>();

            if (WOAs.Contains(woa) == false)
                WOAs.Add(woa);
        }

        public void RemoveWOA(WOA woa)
        {
            if (WOAs == null)
                WOAs = new List<WOA>();

            if (WOAs.Contains(woa))
                WOAs.Remove(woa);
        }
    }

    [Serializable]
    public class CourtDates
    {
        public int AutIntNo { get; set; }
        public int? CrtRIntNo { get; set; }
        public DateTime CDate { get; set; }
    }

    [Serializable]
    public class Summons
    {
        public int SumIntNo { get; set; }
        public string SumStatus { get; set; }
        public string SumCaseNo { get; set; }
        public DateTime? SumCourtDate { get; set; }
        public int SumChargeStatus { get; set; }
        public bool SumLocked { get; set; }
        public string SumCancelled { get; set; }
        public DateTime? SumDateCancelled { get; set; }
        public string SumReasonCancelled { get; set; }
        public int SummonsStatus { get; set; }
        public DateTime? SumPaidDate { get; set; }
        public int? SumPrevSummonsStatus { get; set; }
    }

    [Serializable]
    public class Notice
    {
        public int NotIntNo { get; set; }
        public int NoticeStatus { get; set; }
        public int? NotPrevNoticeStatus { get; set; }
        public string LastUser { get; set; }
    }

    [Serializable]
    public class Notice_Summons
    {
        public int NotSumIntNo { get; set; }
        public int NotIntNo { get; set; }
        public int SumIntNo { get; set; }
        public string LastUser { get; set; }
    }

    [Serializable]
    public class Accused
    {
        public int AccIntNo { get; set; }
    }

    [Serializable]
    public class Charge
    {
        public int ChgIntNo { get; set; }
        public int NotIntNo { get; set; }
        public int CTIntNo { get; set; }
        public string ChgOffenceType { get; set; }
        public string ChgOffenceCode { get; set; }
        public int ChgFineAmount { get; set; }
        public string ChgFineAlloc { get; set; }
        public string LastUser { get; set; }
        public string ChgStatutoryRef { get; set; }
        public string ChgStatRefLine1 { get; set; }
        public string ChgStatRefLine2 { get; set; }
        public string ChgStatRefLine3 { get; set; }
        public string ChgStatRefLine4 { get; set; }
        public string ChgOffenceDescr { get; set; }
        public string ChgOffenceDescr2 { get; set; }
        public string ChgOffenceDescr3 { get; set; }
        public int ChargeStatus { get; set; }
        public DateTime? ChgPaidDate { get; set; }
        public string ChgNoAOG { get; set; }
        public decimal ChgRevFineAmount { get; set; }
        public decimal ChgRevDiscountAmount { get; set; }
        public decimal ChgRevDiscountedAmount { get; set; }
        public int PreviousStatus { get; set; }
        public int ChgContemptCourt { get; set; }
        public string ChgIssueAuthority { get; set; }
        public string ChgOfficerNatisNo { get; set; }
        public string ChgOfficerName { get; set; }
        public decimal ChgDiscountAmount { get; set; }
        public decimal ChgDiscountedAmount { get; set; }
        public string ChgType { get; set; }
        public int ChgDemeritPoints { get; set; }
        public string ChgLegislation { get; set; }
        public string ChgOffenceTypeOld { get; set; }
        public bool ChgIsMain { get; set; }
        public int ChgSequence { get; set; }
        public int? MainChargeID { get; set; }
    }

    [Serializable]
    public class Charge_SumCharge
    {
        public int ChgSChIntNo { get; set; }
        public int ChgIntNo { get; set; }
        public int SChIntNo { get; set; }
        public string LastUser { get; set; }
    }

    [Serializable]
    public class SumCharge
    {
        public int SChIntNo { get; set; }
        public int SumIntNo { get; set; }
        public string SChStatRef { get; set; }
        public string SChNumber { get; set; }
        public string SChCode { get; set; }
        public string SChType { get; set; }
        public string SChDescr { get; set; }
        public string SChOcTDescr { get; set; }
        public string SChOctDescr1 { get; set; }
        public decimal SChFineAmount { get; set; }
        public decimal SChRevAmount { get; set; }
        public DateTime? SChCourtJudgementLoaded { get; set; }
        public decimal SChSentenceAmount { get; set; }
        public decimal SChContemptAmount { get; set; }
        public decimal SChOtherAmount { get; set; }
        public decimal SChPaidAmount { get; set; }
        public int CJTIntNo { get; set; }
        public string SChVerdict { get; set; }
        public string SChSentence { get; set; }
        public string SChNoAOG { get; set; }
        public string SChWOALoaded { get; set; }
        public string LastUser { get; set; }
        public DateTime? GraceExpiryDate { get; set; }
        public string SChDescrAfr { get; set; }
        public bool SchIsMain { get; set; }
        public int SChSequence { get; set; }
        public int SumChargeStatus { get; set; }
        public int? SChPrevSumChargeStatus { get; set; }
        public int? MainSumChargeID { get; set; }
    }

    public class Summons_Charge
    {
        public int SCIntNo { get; set; }
        public int SumIntNo { get; set; }
        public int ChgIntNo { get; set; }
        public string LastUser { get; set; }
    }

    public class WOA
    {
        public int WOAIntNo { get; set; }
        public int SumIntNo { get; set; }
        public string WOANumber { get; set; }
        public DateTime WOALoadDate { get; set; }
        public DateTime? WOAIssueDate { get; set; }
        public DateTime? WOAPrintDate { get; set; }
        public string WOAPrintFileName { get; set; }
        public decimal WOAFineAmount { get; set; }
        public decimal WOAAddAmount { get; set; }
        public string WOAReceiptNo { get; set; }
        public decimal WOAPaidAmount { get; set; }
        public string LastUser { get; set; }
        public DateTime? WOAGeneratedDate { get; set; }
        public DateTime? WOAPaidDate { get; set; }
        public DateTime? WOAExpireDate { get; set; }
        public int? WOAChargeStatus { get; set; }
        public string WOAAuthorised { get; set; }
        public bool WOACancel { get; set; }
        public DateTime? WOACancelDate { get; set; }
        public DateTime? WOABookOutToOfficerDate { get; set; }
        public int WOAServedStatus { get; set; }
        public string WOAOfficerNumber { get; set; }
        public DateTime? WOANewCourtDate { get; set; }
        public int? NotIntNo { get; set; }
        public DateTime? WOAS72ReturnDate { get; set; }
        public int? WOAChargePrevStatus { get; set; }
        public bool IsCurrent { get; set; }
        public DateTime? WOASentToCourtDate { get; set; }
        public DateTime? WOAReturnedFromCourtDate { get; set; }
        public string WOARejectedReason { get; set; }
        public byte WOAEdition { get; set; }
        public bool IsArrested { get; set; }
        public string WOAType { get; set; }
    }

    public class Summons_Bail
    {
        public int SuBaIntNo { get; set; }
        public int SumIntNo { get; set; }
        public int? CrtIntNoTrFrom { get; set; }
        public int CrtIntNoTrTo { get; set; }
        public decimal? SuBaAmount { get; set; }
        public string SuBaReceiptNumber { get; set; }
        public int? WOAArrestedIntNo { get; set; }
        public DateTime? SuBaDatePaid { get; set; }
        public string LastUser { get; set; }
    }
}
