﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml;
using System.IO;
using System.Data.SqlClient;
using System.Reflection;
using System.Xml.Serialization;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Globalization;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.BLL.WOAExecutionSnapshot
{
    public class WoaExecutionSnapshotDB
    {
        // Fields
        private SqlConnection con;

        public WoaExecutionSnapshotDB(string connectionString)
        {
            this.con = new SqlConnection(connectionString);
        }

        public bool SetSnapshot(int woaIntNo, bool lockSnapshot, string lastUser)
        {
            DataSet dsJudgementSnapshot = GetSnapshotDataFromDB(woaIntNo);

            if (dsJudgementSnapshot != null)
            {
                SortSnapshotDataSet(dsJudgementSnapshot);

                XmlDocument xmlDoc = ReadSnapshotToXML(dsJudgementSnapshot);

                if (xmlDoc != null && !String.IsNullOrEmpty(xmlDoc.InnerXml))
                {
                    //Save xml to DB
                    DateTime createDate = DateTime.Today;
                    return AddWoaBeforeExecutionSnapshot(woaIntNo, lockSnapshot, createDate, xmlDoc.InnerXml, lastUser);

                }
            }
            return false;
        }

        public bool RollBackFromSnapshot(int woaIntNo, string lastUser)
        {

            XmlDocument xmlDoc = GetSnapshotFromTable(woaIntNo);

            if (xmlDoc != null && !String.IsNullOrEmpty(xmlDoc.InnerXml))
            {
                WoaExecutionSnapshot snapShot = ReadSnapshotToEntity(xmlDoc);

                if (snapShot != null)
                {
                    //roll back operation here
                    try
                    {
                        RollBackFromSnapShot(woaIntNo, snapShot, lastUser);

                        return true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }


            }
            return false;
        }

        public bool CheckSnapshotIsCreated(int woaIntNo)
        {
            WoaBeforeExecutionSnapshot snapshot = new WoaBeforeExecutionSnapshotService().GetByWoaIntNo(woaIntNo);
            if (snapshot != null && !String.IsNullOrEmpty(snapshot.WbesDataXml))
            {
                return true;
            }
            return false;
        }


        public bool UnLockSnapshot(int woaIntNo, string lastUser)
        {
            try
            {
                WoaBeforeExecutionSnapshotService sbjsService = new WoaBeforeExecutionSnapshotService();
                WoaBeforeExecutionSnapshot snapshot = sbjsService.GetByWoaIntNo(woaIntNo);
                if (snapshot != null)
                {
                    //if (snapshot.WbesDateCreated < DateTime.Today)
                    //{
                    //SetSnapshot(woaIntNo, false, lastUser);
                    DataSet dsJudgementSnapshot = GetSnapshotDataFromDB(woaIntNo);

                    if (dsJudgementSnapshot != null)
                    {
                        SortSnapshotDataSet(dsJudgementSnapshot);

                        XmlDocument xmlDoc = ReadSnapshotToXML(dsJudgementSnapshot);

                        UpdateWoaBeforeExecutionSnapshot(woaIntNo, false, xmlDoc.InnerXml, lastUser);
                    }
                    //}
                    //else
                    //{
                    //    snapshot.WbesLocked = false;
                    //}
                    sbjsService.Save(snapshot);

                    return snapshot != null;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Jake 2014-08-28 added parameter type
        //type only have two values for now, WCR or WER
        //WCR->WOA cancellation reversal
        //WER->WOA execution reversal
        public bool TestRollBack(int woaIntNo, string type,bool reversalofS72Role, out string errorMessage)
        {
            bool flag = true;
            errorMessage = string.Empty;

            //check rollback data
            WoaBeforeExecutionSnapshot snapshot = new WoaBeforeExecutionSnapshotService().GetByWoaIntNo(woaIntNo);

            if (snapshot == null)
            {
                flag = false;
                errorMessage += "Missing rollback snapshot" + "\r\n";
            }
            else
            {
                if (snapshot.WbesLocked)
                {
                    flag = false;
                    errorMessage += "Unable to reverse a WOA " + (type == "WCR" ? "Cancellation" : "execution") + " that has already been reversed / progressed to a new status." + "\r\n";
                }
                TimeSpan ts = DateTime.Now - snapshot.WbesDateCreated;
                if (ts.TotalHours > 24) {
                    if (reversalofS72Role == false) {
                        flag = false;
                        errorMessage = "Do not have permission to perform this operation.\r\nPlease contact administrator whether you have got role [ReversalOfS72].";
                    }
                }
				//Jake 2014-10-20 comment out for 5369 - Section 72 reversal after 72 hours
                //if (snapshot.WbesDateCreated < DateTime.Today || snapshot.WbesDateCreated > DateTime.Today)
                //{
                //    flag = false;
                //    errorMessage += "A WOA execution can only be reversed within 24 hours.\r\n";
                //}
            }

            SIL.AARTO.DAL.Entities.Woa woa = new WoaService().GetByWoaIntNo(woaIntNo);
            if (woa == null)
            {
                flag = false;
                errorMessage += "WOA does not exist" + "\r\n";
            }

            SIL.AARTO.DAL.Entities.Summons summons = new SIL.AARTO.DAL.Services.SummonsService().GetBySumIntNo(woa.SumIntNo);

            if (summons == null)
            {
                flag = false;
                errorMessage += "Summons does not exist" + "\r\n";
            }
            else
            {
                SIL.AARTO.DAL.Entities.NoticeSummons ns = new NoticeSummonsService().GetBySumIntNo(summons.SumIntNo).FirstOrDefault();
                if (ns == null)
                {
                    flag = false;
                    errorMessage += "Notice_Summons does not exist" + "\r\n";
                }
                else
                {
                    SIL.AARTO.DAL.Entities.Notice notice = new NoticeService().GetByNotIntNo(ns.NotIntNo);
                    if (notice == null)
                    {
                        flag = false;
                        errorMessage += "Notice does not exist" + "\r\n";
                    }
                    else
                    {
                        if (notice.NoticeStatus == (int)ChargeStatusList.FullyPaid
                            && woa.WoaChargeStatus != (int)ChargeStatusList.FullyPaid)//&& woa.WoaServedStatus != (int)WoaServedStatusList.ExecutedPaid)
                        {
                            flag = false;
                            errorMessage += "This WOA status [" + ((ChargeStatusList)woa.WoaChargeStatus).ToString() + "] is not eligible for reversal" + "\r\n";
                        }
                    }
                }
            }
            //if (summons.SummonsStatus < (int)ChargeStatusList.CourtSystem || summons.SummonsStatus > (int)ChargeStatusList.WarrantWithdrawalForSummonsReIssue)
            //{
            //    flag = false;
            //    errorMessage += "This summons status [" + ((ChargeStatusList)summons.SummonsStatus).ToString() + "] is not eligible for reversal" + "</br>";
            //}

            //SIL.AARTO.DAL.Data.ReceiptTranQuery rtQuery = new DAL.Data.ReceiptTranQuery();
            //rtQuery.AppendInQuery(ReceiptTranColumn.ChgIntNo, String.Format(" SELECT ChgIntNo FROM dbo.Charge_SumCharge WHERE SChIntNo IN ( "
            //    + " SELECT SChIntNo FROM dbo.SumCharge WHERE SumIntNo={0})", +summons.SumIntNo));


            //if (receipTranList != null && receipTranList.Count > 0)
            //{
            //    if (receipTranList.Sum(r => r.RtAmount) > 0)
            //    {
            //        flag = false;
            //        errorMessage += "A payment has already been made";
            //    }
            //}



            return flag;
        }

        #region DB Access

        // no use
        public DataSet GetSummonsToRollback(int crtIntNo, int crtrIntNo, string summonsNo,
            int pageSize, int pageIndex, out int totalCount)    //2013-04-10 add by Henry for pagination
        {
            SqlCommand command = con.CreateCommand();
            command.CommandText = "GetSummonsAfterJudgement";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@CrtIntNo", SqlDbType.Int)).Value = crtIntNo;
            command.Parameters.Add(new SqlParameter("@CrtRIntNo", SqlDbType.Int)).Value = crtrIntNo;
            command.Parameters.Add(new SqlParameter("@SummonsNo", SqlDbType.VarChar, 50)).Value = summonsNo;

            command.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
            command.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            command.Parameters.Add(paraTotalCount);

            DataSet dsReturn = new DataSet();

            SqlDataAdapter da = new SqlDataAdapter(command);

            try
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                da.Fill(dsReturn);

                //return command.ExecuteReader();
                totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

                return dsReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        Charge GetFirstMainCharge(List<SIL.AARTO.BLL.WOAExecutionSnapshot.Charge> chargeList)
        {
            Charge firstMainCharge = chargeList.Where(c => (c.ChargeStatus == 955 || c.ChargeStatus < 900) && c.ChgIsMain == true).OrderBy(c => c.ChgSequence).FirstOrDefault();

            return firstMainCharge;
        }

        bool AddWoaBeforeExecutionSnapshot(int woaIntNo, bool locked, DateTime createDate, string snapShotXML, string lastUser)
        {

            SqlCommand command = con.CreateCommand();
            command.CommandText = "WOABeforeExecutionSnapshotAdd";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@WoaIntNo", SqlDbType.Int)).Value = woaIntNo;
            command.Parameters.Add(new SqlParameter("@WBESDateCreated", SqlDbType.SmallDateTime)).Value = createDate;
            command.Parameters.Add(new SqlParameter("@WBESDataXML", SqlDbType.Xml)).Value = snapShotXML;
            command.Parameters.Add(new SqlParameter("@WBESLocked", SqlDbType.Bit)).Value = locked;
            command.Parameters.Add(new SqlParameter("@LastUser", SqlDbType.NVarChar, 50)).Value = lastUser;

            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();

                int WBESIntNo = Convert.ToInt32(command.ExecuteScalar());

                return WBESIntNo > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }


        bool UpdateWoaBeforeExecutionSnapshot(int woaIntNo, bool lockSnapShot, string snapShotXML, string lastUser)
        {

            SqlCommand command = con.CreateCommand();
            command.CommandText = "WoaBeforeExecutionSnapshotUpdate";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@WoaIntNo", SqlDbType.Int)).Value = woaIntNo;
            command.Parameters.Add(new SqlParameter("@DataXML", SqlDbType.Xml)).Value = snapShotXML;
            command.Parameters.Add(new SqlParameter("@Locked", SqlDbType.Bit)).Value = lockSnapShot;
            command.Parameters.Add(new SqlParameter("@LastUser", SqlDbType.NVarChar, 50)).Value = lastUser;

            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();

                return Convert.ToInt32(command.ExecuteNonQuery()) > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }



        DataSet GetSnapshotDataFromDB(int woaIntNo)
        {
            DataSet dsReturn = new DataSet();

            SqlCommand command = con.CreateCommand();
            command.CommandText = "CreateWOAExecutionSnapshot";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@WoaIntNo", SqlDbType.Int)).Value = woaIntNo;

            SqlDataAdapter da = new SqlDataAdapter(command);

            try
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                da.Fill(dsReturn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }

            return dsReturn;
        }

        XmlDocument GetSnapshotFromTable(int woaIntNo)
        {
            XmlDocument document = new XmlDocument();
            //MemoryStream memoryStream = new MemoryStream();
            try
            {

                WoaBeforeExecutionSnapshot snapshot = new WoaBeforeExecutionSnapshotService().GetByWoaIntNo(woaIntNo);
                if (snapshot != null && !String.IsNullOrEmpty(snapshot.WbesDataXml))
                {
                    document.LoadXml(snapshot.WbesDataXml);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return document;
        }


        bool RollBackFromSnapShot(int woaIntNo, WoaExecutionSnapshot snapShot, string lastUser)
        {
            try
            {
                Woa w = new WoaService().GetByWoaIntNo(woaIntNo);

                bool woaCancellFlag = false;
                if (w.WoaCancel.HasValue && w.WoaCancel.Value && w.WoaCancelDate.HasValue)
                {
                    woaCancellFlag = true;
                }

                using (ConnectionScope.CreateTransaction())
                {
                    WoaBeforeExecutionSnapshot snapShotDB = new WoaBeforeExecutionSnapshotService().GetByWoaIntNo(woaIntNo);

                    //roll back Summons
                    ProcessSummons(snapShot.Summons, lastUser);

                    //roll back Summons_Bail
                    ProcessSummonsBail(snapShot.SummonsBails, snapShot.Summons);

                    // roll back WOAS
                    ProcessWoa(snapShot.WOAs, snapShot.Charges, snapShot.Summons.SumIntNo, lastUser);

                    //roll back notice
                    ProcessNotice(snapShot.Notices);

                    //roll back notice_summons
                    //ProcessNoticeSummons(snapShot, snapShot.NoticeSummons);

                    //roll back sumcharge
                    ProcessSumCharge(snapShot.SumCharges);

                    //roll back charge_sumcharge
                    //ProcessChargeSumCharge(snapShot, snapShot.ChargeSumCharges);

                    //roll back charge
                    ProcessCharge(snapShot.Charges, woaIntNo, lastUser, woaCancellFlag);


                    snapShotDB.WbesLocked = true;

                    new WoaBeforeExecutionSnapshotService().Save(snapShotDB);

                    ConnectionScope.Complete();

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessWoa(List<WOA> woas, List<WOAExecutionSnapshot.Charge> chargeList, int sumIntNo, string lastUser)
        {
            if (woas != null)
            {
                WoaService woaService = new WoaService();

                TList<Woa> woasInDB = woaService.GetBySumIntNo(sumIntNo);
                if (woas.Count < woasInDB.Count)
                {
                    var woaq = from d in woas
                               select new { WoaIntNo = d.WOAIntNo };

                    foreach (Woa w in woasInDB)
                    {
                        WOAExecutionSnapshot.Charge chg = GetFirstMainCharge(chargeList);

                        if (woaq.Where(q => q.WoaIntNo.Equals(w.WoaIntNo)).Count() == 0)
                        {
                            w.WoaCancel = true;
                            w.WoaCancelDate = DateTime.Now;
                            woaService.Save(w);

                            if (chg != null)
                                InsertEvidencePack(chg.ChgIntNo, DateTime.Now, String.Format("WOA judgement reversed by {0} on {1}", lastUser, DateTime.Now.ToString("yyyy/MM/dd")), "WOA", w.WoaIntNo, lastUser);
                        }
                    }
                }
                foreach (WOA woa in woas)
                {
                    Woa woaDB = woaService.GetByWoaIntNo(woa.WOAIntNo);
                    if (woaDB != null)
                    {
                        woaDB.WoaIntNo = woa.WOAIntNo;
                        woaDB.SumIntNo = woa.SumIntNo;
                        woaDB.WoaNumber = woa.WOANumber;
                        woaDB.WoaLoadDate = woa.WOALoadDate;
                        woaDB.WoaIssueDate = woa.WOAIssueDate;
                        woaDB.WoaPrintDate = woa.WOAPrintDate;
                        woaDB.WoaPrintFileName = woa.WOAPrintFileName;
                        woaDB.WoaFineAmount = woa.WOAFineAmount;
                        woaDB.WoaAddAmount = woa.WOAAddAmount;
                        woaDB.WoaReceiptNo = woa.WOAReceiptNo;
                        woaDB.WoaPaidAmount = woa.WOAPaidAmount;
                        woaDB.LastUser = woa.LastUser;
                        woaDB.WoaGeneratedDate = woa.WOAGeneratedDate;
                        woaDB.WoaPaidDate = woa.WOAPaidDate;
                        woaDB.WoaExpireDate = woa.WOAExpireDate;
                        woaDB.WoaChargeStatus = woa.WOAChargeStatus;
                        woaDB.WoaAuthorised = woa.WOAAuthorised;
                        woaDB.WoaCancel = woa.WOACancel;
                        woaDB.WoaCancelDate = woa.WOACancelDate;
                        woaDB.WoaBookOutToOfficerDate = woa.WOABookOutToOfficerDate;
                        woaDB.WoaServedStatus = woa.WOAServedStatus;
                        woaDB.WoaOfficerNumber = woa.WOAOfficerNumber;
                        woaDB.WoaNewCourtDate = woa.WOANewCourtDate;
                        woaDB.NotIntNo = woa.NotIntNo;
                        woaDB.Woas72ReturnDate = woa.WOAS72ReturnDate;
                        woaDB.WoaChargePrevStatus = woa.WOAChargePrevStatus;
                        woaDB.IsCurrent = woa.IsCurrent;
                        woaDB.WoaSentToCourtDate = woa.WOASentToCourtDate;
                        woaDB.WoaReturnedFromCourtDate = woa.WOAReturnedFromCourtDate;
                        woaDB.WoaRejectedReason = woa.WOARejectedReason;
                        woaDB.WoaEdition = woa.WOAEdition;
                        woaDB.IsArrested = woa.IsArrested;
                        woaDB.WoaType = woa.WOAType;

                        woaService.Save(woaDB);
                    }
                }
            }
        }


        void ProcessSummons(SIL.AARTO.BLL.WOAExecutionSnapshot.Summons s, string supervisorName)
        {
            try
            {
                SummonsService sService = new SummonsService();
                SIL.AARTO.DAL.Entities.Summons summons = sService.GetBySumIntNo(s.SumIntNo);
                summons.SumStatus = s.SumStatus;
                summons.SumChargeStatus = s.SumChargeStatus;
                summons.SumLocked = s.SumLocked;
                summons.SumCancelled = s.SumCancelled;
                summons.SumDateCancelled = s.SumDateCancelled;
                summons.SumReasonCancelled = String.IsNullOrEmpty(s.SumReasonCancelled) ? "" : s.SumReasonCancelled;
                summons.SummonsStatus = s.SummonsStatus;
                summons.SumPaidDate = s.SumPaidDate;
                summons.SumCaseNo = s.SumCaseNo;
                summons.SumCourtDate = s.SumCourtDate;
                summons.SumPrevSummonsStatus = s.SumPrevSummonsStatus;
                summons.SupervisorActionDate = DateTime.Now;
                summons.SupervisorName = supervisorName;

                //summons.SumRemandedFromCaseNumber = null;
                //summons.SumRemandedFromCourtDate = null;
                //summons.SumRemandedSummonsFlag = false;

                sService.Save(summons);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessNotice(SIL.AARTO.BLL.WOAExecutionSnapshot.Notice n)
        {
            try
            {
                NoticeService nService = new NoticeService();
                SIL.AARTO.DAL.Entities.Notice notice = nService.GetByNotIntNo(n.NotIntNo);
                notice.NoticeStatus = n.NoticeStatus;
                notice.NotPrevNoticeStatus = n.NotPrevNoticeStatus;
                nService.Save(notice);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessNoticeSummons(SIL.AARTO.BLL.WOAExecutionSnapshot.Notice_Summons notice_summons)
        {
            try
            {
                SIL.AARTO.DAL.Services.NoticeSummonsService nsService = new NoticeSummonsService();
                SIL.AARTO.DAL.Services.NoticeSummonsCancelledService nscService = new NoticeSummonsCancelledService();

                TList<SIL.AARTO.DAL.Entities.NoticeSummons> nsList = nsService.GetByNotIntNo(notice_summons.NotIntNo);
                nsService.Delete(nsList);

                //SIL.AARTO.DAL.Entities.NoticeSummons noticeSummons = nsService.GetByNotSumIntNo(notice_summons.NotSumIntNo);
                //if (noticeSummons == null)
                //{
                NoticeSummons noticeSummons = new NoticeSummons();
                //}
                noticeSummons.NotIntNo = notice_summons.NotIntNo;
                noticeSummons.SumIntNo = notice_summons.SumIntNo;
                noticeSummons.LastUser = notice_summons.LastUser;

                nsService.Save(noticeSummons);

                TList<NoticeSummonsCancelled> nscList = nscService.GetBySumIntNo(noticeSummons.SumIntNo);
                if (nscList != null && nscList.Count > 0)
                {
                    nscService.Delete(nscList);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessAccused(WOAExecutionSnapshot.Accused accused)
        {
            try { }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessSumCharge(List<WOAExecutionSnapshot.SumCharge> scList)
        {
            try
            {
                if (scList == null) return;
                SumChargeService scService = new SumChargeService();

                foreach (SIL.AARTO.BLL.WOAExecutionSnapshot.SumCharge sc in scList)
                {
                    SIL.AARTO.DAL.Entities.SumCharge sumCharge = scService.GetBySchIntNo(sc.SChIntNo);

                    if (sumCharge == null)
                    {
                        sumCharge = new DAL.Entities.SumCharge();
                    }

                    sumCharge.SchIntNo = sc.SChIntNo;
                    sumCharge.SumIntNo = sc.SumIntNo;
                    sumCharge.SchStatRef = sc.SChStatRef;
                    sumCharge.SchNumber = sc.SChNumber;
                    sumCharge.SchCode = sc.SChCode;
                    sumCharge.SchDescr = sc.SChDescr;
                    sumCharge.SchOcTdescr = sc.SChOcTDescr;
                    sumCharge.SchOctDescr1 = sc.SChOctDescr1;
                    sumCharge.SchFineAmount = sc.SChFineAmount;
                    sumCharge.SchRevAmount = sc.SChRevAmount;
                    sumCharge.SchCourtJudgementLoaded = sc.SChCourtJudgementLoaded;
                    sumCharge.SchSentenceAmount = sc.SChSentenceAmount;
                    sumCharge.SchContemptAmount = sc.SChContemptAmount;
                    sumCharge.SchOtherAmount = sc.SChOtherAmount;
                    sumCharge.SchPaidAmount = sc.SChPaidAmount;
                    sumCharge.CjtIntNo = sc.CJTIntNo;
                    sumCharge.SchVerdict = sc.SChVerdict;
                    sumCharge.SchSentence = sc.SChSentence;
                    sumCharge.SchNoAog = sc.SChNoAOG;
                    sumCharge.SchWoaLoaded = sc.SChWOALoaded;
                    sumCharge.LastUser = sc.LastUser;
                    sumCharge.GraceExpiryDate = sc.GraceExpiryDate;
                    sumCharge.SchDescrAfr = sc.SChDescrAfr;
                    sumCharge.SchIsMain = sc.SchIsMain;
                    sumCharge.SchSequence = (byte)sc.SChSequence;
                    sumCharge.SumChargeStatus = sc.SumChargeStatus;
                    sumCharge.SchPrevSumChargeStatus = sc.SChPrevSumChargeStatus;
                    sumCharge.MainSumChargeId = sc.MainSumChargeID;
                    sumCharge.SchType = sc.SChType;


                    scService.Save(sumCharge);

                    TList<ScDeferredPayments> payments = new ScDeferredPaymentsService().GetBySchIntNo(sumCharge.SchIntNo);

                    if (payments != null && payments.Count > 0)
                    {
                        new ScDeferredPaymentsService().Delete(payments);
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessChargeSumCharge(List<SIL.AARTO.BLL.WOAExecutionSnapshot.Charge_SumCharge> cscList)
        {
            try
            {
                if (cscList == null) return;
                ChargeSumChargeService cscService = new ChargeSumChargeService();
                SumChargeService scService = new SumChargeService();

                //roll back Charge_SumChage row
                SIL.AARTO.DAL.Entities.ChargeSumCharge csCharge = null;
                foreach (SIL.AARTO.BLL.WOAExecutionSnapshot.Charge_SumCharge csc in cscList)
                {
                    //csCharge = cscService.GetByChgSchIntNo(csc.ChgSChIntNo);
                    csCharge = cscService.GetByChgIntNo(csc.ChgIntNo).FirstOrDefault(s => s.SchIntNo.Equals(csc.SChIntNo));
                    if (csCharge == null)
                    {
                        csCharge = new ChargeSumCharge();
                    }
                    csCharge.ChgIntNo = csc.ChgIntNo;
                    //csCharge.ChgSchIntNo = csc.ChgSChIntNo;
                    csCharge.SchIntNo = csc.SChIntNo;
                    csCharge.LastUser = csc.LastUser;

                    cscService.Save(csCharge);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessCharge(List<SIL.AARTO.BLL.WOAExecutionSnapshot.Charge> chargeList, int woaIntNo, string lastUser, bool woaCancelFlag)
        {
            try
            {
                ChargeService cService = new ChargeService();
                if (chargeList == null) return;
                foreach (SIL.AARTO.BLL.WOAExecutionSnapshot.Charge c in chargeList)
                {
                    SIL.AARTO.DAL.Entities.Charge charge = cService.GetByChgIntNo(c.ChgIntNo);
                    if (charge == null)
                    {
                        charge = new DAL.Entities.Charge();
                    }

                    charge.NotIntNo = c.NotIntNo;
                    charge.CtIntNo = c.CTIntNo;
                    charge.ChgOffenceType = c.ChgOffenceType;
                    charge.ChgOffenceCode = c.ChgOffenceCode;
                    charge.ChgFineAmount = c.ChgFineAmount;
                    charge.ChgFineAlloc = c.ChgFineAlloc;
                    charge.LastUser = c.LastUser;
                    charge.ChgStatutoryRef = c.ChgStatutoryRef ?? "";
                    charge.ChgStatRefLine1 = c.ChgStatRefLine1 ?? "";
                    charge.ChgStatRefLine2 = c.ChgStatRefLine2 ?? "";
                    charge.ChgStatRefLine3 = c.ChgStatRefLine3 ?? "";
                    charge.ChgStatRefLine4 = c.ChgStatRefLine4 ?? "";
                    charge.ChgOffenceDescr = c.ChgOffenceDescr ?? "";
                    charge.ChgOffenceDescr2 = c.ChgOffenceDescr2 ?? "";
                    charge.ChgOffenceDescr3 = c.ChgOffenceDescr3 ?? "";
                    charge.ChargeStatus = c.ChargeStatus;
                    charge.ChgPaidDate = c.ChgPaidDate;
                    charge.ChgNoAog = c.ChgNoAOG;
                    charge.ChgRevFineAmount = (float)c.ChgRevFineAmount;
                    charge.ChgRevDiscountAmount = (float)c.ChgRevDiscountAmount;
                    charge.ChgRevDiscountedAmount = (float)c.ChgRevDiscountedAmount;
                    charge.PreviousStatus = c.PreviousStatus;
                    charge.ChgContemptCourt = c.ChgContemptCourt;
                    charge.ChgIssueAuthority = c.ChgIssueAuthority;
                    charge.ChgOfficerNatisNo = c.ChgOfficerNatisNo;
                    charge.ChgOfficerName = c.ChgOfficerName;
                    charge.ChgDiscountAmount = c.ChgDiscountAmount;
                    charge.ChgDiscountedAmount = c.ChgDiscountedAmount;
                    charge.ChgType = c.ChgType;
                    charge.ChgDemeritPoints = c.ChgDemeritPoints;
                    charge.ChgLegislation = c.ChgLegislation;
                    charge.ChgOffenceTypeOld = c.ChgOffenceTypeOld;
                    charge.ChgIsMain = c.ChgIsMain;
                    charge.ChgSequence = (byte)c.ChgSequence;
                    charge.MainChargeId = c.MainChargeID;

                    cService.Save(charge);

                    /* Jake 2013-11-26 comment out
                    if (charge.ChgIsMain && charge.ChgSequence == 1)
                    {
                        //“Judgement Reversed by “ + SupervisorName + “ on “ + SupervisorActionDate.
                        InsertEvidencePack(charge.ChgIntNo, DateTime.Now, String.Format("WOA execution reversed by {0} on {1}", lastUser, DateTime.Now.ToString("yyyy/MM/dd")), "Summons", sumIntNo, lastUser);


                        // 2013-11-19 removed 
                        /*
                        //Jake 2013-10-24 modified 
                        // added double WOA process here 
                        WoaService woaService = new WoaService();
                        // Jake 2013-03-04 add IsCurrent column for section 72 document
                        TList<Woa> woas = woaService.GetBySumIntNo(sumIntNo);
                        Woa woa = woas.Where(w => w.IsCurrent == true && w.WoaCancel != true).OrderByDescending(w => w.WoaEdition).FirstOrDefault();

                        //if WOA has already been cancelled for some or other reason - do not cancel it again!
                        if (woa != null && woa.WoaCancel != true)
                        {
                            if (woa.WoaType == "D")
                            {
                                Woa previousWoa = woas.Where(w => w.WoaIntNo != woa.WoaIntNo && w.WoaCancel != true).OrderByDescending(w => w.WoaEdition).FirstOrDefault();
                                if (previousWoa.WoaIntNo != woa.WoaIntNo)
                                {
                                    if (previousWoa.WoaCancel != null && previousWoa.WoaCancel == true)
                                    {
                                        previousWoa.WoaCancel = false;
                                        previousWoa.WoaCancelDate = null;

                                        woaService.Save(previousWoa);
                                    }
                                }

                            }

                            woa.WoaCancel = true;
                            woa.WoaCancelDate = DateTime.Now;

                            InsertEvidencePack(charge.ChgIntNo, DateTime.Now, String.Format("WOA judgement reversed by {0} on {1}", lastUser, DateTime.Now.ToString("yyyy/MM/dd")), "WOA", woa.WoaIntNo, lastUser);

                            woaService.Save(woa);
                        }
                         
                    }  
                    * */
                }
                Charge firstMainCharge = GetFirstMainCharge(chargeList);// chargeList.Where(c => c.ChargeStatus < 900 && c.ChgIsMain == true).OrderBy(c => c.ChgSequence).FirstOrDefault();
                if (firstMainCharge != null)
                {
                    //“Judgement Reversed by “ + SupervisorName + “ on “ + SupervisorActionDate.
                    string descr = string.Empty;
                    if (woaCancelFlag)
                    {
                        descr = String.Format("WOA cancellation reversed by {0} on {1}", lastUser, DateTime.Now.ToString("yyyy/MM/dd"));
                    }
                    else
                    {
                        descr = String.Format("WOA execution reversed by {0} on {1}", lastUser, DateTime.Now.ToString("yyyy/MM/dd"));
                    }
                    InsertEvidencePack(firstMainCharge.ChgIntNo, DateTime.Now, descr, "WOA", woaIntNo, lastUser);

                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessSummonsCharge(List<SIL.AARTO.BLL.WOAExecutionSnapshot.Summons_Charge> scList)
        {
            try
            {
                if (scList == null) return;
                SummonsChargeService scService = new SummonsChargeService();
                SIL.AARTO.DAL.Services.NoticeSummonsService nsService = new NoticeSummonsService();


                foreach (SIL.AARTO.BLL.WOAExecutionSnapshot.Summons_Charge summonsCharge in scList)
                {
                    SIL.AARTO.DAL.Entities.SummonsCharge sCharge = scService.GetByScIntNo(summonsCharge.SCIntNo);
                    if (sCharge == null)
                    {
                        sCharge = new SummonsCharge();
                    }

                    sCharge.SumIntNo = summonsCharge.SumIntNo;
                    sCharge.ChgIntNo = summonsCharge.ChgIntNo;
                    sCharge.LastUser = summonsCharge.LastUser;

                    scService.Save(sCharge);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessSummonsBail(List<Summons_Bail> summonsBails, Summons summons)
        {
            SummonsBailService summonsBailService = new SummonsBailService();

            TList<SummonsBail> summonsBailsDB = summonsBailService.GetBySumIntNo(summons.SumIntNo);

            summonsBailService.Delete(summonsBailsDB);

            if (summonsBails != null && summonsBails.Count > 0)
            {
                foreach (Summons_Bail sb in summonsBails)
                {
                    TList<SummonsBail> bails = new TList<SummonsBail>();
                    bails.Add(new SummonsBail()
                    {
                        SumIntNo = sb.SumIntNo,
                        CrtIntNoTrFrom = sb.CrtIntNoTrFrom,
                        CrtIntNoTrTo = sb.CrtIntNoTrTo,
                        SuBaAmount = (float?)sb.SuBaAmount,
                        SuBaReceiptNumber = sb.SuBaReceiptNumber,
                        WoaArrestedIntNo = sb.WOAArrestedIntNo,
                        SuBaDatePaid = sb.SuBaDatePaid,
                        LastUser = sb.LastUser
                    });

                    summonsBailService.Save(bails);
                }
            }

        }

        /* no use
        void ProcessCourtDate(SummonsBeforeJudgementSnapshot snapShot, SIL.AARTO.DAL.Entities.SummonsBeforeJudgementSnapshot snapshot)
        {
            try
            {
                //if (snapShot.SumIntNoNew.HasValue && snapShot.SumIntNoNew.Value > 0)
                //{
                CourtDatesService cdService = new CourtDatesService();
                SIL.AARTO.DAL.Entities.Summons summons = new SummonsService().GetBySumIntNo(snapShot.SumIntNo);

                SIL.AARTO.DAL.Entities.CourtDates courtDates = cdService.GetByCdateCrtRintNo(snapshot.SumNewCourtDate.Value, summons.CrtRintNo.Value);
                if (courtDates != null)
                {
                    switch (summons.SumType.ToUpper().Trim())
                    {
                        case "S54":
                            courtDates.Cds54Allocated = (short)((courtDates.Cds54Allocated - 1) > 0 ? (courtDates.Cds54Allocated - 1) : 0);
                            break;
                        case "S56":
                            courtDates.Cds56Allocated = (short)((courtDates.Cds56Allocated - 1) > 0 ? (courtDates.Cds56Allocated - 1) : 0);
                            break;
                    }
                    cdService.Save(courtDates);
                }
                // }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        */

        void InsertEvidencePack(int chgIntNo, DateTime date, string descr, string sourceTable, int sourceId, string lastUser)
        {
            try
            {
                EvidencePack evidencePack = new EvidencePack();
                evidencePack.ChgIntNo = chgIntNo;
                evidencePack.EpItemDate = date;
                evidencePack.EpItemDescr = descr;
                evidencePack.EpSourceId = sourceId;
                evidencePack.EpSourceTable = sourceTable;
                evidencePack.LastUser = lastUser;
                // Jake 2013-07-02 added
                evidencePack.EpTransactionDate = DateTime.Now;
                new EvidencePackService().Save(evidencePack);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        void SortSnapshotDataSet(DataSet ds)
        {
            try
            {
                ds.DataSetName = "WoaExecutionSnapshot";

                for (int index = 0; index < ds.Tables.Count; index++)
                {
                    switch (index)
                    {
                        case 0:
                            ds.Tables[index].TableName = "Summons"; break;
                        case 1:
                            ds.Tables[index].TableName = "Notice"; break;
                        case 2:
                            ds.Tables[index].TableName = "Notice_Summons"; break;
                        case 3:
                            ds.Tables[index].TableName = "SumCharge"; break;
                        case 4:
                            ds.Tables[index].TableName = "Charge_SumCharge"; break;
                        case 5:
                            ds.Tables[index].TableName = "Charge"; break;
                        case 6:
                            ds.Tables[index].TableName = "Summons_Charge"; break;
                        case 7:
                            ds.Tables[index].TableName = "WOA"; break;
                        case 8:
                            ds.Tables[index].TableName = "Summons_Bail"; break;

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        XmlDocument ReadSnapshotToXML(DataSet ds)
        {
            XmlDocument document = new XmlDocument();
            MemoryStream memoryStream = new MemoryStream();
            try
            {
                ds.WriteXml(memoryStream, XmlWriteMode.IgnoreSchema);
                memoryStream.Position = 0;

                document.Load(memoryStream);

                memoryStream.Close();
                memoryStream.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return document;
        }

        WoaExecutionSnapshot ReadSnapshotToEntity(XmlDocument document)
        {
            WoaExecutionSnapshot snapShot = new WoaExecutionSnapshot();

            XmlNode root = document.FirstChild;

            foreach (XmlNode node in root.ChildNodes)
            {
                object obj = null;
                switch (node.Name)
                {
                    case "Summons": obj = new Summons(); break;
                    case "Notice": obj = new Notice(); break;
                    case "Notice_Summons": obj = new Notice_Summons(); break;
                    case "Charge": obj = new Charge(); break;
                    case "Charge_SumCharge": obj = new Charge_SumCharge(); break;
                    case "SumCharge": obj = new SumCharge(); break;
                    case "Summons_Charge": obj = new Summons_Charge(); break;
                    case "CourtDates": obj = new CourtDates(); break;
                    case "WOA": obj = new WOA(); break;
                    case "Summons_Bail": obj = new Summons_Bail(); break;
                }

                if (obj != null)
                {
                    PropertyInfo proInfo = null; object value = null; Type type = null;
                    foreach (XmlNode valueNode in node.ChildNodes)
                    {
                        proInfo = obj.GetType().GetProperty(valueNode.Name);
                        if (proInfo != null)
                        {

                            type = proInfo.PropertyType;

                            if (proInfo.PropertyType.IsGenericType && proInfo.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                            {
                                type = proInfo.PropertyType.GetGenericArguments()[0];
                                switch (type.FullName)
                                {
                                    case "System.DateTime":
                                        if (!String.IsNullOrEmpty(valueNode.InnerText)) value = DateTime.Parse(valueNode.InnerText);
                                        break;
                                    default:
                                        value = valueNode.InnerText.Trim();
                                        break;
                                }

                            }
                            else
                            {
                                value = String.IsNullOrEmpty(valueNode.InnerText) == true ? null : valueNode.InnerText.Trim();
                            }

                            proInfo.SetValue(obj, System.Convert.ChangeType(value, type,CultureInfo.InvariantCulture), null);
                        }
                    }


                    switch (obj.GetType().Name)
                    {
                        case "Summons": snapShot.Summons = obj as Summons; break;
                        case "Notice": snapShot.Notices = obj as Notice; break;
                        case "Notice_Summons": snapShot.NoticeSummons = obj as Notice_Summons; break;
                        case "Charge": snapShot.AddCharge(obj as Charge); break;
                        case "Charge_SumCharge": snapShot.AddChargeSumCharge(obj as Charge_SumCharge); break;
                        case "SumCharge": snapShot.AddSumCharge(obj as SumCharge); break;
                        case "Summons_Charge": snapShot.AddSummmonsCharge(obj as Summons_Charge); break;
                        case "CourtDates": snapShot.CourtDates = obj as CourtDates; break;
                        case "WOA": snapShot.AddWOA(obj as WOA); break;
                        case "Summons_Bail": snapShot.AddSummonsBail(obj as Summons_Bail); break;
                    }
                }

            }

            return snapShot;
        }

        XmlDocument SerialJudgementSnapshot(WoaExecutionSnapshot snapShot)
        {
            XmlDocument doc = new XmlDocument();
            XmlElement element = doc.CreateElement("JudgementSnapshot");

            XmlNode root = doc.AppendChild(element);

            //XmlSerializer xs = new XmlSerializer(snapShot.GetType());

            foreach (PropertyInfo pInfo in snapShot.GetType().GetProperties())
            {
                switch (pInfo.Name)
                {
                    case "Summons":
                        ObjectSerializer.Serialize(doc, snapShot.Summons);
                        break;
                    case "Notices":
                        ObjectSerializer.Serialize(doc, snapShot.Notices);
                        break;
                    case "Charges":
                        ObjectSerializer.Serialize<Charge>(doc, snapShot.Charges);
                        break;
                    case "ChargeSumCharges":
                        ObjectSerializer.Serialize<Charge_SumCharge>(doc, snapShot.ChargeSumCharges);
                        break;
                    case "NoticeSummons":
                        ObjectSerializer.Serialize(doc, snapShot.NoticeSummons);
                        //Serialize<Notice_Summons>(doc, snapShot.NoticeSummons);
                        break;
                    case "SumCharges":
                        ObjectSerializer.Serialize<SumCharge>(doc, snapShot.SumCharges);
                        break;
                    case "CourtDates":
                        ObjectSerializer.Serialize(doc, snapShot.CourtDates);
                        break;
                    case "SummonsCharges":
                        ObjectSerializer.Serialize<Summons_Charge>(doc, snapShot.SummonsCharges);
                        break;
                    case "WOA":
                        ObjectSerializer.Serialize<WOA>(doc, snapShot.WOAs);
                        break;
                    case "Summons_Bail":
                        ObjectSerializer.Serialize<Summons_Bail>(doc, snapShot.SummonsBails);
                        break;
                }
            }

            return doc;
        }

       
    }
}
