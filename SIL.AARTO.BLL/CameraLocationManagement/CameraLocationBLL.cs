﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;

namespace SIL.AARTO.BLL.CameraLocationManagement
{
    public class CameraLocationBLL
    {
        public List<ViewCameraLocation> GetViewCameraLocationList(SearchStruct search, int start, int pageLength, ref int total)
        {
            List<ViewCameraLocation> list = new List<ViewCameraLocation>();
            ViewCameraLocationService service = new ViewCameraLocationService();
            string strWhere = string.Empty;
            if (!string.IsNullOrEmpty(search.CameraSerialNo))
                strWhere += string.Format("CamSerialNo like '%{0}%' and ", search.CameraSerialNo.Replace("'", "''"));
            if (search.LocIntNo > 0)
                strWhere += string.Format("LocIntNo={0} and ", search.LocIntNo);
            if (search.AutIntNo > 0)
                strWhere += string.Format("AutIntNo={0} and ", search.AutIntNo);
            if (!string.IsNullOrEmpty(strWhere))
                strWhere = strWhere.Substring(0, strWhere.Length - 4);

            list = service.GetPaged(strWhere, "LocIntNo", start, pageLength, out total).ToList();
            return list;
        }
        public List<SelfCameraLocationEntity> GetSelfPage(SearchStruct search, int start, int pageLength, ref int total)
        {
            CameraLocationService service = new CameraLocationService();
            List<SelfCameraLocationEntity> list = new List<SelfCameraLocationEntity>();
            string strWhere = string.Empty;
            if (!string.IsNullOrEmpty(search.CameraSerialNo))
                strWhere += string.Format("b.CamSerialNo like '%{0}%' and ", search.CameraSerialNo.Replace("'", "''"));
            if (search.LocIntNo > 0)
                strWhere += string.Format("a.LocIntNo={0} and ", search.LocIntNo);
            if (search.AutIntNo > 0)
                strWhere += string.Format("e.AutIntNo={0} and ", search.AutIntNo);
            if (!string.IsNullOrEmpty(strWhere))
                strWhere = strWhere.Substring(0, strWhere.Length - 4);
            IDataReader dataReader = service.GetPageSelfOfCameraLocation(strWhere, "CLIntNo", start, pageLength);
            CreateSelfCameraLocationEntity(list, dataReader);
            if (dataReader.NextResult())
            {
                while (dataReader.Read())
                {
                    total = (int)dataReader[0];
                }
            }
            dataReader.Close();
            return list;
        }
        public List<SelfCameraLocationEntity> CreateSelfCameraLocationEntity(List<SelfCameraLocationEntity> list, IDataReader dataReader)
        {
            while (dataReader.Read())
            {
                SelfCameraLocationEntity entity = new SelfCameraLocationEntity();
                entity.ClIntNo = (dataReader["ClIntNo"] == null ? "" : dataReader["ClIntNo"].ToString());
                entity.LocIntNo = (dataReader["LocIntNo"] == null ? "" : dataReader["LocIntNo"].ToString());
                entity.CameraIntNo = (dataReader["CameraIntNo"] == null ? "" : dataReader["CameraIntNo"].ToString());
                entity.Lastuser = (dataReader["Lastuser"] == null ? "" : dataReader["Lastuser"].ToString());
                entity.CamIdOld = (dataReader["CamIdOld"] == null ? "" : dataReader["CamIdOld"].ToString());
                entity.CamSerialNo = (dataReader["CamSerialNo"] == null ? "" : dataReader["CamSerialNo"].ToString());
                entity.LocCameraCode = (dataReader["LocCameraCode"] == null ? "" : dataReader["LocCameraCode"].ToString());
                entity.LocCode = (dataReader["LocCode"] == null ? "" : dataReader["LocCode"].ToString());
                entity.LocStreetCode = (dataReader["LocStreetCode"] == null ? "" : dataReader["LocStreetCode"].ToString());
                entity.LocStreetName = (dataReader["LocStreetName"] == null ? "" : dataReader["LocStreetName"].ToString());
                entity.LocDescr = (dataReader["LocDescr"] == null ? "" : dataReader["LocDescr"].ToString());
                entity.LocOffenceSpeedStart = (dataReader["LocOffenceSpeedStart"] == null ? "" : dataReader["LocOffenceSpeedStart"].ToString());
                entity.Province = (dataReader["Province"] == null ? "" : dataReader["Province"].ToString());
                entity.City = (dataReader["City"] == null ? "" : dataReader["City"].ToString());
                entity.Streeta = (dataReader["Streeta"] == null ? "" : dataReader["Streeta"].ToString());
                entity.Streetb = (dataReader["Streetb"] == null ? "" : dataReader["Streetb"].ToString());
                entity.AutIntNo = (dataReader["AutIntNo"] == null ? "" : dataReader["AutIntNo"].ToString());
                entity.AutCode = (dataReader["AutCode"] == null ? "" : dataReader["AutCode"].ToString());
                entity.AutNo = (dataReader["AutNo"] == null ? "" : dataReader["AutNo"].ToString());
                entity.AutName = (dataReader["AutName"] == null ? "" : dataReader["AutName"].ToString());
                list.Add(entity);
            }
            return list;
        }
        public bool CheckIsAdd(int locIntNo, int camIntNo)
        {
            bool flag = false;
            CameraLocationService service = new CameraLocationService();
            CameraLocation cl = service.GetByCameraIntNoLocIntNo(camIntNo, locIntNo);
            if (cl != null)
                flag = true;
            return flag;
        }
        public bool CheckIsAddSiteCode(int lsIntNo, int locIntNo, string siteCode)
        {
            bool flag = false;
            LocationSite site = new LocationSite();
            LocationSiteService service = new LocationSiteService();
            List<LocationSite> list = new List<LocationSite>();
            list = service.GetAll().ToList();
            if (lsIntNo > 0)
            {
                list = list.Where(m => m.LsIntNo != lsIntNo).ToList();
            }
            list = list.Where(m => m.LocIntNo == locIntNo && m.LsSiteCode == siteCode).ToList();
            if (list.Count > 0)
                flag = true;
            return flag;
        }
        public void SaveCameraLocation(int locIntNo, int camIntNo, string lastUser)
        {
            CameraLocationService service = new CameraLocationService();
            CameraLocation cl = new CameraLocation();
            cl.LocIntNo = locIntNo;
            cl.CameraIntNo = camIntNo;
            cl.Lastuser = lastUser;
            service.Save(cl);
        }
        public void DeleteCameraLocation(int lcIntNo)
        {
            CameraLocationService service = new CameraLocationService();
            CameraLocation cl = service.GetByClIntNo(lcIntNo);
            service.Delete(cl);
        }
        public void DeleteLocationSite(int intNo)
        {
            LocationSiteService service = new LocationSiteService();
            LocationSite site = service.GetByLsIntNo(intNo);
            service.Delete(site);
        }
        public List<LocationEntity> GetLocationEntityByAutIntNo(int autIntNo)
        {
            LocationService service = new LocationService();
            List<LocationEntity> list = new List<LocationEntity>();
            IDataReader dataReader = service.GetLocationByAutIntNo(autIntNo);
            while (dataReader.Read())
            {
                LocationEntity entity = new LocationEntity();
                entity.LocIntNo = (int)dataReader["LocIntNo"];
                entity.ACIntNo = (int)dataReader["ACIntNo"];
                entity.LocDescr = (dataReader["LocDescr"] == null ? "" : dataReader["LocDescr"].ToString());
                entity.LocCode = (dataReader["LocCode"] == null ? "" : dataReader["LocCode"].ToString());
                entity.LocStreetName = (dataReader["LocStreetName"] == null ? "" : dataReader["LocStreetName"].ToString());
                entity.Province = (dataReader["Province"] == null ? "" : dataReader["Province"].ToString());
                entity.City = (dataReader["City"] == null ? "" : dataReader["City"].ToString());
                entity.StreetA = (dataReader["StreetA"] == null ? "" : dataReader["StreetA"].ToString());
                entity.StreetB = (dataReader["StreetB"] == null ? "" : dataReader["StreetB"].ToString());
                list.Add(entity);
            }
            dataReader.Close();
            return list;
        }

        public List<Camera> GetCameras(string key, int pageLength)
        {
            List<Camera> list = new List<Camera>();
            CameraService service = new CameraService();
            CameraQuery query = new CameraQuery();
            int count = 0;
            query.AppendContains(CameraColumn.CamSerialNo, key);
            return service.Find(query as IFilterParameterCollection, CameraColumn.CamSerialNo.ToString(), 0, pageLength, out count).ToList();
        }
        public List<Camera> GetCameras()
        {
            CameraService service = new CameraService();
            return service.GetAll().OrderBy(m => m.CamSerialNo).ToList();
        }
        public List<Authority> GetAuthority()
        {
            AuthorityService service = new AuthorityService();
            return service.GetAll().OrderBy(m => m.AutName).ToList();
        }

        public List<Location> GetLocationByAutIntNo(int autIntNo)
        {
            LocationService service = new LocationService();
            return service.GetAll().ToList();
        }
        public List<Camera> GetCameraBySerialNo(string number)
        {
            CameraService service = new CameraService();
            CameraQuery query = new CameraQuery();
            query.AppendLike(CameraColumn.CamSerialNo, number);
            return service.Find(query as IFilterParameterCollection).ToList();
        }
        //*********************************location site*********************
        public List<LocationSiteExtend> GetLocationSitePage(SearchStruct search, int start, int pageLength, ref int total)
        {
            LocationSiteService service = new LocationSiteService();
            LocationSiteQuery query = new LocationSiteQuery();
            TList<LocationSite> list = new TList<LocationSite>();
            List<LocationSiteExtend> listEx = new List<LocationSiteExtend>();
            string strWhere = string.Empty;
            string locIntNos = string.Empty;
            if (search.AutIntNo > 0)
            {
                locIntNos = GetLocationIntNos(search.AutIntNo);
                if (!string.IsNullOrEmpty(locIntNos))
                    strWhere += string.Format("LocIntNo in {0} and ", "(" + locIntNos + ")");
                else
                    strWhere += "1=2 and ";
            }
            if (search.LocIntNo > 0)
                strWhere += string.Format("LocIntNo = {0} and ", search.LocIntNo);
            if (!string.IsNullOrEmpty(search.SiteCode))
                strWhere += string.Format("LsSiteCode like '%{0}%' and ", search.SiteCode.Replace("'", "''"));
            if (!string.IsNullOrEmpty(strWhere))
                strWhere = strWhere.Substring(0, strWhere.Length - 4);

            list = service.GetPaged(strWhere, "LSIntNo", start, pageLength, out total);


            return GetLocationSiteExtend(list);
        }
        public List<LocationSiteExtend> GetLocationSiteExtend(TList<LocationSite> list)
        {
            List<LocationSiteExtend> listEx = new List<LocationSiteExtend>();
            foreach (var item in list)
            {
                LocationSiteExtend extend = new LocationSiteExtend();
                extend.LsIntNo = item.LsIntNo;
                extend.LocIntNo = item.LocIntNo;

                AuthorityAndLocationName autAndLoc = GetAuthorityAndLocationName(item.LocIntNo);
                extend.LocDesc = autAndLoc.LocDesc;
                extend.AutName = autAndLoc.AutName;
                extend.LsSiteCode = item.LsSiteCode;
                extend.LsSiteDescription = item.LsSiteDescription;
                listEx.Add(extend);
            }
            return listEx;
        }
        public AuthorityAndLocationName GetAuthorityAndLocationName(int locIntNo)
        {
            AuthorityAndLocationName autAndLoc = new AuthorityAndLocationName();
            LocationService locService = new LocationService();
            AuthCourtService ASService = new AuthCourtService();
            AuthorityService autService = new AuthorityService();
            Location loc = locService.GetByLocIntNo(locIntNo);

            if (loc != null)
            {
                autAndLoc.LocDesc = loc.LocDescr;
                AuthCourt ac = ASService.GetByAcIntNo(loc.AcIntNo);
                if (ac != null)
                {
                    Authority aut = autService.GetByAutIntNo(ac.AutIntNo);
                    if (aut != null)
                    {
                        autAndLoc.AutIntNo = aut.AutIntNo;
                        autAndLoc.AutName = aut.AutName;
                    }
                }
            }
            return autAndLoc;
        }
        public LocationSiteExtend GetLocationSite(int intNo)
        {
            LocationSiteExtend extend = new LocationSiteExtend();
            LocationSiteService service = new LocationSiteService();
            LocationSite site = service.GetByLsIntNo(intNo);
            if (site != null)
            {
                extend.LsIntNo = site.LsIntNo;
                extend.LocIntNo = site.LocIntNo;

                AuthorityAndLocationName autAndLoc = GetAuthorityAndLocationName(site.LocIntNo);
                extend.LocDesc = autAndLoc.LocDesc;
                extend.AutName = autAndLoc.AutName;
                extend.AutIntNo = autAndLoc.AutIntNo;
                extend.LsSiteCode = site.LsSiteCode;
                extend.LsSiteDescription = site.LsSiteDescription;
            }
            return extend;
        }
        public void SaveLocationSite(int LSIntNo, int locIntNo, string siteCode, string siteDesc, string lastUser)
        {
            LocationSiteService service = new LocationSiteService();
            LocationSite site = new LocationSite();
            if (LSIntNo > 0)
                site = service.GetByLsIntNo(LSIntNo);
            site.LocIntNo = locIntNo;
            site.LsSiteCode = siteCode;
            site.LsSiteDescription = siteDesc;
            site.Lastuser = lastUser;

            service.Save(site);
        }
        public string GetLocationIntNos(int autIntNo)
        {
            StringBuilder sBuild = new StringBuilder();
            AuthCourtService service = new AuthCourtService();
            List<AuthCourt> list = service.GetByAutIntNo(autIntNo).ToList();
            List<int> locIntNoList = new List<int>();
            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    LocationService serviceLocation = new LocationService();
                    List<Location> locationList = serviceLocation.GetByAcIntNo(item.AcIntNo).ToList();
                    foreach (var location in locationList)
                    {
                        if (!locIntNoList.Contains<int>(location.LocIntNo))
                            locIntNoList.Add(location.LocIntNo);
                    }
                }
            }
            for (int i = 0; i < locIntNoList.Count; i++)
            {
                sBuild.Append(string.Format("{0},", locIntNoList[i]));
            }
            if (sBuild.Length > 0)
                sBuild.Remove(sBuild.Length - 1, 1);

            return sBuild.ToString();
        }
    }
    public struct SearchStruct
    {
        public string CameraSerialNo;
        public int LocIntNo;
        public int AutIntNo;
        public string LastUser;
        public string SiteCode;
    }
    public struct LocationEntity
    {
        public int LocIntNo;
        public int ACIntNo;
        public string LocCode;
        public string LocStreetName;
        public string Province;
        public string City;
        public string StreetA;
        public string StreetB;
        public string LocDescr;
    }
    public struct SelfCameraLocationEntity
    {
        public string ClIntNo;
        public string LocIntNo;
        public string CameraIntNo;
        public string Lastuser;
        public string CamIdOld;
        public string CamSerialNo;
        public string LocCameraCode;
        public string LocCode;
        public string LocStreetCode;
        public string LocStreetName;
        public string LocDescr;
        public string LocOffenceSpeedStart;
        public string Province;
        public string City;
        public string Streeta;
        public string Streetb;
        public string AutIntNo;
        public string AutCode;
        public string AutNo;
        public string AutName;
    }
    public class Message
    {
        private bool status = false;

        public bool Status
        {
            get { return status; }
            set { status = value; }
        }
        private string data;

        public string Data
        {
            get { return data; }
            set { data = value; }
        }
    }
    public class LocationSiteExtend : LocationSite
    {
        public string AutName { get; set; }
        public int AutIntNo { get; set; }
        public string LocDesc { get; set; }
    }
    public struct AuthorityAndLocationName
    {
        public int AutIntNo;
        public string AutName;
        public string LocDesc;
    }
}
