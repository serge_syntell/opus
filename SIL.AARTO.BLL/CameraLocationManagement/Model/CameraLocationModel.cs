﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;


namespace SIL.AARTO.BLL.CameraLocationManagement.Model
{
    public class CameraLocationModel
    {
        public string CameraSearch { get; set; }
        public string LocationSearch { get; set; }
        public string CameraSerialNo { get; set; }
        public string LocationName { get; set; }
        public List<SelfCameraLocationEntity> Grid { get; set; }
        public SelectList AutList { get; set; }
        public int AutSearchSelected { get; set; }
        public string AutSelected { get; set; }


        public SelectList LocList { get; set; }
        public int LocSearchSelected { get; set; }
        public string LocSelected { get; set; }

        //
        public SelectList CameraList { get; set; }
        public int CameraSelected { get; set; }


        //
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public int Page { get; set; }
        //
        public string LocationShowModel { get; set; }
        //location site
        public int LSIntNo { get; set; }
        public string LSSiteCode { get; set; }
        public string SearchSiteCode { get; set; }
        public string LSSiteDescription { get; set; }
        public List<LocationSiteExtend> SiteList { get; set; }
    }
}
