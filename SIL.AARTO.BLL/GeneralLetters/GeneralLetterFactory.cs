﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.GeneralLetters
{
    public class GeneralLetterFactory<T>
    {
        private static string db = "SIL.AARTO.BLL";
        private static T _dal = default(T);

        public static T GetDAL(string DocumentTypeId)
        {
            if (_dal == null)
            {
                string _type = typeof(T).Name;

                System.Reflection.Assembly t = System.Reflection.Assembly.Load(db);
                Type b = t.GetType(db + "." + DocumentTypeId + "." + _type);
                Object obj = Activator.CreateInstance(b);
                _dal = (T)obj;
            }

            return _dal;
        }
    }
}
