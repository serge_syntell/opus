﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;
using System.Data.SqlClient;
using SIL.AARTO.BLL.GeneralLetters;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.BLL.Admin;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.Merger;
using ceTe.DynamicPDF.Imaging;
using System.IO;
using System.Data;
using System.Configuration;

namespace SIL.AARTO.BLL.GeneralLetters
{
    public interface GeneralLetterBase
    {
        byte[] SetPDFBody(GeneralLetterEntity model, string tempFileName);
        int AddPrintGeneralLettersHistory(GeneralLetterEntity model, string lastUser);
    }
}
