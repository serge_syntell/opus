﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.QueueLibrary;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Web.Mvc;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.GeneralLetters
{
    public class GeneralLetterEntity
    { 
        public SelectList LetterTemplates { get; set; }
        public string LetterTemplate { get; set; }
        public string TicketSequenceNo { get; set; }

        public int  NotIntNo { get; set; }

        public string OffenderName { get; set; }

        public string OffenderAddress { get; set; }

        public int ChgIntNo { get; set; }

        public string PrintDate { get; set; }

        public string ErrorMsg { get; set; }
        public string LookUpErrorMsg { get; set; }
        public int AutIntNo { get; set; }
    }
}
