﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.BLL.Admin;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.Merger;
using ceTe.DynamicPDF.Imaging;
using System.IO;
using System.Data;
using System.Configuration;
using SIL.AARTO.BLL.JudgementSnapshot;
using System.Transactions;
using SIL.AARTO.BLL.Utility.Printing;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Web;

namespace SIL.AARTO.BLL.GeneralLetters
{
    public class GeneralLetterManager : GeneralLetterBase
    {

        private readonly string connString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString();
        private static readonly GeneralLettersTemplateService service = new GeneralLettersTemplateService();

        private SqlConnection con;

        public GeneralLetterManager()
        {
            this.con = new SqlConnection(connString);
        }

        public SelectList GetGeneralLettersTemplateList()
        {
            List<GeneralLettersTemplate> tList = GetGeneralLettersTemplateListData();
            //return new SelectList(tList, "GLTIntNo", "GLTName");
            return new SelectList(tList, "GLTName", "GLTDescr");
        }

        //public List<GeneralLettersTemplate> GetAll()
        //{
        //    return service.GetAll().OrderBy(c => c.GltName).ToList();
        //}

        //public SelectList GetSelectLetterTemplateList()
        //{
        //    List<GeneralLettersTemplate> tList = GetAll();
        //    return new SelectList(tList, "GLTIntNo", "GLTDescr");
        //}


        public List<GeneralLettersTemplate> GetGeneralLettersTemplateListData()
        {
            List<GeneralLettersTemplate> list = new List<GeneralLettersTemplate>();

            using(SqlCommand command=this.con.CreateCommand()){
                command.CommandText = "GeneralLettersTemplate_GetList";
                command.CommandType = CommandType.StoredProcedure;
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                using(SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection)){
                    while (reader.Read())
                    {
                        list.Add(new GeneralLettersTemplate
                        {
                            //GltIntNo = Convert.ToInt32(reader["GltIntNo"]),
                            //GltName = reader["GltName"].ToString() + " - " + reader["GLTDescr"].ToString()
                            GltName = reader["GltName"].ToString(),
                            GltDescr = reader["GltDescr"].ToString()
                        });
                    }
                }
                return list;
            }
        }

        protected bool ReportCreated { get; set; }

        public byte[] SetPDFBody(GeneralLetterEntity model, string tempFileName)
        {
            byte[] buffer = new byte[] { };
            DocumentLayout docLayout = new DocumentLayout(tempFileName);
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblOffenderName");
            lbl.Text = model.OffenderName==null ? "" : model.OffenderName.Trim();
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblOffenderAddress");
            lbl.Text = model.OffenderAddress == null ? "" : model.OffenderAddress.Trim();
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("lblTicketSequenceNo");
            lbl.Text = model.TicketSequenceNo.Trim().Replace("/", " / ");
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)docLayout.GetElementById("PrintDate");
            lbl.Text = String.Format("{0:yyyy-MM-dd}", DateTime.Now.Date);

            StoredProcedureQuery query = (StoredProcedureQuery)docLayout.GetQueryById("Query");
            query.ConnectionString = connString;
            ParameterDictionary parameters = new ParameterDictionary();

            parameters.Add("ticketSequenceNo", model.TicketSequenceNo);

            Document doc = docLayout.Run(parameters);
            buffer = doc.Draw();

            return buffer;
        }

        public int AddPrintGeneralLettersHistory(GeneralLetterEntity model, string lastUser)
        {
            int result = 0;
            using (TransactionScope scope = new TransactionScope())
            {
                //Insert a record on the history screen EvidencePack
                EvidencePack ep = new EvidencePack()
                {
                    ChgIntNo = model.ChgIntNo,
                    EpItemDate = DateTime.Now,
                    //EpItemDescr = String.Format("The general letter {0} printed by {1} on {2}", model.LetterTemplate, lastUser, String.Format("{0:yyyy-MM-dd}", DateTime.Now)),
                    EpItemDescr = String.Format("General letter [{0}] printed", service.GetByGltName(model.LetterTemplate).GltDescr),
                    EpSourceId = model.NotIntNo,
                    EpSourceTable = "Notice",
                    EpTransactionDate = DateTime.Now,
                    LastUser = lastUser
                };
                new EvidencePackService().Save(ep);

                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connString);
                punchStatistics.PunchStatisticsTransactionAdd(model.AutIntNo, lastUser, PunchStatisticsTranTypeList.PrintGeneralLetters, PunchAction.Add);

                scope.Complete();
                result = 1;
            }
            return result;
        }

        public GeneralLetterEntity GetOffenderDetail(string ticketSequenceNo)
        {
            var model = new GeneralLetterEntity();

            using(SqlCommand command = con.CreateCommand()){
                command.CommandText = "GeneralLettersTemplate_GetByNotTicketNo";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@ticketSequenceNo", SqlDbType.VarChar)).Value = ticketSequenceNo;

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (reader.Read())
                    {
                        model.OffenderName = reader["OffenderName"].ToString();
                        model.OffenderAddress = reader["OffenderAddress"].ToString();
                        model.ChgIntNo = (Int32)reader["ChgIntNo"];
                        model.NotIntNo = (Int32)reader["NotIntNo"];
                    }
                }               
            }
            return model;
        }
    }
}
