﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SIL.AARTO.BLL.EntLib;



namespace SIL.AARTO.BLL.Export
{
    class LoadViolations
    {
        GeneralFunctions gen = new GeneralFunctions();
        public bool ProcessLocalFolders(string targetDirectory, string contractorCode, string exportDirectory)
        {
            //tmsErrorLogDetails.TELProcName = "ProcessLocalFolders";

            bool failed = false;

            string folder = Path.GetFileName(targetDirectory);

            //if (!folder.ToLower().Equals("received") && !folder.ToLower().Equals("sent"))
            //{
            //exportDirectory += "\\" + Path.GetFileName(targetDirectory);

            string file = ""; string errorMessage = string.Empty;

            if (Directory.Exists(targetDirectory))
            {
                string[] fileEntries = System.IO.Directory.GetFiles(targetDirectory);
                foreach (string fileName in fileEntries)
                {
                    file = Path.GetFileName(fileName);
                    if (!File.Exists(fileName))
                    {
                        EntLibLogger.WriteLog("General", "ProcessLocalFolders", String.Format("ProcessLocalFolders :Image file does not exists, Image file name: {0} ", file));
                    }
                    else
                    {

                        EntLibLogger.WriteLog("General", "ProcessLocalFolders", "ProcessLocalFolders: processing " + file + " at " + DateTime.Now.ToString());


                        bool getFolder = gen.CheckFolder(exportDirectory, out errorMessage);

                        if (getFolder)
                        {

                            EntLibLogger.WriteLog("General", "ProcessLocalFolders", "Unable to move files to Receive folder");

                            if (!String.IsNullOrEmpty(errorMessage))
                                EntLibLogger.WriteLog("Error", "ProcessLocalFolders", errorMessage + " at " + DateTime.Now.ToString());
                           
                            failed = true;

                            return failed;
                        }
                        try
                        {
                            gen.CopyFile(fileName, exportDirectory, file, out errorMessage);
                            if (String.IsNullOrEmpty(errorMessage))
                                EntLibLogger.WriteLog("General", "ProcessLocalFolders", "ProcessLocalFolders: processed " + file + " at " + DateTime.Now.ToString());
                            else
                                EntLibLogger.WriteLog("Error", "ProcessLocalFolders", errorMessage + file + " at " + DateTime.Now.ToString());

                        }
                        catch (Exception ex)
                        {
                            EntLibLogger.WriteErrorLog(ex, "General", "ProcessLocalFolders");
                            failed = true;
                            return failed;
                        }

                        //try
                        //{
                        //    if (!rootFolder) Directory.Delete(targetDirectory, true);
                        //}
                        //catch { }

                    }
                }
            }
            if (Directory.Exists(targetDirectory))
            {

                // read the names of the subdirectories of this root folder.
                string[] subdirectoryEntries = System.IO.Directory.GetDirectories(targetDirectory);

                // Recurse into subdirectories of this directory.
                foreach (string subdirectory in subdirectoryEntries)
                {
                    string subfolder = Path.GetFileName(subdirectory);
                    ProcessLocalFolders(subdirectory, contractorCode, exportDirectory);

                }
            }

            return failed;
        }



    }
}
