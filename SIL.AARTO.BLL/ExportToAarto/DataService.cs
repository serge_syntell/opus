﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;


namespace SIL.AARTO.BLL.Export
{
   public class DataService
    {
      //public  static SIL.IMX.DAL.Services.FilmService ImxFilmService = new SIL.IMX.DAL.Services.FilmService();
      //public static SIL.IMX.DAL.Services.FrameService ImxFrameService = new SIL.IMX.DAL.Services.FrameService();
      //public static SIL.IMX.DAL.Services.FieldPackService ImxFieldPackService = new FieldPackService();
      //public static SIL.IMX.DAL.Services.ScanImageService ImxScanImageService = new SIL.IMX.DAL.Services.ScanImageService();
      //public static SIL.IMX.DAL.Services.TrafficOfficerService ImxTrafficOfficerService = new IMX.DAL.Services.TrafficOfficerService();


      public static SIL.AARTO.DAL.Services.FilmService AartoFilmService = new SIL.AARTO.DAL.Services.FilmService();
      public static SIL.AARTO.DAL.Services.FrameService AartoFrameService = new SIL.AARTO.DAL.Services.FrameService();
      public static SIL.AARTO.DAL.Services.ScanImageService AartoScanImageService = new SIL.AARTO.DAL.Services.ScanImageService();
      public static SIL.AARTO.DAL.Services.AuthorityService AartoAuthorityService = new SIL.AARTO.DAL.Services.AuthorityService();
      public static SIL.AARTO.DAL.Services.ImageFileServerService AartoFileServerService = new SIL.AARTO.DAL.Services.ImageFileServerService();
      public static SIL.AARTO.DAL.Services.TrafficOfficerService AartoTrafficOfficerService = new DAL.Services.TrafficOfficerService();
    }
}
