﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.EntLib;
using System.IO;
using System.Xml;

using System.Transactions;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTO.BLL.Model;

namespace SIL.AARTO.BLL.Export
{

    public class ExportDataManager
    {

        //# Comment out no longer in use
        //public void ExportWithCentralModel()
        //{
        //    //Get Films from IMX
        //    List<SIL.IMX.DAL.Entities.Film> imxFilms = GetFilmsToExport();

        //    bool isError = false;
        //    foreach (SIL.IMX.DAL.Entities.Film imxFilm in imxFilms)
        //    {
        //        using (SIL.AARTO.DAL.Services.ConnectionScope.CreateTransaction())
        //        {
        //            isError = false;
        //            //Begin export Film
        //            if (CheckFilmInAarto(imxFilm.FilmNo))
        //            {
        //                // Film No does exists in AARTo
        //                EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", String.Format("Film No does not exists in Aarto ,Film No :{0}", imxFilm.FilmNo));
        //            }
        //            SIL.AARTO.DAL.Entities.Film aartoFilm = new SIL.AARTO.DAL.Entities.Film();
        //            if (UpdateFilmToAarto(imxFilm, aartoFilm))
        //            {
        //                // Insert into Film Table in AARTO
        //                aartoFilm = DataService.AartoFilmService.Save(aartoFilm);

        //                if (aartoFilm == null)
        //                {
        //                    // Inert failure
        //                    EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", String.Format("Load Film failure ,Film No :{0}", imxFilm.FilmNo));
        //                    continue;
        //                }

        //                // Begin export Frames 
        //                foreach (SIL.IMX.DAL.Entities.Frame imxFrame in GetFramesInIMXByFilmIntNo(imxFilm.FilmIntNo))
        //                {
        //                    if (!CheckFrameInAarto(aartoFilm.FilmIntNo, imxFrame.FrameNo))
        //                    {
        //                        // Frame does exists in AARTO
        //                        EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", String.Format("Frame does not exists in Aarto ,Film No :{0}   FrameNo : {1} ", imxFilm.FilmNo, imxFrame.FrameNo));
        //                    }

        //                    SIL.AARTO.DAL.Entities.Frame aartoFrame = new SIL.AARTO.DAL.Entities.Frame();
        //                    if (UpdateFrameToAarto(imxFrame, aartoFrame, aartoFilm.FilmIntNo))
        //                    {
        //                        aartoFrame = DataService.AartoFrameService.Save(aartoFrame);
        //                        if (aartoFrame == null)
        //                        {
        //                            //Insert failure
        //                            EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", String.Format("Load Frame failure ,Film No :{0}    FrameNo : {1}", imxFilm.FilmNo, imxFrame.FrameNo));
        //                            isError = true;
        //                            break;
        //                        }
        //                        //Begin export ScanImages in AARTO
        //                        foreach (SIL.IMX.DAL.Entities.ScanImage imxScanImage in GetScanImageInIMXByFrameIntNo(imxFrame.FrameIntNo))
        //                        {
        //                            SIL.AARTO.DAL.Entities.ScanImage aartoScanImage = new SIL.AARTO.DAL.Entities.ScanImage();
        //                            if (UpdateScanImageToAarto(imxScanImage, aartoScanImage, aartoFrame.FrameIntNo))
        //                            {
        //                                aartoScanImage = DataService.AartoScanImageService.Save(aartoScanImage);
        //                                if (aartoScanImage == null)
        //                                {
        //                                    //Insert failure
        //                                    EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", String.Format("Load Scan Images failure ,Film No :{0}    FrameNo : {1}", imxFilm.FilmNo, imxFrame.FrameNo));
        //                                    isError = true;
        //                                    break;
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        // Assignment value from IMX Frame to AARTO Frame failure 
        //                    }

        //                    if (isError) break;
        //                }

        //                if (!isError)
        //                    SIL.AARTO.DAL.Services.ConnectionScope.Complete();

        //            }
        //            else
        //            {
        //                // Assignment value from IMX Film to AARTO Film failure 
        //            }

        //        }
        //    }
        //}

        //Not use
        //public void ExportWithRemoteModel(string filmPath)
        //{
        //    string imageLocalFolder = String.Empty;
        //    string imageRemoteFolder = String.Empty;
        //    string authCode = String.Empty;
        //    List<XmlFilmEntity> xmlFilmEntityList = new List<XmlFilmEntity>();
        //    foreach (string xmlFileName in Directory.GetFiles(filmPath))
        //    {
        //        FileInfo xmlFile = new FileInfo(xmlFileName);
        //        if (xmlFile.Extension.ToLower().Equals("xml"))
        //        {
        //            XmlDocument xmlDocument = new XmlDocument();
        //            xmlDocument.LoadXml(File.ReadAllText(xmlFileName, Encoding.UTF8));

        //            XmlFilmEntity xmlFilmEntity = XmlLoadManager.GetFilmEntityFromXml(xmlDocument);
        //            if (xmlFilmEntity != null)
        //                xmlFilmEntityList.Add(xmlFilmEntity);
        //        }
        //    }

        //    foreach (XmlFilmEntity xmlFilmEntity in xmlFilmEntityList)
        //    {
        //        FilmEntity filmEntity = new FilmEntity();
        //        filmEntity = xmlFilmEntity.Film;

        //        foreach (FrameEntity frameEntity in xmlFilmEntity.Frames)
        //        {
        //            imageLocalFolder = filmPath + "/" + frameEntity.FrameNo;
        //            if (Directory.Exists(imageLocalFolder))
        //            {
        //                authCode = imageLocalFolder.Substring(0, imageLocalFolder.IndexOf(@"\"));
        //                SIL.AARTO.DAL.Entities.Authority authority = GetAuthorityFromAarto(authCode);
        //                if (authority == null)
        //                {
        //                    // Authority does not exists in Aarto
        //                    continue;
        //                }

        //                imageRemoteFolder = string.Format(@"{0}\{1}\{2}\{3}\{4}\{5}",
        //                    authCode,
        //                    frameEntity.OffenceDate.Year,
        //                    frameEntity.OffenceDate.Month,
        //                    frameEntity.OffenceDate.Day,
        //                    filmEntity.FilmNo,
        //                    frameEntity.FrameNo);

        //            }
        //        }
        //    }
        //}

        public int TimeOutValue { get; set; }

        public ExportDataManager()
        { }
        public ExportDataManager(int timeOut)
        {
            TimeOutValue = timeOut;
        }

        // 2013-07-17 add parameter lastUser by Henry
        public bool DoImport(string filmPath, string filmNo, out string message, string lastUser)
        {
            string targetDirectory = String.Empty;
            string exportDirectory = String.Empty;
            string frameImagePath = String.Empty;
            string authCode = String.Empty;
            bool isContinue = true;
            message = string.Empty;
            try
            {
                List<SIL.AARTO.DAL.Entities.Authority> authorities = DataService.AartoAuthorityService.GetAll().ToList();
                List<SIL.AARTO.DAL.Entities.ImageFileServer> imageServerList = DataService.AartoFileServerService.GetAll().ToList();

                //TransactionScopeOption option = new TransactionScopeOption();
             
                //using (SIL.AARTO.DAL.Services.ConnectionScope.CreateTransaction())

                //using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(TimeOutValue)))
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromMilliseconds(TimeOutValue)))// changed by Oscar,2013-04-25
                {
                    SIL.AARTO.DAL.Entities.Film film = DataService.AartoFilmService.GetByFilmNo(filmNo);

                    int processedFrames = 0;
                    int processedFramesLastTime = 0;
                    if (film == null)
                    {
                        isContinue = false;
                        message = String.Format("Film does not exist the in Aarto database: Film {0}", filmNo);
                        EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", message);

                        return isContinue;
                    }
                    if (film.FilmLoadStatus >= (int)FilmLoadStatusList.FilmLoadedSuccessfully && film.ValidateDataDateTime.HasValue)
                    {
                        isContinue = false;
                        EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", "Film " + film.FilmNo + " has already been successfully loaded");
                        return isContinue;
                    }

                    // Check if this authority processes NaTIS data last
                    AuthorityRuleInfo authRule = new AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(film.AutIntNo,"0400");

                    LoadViolations violation = new LoadViolations();
                    authCode = DataService.AartoAuthorityService.GetByAutIntNo(film.AutIntNo).AutCode;
                    if (String.IsNullOrEmpty(authCode))
                    {
                        EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", String.Format("Authority code is null in Aarto Database"));
                    }
                    List<SIL.AARTO.DAL.Entities.Frame> frames = DataService.AartoFrameService.GetByFilmIntNo(film.FilmIntNo).ToList();

                    SIL.AARTO.DAL.Entities.ImageFileServer imageFileServer = null;
                    SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.ScanImage> scanImageList = new SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.ScanImage>();

                    //SIL.IMX.DAL.Entities.Film imxFilm = DataService.ImxFilmService.GetByFilmNo(filmNo);
                    //if (imxFilm == null)
                    //{
                    //    isContinue = false;
                    //    message = String.Format("No film found in IMX  , FilmNo : {0}", filmNo);
                    //    return isContinue; ;
                    //}

                    if (frames == null || frames.Count == 0)
                    {
                        isContinue = false;
                        message = String.Format("No frames found in Aarto database for Film: {0}", film.FilmNo);
                        return isContinue;
                    }

                    //DateRulesDetails dateRule = new DateRulesDetails();
                    //dateRule.AutIntNo = autIntNo;
                    //dateRule.DtRStartDate = "NotOffenceDate";
                    //dateRule.DtREndDate = "NotIssue1stNoticeDate";
                    //dateRule.LastUser = "TMS_ViolationLoader";
                    //DefaultDateRules rule = new DefaultDateRules(dateRule, _connStr);

                    DateRuleInfo dateRule = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(film.AutIntNo, "NotOffenceDate", "NotIssue1stNoticeDate");
                    int expiryNoOfDays = dateRule.ADRNoOfDays;

                    foreach (SIL.AARTO.DAL.Entities.Frame frame in frames)
                    {

                        if (frame.FrameStatus.Equals((int)FrameStatusList.FrameLoadedViaImporterSuccessfully))
                        {
                            // IF frame status =1000 , Image loaded for this frame
                            processedFramesLastTime += 1;
                            continue;
                        }
                        targetDirectory = SearchFrameFromCD(filmPath, film.FilmNo, frame.FrameNo);
                        if (String.IsNullOrEmpty(targetDirectory))
                        {
                            // System can not find the image folder
                            EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", String.Format("Could not find the images folder for Film: {0} Frame {1}", film.FilmNo, frame.FrameNo));
                            continue;
                        }
                        frameImagePath = GenerateFrameImagePath(film, frame, authCode);

                        if (String.IsNullOrEmpty(frameImagePath))
                        {
                            isContinue = false;
                            message = String.Format("Could not generate frame image path. Please check the film and frame data in AARTO database. film: {0} frame: {1}", film.FilmNo, frame.FrameNo);
                            return isContinue;
                        }

                        scanImageList = DataService.AartoScanImageService.GetByFrameIntNo(frame.FrameIntNo);

                        if (Directory.Exists(targetDirectory))
                        {
                            imageFileServer = imageServerList.SingleOrDefault(i => i.AaSysFileTypeId.Equals((int)AartoSystemFileTypeList.TMSScanImage) && i.IsDefault.Equals(true));
                            if (imageFileServer == null)
                            {
                                // Image File Server does not exists in Aarto;
                                EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", String.Format("Image file server does not exist in Aarto for ImageShareName = CameraImages"));
                                return isContinue = false;
                            }
                            exportDirectory = String.Format(@"\\{0}\{1}\{2}", imageFileServer.ImageMachineName, imageFileServer.ImageShareName, frameImagePath);
                            if (!violation.ProcessLocalFolders(targetDirectory, "", exportDirectory))
                            {

                                if (authRule == null)
                                {
                                    EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader",
                                        String.Format("Authority rule does not exist in AARTO database: code {0}", "0400"));

                                    return isContinue = false;
                                }
                                if (authRule.ARString.Trim().ToUpper().Equals("Y"))
                                {
                                    //"Natis files will be sent after Adjudication";
                                    frame.FrameStatus = (int)FrameStatusList.NL_FramesLoadedReadyForAdjudication;
                                }
                                else if (authRule.ARString.Trim().ToUpper().Equals("N"))
                                {
                                    //"Natis files will be sent before Verification";
                                    frame.FrameStatus = (int)FrameStatusList.FrameLoadedViaImporterSuccessfully;
                                }
                                else
                                {
                                    EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader",
                                       String.Format("Authority rule code does not set in AARTO database: code {0}", "0400"));

                                    return isContinue = false;
                                }
                                frame.IfsIntNo = imageFileServer.IfsIntNo;
                                frame.FrameImagePath = frameImagePath;
                                frame.LastUser = lastUser;
                                DataService.AartoFrameService.Save(frame);

                                processedFrames++;
                                SIL.AARTO.DAL.Entities.ScanImage aartoScanImage = new DAL.Entities.ScanImage();
                                //SIL.IMX.DAL.Entities.ScanImage tempScanImage = new IMX.DAL.Entities.ScanImage();

                                // SIL.IMX.DAL.Entities.Frame imxFrame = DataService.ImxFrameService.GetByFilmIntNoFrameNo(imxFilm.FilmIntNo, frame.FrameNo);

                                //List<SIL.IMX.DAL.Entities.ScanImage> imxScanImages = DataService.ImxScanImageService.GetByFrameIntNo(imxFrame.FrameIntNo).ToList();

                                //not use
                                //FileInfo[] scanImageFiles = new DirectoryInfo(targetDirectory).GetFiles();

                                //foreach (SIL.AARTO.DAL.Entities.ScanImage image in scanImageList)
                                //{
                                //    if (scanImageFiles.Where(f => f.Name.Equals(image.JpegName)).Count() > 0)
                                //    {

                                //    }
                                //}
                                foreach (FileInfo imageFile in new DirectoryInfo(targetDirectory).GetFiles())
                                {
                                    aartoScanImage = scanImageList.SingleOrDefault(s => s.FrameIntNo.Equals(frame.FrameIntNo) && s.JpegName.Equals(imageFile.Name));
                                    //tempScanImage = imxScanImages.SingleOrDefault(s => s.JpegName.Equals(imageFile.Name));

                                    if (aartoScanImage == null)
                                    {
                                        //File.Delete(imageFile.FullName);

                                        EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", String.Format("Image does not exist in Aarto: Image Name {0} Film {1} Frame {2}", imageFile.Name, film.FilmNo, frame.FrameNo));

                                        isContinue = false;
                                        message = String.Format("Image does not exist in Aarto: Image Name {0} Film {1} Frame {2}", imageFile.Name, film.FilmNo, frame.FrameNo);

                                        return isContinue;
                                        //aartoScanImage = new DAL.Entities.ScanImage();
                                        //aartoScanImage.JpegName = imageFile.Name;
                                        //aartoScanImage.FrameIntNo = frame.FrameIntNo;
                                        //if (tempScanImage.SitIntNo.HasValue)
                                        //{
                                        //    aartoScanImage.ScImType = ((SIL.IMX.DAL.Entities.ScanImageTypeList)tempScanImage.SitIntNo.Value).ToString();
                                        //}
                                        //aartoScanImage.LastUser = "SIL.AARTO.IMX.ImageLoader";

                                        //DataService.AartoScanImageService.Save(aartoScanImage);
                                    }
                                }

                                // DataService.AartoScanImageService.Save(scanImageList);

                                //push queue

                                try
                                {
                                    QueueItemProcessor queueProcessor = new QueueItemProcessor();
                                    QueueItem item = new QueueItem();
                                    item.Body = frame.FrameIntNo.ToString();
                                    item.ActDate = frame.OffenceDate.AddDays(expiryNoOfDays);
                                    item.QueueType = ServiceQueueTypeList.CancelExpiredViolations_Frame;
                                    item.Group = authCode;
                                    queueProcessor.Send(item);

                                    if (authRule.ARString.Trim().ToUpper().Equals("Y"))
                                    {
                                        item = new QueueItem();
                                        item.Body = frame.FrameIntNo.ToString();
                                        item.Group = authCode;
                                        TimeSpan ts = DateTime.Now - frame.OffenceDate;
                                        item.Priority = expiryNoOfDays - ts.Days;
                                        item.QueueType = ServiceQueueTypeList.Frame_Natis;
                                        queueProcessor.Send(item);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    EntLibLogger.WriteErrorLog(ex, "Error", "SIL.AARTO.IMX.ImageLoader");
                                    isContinue = false;
                                    Environment.Exit(-1);
                                    break;
                                }
                            }
                            else
                            {
                                isContinue = false;
                                message = String.Format("Load Images failed: Film {0} Frame {1}", film.FilmNo, frame.FrameNo);
                                break;
                            }

                        }
                        else
                        {
                            message = String.Format("Invalid directory: {0}", targetDirectory);
                            // Directory does not exists
                            EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", message);
                            isContinue = false;
                            break;
                        }
                    }
                    if (isContinue)
                    {
                        if (film.NoOfFrames.Equals(processedFrames + processedFramesLastTime))
                        {
                            film.FilmLoadStatus = (int)FilmLoadStatusList.FilmLoadedSuccessfully;
                            film.LastUser = "SIL.AARTO.IMX.ImageLoader";
                            DataService.AartoFilmService.Save(film);
                        }
                        else
                        {
                            message = String.Format("Number of frames does not match");
                            // Directory does not exists
                            EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", message);
                            isContinue = false;
                            return isContinue;
                        }

                        ////Update FieldPack status to ExportedToOPUS

                        //FieldPack fieldPack = DataService.ImxFieldPackService.GetByFilmNo(film.FilmNo);
                        //if (fieldPack != null)
                        //{
                        //    fieldPack.FpsIntNo = (int)FieldPackStatusList.ExportedToOPUS;
                        //    fieldPack.LastUser = "SIL.AARTO.IMX.ImageLoader";
                        //    DataService.ImxFieldPackService.Save(fieldPack);

                        //    EntLibLogger.WriteLog("General", "SIL.AARTO.IMX.ImageLoader", String.Format("Field Pack has been imported to OPUS ,FilmNo : {0}", film.FilmNo));
                        //}

                        //SIL.AARTO.DAL.Services.ConnectionScope.Complete();
                        tran.Complete();
                    }

                }


            }
            catch (Exception ex)
            {
                isContinue = false;
                EntLibLogger.WriteErrorLog(ex, "Error", "SIL.AARTO.IMX.ImageLoader");
                message = ex.Message;
            }

            return isContinue;
        }

        public List<SIL.AARTO.DAL.Entities.Film> GetWaittingForLoadImagesFilms(int authIntNo)
        {
            List<SIL.AARTO.DAL.Entities.Film> films = new List<SIL.AARTO.DAL.Entities.Film>();

            SIL.AARTO.DAL.Data.FilmQuery aartoFilmQuery = new SIL.AARTO.DAL.Data.FilmQuery();
            aartoFilmQuery.Append(SIL.AARTO.DAL.Entities.FilmColumn.FilmLoadStatus, ((int)FilmLoadStatusList.FilmCreatedViaImporter).ToString());
            aartoFilmQuery.Append(SIL.AARTO.DAL.Entities.FilmColumn.AutIntNo, authIntNo.ToString());
            try
            {
                return DataService.AartoFilmService.Find(aartoFilmQuery as SIL.AARTO.DAL.Data.IFilterParameterCollection).ToList();

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        private string SearchFrameFromCD(string filmPath, string filmNo, string frameNo)
        {
            string framePath = String.Empty;
            if (Directory.Exists(filmPath))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(filmPath);
                framePath = SerachFrameFloderFromCD(frameNo, dirInfo);
            }

            return framePath;
        }

        private string SerachFrameFloderFromCD(string frameNo, DirectoryInfo dirInfo)
        {
            try
            {
                if (dirInfo.FullName.ToLower().Contains(@"\" + frameNo.ToLower()))
                {
                    return dirInfo.FullName;
                }
                foreach (DirectoryInfo dir in dirInfo.GetDirectories())
                {
                    if (dir.FullName.ToLower().Contains(@"\" + frameNo.ToLower()))
                    {
                        return dir.FullName;
                    }
                    else
                    {
                        if (dir.GetDirectories().Length > 0)
                        {
                            return SerachFrameFloderFromCD(frameNo, dir);
                        }
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GenerateFrameImagePath(SIL.AARTO.DAL.Entities.Film film, SIL.AARTO.DAL.Entities.Frame frame, string authCode)
        {
            if (film.FirstOffenceDate.HasValue)
            {
                //Jerry 2013-05-16 changed for JMPD secondary offence frame
                //string imageRemoteFolder = string.Format(@"{0}\{1}\{2}\{3}\{4}\{5}",
                //                authCode.Trim(),
                //                film.FirstOffenceDate.Value.Year.ToString("0000"),
                //                film.FirstOffenceDate.Value.Month.ToString("00"),
                //                film.FirstOffenceDate.Value.Day.ToString("00"),
                //                film.FilmNo,
                //                frame.FrameNo);

                string imageRemoteFolder = string.Empty;
                if (frame.ParentFrameIntNo.HasValue)
                {
                    SIL.AARTO.DAL.Entities.Frame parentFrame = new SIL.AARTO.DAL.Services.FrameService().GetByFrameIntNo(frame.ParentFrameIntNo.Value);
                    if (parentFrame != null)
                    {
                        imageRemoteFolder = string.Format(@"{0}\{1}\{2}\{3}\{4}\{5}",
                                    authCode.Trim(),
                                    film.FirstOffenceDate.Value.Year.ToString("0000"),
                                    film.FirstOffenceDate.Value.Month.ToString("00"),
                                    film.FirstOffenceDate.Value.Day.ToString("00"),
                                    film.FilmNo,
                                    parentFrame.FrameNo);
                    }
                }
                else
                {
                    imageRemoteFolder = string.Format(@"{0}\{1}\{2}\{3}\{4}\{5}",
                                    authCode.Trim(),
                                    film.FirstOffenceDate.Value.Year.ToString("0000"),
                                    film.FirstOffenceDate.Value.Month.ToString("00"),
                                    film.FirstOffenceDate.Value.Day.ToString("00"),
                                    film.FilmNo,
                                    frame.FrameNo);
                }

                return imageRemoteFolder;
            }
            else
            {
                return string.Empty;
            }
        }

        //private List<SIL.IMX.DAL.Entities.Film> GetFilmsToExport()
        //{
        //    int totalCount = 0;
        //    int pageSize = 500;
        //    //FieldPackQuery query = new FieldPackQuery();
        //    //query.Append(FieldPackColumn.FpsIntNo, ((int)FieldPackStatusList.ExportedToDVD).ToString());
        //    //List<FieldPack> fieldPacks = imxFieldPackService.Find(query as SIL.IMX.DAL.Data.IFilterParameterCollection).ToList();
        //    string sql = String.Format("Inner Join FieldPack On FieldPack.FilmNo =Film.FilmNo Where FieldPack.FPSIntNo={0}", (int)FieldPackStatusList.ExportedToDVD);

        //    List<SIL.IMX.DAL.Entities.Film> imxFilms = DataService.ImxFilmService.Find(sql, 0, pageSize, out totalCount).ToList();

        //    return imxFilms;

        //}

        //private List<SIL.IMX.DAL.Entities.Frame> GetFramesInIMXByFilmIntNo(int filmIntNo)
        //{
        //    List<SIL.IMX.DAL.Entities.Frame> frames = DataService.ImxFrameService.GetByFilmIntNo(filmIntNo).ToList();

        //    return frames;
        //}

        //private List<SIL.IMX.DAL.Entities.ScanImage> GetScanImageInIMXByFrameIntNo(int frameIntNo)
        //{
        //    return DataService.ImxScanImageService.GetByFrameIntNo(frameIntNo).ToList();
        //}


        //private bool UpdateFilmToAarto(SIL.IMX.DAL.Entities.Film imxFilm, SIL.AARTO.DAL.Entities.Film aartoFilm)
        //{

        //    try
        //    {
        //        SIL.AARTO.DAL.Entities.Authority authority = CheckAuthorityInAarto(imxFilm.AuthIntNo);
        //        if (authority == null)
        //        {
        //            return false;
        //        }
        //        aartoFilm.AutIntNo = imxFilm.AuthIntNo;
        //        aartoFilm.FilmNo = imxFilm.FilmNo;
        //        aartoFilm.MultipleFrames = imxFilm.MultipleFrames;
        //        aartoFilm.NoOfFrames = imxFilm.NoOfFrames;
        //        aartoFilm.NoOfScans = imxFilm.NoOfScans;
        //        aartoFilm.CdLabel = imxFilm.CdLabel;
        //        aartoFilm.MultipleViolations = imxFilm.MultipleViolations;
        //        aartoFilm.FilmDescr = imxFilm.FilmDescr;
        //        aartoFilm.LastUser = imxFilm.LastUser;
        //        aartoFilm.LastRefNo = 0;
        //        aartoFilm.FilmType = "";
        //        aartoFilm.FilmLoadDateTime = DateTime.Now.Date;
        //        aartoFilm.FilmLoadType = "";
        //        aartoFilm.FilmLoadFileName = "";

        //        //aartoFilm.TmsLoadStatus = 0;
        //        aartoFilm.FilmLoadStatus = (int)FilmLoadStatusList.FilmLoadedSuccessfully;
        //        aartoFilm.CameraType = "";
        //        aartoFilm.FilmImageRemoved = true;

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        //private bool UpdateFrameToAarto(SIL.IMX.DAL.Entities.Frame imxFrame, SIL.AARTO.DAL.Entities.Frame aartoFrame, int filmIntNo)
        //{
        //    try
        //    {
        //        aartoFrame.FilmIntNo = filmIntNo;
        //        aartoFrame.FrameNo = imxFrame.FrameNo;
        //        aartoFrame.Sequence = imxFrame.Sequence;
        //        aartoFrame.RegNo = imxFrame.RegNo;
        //        aartoFrame.OffenceDate = imxFrame.OffenceDate.Value;
        //        aartoFrame.FirstSpeed = (byte)imxFrame.FirstSpeed;
        //        aartoFrame.SecondSpeed = (byte)imxFrame.SecondSpeed;
        //        aartoFrame.ElapsedTime = imxFrame.ElapsedTime;
        //        //aartoFrame.TravelDirection = imxFrame
        //        aartoFrame.ManualView = imxFrame.ManualView;
        //        aartoFrame.MultFrames = imxFrame.MultFrames;

        //        // aartoFrame. FrameVerifyDateTime= imxFrame.frmae
        //        // aartoFrame. FrameVerifyUser= imxFrame.framec
        //        aartoFrame.LastUser = "";

        //        aartoFrame.AllowContinue = imxFrame.AllowContinue;

        //        aartoFrame.TomsLocIntNo = imxFrame.LocIntNo;
        //        aartoFrame.TomsVmIntNo = imxFrame.VehMintNo;
        //        aartoFrame.TomsVtIntNo = imxFrame.VehTintNo;
        //        // aartoFrame.Toms_RdtIntNo= imxFrame.
        //        // aartoFrame. Toms_CrtIntNo= imxFrame
        //        aartoFrame.TomsRejIntNo = imxFrame.RejIntNo;
        //        aartoFrame.TomsRegNo = imxFrame.RegNo;

        //        aartoFrame.FrameStatus = (int)FrameStatusList.FrameLoadedViaImporterSuccessfully;


        //        aartoFrame.NatisProxyIndicator = "";
        //        aartoFrame.NoCommunicationFailed = 0;


        //        aartoFrame.AsdTimeDifference = imxFrame.AsdTimeDifference;
        //        aartoFrame.AsdSectionStartLane = imxFrame.AsdSectionStartLane;
        //        aartoFrame.AsdSectionStartLane = imxFrame.AsdSectionStartLane;
        //        aartoFrame.AsdSectionEndLane = imxFrame.AsdSectionEndLane;
        //        aartoFrame.AsdSectionDistance = imxFrame.AsdSectionDistance;
        //        aartoFrame.Asd1stCamUnitId = imxFrame.AsdCameraSerialNo1;
        //        aartoFrame.Asd2ndCamUnitId = imxFrame.AsdCameraSerialNo2;
        //        aartoFrame.FrameImageRemoved = true;


        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //private bool UpdateScanImageToAarto(SIL.IMX.DAL.Entities.ScanImage imxImage, SIL.AARTO.DAL.Entities.ScanImage aartoImage, int frameIntNo)
        //{
        //    try
        //    {
        //        aartoImage.FrameIntNo = frameIntNo;
        //        aartoImage.ScImType = "";
        //        aartoImage.JpegName = imxImage.JpegName;
        //        aartoImage.Xvalue = imxImage.Xvalue;
        //        aartoImage.Yvalue = imxImage.Yvalue;
        //        aartoImage.LastUser = "";
        //        aartoImage.ScImPrintVal = 0;

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        private SIL.AARTO.DAL.Entities.Authority GetAuthorityFromAarto(string authCode)
        {
            try
            {
                return new SIL.AARTO.DAL.Services.AuthorityService().GetByAutCode(authCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public bool CheckFilmInAarto(string filmNo)
        {
            try
            {
                return DataService.AartoFilmService.GetByFilmNo(filmNo) == null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CheckFrameInAarto(int filmIntNo, string frameNo)
        {
            try
            {
                return DataService.AartoFrameService.GetByFilmIntNoFrameNo(filmIntNo, frameNo) == null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public SIL.AARTO.DAL.Entities.Authority CheckAuthorityInAarto(int authIntNo)
        //{
        //    SIL.IMX.DAL.Entities.Authority imxAuthority = new SIL.IMX.DAL.Services.AuthorityService().GetByAuthIntNo(authIntNo);
        //    SIL.AARTO.DAL.Entities.Authority aartoAuthority = new SIL.AARTO.DAL.Services.AuthorityService().GetByAutNo(imxAuthority.AuthNo);
        //    if (aartoAuthority == null)
        //    {
        //        SIL.IMX.DAL.Entities.Metro imxMetro = new SIL.IMX.DAL.Services.MetroService().GetByMtrIntNo(imxAuthority.MtrIntNo);
        //        aartoAuthority.MtrIntNo = CheckAndCreateMetro(imxMetro).MtrIntNo;
        //        aartoAuthority = new DAL.Entities.Authority();
        //        aartoAuthority.AutCode = imxAuthority.AuthCode;
        //        aartoAuthority.AutName = imxAuthority.AuthName;
        //        aartoAuthority.AutNo = imxAuthority.AuthNo;
        //        aartoAuthority.BranchCode = "";
        //        aartoAuthority.AutSendNatisEmailOld = "";
        //        aartoAuthority.AutNatisFolderOld = "";
        //        aartoAuthority.Aut3PcodingSystem = "";
        //        aartoAuthority.AutPhysAddr1 = "";
        //        aartoAuthority.AutPhysAddr2 = "";
        //        aartoAuthority.AutPhysAddr3 = "";
        //        aartoAuthority.AutPhysAddr4 = "";
        //        aartoAuthority.EasyPayNumberLength = 0;
        //        aartoAuthority.EnatisAuthorityNumber = 0;
        //        return null;

        //    }
        //    else
        //    {
        //        return aartoAuthority;
        //    }

        //}

        //public SIL.AARTO.DAL.Entities.TrafficOfficer CheckTrafficOfficerInAarto(string toNo)
        //{
        //    List<SIL.AARTO.DAL.Entities.TrafficOfficer> aartoTrafficerList = DataService.AartoTrafficOfficerService.GetByToNo(toNo).ToList();


        //    if (aartoTrafficerList == null || aartoTrafficerList.Count == 0)
        //    {
        //        List<SIL.IMX.DAL.Entities.TrafficOfficer> imxTrafficOfficer = DataService.ImxTrafficOfficerService.GetByToNo(toNo).ToList();
        //        foreach (SIL.IMX.DAL.Entities.TrafficOfficer to in imxTrafficOfficer)
        //        {
        //            SIL.IMX.DAL.Entities.Metro imxMetro = new IMX.DAL.Services.MetroService().GetByMtrIntNo(to.MtrIntNo);
        //            if (imxMetro != null)
        //            {
        //                List<SIL.AARTO.DAL.Entities.Metro> aartoMetrolist = new SIL.AARTO.DAL.Services.MetroService().GetByMtrNo(imxMetro.MtrNo).ToList();

        //                SIL.AARTO.DAL.Entities.TrafficOfficer aartoTrafficOfficer = new DAL.Entities.TrafficOfficer();
        //                aartoTrafficOfficer.MtrIntNo = CheckAndCreateMetro(imxMetro).MtrIntNo;

        //                aartoTrafficOfficer.ToGroup = to.ToGroup;
        //                aartoTrafficOfficer.ToInit = to.ToInit;
        //                aartoTrafficOfficer.ToNo = to.ToNo;
        //                aartoTrafficOfficer.TosName = to.TosName;

        //            }
        //        }

        //        return null;
        //    }
        //    else
        //    {
        //        return aartoTrafficerList[0];
        //    }
        //    //SIL.IMX.DAL.Entities.TrafficOfficer imxTraffOfficer =DataService.ImxTrafficOfficerService.GetByToNo(toNo) 
        //}

        //private SIL.AARTO.DAL.Entities.Rejection CheckAndCreateRejection(SIL.IMX.DAL.Entities.Rejection xmlRejection)
        //{
        //    try
        //    {
        //        SIL.AARTO.DAL.Entities.Rejection aartoRejection = new SIL.AARTO.DAL.Services.RejectionService().GetByRejReason(xmlRejection.RejReason);
        //        if (aartoRejection == null)
        //        {
        //            aartoRejection = new DAL.Entities.Rejection();

        //            aartoRejection.RejReason = xmlRejection.RejReason;
        //            aartoRejection.LastUser = "SIL.Aarto.Imx.ImageLoader";

        //            aartoRejection = new SIL.AARTO.DAL.Services.RejectionService().Save(aartoRejection);

        //        }

        //        return aartoRejection;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public SIL.AARTO.DAL.Entities.Metro CheckAndCreateMetro(SIL.IMX.DAL.Entities.Metro imxMetro)
        //{
        //    try
        //    {
        //        List<SIL.AARTO.DAL.Entities.Metro> aartoMetroList = new SIL.AARTO.DAL.Services.MetroService().GetByMtrCode(imxMetro.MtrCode).ToList();
        //        if (aartoMetroList == null || aartoMetroList.Count == 0)
        //        {
        //            SIL.AARTO.DAL.Entities.Metro aartoMetro = new DAL.Entities.Metro();
        //            aartoMetro = new DAL.Entities.Metro();
        //            aartoMetro.MtrCode = imxMetro.MtrCode;
        //            aartoMetro.MtrNo = imxMetro.MtrNo;
        //            aartoMetro.MtrName = imxMetro.MtrNo;
        //            aartoMetro.LastUser = "SIL.Aarto.Imx.ImageLoader";
        //            SIL.IMX.DAL.Entities.Rejection imxRejection = new SIL.IMX.DAL.Services.RejectionService().GetByRejIntNo(imxMetro.RegIntNo);
        //            if (imxRejection != null)
        //            {
        //                SIL.AARTO.DAL.Entities.Rejection rejection = CheckAndCreateRejection(imxRejection);
        //                if (rejection != null)
        //                {
        //                    aartoMetro.RegIntNo = rejection.RejIntNo;

        //                    aartoMetro = new SIL.AARTO.DAL.Services.MetroService().Save(aartoMetro);

        //                    return aartoMetro;
        //                }
        //            }

        //        }
        //        else
        //        {
        //            return aartoMetroList[0];
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return null;
        //}

        public AuthorityRuleInfo GetAuthorityRule(int authIntNo, string ruleCode)
        {
            return new AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(authIntNo, ruleCode);
        }

        public AuthorityRuleInfo GetAuthorityRule(string authCode, string ruleCode)
        {
            SIL.AARTO.DAL.Entities.Authority authority = DataService.AartoAuthorityService.GetByAutCode(authCode);
            if (authority != null)
            {
                return new AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(authority.AutIntNo, ruleCode);
            }
            else
            {
                return null;
            }
        }

    }
}
