﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Configuration.Provider;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Xml.Linq;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;

namespace SIL.AARTO.BLL.Provider
{
    public class AARTORoleProvider : RoleProvider
    {
        private UserService userService = new UserService();
        private AartoUserRoleService userRoleService = new AartoUserRoleService();
        private AartoUserRoleConjoinService userRoleConService = new AartoUserRoleConjoinService();

        public override string ApplicationName
        {
            get;
            set;

        }

        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            if (name == null || name.Length == 0)
            {
                name = "AARTORoleProvider";
            }

            base.Initialize(name, config);

            if (config["applicationName"] == null || config["applicationName"].Trim() == string.Empty)
            {
                ApplicationName = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
            }
            else
            {
                ApplicationName = config["applicationName"];
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            #region validate role names
            foreach (string roleName in roleNames)
            {
                if (!RoleExists(roleName))
                {
                    throw new ProviderException(string.Format("Role name '{0}' not found", roleName));
                }
            }

            #endregion

            #region check whether already existing related user - role pairs

            foreach (string userLoginName in usernames)
            {
                // TO DO LIST: update according new requirement
                
                foreach (string roleName in roleNames)
                {
                    if (IsUserInRole(userLoginName, roleName))
                    {
                        throw new ProviderException(
                                string.Format("User login name '{0}' is already in the role '{1}'.", userLoginName, roleName));
                    }
                }
            }

            #endregion

            //
            // Associate users in userNames list with roles in roleNames list
              
            List<int> lstUserID = new List<int>();
            List<int> lstRoleID = new List<int>();

            foreach (string userLoginName in usernames)
            {
                lstUserID.Add(userService.GetByUserLoginName(userLoginName).UserIntNo);
            }

            foreach (string roleName in roleNames)
            {
                lstRoleID.Add(userRoleService.GetByAaUserRoleName(roleName).AaUserRoleId);
            }

            try
            {
                using (ConnectionScope.CreateTransaction())
                {
                    for (int i = 0; i < lstRoleID.Count; i++)
                    {
                        for (int j = 0; j < lstUserID.Count; j++)
                        {
                            AartoUserRoleConjoin userConjoin = new AartoUserRoleConjoin();
                            userConjoin.AaUserId = lstUserID[j];
                            userConjoin.AaUserRoleId = lstRoleID[i];
                            //2013-07-16 add by Henry for LastUser
                            userConjoin.LastUser = HttpContext.Current.User.Identity.Name;
                            //trcEntity.LastUser = string.Empty; // TO DO LIST: Update according refined session control
                            userRoleConService.Insert(userConjoin);
                        }
                    }

                    ConnectionScope.Complete();
                }
            }
            catch
            {
                throw;
            }
        }

        public override void CreateRole(string roleName)
        {
            if (RoleExists(roleName))
            {
                throw new ProviderException(string.Format("Role '{0}' already exists.", roleName));
            }

            try
            {
                AartoUserRole userRole = new AartoUserRole();
                userRole.AaUserRoleName = roleName;
                //userRole.LastUser = string.Empty; // TO DO LIST: Update the refined session control
                // 2013-07-16 add by Henry for LastUser
                userRole.LastUser = HttpContext.Current.User.Identity.Name;
                userRoleService.Insert(userRole);
            }
            catch
            {
                throw;
            }
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            if (!RoleExists(roleName))
            {
                throw new ProviderException(string.Format("Role '{0}' does not exist.", roleName));
            }

            if (throwOnPopulatedRole && GetUsersInRole(roleName).Length > 0)
            {
                throw new ProviderException(string.Format("Can not delete a populated role '{0}'.", roleName));
            }

            try
            {
                using (ConnectionScope.CreateTransaction())
                {
                    userRoleService.Delete(userRoleService.GetByAaUserRoleName(roleName));

                    ConnectionScope.Complete();

                    return true;
                }
            }
            catch
            {
                throw;
            }
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            StringBuilder sbUserNameResult = new StringBuilder();   // generate trader email result

            try
            {
                AartoUserRole usRole = this.userRoleService.GetByAaUserRoleName(roleName);
                if (usRole != null)
                {
                    using (IDataReader reader = this.userService.GetUserByRoleIDAndUserLoginNameMatch(usRole.AaUserRoleId, usernameToMatch))
                    {

                        while (reader.Read())
                        {
                            // The returned table includes columns: TraderEmail, TraderID
                            // 0 indicates to fetch the TraderEmail column's value
                            sbUserNameResult.Append(reader.GetString(0) + ",");
                        }
                    }
                }
            }
            catch
            {
                throw;
            }

            return ConvertStringResult(sbUserNameResult.ToString());
        }

        public override string[] GetAllRoles()
        {
            IEnumerable<AartoUserRole> lstUserRole = userRoleService.GetAll().OrderBy(r => r.AaUserRoleName);

            StringBuilder sbUserRole = new StringBuilder();

            foreach (AartoUserRole userRole in lstUserRole)
            {
                sbUserRole.Append(userRole.AaUserRoleName + ",");
            }

            return ConvertStringResult(sbUserRole.ToString());
        }

        public override string[] GetRolesForUser(string username)
        {
            StringBuilder sbResult = new StringBuilder();


            User user = userService.DeepLoadByUserLoginName(username, true,
                                DeepLoadType.IncludeChildren, typeof(TList<AartoUserRoleConjoin>));
            TList<AartoUserRoleConjoin> lstUserRoleCon = user.AartoUserRoleConjoinCollection;

            foreach (AartoUserRoleConjoin userRoleCon in lstUserRoleCon)
            {
                sbResult.Append(userRoleService.GetByAaUserRoleId(userRoleCon.AaUserRoleId).AaUserRoleName + ",");
            }

            return ConvertStringResult(sbResult.ToString());
        }

        public override string[] GetUsersInRole(string roleName)
        {
            StringBuilder sbResult = new StringBuilder();

            
            AartoUserRole userRole = userRoleService.DeepLoadByAaUserRoleName(roleName, true,
                                DeepLoadType.IncludeChildren, typeof(TList<AartoUserRoleConjoin>));
            TList<AartoUserRoleConjoin> lstTraderRoleCon = userRole.AartoUserRoleConjoinCollection;

            foreach (AartoUserRoleConjoin trdRoleCon in lstTraderRoleCon)
            {
                sbResult.Append(userService.GetByUserIntNo(trdRoleCon.AaUserId).UserLoginName + ",");
            }

            return ConvertStringResult(sbResult.ToString());
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            AartoUserRoleConjoin userRoleCon = userRoleConService.GetByAaUserIdAaUserRoleId(
                                            userService.GetByUserLoginName(username).UserIntNo,
                                            userRoleService.GetByAaUserRoleName(roleName).AaUserRoleId);

            return (userRoleCon != null ? true : false); 
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            // Check whether the given roles already exist in UserRole table
            foreach (string role in roleNames)
            {
                if (!RoleExists(role))
                {
                    throw new ProviderException(string.Format("Role '{0}' not found.", role));
                }
            }

            // Check whether the given user already exist in TraderRoleConjoin table
            foreach (string user in usernames)
            {
                foreach (string role in roleNames)
                {
                    if (!IsUserInRole(user, role))
                    {
                        throw new ProviderException(string.Format("User '{0}' is not associated with the role '{1}'.", user, role));
                    }
                }
            }

            try
            {
                using (ConnectionScope.CreateTransaction())
                {

                    foreach (string user in usernames)
                    {
                        foreach (string role in roleNames)
                        {
                            userRoleConService.Delete(
                                                userRoleConService.GetByAaUserIdAaUserRoleId(
                                                     userService.GetByUserLoginName(user).UserIntNo,
                                                     userRoleService.GetByAaUserRoleName(role).AaUserRoleId)
                                                 );
                        }
                    }

                    ConnectionScope.Complete();
                }
            }
            catch
            {
                throw;
            }
        }

        public override bool RoleExists(string roleName)
        {
            AartoUserRole usrRole =this.userRoleService.GetByAaUserRoleName(roleName);

            return (usrRole != null ? true : false);
        }


        private string[] ConvertStringResult(string strResult)
        {
            if (string.IsNullOrEmpty(strResult))
            {
                return new string[0];
            }
            else
            {
                string resultTmp = strResult.TrimEnd(new char[] { ',', ' ' });
                return resultTmp.Split(new char[] { ',' });
            }
        }
    }
}
