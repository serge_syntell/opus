﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Xml.Linq;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.BLL.Provider
{
    public class AARTOMembershipProvider:MembershipProvider
    {

        #region Fileds

        private bool enablePasswordRetrieval;
        private bool enablePasswordReset;
        private bool requiresQuestionAndAnswer;
        private string applicationName = "SIL.AARTO";
        private int applicationId = 0;
        private bool requiresUniqueEmail;
        private string connectionStringName;
        private string hashAlgorithmType;
        private int maxInvalidPasswordAttempts;
        private int passwordAttemptWindow;
        private int minRequiredPasswordLength;
        private int minRequiredNonalphanumericCharacters;
        private string passwordStrengthRegularExpression;
        private MembershipPasswordFormat passwordFormat;

        private string connectionString;
        private MachineKeySection machineKey;

        
        private UserService userService = new UserService();

        /// <summary>
        /// The name of the application using the custom membership provider.
        /// </summary>
        public override string ApplicationName
        {
            get
            {
                return applicationName;
            }
            set
            {
                applicationName = value; ;
            }
        }

        /// <summary>
        /// Gets the number of invalid password or password-answer attempts allowed before the membership user is locked out.
        /// </summary>
        public override int MaxInvalidPasswordAttempts
        {
            get
            {
                return maxInvalidPasswordAttempts;
            }
        }

        /// <summary>
        /// Gets the minimum number of special characters that must be present in a valid password.
        /// </summary>
        public override int MinRequiredNonAlphanumericCharacters
        {
            get
            {
                return minRequiredNonalphanumericCharacters;
            }
        }

        /// <summary>
        /// Gets the minimum length required for a password.
        /// </summary>
        public override int MinRequiredPasswordLength
        {
            get
            {
                return minRequiredPasswordLength;
            }
        }

        /// <summary>
        /// Gets the number of minutes in which a maximum number of invalid password or password-answer attempts are allowed
        /// before the membership user is locked out.
        /// </summary>
        public override int PasswordAttemptWindow
        {
            get
            {
                return passwordAttemptWindow;
            }
        }

        /// <summary>
        /// Gets a value indicating the format for storing passwords in the membership data store.
        /// </summary>
        public override MembershipPasswordFormat PasswordFormat
        {
            get
            {
                return passwordFormat;
            }
        }

        /// <summary>
        /// Gets the regular expression used to evaluate a password.
        /// </summary>
        public override string PasswordStrengthRegularExpression
        {
            get
            {
                return passwordStrengthRegularExpression;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the membership provider is configured to require the user to 
        /// answer a password question for password reset and retrieval.
        /// </summary>
        public override bool RequiresQuestionAndAnswer
        {
            get
            {
                return requiresQuestionAndAnswer;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the membership provider is configured to require a 
        /// unique e-mail address for each user name.
        /// </summary>
        public override bool RequiresUniqueEmail
        {
            get
            {
                return requiresUniqueEmail;
            }
        }

        /// <summary>
        /// Indicates whether the membership provider is configured to allow users to reset their passwords.
        /// </summary>
        public override bool EnablePasswordReset
        {
            get
            {
                return enablePasswordReset;
            }
        }

        /// <summary>
        /// The name of the application using the custom membership provider.
        /// </summary>
        public override bool EnablePasswordRetrieval
        {
            get
            {
                return enablePasswordRetrieval;
            }
        }
        #endregion

        #region Functions

        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
                throw new ArgumentNullException("config");

            if (name == null || name.Length == 0)
                name = "AARTOMembershipProvider";

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "AARTO Membership provider");
            }

            // Initialize the abstract base class.
            base.Initialize(name, config);

            applicationName = GetConfigValue(config["applicationName"],
                                            System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            maxInvalidPasswordAttempts = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
            passwordAttemptWindow = Convert.ToInt32(GetConfigValue(config["passwordAttemptWindow"], "10"));
            minRequiredNonalphanumericCharacters = Convert.ToInt32(GetConfigValue(config["minRequiredNonAlphanumericCharacters"], "1"));
            minRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "7"));
            passwordStrengthRegularExpression = Convert.ToString(GetConfigValue(config["passwordStrengthRegularExpression"], ""));
            enablePasswordReset = Convert.ToBoolean(GetConfigValue(config["enablePasswordReset"], "true"));
            enablePasswordRetrieval = Convert.ToBoolean(GetConfigValue(config["enablePasswordRetrieval"], "true"));
            requiresQuestionAndAnswer = Convert.ToBoolean(GetConfigValue(config["requiresQuestionAndAnswer"], "false"));
            requiresUniqueEmail = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "true"));
            //w = Convert.ToBoolean(GetConfigValue(config["writeExceptionsToEventLog"], "true"));

            string temp_format = config["passwordFormat"];
            if (temp_format == null)
            {
                temp_format = "Encrypted";
            }

            switch (temp_format)
            {
                case "Hashed":
                    passwordFormat = MembershipPasswordFormat.Hashed;
                    break;
                case "Encrypted":
                    passwordFormat = MembershipPasswordFormat.Encrypted;
                    break;
                case "Clear":
                    passwordFormat = MembershipPasswordFormat.Clear;
                    break;
                default:
                    throw new ProviderException("Password format not supported.");
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            if (!ValidateUser(username, oldPassword))
            {
                return false;
            }

            #region
            ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, newPassword, true);

            OnValidatingPassword(args);

            if (args.Cancel)
            {
                if (args.FailureInformation != null)
                {
                    throw args.FailureInformation;
                }
                else
                {
                    throw new MembershipPasswordException("Change password canceled due to new password validation failure.");
                }
            }
            #endregion

            try
            {

                User user = userService.GetByUserLoginName(username);
                user.UserPassword = newPassword;
                // 2013-07-16 add by Henry for LastUser
                user.LastUser = HttpContext.Current.User.Identity.Name;

                using (ConnectionScope.CreateTransaction())
                {
                    if (userService.Update(user))
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return false;
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException("The method or operation is not implemented.");
        }


        /// <summary>
        /// Crerate a new user
        /// </summary>
        /// <param name="username">user name</param>
        /// <param name="password">user password</param>
        /// <param name="email">user email</param>
        /// <param name="passwordQuestion">used for user first name</param>
        /// <param name="passwordAnswer">userd for user last name</param>
        /// <param name="isApproved"></param>
        /// <param name="providerUserKey"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, password, true);

            OnValidatingPassword(args);

            if (args.Cancel)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }

            if (RequiresUniqueEmail && GetUserNameByEmail(email) != "")
            {
                status = MembershipCreateStatus.DuplicateEmail;
                return null;
            }

            MembershipUser u = GetUser(username, false);

            if (u == null)
            {
                DateTime createDate = DateTime.Now;

                if (providerUserKey == null)
                {
                    providerUserKey = Guid.NewGuid();
                }
                else
                {
                    if (!(providerUserKey is Guid))
                    {
                        status = MembershipCreateStatus.InvalidProviderUserKey;
                        return null;
                    }
                }

                User user = new User();
                user.UserFname = username;
                user.UserEmail = email;
                user.UserPassword = Encrypt.HashPassword(password); //EncodePassword(password);
                user.UserLoginName = username;
                user.UserInit = username.Substring(0, 1).ToUpper();
                user.UserFname = passwordQuestion;
                user.UserSname = passwordAnswer;
                user.UserAccessLevel = 0;
                //2013-07-16 add by Henry for add LastUser
                user.LastUser = HttpContext.Current.User.Identity.Name;

                try
                {
                    using (ConnectionScope.CreateTransaction())
                    {
                        if (DataRepository.UserProvider.Insert(user))
                        //if (this.traderService.Insert(trader))
                        {
                            status = MembershipCreateStatus.Success;
                        }
                        else
                        {
                            status = MembershipCreateStatus.UserRejected;
                        }
                    }
                }
                catch
                {
                    status = MembershipCreateStatus.ProviderError;
                    //throw ex;
                }

                return GetUser(username, false);
            }
            else
            {
                status = MembershipCreateStatus.DuplicateUserName;
            }

            return null;
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection users = new MembershipUserCollection();
            try {
                IList<User> UserList = this.userService.GetPaged(String.Format("Where UserEmail like '%{0}%'",emailToMatch),"UserIntNo",pageIndex, pageSize, out totalRecords) as IList<User>;
                if (totalRecords <= 0)
                {
                    return users;
                }

                foreach (User user in UserList)
                {
                    MembershipUser membershipUser = this.GetUserFromUserTable(user);

                    users.Add(membershipUser);
                }
                return users;
            }
            catch(Exception ex) {
                throw ex;
            }
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection users = new MembershipUserCollection();
            try
            {
                IList<User> UserList = this.userService.GetPaged(String.Format("Where UserLoginName like '%{0}%'", usernameToMatch), "UserIntNo", pageIndex, pageSize, out totalRecords) as IList<User>;
                if (totalRecords <= 0)
                {
                    return users;
                }

                foreach (User user in UserList)
                {
                    MembershipUser membershipUser = this.GetUserFromUserTable(user);

                    users.Add(membershipUser);
                }
                return users;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection users = new MembershipUserCollection();

            try
            {
                IList<User> UserList = this.userService.GetAll(pageIndex, pageSize, out totalRecords) as IList<User>;
                if (totalRecords <= 0)
                {
                    return users;
                }

                foreach (User user in UserList)
                {
                    MembershipUser membershipUser = this.GetUserFromUserTable(user);

                    users.Add(membershipUser);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return users;
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            //throw new NotImplementedException("The method or operation is not implemented.");
            MembershipUser membershipUser = null;

            try
            {
                User user = userService.GetByUserLoginName(username);

                if (user != null)
                {
                    membershipUser = this.GetUserFromUserTable(user);

                    if (userIsOnline)
                    {
                        //Update LastActivityDate
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return membershipUser;

        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            MembershipUser membershipUser = null;

            try
            {
                User user = this.userService.GetByUserIntNo((int)providerUserKey);

                if (user != null)
                {
                    membershipUser = this.GetUserFromUserTable(user);

                    if (userIsOnline)
                    {
                        //Update LastActivityDate
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return membershipUser;
        }

        public override string GetUserNameByEmail(string email)
        {
            //User user = this.userService.GetByUserEmail(email);
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException("The method or operation is not implemented.");
        }

        public override void UpdateUser(MembershipUser user)
        {
            User userEntity = userService.GetByUserLoginName(user.UserName);

            if (userEntity != null)
            {
                userEntity.UserEmail = user.Email;
                userEntity.UserLoginName = user.UserName;
                //2013-07-16 add by Henry for add LastUser
                userEntity.LastUser = HttpContext.Current.User.Identity.Name;
                userService.Save(userEntity);
            }
        }

        public override bool ValidateUser(string username, string password)
        {
            bool validated = false;

            User user = userService.GetByUserLoginName(username);
            if (user == null)
            {
                return validated;
            }

            string storedPwd = user.UserPassword;

            if (passwordFormat == MembershipPasswordFormat.Hashed)
            {
                if (storedPwd == Encrypt.HashPassword(password))
                    validated = true;
            }
            else if (passwordFormat == MembershipPasswordFormat.Encrypted)
            {
                if (storedPwd == Encrypt.HashPassword(password))
                    validated = true;
            }
            else
            {
                if (storedPwd == password)
                    validated = true;
            }
            return validated;
        }

        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
                return defaultValue;

            return configValue;
        }

 
        #endregion

        #region Private Functions

        // Parameter validator
        private static bool ValidateParameter(ref string param, bool checkForNull,
            bool checkIfEmpty, bool checkForCommas, int maxSize)
        {
            if (param == null)
            {
                if (checkForNull)
                {
                    return false;
                }
                return true;
            }

            param = param.Trim();
            if ((checkIfEmpty && param.Length < 1) || (maxSize > 0 && param.Length > maxSize)
                || (checkForCommas && param.IndexOf(",") != -1))
                return false;

            return true;
        }

        private string EncodePassword(string password)
        {
            string encodedPassword = password;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    break;
                case MembershipPasswordFormat.Encrypted:
                    encodedPassword =
                      Convert.ToBase64String(EncryptPassword(Encoding.Unicode.GetBytes(password)));
                    break;
                case MembershipPasswordFormat.Hashed:
                    HMACSHA1 hash = new HMACSHA1();
                    hash.Key = HexToByte(machineKey.ValidationKey);
                    encodedPassword =
                      Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password)));
                    break;
                default:
                    throw new ProviderException("Unsupported password format.");
            }

            return encodedPassword;
        }


        //
        // UnEncodePassword
        //   Decrypts or leaves the password clear based on the PasswordFormat.
        //

        private string UnEncodePassword(string encodedPassword)
        {
            string password = encodedPassword;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    break;
                case MembershipPasswordFormat.Encrypted:
                    password =
                      Encoding.Unicode.GetString(DecryptPassword(Convert.FromBase64String(password)));
                    break;
                case MembershipPasswordFormat.Hashed:
                    throw new ProviderException("Cannot unencode a hashed password.");
                default:
                    throw new ProviderException("Unsupported password format.");
            }

            return password;
        }

        //
        // HexToByte
        //   Converts a hexadecimal string to a byte array. Used to convert encryption
        // key values from the configuration.
        //

        private byte[] HexToByte(string hexString)
        {
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }


        private MembershipUser GetUserFromUserTable(User user)
        {
            object providerUserKey = user.UserIntNo;
            string userName = user.UserLoginName;
            string email = user.UserEmail;

            string passwordQuestion = string.Empty;

            string comment = "";

            bool isApproved = true;
            bool isLockedOut = false;
            DateTime creationDate = new DateTime();

            DateTime lastLoginDate = new DateTime();

            DateTime lastActivityDate = new DateTime();
            DateTime lastPasswordChangedDate = new DateTime();

            DateTime lastLockedOutDate = new DateTime();

            MembershipUser membershipUser = new MembershipUser(this.Name,
                                          userName,
                                          providerUserKey,
                                          email,
                                          passwordQuestion,
                                          comment,
                                          isApproved,
                                          isLockedOut,
                                          creationDate,
                                          lastLoginDate,
                                          lastActivityDate,
                                          lastPasswordChangedDate,
                                          lastLockedOutDate);

            return membershipUser;
        }
        #endregion

    }
}
