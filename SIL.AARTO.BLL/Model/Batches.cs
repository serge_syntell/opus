﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using System.Reflection;

namespace SIL.AARTO.BLL.Model
{   
    public class PrintFactoryBatches
    {
        private static PrintFactoryBatches objInstance = null;

        public List<PrintFactoryBatch> Batches;

        private string _filePath = string.Empty;
        public string FilePath
        {
            set { this._filePath = value; }
        }

        private PrintFactoryBatches()
        {
            this.Batches = new List<PrintFactoryBatch>();
        }

        public static PrintFactoryBatches GetInstance(string path)
        {
            if (objInstance == null)
            {
                objInstance = new PrintFactoryBatches();                
            }
            if (File.Exists(path))
            {
                objInstance = ObjectSerializer.ReadXmlFileToObject<PrintFactoryBatches>(path);                
            }
            objInstance.FilePath = path;
            return objInstance;
        }

        public bool AppendPebsFile(string path)
        {
            bool result = false;
            if (File.Exists(path))
            {
                try 
	            {	                
                    
            		PebsBatch pebs = ObjectSerializer.ReadXmlFileToObject<PebsBatch>(path);

                    PrintFactoryBatch batch = new PrintFactoryBatch();//(PrintFactoryBatch)pebs;
                    batch.Path          = path;
                    batch.AutCode       = pebs.AutCode;
                    batch.Status        = AartoDocumentStatusList.Issued.ToString();
                    batch.BatchCode     = pebs.BatchCode;
                    batch.DocumentType  = pebs.DocumentType;
                    batch.DocumentCount = pebs.DocumentCount;
                    batch.DateIssued    = pebs.DateIssued;
                    batch.BatchId       = pebs.BatchId;
                    //PropertyInfo[] infoes = pebs.GetType().GetProperties();
                    //foreach (PropertyInfo pro in infoes)
                    //{                        
                    //    batch.GetType().GetProperty(pro.Name).SetValue(batch, 
                    //}
                    //batch.AutCode       = pebs.AutCode;
                    //batch.Status        = AartoDocumentStatusList.Issued;
                    //batch.BatchName     = pebs.BatchCode;
                    //batch.DocumentType  = pebs.DocumentType;
                    //batch.DocumentCount = pebs.DocumentCount;                    

                    this.Batches.Add(batch);
                    result = true;
	            }
	            catch (Exception ex)
	            {            		
		            throw ex;
	            }               
            }            
            return result;
        }

        public bool SaveChange()
        {
            bool result = false;
            try
            {
                ObjectSerializer.SaveObjectToXmlFile(this, this._filePath);
            }
            catch (Exception ex)
            {                
                throw;
            }
            return result;
        }
    }

    public class PrintFactoryBatch : PebsBatch
    {   
        //[XmlAttribute]
        //public string AutCode;
        //[XmlAttribute]
        //public AartoDocumentStatusList Status;
        //[XmlAttribute]
        //public string BatchName;
        //[XmlAttribute]
        //public string DocumentType;
        //[XmlAttribute]
        //public int DocumentCount;

        //public PebsBatch Pebs
        //{
        //    get;
        //    set;
        //}
        public string Path
        {
            get;
            set;
        }

        public string PrintedTime
        {
            get 
            {
                if (base.Print != null)
                {
                    return base.Print.PrintedTime; 
                }
                else
                {
                    return string.Empty;
                }
            }
            set {  }
        }
    }
}
