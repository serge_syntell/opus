﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Model
{
    public class CourtRuleInfo
    {
        public int WFRFCID;
        public string WFRFCName;
        public string WFRFCDescr;
        public string WFRFCComment;
        public int CrtIntNo;
        public int CRNumeric;
        public string CRString;


        public CourtRuleInfo GetCourtRuleInfoByWFRFCNameCrtIntNo(int CrtIntNo, string code)
        {
            IDataReader rd = new WorkFlowRuleForCourtService().GetByWFRFCNameCrtIntNo("CR_" + code, CrtIntNo);
            CourtRuleInfo courtRule = null;
            if (rd.Read())
            {
                courtRule = new CourtRuleInfo();
                courtRule.WFRFCName = rd["WFRFCName"].ToString();
                courtRule.WFRFCComment = rd["WFRFCComment"].ToString();
                courtRule.WFRFCDescr = rd["WFRFCDescr"].ToString();
                courtRule.CRNumeric = int.Parse(rd["CRNumeric"].ToString());
                courtRule.CRString = rd["CRString"].ToString();
            }
            rd.Close();
            return courtRule;
        }
    }
}
