﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Globalization;

namespace SIL.AARTO.BLL.Model
{
    [XmlRoot("batch")]
    public class PebsBatch
    {
        [XmlAttribute]
        public int BatchId { get; set; }
        [XmlAttribute]
        public string BatchCode { get; set; }
        [XmlAttribute]
        public int DocumentCount { get; set; }
        [XmlAttribute]
        public string DocumentType { get; set; }
        [XmlAttribute]
        public bool NeedToSynchronize { get; set; }
        [XmlAttribute]
        public string Status { get; set; }
        [XmlAttribute]
        public string AutCode { get; set; }

        private string _dateIssued;
        public DateTime DateIssuedValue
        {
            set
            {
                this._dateIssued = ConvertDateTimeFormat(value, DateTimeFormat);
            }
        }

        [XmlAttribute]
        public string DateIssued
        {
            get { return this._dateIssued; }
            set { this._dateIssued = value; }
        }


        public PebsPrint Print;
        public PebsPost Post;
        public List<PebsReprint> Reprints;
        //public List<string> PebsDocuments;

        public static DateTimeFormatInfo DateTimeFormat;

        private string _languageName = "en-us";
        public string LanguageName
        {
            set
            {
                try
                {
                    this._languageName = new CultureInfo(value, false).Name;
                }
                catch (Exception)
                {
                    //this._languageName = "en-us";
                }
                DateTimeFormat = new CultureInfo(this._languageName, false).DateTimeFormat;
            }
        }

        public static string ConvertDateTimeFormat(DateTime datetime, DateTimeFormatInfo format)
        {
            return ((DateTime)datetime).ToString(format);
        }
    }

    public class PebsPrint
    {
        [XmlAttribute]
        public bool IsPrinted { get; set; }
        
        public DateTime PrintedTimeValue
        {
            set
            {
                this._printedTime = PebsBatch.ConvertDateTimeFormat(value, PebsBatch.DateTimeFormat);
            }
        }
        private string _printedTime;

        [XmlAttribute]
        public string PrintedTime
        {
            get { return this._printedTime; }
            set { this._printedTime = value; }
        }

        [XmlAttribute]
        public string PrintedBy { get; set; }
    }

    public class PebsPost
    {
        [XmlAttribute]
        public bool IsPosted { get; set; }

        public DateTime PostedTimeValue
        {
            set
            {
                this._postedTime = PebsBatch.ConvertDateTimeFormat(value, PebsBatch.DateTimeFormat);
            }
        }

        private string _postedTime;

        [XmlAttribute]
        public string PostedTime
        {
            get { return this._postedTime; }
            set { this._postedTime = value; }
        }

        [XmlAttribute]
        public string PostedBy { get; set; }
    }
  
    public class PebsReprint
    {
        public DateTime ReprintDateValue
        {
            set
            {
                this._reprintDate = PebsBatch.ConvertDateTimeFormat(value, PebsBatch.DateTimeFormat);
            }
        }

        private string _reprintDate;

        [XmlAttribute]
        public string ReprintDate
        {
            get { return this._reprintDate; }
            set { this._reprintDate = value; }
        }

        [XmlAttribute]
        public string ReprintedBy { get; set; }
    }   
}
