﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.BLL.Utility;
using System.Xml.Serialization;

namespace SIL.AARTO.BLL.Model
{   
    public class RemoteSiteWebServices
    {
        public RemoteSiteWebServices()
        {
            this.WebServices = new List<RemoteSiteWebService>();
        }

        public List<RemoteSiteWebService> WebServices;

        public static RemoteSiteWebServices Instance(string RemoteSiteRegistrationsPath)
        {
            if (System.IO.File.Exists(RemoteSiteRegistrationsPath))
            {
                return ObjectSerializer.ReadXmlFileToObject<RemoteSiteWebServices>(RemoteSiteRegistrationsPath);
            }
            else
            {
                return new RemoteSiteWebServices();
            }
        }

        public static bool Save(RemoteSiteWebServices services, string RemoteSiteRegistrationsPath)
        {
            bool result = false;
            try
            {
                ObjectSerializer.SaveObjectToXmlFile(services, RemoteSiteRegistrationsPath);
                result = true;
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
    }

    public class RemoteSiteWebService
    {
        public string AutCode
        {
            get;
            set;
        }
        public string WebServiceUrl
        {
            get;
            set;
        }
    }
}
