﻿using System;
using System.Linq;

namespace SIL.AARTO.BLL.Model
{
    [Serializable]
    public abstract class IRowVersion
    {
        byte[] rowVersion;

        public byte[] RowVersion
        {
            get { return this.rowVersion; }
            set
            {
                if (value == null || !value.Any(v => v > 0)) return;
                this.rowVersion = value;
            }
        }

        public long RowVersion_Long
        {
            get { return GetRowVersionLong(this.rowVersion); }
            set
            {
                if (value <= 0) return;
                this.rowVersion = GetRowVersion(value);
            }
        }

        public void ClearRowVersion()
        {
            this.rowVersion = null;
            this.rowVersion = new byte[8];
        }

        public static byte[] GetRowVersion(long value)
        {
            return BitConverter.GetBytes(value).Reverse().ToArray();
        }

        public static long GetRowVersionLong(byte[] value)
        {
            if (value == null) value = new byte[8];
            return Convert.ToInt64(BitConverter.ToString(value).Replace("-", string.Empty), 16);
        }
    }
}
