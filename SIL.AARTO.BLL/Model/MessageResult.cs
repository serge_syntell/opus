﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;

namespace SIL.AARTO.BLL.Model
{
    public class MessageResult
    {
        public MessageResult()
        {
            Status = true;
        }

        public bool IsMessageResult
        {
            get { return true; }
        }

        public bool Status { get; set; }

        public object Body { get; set; }

        #region Message

        [ScriptIgnore] List<string> messageList = new List<string>();

        [ScriptIgnore]
        public bool HasMessage
        {
            get { return this.messageList.Count > 0; }
        }

        public string Message
        {
            get { return GetContent(this.messageList); }
        }

        public void AddMessage(string message)
        {
            if (string.IsNullOrWhiteSpace(message)
                || this.messageList.FindIndex(s => string.Equals(s, message, StringComparison.OrdinalIgnoreCase)) >= 0)
                return;
            this.messageList.Add(message);
        }

        #endregion

        #region Error

        [ScriptIgnore] List<string> errorList = new List<string>();

        [ScriptIgnore]
        public bool HasError
        {
            get { return this.errorList.Count > 0; }
        }

        public string Error
        {
            get { return GetContent(this.errorList); }
        }

        public void AddError(string error)
        {
            if (string.IsNullOrWhiteSpace(error)
                || this.errorList.FindIndex(s => string.Equals(s, error, StringComparison.OrdinalIgnoreCase)) >= 0)
                return;
            this.errorList.Add(error);
        }

        #endregion

        static string GetContent(IEnumerable<string> msgs)
        {
            var msg = string.Join("<br/>", msgs);
            //int index;
            //if (!string.IsNullOrWhiteSpace(msg)
            //    && (index = msg.IndexOf("\"", StringComparison.OrdinalIgnoreCase)) >= 0)
            //{
            //    var msgList = msg.ToCharArray(0, index).ToList();
            //    for (var i = index; i < msg.Length; i++)
            //    {
            //        var c = msg[i];
            //        if (c == '"' && msg[Math.Max(i - 1, 0)] != '\\')
            //            msgList.Add('\\');
            //        msgList.Add(c);
            //    }
            //    msg = new string(msgList.ToArray());
            //}
            return msg;
        }

        public MessageResult GetMessageResult()
        {
            return new MessageResult
            {
                Status = Status,
                Body = Body,
                messageList = this.messageList,
                errorList = this.errorList
            };
        }
    }
}
