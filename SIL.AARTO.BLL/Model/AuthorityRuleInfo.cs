﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Model
{
    public class AuthorityRuleInfo
    {
        public int WFRFAID;
        public string WFRFAName;
        public string WFRFADescr;
        public string WFRFAComment;
        public int AutIntNo;
        public int ARNumeric;
        public string ARString;


        public AuthorityRuleInfo GetAuthorityRulesInfoByWFRFANameAutoIntNo(int autIntNo, string code)
        {
            IDataReader rd = new WorkFlowRuleForAuthorityService().GetByWFRFANameAutIntNo("AR_" + code, autIntNo);
            AuthorityRuleInfo autRule = null;
            if (rd.Read())
            {
                autRule = new AuthorityRuleInfo();
                autRule.WFRFAName = rd["WFRFAName"].ToString();
                autRule.WFRFAComment = rd["WFRFAComment"].ToString();
                autRule.WFRFADescr = rd["WFRFADescr"].ToString();
                autRule.ARNumeric = int.Parse(rd["ARNumeric"].ToString());
                autRule.ARString = rd["ARString"].ToString();
            }
            rd.Close();
            return autRule;
        }
    }
}
