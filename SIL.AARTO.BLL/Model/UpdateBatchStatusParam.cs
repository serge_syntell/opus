﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Model
{
    public class UpdateBatchStatusParam
    {
        public int batchId;
        public AartoDocumentStatusList docStatus;
        public string userName;
    }
}
