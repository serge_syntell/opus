﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Model
{
    public class DateRuleInfo
    {
        public int DRID;
        public string DRName;
        public string DtRDescr;
        public int AutIntNo;
        public int ADRNoOfDays;


        public DateRuleInfo GetDateRuleInfoByDRNameAutIntNo(int autIntNo, string startDate,string endDate)
        {
            IDataReader rd = new DateRuleService().GetByDRNameAutIntNo("DR_" + startDate + "_" + endDate, autIntNo);
            DateRuleInfo dateRule = null;
            if (rd.Read())
            {
                dateRule = new DateRuleInfo();
                dateRule.DRName = rd["DRName"].ToString();
                dateRule.DtRDescr = rd["DtRDescr"].ToString();
                dateRule.ADRNoOfDays = int.Parse(rd["ADRNoOfDays"].ToString());
            }
            rd.Close();
            return dateRule;
        }
    }
}
