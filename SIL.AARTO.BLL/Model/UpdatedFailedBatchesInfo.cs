﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Model
{
    public class UpdatedFailedBatchesInfo
    {
        public List<UpdatedFailedBatchItem> failedBatches;
    }
    public class UpdatedFailedBatchItem
    {
        public UpdatedFailedBatchItem(int batchId, string errorMsg)
        {
            BatchId = batchId;
            ErrorMsg = errorMsg;
        }

        public UpdatedFailedBatchItem()
        {
        }

        public int BatchId { get; set; }
        public string ErrorMsg { get; set; }
    }
}
