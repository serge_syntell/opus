﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Model
{
    [Serializable]
    public class UpdateMessageInfo
    {
        public string LaCode;
        public int BatchId;
        public AartoDocumentStatusList ToStatus;
        public string UserName;
    }
}
