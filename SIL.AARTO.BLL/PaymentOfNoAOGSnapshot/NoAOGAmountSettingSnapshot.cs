﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.PaymentOfNoAOGSnapshot
{
     
    [Serializable]
    public class NoAOGAmountSettingSnapshot
    {
        public Notice Notices { get; set; }
        public Summons Summons { get; set; }
        public List<Charge> Charges { get; set; }
        public List<Summons_Charge> SummonsCharges { get; set; }
        public List<SumCharge> SumCharges { get; set; }
        public List<WOA> WOAs { get; set; }
      

        public void AddCharge(Charge charge)
        {
            if (Charges == null)
                Charges = new List<Charge>();

            if (Charges.Contains(charge) == false)
                Charges.Add(charge);
        }

        public void RemoveCharge(Charge charge)
        {
            if (Charges == null)
                Charges = new List<Charge>();

            if (Charges.Contains(charge))
                Charges.Remove(charge);
        }

        public void AddSumCharge(SumCharge sumCharge)
        {
            if (SumCharges == null)
                SumCharges = new List<SumCharge>();

            if (SumCharges.Contains(sumCharge) == false)
                SumCharges.Add(sumCharge);
        }

        public void RemoveSumCharge(SumCharge sumCharge)
        {
            if (SumCharges == null)
                SumCharges = new List<SumCharge>();

            if (SumCharges.Contains(sumCharge))
                SumCharges.Remove(sumCharge);
        }

        public void AddSummmonsCharge(Summons_Charge summonsCharge)
        {
            if (SummonsCharges == null)
                SummonsCharges = new List<Summons_Charge>();

            if (SummonsCharges.Contains(summonsCharge) == false)
                SummonsCharges.Add(summonsCharge);
        }

        public void RemoveSummmonsCharge(Summons_Charge summonsCharge)
        {
            if (SummonsCharges == null)
                SummonsCharges = new List<Summons_Charge>();

            if (SummonsCharges.Contains(summonsCharge))
                SummonsCharges.Remove(summonsCharge);
        }

        public void AddWOA(WOA woa)
        {
            if (WOAs == null)
                WOAs = new List<WOA>();

            if (WOAs.Contains(woa) == false)
                WOAs.Add(woa);
        }

        public void RemoveWOA(WOA woa)
        {
            if (WOAs == null)
                WOAs = new List<WOA>();

            if (WOAs.Contains(woa))
                WOAs.Remove(woa);
        }
    }

    [Serializable]
    public class Summons
    {
        public int SumIntNo { get; set; }
        public string SumStatus { get; set; }
        public int SummonsStatus { get; set; }
        public int? SumPrevSummonsStatus { get; set; }
        public string LastUser { get; set; }
    }

    [Serializable]
    public class Notice
    {
        public int NotIntNo { get; set; }
        public int NoticeStatus { get; set; }
        public int? NotPrevNoticeStatus { get; set; }
        public string LastUser { get; set; }
        // 2015-01-08, Oscar added NoAogAmountUpdated (5385)
        public bool? NoAogAmountUpdated { get; set; }
    }

 

    [Serializable]
    public class Charge
    {
        public int ChgIntNo { get; set; }
        public int NotIntNo { get; set; }
        public int ChgFineAmount { get; set; }
        public string LastUser { get; set; }
        public int ChargeStatus { get; set; }
        public string ChgNoAOG { get; set; }
        public decimal ChgRevFineAmount { get; set; }
        public decimal ChgRevDiscountAmount { get; set; }
        public decimal ChgRevDiscountedAmount { get; set; }
        public int PreviousStatus { get; set; } 
    }

    [Serializable]
    public class SumCharge
    {
        public int SChIntNo { get; set; }
        public int SumIntNo { get; set; }
         
        public decimal SChFineAmount { get; set; }
        public decimal SChRevAmount { get; set; }
        public int SumChargeStatus { get; set; }
        public int SChPrevSumChargeStatus { get; set; }
        public decimal SChSentenceAmount { get; set; }
        public decimal SChContemptAmount { get; set; }
        public decimal SChOtherAmount { get; set; }
        public decimal SChPaidAmount { get; set; }
        public string SChNoAOG { get; set; }
        public string LastUser { get; set; }
        
    }

    public class Summons_Charge
    {
        public int SCIntNo { get; set; }
        public int SumIntNo { get; set; }
        public int ChgIntNo { get; set; }
        public string LastUser { get; set; }
    }

    public class WOA
    {
        public int WOAIntNo { get; set; }
        public int SumIntNo { get; set; }
        public string WOANumber { get; set; }
        public string LastUser { get; set; }       
        public int? WOAChargeStatus { get; set; }
        
    }
 
}
