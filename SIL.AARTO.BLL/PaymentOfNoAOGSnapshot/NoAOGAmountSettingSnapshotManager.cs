﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.PaymentOfNoAOGSnapshot
{
    public class NoAOGAmountSettingSnapshotManager
    {
        // Fields
        private SqlConnection con;
        private PaymentOfNoAogSnapshotService snapshotService = new PaymentOfNoAogSnapshotService();

        public NoAOGAmountSettingSnapshotManager(string connstr)
        {
            this.con = new SqlConnection(connstr);
        }

        public bool SetSnapshot(int sumIntNo, bool lockSnapshot, string lastUser)
        {

            DataSet dsSnapshot = GetSnapshotDataFromDB(sumIntNo);

            if (dsSnapshot != null)
            {
                SortSnapshotDataSet(dsSnapshot);

                XmlDocument xmlDoc = ReadSnapshotToXML(dsSnapshot);

                if (xmlDoc != null && !String.IsNullOrEmpty(xmlDoc.InnerXml))
                {
                    if (CheckSnapshotIsCreated(sumIntNo))
                    {
                        return UpdateSnapshot(sumIntNo, false, xmlDoc.InnerXml, lastUser);
                    }
                    else
                    {
                        //Save xml to DB
                        return AddSnapshot(sumIntNo, lockSnapshot, xmlDoc.InnerXml, lastUser);
                    }
                }
            }
            return false;
        }

        public int RollBackFromSnapshot(int sumIntNo, string lastUser)
        {
            int returnValue = 0;
            XmlDocument xmlDoc = GetSnapshotFromTable(sumIntNo);

            if (xmlDoc != null && !String.IsNullOrEmpty(xmlDoc.InnerXml))
            {
                NoAOGAmountSettingSnapshot snapShot = ReadSnapshotToEntity(xmlDoc);

                if (snapShot != null)
                {
                    //roll back operation here
                    try
                    {
                        returnValue = ValidSnapshot(sumIntNo, snapShot);
                        if (returnValue > 0)
                        {
                            bool flag = RollBackFromSnapShot(snapShot, lastUser);

                            if (flag)
                            {
                                return returnValue;
                            }
                            else
                            {
                                return 0;
                            }
                        }
                        else
                        {
                            return returnValue;
                        }

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }


            }
            return 0;
        }

        int ValidSnapshot(int sumIntNo, NoAOGAmountSettingSnapshot snapShot)
        {
            PaymentOfNoAogSnapshot snapShotDB = snapshotService.GetBySumIntNo(snapShot.Summons.SumIntNo).FirstOrDefault();

            if (snapShotDB.PonsDataLocked)
            {
                return -6;
            }

            NoticeSummons ns = new NoticeSummonsService().GetBySumIntNo(sumIntNo).FirstOrDefault();
            if (ns == null) return -1;
            SIL.AARTO.DAL.Entities.Notice notice = new NoticeService().GetByNotIntNo(ns.NotIntNo);
            if (notice.NoticeStatus != snapShot.Notices.NoticeStatus)
            {
                return -2;
            }
            SIL.AARTO.DAL.Entities.Summons summons = new SummonsService().GetBySumIntNo(ns.SumIntNo);
            if (summons.SummonsStatus != snapShot.Summons.SummonsStatus)
            {
                return -3;
            }
            ChargeService cService = new ChargeService();
            foreach (Charge c in snapShot.Charges)
            {
                SIL.AARTO.DAL.Entities.Charge chg = cService.GetByChgIntNo(c.ChgIntNo);

                if (chg == null || chg.ChgNoAog != c.ChgNoAOG || chg.ChargeStatus != c.ChargeStatus)
                {
                    return -4;
                }
            }

            SumChargeService scService = new SumChargeService();
            foreach (SumCharge sc in snapShot.SumCharges)
            {
                SIL.AARTO.DAL.Entities.SumCharge sumCharge = scService.GetBySchIntNo(sc.SChIntNo);
                if (sumCharge == null || sumCharge.SchNoAog != sc.SChNoAOG || sumCharge.SumChargeStatus != sc.SumChargeStatus)
                {
                    return -5;
                }
            }

            return 1;
        }

        bool RollBackFromSnapShot(NoAOGAmountSettingSnapshot snapShot, string lastUser)
        {
            try
            {

                using (ConnectionScope.CreateTransaction())
                {
                    PaymentOfNoAogSnapshot snapShotDB = snapshotService.GetBySumIntNo(snapShot.Summons.SumIntNo).FirstOrDefault();

                    //Roall back Summons
                    ProcessSummons(snapShot.Summons, lastUser);

                    //roll back notice
                    ProcessNotice(snapShot.Notices, lastUser);

                    //roll back sumcharge
                    ProcessSumCharge(snapShot.SumCharges);


                    //roll back charge
                    ProcessCharge(snapShot.Charges, snapShot.Summons.SumIntNo, lastUser);

                    //roll back summons_charge
                    // ProcessSummonsCharge(snapShotDB, snapShot.SummonsCharges);


                    //Jerry 2014-03-05 don't rollback woa
                    //Process WOA
                    //ProcessWoa(snapShot.WOAs, snapShot.Charges, snapShot.Summons.SumIntNo, lastUser);

                    snapShotDB.PonsDataLocked = true;

                    EvidencePack ep = new EvidencePack()
                    {
                        ChgIntNo = snapShot.Charges[0].ChgIntNo,
                        //EpItemAlternateDescr = "",
                        EpItemDate = DateTime.Now,
                        EpItemDescr = String.Format("Fine amount reversed by {0}", lastUser),
                        EpSourceId = snapShot.Summons.SumIntNo,
                        EpSourceTable = "Summons",
                        EpTransactionDate = DateTime.Now,
                        LastUser = lastUser

                    };

                    snapShotDB.LastUser = lastUser;
                    snapshotService.Save(snapShotDB);
                    new EvidencePackService().Save(ep);
                    ConnectionScope.Complete();

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessSummons(SIL.AARTO.BLL.PaymentOfNoAOGSnapshot.Summons s, string supervisorName)
        {
            try
            {
                SummonsService sService = new SummonsService();
                SIL.AARTO.DAL.Entities.Summons summons = sService.GetBySumIntNo(s.SumIntNo);
                summons.SumStatus = s.SumStatus;

                summons.SummonsStatus = s.SummonsStatus;

                summons.SumPrevSummonsStatus = s.SumPrevSummonsStatus;
                summons.PaymentOfNoAogComment = null;
                summons.PaymentOfNoAogPublicProsecutor = null;

                // 2013-07-17 add LastUser by Henry
                summons.LastUser = supervisorName;
                sService.Save(summons);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // 2013-07-17 add parameter lastUser by Henry
        void ProcessNotice(SIL.AARTO.BLL.PaymentOfNoAOGSnapshot.Notice n, string lastUser)
        {
            try
            {
                NoticeService nService = new NoticeService();
                SIL.AARTO.DAL.Entities.Notice notice = nService.GetByNotIntNo(n.NotIntNo);
                notice.NoticeStatus = n.NoticeStatus;
                notice.NotPrevNoticeStatus = n.NotPrevNoticeStatus;
                notice.LastUser = lastUser;
                // 2015-01-08, Oscar added NoAogAmountUpdated (5385)
                notice.NoAogAmountUpdated = n.NoAogAmountUpdated;
                nService.Save(notice);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessSumCharge(List<SIL.AARTO.BLL.PaymentOfNoAOGSnapshot.SumCharge> scList)
        {
            try
            {
                SumChargeService scService = new SumChargeService();

                foreach (SIL.AARTO.BLL.PaymentOfNoAOGSnapshot.SumCharge sc in scList)
                {
                    SIL.AARTO.DAL.Entities.SumCharge sumCharge = scService.GetBySchIntNo(sc.SChIntNo);

                    if (sumCharge == null)
                    {
                        sumCharge = new DAL.Entities.SumCharge();
                    }

                    sumCharge.SchIntNo = sc.SChIntNo;
                    sumCharge.SumIntNo = sc.SumIntNo;
                    sumCharge.SumChargeStatus = sc.SumChargeStatus;
                    sumCharge.SchPrevSumChargeStatus = sc.SChPrevSumChargeStatus;

                    sumCharge.SchFineAmount = sc.SChFineAmount;
                    sumCharge.SchRevAmount = sc.SChRevAmount;

                    sumCharge.SchSentenceAmount = sc.SChSentenceAmount;
                    sumCharge.SchContemptAmount = sc.SChContemptAmount;
                    sumCharge.SchOtherAmount = sc.SChOtherAmount;
                    sumCharge.SchPaidAmount = sc.SChPaidAmount;
                    sumCharge.SchNoAog = sc.SChNoAOG;
                    sumCharge.LastUser = sc.LastUser;


                    scService.Save(sumCharge);

                    //TList<ScDeferredPayments> payments = new ScDeferredPaymentsService().GetBySchIntNo(sumCharge.SchIntNo);

                    //if (payments != null && payments.Count > 0)
                    //{
                    //    new ScDeferredPaymentsService().Delete(payments);
                    //}

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void ProcessCharge(List<SIL.AARTO.BLL.PaymentOfNoAOGSnapshot.Charge> chargeList, int sumIntNo, string lastUser)
        {
            try
            {
                ChargeService cService = new ChargeService();
                foreach (SIL.AARTO.BLL.PaymentOfNoAOGSnapshot.Charge c in chargeList)
                {
                    SIL.AARTO.DAL.Entities.Charge charge = cService.GetByChgIntNo(c.ChgIntNo);
                    if (charge == null)
                    {
                        charge = new DAL.Entities.Charge();
                    }

                    charge.NotIntNo = c.NotIntNo;

                    charge.LastUser = c.LastUser;

                    charge.ChargeStatus = c.ChargeStatus;

                    charge.ChgNoAog = c.ChgNoAOG;
                    charge.ChgRevFineAmount = (float)c.ChgRevFineAmount;
                    charge.ChgRevDiscountAmount = (float)c.ChgRevDiscountAmount;
                    charge.ChgRevDiscountedAmount = (float)c.ChgRevDiscountedAmount;
                    charge.PreviousStatus = c.PreviousStatus;

                    cService.Save(charge);

                }

                //Charge firstMainCharge = GetFirstMainCharge(chargeList);// chargeList.Where(c => c.ChargeStatus < 900 && c.ChgIsMain == true).OrderBy(c => c.ChgSequence).FirstOrDefault();
                //if (firstMainCharge != null)
                //{
                //    //“Judgement Reversed by “ + SupervisorName + “ on “ + SupervisorActionDate.
                //    InsertEvidencePack(firstMainCharge.ChgIntNo, DateTime.Now, String.Format("Judgement reversed by {0} on {1}", lastUser, DateTime.Now.ToString("yyyy/MM/dd")), "Summons", sumIntNo, lastUser);

                //}
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }




        NoAOGAmountSettingSnapshot ReadSnapshotToEntity(XmlDocument document)
        {
            NoAOGAmountSettingSnapshot snapShot = new NoAOGAmountSettingSnapshot();

            XmlNode root = document.FirstChild;

            foreach (XmlNode node in root.ChildNodes)
            {
                object obj = null;
                switch (node.Name)
                {
                    case "Summons": obj = new Summons(); break;
                    case "Notice": obj = new Notice(); break;
                    case "Charge": obj = new Charge(); break;
                    case "SumCharge": obj = new SumCharge(); break;
                    case "Summons_Charge": obj = new Summons_Charge(); break;
                    case "CourtDates": obj = new CourtDates(); break;
                    case "WOA": obj = new WOA(); break;

                }

                if (obj != null)
                {
                    PropertyInfo proInfo = null; object value = null; Type type = null;
                    foreach (XmlNode valueNode in node.ChildNodes)
                    {
                        proInfo = obj.GetType().GetProperty(valueNode.Name);
                        if (proInfo != null)
                        {

                            type = proInfo.PropertyType;

                            if (proInfo.PropertyType.IsGenericType && proInfo.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                            {
                                type = proInfo.PropertyType.GetGenericArguments()[0];
                                switch (type.FullName)
                                {
                                    case "System.DateTime":
                                        if (!String.IsNullOrEmpty(valueNode.InnerText)) value = DateTime.Parse(valueNode.InnerText);
                                        break;
                                    default:
                                        value = valueNode.InnerText.Trim();
                                        break;
                                }

                            }
                            else
                            {
                                value = String.IsNullOrEmpty(valueNode.InnerText) == true ? null : valueNode.InnerText.Trim();
                            }

                            proInfo.SetValue(obj, System.Convert.ChangeType(value, type), null);
                        }
                    }


                    switch (obj.GetType().Name)
                    {
                        case "Summons": snapShot.Summons = obj as Summons; break;
                        case "Notice": snapShot.Notices = obj as Notice; break;
                        //case "Notice_Summons": snapShot.NoticeSummons = obj as Notice_Summons; break;
                        case "Charge": snapShot.AddCharge(obj as Charge); break;
                        //case "Charge_SumCharge": snapShot.AddChargeSumCharge(obj as Charge_SumCharge); break;
                        case "SumCharge": snapShot.AddSumCharge(obj as SumCharge); break;
                        case "Summons_Charge": snapShot.AddSummmonsCharge(obj as Summons_Charge); break;
                        //case "CourtDates": snapShot.CourtDates = obj as CourtDates; break;
                        case "WOA": snapShot.AddWOA(obj as WOA); break;
                        //case "Summons_Bail": snapShot.AddSummonsBail(obj as Summons_Bail); break;
                    }
                }

            }

            return snapShot;
        }


        private bool AddSnapshot(int sumIntNo, bool lockSnapshot, string xml, string lastUser)
        {
            PaymentOfNoAogSnapshot snapshot = new PaymentOfNoAogSnapshot();

            snapshot.PonsDataLocked = lockSnapshot;
            snapshot.PonsDateCreated = DateTime.Now;
            snapshot.SumIntNo = sumIntNo;
            snapshot.PonsDataXml = xml;
            snapshot.LastUser = lastUser;

            return (snapshotService.Save(snapshot) != null);
        }

        private bool UpdateSnapshot(int sumIntNo, bool locked, string xml, string lastUser)
        {
            PaymentOfNoAogSnapshot snapshot = snapshotService.GetBySumIntNo(sumIntNo).FirstOrDefault();
            if (snapshot == null) return false;

            snapshot.PonsDataLocked = locked;
            snapshot.PonsDataXml = xml;
            snapshot.LastUser = lastUser;

            return (snapshotService.Save(snapshot) != null);


        }

        DataSet GetSnapshotDataFromDB(int sumIntNo)
        {
            DataSet dsReturn = new DataSet();

            SqlCommand command = con.CreateCommand();
            command.CommandText = "CreatePaymentOfNoAOGSnapshot";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@SumIntNo", SqlDbType.Int)).Value = sumIntNo;

            SqlDataAdapter da = new SqlDataAdapter(command);

            try
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                da.Fill(dsReturn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }

            return dsReturn;
        }

        XmlDocument GetSnapshotFromTable(int sumIntNo)
        {
            XmlDocument document = new XmlDocument();
            //MemoryStream memoryStream = new MemoryStream();
            try
            {

                PaymentOfNoAogSnapshot snapshot = new PaymentOfNoAogSnapshotService().GetBySumIntNo(sumIntNo).FirstOrDefault();
                if (snapshot != null && !String.IsNullOrEmpty(snapshot.PonsDataXml))
                {
                    document.LoadXml(snapshot.PonsDataXml);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return document;
        }

        void SortSnapshotDataSet(DataSet ds)
        {
            try
            {
                ds.DataSetName = "PaymentOfNoAOGSnapshot";

                for (int index = 0; index < ds.Tables.Count; index++)
                {
                    switch (index)
                    {
                        case 0:
                            ds.Tables[index].TableName = "Summons"; break;
                        case 1:
                            ds.Tables[index].TableName = "Notice"; break;

                        case 2:
                            ds.Tables[index].TableName = "SumCharge"; break;

                        case 3:
                            ds.Tables[index].TableName = "Charge"; break;

                        case 4:
                            ds.Tables[index].TableName = "WOA"; break;

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        XmlDocument ReadSnapshotToXML(DataSet ds)
        {
            XmlDocument document = new XmlDocument();
            MemoryStream memoryStream = new MemoryStream();
            try
            {
                ds.WriteXml(memoryStream, XmlWriteMode.IgnoreSchema);
                memoryStream.Position = 0;

                document.Load(memoryStream);

                memoryStream.Close();
                memoryStream.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return document;
        }

        public bool CheckSnapshotIsCreated(int sumIntNo)
        {
            PaymentOfNoAogSnapshot snapshot = new PaymentOfNoAogSnapshotService().GetBySumIntNo(sumIntNo).FirstOrDefault();
            if (snapshot != null && !String.IsNullOrEmpty(snapshot.PonsDataXml))
            {
                return true;
            }
            return false;
        }
    }
}
