﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.InfringerOption
{
    public class PaymentMethodType
    {
        public const string BankDebitOrder = "D";
        public const string MonthlyDeposits = "M";
    }
    public class RVOfEOReasonsType
    {
        public const string HavePaid = "a";
        public const string HaveSubmittedA04 = "b";
        public const string HaveSubmittedA08 = "c";
        public const string HaveSubmittedA07 = "d";
        public const string HaveSubmittedA10 = "e";
        public const string HaveAppearedInCourt = "f";
        public const string CannotAppearedInCourt = "g";
        public const string OtherReasons = "h";
    }
}
