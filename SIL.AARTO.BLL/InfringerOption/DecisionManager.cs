﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using SIL.AARTO.BLL.InfringerOption.Model;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.BLL.AARTONoticeClock;
using SIL.AARTO.BLL.EntLib;
namespace SIL.AARTO.BLL.InfringerOption
{
    /// <summary>
    /// Author:Lucas Li
    /// created:2010/3/10
    /// </summary>
    public class DecisionManager
    {
        public static int GetTopOneRepresentationIDForDecide(out int docTypeID)
        {
            int repID = -1;
            docTypeID = -1;
            IDataReader idr = new AartoRepresentationService().GetTopOneRepresentationForDecide((int)RepresentationCodeList.AARTO_RepresentationRegistered
                , (int)ChargeStatusList.AARTO_RegisterRepresentation_08
                , (int)ChargeStatusList.AARTO_RegisterRepresentationNominatedDriver_07
                , (int)ChargeStatusList.AARTO_ApplyToPayInstalments_04
                , (int)AartoDocumentStatusList.Received);//, (int)ChargeStatusList.AARTO_ElectCourt_10);
            while (idr.Read())
            {
                docTypeID = idr["AaDocTypeID"] == DBNull.Value ? -1 : Convert.ToInt32(idr["AaDocTypeID"]);
                repID = idr["AaRepID"] == DBNull.Value ? -1 : Convert.ToInt32(idr["AaRepID"]);
                break;
            }
            idr.Close();
            return repID;
        }
        public static int GetTopOneRepresentationForDecideWithLocked(out int docTypeID)
        {
            int repID = -1;
            docTypeID = -1;
            IDataReader idr = new AartoRepresentationService().GetTopOneRepresentationForDecideWithLocked(
                Session.UserID
                , 2
                , (int)RepresentationCodeList.AARTO_RepresentationRegistered
                , (int)ChargeStatusList.AARTO_RegisterRepresentation_08
                , (int)ChargeStatusList.AARTO_RegisterRepresentationNominatedDriver_07
                , (int)ChargeStatusList.AARTO_ApplyToPayInstalments_04
                , (int)AartoDocumentStatusList.Received);
            bool result = false;
            while (idr.Read())
            {
                result = idr[0].ToString() == "1";
                break;
            }
            if (result)
            {
                idr.NextResult();
                while (idr.Read())
                {
                    docTypeID = idr["AaDocTypeID"] == DBNull.Value ? -1 : Convert.ToInt32(idr["AaDocTypeID"]);
                    repID = idr["AaRepID"] == DBNull.Value ? -1 : Convert.ToInt32(idr["AaRepID"]);
                    break;
                }
            }
            idr.Close();
            return repID;
        }
        public static int GetTopOneRevocationOfEnforcementOrderForDecideWithLocked()
        {
            int repID = -1;
            IDataReader idr = new AartoRepresentationService().GetTopOneRevocationOfEnforcementOrderForDecideWithLocked(
                Session.UserID
                ,2
                , (int)RepresentationCodeList.AARTO_RepresentationRegistered
                , (int)AartoDocumentTypeList.AARTO14
                , (int)ChargeStatusList.AARTO_RequestRevocationEnforcementOrder_14
                , (int)AartoDocumentStatusList.Received);
            bool result = false;
            while (idr.Read())
            {
                result = idr[0].ToString() == "1"; 
                break;
            }
            if (result)
            {
                idr.NextResult();
                while (idr.Read())
                {
                    repID = idr["AaRepID"] == DBNull.Value ? -1 : Convert.ToInt32(idr["AaRepID"]);
                    break;
                }
            }
            idr.Close();
            return repID;
        }
        public static int GetTopOneRevocationOfEnforcementOrderForDecide()
        {
            int repID = -1;
            IDataReader idr = new AartoRepresentationService().GetTopOneRevocationOfEnforcementOrderForDecide(
                (int)RepresentationCodeList.AARTO_RepresentationRegistered
                , (int)AartoDocumentTypeList.AARTO14
                , (int)ChargeStatusList.AARTO_RequestRevocationEnforcementOrder_14
                , (int)AartoDocumentStatusList.Received);
            while (idr.Read())
            {
                repID = idr["AaRepID"] == DBNull.Value ? -1 : Convert.ToInt32(idr["AaRepID"]);
                break;
            }
            idr.Close();
            return repID;
        }
        public static int GetTopOneRepresentationForRevocationOfEnforcementOrder(int chgIntNo,out int docTypeID)
        {
            int repID = -1;
            docTypeID = -1;
            IDataReader idr = new AartoRepresentationService().GetTopOneRepresentationForRevocationOfEnforcementOrder(chgIntNo
                , (int)RepresentationCodeList.AARTO_RepresentationRegistered
                , (int)AartoDocumentTypeList.AARTO14
                , (int)AartoDocumentStatusList.Received);
            while (idr.Read())
            {
                docTypeID = idr["AaDocTypeID"] == DBNull.Value ? -1 : Convert.ToInt32(idr["AaDocTypeID"]);
                repID = idr["AaRepID"] == DBNull.Value ? -1 : Convert.ToInt32(idr["AaRepID"]);
                break;
            }
            idr.Close();
            return repID;

        }
        public static DecisionModel GetDecideModelByRepID(int repID, int docTypeID)
        {
            DecisionModel model = new DecisionModel();
            AartoRepresentation rep = new AartoRepresentationService().GetByAaRepId(repID);
            model.PostCodeIsValid = Validate.IsSAPostCode(rep.AaRepNewInfPostalCode);
            if (rep != null)
            {
                
                model.Rep = rep;
                Charge cg = new ChargeService().GetByChgIntNo(rep.ChgIntNo);
                Notice n = new NoticeService().GetByNotIntNo(cg.NotIntNo);
                AartoClock clock = new AartoClockService().GetByNotIntNo(n.NotIntNo)[0];
                AartoRepDocument ad = new AartoRepDocumentService().GetByAaRepIdAaDocTypeId(repID, docTypeID);
                //need be updated 
                TList<AartoDocumentImage> imageList = AARTODocumentManager.GetAARTODocumentImageListByAaDocIDAndAaDocSourceTableID((int)ad.AaRepDocId, (int)AartoDocumentSourceTableList.AARTORepDocument);
                string prePath = "../../GetImage.aspx?id=";
                for(int i=0;i<imageList.Count;i++)
                {
                    AartoDocumentImage image = imageList[i];
                    if (Validate.IsNull(model.DocumentImageUrlList))
                    {
                        model.DocumentImageUrlList = prePath + image.AaDocImgId;
                    }
                    else
                    {
                        model.DocumentImageUrlList = model.DocumentImageUrlList + "|" + prePath + image.AaDocImgId;
                    }
                }
                model.RepID = rep.AaRepId;
                model.AaDocTypeID = ad.AaDocTypeId;
                model.NotIntNo = n.NotIntNo;
                model.ChgIntNo = cg.ChgIntNo;
                model.AaClockTypeID = clock.AaClockTypeId;
                model.NoticeTicketNumber = n.NotTicketNo;
                model.InfringementDate = n.NotOffenceDate;
                model.IssuingAuthority = "";//
            }
            else model = null;
            return model;
        }

        private static string GetPreDocumentImagePath(int ifsID)
        {
            ImageFileServer ifs = new ImageFileServerService().GetByIfsIntNo(ifsID);
            return @"\\" + ifs.ImageMachineName + @"\" + ifs.ImageShareName + @"\";
        }
        public static string GetAARTORepresentationDocumentImagePathByAaDocImgID(long id)
        {
            AartoDocumentImage image = new AartoDocumentImageService().GetByAaDocImgId(id);
            AartoRepDocument repDoc=new AartoRepDocumentService().GetByAaRepDocId(image.AaDocId);
            return GetPreDocumentImagePath((int)repDoc.IfsIntNo) + image.AaDocImgPath;
        }
        public static bool SaveRep(AartoRepresentation rep)
        {
            rep.LastUser = rep.LastUser + "lucasli";
            new AartoRepresentationService().Save(rep);
            return true;
        }
        #region get data list
        public static TList<AartoOrganisationType> GetOrganisationTypeList()
        {
            return new AartoOrganisationTypeService().GetAll();
        }
        public static TList<AartoidType> GetIDTypeList()
        {
            return new AartoidTypeService().GetAll();
        }
        public static TList<AartoLicenceCode> GetLicenceCodeList()
        {
            return new AartoLicenceCodeService().GetAll();
        }
        public static TList<AartoLearnerCode> GetLearnerCodeList()
        {
            return new AartoLearnerCodeService().GetAll();
        }
        public static TList<AartoPrDpCode> GetPrDpCodeList()
        {
            return new AartoPrDpCodeService().GetAll();
        }
        public static TList<VehicleMake> GetVehicleMakeList()
        {
            return new VehicleMakeService().GetAll();
        }
        public static TList<VehicleType> GetVehicleTypeList()
        {
            return new VehicleTypeService().GetAll();
        }
        public static TList<VehicleColour> GetVehicleColourList()
        {
            return new VehicleColourService().GetAll();
        }
        public static TList<Court> GetCourtList()
        {
            return new CourtService().GetAll();
        }
        #endregion

        public static bool DecideRepresentation(DecisionModel model)
        {
            TransactionManager AARTOTM = ConnectionScope.CreateTransaction();
            try
            {
                if (!AARTOTM.IsOpen)
                {
                    AARTOTM.BeginTransaction();
                }
                AartoRepresentation rep = new AartoRepresentationService().GetByAaRepId(model.RepID);
                AartoRepDocument oldDoc = new AartoRepDocumentService().GetByAaRepIdAaDocTypeId(rep.AaRepId, model.AaDocTypeID);

                Charge oldCharge = new ChargeService().GetByChgIntNo(model.ChgIntNo);
                Clock clock = new Clock(model.NotIntNo);
                clock.LastUser = model.LastUser;

                rep.RcCode = model.DecideResult;
                rep.AaRepLockedUserId = null;
                rep.AaRepLockedDateTime = null;
                new AartoRepresentationService().Save(rep);
                Clock newClock = null;
                switch (model.AaDocTypeID)
                {
                    case (int)AartoDocumentTypeList.AARTO04:
                        #region pay instalment
                        if (model.DecideResult == (int)RepresentationCodeList.AARTO_RepresentationSuccessful)
                        {

                            oldCharge.ChargeStatus = (int)ChargeStatusList.AARTO_PayByInstalment_06;
                            oldCharge.LastUser = model.LastUser;
                            new ChargeService().Save(oldCharge);

                            //create defered payment plan
                            //rep.AaRepPenAmount
                            AartoPayInInstallmentsPlan payPlan = null;
                            for (int i = 0; i < (rep.AaRepNumOfMonth ?? 0); i++)
                            {
                                payPlan = new AartoPayInInstallmentsPlan();
                                payPlan.ChgIntNo = rep.ChgIntNo;
                                payPlan.AaDuePaymentDate = ConvertDateTime.GetShortDateTime(ConvertDateTime.GetNowDate().AddDays((double)SystemParameter.Get().InstallmentDays).AddMonths(i));
                                payPlan.AaDuePaymentAmount = (decimal)(rep.AaRepPenAmount / rep.AaRepNumOfMonth);
                                payPlan.LastUser = model.LastUser;
                                new AartoPayInInstallmentsPlanService().Insert(payPlan);
                                //
                            }
                            #region clock
                            clock.Stop();
                            newClock = new InstallmentPaymentClock(model.NotIntNo);
                            newClock.LastUser = model.LastUser;
                            newClock.RepID=model.RepID;
                            newClock.Create();
                            #endregion

                            CancelOtherRepresentation(rep, model.LastUser);
                        }
                        else
                        {

                            oldCharge.ChargeStatus = rep.CsCode;
                            new ChargeService().Save(oldCharge);

                            #region clock
                            clock.Stop();
                            newClock = new RequestIntallmentsRejectedFullPaymentClock(model.NotIntNo);
                            newClock.LastUser = model.LastUser;
                            newClock.RepID=model.RepID;                            
                            newClock.Create();
                            #endregion
                        }
                        #endregion
                        break;
                    case (int)AartoDocumentTypeList.AARTO07:
                        #region nominate driver
                        if (model.DecideResult == (int)RepresentationCodeList.AARTO_RepresentationSuccessful)
                        {

                            oldCharge.ChargeStatus = (int)ChargeStatusList.AARTO_Cancelled_NominatedDriver;//
                            oldCharge.LastUser = model.LastUser;
                            new ChargeService().Save(oldCharge);

                            #region cancel current notice
                            //add notice cancelled links
                            NoticeCancelledLinks ncl = new NoticeCancelledLinks();
                            ncl.ChgIntNo = model.ChgIntNo;
                            ncl.NotIntNo = model.NotIntNo;
                            ncl.LastUser = model.LastUser;
                            new NoticeCancelledLinksService().Save(ncl);
                            //update aartoDocument
                            //will update the oldDoc when create the AARTO05b

                            AddEvidencePack(Constant.NoticeCancelled, model.ChgIntNo, "Notice", model.NotIntNo, model.LastUser, (int)EpActionTypeList.Others);
                            #endregion

                            #region new notice
                            Notice oldNotice = new NoticeService().GetByNotIntNo(model.NotIntNo);
                            Notice newNotice = new Notice();// (Notice)oldNotice.Clone();
                            #region new notice fields
                            newNotice.AutIntNo = oldNotice.AutIntNo;
                            newNotice.NotTicketNo = TicketNumber.GenerateNotNo(newNotice.AutIntNo, model.LastUser);
                            newNotice.NotLocDescr = oldNotice.NotLocDescr;
                            newNotice.NotLocCode = oldNotice.NotLocCode;
                            newNotice.NotSpeedLimit = oldNotice.NotSpeedLimit;
                            newNotice.NotCameraId = oldNotice.NotCameraId;
                            newNotice.NotOfficerSname = oldNotice.NotOfficerSname;
                            newNotice.NotOfficerInit = oldNotice.NotOfficerInit;
                            newNotice.NotOfficerNo = oldNotice.NotOfficerNo;
                            newNotice.NotOfficerGroup = oldNotice.NotOfficerGroup;
                            newNotice.NotRegNo = oldNotice.NotRegNo;
                            newNotice.NotSpeed1 = oldNotice.NotSpeed1;
                            newNotice.NotSpeed2 = oldNotice.NotSpeed2;
                            newNotice.NotVehicleMake = oldNotice.NotVehicleMake;
                            newNotice.NotVehicleType = oldNotice.NotVehicleType;
                            newNotice.NotVehicleMakeCode = oldNotice.NotVehicleMakeCode;
                            newNotice.NotVehicleTypeCode = oldNotice.NotVehicleTypeCode;
                            newNotice.NotOffenderType = oldNotice.NotOffenderType;
                            newNotice.NotFilmNo = oldNotice.NotFilmNo;
                            newNotice.NotFrameNo = oldNotice.NotFrameNo;
                            newNotice.NotRefNo = oldNotice.NotRefNo;
                            newNotice.NotSeqNo = oldNotice.NotSeqNo;
                            newNotice.NotOffenceDate = oldNotice.NotOffenceDate;
                            newNotice.NotSource = oldNotice.NotSource;
                            newNotice.NotOffenceType = oldNotice.NotOffenceType;
                            newNotice.NotRdTypeCode = oldNotice.NotRdTypeCode;
                            newNotice.NotRdTypeDescr = oldNotice.NotRdTypeDescr;
                            newNotice.NotCourtNo = oldNotice.NotCourtNo;
                            newNotice.NotCourtName = oldNotice.NotCourtName;
                            newNotice.NotTravelDirection = oldNotice.NotTravelDirection;
                            newNotice.NotElapsedTime = oldNotice.NotElapsedTime;
                            newNotice.NotIssue1stNoticeDate = oldNotice.NotIssue1stNoticeDate;
                            newNotice.NotLoadDate = oldNotice.NotLoadDate;
                            newNotice.NotNaTisRegNo = oldNotice.NotNaTisRegNo;
                            newNotice.NotDateCoo = oldNotice.NotDateCoo;
                            newNotice.NotVehicleDescr = oldNotice.NotVehicleDescr;
                            newNotice.NotVehicleCat = oldNotice.NotVehicleCat;
                            newNotice.NotClearanceCert = oldNotice.NotClearanceCert;
                            newNotice.NotRegisterAuth = oldNotice.NotRegisterAuth;
                            newNotice.NotStatutoryOwner = oldNotice.NotStatutoryOwner;
                            newNotice.NotNatureOfPerson = oldNotice.NotNatureOfPerson;
                            newNotice.NotNaTisvmCode = oldNotice.NotNaTisvmCode;
                            newNotice.NotNaTisvtCode = oldNotice.NotNaTisvtCode;
                            newNotice.NotVehicleUsage = oldNotice.NotVehicleUsage;
                            newNotice.NotVehicleColour = oldNotice.NotVehicleColour;
                            newNotice.NotVehicleRegisterNo = oldNotice.NotVehicleRegisterNo;
                            newNotice.NotViNorChassis = oldNotice.NotViNorChassis;
                            newNotice.NotEngine = oldNotice.NotEngine;
                            newNotice.NotVehicleLicenceExpiry = oldNotice.NotVehicleLicenceExpiry;
                            newNotice.NotProxyFlag = oldNotice.NotProxyFlag;
                            newNotice.NotNaTisIndicator = oldNotice.NotNaTisIndicator;
                            newNotice.NotNaTisErrorFieldList = oldNotice.NotNaTisErrorFieldList;
                            newNotice.NotCiprusPrintReqName = oldNotice.NotCiprusPrintReqName;
                            newNotice.NotSendTo = oldNotice.NotSendTo;
                            newNotice.NotCamSerialNo = oldNotice.NotCamSerialNo;
                            newNotice.NotPaymentDate = oldNotice.NotPaymentDate;
                            newNotice.NotNatisProxyIndicator = oldNotice.NotNatisProxyIndicator;
                            newNotice.OrigRegNo = oldNotice.OrigRegNo;
                            newNotice.NotTicketProcessor = oldNotice.NotTicketProcessor;
                            newNotice.AdjOfficerNo = oldNotice.AdjOfficerNo;
                            newNotice.LastUser = model.LastUser;
                            #endregion
                            new NoticeService().Insert(newNotice);

                            NoticeToBePostedA03Clock newNoticeClock = new NoticeToBePostedA03Clock(newNotice.NotIntNo);
                            newNoticeClock.LastUser = model.LastUser;
                            newNoticeClock.Create();
                            newNoticeClock.Start(newNotice.NotOffenceDate);

                            //update notice frame links
                            UpdateNoticeFrameLinksByNotIntNo(newNotice.NotIntNo, model.NotIntNo);
                            //new charge
                            Charge newCharge = new Charge();
                            #region new charge fields
                            newCharge.NotIntNo = newNotice.NotIntNo;
                            newCharge.CtIntNo = oldCharge.CtIntNo;
                            newCharge.ChgOffenceType = oldCharge.ChgOffenceType;
                            newCharge.ChgOffenceCode = oldCharge.ChgOffenceCode;
                            newCharge.ChgOffenceDescr = oldCharge.ChgOffenceDescr;
                            newCharge.ChgOffenceDescr2 = oldCharge.ChgOffenceDescr2;
                            newCharge.ChgOffenceDescr3 = oldCharge.ChgOffenceDescr3;
                            newCharge.ChgStatutoryRef = oldCharge.ChgStatutoryRef;
                            newCharge.ChgStatRefLine1 = oldCharge.ChgStatRefLine1;
                            newCharge.ChgStatRefLine2 = oldCharge.ChgStatRefLine2;
                            newCharge.ChgStatRefLine3 = oldCharge.ChgStatRefLine3;
                            newCharge.ChgStatRefLine4 = oldCharge.ChgStatRefLine4;
                            newCharge.ChgNoAog = oldCharge.ChgNoAog;
                            newCharge.ChgFineAmount = oldCharge.ChgFineAmount;
                            //0.5 should be get from some other where
                            newCharge.ChgDiscountAmount = Convert.ToDecimal(0.5 * newCharge.ChgFineAmount);
                            newCharge.ChgRevFineAmount = oldCharge.ChgRevFineAmount;

                            if (!Validate.IsSAPostCode(rep.AaRepNewInfPostalCode))
                                newCharge.ChargeStatus = (int)ChargeStatusList.CPI_NoticeNeedsCorrection;
                            else
                                newCharge.ChargeStatus = (int)ChargeStatusList.CPI_CivitasAllocationLoaded;
                            #endregion
                            newCharge.LastUser = model.LastUser;
                            new ChargeService().Insert(newCharge);
                            //new owner
                            Owner newOwer = (Owner)new OwnerService().GetByNotIntNo(model.NotIntNo)[0].Clone();
                            newOwer.NotIntNo = newNotice.NotIntNo;
                            newOwer.RowVersion = new byte[0];
                            newOwer.OwnIntNo = 0;
                            newOwer.LastUser = model.LastUser;
                            new OwnerService().Insert(newOwer);

                            Driver newDriver = new Driver();
                            #region new driver fields
                            newDriver.NotIntNo = newNotice.NotIntNo;
                            newDriver.DrvSurname = rep.AaRepNewInfSurname;
                            newDriver.DrvInitials = rep.AaRepNewInfInitials;
                            newDriver.DrvIdType = rep.AaRepNewInfIdTypeId.ToString();
                            newDriver.DrvIdNumber = rep.AaRepNewInfIdNo;
                            newDriver.DrvAge = (DateTime.Now.Year - ((DateTime)rep.AaRepNewInfDob).Year).ToString();
                            newDriver.DrvPoAdd1 = rep.AaRepNewInfPosAdd1;
                            newDriver.DrvPoAdd2 = rep.AaRepNewInfPosAdd2;
                            newDriver.DrvPoCode = rep.AaRepNewInfPostalCode;
                            newDriver.DrvStAdd1 = rep.AaRepNewInfStrAdd1;
                            newDriver.DrvStAdd2 = rep.AaRepNewInfStrAdd2;
                            newDriver.DrvLicenceCode = rep.AaRepNewInfLiCodeId.ToString();
                            newDriver.LastUser = model.LastUser;
                            newDriver.DrvCellNo = rep.AaRepNewInfCell;
                            newDriver.DrvHomeNo = rep.AaRepNewInfHomeTel;
                            newDriver.DrvWorkNo = rep.AaRepNewInfWorkTel;
                            newDriver.DrvFullName = rep.AaRepNewInfFirstName + rep.AaRepNewInfSurname;
                            newDriver.DrvForenames = rep.AaRepNewInfFirstName;
                            #endregion
                            new DriverService().Insert(newDriver);

                            #region searchID and searchName
                            NoticeManager.DeleteSearchIDAndNameByDriverInf(model.NotIntNo);
                            NoticeManager.AddSearchIDAndName(newNotice.NotIntNo, newDriver.DrvIdNumber, newDriver.DrvSurname, newDriver.DrvInitials);
                            #endregion

                            AddEvidencePack(Constant.NoticeAdded, newCharge.ChgIntNo, "Notice", newNotice.NotIntNo, model.LastUser, (int)EpActionTypeList.Generate);
                            #endregion

                            #region clock
                            clock.Stop();
                            #endregion
                            AARTODocumentManager.CreateAARTONoticeDocument(model.RepID, (int)AartoDocumentTypeList.AARTO05b, model.LastUser);
                            ////CancelOtherRepresentation(rep);
                        }
                        else
                        {

                            oldCharge.ChargeStatus = rep.CsCode;
                            new ChargeService().Save(oldCharge);

                            #region clock
                            clock.Stop();
                            newClock = new NominateDriverFailedFullPaymentClock(model.NotIntNo);
                            newClock.LastUser = model.LastUser;
                            newClock.RepID=model.RepID;
                            newClock.Create();
                            #endregion
                        }
                        #endregion
                        break;
                    case (int)AartoDocumentTypeList.AARTO08:
                        #region standard rep.
                        if (model.DecideResult == (int)RepresentationCodeList.AARTO_RepresentationSuccessful)
                        {

                            oldCharge.ChargeStatus = (int)ChargeStatusList.AARTO_Cancelled_Representation;//
                            oldCharge.LastUser = model.LastUser;
                            new ChargeService().Save(oldCharge);

                            #region cancel current notice
                            //add notice cancelled links
                            NoticeCancelledLinks ncl = new NoticeCancelledLinks();
                            ncl.ChgIntNo = model.ChgIntNo;
                            ncl.NotIntNo = model.NotIntNo;
                            ncl.LastUser = model.LastUser;
                            new NoticeCancelledLinksService().Save(ncl);
                            //update aartoDocument
                            //will update the oldDoc when create the AARTO09

                            DeleteNoticeFrameLinksByNotIntNo(model.NotIntNo);

                            AddEvidencePack(Constant.NoticeCancelled, model.ChgIntNo, "Notice", model.NotIntNo, model.LastUser, (int)EpActionTypeList.Others);
                            #endregion

                            #region clock
                            clock.Stop();
                            #endregion
                            AARTODocumentManager.CreateAARTONoticeDocument(model.RepID, (int)AartoDocumentTypeList.AARTO09, model.LastUser);

                            ////CancelOtherRepresentation(rep);
                        }
                        else
                        {

                            oldCharge.ChargeStatus = rep.CsCode;
                            oldCharge.LastUser = model.LastUser;
                            new ChargeService().Save(oldCharge);

                            #region clock
                            clock.Stop();
                            newClock = new RepFailedFullPaymentClock(model.NotIntNo);
                            newClock.RepID = rep.AaRepId;
                            newClock.LastUser = model.LastUser;
                            newClock.Create();
                            #endregion
                        }
                        #endregion
                        break;
                    case (int)AartoDocumentTypeList.AARTO10:
                        #region election to be tried in court
                        oldCharge.ChargeStatus = (int)ChargeStatusList.EligibleForSummons;//
                        oldCharge.LastUser = model.LastUser;
                        new ChargeService().Save(oldCharge);

                        #region clock
                        clock.Stop();
                        newClock = new ElectCourtClock(model.NotIntNo);
                        newClock.LastUser = model.LastUser;
                        newClock.RepID=model.RepID;
                        newClock.Create();
                        #endregion

                        CancelOtherRepresentation(rep, model.LastUser);
                        #endregion
                        break;
                    case (int)AartoDocumentTypeList.AARTO14:
                        #region revocation of enforcement order
                        //update aartoDocument
                        //will update the oldDoc when create the AARTO15

                        AARTODocumentManager.CreateAARTONoticeDocument(model.RepID, (int)AartoDocumentTypeList.AARTO15, model.LastUser);
                        if (model.DecideResult == (int)RepresentationCodeList.AARTO_RepresentationSuccessful)
                        {
                            rep.AaEnfOrderRevAdjReason = "";
                            rep.AaEnfOrderRevAdjReason += model.RVReasons[0] ? "," + RVOfEOReasonsType.HavePaid : "";
                            rep.AaEnfOrderRevAdjReason += model.RVReasons[1] ? "," + RVOfEOReasonsType.HaveSubmittedA04 : "";
                            rep.AaEnfOrderRevAdjReason += model.RVReasons[2] ? "," + RVOfEOReasonsType.HaveSubmittedA07 : "";
                            rep.AaEnfOrderRevAdjReason += model.RVReasons[3] ? "," + RVOfEOReasonsType.HaveSubmittedA08 : "";
                            rep.AaEnfOrderRevAdjReason += model.RVReasons[4] ? "," + RVOfEOReasonsType.HaveSubmittedA10 : "";
                            rep.AaEnfOrderRevAdjReason += model.RVReasons[5] ? "," + RVOfEOReasonsType.HaveAppearedInCourt : "";
                            rep.AaEnfOrderRevAdjReason += model.RVReasons[6] ? "," + RVOfEOReasonsType.CannotAppearedInCourt : "";
                            rep.AaEnfOrderRevAdjReason += model.RVReasons[7] ? "," + RVOfEOReasonsType.OtherReasons : "";
                            if (rep.AaEnfOrderRevAdjReason.Length > 0) rep.AaEnfOrderRevAdjReason = rep.AaEnfOrderRevAdjReason.Substring(1);
                        }
                        #region clock
                        if (clock.AaClockTypeID == (int)AartoClockTypeList.EnforcementOrder)
                        {
                            if (model.DecideResult == (int)RepresentationCodeList.AARTO_RepresentationSuccessful)
                            {
                                new AartoRepresentationService().Save(rep);
                                if (clock.ClockStatusID == (int)AartoClockStatusList.Paused)
                                    clock.Reset();
                            }
                            else
                            {
                                if (clock.ClockStatusID == (int)AartoClockStatusList.Paused)
                                    clock.Continue();
                                FineManager.AddDocFees(model.NotIntNo, (int)AartoDocumentTypeList.AARTO15, model.LastUser);
                            }
                        }
                        else
                        {
                            new AartoRepresentationService().Save(rep);
                        }
                        #endregion
                        #endregion
                        break;
                    case -1:
                        break;
                }

                if (AARTOTM.IsOpen) AARTOTM.Commit();
            }
            catch (Exception ex)
            {
                if (AARTOTM.IsOpen) AARTOTM.Rollback();
                //ErrorLog.AddErrorLog(ex, model.LastUser);
                EntLibLogger.WriteErrorLog(ex, LogCategory.Error, model.LastUser);

                throw ex;
            }
            finally
            {
                AARTOTM.Dispose();
            }
            return true;
        }
        
        private static void UpdateNoticeFrameLinksByNotIntNo(int newNotIntNo, int oldNotIntNo)
        {
            TList<NoticeFrame> nfList = new NoticeFrameService().GetByNotIntNo(oldNotIntNo);
            foreach (NoticeFrame nf in nfList)
            {
                nf.NotIntNo = newNotIntNo;
            }
            new NoticeFrameService().Save(nfList);
        }
        private static void DeleteNoticeFrameLinksByNotIntNo(int notIntNo)
        {
            TList<NoticeFrame> nfList = new NoticeFrameService().GetByNotIntNo(notIntNo);
            new NoticeFrameService().Delete(nfList);
        }
        private static bool AreThereAnyOtherUnadjudicatedRepresentations(AartoRepresentation rep)
        {
            AartoRepresentationQuery query = new AartoRepresentationQuery();
            query.Append(AartoRepresentationColumn.RcCode, ((int)RepresentationCodeList.AARTO_RepresentationRegistered).ToString());
            query.Append(AartoRepresentationColumn.ChgIntNo, rep.ChgIntNo.ToString());
            TList<AartoRepresentation> repList = new AartoRepresentationService().Find(query as IFilterParameterCollection);
            return repList.Count > 0;
        }
        private static void CancelOtherRepresentation(AartoRepresentation rep,string lastUser)
        {
            TList<AartoRepresentation> repList = new AartoRepresentationService().GetByChgIntNo(rep.ChgIntNo);
            foreach (AartoRepresentation tempRep in repList)
            {
                if (tempRep.AaRepId != rep.AaRepId && tempRep.RcCode == (int)RepresentationCodeList.AARTO_RepresentationRegistered)
                {
                    tempRep.RcCode = (int)RepresentationCodeList.RepresentationCancelled;
                    tempRep.LastUser = lastUser;
                    tempRep.AaRepCanceledReason = Constant.AlreadyAdjudicated;
                    new AartoRepresentationService().Save(tempRep);
                }
            }
        }
        private static void CreateResultForRepDecide(int repID, int docTypeID,string lastUser)
        {
            AartoRepDocument repDoc = new AartoRepDocument();
            repDoc.AaRepId = repID;
            repDoc.AaDocTypeId = docTypeID;
            repDoc.AaDocStatusId = (int)AartoDocumentStatusList.New;
            repDoc.LastUser = lastUser;
            new AartoRepDocumentService().Insert(repDoc);
        }

        /// <summary>
        /// Insert EvidencePack
        /// Jerry 2013-10-15 add epatIntNo
        /// </summary>
        /// <param name="ePItemDescr"></param>
        /// <param name="chgIntNo"></param>
        /// <param name="ePSourceTable"></param>
        /// <param name="ePSourceID"></param>
        /// <param name="lastUser"></param>
        /// <param name="epatIntNo"></param>
        /// <returns></returns>
        public static bool AddEvidencePack(string ePItemDescr, int chgIntNo, string ePSourceTable, int ePSourceID, string lastUser, int epatIntNo)
        {
            EvidencePack ep = new EvidencePack();
            ep.ChgIntNo = chgIntNo;
            ep.EpItemDate = DateTime.Now;
            ep.EpItemDescr = ePItemDescr;
            ep.EpSourceTable = ePSourceTable;
            ep.EpSourceId = ePSourceID;
            ep.EpItemDateUpdated = true;
            ep.LastUser = lastUser;
            ep.EpatIntNo = epatIntNo;//Jerry 2013-10-15 add
            return new EvidencePackService().Insert(ep);
        }
        public static TList<EvidencePack> GetEvidencePackListByChgIntNo(int chgIntNo)
        {
            return new EvidencePackService().GetByChgIntNo(chgIntNo);
        }
    }
}
