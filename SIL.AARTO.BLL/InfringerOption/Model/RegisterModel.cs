﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.BLL.Utility.DMS;
namespace SIL.AARTO.BLL.InfringerOption.Model
{
    public class RegisterModel
    {
        public BatchDocumentEntity BatchDocumentEntity
        {
            get;
            set;
        }
        public string ScriptOfCreateDocumentFields
        {
            get;
            set;
        }
        public string ScriptOfCreateDocumentImages
        {
            get;
            set;
        }
    }
}
