﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

using SIL.AARTO.DAL.Entities;
namespace SIL.AARTO.BLL.InfringerOption.Model
{
    public class DecisionModel
    {
        public string NoticeTicketNumber { get; set; }
        public DateTime InfringementDate { get; set; }
        public string IssuingAuthority { get; set; }
        public AartoRepresentation Rep { get; set; }
        public int RepID { get; set; }
        public int A14RepID { get; set; }
        public int ChgIntNo { get; set; }
        public int NotIntNo { get; set; }
        public int AutIntNo { get; set; }
        public int AaDocTypeID { get; set; }
        public int AaClockTypeID { get; set; }
        public string EvidencePack { get; set; }

        public List<bool> RVReasons { get; set; }

        public string DocumentImageUrlList { get; set; }

        public SelectList OrganisationTypeList { get; set; }
        public SelectList IDTypeList { get; set; }
        public SelectList LicenceCodeList { get; set; }
        public SelectList LearnersCodeList { get; set; }
        public SelectList PrDPCodeList { get; set; }

        public SelectList VehicleMakeList { get; set; }
        public SelectList VehicleTypeList { get; set; }
        public SelectList VehicleColourList { get; set; }

        public SelectList TypeOfAccountList { get; set; }

        public SelectList CourtList { get; set; }
        public int DecideResult { get; set; }
        public string LastUser { get; set; }
        public bool PostCodeIsValid { get; set; }
        public DecisionModel()
        {
        }
    }
}
