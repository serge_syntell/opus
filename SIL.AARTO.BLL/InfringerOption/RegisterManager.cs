﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

using SIL.AARTO.BLL.InfringerOption.Model;
using SIL.AARTO.BLL.Utility.DMS;
using SIL.DMS.DAL.Entities;
namespace SIL.AARTO.BLL.InfringerOption
{
    public class RegisterManager
    {
        public static RegisterModel GetRegisterModel()
        {
            RegisterModel registerModel = new RegisterModel();
            registerModel.BatchDocumentEntity = DMSManager.GetTopOneBatchDocumentInfomationByTemplateTypeAndStatus(1,10);
            StringBuilder bdiStr = new StringBuilder();
            //documentImages.push(new DocumentImage(url, orderNumber));
            foreach (BatchDocumentImage bdi in registerModel.BatchDocumentEntity.BatchDocumentImageList)
            {
                bdiStr.Append("documentImages.push(");
                bdiStr.Append("new DocumentImage('");
                bdiStr.Append(ConfigurationManager.AppSettings["AARTODocumentURL"] + "/"+bdi.BaDoImFilePath);//"http://localhost:49586/"
                bdiStr.Append("',");
                bdiStr.Append(bdi.BaDoImOrder);
                bdiStr.Append("));");
                bdiStr.Append("\r\n");
            }
            registerModel.ScriptOfCreateDocumentImages = bdiStr.ToString();

            //documentFields.push(new DocumentField(field, xPos, yPos, width, height, pageNumber));
            StringBuilder dtdStr = new StringBuilder();
            foreach (DocumentTemplateDetail dtd in registerModel.BatchDocumentEntity.DocumentTemplateDetailList)
            {
                dtdStr.Append("documentFields.push(");
                dtdStr.Append("new DocumentField('");
                dtdStr.Append(dtd.DoTeDeFieldName);
                dtdStr.Append("',");
                dtdStr.Append(dtd.DoTeDeRegionFromx);
                dtdStr.Append(",");
                dtdStr.Append(dtd.DoTeDeRegionFromy);
                dtdStr.Append(",");
                dtdStr.Append(dtd.DoTeDeRegionWidth);
                dtdStr.Append(",");
                dtdStr.Append(dtd.DoTeDeRegionHeight);
                dtdStr.Append(",");
                dtdStr.Append(dtd.DoTeDePageNumber);
                dtdStr.Append("));");
                dtdStr.Append("\r\n");
            }
            registerModel.ScriptOfCreateDocumentFields = dtdStr.ToString();
            return registerModel;
        }
    }
}
