﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.BLL.AARTONoticeClock;
using SIL.AARTO.BLL.Utility;
using System.IO;
using SIL.AARTO.BLL.Model;
namespace SIL.AARTO.BLL.InfringerOption
{
    public class AARTODocumentManager
    {
        public static void CreateAARTONoticeDocument(int id, int docTypeID, string lastUser)
        {
            AartoDocumentSourceTableList tableSource = AARTODocumentSourceTable.IsForNotice(docTypeID);
            if (tableSource == AartoDocumentSourceTableList.AARTONoticeDocument)
            {
                AartoNoticeDocument nDoc = new AartoNoticeDocument();
                nDoc.AaDocStatusId = (int)AartoDocumentStatusList.New;
                nDoc.AaDocTypeId = docTypeID;
                nDoc.NotIntNo = id;
                nDoc.LastUser = lastUser;
                nDoc.AaDocCreatedDate = DateTime.Now;
                new AartoNoticeDocumentService().Insert(nDoc);
                switch (docTypeID)
                {
                    case (int)AartoDocumentTypeList.AARTO12:
                        NoticeManager.UpdateChargeStatus(ChargeStatusList.AARTO_GenerateCourtesyLetter_12, id, lastUser);
                        break;
                    case (int)AartoDocumentTypeList.AARTO13:
                        NoticeManager.UpdateChargeStatus(ChargeStatusList.AARTO_GenerateEnforcementOrder_13, id, lastUser);
                        break;
                    case (int)AartoDocumentTypeList.AARTO24:
                        NoticeManager.UpdateChargeStatus(ChargeStatusList.AARTO_GenerateWarrant_24, id, lastUser);
                        break;
                    default: break;
                }
            }
            else if (tableSource == AartoDocumentSourceTableList.AARTORepDocument)
            {
                AartoRepDocument doc = new AartoRepDocument();
                doc.AaDocStatusId = (int)AartoDocumentStatusList.New;
                doc.AaDocTypeId = docTypeID;
                doc.AaRepId = id;
                doc.LastUser = lastUser;
                doc.AaRepDocCreatedDate = DateTime.Now;
                new AartoRepDocumentService().Save(doc);
                AartoRepDocument repDoc = null;
                switch (docTypeID)
                {
                    case (int)AartoDocumentTypeList.AARTO05b:
                        repDoc = new AartoRepDocumentService().GetByAaRepIdAaDocTypeId(id, (int)AartoDocumentTypeList.AARTO07);
                        break;
                    case (int)AartoDocumentTypeList.AARTO05d:
                        repDoc = new AartoRepDocumentService().GetByAaRepIdAaDocTypeId(id, (int)AartoDocumentTypeList.AARTO10);
                        break;
                    case (int)AartoDocumentTypeList.AARTO06:
                        repDoc = new AartoRepDocumentService().GetByAaRepIdAaDocTypeId(id, (int)AartoDocumentTypeList.AARTO04);
                        break;
                    case (int)AartoDocumentTypeList.AARTO09:
                        repDoc = new AartoRepDocumentService().GetByAaRepIdAaDocTypeId(id, (int)AartoDocumentTypeList.AARTO08);
                        break;
                    case (int)AartoDocumentTypeList.AARTO15:
                        repDoc = new AartoRepDocumentService().GetByAaRepIdAaDocTypeId(id, (int)AartoDocumentTypeList.AARTO14);
                        break;
                    default: break;
                }
                if (repDoc != null)
                {
                    repDoc.AaRepDocAdjudicatedDate = DateTime.Now;
                    repDoc.LastUser = lastUser;
                    repDoc.AaDocStatusId = (int)AartoDocumentStatusList.Adjudicated;
                    new AartoRepDocumentService().Save(repDoc);
                }
            }
        }
        public static void UpdateAARTODocumentAfterCreateBatch(long docID, int batchID, int docTypeID, string lastUser)
        {
            AartoDocumentSourceTableList tableSource = AARTODocumentSourceTable.IsForNotice(docTypeID);
            if (tableSource == AartoDocumentSourceTableList.AARTONoticeDocument)
            {
                AartoNoticeDocument doc = new AartoNoticeDocumentService().GetByAaNotDocId(docID);
                doc.AaDocStatusId = (int)AartoDocumentStatusList.Issued;
                doc.AaDocIssuedDate = DateTime.Now;
                doc.AaDocBatchId = batchID;
                doc.LastUser = lastUser;
                new AartoNoticeDocumentService().Save(doc);
            }
            else if (tableSource == AartoDocumentSourceTableList.AARTORepDocument)
            {
                AartoRepDocument doc = new AartoRepDocumentService().GetByAaRepDocId(docID);
                doc.AaDocStatusId = (int)AartoDocumentStatusList.Issued;
                doc.AaRepDocIssuedDate = DateTime.Now;
                doc.AaDocBatchId = batchID;
                doc.LastUser = lastUser;
                new AartoRepDocumentService().Save(doc);
            }
        }
        public static void UpdateAARTODocumentAfterGenerated(long docID, int docTypeID, int ifsIntNo, string docPath, string lastUser)
        {
            AartoDocumentSourceTableList tableSource = AARTODocumentSourceTable.IsForNotice(docTypeID);
            if (tableSource == AartoDocumentSourceTableList.AARTONoticeDocument)
            {
                AartoNoticeDocument doc = new AartoNoticeDocumentService().GetByAaNotDocId(docID);
                doc.IfsIntNo = ifsIntNo;
                doc.AaNotDocPath = docPath;
                doc.LastUser = lastUser;
                new AartoNoticeDocumentService().Save(doc);
            }
            else if (tableSource == AartoDocumentSourceTableList.AARTORepDocument)
            {
                AartoRepDocument doc = new AartoRepDocumentService().GetByAaRepDocId(docID);
                doc.IfsIntNo = ifsIntNo;
                doc.AaRepDocPath = docPath;
                doc.LastUser = lastUser;
                new AartoRepDocumentService().Save(doc);
            }
        }

        public static void UpdateAARTODocumentAfterGenerateFailed(long docID, int docTypeID, string lastUser)
        {
            AartoDocumentSourceTableList tableSource = AARTODocumentSourceTable.IsForNotice(docTypeID);
            if (tableSource == AartoDocumentSourceTableList.AARTONoticeDocument)
            {
                AartoNoticeDocument doc = new AartoNoticeDocumentService().GetByAaNotDocId(docID);
                doc.AaDocBatchId = null;
                doc.AaDocStatusId = (int)AartoDocumentStatusList.GenerateFailed;
                doc.LastUser = lastUser;
                new AartoNoticeDocumentService().Save(doc);
            }
            else if (tableSource == AartoDocumentSourceTableList.AARTORepDocument)
            {
                AartoRepDocument doc = new AartoRepDocumentService().GetByAaRepDocId(docID);
                doc.AaDocBatchId = null;
                doc.AaDocStatusId = (int)AartoDocumentStatusList.GenerateFailed;
                doc.LastUser = lastUser;
                new AartoRepDocumentService().Save(doc);
            }
        }

        public static void UpdateAARTODocumentAfterPrinted(long docID, int docTypeID, string lastUser)
        {
            AartoDocumentSourceTableList tableSource = AARTODocumentSourceTable.IsForNotice(docTypeID);
            int ifsIntNo;
            string docPath;
            if (tableSource == AartoDocumentSourceTableList.AARTONoticeDocument)
            {
                AartoNoticeDocument doc = new AartoNoticeDocumentService().GetByAaNotDocId(docID);
                doc.AaDocStatusId = (int)AartoDocumentStatusList.Printed;
                doc.AaDocPrintedDate = DateTime.Now;
                ifsIntNo = doc.IfsIntNo.Value;
                docPath = doc.AaNotDocPath;
                doc.AaNotDocPath = doc.AaNotDocPath.Replace("Issued", "Printed");
                new AartoNoticeDocumentService().Save(doc);
                switch (docTypeID)
                {
                    case (int)AartoDocumentTypeList.AARTO03:
                        NoticeManager.UpdateChargeStatus(ChargeStatusList.FirstNoticePrinted, doc.NotIntNo, lastUser);
                        break;
                    case (int)AartoDocumentTypeList.AARTO12:
                        NoticeManager.UpdateChargeStatus(ChargeStatusList.AARTO_PrintCourtesyLetter_12, doc.NotIntNo, lastUser);
                        break;
                    case (int)AartoDocumentTypeList.AARTO13:
                        NoticeManager.UpdateChargeStatus(ChargeStatusList.AARTO_PrintEnforcementOrder_13, doc.NotIntNo, lastUser);
                        break;
                    case (int)AartoDocumentTypeList.AARTO24:
                        NoticeManager.UpdateChargeStatus(ChargeStatusList.AARTO_PrintWarrant_24, doc.NotIntNo, lastUser);
                        break;
                    default:
                        break;
                }
            }
            else //if (tableSource == AartoDocumentSourceTableList.AARTORepDocument)
            {
                AartoRepDocument doc = new AartoRepDocumentService().GetByAaRepDocId(docID);
                doc.AaDocStatusId = (int)AartoDocumentStatusList.Printed;
                doc.AaRepDocPrintedDate = DateTime.Now;
                ifsIntNo = doc.IfsIntNo.Value;
                docPath = doc.AaRepDocPath;
                doc.AaRepDocPath = doc.AaRepDocPath.Replace("Issued", "Printed");
                new AartoRepDocumentService().Save(doc);
            }
            MoveIssuedFile2Printed(ifsIntNo, docPath);

        }
        public static void UpdateAARTODocumentAfterPosted(long docID, int docTypeID, string lastUser)
        {
            Clock clock = null;
            AartoDocumentSourceTableList tableSource = AARTODocumentSourceTable.IsForNotice(docTypeID);
            int ifsIntNo;
            string docPath;
            if (tableSource == AartoDocumentSourceTableList.AARTONoticeDocument)
            {
                AartoNoticeDocument doc = new AartoNoticeDocumentService().GetByAaNotDocId(docID);
                ifsIntNo = doc.IfsIntNo.Value;
                docPath = doc.AaNotDocPath;
                doc.AaNotDocPath = doc.AaNotDocPath.Replace("Printed", "Posted");
                doc.AaDocStatusId = (int)AartoDocumentStatusList.Posted;
                doc.AaDocPostedDate = DateTime.Now;
                new AartoNoticeDocumentService().Save(doc);
                clock = ClockManager.GetClockByNoticeID(doc.NotIntNo);
                clock.LastUser = lastUser;
                switch (clock.AaClockTypeID)
                {
                    case (int)AartoClockTypeList.NoticeToBePostedA03:
                        if (docTypeID == (int)AartoDocumentTypeList.AARTO03)
                            clock.Stop();
                        break;
                    case (int)AartoClockTypeList.CourtesyLetter:
                        if (docTypeID == (int)AartoDocumentTypeList.AARTO12)
                            clock.Start();
                        break;
                    case (int)AartoClockTypeList.EnforcementOrder:
                        if (docTypeID == (int)AartoDocumentTypeList.AARTO13)
                            clock.Start();
                        break;
                    //case (int)AartoClockTypeList.WarrantOfAttachment://serverd
                    //    if (docTypeID == (int)AartoDocumentTypeList.AARTO24)
                    //        clock.Start();
                    //    break;
                    case (int)AartoClockTypeList.DishonouredInstallment:
                        if (docTypeID == (int)AartoDocumentTypeList.AARTO16)
                            clock.Start();
                        break;
                    case (int)AartoClockTypeList.PartialPayment:
                        if (docTypeID == (int)AartoDocumentTypeList.AARTO17)
                            clock.Start();
                        break;
                    case (int)AartoClockTypeList.DishonouredPayment:
                        if (docTypeID == (int)AartoDocumentTypeList.AARTO18)
                            clock.Start();
                        break;
                    default: break;
                }
                switch (docTypeID)
                {
                    case (int)AartoDocumentTypeList.AARTO03:
                        NoticeManager.UpdateChargeStatus(ChargeStatusList.FirstNoticePosted, doc.NotIntNo, lastUser);
                        break;
                    case (int)AartoDocumentTypeList.AARTO12:
                        NoticeManager.UpdateChargeStatus(ChargeStatusList.AARTO_PostCourtesyLetter_12, doc.NotIntNo, lastUser);
                        break;
                    case (int)AartoDocumentTypeList.AARTO13:
                        NoticeManager.UpdateChargeStatus(ChargeStatusList.AARTO_PostEnforcementOrder_13, doc.NotIntNo, lastUser);
                        break;
                    case (int)AartoDocumentTypeList.AARTO24:
                        NoticeManager.UpdateChargeStatus(ChargeStatusList.AARTO_PostWarrant_24, doc.NotIntNo, lastUser);
                        break;
                    default: break;
                }
            }
            else //if (tableSource == AartoDocumentSourceTableList.AARTORepDocument)
            {
                AartoRepDocument doc = new AartoRepDocumentService().GetByAaRepDocId(docID);
                ifsIntNo = doc.IfsIntNo.Value;
                docPath = doc.AaRepDocPath;
                doc.AaRepDocPath = doc.AaRepDocPath.Replace("Printed", "Posted");
                doc.AaDocStatusId = (int)AartoDocumentStatusList.Posted;
                doc.AaRepDocPostedDate = DateTime.Now;
                new AartoRepDocumentService().Save(doc);
                clock = ClockManager.GetClockByRepID((int)doc.AaRepId);
                clock.LastUser = lastUser;
                AartoRepresentation rep = new AartoRepresentationService().GetByAaRepId((int)doc.AaRepId);
                switch (clock.AaClockTypeID)
                {
                    case (int)AartoClockTypeList.RepFailedFullPayment:
                        if (docTypeID == (int)AartoDocumentTypeList.AARTO09 && (rep.RcCode == (int)RepresentationCodeList.AARTO_RepresentationUnsuccessful || rep.RcCode == (int)RepresentationCodeList.AARTO_RepresentationAdviseElectCourt))
                            clock.Start();
                        break;
                    case (int)AartoClockTypeList.NominateDriverFailedFullPayment:
                        if (docTypeID == (int)AartoDocumentTypeList.AARTO05b && rep.RcCode == (int)RepresentationCodeList.AARTO_RepresentationUnsuccessful)
                            clock.Start();
                        break;
                    case (int)AartoClockTypeList.RequestIntallmentsRejectedFullPayment:
                    case (int)AartoClockTypeList.InstallmentPayment:
                        if (docTypeID == (int)AartoDocumentTypeList.AARTO06)
                            clock.Start();
                        break;
                    case (int)AartoClockTypeList.ElectCourt:
                        if (docTypeID == (int)AartoDocumentTypeList.AARTO05d && rep.RcCode == (int)RepresentationCodeList.AARTO_RepresentationSuccessful)
                            clock.Start();
                        break;
                    default: break;
                }
            }
            MoveIssuedFile2Printed(ifsIntNo, docPath);
        }
        public static void UpdateAARTODocumentAfterAdjudicated(long docID, int docTypeID, string lastUser)
        {
            AartoDocumentSourceTableList tableSource = AARTODocumentSourceTable.IsForNotice(docTypeID);
            if (tableSource == AartoDocumentSourceTableList.AARTORepDocument)
            {
                AartoRepDocument doc = new AartoRepDocumentService().GetByAaRepDocId(docID);
                doc.AaDocStatusId = (int)AartoDocumentStatusList.Adjudicated;
                doc.AaRepDocAdjudicatedDate = DateTime.Now;
                new AartoRepDocumentService().Save(doc);
            }
        }

        public static void UpdateAARTODocumentBatchAfterIssued(int docBatchID, string docBatchPath, int ifsIntNo, string lastUser)
        {
            AartoDocBatch docBatch = new AartoDocBatchService().GetByAaDocBatchId(docBatchID);
            docBatch.AaDocStatusId = (int)AartoDocumentStatusList.Issued;
            docBatch.AaDocIssuedDate = DateTime.Now;
            docBatch.AaDocBatchPath = docBatchPath;
            docBatch.IfsIntNo = ifsIntNo;
            docBatch.LastUser = lastUser;
            new AartoDocBatchService().Save(docBatch);
        }
        private static string MoveIssuedFile2Printed(int ifsIntNo, string issuedPath)
        {
            string issuedStrForReplace = "Issued";
            string printedStrForReplace = "Printed";
            if (issuedPath.Contains(issuedStrForReplace))
            {
                string fullFromPath = AARTODocBatchHelper.GetDocxFilePath(ifsIntNo, issuedPath);
                string printedPath = fullFromPath.Replace(issuedStrForReplace, printedStrForReplace);

                if (File.Exists(fullFromPath))
                {
                    FileInfo fi = new FileInfo(printedPath);
                    if (!fi.Directory.Exists)
                    {
                        fi.Directory.Create();
                    }
                    File.Move(fullFromPath, printedPath);
                    return printedPath;
                }
            }
            return null;
        }
        private static string MovePrintedFile2Posted(int ifsIntNo, string printedPath)
        {
            string printedStrForReplace = "Printed";
            string postedStrForReplace = "Posted";
            if (printedPath.Contains(printedStrForReplace))
            {
                string fullFromPath = AARTODocBatchHelper.GetDocxFilePath(ifsIntNo, printedPath);
                string toPath = fullFromPath.Replace(printedStrForReplace, postedStrForReplace);

                if (File.Exists(fullFromPath))
                {
                    FileInfo fi = new FileInfo(toPath);
                    if (!fi.Directory.Exists)
                    {
                        fi.Directory.Create();
                    }
                    File.Move(fullFromPath, toPath);
                    return toPath;
                }
            }
            return null;
        }
        public static void UpdatePebsFromIssued2Printed(AartoDocBatch batch, DateTime printedTime, string lastUser)
        {
            string fullBatchDocPath = AARTODocBatchHelper.GetDocxFilePath(batch.IfsIntNo.Value, batch.AaDocBatchPath);
            string ext = new FileInfo(fullBatchDocPath).Extension;
            string pebsPath = batch.AaDocBatchPath.Replace(ext, ".pebs");
            string fullPebsPath = fullBatchDocPath.Substring(0
                , fullBatchDocPath.Length - ext.Length) + ".pebs";
            if (File.Exists(fullPebsPath))
            {
                PebsBatch pebsObj = ObjectSerializer.ReadXmlFileToObject<PebsBatch>(fullPebsPath);
                pebsObj.Status = "Printed";
                pebsObj.Print = new PebsPrint();
                pebsObj.Print.IsPrinted = true;
                pebsObj.Print.PrintedTimeValue = printedTime;
                pebsObj.Print.PrintedBy = lastUser;
                ObjectSerializer.SaveObjectToXmlFile(pebsObj, fullPebsPath);
            }
            MoveIssuedFile2Printed(batch.IfsIntNo.Value, pebsPath);
        }
        public static void UpdatePebsFromPrinted2Posted(AartoDocBatch batch, DateTime time, string lastUser)
        {
            string fullBatchDocPath = AARTODocBatchHelper.GetDocxFilePath(batch.IfsIntNo.Value, batch.AaDocBatchPath);
            string ext = new FileInfo(fullBatchDocPath).Extension;
            string pebsPath = batch.AaDocBatchPath.Replace(ext, ".pebs");
            string fullPebsPath = fullBatchDocPath.Substring(0
                , fullBatchDocPath.Length - ext.Length) + ".pebs";
            if (File.Exists(fullPebsPath))
            {
                PebsBatch pebsObj = ObjectSerializer.ReadXmlFileToObject<PebsBatch>(fullPebsPath);
                pebsObj.Status = "Posted";
                pebsObj.Post = new PebsPost();
                pebsObj.Post.IsPosted = true;
                pebsObj.Post.PostedTimeValue = time;
                pebsObj.Post.PostedBy = lastUser;
                ObjectSerializer.SaveObjectToXmlFile(pebsObj, fullPebsPath);
            }
            MovePrintedFile2Posted(batch.IfsIntNo.Value, pebsPath);
        }
        public static void UpdateAARTODocumentBatchAfterPrinted(int batchId, string lastUser)
        {
            AartoDocBatchService aService = new AartoDocBatchService();
            AartoDocBatch batch = aService.GetByAaDocBatchId(batchId);
            DateTime nowTime = DateTime.Now;

            // Alter & Move Pebs File
            UpdatePebsFromIssued2Printed(batch, nowTime, lastUser);

            // Move Batch File
            MoveIssuedFile2Printed(batch.IfsIntNo.Value, batch.AaDocBatchPath);

            // Alter Batch Record
            batch.AaDocStatusId = (int)AartoDocumentStatusList.Printed;
            batch.AaDocPrintedDate = nowTime;
            batch.AaDocPrintUser = lastUser;
            batch.LastUser = lastUser;
            batch.AaDocBatchPath = batch.AaDocBatchPath.Replace("Issued", "Printed");
            aService.Update(batch);

            // Alter Doc Record & Move Doc file
            int docTypeID = batch.AaDocTypeId;
            AartoDocumentSourceTableList tableSource = AARTODocumentSourceTable.IsForNotice(docTypeID);
            if (tableSource == AartoDocumentSourceTableList.AARTONoticeDocument)
            {
                AartoNoticeDocumentService nService = new AartoNoticeDocumentService();
                var nDocList = nService.GetByAaDocBatchId(batchId);
                for (int i = 0; i < nDocList.Count(); i++)
                {
                    AartoNoticeDocument nDoc = nDocList[i];
                    UpdateAARTODocumentAfterPrinted(nDoc.AaNotDocId, docTypeID, lastUser);
                }
            }
            else
            {
                AartoRepDocumentService rService = new AartoRepDocumentService();
                var rDocList = rService.GetByAaDocBatchId(batchId);
                for (int i = 0; i < rDocList.Count(); i++)
                {
                    AartoRepDocument rDoc = rDocList[i];
                    UpdateAARTODocumentAfterPrinted(rDoc.AaRepDocId, docTypeID, lastUser);
                }
            }
        }
        public static void UpdateAARTODocumentBatchAfterPosted(int batchId, DateTime dt, string lastUser)
        {
            AartoDocBatchService aService = new AartoDocBatchService();
            AartoDocBatch batch = aService.GetByAaDocBatchId(batchId);

            // Alter & Move Pebs File
            UpdatePebsFromPrinted2Posted(batch, dt, lastUser);

            // Move Batch File
            MovePrintedFile2Posted(batch.IfsIntNo.Value, batch.AaDocBatchPath);

            // Alter Batch Record
            batch.AaDocStatusId = (int)AartoDocumentStatusList.Posted;
            batch.AaDocPostedDate = dt;
            batch.AaDocPostedUser = lastUser;
            batch.LastUser = lastUser;
            aService.Save(batch);

            // Alter Doc Record & Move Doc file
            int docTypeID = batch.AaDocTypeId;
            AartoDocumentSourceTableList tableSource = AARTODocumentSourceTable.IsForNotice(docTypeID);
            if (tableSource == AartoDocumentSourceTableList.AARTONoticeDocument)
            {
                AartoNoticeDocumentService nService = new AartoNoticeDocumentService();
                var nDocList = nService.GetByAaDocBatchId(batchId);
                for (int i = 0; i < nDocList.Count(); i++)
                {
                    AartoNoticeDocument nDoc = nDocList[i];
                    UpdateAARTODocumentAfterPosted(nDoc.AaNotDocId, docTypeID, lastUser);
                }
            }
            else
            {
                AartoRepDocumentService rService = new AartoRepDocumentService();
                var rDocList = rService.GetByAaDocBatchId(batchId);
                for (int i = 0; i < rDocList.Count(); i++)
                {
                    AartoRepDocument rDoc = rDocList[i];
                    UpdateAARTODocumentAfterPosted(rDoc.AaRepDocId, docTypeID, lastUser);
                }
            }
        }

        public static TList<AartoDocumentImage> GetAARTODocumentImageListByAaDocIDAndAaDocSourceTableID(int aaDocID, int aaSourceTabelID)
        {
            TList<AartoDocumentImage> imgList = new TList<AartoDocumentImage>();
            imgList = new AartoDocumentImageService().GetAARTODocumentImageListByAaDocIDAndAaDocSourceTableID(aaDocID, aaSourceTabelID);
            //AartoDocumentImage img = null;
            //IDataReader idr = new AartoDocumentImageService().GetAARTODocumentImageListByAaDocIDAndAaDocSourceTableID(aaDocID, aaSourceTabelID);
            //while (idr.Read())
            //{
            //    img = new AartoDocumentImage();
            //    img.AaDocId = aaDocID;
            //    img.AaDocImgId = Convert.ToInt32(idr["AaDocImgId"]);
            //    img.AaDocImgOrder = idr["AaDocImgOrder"] == DBNull.Value ? -1 : Convert.ToInt32(idr["AaDocImgOrder"]);
            //    img.AaDocImgPath = idr["AaDocImgPath"].ToString();
            //    img.AaDocSourceTableId = aaSourceTabelID;
            //    img.AaDocImgDateLoaded = idr["AaDocImgDateLoaded"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(idr["AaDocImgDateLoaded"]);
            //    imgList.Add(img);
            //}
            //idr.Close();
            return imgList;
        }

        public static void DeleteAARTODocumentBatchForNoDocs(int docBatchID)
        {
            AartoDocBatchService service = new AartoDocBatchService();
            AartoDocBatch docBatch = service.GetByAaDocBatchId(docBatchID);
            service.Delete(docBatch);            
        }
    }
}
