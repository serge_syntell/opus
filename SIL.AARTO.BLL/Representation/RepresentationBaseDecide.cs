﻿using System.Linq;
using SIL.AARTO.BLL.Representation.Model;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource.Representation;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Representation
{
    public abstract partial class RepresentationBase
    {
        #region Actions

        public MessageResult Decide(ref RepresentationModel model)
        {
            PunchStatisticsTranTypeList? pstType;
            int chargeStatus;
            if (!CheckModel(ref model)
                || !CheckModelRowVersion(ref model)
                || IsHandwrittenCorrectionOrPaid(model.ChgIntNo, model.IsSummons, model)
                || !model.ValidateDecision(Rules, out pstType, out chargeStatus))
                return model;

            var rep = model;
            Notice notice = null;
            if (!TransactionScope(() =>
            {
                if (rep.RepIntNo == 0 && RepAction.Equals(RepAction.O))
                {
                    //rep.IsChangeOfOffender = true;
                    rep.RepType = RepType.A;
                    rep.Charge.ChargeStatus = chargeStatus;

                    //var rep2 = new RepresentationModel
                    //{
                    //    AutIntNo = rep.AutIntNo,
                    //    IsSummons = rep.IsSummons,
                    //    ChgIntNo = rep.ChgIntNo,
                    //    RepOffenderName = rep.RepOffenderName,
                    //    RepOffenderAddress = rep.RepOffenderAddress,
                    //    RepCurrentFineAmount = rep.RepCurrentFineAmount,
                    //    IsChangeOfOffender = true,
                    //    RepOfficial = rep.RepOfficial,
                    //    RepType = RepType.A,
                    //    Charge =
                    //    {
                    //        //ChargeStatus = !rep.IsSummons ?
                    //        //    CODE_REP_LOGGED_CHARGE : CODE_REP_LOGGED_SUMMONS
                    //        ChargeStatus = chargeStatus
                    //    },
                    //};

                    //if (!NewUpdateRepresentation(ref rep2))
                    //{
                    //    rep.RefCopy(rep2.GetMessageResult());
                    //    return false;
                    //}

                    //rep.RepIntNo = rep2.RepIntNo;
                    //rep.Charge.RowVersion_Long = GetChargeRowVersion(rep.IsSummons, rep.ChgIntNo);
                }

                if (!RepresentationDecide(ref rep))
                    return false;

                // implement Heidi's "for all Punch Statistics Transaction(5084)"
                if (pstType.HasValue
                    && this.punch.PunchStatisticsTransactionAdd(rep.AutIntNo, LastUser, pstType.Value, PunchAction.Add) <= 0)
                {
                    rep.AddError(this.punch.ErrorMessage);
                    return false;
                }

                bool pushGenerateSummons;
                notice = HandleTargetCSCode(ref rep, out pushGenerateSummons);
                if (notice.NotIntNo <= 0) return false;

                if (!PushQueue_SearchNameIDUpdate(rep.Notice.NotIntNo, rep)) return false;

                if ((rep.IsChangeOfOffender && !rep.IsChangeOfRegNo)
                    || (rep.IsChangeOfRegNo && !rep.NewRegNoDetailsChecked))
                {
                    // update NoticeStatus to TargetCSCode for NoticeStatus=410
                    if (notice.NoticeStatus.Equals2(CODE_REP_LOGGED_CHARGE))
                    {
                        var tmp = new RepresentationModel();
                        tmp.Notice.NoticeStatus = notice.NotPrevNoticeStatus.GetValueOrDefault(notice.NoticeStatus);
                        if (!SaveTargetCSCode(ref tmp, t => true))
                            return false;

                        notice.NoticeStatus = tmp.TargetCSCode.GetValueOrDefault(notice.NotPrevNoticeStatus.GetValueOrDefault(notice.NoticeStatus));
                        if (!tmp.Notice.NoticeStatus.Equals2(notice.NoticeStatus))
                        {
                            this.noticeService.Save(notice);

                            foreach (var chg in this.chargeService.GetByNotIntNo(notice.NotIntNo)
                                .Where(c => c.ChargeStatus < Code_Completion && !c.ChargeStatus.Equals2(notice.NoticeStatus)))
                            {
                                chg.ChargeStatus = notice.NoticeStatus;
                                this.chargeService.Save(chg);
                            }
                        }
                    }

                    // 2015-01-20, Oscar added (bontq 1797, ref 1796)
                    if (rep.IsChangeOfOffender
                        && !string.IsNullOrWhiteSpace(notice.NewOffenderPrintFileName))
                    {
                        notice.NewOffenderPrintFileName = null;
                        notice.LastUser = LastUser;
                        this.noticeService.Save(notice);
                    }
                    else if (rep.IsChangeOfRegNo
                        && rep.NewVMIntNo > 0 && rep.NewVTIntNo > 0
                        && !string.IsNullOrWhiteSpace(notice.NewRegNoPrintFileName))
                    {
                        notice.NewRegNoPrintFileName = null;
                        notice.LastUser = LastUser;
                        this.noticeService.Save(notice);
                    }

                    if (!PushQueue_ProcessNewOffenderNotices(rep.Notice.NotIntNo, rep.AutCode, rep.Notice.IsNoAOG || rep.Notice.IsSection35, rep))
                        return false;
                }

                // COO and COR no need to push GenerateSummons, because service ProcessNewOffenderNotices will do that.
                if (!rep.IsChangeOfOffender && !rep.IsChangeOfRegNo)
                {
                    if (rep.IsSummons && rep.RepresentationCode.In(SUMMONS_REPCODE_REISSUE, SUMMONS_REPCODE_ADDRESS)
                        && (!PushQueue_GenerateSummons(rep.Notice.NotIntNo, rep.AutCode, DateTimeNow.AddDays(GetDelayActionDate_InHours()))
                            || (Rules.AR_5050_Bool.GetValueOrDefault()
                                && !PushQueue_GenerateSummons(rep.Notice.NotIntNo, rep.AutCode, notice.NotOffenceDate.AddMonths(Rules.AR_5050_Int.GetValueOrDefault()).AddDays(-5), rep))))
                        return false;

                    if ((pushGenerateSummons
                        || (!rep.IsSummons && !notice.NoticeStatus.Between(CODE_Officer_Error, CODE_Error_End)
                            && (rep.RepresentationCode.In(REPCODE_REDUCTION, REPCODE_NOCHANGE)
                                || (rep.RepresentationCode.Equals2(REPCODE_WITHDRAW) && notice.NoticeStatus != CODE_CASE_CANCELLED))))
                        && !PushQueue_GenerateSummons(ref rep, false))
                        return false;
                }

                return true;
            }, ex => rep.AddError(ex.ToString())))
            {
                model.IsSuccessful = false;
                return model;
            }

            if (rep.IsChangeOfOffender
                || rep.IsChangeAddress)
                model.AddMessage(RepresentationResx.TheNewOffenderDetailsHaveBeenCaptured);
            else if (rep.IsChangeOfRegNo)
                model.AddMessage(RepresentationResx.TheNewRegistrationDetailsHaveBeenCaptured);
            else if (rep.RepresentationCode.Equals2(SUMMONS_REPCODE_REISSUE))
                model.AddMessage(RepresentationResx.TheSummonsHasBeenWithdrawnForReIssue);
                // if !IsIBM and (RepCode in (3, 13) and NoticeStatus not in (927, 940)), no need to PostRepLetter.
            else if (Rules.AR_6209.GetValueOrDefault()
                || (!model.RepresentationCode.In(REPCODE_WITHDRAW, SUMMONS_REPCODE_WITHDRAW)
                    || notice.NoticeStatus.In(CODE_CASE_CANCELLED, CODE_SUMMONS_WITHDRAWN_CHARGE)))
                PostRepLetter(ref model, true);

            //switch (rep.RepresentationCode.GetValueOrDefault())
            //{
            //    case REPCODE_CHANGEOFFENDER:
            //    case SUMMONS_REPCODE_CHANGEOFFENDER:
            //    case SUMMONS_REPCODE_ADDRESS:
            //        model.AddMessage(RepresentationResx.TheNewOffenderDetailsHaveBeenCaptured);
            //        break;

            //    case SUMMONS_REPCODE_REISSUE:
            //        model.AddMessage(RepresentationResx.TheSummonsHasBeenWithdrawnForReIssue);
            //        break;

            //    default:
            //        PostRepLetter(ref model, true);
            //        break;
            //}

            return model;
        }

        #endregion
    }
}
