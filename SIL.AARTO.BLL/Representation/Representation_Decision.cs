﻿using System;
using System.Collections.Generic;
using SIL.AARTO.BLL.Representation.Model;
using SIL.AARTO.Web.Resource.Representation;
using SIL.ServiceBase;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.Representation
{
    public class Representation_Decision : RepresentationBase
    {
        protected override void OnConstructorLoading()
        {
            Title = RepresentationResx.RepresentationDecision;
            RepAction = RepAction.Decide;
        }

        public override void OnConstructorLoaded() {}

        #region Operations

        #region Disabled

        //protected override bool NewUpdateRepresentation(ref RepresentationModel model)
        //{
        //    var rep = model;
        //    if (!TransactionScope(() =>
        //    {
        //        if (!RepresentationUpdate(ref rep))
        //            return false;

        //        rep.AddMessage(RepresentationResx.RepresentationHasBeenUpdated);

        //        return true;
        //    }, ex => rep.AddError(ex.Message)))
        //    {
        //        model.IsSummons = false;
        //        return false;
        //    }

        //    return true;
        //}

        #endregion

        protected override bool BuildDetails(ref RepresentationModel model, int repCode)
        {
            RepControlStatus.txtOffenceDescrEnabled = false;
            RepControlStatus.txtRecommendVisible = true;
            RepControlStatus.txtReversalReasonVisible = false;
            RepControlStatus.btnSaveVisible = false;
            RepControlStatus.btnDecideVisible = true;
            RepControlStatus.btnReverseVisible = true;
            RepControlStatus.rblCodesEnabled = true;
            RepControlStatus.pnlAmountVisible = true;

            if (isNew)
            {
                RepControlStatus.txtOffenceDescrEnabled = true;
                RepControlStatus.btnReverseVisible = false;

                GenerateNewEmptyRep(ref model);

                var rep = model;
                if (!TransactionScope(() => rep.RepIntNo == 0
                    && SaveTargetCSCode(ref rep, m => RepresentationUpdate(ref m))
                    && PushQueue_RepresentationGracePeriod_Notice(rep.RepIntNo, rep.AutCode, rep),
                    ex => rep.AddError(ex.ToString())))
                    return (model.IsSuccessful = false);
            }
            else
            {
                if (repCode == REPCODE_CHANGEOFFENDER)
                    repCode = REPCODE_NONE;

                if (model.IsSummons
                    && repCode.In(REPCODE_NONE,
                        SUMMONS_REPCODE_NONE,
                        SUMMONS_REPCODE_CHANGEOFFENDER))
                    model.RepresentationCode = SUMMONS_REPCODE_NONE;
                else if (!model.RepCodeList.IsNullOrEmpty()
                    && model.RepCodeList.FindIndex(i => i.Value == Convert.ToString(repCode)) >= 0)
                    model.RepresentationCode = repCode;

                if ((model.IsSummons
                    && !model.Notice.NotFilmTypeIsM
                    && !repCode.In(REPCODE_WITHDRAW, SUMMONS_REPCODE_WITHDRAW, SUMMONS_REPCODE_REDUCTION)
                    && (repCode != SUMMONS_REPCODE_NONE || model.Charge.ChargeStatus != CODE_REP_LOGGED_SUMMONS))
                    // not allowed a 2-steps reversed rep do reverse again.
                    || (model.RepType.Equals2(RepType.N) && repCode.In(REPCODE_NONE, SUMMONS_REPCODE_NONE)))
                {
                    RepControlStatus.btnReverseVisible = false;
                }
                else
                {
                    RepControlStatus.txtReversalReasonVisible = true;
                    RepControlStatus.btnReverseVisible = true;

                    if (model.Notice.UnwithdrawnChargesCount <= 0)
                    {
                        RepControlStatus.btnDecideVisible = false;

                        if (!model.RepCodeList.IsNullOrEmpty())
                            model.RepCodeList.Clear();
                    }
                }
            }

            if (!RetrieveRepresentation(ref model, repCode))
                return false;

            // Do not allow reversal of rep that was decided before case no allocated
            var courtDateList = courtDatesDB.GetCourtDatesDetailsBySumIntNo(model.Notice.SumIntNo);
            var courtRollCreatedDate = !courtDateList.IsNullOrEmpty() && courtDateList[0].CDCourtRollCreatedDate2.HasValue ? courtDateList[0].CDCourtRollCreatedDate2 : null;

            if (model.IsSummons
                && !model.Charge.IsReadOnly
                && RepControlStatus.btnReverseVisible
                && model.RepRecommendDate.HasValue
                && model.Charge.HasCaseNo
                && courtRollCreatedDate.HasValue
                && courtRollCreatedDate.Value.Date >= model.RepRecommendDate.Value.Date)
            {
                RepControlStatus.btnReverseVisible = false;
            }

            if (model.IsSummons
                && !model.Charge.IsReadOnly
                && RepControlStatus.btnReverseVisible)
            {
                var cjtCode = GetJudgementCode(model.Charge.CJTIntNo);
                var sr = GetSummonsRepresentation(model.RepIntNo);
                var previousStatus = sr.SumStatusBeforeRepDecision.GetValueOrDefault() > 0
                    ? sr.SumStatusBeforeRepDecision.Value : (model.Notice.NoticeStatus == CODE_SUMMONS_WITHDRAWN_CHARGE ? sr.SumChargeStatus : model.Notice.NoticeStatus);

                if (model.RepresentationCode.Equals2(SUMMONS_REPCODE_REDUCTION))
                {
                    if (previousStatus < CODE_Case_Number_Generated && (model.Notice.NoticeStatus != previousStatus || model.Charge.HasCaseNo && courtRollCreatedDate.HasValue)
                        || previousStatus >= CODE_Case_Number_Generated && (model.Notice.NoticeStatus != previousStatus || cjtCode > 0))
                        RepControlStatus.btnReverseVisible = false;
                }
                else if (model.RepresentationCode.Equals2(SUMMONS_REPCODE_WITHDRAW))
                {
                    if (model.Notice.NoticeStatus == CODE_SUMMONS_WITHDRAWN_CHARGE)
                    {
                        if (previousStatus < CODE_Case_Number_Generated && model.Charge.HasCaseNo && courtRollCreatedDate.HasValue
                            || previousStatus >= CODE_Case_Number_Generated && cjtCode > 0)
                            RepControlStatus.btnReverseVisible = false;
                    }
                    else if (previousStatus < CODE_Case_Number_Generated && (model.Notice.NoticeStatus != previousStatus || model.Charge.HasCaseNo && courtRollCreatedDate.HasValue)
                        || previousStatus >= CODE_Case_Number_Generated && (model.Notice.NoticeStatus != previousStatus || cjtCode > 0))
                        RepControlStatus.btnReverseVisible = false;
                }
                else if (model.RepresentationCode.Equals2(SUMMONS_REPCODE_REISSUE))
                    RepControlStatus.btnReverseVisible = false;
            }

            return true;
        }

        protected override bool RepresentationUpdate(ref RepresentationModel model)
        {
            return NewUpdateRepresentation(ref model);
        }

        /// <summary>
        ///     Set model's values: RepIntNo, RepresentationCode, RepOfficial, RepRecommend, RepDate, ChgIntNo,
        ///     RepRevisedFineAmount, Charge.ChargeStatus, Charge.RowVersion_Long, RepCurrentFineAmount, ChangeOfOffender,
        ///     RepDetails, LetterTo,
        ///     IsSummons, ChangeOfRegNo, Notice.NotIntNo, ChangeOfRegNoType, Notice.NotRegNo, NewVMIntNo, NewVTIntNo,
        ///     NewVehicleColourDescr, Driver, MIPASource,
        /// </summary>
        protected override bool RepresentationDecide(ref RepresentationModel model)
        {
            if (!CheckModel(ref model, true)) return false;

            SwitchRevAmount(ref model, true);

            if (model.Driver != null)
            {
                if (!string.IsNullOrWhiteSpace(model.Driver.DrvIDNumber))
                    model.Driver.DrvIDType = IDType.Type02;
                else
                {
                    model.Driver.DrvIDNumber = model.Driver.DrvPassport;
                    model.Driver.DrvIDType = IDType.Type01;
                }
            }

            string errorMsg = null;
            var result = representationDB.DecideRepresentation(
                model.RepIntNo,
                model.RepresentationCode.GetValueOrDefault(),
                model.RWRIntNo,
                model.RepOfficial ?? "",
                model.RepRecommend ?? "",
                model.RepDate.GetValueOrDefault(DateTimeNow),
                LastUser,
                model.ChgIntNo,
                model.RepRevisedFineAmount,
                model.Charge.ChargeStatus,
                model.Charge.RowVersion_Long,
                model.RepCurrentFineAmount,
                model.ChangeOfOffender ?? "",
                model.RepDetails ?? "",
                model.LetterTo ?? "",
                model.IsSummons,
                model.ChangeOfRegNo ?? "",
                model.Notice.NotIntNo,
                model.ChangeOfRegNoType ?? "",
                model.Notice.NotRegNo ?? "",
                model.NewVMIntNo,
                model.NewVTIntNo,
                model.NewVehicleColourDescr ?? "",
                model.Driver,
                false, 0, false,
                model.MIPASource ?? "",
                false, ref errorMsg,
                Rules.AR_4660.GetValueOrDefault(),
                Rules.AR_9060 ?? N,
                Rules.AR_9120 ?? N,
                Rules.AR_9130.GetValueOrDefault(12),
                RepAction.Is(RepAction.PresentationOfDocument));

            if (result == null || result.Count <= 0 || result[0] <= 0)
            {
                model.AddError(string.Format(RepresentationResx.TheRepresentationCannotBeDecidedAsThereWasAnError, errorMsg));
                return false;
            }

            model.RepIntNo = result[0];
            return model.IsSuccessful;
        }

        protected override bool RepresentationReverse(ref RepresentationModel model)
        {
            return ReverseRepresentation(ref model);
        }

        #endregion
    }
}
